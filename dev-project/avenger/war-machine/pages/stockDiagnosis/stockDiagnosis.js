const pageInterceptor = require('../../components/pageInterceptor').pageInterceptor;
const constant = require('../../components/constant');
const request = require('../../components/request');

// Page({
pageInterceptor({

  /**
   * 页面的初始数据
   */
  data: {
    /**
     * 跳转到这个页面的路径
     */
    path: undefined,
    /**
     * 短期策略
     */
    shortPeriodStrategy: undefined,
    /**
     * 中期策略
     */
    middlePeriodStrategy: undefined,
    /**
     * 长期策略
     */
    longPeriodStrategy: undefined,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 存储将跳转过来的参数
    this.setData({
      path: options.path
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /**
   * 诊断个股
   * @param {*} e 
   */
  diagnose: function (e) {
    let codeOrName = e.detail.value.codeOrName;
    console.info(`股票的代码或名字为：${codeOrName}`);

    // 调用诊断接口
    let user = wx.getStorageSync('user');
    let _user = JSON.stringify(user);
    request.get(constant.api.data.prefix + constant.api.data.url.diagnose.replace('{codeOrName}', codeOrName),
      null, {
        'content-type': 'application/json',
        'user': _user,
      }).action((res) => {
        console.info(`股票【${codeOrName}】的诊断数据为${res}`);
        let mdlStockAnalysis = JSON.parse(res.data.data);

        // 判断趋势
        // 短期趋势，kd指标
        if(mdlStockAnalysis.KD_TREND == 1){
          this.setData({
            shortPeriodStrategy: '持股'
          });
        }
        if(mdlStockAnalysis.KD_TREND == 0){
          this.setData({
            shortPeriodStrategy: '趋势不明'
          });
        }
        if(mdlStockAnalysis.KD_TREND == -1){
          this.setData({
            shortPeriodStrategy: '持币'
          });
        }
        // 中期趋势，macd指标
        if(mdlStockAnalysis.MACD_TREND == 1){
          this.setData({
            middlePeriodStrategy: '持股'
          });
        }
        if(mdlStockAnalysis.MACD_TREND == 0){
          this.setData({
            middlePeriodStrategy: '趋势不明'
          });
        }
        if(mdlStockAnalysis.MACD_TREND == -1){
          this.setData({
            middlePeriodStrategy: '持币'
          });
        }
        // 长期趋势，ma指标
        if(mdlStockAnalysis.MA_TREND == 1){
          this.setData({
            longPeriodStrategy: '持股'
          });
        }
        if(mdlStockAnalysis.MA_TREND == 0){
          this.setData({
            longPeriodStrategy: '趋势不明'
          });
        }
        if(mdlStockAnalysis.MA_TREND == -1){
          this.setData({
            longePeriodStrategy: '持币'
          });
        }

        // 用这种方式返回给request.js接口的url，用于解决接口格式为/api/diagnose/{codeOrName}这种情况
        return { 'url': constant.api.data.prefix + constant.api.data.url.diagnose };
    });
  }
  // })
}, true)