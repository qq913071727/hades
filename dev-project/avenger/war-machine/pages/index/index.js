const constant = require('../../components/constant');
const request = require('../../components/request');
const pageInterceptor = require('../../components/pageInterceptor').pageInterceptor;

Page({
// pageInterceptor({
  data: {
    /**
     * 跳转到这个页面的路径
     */
    path: undefined,
    /**
     * 判断小程序的API，回调，参数，组件等是否在当前版本可用
     */
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    /**
     * 是否显示授权视图(view)
     */
    autViewShow: false,
    /**
     * 登录成功后，跳转到想去的页面
     */
    afterLogin: function(){}
  },
  
  onLoad: function (options) {

  },

  /**
   * 登录按钮
   */
  login: function (e) {
    this.authenticate(() => {});
  },

  /**
   * 授权
   * 如果用户允许授权，则调用/api/auth接口，获取认证信息，然后隐藏授权视图，显示业务相关视图。
   * 如果用户拒绝授权，则无法进入小程序，仍然停留在授权视图。
   */
  bindGetUserInfo: function (e) {
    if (e.detail.userInfo) {
      // 用户按了允许授权按钮
      console.log(`用户按了允许授权按钮，用户的信息如下：${e.detail.userInfo}`);

      // 发送code到后台换取openid, session_key, unionid
      let _this = this;
      wx.getUserInfo({
        success: function (res) {
          let that = _this;
          // 调用微信的wx.login接口，从而获取code
          wx.login({
            success: res => {
              // 获取到用户的code
              console.log(`用户的code: ${res.code}`);
              wx.setStorageSync('code', res.code);

              let code = res.code;
              let _that = that;

              // 为了审核通过，暂时先用下面代码替换登录过程
              // 小程序端存储loginKey
              wx.setStorageSync("user", {
                errcode: 0,
                errmsg: "ddd",
                openid: "222",
                session_key: "bbb",
                stockCodeList:  ["300827", "605111", "000004", "000005"],
                token: "f30304e6-3fb1-428c-bce4-c979343e001a",
                unionid: "ccc",
                userId: "5f7e5577b571340fded1da5d"
              });
              // 授权成功后，隐藏授权页面，显示业务相关的页面
              _that.setData({
                authViewShow: false
              });
              // 跳转到想要去的页面
              _that.data.afterLogin();

              // 为了审核通过，暂时先将下面调用接口的代码注释掉
              // request.post(constant.api.service.prefix + constant.api.service.url.auth, {
              //   'code': code
              // }, {
              //   'content-type': 'application/json',
              // }).action((res) => {
              //   console.log(`接口${constant.api.service.url.auth}：${res}`);
              //   // 小程序端存储loginKey
              //   wx.setStorageSync("user", res.data.data);
              //   // 授权成功后，隐藏授权页面，显示业务相关的页面
              //   _that.setData({
              //     authViewShow: false
              //   });
              //   // 跳转到想要去的页面
              //   _that.data.afterLogin();
              // }).catch(err => {
              //   console.error(err);
              // });
            }
          });
        }
      });
    } else {
      // 用户按了拒绝按钮
      wx.showModal({
        title: '警告',
        content: '您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!',
        showCancel: false,
        confirmText: '返回首页',
        success: function (res) {
          // 用户没有授权成功，不需要改变authViewShow的值
          if (res.confirm) {
            console.log('用户点击了“返回授权”');
          }
          // 跳转回首页
          wx.navigateTo({
            url: '/pages/index/index?path=/pages/index/index',
          });
        }
      });
    }
  },

  /**
   * 实时数据
   */
  clickRealTimeData: function (e) {
    this.setData({
      afterLogin: () => {
        wx.navigateTo({
          url: '/pages/realTimeData/realTimeData?path=/pages/realTimeData/realTimeData',
        });
      }
    });
    this.authenticate(() => {
      wx.navigateTo({
        url: '/pages/realTimeData/realTimeData?path=/pages/realTimeData/realTimeData',
      });
    });
  },

  /**
   * 个股诊断
   */
  clickStockDiagnosis: function (e) {
    console.log('clickStockDiagnosis');
    wx.navigateTo({
      url: '/pages/stockDiagnosis/stockDiagnosis?path=/pages/stockDiagnosis/stockDiagnosis',
    });
  },

  /**
   * 股票推荐
   */
  clickStockRecommendation: function (e) {
    console.log('clickStockRecommendation');
    wx.navigateTo({
      url: '/pages/stockRecommendation/stockRecommendation?path=/pages/stockRecommendation/stockRecommendation',
    });
  },

  /**
   * 量化选股
   */
  clickQuantitativeStockSelection: function (e) {
    console.log('clickQuantitativeStockSelection');
    wx.navigateTo({
      url: '/pages/quantitativeStockSelection/quantitativeStockSelection?path=/pages/quantitativeStockSelection/quantitativeStockSelection',
    });
  },

  /**
   * 留言板
   */
  clickMessageBoard: function (e) {
    this.setData({
      afterLogin: () => {
        wx.navigateTo({
          url: '/pages/messageBoard/messageBoard?path=/pages/messageBoard/messageBoard',
        });
      }
    });
    this.authenticate(() => {
      console.log('clickMessageBoard');
      wx.navigateTo({
        url: '/pages/messageBoard/messageBoard?path=/pages/messageBoard/messageBoard',
      });
    });
  },

  /**
   * 触按设置
   */
  clickConfiguration: function (e) {
    console.log('clickConfiguration');
    wx.navigateTo({
      url: '/pages/configuration/configuration?path=/pages/configuration/configuration',
    });
  },

  /**
   * 判断用户是否已经被授权。
   * 如果没有被授权，则显示授权视图；如果已经被授权，不需要显示授权页面，跳转至对应的页面。
   * 
   * wx.getSetting 在 未拒绝 和 未同意 状态下 ，success: (res) => { } 的res.authSetting{}值是空的。
   * 在getUserInfo()后，点击拒绝后 wx.getSetting的fail:(res)=>{}触发，res.authSetting['scope.userInfo'] 的值是false ，
   * 表示scope.userInfo这个权限没有授权。
   * 在getUserInfo()后，点击同意后 wx.getSetting的success:(res)=>{}触发，res.authSetting['scope.userInfo'] 的值是true，
   * 表示scope.userInfo这个权限已经授权。
   */
  authenticate: function(callback){
    let that = this;
    // 查看是否授权
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          // 用户已经授权过，不需要显示授权页面，所以不需要改变authViewShow的值
          console.log('用户已经授权过，不需要显示授权页面，所以不需要改变authViewShow的值');
          // 执行回调函数，跳转页面
          callback();
          // wx.showModal({
          //   title: '提示',
          //   content: '您已经登录',
          //   showCancel: false,
          //   confirmText: '关闭',
          //   success: function (res) {
              
          //   }
          // });
        } else {
          // 用户没有授权, 改变authViewShow的值，显示授权页面
          console.log('用户没有授权, 改变authViewShow的值，显示授权页面');
          that.setData({
            authViewShow: true
          });
        }
      }
    });
  }
})
// }, true)