const pageInterceptor = require('../../components/pageInterceptor').pageInterceptor;

// Page({
pageInterceptor({
	/**
	 * 页面的初始数据
	 */
	data: {
		msgData: []
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	changeInputValue(ev) {
		this.setData({
			inputVal: ev.detail.value
		})
	},
	//删除留言
	DelMsg(ev) {
		var n = ev.target.dataset.index;

		var list = this.data.msgData;
		list.splice(n, 1);

		this.setData({
			msgData: list
		});
	},
	//添加留言
	addMsg() {
		var list = this.data.msgData;
		list.push({
			msg: this.data.inputVal
		});
		//更新
		this.setData({
			msgData: list,
			inputVal: ''
		});
	},
	// })
}, true)