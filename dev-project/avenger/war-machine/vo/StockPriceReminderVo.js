/**
 * 设置估计提醒时的参数对象
 */
class StockPriceReminderVo {
	constructor() {
		/**
		 * 股票code
		 */
		this.stockCode = null;
		/**
		 * 股价上涨达到
		 */
		this.stockPriceUp = null;
		/**
		 * 股价下跌达到
		 */
		this.stockPriceDown = null;
		/**
		 * 日涨跌幅达到
		 */
		this.changeRange = null;
	}
}

exports.StockPriceReminderVo = StockPriceReminderVo;