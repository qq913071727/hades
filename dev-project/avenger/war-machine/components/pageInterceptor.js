const constant = require('../components/constant');

exports.pageInterceptor = function (pageObj, flag = false) {
	if (flag) {
		let _onShow = pageObj.onShow;
		pageObj.onShow = function () {
			let that = this;
			let user = wx.getStorageSync('user');
			let pageInfo = {
				userId: user.userId,
				path: this.options.path,
				queryString: this.options
			};
			// 调用页面访问接口
			savePageVisitLog(pageInfo);

			// 展示页面
			_onShow.call(that);
		}
	}
	return Page(pageObj)
};

/**
 * 添加页面访问日志
 */
function savePageVisitLog(pageInfo) {
	wx.request({
		url: constant.api.service.prefix + constant.api.service.url.savePageVisitLog,
		header: {
			'content-type': 'application/json'
		},
		data: pageInfo,
		method: 'post',
		success: function (res) {}
	});
}