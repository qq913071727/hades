/**
 * 格式化日期格式
 */
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

/**
 * 验证价格：包括两位小数
 * @param {*} val 
 */
const validatePrice = val => {
  var patten = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
  return patten.test(val);
}

/**
 * 验证百分比
 * @param {*} val 
 */
const validatePercentage = val => {
  var patten = /^(?:\d{1,2}(\.\d{1,2})?|100)$/;
  return patten.test(val);
}

module.exports = {
  formatTime,
  validatePrice,
  validatePercentage,
}
