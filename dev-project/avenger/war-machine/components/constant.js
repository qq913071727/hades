module.exports = {
  /**
   * api相关
   */
  api: {
    /**
     * 服务类接口。galaxy_guardians服务api接口
     */
    service: {
      prefix: 'http://localhost:9000',
      url: {
        auth: '/api/auth',
        saveLoginLog: '/api/loginLog/save',
        saveApiCallLog: '/api/apiCallLog/save',
        savePageVisitLog: '/api/pageVisitLog/save'
      }
    },
    /**
     * 数据类接口。iron-man服务api接口
     */
    data: {
      prefix: 'http://localhost:3000',
      url: {
        findStockTransactionList: '/api/find/stockTransactionDataList',
        subscribeStock: '/api/subscribe/stock',
        deleteSubscription: '/api/delete/subscription',
        diagnose: '/api/diagnose/{codeOrName}',
        remindStockPrice: '/api/remind/stockPrice'
      }
    }
  },
  /**
   * 定时相关
   */
  timing: {
    /**
     * 定时获取股票交易数据的时间
     */
    timeForGettingViewStockTransactionDataRegularly: 1000 * 10,
  },
  /**
   * http的状态相关
   */
  httpStatus: {
    /**
     * 未经授权
     */
    unauthorized: 401,
  },
};