/**
 * 股价提醒--弹出窗口
 */
class ViewStockPriceReminder {
	constructor(){
		// 股票code
		this.stockCode = null;
		// 股价上涨达到
		/**
		 * 名称
		 */
		this.stockPriceUpName = 'stockPriceUp';
		/**
		 * 是否被勾选
		 */
		this.stockPriceUpCheck = false;
		/**
		 * 价格
		 */
		this.stockPriceUp = null;
		/**
		 * 验证是否通过
		 */
		this.stockPriceUpValid = false;
		/**
		 * 验证不通过时的错误提示
		 */
		this.stockPriceUpInvalidMessage = '\'股价上涨达到\'不能为空';

		// 股价下跌达到
		this.stockPriceDownName = 'stockPriceDown';
		this.stockPriceDownCheck = false;
		this.stockPriceDown = null;
		this.stockPriceDownValid = false;
		this.stockPriceDownInvalidMessage = '\'股价下跌达到\'不能为空';

		// 日涨跌幅达到
		this.changeRangeName = 'changeRangeCheck';
		this.changeRangeCheck = false;
		this.changeRange = null;
		this.changeRangeValid = false;
		this.changeRangeInvalidMessage = '\'日涨跌幅达到\'不能为空';
	}
}

exports.ViewStockPriceReminder = ViewStockPriceReminder;