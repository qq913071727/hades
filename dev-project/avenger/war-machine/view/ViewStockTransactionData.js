/**
 * StockTransactionData的封装类
 */
class ViewStockTransactionData {
	constructor(stockTransactionData) {
		this.code = stockTransactionData.code;
		this.name = stockTransactionData.name;
		this.newestPrice = stockTransactionData.newestPrice;
		this.upDownPrice = stockTransactionData.upDownPrice;
		this.upDownPercentage = stockTransactionData.upDownPercentage;
		this.checked = false;
	}

	/**
	 * 将StockTransactionData的list数组转换为ViewStockTransactionData的list数组
	 */
	static toViewStockTransactionDataList(stockTransactionDataList) {
		let viewStockTransactionDataList = [];
		for (let i = 0; i < stockTransactionDataList.length; i++) {
			let viewStockTransactionData = new ViewStockTransactionData(stockTransactionDataList[i]);
			viewStockTransactionDataList.push(viewStockTransactionData);
		}
		return viewStockTransactionDataList;
	}
}

exports.ViewStockTransactionData = ViewStockTransactionData;