package org.avenger.bucky.constant;

import java.util.ArrayList;
import java.util.List;

public class Regex {

    /**
     * 一些正则表达式中有意义的符号
     */
    public static final List<String> KEY_MARK = new ArrayList<>();

    static {
        KEY_MARK.add("(");
        KEY_MARK.add(")");
        KEY_MARK.add("{");
        KEY_MARK.add("}");
        KEY_MARK.add("[");
        KEY_MARK.add("]");
    }
}
