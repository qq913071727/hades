package org.avenger.bucky.pojo;

import lombok.Data;

import java.util.List;

/**
 * 用于备份所有的数据库中的所有集合
 */
@Data
public class BackupAllData {

    /**
     * 数据库名称
     */
    private String databaseName;

    /**
     * 集合名称列表
     */
    private List<String> collectionList;
}
