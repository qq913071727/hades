package org.avenger.bucky.task;

import org.avenger.bucky.handler.TestQuestionHandler;

/**
 * 任务：查找相同或相似的TestQuestion对象
 */
public class FindSimilarTestQuestionTask {

    public static void main(String[] args){
        TestQuestionHandler testQuestionHandler = new TestQuestionHandler();
        testQuestionHandler.findSimilarTestQuestionByRegex();
    }
}
