package org.avenger.bucky.constant;

/**
 * 题型常量类
 */
public class QuestionType {

    /**
     * 单选题
     */
    public static final String SINGLE_CHOICE_QUESTION = "单选题";

    /**
     * 多选题
     */
    public static final String MULTIPLE_CHOICE_QUESTION = "多选题";

    /**
     * 简答题
     */
    public static final String SIMPLE_ANSWER_QUESTION = "简答题";
}
