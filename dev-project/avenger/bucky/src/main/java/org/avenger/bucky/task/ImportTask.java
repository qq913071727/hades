package org.avenger.bucky.task;

import org.avenger.bucky.handler.DataHandler;

/**
 * 任务：导如文件中的数据
 */
public class ImportTask {

    public static void main(String[] args){
        DataHandler dataHandler = new DataHandler();
        dataHandler.importDataFromJsonFile();
    }
}
