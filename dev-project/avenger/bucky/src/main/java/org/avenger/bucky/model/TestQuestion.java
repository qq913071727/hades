package org.avenger.bucky.model;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.avenger.bucky.constant.QuestionFormat;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * 试题类
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TestQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private ObjectId id;

    /**
     * 分类。包括：java基础
     */
    private String classification;

    /**
     * 题型。包括：单选题、多选题、简答题
     */
    private String questionType;

    /**
     * 题干
     */
    private String subject;

    /**
     * 选项列表
     */
    private List<Option> optionList;

    /**
     * 正确答案
     */
    private String answer;

    /**
     * 解释
     */
    private String explain;

    /**
     * 是否还在使用
     */
    private String inUse;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 格式化试题的内容，以便打印
     * @param number
     * @return
     */
    public String format(Integer number){
        if (null != this){
            StringBuffer stringBuffer = new StringBuffer();
            // 题干
            stringBuffer.append(number).append(". ").append(this.getSubject()).append("\n");
            // 选项
            List<Option> optionList = this.getOptionList();
            if (null != optionList && optionList.size() > 0){
                for (int i=0; i<optionList.size(); i++){
                    Option option = optionList.get(i);
                    stringBuffer.append("    " + option.getNumber()).append(". ").append(option.getContent()).append("\n");
                }
            }
            // 分割线
            stringBuffer.append(QuestionFormat.SPLIT_LINE);
            // 正确答案
            stringBuffer.append("正确答案是：").append(this.getAnswer()).append("\n");
            // 解释
            stringBuffer.append("解释：").append(this.getExplain() == null ? "无" : this.getExplain()).append("\n");
            // 空行
            stringBuffer.append(QuestionFormat.EMPTY_LINE);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 将TestQuestion类型对象转换为Document类型对象
     * @return
     */
    public Document toDocument(){
        if (null != this){
            Document document = new Document();
            document.put("classification", this.getClassification());
            document.put("question_type", this.getQuestionType());
            document.put("subject", this.getSubject());

            List<Option> optionList = this.getOptionList();
            if (null != optionList && optionList.size() > 0){
                List<DBObject> dbObjectList = new ArrayList<DBObject>();
                for (int i=0; i<optionList.size(); i++){
                    Option option = optionList.get(i);
                    BasicDBObject basicDBObject = new BasicDBObject();
                    basicDBObject.put("number", option.getNumber());
                    basicDBObject.put("content", option.getContent());
                    dbObjectList.add(basicDBObject);
                }
                document.put("option_list", dbObjectList);
            }

            document.put("answer", this.getAnswer());
            document.put("explain", this.getExplain());
            document.put("in_use", this.getInUse());
            document.put("create_time", this.getCreateTime());
            document.put("update_time", this.getUpdateTime());
            return document;
        } else {
            return null;
        }
    }
}
