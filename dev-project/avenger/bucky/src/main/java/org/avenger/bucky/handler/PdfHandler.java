package org.avenger.bucky.handler;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.avenger.bucky.constant.QuestionType;
import org.avenger.bucky.model.TestQuestion;

/**
 * pdf处理类
 */
public class PdfHandler extends AbstractHandler {

    private static Logger log = LogManager.getLogger(PdfHandler.class);

    /**
     * 创建计算机试题pdf文档
     * @param pdfTtfPath
     * @param pdfOutputPath
     */
    public void generateExam(String pdfTtfPath, String pdfOutputPath) {
        log.info("创建计算机试题pdf文档");

        BaseFont bfCN;
        Document document = null;

        // 查找单选题
        List<TestQuestion> singleChoiceQuestionList = mongoManager.findByQuestionType(
            QuestionType.SINGLE_CHOICE_QUESTION);
        // 查找多选题
        List<TestQuestion> multipleChoiceQuestionList = mongoManager.findByQuestionType(
            QuestionType.MULTIPLE_CHOICE_QUESTION);
        // 查找简答题
        List<TestQuestion> simpleAnswerQuestionList = mongoManager.findByQuestionType(
            QuestionType.SIMPLE_ANSWER_QUESTION);

        try {
            bfCN = BaseFont.createFont(
                pdfTtfPath,
                BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            // 章的字体
            Font chFont = new Font(bfCN, 26, Font.BOLD, BaseColor.BLACK);
            // 节的字体
            Font secFont = new Font(bfCN, 14, Font.BOLD, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 12, Font.NORMAL, BaseColor.BLACK);

            document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(pdfOutputPath));
            document.open();

            int chNum = 1;

            Chapter chapter = new Chapter(new Paragraph("试题", chFont), chNum++);

            // 单选题
            Section singleChoiceQuestionSection = chapter.addSection(new Paragraph(QuestionType.SINGLE_CHOICE_QUESTION, secFont));
            singleChoiceQuestionSection.setIndentation(10);
            singleChoiceQuestionSection.setIndentationLeft(10);
            singleChoiceQuestionSection.setBookmarkOpen(false);
            singleChoiceQuestionSection.setNumberStyle(Section.NUMBERSTYLE_DOTTED_WITHOUT_FINAL_DOT);
            for (int i = 0; i < singleChoiceQuestionList.size(); i++) {
                singleChoiceQuestionSection.add(new Paragraph(singleChoiceQuestionList.get(i).format(i + 1), textFont));
            }

            // 多选题
            Section multipleChoiceQuestionSection = chapter.addSection(new Paragraph(QuestionType.MULTIPLE_CHOICE_QUESTION, secFont));
            multipleChoiceQuestionSection.setIndentation(10);
            multipleChoiceQuestionSection.setIndentationLeft(10);
            multipleChoiceQuestionSection.setBookmarkOpen(false);
            multipleChoiceQuestionSection.setNumberStyle(Section.NUMBERSTYLE_DOTTED_WITHOUT_FINAL_DOT);
            for (int i = 0; i < multipleChoiceQuestionList.size(); i++) {
                multipleChoiceQuestionSection.add(new Paragraph(multipleChoiceQuestionList.get(i).format(i + 1), textFont));
            }

            // 简答题
            Section simpleAnswerQuestionSection = chapter.addSection(new Paragraph(QuestionType.SIMPLE_ANSWER_QUESTION, secFont));
            simpleAnswerQuestionSection.setIndentation(10);
            simpleAnswerQuestionSection.setIndentationLeft(10);
            simpleAnswerQuestionSection.setBookmarkOpen(false);
            simpleAnswerQuestionSection.setNumberStyle(Section.NUMBERSTYLE_DOTTED_WITHOUT_FINAL_DOT);
            for (int i = 0; i < simpleAnswerQuestionList.size(); i++) {
                simpleAnswerQuestionSection.add(new Paragraph(simpleAnswerQuestionList.get(i).format(i + 1), textFont));
            }

            document.add(chapter);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }
    }
}
