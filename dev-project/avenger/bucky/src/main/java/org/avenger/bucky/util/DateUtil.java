package org.avenger.bucky.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date类的工具类
 */
public class DateUtil {

    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String str = df.format(date);
        return str;
    }
}
