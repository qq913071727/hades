package org.avenger.bucky.handler;

import org.avenger.bucky.constant.ConfigFile;
import org.avenger.bucky.manager.MongoManager;
import org.avenger.bucky.util.PropertiesUtil;

/**
 * 抽象处理器
 */
public class AbstractHandler {

    protected String ip;
    protected String port;
    protected String database;

    protected MongoManager mongoManager;

    public AbstractHandler(){
        ip = PropertiesUtil.getValue(ConfigFile.BUCKY_PROPERTIES, "mongodb.ip");
        port = PropertiesUtil.getValue(ConfigFile.BUCKY_PROPERTIES, "mongodb.port");
        database = PropertiesUtil.getValue(ConfigFile.BUCKY_PROPERTIES, "mongodb.database");
        mongoManager = new MongoManager(ip, Integer.parseInt(port), database);
    }

    public AbstractHandler(MongoManager mongoManager) {
        this.mongoManager = mongoManager;
    }
}
