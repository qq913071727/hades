package org.avenger.bucky.constant;

/**
 * 集合的常量类
 */
public class Collection {

    /**
     * 计算机笔试试题
     */
    public static final String TEST_QUESTION = "test_question";

    /**
     * 忽略的选项
     */
    public static final String IGNORE_OPTION = "ignore_option";
}
