package org.avenger.bucky.task;

import org.avenger.bucky.constant.ConfigFile;
import org.avenger.bucky.handler.PdfHandler;
import org.avenger.bucky.util.PropertiesUtil;

/**
 * 任务：创建计算机试题pdf文档
 */
public class GenerateTestQuestionTask {

    public static void main(String[] args) {
        String pdfTtfPath = PropertiesUtil.getValue(ConfigFile.BUCKY_PROPERTIES, "pdf.ttf.path");
        String pdfOutputPath = PropertiesUtil.getValue(ConfigFile.BUCKY_PROPERTIES, "pdf.output.path");

        PdfHandler pdfHandler = new PdfHandler();
        pdfHandler.generateExam(pdfTtfPath, pdfOutputPath);
    }
}
