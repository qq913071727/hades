package org.avenger.bucky.task;

import org.avenger.bucky.handler.TestQuestionHandler;

/**
 * 任务：向mongodb中插入TestQuestion对象
 */
public class InsertTestQuestionTask {

    public static void main(String[] args){
        TestQuestionHandler testQuestionHandler = new TestQuestionHandler();
        testQuestionHandler.insertTestQuestion();
    }
}
