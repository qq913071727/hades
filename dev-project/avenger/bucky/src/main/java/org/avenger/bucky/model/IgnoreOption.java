package org.avenger.bucky.model;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 忽略的选项
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IgnoreOption implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内容
     */
    private String content;
}
