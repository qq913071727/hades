package org.avenger.bucky.model;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 试题的选项
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Option implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号。包括A、B、C、D、E等
     */
    private String number;

    /**
     * 选项的内容
     */
    private String content;
}
