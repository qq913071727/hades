package org.avenger.bucky.constant;

/**
 * 属性文件常量类
 */
public class ConfigFile {

    /**
     * bucky.properties配置文件
     */
    public static final String BUCKY_PROPERTIES = "C:\\github-repository\\hades\\dev-project\\avenger\\bucky\\src\\main\\resources\\config\\bucky.properties";

    /**
     * backup-all-data.json配置文件
     */
    public static final String BACKUP_ALL_DATA_JSON = "C:\\github-repository\\hades\\dev-project\\avenger\\bucky\\src\\main\\resources\\config\\backup-all-data.json";

    /**
     * backup-all-data.properties配置文件
     */
    public static final String BACKUP_ALL_DATA_PROPERTIES = "C:\\github-repository\\hades\\dev-project\\avenger\\bucky\\src\\main\\resources\\config\\backup-all-data.properties";

    /**
     * insert.json文件
     */
    public static final String INSERT_JSON = "C:\\github-repository\\hades\\dev-project\\avenger\\bucky\\src\\main\\resources\\insert.json";
//
//    /**
//     * export.json文件
//     */
//    public static final String EXPORT_JSON = "E:\\github-repository\\hades\\dev-project\\avenger\\bucky\\src\\main\\resources\\export.json";

}
