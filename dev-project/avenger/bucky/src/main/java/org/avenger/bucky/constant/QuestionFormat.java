package org.avenger.bucky.constant;

/**
 * 试题格式常量类
 */
public class QuestionFormat {

    /**
     * 分割线
     */
    public static final String SPLIT_LINE = "-------------------------------------------------\n";

    /**
     * 空行
     */
    public static final String EMPTY_LINE = "\n";

}
