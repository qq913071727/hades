package org.avenger.bucky.task;

import org.avenger.bucky.handler.DataHandler;

/**
 * 任务：导出所有数据库的文档到文件
 */
public class ExportAllDataTask {

    public static void main(String[] args){
        DataHandler dataHandler = new DataHandler();
        dataHandler.exportAllDataIntoJsonFile();
    }
}
