import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import org.bson.types.ObjectId;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PictureSaveTest {

    public static void main(String[] args) {
//        MongoClient client = new MongoClient("127.0.0.1", 27017);
//        DB db = client.getDB("files");
//        GridFS fs = new GridFS(db);
//        try {
//            GridFSInputFile gridFSInputFile = fs.createFile(new File("E://blog.csdn.net_z_ryan_article_details_79685072.png"));
//            gridFSInputFile.setFilename("db_test.png");
//            gridFSInputFile.save();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        String fileId = "61de469ca1bf7528f4be78a8";//这里可以通过参数取代
        try {
            MongoClient client = new MongoClient("127.0.0.1", 27017);
            DB db = client.getDB("files");
            GridFS fs = new GridFS(db);
            GridFSDBFile gridFSDBFile = fs.findOne(new ObjectId(fileId));
            File file = new File("F://111.png");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            gridFSDBFile.writeTo(fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
