const xlsx = require('node-xlsx');
const fs = require('fs');

/**
 * 读取excel文件的内容
 * @param {*} path 
 */
module.exports = {
  read(path) {
    const obj = xlsx.parse(path);
    const excelObj = obj[0].data;

    let data = [];
    for (let i in excelObj) {
      let arr = [];
      let value = excelObj[i];
      for (let j in value) {
        arr.push(value[j]);
      }
      data.push(arr);
    }
    return data;
  }
};
