const MongoClient = require("mongodb").MongoClient;
const logger = require('../components/logger');

module.exports = {
  url: 'mongodb://localhost:27017/black-widow',
  database: 'black-widow',
  collection: 'stock_info',
  /**
   * 创建集合
   */
  createCollection(collectionName) {
    MongoClient.connect(this.url, this.database, collectionName, (err, client) => {
      client.db(this.database).createCollection(collectionName).then(
        function (result) {
          logger.info(`集合${collection}创建成功`);
        },
        function (err) {
          logger.warn(`集合${collection}创建失败，失败原因：` + err.message);
        }
      )
    })
  },
  /**
   * 获取所有集合
   */
  listCollections() {
    MongoClient.connect(this.url, this.database, (err, client) => {
      client.db(this.database).listCollections().toArray().then(
        function (tables) {
          logger.info('集合名称如下：');
          tables.forEach(function (value, index, ts) {
            logger.info(value.name);
          })
        },
        function (err) {
          logger.warn('获取所有集合失败，失败原因：' + err.message);
        })
    });
  },
  /**
   * 向集合中插入数据
   * @param {*} db 
   * @param {*} collectionName 
   * @param {*} obj 
   */
  insertDocument(document) {
    let _this = this;
    MongoClient.connect(this.url, this.database, this.collection, (err, client) => {
      let that = _this;
      client.db(_this.database).collection(_this.collection).insertOne(document).then(
        function (result) {
          logger.info(`向数据库${that.database}的集合${that.collection}中` +
            `插入文档${JSON.stringify(document)}成功`);
        },
        function (err) {
          logger.warn(`向数据库${that.database}的集合${that.collection}中` +
            `插入文档${JSON.stringify(document)}失败，失败原因：` + err.message);
        })
    });
  },
  /**
   * 向集合中批量插入数据
   * @param {*} documentList 
   */
  insertDocumentList(documentList) {
    let _this = this;
    MongoClient.connect(this.url, this.database, this.collection, (err, client) => {
      const collection = client.db(_this.database).collection(_this.collection);
      if (documentList) {
        for (let index in documentList) {
          let that = _this;
          collection.insertOne(documentList[index]).then(
            function (result) {
              logger.info(`向数据库${that.database}的集合${that.collection}中` +
                `插入文档${JSON.stringify(documentList[index])}成功`);
            },
            function (err) {
              logger.warn(`向数据库${that.database}的集合${that.collection}中` +
                `插入文档${JSON.stringify(documentList[index])}失败，失败原因：` + err.message);
            })
        }
      } else {
        logger.warn('documentList参数为空')
      }
    })
  },
  /**
   * 获取集合中所有数据
   */
  findAllDocument() {
    let _this = this;
    return new Promise((resolve, reject) => {
      let that = _this;
      MongoClient.connect(_this.url, _this.database, _this.collection, function (err, client) {
        client.db(that.database).collection(that.collection).find({}).toArray().then(
          function (result) {
            resolve(result);
          },
          function (err) {
            logger.warn('获取集合中所有数据失败，失败原因：' + err.message);
          })
      });
    });
  },
  /**
   * 修改数据
   * @param {*} document 
   */
  updateDocument(document) {
    let _this = this;
    MongoClient.connect(this.url, this.database, this.collection, function (err, client) {
      client.db(_this.database).collection(_this.collection).find(document).toArray().then(
        function (result) {
        },
        function (err) {
        })
    });
  },
  /**
   * 按条件查询
   * @param {*} document 
   */
  findDocument(document) {
    let _this = this;
    MongoClient.connect(this.url, this.database, this.collection, function (err, client) {
      client.db(_this.database).collection(_this.collection).find(document).toArray().then(
        function (result) {
        },
        function (err) {
        })
    });
  },
  /**
   * 删除数据
   * @param {*} document 
   */
  deleteDocument(document) {
    let _this = this;
    MongoClient.connect(this.url, this.database, this.collection, function (err, client) {
      client.db(_this.database).collection(_this.collection).deleteMany(document).then(
        function (result) {
        },
        function (err) {
        })
    });
  }
};