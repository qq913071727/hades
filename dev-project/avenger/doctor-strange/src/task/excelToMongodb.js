const excelUtil = require('../excel/excelUtil');
const mongodbUtil = require('../mongodb/mongodbUtil');

// 从excel文件中查找数据，然后插入到Mongodb中（数据库：black-widow，集合：stock_info）
let stockInfoRow = excelUtil.read('D:/github-workspace/Stock.csv');
let stockTransactionDataList = [];
let stockTransactionData = {};
for (let i in stockInfoRow) {
  let stockInfoColumn = stockInfoRow[i];
  let code;
  let name;
  let urlParam;
  for (let j in stockInfoColumn) {
    if (j == 0) {
      code = stockInfoColumn[j].toLowerCase().slice(2);
      urlParam = stockInfoColumn[j].toLowerCase();
    }
    if (j == 1) {
      name = stockInfoColumn[j];
    }
  }
  stockTransactionData = { code, name, 'url_param': urlParam };
  stockTransactionDataList.push(stockTransactionData);
}

// 向mongodb中插入数据
mongodbUtil.insertDocumentList(stockTransactionDataList);