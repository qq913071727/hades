/**
 * redis数据库连接信息
 */
const REDIS_CONNECTION_INFO = {
	port: 6379,
	host: '127.0.0.1',
	options: { db: 5 },
};

/**
 * mdl_stock_analysis表中的数据存储在redis中的默认时间，2个小时
 */
const MDL_STOCK_ANALYSIS_DEFAULT_EXPIRE_TIME = 2 * 60 * 60;

/**
 * 在redis中的，mdl_stock_analysis的最大日期。可能跟oracle的mdl_stock_analysis表的最大日期不一样
 */
const KEY_OF_MAX_DATE_OF_MDL_STOCK_ANALYSIS_IN_REDIS = 'keyOfMaxDateOfMdlStockAnalysisInRedis';

/*************************************** 导出属性 ****************************************/

module.exports = { REDIS_CONNECTION_INFO,
  MDL_STOCK_ANALYSIS_DEFAULT_EXPIRE_TIME,
  KEY_OF_MAX_DATE_OF_MDL_STOCK_ANALYSIS_IN_REDIS };
