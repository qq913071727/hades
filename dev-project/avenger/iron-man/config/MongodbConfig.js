/**
 * mongodb数据库连接信息
 */
const MONGODB_CONNECTION_INFO = {
  /**
   * mongodb的url
   */
  url: 'mongodb://localhost:27017/black-widow',
  /**
   * 数据库名
   */
  database: 'black-widow',
};



/*************************************** 导出属性 ****************************************/

module.exports = { MONGODB_CONNECTION_INFO };
