/**
 * oracle数据库连接信息
 */
const ORACLE_CONNECTION_INFO = {
	user: 'c##scott',
	password: 'tiger',
	options: { db: 5 },
};



/*************************************** 导出属性 ****************************************/

module.exports = { ORACLE_CONNECTION_INFO };