/**
 * mongodb的update方法的参数
 * 注意：MongodbUpdator和MongodbSetter这样设计主要是因为mongodb的nodejs版本有有很多，版本之间差异很大，
 * 这样设计也是为了尽量兼容，修改起来方便。
 */
class MongodbUpdator {
  constructor(key, mongodbSetter, options) {
    this.key = key;
    this.mongodbSetter = mongodbSetter;
    this.options = options;
  }
}

exports.MongodbUpdator = MongodbUpdator;