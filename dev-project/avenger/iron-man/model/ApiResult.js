/**
 * api接口的返回值
 * @param {*} code 
 * @param {*} message 
 * @param {*} data 
 */
module.exports = class ApiResult {
  constructor(success, code, message, data) {
    this.success = success;
    this.code = code;
    this.message = message;
    this.data = data;
  }
}