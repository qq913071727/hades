/**
 * mongodb的update方法的$set参数
 * 注意：MongodbUpdator和MongodbSetter这样设计主要是因为mongodb的nodejs版本有有很多，版本之间差异很大，
 * 这样设计也是为了尽量兼容，修改起来方便。
 */
class MongodbSetter {
  constructor(stockPriceUp, stockPriceDown, changeRange) {
    let setter = {};
    if(stockPriceUp){
      setter['price_up'] = stockPriceUp;
    }
    if(stockPriceDown){
      setter['price_down'] = stockPriceDown;
    }
    if(changeRange){
      setter['change_range'] = changeRange;
    }
    this['@set'] = setter;
  }
}

exports.MongodbSetter = MongodbSetter;