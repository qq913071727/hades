const redis = require('redis');
const RedisConfig = require('../config/RedisConfig');

module.exports = {

  /**
   * key-value类型。根据userId获取返回值
   * @param {*} userId 
   * @param {*} callback 
   */
  get: function (userId, callback = null) {
    // 连接redis
    const client = redis.createClient(RedisConfig.REDIS_CONNECTION_INFO.port,
      RedisConfig.REDIS_CONNECTION_INFO.host, RedisConfig.REDIS_CONNECTION_INFO.options);

    return new Promise((resolve, reject) => {
      client.get(userId, function (err, result) {
        client.end(true);
        if (!err) {
          if(callback){
            callback(err, result);
          }
          resolve(result);
        }
        else {
          reject(err);
        };
      });
    });
  },

  /**
   * key-value类型。设置值
   * @param {*} key 
   * @param {*} value 
   */
  set: function (key, value) {
    // 连接redis
    const client = redis.createClient(RedisConfig.REDIS_CONNECTION_INFO.port,
      RedisConfig.REDIS_CONNECTION_INFO.host, RedisConfig.REDIS_CONNECTION_INFO.options);

    return new Promise((resolve, reject) => {
      client.set(key, value, function (err, result) {
        client.end(true);
        if(!err){
          resolve(result);
        } else {
          reject(err);
        }
      });
    });
  },

  /**
   * hset类型。查找所有的hkeys
   * @param {*} _key 
   * @param {*} eve 
   */
  hkeys: function (_key, eve) {
    // 连接redis
    const client = redis.createClient(RedisConfig.REDIS_CONNECTION_INFO.port,
      RedisConfig.REDIS_CONNECTION_INFO.host, RedisConfig.REDIS_CONNECTION_INFO.options);

    return new Promise((resolve, reject) => {
      client.hkeys(_key, function (err, result) {
        client.end(true);
        if (!err) {
          resolve(eve(result));
        } else {
          reject(err);
        }
      });
    });
  },

  /**
   * hset类型。设置值
   * @param {*} _key 
   * @param {*} key2 
   * @param {*} value 
   * @param {*} callback 
   */
  hset: function (_key, key2, value, expireTime, callback = null) {
    // 连接redis
    const client = redis.createClient(RedisConfig.REDIS_CONNECTION_INFO.port,
      RedisConfig.REDIS_CONNECTION_INFO.host, RedisConfig.REDIS_CONNECTION_INFO.options);

    return new Promise((resolve, reject) => {
      client.hset(_key, key2, value, function (err, result) {
        if(!err){
          if (expireTime) {
            client.expire(_key, expireTime);
          }
          client.end(true);
          if(callback){
            callback(err, result);
          }
          resolve(result);
        } else {
          client.end(true);
          reject(err);
        }
      });
    });
  },

  /**
   * hset类型。获取所有值
   * @param {*} _key 
   * @param {*} callback 
   */
  hgetall: function (_key, callback = null) {
    // 连接redis
    const client = redis.createClient(RedisConfig.REDIS_CONNECTION_INFO.port,
      RedisConfig.REDIS_CONNECTION_INFO.host, RedisConfig.REDIS_CONNECTION_INFO.options);

    return new Promise((resolve, reject) => {
      client.hgetall(_key, function (err, result) {
        client.end(true);
        if (!err) {
          if(callback){
            callback(err, result);
          }
          resolve(result);
        } else {
          reject(err);
        }
      });
    });
  },

  /**
   * hset类型。获取值
   * @param {*} _key 
   * @param {*} key2 
   * @param {*} callback 
   */
  hget: function (_key, key2, callback = null) {
    // 连接redis
    const client = redis.createClient(RedisConfig.REDIS_CONNECTION_INFO.port,
      RedisConfig.REDIS_CONNECTION_INFO.host, RedisConfig.REDIS_CONNECTION_INFO.options);

    return new Promise((resolve, reject) => {
      client.hget(_key, key2, function (err, result) {
        client.end(true);
        if (!err) {
          if(callback){
            callback(err, result);
          }
          resolve(result);
        }
        else {
          reject(err);
        };
      });
    });
  }
};