const { Date_Format } = require('./constant');

/**
 * 将浮点数四舍五入，取小数点后2位，如果不足2位则补0，这个函数返回的是字符串的格式。
 * @param {*} x 
 */
exports.changeTwoDecimal_f = (x) => {
  var f_x = parseFloat(x);
  var f_x = Math.round(x * 100) / 100;
  var s_x = f_x.toString();
  var pos_decimal = s_x.indexOf('.');
  if (pos_decimal < 0) {
    pos_decimal = s_x.length;
    s_x += '.';
  }
  while (s_x.length <= pos_decimal + 2) {
    s_x += '0';
  }
  return s_x;
}

/**
 * 时间格式化
 * @param {*} date 
 * @param {*} format 
 */
exports.formatDate = (date, format) => {
  if(!date){
    return null;
  }
  const d = new Date(date)
  const y = d.getFullYear() // 年份
  const m = (d.getMonth() + 1).toString().padStart(2, '0') // 月份
  const r = d.getDate().toString().padStart(2, '0') // 日子
  const hh = d.getHours().toString().padStart(2, '0') // 小时
  const mm = d.getMinutes().toString().padStart(2, '0') // 分钟
  const ss = d.getSeconds().toString().padStart(2, '0') // 秒
  if(format == Date_Format.YYYY_MM_DD_HH_MM_SS){
    return `${y}-${m}-${r} ${hh}:${mm}:${ss}`// es6 字符串模板
  }else if(format == Date_Format.YYYY_MM_DD){
    return `${y}-${m}-${r}`// es6 字符串模板
  }
  
}