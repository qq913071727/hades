module.exports = {
	/**
	 * mongodb的集合
	 */
	Collection: {
		STOCK_INFO: 'stock_info',
		USER: 'user',
		USER_LOGIN_LOG: 'user_login_log',
    REL_USER_STOCK_INFO: 'rel_user_stock_info',
    API_CALL_LOG: 'api_call_log',
    API_INFO: 'api_info',
    PAGE_INFO: 'page_info',
    PAGE_VISIT_LOG: 'page_visit_log',
	},

	/**
	 * Date类型对象转换为字符串时的格式
	 */
	Date_Format: {
		YYYY_MM_DD_HH_MM_SS: 1,
		YYYY_MM_DD: 2
	}
};