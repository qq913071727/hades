const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const { loadMdlStockAnalysisIntoRedis } = require('./component/scheduleHandler');
const api = require('./api/api');
const logHandler = require('./component/logHandler');
const ApiResult = require('./model/ApiResult');
const httpStatusCode = require("httpstatuscode").httpStatusCode;
const redis = require('redis');
const redisManager = require('./manager/redisManager');
const { formatDate } = require('./component/util');

let app = express();

/**
 * express配置模板视图
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

/**
 * 引入要使用的模块
 */
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * 拦截器
 */
app.all('/*', function (request, response, next) {
  // 从header中取出user，再取出token，然后去redis上判断这个token是否合法
  let user = JSON.parse(request.header('user'));
  // token
  let token = user.token;
  logHandler.info(`header的user中的token：${token}`);
  // userId
  let userId = user.userId;
  logHandler.info(`header的user中的userId：${userId}`);

  if (token) {
    // let _response = response;

    /**
     * redisManager.get方法的回调函数，用于验证header中的token和redis中的token
     */
    function callback(err, result){
      if(!err){
        let redisToken = result;

        // 如果header的usery中有token，redis中的token已过期，则认证失败
        if (!redisToken && token) {
          logHandler.warn('header的user中有token，redis中的token已过期')
          let apiResult = new ApiResult(false, httpStatusCode.Unauthorized, ' header的user中有token，redis中的token已过期', null);
          response.end(JSON.stringify(apiResult));
        }

        // 如果header的user中的token与redis中的token相同，认证通过
        if(redisToken && token && redisToken == token){
          logHandler.info('header的user中的token与redis中的token相同，认证通过');
          next();
        }
      }else{
        // 从redis获取数据失败
        logHandler.error(err);
      }
    }

    // 根据userId，从redis中获取token。才callback函数中验证两个token，如果两个token相等，则表示认证通过
    redisManager.get(userId, callback);
  } else {
    // header的user中没有token
    logHandler.warn('header的user中没有token')
    let apiResult = new ApiResult(false, httpStatusCode.Unauthorized, 'header的user中没有token', null);
    res.end(apiResult);
  }
});

/**
 * 配置api接口
 */
app.use('/', api);

/**
 * 捕获http 404错误，然后跳转到错误处理
 */
app.use(function (req, res, next) {
  next(createError(404));
});

/**
 * 错误处理
 */
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// 启动定时任务。将ADAM库中的mdl_stock_analysis表中的数据加载到redis
let date = new Date();
let dateString = formatDate(date);
loadMdlStockAnalysisIntoRedis('2020-11-02');

module.exports = app;
