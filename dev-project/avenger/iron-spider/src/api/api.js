/**
 * 引入fetch、baseUrl
 * @param params
 * @returns {*}
 */
import { fetch, baseUrl } from 'config/index'

// 登录接口
export function login(params) {
  return fetch(`${baseUrl}/root/login/checkMemberLogin`, params)
}