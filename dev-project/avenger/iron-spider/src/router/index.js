import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import Login from '@/pages/auth/Login'
import Configuration from '@/pages/configuration/Configuration'
import MessageBoard from '@/pages/messageBoard/MessageBoard'
import QuantitativeStockSelection from '@/pages/quantitativeStockSelection/QuantitativeStockSelection'
import RealTimeData from '@/pages/realTimeData/RealTimeData'
import StockDiagnosis from '@/pages/stockDiagnosis/StockDiagnosis'
import StockRecommendation from '@/pages/stockRecommendation/StockRecommendation'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/configuration',
      name: 'Configuration',
      component: Configuration,
    },
    {
      path: '/messageBoard',
      name: 'MessageBoard',
      component: MessageBoard,
    },
    {
      path: '/quantitativeStockSelection',
      name: 'QuantitativeStockSelection',
      component: QuantitativeStockSelection,
    },
    {
      path: '/realTimeData',
      name: 'RealTimeData',
      component: RealTimeData,
    },
    {
      path: '/stockDiagnosis',
      name: 'StockDiagnosis',
      component: StockDiagnosis,
    },
    {
      path: '/stockRecommendation',
      name: 'StockRecommendation',
      component: StockRecommendation,
    },
  ]
})
