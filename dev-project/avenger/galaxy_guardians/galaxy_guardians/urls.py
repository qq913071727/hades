"""galaxy_guardians URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from star_lord.views import set_session, get_session
from star_lord.views import auth
from gamora.views import save_login_log
from gamora.views import save_api_call_log
from gamora.views import save_page_visit_log

urlpatterns = [
    # 登录django管理页面
    path('admin', admin.site.urls),

    # 保存session数据，用于测试
    url('set_session', set_session),
    # 获取session数据，用于测试
    url('get_session', get_session),

    # 认证
    path('api/auth', auth),

    # 保存登录日志
    path('api/loginLog/save', save_login_log),

    # 保存接口调用日志
    path('api/apiCallLog/save', save_api_call_log),

    # 保存页面访问日志
    path('api/pageVisitLog/save', save_page_visit_log)
]
