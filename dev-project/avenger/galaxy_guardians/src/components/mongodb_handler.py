from pymongo import MongoClient


class MongodbHandler:

    # 数据库连接
    Connection = None
    # 数据库
    Database = None

    @staticmethod
    def get_db(ip, port, database):
        """
        连接数据库，返回Database对象
        :param database:
        :param ip:
        :param port:
        :return:
        """
        # 连接服务器
        MongodbHandler.Connection = MongoClient(ip, port)
        # 连接数据库
        MongodbHandler.Database = MongodbHandler.Connection[database]
        return MongodbHandler.Database

    @staticmethod
    def close():
        """
        关闭数据库连接
        :return:
        """
        MongodbHandler.Connection.close()
