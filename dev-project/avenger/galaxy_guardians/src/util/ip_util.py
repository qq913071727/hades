from src.constant.province_city import ProvinceCity as ProvinceCity


class IpUtil:
    """
    ip的工具类
    """

    @staticmethod
    def get_ip(request):
        """
        获取ip
        :param self:
        :param request:
        :return:
        """
        ip = str()
        if request.META.get('HTTP_X_FORWARDED_FOR'):
            ip = request.META.get("HTTP_X_FORWARDED_FOR")
            return ip
        else:
            ip = request.META.get("REMOTE_ADDR")
            return ip

    @staticmethod
    def is_local_ip(self, param_ip):
        """
        判断ip是否是本地ip
        :param self:
        :param param_ip:
        :return:
        """
        if param_ip == None or param_ip is None:
            return None
        else:
            is_local_ip = None
            for ip in ProvinceCity.Local_IP:
                if param_ip == ip:
                    is_local_ip = True
                    break
            return is_local_ip
