from django.http import JsonResponse


class Response(JsonResponse):
    """
    接口返回对象
    """
    # 接口调用是否成功
    success = False
    # 代码
    code = int()
    # 接口返回值
    data = str()
    # 接口消息
    message = str()

    def __init__(self, success, code, data, message):
        self.success = success
        self.code = code
        self.data = data
        self.message = message
        # 父类JsonResponse对象构造函数的参数必须是dict类型
        super(Response, self).__init__(self.__dict__)
