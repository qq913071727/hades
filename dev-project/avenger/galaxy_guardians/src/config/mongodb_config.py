class MongodbConfig:
    """
    mongodb的连接信息
    """
    # 数据库ip
    IP = 'localhost'

    # 端口
    PORT = 27017

    DATABASE = 'black-widow'
