class RedisConfig:
    """
    redis的连接信息
    """
    # IP
    IP = '127.0.0.1'
    # 端口
    PORT = 6379
    # 数据库
    DATABASE = 5
