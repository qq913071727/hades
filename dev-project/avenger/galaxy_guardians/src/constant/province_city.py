class ProvinceCity:
    """
    省、市
    """

    # 本地
    Local = '本地'
    # 表示本地的IP
    Local_IP = ['127.0.0.1']
    # 未知
    Unknown = '未知'
