from django.apps import AppConfig


class StarLordConfig(AppConfig):
    name = 'star_lord'
