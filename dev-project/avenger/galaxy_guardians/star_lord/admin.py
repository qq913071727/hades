from django.contrib import admin

# 注册模型类
from star_lord.models import User

admin.site.register(User)
