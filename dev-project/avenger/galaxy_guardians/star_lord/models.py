from django.db import models


# 在这个文件中创建模型类，一个模型类对应一个表，有几个模型类就有几个表
class User(models.Model):
    """
    用户表
    """
    # 主键
    id = models.AutoField(primary_key=True)
    openid = models.CharField(max_length=100)
    session_key = models.CharField(max_length=100)
    unionid = models.CharField(max_length=100)
    # 是否登录。0表示未登录，1表示以登录
    login = models.IntegerField


class UserLoginLog(models.Model):
    """
    用户登录日志表
    """
    # 主键
    id = models.AutoField(primary_key=True)
    # 用户id
    user_id = models.CharField(max_length=50)
    # 创建时间
    create_time = models.DateTimeField(auto_now_add=True)
    # 登录是否成功。0表示失败，1表示成功
    success = models.IntegerField
    # 描述
    description = models.CharField(max_length=200)
    # IP
    ip = models.CharField(max_length=50)
    # 省
    province = models.CharField(max_length=50)
    # 市
    city = models.CharField(max_length=50)


class RelUserStockInfo(models.Model):
    """
    用户表和股票信息表的关系表，多对多关系
    """
    # 主键
    id = models.AutoField(primary_key=True)
    # 用户id
    user_id = models.CharField(max_length=50)
    # 股票id
    stock_info_id = models.CharField(max_length=50)
    # 订阅时间
    subscription_time = models.DateTimeField(auto_now_add=True)