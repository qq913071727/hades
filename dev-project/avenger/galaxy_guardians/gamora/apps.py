from django.apps import AppConfig


class GamoraConfig(AppConfig):
    name = 'gamora'
