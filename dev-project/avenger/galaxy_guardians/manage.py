#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from src.components.pre_loader import PreLoader


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'galaxy_guardians.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    # 将Mongodb的api_info集合中的数据存储在redis中
    PreLoader.store_api_info_in_redis()
    # 将Mongodb的page_info集合中的数据存储在redis中
    PreLoader.store_page_info_in_redis()

    main()
