exports.stompConnect = (param) => {
  const Stomp = require('stomp-client');
  // const destination = '/topic/myTopic';
  const destination = '/topic/';
  const client = new Stomp('127.0.0.1', 61613, 'admin', 'admin');

  client.connect(function (sessionId) {
    let code = param.code;
    client.publish(destination + code, JSON.stringify(param));
  });
};

exports.stompDisconnect = () => {
  client.disconnect();
};