package org.feign.service.impl;

import org.feign.service.ScheduleService;
import org.springframework.stereotype.Component;

@Component
public class ScheduleServiceHystric implements ScheduleService {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
