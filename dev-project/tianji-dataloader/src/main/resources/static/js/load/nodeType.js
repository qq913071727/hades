/**
 * 表示树形目录中节点的类型
 * @type {{}}
 */
const nodeType={
    /**
     * 中心节点
     */
    CENTER: "center",

    /**
     * ftp节点
     */
    FTP:"ftp",

    /**
     * 文件节点
     */
     FILE: "file"
};