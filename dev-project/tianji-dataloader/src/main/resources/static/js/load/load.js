/**
 * load页面的对象
 * @type {{}}
 */
var loadMgr = {
    /**
     * 表示当前正在操作的树节点
     */
    currentTreeNode: new Object(),

    /**
     * 返回树的根节点
     */
    showTreeRootNode: function () {
        // 返回源树形目录的节点
        $.ajax({
            type: "GET",
            url: "/dataloader/getRootNode?type=origin",
            dataType: "json",
            success: function (data) {
                $.fn.zTree.init($("#dataTree"), setting, data);
            }
        });

        // 返回目标树形目录节点
        $.ajax({
            type: "GET",
            url: "/dataloader/getRootNode?type=target",
            dataType: "json",
            success: function (data) {
                $.fn.zTree.init($("#dataTargetTree"), setting, data);
            }
        });
    },

    /**
     * 绑定方法
     */
    bindFunction: function () {
        /**
         * 同步文件
         */
        $("#syncButton").click(function () {
            var treeObj = $.fn.zTree.getZTreeObj("dataTree");
            var nodes = treeObj.getCheckedNodes(true);
            if(!_.isEmpty(nodes)){
                for(var i=0; i<nodes.length; i++){
                    if(nodes[i].type==nodeType.FTP){
                        // 将ftp节点及其子节点的数据发送给服务端，在服务端判断哪些文件需要同步
                        $.ajax({
                            type: "POST",
                            url: "/dataloader/syncFileNode",
                            dataType: "json",
                            data: {"nodeList": JSON.parse(nodes[i])},
                            success: function (data) {
                            }
                        });


                        // var children=nodes[i].children;
                        // if(!_.isEmpty(children)){
                        //     for(var j=0; j<children.length; j++){
                        //         if(children[j].checked){
                        //             console.log("111");
                        //         }
                        //     }
                        // }
                    }
                }
            }
        });
    },
};

var setting = {
    async: {
        enable: true,
        type: "get",
        dataType: "json",
        contentType: "application/json",
        url: "/dataloader/getFileNode",
        autoParam: ["name", "ip", "port", "username", "password"]
    },
    view: {
        showLine: false,    //显示连接线
        showIcon: false  //显示节点图片
    },
    check: {
        enable: true,
        chkStyle: "checkbox"
    },
    callback: {
        /**
         *
         * @param treeId
         * @param treeNode
         */
        beforeAsync: function (treeId, treeNode) {
        }
    }
};

// 入口
$(function ($) {
    // 展示树形目录
    loadMgr.showTreeRootNode();

    // 绑定方法
    loadMgr.bindFunction();
});