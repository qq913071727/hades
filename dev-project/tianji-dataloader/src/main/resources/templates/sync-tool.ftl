<!doctype html>
<html lang="zh-CN">
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<head>
    <title>节点同步</title>
</head>
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-content" style="width: 100%">
            <div class="row">
                <div class="col-lg-12">
                    <div style="margin: 0 auto;width: 300px;padding-bottom: 20px;color: #716aca" >
                        <h2>节点同步工具</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--同步节点-->
                <div class="col-lg-6">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" style="font-size: 1.8rem">
                                        请选择需要同步的节点
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <!--begin::Section-->
                            <div class="m-section m-section--last">
                                <div class="m-section__content">
                                    <!--begin::Preview-->
                                    <div class="m-demo">
                                        <div class="m-demo__preview" style="min-height: 400px">
                                            <div class="m-list-search">
                                                <div class="m-list-search__results">
                                                    <span style="font-size: 16px"
                                                          class="m-list-search__result-category m-list-search__result-category--first">
                                                        同步节点名称
													</span>
                                                    <ul id="dataTree" class="ztree"></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Preview-->
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                    </div>
                </div>
                <!--目标节点-->
                <div class="col-lg-6">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text" style="font-size: 1.8rem">
                                        请选择同步的目标节点
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <!--begin::Section-->
                            <div class="m-section m-section--last">
                                <div class="m-section__content">
                                    <!--begin::Preview-->
                                    <div class="m-demo">
                                        <div class="m-demo__preview" style="min-height: 400px">
                                            <div class="m-list-search">
                                                <div class="m-list-search__results">
                                                    <span style="font-size: 16px"
                                                          class="m-list-search__result-category m-list-search__result-category--first">
                                                        同步目标节点名称
													</span>
                                                    <ul id="dataTargetTree" class="ztree"></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Preview-->
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="m-portlet__foot m-portlet__foot--fit" style="margin: 0 auto">
                    <div class="m-form__actions m-form__actions">
                        <div class="col-lg-12 ml-lg-auto">
                            <button id="syncButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#m_modal_1">
                                开始同步
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Modal title
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												×
											</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<link rel="stylesheet" href="static/css/metronic/vendors.bundle.css"/>
<link rel="stylesheet" href="static/css/metronic/style.bundle.css"/>
<link rel="stylesheet" href="static/css/zTreeStyle.css"/>
<!--<link rel="stylesheet" href="static/css/bootstrap.min.css"/>-->
<!--<link rel="stylesheet" href="static/css/bootstrap-theme.min.css"/>-->
<!--<link rel="stylesheet" href="static/css/load/load.css"/>-->

<script type="text/javascript" src="static/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="static/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="static/js/underscore-min.js"></script>
<!-- 导入vendors.bundle.js后，使得$.fn.zTree is undefined -->
<!--<script type="text/javascript" src="static/js/vendors.bundle.js"></script>-->
<script type="text/javascript" src="static/js/load/nodeType.js"></script>
<script type="text/javascript" src="static/js/load/load.js"></script>


