package com.watertek.dataloader.service.impl;

import com.alibaba.fastjson.JSON;
import com.watertek.dataloader.common.NodeType;
import com.watertek.dataloader.service.AbstractService;
import com.watertek.dataloader.service.DataLoaderService;
import com.watertek.dataloader.util.FTPUtil;
import com.watertek.dataloader.vo.FileNode;
import com.watertek.dataloader.vo.FileNode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * DataLoader的服务类实现类
 */
@Transactional
@Service("dataLoaderService")
public class DataLoaderServiceImpl extends AbstractService implements DataLoaderService {

    /**
     * 返回树的根节点
     * @param type
     * @return
     */
    @Override
    public String getRootNode(String type) {
        String rootJson = null;
        if(null!=type && type.equals(NodeType.ORIGIN_NODE)){
            rootJson = getFileContents("/config/origin-nodes.json");
        }else {
            rootJson = getFileContents("/config/target-nodes.json");
        }
        return rootJson;
    }

    /**
     * 返回子节点
     *
     * @param queryString
     * @return
     */
    public String getFileNode(String queryString) {
        // 使用查询字符串中的每个参数创建数组
        String[] parameters = queryString.split("&");
        if (parameters != null && parameters.length > 0) {
            // ftp服务器名
            String name = parameters[0].split("=")[1];
            // ftp服务器ip
            String ip = parameters[1].split("=")[1];
            // ftp服务器端口
            int port = Integer.parseInt(parameters[2].split("=")[1]);
            // ftp服务器用户名
            String username = parameters[3].split("=")[1];
            // ftp服务器密码
            String password = parameters[4].split("=")[1];

            // 获取指定ftp服务器上的指定路径下的文件名
            FTPUtil ftpUtil = new FTPUtil(ip, port, username, password, FTP_CLIENT_SYSTEM_TYPE);
            List<String> fileNameList = ftpUtil.getFileName(FTP_SERVER_FILE_PATH);
            if(null!=fileNameList && fileNameList.size()>0){
                List<FileNode> list=new ArrayList<>();
                for(String fileName:fileNameList){
                    FileNode fileNode=new FileNode();
                    fileNode.setName(fileName);
                    fileNode.setType(NodeType.FILE);
                    list.add(fileNode);
                }
                return JSON.toJSONString(list);
            }
        }
        return null;
    }

    @Override
    public void syncFileNode() {

    }
}
