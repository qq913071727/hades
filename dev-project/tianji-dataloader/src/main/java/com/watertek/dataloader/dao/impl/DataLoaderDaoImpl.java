package com.watertek.dataloader.dao.impl;

import com.watertek.dataloader.dao.AbstractDao;
import com.watertek.dataloader.dao.DataLoaderDao;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DataLoader的DAO实现类
 */
@Repository
public class DataLoaderDaoImpl extends AbstractDao implements DataLoaderDao {

    @Override
    public List testOracleJdbcTemplate() {
        List list=oracleJdbcTemplate.queryForList("select * from demo1");
        System.out.println(list.size());
        return list;
    }

    @Override
    public List testMysqlJdbcTemplate() {
        List list=mysqlJdbcTemplate.queryForList("select * from test1");
        System.out.println(list.size());
        return list;
    }

    @Override
    public List testHbaseTemplate() {
        hbaseTemplate.put("t1", "r2", "f1", "c1", Bytes.toBytes("iwherelink"));
        return null;
    }
}
