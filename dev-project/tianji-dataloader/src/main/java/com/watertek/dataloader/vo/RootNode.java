package com.watertek.dataloader.vo;

import java.util.List;

/**
 * 树形目录的根节点
 */
public class RootNode {
    /**
     * 节点名称
     */
    private String name;

    /**
     * 节点是否展开
     */
    private boolean open;

    /**
     * 是否是父节点
     */
    private boolean isParent;

    /**
     * 子节点列表
     */
    private List<FtpNode> children;

    /**
     * 节点类型
     */
    private String type;

    /**
     * 节点id
     */
    private int id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<FtpNode> getChildren() {

        return children;
    }

    public void setChildren(List<FtpNode> children) {
        this.children = children;
    }

    public boolean isParent() {

        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public boolean isOpen() {

        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
