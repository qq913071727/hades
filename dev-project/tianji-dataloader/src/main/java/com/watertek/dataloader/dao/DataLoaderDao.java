package com.watertek.dataloader.dao;

import java.util.List;

/**
 * DataLoader的DAO接口
 */
public interface DataLoaderDao {
    List testOracleJdbcTemplate();

    List testMysqlJdbcTemplate();

    List testHbaseTemplate();
}