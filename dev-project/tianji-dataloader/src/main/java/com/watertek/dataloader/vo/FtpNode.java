package com.watertek.dataloader.vo;

/**
 * 树形目录的ftp节点
 */
public class FtpNode {
    /**
     * 节点名称
     */
    private String name;

    /**
     * 节点是否展开
     */
    private boolean open;

    /**
     * 是否是父节点
     */
    private boolean isParent;

    /**
     * ftp服务器的ip
     */
    private String ip;

    /**
     * ftp服务器的端口
     */
    private int port;

    /**
     * ftp服务器的用户名
     */
    private String username;

    /**
     * ftp服务器的密码
     */
    private String password;

    /**
     * 节点类型
     */
    private String type;

    /**
     * 节点id
     */
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPort() {

        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIp() {

        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isParent() {

        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public boolean isOpen() {

        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
