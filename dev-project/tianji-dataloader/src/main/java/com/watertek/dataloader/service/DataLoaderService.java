package com.watertek.dataloader.service;


/**
 * zTree类的服务类接口
 */
public interface DataLoaderService {

    /**
     * 返回树的根节点
     * @param type
     * @return
     */
    String getRootNode(String type);

    /**
     * 返回子节点
     * @param queryString
     * @return
     */
    String getFileNode(String queryString);

    void syncFileNode();
}
