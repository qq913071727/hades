package com.watertek.dataloader.controller;

import com.watertek.dataloader.service.DataLoaderService;
import com.watertek.dataloader.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * 抽象控制器类
 */
@RestController
public abstract class AbstractController {

    /**
     * DataLoader的服务类对象
     */
    @Autowired
    protected DataLoaderService dataLoaderService;


}













