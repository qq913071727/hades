package com.watertek.dataloader.service;

import com.watertek.dataloader.dao.DataLoaderDao;

import com.watertek.dataloader.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;

/**
 * 抽象服务类
 */
public abstract class AbstractService {

    /**
     * ftp客户端系统类型
     */
    protected static final String FTP_CLIENT_SYSTEM_TYPE= PropertiesUtil.getValue("config/config.properties", "ftp.client.system.type");

    /**
     * ftp服务端文件存储路径
     */
    protected static final String FTP_SERVER_FILE_PATH=PropertiesUtil.getValue("config/config.properties", "ftp.server.file.path");

    /**
     * DataLoader的Dao对象
     */
    @Autowired
    protected DataLoaderDao dataLoaderDao;

    /**
     * 将读取的文件内容转换为字符串，path从项目根目录下开始
     * @param path
     * @return
     */
    protected String getFileContents(String path){
        InputStream stream = getClass().getResourceAsStream(path);
        byte[] line = new byte[1024*1024];
        String str=null;
        try {
            stream.read(line);
            str = new String(line, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.trim();
    }
}
