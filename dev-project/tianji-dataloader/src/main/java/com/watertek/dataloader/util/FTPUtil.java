package com.watertek.dataloader.util;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * ftp工具类
 */
public class FTPUtil {

    /**
     * ftp服务器的IP
     */
    private String ip;

    /**
     * ftp服务器的端口
     */
    private int port;

    /**
     * ftp服务器的用户名
     */
    private String username;

    /**
     * ftp服务器的密码
     */
    private String password;

    /**
     * ftp客户端的系统类型
     */
    private String systemType;

    public FTPUtil() {
    }

    public FTPUtil(String ip, int port, String username, String password, String systemType) {
        this.ip = ip;
        this.port = port;
        this.username = username;
        this.password = password;
        this.systemType = systemType;
    }

    /**
     * 根据路径获取文件名
     *
     * @param path
     * @return
     */
    public List<String> getFileName(String path) {
        // 用来存储文件名
        List<String> fileNameList = new ArrayList<>();

        try {
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(this.ip, this.port);
            ftpClient.login(this.username, this.password);
            // 如果没有下面这一句，则会出现超时，或者返回length为0的数组
            ftpClient.enterLocalActiveMode();
            ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
            FTPClientConfig conf = new FTPClientConfig(this.systemType);
            ftpClient.configure(conf);
            ftpClient.changeWorkingDirectory(path);

            // 存储指定路径下的文件名
            String[] nameArray = ftpClient.listNames(path);
            if (null != nameArray && nameArray.length > 0) {
                for (String name : nameArray) {
                    String[] str = name.split("/");
                    fileNameList.add(str[str.length - 1]);
                }
            }
//            System.out.println("--------------------------------------------------");
//            FTPFile[] ftpFileArray=ftpClient.listFiles(path);
//            if(null!=ftpFileArray && ftpFileArray.length>0) {
//                for (FTPFile ftpFile : ftpFileArray) {
//                    System.out.println(ftpFile.getName());
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileNameList;
    }

    public void copy(String originFtpIp, int originFtpPort, String originFtpUsername, String originFtpPassword,
                     String targetFtpIp, int targetFtpPort, String targetFtpUsername, String targetFtpPassword) {
        try {
            org.apache.commons.net.ftp.FTPClient ftp1 = new org.apache.commons.net.ftp.FTPClient();
            org.apache.commons.net.ftp.FTPClient ftp2 = new org.apache.commons.net.ftp.FTPClient();
            //连接ftp
            ftp1.connect(originFtpIp, originFtpPort); //可以不需要port
            ftp1.login(originFtpUsername, originFtpPassword);

            //连接ftp2
            ftp2.connect(targetFtpIp, targetFtpPort);
            ftp2.login(targetFtpUsername, targetFtpPassword);

            //数据传输ftp1 到ftp2
            FTPFile[] file1Array = ftp1.listFiles();
            FTPFile[] file2Array = ftp2.listFiles();
//            if(null!=file)
            FTPFile file1 = file1Array[0];

            OutputStream is = ftp2.storeFileStream(ftp1.printWorkingDirectory() + "/" + file1.getName());//这句话就是获取ftp2的流

            ftp1.retrieveFile(file1.getName(), is); //这句话是把文件从ftp1复制到ftp2中，通过流is

            is.close();
            ftp1.completePendingCommand();   //完成数据传送,进入文件管理,这条语句特别关键，不然你不能进行文件管理操作（获取当前目录）特别重要
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
