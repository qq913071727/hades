package com.watertek.dataloader.common;

/**
 * 树形目录节点的类型
 */
public class NodeType {
    /**
     * 中心节点
     */
    public static final String CENTER="center";

    /**
     * ftp节点
     */
    public static final String FTP="ftp";

    /**
     * 文件节点
     */
    public static final String FILE="file";

    /**
     * 源树形目录节点
     */
    public static final String ORIGIN_NODE="origin";

    /**
     * 目标树形目录节点
     */
    public static final String TARGET_NODE="target";
}
