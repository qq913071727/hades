package com.watertek.dataloader.vo;

/**
 * 树形目录的文件节点
 */
public class FileNode {

    /**
     * 文件名
     */
    private String name;

    /**
     * 节点类型
     */
    private String type;

    /**
     * 节点是否被勾选
     */
    private boolean checked;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
