package com.watertek.dataloader.controller;

import com.watertek.dataloader.common.NodeType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Dataloader的controllDer类
 */
@RestController
@RequestMapping(value = "/dataloader")
public class DataLoaderController extends AbstractController {

    /**
     * 导航到加载数据页面
     *
     * @param model
     * @return
     */
    @RequestMapping("sync")
    public ModelAndView load(Model model) {
        ModelAndView mv = new ModelAndView("sync-tool");
        return mv;
    }

    /**
     * 返回树的根节点
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("getRootNode")
    @ResponseBody
    public String getRootNode(HttpServletRequest request, HttpServletResponse response) {
        String type=request.getQueryString().split("=")[1];
        return dataLoaderService.getRootNode(type);
    }

    /**
     * 返回子节点
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getFileNode", method = RequestMethod.GET)
    public String getFileNode(HttpServletRequest request, HttpServletResponse response) {
        return dataLoaderService.getFileNode(request.getQueryString());
    }

    /**
     * 同步文件节点
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "syncFileNode", method = RequestMethod.POST)
    public void syncFileNode(HttpServletRequest request, HttpServletResponse response) {
        String nodeList=request.getParameter("nodeList");
        dataLoaderService.syncFileNode();
    }

    /**
     * 导入指定文件中的数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("importData")
    public ModelAndView importData(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("load/load");
        return mv;
    }
}




