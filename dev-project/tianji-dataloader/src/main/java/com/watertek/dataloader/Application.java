package com.watertek.dataloader;

//import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication
//@EnableDiscoveryClient
//@MapperScan("com.watertek.bdcenter.mapper")
@ImportResource(locations = {"classpath:/config/hbase-spring.xml"})
public class Application {
//
//    @Bean
//    public BCryptPasswordEncoder bCryptPasswordEncoder() {
//        return new BCryptPasswordEncoder();
//    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        return new UserDetailsService();
//    }

    // 自动配置Spring框架
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
