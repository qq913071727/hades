import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.OutputStream;

public class Test {
    public static void main(String[] args) {
        try {
            FTPClient ftp1 = new FTPClient();
            org.apache.commons.net.ftp.FTPClient ftp2 = new org.apache.commons.net.ftp.FTPClient();
//连接ftp
            ftp1.connect("10.0.6.15", 21); //可以不需要port
            ftp1.login("tianji", "tianji");

//ftp2照样如此……
            ftp2.connect("10.0.6.14", 21); //可以不需要port
            ftp2.login("tianji", "tianji");

//数据传输ftp1 到ftp2
            FTPFile[] fileArray = ftp1.listFiles();  //获取ftp1下的目录文件
            FTPFile file = fileArray[0]; //在这里只获取第一个文件

            OutputStream is = ftp2.storeFileStream(ftp1.printWorkingDirectory() + "/" + file.getName());//这句话就是获取ftp2的流

            ftp1.retrieveFile(file.getName(), is); //这句话是把文件从ftp1复制到ftp2中，通过流is

            is.close();
            ftp1.completePendingCommand();   //完成数据传送,进入文件管理,这条语句特别关键，不然你不能进行文件管理操作（获取当前目录）特别重要
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
