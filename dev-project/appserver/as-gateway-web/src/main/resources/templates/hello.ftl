<#--<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>${bdtLawContent.title!}</title>
</head>-->

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>机构编制云</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telphone=no, email=no" />
    <!--meta标签让应用全屏模式显示-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="screen-orientation" content="portrait">
    <meta name="x5-orientation" content="portrait">
    <meta name="full-screen" content="yes">
    <meta name="x5-fullscreen" content="true">
    <meta name="browsermode" content="application">
    <link rel="stylesheet" type="text/css" href="http://bdt.conac.cn/r/cms/5b/ba/fb/36/bc/86/17/44/app.conac.cn/default/css/20170223normalize.css"/>

    <script type="text/javascript" src="http://bdt.conac.cn/r/cms/5b/ba/fb/36/bc/86/17/44/app.conac.cn/default/js/20170223rem.js" ></script>
    <script src="http://bdt.conac.cn/r/cms/front.js" type="text/javascript"></script>
    <script type="text/javascript">
        <#--0.33/2 0.33 0.33*2 0.33*3 -->
        function changeFont(fontSizeValue){

            var divConent = document.getElementById("contentFont");
            switch (fontSizeValue){
                case 0:
                    divConent.style.fontSize="0.231rem";
                    break;
                case 1:
                    divConent.style.fontSize="0.33rem";
                    break;
                case 2:
                    divConent.style.fontSize="0.495rem";
                    break;
                case 3:
                    divConent.style.fontSize="0.66rem";
                    break;

            }
        }
        
        
        
    </script>

    <style>
        *{
            padding: 0;
            margin: 0;
        }
        body {
            -webkit-touch-callout: none;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            -moz-text-size-adjust: none;
            text-size-adjust: none;
            -webkit-tap-highlight-color: transparent;
            -webkit-tap-highlight-color: transparent;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: 0;
            padding: 0;
            color: #000;
            word-wrap: break-word;
            font-size: 14px;
            font-family: -apple-system;
            font-family: "-apple-system","Helvetica Neue",Roboto,"Segoe UI",sans-serif;
            line-height: 20px;
            text-rendering: optimizeLegibility;
            -webkit-backface-visibility: hidden;
            -webkit-user-drag: none;
            -ms-content-zooming: none;
        }
        .title{
            font-size: 0.35rem;
            line-height: 2;
            text-align: center;
            margin-bottom: 0.2rem;
        }
        .row {
            color: #666666;
            border-bottom: 1px solid #e2dede;
            line-height: 2;
        }
        .row label{
            float: left;
            width: 1.4rem;
            text-align: right;
        }
        .row p{
            margin-left: 1.4rem;
        }
        .row .col-2{
            width: 50%;
            float: left;
        }
        .clearfix{
            clear: both;
            overflow: hidden;
        }
    </style>
</head>
<body>


<section style="padding: 0.25rem">
    <div class="title">${bdtLawClsIdx.title!}</div>
    <div class="row"><label for="">发布部门：</label><p>${bdtLawClsIdx.issueDepartment!}</p></div>
    <div class="row"><label for="">发文字号：</label><p>
    <#if bdtLawClsIdx.documentNo=='none' >
        --
    <#else>
       ${bdtLawClsIdx.documentNo!}
    </#if>


    </p></div>
    <div class="row clearfix">
        <div class="col-2"><label for="">发布日期：</label><p>${bdtLawClsIdx.issueDate!}</p></div>
        <div class="col-2"><label for="">实施日期：</label><p>${bdtLawClsIdx.implementDate!}</p></div>
    </div>
    <div class="row clearfix">
        <div class="col-2"><label for="">时效性：</label><p>${bdtLawClsIdx.timeliness!}</p></div>
        <div class="col-2"><label for="">效力级别：</label><p>${bdtLawClsIdx.effectiveness!}</p></div>
    </div>
</section>
<section id="contentFont" style="padding: 0.25rem;text-indent: 2em;line-height: 2;font-size: 0.33rem;">
    <p>${bdtLawContent.fullText!}
    </p>
</section>

<#--<div >
    <span>${bdtLawClsIdx.title!}</span>
    <p>发布部门：${bdtLawClsIdx.issueDepartment!}</p>
    <p>发文字号：${bdtLawClsIdx.documentNo!}</p>
    <p>发布日期：${bdtLawClsIdx.issueDate!}</p>
    <p>实施日期：${bdtLawClsIdx.implementDate!}</p>
    <p>时效性：${bdtLawClsIdx.timeliness!}</p>
    <p>效力级别：${bdtLawClsIdx.effectiveness!}</p>
</div>
<div>
${bdtLawContent.fullText!}

</div>-->

</body>
</html>