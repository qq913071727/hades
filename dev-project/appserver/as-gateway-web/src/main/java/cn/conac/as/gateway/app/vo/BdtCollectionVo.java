package cn.conac.as.gateway.app.vo;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by liuwei on 2017-7-20.
 */
@ApiModel
public class BdtCollectionVo
{
    private static final long serialVersionUID = -8014499779473830661L;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("类型名称")
    private String typeName;

    @ApiModelProperty("类型ID")
    private String typeId;

    @ApiModelProperty("创建时间")
    private Date createDate;
    @ApiModelProperty("收藏内容路径")
    private String contentPath;

    @ApiModelProperty("收藏内容标题")
    private String contentTitle;

    @ApiModelProperty("每页数量")
    private Integer pageSize;

    @ApiModelProperty("当前页索引")
    private Integer pageIndex;

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public Integer getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;
    }

    public Integer getPageIndex()
    {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex)
    {
        this.pageIndex = pageIndex;
    }

    public String getTypeName()
    {
        return typeName;
    }

    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeId()
    {
        return typeId;
    }

    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public String getContentPath()
    {
        return contentPath;
    }

    public void setContentPath(String contentPath)
    {
        this.contentPath = contentPath;
    }

    public String getContentTitle()
    {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle)
    {
        this.contentTitle = contentTitle;
    }
}
