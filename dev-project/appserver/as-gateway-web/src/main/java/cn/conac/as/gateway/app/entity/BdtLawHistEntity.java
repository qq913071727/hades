package cn.conac.as.gateway.app.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawHistEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
public class BdtLawHistEntity implements Serializable {

	private static final long serialVersionUID = 1490699901401113833L;

	@ApiModelProperty("主键id")
	private String id;

	@ApiModelProperty("创建者")
	private String createUser;

	@ApiModelProperty("创建时间")
	private Date createDate;

	@ApiModelProperty("更新者")
	private String updateUser;

	@ApiModelProperty("更新日期")
	private Date updateDate;

	@ApiModelProperty("删除标志0：正常 1：删除")
	private String deleteMark;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("关联法律法规ID")
	private String relatedLawIds;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setRelatedLawIds(String relatedLawIds){
		this.relatedLawIds=relatedLawIds;
	}

	public String getRelatedLawIds(){
		return relatedLawIds;
	}

}
