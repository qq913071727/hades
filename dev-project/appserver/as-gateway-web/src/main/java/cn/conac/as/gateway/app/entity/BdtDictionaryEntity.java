package cn.conac.as.gateway.app.entity;

import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtDictionaryEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
public class BdtDictionaryEntity implements Serializable {

	private static final long serialVersionUID = 1490699901230695278L;

	@ApiModelProperty("主键id")
	private String id;

	@ApiModelProperty("创建者")
	private String createUser;

	@ApiModelProperty("创建时间")
	private Date createDate;

	@ApiModelProperty("更新者")
	private String updateUser;

	@ApiModelProperty("更新日期")
	private Date updateDate;

	@ApiModelProperty("删除标志0：正常 1：删除")
	private String deleteMark;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("一级分类名称")
	private String clsName1;

	@ApiModelProperty("一级分类CD")
	private String clsCode1;

	@ApiModelProperty("二级分类名称")
	private String clsName2;

	@ApiModelProperty("二级分类CD")
	private String clsCode2;

	@ApiModelProperty("二级分类排序")
	private Integer clsSort2;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setClsName1(String clsName1){
		this.clsName1=clsName1;
	}

	public String getClsName1(){
		return clsName1;
	}

	public void setClsCode1(String clsCode1){
		this.clsCode1=clsCode1;
	}

	public String getClsCode1(){
		return clsCode1;
	}

	public void setClsName2(String clsName2){
		this.clsName2=clsName2;
	}

	public String getClsName2(){
		return clsName2;
	}

	public void setClsCode2(String clsCode2){
		this.clsCode2=clsCode2;
	}

	public String getClsCode2(){
		return clsCode2;
	}

	public void setClsSort2(Integer clsSort2){
		this.clsSort2=clsSort2;
	}

	public Integer getClsSort2(){
		return clsSort2;
	}

}
