package cn.conac.as.gateway.app.vo;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawSrhVo类
 *
 * @author wangmeng
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
public class BdtLawSrhVo implements Serializable {

	private static final long serialVersionUID = 149069990135550492L;

	@ApiModelProperty("法律法规标题")
	private String title;

	@ApiModelProperty("时效性CD")
	private String timelinessDic;

	@ApiModelProperty("时效性NAME")
	private String timeliness;

	@ApiModelProperty("效力级别CD")
	private String effectivenessDic;

	@ApiModelProperty("效力级别NAME")
	private String effectiveness;

	@ApiModelProperty("法规类别")
	private String category;

	@ApiModelProperty("发布部门")
	private String issueDepartment;

	@ApiModelProperty("发文字号")
	private String documentNo;

	@ApiModelProperty("发布日期")
	private String issueDate;

	@ApiModelProperty("实施日期")
	private String implementDate;

	@ApiModelProperty("每页数量")
	private String pageSize;

	@ApiModelProperty("当前页索引")
	private String pageIndex;

	@ApiModelProperty("唯一标志")
	private String gid;

	@ApiModelProperty("数据库标志")
	private String library;

	@ApiModelProperty("数据更新时间")
	private String updateTime;

	@ApiModelProperty("数据统计时间")
	private String statisticsTime;

	@ApiModelProperty("条")
	private String tiao;

	@ApiModelProperty("款")
	private String kuan;

	@ApiModelProperty("项")
	private String xiang;

	@ApiModelProperty("modelTitle")
	private String modelTitle;

	@ApiModelProperty("modelGid")
	private String modelGid;

	@ApiModelProperty("修改依据")
	private String revisedBasis;

	@ApiModelProperty("部分失效依据")
	private String partialFailureBasis;

	@ApiModelProperty("失效依据")
	private String failureBasis;

	public String getModelGid() {
		return modelGid;
	}

	public void setModelGid(String modelGid) {
		this.modelGid = modelGid;
	}

	public String getModelTitle() {
		return modelTitle;
	}

	public void setModelTitle(String modelTitle) {
		this.modelTitle = modelTitle;
	}

	public void setTitle(String title){
		this.title=title;
	}

	public String getTitle(){
		return title;
	}

	public void setTimelinessDic(String timelinessDic){
		this.timelinessDic=timelinessDic;
	}

	public String getTimelinessDic(){
		return timelinessDic;
	}

	public void setEffectivenessDic(String effectivenessDic){
		this.effectivenessDic=effectivenessDic;
	}

	public String getEffectivenessDic(){
		return effectivenessDic;
	}

	public void setIssueDepartment(String issueDepartment){
		this.issueDepartment=issueDepartment;
	}

	public String getIssueDepartment(){
		return issueDepartment;
	}

	public void setDocumentNo(String documentNo){
		this.documentNo=documentNo;
	}

	public String getDocumentNo(){
		return documentNo;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getImplementDate() {
		return implementDate;
	}

	public void setImplementDate(String implementDate) {
		this.implementDate = implementDate;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public void setLibrary(String library){
		this.library=library;
	}

	public String getLibrary(){
		return library;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getTiao() {
		return tiao;
	}

	public void setTiao(String tiao) {
		this.tiao = tiao;
	}

	public String getKuan() {
		return kuan;
	}

	public void setKuan(String kuan) {
		this.kuan = kuan;
	}

	public String getXiang() {
		return xiang;
	}

	public void setXiang(String xiang) {
		this.xiang = xiang;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getStatisticsTime() {
		return statisticsTime;
	}

	public void setStatisticsTime(String statisticsTime) {
		this.statisticsTime = statisticsTime;
	}

	public String getTimeliness() {
		return timeliness;
	}

	public void setTimeliness(String timeliness) {
		this.timeliness = timeliness;
	}

	public String getEffectiveness() {
		return effectiveness;
	}

	public void setEffectiveness(String effectiveness) {
		this.effectiveness = effectiveness;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRevisedBasis() {
		return revisedBasis;
	}

	public void setRevisedBasis(String revisedBasis) {
		this.revisedBasis = revisedBasis;
	}

	public String getPartialFailureBasis() {
		return partialFailureBasis;
	}

	public void setPartialFailureBasis(String partialFailureBasis) {
		this.partialFailureBasis = partialFailureBasis;
	}

	public String getFailureBasis() {
		return failureBasis;
	}

	public void setFailureBasis(String failureBasis) {
		this.failureBasis = failureBasis;
	}
}
