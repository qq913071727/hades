package cn.conac.as.gateway.app.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.as.gateway.app.entity.BdtCollectionEntity;
import cn.conac.as.gateway.app.entity.BdtDictionaryEntity;
import cn.conac.as.gateway.app.entity.BdtLawClsIdxEntity;
import cn.conac.as.gateway.app.entity.BdtLawContentEntity;
import cn.conac.as.gateway.app.entity.BdtLawHistEntity;
import cn.conac.as.gateway.app.vo.BdtCollectionVo;
import cn.conac.as.gateway.app.vo.BdtLawSrhVo;
import io.swagger.annotations.ApiParam;

/**
 * 本地数据库操作用Service
 * @author wangmeng
 * @version 1.0
 */
@FeignClient("AS-SERVICE-MONITOR")
public interface AppLawDBService {

    @RequestMapping(method = RequestMethod.POST, value = "/bdtDictionary/update")
    public JSONObject savOrUpdBdtDictionary(
            @ApiParam(value = "保存对象", required = true) @RequestBody BdtDictionaryEntity entity);

    @RequestMapping(method = RequestMethod.POST, value = "/bdtLawClsIdx/update")
    public JSONObject savOrUpdBdtLawClsIdx(
            @ApiParam(value = "保存对象", required = true) @RequestBody BdtLawClsIdxEntity entity);

    @RequestMapping(method = RequestMethod.POST, value = "/bdtLawContent/update")
    public JSONObject savOrUpdBdtLawContent(
            @ApiParam(value = "保存对象", required = true) @RequestBody BdtLawContentEntity entity);

    @RequestMapping(method = RequestMethod.POST, value = "/bdtLawHist/update")
    public JSONObject savOrUpdBdtLawHist(
            @ApiParam(value = "保存对象", required = true) @RequestBody BdtLawHistEntity entity);


    @RequestMapping(method = RequestMethod.POST, value = "/bdtCollection/update")
    public JSONObject savOrUpdBdtCollection(
            @ApiParam(value = "保存对象", required = true) @RequestBody BdtCollectionEntity entity);

    @RequestMapping(method = RequestMethod.POST, value = "/bdtLawClsIdx/detail")
    public JSONObject findBdtLawClsIdxByTitleAndDocNoAndIssueDate(
            @ApiParam(value = "检索对象", required = true) @RequestBody BdtLawClsIdxEntity entity);

    @RequestMapping(method = RequestMethod.POST, value = "/bdtLawClsIdx/list")
    public JSONObject findBdtLawClsIdxList(
            @ApiParam(value = "检索对象", required = true) @RequestBody BdtLawSrhVo bdtLawSrhVo);

    @RequestMapping(method = RequestMethod.POST, value = "/bdtCollection/list")
    public JSONObject findBdtCollection(
            @ApiParam(value = "检索对象", required = true) @RequestBody BdtCollectionVo bdtCollectionVo);

    @RequestMapping(method = RequestMethod.GET, value = "/bdtLawClsIdx/list/{effectivenessDic}/{rowNum}")
    public JSONObject findByEffectivenessDicAndRowNum(
            @ApiParam(value = "效力级别", required = true) @PathVariable("effectivenessDic") String effectivenessDic,
            @ApiParam(value = "显示行数", required = true) @PathVariable("rowNum") String rowNum);

    @RequestMapping(method = RequestMethod.GET, value = "/bdtLawContent/{id}")
    public JSONObject findBdtLawContentById(@ApiParam(value = "id", required = true) @PathVariable("id") String id);

    @RequestMapping(method = RequestMethod.GET, value = "/bdtLawClsIdx/{id}")
    public JSONObject findbdtLawClsIdxById(@ApiParam(value = "id", required = true) @PathVariable("id") String id);


    @RequestMapping(method = RequestMethod.GET, value = "/bdtDictionary/clsCode12/{clsCode1}/{clsCode2}")
    public JSONObject findByClsCode12(
            @ApiParam(value = "一级分类CD", required = true) @PathVariable("clsCode1") String clsCode1,
            @ApiParam(value = "二级分类CD", required = true) @PathVariable("clsCode2") String clsCode2);

    @RequestMapping(method = RequestMethod.GET, value = "/bdtDictionary/clsCode1/{clsCode1}")
    public JSONObject findByClsCode1(
            @ApiParam(value = "一级分类CD", required = true) @PathVariable("clsCode1") String clsCode1);

    @RequestMapping(method = RequestMethod.DELETE, value = "/bdtCollection/{id}/delete")
    public JSONObject deleteById(
            @ApiParam(value = "id", required = true) @PathVariable("id") String id);



}
