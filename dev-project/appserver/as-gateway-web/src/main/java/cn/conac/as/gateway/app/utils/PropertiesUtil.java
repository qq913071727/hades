package cn.conac.as.gateway.app.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Created by liuwei on 2017-5-23.
 */
public class PropertiesUtil
{


    private final static String path = PropertiesUtil.class.getResource("/").getPath()+"file.properties";

    public static void setProper(String key,String value){
        Properties prop = new Properties();

        InputStream fis = null;
        OutputStream fos = null;
        try {
            File file = new File(path);
            fis = new FileInputStream(file);
            prop.load(fis);
            fis.close();//一定要在修改值之前关闭fis
            fos = new FileOutputStream(file);
            prop.setProperty(key, value);//设值-保存
            prop.store(fos, null);
        } catch (IOException e) {
        } finally{
            try {
                fos.close();
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
