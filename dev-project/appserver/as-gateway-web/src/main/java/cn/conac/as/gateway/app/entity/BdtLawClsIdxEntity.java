package cn.conac.as.gateway.app.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawClsIdxEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
public class BdtLawClsIdxEntity implements Serializable {

	private static final long serialVersionUID = 149069990135550492L;

	@ApiModelProperty("主键id")
	private String id;

	@ApiModelProperty("创建者")
	private String createUser;

	@ApiModelProperty("创建时间")
	private Date createDate;

	@ApiModelProperty("更新者")
	private String updateUser;

	@ApiModelProperty("更新日期")
	private Date updateDate;

	@ApiModelProperty("删除标志0：正常 1：删除")
	private String deleteMark;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("法律法规标题")
	private String title;

	@ApiModelProperty("时效性NAME")
	private String timeliness;

	@ApiModelProperty("效力级别NAME")
	private String effectiveness;

	@ApiModelProperty("时效性CD")
	private String timelinessDic;

	@ApiModelProperty("效力级别CD")
	private String effectivenessDic;

	@ApiModelProperty("法规类别")
	private String category;

	@ApiModelProperty("发布部门")
	private String issueDepartment;

	@ApiModelProperty("发文字号")
	private String documentNo;

	@ApiModelProperty("发布日期")
	private String issueDate;

	@ApiModelProperty("实施日期")
	private String implementDate;

	@ApiModelProperty("数据库标志")
	private String library;

	@ApiModelProperty("唯一标志")
	private String gid;

	@ApiModelProperty("修改依据")
	private String revisedBasis;

	@ApiModelProperty("部分失效依据")
	private String partialFailureBasis;

	@ApiModelProperty("失效依据")
	private String failureBasis;

	@ApiModelProperty("页面相对路径")
	private String urlPath;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setTitle(String title){
		this.title=title;
	}

	public String getTitle(){
		return title;
	}

	public void setTimelinessDic(String timelinessDic){
		this.timelinessDic=timelinessDic;
	}

	public String getTimelinessDic(){
		return timelinessDic;
	}

	public void setEffectivenessDic(String effectivenessDic){
		this.effectivenessDic=effectivenessDic;
	}

	public String getEffectivenessDic(){
		return effectivenessDic;
	}

	public void setCategory(String category){
		this.category=category;
	}

	public String getCategory(){
		return category;
	}

	public void setIssueDepartment(String issueDepartment){
		this.issueDepartment=issueDepartment;
	}

	public String getIssueDepartment(){
		return issueDepartment;
	}

	public void setDocumentNo(String documentNo){
		this.documentNo=documentNo;
	}

	public String getDocumentNo(){
		return documentNo;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getImplementDate() {
		return implementDate;
	}

	public void setImplementDate(String implementDate) {
		this.implementDate = implementDate;
	}

	public void setLibrary(String library){
		this.library=library;
	}

	public String getLibrary(){
		return library;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public void setRevisedBasis(String revisedBasis){
		this.revisedBasis=revisedBasis;
	}

	public String getRevisedBasis(){
		return revisedBasis;
	}

	public void setPartialFailureBasis(String partialFailureBasis){
		this.partialFailureBasis=partialFailureBasis;
	}

	public String getPartialFailureBasis(){
		return partialFailureBasis;
	}

	public void setFailureBasis(String failureBasis){
		this.failureBasis=failureBasis;
	}

	public String getFailureBasis(){
		return failureBasis;
	}

	public String getTimeliness() {
		return timeliness;
	}

	public void setTimeliness(String timeliness) {
		this.timeliness = timeliness;
	}

	public String getEffectiveness() {
		return effectiveness;
	}

	public void setEffectiveness(String effectiveness) {
		this.effectiveness = effectiveness;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(String urlPath)
	{
		this.urlPath = urlPath;
	}
}
