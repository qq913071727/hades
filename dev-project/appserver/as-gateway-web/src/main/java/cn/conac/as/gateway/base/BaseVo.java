package cn.conac.as.gateway.base;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class BaseVo implements Serializable {
   
    private static final long serialVersionUID = 7715089534680387578L;

    @ApiModelProperty("主键id")
    protected String id;

    @ApiModelProperty("备注")
    protected String remarks;

    @ApiModelProperty("创建者")
    protected String createUser;

    @ApiModelProperty("创建时间")
    protected Date createDate;

    @ApiModelProperty("更新者")
    protected String updateUser;

    @ApiModelProperty("更新日期")
    protected Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
