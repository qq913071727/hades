package cn.conac.as.gateway;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import cn.conac.as.gateway.app.service.AppLawJobService;
@Configuration
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableZuulProxy
@PropertySources({@PropertySource(value = "classpath:remoteConfig.properties"),@PropertySource("classpath:file.properties")})
public class Application {

    /**
     * 定时任务开关
     */
    @Value("${remote.pkulaw.switchFlg}")
    private String switchFlg;

    /**
     * URL uses the logical name of account-service - upper or lower case,
     * doesn't matter.
     */
    public static final String SSSX = "SSSX";// 数据筛选
    public static final String SSJS = "SSJS";// 数据检索
    public static final String SSXX = "SSXX";// 数据查询
    public static final String SSGX = "SSGX";// 数据更新
    public static final String SSTJ = "SSTJ";// 数据统计

    /**
     * 数据监测服务
     */
    @Resource(name = "appLawJobService")
    private AppLawJobService appLawJobService;

    // @Scheduled(fixedRate = 1 * 60 * 60 * 1000)
    @Scheduled(cron = "0 0 4 * * ?")
    public void getLawFromRemoteJob() throws Exception {
        // 定时任务开关
        if("0".equals(switchFlg)){
            appLawJobService.getDataFromRemote();
        }
    }

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}