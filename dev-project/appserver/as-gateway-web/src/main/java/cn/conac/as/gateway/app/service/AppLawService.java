package cn.conac.as.gateway.app.service;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.conac.as.gateway.Application;
import cn.conac.as.gateway.app.vo.BdtLawSrhVo;
import cn.conac.as.gateway.base.BaseService;

/**
 * 北大法宝接口用Service
 * @author wangmeng
 * @version 1.0
 */
@Service
public class AppLawService extends BaseService {

    /**
     * 北大法宝接口地址
     */
    @Value("${remote.pkulaw.url}")
    private String serviceUrl;

    /**
     * 控制台日志
     */
    private Logger logger = Logger.getLogger(AppLawDBService.class.getName());

    public AppLawService() {
    }

//    public AppLawService(String serviceUrl) {
//        this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
//    }

    /**
     * The RestTemplate works because it uses a custom request-factory that uses
     * Ribbon to look-up the service to use. This method simply exists to show
     * this.
     */
    @PostConstruct
    public void demoOnly() {
        // Can't do this in the constructor because the RestTemplate injection
        // happens afterwards.
        logger.warn("The RestTemplate request factory is " + restTemplate.getRequestFactory());
    }

    /**
     * 数据筛选
     * @param sssxVo
     * @return JSONObject
     */
    public JSONObject shuJuShaXuan(BdtLawSrhVo sssxVo) {
        logger.info("shuJuShaXuan() invoked params: " + JSONObject.toJSON(sssxVo));
        String str = postHttp(serviceUrl, Application.SSSX, sssxVo);
        logger.info("shuJuShaXuan() result: " + JSON.parseObject(str));
        return JSON.parseObject(str);
    }

    /**
     * 数据检索
     * @param ssjsVo
     * @return JSONObject
     */
    public JSONObject shuJuJianSuo(BdtLawSrhVo ssjsVo) {
        logger.info("shuJuJianSuo() invoked params: " + JSONObject.toJSON(ssjsVo));
        String str = postHttp(serviceUrl, Application.SSJS, ssjsVo);
        logger.info("shuJuJianSuo() result: " + JSON.parseObject(str));
        return JSON.parseObject(str);
    }

    /**
     * 数据查询
     * @param ssxxVo
     * @return JSONObject
     */
    public JSONObject shuJuChaXun(BdtLawSrhVo ssxxVo) {
        logger.info("shuJuChaXun() invoked params: " + JSONObject.toJSON(ssxxVo));
        String str = postHttp(serviceUrl, Application.SSXX, ssxxVo);
        logger.info("shuJuChaXun() result: " + JSON.parseObject(str));
        return JSON.parseObject(str);
    }

    /**
     * 数据更新
     * @param ssgxVo
     * @return JSONObject
     */
    public JSONObject shuJuGengXin(BdtLawSrhVo ssgxVo) {
        logger.info("shuJuGengXin() invoked params: " + JSONObject.toJSON(ssgxVo));
        String str = postHttp(serviceUrl, Application.SSGX, ssgxVo);
        logger.info("shuJuGengXin() result: " + JSON.parseObject(str));
        return JSON.parseObject(str);
    }

    /**
     * 数据统计
     * @param sstjVo
     * @return JSONObject
     */
    public JSONObject shuJuTongJi(BdtLawSrhVo sstjVo) {
        logger.info("shuJuTongJi() invoked params: " + JSONObject.toJSON(sstjVo));
        String str = postHttp(serviceUrl, Application.SSTJ, sstjVo);
        logger.info("shuJuTongJi() result: " + JSON.parseObject(str));
        return JSON.parseObject(str);
    }
    
}
