import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import cn.conac.as.framework.utils.FastJsonUtil;
import cn.conac.as.monitor.constant.SystemType;
import cn.conac.as.monitor.constant.WeixinFinalValueType;
import cn.conac.as.monitor.entity.weixin.AccessToken;
import cn.conac.as.monitor.entity.weixin.ErrorEntity;
import cn.conac.as.monitor.entity.weixin.RefreshAccessTokenTask;
import cn.conac.as.monitor.entity.weixin.TemplateData;
import cn.conac.as.monitor.entity.weixin.UserInfo;
import cn.conac.as.monitor.entity.weixin.WxinTemplate;
import cn.conac.as.monitor.service.WeixinService;

/**
 * Created by liuwei on 2017-7-12.
 */
public class TestInit
{

//    @Test
    public void testPostMsg(){
        WxinTemplate tm  = new WxinTemplate();
        tm.setTouser("okkme1JBDBkyuJ01Ro51FgLaeNwk");
        tm.setTemplate_id("KNIqtYUSDnn7WofMvJ26PR0oJJl6XI27MJqgBUExspI");
        tm.setTopcolor("#ff0000");
        tm.setUrl("http://jgbzy.conac.cn");
        Map<String ,Object> data = new HashMap<String,Object>();
        data.put("path",new TemplateData("http://batlaw/index","#00ff00"));
        tm.setData(data);
//        System.out.println(FastJsonUtil.getJson(tm));

        WeixinService.postTemplateMsg(tm);




    }


    @Test
    public void testHttpClient(){
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            String url = WeixinFinalValueType.ACCESS_TOKEN_URL;
            url = url.replaceAll("APPID", WeixinFinalValueType.APPID);
            url = url.replaceAll("APPSECRET", WeixinFinalValueType.APPSECRET);
            HttpGet get = new HttpGet(url);
            CloseableHttpResponse resp = client.execute(get);
            int statusCode = resp.getStatusLine().getStatusCode();
            if(statusCode>=200&&statusCode<300) {
                HttpEntity entity = resp.getEntity();
                String content = EntityUtils.toString(entity);
                try {
//					AccessToken at = (AccessToken) JsonUtil.getInstance().json2obj(content, AccessToken.class);
                    AccessToken at = (AccessToken) FastJsonUtil.getObject(content, AccessToken.class);


                    System.out.println("akdkddkdtoken================"+at.getAccess_token());
                } catch (Exception e) {
//					ErrorEntity err = (ErrorEntity)JsonUtil.getInstance().json2obj(content, ErrorEntity.class);
                    ErrorEntity err = (ErrorEntity)FastJsonUtil.getObject(content, ErrorEntity.class);
                    System.out.println("获取token异常:"+err.getErrcode()+","+err.getErrmsg());

                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    @Test
    public void testGetUser(){
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            String url = WeixinFinalValueType.USER_GET;
            url = url.replaceAll("ACCESS_TOKEN", RefreshAccessTokenTask.at);
            url = url.replaceAll("NEXT_OPENID", "");
            HttpGet get = new HttpGet(url);
            CloseableHttpResponse resp = client.execute(get);
            int statusCode = resp.getStatusLine().getStatusCode();
            if(statusCode>=200&&statusCode<300) {
                HttpEntity entity = resp.getEntity();
                String content = EntityUtils.toString(entity,"UTF-8");
                try {
//					AccessToken at = (AccessToken) JsonUtil.getInstance().json2obj(content, AccessToken.class);
                    UserInfo userInfo = FastJsonUtil.getObject(content, UserInfo.class);
                    Map<String, List<String>> data = userInfo.getData();
                    List<String> openidList = data.get("openid");

                    System.out.println(openidList.toString());
                    System.out.println(data.toString()+"data");
                    System.out.println(content);
                } catch (Exception e) {
//					ErrorEntity err = (ErrorEntity)JsonUtil.getInstance().json2obj(content, ErrorEntity.class);
                    ErrorEntity err = (ErrorEntity)FastJsonUtil.getObject(content, ErrorEntity.class);
                    System.out.println("获取token异常:"+err.getErrcode()+","+err.getErrmsg());

                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @Test
    public void json(){
        SystemType sy = new SystemType();

        Map dataMap = sy.dataMap;
        String o = (String )dataMap.get("48470c874bd744dd8d71bae61770b772");
        System.out.println(dataMap);
        System.out.println(o);
    }


}
