import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;

import cn.conac.as.framework.utils.IdGen;
import cn.conac.as.monitor.entity.BdtInterfaceEntity;
import cn.conac.as.monitor.entity.BdtInterfaceMonitorEntity;

public class TestMonitor {

	public TestMonitor() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 登录验证
	 */
	@Test
	public void auth() {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		HttpPost httpPost = null;
		try {
			HttpContext httpContext = new BasicHttpContext();
			RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(10000).build();
			httpclient = HttpClients.createDefault();
			httpPost = new HttpPost("http://jgbzy.conac.cn/api/public/auth");
			// 如果没有这个设置，汇报"error":"Unsupported Media Type"
			httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
			httpPost.setHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDI1MjI0MzIsInN1YiI6IuaJi-acuuS6keW5s-WPsCIsImF1ZGllbmNlIjoid2ViIiwiY3JlYXRlZCI6MTUwMTY1ODQzMjAyNCwidXNlciI6eyJpZCI6IjEwMjU1NTIiLCJ1c2VybmFtZSI6IuaJi-acuuS6keW5s-WPsCIsInVzZXJUeXBlIjoxLCJhcmVhSWQiOiI0QTRBMTkzMUUyODI1MTI0RTA1MzczNjQxQUFDQTMxMCIsImFyZWFOYW1lIjoi6K-V55So5Zyw5Yy6IiwiYXJlYUFkbWluQ29kZSI6IjExMDExOCIsImFyZWFDb2RlIjoiMDEwMDgwMDEwIiwiYXJlYUx2bCI6IjMiLCJtb2JpbGUiOm51bGwsIm9yZ0lkIjoiMTA3OTY5NSIsIm9yZ0Jhc2VJZCI6IjExMzM3ODgiLCJvcmdOYW1lIjoiQ09OQUPmtYvor5UiLCJvcmdTeXNUeXBlIjoiMTAiLCJvcmdUeXBlIjoiMiIsImlzQWRtaW4iOiIwIiwiZnVsbE5hbWUiOm51bGwsIm5pY2tOYW1lIjpudWxsLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiMTA2In1dLCJlbmFibGVkIjp0cnVlLCJpc1RyaWFsUG9pbnRzIjoiMSIsImlzTW9iaWxlTG9naW4iOm51bGwsImlzQ29tcGxldGUiOiIwIiwiaXNSZXBlYXQiOm51bGwsInNpQ29kZSI6bnVsbCwiaW1nVXJsIjpudWxsLCJxckNvZGVLZXkiOm51bGx9fQ.2Pvl_uJO2jN0X2vVFnovTKYaGRebYwHXFbYoskBr1hgI-PvxOCIXBYG_mFZbwPwO9uMa7WFCxLw-f6Y7FEs6Ww");
			httpPost.setConfig(defaultRequestConfig);
			StringEntity strEntity = new StringEntity("{\"password\":\"ypt123456\",\"username\":\"手机云平台\"}");
			httpPost.setEntity(strEntity);
			response = httpclient.execute(httpPost, httpContext);
			HttpHost currentHost = (HttpHost) httpContext.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
			HttpUriRequest req = (HttpUriRequest) httpContext.getAttribute(ExecutionContext.HTTP_REQUEST);
			System.out.println((req.getURI().isAbsolute()) ? req.getURI().toString() : (currentHost.toURI() + req.getURI()));

			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(entity);
			System.out.println("测试的返回值： " + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void post() {		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("http://jgbzy.conac.cn/api/auth/refresh");
		// 如果没有这个设置，汇报"error":"Unsupported Media Type"
		httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
		// 在header里存储token，否则没有权限
		httpPost.setHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDM2MjQwNTQsInN1YiI6IuWImOS8nyIsImF1ZGllbmNlIjoid2ViIiwiY3JlYXRlZCI6MTUwMjc2MDA1NDk2NCwidXNlciI6eyJpZCI6IjEwODg4NDMiLCJ1c2VybmFtZSI6InJjXzEwODg4NDMiLCJ1c2VyVHlwZSI6MSwiYXJlYUlkIjoiQzFGMUI4RDZBQUYwMjk5M0UwNDAxMUFDQUE0NjZFNTMiLCJhcmVhTmFtZSI6IuS4reWkriIsImFyZWFBZG1pbkNvZGUiOiIxMDAwMDAiLCJhcmVhQ29kZSI6IjMzMDAwMDAwMCIsImFyZWFMdmwiOiIxIiwibW9iaWxlIjpudWxsLCJvcmdJZCI6IjEwODA4NTEiLCJvcmdCYXNlSWQiOm51bGwsIm9yZ05hbWUiOiLkuK3lpK7nvJblip7kuovkuJrlj5HlsZXkuK3lv4MiLCJvcmdTeXNUeXBlIjoiMDEiLCJvcmdUeXBlIjoiMiIsImlzQWRtaW4iOiIwIiwiaXNQdWJsaWMiOjAsImZ1bGxOYW1lIjoi5YiY5LyfIiwibmlja05hbWUiOiLliJjkvJ8tY29uYWMiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiMTExIn1dLCJlbmFibGVkIjp0cnVlLCJpc1RyaWFsUG9pbnRzIjoiMSIsImlzTW9iaWxlTG9naW4iOiIxIiwiaXNDb21wbGV0ZSI6IjAiLCJpc1JlcGVhdCI6bnVsbCwic2lDb2RlIjoiNDA0ODc3MzIiLCJpbWdVcmwiOiIvb3JnaW5mby91c2VyL2Y4Y2YvOTRlYS9lNTliL2VhOTYvODRkM2JkZGVhZWRmZWU0L3VzZXIwNC5wbmciLCJxckNvZGVLZXkiOm51bGwsIm9yZ0RvbWFpbk5hbWUiOm51bGx9fQ.huUVzzEYdHdaZ2FgD_ghv7h-MlG3GsZHBilWqfVIJIXKlbAGJu7E6Oexig1IqnYpX7ztZsxQgk04QToIF5qrBQ");

		try {
//			StringEntity strEntity = new StringEntity("{\"deviceNumber\": \"123\",\"mobilePhoneType\": \"abc123\",\"operationSystemVersion\": \"1.2.3\",\"operationSystemType\": \"1\",\"umengNumber\": \"AjlWGwhWckbEojONg_YVTsFVDKX2CzwbpyGDytrS_RDK\"}");
//			httpPost.setEntity(strEntity);

			CloseableHttpResponse response = httpClient.execute(httpPost);

			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(entity);
			System.out.println("测试的返回值： " + result);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 关闭连接,释放资源
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void get(){
		BdtInterfaceEntity bdtInterfaceEntity = new BdtInterfaceEntity();
		bdtInterfaceEntity.setUrl("http://jgbzy.conac.cn/api/client/whether/collect");
//		bdtInterfaceEntity.setParameterUseCase("{\"mobileNum\":\"18513883069\",\"other\":\"r\"}");
		
		String url=bdtInterfaceEntity.getUrl();
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		// 加载参数
		try {
			if(null!=bdtInterfaceEntity.getParameterUseCase()){
				JSONObject obj=JSON.parseObject(bdtInterfaceEntity.getParameterUseCase(),Feature.OrderedField);
				Set keySet=obj.keySet();
				Iterator it=keySet.iterator();
				while(it.hasNext()){
					String key=(String) it.next();
					String value=(String) obj.get(key);
					System.out.println(key);
					System.out.println(value);
					url+="/"+value;
				}
			}
			System.out.println(url);
			
			HttpGet httpGet = new HttpGet(url);
			// 如果没有这个设置，汇报"error":"Unsupported Media Type"
			httpGet.setHeader("Content-Type", "application/json;charset=UTF-8");
			// 在header里存储token，否则没有权限
			httpGet.setHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDM2MjQwNTQsInN1YiI6IuWImOS8nyIsImF1ZGllbmNlIjoid2ViIiwiY3JlYXRlZCI6MTUwMjc2MDA1NDk2NCwidXNlciI6eyJpZCI6IjEwODg4NDMiLCJ1c2VybmFtZSI6InJjXzEwODg4NDMiLCJ1c2VyVHlwZSI6MSwiYXJlYUlkIjoiQzFGMUI4RDZBQUYwMjk5M0UwNDAxMUFDQUE0NjZFNTMiLCJhcmVhTmFtZSI6IuS4reWkriIsImFyZWFBZG1pbkNvZGUiOiIxMDAwMDAiLCJhcmVhQ29kZSI6IjMzMDAwMDAwMCIsImFyZWFMdmwiOiIxIiwibW9iaWxlIjpudWxsLCJvcmdJZCI6IjEwODA4NTEiLCJvcmdCYXNlSWQiOm51bGwsIm9yZ05hbWUiOiLkuK3lpK7nvJblip7kuovkuJrlj5HlsZXkuK3lv4MiLCJvcmdTeXNUeXBlIjoiMDEiLCJvcmdUeXBlIjoiMiIsImlzQWRtaW4iOiIwIiwiaXNQdWJsaWMiOjAsImZ1bGxOYW1lIjoi5YiY5LyfIiwibmlja05hbWUiOiLliJjkvJ8tY29uYWMiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiMTExIn1dLCJlbmFibGVkIjp0cnVlLCJpc1RyaWFsUG9pbnRzIjoiMSIsImlzTW9iaWxlTG9naW4iOiIxIiwiaXNDb21wbGV0ZSI6IjAiLCJpc1JlcGVhdCI6bnVsbCwic2lDb2RlIjoiNDA0ODc3MzIiLCJpbWdVcmwiOiIvb3JnaW5mby91c2VyL2Y4Y2YvOTRlYS9lNTliL2VhOTYvODRkM2JkZGVhZWRmZWU0L3VzZXIwNC5wbmciLCJxckNvZGVLZXkiOm51bGwsIm9yZ0RvbWFpbk5hbWUiOm51bGx9fQ.huUVzzEYdHdaZ2FgD_ghv7h-MlG3GsZHBilWqfVIJIXKlbAGJu7E6Oexig1IqnYpX7ztZsxQgk04QToIF5qrBQ");
			
			// 调用接口，开始测试
			CloseableHttpResponse response = httpClient.execute(httpGet);
					
			HttpEntity entity = response.getEntity();
			String result=EntityUtils.toString(entity);
			System.out.println("测试的返回值： "+result);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 关闭连接,释放资源
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
