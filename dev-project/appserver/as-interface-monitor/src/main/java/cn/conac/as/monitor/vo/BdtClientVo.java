package cn.conac.as.monitor.vo;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

import cn.conac.as.monitor.entity.BdtClientEntity;
import io.swagger.annotations.ApiModel;

/**
 * 客户端实体的vo类
 * @author lishen
 * @date	2017年7月25日下午3:25:02
 * @version 1.0
 */
@ApiModel
public class BdtClientVo extends BdtClientEntity {

	private static final long serialVersionUID = -5081935096617065279L;

	/**
	 * 将BtdClientVo对象转换为BtdClientEntity对象
	 * @param vo
	 * @return
	 */
	public BdtClientEntity toEntity(){
		BdtClientEntity bdtClientEntity=new BdtClientEntity();
		try {
			PropertyUtils.copyProperties(bdtClientEntity, this);
			return bdtClientEntity;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
}
