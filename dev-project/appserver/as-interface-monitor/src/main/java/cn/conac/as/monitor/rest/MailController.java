package cn.conac.as.monitor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.conac.as.monitor.service.EmailService;

/**
 * Created by liuwei on 2017-7-13.
 */
@Controller
public class MailController
{
        @Autowired
        private EmailService emailService;

        @RequestMapping(value = "/bdtlaw/monitor/mail",method = RequestMethod.GET)
        public String mail() {
//            emailService.sendTemplateMail("1534499736@qq.com");
            return "success";
        }

}
