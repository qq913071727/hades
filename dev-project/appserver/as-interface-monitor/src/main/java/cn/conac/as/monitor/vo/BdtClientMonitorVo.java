package cn.conac.as.monitor.vo;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;

import org.apache.commons.beanutils.PropertyUtils;

import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import io.swagger.annotations.ApiModel;

/**
 * 客户端监控VO类
 * @author lishen
 * @date	2017年7月12日下午2:21:36
 * @version 1.0
 */
@ApiModel
public class BdtClientMonitorVo extends BdtClientMonitorEntity {

	private static final long serialVersionUID = -3463934026104411027L;
	
	/**
	 * 异常错误的开始时间
	 */
	private String beginTime;

	/**异常错误的结束时间
	 * 
	 */
	private String endTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public BdtClientMonitorVo() {
		super();
	}


	/**
	 * 将BtdClientMonitorVo对象转换为BtdClientMonitorEntity对象
	 * @param vo
	 * @return
	 */
	public BdtClientMonitorEntity toEntity(){
		BdtClientMonitorEntity bdtClientMonitorEntity=new BdtClientMonitorEntity();
		try {
			PropertyUtils.copyProperties(bdtClientMonitorEntity, this);
			return bdtClientMonitorEntity;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
}
