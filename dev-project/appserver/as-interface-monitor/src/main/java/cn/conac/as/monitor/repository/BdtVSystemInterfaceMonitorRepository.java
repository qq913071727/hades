package cn.conac.as.monitor.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtVSystemInterfaceMonitorEntity;

/**
 * 系统接口监控视图的DAO类
 * @author lishen
 * @date	2017年7月13日上午8:40:34
 * @version 1.0
 */
@Repository
public interface BdtVSystemInterfaceMonitorRepository extends GenericDao<BdtVSystemInterfaceMonitorEntity, String> {

	/**
	 * 根据id和interfaceId获取BdtVSystemInterfaceMonitorEntity对象
	 * @param id
	 * @param interfaceId
	 * @return
	 */
	BdtVSystemInterfaceMonitorEntity findByIdAndInterfaceId(String id, String interfaceId);
	
	/**
	 * 根据id查找BdtVSystemInterfaceMonitorEntity对象列表，并按照降序排列
	 * @param id
	 * @return
	 */
	List<BdtVSystemInterfaceMonitorEntity> findByIdOrderByEndTimeDesc(String id);
	
	/**
	 * 根据interfaceId和environmentType查找BdtVSystemInterfaceMonitorEntity对象列表
	 * @param interfaceId
	 * @param environmentType
	 * @return
	 */
	List<BdtVSystemInterfaceMonitorEntity> findByInterfaceIdAndEnvironmentTypeOrderByEndTimeAsc(String interfaceId, String environmentType);
	
}
