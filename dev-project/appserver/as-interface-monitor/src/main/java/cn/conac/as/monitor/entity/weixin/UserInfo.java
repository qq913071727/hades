package cn.conac.as.monitor.entity.weixin;

import java.util.List;
import java.util.Map;

/**
 * Created by liuwei on 2017-7-12.
 */
public class UserInfo
{
    private String openid;
    private String total;
    private String count;
    private Map<String,List<String>> data;

    public String getOpenid()
    {
        return openid;
    }

    public void setOpenid(String openid)
    {
        this.openid = openid;
    }

    public String getTotal()
    {
        return total;
    }

    public void setTotal(String total)
    {
        this.total = total;
    }

    public String getCount()
    {
        return count;
    }

    public void setCount(String count)
    {
        this.count = count;
    }

    public Map<String, List<String>> getData()
    {
        return data;
    }

    public void setData(Map<String, List<String>> data)
    {
        this.data = data;
    }
}
