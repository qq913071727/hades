package cn.conac.as.monitor.constant;

import cn.conac.as.monitor.util.PropertiesUtil;

/**
 * 扫描结果类型类
 * @author lishen
 * @date	2017年7月10日下午3:17:18
 * @version 1.0
 */
public class ScanResultType {

	/**
	 * 不可用
	 */
	public static final String DISABLE="0";
	
	/**
	 * 接口不可用的中文描述
	 */
	public static final String DISABLE_DESCRIPTION="接口不可用";
	
	/**
	 * 正常
	 */
	public static final String NORMAL="1";
	
	/**
	 * 接口正常的中文描述
	 */
	public static final String NORMAL_DESCRIPTION="接口正常";
	
	/**
	 * 预警
	 */
	public static final String PREWARNING="2";
	
	/**
	 * 接口预警的中文描述
	 */
	public static final String PREWARNING_DESCRIPTION="接口预警";
	
	/**
	 * 警告
	 */
	public static final String WARNING="3";
	
	/**
	 * 接口警告的中文描述
	 */
	public static final String WARNING_DESCRIPTION="接口响应时间超过"+Integer
			.parseInt(PropertiesUtil.getValue("config/config.properties", "interface.response.warning.time"))+"秒";
	
	/**
	 * 接口调用返回结果为json时，表示调用是否成功的key
	 */
	public static final String RESULT_KEY="code";
	
	/**
	 * 接口成功返回后，msg属性的值
	 */
	public static final String SUCCESS="SUCCESS";
	
	/**
	 * 接口成功返回后，code属性的值
	 */
	public static final String SUCCESS_CODE="1000";
	
	/**
	 * 接口调用成功
	 */
	public static final String INTERFACE_SUCCESS="1";
	
	/**
	 * 接口调用失败
	 */
	public static final String INTERFACE_FAIL="0";
	
	/**
	 * 返回类型为JSON字符串
	 */
	public static final String JSON_ARRAY="JSONArray";
	
	/**
	 * 返回类型为InputStream（下载文件时）
	 */
	public static final String INPUT_STREAM="InputStream";
}
