package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import cn.conac.as.monitor.constant.ScanResultType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 接口监控类
 * @author lishen
 * @date	2017年7月7日上午11:06:15
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_INTERFACE_MONITOR")
public class BdtInterfaceMonitorEntity extends BaseEntity<BdtInterfaceMonitorEntity> implements Serializable {

	private static final long serialVersionUID = 753967427132617726L;
	
	@ApiModelProperty("接口id")
	private String interfaceId;
	
	@ApiModelProperty("扫描时间")
	private Timestamp scanTime;
	
	@ApiModelProperty("结束时间")
	private Timestamp endTime;
	
	@ApiModelProperty("响应时间")
	private String responseTime;
	
	@ApiModelProperty("错误信息")
	private String errorInfo;
	
	@ApiModelProperty("扫描结果")
	private String scanResult;
	
	public BdtInterfaceMonitorEntity() {
		super();
	}

	public BdtInterfaceMonitorEntity(String interfaceId, Timestamp scanTime, Timestamp endTime, String responseTime,
			String errorInfo, String scanResult) {
		super();
		this.interfaceId = interfaceId;
		this.scanTime = scanTime;
		this.endTime = endTime;
		this.responseTime = responseTime;
		this.errorInfo = errorInfo;
		this.scanResult = scanResult;
	}

	public String getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}

	public Timestamp getScanTime() {
		return scanTime;
	}

	public void setScanTime(Timestamp scanTime) {
		this.scanTime = scanTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getScanResult() {
		return scanResult;
	}

	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((errorInfo == null) ? 0 : errorInfo.hashCode());
		result = prime * result + ((interfaceId == null) ? 0 : interfaceId.hashCode());
		result = prime * result + ((responseTime == null) ? 0 : responseTime.hashCode());
		result = prime * result + ((scanResult == null) ? 0 : scanResult.hashCode());
		result = prime * result + ((scanTime == null) ? 0 : scanTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdtInterfaceMonitorEntity other = (BdtInterfaceMonitorEntity) obj;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (errorInfo == null) {
			if (other.errorInfo != null)
				return false;
		} else if (!errorInfo.equals(other.errorInfo))
			return false;
		if (interfaceId == null) {
			if (other.interfaceId != null)
				return false;
		} else if (!interfaceId.equals(other.interfaceId))
			return false;
		if (responseTime == null) {
			if (other.responseTime != null)
				return false;
		} else if (!responseTime.equals(other.responseTime))
			return false;
		if (scanResult == null) {
			if (other.scanResult != null)
				return false;
		} else if (!scanResult.equals(other.scanResult))
			return false;
		if (scanTime == null) {
			if (other.scanTime != null)
				return false;
		} else if (!scanTime.equals(other.scanTime))
			return false;
		return true;
	}

	/**
	 * 返回接口监控对象的中文描述
	 * @return
	 */
	public BdtInterfaceMonitorEntity toDescription(){
		if(this.getScanResult().equals(ScanResultType.DISABLE)){
			this.setScanResult(ScanResultType.DISABLE_DESCRIPTION);
		}
		if(this.getScanResult().equals(ScanResultType.NORMAL)){
			this.setScanResult(ScanResultType.NORMAL_DESCRIPTION);
		}
		if(this.getScanResult().equals(ScanResultType.PREWARNING)){
			this.setScanResult(ScanResultType.PREWARNING_DESCRIPTION);
		}
		if(this.getScanResult().equals(ScanResultType.WARNING)){
			this.setScanResult(ScanResultType.WARNING_DESCRIPTION);
		}
		return this;
	}
	
}
