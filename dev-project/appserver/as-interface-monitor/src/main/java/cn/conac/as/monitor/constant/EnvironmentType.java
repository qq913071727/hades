package cn.conac.as.monitor.constant;

/**
 * 环境类型常量类
 * @author lishen
 * @date	2017年7月27日下午3:31:46
 * @version 1.0
 */
public class EnvironmentType {

	/**
	 * 生产环境
	 */
	public static final String PRODUCTION_ENVIRONMENT="1";
	
	/**
	 * 生产环境的中文描述
	 */
	public static final String PRODUCTION_ENVIRONMENT_DESCRIPTION="生产环境";
	
	/**
	 * 测试环境
	 */
	public static final String TEST_ENVIRONMENT="2";
	
	/**
	 * 测试环境的中文描述
	 */
	public static final String TEST_ENVIRONMENT_DESCRIPTION="测试环境";

}
