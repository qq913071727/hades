package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 客户端监控实体类
 * @author lishen
 * @date	2017年7月12日上午9:21:13
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_CLIENT_MONITOR")
public class BdtClientMonitorEntity extends BaseEntity<BdtClientMonitorEntity> implements Serializable {

	private static final long serialVersionUID = -7208250694284875795L;

	@ApiModelProperty("运营商")
	private String carrieroperator;
	
	@ApiModelProperty("客户端版本")
	private String clientVersion;
	
	@ApiModelProperty("联网方式")
	private String connectNetworkType;
	
	@ApiModelProperty("地域")
	private String userRegion;
	
	@ApiModelProperty("异常/错误时间")
	private Timestamp exceptionErrorTime;
	
	@ApiModelProperty("异常/错误信息")
	private String exceptionErrorInfo;
	
	@ApiModelProperty("异常/错误级别")
	private String exceptionErrorLevel;
	
	@ApiModelProperty("令牌")
	private String token;
	
	@ApiModelProperty("登录类型")
	private String loginType;
	
	@ApiModelProperty("用户名")
	private String username;
	
	@ApiModelProperty("客户端id")
	private String clientId;

	public BdtClientMonitorEntity() {
	}
	
	public BdtClientMonitorEntity(String carrieroperator, String clientVersion, String connectNetworkType,
			String userRegion, Timestamp exceptionErrorTime, String exceptionErrorInfo,
			String exceptionErrorLevel, String token, String loginType, String username, String clientId) {
		super();
		this.carrieroperator = carrieroperator;
		this.clientVersion = clientVersion;
		this.connectNetworkType = connectNetworkType;
		this.userRegion = userRegion;
		this.exceptionErrorTime = exceptionErrorTime;
		this.exceptionErrorInfo = exceptionErrorInfo;
		this.exceptionErrorLevel = exceptionErrorLevel;
		this.token = token;
		this.loginType = loginType;
		this.username = username;
		this.clientId = clientId;
	}

	public String getCarrieroperator() {
		return carrieroperator;
	}

	public void setCarrieroperator(String carrieroperator) {
		this.carrieroperator = carrieroperator;
	}

	public String getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}

	public String getConnectNetworkType() {
		return connectNetworkType;
	}

	public void setConnectNetworkType(String connectNetworkType) {
		this.connectNetworkType = connectNetworkType;
	}

	public String getUserRegion() {
		return userRegion;
	}

	public void setUserRegion(String userRegion) {
		this.userRegion = userRegion;
	}

	public Timestamp getExceptionErrorTime() {
		return exceptionErrorTime;
	}

	public void setExceptionErrorTime(Timestamp exceptionErrorTime) {
		this.exceptionErrorTime = exceptionErrorTime;
	}

	public String getExceptionErrorInfo() {
		return exceptionErrorInfo;
	}

	public void setExceptionErrorInfo(String exceptionErrorInfo) {
		this.exceptionErrorInfo = exceptionErrorInfo;
	}

	public String getExceptionErrorLevel() {
		return exceptionErrorLevel;
	}

	public void setExceptionErrorLevel(String exceptionErrorLevel) {
		this.exceptionErrorLevel = exceptionErrorLevel;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}	

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carrieroperator == null) ? 0 : carrieroperator.hashCode());
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((clientVersion == null) ? 0 : clientVersion.hashCode());
		result = prime * result + ((connectNetworkType == null) ? 0 : connectNetworkType.hashCode());
		result = prime * result + ((exceptionErrorInfo == null) ? 0 : exceptionErrorInfo.hashCode());
		result = prime * result + ((exceptionErrorLevel == null) ? 0 : exceptionErrorLevel.hashCode());
		result = prime * result + ((exceptionErrorTime == null) ? 0 : exceptionErrorTime.hashCode());
		result = prime * result + ((loginType == null) ? 0 : loginType.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((userRegion == null) ? 0 : userRegion.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdtClientMonitorEntity other = (BdtClientMonitorEntity) obj;
		if (carrieroperator == null) {
			if (other.carrieroperator != null)
				return false;
		} else if (!carrieroperator.equals(other.carrieroperator))
			return false;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (clientVersion == null) {
			if (other.clientVersion != null)
				return false;
		} else if (!clientVersion.equals(other.clientVersion))
			return false;
		if (connectNetworkType == null) {
			if (other.connectNetworkType != null)
				return false;
		} else if (!connectNetworkType.equals(other.connectNetworkType))
			return false;
		if (exceptionErrorInfo == null) {
			if (other.exceptionErrorInfo != null)
				return false;
		} else if (!exceptionErrorInfo.equals(other.exceptionErrorInfo))
			return false;
		if (exceptionErrorLevel == null) {
			if (other.exceptionErrorLevel != null)
				return false;
		} else if (!exceptionErrorLevel.equals(other.exceptionErrorLevel))
			return false;
		if (exceptionErrorTime == null) {
			if (other.exceptionErrorTime != null)
				return false;
		} else if (!exceptionErrorTime.equals(other.exceptionErrorTime))
			return false;
		if (loginType == null) {
			if (other.loginType != null)
				return false;
		} else if (!loginType.equals(other.loginType))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (userRegion == null) {
			if (other.userRegion != null)
				return false;
		} else if (!userRegion.equals(other.userRegion))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	
}
