package cn.conac.as.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import cn.conac.as.monitor.constant.OperationSystemType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 客户端实体类
 * @author lishen
 * @date	2017年7月25日上午11:14:19
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_CLIENT")
public class BdtClientEntity extends BaseEntity<BdtClientEntity> implements Serializable {

	private static final long serialVersionUID = 3329280571792298362L;

	@ApiModelProperty("设备号")
	private String deviceNumber;
	
	@ApiModelProperty("手机型号")
	private String mobilePhoneType;
	
	@ApiModelProperty("操作系统版本")
	private String operationSystemVersion;
	
	@ApiModelProperty("操作系统类型")
	private String operationSystemType;
	
	@ApiModelProperty("友盟编号")
	private String umengNumber;
	
	public BdtClientEntity() {
		super();
	}

	public BdtClientEntity(String deviceNumber, String mobilePhoneType, String operationSystemVersion,
			String operationSystemType, String umengNumber) {
		super();
		this.deviceNumber = deviceNumber;
		this.mobilePhoneType = mobilePhoneType;
		this.operationSystemVersion = operationSystemVersion;
		this.operationSystemType = operationSystemType;
		this.umengNumber = umengNumber;
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getMobilePhoneType() {
		return mobilePhoneType;
	}

	public void setMobilePhoneType(String mobilePhoneType) {
		this.mobilePhoneType = mobilePhoneType;
	}

	public String getOperationSystemVersion() {
		return operationSystemVersion;
	}

	public void setOperationSystemVersion(String operationSystemVersion) {
		this.operationSystemVersion = operationSystemVersion;
	}

	public String getOperationSystemType() {
		return operationSystemType;
	}

	public void setOperationSystemType(String operationSystemType) {
		this.operationSystemType = operationSystemType;
	}

	public String getUmengNumber() {
		return umengNumber;
	}

	public void setUmengNumber(String umengNumber) {
		this.umengNumber = umengNumber;
	}

	/**
	 * 用中文来描述运营商等字段
	 * @return
	 */
	public BdtClientEntity toDescription() {
		// 操作系统
		if (this.getOperationSystemType().equals(OperationSystemType.OTHER)) {
			this.setOperationSystemType(OperationSystemType.OTHER_DESCRIPTION);
		}
		if (this.getOperationSystemType().equals(OperationSystemType.ANDROID)) {
			this.setOperationSystemType(OperationSystemType.ANDROID_DESCRIPTION);
		}
		if (this.getOperationSystemType().equals(OperationSystemType.IOS)) {
			this.setOperationSystemType(OperationSystemType.IOS_DESCRIPTION);
		}
		return this;
	}
}
