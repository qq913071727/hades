package cn.conac.as.monitor.constant;

/**
 * 运营商常量类
 * @author lishen
 * @date	2017年7月12日下午5:27:18
 * @version 1.0
 */
public class Carrieroperator {

	/**
	 * 其他
	 */
	public static final String OTHER="0";
	
	/**
	 * 其他的中文描述
	 */
	public static final String OTHER_DESCRIPTION="其他";
	
	/**
	 * 中国移动
	 */
	public static final String CHINA_MOBILE="1";
	
	/**
	 * 中国移动的中文描述
	 */
	public static final String CHINA_MOBILE_DESCRIPTION="中国移动";

	/**
	 * 中国联通
	 */
	public static final String CHINA_UNICOM="2";
	
	/**
	 * 中国联通的中文描述
	 */
	public static final String CHINA_UNICOM_DESCRIPTION="中国联通";
	
	/**
	 * 中国电信
	 */
	public static final String CHINA_TELECOM="3";
	
	/**
	 * 中国电信的中文描述
	 */
	public static final String CHINA_TELECOM_DESCRIPTION="中国电信";
}
