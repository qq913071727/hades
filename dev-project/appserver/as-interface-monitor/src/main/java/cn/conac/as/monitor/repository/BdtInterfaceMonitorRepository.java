package cn.conac.as.monitor.repository;

import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtInterfaceMonitorEntity;

/**
 * 接口监控的DAO类
 * @author lishen
 * @date	2017年7月7日下午2:17:59
 * @version 1.0
 */
@Repository
public interface BdtInterfaceMonitorRepository extends GenericDao<BdtInterfaceMonitorEntity, String> {

}
