package cn.conac.as.monitor.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.conac.as.monitor.constant.AllowTest;
import cn.conac.as.monitor.constant.EnvironmentType;
import cn.conac.as.monitor.entity.BdtInterfaceEntity;
import cn.conac.as.monitor.entity.BdtVSystemInterfaceMonitorEntity;
import cn.conac.as.monitor.service.BdtInterfaceService;
import cn.conac.as.monitor.service.BdtVSystemInterfaceMonitorService;

/**
 * Created by liuwei on 2017-7-13.
 */
@Controller
public class WebController {
	@Autowired
	private BdtInterfaceService bdtInterfaceService;
	
	@Autowired
	private BdtVSystemInterfaceMonitorService bdtVSystemInterfaceMonitorService;

	@RequestMapping(value = "/bdtlaw/monitor/index", method = RequestMethod.GET)
	public String index(Model model) {
		List<BdtInterfaceEntity> interfaceList = bdtInterfaceService
				.findByEnvironmentTypeAndAllowTest(EnvironmentType.PRODUCTION_ENVIRONMENT, AllowTest.ALLOW_TEST);
		model.addAttribute("interfaceList", interfaceList);
		return "/statistics/interflist";
	}

	@RequestMapping(value = "/bdtlaw/monitor/interfacelist", method = RequestMethod.GET)
	public String interfaceList(Model model) {
		List<BdtInterfaceEntity> interfaceList = bdtInterfaceService
				.findByEnvironmentTypeAndAllowTest(EnvironmentType.PRODUCTION_ENVIRONMENT, AllowTest.ALLOW_TEST);
		model.addAttribute("interfaceList", interfaceList);
		return "/statistics/interflist";
	}

	@RequestMapping(value = "/bdtlaw/monitor/statistics/{interfaceId}", method = RequestMethod.GET)
	public String statistics(HttpServletRequest request, HttpServletResponse response, Model model, 
			@PathVariable("interfaceId") String interfaceId) {
		List<BdtVSystemInterfaceMonitorEntity> bdtVSystemInterfaceMonitorEntityList=bdtVSystemInterfaceMonitorService.findByInterfaceIdAndEnvironmentTypeOrderByEndTimeAsc(interfaceId, EnvironmentType.PRODUCTION_ENVIRONMENT_DESCRIPTION);
		model.addAttribute("bdtVSystemInterfaceMonitorEntityList", bdtVSystemInterfaceMonitorEntityList);
		model.addAttribute("description", bdtVSystemInterfaceMonitorEntityList.get(0).getDescription());
		model.addAttribute("url", bdtVSystemInterfaceMonitorEntityList.get(0).getUrl());
		model.addAttribute("method", bdtVSystemInterfaceMonitorEntityList.get(0).getMethod());
		return "/statistics/statistics";
	}

}
