package cn.conac.as.monitor.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.as.framework.vo.ResultPojo;
import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import cn.conac.as.monitor.entity.BdtVClientMonitorEntity;
import cn.conac.as.monitor.service.BdtVClientMonitorService;
import cn.conac.as.monitor.vo.BdtClientMonitorVo;
import cn.conac.as.monitor.vo.BdtVClientMonitorVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 客户端监控视图的控制器
 * @author lishen
 * @date	2017年7月25日下午2:58:03
 * @version 1.0
 */
@RestController
public class BdtVClientMonitorController {

	@Autowired
	BdtVClientMonitorService bdtVClientMonitorService;

	/**
	 * 接口：根据id获取某一次客户端监控的详细信息
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "根据id获取某一次客户端监控的详细信息", httpMethod = "GET", response = BdtVClientMonitorEntity.class, notes = "根据id获取某一次客户端监控的详细信息")
	@RequestMapping(value = "/api/client/get/clientexception/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// service调用
		BdtVClientMonitorEntity bdtVClientMonitorEntity = bdtVClientMonitorService.findById(id);
		
		// 将部分字段转换为中文描述
		if(null!=bdtVClientMonitorEntity){
			bdtVClientMonitorEntity.toDescription();
		}
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		result.setResult(bdtVClientMonitorEntity);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	/**
	 * 查找所有的BdtVClientMonitorEntity对象，并且按照endTime降序排列
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value ="/bdtlaw/monitor/findAllBdtVClientMonitorEntity", method = RequestMethod.POST)
    public List<BdtVClientMonitorEntity> findAll(HttpServletRequest request, HttpServletResponse response){
        List<BdtVClientMonitorEntity> bdtVClientMonitorEntityList = bdtVClientMonitorService.findAll(new Sort(Sort.Direction.DESC, "exceptionErrorTime"));
        if(null!=bdtVClientMonitorEntityList && bdtVClientMonitorEntityList.size()!=0){
        	for(int i=0; i<bdtVClientMonitorEntityList.size(); i++){
        		bdtVClientMonitorEntityList.get(i).toDescription();
        	}
        	return bdtVClientMonitorEntityList;
        }
        return null;
    }
	
	/**
	 * 根据条件查找BdtVClientMonitorEntity对象
	 * @param request
	 * @param response
	 * @param scanTime
	 * @param endTime
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value ="/bdtlaw/monitor/findVClientMonitorByCondition", method = RequestMethod.POST)
    public List<BdtVClientMonitorEntity> findClientMonitorByCondition(HttpServletRequest request, HttpServletResponse response, 
    		@ModelAttribute BdtVClientMonitorVo bdtVClientMonitorVo){
		
		List<BdtVClientMonitorEntity> bdtVClientMonitorEntityList=bdtVClientMonitorService.findVClientMonitorByCondition(bdtVClientMonitorVo);
		for(int i=0; i<bdtVClientMonitorEntityList.size(); i++){
    		bdtVClientMonitorEntityList.get(i).toDescription();
    	}
		return bdtVClientMonitorEntityList;
    }
}
