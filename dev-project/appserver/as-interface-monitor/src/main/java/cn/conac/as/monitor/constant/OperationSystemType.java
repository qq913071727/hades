package cn.conac.as.monitor.constant;

/**
 * 操作系统类型常量类
 * @author lishen
 * @date	2017年7月12日下午5:24:59
 * @version 1.0
 */
public class OperationSystemType {

	/**
	 * 其他
	 */
	public static final String OTHER="0";
	
	/**
	 * 其他的中文描述
	 */
	public static final String OTHER_DESCRIPTION="其他";
	
	/**
	 * android
	 */
	public static final String ANDROID="1";
	
	/**
	 * android的中文描述
	 */
	public static final String ANDROID_DESCRIPTION="android";
	
	/**
	 * IOS
	 */
	public static final String IOS="2";
	
	/**
	 * IOS的中文描述
	 */
	public static final String IOS_DESCRIPTION="IOS";
}
