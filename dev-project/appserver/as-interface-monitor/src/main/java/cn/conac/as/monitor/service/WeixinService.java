package cn.conac.as.monitor.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.utils.FastJsonUtil;
import cn.conac.as.monitor.constant.WeixinFinalValueType;
import cn.conac.as.monitor.entity.weixin.ErrorEntity;
import cn.conac.as.monitor.entity.weixin.UserInfo;
import cn.conac.as.monitor.entity.weixin.WeixinContext;
import cn.conac.as.monitor.entity.weixin.WxinTemplate;

/**
 * Created by liuwei on 2017-7-13.
 */
@Service
public class WeixinService
{

    public static String postTemplateMsg(WxinTemplate tm){
        CloseableHttpClient client =null;
        CloseableHttpResponse resp = null;
        try {
            client= HttpClients.createDefault();
            String url = WeixinFinalValueType.SEND_TEMPLATE_MSG;
//            url = url.replace("ACCESS_TOKEN", RefreshAccessTokenTask.at);
            url = url.replace("ACCESS_TOKEN", WeixinContext.getAccessToken());
            System.out.println(WeixinContext.getAccessToken()+"++获得的token+++++++++++++++++++++++++++++++++++22222");
//            url = url.replace("ACCESS_TOKEN", "NLfM9HENzkulBe1FevVw1HCBU_AYA92POaY_bl2a2IVOL25bl6_LiaWjtImNE_DrpxoWLI0ysdOgQHOpU99ScJUmo5xMf25XwnE272O8U15SweclKTD3YN9rxGowlm0DOAUcABAVHB");
            HttpPost post = new HttpPost(url);
            String json = FastJsonUtil.getJson(tm);
            post.addHeader("Content-type","application/json");

            StringEntity entity = new StringEntity(json, ContentType.create("application/json","UTF-8"));
            post.setEntity(entity);
            resp = client.execute(post);
            int statusCode = resp.getStatusLine().getStatusCode();
            if(statusCode >=200 && statusCode<300){
                String str = EntityUtils.toString(resp.getEntity());
                System.out.println(str);
                return str;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(client!=null) client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if(resp!=null) resp.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<String> getUserOpenId(){

        try {
            CloseableHttpClient client = HttpClients.createDefault();
            String url = WeixinFinalValueType.USER_GET;
//            url = url.replaceAll("ACCESS_TOKEN", RefreshAccessTokenTask.at);
            url = url.replaceAll("ACCESS_TOKEN", WeixinContext.getAccessToken());
            System.out.println(WeixinContext.getAccessToken()+"++获得的token+++++++++++++++++++++++++++++++++++");
            url = url.replaceAll("NEXT_OPENID", "");
            HttpGet get = new HttpGet(url);
            CloseableHttpResponse resp = client.execute(get);
            int statusCode = resp.getStatusLine().getStatusCode();
            if(statusCode>=200&&statusCode<300) {
                HttpEntity entity = resp.getEntity();
                String content = EntityUtils.toString(entity,"UTF-8");
                try {
//					AccessToken at = (AccessToken) JsonUtil.getInstance().json2obj(content, AccessToken.class);
                    UserInfo userInfo = FastJsonUtil.getObject(content, UserInfo.class);
                    Map<String, List<String>> data = userInfo.getData();
                    List<String> openidList = data.get("openid");
                    System.out.println(openidList.toString());
                    System.out.println(data.toString()+"data");
                    System.out.println(content);
                    return openidList;

                } catch (Exception e) {
//					ErrorEntity err = (ErrorEntity)JsonUtil.getInstance().json2obj(content, ErrorEntity.class);
                    ErrorEntity err = (ErrorEntity)FastJsonUtil.getObject(content, ErrorEntity.class);
                    System.out.println("获取token异常:"+err.getErrcode()+","+err.getErrmsg());

                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
