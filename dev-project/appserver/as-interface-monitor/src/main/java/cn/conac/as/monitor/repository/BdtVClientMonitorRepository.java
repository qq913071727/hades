package cn.conac.as.monitor.repository;

import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtVClientMonitorEntity;

/**
 * 客户端监控的试图类的dao类
 * @author lishen
 * @date	2017年7月25日下午2:38:32
 * @version 1.0
 */
@Repository
public interface BdtVClientMonitorRepository extends GenericDao<BdtVClientMonitorEntity, String> {

}
