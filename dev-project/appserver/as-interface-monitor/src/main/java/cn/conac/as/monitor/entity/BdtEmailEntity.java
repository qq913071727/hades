package cn.conac.as.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 电子邮箱实体类
 * @author lishen
 * @date	2017年7月26日下午3:41:25
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_EMAIL")
public class BdtEmailEntity extends BaseEntity<BdtEmailEntity> implements Serializable {

	private static final long serialVersionUID = -4025682005401587102L;
	
	@ApiModelProperty("邮箱地址")
	private String address;
	
	@ApiModelProperty("是否发送邮件")
	private String allowSend;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAllowSend() {
		return allowSend;
	}

	public void setAllowSend(String allowSend) {
		this.allowSend = allowSend;
	}

	public BdtEmailEntity() {
		super();
	}

	public BdtEmailEntity(String address, String allowSend) {
		super();
		this.address = address;
		this.allowSend = allowSend;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((allowSend == null) ? 0 : allowSend.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdtEmailEntity other = (BdtEmailEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (allowSend == null) {
			if (other.allowSend != null)
				return false;
		} else if (!allowSend.equals(other.allowSend))
			return false;
		return true;
	}
}
