package cn.conac.as.monitor.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtEmailEntity;

/**
 * 邮箱的DAO类
 * @author lishen
 * @date	2017年7月26日下午3:47:11
 * @version 1.0
 */
@Repository
public interface BdtEmailRepository extends GenericDao<BdtEmailEntity, String>, JpaSpecificationExecutor<BdtEmailEntity> {

}
