package cn.conac.as.monitor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.conac.as.monitor.service.BdtInterfaceMonitorService;
import cn.conac.as.monitor.util.HttpClientUtil;
import cn.conac.as.monitor.util.PropertiesUtil;

/**
 * 接口定时监控器类
 * 
 * @author lishen
 * @date 2017年7月7日上午10:46:43
 * @version 1.0
 */
@Component
@Order(value = 1)
public class InterfaceTimingMonitor implements CommandLineRunner {
	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(InterfaceTimingMonitor.class);
	
	/**
	 * 首次执行的延时时间
	 */
	private static final int FIRST_RUN_DELAY_TIME = Integer
			.parseInt(PropertiesUtil.getValue("config/config.properties", "first.run.delay.time"));

	/**
	 * 定时执行的间隔时间
	 */
	private static final int REGULAR_TIME = Integer
			.parseInt(PropertiesUtil.getValue("config/config.properties", "regular.time"));

	@Autowired
	BdtInterfaceMonitorService bdtInterfaceMonitorService;

	/**
	 * 容器启动后，执行下面的方法
	 */
	@Override
	public void run(String... arg0) throws Exception {
		Runnable runnable = new Runnable() {
			public void run() {
				logger.info("接口监控定时任务开始...");
				bdtInterfaceMonitorService.startTimingMonitor();
				logger.info("本次接口监控定时任务结束...");
			}
		};
		ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
		service.scheduleAtFixedRate(runnable, FIRST_RUN_DELAY_TIME, REGULAR_TIME, TimeUnit.SECONDS);
	}

}
