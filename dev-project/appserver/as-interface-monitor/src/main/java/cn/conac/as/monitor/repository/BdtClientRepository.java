package cn.conac.as.monitor.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtClientEntity;

/**
 * 客户端的dao类
 * @author lishen
 * @date	2017年7月25日下午3:14:12
 * @version 1.0
 */
@Repository
public interface BdtClientRepository extends GenericDao<BdtClientEntity, String>, JpaSpecificationExecutor<BdtClientEntity> {

	/**
	 * 根据设备号获取一个客户端的基础信息
	 * @param deviceNumber
	 * @return
	 */
	BdtClientEntity findByDeviceNumber(String deviceNumber);
}
