package cn.conac.as.monitor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.service.GenericService;
import cn.conac.as.monitor.entity.BdtClientEntity;
import cn.conac.as.monitor.repository.BdtClientRepository;

/**
 * 客户端的服务类
 * @author lishen
 * @date	2017年7月25日下午3:15:54
 * @version 1.0
 */
@Service
public class BdtClientService extends GenericService<BdtClientEntity, String> {

	@Autowired
	private BdtClientRepository bdtClientRepository;

	/**
	 * 根据设备号获取一个客户端的基础信息
	 * @param deviceNumber
	 * @return
	 */
	public BdtClientEntity findByDeviceNumber(String deviceNumber){
		return bdtClientRepository.findByDeviceNumber(deviceNumber);
	}
}
