package cn.conac.as.monitor.constant;

/**
 * 联网方式常量类
 * @author lishen
 * @date	2017年7月12日下午5:30:39
 * @version 1.0
 */
public class ConnectNetworkType {

	/**
	 * 其他
	 */
	public static final String OTHER="0";
	
	/**
	 * 其他的中文描述
	 */
	public static final String OTHER_DESCRIPTION="其他";

	/**
	 * wifi
	 */
	public static final String WIFI="1";
	
	/**
	 * wifi的中文描述
	 */
	public static final String WIFI_DESCRIPTION="wifi";
	
	/**
	 * 2G
	 */
	public static final String TWO_G="2";
	
	/**
	 * 2G的中文描述
	 */
	public static final String TWO_G_DESCRIPTION="2G";
	
	/**
	 * 3G
	 */
	public static final String THREE_G="3";
	
	/**
	 * 3G的中文描述
	 */
	public static final String THREE_G_DESCRIPTION="3G";
	
	/**
	 * 4G
	 */
	public static final String FOUR_G="4";
	
	/**
	 * 4G的中文描述
	 */
	public static final String FOUR_G_DESCRIPTION="4G";
}
