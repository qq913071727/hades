package cn.conac.as.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;

@Entity
@Table(name = "BDT_SYSTEM")
public class BdtSystemEntity extends BaseEntity<BdtSystemEntity> implements Serializable{
    private static final long serialVersionUID = 7296822956205941897L;

    private String systemName;

    public String getSystemName()
    {
        return systemName;
    }

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }
}
