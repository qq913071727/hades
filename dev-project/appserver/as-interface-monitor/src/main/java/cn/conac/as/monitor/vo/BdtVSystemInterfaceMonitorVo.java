package cn.conac.as.monitor.vo;

import java.sql.Timestamp;

import cn.conac.as.monitor.entity.BdtVSystemInterfaceMonitorEntity;
import io.swagger.annotations.ApiModel;

/**
 * 接口监控VO类
 * @author lishen
 * @date	2017年7月20日下午3:26:01
 * @version 1.0
 */
@ApiModel
public class BdtVSystemInterfaceMonitorVo extends BdtVSystemInterfaceMonitorEntity {

	private static final long serialVersionUID = -7312543760293326317L;
	
	/**
	 * 异常错误的开始时间
	 */
	private String beginTime;

	/**异常错误的结束时间
	 * 
	 */
	private String finishTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

	public BdtVSystemInterfaceMonitorVo() {
	}

	public BdtVSystemInterfaceMonitorVo(Timestamp scanTime, Timestamp endTime, String responseTime, String errorInfo,
			String scanResult, String interfaceId, String url, String method, String description,
			String parameterUseCase, String environmentType, String operationType, String allowTest, String systemId,
			String systemName) {
		super(scanTime, endTime, responseTime, errorInfo, scanResult, interfaceId, url, method, description,
				parameterUseCase, environmentType, operationType, allowTest, systemId, systemName);
	}

	public BdtVSystemInterfaceMonitorVo(String beginTime, String finishTime) {
		super();
		this.beginTime = beginTime;
		this.finishTime = finishTime;
	}

}
