package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import cn.conac.as.monitor.constant.SystemType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 接口类
 * @author lishen
 * @date	2017年7月7日下午4:21:48
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_INTERFACE")
public class BdtInterfaceEntity extends BaseEntity<BdtInterfaceEntity> implements Serializable {

	private static final long serialVersionUID = -7004339376277239231L;

	@ApiModelProperty("url")
	private String url;
	
	@ApiModelProperty("HTTP请求方式")
	private String method;
	
	@ApiModelProperty("描述")
	private String description;
	
	@ApiModelProperty("参数以及用例说明")
	private String parameterUseCase;
	
	@ApiModelProperty("环境类型")
	private String environmentType;
	
	@ApiModelProperty("操作类型")
	private String operationType;
	
	@ApiModelProperty("所属系统id")
	private String belongSystemId;
	
	@ApiModelProperty("接口是否被测试")
	private String allowTest;
	
	@ApiModelProperty("接口调用成功时的code")
	private String successCode;

	public BdtInterfaceEntity() {
		super();
	}

	public BdtInterfaceEntity(String url, String method, String description, String parameterUseCase,
			String environmentType, String operationType, String belongSystemId, String allowTest, String successCode) {
		super();
		this.url = url;
		this.method = method;
		this.description = description;
		this.parameterUseCase = parameterUseCase;
		this.environmentType = environmentType;
		this.operationType = operationType;
		this.belongSystemId = belongSystemId;
		this.allowTest = allowTest;
		this.successCode = successCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParameterUseCase() {
		return parameterUseCase;
	}

	public void setParameterUseCase(String parameterUseCase) {
		this.parameterUseCase = parameterUseCase;
	}

	public String getEnvironmentType() {
		return environmentType;
	}

	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getBelongSystemId() {
		return belongSystemId;
	}

	public void setBelongSystemId(String belongSystemId) {
		this.belongSystemId = belongSystemId;
	}

	public String getAllowTest() {
		return allowTest;
	}

	public void setAllowTest(String allowTest) {
		this.allowTest = allowTest;
	}

	public String getSuccessCode() {
		return successCode;
	}

	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allowTest == null) ? 0 : allowTest.hashCode());
		result = prime * result + ((belongSystemId == null) ? 0 : belongSystemId.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((environmentType == null) ? 0 : environmentType.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((operationType == null) ? 0 : operationType.hashCode());
		result = prime * result + ((parameterUseCase == null) ? 0 : parameterUseCase.hashCode());
		result = prime * result + ((successCode == null) ? 0 : successCode.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdtInterfaceEntity other = (BdtInterfaceEntity) obj;
		if (allowTest == null) {
			if (other.allowTest != null)
				return false;
		} else if (!allowTest.equals(other.allowTest))
			return false;
		if (belongSystemId == null) {
			if (other.belongSystemId != null)
				return false;
		} else if (!belongSystemId.equals(other.belongSystemId))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (environmentType == null) {
			if (other.environmentType != null)
				return false;
		} else if (!environmentType.equals(other.environmentType))
			return false;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		if (operationType == null) {
			if (other.operationType != null)
				return false;
		} else if (!operationType.equals(other.operationType))
			return false;
		if (parameterUseCase == null) {
			if (other.parameterUseCase != null)
				return false;
		} else if (!parameterUseCase.equals(other.parameterUseCase))
			return false;
		if (successCode == null) {
			if (other.successCode != null)
				return false;
		} else if (!successCode.equals(other.successCode))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	/**
	 * 高亮title里的搜索关键字
	 * @return
	 */
	public String getSystemName(String id) {
		SystemType sy = new SystemType();
		Map dataMap = sy.dataMap;
		String sysName = (String )dataMap.get(id);

		return sysName;
	}
}
