package cn.conac.as.monitor.constant;

/**
 * 接口是否被测试
 * @author lishen
 * @date	2017年7月13日下午3:16:12
 * @version 1.0
 */
public class AllowTest {

	/**
	 * 接口不被测试
	 */
	public static final String NOT_ALLOW_TEST="0";
	
	/**
	 * 接口被测试
	 */
	public static final String ALLOW_TEST="1";

}
