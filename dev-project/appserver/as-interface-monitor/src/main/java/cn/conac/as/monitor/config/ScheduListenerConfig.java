package cn.conac.as.monitor.config;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import cn.conac.as.monitor.entity.weixin.MyScheduler;

/**
 * Created by liuwei on 2017-7-12.
 */
@Configuration
public class ScheduListenerConfig implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	public MyScheduler myScheduler;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		try {
			myScheduler.scheduleJobs();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

	}

	@Bean
	public SchedulerFactoryBean schedulerFactoryBean() {
		SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
		return schedulerFactoryBean;
	}

}
