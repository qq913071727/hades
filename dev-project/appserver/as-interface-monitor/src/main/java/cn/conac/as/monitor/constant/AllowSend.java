package cn.conac.as.monitor.constant;

/**
 * 是否发送邮件
 * @author lishen
 * @date	2017年7月26日下午4:03:42
 * @version 1.0
 */
public class AllowSend {

	/**
	 * 邮件不被发送
	 */
	public static final String NOT_ALLOW_SEND="0";
	
	/**
	 * 邮件被发送
	 */
	public static final String ALLOW_SEND="1";
}
