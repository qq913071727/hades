package cn.conac.as.monitor.vo;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;

import org.apache.commons.beanutils.PropertyUtils;

import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import cn.conac.as.monitor.entity.BdtVClientMonitorEntity;
import io.swagger.annotations.ApiModel;

/**
 * 客户端监控视图的VO类
 * @author lishen
 * @date	2017年7月25日下午2:50:28
 * @version 1.0
 */
@ApiModel
public class BdtVClientMonitorVo extends BdtVClientMonitorEntity {

	private static final long serialVersionUID = 2343691076058721144L;
	
	/**
	 * 异常错误的开始时间
	 */
	private String beginTime;

	/**异常错误的结束时间
	 * 
	 */
	private String endTime;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public BdtVClientMonitorVo() {
	}
	
	public BdtVClientMonitorVo(String beginTime, String endTime) {
		super();
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	/**
	 * 将BtdVClientMonitorVo对象转换为BtdVClientMonitorEntity对象
	 * @param vo
	 * @return
	 */
	public BdtVClientMonitorEntity toEntity(){
		BdtVClientMonitorEntity bdtVClientMonitorEntity=new BdtVClientMonitorEntity();
		try {
			PropertyUtils.copyProperties(bdtVClientMonitorEntity, this);
			return bdtVClientMonitorEntity;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
}
