package cn.conac.as.monitor.constant;

/**
 * HTTP请求方式类型类
 * @author lishen
 * @date	2017年7月7日下午7:36:01
 * @version 1.0
 */
public class HttpMethodType {

	/**
	 * POST方法
	 */
	public static final String POST="1";
	
	/**
	 * GET方法
	 */
	public static final String GET="2";

}
