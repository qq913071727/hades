package cn.conac.as.monitor.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.service.GenericService;
import cn.conac.as.framework.utils.StringUtils;
import cn.conac.as.framework.utils.TimestampUtils;
import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import cn.conac.as.monitor.repository.BdtClientMonitorRepository;
import cn.conac.as.monitor.util.PropertiesUtil;
import cn.conac.as.monitor.vo.BdtClientMonitorVo;

/**
 * 客户端监控类的服务类
 * @author lishen
 * @date	2017年7月12日上午9:20:17
 * @version 1.0
 */
@Service
public class BdtClientMonitorService extends GenericService<BdtClientMonitorEntity, String> {

	@Autowired
	private BdtClientMonitorRepository bdtClientMonitorRepository;
	
	/**
	 * 判断客户端是否采集数据
	 * @return
	 */
	public boolean isCollect(){
		return Boolean.valueOf(PropertiesUtil.getValue("config/config.properties", "client.whether.collect"));
	}

	/**
	 * 根据id获取某一次客户端监控的详细信息
	 */
	public BdtClientMonitorEntity findById(String id){
		return bdtClientMonitorRepository.findOne(id);
	}
	
	/**
	 * 获取所有的BdtClientMonitoryEntity对象
	 */
	public List<BdtClientMonitorEntity> findAll(){
		return bdtClientMonitorRepository.findAll();
	}
	
	/**
	 * 根据条件查找BdtClientMonitorEntity对象
	 * @return
	 */
	public List<BdtClientMonitorEntity> findClientMonitorByCondition(final BdtClientMonitorVo bdtClientMonitorVo){
		List<BdtClientMonitorEntity> result = bdtClientMonitorRepository.findAll(new Specification<BdtClientMonitorEntity>() {
	        @Override
	        public Predicate toPredicate(Root<BdtClientMonitorEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
	            List<Predicate> list = new ArrayList<Predicate>();

	            // 开始时间和结束时间
	            if (!StringUtils.isEmpty(bdtClientMonitorVo.getBeginTime()) && !StringUtils.isEmpty(bdtClientMonitorVo.getEndTime())) {
	                Timestamp beginTime=TimestampUtils.valueOf(bdtClientMonitorVo.getBeginTime());
	                Timestamp endTime=TimestampUtils.valueOf(bdtClientMonitorVo.getEndTime());
	                
	            	list.add(cb.between(root.get("exceptionErrorTime").as(Timestamp.class), beginTime, endTime));
	            }

	            Predicate[] p = new Predicate[list.size()];
	            return cb.and(list.toArray(p));
	        }

	    });

	    return result;
	}
}
