package cn.conac.as.monitor.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtClientMonitorEntity;

/**
 * 客户端监控类的dao类
 * 
 * @author lishen
 * @date 2017年7月12日上午11:48:22
 * @version 1.0
 */
@Repository
public interface BdtClientMonitorRepository
		extends GenericDao<BdtClientMonitorEntity, String>, JpaSpecificationExecutor<BdtClientMonitorEntity> {

}
