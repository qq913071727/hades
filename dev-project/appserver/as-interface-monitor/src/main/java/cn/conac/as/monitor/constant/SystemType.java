package cn.conac.as.monitor.constant;

import java.util.Map;

import cn.conac.as.framework.utils.FastJsonUtil;

/**
 * Created by liuwei on 2017-7-17.
 */
public class SystemType
{
    public static final String BBS="48470c874bd744dd8d71bae61770b772";

    public static final String BBS_DESCRIPTION="交流系统";

    public static final String LAW="cef4e5913e9344d4b8111203e64cbc43";

    public static final String LAW_DESCRIPTION="法律法规";

    public static final String ZLGX="fff95279197143709c16fa7ae1a88c41";

    public static final String ZLGX_DESCRIPTION="资料共享";

    public static final String OAUTH="77900d23ee524434bf3899589766e303";

    public static final String OAUTH_DESCRIPTION="用户认证相关";

    public static final String MONITOR="76a64784355443f5a940727c89c0ee29";

    public static final String MONITOR_DESCRIPTION="监控系统";

    public static final String SEARCH="c0bf8cd117864f6f908cc4eba4c22951";

    public static final String SEARCH_DESCRIPTION="三定信息-搜索接口";

    public static final String ORG="c28e08505d474862b709fe6fa4b3f390";

    public static final String ORG_DESCRIPTION="三定信息-部门库信息";

    public static final String HISTORY="6a23ab4f78a44aff8de9712d21ed35d0";

    public static final String HISTORY_DESCRIPTION="三定信息-历史沿革的列表";

    public static final String BUTY="880d2ba1efef47bf92c6c3a8abf74834";

    public static final String BUTY_DESCRIPTION="三定分析-权责清单信息";
    public static final String jsons = "{\""+BBS+"\":\""+BBS_DESCRIPTION+"\",\""+LAW+"\":\""+LAW_DESCRIPTION+"\",\""+ZLGX+"\":\""+ZLGX_DESCRIPTION+"\""
            + ",\""+OAUTH+"\":\""+OAUTH_DESCRIPTION+"\",\""+MONITOR+"\":\""+MONITOR_DESCRIPTION+"\""
            + ",\""+SEARCH+"\":\""+SEARCH_DESCRIPTION+"\",\""+ORG+"\":\""+ORG_DESCRIPTION+"\""
            + ",\""+HISTORY+"\":\""+HISTORY_DESCRIPTION+"\",\""+BUTY+"\":\""+BUTY_DESCRIPTION+"\"}";
    public static Map<?, ?> dataMap = FastJsonUtil.jsonToMap(jsons);
}
