package cn.conac.as.monitor.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtInterfaceEntity;

/**
 * 接口的DAO类
 * @author lishen
 * @date	2017年7月7日下午7:17:27
 * @version 1.0
 */
@Repository
public interface BdtInterfaceRepository extends GenericDao<BdtInterfaceEntity, String> {

	/**
	 * 根据环境类型和是否允许测试查找BdtInterfaceEntity对象列表
	 * @param environmentType
	 * @param allowTest
	 * @return
	 */
	List<BdtInterfaceEntity> findByEnvironmentTypeAndAllowTest(String environmentType, String allowTest);
}
