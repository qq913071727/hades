package cn.conac.as.monitor.service;

import cn.conac.as.monitor.entity.BdtVSystemInterfaceMonitorEntity;

/**
 * Created by liuwei on 2017-7-13.
 */
public interface EmailService
{

    /**
     * 发送模板邮件
     * @param sendTo
     * @param BdtVSystemInterfaceMonitorEntity
     */
    public void sendTemplateMail(String sendTo, BdtVSystemInterfaceMonitorEntity bdtVSystemInterfaceMonitorEntity);
}
