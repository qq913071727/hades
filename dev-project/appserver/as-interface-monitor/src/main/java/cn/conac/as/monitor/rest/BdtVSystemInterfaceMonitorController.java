package cn.conac.as.monitor.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import cn.conac.as.framework.vo.ResultPojo;
import cn.conac.as.monitor.constant.ScanResultType;
import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import cn.conac.as.monitor.entity.BdtVSystemInterfaceMonitorEntity;
import cn.conac.as.monitor.service.BdtClientMonitorService;
import cn.conac.as.monitor.service.BdtVSystemInterfaceMonitorService;
import cn.conac.as.monitor.vo.BdtClientMonitorVo;
import cn.conac.as.monitor.vo.BdtVSystemInterfaceMonitorVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 系统接口监控视图的controller类
 * @author lishen
 * @date	2017年7月13日上午8:47:26
 * @version 1.0
 */
@RestController
//@RequestMapping(value="bdtVSystemInterfaceMonitor/")
public class BdtVSystemInterfaceMonitorController {

	@Autowired
	BdtVSystemInterfaceMonitorService bdtVSystemInterfaceMonitorService;
	
	/**
	 * 接口：根据id获取某一次接口监控的详细信息
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "根据id获取某一次接口监控的详细信息", httpMethod = "GET", response = BdtVSystemInterfaceMonitorEntity.class, notes = "根据id获取某一次接口监控的详细信息")
	@RequestMapping(value = "/api/client/get/interfaceerror/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// service调用
		BdtVSystemInterfaceMonitorEntity bdtVSystemInterfaceMonitorEntity = bdtVSystemInterfaceMonitorService.findById(id);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		result.setResult(bdtVSystemInterfaceMonitorEntity);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	/**
	 * 查找BdtVSystemInterfaceMonitorEntity对象，只包括那些性能有问题和不可用的，并且按照endTime降序排列
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value ="/bdtlaw/monitor/findBdtVSystemInterfaceMonitorEntityByPerformanceAndUsability", method = RequestMethod.POST)
    public List<BdtVSystemInterfaceMonitorEntity> findBdtVSystemInterfaceMonitorEntityByPerformanceAndUsability(HttpServletRequest request, HttpServletResponse response){
       
		List<BdtVSystemInterfaceMonitorEntity> bdtVSystemInterfaceMonitorEntityList = bdtVSystemInterfaceMonitorService.findBdtSystemInterfaceMonitorEntityByPerformanceAndUsability();
        if(null!=bdtVSystemInterfaceMonitorEntityList && bdtVSystemInterfaceMonitorEntityList.size()!=0){
        	return bdtVSystemInterfaceMonitorEntityList;
        }
        return null;
    }
	
	/**
	 * 根据条件查找BdtVSystemInterfaceMonitorEntity对象，并且按照endTime降序排列
	 * @param request
	 * @param response
	 * @param scanTime
	 * @param endTime
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value ="/bdtlaw/monitor/findVSystemInterfaceMonitorByCondition", method = RequestMethod.POST)
    public List<BdtVSystemInterfaceMonitorEntity> findVSystemInterfaceMonitorByCondition(HttpServletRequest request, HttpServletResponse response, 
    		@ModelAttribute BdtVSystemInterfaceMonitorVo bdtVSystemInterfaceMonitorVo){
		
		List<BdtVSystemInterfaceMonitorEntity> bdtVSystemInterfaceMonitorEntityList=bdtVSystemInterfaceMonitorService.findVSystemInterfaceMonitorByCondition(bdtVSystemInterfaceMonitorVo);

		return bdtVSystemInterfaceMonitorEntityList;
    }
	
	/**
	 * 根据interfaceId查找BdtVSystemInterfaceMonitorEntity对象列表
	 * @param requestfindVSystemInterfaceMonitorByCondition
	 * @param response
	 * @param scanTime
	 * @param endTime
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value ="/bdtlaw/monitor/findVSystemInterfaceMonitorByInterfaceId", method = RequestMethod.POST)
	public List<BdtVSystemInterfaceMonitorEntity> findVSystemInterfaceMonitorByInterfaceId(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute BdtVSystemInterfaceMonitorVo bdtVSystemInterfaceMonitorVo) {

		List<BdtVSystemInterfaceMonitorEntity> bdtVSystemInterfaceMonitorEntityList = bdtVSystemInterfaceMonitorService
				.findByInterfaceIdAndScanTimeAndEndTimeOrderByEndTimeAsc(bdtVSystemInterfaceMonitorVo.getInterfaceId(),
						bdtVSystemInterfaceMonitorVo.getBeginTime(), bdtVSystemInterfaceMonitorVo.getFinishTime());

		if (null != bdtVSystemInterfaceMonitorEntityList && bdtVSystemInterfaceMonitorEntityList.size() != 0) {
			return bdtVSystemInterfaceMonitorEntityList;
		}
		return null;
	}
	
	/***************************************** 下面是页面的跳转方法 *********************************************/
	/**
	 * 跳转到接口监控页
	 * @return
	 */
	@RequestMapping(value = "/bdtlaw/monitor/toVSystemInterfaceMonitor",method = RequestMethod.GET)
	public ModelAndView toSystemInterfaceMonitor() {
		ModelAndView mv = new ModelAndView("v_system_interface_monitor/v_system_interface_monitor");
		return mv;
    }
}
