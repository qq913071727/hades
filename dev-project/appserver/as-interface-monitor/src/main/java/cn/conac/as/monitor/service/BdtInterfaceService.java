package cn.conac.as.monitor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.service.GenericService;
import cn.conac.as.monitor.constant.AllowTest;
import cn.conac.as.monitor.constant.EnvironmentType;
import cn.conac.as.monitor.entity.BdtInterfaceEntity;
import cn.conac.as.monitor.repository.BdtInterfaceRepository;

/**
 * 接口服务类
 * @author lishen
 * @date	2017年7月7日下午7:25:36
 * @version 1.0
 */
@Service
public class BdtInterfaceService extends GenericService<BdtInterfaceEntity, String>{

	 @Autowired
	 private BdtInterfaceRepository iRepository;
	 
	 /**
	  * 获取BdtInterfaceEntity类型对象列表
	  */
	 public List<BdtInterfaceEntity> findAll(){
		 return iRepository.findAll();
	 }
	 
	 /**
	  * 根据环境类型和是否允许测试查找BdtInterfaceEntity对象列表
	  * @param environmentType
	  * @param allowTest
	  * @return
	  */
	public List<BdtInterfaceEntity> findByEnvironmentTypeAndAllowTest(String environmentType, String allowTest){
		return iRepository.findByEnvironmentTypeAndAllowTest(environmentType, allowTest);
	}
}
