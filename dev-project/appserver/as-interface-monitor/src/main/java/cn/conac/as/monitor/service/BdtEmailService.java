package cn.conac.as.monitor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.service.GenericService;
import cn.conac.as.monitor.entity.BdtEmailEntity;
import cn.conac.as.monitor.repository.BdtEmailRepository;

/**
 * 邮箱的服务类
 * @author lishen
 * @date	2017年7月26日下午3:48:49
 * @version 1.0
 */
@Service
public class BdtEmailService extends GenericService<BdtEmailEntity, String> {

	@Autowired
	private BdtEmailRepository bdtEmailRepository;
	
	/**
	 * 查找所有的BdtEmailEntity对象
	 */
	public List<BdtEmailEntity> findAll(){
		return bdtEmailRepository.findAll();
	}
}
