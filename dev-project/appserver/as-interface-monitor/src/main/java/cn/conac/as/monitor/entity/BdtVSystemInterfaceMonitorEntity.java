package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 系统接口监控视图类
 * @author lishen
 * @date	2017年7月12日下午8:20:37
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_V_SYSTEM_INTERFACE_MONITOR")
public class BdtVSystemInterfaceMonitorEntity extends BaseEntity<BdtVSystemInterfaceMonitorEntity> implements Serializable {

	private static final long serialVersionUID = -4817881689322745106L;
	
	@ApiModelProperty("扫描时间")
	private Timestamp scanTime;
	
	@ApiModelProperty("结束时间")
	private Timestamp endTime;
	
	@ApiModelProperty("响应时间")
	private String responseTime;
	
	@ApiModelProperty("错误信息")
	private String errorInfo;
	
	@ApiModelProperty("扫描结果")
	private String scanResult;
	
	@ApiModelProperty("接口id")
	private String interfaceId;
	
	@ApiModelProperty("url")
	private String url;
	
	@ApiModelProperty("HTTP请求方式")
	private String method;
	
	@ApiModelProperty("描述")
	private String description;
	
	@ApiModelProperty("参数以及用例说明")
	private String parameterUseCase;
	
	@ApiModelProperty("环境类型")
	private String environmentType;
	
	@ApiModelProperty("操作类型")
	private String operationType;
	
	@ApiModelProperty("接口是否被测试")
	private String allowTest;
	
	@ApiModelProperty("系统id")
	private String systemId;
	
	@ApiModelProperty("系统名称")
	private String systemName;

	public BdtVSystemInterfaceMonitorEntity() {
		super();
	}

	public BdtVSystemInterfaceMonitorEntity(Timestamp scanTime, Timestamp endTime, String responseTime,
			String errorInfo, String scanResult, String interfaceId, String url, String method, String description,
			String parameterUseCase, String environmentType, String operationType, String allowTest, String systemId,
			String systemName) {
		super();
		this.scanTime = scanTime;
		this.endTime = endTime;
		this.responseTime = responseTime;
		this.errorInfo = errorInfo;
		this.scanResult = scanResult;
		this.interfaceId = interfaceId;
		this.url = url;
		this.method = method;
		this.description = description;
		this.parameterUseCase = parameterUseCase;
		this.environmentType = environmentType;
		this.operationType = operationType;
		this.allowTest = allowTest;
		this.systemId = systemId;
		this.systemName = systemName;
	}

	public Timestamp getScanTime() {
		return scanTime;
	}

	public void setScanTime(Timestamp scanTime) {
		this.scanTime = scanTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getScanResult() {
		return scanResult;
	}

	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}

	public String getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParameterUseCase() {
		return parameterUseCase;
	}

	public void setParameterUseCase(String parameterUseCase) {
		this.parameterUseCase = parameterUseCase;
	}

	public String getEnvironmentType() {
		return environmentType;
	}

	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getAllowTest() {
		return allowTest;
	}

	public void setAllowTest(String allowTest) {
		this.allowTest = allowTest;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allowTest == null) ? 0 : allowTest.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((environmentType == null) ? 0 : environmentType.hashCode());
		result = prime * result + ((errorInfo == null) ? 0 : errorInfo.hashCode());
		result = prime * result + ((interfaceId == null) ? 0 : interfaceId.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((operationType == null) ? 0 : operationType.hashCode());
		result = prime * result + ((parameterUseCase == null) ? 0 : parameterUseCase.hashCode());
		result = prime * result + ((responseTime == null) ? 0 : responseTime.hashCode());
		result = prime * result + ((scanResult == null) ? 0 : scanResult.hashCode());
		result = prime * result + ((scanTime == null) ? 0 : scanTime.hashCode());
		result = prime * result + ((systemId == null) ? 0 : systemId.hashCode());
		result = prime * result + ((systemName == null) ? 0 : systemName.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdtVSystemInterfaceMonitorEntity other = (BdtVSystemInterfaceMonitorEntity) obj;
		if (allowTest == null) {
			if (other.allowTest != null)
				return false;
		} else if (!allowTest.equals(other.allowTest))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (environmentType == null) {
			if (other.environmentType != null)
				return false;
		} else if (!environmentType.equals(other.environmentType))
			return false;
		if (errorInfo == null) {
			if (other.errorInfo != null)
				return false;
		} else if (!errorInfo.equals(other.errorInfo))
			return false;
		if (interfaceId == null) {
			if (other.interfaceId != null)
				return false;
		} else if (!interfaceId.equals(other.interfaceId))
			return false;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		if (operationType == null) {
			if (other.operationType != null)
				return false;
		} else if (!operationType.equals(other.operationType))
			return false;
		if (parameterUseCase == null) {
			if (other.parameterUseCase != null)
				return false;
		} else if (!parameterUseCase.equals(other.parameterUseCase))
			return false;
		if (responseTime == null) {
			if (other.responseTime != null)
				return false;
		} else if (!responseTime.equals(other.responseTime))
			return false;
		if (scanResult == null) {
			if (other.scanResult != null)
				return false;
		} else if (!scanResult.equals(other.scanResult))
			return false;
		if (scanTime == null) {
			if (other.scanTime != null)
				return false;
		} else if (!scanTime.equals(other.scanTime))
			return false;
		if (systemId == null) {
			if (other.systemId != null)
				return false;
		} else if (!systemId.equals(other.systemId))
			return false;
		if (systemName == null) {
			if (other.systemName != null)
				return false;
		} else if (!systemName.equals(other.systemName))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	
}
