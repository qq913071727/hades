package cn.conac.as.monitor.entity.weixin;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

/**
 * Created by liuwei on 2017-7-12.
 */
@Component
public class MyScheduler
{
    @Autowired
    SchedulerFactoryBean schedulerFactoryBean;

    public void scheduleJobs() throws SchedulerException
    {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        startJob1(scheduler);
    }

    private void startJob1(Scheduler scheduler) throws SchedulerException{
        JobDataMap jobDataMap = new JobDataMap();
//        Map<String,Object> jobDataMap = new HashedMap();
        RefreshAccessTokenTask refreshAccessTokenTask = new RefreshAccessTokenTask();

        jobDataMap.put("refreshAccessTokenTask",refreshAccessTokenTask);
        JobDetail jobDetail = JobBuilder.newJob(WeixinQuartzJob.class).setJobData(jobDataMap)
                .withIdentity("job1", "group1").build();
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0 0 */1 * * ?");
//        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0 0/1 * * * ?");
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1")
                .withSchedule(scheduleBuilder).build();
        scheduler.scheduleJob(jobDetail,cronTrigger);
    }
}
