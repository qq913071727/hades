package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.BaseEntity;
import cn.conac.as.framework.utils.DateUtils;
import cn.conac.as.monitor.constant.Carrieroperator;
import cn.conac.as.monitor.constant.ConnectNetworkType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 客户端监控试图类
 * @author lishen
 * @date	2017年7月25日下午2:33:15
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name = "BDT_V_CLIENT_MONITOR")
public class BdtVClientMonitorEntity extends BaseEntity<BdtVClientMonitorEntity> implements Serializable {

	private static final long serialVersionUID = 8040975601139596819L;

	@ApiModelProperty("设备号")
	private String deviceNumber;
	
	@ApiModelProperty("手机型号")
	private String mobilePhoneType;
	
	@ApiModelProperty("操作系统版本")
	private String operationSystemVersion;
	
	@ApiModelProperty("操作系统类型")
	private String operationSystemType;
	
	@ApiModelProperty("运营商")
	private String carrieroperator;
	
	@ApiModelProperty("客户端版本")
	private String clientVersion;
	
	@ApiModelProperty("联网方式")
	private String connectNetworkType;
	
	@ApiModelProperty("地域")
	private String userRegion;
	
	@ApiModelProperty("异常/错误时间")
	private Timestamp exceptionErrorTime;
	
	@ApiModelProperty("异常/错误信息")
	private String exceptionErrorInfo;
	
	@ApiModelProperty("异常/错误级别")
	private String exceptionErrorLevel;
	
	@ApiModelProperty("令牌")
	private String token;
	
	@ApiModelProperty("登录类型")
	private String loginType;
	
	@ApiModelProperty("用户名")
	private String username;
	
	@ApiModelProperty("客户端id")
	private String clientId;

	public BdtVClientMonitorEntity() {
		super();
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getMobilePhoneType() {
		return mobilePhoneType;
	}

	public void setMobilePhoneType(String mobilePhoneType) {
		this.mobilePhoneType = mobilePhoneType;
	}

	public String getOperationSystemVersion() {
		return operationSystemVersion;
	}

	public void setOperationSystemVersion(String operationSystemVersion) {
		this.operationSystemVersion = operationSystemVersion;
	}

	public String getOperationSystemType() {
		return operationSystemType;
	}

	public void setOperationSystemType(String operationSystemType) {
		this.operationSystemType = operationSystemType;
	}

	public String getCarrieroperator() {
		return carrieroperator;
	}

	public void setCarrieroperator(String carrieroperator) {
		this.carrieroperator = carrieroperator;
	}

	public String getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}

	public String getConnectNetworkType() {
		return connectNetworkType;
	}

	public void setConnectNetworkType(String connectNetworkType) {
		this.connectNetworkType = connectNetworkType;
	}

	public String getUserRegion() {
		return userRegion;
	}

	public void setUserRegion(String userRegion) {
		this.userRegion = userRegion;
	}

	public Timestamp getExceptionErrorTime() {
		return exceptionErrorTime;
	}

	public void setExceptionErrorTime(Timestamp exceptionErrorTime) {
		this.exceptionErrorTime = exceptionErrorTime;
	}

	public String getExceptionErrorInfo() {
		return exceptionErrorInfo;
	}

	public void setExceptionErrorInfo(String exceptionErrorInfo) {
		this.exceptionErrorInfo = exceptionErrorInfo;
	}

	public String getExceptionErrorLevel() {
		return exceptionErrorLevel;
	}

	public void setExceptionErrorLevel(String exceptionErrorLevel) {
		this.exceptionErrorLevel = exceptionErrorLevel;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carrieroperator == null) ? 0 : carrieroperator.hashCode());
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((clientVersion == null) ? 0 : clientVersion.hashCode());
		result = prime * result + ((connectNetworkType == null) ? 0 : connectNetworkType.hashCode());
		result = prime * result + ((deviceNumber == null) ? 0 : deviceNumber.hashCode());
		result = prime * result + ((exceptionErrorInfo == null) ? 0 : exceptionErrorInfo.hashCode());
		result = prime * result + ((exceptionErrorLevel == null) ? 0 : exceptionErrorLevel.hashCode());
		result = prime * result + ((exceptionErrorTime == null) ? 0 : exceptionErrorTime.hashCode());
		result = prime * result + ((loginType == null) ? 0 : loginType.hashCode());
		result = prime * result + ((mobilePhoneType == null) ? 0 : mobilePhoneType.hashCode());
		result = prime * result + ((operationSystemType == null) ? 0 : operationSystemType.hashCode());
		result = prime * result + ((operationSystemVersion == null) ? 0 : operationSystemVersion.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((userRegion == null) ? 0 : userRegion.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BdtVClientMonitorEntity other = (BdtVClientMonitorEntity) obj;
		if (carrieroperator == null) {
			if (other.carrieroperator != null)
				return false;
		} else if (!carrieroperator.equals(other.carrieroperator))
			return false;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (clientVersion == null) {
			if (other.clientVersion != null)
				return false;
		} else if (!clientVersion.equals(other.clientVersion))
			return false;
		if (connectNetworkType == null) {
			if (other.connectNetworkType != null)
				return false;
		} else if (!connectNetworkType.equals(other.connectNetworkType))
			return false;
		if (deviceNumber == null) {
			if (other.deviceNumber != null)
				return false;
		} else if (!deviceNumber.equals(other.deviceNumber))
			return false;
		if (exceptionErrorInfo == null) {
			if (other.exceptionErrorInfo != null)
				return false;
		} else if (!exceptionErrorInfo.equals(other.exceptionErrorInfo))
			return false;
		if (exceptionErrorLevel == null) {
			if (other.exceptionErrorLevel != null)
				return false;
		} else if (!exceptionErrorLevel.equals(other.exceptionErrorLevel))
			return false;
		if (exceptionErrorTime == null) {
			if (other.exceptionErrorTime != null)
				return false;
		} else if (!exceptionErrorTime.equals(other.exceptionErrorTime))
			return false;
		if (loginType == null) {
			if (other.loginType != null)
				return false;
		} else if (!loginType.equals(other.loginType))
			return false;
		if (mobilePhoneType == null) {
			if (other.mobilePhoneType != null)
				return false;
		} else if (!mobilePhoneType.equals(other.mobilePhoneType))
			return false;
		if (operationSystemType == null) {
			if (other.operationSystemType != null)
				return false;
		} else if (!operationSystemType.equals(other.operationSystemType))
			return false;
		if (operationSystemVersion == null) {
			if (other.operationSystemVersion != null)
				return false;
		} else if (!operationSystemVersion.equals(other.operationSystemVersion))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (userRegion == null) {
			if (other.userRegion != null)
				return false;
		} else if (!userRegion.equals(other.userRegion))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	/**
	 * 用中文来描述运营商等字段
	 * @return
	 */
	public BdtVClientMonitorEntity toDescription(){
		// 运营商
		if(this.getCarrieroperator().equals(Carrieroperator.OTHER)){
			this.setCarrieroperator(Carrieroperator.OTHER_DESCRIPTION);
		}
		if(this.getCarrieroperator().equals(Carrieroperator.CHINA_MOBILE)){
			this.setCarrieroperator(Carrieroperator.CHINA_MOBILE_DESCRIPTION);
		}
		if(this.getCarrieroperator().equals(Carrieroperator.CHINA_UNICOM)){
			this.setCarrieroperator(Carrieroperator.CHINA_UNICOM_DESCRIPTION);
		}
		if(this.getCarrieroperator().equals(Carrieroperator.CHINA_TELECOM)){
			this.setCarrieroperator(Carrieroperator.CHINA_TELECOM_DESCRIPTION);
		}
		
		// 联网方式
		if(this.getConnectNetworkType().equals(ConnectNetworkType.OTHER)){
			this.setConnectNetworkType(ConnectNetworkType.OTHER_DESCRIPTION);
		}
		if(this.getConnectNetworkType().equals(ConnectNetworkType.WIFI)){
			this.setConnectNetworkType(ConnectNetworkType.WIFI_DESCRIPTION);
		}
		if(this.getConnectNetworkType().equals(ConnectNetworkType.TWO_G)){
			this.setConnectNetworkType(ConnectNetworkType.TWO_G_DESCRIPTION);
		}
		if(this.getConnectNetworkType().equals(ConnectNetworkType.THREE_G)){
			this.setConnectNetworkType(ConnectNetworkType.THREE_G_DESCRIPTION);
		}
		if(this.getConnectNetworkType().equals(ConnectNetworkType.FOUR_G)){
			this.setConnectNetworkType(ConnectNetworkType.FOUR_G_DESCRIPTION);
		}
		
		// 异常/错误时间
		DateUtils.formatDateTime(this.getExceptionErrorTime().getTime());
		return this;
	}
}
