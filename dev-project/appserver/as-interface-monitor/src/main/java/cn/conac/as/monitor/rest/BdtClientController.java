package cn.conac.as.monitor.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import cn.conac.as.framework.utils.Encodes;
import cn.conac.as.framework.utils.IdGen;
import cn.conac.as.framework.vo.ResultPojo;
import cn.conac.as.monitor.constant.ScanResultType;
import cn.conac.as.monitor.entity.BdtClientEntity;
import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import cn.conac.as.monitor.service.BdtClientService;
import cn.conac.as.monitor.vo.BdtClientMonitorVo;
import cn.conac.as.monitor.vo.BdtClientVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 客户端控制器类
 * @author lishen
 * @date	2017年7月25日下午3:12:45
 * @version 1.0
 */
@RestController
public class BdtClientController {

	@Autowired
	BdtClientService bdtClientService;

	/**
	 * 手机客户端基础信息
	 * @param request
	 * @param response
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "收集客户端基础信息", httpMethod = "POST", response = BdtClientEntity.class, notes = "收集客户端基础信息")
	@RequestMapping(value = "/api/get/client/basic", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> addClientBasic(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "客户端基础信息对象", required = true) @RequestBody BdtClientVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();

		// 根据设备号查询客户端基本信息，如果返回空则说明没有这个客户端的基本信息，就将这个客户端的基本信息插入数据库；
		// 当返回不为空时，则修改这个客户端基本信息（通常是友盟编号）
		BdtClientEntity bdtClientEntity=vo.toEntity();
		BdtClientEntity bdtClientEntityTemp=bdtClientService.findByDeviceNumber(bdtClientEntity.getDeviceNumber());
		if(null==bdtClientEntityTemp){
			bdtClientEntity.setId(IdGen.uuid());
		}else{
			bdtClientEntity.setId(bdtClientEntityTemp.getId());
		}
		bdtClientService.save(bdtClientEntity);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(JSON.toJSONString(bdtClientEntity));
		result.setMsg(ResultPojo.MSG_SUCCESS);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
