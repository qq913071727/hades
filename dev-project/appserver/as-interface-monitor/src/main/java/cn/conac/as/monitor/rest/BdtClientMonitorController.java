package cn.conac.as.monitor.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import cn.conac.as.framework.utils.Encodes;
import cn.conac.as.framework.utils.IdGen;
import cn.conac.as.framework.vo.ResultPojo;
import cn.conac.as.monitor.constant.ScanResultType;
import cn.conac.as.monitor.entity.BdtClientMonitorEntity;
import cn.conac.as.monitor.service.BdtClientMonitorService;
import cn.conac.as.monitor.util.HttpClientUtil;
import cn.conac.as.monitor.vo.BdtClientMonitorVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 客户端监控的controller类
 * @author lishen
 * @date	2017年7月12日上午9:19:05
 * @version 1.0
 */
@RestController
//@RequestMapping(value="api/")
public class BdtClientMonitorController {
	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(BdtClientMonitorController.class);
	
	@Autowired
	BdtClientMonitorService bdtClientMonitorService;

	/**
	 * 接口：判断客户端是否采集数据
	 * @param request
	 * @param response
	 * @param clsCode1
	 * @param clsCode2
	 * @return
	 */
	@ApiOperation(value = "判断客户端是否采集数据", httpMethod = "GET", response = BdtClientMonitorEntity.class, notes = "判断客户端是否采集数据")
	@RequestMapping(value = "/api/client/whether/collect", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> isCollect(HttpServletRequest request, HttpServletResponse response) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// service调用
		boolean collectFlag = bdtClientMonitorService.isCollect();
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		if(collectFlag){
			result.setResult(ScanResultType.INTERFACE_SUCCESS);
		}else{
			result.setResult(ScanResultType.INTERFACE_FAIL);
		}
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	/**
	 * 接口：收集客户端异常信息
	 * @param request
	 * @param response
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "收集客户端异常信息", httpMethod = "POST", response = BdtClientMonitorEntity.class, notes = "收集客户端异常信息")
	@RequestMapping(value = "/api/client/add/exception", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> addClientException(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "客户端监控异常信息对象", required = true) @RequestBody BdtClientMonitorVo vo) throws Exception {
		// 声明返回结果集
		logger.info("client/add/exception");
		ResultPojo result = new ResultPojo();

		// service调用
		BdtClientMonitorEntity bdtClientMonitorEntity=vo.toEntity();
		bdtClientMonitorEntity.setId(IdGen.uuid());
		
		if(null!=bdtClientMonitorEntity.getUserRegion()){
			String userRegion=Encodes.urlDecode(bdtClientMonitorEntity.getUserRegion());
			bdtClientMonitorEntity.setUserRegion(userRegion);
		}
		
		bdtClientMonitorService.save(bdtClientMonitorEntity);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(ScanResultType.INTERFACE_SUCCESS);
		result.setMsg(ResultPojo.MSG_SUCCESS);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	/***************************************** 下面是页面的跳转方法 *********************************************/
	/**
	 * 跳转到客户端监控页
	 * @return
	 */
	@RequestMapping(value = "/bdtlaw/monitor/toClientMonitor",method = RequestMethod.GET)
	public ModelAndView toClientMonitor() {
		ModelAndView mv = new ModelAndView("client_monitor/client_monitor");
		return mv;
    }

}
