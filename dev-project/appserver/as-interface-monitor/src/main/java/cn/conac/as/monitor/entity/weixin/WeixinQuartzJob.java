package cn.conac.as.monitor.entity.weixin;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Created by liuwei on 2017-7-12.
 */
public class WeixinQuartzJob extends QuartzJobBean
{
    @Autowired
    private RefreshAccessTokenTask refreshAccessTokenTask;

    public void setRefreshAccessTokenTask(
            RefreshAccessTokenTask refreshAccessTokenTask) {
        this.refreshAccessTokenTask = refreshAccessTokenTask;
    }
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        refreshAccessTokenTask.refreshToken();
        System.out.println("sdilllllllllllllllllllllllll");
    }
}
