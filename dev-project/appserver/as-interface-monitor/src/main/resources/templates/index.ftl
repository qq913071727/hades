<#include "./common/layout.ftl">
<@html page_title="首页">


<div class="col-xs-12">
    <div class="panel-body paginate-bot">

        <div><h4 style="color: #1abc9c;text-align: center;">接口列表</h4></div>
        <table class="table table-hover table-bordered">
            <thead>

            <tr>
                <th style="text-align: center;width: 40%;">接口名称</th>
                <th style="text-align: center;width: 40%;">描述</th>
                <th style="text-align: center;width: 20%;">所属系统</th>
            </tr>
            </thead>
            <tbody>
              <#list interfaceList as intf>

              <tr>
                  <td>${intf.url!}</td>
                  <td>${intf.description!}</td>
                  <td style="text-align: center;width: 20%;">${ intf.getSystemName(intf.belongSystemId)!}</td>
              </tr>
              </#list>
            </tbody>
        </table>

    </div>


</div>
</@html>