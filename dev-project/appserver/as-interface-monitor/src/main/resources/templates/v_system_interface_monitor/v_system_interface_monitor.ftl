<#include "../common/layout.ftl">
<@html page_title="接口监控">
      
<head>

	<script type="text/javascript" src="/bdtlaw/monitor/static/js/v_system_interface_monitor/v_system_interface_monitor.js"></script>
</head>
      
	<body>
	    <div class="container">
	    	<!-- 搜索 -->
	    	<div >
				<label style="float: left; width: 130px;text-align: right; padding: 4px;margin: 1px;">URL：</label><input id="url" size="40" type="text" value="">
                <label style="width: 130px;text-align: right; padding: 4px;margin: 1px;">描述：</label><input id="description" size="40" type="text" value=""></div>
            <div>
                <label style="float: left; width: 130px;text-align: right; padding: 4px;margin: 1px;">开始时间：</label><input id="beginTime" size="40" type="text" value="" readonly class="form_datetime">
                <label style=" width: 130px;text-align: right; padding: 4px;margin: 1px;">结束时间：</label><input id="finishTime" size="40" type="text" value="" readonly class="form_datetime">
				</div>
			<div align="center" style="margin: 15px;">
				<button id="findVSystemInterfaceMonitorByCondition" type="button" class="btn btn-default">确定</button>
				<button id="reset" type="button" class="btn btn-default">重置</button>
			</div>
	    	<!-- 表格 -->
	        <table id="vSystemInterfaceMonitorTable">
	        </table>
	    </div>
	    
	    <!-- 错误的详细信息 -->
		<div class="modal fade fade bs-example-modal-lg" id="errorInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   <div class="modal-dialog modal-lg">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
		                  &times;
		            </button>
		            <!--  模态框（Modal）标题 -->
		            <h4 class="modal-title" id="modalTitle"></h4>
		         </div>
		         <div id="errorInfo" class="modal-body" style="word-break:break-all">
		         	<span></span>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-default" data-dismiss="modal">
		            	关闭
		            </button>
		         </div>
		      </div>
		</div>
		
		<div id="dialog" title="警告">
			<p>开始时间和结束时间或者都为空，或者都不为空！</p>
		</div>
	</body>
<script type="application/javascript">
    $('.navbar-nav > li').removeClass('active');
    $('#interfaceMonitor').addClass('active');


</script>
</@html>
