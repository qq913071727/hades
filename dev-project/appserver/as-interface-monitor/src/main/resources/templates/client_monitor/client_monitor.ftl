<#include "../common/layout.ftl">
<@html page_title="客户端监控">
	
	<head>
		<script type="text/javascript" src="/bdtlaw/monitor/static/js/client_monitor/client_monitor.js"></script>
	</head>
	<body>
		<div class="container">
		    <!-- 搜索 -->			
			开始时间：<input id="beginTime" size="25" type="text" value="" readonly class="form_datetime">
			结束时间：<input id="endTime" size="25" type="text" value="" readonly class="form_datetime">
			<button id="findVClientMonitorByCondition" type="button" class="btn btn-default">确定</button>
			<button id="reset" type="button" class="btn btn-default">重置</button>
	    
	    	<!-- 表格 -->
	        <table id="vClientMonitorTable">
	        </table>
	    </div>
		
		<!-- 异常/错误的详细信息和历史信息 -->
		<div class="modal fade" id="exceptionErrorInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   <div class="modal-dialog">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
		                  &times;
		            </button>
		            <!--  模态框（Modal）标题 -->
		            <h4 class="modal-title" id="modalTitle"></h4>
		         </div>
		         <div id="exceptionErrorInfo" class="modal-body" style="word-break:break-all">
		         	<span></span>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-default" data-dismiss="modal" data-reveal-id="myModal">
		            	关闭
		            </button>
		         </div>
		      </div>
		</div>

		<div id="dialog" title="警告">
			<p>开始时间和结束时间或者都为空，或者都不为空！</p>
		</div>
	</body>
<script type="application/javascript">
    $('.navbar-nav > li').removeClass('active');
    $('#clientMonitor').addClass('active');


</script>
</@html>
