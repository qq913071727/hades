<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
	<div style="width: 600px; text-align: left; margin: 0 auto;">
		<h1 style="color: #005da7;">接口监控警告</h1>
		<div style="border-bottom: 5px solid #005da7; height: 2px; width: 100%;"></div>
		<div style="border: 1px solid #005da7; font-size: 16px; line-height: 50px; padding: 20px; word-break: break-all">
			<div>${email!}，您好！</div>
			<div>
				扫描时间：${vSystemInterfaceMonitor.scanTime!}<br />
				结束时间：${vSystemInterfaceMonitor.endTime!}<br />
				响应时间：${vSystemInterfaceMonitor.responseTime!}<br />
				扫描结果：${vSystemInterfaceMonitor.scanResult!}<br />
				url：${vSystemInterfaceMonitor.url!}<br />
				参数：${vSystemInterfaceMonitor.parameterUseCase!}<br />
				HTTP请求方式：${vSystemInterfaceMonitor.method!}<br />
				描述：${vSystemInterfaceMonitor.description!}<br />
				环境类型：${vSystemInterfaceMonitor.environmentType!}<br />
				系统名称：${vSystemInterfaceMonitor.systemName!}<br />
				错误信息：${vSystemInterfaceMonitor.errorInfo!}<br />
			</div>
			
			<div style="border-bottom: 2px solid #005da7; height: 2px; width: 100%;"></div>

			<div>
				想了解更多信息，请访问 <a href="">http://jgbzy.conac.cn</a>
			</div>
		</div>
	</div>
</body>
</html>
