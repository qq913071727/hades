<#macro footer>
<div class="container">
  <br>
  <div class="text-center">
    &copy;2017 Powered by <a href="//www.conac.cn" target="_blank">政务和公益机构域名注册管理中心</a>
    <a href="http://www.miitbeian.gov.cn/">${beianName!}</a>

  </div>

</div>
</#macro>