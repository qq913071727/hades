<#macro header page_tab="">
<div class="container" style="margin-top: 10px;">
    <div class="row"></div>
    <div class="col-xs-12">
        <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                    <span class="sr-only">Toggle navigation</span>
                </button>
                <a class="navbar-brand" href="/bdtlaw/monitor/interfacelist">CONAC监控</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-01">
                <ul class="nav navbar-nav navbar-left" id="mainMenu">
                    <li  class="menu-item" id="interfacelist">
                        <a href="/bdtlaw/monitor/interfacelist">接口列表</a>
                    </li>
                    <li class="menu-item" id="clientMonitor">
                    	<a href="/bdtlaw/monitor/toClientMonitor">客户端监控 </a>
                    </li>
                    <li class="menu-item" id="interfaceMonitor">
                    	<a href="/bdtlaw/monitor/toVSystemInterfaceMonitor" >接口监控 <#--<b class="caret"></b>--></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
</div>
</#macro>