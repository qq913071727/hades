<#macro html page_title page_tab="">
<!doctype html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="renderer" content="webkit">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
${weibometa!}
  <title>${page_title}</title>
  <link rel="icon" href="/bdtlaw/monitor/static/img/favicon.ico">

  <link rel="stylesheet" href="/bdtlaw/monitor/static/css/pybbs.css">
  <link rel="stylesheet" href="/bdtlaw/monitor/static/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bdtlaw/monitor/static/css/flat-ui.min.css">
    <link rel="stylesheet" href="/bdtlaw/monitor/static/css/jquery-ui.min.css">
  <script src="/bdtlaw/monitor/static/js/jquery-2.2.3.min.js"></script>
    <script src="/bdtlaw/monitor/static/js/jquery-ui.min.js"></script>
  <script src="/bdtlaw/monitor/static/js/bootstrap.min.js"></script>
    <script src="/bdtlaw/monitor/static/js/macarons.js"></script>
    <script src="/bdtlaw/monitor/static/js/echarts.js"></script>

    <link rel="stylesheet" href="/bdtlaw/monitor/static/css/bootstrap-table.css">
    <link rel="stylesheet" href="/bdtlaw/monitor/static/css/bootstrap-datetimepicker.min.css">

    <script type ="text/javascript" src="/bdtlaw/monitor/static/js/bootstrap-table.min.js"></script>
    <script type ="text/javascript" src="/bdtlaw/monitor/static/js/bootstrap-table-zh-CN.min.js"></script>
    <script type ="text/javascript" src="/bdtlaw/monitor/static/js/bootstrap-table-export.js"></script>
    <script type ="text/javascript" src="/bdtlaw/monitor/static/js/bootstrap-datetimepicker.min.js"></script>
    <script type ="text/javascript" src="/bdtlaw/monitor/static/js/bootstrap-datetimepicker.zh-CN.js"></script>

    <script type="text/javascript" src="/bdtlaw/monitor/static/js/util.js"></script>
    <script type="text/javascript" src="/bdtlaw/monitor/static/js/extension.js"></script>

</head>
<body>

  <#include "./header.ftl">
  <@header page_tab=page_tab/>
  <div class="container">
    <#nested />
  </div>

  <#include "./footer.ftl">
  <@footer/>

</body>
</html>
</#macro>