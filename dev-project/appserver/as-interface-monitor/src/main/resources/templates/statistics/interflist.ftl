<#include "../common/layout.ftl">
<@html page_title="接口列表">

<script type ="text/javascript" src="/bdtlaw/monitor/static/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="/bdtlaw/monitor/static/js/statistics/interflist.js"></script>
<script type="text/javascript" src="/bdtlaw/monitor/static/js/util.js"></script>

<div class="col-xs-12">
    <div class="panel-body paginate-bot">

        <div><h4 style="color: #1abc9c;text-align: center;">接口列表</h4></div>
        <table class="table table-hover table-bordered">
            <thead>

            <tr>
                <th style="text-align: center;">接口名称</th>
                <th style="text-align: center;">描述</th>
                <th style="text-align: center;">所属系统</th>
                <th style="text-align: center;">统计信息</th>
            </tr>
            </thead>
            <tbody>
                <#list interfaceList as intf>

                <tr>
                    <td>${intf.url!}</td>
                    <td>${intf.description!}</td>
                    <td>${ intf.getSystemName(intf.belongSystemId)!}</td>
                    <td><button id="statistics_${intf.id!}" type="button" class="btn btn-default" data-toggle="modal" onclick="interfaceMgr.showStatistics(this)">统计信息</button></td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>


</div>
<script type="application/javascript">
    $('.navbar-nav > li').removeClass('active');
    $('#interfacelist').addClass('active');
    $('#instanceMenu').addClass('active');


</script>
</@html>