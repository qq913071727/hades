<#include "../common/layout.ftl">
<@html page_title="接口统计">

<head>
	<script type="text/javascript" src="/bdtlaw/monitor/static/js/statistics/statistics.js"></script>
	<script type="text/javascript" src="/bdtlaw/monitor/static/js/constant.js"></script>
</head>

<body>
	<div class="row">
	    <div>
	    	<!-- 搜索 -->			
			开始时间：<input id="beginTime" size="25" type="text" value="" readonly class="form_datetime">
			结束时间：<input id="endTime" size="25" type="text" value="" readonly class="form_datetime">
			<button id="findVSystemInterfaceMonitorByCondition" type="button" class="btn btn-default">确定</button>
			<button id="reset" type="button" class="btn btn-default">重置</button>
	    </div>
	    <div class="col-md-12">
	        <h4>
	            <a href="">${description!}</a>
	            <small><a href="${url!}" target="_blank" class="text-muted">${url!}</a>
	                ${method!}
	            </small>
	        </h4>
	
	        <div id="interfaceMonitorChart_${interfaceId!}" style="height:600px; width:100%"></div>
	    </div>
	</div>
	
	<div id="dialog" title="警告">
		<p>开始时间和结束时间或者都为空，或者都不为空！</p>
	</div>
</body>

</@html>