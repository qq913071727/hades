/**
 * 接口扫描结果常量对象
 */
const ScanResultType = {
	/**
	 * 不可用
	 */
	DISABLE : "0",

	/**
	 * 接口不可用的中文描述
	 */
	DISABLE_DESCRIPTION : "不可用",

	/**
	 * 正常
	 */
	NORMAL : "1",

	/**
	 * 接口正常的中文描述
	 */
	NORMAL_DESCRIPTION : "正常"
};