option = {
    title: {
        text: "接口效率及可用性统计分析"
    },
    tooltip: {
        trigger: "axis",
        axisPointer: {
            type: "cross"
        }
    },
    toolbox: {
        show: true,
        feature: {
            saveAsImage: {}
        }
    },
    xAxis:  {
        type: "category",
        boundaryGap: false,
        data: []
    },
    yAxis: {
        type: "value",
        axisLabel: {
            formatter: "{value} 秒"
        },
        axisPointer: {
            snap: true
        }
    },
    visualMap: {
        show: false,
        dimension: 0,
        pieces: [{
            lte: 6,
            color: "green"
        }, {
            gt: 6,
            lte: 8,
            color: "green"
        }, {
            gt: 8,
            lte: 14,
            color: "green"
        }, {
            gt: 14,
            lte: 17,
            color: "green"
        }, {
            gt: 17,
            color: "green"
        }]
    },
    series: [
        {
            name:"响应时间",
            type:"line",
            smooth: true,
            data: [],
            markArea: {
            	data: []
            }
//	        markArea: {
//	            data: [ [{
//	                name: "早高峰",
//	                xAxis: "07:30"
//	            }, {
//	                xAxis: "10:00"
//	            }], [{
//	                name: "晚高峰",
//	                xAxis: "17:30"
//	            }, {
//	                xAxis: "21:15"
//	            }] ]
//	        }
        }
    ]
};

/**
 * 统计信息对象
 */
var statisticsMgr={
	seriesData : new Array(),
		
	xAxisData : new Array(),
	
	/**
	 * 初始化接口监控图表
	 */
	initInterfaceMonitorChart : function() {
		var interfaceId = $("div[id^='interfaceMonitorChart']").attr("id").split("_")[1];
		
		// 取前一天和今天
		var beginTime = Date.parse(new Date(new Date()-24*60*60*1000));
		var newBeginTime=new Date(beginTime);
		var paramBeginTime=newBeginTime.format("yyyy-MM-dd hh:mm:ss");
		var finishTime = Date.parse(new Date());
		var newFinishTime=new Date(finishTime);
		var paramFinishTime=newFinishTime.format("yyyy-MM-dd hh:mm:ss");
		
		$.ajax({
			type : "post",
			url : "../findVSystemInterfaceMonitorByInterfaceId",
			data : {
				"interfaceId" : interfaceId,
				"beginTime" : paramBeginTime,
				"finishTime" : paramFinishTime
			},
			dataType : "json",
			success : function(data) {
				var interfaceId = $("div[id^='interfaceMonitorChart']").attr("id").split("_")[1];
				statisticsMgr.generateEChart(data, interfaceId);
			}
		});		
	},

	/**
	 * 初始化datetimepicker
	 */
	initDatetimepicker : function(){
		$("#beginTime").datetimepicker({
			format: "yyyy-mm-dd hh:ii:ss",
			language : "zh-CN",
			autoclose: true,
			todayBtn: true,
			minuteStep: 10

		}).on("click",function(){
			$("#beginTime").datetimepicker("setEndDate",$("#endTime").val())
		});
		$("#endTime").datetimepicker({
			format: "yyyy-mm-dd hh:ii:ss",
			language : "zh-CN",
			autoclose: true,
			todayBtn: true,
			minuteStep: 10
		}).on("click",function(){
			$("#endTime").datetimepicker("setStartDate",$("#beginTime").val())
		});
	},
	
	/**
	 * 初始化事件处理函数
	 */
	bindEventHandler : function(){
		/**
		 * 按开始时间和结束时间查询数据，并重新展示echart图
		 */
		$("#findVSystemInterfaceMonitorByCondition").bind("click", function(){
			var interfaceId = $("div[id^='interfaceMonitorChart']").attr("id").split("_")[1];
			
			var beginTime=$("#beginTime").val();
			var endTime=$("#endTime").val();
			
			// 开始时间和结束时间或者都为空，或者都不为空
			if((util.isNull(beginTime) && !util.isNull(endTime)) || (!util.isNull(beginTime) && util.isNull(endTime))){
				$("#dialog").dialog({
					  modal: true,
					  buttons: [ {
						  text: "关闭",
					      click: function() {
					    	  $(this).dialog("close");
					      }
					  } ]
				});
				return false;
			}
			
			$.ajax({
	             type: "post",
	             url: "../findVSystemInterfaceMonitorByInterfaceId",
	             data : {
	            	 "interfaceId" : interfaceId,
	            	 "beginTime" : beginTime,
	            	 "finishTime" : endTime
	             },
	             dataType: "json",
	             success: function(data){
	            	 var interfaceId = $("div[id^='interfaceMonitorChart']").attr("id").split("_")[1];
	            	 statisticsMgr.generateEChart(data, interfaceId);
	             }
	         });
		});
		
		/**
		 * 按重置按钮后触发的事件处理函数
		 */
		$("#reset").bind("click", function(){
			$("#beginTime").val("");
			$("#endTime").val("");
		});
	},
	
	/**
	 * 创建echart图S
	 */
	generateEChart : function(data, interfaceId){
		// 销毁echarts实例
		echarts.dispose(document.getElementById("interfaceMonitorChart_"+interfaceId));
		
		if(!util.isEmptyArray(data)){
			// 保存x轴和y轴数据
			// 如果没有下面两句，之前的数据也会存在
			statisticsMgr.seriesData=new Array();
			statisticsMgr.xAxisData=new Array();
			for(var i=0; i<data.length; i++){
				statisticsMgr.seriesData.push(parseFloat(data[i].responseTime));
				statisticsMgr.xAxisData.push(util.timestampToDate(data[i].scanTime));
			}
			
			// 保存不可用的时间段
			for(var i=0; i<data.length; i++){
				var iScanResult=data[i].scanResult;
				var iScanTime=util.timestampToDate(data[i].scanTime);
				
				if(iScanResult===ScanResultType.DISABLE_DESCRIPTION){
					var unavailableArray=new Array();
					// name的值暂时未空字符串，否则图表上面可能会出现大量相互叠在一起的文字
					unavailableArray.push({"name" : "", "xAxis" : iScanTime});
					
					for(var j=i+1; j<data.length; j++){
						var jScanResult=data[j].scanResult;
						var jScanTime=util.timestampToDate(data[j].scanTime);
						i=j;
						if(jScanResult===ScanResultType.DISABLE_DESCRIPTION && j!=data.length-1){
							// 不是最后一条记录
							continue;
						}else if(jScanResult===ScanResultType.DISABLE_DESCRIPTION && j==data.length-1){
							// 是最后一条记录
							unavailableArray.push({"xAxis" : jScanTime});
							option.series[0].markArea.data.push(unavailableArray);
						}else{
							unavailableArray.push({"xAxis" : jScanTime});
							option.series[0].markArea.data.push(unavailableArray);
							break;
						}
					}
					
					
				}
			}
			
			option.xAxis.data=statisticsMgr.xAxisData;
			option.series[0].data=statisticsMgr.seriesData;
			
			var interfaceMonitorChart = echarts.init(document.getElementById("interfaceMonitorChart_"+interfaceId), e_macarons);
			interfaceMonitorChart.setOption(option);
		}
	}
};


/**
 * 入口函数
 */
$(function() {
	$("#dialog").dialog();
	$("#dialog").dialog("close");
	
	statisticsMgr.initInterfaceMonitorChart();
	statisticsMgr.initDatetimepicker();
	statisticsMgr.bindEventHandler();
});