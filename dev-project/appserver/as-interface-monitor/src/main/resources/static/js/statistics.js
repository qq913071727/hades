function randomData() {
    now = new Date(+now + oneDay);
    value = value + Math.random() * 21 - 10;
    return {
        name: now.toString(),
        value: [
            [now.getFullYear(), now.getMonth() + 1, now.getDate()].join('/'),
            Math.round(value)
        ]
    }
}

var data = [];
var now = +new Date(1997, 9, 3);
var oneDay = 24 * 3600 * 1000;
var value = Math.random() * 1000;
for (var i = 0; i < 1000; i++) {
    data.push(randomData());
}

require.config({
    paths: {
        echarts: '/api/static/js/echarts'
    }
});
require(
    [
        'echarts',
        'echarts/chart/bar',
        'echarts/chart/line'
    ],
    function (ec) {
        // <c:forEach items="${indexDto.instanceDtos}" var="ins" varStatus="s">
        var option1 = {
            title: {
                text: '用于法律法规数据检索（检索正文内容）',
                subtext: 'http://172.17.80.166:18119/api/bdt/law/ssjs/'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: [{name:'链接时间'}]
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {show: false},
                    dataView: {show: false, readOnly: false},
                    magicType: {show: true, type: ['line', 'bar']},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            xAxis: [
                {
                    type: 'category',
                    data:'时间'
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: '链接时间（ms）',
                    type: 'line',
                    data:data,
                    markLine: {
                        data: [
                            {
                                type: 'average',
                                name: '平均链接时间'
                            }
                        ]
                    }
                }
            ]
        };

        var myChart1= ec.init(document.getElementById('chart1'));
        myChart1.setOption(option1);
        myChart1.setTheme('macarons');

        var lastLogDate1 = '';
       /* setInterval(function () {
            $.get("load_addition_monitor_logs.hb", {
                lastLogDate: lastLogDate1,
            guid: 'id'
        }, function (data) {
                myChart1.addData(eval(data.additionData));
                lastLogDate1 = data.lastLogDate;
            });

        }, 1000);*/
        setInterval(function () {

            for (var i = 0; i < 5; i++) {
                data.shift();
                data.push(randomData());
            }

            myChart.setOption({
                series: [{
                    data: data
                }]
            });
        }, 1000);




        // </c:forEach>
    }
);
