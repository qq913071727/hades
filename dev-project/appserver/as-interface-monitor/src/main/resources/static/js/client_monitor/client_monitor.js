var vClientMonitorMgr={
		/**
		 * 初始化bootstrap-table
		 */
		initTable : function() {
			$("#vClientMonitorTable").bootstrapTable({		
				method : "POST",
				dataType : "json",
				contentType : "application/x-www-form-urlencoded",
				cache : false,
				striped : true, // 是否显示行间隔色
				url : "findAllBdtVClientMonitorEntity",
				width : "60%",
				showColumns : true,	// 是否显示 内容列下拉框
				pagination : true,
				showPaginationSwitch : true, //	是否显示 数据条数选择框
				pageNumber : 1, // 初始化加载第一页，默认第一页
				pageSize : 10, // 每页的记录行数（*）
				pageList : [ 5, 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
				sortable: true, //是否启用排序 
				sortOrder: "exceptionErrorTime desc", //排序方式
				showRefresh : true,
				uniqueId : "id", // 每一行的唯一标识，一般为主键列
//				showExport : true,	// 表示是否显示导出的按钮
				showToggle : true,	// 是否显示 切换试图（table/card）按钮
//				detailView : true,	// 设置为 true 可以显示详细页面模式。
//				search : true,	// 是否启用搜索框
//				searchOnEnterKey : true	// 设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
				columns : [ {
                    title: "编号",
                    formatter: function (value, row, index) {  
                        return index+1;  
                    }  
                }, {
					field : "id",
					title : "id",
					visible : false
				}, {
					field : "deviceNumber",
					title : "设备号",
//					titleTooltip : "test",	// The column title tooltip text. This option also support the title HTML attribute
					align : "center",
					valign : "middle"
					
				}, {
					field : "mobilePhoneType",
					title : "手机型号",
					align : "center",
					valign : "middle"
				}, {
					field : "operationSystemVersion",
					title : "操作系统版本",
					align : "center",
					valign : "middle"
				}, {
					field : "operationSystemType",
					title : "操作系统类型",
					align : "center",
					valign : "middle"
				}, {
					field : "carrieroperator",
					title : "运营商",
					align : "center",
					valign : "middle"
				}, {
					field : "clientVersion",
					title : "客户端版本",
					align : "center",
					valign : "left"
				}, {
					field : "connectNetworkType",
					title : "联网方式",
					align : "center",
					valign : "middle"
				}, {
					field : "userRegion",
					title : "地域",
					align : "center",
					valign : "middle"
				}, {
					field : "exceptionErrorTime",
					title : "异常/错误时间",
					align : "center",
					valign : "middle",
					sortable : true,
					formatter : function(str){
						return new Date(str).format("yyyy-MM-dd hh:mm:ss.SSS");
					}
				}, {
//					field : "exceptionErrorInfo",
					title : "异常/错误信息",
					align : "center",
					valign : "middle",
					/**
					 * 在异常/错误信息列展示详细信息和历史信息按钮
					 */
					formatter : function(value, row, index){
						var detail="<button id='detail_"+row.id+"' type='button' class='btn btn-default' data-toggle='modal' data-target='#exceptionErrorInfo' onclick='vClientMonitorMgr.showDetailInfo(this)'>详细信息</button>";
												
						return detail;
					}
				} ]
			});
		},

		/**
		 * 初始化datetimepicker
		 */
		initDatetimepicker : function(){
		    /*$(".form_datetime").datetimepicker({
		        format: "yyyy-mm-dd hh:ii:ss",
		        language : "zh-CN",
		        autoclose: true,
		        todayBtn: true,
		        minuteStep: 10
		    });*/


			$("#beginTime").datetimepicker({
				format: "yyyy-mm-dd hh:ii:ss",
				language : "zh-CN",
				autoclose: true,
				todayBtn: true,
				minuteStep: 10

			}).on("click",function(){
				$("#beginTime").datetimepicker("setEndDate",$("#endTime").val())
			});
			$("#endTime").datetimepicker({
				format: "yyyy-mm-dd hh:ii:ss",
				language : "zh-CN",
				autoclose: true,
				todayBtn: true,
				minuteStep: 10
			}).on("click",function(){
				$("#endTime").datetimepicker("setStartDate",$("#beginTime").val())
			});
		},
		
		/**
		 * 初始化事件处理函数
		 */
		bindEventHandler : function(){
			/**
			 * 按条件查询客户端异常信息，将结果展示在bootstrap-table中，跳回第一页
			 */
			$("#findVClientMonitorByCondition").bind("click", function(){
				var beginTime=$("#beginTime").val();
				var endTime=$("#endTime").val();
				
				// 开始时间和结束时间或者都为空，或者都不为空
				if((util.isNull(beginTime) && !util.isNull(endTime)) || (!util.isNull(beginTime) && util.isNull(endTime))){
					$("#dialog").dialog({
						  modal: true,
						  buttons: [ {
							  text: "关闭",
						      click: function() {
						    	  $(this).dialog("close");
						      }
						  } ]
					});
					return false;
				}
				
				// 根据开始时间和结束时间查找客户端监控数据，并将其重新填入bootstrap-table表格中
				$.ajax({
		             type: "post",
		             url: "findVClientMonitorByCondition",
		             data : {
		            	 "beginTime" : beginTime,
		            	 "endTime" : endTime
		             },
		             dataType: "json",
		             success: function(data){
		            	 $("#vClientMonitorTable").bootstrapTable("load", data);
		            	 $("#vClientMonitorTable").bootstrapTable("selectPage", 1);
		             }
		         });
			});
			
			/**
			 * 按重置按钮后触发的事件处理函数
			 */
			$("#reset").bind("click", function(){
				$("#beginTime").val("");
				$("#endTime").val("");
			});
		},
		
		/**
		 * 展示接口异常/错误的详细信息
		 */
		showDetailInfo : function(param){
			// 从bootstrap-table表格中根据id获取相应行的exceptionErrorInfo信息
			var id=$(param).attr("id").split("_")[1];
			var row=$("#vClientMonitorTable").bootstrapTable("getRowByUniqueId", id);
			var exceptionErrorInfo=row.exceptionErrorInfo;
						
			// 更新弹出框的标题
			$("#modalTitle").empty();
			$("#modalTitle").append("接口异常/错误的详细信息");
			
			// 更新弹出框的内容
			$("#exceptionErrorInfo span").empty();
			var date=new Date(row.exceptionErrorTime).format("yyyy-MM-dd hh:mm:ss.SSS");
			$("#exceptionErrorInfo span").append(date+"   "+exceptionErrorInfo);
		}
};

/**
 * 入口函数
 */
$(function() {
	$("#beginTime").val("");
	$("#endTime").val("");
	
	$("#dialog").dialog();
	$("#dialog").dialog("close");
	
	// 初始化bootstrap-table表格
	vClientMonitorMgr.initTable();
	
	// 初始化datetimepicker日期时间控件
	vClientMonitorMgr.initDatetimepicker();
	
	// 绑定事件处理函数
	vClientMonitorMgr.bindEventHandler();
	
	
	
	
	
	
	
	
	
	
	
});