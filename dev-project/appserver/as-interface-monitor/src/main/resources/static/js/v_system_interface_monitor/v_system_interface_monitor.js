var vSystemInterfaceMonitorMgr={
		/**
		 * 初始化bootstrap-table
		 */
		initTable : function() {
			$("#vSystemInterfaceMonitorTable").bootstrapTable({		
				method : "POST",
				dataType : "json",
				contentType : "application/x-www-form-urlencoded",
				cache : false,
				striped : true, // 是否显示行间隔色
				url : "findBdtVSystemInterfaceMonitorEntityByPerformanceAndUsability",
				width : "60%",
				showColumns : true,	// 是否显示 内容列下拉框
				pagination : true,
				showPaginationSwitch : true, //	是否显示 数据条数选择框
				pageNumber : 1, // 初始化加载第一页，默认第一页
				pageSize : 10, // 每页的记录行数（*）
				pageList : [ 5, 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
				sortable: true, //是否启用排序 
				sortOrder: "endTime desc", //排序方式
				showRefresh : true,
				uniqueId : "id", // 每一行的唯一标识，一般为主键列
//				showExport : true,	// 表示是否显示导出的按钮
				showToggle : true,	// 是否显示 切换试图（table/card）按钮
//				detailView : true,	// 设置为 true 可以显示详细页面模式。
//				search : true,	// 是否启用搜索框
//				searchOnEnterKey : true	// 设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
				columns : [ {
                    title: "编号",
                    formatter: function (value, row, index) {  
                        return index+1;  
                    }  
                }, {
					field : "id",
					title : "id",
					visible : false
				}, {
					field : "接口id",
					title : "interfaceId",
					visible : false
				}, {
					field : "endTime",
					title : "扫描时间",
					align : "center",
					valign : "middle",
					sortable : true,
					formatter : function(str){
						return new Date(str).format("yyyy-MM-dd hh:mm:ss.SSS");
					}
				}, {
					field : "responseTime",
					title : "响应时间",
					align : "center",
					valign : "middle"
				}, {
					field : "scanResult",
					title : "扫描结果",
					align : "center",
					valign : "middle"
				}, {
					field : "url",
					title : "接口URL",
					align : "center",
					valign : "middle"
				}, {
					field : "method",
					title : "请求方式",
					align : "center",
					valign : "left"
				}, {
					field : "description",
					title : "描述",
					align : "center",
					valign : "middle"
				}, {
					field : "systemName",
					title : "所属系统名称",
					align : "center",
					valign : "middle"
				}, {
//					field : "errorInfo",
					title : "错误信息",
					align : "center",
					valign : "middle",
					/**
					 * 在异常/错误信息列展示详细信息和历史信息按钮
					 */
					formatter : function(value, row, index){
						var detail="<button id='detail_"+row.id+"' type='button' class='btn btn-default' data-toggle='modal' data-target='#errorInfo' onclick='vSystemInterfaceMonitorMgr.showDetailInfo(this)'>详细信息</button>";
						var history="<button id='history_"+row.interfaceId+"' type='button' style='margin: 1px;' class='btn btn-default' data-toggle='modal' data-target='#errorInfo' onclick='vSystemInterfaceMonitorMgr.showHistoryInfo(this)'>历史信息</button>";
												
						return detail+history;
					}
				} ]
			});
		},

		/**
		 * 初始化datetimepicker
		 */
		initDatetimepicker : function(){
			$("#beginTime").datetimepicker({
				format: "yyyy-mm-dd hh:ii:ss",
				language : "zh-CN",
				autoclose: true,
				todayBtn: true,
				minuteStep: 10

			}).on("click",function(){
				$("#beginTime").datetimepicker("setEndDate",$("#finishTime").val())
			});
			$("#finishTime").datetimepicker({
				format: "yyyy-mm-dd hh:ii:ss",
				language : "zh-CN",
				autoclose: true,
				todayBtn: true,
				minuteStep: 10
			}).on("click",function(){
				$("#finishTime").datetimepicker("setStartDate",$("#beginTime").val())
			});
		},
		
		/**
		 * 初始化事件处理函数
		 */
		bindEventHandler : function(){
			/**
			 * 按条件查询客户端异常信息，将结果展示在bootstrap-table中，跳回第一页
			 */
			$("#findVSystemInterfaceMonitorByCondition").bind("click", function(){
				var url=$("#url").val();
				var description=$("#description").val();
				var beginTime=$("#beginTime").val();
				var finishTime=$("#finishTime").val();
				
				// 开始时间和结束时间或者都为空，或者都不为空
				if((util.isNull(beginTime) && !util.isNull(finishTime)) || (!util.isNull(beginTime) && util.isNull(finishTime))){
					$("#dialog").dialog({
						  modal: true,
						  buttons: [ {
							  text: "关闭",
						      click: function() {
						    	  $(this).dialog("close");
						      }
						  } ]
					});
					return false;
				}
				
				$.ajax({
		             type: "post",
		             url: "findVSystemInterfaceMonitorByCondition",
		             data : {
		            	 "url" : url,
		            	 "description" : description,
		            	 "beginTime" : beginTime,
		            	 "finishTime" : finishTime
		             },
		             dataType: "json",
		             success: function(data){
		            	 $("#vSystemInterfaceMonitorTable").bootstrapTable("load", data);
		            	 $("#vSystemInterfaceMonitorTable").bootstrapTable("selectPage", 1);
		             }
		         });
			});
			
			/**
			 * 按重置按钮后触发的事件处理函数
			 */
			$("#reset").bind("click", function(){
				$("#url").val("");
				$("#description").val("");
				$("#beginTime").val("");
				$("#finishTime").val("");
			});
			
			/**
			 * 当光标在url、描述的文本框中，按回车按钮时触发的事件处理函数
			 */
			$("#url, #description").keydown(function(event){
				if(event.keyCode==13){
					$("#findVSystemInterfaceMonitorByCondition").trigger("click");
				}
			});
		},
		
		/**
		 * 展示接口异常/错误的详细信息
		 */
		showDetailInfo : function(param){
			$("#errorInfo span").bootstrapTable('destroy');
			// 从bootstrap-table表格中根据id获取相应行的errorInfo信息
			var id=$(param).attr("id").split("_")[1];
			var row=$("#vSystemInterfaceMonitorTable").bootstrapTable("getRowByUniqueId", id);
			var errorInfo=row.errorInfo;
						
			// 更新弹出框的标题
			$("#modalTitle").empty();
			$("#modalTitle").append("错误的详细信息");
			
			// 更新弹出框的内容
			$("#errorInfo span").empty();
			var date=new Date(row.endTime).format("yyyy-MM-dd hh:mm:ss.SSS");
			$("#errorInfo span").append(date+"   "+errorInfo);
		},
		
		/**
		 * 展示接口异常/错误的历史信息
		 */
		showHistoryInfo : function(param){
			$("#errorInfo span").empty();
			$("#errorInfo span").bootstrapTable('destroy');
			// 根据interfaceId，从bootstrap-table中选出相同的行，并将其errorInfo列合并起来在弹出框显示
			var interfaceId=$(param).attr("id").split("_")[1];
			var allRows=$("#vSystemInterfaceMonitorTable").bootstrapTable("getData", false);

			if(!util.isEmptyArray(allRows)){

						$("#errorInfo span").bootstrapTable({
							cache: false,
							striped: true, // 是否显示行间隔色
							width: "60%",
							showColumns: true,	// 是否显示 内容列下拉框
							pagination: true,
							showPaginationSwitch: true, //	是否显示 数据条数选择框
							pageNumber: 1, // 初始化加载第一页，默认第一页
							pageSize: 2, // 每页的记录行数（*）
							pageList: [2, 4, 6, 8, 10], // 可供选择的每页的行数（*）
							sortable: true, //是否启用排序
							sortOrder: "endTime desc", //排序方式
							showRefresh: true,
							uniqueId: "id", // 每一行的唯一标识，一般为主键列
//				showExport : true,	// 表示是否显示导出的按钮
							showToggle: true,	// 是否显示 切换试图（table/card）按钮
//				detailView : true,	// 设置为 true 可以显示详细页面模式。
//				search : true,	// 是否启用搜索框
//				searchOnEnterKey : true	// 设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
							columns: [{
								field: "endTime",
								title: "扫描时间",
								align: "center",
								valign: "middle",
								sortable: true,
								formatter: function (str)
								{
									return new Date(str).format("yyyy-MM-dd hh:mm:ss.SSS");
								}
							}, {
								field: "errorInfo",
								title: "错误信息",
								align: "center",
								valign: "middle"

							}]
						});
						$("#errorInfo span").bootstrapTable("load", allRows);

/*

				$.each(allRows, function(index, row) {
					if(row.interfaceId===interfaceId){
						var date=new Date(row.endTime).format("yyyy-MM-dd hh:mm:ss.SSS");
						$("#errorInfo span").append(date+"   "+row.errorInfo+"<br />");
					}
				});*/
			}
			
			// 更新弹出框的标题
			$("#modalTitle").empty();
			$("#modalTitle").append("错误的历史信息");

		}
};

/**
 * 入口函数
 */
$(function() {
	$("#dialog").dialog();
	$("#dialog").dialog("close");

	// 初始化bootstrap-table表格
	vSystemInterfaceMonitorMgr.initTable();
	
	// 初始化datetimepicker日期时间控件
	vSystemInterfaceMonitorMgr.initDatetimepicker();
	
	// 绑定事件处理函数
	vSystemInterfaceMonitorMgr.bindEventHandler();
	
	
	
	
	
	
	
	
	
	
	
});