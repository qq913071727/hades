package cn.conac.as.framework.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 返回结果封装类
 */
@ApiModel(value = "返回结果封装类")
public class ResultPojo implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * CODE_SUCCESS
     */
    public static final String CODE_SUCCESS = "1000";
    /**
     * MSG_SUCCESS
     */
    public static final String MSG_SUCCESS = "成功";
    /**
     * CODE_FAILURE
     */
    public static final String CODE_FAILURE = "2000";
    /**
     * MSG_FAILURE
     */
    public static final String MSG_FAILURE = "失败";

    /**
     * 请登录
     */
    public static final String CODE_NOT_LOGIN = "2400";
    /**
     * 没有权限操作
     */
    public static final String CODE_NOT_AUTHORIZE = "2403";
    /**
     * 格式校验失败
     */
    public static final String CODE_FORMAT_ERR = "2501";
    /**
     * 逻辑校验失败
     */
    public static final String CODE_LOGIC_ERR = "2502";
    

    @ApiModelProperty(value = "结果code。1000：成功，2000：失败", required = true)
    private String code = CODE_SUCCESS;

    @ApiModelProperty(value = "消息提示", required = false)
    private String msg = MSG_SUCCESS;

    @ApiModelProperty(value = "真正返回结果数据", required = false)
    private Object result = null;

    /**
     * 自定义返回内容
     */
    public ResultPojo(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 自定义返回内容
     */
    public ResultPojo(String code, String msg, Object result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public ResultPojo() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResultPojo{" + "code='" + code + '\'' + ", msg='" + msg + '\'' + ", result=" + result + '}';
    }
}
