/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package cn.conac.as.framework.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModelProperty;

/**
 * 只读的数据类，如log数据
 * @param <T>
 * @author zhangfz
 * @version 1.0
 */
@MappedSuperclass
public abstract class ReadOnlyDataEntity<T> extends BaseEntity<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("备注")
    protected String remarks;

    @ApiModelProperty("创建者")
    protected String createUser;

    @ApiModelProperty("创建时间")
    protected Date createDate;//

    public ReadOnlyDataEntity() {
        super();
    }

    @Override
    @PrePersist
    public void prePersist() {
        super.prePersist();
        this.createDate = new Date();
    }

    @Length(min = 0, max = 255)

    @Column(name = "REMARKS")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", updatable = false)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "CREATE_USER", updatable = false)
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

}
