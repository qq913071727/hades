package cn.conac.as.push.entity;

/**
 * Created by liuwei on 2017-6-22.
 */
public class Extra
{
    private String extraKey;
    private String extraValue;

    public String getExtraKey()
    {
        return extraKey;
    }

    public void setExtraKey(String extraKey)
    {
        this.extraKey = extraKey;
    }

    public String getExtraValue()
    {
        return extraValue;
    }

    public void setExtraValue(String extraValue)
    {
        this.extraValue = extraValue;
    }
}
