package cn.conac.as.push.entity;

import java.util.List;

import javax.validation.constraints.Size;

/**
 * Created by liuwei on 2017-6-20.
 */
public class Broadcast
{

    private String description;

    private String title;

    @Size(max=400,message = "不能超过400个字符")

    private String text;

    private List<Extra> extraJson;



    ///定时发送时间，若不填写表示立即发送。格式: "YYYY-MM-DD hh:mm:ss"。
    private String startTime;

    private String expireTime;


    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public List<Extra> getExtraJson()
    {
        return extraJson;
    }

    public void setExtraJson(List<Extra> extraJson)
    {
        this.extraJson = extraJson;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }

    public String getExpireTime()
    {
        return expireTime;
    }

    public void setExpireTime(String expireTime)
    {
        this.expireTime = expireTime;
    }
}
