package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.conac.as.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawContentEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="BDT_LAW_CONTENT")
public class BdtLawContentEntity  extends DataEntity<BdtLawContentEntity> implements Serializable {

	private static final long serialVersionUID = 1490699901380936930L;

	@ApiModelProperty("法律法规标题")
	private String title;

	@ApiModelProperty("批准日期")
	private String ratifyDate;

	@ApiModelProperty("批准部门")
	private String ratifyDepartment;

	@ApiModelProperty("全文")
	private String fullText;

	public void setTitle(String title){
		this.title=title;
	}

	public String getTitle(){
		return title;
	}

	public String getRatifyDate() {
		return ratifyDate;
	}

	public void setRatifyDate(String ratifyDate) {
		this.ratifyDate = ratifyDate;
	}

	public void setRatifyDepartment(String ratifyDepartment){
		this.ratifyDepartment=ratifyDepartment;
	}

	public String getRatifyDepartment(){
		return ratifyDepartment;
	}

	public void setFullText(String fullText){
		this.fullText=fullText;
	}

	public String getFullText(){
		return fullText;
	}

}
