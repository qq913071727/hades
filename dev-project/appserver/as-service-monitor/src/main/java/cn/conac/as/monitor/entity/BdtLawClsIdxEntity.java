package cn.conac.as.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.as.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawClsIdxEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="BDT_LAW_CLS_IDX")
public class BdtLawClsIdxEntity extends DataEntity<BdtLawClsIdxEntity> implements Serializable {

	private static final long serialVersionUID = 149069990135550492L;

	@ApiModelProperty("法律法规标题")
	private String title;

	@ApiModelProperty("时效性")
	private String timelinessDic;

	@ApiModelProperty("效力级别")
	private String effectivenessDic;

	@ApiModelProperty("法规类别")
	private String category;

	@ApiModelProperty("发布部门")
	private String issueDepartment;

	@ApiModelProperty("发文字号")
	private String documentNo;

	@ApiModelProperty("发布日期")
	private String issueDate;

	@ApiModelProperty("实施日期")
	private String implementDate;

	@ApiModelProperty("数据库标志")
	private String library;

	@ApiModelProperty("唯一标志")
	private String gid;

	@ApiModelProperty("修改依据")
	private String revisedBasis;

	@ApiModelProperty("部分失效依据")
	private String partialFailureBasis;

	@ApiModelProperty("失效依据")
	private String failureBasis;

	@ApiModelProperty("时效性NAME")
	private String timeliness;

	@ApiModelProperty("效力级别NAME")
	private String effectiveness;

	@ApiModelProperty("页面相对路径")
	private String urlPath;

	public void setTitle(String title){
		this.title=title;
	}

	public String getTitle(){
		return title;
	}

	public void setTimelinessDic(String timelinessDic){
		this.timelinessDic=timelinessDic;
	}

	public String getTimelinessDic(){
		return timelinessDic;
	}

	public void setEffectivenessDic(String effectivenessDic){
		this.effectivenessDic=effectivenessDic;
	}

	public String getEffectivenessDic(){
		return effectivenessDic;
	}

	public void setCategory(String category){
		this.category=category;
	}

	public String getCategory(){
		return category;
	}

	public void setIssueDepartment(String issueDepartment){
		this.issueDepartment=issueDepartment;
	}

	public String getIssueDepartment(){
		return issueDepartment;
	}

	public void setDocumentNo(String documentNo){
		this.documentNo=documentNo;
	}

	public String getDocumentNo(){
		return documentNo;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getImplementDate() {
		return implementDate;
	}

	public void setImplementDate(String implementDate) {
		this.implementDate = implementDate;
	}

	public void setLibrary(String library){
		this.library=library;
	}

	public String getLibrary(){
		return library;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public void setRevisedBasis(String revisedBasis){
		this.revisedBasis=revisedBasis;
	}

	public String getRevisedBasis(){
		return revisedBasis;
	}

	public void setPartialFailureBasis(String partialFailureBasis){
		this.partialFailureBasis=partialFailureBasis;
	}

	public String getPartialFailureBasis(){
		return partialFailureBasis;
	}

	public void setFailureBasis(String failureBasis){
		this.failureBasis=failureBasis;
	}

	public String getFailureBasis(){
		return failureBasis;
	}

	public String getTimeliness() {
		return timeliness;
	}

	public void setTimeliness(String timeliness) {
		this.timeliness = timeliness;
	}

	public String getEffectiveness() {
		return effectiveness;
	}

	public void setEffectiveness(String effectiveness) {
		this.effectiveness = effectiveness;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(String urlPath)
	{
		this.urlPath = urlPath;
	}
}
