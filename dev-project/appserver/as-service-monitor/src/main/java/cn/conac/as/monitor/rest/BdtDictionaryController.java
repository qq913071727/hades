package cn.conac.as.monitor.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.as.monitor.entity.BdtDictionaryEntity;
import cn.conac.as.monitor.service.BdtDictionaryService;
import cn.conac.as.monitor.vo.BdtDictionaryVo;
import cn.conac.as.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

/**
 * BdtDictionaryController类
 *
 * @author controllerCreator
 * @date 2017-03-28
 * @version 1.0
 */
@RestController
@RequestMapping(value="bdtDictionary/")
public class BdtDictionaryController {

	@Autowired
	BdtDictionaryService service;

	@ApiOperation(value = "根据一、二级分类CD查询分类信息", httpMethod = "GET", response = BdtDictionaryEntity.class, notes = "根据一、二级分类CD查询分类信息")
	@RequestMapping(value = "clsCode12/{clsCode1}/{clsCode2}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByClsCode12(HttpServletRequest request, HttpServletResponse response,
											 @ApiParam(value = "clsCode1", required = true) @PathVariable("clsCode1") String clsCode1,
											 @ApiParam(value = "clsCode2", required = true) @PathVariable("clsCode2") String clsCode2) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		BdtDictionaryEntity entity = service.findByClsCode12(clsCode1,clsCode2);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "详情", httpMethod = "GET", response = BdtDictionaryEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "clsCode1/{clsCode1}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByClsCode1(HttpServletRequest request, HttpServletResponse response,
													 @ApiParam(value = "clsCode1", required = true) @PathVariable("clsCode1") String clsCode1) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<BdtDictionaryEntity> list = service.findByClsCode1(clsCode1);
		// 结果集设定
		if(list ==null || list.size() ==0){
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		} else {
			result.setResult(list);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "详情", httpMethod = "GET", response = BdtDictionaryEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		BdtDictionaryEntity entity = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody BdtDictionaryVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = BdtDictionaryEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody BdtDictionaryVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<BdtDictionaryEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新数据", httpMethod = "POST", response = BdtDictionaryEntity.class, notes = "新增或者更新数据库资源")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "对象", required = true) @RequestBody BdtDictionaryEntity entity) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		service.save(entity);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = BdtDictionaryEntity.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		if(service.exists(id)){
			service.delete(id);
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}
		result.setResult(id);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
