package cn.conac.as.monitor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtLawClsIdxEntity;

/**
 * BdtLawClsIdxRepository类
 *
 * @author repositoryCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Repository
public interface BdtLawClsIdxRepository extends GenericDao<BdtLawClsIdxEntity, String> {
	
	@Query(value = "select * from BDT_LAW_CLS_IDX where title =:title and document_no =:documentNo and issue_date =:issueDate order by UPDATE_DATE desc",
			nativeQuery = true)
	List<BdtLawClsIdxEntity> findByTitleAndDocumentNoAndIssueDate(@Param("title") String title, @Param("documentNo") String documentNo, @Param("issueDate") String issueDate);
	
	@Query(value = "select * from BDT_LAW_CLS_IDX where upper(EFFECTIVENESS_DIC) =upper(:effectivenessDic) and ROWNUM <= :rowNum order by UPDATE_DATE desc",
			nativeQuery = true)
	List<BdtLawClsIdxEntity> findByEffectivenessDicAndRowNum(@Param("effectivenessDic") String effectivenessDic, @Param("rowNum") String rowNum);
	
}
