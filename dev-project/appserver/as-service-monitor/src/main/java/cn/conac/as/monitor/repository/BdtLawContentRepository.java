package cn.conac.as.monitor.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.as.monitor.entity.BdtLawContentEntity;
import cn.conac.as.framework.repository.GenericDao;

/**
 * BdtLawContentRepository类
 *
 * @author repositoryCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Repository
public interface BdtLawContentRepository extends GenericDao<BdtLawContentEntity, String> {
}
