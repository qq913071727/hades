package cn.conac.as.monitor.vo;

import javax.persistence.Transient;

import cn.conac.as.monitor.entity.BdtCollectionEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by liuwei on 2017-7-20.
 */
@ApiModel
public class BdtCollectionVo extends BdtCollectionEntity
{
    private static final long serialVersionUID = -8014499779473830661L;

    @Transient
    @ApiModelProperty("当前分页")
    private Integer pageIndex;

    @Transient
    @ApiModelProperty("每页个数")
    private Integer pageSize;

    public Integer getPageIndex()
    {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex)
    {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;
    }
}
