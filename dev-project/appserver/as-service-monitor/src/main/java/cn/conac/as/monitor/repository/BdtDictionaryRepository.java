package cn.conac.as.monitor.repository;

import cn.conac.as.monitor.entity.BdtLawClsIdxEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.as.monitor.entity.BdtDictionaryEntity;
import cn.conac.as.framework.repository.GenericDao;

import java.util.List;

/**
 * BdtDictionaryRepository类
 *
 * @author repositoryCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Repository
public interface BdtDictionaryRepository extends GenericDao<BdtDictionaryEntity, String> {

    @Query(value = "select t.* from BDT_DICTIONARY t where t.CLS_CODE1 =:clsCode1 and t.CLS_CODE2 =:clsCode2", nativeQuery = true)
    BdtDictionaryEntity findByClsCode12(@Param("clsCode1") String clsCode1, @Param("clsCode2") String clsCode2);

    @Query(value = "select t.* from BDT_DICTIONARY t where t.CLS_CODE1 =:clsCode1 ORDER BY t.CLS_SORT2 ASC", nativeQuery = true)
    List<BdtDictionaryEntity> findByClsCode1(@Param("clsCode1") String clsCode1);

    List<BdtDictionaryEntity> findByClsCode1OrderByClsSort2Asc(String clsCode1);
}
