package cn.conac.as.monitor.vo;

import java.util.Date;

import javax.persistence.Transient;

import cn.conac.as.monitor.entity.BdtLawClsIdxEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawClsIdxVo类
 *
 * @author voCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
public class BdtLawClsIdxVo extends BdtLawClsIdxEntity {

	private static final long serialVersionUID = 1490699901467331118L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer pageIndex;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer pageSize;

	@Transient
	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	@Transient
	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	@Transient
	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;

	@Transient
	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

}
