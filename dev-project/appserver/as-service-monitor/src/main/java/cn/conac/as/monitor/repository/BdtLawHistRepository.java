package cn.conac.as.monitor.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.as.monitor.entity.BdtLawHistEntity;
import cn.conac.as.framework.repository.GenericDao;

/**
 * BdtLawHistRepository类
 *
 * @author repositoryCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Repository
public interface BdtLawHistRepository extends GenericDao<BdtLawHistEntity, String> {
}
