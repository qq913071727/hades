package cn.conac.as.monitor.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.as.framework.vo.ResultPojo;
import cn.conac.as.monitor.entity.BdtLawClsIdxEntity;
import cn.conac.as.monitor.service.BdtLawClsIdxService;
import cn.conac.as.monitor.vo.BdtLawClsIdxVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * BdtLawClsIdxController类
 *
 * @author controllerCreator
 * @date 2017-03-28
 * @version 1.0
 */
@RestController
@RequestMapping(value="bdtLawClsIdx/")
public class BdtLawClsIdxController {

	@Autowired
	BdtLawClsIdxService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = BdtLawClsIdxEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		BdtLawClsIdxEntity entity = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据title和documentNo,issueDate查询数据", httpMethod = "POST", response = BdtLawClsIdxEntity.class, notes = "根据title和documentNo,issueDate查询后台数据、获取法律信息")
	@RequestMapping(value = "detail", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findByTitleAndDocumentNoAndIssueDate(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "对象", required = true) @RequestBody BdtLawClsIdxEntity entity) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		BdtLawClsIdxEntity rtl = service.findByTitleAndDocumentNoAndIssueDate(entity.getTitle(), entity.getDocumentNo(), entity.getIssueDate());
		// 结果集设定
		if(rtl != null){
			result.setResult(rtl);
		} else {
			result = new ResultPojo(ResultPojo.CODE_FAILURE,ResultPojo.MSG_FAILURE);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据EffectivenessDic以及RowNum查询数据", httpMethod = "GET", response = BdtLawClsIdxEntity.class, notes = "根据EffectivenessDic以及RowNum查询后台数据、获取法律信息")
	@RequestMapping(value = "list/{effectivenessDic}/{rowNum}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByEffectivenessDicAndRowNum(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "effectivenessDic", required = true) @PathVariable("effectivenessDic") String effectivenessDic,
			@ApiParam(value = "rowNum", required = true) @PathVariable("rowNum") String rowNum) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<BdtLawClsIdxEntity> rtl = service.findByEffectivenessDicAndRowNum(effectivenessDic, rowNum);
		// 结果集设定
		if(rtl != null && rtl.size() > 0){
			result.setResult(rtl);
		} else {
			result = new ResultPojo(ResultPojo.CODE_FAILURE,ResultPojo.MSG_FAILURE);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody BdtLawClsIdxVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = BdtLawClsIdxEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody BdtLawClsIdxVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<BdtLawClsIdxEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新数据", httpMethod = "POST", response = BdtLawClsIdxEntity.class, notes = "新增或者更新数据库资源")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "对象", required = true) @RequestBody BdtLawClsIdxEntity entity) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		service.save(entity);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = BdtLawClsIdxEntity.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		if(service.exists(id)){
			service.delete(id);
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}
		result.setResult(id);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
