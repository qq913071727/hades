package cn.conac.as.monitor.service;

import java.util.List;

import cn.conac.as.framework.jpa.Restrictions;
import cn.conac.as.framework.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.jpa.Criteria;
import cn.conac.as.framework.service.GenericService;
import cn.conac.as.monitor.entity.BdtLawClsIdxEntity;
import cn.conac.as.monitor.repository.BdtLawClsIdxRepository;
import cn.conac.as.monitor.vo.BdtLawClsIdxVo;

/**
 * BdtLawClsIdxService类
 *
 * @author serviceCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Service
public class BdtLawClsIdxService extends GenericService<BdtLawClsIdxEntity, String> {

	@Autowired
	private BdtLawClsIdxRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(BdtLawClsIdxVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<BdtLawClsIdxEntity> list(BdtLawClsIdxVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.DESC, "updateDate");
			Pageable pageable = new PageRequest(vo.getPageIndex(), vo.getPageSize(), sort);
			Criteria<BdtLawClsIdxEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<BdtLawClsIdxEntity> createCriteria(BdtLawClsIdxVo param) {
		Criteria<BdtLawClsIdxEntity> dc = new Criteria<BdtLawClsIdxEntity>();
		// 标题
		if(StringUtils.isNotBlank(param.getTitle())){
			dc.add(Restrictions.like("title", param.getTitle(), true));
		}
		// 发文字号
		if(StringUtils.isNotBlank(param.getDocumentNo())){
			dc.add(Restrictions.like("documentNo", param.getDocumentNo(), true));
		}

		return dc;
	}

	/**
	 * 根据标题以及发文字号，发布日期查询法律法规
	 * @param title
	 * @param documentNo
	 * @param issueDate
	 * @return 
	 */
	public BdtLawClsIdxEntity findByTitleAndDocumentNoAndIssueDate(String title, String documentNo, String issueDate){
		BdtLawClsIdxEntity rtl = null;
		// 根据Title查询数据是否存在
		List<BdtLawClsIdxEntity> rtlList = repository.findByTitleAndDocumentNoAndIssueDate(title, documentNo, issueDate);
		if(rtlList != null && rtlList.size() > 0){
			rtl = rtlList.get(0);
		} 
		return rtl;
	}
	
	/**
	 * 根据效力级别以及显示行数查询法律法规
	 * @param effectivenessDic
	 * @param rowNum
	 * @return 
	 */
	public List<BdtLawClsIdxEntity> findByEffectivenessDicAndRowNum(String effectivenessDic, String rowNum){
		return repository.findByEffectivenessDicAndRowNum(effectivenessDic, rowNum);
	}

}
