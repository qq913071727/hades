package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.conac.as.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtLawHistEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="BDT_LAW_HIST")
public class BdtLawHistEntity  extends DataEntity<BdtLawHistEntity> implements Serializable {

	private static final long serialVersionUID = 1490699901401113833L;

	@ApiModelProperty("关联法律法规ID")
	private String relatedLawIds;

	public void setRelatedLawIds(String relatedLawIds){
		this.relatedLawIds=relatedLawIds;
	}

	public String getRelatedLawIds(){
		return relatedLawIds;
	}

}
