package cn.conac.as.monitor.service;

import cn.conac.as.monitor.entity.BdtLawClsIdxEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.as.monitor.entity.BdtDictionaryEntity;
import cn.conac.as.monitor.repository.BdtDictionaryRepository;
import cn.conac.as.monitor.vo.BdtDictionaryVo;
import cn.conac.as.framework.jpa.Criteria;
import cn.conac.as.framework.service.GenericService;

import java.util.List;

/**
 * BdtDictionaryService类
 *
 * @author serviceCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Service
public class BdtDictionaryService extends GenericService<BdtDictionaryEntity, String> {

	@Autowired
	private BdtDictionaryRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(BdtDictionaryVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<BdtDictionaryEntity> list(BdtDictionaryVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<BdtDictionaryEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<BdtDictionaryEntity> createCriteria(BdtDictionaryVo param) {
		Criteria<BdtDictionaryEntity> dc = new Criteria<BdtDictionaryEntity>();
		// TODO 具体条件赋值

		return dc;
	}

	/**
	 * 根据一、二级分类查询分类信息
	 * @param clsCode1
	 * @param clsCode2
	 * @return
	 */
	public BdtDictionaryEntity findByClsCode12(String clsCode1, String clsCode2){
		return repository.findByClsCode12(clsCode1, clsCode2);
	}

	/**
	 * 根据一级分类查询分类信息列表
	 * @param clsCode1
	 * @return
	 */
	public List<BdtDictionaryEntity> findByClsCode1(String clsCode1){
		// return repository.findByClsCode1(clsCode1);
		return repository.findByClsCode1OrderByClsSort2Asc(clsCode1);
	}


}
