package cn.conac.as.monitor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.as.monitor.entity.BdtLawHistEntity;
import cn.conac.as.monitor.repository.BdtLawHistRepository;
import cn.conac.as.monitor.vo.BdtLawHistVo;
import cn.conac.as.framework.jpa.Criteria;
import cn.conac.as.framework.service.GenericService;

/**
 * BdtLawHistService类
 *
 * @author serviceCreator
 * @date 2017-03-28
 * @version 1.0
 */
@Service
public class BdtLawHistService extends GenericService<BdtLawHistEntity, String> {

	@Autowired
	private BdtLawHistRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(BdtLawHistVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<BdtLawHistEntity> list(BdtLawHistVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<BdtLawHistEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<BdtLawHistEntity> createCriteria(BdtLawHistVo param) {
		Criteria<BdtLawHistEntity> dc = new Criteria<BdtLawHistEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
