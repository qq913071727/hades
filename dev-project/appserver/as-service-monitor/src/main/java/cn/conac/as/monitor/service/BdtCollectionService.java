package cn.conac.as.monitor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import cn.conac.as.framework.jpa.Criteria;
import cn.conac.as.framework.jpa.Restrictions;
import cn.conac.as.framework.service.GenericService;
import cn.conac.as.framework.utils.StringUtils;
import cn.conac.as.monitor.entity.BdtCollectionEntity;
import cn.conac.as.monitor.repository.BdtCollectionReposity;
import cn.conac.as.monitor.vo.BdtCollectionVo;

/**
 * Created by liuwei on 2017-7-20.
 */
@Service
public class BdtCollectionService extends GenericService<BdtCollectionEntity, String>
{

   @Autowired
    private BdtCollectionReposity reposity;



    /**
     * 动态查询，分页，排序查询
     * @param vo
     * @return Page
     * @throws Exception
     */
    public Page<BdtCollectionEntity> list(BdtCollectionVo vo){
        Sort sort = new Sort(Sort.Direction.DESC, "createDate");
        Pageable pageable = new PageRequest(vo.getPageIndex(), vo.getPageSize(),sort);
        Criteria<BdtCollectionEntity> dc = this.createCriteria(vo);
        return reposity.findAll(dc,pageable);
    }


    /**
     * 动态查询条件
     * @param param
     * @return Criteria
     */
    private Criteria<BdtCollectionEntity> createCriteria(BdtCollectionVo param) {
        Criteria<BdtCollectionEntity> dc = new Criteria<BdtCollectionEntity>();

        if(StringUtils.isNoneBlank(param.getUserId())){
            dc.add(Restrictions.eq("userId",param.getUserId(),true));

        }

        if(StringUtils.isNoneBlank(param.getTypeId())){
            dc.add(Restrictions.eq("typeId",param.getTypeId(),true));

        }

        if(StringUtils.isNoneBlank(param.getContentTitle())){
            System.out.println(param.getContentTitle());
            dc.add(Restrictions.eq("contentTitle",param.getContentTitle(),true));

        }

        // TODO 具体条件赋值

        return dc;
    }



}
