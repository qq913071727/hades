package cn.conac.as.monitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.conac.as.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BdtDictionaryEntity类
 *
 * @author beanCreator
 * @date 2017-03-28
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="BDT_DICTIONARY")
public class BdtDictionaryEntity extends DataEntity<BdtDictionaryEntity> implements Serializable {

	private static final long serialVersionUID = 1490699901230695278L;

	@ApiModelProperty("一级分类名称")
	private String clsName1;

	@ApiModelProperty("一级分类CD")
	private String clsCode1;

	@ApiModelProperty("二级分类名称")
	private String clsName2;

	@ApiModelProperty("二级分类CD")
	private String clsCode2;

	@ApiModelProperty("二级分类排序")
	private Integer clsSort2;

	public void setClsName1(String clsName1){
		this.clsName1=clsName1;
	}

	public String getClsName1(){
		return clsName1;
	}

	public void setClsCode1(String clsCode1){
		this.clsCode1=clsCode1;
	}

	public String getClsCode1(){
		return clsCode1;
	}

	public void setClsName2(String clsName2){
		this.clsName2=clsName2;
	}

	public String getClsName2(){
		return clsName2;
	}

	public void setClsCode2(String clsCode2){
		this.clsCode2=clsCode2;
	}

	public String getClsCode2(){
		return clsCode2;
	}

	public void setClsSort2(Integer clsSort2){
		this.clsSort2=clsSort2;
	}

	public Integer getClsSort2(){
		return clsSort2;
	}

}
