package cn.conac.as.monitor.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.as.framework.vo.ResultPojo;
import cn.conac.as.monitor.entity.BdtCollectionEntity;
import cn.conac.as.monitor.entity.BdtDictionaryEntity;
import cn.conac.as.monitor.service.BdtCollectionService;
import cn.conac.as.monitor.vo.BdtCollectionVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by liuwei on 2017-7-20.
 */
@RestController
@RequestMapping(value="bdtCollection/")
public class BdtCollectionController
{
    @Autowired
    BdtCollectionService service;

    @ApiOperation(value = "新增或者更新数据", httpMethod = "POST", response = BdtDictionaryEntity.class, notes = "新增或者更新数据库资源")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "对象", required = true) @RequestBody BdtCollectionEntity entity) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        service.save(entity);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(entity);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }


    @ApiOperation(value = "列表", httpMethod = "POST", response = BdtDictionaryEntity.class, notes = "根据条件获得收藏内容列表")
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody BdtCollectionVo vo) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        Page<BdtCollectionEntity> list = service.list(vo);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(list);
        System.out.println("list++++++++++++++++++"+list.getContent());
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }


    @ApiOperation(value = "列表", httpMethod = "GET", response = BdtDictionaryEntity.class, notes = "根据条件获得收藏内容列表")
    @RequestMapping(value = "{id}/delete", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "收藏记录id", required = true) @PathVariable("id") String id) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        service.delete(id);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

}
