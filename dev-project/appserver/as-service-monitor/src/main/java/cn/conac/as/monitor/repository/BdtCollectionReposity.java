package cn.conac.as.monitor.repository;

import org.springframework.stereotype.Repository;

import cn.conac.as.framework.repository.GenericDao;
import cn.conac.as.monitor.entity.BdtCollectionEntity;

/**
 * Created by liuwei on 2017-7-20.
 */
@Repository
public interface BdtCollectionReposity extends GenericDao<BdtCollectionEntity,String>
{
}
