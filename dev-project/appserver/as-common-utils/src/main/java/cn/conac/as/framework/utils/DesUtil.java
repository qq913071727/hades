/**
 * Copyright (c) 2005-2012 springside.org.cn
 */
package cn.conac.as.framework.utils;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * des加密工具类
 * @author wangmeng
 * @version 1.0
 */
@SuppressWarnings("restriction")
public class DesUtil {

    /**
     * DES
     */
    private final static String DES = "DES";

    /**
     * 加密Key值
     */
    private final static String KEY = "opeddsaead323353484591dadbc382a18340bf8341a660";

    /**
     * 根据键值进行加密
     * @param data 加密对象
     * @param key 加密键byte数组
     * @return String 加密后结果
     * @throws Exception 异常
     */
    public static String desEncrypt(String data, String key) throws Exception {
        // 判空处理
        if(StringUtils.isEmpty(data)){
            return data;
        }
        if (key == null) {
            key = KEY;
        }

        byte[] bt = encrypt(data.getBytes(), key.getBytes());
        String strs = new BASE64Encoder().encode(bt);

        return URLEncoder.encode(URLEncoder.encode(strs,"UTF-8"),"UTF-8");

    }

    /**
     * 根据键值进行解密
     * @param data 加密后对象
     * @param key 加密键byte数组
     * @return String 解密后原值
     * @throws IOException 异常
     * @throws Exception 异常
     */
    public static String desDecrypt(String data, String key) throws IOException, Exception {
        // 判空处理
        if(StringUtils.isEmpty(data)){
            return data;
        }
        if (key == null) {
            key = KEY;
        }

        data = URLDecoder.decode(data,"UTF-8");
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] buf = decoder.decodeBuffer(data);
        byte[] bt = decrypt(buf, key.getBytes());

        return new String(bt);
    }

    /**
     * Description 根据键值进行加密
     * @param data 加密对象
     * @param key 加密键byte数组
     * @return byte[] 加密后数组
     * @throws Exception 异常
     */
    private static byte[] encrypt(byte[] data, byte[] key) throws Exception {
        // 生成一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        // 从原始密钥数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成加密操作
        Cipher cipher = Cipher.getInstance(DES);
        // 用密钥初始化Cipher对象
        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
        return cipher.doFinal(data);
    }

    /**
     * Description 根据键值进行解密
     * @param data 解密对象
     * @param key 加密键byte数组
     * @return byte[] 解密后原值数组
     * @throws Exception 异常
     */
    private static byte[] decrypt(byte[] data, byte[] key) throws Exception {
        // 生成一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        // 从原始密钥数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance(DES);
        // 用密钥初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
        return cipher.doFinal(data);
    }
}