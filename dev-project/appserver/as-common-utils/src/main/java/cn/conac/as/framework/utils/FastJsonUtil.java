package cn.conac.as.framework.utils;

import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;

/**
 * FastJson工具类
 * @author wangmeng
 * @version 1.0
 */
public class FastJsonUtil {
    private static JSONObject jsonObject;

	/**
     *  生成json.
     * @param obj Object
     * @return String
     */
    public static String getJson(Object obj) {
        return JSON.toJSONString(obj, SerializerFeature.WriteMapNullValue);
    }

    /**
     *  解析Object
     * @param <T> 泛型对象
     * @param json String
     * @param c Class<T>
     * @return T
     */
    public static <T> T getObject(String json, Class<T> c) {
        T t = null;
        try {
            t = JSON.parseObject(json, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     *  解析List.
     * @param <T> 泛型对象
     * @param json String
     * @param c Class<T>
     * @return List<T>
     */
    public static <T> List<T> getList(String json, Class<T> c) {
        List<T> list = new ArrayList<T>();
        try {
            list = JSON.parseArray(json, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *  解析List.
     * @param json String
     * @return List<String>
     */
    public static List<String> getList(String json) {
        List<String> list = new ArrayList<String>();
        try {
            list = JSON.parseArray(json, String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *  解析List.
     * @param json String
     * @return List<Map<String, Object>>
     */
    public static List<Map<String, Object>> listKeyMaps(String json) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            list = JSON.parseObject(json, new TypeReference<List<Map<String, Object>>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *  解析json.
     * @param jsonObj
     * @return
     */
    public static String getJSONValue(JSONObject jsonObj){
        String rtl = StringUtils.EMPTY;
        if(jsonObj != null){
            Set<String> keySet = jsonObj.keySet();

            Iterator<String> setStr = keySet.iterator();
            while(setStr.hasNext()){
                rtl += "," + jsonObj.getString(setStr.next());
            }

//    		for(String setStr : keySet){
//    			rtl += jsonObj.getString(setStr);
//    		}
        }

        return StringUtils.isEmpty(rtl)?StringUtils.EMPTY:rtl.substring(1);
    }

    /**
     *  解析json.
     * @param jsonObj
     * @return
     */
    public static String getJSONKey(JSONObject jsonObj){
        String rtl = StringUtils.EMPTY;

        if(jsonObj != null){
            Set<String> keySet = jsonObj.keySet();

            Iterator<String> setStr = keySet.iterator();
            while(setStr.hasNext()){
                rtl = setStr.next();
            }
        }

        return rtl;
    }
    
    /**
     * 将json字符串转换为Map对象
     * @param jsonString
     * @return
     */
    public static Map<String, String> jsonToMap(String jsonString){
    	Map<String, String> map=new HashMap<String, String>();
    	if(null!=jsonString){
    		JSONObject json=JSON.parseObject(jsonString);
    		Iterator<String> iterator=json.keySet().iterator();
    		while(iterator.hasNext()){
    			String key=iterator.next().toString();
    			String value=json.get(key).toString();
    			map.put(key, value);
    		}
    		return map;
    	}
    	
    	return null;
    }
    
	/**
	 * 判断是否是json结构
	 */
	public static boolean isJson(String value) {
		final char[] strChar = value.substring(0, 1).toCharArray();
		final char firstChar = strChar[0];

		if (firstChar == '{') {
			return true;
		} else if (firstChar == '[') {
			return true;
		} else {
			return false;
		}
	}
}
