package cn.conac.as.framework.utils;

import java.sql.Timestamp;

/**
 * Timestamp的工具类
 * @author lishen
 * @date	2017年7月20日上午10:55:23
 * @version 1.0
 */
public class TimestampUtils {

	/**
	 * 将String类型转换为Timestamp类型，字符串格式为：yyyy-mm-dd hh:mm:ss
	 * @param str
	 * @return
	 */
	public static Timestamp valueOf(String str) {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		try {
			ts = Timestamp.valueOf(str);
			return ts;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
