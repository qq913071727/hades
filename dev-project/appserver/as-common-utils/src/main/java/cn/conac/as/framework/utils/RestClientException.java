package cn.conac.as.framework.utils;

import org.springframework.core.NestedRuntimeException;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-09-14T12:23:25.047Z")
public class RestClientException extends NestedRuntimeException {

    private int code;

    public RestClientException(int code, String msg) {
        super(msg);
        this.setCode(code);
    }

    private static final long serialVersionUID = -4084444984163796577L;

    /**
     * Construct a new instance of {@code HttpClientException} with the given
     * message.
     * @param msg the message
     */
    public RestClientException(String msg) {
        super(msg);
    }

    /**
     * Construct a new instance of {@code HttpClientException} with the given
     * message and exception.
     * @param msg the message
     * @param ex the exception
     */
    public RestClientException(String msg, Throwable ex) {
        super(msg, ex);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
