package cn.com.acca.ma.test;

import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import junit.framework.TestCase;

import org.junit.Test;

public class PropertiesTest extends TestCase{
	private static final Logger logger = Logger.getLogger(Test.class);
	
	public void testProperty6(){
		String filename="test.properties";
		InputStream in = ClassLoader.getSystemResourceAsStream(filename);
		Properties p = new Properties();
		try {
			p.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(p.getProperty("test"));
	}
	
	public void testProperty5(){
		String filename="test.properties";
		InputStream in = PropertiesTest.class.getClassLoader().getResourceAsStream(filename); 
		Properties p = new Properties();
		try {
			p.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(p.getProperty("test"));
	}
	
	public void testProperty4(){
		String filename="/test.properties";
		InputStream in = PropertiesTest.class.getResourceAsStream(filename);
		Properties p = new Properties();
		try {
			p.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(p.getProperty("test"));
	}
	
	public void testProperty3(){
		String filename=System.getProperty("user.dir")+"/src/main/resources/test.properties";
		InputStream in=null;
		ResourceBundle rb=null;
		try {
			in = new BufferedInputStream(new FileInputStream(filename));
			rb = new PropertyResourceBundle(in);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(rb.getString("test"));
	}
	
	public void testProperty2(){
		ResourceBundle rb = ResourceBundle.getBundle("test", Locale.getDefault());
		logger.info(rb.getString("test"));
	}

	public void testProperty1() {
		String filename=System.getProperty("user.dir")+"/src/main/resources/parameter.properties";
		InputStream in=null;
		try {
			in = new BufferedInputStream(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties p = new Properties();
		try {
			p.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(p.getProperty("stock.time.begin"));
	}
}
