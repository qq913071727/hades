package cn.com.acca.ma.common.util;

import junit.framework.Assert;

import org.junit.Test;

public class StringUtilTest {
	
	
	@Test
	public void testConvertDateStringByFormat() {
		Assert.assertEquals(StringUtil.convertDateStringByFormat("20140101", "-"), "2014-01-01");
	}
}
