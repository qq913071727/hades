package cn.com.acca.ma.common.util;

import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;

public class DateUtilTest {

	@Test
	public void testDateToString() {
		Assert.assertEquals(DateUtil.dateToString(new Date()), "20150128");
	}
	
	@Test
	public void stringToDate(){
		Assert.assertEquals(DateUtil.stringToDate("20150305"), new Date());
	}
	
	public static void main(String[] arg){

		System.out.println(DateUtil.fromYearAndWeekNumberToFriday(2021, 1));

//		Date date = new Date();
//		long timestamp = date.getTime();
//		System.out.println(timestamp);

//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String dateString = "2020-10-16 14:02:08";
//		try {
//			Date dateParse = sdf.parse(dateString);
////			System.out.println(dateParse.getTime());
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}

//		Calendar cal = Calendar.getInstance();
//		cal.set(Calendar.WEEK_OF_YEAR,42);//18为周数
//		Calendar cal1 = (Calendar)cal.clone();
//		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//		cal.add(Calendar.DATE,cal.getActualMinimum(Calendar.DAY_OF_WEEK)-dayOfWeek);
//		cal.add(Calendar.DATE, 1);
//		Date d = cal.getTime();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		System.out.println(sdf.format(d));
//		cal1.add(Calendar.DATE,cal1.getActualMaximum(Calendar.DAY_OF_WEEK)-dayOfWeek);
//		cal1.add(Calendar.DATE, 1);
//		System.out.println(sdf.format(cal1.getTime()));

	}
}
