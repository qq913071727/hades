package cn.com.acca.ma.test;

import lombok.extern.log4j.Log4j;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class Robot4Analysis {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String str="values (18916, to_date('28-09-2020', 'dd-mm-yyyy'), '����', 7, 259363, 101673, 361036, 4032020);";
        str=new String(str.getBytes("GBK"),"utf-8");
        System.out.println(str);

//        Robot4Analysis.StandardDeviation standardDeviation = new StandardDeviation();
//        standardDeviation.standardDeviation(StandardDeviation._2011AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2012AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2013AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2014AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2015AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2016AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2017AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2018AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2019AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2020AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2021AverageAnnualReturnRateList);
//        standardDeviation.standardDeviation(StandardDeviation._2022AverageAnnualReturnRateList);
    }

    /**
     * 计算每个算法在某一个月的标准差
     */
    static class StandardDeviation {

        public static List<Double> _2011AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2012AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2013AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2014AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2015AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2016AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2017AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2018AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2019AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2020AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2021AverageAnnualReturnRateList = new ArrayList<>();
        public static List<Double> _2022AverageAnnualReturnRateList = new ArrayList<>();

        static {
            // 2011年
            _2011AverageAnnualReturnRateList.add(52.5); // 401
            _2011AverageAnnualReturnRateList.add(14.25); // 403
            _2011AverageAnnualReturnRateList.add(55.75); // 405
            _2011AverageAnnualReturnRateList.add(49D); // 409
            _2011AverageAnnualReturnRateList.add(44.5); //433

            // 2012年
            _2012AverageAnnualReturnRateList.add(22D); // 401
            _2012AverageAnnualReturnRateList.add(14.25); // 403
            _2012AverageAnnualReturnRateList.add(27.5); // 405
            _2012AverageAnnualReturnRateList.add(12.25); // 409
            _2012AverageAnnualReturnRateList.add(10.25); //433

            // 2013年
            _2013AverageAnnualReturnRateList.add(12.75); // 401
            _2013AverageAnnualReturnRateList.add(11.25); // 403
            _2013AverageAnnualReturnRateList.add(18.5); // 405
            _2013AverageAnnualReturnRateList.add(22.25); // 409
            _2013AverageAnnualReturnRateList.add(24.25); //433

            // 2014年
            _2014AverageAnnualReturnRateList.add(-2.25); // 401
            _2014AverageAnnualReturnRateList.add(18D); // 403
            _2014AverageAnnualReturnRateList.add(-0.5); // 405
            _2014AverageAnnualReturnRateList.add(3.25); // 409
            _2014AverageAnnualReturnRateList.add(19.5); //433

            // 2015年
            _2015AverageAnnualReturnRateList.add(61.5); // 401
            _2015AverageAnnualReturnRateList.add(80.5); // 403
            _2015AverageAnnualReturnRateList.add(62.5); // 405
            _2015AverageAnnualReturnRateList.add(65D); // 409
            _2015AverageAnnualReturnRateList.add(101.25); //433

            // 2016年
            _2016AverageAnnualReturnRateList.add(4D); // 401
            _2016AverageAnnualReturnRateList.add(-4D); // 403
            _2016AverageAnnualReturnRateList.add(-0.75D); // 405
            _2016AverageAnnualReturnRateList.add(-7D); // 409
            _2016AverageAnnualReturnRateList.add(-8.75D); //433

            // 2017年
            _2017AverageAnnualReturnRateList.add(25.5); // 401
            _2017AverageAnnualReturnRateList.add(40D); // 403
            _2017AverageAnnualReturnRateList.add(40.25); // 405
            _2017AverageAnnualReturnRateList.add(22.75); // 409
            _2017AverageAnnualReturnRateList.add(17.75); //433

            // 2018年
            _2018AverageAnnualReturnRateList.add(-11.25); // 401
            _2018AverageAnnualReturnRateList.add(-9.75); // 403
            _2018AverageAnnualReturnRateList.add(-14.25); // 405
            _2018AverageAnnualReturnRateList.add(30.5); // 409
            _2018AverageAnnualReturnRateList.add(13.75); //433

            // 2019年
            _2019AverageAnnualReturnRateList.add(20D); // 401
            _2019AverageAnnualReturnRateList.add(-1D); // 403
            _2019AverageAnnualReturnRateList.add(22.5); // 405
            _2019AverageAnnualReturnRateList.add(21.5); // 409
            _2019AverageAnnualReturnRateList.add(9D); //433

            // 2020年
            _2020AverageAnnualReturnRateList.add(1.5); // 401
            _2020AverageAnnualReturnRateList.add(16.5); // 403
            _2020AverageAnnualReturnRateList.add(-8.25); // 405
            _2020AverageAnnualReturnRateList.add(7.25); // 409
            _2020AverageAnnualReturnRateList.add(22.25); //433

            // 2011年
            _2021AverageAnnualReturnRateList.add(18.75); // 401
            _2021AverageAnnualReturnRateList.add(0.75); // 403
            _2021AverageAnnualReturnRateList.add(10.5); // 405
            _2021AverageAnnualReturnRateList.add(19.5); // 409
            _2021AverageAnnualReturnRateList.add(-0.5); //433

            // 2022年
            _2022AverageAnnualReturnRateList.add(2.75); // 401
            _2022AverageAnnualReturnRateList.add(24.25); // 403
            _2022AverageAnnualReturnRateList.add(9.5); // 405
            _2022AverageAnnualReturnRateList.add(11.25); // 409
            _2022AverageAnnualReturnRateList.add(15.5); //433
        }

        /**
         * 计算各个算法的收益率在某年的标准差
         */
        public void standardDeviation(List<Double> averageAnnualReturnRateList) {
            int length = averageAnnualReturnRateList.size();
            double sum = 0D;
            for (Double averageAnnualReturnRate : averageAnnualReturnRateList){
                sum += averageAnnualReturnRate.doubleValue();
            }
            double avg = sum / length;
            double _d = 0D;
            for (Double d : averageAnnualReturnRateList){
                _d += Math.pow(d  - avg, 2);
            }
            double standardDeviation = Math.sqrt(_d / length);
            log.info("各个年份之间的标准差为：" + standardDeviation);
        }
    }


}
