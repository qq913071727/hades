package cn.com.acca.ma.test;

public class TestThread {
	private int j;
	
	public static void main(String[] args){
		TestThread tt=new TestThread();
		for(int i=0;i<2;i++){
			new Thread(tt.new AddClass()).start();
			new Thread(tt.new SubtractClass()).start();
		}
	}
	
	public synchronized void add(){
		j++;
		System.out.println("thread name is "+Thread.currentThread().getName()+". j++ "+j);
	}
	
	public synchronized void subtract(){
		j--;
		System.out.println("thread name is "+Thread.currentThread().getName()+". j-- "+j);
	}
	
	class AddClass implements Runnable{
		public void run() {
			add();
		}
	}
	
	class SubtractClass implements Runnable{
		public void run() {
			subtract();
		}
	}
}
