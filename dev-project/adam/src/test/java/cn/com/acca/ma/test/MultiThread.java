package cn.com.acca.ma.test;

class MultiThread{
	public static void main(String[] args){
	   	MyThread mt=new MyThread();
	   	mt.getThread().start();
	   	mt.getThread().start();
	   	mt.getThread().start();
	   	mt.getThread().start();
	   	while(true){
	    	System.out.println("main："+Thread.currentThread().getName());
	   	}
	}
}

class MyThread{
	int index=0;
	private class InnerThread extends Thread{
	   	public void run(){
	    	while(true){
	     		System.out.println(Thread.currentThread().getName()+":"+index++);
	    	}
	   	}
	}

	Thread getThread(){
	   	return new InnerThread();
	}
}
