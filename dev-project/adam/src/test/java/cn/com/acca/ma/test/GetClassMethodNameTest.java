package cn.com.acca.ma.test;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

public class GetClassMethodNameTest extends TestCase {
	private static final Logger logger = Logger.getLogger(GetClassMethodNameTest.class);

	public void testGetClassName() {
		String className=this.getClass().getName();
		logger.info(className);
	}

	public void testGetFunctionName() {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        logger.info(methodName);
    } 
}
