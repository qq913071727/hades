package cn.com.acca.ma.common.util;

import junit.framework.Assert;

import org.junit.Test;

public class PropertiesUtilTest {
	private static final String DIR = System.getProperty("user.dir");
	
	public static void main(String[] args){
		PropertiesUtil.setValue(DIR + "/src/main/resources/test.properties","stockRecord.time.begin","123");
		String str1=PropertiesUtil.getValue(DIR + "/src/main/resources/test.properties","stockRecord.time.begin");
		System.out.println(str1);
	}

	@Test
	public void testGetValue() {
		Assert.assertEquals(PropertiesUtil.getValue(DIR
				+ "/src/main/resources/test.properties",
				"stockRecord.time.begin"), "20140701");
	}
}
