package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Robot6EtfFilterDao;
import cn.com.acca.ma.model.Robot6StockTransactRecord;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Robot6EtfFilterDaoImpl extends BaseDaoImpl<Robot6EtfFilterDao> implements
        Robot6EtfFilterDao {

    public Robot6EtfFilterDaoImpl() {
        super();
    }

    /**
     * 清空robot6_stock_filter表
     */
    @Override
    public void truncateTableRobotEtfFilter() {
        logger.info("清空robot6_stock_filter表");

        String sql = "truncate table robot6_stock_filter";

        doSQLInTransaction(sql);
    }

    /**
     * 插入记录
     * @param list
     */
    @Override
    public void insertEtfRobot6EtfFilter(List<String> list) {
        logger.info("插入记录");

        if (null != list && list.size() > 0 ){
            StringBuffer stringBuffer = new StringBuffer("insert into robot6_stock_filter(stock_code) values");
            for (int i=0; i<list.size(); i++){
                stringBuffer.append("('" + list.get(i) + "'),");

            }
            String sql = stringBuffer.toString();
            doSQLInTransaction(sql.substring(0, sql.length() - 1));
        }
    }

    /**
     * 根据etf代码列表，向robot6_stock_filter表中插入etf代码，正在持有的etf不插入
     * 用于做多
     *
     * @param codeList
     */
    @Override
    public void insertBullEtfCodeFromEtfTransactionDataByCodeList(List<String> codeList) {
        logger.info("根据etf代码列表，向robot6_stock_filter表中插入etf代码，正在持有的etf不插入");

        if (null != codeList && codeList.size() > 0 ) {
            for (int i=0; i<codeList.size(); i++) {
                String selectSql = "select * from robot6_stock_transact_record t where t.stock_code=:etfCode and t.sell_date is null and t.sell_price is null and t.sell_amount is null";
                Map<String, Object> map = new HashMap();
                map.put("etfCode", codeList.get(i));
                List<Robot6StockTransactRecord> robot6StockTransactRecordList = doSQLQueryInTransaction(selectSql, map, Robot6StockTransactRecord.class);

                if (robot6StockTransactRecordList.size() == 0){
                    String insertSql = "insert into robot6_stock_filter(stock_code) values('" + codeList.get(i) + "')";
                    doSQLInTransaction(insertSql);
                }
            }
        }
    }

    /**
     * 过滤条件：MA不单调递减，过滤etf
     * 用于做多
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    @Override
    public void filterByMANotDecreasing(String date, Integer maLevel, String dateNumber) {
        logger.info("过滤条件：MA不单调递减，过滤etf。date为【" + date + "】，maLevel为【" + maLevel + "】,"
                + "dateNumber为【" + dateNumber + "】");

        String sql = "{call PKG_ROBOT6.filter_by_ma_not_decreasing(" + maLevel + ", '" + date + "', " + dateNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 只保留这个价格区间以内的etf
     *
     * @param date
     * @param closePriceStart
     * @param closePriceEnd
     */
    @Override
    public void filterByLessThanClosePrice(String date, Double closePriceStart, Double closePriceEnd) {
        logger.info("收盘价在【" + closePriceStart + "】和【" + closePriceEnd + "】价格区间以外的etf都被删除");

        String sql = "delete from robot6_stock_filter where stock_code not in("
                + "select t.code_ from etf_transaction_data t "
                + "where t.date_=to_date('" + date + "','yyyy-mm-dd') and t.close_price between " + closePriceStart + " and " + closePriceEnd + ")";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除收盘价小于250均线的ETF
     * @param date
     */
    @Override
    public void filterClosePriceLessThanMA(String date) {
        logger.info("日期为【" + date + "】，删除收盘价小于250均线的ETF");

        String sql = "delete from robot6_stock_filter where stock_code in("
                + "select t.code_ from etf_transaction_data t "
                + "where t.date_=to_date('" + date + "','yyyy-mm-dd') and t.close_price < t.ma20)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除除过权的etf
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public void filterByXr(String beginDate, String endDate) {
        logger.info("过滤条件：删除除过权的etf，时间从【" + beginDate + "】至【" + endDate + "】");

        String sql = "{call PKG_ROBOT6.filter_by_xr('" + beginDate + "', '" + endDate + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：从robot6_stock_filter表中删除那些不是交易日的记录
     * @param date
     * @param mdlEtfType
     */
    @Override
    public void filterByBuyDate(String date, Integer mdlEtfType) {
        logger.info("过滤条件：从robot6_stock_filter表中删除那些不是交易日【" + date + "】，type不是【" + mdlEtfType + "】的记录");

        String sql = "{call PKG_ROBOT6.filter_by_buy_date('" + date + "', " + mdlEtfType + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：更新robot6_stock_filter表：direction为1；filter_type为交易算法；accumulative_profit_rate累计收益。单位：元
     * @param date
     */
    @Override
    public void updateRobotEtfFilter(String date) {
        logger.info("过滤条件：更新robot6_stock_filter表：direction为1；filter_type为交易算法；accumulative_profit_rate累计收益。单位：元");

        String sql = "{call PKG_ROBOT6.update_robot_stock_filter('" + date + "')}";

        doSQLInTransaction(sql);
    }
}
