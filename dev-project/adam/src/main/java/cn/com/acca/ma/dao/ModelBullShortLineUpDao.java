package cn.com.acca.ma.dao;

public interface ModelBullShortLineUpDao extends BaseDao {

    /**
     * 海量地向表MDL_BULL_SHORT_LINE_UP中写入数据
     */
    void writeModelBullShortLineUp();

    /**
     * 增量地向表MDL_BULL_SHORT_LINE_UP中写入数据
     * @param date
     */
    void writeModelBullShortLineUpIncr(String date);
}
