package cn.com.acca.ma.service;

public interface ModelStockIndexKDGoldCrossService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用KD金叉模型算法，向表MDL_STOCK_INDEX_KD_GOLD_CROSS插入数据
     */
    void writeModelStockIndexKDGoldCross();
}
