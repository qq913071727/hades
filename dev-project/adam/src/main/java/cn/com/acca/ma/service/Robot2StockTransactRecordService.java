package cn.com.acca.ma.service;

public interface Robot2StockTransactRecordService extends BaseService {

    /**
     * 执行某段时间内的股票买卖交易（平均金叉死叉）
     */
    void doBuyAndSellByBeginDateAndEndDate_averageGoldCrossDeadCross();

    /**
     * 卖股票/买股票
     */
    void sellOrBuy();

    /**
     * 买股票/卖股票
     */
    void buyOrSell();

    /**
     * 打印买卖建议
     */
    void printBuySellSuggestion();
}
