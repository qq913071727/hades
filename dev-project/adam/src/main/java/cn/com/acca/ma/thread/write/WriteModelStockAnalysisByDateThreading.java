package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.ModelStockAnalysisService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 计算model_stock_analysis表的某一日的数据
 */
public class WriteModelStockAnalysisByDateThreading extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteModelStockAnalysisByDateThreading.class);

    private ModelStockAnalysisService modelStockAnalysisService;

    public WriteModelStockAnalysisByDateThreading() {
    }

    public WriteModelStockAnalysisByDateThreading(
        ModelStockAnalysisService modelStockAnalysisService) {
        this.modelStockAnalysisService = modelStockAnalysisService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始计算model_stock_analysis表的某一日的数据，"
            + "调用的方法为【writeModelStockAnalysisByDate】");

        modelStockAnalysisService.writeModelStockAnalysisByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
