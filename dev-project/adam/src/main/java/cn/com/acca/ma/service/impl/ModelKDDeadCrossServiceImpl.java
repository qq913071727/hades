package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelKDDeadCross;
import cn.com.acca.ma.model.ModelWeekKDGoldCross;
import cn.com.acca.ma.service.ModelKDDeadCrossService;
import cn.com.acca.ma.service.ModelKDGoldCrossService;

import java.util.List;

public class ModelKDDeadCrossServiceImpl extends BaseServiceImpl<ModelKDDeadCrossServiceImpl, ModelKDDeadCross> implements
        ModelKDDeadCrossService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据日期endDate，使用KD死叉模型算法，向表MDL_KD_DEAD_CROSS插入数据
     */
    public void writeModelKDDeadCrossIncr() {
        String endDate = PropertiesUtil
                .getValue(MODEL_KD_DEAD_CROSS_PROPERTIES, "kd.dead.cross.end_date");
        modelKDDeadCrossDao.writeModelKDDeadCrossIncr(endDate);
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用周线级别KD死叉模型算法，向表MDL_KD_DEAD_CROSS插入数据
     */
    public void writeModelKDDeadCross() {
        modelKDDeadCrossDao.writeModelKDDeadCross();
    }

    @Override
    public String listToString(List list) {
        // TODO Auto-generated method stub
        return null;
    }
}
