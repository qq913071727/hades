package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 周线级别平均ma5和close_price
 */
@Data
public class StockWeekAverageClosePriceAndMa5 {

    /**
     * 平均MA5
     */
    private BigDecimal averageMA5;

    /**
     * 平均close_price
     */
    private BigDecimal averageClosePrice;
}
