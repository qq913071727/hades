package cn.com.acca.ma.constant;

/**
 * 仓位控制
 */
public class ShippingSpaceControl {

    /**
     * 不控制仓位
     */
    public final static Integer NO = 0;

    /**
     * 控制仓位（成功率）
     */
    public final static Integer YES_SUCCESS_RATE = 1;

    /**
     * 控制仓位（百分比）
     */
    public final static Integer YES_PERCENTAGE = 2;


}
