package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MDL_WEEK_BOLL_H_K_A_UP_DOWN")
public class ModelWeekBollHeiKinAshiUpDown implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "STOCK_CODE")
    private String stockCode;

    @Column(name = "BUY_DATE")
    private Date buyDate;

    @Column(name = "BUY_PRICE")
    private Double buyPrice;

    @Column(name = "ACCUMULATIVE_PROFIT_LOSS")
    private Double accumulativeProfitLoss;

    @Column(name = "PROFIT_LOSS")
    private Double profitLoss;

    @Column(name = "SELL_DATE")
    private Date sellDate;

    @Column(name = "SELL_PRICE")
    private Double sellPrice;
}
