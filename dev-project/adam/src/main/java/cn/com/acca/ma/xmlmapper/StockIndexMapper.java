package cn.com.acca.ma.xmlmapper;//cn.com.acca.ma.xmlmapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.acca.ma.model.StockIndex;

public interface StockIndexMapper {
	/***************************************** 更新每天的数据 ***********************************************************/
	void deleteStockIndexMyBatis();
	void writeStockIndexMyBatis();

	/**
	 * 根据日期date，计算某一日所有指数的移动平均线数据
	 * @param date
	 */
	void writeStockIndexMAByDate(String date);

	/**
	 * 根据日期date，计算某一日所有指数的Hei Kin Ashi数据
	 * @param date
	 */
	void writeStockIndexHeiKinAshiByDate(String date);

	/**
	 * 根据日期date，计算所有指数某一日的乖离率
	 * @param date
	 */
	void writeStockIndexBiasByDate(String date);

	/**
	 * 计算所有指数某一天的MACD（ema）
	 * @param date
	 */
	void writeStockIndexMACDEmaByDate(String date);

	/**
	 * 计算所有指数某一天的MACD（dif）
	 * @param date
	 */
	void writeStockIndexMACDDifByDate(String date);

	/**
	 * 计算所有指数某一天的MACD（dea）
	 * @param date
	 */
	void writeStockIndexMACDDeaByDate(String date);

	/**
	 * 计算所有指数某一天的MACD（macd）
	 * @param date
	 */
	void writeStockIndexMACDByDate(String date);

	/**
	 * 计算所有指数某一天的KD（rsv）
	 * @param date
	 */
	void writeStockIndexKDRsvByDate(String date);

	/**
	 * 计算所有指数某一天的KD（k）
	 * @param date
	 */
	void writeStockIndexKDKByDate(String date);

	/**
	 * 计算所有指数某一天的KD（d）
	 * @param date
	 */
	void writeStockIndexKDDByDate(String date);
	
	/**************************************** 根据每日数据更新图表 **********************************************************/
	/**
	 * 为创建28分化图表中的线图，获取数据
	 * @param map
	 * @return
	 */
	List<StockIndex> getDataForLineChart(Map<String,String> map);
	
	/**
	 * 为创建28分化图表中的柱状图，获取日期数据
	 * @param map
	 * @return
	 */
	List<Date> getDateForBarChart(Map<String,String> map);
	
	/**
	 * 根据开始时间和结束时间，在STOCK_INDEX表中查找所有INDEX_DATE字段
	 * @param map
	 * @return
	 */
	List<Date> getDateByCondition(Map<String,String> map);
	
	/**
	 * 根据日期，获取StockIndex对象
	 * @param map
	 * @return
	 */
	List<StockIndex> getStockIndexByDate(Map<String,String> map);
	
	/**
	 * 保存StockIndex对象
	 * @param stockIndex
	 */
	void saveStockIndexMyBatis(StockIndex stockIndex);

	/**
	 * 在开始时间和结束时间之间，查找某个指数的最大值和最小值
	 * @param map
	 * @return
	 */
	List getMaxMinClosePriceBetween(Map<String,String> map);
	
	/******************************************** 更新全部的数据 ********************************************************/
	/**
	 * 计算5日均线
	 */
	void writeStockIndexMA5();

	/**
	 * 计算10日均线
	 */
	void writeStockIndexMA10();

	/**
	 * 计算20日均线
	 */
	void writeStockIndexMA20();

	/**
	 * 计算60日均线
	 */
	void writeStockIndexMA60();

	/**
	 * 计算120日均线
	 */
	void writeStockIndexMA120();

	/**
	 * 计算250日均线
	 */
	void writeStockIndexMA250();

	/**
	 * 计算Hei Kin Ashi相关的字段
	 */
	void writeStockIndexHeiKinAshi();

	/**
	 * 计算所有指数的乖离率
	 */
	void writeStockIndexBias();

	/**
	 * 计算所有指数的MACD（初始化）
	 */
	void writeStockIndexMACDInit();

	/**
	 * 计算所有指数的MACD（ema）
	 */
	void writeStockIndexMACDEma();

	/**
	 * 计算所有指数的MACD（dif）
	 */
	void writeStockIndexMACDDif();

	/**
	 * 计算所有指数的MACD（dea）
	 */
	void writeStockIndexMACDDea();

	/**
	 * 计算所有指数的MACD（macd）
	 */
	void writeStockIndexMACD();

	/**
	 * 计算所有指数的KD（init）
	 */
	void writeStockIndexKDInit();

	/**
	 * 计算所有指数的KD（rsv）
	 */
	void writeStockIndexKDRsv();

	/**
	 * 计算所有指数的KD（k）
	 */
	void writeStockIndexKDK();

	/**
	 * 计算所有指数的KD（d）
	 */
	void writeStockIndexKDD();
}
