package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelKDDeadCrossDao;
import cn.com.acca.ma.hibernate.impl.GetKdDeadCrossSuccessRateWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelKDDeadCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelKDDeadCrossDaoImpl extends BaseDaoImpl<ModelKDDeadCrossDaoImpl> implements
        ModelKDDeadCrossDao {

    public ModelKDDeadCrossDaoImpl() {
        super();
    }

    /**
     * 根据日期endDate，使用KD金叉模型算法，向表MDL_KD_DEAD_CROSS插入数据
     * @param endDate
     */
    @Override
    public void writeModelKDDeadCrossIncr(String endDate) {
        logger.info("根据日期【" + endDate + "】，使用KD死叉模型算法，向表MDL_KD_DEAD_CROSS插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_KD_DEAD_CROSS_INCR(?)}");
        query.setString(0, endDate);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("日线级别KD死叉算法结束");
    }

    /**
     * 使用周线级别KD金叉模型算法，向表MDL_KD_DEAD_CROSS插入数据
     */
    public void writeModelKDDeadCross() {
        logger.info("开始日线级别KD死叉算法");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_KD_DEAD_CROSS()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("日线级别KD死叉算法结束");
    }

    /**
     * 获取kd死叉在开始时间到结束时间内成功率的数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<SuccessRateOrPercentagePojo> getKdDeadCrossSuccessRate(boolean multithreading, final String beginDate, final String endDate, Integer dateNumber){
        logger.info("获取开始时间【" + beginDate + "】至【" + endDate + "】内，最近【" + dateNumber + "】天，kd死叉的成功率");

        final List<SuccessRateOrPercentagePojo> list=new ArrayList<SuccessRateOrPercentagePojo>();

        if (multithreading) {
            Session newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            newSession.doWork(new GetKdDeadCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session=HibernateUtil.currentSession();
            session.beginTransaction();
            session.doWork(new GetKdDeadCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            session.getTransaction().commit();
            session.close();
        }

        logger.info("获取一段时间内，最近" + dateNumber + "天，kd死叉的成功率完成");
        return list;
    }

    /**
     * 获取一段时间内，kd死叉的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SuccessRateOrPercentagePojo> getKdDeadCrossPercentage(boolean multithreading, String beginDate, String endDate){
        logger.info("获取一段时间内【" + beginDate + "】至【" + endDate + "】，kd死叉的百分比");

        String hql = "select t.date_ transactionDate, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_ " +
                "and t1.k<t1.d)/ " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 successRateOrPercentage " +
                "from stock_transaction_data_all t " +
                "where t.date_ between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd') " +
                "group by t.date_ order by t.date_ asc";

        Session newSession = null;
        Query query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        }

        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<SuccessRateOrPercentagePojo> list = query.list();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        return list;
    }

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_KD_DEAD_CROSS中获取buy_date字段
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Date> getDateByCondition(String beginDate, String endDate){
        logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
                + "从表MDL_KD_DEAD_CROSS中获取buy_date字段");

        StringBuffer hql = new StringBuffer("select distinct t.buyDate from ModelKDDeadCross t " +
                "where t.buyDate between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd')");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<Date> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 从表MDL_KD_DEAD_CROSS中获取date日的所有ModelKDDeadCross对象
     * @param date
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<ModelKDDeadCross> getModelKDDeadCrossByDate(String date){
        logger.info("从表MDL_KD_DEAD_CROSS中获取日期【" + date + "】的所有ModelKDDeadCross对象");

        StringBuffer hql = new StringBuffer("select t from ModelKDDeadCross t " +
                "where t.buyDate=to_date(?,'yyyy-mm-dd') order by t.buyDate asc");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, date);
        List<ModelKDDeadCross> list=query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 向表MDL_KD_DEAD_CROSS中插入ModelKDDeadCross对象
     * @param modelKDDeadCross
     */
    public void save(ModelKDDeadCross modelKDDeadCross){
        logger.info("向表MDL_KD_DEAD_CROSS中插入ModelKDDeadCross对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.persist(modelKDDeadCross);
        session.getTransaction().commit();
        session.close();
    }
}
