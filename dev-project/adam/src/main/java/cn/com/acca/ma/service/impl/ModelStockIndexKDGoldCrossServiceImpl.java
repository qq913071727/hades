package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.ModelStockIndexKDGoldCross;
import cn.com.acca.ma.service.ModelStockIndexKDGoldCrossService;

import java.util.List;

public class ModelStockIndexKDGoldCrossServiceImpl extends BaseServiceImpl<ModelStockIndexKDGoldCrossServiceImpl, ModelStockIndexKDGoldCross> implements
        ModelStockIndexKDGoldCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用KD金叉模型算法，向表MDL_STOCK_INDEX_KD_GOLD_CROSS插入数据
     */
    @Override
    public void writeModelStockIndexKDGoldCross() {
        modelStockIndexKDGoldCrossDao.writeModelStockIndexKDGoldCross();
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
