package cn.com.acca.ma.service;

import java.util.List;

public interface ModelStockAnalysisService extends BaseService {

    /**
     * 计算model_stock_analysis表的全部数据
     */
    void writeModelStockAnalysis();

    /**
     * 计算model_stock_analysis表的某一日的数据
     * @param multithreading
     */
    void writeModelStockAnalysisByDate(boolean multithreading);

    /**
     * 创建日线级别标准差和平均收盘价的折线图
     */
    void createModelStockAnalysisStandardDeviationPictureAndAverageClosePrice();


}
