package cn.com.acca.ma.dao;

import java.util.HashMap;
import java.util.List;

public interface ModelSubNewStockDao extends BaseDao {

	/**
	 * 计算次新股收盘价的平均值
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List<HashMap> getSubNewStockAverageClose(boolean multithreading, final String beginDate, final String endDate);

	/*********************************************************************************************************************
	 *
	 * 												   	创建报告
	 *
	 *********************************************************************************************************************/
	/**
	 * 将视图V_SUB_NEW_STOCK中的数据打印出来
	 */
	void printSubNewStock();
}
