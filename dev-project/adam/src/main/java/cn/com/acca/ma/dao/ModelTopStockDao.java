package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelMACDGoldCross;
import java.util.Date;
import java.util.List;

import cn.com.acca.ma.model.ModelTopStock;

public interface ModelTopStockDao extends BaseDao {

	/**
	 * 根据开始日期和结束日期，获取ModelTopStock对象
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<ModelTopStock> getModelTopStocks(boolean multithreading, String beginDate, String endDate);

	/**
	 * 按照日期，向表MDL_TOP_STOCK中写入数据
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param numberPercentage
	 */
	void writeModelTopStock(boolean multithreading, String beginDate, String endDate, Double numberPercentage);

	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_TOP_STOCK中获取DATE_字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<Date> getDateByCondition(String beginDate, String endDate);

	/**
	 * 从表MDL_TOP_STOCK中获取date日的所有ModelTopStock对象
	 * @param date
	 * @return
	 */
	List<ModelTopStock> getModelTopStockByDate(String date);

	/**
	 * 向表MDL_TOP_STOCK中插入ModelTopStock对象
	 * @param modelTopStock
	 */
	void save(ModelTopStock modelTopStock);
}
