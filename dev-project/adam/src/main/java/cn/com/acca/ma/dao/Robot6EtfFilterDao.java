package cn.com.acca.ma.dao;

import java.util.List;

public interface Robot6EtfFilterDao extends BaseDao {

    /**
     * 清空robot6_stock_filter表
     */
    void truncateTableRobotEtfFilter();

    /**
     * 插入记录
     * @param list
     */
    void insertEtfRobot6EtfFilter(List<String> list);

    /**
     * 根据etf代码列表，向robot6_stock_filter表中插入etf代码，正在持有的etf不插入
     * 用于做多
     * @param codeList
     */
    void insertBullEtfCodeFromEtfTransactionDataByCodeList(List<String> codeList);

    /**
     * 过滤条件：MA不单调递减，过滤etf
     * 用于做多
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    void filterByMANotDecreasing(String date, Integer maLevel, String dateNumber);

    /**
     * 只保留这个价格区间以内的etf
     * @param date
     * @param closePriceStart
     * @param closePriceEnd
     */
    void filterByLessThanClosePrice(String date, Double closePriceStart, Double closePriceEnd);

    /**
     * 过滤条件：删除收盘价小于250均线的ETF
     * @param date
     */
    void filterClosePriceLessThanMA(String date);

    /**
     * 过滤条件：删除除过权的etf
     * @param beginDate
     * @param endDate
     */
    void filterByXr(String beginDate, String endDate);

    /**
     * 过滤条件：从robot6_stock_filter表中删除那些不是交易日的记录
     * @param date
     * @param mdlEtfType
     */
    void filterByBuyDate(String date, Integer mdlEtfType);

    /**
     * 过滤条件：更新robot6_stock_filter表：direction为1；filter_type为交易算法；accumulative_profit_rate累计收益。单位：元
     * @param date
     */
    void updateRobotEtfFilter(String date);
}
