package cn.com.acca.ma.service;

public interface Robot4StockTransactRecordService extends BaseService {

    /**
     * 执行某段时间内的股票买卖交易（周线级金叉死叉结合日线级别买卖信号）
     */
    void doBuyAndSellByBeginDateAndEndDate_week();

    /**
     * 卖股票/买股票
     */
    void sellOrBuy();

    /**
     * 买股票/卖股票
     */
    void buyOrSell();

    /**
     * 打印买卖建议
     */
    void printBuySellSuggestion();
}
