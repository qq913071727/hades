package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 机器人--账户（历史数据）
 */
@Data
public class HistoryRobotAccount {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 机器人名字
     */
    private String robotName;

    /**
     * 持股数量
     */
    private Integer holdStockNumber;

    /**
     * 股票资产
     */
    private BigDecimal stockAssets;

    /**
     * 资金资产
     */
    private BigDecimal capitalAssets;

    /**
     * 总资产
     */
    private BigDecimal totalAssets;

    /**
     * 模型id
     */
    private Integer modelId;

}
