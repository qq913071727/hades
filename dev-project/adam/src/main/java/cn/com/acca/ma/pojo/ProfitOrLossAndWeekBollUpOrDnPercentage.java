package cn.com.acca.ma.pojo;

import lombok.Data;

@Data
public class ProfitOrLossAndWeekBollUpOrDnPercentage {

    /**
     * 收益率
     */
    private Double profitOrLoss;

    /**
     * 百分比，类似：(最高价-布林带上轨)/布林带上轨
     */
    private Double weekBollUpOrDnPercentage;
}
