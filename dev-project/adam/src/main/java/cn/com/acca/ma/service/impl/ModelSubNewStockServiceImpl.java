package cn.com.acca.ma.service.impl;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.service.ModelSubNewStockService;

public class ModelSubNewStockServiceImpl extends BaseServiceImpl<ModelSubNewStockServiceImpl, Object> implements ModelSubNewStockService {

	/**
	 * 创建次新股收盘价平均值图
	 * @param multithreading
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public void createSubNewStockAverageClosePicture(boolean multithreading) {
		String beginDate=PropertiesUtil.getValue(MODEL_SUB_NEW_STOCK_PICTURE_PROPERTIES, "sub.new.stock.begin_date");
		String endDate=PropertiesUtil.getValue(MODEL_SUB_NEW_STOCK_PICTURE_PROPERTIES, "sub.new.stock.end_date");
		List modelSubNewStocklist = modelSubNewStockDao.getSubNewStockAverageClose(multithreading, beginDate, endDate);
		
		JFreeChart jfreechart;
		
		TimeSeries modelSubNewStockSeries = new TimeSeries("Sub New Stock Average Close",org.jfree.data.time.Day.class);
		TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();

		/* 
		 * dateList集合中包含的是从开始日期至结束日期的所有交易日，bullRankNumberList集合和shortOrderNumberList集合中包含的是这段时间中
		 * 每个交易日的多头排列和空头排列的数量，但是如果是0，则没有记录。因此dateList，bullRankNumberList和shortOrderNumberList这三个
		 * 集合的大小可能是不一样的。下面两个for循环先判断dateList和bullRankNumberList或hortOrderNumberList的日期是否一样，如果不一样，
		 * 则在dateList对应的日期上添加0，从而解决了这个问题。
		 */
		for (int i = 0; i < modelSubNewStocklist.size();i++) {
			HashMap hashMap=(HashMap) modelSubNewStocklist.get(i);
			Date subNewStockDate = (Date) hashMap.get("subNewStockDate");
			int year = DateUtil.getYearFromDate(subNewStockDate);
			int month = DateUtil.getMonthFromDate(subNewStockDate);
			int day = DateUtil.getDateFromDate(subNewStockDate);
			BigDecimal subNewStockAvgCLose=(BigDecimal) hashMap.get("subNewStockAvgCLose");
			modelSubNewStockSeries.add(new Day(day, month, year),subNewStockAvgCLose);
		}
		
		timeSeriesCollection.addSeries(modelSubNewStockSeries);

		jfreechart = ChartFactory.createTimeSeriesChart("Sub New Stock Average Close", 
				"Date","Average Close", timeSeriesCollection, true, true, true);
		jfreechart.setBackgroundPaint(Color.white);
		XYPlot xyplot = (XYPlot) jfreechart.getPlot();
		xyplot.setBackgroundPaint(Color.lightGray);
		xyplot.setDomainGridlinePaint(Color.white);
		xyplot.setRangeGridlinePaint(Color.white);
		xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
		xyplot.setDomainCrosshairVisible(true);
		xyplot.setRangeCrosshairVisible(true);
		org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
		if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyitemrenderer;
			xylineandshaperenderer.setBaseShapesVisible(true);
			xylineandshaperenderer.setBaseShapesFilled(true);
		}
		DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
		dateaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

		try {
			FileOutputStream fos = new FileOutputStream(PICTURE_PATH+"sub_new_stock/"+beginDate+"-"+endDate+PICTURE_FORMAT);
			// 将统计图标输出成JPG文件
			ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
					1, // JPEG图片的质量，0~1之间
					jfreechart, // 统计图标对象
					imageWidth, // 宽
					IMAGE_HEIGHT, // 高
					null // ChartRenderingInfo 信息
					);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*********************************************************************************************************************
	 *
	 * 												   	创建报告
	 *
	 *********************************************************************************************************************/
	/**
	 * 将视图V_SUB_NEW_STOCK中的数据打印出来
	 */
	public void printSubNewStock() {
		modelSubNewStockDao.printSubNewStock();
	}

	public double averageCloseAfterAdjust(double original, double max,
			double min) {
		// TODO Auto-generated method stub
		return 0;
	}

	public double adjustData(double maxClose, double minClose,
			double maxDifDea, double minDifDea, double currentClose) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String listToString(List list) {
		// TODO Auto-generated method stub
		return null;
	}

}
