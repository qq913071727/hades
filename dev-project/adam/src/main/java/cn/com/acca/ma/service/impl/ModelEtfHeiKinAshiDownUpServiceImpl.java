package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelEtfHeiKinAshiDownUp;
import cn.com.acca.ma.model.ModelStockIndexHeiKinAshiDownUp;
import cn.com.acca.ma.service.ModelEtfHeiKinAshiDownUpService;
import cn.com.acca.ma.service.ModelStockIndexHeiKinAshiDownUpService;

import java.util.List;

public class ModelEtfHeiKinAshiDownUpServiceImpl extends BaseServiceImpl<ModelEtfHeiKinAshiDownUpServiceImpl, ModelEtfHeiKinAshiDownUp> implements
        ModelEtfHeiKinAshiDownUpService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表mdl_etf_hei_kin_ashi_down_up插入数据
     */
    @Override
    public void writeModelEtfHeiKinAshiDownUp() {
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 0, "19970101", "20221231", 1);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 1, "19970101", "20221231", 2);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 0, "19970101", "20221231", 3);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 1, "19970101", "20221231", 4);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 0, "20110101", "20221231", 5);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 1, "20110101", "20221231", 6);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 0, "20110101", "20221231", 7);
        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 1, "20110101", "20221231", 8);
    }

    /*********************************************************************************************************************
     *
     * 												计算增量数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表mdl_etf_hei_kin_ashi_down_up插入数据
     */
    @Override
    public void writeModelEtfHeiKinAshiDownUpByDate() {
        String endDate = PropertiesUtil.getValue(MDL_ETF_HEI_KIN_ASHI_DOWN_UP_PROPERTIES, "etf.hei.kin.ashi.down.up.end_date");

        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 0, "19970101", endDate, 1);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 1, "19970101", endDate, 2);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 0, "19970101", endDate, 3);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 1, "19970101", endDate, 4);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 0, "20110101", endDate, 5);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 1, "20110101", endDate, 6);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(1, 0, "20110101", endDate, 7);
//        modelEtfHeiKinAshiDownUpDao.writeModelEtfHeiKinAshiDownUp(0, 1, "20110101", endDate, 8);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
