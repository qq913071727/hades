package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.HistoryRobotAccountLog;

import java.util.List;

public interface HistoryRobotAccountLogDao extends BaseDao {

    /**
     * 根据开始日期和结束日期、账号名、模型id查找HistoryRobotAccountLog类型对象列表，并按照date的升序排列
     * @param beginDate
     * @param endDate
     * @param robotName
     * @param modelId
     * @param initAssets
     * @return
     */
    List<HistoryRobotAccountLog> findByDateBetweenAndRobotNameAndModelIdOrderByDateAsc(String beginDate, String endDate, String robotName, Integer modelId, Integer initAssets);
}
