package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.ModelStockIndexClosePriceMA5GoldCross;
import cn.com.acca.ma.service.ModelStockIndexClosePriceMA5GoldCrossService;

import java.util.List;

public class ModelStockIndexClosePriceMA5GoldCrossServiceImpl extends BaseServiceImpl<ModelStockIndexClosePriceMA5GoldCrossServiceImpl, ModelStockIndexClosePriceMA5GoldCross> implements
        ModelStockIndexClosePriceMA5GoldCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据
     */
    @Override
    public void writeModelStockIndexClosePriceMA5GoldCross() {
        modelStockIndexClosePriceMA5GoldCrossDao.writeModelStockIndexClosePriceMA5GoldCross();
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
