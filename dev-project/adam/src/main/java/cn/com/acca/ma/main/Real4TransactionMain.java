package cn.com.acca.ma.main;

import lombok.extern.log4j.Log4j;

@Log4j
public class Real4TransactionMain extends AbstractMain {

    public static void main(String[] args) {
        log.info("Real4TransactionMain程序开始运行......");

        /************************ 设置代理服务器的IP和port ********************************/
        setProxyProperties();

        /****************************** 收集数据库统计信息 ********************************/
//		gatherDatabaseStatistics();

        /************************** 预先计算股票的买卖条件 ********************************/
//        preCalculateTransactionCondition_real4();

        /************* 定时地判断买卖条件，给出交易建议，并保存在文件中，发送邮件 *****************/
        realTimeJudgeConditionAndGiveSuggestionAndSendEMail_real4();

        log.info("Real4TransactionMain程序执行完毕。");
    }
}
