package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AverageClosePriceAndMaBullShortLineByBias {

    private String date_;
    private BigDecimal averageClosePrice;
    private BigDecimal averageMa5;
    private BigDecimal averageMa10;
    private BigDecimal averageMa20;
    private BigDecimal averageMa60;
    private BigDecimal averageMa120;
    private BigDecimal averageMa250;
    private BigDecimal averageBias5;
    private BigDecimal averageBias10;
    private BigDecimal averageBias20;
    private BigDecimal averageBias60;
    private BigDecimal averageBias120;
    private BigDecimal averageBias250;
}
