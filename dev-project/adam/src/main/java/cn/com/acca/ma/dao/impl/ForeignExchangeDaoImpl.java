package cn.com.acca.ma.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.acca.ma.common.util.StringUtil;
import cn.com.acca.ma.dao.ForeignExchangeDao;
import cn.com.acca.ma.model.ForeignExchange;
import cn.com.acca.ma.mybatis.util.MyBatisUtil;
import cn.com.acca.ma.xmlmapper.ForeignExchangeMapper;

public class ForeignExchangeDaoImpl extends BaseDaoImpl<ForeignExchangeDaoImpl> implements ForeignExchangeDao {

	/**
	 * 向FOREIGN_EXCHANGE表中插入数据
	 * @param foreignExchange
	 */
	public void insertForeignExchange(ForeignExchange foreignExchange) {
		logger.info("method insertForeignExchange begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("name", StringUtil.removeBracket(foreignExchange.getName().trim()));
		param.put("description", foreignExchange.getDescription().trim());
		sqlSession.insert("cn.com.acca.ma.xmlmapper.ForeignExchangeMapper.insertForeignExchange",param);
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method insertForeignExchange finish");
	}

	/**
	 * 获取所有ForeignExchange对象
	 * @return
	 */
	public List<ForeignExchange> getAllForeignExchange(){
		logger.info("method getAllForeignExchange begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		List<ForeignExchange> foreignExchangeList=sqlSession.selectList("cn.com.acca.ma.xmlmapper.ForeignExchangeMapper.getAllForeignExchange");
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method getAllForeignExchange finish");
		return foreignExchangeList;
	}
}
