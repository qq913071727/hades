package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Board2Index;

import java.util.List;

public interface Board2IndexDao {

    /**
     * 保存Board2Index对象
     * @param board2Index
     */
    void saveBoard2Index(Board2Index board2Index);

    /**
     * 保存Board2Index对象列表
     * @param board2IndexList
     */
    void saveBoard2IndexList(List<Board2Index> board2IndexList);
}
