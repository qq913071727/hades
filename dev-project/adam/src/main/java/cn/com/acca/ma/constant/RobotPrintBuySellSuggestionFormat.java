package cn.com.acca.ma.constant;

/**
 * 机器人打印买卖建议的格式
 */
public class RobotPrintBuySellSuggestionFormat {

    /**
     * json格式
     */
    public static final Integer JSON = 1;

    /**
     * 简单文本格式
     */
    public static final Integer SIMPLE = 2;
}
