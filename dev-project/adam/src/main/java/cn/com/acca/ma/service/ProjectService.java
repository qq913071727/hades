package cn.com.acca.ma.service;

public interface ProjectService extends BaseService {
	/**************************************** 设置代理服务器的IP和port **************************************************/
	/**
	 * 如果程序是在公司环境运行，则设置代理服务器；否则直接跳过
	 */
	void setProxyProperties();
	
	/**
	 * 判断程序是否是在家里的电脑上运行。
	 */
	boolean isHomeComputer();
	
	/**************************************** 收集数据库统计信息 ************************************************/
	/**
	 * 收集数据库统计信息
	 */
	void gatherDatabaseStatistics();
	
	/******************************************* 备份项目代码 ****************************************************/
	/**
	 * 备份项目
	 */
	void backupCode();
	
	/******************************************* 备份数据库/表 （海量和增量） ********************************************/
	/**
	 * 海量备份数据库，每次只备份一部分
	 */
	void backupDatabaseMass();

	/**
	 * 增量备份数据库的各个表
	 * 注意：由于各个表的表结构不同，在备份和回复时，也是采取的不同策略。
	 * 每个方法的注解中都详细地写清楚了备份和回复的策略，因此，需要仔细阅读每个方法的注解才能了解具体的策略。
	 * @param multithreading
	 */
	void backupDatabaseIncrementalByJson(boolean multithreading);

	/**
	 * 使操作系统进入睡眠状态
	 */
	void makeOperationSystemSleep();

	/**
	 * 增量备份表STOCK_TRANSACTION_DATA
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 */
	void backupStockTransactionDataIncrementalByJson(boolean multithreading, String beginDate,String endDate);
	
	/**
	 * 增量备份表STOCK_WEEK
	 * 每只股票的周线级别的记录的WEEKEND_BEGIN_DATE不一定是星期一，WEEKEND_END_DATE也不一定是星期五。
	 * 备份时是用WEEKEND_END_DATE字段为标准，每个WEEKEND_END_DATE字段值相同的为一个zip文件。
	 * 有时多个zip文件一起构成了某一周的所有记录。
	 * @param beginDate
	 * @param endDate
	 */
	void backupStockWeekIncrementalByJson(String beginDate,String endDate);
	
	/**
	 * 增量备份表STOCK_INDEX
	 * @param beginDate
	 * @param endDate
	 */
	void backupStockIndexIncrementalByJson(String beginDate,String endDate);
	
	/**
	 * 增量备份表STOCK_INDEX_WEEK
	 * @param beginDate
	 * @param endDate
	 */
	void backupStockIndexWeekIncrementalByJson(String beginDate, String endDate);
	
	/**
	 * 增量备份表BOARD_INDEX
	 * @param beginDate
	 * @param endDate
	 */
	void backupBoardIndexIncrementalByJson(String beginDate,String endDate);
	
	/**
	 * 增量备份表REPORT
	 * @param beginDate
	 * @param endDate
	 */
	void backupReportIncrementalByJson(String beginDate,String endDate);
	
	/**
	 * 增量备份表MDL_MACD_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelMACDGoldCrossIncrementalByJson(String beginDate,String endDate);

	/**
	 * 增量备份表MDL_HEI_KIN_ASHI_UP_DOWN
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelHeiKinAshiUpDownIncrementalByJson(String beginDate,String endDate);

	/**
	 * 增量备份表MDL_TOP_STOCK
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelTopStockIncrementalByJson(String beginDate,String endDate);

	/**
	 * MDL_STOCK_ANALYSIS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelStockAnalysisIncrementalByJson(String beginDate,String endDate);

	/**
	 * STOCK_MONTH
	 * @param beginDate
	 * @param endDate
	 */
	void backupStockMonthIncrementalByJson(String beginDate,String endDate);

	/**
	 * MDL_STOCK_MONTH_ANALYSIS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelStockMonthAnalysisIncrementalByJson(String beginDate,String endDate);

	/**
	 * 增量备份表MDL_KD_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelKDGoldCrossIncrementalByJson(String beginDate,String endDate);

	/**
	 * 增量备份表MDL_WEEK_KD_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelWeekKDGoldCrossIncrementalByJson(String beginDate, String endDate);
	
	/**
	 * 增量备份表FOREIGN_EXCHANGE_RECORD
	 * @param beginDate
	 * @param endDate
	 */
	void backupForeignExchangeRecord(String beginDate,String endDate);

	/**
	 * 增量备份表MDL_CLOSE_PRICE_MA5_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelClosePriceMA5GoldCrossIncrementalByJson(String beginDate, String endDate);

	/**
	 * 增量备份表MDL_ALL_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	void backupModelAllGoldCrossIncrementalByJson(String beginDate, String endDate);
	
	/**
	 * 增量备份表BOARD
	 */
	void backupBoardIncrementalByJson();
	
	/**
	 * 增量备份表STOCK_INFO
	 */
	void backupStockInfoIncrementalByJson();
	
	/**
	 * 增量备份表MODEL
	 */
	void backupModelIncrementalByJson();
	
	/**
	 * 增量备份表FOREIGN_EXCHANGE
	 */
	void backupForeignExchangeIncrementalByJson();
	
	/*******************************************使用json文件恢复数据库中各个表的数据 ********************************************/
	/**
	 * 恢复数据库中各个表的数据
	 */
	void restoreTableIncrementalByJson();
	
	/**
	 * 恢复表STOCK_TRANSACTION_DATA中的数据
	 * @param beginDate
	 * @param endDate
	 */
	void restoreStockTransactionDataIncrementalByJson(String beginDate,String endDate);
	
	/**
	 * 恢复表STOCK_WEEK中的数据
	 * @param beginDate
	 * @param endDate
	 */
	void restoreStockWeekIncrementalByJson(String beginDate,String endDate);

	/**
	 * 恢复表STOCK_MONTH中的数据
	 */
	void restoreStockMonthIncrementalByJson();
	
	/**
	 * 恢复表STOCK_INDEX中的数据
	 */
	void restoreStockIndexIncrementalByJson();
	
	/**
	 * 恢复表STOCK_INDEX_WEEK中的数据
	 */
	void restoreStockIndexWeekIncrementalByJson();
	
	/**
	 * 恢复表BOARD_INDEX中的数据
	 */
	void restoreBoardIndexIncrementalByJson();
	
	/**
	 * 恢复表REPORT中的数据
	 */
	void restoreReportIncrementalByJson();

	/**
	 * 恢复表MDL_CLOSE_PRICE_MA5_GOLD_CROSS的数据
	 */
	void restoreModelClosePriceMA5GoldCrossIncrementalByJson();

	/**
	 * 恢复表MDL_ALL_GOLD_CROSS的数据
	 */
	void restoreModelAllGoldCrossIncrementalByJson();
	
	/**
	 * 恢复表MDL_MACD_GOLD_CROSS中的数据
	 */
	void restoreModelMACDGoldCrossIncrementalByJson();

	/**
	 * 恢复表MDL_HEI_KIN_ASHI_UP_DOWN中的数据
	 */
	void restoreModelHeiKinAshiUpDownIncrementalByJson();

	/**
	 * 恢复表MDL_TOP_STOCK中的数据
	 */
	void restoreModelTopStockIncrementalByJson();

	/**
	 * 向表MDL_STOCK_ANALYSIS中插入数据
	 */
	void restoreModelStockAnalysisIncrementalByJson();

	/**
	 * 向表MDL_STOCK_MONTH_ANALYSIS中插入数据
	 */
	void restoreModelStockMonthAnalysisIncrementalByJson();

	/**
	 * 恢复表MDL_WEEK_KD_GOLD_CROSS中的数据
	 */
	void restoreModelWeekKDGoldCrossIncrementalByJson();

	/**
	 * 恢复表MDL_KD_GOLD_CROSS中的数据
	 */
	void restoreModelKDGoldCrossIncrementalByJson();
	
	/**
	 * 恢复表FOREIGN_EXCHANGE_RECORD的数据
	 */
	void restoreForeignExchangeRecordByJson();
	
	/**
	 * 恢复表BOARD中的数据
	 */
	void restoreBoardIncrementalByJson();
	
	/**
	 * 恢复表STOCK_INFO中的数据
	 */
	void restoreStockInfoIncrementalByJson();
	
	/**
	 * 恢复表MODEL中的数据
	 */
	void restoreModelIncrementalByJson();
	
	/**
	 * 恢复表FOREIGN_EXCHANGE中的数据
	 */
	void restoreForeignExchangeIncrementalByJson();

	/**************************************** 发送邮件 ************************************************/
	/**
	 * 发送邮件
	 */
	void sendEMail();
}



