package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 周线级别平均KD
 */
@Data
public class StockWeekAverageKD {

    /**
     * 平均K
     */
    private BigDecimal averageK;

    /**
     * 平均D
     */
    private BigDecimal averageD;
}
