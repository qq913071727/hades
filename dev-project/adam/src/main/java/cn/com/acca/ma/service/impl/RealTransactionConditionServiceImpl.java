package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.RealTransactionCondition;
import cn.com.acca.ma.service.RealTransactionConditionService;
import java.util.List;

public class RealTransactionConditionServiceImpl extends
    BaseServiceImpl<RealTransactionConditionServiceImpl, RealTransactionCondition> implements
    RealTransactionConditionService {

    public RealTransactionConditionServiceImpl() {
        super();
    }

    /**
     * 预先计算股票的买卖条件
     */
    @Override
    public void preCalculateTransactionCondition_real() {
        logger.info("开始预先计算股票的买卖条件");

        String date = PropertiesUtil
            .getValue(MODEL_FILTER_PROPERTIES,
                "filter.real.transaction.pre.calculate.transaction.condition.date");
//        Double closePriceStart = Double.parseDouble(PropertiesUtil
//            .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.closePrice.start"));
//        Double closePriceEnd = Double.parseDouble(PropertiesUtil
//            .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.closePrice.end"));
        String xrBeginDate = PropertiesUtil
            .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.xr.begin.date");
//        Integer currentClosePriceCompareToSomeTimeHighestPrice = Integer.parseInt(PropertiesUtil
//            .getValue(MODEL_FILTER_PROPERTIES,
//                "filter.real.transaction.current.closePrice.compareTo.someTime.highest.price"));
//        Integer currentClosePriceCompareToSomeTimeLowestPrice = Integer.parseInt(PropertiesUtil
//                .getValue(MODEL_FILTER_PROPERTIES,
//                        "filter.real.transaction.current.closePrice.compareTo.someTime.lowest.price"));
        String maNotDecreasingLevel = PropertiesUtil
            .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.ma.not.decreasing.level");
        String maNotDecreasingDateNumber = PropertiesUtil
            .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.ma.not.decreasing.dateNumber");
        String maNotIncreasingLevel = PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.ma.not.increasing.level");
        String maNotIncreasingDateNumber = PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.ma.not.increasing.dateNumber");

        // 删除real_transaction_condition表中的数据
        realTransactionConditionDao.truncateTableRealTransactionCondition();

        // 拼参数（用于做多）
        String[] maNotDecreasingLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
        String[] maNotDecreasingDateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
        Integer ma120NotDecreasing = -1;
        Integer ma120NotDecreasingDateNumber = -1;
        Integer ma250NotDecreasing = -1;
        Integer ma250NotDecreasingDateNumber = -1;
        if (maNotDecreasingLevelArray.length == 1){
            if(maNotDecreasingLevelArray[0].trim().equals("120")){
                ma120NotDecreasing = 1;
            }
            if(maNotDecreasingLevelArray[0].trim().equals("250")){
                ma250NotDecreasing = 1;
            }
        }
        if (maNotDecreasingLevelArray.length == 2){
            if(maNotDecreasingLevelArray[0].trim().equals("120") || maNotDecreasingLevelArray[1].trim().equals("120")){
                ma120NotDecreasing = 1;
            }
            if(maNotDecreasingLevelArray[0].trim().equals("250") || maNotDecreasingLevelArray[1].trim().equals("250")){
                ma250NotDecreasing = 1;
            }
        }
        if (maNotDecreasingDateNumberArray.length == 1){
            if(ma120NotDecreasing.equals(1)){
                ma120NotDecreasingDateNumber = Integer.parseInt(maNotDecreasingDateNumberArray[0].trim());
            }
            if(ma250NotDecreasing.equals(1)){
                ma250NotDecreasingDateNumber = Integer.parseInt(maNotDecreasingDateNumberArray[1].trim());
            }
        }
        if (maNotDecreasingDateNumberArray.length == 2){
            if(ma120NotDecreasing.equals(1)){
                ma120NotDecreasingDateNumber = Integer.parseInt(maNotDecreasingDateNumberArray[0].trim());
            }
            if(ma250NotDecreasing.equals(1)){
                ma250NotDecreasingDateNumber = Integer.parseInt(maNotDecreasingDateNumberArray[1].trim());
            }
        }

        // 拼参数（用于做多）
        String[] maNotIncreasingLevelArray = maNotIncreasingLevel.replace("[", "").replace("]", "").split(",");
        String[] maNotIncreasingDateNumberArray = maNotIncreasingDateNumber.replace("[", "").replace("]", "").split(",");
        Integer ma120NotIncreasing = -1;
        Integer ma120NotIncreasingDateNumber = -1;
        Integer ma250NotIncreasing = -1;
        Integer ma250NotIncreasingDateNumber = -1;
        if (maNotIncreasingLevelArray.length == 1){
            if(maNotIncreasingLevelArray[0].trim().equals("120")){
                ma120NotIncreasing = 1;
            }
            if(maNotIncreasingLevelArray[0].trim().equals("250")){
                ma250NotIncreasing = 1;
            }
        }
        if (maNotIncreasingLevelArray.length == 2){
            if(maNotIncreasingLevelArray[0].trim().equals("120") || maNotIncreasingLevelArray[1].trim().equals("120")){
                ma120NotIncreasing = 1;
            }
            if(maNotIncreasingLevelArray[0].trim().equals("250") || maNotIncreasingLevelArray[1].trim().equals("250")){
                ma250NotIncreasing = 1;
            }
        }
        if (maNotIncreasingDateNumberArray.length == 1){
            if(ma120NotIncreasing.equals(1)){
                ma120NotIncreasingDateNumber = Integer.parseInt(maNotIncreasingDateNumberArray[0].trim());
            }
            if(ma250NotIncreasing.equals(1)){
                ma250NotIncreasingDateNumber = Integer.parseInt(maNotIncreasingDateNumberArray[0].trim());
            }
        }
        if (maNotIncreasingDateNumberArray.length == 2){
            if(ma120NotIncreasing.equals(1)){
                ma120NotIncreasingDateNumber = Integer.parseInt(maNotIncreasingDateNumberArray[0].trim());
            }
            if(ma250NotIncreasing.equals(1)){
                ma250NotIncreasingDateNumber = Integer.parseInt(maNotIncreasingDateNumberArray[1].trim());
            }
        }

        // 向real_transaction_condition表中写数据
        realTransactionConditionDao.writeRealTransactionCondition(xrBeginDate, date,
            /*currentClosePriceCompareToSomeTimeHighestPrice, currentClosePriceCompareToSomeTimeLowestPrice,
                closePriceStart, closePriceEnd,*/ ma120NotDecreasing, ma120NotDecreasingDateNumber,
                ma250NotDecreasing, ma250NotDecreasingDateNumber, ma120NotIncreasing,
                ma120NotIncreasingDateNumber, ma250NotIncreasing, ma250NotIncreasingDateNumber);
    }


    @Override
    public String listToString(List list) {
        return null;
    }


}
