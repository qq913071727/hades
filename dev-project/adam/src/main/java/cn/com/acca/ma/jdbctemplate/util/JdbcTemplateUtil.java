package cn.com.acca.ma.jdbctemplate.util;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * JdbcTemplate工具类
 */
public class JdbcTemplateUtil {

    private static DataSource dataSource;

    /**
     * jdbcTemplate对象
     */
    private static JdbcTemplate jdbcTemplate;

    /**
     * namedParameterJdbcTemplate对象
     */
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /**
     * transactionTemplate对象
     */
    private static TransactionTemplate transactionTemplate;

    static {
        try {
            //1.加载配置文件
            Properties properties = new Properties();
            //使用ClassLoader加载配置文件，获取字节输入流
            InputStream inputStream = JdbcTemplateUtil.class.getClassLoader()
                .getResourceAsStream("jdbc-template.properties");
            properties.load(inputStream);

            //2.初始化连接池对象
            dataSource = DruidDataSourceFactory.createDataSource(properties);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取连接池对象
     */
    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     * 创建一个新的DataSource对象
     * @return
     */
    public static DataSource newDataSource() {
        try {
            //1.加载配置文件
            Properties properties = new Properties();
            //使用ClassLoader加载配置文件，获取字节输入流
            InputStream inputStream = JdbcTemplateUtil.class.getClassLoader()
                .getResourceAsStream("jdbc-template.properties");
            properties.load(inputStream);

            //2.初始化连接池对象
            DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);
            return dataSource;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取连接Connection对象
     */
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * 返回jdbcTemplate对象
     *
     * @return
     */
    public static JdbcTemplate getJdbcTemplate() {
        if (null == jdbcTemplate) {
            jdbcTemplate = new JdbcTemplate(getDataSource());
        }
        return jdbcTemplate;
    }

    /**
     * 创建一个新的JdbcTemplate对象
     * @return
     */
    public static JdbcTemplate newJdbcTemplate(){
        DataSource dataSource = newDataSource();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    /**
     * 返回namedParameterJdbcTemplate对象
     *
     * @return
     */
    public static NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        if (null == namedParameterJdbcTemplate) {
            namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());
        }
        return namedParameterJdbcTemplate;
    }

    /**
     * 创建一个新的NamedParameterJdbcTemplate对象
     * @return
     */
    public static NamedParameterJdbcTemplate newNamedParameterJdbcTemplate(){
        DataSource dataSource = newDataSource();
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        return namedParameterJdbcTemplate;
    }

    /**
     * 事务管理
     * @return
     */
    public static DataSourceTransactionManager getDataSourceTransactionManager() {
        return new DataSourceTransactionManager(getDataSource());
    }

    /**
     * 创建一个新的DataSourceTransactionManager
     * @return
     */
    public static DataSourceTransactionManager newDataSourceTransactionManager() {
        return new DataSourceTransactionManager(newDataSource());
    }

    /**
     * 事务模板
     * @return
     */
    public static TransactionTemplate getTransactionTemplate (){
        if (null == transactionTemplate) {
            transactionTemplate = new TransactionTemplate(getDataSourceTransactionManager());
        }
        return transactionTemplate;
    }

    /**
     * 创建一个新的TransactionTemplate对象
     * @return
     */
    public static TransactionTemplate newTransactionTemplate(){
        TransactionTemplate transactionTemplate = new TransactionTemplate(newDataSourceTransactionManager());
        return transactionTemplate;
    }

}
