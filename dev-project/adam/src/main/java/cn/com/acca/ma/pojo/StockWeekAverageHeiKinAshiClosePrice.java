package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 周线级别平均hei_kin_ashi收盘价
 */
@Data
public class StockWeekAverageHeiKinAshiClosePrice {

    /**
     * 平均ha收盘价
     */
    private BigDecimal averageHeiKinAshiClosePrice;

    /**
     * 平均ha开盘价
     */
    private BigDecimal averageHeiKinAshiOpenPrice;

}
