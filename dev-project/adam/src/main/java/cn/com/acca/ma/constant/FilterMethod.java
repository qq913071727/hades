package cn.com.acca.ma.constant;

/**
 * 过滤算法。单数用于做多，双数用于做空
 */
public class FilterMethod {

    /**
     * 强制止盈
     */
    public static final Integer MANDATORY_STOP_PROFIT = -1;

    /**
     * 强制止损
     */
    public static final Integer MANDATORY_STOP_LOSS = -2;

    /**
     * MACD金叉
     */
    public static final Integer MACD_GOLD_CROSS = 1;

    /**
     * MACD死叉
     */
    public static final Integer MACD_DEAD_CROSS = 2;

    /**
     * 收盘价金叉叉5日均线
     */
    public static final Integer CLOSE_PRICE_GOLD_CROSS_MA = 3;

    /**
     * 收盘价死叉5日均线
     */
    public static final Integer CLOSE_PRICE_DEAD_CROSS_MA = 4;

    /**
     * 平均K线从下跌趋势变为上涨趋势
     */
    public static final Integer HEI_KIN_ASHI_UP_DOWN = 5;

    /**
     * 平均K线从上涨趋势变为下跌趋势
     */
    public static final Integer HEI_KIN_ASHI_DOWN_UP = 6;

    /**
     * KD金叉
     */
    public static final Integer KD_GOLD_CROSS = 7;

    /**
     * KD死叉
     */
    public static final Integer KD_DEAD_CROSS = 8;

    /**
     * 选出来的过滤算法
     */
    public static Integer SELECT_FILTER_METHOD = null;

    /**
     * 选出来的过滤算法的成功率/百分比/平均值
     */
    public static Double SELECT_FILTER_METHOD_VALUE = 0.0;

    /**
     * 将数字转换为中文描述
     * @param filterType
     * @return
     */
    public static String toDescription(Integer filterType){
        if (filterType == -1){
            return "强制止盈";
        }
        if (filterType == -2){
            return "强制止损";
        }
        if (filterType == 1){
            return "MACD金叉";
        }
        if (filterType == 2){
            return "MACD死叉";
        }
        if (filterType == 3){
            return "close_price金叉ma5";
        }
        if (filterType == 4){
            return "close_price死叉ma5";
        }
        if (filterType == 5){
            return "hei_kin_ashi上涨趋势";
        }
        if (filterType == 6){
            return "hei_kin_ashi下跌趋势";
        }
        if (filterType == 7){
            return "KD金叉";
        }
        if (filterType == 8){
            return "KD金叉";
        }
        return null;
    }
}

