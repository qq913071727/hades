package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Robot4StockTransactRecordDao;
import cn.com.acca.ma.model.Robot4StockTransactRecord;
import cn.com.acca.ma.pojo.BuySuggestion;
import cn.com.acca.ma.pojo.SellSuggestion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Robot4StockTransactRecordDaoImpl extends BaseDaoImpl<Robot4StockTransactRecord> implements
    Robot4StockTransactRecordDao {

    public Robot4StockTransactRecordDaoImpl() {
        super();
    }

    /**
     * 卖股票/买股票
     * @param sellDate
     * @param mandatoryStopLoss
     * @param mandatoryStopLossRate
     * @param mandatoryStopProfit
     * @param mandatoryStopProfitRate
     */
    @Override
    public void sellOrBuy(String sellDate, Integer mandatoryStopLoss, Double mandatoryStopLossRate, Integer mandatoryStopProfit, Double mandatoryStopProfitRate) {
        logger.info("卖股票/买股票");

        String sql = "{call PKG_ROBOT4.sell_or_buy('" + sellDate + "', " + mandatoryStopLoss + ", "
                + mandatoryStopLossRate + ", " + mandatoryStopProfit + ", " + mandatoryStopProfitRate + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 买股票/卖股票。同时用于做多和做空
     * @param buyDate
     * @param backwardMonth
     * @param averageDateNumber
     * @param successRateType
     * @param successRate
     * @param direction
     * @param shippingSpaceControl
     * @param holdStockNumber
     */
    @Override
    public void buyOrSell(String buyDate, Integer backwardMonth, Integer averageDateNumber, Integer successRateType,
                          Double successRate, Integer direction, Integer shippingSpaceControl, Integer holdStockNumber) {
        logger.info("买股票/卖股票，日期为【" + buyDate + "】，向前月数为【" + backwardMonth + "】，平均成功率天数为【"
            + averageDateNumber + "】，类型为【" + successRateType + "】");

        // 存储过程最后的参数-1其实没有意义，不用管
        String sql = "{call PKG_ROBOT4.buy_or_sell('" + buyDate + "', " + backwardMonth + ", " + averageDateNumber +
                ", null, 0, " + direction + ", " + shippingSpaceControl +
                ", -1, -1, " + holdStockNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 买股票/卖股票。同时用于做多和做空
     * @param buyDate
     * @param backwardMonth
     * @param averageDateNumber
     * @param successRateType
     * @param successRate
     * @param direction
     * @param shippingSpaceControl
     * @param percentageTopThreshold
     * @param shippingSpace
     */
    @Override
    public void buyOrSell(String buyDate, Integer backwardMonth, Integer averageDateNumber, Integer successRateType, Double successRate, Integer direction, Integer shippingSpaceControl, Double percentageTopThreshold, Integer shippingSpace) {
        logger.info("买股票/卖股票，日期为【" + buyDate + "】，向前月数为【" + backwardMonth + "】，平均成功率天数为【"
                + averageDateNumber + "】，成功率类型为【" + successRateType + "】");

        String sql = "{call PKG_ROBOT4.buy_or_sell('" + buyDate + "', " + backwardMonth + ", " + averageDateNumber + ", " + successRateType + ", " + successRate + ", " + direction + ", " + shippingSpaceControl + ", " + percentageTopThreshold + ", " + shippingSpace + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 获取买入建议列表
     * 用于做多
     * @param buyDate
     * @return
     */
    public List<Robot4StockTransactRecord> getBullRobotStockTransactRecordListForBuy(String buyDate) {
        logger.info("获取买入建议列表，日期【" + buyDate + "】，用于做多");

        String sql = "select * from robot4_stock_transact_record t "
            + "where t.buy_date=to_date(:buyDate,'yyyy-mm-dd') "
            + "and t.sell_date is null and t.sell_price is null and t.sell_amount is null "
            + "and t.direction=1";
        Map map = new HashMap();
        map.put("buyDate", buyDate);

        return (List<Robot4StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot4StockTransactRecord.class);
    }

    /**
     * 获取卖出建议列表
     * 用于做多
     * @param sellDate
     * @return
     */
    public List<Robot4StockTransactRecord> getBullRobotStockTransactRecordListForSell(String sellDate) {
        logger.info("获取卖出建议列表，日期【" + sellDate + "】，用于做多");

        String sql = "select * from robot4_stock_transact_record t "
            + "where t.sell_date=to_date(:sellDate,'yyyy-mm-dd') "
            + "and t.sell_date is not null and t.sell_price is not null and t.sell_amount is not null "
            + "and t.direction=1";
        Map map = new HashMap();
        map.put("sellDate", sellDate);

        return (List<Robot4StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot4StockTransactRecord.class);
    }

    /**
     * 获取卖出建议列表
     * 用于做空
     * @param sellDate
     * @return
     */
    public List<Robot4StockTransactRecord> getShortRobotStockTransactRecordListForSell(String sellDate) {
        logger.info("获取卖出建议列表，日期【" + sellDate + "】，用于做空");

        String sql = "select * from robot4_stock_transact_record t "
                + "where t.sell_date=to_date(:sellDate,'yyyy-mm-dd') "
                + "and t.buy_date is null and t.buy_price is null and t.buy_amount is null "
                + "and t.direction=-1";
        Map map = new HashMap();
        map.put("sellDate", sellDate);

        return (List<Robot4StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot4StockTransactRecord.class);
    }

    /**
     * 获取买入建议列表
     * 用于做空
     * @param buyDate
     * @return
     */
    public List<Robot4StockTransactRecord> getShortRobotStockTransactRecordListForBuy(String buyDate) {
        logger.info("获取买入建议列表，日期【" + buyDate + "】，用于做空");

        String sql = "select * from robot4_stock_transact_record t "
                + "where t.buy_date=to_date(:buyDate,'yyyy-mm-dd') "
                + "and t.buy_date is not null and t.buy_price is not null and t.buy_amount is not null "
                + "and t.direction=-1";
        Map map = new HashMap();
        map.put("buyDate", buyDate);

        return (List<Robot4StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot4StockTransactRecord.class);
    }

    /**
     * 获取卖出建议列表
     * @param sellDate
     * @return
     */
    public List<SellSuggestion> getSellSuggestionList_weekKD(String sellDate){
        logger.info("获取卖出建议列表，日期【" + sellDate + "】");

        String sql = "select rstr.stock_code stockCode, si_.name_ stockName, rstr.sell_date sellDate, "
                        + "rstr.sell_price sellPrice, rstr.sell_amount sellAmount, rstr.filter_type filterType, rstr.direction direction "
                    + "from robot4_stock_transact_record rstr "
                    + "join stock_info si_ on si_.code_=rstr.stock_code "
                    + "where rstr.sell_date=to_date(:sellDate,'yyyy-mm-dd') "
                        + "and (rstr.sell_date is not null and rstr.sell_price is not null and rstr.sell_amount is not null and rstr.direction=1) "
                        + "or (rstr.buy_date is null and rstr.buy_price is null and rstr.buy_amount is null and rstr.direction=-1)";
        Map map = new HashMap();
        map.put("sellDate", sellDate);

        return (List<SellSuggestion>)doSQLQueryInTransaction(sql, map, SellSuggestion.class);
    }

    /**
     * 获取买入建议列表
     * @param buyDate
     * @return
     */
    public List<BuySuggestion> getBuySuggestionList_weekKD(String buyDate) {
        logger.info("获取买入建议列表，日期【" + buyDate + "】");

        String sql = "select rstr.stock_code stockCode, si_.name_ stockName, rstr.buy_date buyDate, "
                    + "rstr.buy_price buyPrice, rstr.buy_amount buyAmount, rstr.filter_type filterType, rstr.direction direction "
                    + "from robot4_stock_transact_record rstr "
                    + "join stock_info si_ on si_.code_=rstr.stock_code "
                    + "where rstr.buy_date=to_date(:buyDate,'yyyy-mm-dd') "
                        + "and (rstr.sell_date is null and rstr.sell_price is null and rstr.sell_amount is null and rstr.direction=1)"
                        + "or (rstr.buy_date is not null and rstr.buy_price is not null and rstr.buy_amount is not null and rstr.direction=-1)";
        Map map = new HashMap();
        map.put("buyDate", buyDate);

        return (List<BuySuggestion>)doSQLQueryInTransaction(sql, map, BuySuggestion.class);
    }

}
