//package cn.com.acca.ma.pojo;
//
//import lombok.Data;
//
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import java.math.BigDecimal;
//
///**
// * 当前日期和前一日平均dif和平均dea
// */
////@Data
////@Entity
//public class CurrentDateAndPreviousDateAverageDifDea extends BaseCurrentDataAndPreviousDateAverage implements Comparable {
//
//    /**
//     * 当前日期
//     */
////    @Id
//    private String currentDate;
//
//    /**
//     * 前一日
//     */
////    private String previousDate;
//
//    /**
//     * 当前日期dif和dea的平均值
//     */
////    private BigDecimal averageDifDea;
//
//    /**
//     * 当前日期的平均dif
//     */
////    private BigDecimal currentAverageDif;
//
//    /**
//     * 当前日期的平均dea
//     */
////    private BigDecimal currentAverageDea;
//
//    /**
//     * 前一日的平均dif
//     */
////    private BigDecimal previousAverageDif;
//
//    /**
//     * 前一日的平均dea
//     */
////    private BigDecimal previousAverageDea;
//
//    /**
//     * 金叉或死叉。1表示金叉，2表示死叉
//     */
//    private Integer goldCrossOrDeadCross;
//
//    /**
//     * 降序排列
//     * @param o
//     * @return
//     */
//    @Override
//    public int compareTo(Object o) {
//        CurrentDateAndPreviousDateAverageDifDea currentDateAndPreviousDateAverageDifDea = (CurrentDateAndPreviousDateAverageDifDea)o;
//        return currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage());
////        BigDecimal result = currentDateAndPreviousDateAverageDifDea.getCurrentAverageDif().add(currentDateAndPreviousDateAverageDifDea.getCurrentAverageDea()).divide(new BigDecimal(2))
////                .subtract(this.getCurrentAverageDif().add(this.getCurrentAverageDea()).divide(new BigDecimal(2)));
////        if (currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage()) > 0){
////            return 1;
////        }
////        if (result.doubleValue() < 0){
////            return  -1;
////        }
////        if (result.doubleValue() == 0){
////            return 0;
////        }
////        return 0;
//    }
//
//}
