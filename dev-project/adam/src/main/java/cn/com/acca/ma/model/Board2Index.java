package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 从网上爬到的板块数据，每个交易日都更新
 */
@Data
@Entity
@Table(name="BOARD2_INDEX")
public class Board2Index implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
    private Integer id;

    /**
     * 板块名称
     */
    @Column(name="BOARD_NAME")
    private String boardName;

    /**
     * 当日涨跌幅
     */
    @Column(name="CURRENT_DAY_CHANGE_RANGE")
    private Double currentDayChangeRange;

    /**
     * 涨停家数
     */
    @Column(name="LIMIT_UP_NUMBER")
    private Integer limitUpNumber;

    /**
     * 跌停家数
     */
    @Column(name="LIMIT_DOWN_NUMBER")
    private Integer limitDownNumber;

    /**
     * 交易日期，也是创建时间
     */
    @Column(name="TRANSACTION_DATE")
    private Date transactionDate;

    /**
     * 5分钟涨跌幅
     */
    @Column(name="FIVE_MINUTE_CHANGE_RANGE")
    private Double fiveMinuteChangeRange;

    /**
     * 15分钟涨跌幅
     */
    @Column(name="FIFTEEN_MINUTE_CHANGE_RANGE")
    private Double fifteenMinuteChangeRange;

    /**
     * 5日涨跌幅
     */
    @Column(name="FIVE_DAY_CHANGE_RANGE")
    private Double fiveDayChangeRange;

    /**
     * 主力资金流入（万）
     */
    @Column(name="MAJOR_CAPITAL_INFLOW")
    private BigDecimal majorCapitalInflow;

    /**
     * 贡献最大股票名称
     */
    @Column(name="TOP_CONTRIBUTE_STOCK_NAME")
    private String topContributeStockName;

    /**
     * 贡献最大股票收盘价
     */
    @Column(name="TOP_CONTRIBUTE_STOCK_C_P")
    private Double topContributeStockClosePrice;

    /**
     * 贡献最大股票涨跌幅
     */
    @Column(name="TOP_CONTRIBUTE_STOCK_C_R")
    private Double topContributeStockChangeRange;
}
