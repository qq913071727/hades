package cn.com.acca.ma.dao;

public interface ModelWeekAllGoldCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据
     * @param type
     */
    void writeModelWeekAllGoldCross(String type);
}
