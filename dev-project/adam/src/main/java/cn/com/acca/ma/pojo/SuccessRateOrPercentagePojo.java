package cn.com.acca.ma.pojo;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 某一日金叉死叉成功率百分比
 */
@Data
@Entity
@Table(name="success_rate_or_percentage")
public class SuccessRateOrPercentagePojo {

    /**
     * 日期
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="transactionDate")
    private Date transactionDate;

    /**
     * 金叉死叉股票的成功率或百分比
     */
    @Column(name="successRateOrPercentage")
    private BigDecimal successRateOrPercentage;

    public SuccessRateOrPercentagePojo() {
    }

    public SuccessRateOrPercentagePojo(Date transactionDate, BigDecimal successRateOrPercentage) {
        this.transactionDate = transactionDate;
        this.successRateOrPercentage = successRateOrPercentage;
    }

}
