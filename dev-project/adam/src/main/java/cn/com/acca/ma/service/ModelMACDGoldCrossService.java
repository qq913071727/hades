package cn.com.acca.ma.service;

public interface ModelMACDGoldCrossService extends BaseService {
	
	/*********************************************************************************************************************
	 * 
	 * 												按日期计算数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用日线级别MACD金叉模型算法，根据endDate日期，向表MDL_MACD_GOLD_CROSS插入数据
	 * @param multithreading
	 */
	void writeModelMACDGoldCrossIncr(boolean multithreading);
	
	/*********************************************************************************************************************
	 * 
	 * 												   	创建图表
	 * 
	 *********************************************************************************************************************/
	/**
	 * 创建MACD金叉散点图
	 * @param multithreading
	 */
	void createMACDGoldCrossScatterPlotPicture(boolean multithreading);

	/**
	 * MACD交易方法成功时，dif和dea的分布图。profitLoss为true时表示盈利，为true时表示亏损
	 * @param multithreading
	 * @param profitLoss
	 */
	void createMACDSuccessDidDeaScatterPlotPicture(boolean multithreading, boolean profitLoss);

	/**
	 * 创建MACD金叉算法成功率图
	 * @param multithreading
	 */
	void createMACDSuccessRatePicture(boolean multithreading);
	
	/*********************************************************************************************************************
	 * 
	 * 												计算全部数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用日线级别MACD金叉模型算法，向表MDL_MACD_GOLD_CROSS插入数据
	 */
	void writeModelMACDGoldCross();

}
