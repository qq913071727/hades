package cn.com.acca.ma.dao;

public interface Robot6AccountDao extends BaseDao {

    /**************************************** 卖etf ********************************************/
    /**
     * 根据当日持有etf的收盘价，在卖etf之前，更新robot6_account表
     *
     * @param date
     */
    void updateRobotAccountBeforeSellOrBuy(String date);

    /**
     * 根据当日卖出etf的收盘价，在卖etf之后，更新robot6_account表
     *
     * @param date
     */
    void updateRobotAccountAfterSellOrBuy(String date);

    /**
     * 返回记录数
     *
     * @return
     */
    Integer count();

    /**
     * 列hold_stock_number求和
     *
     * @return
     */
    Integer sumHoldEtfNumber();

    /**************************************** 买etf ********************************************/
    /**
     * 根据当日买入etf的收盘价，在买etf之后，更新robot6_account表
     *
     * @param date
     */
    void updateRobotAccountAfterBuyOrSell(String date);
}
