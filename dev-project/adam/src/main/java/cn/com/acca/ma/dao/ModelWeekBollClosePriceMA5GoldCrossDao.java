package cn.com.acca.ma.dao;

import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;

import java.util.List;

public interface ModelWeekBollClosePriceMA5GoldCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_BOLL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     */
    void writeModelWeekBollClosePriceMA5GoldCross();

    /**
     * 查询close_price金叉MA5交易收益率和最高价突破布林带下轨的百分比
     * @param beginDate
     * @param endDate
     */
    List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekClosePriceGoldCrossMA5ProfitOrLossAndBollDn(String beginDate, String endDate);
}
