package cn.com.acca.ma.dao.impl;

import org.hibernate.SQLQuery;

import cn.com.acca.ma.dao.ProjectDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;

public class ProjectDaoImpl extends BaseDaoImpl<ProjectDaoImpl> implements ProjectDao {

	/**
	 * 收集数据库统计信息
	 */
	public void gatherDatabaseStatistics(){
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("{call PKG_TOOL.GATHER_DATEBASE_STATISTICS()}");
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
	}

}
