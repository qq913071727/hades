package cn.com.acca.ma.constant;

/**
 * 多空方向
 */
public class BullOrShortDirection {

    /**
     * 做多
     */
    public static final Integer BULL = 1;

    /**
     * 做空
     */
    public static final Integer SHORT = -1;
}
