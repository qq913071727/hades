package cn.com.acca.ma.service;

public interface ModelWeekBollHeiKinAshiUpDownService extends BaseService {

    /**
     * 海量地向表MDL_WEEK_BOLL_H_K_A_UP_DOWN中插入数据
     */
    void writeModelWeekBollHeiKinAshiUpDown();

    /**
     * 创建周线级别最低价突破布林带下轨的百分比和hei_kin_ashi上涨趋势交易收益率的散点图，x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨
     */
    void createWeekLowestPriceDnBollPercentAndHeiKinAshiUpDownProfitScatterPlotPicture();
}
