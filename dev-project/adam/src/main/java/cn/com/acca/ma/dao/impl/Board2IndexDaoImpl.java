package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Board2IndexDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.Board2Index;

import java.util.List;

public class Board2IndexDaoImpl extends BaseDaoImpl<Board2IndexDaoImpl> implements Board2IndexDao {

    public Board2IndexDaoImpl() {
        super();
    }

    /**
     * 保存Board2Index对象
     *
     * @param board2Index
     */
    @Override
    public void saveBoard2Index(Board2Index board2Index) {
        logger.info("保存Board2Index对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.save(board2Index);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 保存Board2Index对象列表
     *
     * @param board2IndexList
     */
    @Override
    public void saveBoard2IndexList(List<Board2Index> board2IndexList) {
        logger.info("保存Board2Index对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        for (Board2Index board2Index : board2IndexList) {
            session.save(board2Index);
        }
        session.getTransaction().commit();
        session.close();
    }
}
