package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.StockMovingAverage;
import cn.com.acca.ma.model.StockTransactionDataAll;
import cn.com.acca.ma.service.StockTransactionDataAllService;
import cn.com.acca.ma.service.StockTransactionDataService;
import java.util.List;

public class StockTransactionDataAllServiceImpl extends BaseServiceImpl<StockTransactionDataAllServiceImpl, StockTransactionDataAll> implements
    StockTransactionDataAllService {

    public StockTransactionDataAllServiceImpl() {
        super();
    }

    /**
     * 根据开始日期和结束日期，从表stock_transaction_data向表stock_transaction_data_all中写数据
     */
    public void writeStockTransactionDataAllByDate(boolean multithreading){
        String beginDate = PropertiesUtil.getValue(STOCK_RECORD_PROPERTIES, "stockRecord.all.write.beginDate");
        String endDate = PropertiesUtil.getValue(STOCK_RECORD_PROPERTIES, "stockRecord.all.write.endDate");
        stockTransactionDataAllDao.writeStockTransactionDataAllByDate(multithreading, beginDate, endDate);
    }

    @Override
    public String listToString(List list) {
        return null;
    }

}
