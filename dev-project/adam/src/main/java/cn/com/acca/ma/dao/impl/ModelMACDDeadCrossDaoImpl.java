package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelMACDDeadCrossDao;
import cn.com.acca.ma.hibernate.impl.GetMACDDeadCrossSuccessRateWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelMACDDeadCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelMACDDeadCrossDaoImpl extends BaseDaoImpl<ModelMACDDeadCross> implements ModelMACDDeadCrossDao {

    public ModelMACDDeadCrossDaoImpl() {
        super();
    }

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD死叉模型算法，根据endDate日期，向表MDL_MACD_DEAD_CROSS插入数据
     * @param multithreading
     * @param endDate
     */
    @Override
    public void writeModelMACDDeadCrossIncr(boolean multithreading, String endDate) {
        logger.info("开始增量地计算MDL_MACD_DEAD_CROSS表中的数据，日期为【" + endDate + "】");

        Session newSession = null;
        SQLQuery query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_MACD_DEAD_CROSS_INCR(?)}");
        } else {
            session= HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_MACD_DEAD_CROSS_INCR(?)}");
        }

        query.setString(0, endDate);
        query.executeUpdate();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        logger.info("增量地计算MDL_MACD_DEAD_CROSS表中的数据完成，日期为【" + endDate + "】");
    }

    /*********************************************************************************************************************
     *
     * 												   	创建图表
     *
     *********************************************************************************************************************/
    /**
     * 获取MACD死叉算法在开始时间到结束时间内成功率的数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<SuccessRateOrPercentagePojo> getMACDDeadCrossSuccessRate(boolean multithreading, final String beginDate, final String endDate, Integer dateNumber){
        logger.info("获取开始时间【" + beginDate + "】至【" + endDate + "】内，最近【" + dateNumber + "】天，MACD死叉的成功率");

        final List<SuccessRateOrPercentagePojo> list=new ArrayList<SuccessRateOrPercentagePojo>();

        if (multithreading) {
            Session newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            newSession.doWork(new GetMACDDeadCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session=HibernateUtil.currentSession();
            session.beginTransaction();
            session.doWork(new GetMACDDeadCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            session.getTransaction().commit();
            session.close();
        }

        logger.info("获取一段时间内，最近" + dateNumber + "天，MACD死叉的成功率完成");
        return list;
    }

    /**
     * 获取MACD死叉算法在开始时间到结束时间内，死叉股票的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SuccessRateOrPercentagePojo> getMACDDeadCrossPercentage(boolean multithreading, final String beginDate, final String endDate){
        logger.info("开始获取MACD死叉算法在开始时间【" + beginDate + "】到结束时间【" + endDate + "】内，死叉股票的百分比");

        String hql = "select t.date_ transactionDate, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_ " +
                "and t1.dif<t1.dea)/ " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 successRateOrPercentage " +
                "from stock_transaction_data_all t " +
                "where t.date_ between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd') " +
                "group by t.date_ order by t.date_ asc";

        Session newSession = null;
        Query query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        }

        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<SuccessRateOrPercentagePojo> list = query.list();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        return list;
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD死叉模型算法，向表MDL_MACD_DEAD_CROSS插入数据
     */
    @Override
    public void writeModelMACDDeadCross() {
        logger.info("开始使用日线级别MACD死叉模型算法，向表MDL_MACD_DEAD_CROSS插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_MACD_DEAD_CROSS()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("使用日线级别MACD死叉模型算法，向表MDL_MACD_DEAD_CROSS插入数据完成");
    }

    /*********************************************************************************************************************
     *
     * 										增量备份表MDL_MACD_DEAD_CROSS
     *
     *********************************************************************************************************************/
    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_MACD_DEAD_CROSS中获取BUY_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    @Override
    public List<Date> getDateByCondition(String beginDate, String endDate) {
        logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
                + "从表MDL_MACD_DEAD_CROSS中获取BUY_DATE字段");

        StringBuffer hql = new StringBuffer("select distinct t.buyDate from ModelMACDDeadCross t " +
                "where t.buyDate between to_date(?, 'yyyy-mm-dd') and to_date(?, 'yyyy-mm-dd')");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<Date> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 从表MDL_MACD_DEAD_CROSS中获取sell_date日的所有ModelMACDDeadCross对象
     * @param date
     * @return
     */
    @Override
    public List<ModelMACDDeadCross> getModelMACDDeadCrossByDate(String date) {
        logger.info("从表MDL_MACD_DEAD_CROSS中获取日期【" + date + "】的所有ModelMACDDeadCross对象");

        StringBuffer hql = new StringBuffer("select t from ModelMACDDeadCross t " +
                "where t.buyDate=to_date(?,'yyyy-mm-dd') order by t.buyDate asc");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, date);
        List<ModelMACDDeadCross> list=query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 向表MDL_MACD_DEAD_CROSS中插入ModelMACDDeadCross对象
     * @param modelMACDDeadCross
     */
    public void save(ModelMACDDeadCross modelMACDDeadCross){
        logger.info("向表MDL_MACD_DEAD_CROSS中插入ModelMACDDeadCross对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.persist(modelMACDDeadCross);
        session.getTransaction().commit();
        session.close();
    }
}
