package cn.com.acca.ma.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 周线级别交易数据
 * @author Administrator
 */
@Data
@Entity
@Table(name = "STOCK_WEEK")
public class StockWeek implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_")
//	@GeneratedValue(strategy= GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "CODE_")
	private String code;

	@Column(name = "NUMBER_")
	private BigDecimal number;

	@Column(name = "BEGIN_DATE")
	private Date beginDate;

	@Column(name = "END_DATE")
	private Date endDate;

	@Column(name = "UP_DOWN")
	private BigDecimal upDown;

	@Column(name = "OPEN_PRICE")
	private BigDecimal openPrice;

	@Column(name = "CLOSE_PRICE")
	private BigDecimal closePrice;

	@Column(name = "HIGHEST_PRICE")
	private BigDecimal highestPrice;

	@Column(name = "LOWEST_PRICE")
	private BigDecimal lowestPrice;

	@Column(name = "VOLUME")
	private BigDecimal volume;

	@Column(name = "TURNOVER")
	private BigDecimal turnover;

	/**
	 * 5日均线
	 */
	@Column(name="MA5")
	private BigDecimal ma5;

	/**
	 * 10日均线
	 */
	@Column(name="MA10")
	private BigDecimal ma10;

	/**
	 * 20日均线
	 */
	@Column(name="MA20")
	private BigDecimal ma20;

	/**
	 * 60日均线
	 */
	@Column(name="MA60")
	private BigDecimal ma60;

	/**
	 * 120日均线
	 */
	@Column(name="MA120")
	private BigDecimal ma120;

	/**
	 * 250日均线
	 */
	@Column(name="MA250")
	private BigDecimal ma250;

	@Column(name = "RSV")
	private BigDecimal rsv;

	@Column(name = "K")
	private BigDecimal k;

	@Column(name = "D")
	private BigDecimal d;

	@Column(name = "EMA12")
	private BigDecimal ema12;

	@Column(name = "EMA26")
	private BigDecimal ema26;

	@Column(name = "DIF")
	private BigDecimal dif;

	@Column(name = "DEA")
	private BigDecimal dea;
	
	@Column(name = "MB")
	private BigDecimal mb;
	
	@Column(name = "UP")
	private BigDecimal up;
	
	@Column(name = "DN_")
	private BigDecimal dn;

    /**
     * 带除权的本周股价的涨跌幅。单位：%
     */
    @Column(name = "CHANGE_RANGE_EX_RIGHT")
    private BigDecimal changeRangeExRight;

	/**
	 * HeiKinAshi本周开盘价
	 */
	@Column(name = "HA_WEEK_OPEN_PRICE")
	private BigDecimal haWeekOpenPrice;

	/**
	 * HeiKinAshi本周收盘价
	 */
	@Column(name = "HA_WEEK_CLOSE_PRICE")
	private BigDecimal haWeekClosePrice;

	/**
	 * HeiKinAshi本周最高价
	 */
	@Column(name = "HA_WEEK_HIGHEST_PRICE")
	private BigDecimal haWeekHighestPrice;

	/**
	 * HeiKinAshi本周最低价
	 */
	@Column(name = "HA_WEEK_LOWEST_PRICE")
	private BigDecimal haWeekLowestPrice;

}
