package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 机器人3--账户
 */
@Data
public class Robot3Account {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 机器人3名字
     */
    private String robotName;

    /**
     * 持股数量
     */
    private Integer holdStockNumber;

    /**
     * 股票资产
     */
    private BigDecimal stockAssets;

    /**
     * 资金资产
     */
    private BigDecimal capitalAssets;

    /**
     * 总资产
     */
    private BigDecimal totalAssets;


}
