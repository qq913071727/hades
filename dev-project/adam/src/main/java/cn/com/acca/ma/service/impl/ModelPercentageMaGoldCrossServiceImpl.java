package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.ModelWeekKDGoldCross;
import cn.com.acca.ma.service.ModelPercentageMaGoldCrossService;
import java.util.List;

public class ModelPercentageMaGoldCrossServiceImpl extends BaseServiceImpl<ModelPercentageMaGoldCrossServiceImpl, ModelWeekKDGoldCross> implements
    ModelPercentageMaGoldCrossService {

    @Override
    public String listToString(List list) {
        return null;
    }

    /**
     * 使用percentage*_ma*金叉模型算法，海量地向表MDL_PERCENTAGE_MA_GOLD_CROSS插入数据
     */
    @Override
    public void writeModelPercentageMaGoldCross() {
        modelPercentageMaGoldCrossDao.writeModelPercentageMaGoldCross();
    }
}
