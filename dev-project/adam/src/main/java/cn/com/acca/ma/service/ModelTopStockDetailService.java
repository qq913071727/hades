package cn.com.acca.ma.service;

public interface ModelTopStockDetailService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 按照日期，向表MDL_TOP_STOCK_DETAIL中写入数据
     * @param multithreading
     */
    void writeModelTopStockDetailByDate(boolean multithreading);

    /**
     * 根据开始日期beginDate和结束日期endDate计算表MDL_TOP_STOCK_DETAIL中的percentage_*_ma*字段
     */
    void writeModelTopStockDetailMAByDate();

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 按照日期，创建每支股票上涨百分比的折线图
     */
    void createModelTopStockDetailPicture();
}
