package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Real3StockTransactionRecord;

import java.util.HashMap;
import java.util.List;

public interface Real3StockTransactionRecordDao extends BaseDao {

    /**
     * 计算MACD的成功率
     * @param date
     * @param backwardMonth
     * @param averageDateNumber
     * @return
     */
    List<HashMap> calculateMACDSuccessRate(String date, Integer backwardMonth, Integer averageDateNumber);

    /**
     * 查找real3_stock_transaction_record表中，sell_date、sell_price、sell_amount列都为空的记录
     */
    List<Real3StockTransactionRecord> findBySellDateNullAndSellPriceNullAndSellAmountNull();

    /**
     * 查找real3_stock_transaction_record表中，buy_date、buy_price、buy_amount列都为空的记录
     */
    List<Real3StockTransactionRecord> findByBuyDateNullAndBuyPriceNullAndBuyAmountNull();

}
