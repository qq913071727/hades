package cn.com.acca.ma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MDL_WEEK_ALL_GOLD_CROSS")
public class ModelWeekAllGoldCross implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "STOCK_CODE")
	private String stockCode;

	@Column(name = "SELL_BEGIN_DATE")
	private Date sellBeginDate;

	@Column(name = "SELL_END_DATE")
	private Date sellEndDate;

	@Column(name = "SELL_PRICE")
	private BigDecimal sellPrice;

	@Column(name = "BUY_BEGIN_DATE")
	private Date buyBeginDate;

    @Column(name = "BUY_END_DATE")
    private Date buyEndDate;

	@Column(name = "BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

	@Column(name = "TYPE_")
	private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Date getSellBeginDate() {
        return sellBeginDate;
    }

    public void setSellBeginDate(Date sellBeginDate) {
        this.sellBeginDate = sellBeginDate;
    }

    public Date getSellEndDate() {
        return sellEndDate;
    }

    public void setSellEndDate(Date sellEndDate) {
        this.sellEndDate = sellEndDate;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Date getBuyBeginDate() {
        return buyBeginDate;
    }

    public void setBuyBeginDate(Date buyBeginDate) {
        this.buyBeginDate = buyBeginDate;
    }

    public Date getBuyEndDate() {
        return buyEndDate;
    }

    public void setBuyEndDate(Date buyEndDate) {
        this.buyEndDate = buyEndDate;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getAccumulativeProfitLoss() {
        return accumulativeProfitLoss;
    }

    public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
        this.accumulativeProfitLoss = accumulativeProfitLoss;
    }

    public BigDecimal getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(BigDecimal profitLoss) {
        this.profitLoss = profitLoss;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
