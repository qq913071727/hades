package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexClosePriceMA5GoldCross;

import java.util.List;

public interface ModelStockIndexClosePriceMA5GoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据
     */
    void writeModelStockIndexClosePriceMA5GoldCross();

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_c_p_ma5_g_c中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
    List<ModelStockIndexClosePriceMA5GoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
