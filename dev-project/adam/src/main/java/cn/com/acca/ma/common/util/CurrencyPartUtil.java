package cn.com.acca.ma.common.util;

import cn.com.acca.ma.enumeration.CurrencyLevel;
import cn.com.acca.ma.enumeration.CurrencyPart;

public class CurrencyPartUtil {
	/**
	 * 根据currency的名称，返回对应的货币对的描述
	 * @param currency
	 * @return
	 */
	public static String getDescription(String currency){
		if(currency.equals(CurrencyPart.AUDUSD.getName())){
			return CurrencyPart.AUDUSD.getDescription();
		}else if(currency.equals(CurrencyPart.EURUSD.getName())){
			return CurrencyPart.EURUSD.getDescription();
		}else if(currency.equals(CurrencyPart.GBPUSD.getName())){
			return CurrencyPart.GBPUSD.getDescription();
		}else if(currency.equals(CurrencyPart.NZDUSD.getName())){
			return CurrencyPart.NZDUSD.getDescription();
		}else if(currency.equals(CurrencyPart.USDCAD.getName())){
			return CurrencyPart.USDCAD.getDescription();
		}else if(currency.equals(CurrencyPart.USDCHF.getName())){
			return CurrencyPart.USDCHF.getDescription();
		}else if(currency.equals(CurrencyPart.USDJPY.getName())){
			return CurrencyPart.USDJPY.getDescription();
		}else if(currency.equals(CurrencyPart.USDNOK.getName())){
			return CurrencyPart.USDNOK.getDescription();
		}else if(currency.equals(CurrencyPart.USDSEK.getName())){
			return CurrencyPart.USDSEK.getDescription();
		}else if(currency.equals(CurrencyPart.USDSGD.getName())){
			return CurrencyPart.USDSGD.getDescription();
		}
		return null;
	}
	
	/**
	 * 从外汇csv文件中获取fileMark标记
	 * @param filename
	 * @return
	 */
	public static Integer getFileMark(String filename){
		String fileMarkString=filename.substring(19, 22);
		if(CurrencyLevel.ONE_WEEK.getFileMark().equals(fileMarkString)){
			return new Integer(1);
		}else if(CurrencyLevel.ONE_DAY.getFileMark().equals(fileMarkString)){
			return new Integer(2);
		}else if(CurrencyLevel.FOUR_HOUR.getFileMark().equals(fileMarkString)){
			return new Integer(3);
		}else if(CurrencyLevel.ONE_HOUR.getFileMark().equals(fileMarkString)){
			return new Integer(4);
		}
		return null;
	}
}
