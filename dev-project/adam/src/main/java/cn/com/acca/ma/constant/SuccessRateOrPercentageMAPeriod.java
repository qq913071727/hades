package cn.com.acca.ma.constant;

/**
 * 成功率或百分比的均线的周期
 */
public class SuccessRateOrPercentageMAPeriod {

    /**
     * 10天
     */
    public static final Integer TEN = 10;

    /**
     * 20天
     */
    public static final Integer TWEENTY = 20;
}
