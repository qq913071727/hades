package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.ModelMACDGoldCrossService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建MACD金叉散点图
 */
public class CreateMACDGoldCrossScatterPlotPictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateMACDGoldCrossScatterPlotPictureThread.class);

    private ModelMACDGoldCrossService modelMACDGoldCrossService;

    public CreateMACDGoldCrossScatterPlotPictureThread() {
    }

    public CreateMACDGoldCrossScatterPlotPictureThread(
        ModelMACDGoldCrossService modelMACDGoldCrossService) {
        this.modelMACDGoldCrossService = modelMACDGoldCrossService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始创建MACD金叉散点图，调用的方法为【createMACDGoldCrossScatterPlotPicture】");

        modelMACDGoldCrossService.createMACDGoldCrossScatterPlotPicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
