package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.WeekDateDao;
import cn.com.acca.ma.jpa.util.JpaUtil;
import cn.com.acca.ma.model.WeekDate;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class WeekDateDaoImpl extends BaseDaoImpl<WeekDateDaoImpl> implements WeekDateDao {

    public WeekDateDaoImpl() {
        super();
    }

    /**
     * 查找开始日期和结束日期在date之间的记录
     *
     * @param date
     * @return
     */
    @Override
    public WeekDate findByDateBetween(Date date) {
        logger.info("查找开始日期和结束日期在【" + date + "】之间的记录");

        String queryString = new String("select * from week_date t where ? between t.begin_date and t.end_date");
        em = JpaUtil.currentEntityManager();
        em.getTransaction().begin();
        Query query = em.createNativeQuery(queryString);
        query.setParameter(1, date);
        Object[] objectArray = (Object[]) query.getSingleResult();
        WeekDate weekDate = new WeekDate();
        weekDate.setId(new Integer(objectArray[0].toString()));
        weekDate.setBeginDate((Date) objectArray[1]);
        weekDate.setEndDate((Date) objectArray[2]);
        em.getTransaction().commit();
        em.close();

        logger.info("查找开始日期和结束日期在【" + date + "】之间的记录结束");
        return weekDate;
    }
}
