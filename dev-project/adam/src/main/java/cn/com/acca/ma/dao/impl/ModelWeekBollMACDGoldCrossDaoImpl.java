package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelWeekBollMACDGoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelWeekBollMACDGoldCross;
import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.sql.ARRAY;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ModelWeekBollMACDGoldCrossDaoImpl extends BaseDaoImpl<ModelWeekBollMACDGoldCross> implements
        ModelWeekBollMACDGoldCrossDao {

    public ModelWeekBollMACDGoldCrossDaoImpl() {
        super();
    }

    /**
     * 海量地向表MDL_WEEK_BOLL_MACD_GOLD_CROSS中插入数据
     */
    @Override
    public void writeModelWeekBollMACDGoldCross() {
        logger.info("开始海量地向表MDL_WEEK_BOLL_MACD_GOLD_CROSS中插入数据");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_WEEK.CAL_MDL_WEEK_BOLL_MACD_G_C()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("海量地向表MDL_WEEK_BOLL_MACD_GOLD_CROSS中插入数据结束");
    }

    /**
     * 查询MACD金叉交易收益率和最高价突破布林带下轨的百分比
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekMACDGoldCrossProfitOrLossPercentageAndBollDn(String beginDate, String endDate) {
        logger.info("开始查询KD金叉交易收益率和最高价突破布林带下轨的百分比的散点图");

        final List<ProfitOrLossAndWeekBollUpOrDnPercentage> profitOrLossAndWeekBollUpOrDnPercentageList = new ArrayList<>();
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                DatabaseMetaData databaseMetadata = connection.getMetaData();
                OracleConnection oracleConnection = (OracleConnection) databaseMetadata.getConnection();
                OracleCallableStatement ocs = (OracleCallableStatement) oracleConnection.prepareCall("{call pkg_model_week.find_w_l_p_d_b_p_a_macd_g_c_p(?,?,?)}");
                ocs.setString(1, beginDate);
                ocs.setString(2, endDate);
                ocs.registerOutParameter(3, oracle.jdbc.OracleTypes.ARRAY, "T_WEEK_BOLL_U_D_P_L_P_ARRAY");
                ocs.execute();
                ARRAY array = ocs.getARRAY(3);
                Datum[] datas = array.getOracleArray();
                if (datas.length > 0) {
                    for (int i = 0; i < datas.length; i++) {
                        if (datas[i] != null && ((STRUCT) datas[i]) != null) {
                            Datum[] stockResultAttributes = ((STRUCT) datas[i]).getOracleAttributes();
                            ProfitOrLossAndWeekBollUpOrDnPercentage profitOrLossAndWeekBollUpOrDnPercentage = new ProfitOrLossAndWeekBollUpOrDnPercentage();
                            profitOrLossAndWeekBollUpOrDnPercentage.setProfitOrLoss(stockResultAttributes[0].doubleValue());
                            profitOrLossAndWeekBollUpOrDnPercentage.setWeekBollUpOrDnPercentage(stockResultAttributes[1].doubleValue());
                            profitOrLossAndWeekBollUpOrDnPercentageList.add(profitOrLossAndWeekBollUpOrDnPercentage);
                        } else {
                            logger.info("datas[" + i + "]是null.");
                        }
                    }
                }
            }
        });
        session.getTransaction().commit();
        session.close();

        return profitOrLossAndWeekBollUpOrDnPercentageList;
    }


}
