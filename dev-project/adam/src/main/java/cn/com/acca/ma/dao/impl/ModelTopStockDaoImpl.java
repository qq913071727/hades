package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.model.ModelMACDGoldCross;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;

import cn.com.acca.ma.dao.ModelTopStockDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelTopStock;
import org.hibernate.Session;

public class ModelTopStockDaoImpl extends BaseDaoImpl<StockTransactionDataDaoImpl> implements ModelTopStockDao {

	/**
	 * 根据开始日期和结束日期，获取ModelTopStock对象
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ModelTopStock> getModelTopStocks(boolean multithreading, String beginDate,String endDate) {
		logger.info("开始根据开始日期【" + beginDate + "】和结束日期【" + endDate + "】，获取ModelTopStock对象");
		
		String hql = "select t from ModelTopStock t where t.date between to_date(?,'yyyy-mm-dd') " +
				"and to_date(?,'yyyy-mm-dd') order by t.date asc";

		Session newSession = null;
		Query query;
		if (multithreading){
			newSession = HibernateUtil.newSession();
			newSession.beginTransaction();
			query = newSession.createQuery(hql);
		} else {
			session = HibernateUtil.currentSession();
			session.beginTransaction();
			query = session.createQuery(hql);
		}

		query.setParameter(0, beginDate);
		query.setParameter(1, endDate);
		List<ModelTopStock> list = query.list();

		if (multithreading){
			newSession.getTransaction().commit();
			HibernateUtil.closeNewSessionFactory(newSession);
		} else {
			session.getTransaction().commit();
			session.close();
		}

		logger.info("根据开始日期【" + beginDate + "】和结束日期【" + endDate + "】，获取ModelTopStock对象完成");
		return list;
	}

	/**
	 * 按照日期，向表MDL_TOP_STOCK中写入数据
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param numberPercentage
	 */
	public void writeModelTopStock(boolean multithreading, String beginDate, String endDate, Double numberPercentage){
		logger.info("write model top stock object begin");

		Session newSession = null;
		SQLQuery query;
		if (multithreading) {
			newSession = HibernateUtil.newSession();
			newSession.beginTransaction();
			query = newSession.createSQLQuery("{call PKG_MODEL_RECORD.WRITE_MDL_TOP_STOCK(?, ?, ?)}");
		} else {
			session = HibernateUtil.currentSession();
			session.beginTransaction();
			query = session.createSQLQuery("{call PKG_MODEL_RECORD.WRITE_MDL_TOP_STOCK(?, ?, ?)}");
		}

		query.setParameter(0, beginDate);
		query.setParameter(1, endDate);
		query.setParameter(2, numberPercentage);
		query.executeUpdate();

		if (multithreading) {
			newSession.getTransaction().commit();
			HibernateUtil.closeNewSessionFactory(newSession);
		} else {
			session.getTransaction().commit();
			session.close();
		}

		logger.info("write model top stock object finish");
	}

	/*********************************************************************************************************************
	 *
	 * 										增量备份表MDL_TOP_STOCK
	 *
	 *********************************************************************************************************************/
	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_TOP_STOCK中获取DATE_字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Date> getDateByCondition(String beginDate, String endDate){
		logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
			+ "从表MDL_TOP_STOCK中获取DATE_字段");

		StringBuffer hql = new StringBuffer("select distinct t.date from ModelTopStock t " +
			"where t.date between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd')");
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql.toString());
		query.setParameter(0, beginDate);
		query.setParameter(1, endDate);
		List<Date> list = query.list();
		session.getTransaction().commit();
		session.close();

		return list;
	}

	/**
	 * 从表MDL_TOP_STOCK中获取date日的所有ModelTopStock对象
	 * @param date
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ModelTopStock> getModelTopStockByDate(String date){
		logger.info("从表MDL_TOP_STOCK中获取日期【" + date + "】的所有ModelTopStock对象");

		StringBuffer hql = new StringBuffer("select t from ModelTopStock t " +
			"where t.date=to_date(?,'yyyy-mm-dd') order by t.date asc");
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql.toString());
		query.setParameter(0, date);
		List<ModelTopStock> list=query.list();
		session.getTransaction().commit();
		session.close();

		return list;
	}

	/**
	 * 向表MDL_TOP_STOCK中插入ModelTopStock对象
	 * @param modelTopStock
	 */
	public void save(ModelTopStock modelTopStock){
		logger.info("向表MDL_TOP_STOCK中插入ModelTopStock对象");

		session = HibernateUtil.currentSession();
		session.beginTransaction();
		session.persist(modelTopStock);
		session.getTransaction().commit();
		session.close();
	}
}
