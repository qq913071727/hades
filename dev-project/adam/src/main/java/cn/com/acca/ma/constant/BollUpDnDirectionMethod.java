package cn.com.acca.ma.constant;

/**
 * 突破布林带上轨或下轨时才用的交易方法
 */
public class BollUpDnDirectionMethod {

    /**
     * 为1时，表示顺着趋势操作，如果突破布林带上轨的股票数量多于跌破布林带下轨的股票的数量，则寻找跌破下轨后金叉的股票做多
     * 为1时，表示顺着趋势操作，如果突破布林带上轨的股票数量少于跌破布林带下轨的股票的数量，则寻找突破上轨后死叉的股票做空
     */
    public static final Integer FOLLOW_TREND_OPERATION = 1;

    /**
     * 为2时，表示逆着趋势操作，如果突破布林带上轨的股票数量多于跌破布林带下轨的股票的数量，则寻找突破上轨后死叉的股票做空
     * 为2时，表示逆着趋势操作，如果突破布林带上轨的股票数量少于跌破布林带下轨的股票的数量，则寻找跌破下轨后金叉的股票做多
     */
    public static final Integer REVERSE_TREND_OPERATION = 2;
}
