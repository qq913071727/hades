package cn.com.acca.ma.enumeration;

public enum UpDown {
	UP(1,"上涨"),DOWN(-1,"下跌"),MIDDLE(0,"平");
	
	private int index;
	private String description;
	
	UpDown(int index, String description) {
		this.index = index;
		this.description = description;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
