package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.dao.Real4StockTransactionRecordDao;
import cn.com.acca.ma.dao.RealStockTransactionRecordDao;
import cn.com.acca.ma.model.Real4StockTransactionRecord;
import cn.com.acca.ma.model.RealStockTransactionRecord;

import java.util.HashMap;
import java.util.List;

public class Real4StockTransactionRecordDaoImpl extends BaseDaoImpl<Real4StockTransactionRecordDao> implements
    Real4StockTransactionRecordDao {

    public Real4StockTransactionRecordDaoImpl() {
        super();
    }

    /**
     * 计算MACD的成功率
     * @param date
     * @param backwardMonth
     * @param averageDateNumber
     * @return
     */
    @Override
    public List<HashMap> calculateMACDSuccessRate(String date, Integer backwardMonth, Integer averageDateNumber) {
        logger.info("计算MACD的成功率，然后计算仓位");

        String sql = "{call PKG_MODEL_RECORD.FIND_MACD_G_C_SUCCESS_RATE(?,?,?,?)}";

        // 参数
        Object[] inParameter = new Object[3];
        inParameter[0] = DateUtil.dateToString(DateUtil.stepMonth(DateUtil.stringToDate(date), - backwardMonth));
        inParameter[1] = date;
        inParameter[2] = averageDateNumber;
        Class[] inParameterType = new Class[3];
        inParameterType[0] = String.class;
        inParameterType[1] = String.class;
        inParameterType[2] = Integer.class;

        // 计算成功率
        return  (List<HashMap>) doProcedureInTransaction(sql, "T_MACD_SUCCESS_RATE_ARRAY", inParameter, inParameterType);
    }

    /**
     * 查找real4_stock_transaction_record表中，sell_date、sell_price、sell_amount列都为空的记录
     */
    @Override
    public List<Real4StockTransactionRecord> findBySellDateNullAndSellPriceNullAndSellAmountNull() {
        logger.info("查找real4_stock_transaction_record表中，sell_date、sell_price、sell_amount列都为空的记录");

        String sql = "select * "
            + "from real4_stock_transaction_record t "
            + "where t.sell_date is null and t.sell_price is null and t.sell_amount is null";

        return doSQLQueryInTransaction(sql, null, Real4StockTransactionRecord.class);
    }

    /**
     * 查找real4_stock_transaction_record表中，buy_date、buy_price、buy_amount列都为空的记录
     */
    @Override
    public List<Real4StockTransactionRecord> findByBuyDateNullAndBuyPriceNullAndBuyAmountNull() {
        logger.info("查找real4_stock_transaction_record表中，buy_date、buy_price、buy_amount列都为空的记录");

        String sql = "select * "
                + "from real4_stock_transaction_record t "
                + "where t.buy_date is null and t.buy_price is null and t.buy_amount is null";

        return doSQLQueryInTransaction(sql, null, Real4StockTransactionRecord.class);
    }

}
