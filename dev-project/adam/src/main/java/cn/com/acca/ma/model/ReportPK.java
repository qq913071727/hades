package cn.com.acca.ma.model;

import java.util.Date;

import javax.persistence.Id;

public class ReportPK {
	
	@Id
	private Date reportDate;
	@Id
	private String reportName;
	
	public ReportPK() {
	}

	public ReportPK(Date reportDate, String reportName) {
		super();
		this.reportDate = reportDate;
		this.reportName = reportName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((reportDate == null) ? 0 : reportDate.hashCode());
		result = prime * result
				+ ((reportName == null) ? 0 : reportName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportPK other = (ReportPK) obj;
		if (reportDate == null) {
			if (other.reportDate != null)
				return false;
		} else if (!reportDate.equals(other.reportDate))
			return false;
		if (reportName == null) {
			if (other.reportName != null)
				return false;
		} else if (!reportName.equals(other.reportName))
			return false;
		return true;
	}
}
