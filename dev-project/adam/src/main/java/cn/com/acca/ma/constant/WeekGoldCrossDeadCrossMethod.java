package cn.com.acca.ma.constant;

/**
 * 周线级别金叉死叉方式
 */
public class WeekGoldCrossDeadCrossMethod {

    /**
     * MACD金叉死叉
     */
    public static final Integer MACD_GOLD_CROSS_DEAD_CROSS = 1;

    /**
     * close_price和ma5金叉死叉
     */
    public static final Integer CLOSE_PRICE_MA5_GOLD_CROSS_DEAD_CROSS = 2;

    /**
     * hei_kin_ashi上涨下跌
     */
    public static final Integer HEI_KIN_ASHI_UP_DOWN = 3;

    /**
     * kd金叉死叉
     */
    public static final Integer KD_GOLD_CROSS_DEAD_CROSS = 4;
}
