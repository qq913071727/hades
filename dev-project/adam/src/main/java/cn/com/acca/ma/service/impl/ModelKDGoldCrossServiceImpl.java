package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelKDGoldCross;
import cn.com.acca.ma.model.ModelWeekKDGoldCross;
import cn.com.acca.ma.service.ModelKDGoldCrossService;
import java.util.List;

public class ModelKDGoldCrossServiceImpl extends BaseServiceImpl<ModelKDGoldCrossServiceImpl, ModelKDGoldCross> implements
	ModelKDGoldCrossService {

	/*********************************************************************************************************************
	 *
	 * 												按日期计算数据
	 *
	 *********************************************************************************************************************/
	/**
	 * 根据日期endDate，使用KD金叉模型算法，向表MDL_KD_GOLD_CROSS插入数据
	 */
	public void writeModelKDGoldCrossIncr() {
		String endDate = PropertiesUtil
			.getValue(MODEL_KD_GOLD_CROSS_PROPERTIES, "kd.gold.cross.end_date");
		modelKDGoldCrossDao.writeModelKDGoldCrossIncr(endDate);
	}
	
	/*********************************************************************************************************************
	 * 
	 * 												计算全部数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用周线级别KD金叉模型算法，向表MDL_KD_GOLD_CROSS插入数据
	 */
	public void writeModelKDGoldCross() {
		modelKDGoldCrossDao.writeModelKDGoldCross();
	}

	@Override
	public String listToString(List list) {
		// TODO Auto-generated method stub
		return null;
	}
}
