package cn.com.acca.ma.xmlmapper;

public interface StockMonthMapper {

    /**************************************************************************************************
     *
     * 									       更新周线级别，所有月份的股票数据
     *
     **************************************************************************************************/
    /**
     * 计算月线级别，所有股票的基础数据
     */
    void writeStockMonth();

    /**
     * 计算月线级别，所有股票的5日均线
     */
    void writeStockMonthMA5();

    /**
     * 计算月线级别，所有股票的10日均线
     */
    void writeStockMonthMA10();

    /**
     * 计算月线级别，所有股票的20日均线
     */
    void writeStockMonthMA20();

    /**
     * 计算月线级别，所有股票的60日均线
     */
    void writeStockMonthMA60();

    /**
     * 计算月线级别，所有股票的120日均线
     */
    void writeStockMonthMA120();

    /**
     * 计算月线级别，所有股票的250日均线
     */
    void writeStockMonthMA250();

    /**
     * 计算月线级别，所有股票的KD，执行存储过程WRITE_MONTH_KD_INIT
     */
    void writeStockMonthKD_init();

    /**
     * 计算月线级别，所有股票的KD，执行存储过程WRITE_MONTH_KD_RSV
     */
    void writeStockMonthKD_rsv();

    /**
     * 计算月线级别，所有股票的KD，执行存储过程WRITE_MONTH_KD_K
     */
    void writeStockMonthKD_k();

    /**
     * 计算月线级别，所有股票的KD，执行存储过程WRITE_MONTH_KD_D
     */
    void writeStockMonthKD_d();

    /**
     * 计算月线级别，所有股票的MACD，执行存储过程WRITE_MONTH_MACD_INIT
     */
    void writeStockMonthMACD_init();

    /**
     * 计算月线级别，所有股票的MACD，执行存储过程WRITE_MONTH_MACD_EMA
     */
    void writeStockMonthMACD_ema();

    /**
     * 计算月线级别，所有股票的MACD，执行存储过程WRITE_MONTH_MACD_DIF
     */
    void writeStockMonthMACD_dif();

    /**
     * 计算月线级别，所有股票的MACD，执行存储过程WRITE_MONTH_MACD_DEA
     */
    void writeStockMonthMACD_dea();

    /**
     * 计算月线级别，所有股票的MACD，执行存储过程WRITE_MONTH_MACD
     */
    void writeStockMonthMACD_macd();

    /**************************************************************************************************
     *
     * 									       更新周线级别，某一月的股票数据
     *
     **************************************************************************************************/
    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的基础数据
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthByDate(String beginDate, String endDate);

    /**
     * 根据每个月的开始时间和结束时间，计算某一个月的所有股票的移动平均线
     * @param currentMonthBeginDate
     * @param currentMonthEndDate
     */
    void writeStockMonthMAByDate(String currentMonthBeginDate, String currentMonthEndDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的KD，执行存储过程WRITE_MONTH_KD_BY_DATE_RSV
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthKDByDate_rsv(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的KD，执行存储过程WRITE_MONTH_KD_BY_DATE_K
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthKDByDate_k(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的KD，执行存储过程WRITE_MONTH_KD_BY_DATE_D
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthKDByDate_d(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的MACD，执行存储过程WRITE_MONTH_MACD_EMA_BY_DATE
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthMACDByDate_ema(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的MACD，执行存储过程WRITE_MONTH_MACD_DIF_BY_DATE
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthMACDByDate_dif(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的MACD，执行存储过程WRITE_MONTH_MACD_DEA_BY_DATE
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthMACDByDate_dea(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的MACD，执行存储过程WRITE_MONTH_MACD_BY_DATE
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthMACDByDate(String beginDate, String endDate);
}
