package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 周线级别，收盘价突破布林带上轨或下轨的股票的数量
 */
@Data
public class StockWeekBollUpDnCount {

    /**
     * 收盘价突破布林带上轨的股票的数量
     */
    private BigDecimal closePriceUpBollStockCount;

    /**
     * 收盘价跌破布林带下轨的股票的数量
     */
    private BigDecimal closePriceDnBollStockCount;
}
