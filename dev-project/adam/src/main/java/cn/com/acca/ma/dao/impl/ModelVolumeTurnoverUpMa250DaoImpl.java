package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelKDGoldCrossDao;
import cn.com.acca.ma.dao.ModelVolumeTurnoverUpMa250Dao;
import cn.com.acca.ma.hibernate.impl.GetKdGoldCrossSuccessRateWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelKDGoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelVolumeTurnoverUpMa250DaoImpl extends BaseDaoImpl<ModelVolumeTurnoverUpMa250DaoImpl> implements
		ModelVolumeTurnoverUpMa250Dao {

	public ModelVolumeTurnoverUpMa250DaoImpl() {
		super();
	}

	/**
	 * 根据日期endDate，使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据
	 * @param endDate
	 */
	@Override
	public void writeModelVolumeTurnoverUpMa250Incr(String endDate) {
		logger.info("根据日期【" + endDate + "】，使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据");

		session= HibernateUtil.currentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_V_T_UP_MA250_INCR(?)}");
		query.setString(0, endDate);
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();

		logger.info("日线级别使用成交量/成交额突破MA250模型算法结束");
	}

	/**
	 * 使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据
	 */
	public void writeModelVolumeTurnoverUpMa250() {
		logger.info("开始使用成交量/成交额突破MA250模型算法");
		
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_V_T_UP_MA250()}");
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
		
		logger.info("使用成交量/成交额突破MA250模型算法结束");
	}
}
