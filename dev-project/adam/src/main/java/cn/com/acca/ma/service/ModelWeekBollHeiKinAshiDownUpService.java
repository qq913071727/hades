package cn.com.acca.ma.service;

public interface ModelWeekBollHeiKinAshiDownUpService extends BaseService {

    /**
     * 海量地向表MDL_WEEK_BOLL_HEI_KIN_ASHI_DOWN_UP中插入数据
     */
    void writeModelWeekBollHeiKinAshiDownUp();

    /**
     * 创建周线级别最高价突破布林带上轨的百分比和hei_kin_ashi下跌趋势交易收益率的散点图，x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨
     */
    void createWeekHeighestPriceUpBollPercentAndHeiKinAshiDownUpProfitScatterPlotPicture();
}
