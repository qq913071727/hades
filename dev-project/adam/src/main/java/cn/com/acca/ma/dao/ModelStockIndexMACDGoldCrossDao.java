package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexMACDGoldCross;

import java.util.List;

public interface ModelStockIndexMACDGoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD金叉模型算法，向表MDL_STOCK_INDEX_MACD_G_C插入数据
     */
    void writeModelStockIndexMACDGoldCross();

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_macd_g_c中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
    List<ModelStockIndexMACDGoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
