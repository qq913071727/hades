package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 金叉死叉分析
 */
//@Data
@Entity
@Table(name = "MDL_G_C_D_C_ANALYSIS")
public class ModelGoldCrossDeadCrossAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_")
    private Integer id;

    /**
     * 日期
     */
    @Column(name = "DATE_")
    private Date date;

    /**
     * macd金叉百分比
     */
    @Column(name = "MACD_G_C_PERCENT")
    private BigDecimal macdGoldCrossPercent;

    /**
     * macd死叉百分比
     */
    @Column(name = "MACD_D_C_PERCENT")
    private BigDecimal macdDeadCrossPercent;

    /**
     * close_price死叉ma5百分比
     */
    @Column(name = "CLOSE_PRICE_D_C_MA5_PERCENT")
    private BigDecimal closePriceDeadCrossMA5Percent;

    /**
     * hei_kin_ashi上涨趋势百分比
     */
    @Column(name = "HEI_KIN_ASHI_U_D_PERCENT")
    private BigDecimal heiKinAshiUpDownPercent;

    /**
     * hei_kin_ashi下跌趋势百分比
     */
    @Column(name = "HEI_KIN_ASHI_D_U_PERCENT")
    private BigDecimal heiKinAshiUpDownpPercent;

    /**
     * kd金叉百分比
     */
    @Column(name = "KD_G_C_PERCENT")
    private BigDecimal kdGoldCrossPercent;

    /**
     * kd死叉百分比
     */
    @Column(name = "KD_D_C_PERCENT")
    private BigDecimal kdDeadCrossPercent;

    /**
     * macd金叉百分比的5日均值
     */
    @Column(name = "MACD_G_C_PERCENT_MA5")
    private BigDecimal macdGoldCrossPercentMA5;

    /**
     * macd金叉百分比的10日均值
     */
    @Column(name = "MACD_G_C_PERCENT_MA10")
    private BigDecimal macdGoldCrossPercentMA10;

    /**
     * macd金叉百分比的20日均值
     */
    @Column(name = "MACD_G_C_PERCENT_MA20")
    private BigDecimal macdGoldCrossPercentMA20;

    /**
     * macd死叉百分比的5日均值
     */
    @Column(name = "MACD_D_C_PERCENT_MA5")
    private BigDecimal macdDeadCrossPercentMA5;

    /**
     * macd死叉百分比的10日均值
     */
    @Column(name = "MACD_D_C_PERCENT_MA10")
    private BigDecimal macdDeadCrossPercentMA10;

    /**
     * macd死叉百分比的20日均值
     */
    @Column(name = "MACD_D_C_PERCENT_MA20")
    private BigDecimal macdDeadCrossPercentMA20;

    /**
     * close_price金叉ma5百分比的5日均值
     */
    @Column(name = "C_P_G_C_MA5_PERCENT_MA5")
    private BigDecimal closePriceGoldCrossMA5PercentMA5;

    /**
     * close_price金叉ma5百分比的10日均值
     */
    @Column(name = "C_P_G_C_MA5_PERCENT_MA10")
    private BigDecimal closePriceGoldCrossMA5PercentMA10;

    /**
     * close_price金叉ma5百分比的20日均值
     */
    @Column(name = "C_P_G_C_MA5_PERCENT_MA20")
    private BigDecimal closePriceGoldCrossMA5PercentMA20;

    /**
     * close_price死叉ma5百分比的5日均值
     */
    @Column(name = "C_P_D_C_MA5_PERCENT_MA5")
    private BigDecimal closePriceDeadCrossMA5PercentMA5;

    /**
     * close_price死叉ma5百分比的10日均值
     */
    @Column(name = "C_P_D_C_MA5_PERCENT_MA10")
    private BigDecimal closePriceDeadCrossMA5PercentMA10;

    /**
     * close_price死叉ma5百分比的20日均值
     */
    @Column(name = "C_P_D_C_MA5_PERCENT_MA20")
    private BigDecimal closePriceDeadCrossMA5PercentMA20;

    /**
     * hei_kin_ashi上涨趋势百分比的5日均值
     */
    @Column(name = "HEI_KIN_ASHI_U_D_PERCENT_MA5")
    private BigDecimal heiKinAshiUpDownPercentMA5;

    /**
     * hei_kin_ashi上涨趋势百分比的10日均值
     */
    @Column(name = "HEI_KIN_ASHI_U_D_PERCENT_MA10")
    private BigDecimal heiKinAshiUpDownPercentMA10;

    /**
     * hei_kin_ashi上涨趋势百分比的20日均值
     */
    @Column(name = "HEI_KIN_ASHI_U_D_PERCENT_MA20")
    private BigDecimal heiKinAshiUpDownPercentMA20;

    /**
     * hei_kin_ashi下跌趋势百分比的5日均值
     */
    @Column(name = "HEI_KIN_ASHI_D_U_PERCENT_MA5")
    private BigDecimal heiKinAshiDownUpPercentMA5;

    /**
     * hei_kin_ashi下跌趋势百分比的10日均值
     */
    @Column(name = "HEI_KIN_ASHI_D_U_PERCENT_MA10")
    private BigDecimal heiKinAshiDownUpPercentMA10;

    /**
     * hei_kin_ashi下跌趋势百分比的20日均值
     */
    @Column(name = "HEI_KIN_ASHI_D_U_PERCENT_MA20")
    private BigDecimal heiKinAshiDownUpPercentMA20;

    /**
     * kd金叉百分比的5日均值
     */
    @Column(name = "KD_G_C_PERCENT_MA5")
    private BigDecimal kdGoldCrossPercentMA5;

    /**
     * kd金叉百分比的10日均值
     */
    @Column(name = "KD_G_C_PERCENT_MA10")
    private BigDecimal kdGoldCrossPercentMA10;

    /**
     * kd金叉百分比的20日均值
     */
    @Column(name = "KD_G_C_PERCENT_MA20")
    private BigDecimal kdGoldCrossPercentMA20;

    /**
     * kd死叉百分比的5日均值
     */
    @Column(name = "KD_D_C_PERCENT_MA5")
    private BigDecimal kdDeadCrossPercentMA5;

    /**
     * kd死叉百分比的10日均值
     */
    @Column(name = "KD_D_C_PERCENT_MA10")
    private BigDecimal kdDeadCrossPercentMA10;

    /**
     * kd死叉百分比的20日均值
     */
    @Column(name = "KD_D_C_PERCENT_MA20")
    private BigDecimal kdDeadCrossPercentMA20;

    /**
     * macd金叉成功率的5日均值
     */
    @Column(name = "MACD_G_C_SUCCESS_RATE_MA5")
    private BigDecimal macdGoldCrossSuccessRateMA5;

    /**
     * macd金叉成功率的10日均值
     */
    @Column(name = "MACD_G_C_SUCCESS_RATE_MA10")
    private BigDecimal macdGoldCrossSuccessRateMA10;

    /**
     * macd金叉成功率的20日均值
     */
    @Column(name = "MACD_G_C_SUCCESS_RATE_MA20")
    private BigDecimal macdGoldCrossSuccessRateMA20;

    /**
     * macd死叉成功率的5日均值
     */
    @Column(name = "MACD_D_C_SUCCESS_RATE_MA5")
    private BigDecimal macdDeadCrossSuccessRateMA5;

    /**
     * macd死叉成功率的10日均值
     */
    @Column(name = "MACD_D_C_SUCCESS_RATE_MA10")
    private BigDecimal macdDeadCrossSuccessRateMA10;

    /**
     * macd死叉成功率的20日均值
     */
    @Column(name = "MACD_D_C_SUCCESS_RATE_MA20")
    private BigDecimal macdDeadCrossSuccessRateMA20;

    /**
     * close_price金叉ma5成功率的5日均值
     */
    @Column(name = "C_P_G_C_MA5_SUCCESS_RATE_MA5")
    private BigDecimal closePriceGoldCrossMA5SuccessRateMA5;

    /**
     * close_price金叉ma5成功率的10日均值
     */
    @Column(name = "C_P_G_C_MA5_SUCCESS_RATE_MA10")
    private BigDecimal closePriceGoldCrossMA5SuccessRateMA10;

    /**
     * close_price金叉ma5成功率的20日均值
     */
    @Column(name = "C_P_G_C_MA5_SUCCESS_RATE_MA20")
    private BigDecimal closePriceGoldCrossMA5SuccessRateMA20;

    /**
     * close_price死叉ma5成功率的5日均值
     */
    @Column(name = "C_P_D_C_MA5_SUCCESS_RATE_MA5")
    private BigDecimal closePriceDeadCrossMA5SuccessRateMA5;

    /**
     * close_price死叉ma5成功率的10日均值
     */
    @Column(name = "C_P_D_C_MA5_SUCCESS_RATE_MA10")
    private BigDecimal closePriceDeadCrossMA5SuccessRateMA10;

    /**
     * close_price死叉ma5成功率的20日均值
     */
    @Column(name = "C_P_D_C_MA5_SUCCESS_RATE_MA20")
    private BigDecimal closePriceDeadCrossMA5SuccessRateMA20;

    /**
     * hei_kin_ashi上涨趋势成功率的5日均值
     */
    @Column(name = "HEI_KIN_ASHI_U_D_S_R_MA5")
    private BigDecimal heiKinAshiUpDownSuccessRateMA5;

    /**
     * hei_kin_ashi上涨趋势成功率的10日均值
     */
    @Column(name = "HEI_KIN_ASHI_U_D_S_R_MA10")
    private BigDecimal heiKinAshiUpDownSuccessRateMA10;

    /**
     * hei_kin_ashi上涨趋势成功率的20日均值
     */
    @Column(name = "HEI_KIN_ASHI_U_D_S_R_MA20")
    private BigDecimal heiKinAshiUpDownSuccessRateMA20;

    /**
     * hei_kin_ashi下跌趋势成功率的5日均值
     */
    @Column(name = "HEI_KIN_ASHI_D_U_S_R_MA5")
    private BigDecimal heiKinAshiDownUpSuccessRateMA5;

    /**
     * hei_kin_ashi下跌趋势成功率的10日均值
     */
    @Column(name = "HEI_KIN_ASHI_D_U_S_R_MA10")
    private BigDecimal heiKinAshiDownUpSuccessRateMA10;

    /**
     * hei_kin_ashi下跌趋势成功率的20日均值
     */
    @Column(name = "HEI_KIN_ASHI_D_U_S_R_MA20")
    private BigDecimal heiKinAshiDownUpSuccessRateMA20;

    /**
     * kd金叉成功率的5日均值
     */
    @Column(name = "KD_G_C_SUCCESS_RATE_MA5")
    private BigDecimal kdGoldCrossSuccessRateMA5;

    /**
     * kd金叉成功率的10日均值
     */
    @Column(name = "KD_G_C_SUCCESS_RATE_MA10")
    private BigDecimal kdGoldCrossSuccessRateMA10;

    /**
     * kd金叉成功率的20日均值
     */
    @Column(name = "KD_G_C_SUCCESS_RATE_MA20")
    private BigDecimal kdGoldCrossSuccessRateMA20;

    /**
     * kd死叉成功率的5日均值
     */
    @Column(name = "KD_D_C_SUCCESS_RATE_MA5")
    private BigDecimal kdDeadCrossSuccessRateMA5;

    /**
     * kd死叉成功率的10日均值
     */
    @Column(name = "KD_D_C_SUCCESS_RATE_MA10")
    private BigDecimal kdDeadCrossSuccessRateMA10;

    /**
     * kd死叉成功率的20日均值
     */
    @Column(name = "KD_D_C_SUCCESS_RATE_MA20")
    private BigDecimal kdDeadCrossSuccessRateMA20;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getMacdGoldCrossPercent() {
        return macdGoldCrossPercent;
    }

    public void setMacdGoldCrossPercent(BigDecimal macdGoldCrossPercent) {
        this.macdGoldCrossPercent = macdGoldCrossPercent;
    }

    public BigDecimal getMacdDeadCrossPercent() {
        return macdDeadCrossPercent;
    }

    public void setMacdDeadCrossPercent(BigDecimal macdDeadCrossPercent) {
        this.macdDeadCrossPercent = macdDeadCrossPercent;
    }

    public BigDecimal getClosePriceDeadCrossMA5Percent() {
        return closePriceDeadCrossMA5Percent;
    }

    public void setClosePriceDeadCrossMA5Percent(BigDecimal closePriceDeadCrossMA5Percent) {
        this.closePriceDeadCrossMA5Percent = closePriceDeadCrossMA5Percent;
    }

    public BigDecimal getHeiKinAshiUpDownPercent() {
        return heiKinAshiUpDownPercent;
    }

    public void setHeiKinAshiUpDownPercent(BigDecimal heiKinAshiUpDownPercent) {
        this.heiKinAshiUpDownPercent = heiKinAshiUpDownPercent;
    }

    public BigDecimal getHeiKinAshiUpDownpPercent() {
        return heiKinAshiUpDownpPercent;
    }

    public void setHeiKinAshiUpDownpPercent(BigDecimal heiKinAshiUpDownpPercent) {
        this.heiKinAshiUpDownpPercent = heiKinAshiUpDownpPercent;
    }

    public BigDecimal getKdGoldCrossPercent() {
        return kdGoldCrossPercent;
    }

    public void setKdGoldCrossPercent(BigDecimal kdGoldCrossPercent) {
        this.kdGoldCrossPercent = kdGoldCrossPercent;
    }

    public BigDecimal getKdDeadCrossPercent() {
        return kdDeadCrossPercent;
    }

    public void setKdDeadCrossPercent(BigDecimal kdDeadCrossPercent) {
        this.kdDeadCrossPercent = kdDeadCrossPercent;
    }

    public BigDecimal getMacdGoldCrossPercentMA5() {
        return macdGoldCrossPercentMA5;
    }

    public void setMacdGoldCrossPercentMA5(BigDecimal macdGoldCrossPercentMA5) {
        this.macdGoldCrossPercentMA5 = macdGoldCrossPercentMA5;
    }

    public BigDecimal getMacdGoldCrossPercentMA10() {
        return macdGoldCrossPercentMA10;
    }

    public void setMacdGoldCrossPercentMA10(BigDecimal macdGoldCrossPercentMA10) {
        this.macdGoldCrossPercentMA10 = macdGoldCrossPercentMA10;
    }

    public BigDecimal getMacdGoldCrossPercentMA20() {
        return macdGoldCrossPercentMA20;
    }

    public void setMacdGoldCrossPercentMA20(BigDecimal macdGoldCrossPercentMA20) {
        this.macdGoldCrossPercentMA20 = macdGoldCrossPercentMA20;
    }

    public BigDecimal getMacdDeadCrossPercentMA5() {
        return macdDeadCrossPercentMA5;
    }

    public void setMacdDeadCrossPercentMA5(BigDecimal macdDeadCrossPercentMA5) {
        this.macdDeadCrossPercentMA5 = macdDeadCrossPercentMA5;
    }

    public BigDecimal getMacdDeadCrossPercentMA10() {
        return macdDeadCrossPercentMA10;
    }

    public void setMacdDeadCrossPercentMA10(BigDecimal macdDeadCrossPercentMA10) {
        this.macdDeadCrossPercentMA10 = macdDeadCrossPercentMA10;
    }

    public BigDecimal getMacdDeadCrossPercentMA20() {
        return macdDeadCrossPercentMA20;
    }

    public void setMacdDeadCrossPercentMA20(BigDecimal macdDeadCrossPercentMA20) {
        this.macdDeadCrossPercentMA20 = macdDeadCrossPercentMA20;
    }

    public BigDecimal getClosePriceGoldCrossMA5PercentMA5() {
        return closePriceGoldCrossMA5PercentMA5;
    }

    public void setClosePriceGoldCrossMA5PercentMA5(BigDecimal closePriceGoldCrossMA5PercentMA5) {
        this.closePriceGoldCrossMA5PercentMA5 = closePriceGoldCrossMA5PercentMA5;
    }

    public BigDecimal getClosePriceGoldCrossMA5PercentMA10() {
        return closePriceGoldCrossMA5PercentMA10;
    }

    public void setClosePriceGoldCrossMA5PercentMA10(BigDecimal closePriceGoldCrossMA5PercentMA10) {
        this.closePriceGoldCrossMA5PercentMA10 = closePriceGoldCrossMA5PercentMA10;
    }

    public BigDecimal getClosePriceGoldCrossMA5PercentMA20() {
        return closePriceGoldCrossMA5PercentMA20;
    }

    public void setClosePriceGoldCrossMA5PercentMA20(BigDecimal closePriceGoldCrossMA5PercentMA20) {
        this.closePriceGoldCrossMA5PercentMA20 = closePriceGoldCrossMA5PercentMA20;
    }

    public BigDecimal getClosePriceDeadCrossMA5PercentMA5() {
        return closePriceDeadCrossMA5PercentMA5;
    }

    public void setClosePriceDeadCrossMA5PercentMA5(BigDecimal closePriceDeadCrossMA5PercentMA5) {
        this.closePriceDeadCrossMA5PercentMA5 = closePriceDeadCrossMA5PercentMA5;
    }

    public BigDecimal getClosePriceDeadCrossMA5PercentMA10() {
        return closePriceDeadCrossMA5PercentMA10;
    }

    public void setClosePriceDeadCrossMA5PercentMA10(BigDecimal closePriceDeadCrossMA5PercentMA10) {
        this.closePriceDeadCrossMA5PercentMA10 = closePriceDeadCrossMA5PercentMA10;
    }

    public BigDecimal getClosePriceDeadCrossMA5PercentMA20() {
        return closePriceDeadCrossMA5PercentMA20;
    }

    public void setClosePriceDeadCrossMA5PercentMA20(BigDecimal closePriceDeadCrossMA5PercentMA20) {
        this.closePriceDeadCrossMA5PercentMA20 = closePriceDeadCrossMA5PercentMA20;
    }

    public BigDecimal getHeiKinAshiUpDownPercentMA5() {
        return heiKinAshiUpDownPercentMA5;
    }

    public void setHeiKinAshiUpDownPercentMA5(BigDecimal heiKinAshiUpDownPercentMA5) {
        this.heiKinAshiUpDownPercentMA5 = heiKinAshiUpDownPercentMA5;
    }

    public BigDecimal getHeiKinAshiUpDownPercentMA10() {
        return heiKinAshiUpDownPercentMA10;
    }

    public void setHeiKinAshiUpDownPercentMA10(BigDecimal heiKinAshiUpDownPercentMA10) {
        this.heiKinAshiUpDownPercentMA10 = heiKinAshiUpDownPercentMA10;
    }

    public BigDecimal getHeiKinAshiUpDownPercentMA20() {
        return heiKinAshiUpDownPercentMA20;
    }

    public void setHeiKinAshiUpDownPercentMA20(BigDecimal heiKinAshiUpDownPercentMA20) {
        this.heiKinAshiUpDownPercentMA20 = heiKinAshiUpDownPercentMA20;
    }

    public BigDecimal getHeiKinAshiDownUpPercentMA5() {
        return heiKinAshiDownUpPercentMA5;
    }

    public void setHeiKinAshiDownUpPercentMA5(BigDecimal heiKinAshiDownUpPercentMA5) {
        this.heiKinAshiDownUpPercentMA5 = heiKinAshiDownUpPercentMA5;
    }

    public BigDecimal getHeiKinAshiDownUpPercentMA10() {
        return heiKinAshiDownUpPercentMA10;
    }

    public void setHeiKinAshiDownUpPercentMA10(BigDecimal heiKinAshiDownUpPercentMA10) {
        this.heiKinAshiDownUpPercentMA10 = heiKinAshiDownUpPercentMA10;
    }

    public BigDecimal getHeiKinAshiDownUpPercentMA20() {
        return heiKinAshiDownUpPercentMA20;
    }

    public void setHeiKinAshiDownUpPercentMA20(BigDecimal heiKinAshiDownUpPercentMA20) {
        this.heiKinAshiDownUpPercentMA20 = heiKinAshiDownUpPercentMA20;
    }

    public BigDecimal getKdGoldCrossPercentMA5() {
        return kdGoldCrossPercentMA5;
    }

    public void setKdGoldCrossPercentMA5(BigDecimal kdGoldCrossPercentMA5) {
        this.kdGoldCrossPercentMA5 = kdGoldCrossPercentMA5;
    }

    public BigDecimal getKdGoldCrossPercentMA10() {
        return kdGoldCrossPercentMA10;
    }

    public void setKdGoldCrossPercentMA10(BigDecimal kdGoldCrossPercentMA10) {
        this.kdGoldCrossPercentMA10 = kdGoldCrossPercentMA10;
    }

    public BigDecimal getKdGoldCrossPercentMA20() {
        return kdGoldCrossPercentMA20;
    }

    public void setKdGoldCrossPercentMA20(BigDecimal kdGoldCrossPercentMA20) {
        this.kdGoldCrossPercentMA20 = kdGoldCrossPercentMA20;
    }

    public BigDecimal getKdDeadCrossPercentMA5() {
        return kdDeadCrossPercentMA5;
    }

    public void setKdDeadCrossPercentMA5(BigDecimal kdDeadCrossPercentMA5) {
        this.kdDeadCrossPercentMA5 = kdDeadCrossPercentMA5;
    }

    public BigDecimal getKdDeadCrossPercentMA10() {
        return kdDeadCrossPercentMA10;
    }

    public void setKdDeadCrossPercentMA10(BigDecimal kdDeadCrossPercentMA10) {
        this.kdDeadCrossPercentMA10 = kdDeadCrossPercentMA10;
    }

    public BigDecimal getKdDeadCrossPercentMA20() {
        return kdDeadCrossPercentMA20;
    }

    public void setKdDeadCrossPercentMA20(BigDecimal kdDeadCrossPercentMA20) {
        this.kdDeadCrossPercentMA20 = kdDeadCrossPercentMA20;
    }

    public BigDecimal getMacdGoldCrossSuccessRateMA5() {
        return macdGoldCrossSuccessRateMA5;
    }

    public void setMacdGoldCrossSuccessRateMA5(BigDecimal macdGoldCrossSuccessRateMA5) {
        this.macdGoldCrossSuccessRateMA5 = macdGoldCrossSuccessRateMA5;
    }

    public BigDecimal getMacdGoldCrossSuccessRateMA10() {
        return macdGoldCrossSuccessRateMA10;
    }

    public void setMacdGoldCrossSuccessRateMA10(BigDecimal macdGoldCrossSuccessRateMA10) {
        this.macdGoldCrossSuccessRateMA10 = macdGoldCrossSuccessRateMA10;
    }

    public BigDecimal getMacdGoldCrossSuccessRateMA20() {
        return macdGoldCrossSuccessRateMA20;
    }

    public void setMacdGoldCrossSuccessRateMA20(BigDecimal macdGoldCrossSuccessRateMA20) {
        this.macdGoldCrossSuccessRateMA20 = macdGoldCrossSuccessRateMA20;
    }

    public BigDecimal getMacdDeadCrossSuccessRateMA5() {
        return macdDeadCrossSuccessRateMA5;
    }

    public void setMacdDeadCrossSuccessRateMA5(BigDecimal macdDeadCrossSuccessRateMA5) {
        this.macdDeadCrossSuccessRateMA5 = macdDeadCrossSuccessRateMA5;
    }

    public BigDecimal getMacdDeadCrossSuccessRateMA10() {
        return macdDeadCrossSuccessRateMA10;
    }

    public void setMacdDeadCrossSuccessRateMA10(BigDecimal macdDeadCrossSuccessRateMA10) {
        this.macdDeadCrossSuccessRateMA10 = macdDeadCrossSuccessRateMA10;
    }

    public BigDecimal getMacdDeadCrossSuccessRateMA20() {
        return macdDeadCrossSuccessRateMA20;
    }

    public void setMacdDeadCrossSuccessRateMA20(BigDecimal macdDeadCrossSuccessRateMA20) {
        this.macdDeadCrossSuccessRateMA20 = macdDeadCrossSuccessRateMA20;
    }

    public BigDecimal getClosePriceGoldCrossMA5SuccessRateMA5() {
        return closePriceGoldCrossMA5SuccessRateMA5;
    }

    public void setClosePriceGoldCrossMA5SuccessRateMA5(BigDecimal closePriceGoldCrossMA5SuccessRateMA5) {
        this.closePriceGoldCrossMA5SuccessRateMA5 = closePriceGoldCrossMA5SuccessRateMA5;
    }

    public BigDecimal getClosePriceGoldCrossMA5SuccessRateMA10() {
        return closePriceGoldCrossMA5SuccessRateMA10;
    }

    public void setClosePriceGoldCrossMA5SuccessRateMA10(BigDecimal closePriceGoldCrossMA5SuccessRateMA10) {
        this.closePriceGoldCrossMA5SuccessRateMA10 = closePriceGoldCrossMA5SuccessRateMA10;
    }

    public BigDecimal getClosePriceGoldCrossMA5SuccessRateMA20() {
        return closePriceGoldCrossMA5SuccessRateMA20;
    }

    public void setClosePriceGoldCrossMA5SuccessRateMA20(BigDecimal closePriceGoldCrossMA5SuccessRateMA20) {
        this.closePriceGoldCrossMA5SuccessRateMA20 = closePriceGoldCrossMA5SuccessRateMA20;
    }

    public BigDecimal getClosePriceDeadCrossMA5SuccessRateMA5() {
        return closePriceDeadCrossMA5SuccessRateMA5;
    }

    public void setClosePriceDeadCrossMA5SuccessRateMA5(BigDecimal closePriceDeadCrossMA5SuccessRateMA5) {
        this.closePriceDeadCrossMA5SuccessRateMA5 = closePriceDeadCrossMA5SuccessRateMA5;
    }

    public BigDecimal getClosePriceDeadCrossMA5SuccessRateMA10() {
        return closePriceDeadCrossMA5SuccessRateMA10;
    }

    public void setClosePriceDeadCrossMA5SuccessRateMA10(BigDecimal closePriceDeadCrossMA5SuccessRateMA10) {
        this.closePriceDeadCrossMA5SuccessRateMA10 = closePriceDeadCrossMA5SuccessRateMA10;
    }

    public BigDecimal getClosePriceDeadCrossMA5SuccessRateMA20() {
        return closePriceDeadCrossMA5SuccessRateMA20;
    }

    public void setClosePriceDeadCrossMA5SuccessRateMA20(BigDecimal closePriceDeadCrossMA5SuccessRateMA20) {
        this.closePriceDeadCrossMA5SuccessRateMA20 = closePriceDeadCrossMA5SuccessRateMA20;
    }

    public BigDecimal getHeiKinAshiUpDownSuccessRateMA5() {
        return heiKinAshiUpDownSuccessRateMA5;
    }

    public void setHeiKinAshiUpDownSuccessRateMA5(BigDecimal heiKinAshiUpDownSuccessRateMA5) {
        this.heiKinAshiUpDownSuccessRateMA5 = heiKinAshiUpDownSuccessRateMA5;
    }

    public BigDecimal getHeiKinAshiUpDownSuccessRateMA10() {
        return heiKinAshiUpDownSuccessRateMA10;
    }

    public void setHeiKinAshiUpDownSuccessRateMA10(BigDecimal heiKinAshiUpDownSuccessRateMA10) {
        this.heiKinAshiUpDownSuccessRateMA10 = heiKinAshiUpDownSuccessRateMA10;
    }

    public BigDecimal getHeiKinAshiUpDownSuccessRateMA20() {
        return heiKinAshiUpDownSuccessRateMA20;
    }

    public void setHeiKinAshiUpDownSuccessRateMA20(BigDecimal heiKinAshiUpDownSuccessRateMA20) {
        this.heiKinAshiUpDownSuccessRateMA20 = heiKinAshiUpDownSuccessRateMA20;
    }

    public BigDecimal getHeiKinAshiDownUpSuccessRateMA5() {
        return heiKinAshiDownUpSuccessRateMA5;
    }

    public void setHeiKinAshiDownUpSuccessRateMA5(BigDecimal heiKinAshiDownUpSuccessRateMA5) {
        this.heiKinAshiDownUpSuccessRateMA5 = heiKinAshiDownUpSuccessRateMA5;
    }

    public BigDecimal getHeiKinAshiDownUpSuccessRateMA10() {
        return heiKinAshiDownUpSuccessRateMA10;
    }

    public void setHeiKinAshiDownUpSuccessRateMA10(BigDecimal heiKinAshiDownUpSuccessRateMA10) {
        this.heiKinAshiDownUpSuccessRateMA10 = heiKinAshiDownUpSuccessRateMA10;
    }

    public BigDecimal getHeiKinAshiDownUpSuccessRateMA20() {
        return heiKinAshiDownUpSuccessRateMA20;
    }

    public void setHeiKinAshiDownUpSuccessRateMA20(BigDecimal heiKinAshiDownUpSuccessRateMA20) {
        this.heiKinAshiDownUpSuccessRateMA20 = heiKinAshiDownUpSuccessRateMA20;
    }

    public BigDecimal getKdGoldCrossSuccessRateMA5() {
        return kdGoldCrossSuccessRateMA5;
    }

    public void setKdGoldCrossSuccessRateMA5(BigDecimal kdGoldCrossSuccessRateMA5) {
        this.kdGoldCrossSuccessRateMA5 = kdGoldCrossSuccessRateMA5;
    }

    public BigDecimal getKdGoldCrossSuccessRateMA10() {
        return kdGoldCrossSuccessRateMA10;
    }

    public void setKdGoldCrossSuccessRateMA10(BigDecimal kdGoldCrossSuccessRateMA10) {
        this.kdGoldCrossSuccessRateMA10 = kdGoldCrossSuccessRateMA10;
    }

    public BigDecimal getKdGoldCrossSuccessRateMA20() {
        return kdGoldCrossSuccessRateMA20;
    }

    public void setKdGoldCrossSuccessRateMA20(BigDecimal kdGoldCrossSuccessRateMA20) {
        this.kdGoldCrossSuccessRateMA20 = kdGoldCrossSuccessRateMA20;
    }

    public BigDecimal getKdDeadCrossSuccessRateMA5() {
        return kdDeadCrossSuccessRateMA5;
    }

    public void setKdDeadCrossSuccessRateMA5(BigDecimal kdDeadCrossSuccessRateMA5) {
        this.kdDeadCrossSuccessRateMA5 = kdDeadCrossSuccessRateMA5;
    }

    public BigDecimal getKdDeadCrossSuccessRateMA10() {
        return kdDeadCrossSuccessRateMA10;
    }

    public void setKdDeadCrossSuccessRateMA10(BigDecimal kdDeadCrossSuccessRateMA10) {
        this.kdDeadCrossSuccessRateMA10 = kdDeadCrossSuccessRateMA10;
    }

    public BigDecimal getKdDeadCrossSuccessRateMA20() {
        return kdDeadCrossSuccessRateMA20;
    }

    public void setKdDeadCrossSuccessRateMA20(BigDecimal kdDeadCrossSuccessRateMA20) {
        this.kdDeadCrossSuccessRateMA20 = kdDeadCrossSuccessRateMA20;
    }
}
