package cn.com.acca.ma.service;

public interface ModelGoldCrossDeadCrossAnalysisService extends BaseService {

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的基础数据
     */
    void writeModelGoldCrossDeadCrossAnalysis();

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的百分比的均值
     */
    void writeModelGoldCrossDeadCrossAnalysisPercentMA();

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的成功率的均值
     */
    void writeModelGoldCrossDeadCrossAnalysisSuccessRateMA();

    /**
     * 根据日期，计算表MDL_G_C_D_C_ANALYSIS的数据
     */
    void writeModelGoldCrossDeadCrossAnalysisByDate();
}
