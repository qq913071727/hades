package cn.com.acca.ma.service;

public interface RealStockTransactionRecordService extends BaseService {

    /**
     * 实时地判断买卖条件，给出交易建议，并保存在文件中
     */
    void realTimeJudgeConditionAndGiveSuggestion();
}
