package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 机器人6--账户
 */
@Data
public class Robot6Account {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 机器人6名字
     */
    private String robotName;

    /**
     * 持股数量
     */
    private Integer holdStockNumber;

    /**
     * etf资产
     */
    private BigDecimal stockAssets;

    /**
     * 资金资产
     */
    private BigDecimal capitalAssets;

    /**
     * 总资产
     */
    private BigDecimal totalAssets;


}
