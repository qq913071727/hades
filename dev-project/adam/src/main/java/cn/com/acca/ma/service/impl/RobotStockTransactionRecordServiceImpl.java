package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.constant.*;
import cn.com.acca.ma.model.ModelGoldCrossDeadCrossAnalysis;
import cn.com.acca.ma.model.RobotStockTransactionRecord;
import cn.com.acca.ma.model.StockWeek;
import cn.com.acca.ma.pojo.*;
import cn.com.acca.ma.service.RobotStockTransactionRecordService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class RobotStockTransactionRecordServiceImpl extends BaseServiceImpl<RobotStockTransactionRecordServiceImpl, RobotStockTransactionRecord> implements
        RobotStockTransactionRecordService {

    /**
     * 执行某段时间内的股票买卖交易（成功率）
     */
    @Override
    public void doBuyAndSellByBeginDateAndEndDate_successRate() {
        logger.info("执行某段时间内的股票买卖交易（成功率）");

        String buySellBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.begin.date");
        String buySellEndDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.end.date");
        Integer transactionStrategy = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.robot.transaction.strategy"));
        Double closePriceStart = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.closePrice.start"));
        Double closePriceEnd = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.closePrice.end"));
        String xrBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.xr.begin.date");
        String currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.current.closePrice.compareTo.someTime.highest.lowest.price.begin.date");
        Integer currentClosePriceCompareToSomeTimeHighestPrice = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.current.closePrice.compareTo.someTime.highest.price"));
        Integer currentClosePriceCompareToSomeTimeLowestPrice = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.current.closePrice.compareTo.someTime.lowest.price"));
        String maNotDecreasingLevel = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.ma.not.decreasing.level");
        String maNotDecreasingDateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.ma.not.decreasing.dateNumber");
        String maNotIncreasingLevel = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.ma.not.increasing.level");
        String maNotIncreasingDateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.ma.not.increasing.dateNumber");
        Integer backwardMonth = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.backward.month"));
        Integer averageDateNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.average.date.number"));
        Double macdMultiplicationFactor = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.average.date.macd.multiplication.factor"));
        Double closePriceMa5MultiplicationFactor = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.average.date.closePriceMa5.multiplication.factor"));
        Double heiKinAshiMultiplicationFactor = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.average.date.heiKinAshi.multiplication.factor"));
        Double kdMultiplicationFactor = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.average.date.kd.multiplication.factor"));
//        Integer successRateType = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.stock.success.rate.type"));
        Double threshold = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.robot.transaction.strategy.success.rate.threshold"));
        Integer mandatoryStopLoss = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.mandatoryStopLoss"));
        Double mandatoryStopLossRate = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.mandatoryStopLossRate"));
        Integer holdStockNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.holdStockNumber"));
        Integer printFormat = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.print.format"));
        Integer shippingSpaceControl = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.shippingSpaceControl"));

        // 获取日期列表
        String previousBuySellBeginDate = DateUtil.dateToString(stockTransactionDataDao.findPreviousDateByDate(false, buySellBeginDate));
//        String previousBuySellEndDate = DateUtil.dateToString(stockTransactionDataDao.findPreviousDateByDate(false, buySellEndDate));
        List<String> dateList = stockTransactionDataDao.findDistinctDateBetweenBeginDateAndEndDate(false, buySellBeginDate, buySellEndDate);

        if (null != dateList && dateList.size() > 0) {
            for (int i = 0; i < dateList.size(); i++) {
                String date = dateList.get(i);
                String beginDate = DateUtil.dateToString(DateUtil.stepMonth(DateUtil.stringToDate(date), -backwardMonth));

                /*************************************** 卖股票/买股票 ***********************************************/
                // 更新robot_account表
                robotAccountDao.updateRobotAccountBeforeSellOrBuy(date);

                // 卖股票
                robotStockTransactionRecordDao.sellOrBuy(date, mandatoryStopLoss, mandatoryStopLossRate);

                // 更新robot_account表
                robotAccountDao.updateRobotAccountAfterSellOrBuy(date);

                // 如果所有账号都已经满仓，则跳过这个交易日
                if (isFullStockInRobotAccount(holdStockNumber)){
                    logger.info("所有账号都是满仓，因此日期【" + date + "】不做任何交易，直接跳过");

                    // 更新robot_account表的total_assets字段
                    robotAccountDao.updateRobotAccountAfterBuyOrSell(date);

                    continue;
                }

                /********************************** 选择最近成功率最高的算法 ***********************************/
                // 计算所有算法的近期的成功率/百分比
                List<SuccessRateOrPercentagePojo> macdGoldCrossList = null;
                List<SuccessRateOrPercentagePojo> macdDeadCrossList = null;
                List<SuccessRateOrPercentagePojo> closePriceMa5GoldCrossList = null;
                List<SuccessRateOrPercentagePojo> closePriceMa5DeadCrossList = null;
                List<SuccessRateOrPercentagePojo> heiKinAshiUpDownList = null;
                List<SuccessRateOrPercentagePojo> heiKinAshiDownUpList = null;
                List<SuccessRateOrPercentagePojo> kdGoldCrossList = null;
                List<SuccessRateOrPercentagePojo> kdDeadCrossList = null;
//                Double macdGoldCrossSuccessRate = null;
//                Double macdDeadCrossSuccessRate = null;
//                Double closePriceMa5GoldCrossSuccessRate = null;
//                Double closePriceMa5DeadCrossSuccessRate = null;
//                Double heiKinAshiUpDownSuccessRate = null;
//                Double heiKinAshiDownUpSuccessRate = null;
//                Double kdGoldCrossSuccessRate = null;
//                Double kdDeadCrossSuccessRate = null;
//                Double macdGoldCrossPercent = null;
//                Double macdDeadCrossPercent = null;
//                Double closePriceMa5GoldCrossPercent = null;
//                Double closePriceMa5DeadCrossPercent = null;
//                Double heiKinAshiUpDownPercent = null;
//                Double heiKinAshiDownUpPercent = null;
//                Double kdGoldCrossPercent = null;
//                Double kdDeadCrossPercent = null;

//                ModelGoldCrossDeadCrossAnalysis modelGoldCrossDeadCrossAnalysis = modelGoldCrossDeadCrossAnalysisDao.findByDate(date);

                if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                    logger.info("使用金叉死叉算法的【成功率】作为标准");

                    macdGoldCrossList = modelMACDGoldCrossDao.getMACDGoldCrossSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * macdMultiplicationFactor.doubleValue()));
                    macdDeadCrossList = modelMACDDeadCrossDao.getMACDDeadCrossSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * macdMultiplicationFactor.doubleValue()));
                    closePriceMa5GoldCrossList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * closePriceMa5MultiplicationFactor.doubleValue()));
                    closePriceMa5DeadCrossList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * closePriceMa5MultiplicationFactor.doubleValue()));
                    heiKinAshiUpDownList = modelHeiKinAshiUpDownDao.getHeiKinAshiUpDownSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * heiKinAshiMultiplicationFactor.doubleValue()));
                    heiKinAshiDownUpList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * heiKinAshiMultiplicationFactor.doubleValue()));
                    kdGoldCrossList = modelKDGoldCrossDao.getKdGoldCrossSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * kdMultiplicationFactor.doubleValue()));
                    kdDeadCrossList = modelKDDeadCrossDao.getKdDeadCrossSuccessRate(false, beginDate, date, (int)(averageDateNumber.intValue() * kdMultiplicationFactor.doubleValue()));

//                    if (averageDateNumber.equals(SuccessRateOrPercentageMAPeriod.TEN)){
//                        macdGoldCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getMacdGoldCrossSuccessRateMA10().doubleValue();
//                        macdDeadCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getMacdDeadCrossSuccessRateMA10().doubleValue();
//                        closePriceMa5GoldCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getClosePriceGoldCrossMA5SuccessRateMA10().doubleValue();
//                        closePriceMa5DeadCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getClosePriceDeadCrossMA5SuccessRateMA10().doubleValue();
//                        heiKinAshiUpDownSuccessRate = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiUpDownSuccessRateMA10().doubleValue();
//                        heiKinAshiDownUpSuccessRate = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiDownUpSuccessRateMA10().doubleValue();
//                        kdGoldCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossSuccessRateMA10().doubleValue();
//                        kdDeadCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossSuccessRateMA10().doubleValue();
//                    }
//                    if (averageDateNumber.equals(SuccessRateOrPercentageMAPeriod.TWEENTY)){
//                        macdGoldCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getMacdGoldCrossSuccessRateMA20().doubleValue();
//                        macdDeadCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getMacdDeadCrossSuccessRateMA20().doubleValue();
//                        closePriceMa5GoldCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getClosePriceGoldCrossMA5SuccessRateMA20().doubleValue();
//                        closePriceMa5DeadCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getClosePriceDeadCrossMA5SuccessRateMA20().doubleValue();
//                        heiKinAshiUpDownSuccessRate = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiUpDownSuccessRateMA20().doubleValue();
//                        heiKinAshiDownUpSuccessRate = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiDownUpSuccessRateMA20().doubleValue();
//                        kdGoldCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossSuccessRateMA20().doubleValue();
//                        kdDeadCrossSuccessRate = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossSuccessRateMA20().doubleValue();
//                    }
                }
                if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                    logger.info("使用金叉死叉算法的【百分比】作为标准");

                    macdGoldCrossList = modelMACDGoldCrossDao.getMACDGoldCrossPercentage(false, beginDate, date);
                    macdDeadCrossList = modelMACDDeadCrossDao.getMACDDeadCrossPercentage(false, beginDate, date);
                    closePriceMa5GoldCrossList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossPercentage(false, beginDate, date);
                    closePriceMa5DeadCrossList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossPercentage(false, beginDate, date);
                    heiKinAshiUpDownList = modelHeiKinAshiUpDownDao.getHeiKinAshiUpDownPercentage(false, beginDate, date);
                    heiKinAshiDownUpList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpPercentage(false, beginDate, date);
                    kdGoldCrossList = modelKDGoldCrossDao.getKdGoldCrossPercentage(false, beginDate, date);
                    kdDeadCrossList = modelKDDeadCrossDao.getKdDeadCrossPercentage(false, beginDate, date);

//                    if (averageDateNumber.equals(SuccessRateOrPercentageMAPeriod.TEN)){
//                        macdGoldCrossPercent = modelGoldCrossDeadCrossAnalysis.getMacdGoldCrossPercentMA10().doubleValue();
//                        macdDeadCrossPercent = modelGoldCrossDeadCrossAnalysis.getMacdDeadCrossPercentMA10().doubleValue();
//                        closePriceMa5GoldCrossPercent = modelGoldCrossDeadCrossAnalysis.getClosePriceGoldCrossMA5PercentMA10().doubleValue();
//                        closePriceMa5DeadCrossPercent = modelGoldCrossDeadCrossAnalysis.getClosePriceDeadCrossMA5PercentMA10().doubleValue();
//                        heiKinAshiUpDownPercent = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiUpDownPercentMA10().doubleValue();
//                        heiKinAshiDownUpPercent = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiDownUpPercentMA10().doubleValue();
//                        kdGoldCrossPercent = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossPercentMA10().doubleValue();
//                        kdDeadCrossPercent = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossPercentMA10().doubleValue();
//                    }
//                    if (averageDateNumber.equals(SuccessRateOrPercentageMAPeriod.TWEENTY)){
//                        macdGoldCrossPercent = modelGoldCrossDeadCrossAnalysis.getMacdGoldCrossPercentMA20().doubleValue();
//                        macdDeadCrossPercent = modelGoldCrossDeadCrossAnalysis.getMacdDeadCrossPercentMA20().doubleValue();
//                        closePriceMa5GoldCrossPercent = modelGoldCrossDeadCrossAnalysis.getClosePriceGoldCrossMA5PercentMA20().doubleValue();
//                        closePriceMa5DeadCrossPercent = modelGoldCrossDeadCrossAnalysis.getClosePriceDeadCrossMA5PercentMA20().doubleValue();
//                        heiKinAshiUpDownPercent = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiUpDownPercentMA20().doubleValue();
//                        heiKinAshiDownUpPercent = modelGoldCrossDeadCrossAnalysis.getHeiKinAshiDownUpPercentMA20().doubleValue();
//                        kdGoldCrossPercent = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossPercentMA20().doubleValue();
//                        kdDeadCrossPercent = modelGoldCrossDeadCrossAnalysis.getKdGoldCrossPercentMA20().doubleValue();
//                    }
                }

                // 判断哪种算法的成功率/百分比最高
                FilterMethod.SELECT_FILTER_METHOD_VALUE = 0.0;
                FilterMethod.SELECT_FILTER_METHOD = null;
                if (null != macdGoldCrossList && macdGoldCrossList.size() > 0) {
                    Double value = Double.valueOf((macdGoldCrossList.get(macdGoldCrossList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != macdGoldCrossSuccessRate || null != macdGoldCrossPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = macdGoldCrossSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = macdGoldCrossPercent;
//                    }
                    logger.info("MACD金叉算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_GOLD_CROSS;
                    }
                }
                if (null != macdDeadCrossList && macdDeadCrossList.size() > 0) {
                    Double value = Double.valueOf((macdDeadCrossList.get(macdDeadCrossList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != macdDeadCrossSuccessRate || null != macdDeadCrossPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = macdDeadCrossSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = macdDeadCrossPercent;;
//                    }
                    logger.info("MACD死叉算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_DEAD_CROSS;
                    }
                }
                if (null != closePriceMa5GoldCrossList && closePriceMa5GoldCrossList.size() > 0) {
                    Double value = Double.valueOf((closePriceMa5GoldCrossList.get(closePriceMa5GoldCrossList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != closePriceMa5GoldCrossSuccessRate || null != closePriceMa5GoldCrossPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = closePriceMa5GoldCrossSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = closePriceMa5GoldCrossPercent;;
//                    }
                    logger.info("close_price金叉ma5算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA;
                    }
                }
                if (null != closePriceMa5DeadCrossList && closePriceMa5DeadCrossList.size() > 0) {
                    Double value = Double.valueOf((closePriceMa5DeadCrossList.get(closePriceMa5DeadCrossList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != closePriceMa5DeadCrossSuccessRate || null != closePriceMa5DeadCrossPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = closePriceMa5DeadCrossSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = closePriceMa5DeadCrossPercent;;
//                    }
                    logger.info("close_price死叉ma5算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA;
                    }
                }
                if (null != heiKinAshiUpDownList && heiKinAshiUpDownList.size() > 0) {
                    Double value = Double.valueOf((heiKinAshiUpDownList.get(heiKinAshiUpDownList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != heiKinAshiUpDownSuccessRate || null != heiKinAshiUpDownPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = heiKinAshiUpDownSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = heiKinAshiUpDownPercent;;
//                    }
                    logger.info("hei kin ashi上升趋势算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_UP_DOWN;
                    }
                }
                if (null != heiKinAshiDownUpList && heiKinAshiDownUpList.size() > 0) {
                    Double value = Double.valueOf((heiKinAshiDownUpList.get(heiKinAshiDownUpList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != heiKinAshiDownUpSuccessRate || null != heiKinAshiDownUpPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = heiKinAshiDownUpSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = heiKinAshiDownUpPercent;;
//                    }
                    logger.info("hei kin ashi下跌趋势算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_DOWN_UP;
                    }
                }
                if (null != kdGoldCrossList && kdGoldCrossList.size() > 0) {
                    Double value = Double.valueOf((kdGoldCrossList.get(kdGoldCrossList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != kdGoldCrossSuccessRate || null != kdGoldCrossPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = kdGoldCrossSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = kdGoldCrossPercent;;
//                    }
                    logger.info("KD金叉算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_GOLD_CROSS;
                    }
                }
                if (null != kdDeadCrossList && kdDeadCrossList.size() > 0) {
                    Double value = Double.valueOf((kdDeadCrossList.get(kdDeadCrossList.size() - 1)).getSuccessRateOrPercentage().toString());
//                if (null != kdDeadCrossSuccessRate || null != kdDeadCrossPercent) {
//                    Double value = null;
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
//                        value = kdDeadCrossSuccessRate;
//                    }
//                    if (transactionStrategy.equals(TransactionStrategy.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
//                        value = kdDeadCrossPercent;;
//                    }
                    logger.info("KD死叉算法的成功率/百分比【" + value + "】");
                    if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                        FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_DEAD_CROSS;
                    }
                }

                // 如果最大的过滤算法成功率/百分比比阈值低，则不做任何交易
                if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(threshold)) {
                    logger.info("所有的过滤算法中最大的成功率/百分比是【" + FilterMethod.SELECT_FILTER_METHOD_VALUE
                            + "】，对应的过滤算法是【" + FilterMethod.SELECT_FILTER_METHOD + "】，小于阈值【"
                            + threshold + "】，因此不做任何交易");
                    FilterMethod.SELECT_FILTER_METHOD = null;
                } else {
                    logger.info("采用的过滤算法【" + FilterMethod.SELECT_FILTER_METHOD + "】，其成功率/百分比【"
                            + FilterMethod.SELECT_FILTER_METHOD_VALUE + "】");
                }

                /*************************************** 按照条件，过滤股票 ***********************************/
                if (null != FilterMethod.SELECT_FILTER_METHOD) {
                    // 清空robot_stock_filter表
                    robotStockFilterDao.truncateTableRobotStockFilter();

                    // 金叉做多的算法
                    if (1 == FilterMethod.SELECT_FILTER_METHOD % 2) {
                        // 根据date，从stock_transaction_data表中，向robot_stock_filter表中插入股票代码
                        robotStockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(date);

                        // 过滤条件：当前收盘价与某段时间最高价的百分比
                        robotStockFilterDao
                                .filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(
                                        currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, date,
                                        currentClosePriceCompareToSomeTimeHighestPrice);

                        // 过滤条件：MA不单调递减，过滤股票
                        String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
                        String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
                        for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
                            String maLevel = maLevelArray[j].trim();
                            String dateNumber = dateNumberArray[j].trim();
                            robotStockFilterDao.filterByMANotDecreasing(date, Integer.parseInt(maLevel), dateNumber);
                        }
                    }
                    // 死叉做空的算法
                    if (0 == FilterMethod.SELECT_FILTER_METHOD % 2) {
                        // 根据date，从stock_transaction_data表中，向robot_stock_filter表中插入股票代码
                        robotStockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(date);

                        // 过滤条件：当前收盘价与某段时间最低价的百分比
                        robotStockFilterDao
                                .filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(
                                        currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, date,
                                        currentClosePriceCompareToSomeTimeLowestPrice);

                        // 过滤条件：MA不单调递增，过滤股票
                        String[] maLevelArray = maNotIncreasingLevel.replace("[", "").replace("]", "").split(",");
                        String[] dateNumberArray = maNotIncreasingDateNumber.replace("[", "").replace("]", "").split(",");
                        for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
                            String maLevel = maLevelArray[j].trim();
                            String dateNumber = dateNumberArray[j].trim();
                            robotStockFilterDao.filterByMANotIncreasing(date, Integer.parseInt(maLevel), dateNumber);
                        }
                    }
                    // 只保留这个价格区间以内的股票
                    robotStockFilterDao.filterByLessThanClosePrice(date, closePriceStart, closePriceEnd);

                    // 过滤条件：删除除过权的股票
                    robotStockFilterDao.filterByXr(xrBeginDate, date);

                    /*************************************** 执行过滤算法 ***********************************/
                    // 过滤条件：MACD金叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.MACD_GOLD_CROSS)) {
                        robotStockFilterDao.filterByMACDGoldCross(date);
                    }
                    // 过滤条件：收盘价金叉五日均线
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA)) {
                        robotStockFilterDao.filterByClosePriceGoldCrossMA5(date);
                    }
                    // 过滤条件：平均K线从下跌趋势变为上涨趋势
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.HEI_KIN_ASHI_UP_DOWN)) {
                        robotStockFilterDao.filterByHeiKinAshiUpDown(date);
                    }
                    // 过滤条件：KD金叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.KD_GOLD_CROSS)) {
                        robotStockFilterDao.filterByKDGoldCross(date);
                    }
                    // 过滤条件：MACD死叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.MACD_DEAD_CROSS)) {
                        robotStockFilterDao.filterByMACDDeadCross(date);
                    }
                    // 过滤条件：收盘价死叉五日均线
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA)) {
                        robotStockFilterDao.filterByClosePriceDeadCrossMA5(date);
                    }
                    // 过滤条件：平均K线从上涨趋势变为下跌趋势
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.HEI_KIN_ASHI_DOWN_UP)) {
                        robotStockFilterDao.filterByHeiKinAshiDownUp(date);
                    }
                    // 过滤条件：KD死叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.KD_DEAD_CROSS)) {
                        robotStockFilterDao.filterByKDDeadCross(date);
                    }

                    // robot_stock_filter表的记录数
                    Integer recordCount = robotStockFilterDao.count();
                    logger.info("robot_stock_filter表的记录数：" + recordCount);
                }

                /*************************************** 买股票/卖股票 ******************************************/
                if (null != FilterMethod.SELECT_FILTER_METHOD) {
                    // 买股票/卖股票
                    // 确定交易方向（做多/做空）
                    Integer direction;
                    if (0 == FilterMethod.SELECT_FILTER_METHOD % 2) {
                        direction = -1;
                    } else {
                        direction = 1;
                    }
                    robotStockTransactionRecordDao.buyOrSell(date, backwardMonth, averageDateNumber, FilterMethod.SELECT_FILTER_METHOD, FilterMethod.SELECT_FILTER_METHOD_VALUE, direction, shippingSpaceControl, holdStockNumber);

                    // 更新robot_account表的total_assets字段
                    robotAccountDao.updateRobotAccountAfterBuyOrSell(date);
                }

                // 获取买卖建议
                String printText = null;
                if (printFormat.equals(RobotPrintBuySellSuggestionFormat.JSON)) {
                    // json方式
                    printText = super.printBuySellSuggestionByJson_successRate(date);
                }
                if (printFormat.equals(RobotPrintBuySellSuggestionFormat.SIMPLE)) {
                    // 简单文本格式
                    printText = super.printBuySellSuggestionBySimple_successRate(date);
                }
                logger.info("买卖建议：\n" + printText);
            }
        }

        logger.info("执行某段时间内的股票买卖交易完成（成功率）");
    }

    /**
     * 卖股票/买股票
     */
    @Override
    public void sellOrBuy() {
        logger.info("卖股票/买股票");

        String sellDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.sell.stock.date");
        Integer mandatoryStopLoss = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.sell.stock.mandatoryStopLoss"));
        Double mandatoryStopLossRate = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.sell.stock.mandatoryStopLossRate"));

        robotStockTransactionRecordDao.sellOrBuy(sellDate, mandatoryStopLoss, mandatoryStopLossRate);
    }

    /**
     * 买股票/卖股票
     */
    @Override
    public void buyOrSell() {
        logger.info("买股票/卖股票");

        String buyDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.date");
        Integer backwardMonth = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.success.rate.backward.month"));
        Integer averageDateNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.success.rate.average.date.number"));
//        Integer successRateType = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.success.rate.type"));
        Integer successRateType = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.shippingSpaceControl"));
        Integer shippingSpaceControl = FilterMethod.SELECT_FILTER_METHOD;
        Double successRate = FilterMethod.SELECT_FILTER_METHOD_VALUE;
        Integer holdStockNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.holdStockNumber"));

        // 确定交易方向（做多/做空）
        Integer direction;
        if (0 == FilterMethod.SELECT_FILTER_METHOD % 2) {
            direction = -1;
        } else {
            direction = 1;
        }
        // 买股票
        robotStockTransactionRecordDao.buyOrSell(buyDate, backwardMonth, averageDateNumber, successRateType,
                successRate, direction, shippingSpaceControl, holdStockNumber);
    }

    /**
     * 打印买卖建议
     */
    @Override
    public void printBuySellSuggestion() {
        logger.info("打印买卖建议");

        String printDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.print.day");
        Integer printFormat = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.print.format"));

        // 获取买卖建议
        String printText = null;
        if (printFormat.equals(RobotPrintBuySellSuggestionFormat.JSON)) {
            // json方式
            printText = super.printBuySellSuggestionByJson_successRate(printDate);
        }
        if (printFormat.equals(RobotPrintBuySellSuggestionFormat.SIMPLE)) {
            // 简单文本格式
            printText = super.printBuySellSuggestionBySimple_successRate(printDate);
        }
        System.out.println("买卖建议：\n" + printText);

        // 打印买卖建议
        File file;
        try {
            file = new File(PRINT_ROBOT_BUY_SELL_SUGGESTION);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(file);
            writer.write(printText);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
