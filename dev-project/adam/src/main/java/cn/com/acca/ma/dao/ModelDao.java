package cn.com.acca.ma.dao;

import java.util.List;

import cn.com.acca.ma.model.Model;

public interface ModelDao extends BaseDao {
	/**
	 * 获取所有的Model对象
	 * @return
	 */
	List<Model> findAll();
	
	/**
	 * 保存Model对象
	 * @param model
	 */
	void saveModel(Model model);

	/**
	 * 按照create_time列降序排列
	 * @return
	 */
	List<Model> orderByCreateTime();

	/**
	 * 按照id列降序排列
	 * @return
	 */
	List<Model> orderById();

	/**
	 * 删除TEMP_开头的四个表
	 */
	void dropTableBeginWithTemp();

    /**
     * 将ROBOT_开头的四个表的数据导入到TEMP_开头的四个表中
     */
	void importIntoTableBeginWithTempFromTableBeginWithRobot();

	/**
	 * 将ROBOT2_开头的四个表的数据导入到TEMP_开头的四个表中
	 */
	void importIntoTableBeginWithTempFromTableBeginWithRobot2();

	/**
	 * 将ROBOT3_开头的四个表的数据导入到TEMP_开头的四个表中
	 */
	void importIntoTableBeginWithTempFromTableBeginWithRobot3();

	/**
	 * 将ROBOT4_开头的四个表的数据导入到TEMP_开头的四个表中
	 */
	void importIntoTableBeginWithTempFromTableBeginWithRobot4();

	/**
	 * 将ROBOT5_开头的四个表的数据导入到TEMP_开头的四个表中
	 */
	void importIntoTableBeginWithTempFromTableBeginWithRobot5();

	/**
	 * 将ROBOT6_开头的四个表的数据导入到TEMP_开头的四个表中
	 */
	void importIntoTableBeginWithTempFromTableBeginWithRobot6();

    /**
     * 将TEMP_开头的表中的数据导入到HIS_开头的表中
     * @param id
     */
	void insertIntoTableBeginWithHisFromTableBeginWithTemp(Integer id);

    /**
     * 删除或重置ROBOT_开头的表中的记录
     * @param initAssets
     */
	void truncateOrResetTableBeginRobot(Integer initAssets);

	/**
	 * 删除或重置ROBOT2_开头的表中的记录
	 * @param initAssets
	 */
	void truncateOrResetTableBeginRobot2(Integer initAssets);

	/**
	 * 删除或重置ROBOT3_开头的表中的记录
	 * @param initAssets
	 */
	void truncateOrResetTableBeginRobot3(Integer initAssets);

	/**
	 * 删除或重置ROBOT4_开头的表中的记录
	 * @param initAssets
	 */
	void truncateOrResetTableBeginRobot4(Integer initAssets);

	/**
	 * 删除或重置ROBOT5_开头的表中的记录
	 * @param initAssets
	 */
	void truncateOrResetTableBeginRobot5(Integer initAssets);

	/**
	 * 删除或重置ROBOT6_开头的表中的记录
	 * @param initAssets
	 */
	void truncateOrResetTableBeginRobot6(Integer initAssets);
}
