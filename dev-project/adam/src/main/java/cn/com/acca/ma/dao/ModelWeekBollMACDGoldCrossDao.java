package cn.com.acca.ma.dao;

import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;

import java.util.List;

public interface ModelWeekBollMACDGoldCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_BOLL_MACD_GOLD_CROSS中插入数据
     */
    void writeModelWeekBollMACDGoldCross();

    /**
     * 查询MACD金叉交易收益率和最高价突破布林带下轨的百分比
     * @param beginDate
     * @param endDate
     */
    List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekMACDGoldCrossProfitOrLossPercentageAndBollDn(String beginDate, String endDate);
}
