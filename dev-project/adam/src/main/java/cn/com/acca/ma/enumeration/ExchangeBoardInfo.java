package cn.com.acca.ma.enumeration;

import java.util.List;

/**
 * 板块（按交易所划分）
 */
public enum ExchangeBoardInfo {

    /**
     * 沪市A股
     */
    SH_PREFIX(new String[]{"600", "601", "603", "605"}, "沪市A股", "0"),
    /**
     * 科创板
     */
    KCB_PREFIX(new String[]{"688"}, "科创板", "0"),
    /**
     * 深市A股
     */
    SZ_PREFIX(new String[]{"000", "001"}, "深市A股", "1"),
    /**
     * 中小板
     */
    ZXB_PREFIX(new String[]{"002", "003"}, "中小板", "1"),
    /**
     * 创业板
     */
    CYB_PREFIX(new String[]{"300"}, "创业板", "1");

    /**
     * 代码前缀数组
     */
    private String[] codePrefixArray;

    /**
     * 板块名称
     */
    private String name;

    /**
     * url参数前缀
     */
    private String urlParamPrefix;

    ExchangeBoardInfo(String[] codePrefixArray, String name, String urlParamPrefix) {
        this.codePrefixArray = codePrefixArray;
        this.name = name;
        this.urlParamPrefix = urlParamPrefix;
    }

    public String[] getCodePrefixArray() {
        return codePrefixArray;
    }

    public void setCodePrefixArray(String[] codePrefixArray) {
        this.codePrefixArray = codePrefixArray;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlParamPrefix() {
        return urlParamPrefix;
    }

    public void setUrlParamPrefix(String urlParamPrefix) {
        this.urlParamPrefix = urlParamPrefix;
    }
}
