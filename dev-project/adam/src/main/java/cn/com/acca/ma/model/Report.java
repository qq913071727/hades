package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 报告
 * @author Administrator
 */
@Entity
@Table(name = "REPORT")
public class Report implements Serializable {

	private static final long serialVersionUID = 3961580730975461507L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "REPORT_DATE")
	private Date reportDate;

	@Id
	@Column(name = "REPORT_NAME")
	private String reportName;

	@Column(name = "REPORT_TITLE")
	private String reportTitle;

	@Column(name = "STOCK_SAMPLE_SUM")
	private BigDecimal stockSampleSum;

	@Column(name = "STOCK_UP_NUMBER")
	private BigDecimal stockUpNumber;

	@Column(name = "STOCK_MIDDLE_NUMBER")
	private BigDecimal stockMiddleNumber;

	@Column(name = "STOCK_DOWN_NUMBER")
	private BigDecimal stockDownNumber;

	@Column(name = "BULL_RANK_NUMBER")
	private BigDecimal bullRankNumber;

	@Column(name = "SHORT_ORDER_NUMBER")
	private BigDecimal shortOrderNumber;

	@Column(name = "BULL_RANK_STOCK_INFO")
	private String bullRankStockInfo;

	@Column(name = "SHORT_ORDER_STOCK_INFO")
	private String shortOrderStockInfo;

	@Column(name = "DIFFERENTIATION_BEGIN_DATE")
	private Date differentiationBeginDate;

	@Column(name = "DIFFERENTIATION_END_DATE")
	private Date differentiationEndDate;

	@Column(name = "TENYEAR_BULL_SHORT_BEGIN_DATE")
	private Date tenYearBullRankShortOrderBeginDate;

	@Column(name = "TENYEAR_BULL_SHORT_END_DATE")
	private Date tenYearBullRankShortOrderEndDate;

	@Column(name = "HALFYEAR_BULL_SHORT_BEGIN_DATE")
	private Date halfYearBullRankShortOrderBeginDate;

	@Column(name = "HALFYEAR_BULL_SHORT_END_DATE")
	private Date halfYearBullRankShortOrderEndDate;

	@Column(name = "TEN_YEAR_CLOSE_BEGIN_DATE")
	private Date tenYearCloseBeginDate;

	@Column(name = "TEN_YEAR_CLOSE_END_DATE")
	private Date tenYearCloseEndDate;

	@Column(name = "HALF_YEAR_CLOSE_BEGIN_DATE")
	private Date halfYearCloseBeginDate;

	@Column(name = "HALF_YEAR_CLOSE_END_DATE")
	private Date halfYearCloseEndDate;

	@Column(name = "DAY_MACD_BEGIN_DATE")
	private Date dayMacdBeginDate;

	@Column(name = "DAY_MACD_END_DATE")
	private Date dayMacdEndDate;

	@Column(name = "WEEKEND_KD_BEGIN_DATE")
	private Date weekednKDBeginDate;

	@Column(name = "WEEKEND_KD_END_DATE")
	private Date weekednKDEndDate;

	@Column(name = "WEEKEND_MACD_END_DEVIATE")
	private String weekendMacdEndDeviate;

	@Column(name = "WEEKEND_MACD_END_DEVIATE_RATE")
	private Date weekendMacdEndDeviateRate;

	@Column(name = "MACD_UP_BELOW_ZERO_DATE")
	private Date macdUpBelowZeroDate;

	@Column(name = "MACD_UP_BELOW_ZERO_RATE")
	private BigDecimal macdUpBelowZeroRate;

	@Column(name = "MACD_UP_BELOW_ZERO_STOCK")
	private String macdUpBelowZeroStock;

	@Column(name = "WEEKEND_KD_UP_BEGIN_DATE")
	private Date weekendKDUpBeginDate;

	@Column(name = "WEEKEND_KD_UP_END_DATE")
	private Date weekendKDUpEndDate;

	@Column(name = "WEEKEND_KD_UP_CROSS_POINT")
	private BigDecimal weekendKDUpCrossPoint;

	@Column(name = "WEEKEND_KD_UP_STOCK")
	private String weekendKDUpStock;

	@Column(name = "ABNORMAL_STOCK_DATE")
	private Date abnormalStockDate;

	@Column(name = "ABNORMAL_STOCK_RATE")
	private BigDecimal abnormalStockRate;

	@Column(name = "ABNORMAL_STOCK")
	private String abnormalStock;

	@Column(name = "SPIDER_WEB_DATE")
	private Date spiderWebDate;

	@Column(name = "SPIDER_WEB_DATE_NUMBER")
	private BigDecimal spiderWebDateNumber;

	@Column(name = "SPIDER_WEB_LIMIT_RATE")
	private BigDecimal spiderWebLimitRate;

	@Column(name = "LOWEST_MACD_STOCK_DATE")
	private Date lowestMacdStockDate;

	@Column(name = "LOWEST_MACD_STOCK_NUMBER")
	private BigDecimal lowestMacdStockNumber;
	
	@Column(name = "LOWEST_MACD_STOCK_INTERVAL")
	private BigDecimal lowestMacdStockInterval;

	@Column(name = "LOWEST_MACD_STOCK")
	private String lowestMacdStock;

	@Column(name = "LOWEST_KD_WEEKEND_BEGIN_DATE")
	private Date lowestKdWeekendBeginDate;

	@Column(name = "LOWEST_KD_WEEKEND_END_DATE")
	private Date lowestKdWeekendEndDate;

	@Column(name = "LOWEST_KD_WEEKEND_TIME_SPAN")
	private Date lowestKdWeekendTimeSpan;

	@Column(name = "LOWEST_KD_WEEKEND_NUMBER")
	private BigDecimal lowestKdWeekendStockNumber;

	@Column(name = "LOWEST_KD_WEEKEND_STOCK")
	private String lowestKdWeekendStock;

	@Column(name = "REVERSE_STOCK_DATE")
	private Date reverseStockDate;

	@Column(name = "REVERSE_STOCK_RATE")
	private Double reverseStockRate;

	@Column(name = "REVERSE_STOCK")
	private String reverseStock;

	@Column(name = "YEARLINE_SUPPORT_DATE")
	private Date yearLineSupportDate;

	@Column(name = "YEARLINE_SUPPORT_NEAR_RATE")
	private BigDecimal yearLineSupportNearRate;

	@Column(name = "YEARLINE_SUPPORT_DOWN_RATE")
	private BigDecimal yearLineSupportDownRate;

	@Column(name = "HALFYEARLINE_SUPPORT_DATE")
	private Date halfYearLineSupportDate;

	@Column(name = "HALFYEARLINE_SUPPORT_NEAR_RATE")
	private BigDecimal halfYearLineSupportNearRate;

	@Column(name = "HALFYEARLINE_SUPPORT_DOWN_RATE")
	private BigDecimal halfYearLineSupportDownRate;

	@Column(name = "TOP_STOCK_BEGINDATE")
	private Date topStockBeginDate;

	@Column(name = "TOP_STOCK_ENDDATE")
	private Date topStockEndDate;

	@Column(name = "TOP_STOCK_NUMBER")
	private BigDecimal topStockNumber;
	
	@Column(name = "TOP_STOCK_WORD_LIMIT_NUM")
	private BigDecimal topStockWordLimitNumber;
	
	@Column(name = "LAST_STOCK_BEGINDATE")
	private Date lastStockBeginDate;

	@Column(name = "LAST_STOCK_ENDDATE")
	private Date lastStockEndDate;

	@Column(name = "LAST_STOCK_NUMBER")
	private BigDecimal lastStockNumber;

	@Column(name = "BOARD_INDEX_BEGIN_DATE")
	private Date boardIndexBeginDate;

	@Column(name = "BOARD_INDEX_END_DATE")
	private Date boardIndexEndDate;

	@Column(name = "BOARD_HALF_CLOSE_BEGIN_DATE")
	private Date boardHalfCloseEnginDate;

	@Column(name = "BOARD_HALF_CLOSE_END_DATE")
	private Date boardHalfCloseEndDate;

	@Column(name = "BOARD_TEN_CLOSE_BEGIN_DATE")
	private Date boardTenCloseEnginDate;

	@Column(name = "BOARD_TEN_CLOSE_END_DATE")
	private Date boardTenCloseEndDate;

	@Column(name = "HALFYEAR_FIVETENRATE_BEGINDATE")
	private Date halfYearFiveTenDayRateBeginDate;

	@Column(name = "HALFYEAR_FIVETENRATE_ENDDATE")
	private Date halfYearFiveTenDayRateEndDate;

	@Column(name = "TENYEAR_FIVETENRATE_BEGINDATE")
	private Date tenYearFiveTenDayRateBeginDate;

	@Column(name = "TENYEAR_FIVETENRATE_ENDDATE")
	private Date tenYearFiveTenDayRateEndDate;

	@Column(name = "LIMITUPDOWN_BEGINDATE")
	private Date limitUpDownBeginDate;

	@Column(name = "LIMITUPDOWN_ENDDATE")
	private Date limitUpDownEndDate;

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public BigDecimal getStockSampleSum() {
		return stockSampleSum;
	}

	public void setStockSampleSum(BigDecimal stockSampleSum) {
		this.stockSampleSum = stockSampleSum;
	}

	public BigDecimal getStockUpNumber() {
		return stockUpNumber;
	}

	public void setStockUpNumber(BigDecimal stockUpNumber) {
		this.stockUpNumber = stockUpNumber;
	}

	public BigDecimal getStockMiddleNumber() {
		return stockMiddleNumber;
	}

	public void setStockMiddleNumber(BigDecimal stockMiddleNumber) {
		this.stockMiddleNumber = stockMiddleNumber;
	}

	public BigDecimal getStockDownNumber() {
		return stockDownNumber;
	}

	public void setStockDownNumber(BigDecimal stockDownNumber) {
		this.stockDownNumber = stockDownNumber;
	}

	public BigDecimal getBullRankNumber() {
		return bullRankNumber;
	}

	public void setBullRankNumber(BigDecimal bullRankNumber) {
		this.bullRankNumber = bullRankNumber;
	}

	public BigDecimal getShortOrderNumber() {
		return shortOrderNumber;
	}

	public void setShortOrderNumber(BigDecimal shortOrderNumber) {
		this.shortOrderNumber = shortOrderNumber;
	}

	public String getBullRankStockInfo() {
		return bullRankStockInfo;
	}

	public void setBullRankStockInfo(String bullRankStockInfo) {
		this.bullRankStockInfo = bullRankStockInfo;
	}

	public String getShortOrderStockInfo() {
		return shortOrderStockInfo;
	}

	public void setShortOrderStockInfo(String shortOrderStockInfo) {
		this.shortOrderStockInfo = shortOrderStockInfo;
	}

	public Date getDifferentiationBeginDate() {
		return differentiationBeginDate;
	}

	public void setDifferentiationBeginDate(Date differentiationBeginDate) {
		this.differentiationBeginDate = differentiationBeginDate;
	}

	public Date getDifferentiationEndDate() {
		return differentiationEndDate;
	}

	public void setDifferentiationEndDate(Date differentiationEndDate) {
		this.differentiationEndDate = differentiationEndDate;
	}

	public Date getTenYearBullRankShortOrderBeginDate() {
		return tenYearBullRankShortOrderBeginDate;
	}

	public void setTenYearBullRankShortOrderBeginDate(
			Date tenYearBullRankShortOrderBeginDate) {
		this.tenYearBullRankShortOrderBeginDate = tenYearBullRankShortOrderBeginDate;
	}

	public Date getTenYearBullRankShortOrderEndDate() {
		return tenYearBullRankShortOrderEndDate;
	}

	public void setTenYearBullRankShortOrderEndDate(
			Date tenYearBullRankShortOrderEndDate) {
		this.tenYearBullRankShortOrderEndDate = tenYearBullRankShortOrderEndDate;
	}

	public Date getHalfYearBullRankShortOrderBeginDate() {
		return halfYearBullRankShortOrderBeginDate;
	}

	public void setHalfYearBullRankShortOrderBeginDate(
			Date halfYearBullRankShortOrderBeginDate) {
		this.halfYearBullRankShortOrderBeginDate = halfYearBullRankShortOrderBeginDate;
	}

	public Date getHalfYearBullRankShortOrderEndDate() {
		return halfYearBullRankShortOrderEndDate;
	}

	public void setHalfYearBullRankShortOrderEndDate(
			Date halfYearBullRankShortOrderEndDate) {
		this.halfYearBullRankShortOrderEndDate = halfYearBullRankShortOrderEndDate;
	}

	public Date getHalfYearCloseBeginDate() {
		return halfYearCloseBeginDate;
	}

	public void setHalfYearCloseBeginDate(Date halfYearCloseBeginDate) {
		this.halfYearCloseBeginDate = halfYearCloseBeginDate;
	}

	public Date getHalfYearCloseEndDate() {
		return halfYearCloseEndDate;
	}

	public void setHalfYearCloseEndDate(Date halfYearCloseEndDate) {
		this.halfYearCloseEndDate = halfYearCloseEndDate;
	}

	public Date getTenYearCloseBeginDate() {
		return tenYearCloseBeginDate;
	}

	public void setTenYearCloseBeginDate(Date tenYearCloseBeginDate) {
		this.tenYearCloseBeginDate = tenYearCloseBeginDate;
	}

	public Date getTenYearCloseEndDate() {
		return tenYearCloseEndDate;
	}

	public void setTenYearCloseEndDate(Date tenYearCloseEndDate) {
		this.tenYearCloseEndDate = tenYearCloseEndDate;
	}

	public Date getDayMacdBeginDate() {
		return dayMacdBeginDate;
	}

	public void setDayMacdBeginDate(Date dayMacdBeginDate) {
		this.dayMacdBeginDate = dayMacdBeginDate;
	}

	public Date getDayMacdEndDate() {
		return dayMacdEndDate;
	}

	public void setDayMacdEndDate(Date dayMacdEndDate) {
		this.dayMacdEndDate = dayMacdEndDate;
	}

	public Date getWeekednKDBeginDate() {
		return weekednKDBeginDate;
	}

	public void setWeekednKDBeginDate(Date weekednKDBeginDate) {
		this.weekednKDBeginDate = weekednKDBeginDate;
	}

	public Date getWeekednKDEndDate() {
		return weekednKDEndDate;
	}

	public void setWeekednKDEndDate(Date weekednKDEndDate) {
		this.weekednKDEndDate = weekednKDEndDate;
	}

	public String getWeekendMacdEndDeviate() {
		return weekendMacdEndDeviate;
	}

	public void setWeekendMacdEndDeviate(String weekendMacdEndDeviate) {
		this.weekendMacdEndDeviate = weekendMacdEndDeviate;
	}

	public Date getWeekendMacdEndDeviateRate() {
		return weekendMacdEndDeviateRate;
	}

	public void setWeekendMacdEndDeviateRate(Date weekendMacdEndDeviateRate) {
		this.weekendMacdEndDeviateRate = weekendMacdEndDeviateRate;
	}

	public Date getMacdUpBelowZeroDate() {
		return macdUpBelowZeroDate;
	}

	public void setMacdUpBelowZeroDate(Date macdUpBelowZeroDate) {
		this.macdUpBelowZeroDate = macdUpBelowZeroDate;
	}

	public BigDecimal getMacdUpBelowZeroRate() {
		return macdUpBelowZeroRate;
	}

	public void setMacdUpBelowZeroRate(BigDecimal macdUpBelowZeroRate) {
		this.macdUpBelowZeroRate = macdUpBelowZeroRate;
	}

	public String getMacdUpBelowZeroStock() {
		return macdUpBelowZeroStock;
	}

	public void setMacdUpBelowZeroStock(String macdUpBelowZeroStock) {
		this.macdUpBelowZeroStock = macdUpBelowZeroStock;
	}

	public Date getWeekendKDUpBeginDate() {
		return weekendKDUpBeginDate;
	}

	public void setWeekendKDUpBeginDate(Date weekendKDUpBeginDate) {
		this.weekendKDUpBeginDate = weekendKDUpBeginDate;
	}

	public Date getWeekendKDUpEndDate() {
		return weekendKDUpEndDate;
	}

	public void setWeekendKDUpEndDate(Date weekendKDUpEndDate) {
		this.weekendKDUpEndDate = weekendKDUpEndDate;
	}

	public BigDecimal getWeekendKDUpCrossPoint() {
		return weekendKDUpCrossPoint;
	}

	public void setWeekendKDUpCrossPoint(BigDecimal weekendKDUpCrossPoint) {
		this.weekendKDUpCrossPoint = weekendKDUpCrossPoint;
	}

	public String getWeekendKDUpStock() {
		return weekendKDUpStock;
	}

	public void setWeekendKDUpStock(String weekendKDUpStock) {
		this.weekendKDUpStock = weekendKDUpStock;
	}

	public Date getAbnormalStockDate() {
		return abnormalStockDate;
	}

	public void setAbnormalStockDate(Date abnormalStockDate) {
		this.abnormalStockDate = abnormalStockDate;
	}

	public BigDecimal getAbnormalStockRate() {
		return abnormalStockRate;
	}

	public void setAbnormalStockRate(BigDecimal abnormalStockRate) {
		this.abnormalStockRate = abnormalStockRate;
	}

	public String getAbnormalStock() {
		return abnormalStock;
	}

	public void setAbnormalStock(String abnormalStock) {
		this.abnormalStock = abnormalStock;
	}

	public Date getSpiderWebDate() {
		return spiderWebDate;
	}

	public void setSpiderWebDate(Date spiderWebDate) {
		this.spiderWebDate = spiderWebDate;
	}

	public BigDecimal getSpiderWebDateNumber() {
		return spiderWebDateNumber;
	}

	public void setSpiderWebDateNumber(BigDecimal spiderWebDateNumber) {
		this.spiderWebDateNumber = spiderWebDateNumber;
	}

	public BigDecimal getSpiderWebLimitRate() {
		return spiderWebLimitRate;
	}

	public void setSpiderWebLimitRate(BigDecimal spiderWebLimitRate) {
		this.spiderWebLimitRate = spiderWebLimitRate;
	}

	public Date getLowestMacdStockDate() {
		return lowestMacdStockDate;
	}

	public void setLowestMacdStockDate(Date lowestMacdStockDate) {
		this.lowestMacdStockDate = lowestMacdStockDate;
	}

	public BigDecimal getLowestMacdStockNumber() {
		return lowestMacdStockNumber;
	}

	public void setLowestMacdStockNumber(BigDecimal lowestMacdStockNumber) {
		this.lowestMacdStockNumber = lowestMacdStockNumber;
	}

	public String getLowestMacdStock() {
		return lowestMacdStock;
	}

	public void setLowestMacdStock(String lowestMacdStock) {
		this.lowestMacdStock = lowestMacdStock;
	}

	public BigDecimal getLowestMacdStockInterval() {
		return lowestMacdStockInterval;
	}

	public void setLowestMacdStockInterval(BigDecimal lowestMacdStockInterval) {
		this.lowestMacdStockInterval = lowestMacdStockInterval;
	}

	public Date getLowestKdWeekendBeginDate() {
		return lowestKdWeekendBeginDate;
	}

	public void setLowestKdWeekendBeginDate(Date lowestKdWeekendBeginDate) {
		this.lowestKdWeekendBeginDate = lowestKdWeekendBeginDate;
	}

	public Date getLowestKdWeekendEndDate() {
		return lowestKdWeekendEndDate;
	}

	public void setLowestKdWeekendEndDate(Date lowestKdWeekendEndDate) {
		this.lowestKdWeekendEndDate = lowestKdWeekendEndDate;
	}

	public Date getLowestKdWeekendTimeSpan() {
		return lowestKdWeekendTimeSpan;
	}

	public void setLowestKdWeekendTimeSpan(Date lowestKdWeekendTimeSpan) {
		this.lowestKdWeekendTimeSpan = lowestKdWeekendTimeSpan;
	}

	public BigDecimal getLowestKdWeekendStockNumber() {
		return lowestKdWeekendStockNumber;
	}

	public void setLowestKdWeekendStockNumber(
			BigDecimal lowestKdWeekendStockNumber) {
		this.lowestKdWeekendStockNumber = lowestKdWeekendStockNumber;
	}

	public String getLowestKdWeekendStock() {
		return lowestKdWeekendStock;
	}

	public void setLowestKdWeekendStock(String lowestKdWeekendStock) {
		this.lowestKdWeekendStock = lowestKdWeekendStock;
	}

	public Date getReverseStockDate() {
		return reverseStockDate;
	}

	public void setReverseStockDate(Date reverseStockDate) {
		this.reverseStockDate = reverseStockDate;
	}

	public Double getReverseStockRate() {
		return reverseStockRate;
	}

	public void setReverseStockRate(Double reverseStockRate) {
		this.reverseStockRate = reverseStockRate;
	}

	public String getReverseStock() {
		return reverseStock;
	}

	public void setReverseStock(String reverseStock) {
		this.reverseStock = reverseStock;
	}

	public Date getYearLineSupportDate() {
		return yearLineSupportDate;
	}

	public void setYearLineSupportDate(Date yearLineSupportDate) {
		this.yearLineSupportDate = yearLineSupportDate;
	}

	public BigDecimal getYearLineSupportNearRate() {
		return yearLineSupportNearRate;
	}

	public void setYearLineSupportNearRate(BigDecimal yearLineSupportNearRate) {
		this.yearLineSupportNearRate = yearLineSupportNearRate;
	}

	public BigDecimal getYearLineSupportDownRate() {
		return yearLineSupportDownRate;
	}

	public void setYearLineSupportDownRate(BigDecimal yearLineSupportDownRate) {
		this.yearLineSupportDownRate = yearLineSupportDownRate;
	}

	public Date getHalfYearLineSupportDate() {
		return halfYearLineSupportDate;
	}

	public void setHalfYearLineSupportDate(Date halfYearLineSupportDate) {
		this.halfYearLineSupportDate = halfYearLineSupportDate;
	}

	public BigDecimal getHalfYearLineSupportNearRate() {
		return halfYearLineSupportNearRate;
	}

	public void setHalfYearLineSupportNearRate(
			BigDecimal halfYearLineSupportNearRate) {
		this.halfYearLineSupportNearRate = halfYearLineSupportNearRate;
	}

	public BigDecimal getHalfYearLineSupportDownRate() {
		return halfYearLineSupportDownRate;
	}

	public void setHalfYearLineSupportDownRate(
			BigDecimal halfYearLineSupportDownRate) {
		this.halfYearLineSupportDownRate = halfYearLineSupportDownRate;
	}

	public Date getTopStockBeginDate() {
		return topStockBeginDate;
	}

	public void setTopStockBeginDate(Date topStockBeginDate) {
		this.topStockBeginDate = topStockBeginDate;
	}

	public Date getTopStockEndDate() {
		return topStockEndDate;
	}

	public void setTopStockEndDate(Date topStockEndDate) {
		this.topStockEndDate = topStockEndDate;
	}

	public BigDecimal getTopStockNumber() {
		return topStockNumber;
	}

	public void setTopStockNumber(BigDecimal topStockNumber) {
		this.topStockNumber = topStockNumber;
	}

	public BigDecimal getTopStockWordLimitNumber() {
		return topStockWordLimitNumber;
	}

	public void setTopStockWordLimitNumber(BigDecimal topStockWordLimitNumber) {
		this.topStockWordLimitNumber = topStockWordLimitNumber;
	}

	public Date getLastStockBeginDate() {
		return lastStockBeginDate;
	}

	public void setLastStockBeginDate(Date lastStockBeginDate) {
		this.lastStockBeginDate = lastStockBeginDate;
	}

	public Date getLastStockEndDate() {
		return lastStockEndDate;
	}

	public void setLastStockEndDate(Date lastStockEndDate) {
		this.lastStockEndDate = lastStockEndDate;
	}

	public BigDecimal getLastStockNumber() {
		return lastStockNumber;
	}

	public void setLastStockNumber(BigDecimal lastStockNumber) {
		this.lastStockNumber = lastStockNumber;
	}

	public Date getBoardIndexBeginDate() {
		return boardIndexBeginDate;
	}

	public void setBoardIndexBeginDate(Date boardIndexBeginDate) {
		this.boardIndexBeginDate = boardIndexBeginDate;
	}

	public Date getBoardIndexEndDate() {
		return boardIndexEndDate;
	}

	public void setBoardIndexEndDate(Date boardIndexEndDate) {
		this.boardIndexEndDate = boardIndexEndDate;
	}

	public Date getBoardHalfCloseEnginDate() {
		return boardHalfCloseEnginDate;
	}

	public void setBoardHalfCloseEnginDate(Date boardHalfCloseEnginDate) {
		this.boardHalfCloseEnginDate = boardHalfCloseEnginDate;
	}

	public Date getBoardHalfCloseEndDate() {
		return boardHalfCloseEndDate;
	}

	public void setBoardHalfCloseEndDate(Date boardHalfCloseEndDate) {
		this.boardHalfCloseEndDate = boardHalfCloseEndDate;
	}

	public Date getBoardTenCloseEnginDate() {
		return boardTenCloseEnginDate;
	}

	public void setBoardTenCloseEnginDate(Date boardTenCloseEnginDate) {
		this.boardTenCloseEnginDate = boardTenCloseEnginDate;
	}

	public Date getBoardTenCloseEndDate() {
		return boardTenCloseEndDate;
	}

	public void setBoardTenCloseEndDate(Date boardTenCloseEndDate) {
		this.boardTenCloseEndDate = boardTenCloseEndDate;
	}

	public Date getHalfYearFiveTenDayRateBeginDate() {
		return halfYearFiveTenDayRateBeginDate;
	}

	public void setHalfYearFiveTenDayRateBeginDate(
			Date halfYearFiveTenDayRateBeginDate) {
		this.halfYearFiveTenDayRateBeginDate = halfYearFiveTenDayRateBeginDate;
	}

	public Date getHalfYearFiveTenDayRateEndDate() {
		return halfYearFiveTenDayRateEndDate;
	}

	public void setHalfYearFiveTenDayRateEndDate(
			Date halfYearFiveTenDayRateEndDate) {
		this.halfYearFiveTenDayRateEndDate = halfYearFiveTenDayRateEndDate;
	}

	public Date getTenYearFiveTenDayRateBeginDate() {
		return tenYearFiveTenDayRateBeginDate;
	}

	public void setTenYearFiveTenDayRateBeginDate(
			Date tenYearFiveTenDayRateBeginDate) {
		this.tenYearFiveTenDayRateBeginDate = tenYearFiveTenDayRateBeginDate;
	}

	public Date getTenYearFiveTenDayRateEndDate() {
		return tenYearFiveTenDayRateEndDate;
	}

	public void setTenYearFiveTenDayRateEndDate(
			Date tenYearFiveTenDayRateEndDate) {
		this.tenYearFiveTenDayRateEndDate = tenYearFiveTenDayRateEndDate;
	}

	public Date getLimitUpDownBeginDate() {
		return limitUpDownBeginDate;
	}

	public void setLimitUpDownBeginDate(Date limitUpDownBeginDate) {
		this.limitUpDownBeginDate = limitUpDownBeginDate;
	}

	public Date getLimitUpDownEndDate() {
		return limitUpDownEndDate;
	}

	public void setLimitUpDownEndDate(Date limitUpDownEndDate) {
		this.limitUpDownEndDate = limitUpDownEndDate;
	}
}
