package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelGoldCrossDeadCrossAnalysis;
import cn.com.acca.ma.service.ModelGoldCrossDeadCrossAnalysisService;

import java.util.List;

public class ModelGoldCrossDeadCrossAnalysisServiceImpl extends BaseServiceImpl<ModelGoldCrossDeadCrossAnalysisServiceImpl, ModelGoldCrossDeadCrossAnalysis> implements
        ModelGoldCrossDeadCrossAnalysisService {

    @Override
    public String listToString(List list) {
        return null;
    }

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的基础数据
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysis() {
        modelGoldCrossDeadCrossAnalysisDao.writeModelGoldCrossDeadCrossAnalysis();
    }

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的百分比的均值
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysisPercentMA() {
        modelGoldCrossDeadCrossAnalysisDao.writeModelGoldCrossDeadCrossAnalysisPercentMA();
    }

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的成功率的均值
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysisSuccessRateMA() {
        modelGoldCrossDeadCrossAnalysisDao.writeModelGoldCrossDeadCrossAnalysisSuccessRateMA();
    }

    /**
     * 根据日期，计算表MDL_G_C_D_C_ANALYSIS的数据
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysisByDate() {
        String beginDate = PropertiesUtil.getValue(MODEL_GOLD_CROSS_DEAD_CROSS_ANALYSIS_PROPERTIES, "stock.gold.cross.dead.cross.analysis.beginDate");
        String endDate = PropertiesUtil.getValue(MODEL_GOLD_CROSS_DEAD_CROSS_ANALYSIS_PROPERTIES, "stock.gold.cross.dead.cross.analysis.endDate");
        modelGoldCrossDeadCrossAnalysisDao.writeModelGoldCrossDeadCrossAnalysisByDate(beginDate, endDate);
    }
}
