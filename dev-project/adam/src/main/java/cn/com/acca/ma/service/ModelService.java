package cn.com.acca.ma.service;

public interface ModelService extends BaseService {

    /**
     * 将所有交易策略金叉成功率和收盘价绘制在一张折线图上
     * @param multithreading
     */
    void createAllTransactionStrategyGoldCrossSuccessRatePicture(boolean multithreading);

    /**
     * 将所有交易策略死叉成功率和收盘价绘制在一张折线图上
     * @param multithreading
     */
    void createAllTransactionStrategyDeadCrossSuccessRatePicture(boolean multithreading);

    /**
     * 将所有交易策略金叉百分比和收盘价绘制在一张折线图上
     * @param multithreading
     */
    void createAllTransactionStrategyGoldCrossPercentagePicture(boolean multithreading);

    /**
     * 将所有交易策略死叉百分比和收盘价绘制在一张折线图上
     * @param multithreading
     */
    void createAllTransactionStrategyDeadCrossPercentagePicture(boolean multithreading);

    /**
     * 从robot开头的表，将测试数据导入到HIS_开头的表中。如果被错误地执行了多次也没事
     */
    void importIntoHistoryDataTableFromRobot();

    /**
     * 从robot2开头的表，将测试数据导入到HIS_开头的表中。如果被错误地执行了多次也没事
     */
    void importIntoHistoryDataTableFromRobot2();

    /**
     * 从robot3开头的表，将测试数据导入到HIS_开头的表中。如果被错误地执行了多次也没事
     */
    void importIntoHistoryDataTableFromRobot3();

    /**
     * 从robot4开头的表，将测试数据导入到HIS_开头的表中。如果被错误地执行了多次也没事
     */
    void importIntoHistoryDataTableFromRobot4();

    /**
     * 从robot5开头的表，将测试数据导入到HIS_开头的表中。如果被错误地执行了多次也没事
     */
    void importIntoHistoryDataTableFromRobot5();

    /**
     * 从robot6开头的表，将测试数据导入到HIS_开头的表中。如果被错误地执行了多次也没事
     */
    void importIntoHistoryDataTableFromRobot6();

    /**
     * 创建机器人账户的收益率图表
     * @param multithreading
     */
    void createRobotAccountLogProfitRatePicture(boolean multithreading);

    /**
     * 创建机器人账户（ETF）的收益率图表
     * @param multithreading
     */
    void createRobotEtfAccountLogProfitRatePicture(boolean multithreading);

}
