package cn.com.acca.ma.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 股票信息
 * @author Administrator
 */
@Entity
@Table(name="STOCK_INFO")
public class StockInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_")
	private Integer id;

	/**
	 * 表示股票代码
	 */
	@Column(name="CODE_")
	private String code;

	/**
	 * 表示股票名称
	 */
	@Column(name="NAME_")
	private String name;

	/**
	 * 表示股票所属的板块
	 */
	@ManyToOne
	private Board board;

	/**
	 * 获取股票实时交易数据的url中的参数
	 */
	@Column(name="URL_PARAM")
	private String urlParam;

	/**
	 * R表示融资融券
	 */
	@Column(name="MARK")
	private String mark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public String getUrlParam() {
		return urlParam;
	}

	public void setUrlParam(String urlParam) {
		this.urlParam = urlParam;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}
}
