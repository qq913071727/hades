package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelHeiKinAshiUpDown;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelHeiKinAshiUpDownDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线ha_close_price大于ha_open_price时买入，ha_close_price小于等于ha_open_price时卖出，
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN插入数据
     * @param multithreading
     * @param endDate
     */
    void writeModelHeiKinAshiUpDownIncr(boolean multithreading, String endDate);

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_close_price大于ha_open_price时买入，ha_close_price小于等于ha_open_price时卖出，
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN插入数据
     */
    void writeModelHeiKinAshiUpDown();

    /**
     * 获取一段时间内，最近dateNumber天，hei_kin_ashi上涨趋势的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    List<SuccessRateOrPercentagePojo> getHeiKinAshiUpDownSuccessRate(boolean multithreading, String beginDate, String endDate, Integer dateNumber);

    /**
     * 获取一段时间内，hei_kin_ashi上涨趋势的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<SuccessRateOrPercentagePojo> getHeiKinAshiUpDownPercentage(boolean multithreading, String beginDate, String endDate);

    /*********************************************************************************************************************
     *
     * 										增量备份表MDL_HEI_KIN_ASHI_UP_DOWN
     *
     *********************************************************************************************************************/
    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_HEI_KIN_ASHI_UP_DOWN中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_HEI_KIN_ASHI_UP_DOWN中获取date日的所有ModelHeiKinAshiUpDown对象
     * @param date
     * @return
     */
    List<ModelHeiKinAshiUpDown> getModelHeiKinAshiUpDownByDate(String date);

    /**
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN中插入ModelHeiKinAshiUpDown对象
     * @param modelHeiKinAshiUpDown
     */
    void save(ModelHeiKinAshiUpDown modelHeiKinAshiUpDown);
}
