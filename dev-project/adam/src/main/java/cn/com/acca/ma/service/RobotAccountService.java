package cn.com.acca.ma.service;

public interface RobotAccountService extends BaseService {

    /**************************************** 卖股票/买股票 ********************************************/
    /**
     * 根据当日持有股票的收盘价，在卖股票/买股票之前，更新robot_account表
     */
    void updateRobotAccountBeforeSellOrBuy();

    /**
     * 根据当日卖出/买入股票的收盘价，在卖股票/买股票之后，更新robot_account表
     */
    void updateRobotAccountAfterSellOrBuy();

    /**************************************** 买股票/卖股票 ********************************************/
    /**
     * 根据当日买入股票/卖出股票的收盘价，在买股票/卖股票之后，更新robot_account表
     */
    void updateRobotAccountAfterBuyOrSell();
}
