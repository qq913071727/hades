package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Robot6AccountDao;

public class Robot6AccountDaoImpl extends BaseDaoImpl<Robot6AccountDao> implements
        Robot6AccountDao {

    public Robot6AccountDaoImpl() {
        super();
    }

    /**************************************** 卖etf ********************************************/
    /**
     * 根据当日持有etf的收盘价，在卖etf之前，更新robot6_account表
     * @param date
     */
    @Override
    public void updateRobotAccountBeforeSellOrBuy(String date){
        logger.info("更新robot6_account表的etf_assets、total_assets字段，"
                + "日期为【" + date + "】");

        String sql = "{call PKG_ROBOT6.update_robot_account_b_s_b('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 根据当日卖出etf的收盘价，在卖etf之后，更新robot6_account表
     * @param date
     */
    @Override
    public void updateRobotAccountAfterSellOrBuy(String date) {
        logger.info("更新robot6_account表的hold_etf_number、etf_assets、capital_assets字段，"
                + "日期为【" + date + "】");

        String sql = "{call PKG_ROBOT6.update_robot_account_after_s_b('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 返回记录数
     * @return
     */
    @Override
    public Integer count() {
        logger.info("返回记录数");

        String sql = "select count(*) from robot6_account";

        return doSQLQueryInTransaction(sql, null);
    }

    /**
     * 列hold_etf_number求和
     * @return
     */
    @Override
    public Integer sumHoldEtfNumber() {
        logger.info("列hold_etf_number求和");

        String sql = "select sum(hold_stock_number) from robot6_account";

        return doSQLQueryInTransaction(sql, null);
    }

    /**************************************** 买etf  ********************************************/
    /**
     * 根据当日买入etf的收盘价，在买etf之后，更新robot6_account表
     * @param date
     */
    @Override
    public void updateRobotAccountAfterBuyOrSell(String date) {
        logger.info("更新robot6_account表的hold_etf_number、etf_assets、capital_assets字段，"
                + "日期为【" + date + "】");

        String sql = "{call PKG_ROBOT6.update_robot_account_after_b_s('" + date + "')}";

        doSQLInTransaction(sql);
    }
}
