package cn.com.acca.ma.constant;

/**
 * 板块类型
 */
public class Board2Type {

    /**
     * 证监会行业板块
     */
    public static final Integer INDUSTRY_BOARD = 1;

    /**
     * 概念板块
     */
    public static final Integer CONCEPTION_BOARD = 2;

}
