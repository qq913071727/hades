package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.CommodityFutureDateContractData;
import cn.com.acca.ma.model.ForeignExchangeRecord;

import java.util.List;

public interface CommodityFutureDateContractDataDao extends BaseDao {

    /**
     * 根据开始日期beginDate和结束日期endDate，获取DATETIME字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<CommodityFutureDateContractData> getDateByCondition(String beginDate, String endDate);

    /**
     * 根据日期date获取所有CommodityFutureDateContractData对象
     * @param date
     * @return
     */
    List<CommodityFutureDateContractData> getCommodityFutureDateContractDataByDate(String date);
}
