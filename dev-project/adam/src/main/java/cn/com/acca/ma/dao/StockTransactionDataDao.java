package cn.com.acca.ma.dao;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.StockTransactionData;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cn.com.acca.ma.enumeration.UpDown;
import cn.com.acca.ma.model.StockInfo;
import cn.com.acca.ma.model.StockTransactionDataAll;
import cn.com.acca.ma.pojo.*;

public interface StockTransactionDataDao extends BaseDao {

    /**
     * 根据股票代码和日期，查找StockTransactionData类型对象
     *
     * @param code
     * @param date
     * @return
     */
    StockTransactionDataAll findByCodeAndDate(String code, Date date);

    /**
     * 根据股票代码和日期，查找上一个交易日的StockTransactionData类型对象
     *
     * @param code
     * @param date
     * @return
     */
    StockTransactionData findLastByCodeAndDate(String code, Date date);

    /**
     * 查找某只股票开始时间和结束时间之内的最大收盘价
     *
     * @param stockCode
     * @param beginDate
     * @param endDate
     * @return
     */
    BigDecimal findMaxClosePriceBetweenDate(String stockCode, String beginDate, String endDate);

    /**
     * 查找某只股票开始时间和结束时间之内的最小收盘价
     *
     * @param stockCode
     * @param beginDate
     * @param endDate
     * @return
     */
    BigDecimal findMinClosePriceBetweenDate(String stockCode, String beginDate, String endDate);

    /**
     * 查找某只股票开始时间和结束时间之内的最大最高价
     *
     * @param stockCode
     * @param beginDate
     * @param endDate
     * @return
     */
    BigDecimal findMaxHighestPriceBetweenDate(String stockCode, String beginDate, String endDate);

    /**
     * 查找某只股票开始时间和结束时间之内的最小最低价
     *
     * @param stockCode
     * @param beginDate
     * @param endDate
     * @return
     */
    BigDecimal findMinLowestPriceBetweenDate(String stockCode, String beginDate, String endDate);

    /**
     * 判断某只股票在某个时间区间内是否除权了。如果出现除权，则返回1；否则返回-1
     *
     * @param stockCode
     * @param beginDate
     * @param endDate
     * @return
     */
    Integer stockXR(String stockCode, String beginDate, String endDate);

    /**
     * 根据code查找StockTransactionData对象中最大的日期
     *
     * @param code
     * @return
     */
    Date findMaxDateByCode(String code);

    /**
     * 根据code和date，查找某只股票最后一个交易日的close_price
     *
     * @param multithreading
     * @param code
     * @param date
     * @return
     */
    BigDecimal findLastClosePriceByCodeAndDate(boolean multithreading, String code, String date);

    /**
     * 某一只股票，在某一日，120均线是否不单调递减，数据库里查119条+currentClosePrice。true表示是，false表示否
     *
     * @param stockCode
     * @param currentClosePrice
     * @param date
     * @return
     */
    Boolean isMa120NotDecreasing(String stockCode, BigDecimal currentClosePrice, String date);

    /**
     * 某一只股票，在某一日，250均线是否不单调递减，数据库里查249条+currentClosePrice。true表示是，false表示否
     *
     * @param stockCode
     * @param currentClosePrice
     * @param date
     * @return
     */
    Boolean isMa250NotDecreasing(String stockCode, BigDecimal currentClosePrice, String date);

    /**
     * 某一只股票，在某一日，120均线是否不单调递增，数据库里查119条+currentClosePrice。true表示是，false表示否
     *
     * @param stockCode
     * @param currentClosePrice
     * @param date
     * @return
     */
    Boolean isMa120NotIncreasing(String stockCode, BigDecimal currentClosePrice, String date);

    /**
     * 某一只股票，在某一日，250均线是否不单调递增，数据库里查249条+currentClosePrice。true表示是，false表示否
     *
     * @param stockCode
     * @param currentClosePrice
     * @param date
     * @return
     */
    Boolean isMa250NotIncreasing(String stockCode, BigDecimal currentClosePrice, String date);

    /**
     * 查找所有股票某一日的前收盘
     *
     * @param date
     * @return
     */
    List findLastClosePrice(Date date);

    /**
     * 查找turnover_rate、turnover、total_market_value、circulation_market_value字段为空的记录
     *
     * @param beginDate
     * @param endDate
     * @return
     */
    List<StockTransactionData> findByTurnoverRateNullOrTurnoverNullOrTotalMarketValueNullOrCirculationMarketValueNull(String beginDate, String endDate);

    /**
     * 根据code进行分组，查找每只股票的最后交易日期和最后的收盘价
     *
     * @return
     */
    List findCodeAndMaxDateAndLastClosePriceGroupByCode();

    /**
     * 将从网络中（或.json文件中）读取的数据写入到表STOCK_TRANSACTION_DATA中。
     *
     * @param stockTransactionDataList
     */
    void writeStockTransactionDataList(List<StockTransactionData> stockTransactionDataList);

    /**
     * 将从网络中读取数据，更新STOCK_TRANSACTION_DATA表中的数据。
     *
     * @param stockTransactionData
     */
    void updateStockTransactionData(StockTransactionData stockTransactionData);

    /**
     * 从表STOCK_INFO中读取数据
     *
     * @return
     */
    List<StockInfo> readStockInfo();

    /***************************** 从网络上获取数据，存储在.json文件中；然后再解析文件，并插入到数据库中 *************************/
    /**
     * 将.json文件中的数据插入数据库
     */
    void insertAllRecordsFromJson(List<StockTransactionData> stockTransactionDataList);

    /************************************************ 更新每天的数据 *********************************************************/
    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的FIVE，TEN，TWENTY，SIXTY和ONE_HUNDRED_TWENTY字段
     *
     * @param multithreading
     * @param date
     */
    void writeMovingAverageByDate(boolean multithreading, String date);

    /**
     * 更新date日，更新stock_transaction_data表的change_range_ex_right字段的值
     *
     * @param date
     */
    void writeChangeRangeExRightByDate(String date);

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的UP_DOWN字段
     */
    void writeUpDownByDate(String date);

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的EMA15，EMA26，DIF和DEA字段
     *
     * @param multithreading
     * @param date
     */
    void writeMACDByDate(boolean multithreading, String date);

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的TODAY_UP_DOWN_PERCENTAGE字段
     */
    void calUpDownPercentageByDate(String date);

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的FIVE_DAY_VOLATILITY，TEN_DAY_VOLATILITY和TWENTY_DAY_VOLATILITY字段
     */
    void calVolatilityByDate(String date);

    /**
     * 更新stock_transaction_data表中某一日的KD指标
     *
     * @param multithreading
     * @param date
     */
    void writeKDByDate(boolean multithreading, String date);

    /**
     * 更新stock_transaction_data表中某一日的HA_OPEN_PRICE、HA_CLOSE_PRICE、HA_HIGHEST_PRICE、HA_LOWEST_PRICE指标
     *
     * @param multithreading
     * @param date
     */
    void writeHaByDate(boolean multithreading, String date);

    /**
     * 更新stock_transaction_data表中某一日的boll相关字段
     *
     * @param multithreading
     * @param date
     */
    void writeBollByDate(boolean multithreading, String date);

    /**
     * 更新stock_transaction_data表中某一日的bias相关字段
     *
     * @param multithreading
     * @param date
     */
    void writeBiasByDate(boolean multithreading, String date);

    /**
     * 更新stock_transaction_data表中某一日的方差相关字段
     *
     * @param multithreading
     * @param date
     */
    void writeVarianceByDate(boolean multithreading, String date);

    /**
     * 更新stock_transaction_data_all表中某一日的成交量移动平均线相关字段
     *
     * @param multithreading
     * @param date
     */
    void writeVolumeMaByDate(boolean multithreading, String date);

    /**
     * 更新stock_transaction_data_all表中某一日的成交额移动平均线相关字段
     *
     * @param multithreading
     * @param date
     */
    void writeTurnoverMaByDate(boolean multithreading, String date);

    /**************************************** 根据每日数据更新图表 *************************************************************/
    /**
     * 根据开始日期和结束日期，获取其中的所有交易日期
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param tenYear
     * @return
     */
    List<String> getDateByCondition(boolean multithreading, String beginDate, String endDate, boolean tenYear);

    /**
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<String> findDistinctDateBetweenDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取某一日dif的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageDif(boolean multithreading, String date);

    /**
     * 获取某一日dea的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageDea(boolean multithreading, String date);

    /**
     * 获取某一日k的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageK(boolean multithreading, String date);

    /**
     * 获取某一日d的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageD(boolean multithreading, String date);

    /**
     * 获取某一日close_price的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageClosePrice(boolean multithreading, String date);

    /**
     * 获取某一日d的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageMA5(boolean multithreading, String date);

    /**
     * 获取某一日ha_close_price的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageHaClosePrice(boolean multithreading, String date);

    /**
     * 获取某一日ha_open_price的平均值
     *
     * @param multithreading
     * @param date
     * @return
     */
    String getAverageHaOpenPrice(boolean multithreading, String date);

    /**
     * 获取从开始日期至结束日期，上证指数的所有交易日的日期
     *
     * @param multithreading
     * @param closeBeginDate
     * @param closeEndDate
     * @param indexCode
     * @return
     */
    List<String> getDateByConditionForClose(boolean multithreading, String closeBeginDate, String closeEndDate, String indexCode);

    /**
     * 获取从开始日期至结束日期，每个交易日所有股票的平均价格（所有股票的收盘价之和/参加交易的股票的数量）
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param tenYear
     * @param codePrefixArray
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getAverageCloseWithDate(boolean multithreading, String beginDate, String endDate, boolean tenYear, String... codePrefixArray);

    /**
     * 为创建28分化图表中的柱状图，获取某个指定日期时，UP_DOWN字段分别为1,-1和0的数量
     *
     * @param multithreading
     * @param date
     * @param upDown
     * @return
     */
    int getUpDownNumberForBarChart(boolean multithreading, String date, UpDown upDown);

    /**
     * 获取某一段时间内股票涨停或跌停家数
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param rate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getStockLimitUpAndDownNumber(boolean multithreading, String beginDate, String endDate, double rate);

    /**
     * 获取从开始日期至结束日期内MACD处于金叉状态（dif>dea）的股票的比率
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMACDGoldCrossStockRate(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取某一段时间内平均价格中的最大价格和最小价格
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMaxMinAverageCloseWeek(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取某一段时间内dif和dea的最大价格和最小价格
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMaxMinDifDea(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取某一段时间内k和d的最大价格和最小价格
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMaxMinKD(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取某一段时间内close_price和ma5的最大价格和最小价格
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMaxMinClosePriceAndMa5(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取某一段时间内ha_close_price和ha_open_price的最大价格和最小价格
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMaxMinHaClosePriceAndHaOpenPrice(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取上证股票，深证股票，中小板股票和创业板股票日线级别平均涨跌
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param indexPrefix
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getStockBigBoardUpDownPercentage(boolean multithreading, String beginDate, String endDate, String indexPrefix);

    /************************************************* 更新所有的数据 **********************************************************/
    void calculateMovingAverage();

    void writeUpDown();

    void writeChangeRangeExRight();

    void writeMACD();

    void calculateUpDownPercentage();

    void calculateVolatility();

    void writeKD();

    void writeHa();

    void writeBoll();

    void writeBias();

    void writeVariance();

    void writeVolumeMa();

    void writeTurnoverMa();

    /**
     * 获取某一日多头排列的股票
     *
     * @param date
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getBullRank(String date);

    /**
     * 获取某一日空头排列的股票
     *
     * @param date
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getShortOrder(String date);

    /**
     * 获取某段时间内，每个交易日的多头排列的股票的数量
     *
     * @param beginDate
     * @param endDate
     * @param tenYear
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getBullRankNumberWithinDate(String beginDate, String endDate, boolean tenYear);

    /**
     * 获取某段时间内，每个交易日的空头排列的股票的数量
     *
     * @param beginDate
     * @param endDate
     * @param tenYear
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getShortOrderNumberWithinDate(String beginDate, String endDate, boolean tenYear);

    /**
     * STOCK_TRANSACTION_DATA表中，某个交易日的全部记录
     *
     * @param date
     * @return
     */
    List<StockTransactionData> getStocksByDate(String date);

    /**
     * 根据开始日期和结束日期，删除表stock_transaction_data中的记录
     */
    void deleteStockTransactionDataByDate(String beginDate, String endDate);

    /************************************************* RobotMain类过滤股票 *********************************************************************/
    /**
     * 根据date，查找StockTransactionData对象列表
     *
     * @param date
     * @return
     */
    List<StockTransactionData> findByDate(String date);

    /**
     * 根据条件code、date，查找stock_transaction_data表，按降序排列，只取前rownum条记录
     *
     * @param code
     * @param date
     * @param rownum
     * @param multithreading
     * @return
     */
    List<StockTransactionData> findByCodeAndDateOrderByDateDescLtqRownum(String code, String date, Integer rownum, boolean multithreading);

    /**
     * 根据开始日期和结束日期，查找中间的日期，并去重
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<String> findDistinctDateBetweenBeginDateAndEndDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 从stock_index表中查找某个交易日之前的日期
     *
     * @param multithreading
     * @param date
     * @return
     */
    Date findPreviousDateByDate(boolean multithreading, String date);

    /**
     * 获取当前日期和前一日的平均dif和平均dea
     *
     * @param multithreading
     * @param currentDate
     * @param previousDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List<BaseCurrentDataAndPreviousDateAverage> getAverageDifDeaByCurrentDateAndPreviousDate(boolean multithreading, final String currentDate, final String previousDate);

    /**
     * 获取当前日期和前一日的平均close_price和平均ma5
     *
     * @param multithreading
     * @param currentDate
     * @param previousDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List<BaseCurrentDataAndPreviousDateAverage> getAverageClosePriceMa5ByCurrentDateAndPreviousDate(boolean multithreading, final String currentDate, final String previousDate);

    /**
     * 获取当前日期和前一日的平均ha_close_price和平均ha_open_price
     *
     * @param multithreading
     * @param currentDate
     * @param previousDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List<BaseCurrentDataAndPreviousDateAverage> getAverageHaClosePriceHaOpenPriceByCurrentDateAndPreviousDate(boolean multithreading, final String currentDate, final String previousDate);

    /**
     * 获取当前日期和前一日的平均k和平均d
     *
     * @param multithreading
     * @param currentDate
     * @param previousDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List<BaseCurrentDataAndPreviousDateAverage> getAverageKDByCurrentDateAndPreviousDate(boolean multithreading, final String currentDate, final String previousDate);

    /************************************************* RealTransactionMain用到的方法 *********************************************************************/

    /**
     * 根据code和date，查找最近8日内的最高收盘价和最低收盘价
     *
     * @param code
     * @param date
     * @return
     */
    Object findHighestAndLowestClosePriceByCodeAndDateInEightDay(String code, Date date);

    /***************************************** 打印文本报告 **********************************************************/
    /**
     * 查询波段操作的股票
     * @param date
     * @param mailHost
     * @param mailTransportProtocol
     * @param mailSmtpAuth
     * @param mailUsername
     * @param mailPassword
     * @param mailSubject
     */
    void findBandOperationStockAndSendMail(String date, String mailHost, String mailTransportProtocol, String mailSmtpAuth, String mailUsername, String mailPassword, String mailSubject);

    /************************************************* 实验 *********************************************************************/
    void test();

    /************************************************* 备份项目代码和数据库的表 *********************************************************************/
    /**
     * 持久化一个StockTransactionData对象
     *
     * @param sma
     */
    void save(StockTransactionData sma);
}
