package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockAnalysis;
import cn.com.acca.ma.model.ModelTopStock;
import java.util.Date;
import java.util.List;

public interface ModelStockAnalysisDao extends BaseDao {

    /**
     * 计算MDL_STOCK_ANALYSIS表的全部数据
     */
    void writeModelStockAnalysis();

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_STOCK_ANALYSIS中获取DATE_字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 计算MDL_STOCK_ANALYSIS表的某一日的数据
     * @param multithreading
     * @param date
     */
    void writeModelStockAnalysisByDate(boolean multithreading, String date);

    /**
     * 从表MDL_STOCK_ANALYSIS中获取date日的所有ModelStockAnalysis对象
     * @param date
     * @return
     */
    List<ModelStockAnalysis> getModelStockAnalysisByDate(String date);

    /**
     * 向表MDL_STOCK_ANALYSIS中插入ModelStockAnalysis对象
     * @param modelStockAnalysis
     */
    void save(ModelStockAnalysis modelStockAnalysis);

    /**
     * 根据开始时间和结束时间，计算每个交易日、所有股票的平均收盘价和平均方差，并按时间升序排列
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Object> findAverageClosePriceAndAverageStandardDeviationByBeginDateAndEndDateOrderByDate(String beginDate, String endDate);
}
