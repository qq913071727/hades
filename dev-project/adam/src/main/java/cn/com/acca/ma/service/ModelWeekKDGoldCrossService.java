package cn.com.acca.ma.service;

public interface ModelWeekKDGoldCrossService extends BaseService {
	
	/*********************************************************************************************************************
	 * 
	 * 												计算全部数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用周线级别KD金叉模型算法，向表MDL_WEEK_KD_GOLD_CROSS插入数据
	 */
	void writeModelWeekKDGoldCross();
}
