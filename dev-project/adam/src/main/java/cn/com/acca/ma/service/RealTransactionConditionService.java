package cn.com.acca.ma.service;

public interface RealTransactionConditionService extends BaseService {

    /**
     * 预先计算股票的买卖条件
     */
    void preCalculateTransactionCondition_real();
}
