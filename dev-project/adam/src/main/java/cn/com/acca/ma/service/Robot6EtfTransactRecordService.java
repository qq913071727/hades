package cn.com.acca.ma.service;

public interface Robot6EtfTransactRecordService extends BaseService {

    /**
     * 执行某段时间内的股票买卖交易（etf四种算法最近12年中平均收益率最高的）
     */
    void doBuyAndSellByBeginDateAndEndDate_etf12YearHighestAverageProfitRate();
}
