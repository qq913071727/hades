package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 周线级别平均MA和close_price
 */
@Data
public class StockWeekAverageClosePriceAndAllAverageMa {

    /**
     * 平均MA5
     */
    private BigDecimal averageMA5;

    /**
     * 平均MA10
     */
    private BigDecimal averageMA10;

    /**
     * 平均MA20
     */
    private BigDecimal averageMA20;

    /**
     * 平均MA60
     */
    private BigDecimal averageMA60;

    /**
     * 平均MA120
     */
    private BigDecimal averageMA120;

    /**
     * 平均MA250
     */
    private BigDecimal averageMA250;


    /**
     * 平均close_price
     */
    private BigDecimal averageClosePrice;

    /**
     * 这一周的最后一天
     */
    private Date endDate;
}
