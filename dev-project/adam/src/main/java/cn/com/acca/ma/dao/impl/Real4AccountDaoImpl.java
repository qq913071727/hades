package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Real4AccountDao;
import cn.com.acca.ma.model.Real4Account;

import java.util.List;

public class Real4AccountDaoImpl extends BaseDaoImpl<Real4AccountDao> implements
        Real4AccountDao {

    public Real4AccountDaoImpl() {
        super();
    }

    /**
     * 查找所有Real4Account类型对象
     *
     * @return
     */
    @Override
    public List<Real4Account> findAll() {
        logger.info("查找所有Real4Account类型对象");

        String sql = "select * from real4_account";

        return doSQLQueryInTransaction(sql, null, Real4Account.class);
    }
}
