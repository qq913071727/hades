package cn.com.acca.ma.service;

public interface ModelStockMonthAnalysisService extends BaseService {

    /**
     * 计算model_stock_month_analysis表的全部数据
     */
    void writeModelStockMonthAnalysis();

    /**
     * 计算model_stock_month_analysis表的某一日的数据
     * @param multithreading
     */
    void writeModelStockMonthAnalysisByDate(boolean multithreading);
}
