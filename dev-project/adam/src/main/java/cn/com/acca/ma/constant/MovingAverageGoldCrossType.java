package cn.com.acca.ma.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 计算表mdl_all_gold_cross表数据时，表示金叉类型的常量类
 */
public class MovingAverageGoldCrossType {

    public static final List<String> MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST = new ArrayList<>();

    static {
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("120_250");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("60_250");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("20_250");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("10_250");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("5_250");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("c_250");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("60_120");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("20_120");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("10_120");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("5_120");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("c_120");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("20_60");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("10_60");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("5_60");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("c_60");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("10_20");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("5_20");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("c_20");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("5_10");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("c_10");
        MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.add("c_5");
    }

}
