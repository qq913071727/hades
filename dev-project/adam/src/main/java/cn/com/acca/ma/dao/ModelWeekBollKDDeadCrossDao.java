package cn.com.acca.ma.dao;

import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;

import java.util.List;

public interface ModelWeekBollKDDeadCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_BOLL_KD_DEAD_CROSS中插入数据
     */
    void writeModelWeekBollKDDeadCross();

    /**
     * 查询KD死叉交易收益率和最高价突破布林带上轨的百分比
     * @param beginDate
     * @param endDate
     */
    List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekKDDeadCrossProfitOrLossAndBollUp(String beginDate, String endDate);
}
