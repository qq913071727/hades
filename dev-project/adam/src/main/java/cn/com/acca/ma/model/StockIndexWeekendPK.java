package cn.com.acca.ma.model;

import java.util.Date;

import javax.persistence.Id;

public class StockIndexWeekendPK {
	@Id
	private String indexCode;
	
	@Id
	private Date indexBeginDate;
	
	@Id
	private Date indexEndDate;

	private StockIndexWeekendPK() {
	}

	private StockIndexWeekendPK(String indexCode, Date indexBeginDate,
			Date indexEndDate) {
		super();
		this.indexCode = indexCode;
		this.indexBeginDate = indexBeginDate;
		this.indexEndDate = indexEndDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((indexBeginDate == null) ? 0 : indexBeginDate.hashCode());
		result = prime * result
				+ ((indexCode == null) ? 0 : indexCode.hashCode());
		result = prime * result
				+ ((indexEndDate == null) ? 0 : indexEndDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockIndexWeekendPK other = (StockIndexWeekendPK) obj;
		if (indexBeginDate == null) {
			if (other.indexBeginDate != null)
				return false;
		} else if (!indexBeginDate.equals(other.indexBeginDate))
			return false;
		if (indexCode == null) {
			if (other.indexCode != null)
				return false;
		} else if (!indexCode.equals(other.indexCode))
			return false;
		if (indexEndDate == null) {
			if (other.indexEndDate != null)
				return false;
		} else if (!indexEndDate.equals(other.indexEndDate))
			return false;
		return true;
	}
	
}
