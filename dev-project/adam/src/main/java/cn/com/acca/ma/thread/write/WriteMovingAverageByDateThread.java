package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 计算股票的移动平均线
 */
public class WriteMovingAverageByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteMovingAverageByDateThread.class);

    private StockTransactionDataService stockTransactionDataService;

    public WriteMovingAverageByDateThread() {
    }

    public WriteMovingAverageByDateThread(StockTransactionDataService stockTransactionDataService) {
        this.stockTransactionDataService = stockTransactionDataService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始计算股票的移动平均线，调用的方法为【writeMovingAverageByDate】");

        stockTransactionDataService.writeMovingAverageByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
