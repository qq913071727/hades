package cn.com.acca.ma.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 真实交易条件--预先计算的交易条件
 */
//@Data
public class Real4TransactionCondition {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 交易日期
     */
    private Date transactionDate;

    /**
     * 计算这条记录的日期
     */
    private Date calculateDate;

    /**
     * MACD金叉时的阈值（如果收盘价大于这个值，则金叉）
     */
    private BigDecimal macdGoldCrossClosePrice;

    /**
     * MACD死叉时的阈值（如果收盘价小于这个值，则死叉）
     */
    private BigDecimal macdDeadCrossClosePrice;

    /**
     * 表示是否除过权。1表示除过，0表示没有
     */
    private Integer isXr;

    /**
     * 表示当前收盘价是否在某个价格价格区间之内。1表示是，0表示否
     */
    private Integer isBetweenClosePrice;

    /**
     * 表示当前收盘价是否在某段时间最高价的百分比之下。1表示是，0表示否
     */
    private Integer isLessThanPercentage;

    /**
     * 表示当前收盘价是否在某段时间最低价的百分比之下。1表示是，0表示否
     */
    private Integer isMoreThanPercentage;

    /**
     * 表示120日均线是否单调不递减。1表示是，-1表示否
     */
    private Integer isMa120NotDecreasing;

    /**
     * 表示250日均线是否单调不递减。1表示是，-1表示否
     */
    private Integer isMa250NotDecreasing;

    /**
     * 收盘价金叉ma5时的收盘价
     */
    private BigDecimal cPMa5GCClosePrice;

    /**
     * 收盘价死叉ma5时的收盘价
     */
    private BigDecimal cPMa5DCClosePrice;

    /**
     * kei_kin_ashi处于上涨趋势时的kei_kin_ashi开盘价
     */
    private BigDecimal hKAUpHKAOpenPrice;

    /**
     * kei_kin_ashi处于下跌趋势时的kei_kin_ashi开盘价
     */
    private BigDecimal hKADownHKAOpenPrice;

    /**
     * kd金叉时的RSV
     */
    private BigDecimal kdGoldCrossRsv;

    /**
     * kd死叉时的RSV
     */
    private BigDecimal kdDeadCrossRsv;

    /**
     * 表示120日均线是否单调不递增。1表示是，-1表示否
     */
    private Integer isMa120NotIncreasing;

    /**
     * 表示250日均线是否单调不递增。1表示是，-1表示否
     */
    private Integer isMa250NotIncreasing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getCalculateDate() {
        return calculateDate;
    }

    public void setCalculateDate(Date calculateDate) {
        this.calculateDate = calculateDate;
    }

    public BigDecimal getMacdGoldCrossClosePrice() {
        return macdGoldCrossClosePrice;
    }

    public void setMacdGoldCrossClosePrice(BigDecimal macdGoldCrossClosePrice) {
        this.macdGoldCrossClosePrice = macdGoldCrossClosePrice;
    }

    public BigDecimal getMacdDeadCrossClosePrice() {
        return macdDeadCrossClosePrice;
    }

    public void setMacdDeadCrossClosePrice(BigDecimal macdDeadCrossClosePrice) {
        this.macdDeadCrossClosePrice = macdDeadCrossClosePrice;
    }

    public Integer getIsXr() {
        return isXr;
    }

    public void setIsXr(Integer isXr) {
        this.isXr = isXr;
    }

    public Integer getIsBetweenClosePrice() {
        return isBetweenClosePrice;
    }

    public void setIsBetweenClosePrice(Integer isBetweenClosePrice) {
        this.isBetweenClosePrice = isBetweenClosePrice;
    }

    public Integer getIsLessThanPercentage() {
        return isLessThanPercentage;
    }

    public void setIsLessThanPercentage(Integer isLessThanPercentage) {
        this.isLessThanPercentage = isLessThanPercentage;
    }

    public Integer getIsMoreThanPercentage() {
        return isMoreThanPercentage;
    }

    public void setIsMoreThanPercentage(Integer isMoreThanPercentage) {
        this.isMoreThanPercentage = isMoreThanPercentage;
    }

    public Integer getIsMa120NotDecreasing() {
        return isMa120NotDecreasing;
    }

    public void setIsMa120NotDecreasing(Integer isMa120NotDecreasing) {
        this.isMa120NotDecreasing = isMa120NotDecreasing;
    }

    public Integer getIsMa250NotDecreasing() {
        return isMa250NotDecreasing;
    }

    public void setIsMa250NotDecreasing(Integer isMa250NotDecreasing) {
        this.isMa250NotDecreasing = isMa250NotDecreasing;
    }

    public BigDecimal getCPMa5GCClosePrice() {
        return cPMa5GCClosePrice;
    }

    public void setCPMa5GCClosePrice(BigDecimal cPMa5GCClosePrice) {
        this.cPMa5GCClosePrice = cPMa5GCClosePrice;
    }

    public BigDecimal getCPMa5DCClosePrice() {
        return cPMa5DCClosePrice;
    }

    public void setCPMa5DCClosePrice(BigDecimal cPMa5DCClosePrice) {
        this.cPMa5DCClosePrice = cPMa5DCClosePrice;
    }

    public BigDecimal getHKAUpHKAOpenPrice() {
        return hKAUpHKAOpenPrice;
    }

    public void setHKAUpHKAOpenPrice(BigDecimal hKAUpHKAOpenPrice) {
        this.hKAUpHKAOpenPrice = hKAUpHKAOpenPrice;
    }

    public BigDecimal getHKADownHKAOpenPrice() {
        return hKADownHKAOpenPrice;
    }

    public void setHKADownHKAOpenPrice(BigDecimal hKADownHKAOpenPrice) {
        this.hKADownHKAOpenPrice = hKADownHKAOpenPrice;
    }

    public BigDecimal getKdGoldCrossRsv() {
        return kdGoldCrossRsv;
    }

    public void setKdGoldCrossRsv(BigDecimal kdGoldCrossRsv) {
        this.kdGoldCrossRsv = kdGoldCrossRsv;
    }

    public BigDecimal getKdDeadCrossRsv() {
        return kdDeadCrossRsv;
    }

    public void setKdDeadCrossRsv(BigDecimal kdDeadCrossRsv) {
        this.kdDeadCrossRsv = kdDeadCrossRsv;
    }

    public Integer getIsMa120NotIncreasing() {
        return isMa120NotIncreasing;
    }

    public void setIsMa120NotIncreasing(Integer isMa120NotIncreasing) {
        this.isMa120NotIncreasing = isMa120NotIncreasing;
    }

    public Integer getIsMa250NotIncreasing() {
        return isMa250NotIncreasing;
    }

    public void setIsMa250NotIncreasing(Integer isMa250NotIncreasing) {
        this.isMa250NotIncreasing = isMa250NotIncreasing;
    }
}
