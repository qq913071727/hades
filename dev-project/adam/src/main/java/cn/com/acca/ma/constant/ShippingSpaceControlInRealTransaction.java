package cn.com.acca.ma.constant;

/**
 * 在真实交易中的仓位控制
 */
public class ShippingSpaceControlInRealTransaction {

    /**
     * 控制仓位
     */
    public final static Integer YES = 1;

    /**
     * 不控制仓位
     */
    public final static Integer NO = 2;
}
