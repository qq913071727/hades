package cn.com.acca.ma.enumeration;

public enum CurrencyLevel {
	ONE_WEEK("1周","1_W"),
	ONE_DAY("1日","1_D"),
	FOUR_HOUR("4小时","4_h"),
	ONE_HOUR("1小时","1_h");
	
	private String name;
	private String fileMark;

	private CurrencyLevel(String name, String fileMark) {
		this.name = name;
		this.fileMark = fileMark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileMark() {
		return fileMark;
	}

	public void setFileMark(String fileMark) {
		this.fileMark = fileMark;
	}
	
}
