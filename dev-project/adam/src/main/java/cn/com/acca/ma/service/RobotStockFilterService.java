package cn.com.acca.ma.service;

public interface RobotStockFilterService extends BaseService {

    /*********************************** RobotMain类过滤股票 ********************************************/
    /**
     * 先清空robot_stock_filter表，再向其添加数据
     * 用于做多
     */
    void initBullRobotStockFilter();

    /**
     * 先清空robot_stock_filter表，再向其添加数据
     * 用于做空
     */
    void initShortRobotStockFilter();

    /**
     * 只保留收盘价在某个价格之下的股票
     */
    void filterByLessThanClosePrice();

    /**
     * 过滤条件：删除除过权的股票
     */
    void filterByXr();

    /**
     * 过滤条件：周线级别KD金叉
     */
    void filterByWeekKDGoldCross();

    /**
     * 过滤条件：当前收盘价与某段时间最高价的百分比
     * 通常和filterByXr方法一起使用
     */
    void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice();

    /**
     * 过滤条件：当前收盘价与某段时间最低价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做空
     */
    void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice();

    /**
     * 过滤条件：MA不单调递减，过滤股票
     * 用于做多
     */
    void filterByMANotDecreasing();

    /**
     * 过滤条件：MA不单调递增，过滤股票
     * 用于做空
     */
    void filterByMANotIncreasing();

    /**
     * 选择最近成功率最高的那个算法
     * @param multithreading
     */
    void selectHighestSuccessRateStrategy(boolean multithreading);

    /**
     * 过滤条件：使用最近成功率最高的那个算法过滤股票
     */
    void filterByHighestSuccessRateStrategy();

    /**
     * 过滤条件：MACD金叉
     */
    void filterByMACDGoldCross();

    /**
     * 过滤条件：收盘价金叉五日均线
     */
    void filterByClosePriceGoldCrossMA5();

    /**
     * 过滤条件：平均K线从下跌趋势变为上涨趋势
     */
    void filterByHeiKinAshiUp();

    /**
     * 过滤条件：KD金叉
     */
    void filterByKDGoldCross();

}
