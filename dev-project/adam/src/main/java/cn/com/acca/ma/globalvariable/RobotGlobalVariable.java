package cn.com.acca.ma.globalvariable;

import cn.com.acca.ma.model.StockTransactionData;
import java.util.ArrayList;
import java.util.List;

/**
 * RobotMain类使用的全局变量
 */
public class RobotGlobalVariable {

    /**
     * StockTransactionData对象列表
     */
    public static List<StockTransactionData> stockTransactionDataList;

    /**
     * 模型线程完成数量
     */
    public static int modelThreadFinishNumber = 0;

    /**
     * renew一个List<StockTransactionData>类型对象
     */
    public static void renewStockTransactionDataList(){
        if (null == stockTransactionDataList){
            stockTransactionDataList = new ArrayList<>();
        } else {
            stockTransactionDataList.clear();
        }
    }

}
