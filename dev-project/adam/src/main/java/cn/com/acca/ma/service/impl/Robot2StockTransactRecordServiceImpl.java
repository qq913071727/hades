package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.constant.*;
import cn.com.acca.ma.model.Robot2StockTransactRecord;
import cn.com.acca.ma.pojo.BaseCurrentDataAndPreviousDateAverage;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import cn.com.acca.ma.service.Robot2StockTransactRecordService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Robot2StockTransactRecordServiceImpl extends BaseServiceImpl<Robot2StockTransactRecordServiceImpl, Robot2StockTransactRecord> implements
        Robot2StockTransactRecordService {

    /**
     * 执行某段时间内的股票买卖交易（平均金叉死叉）
     */
    @Override
    public void doBuyAndSellByBeginDateAndEndDate_averageGoldCrossDeadCross() {
        logger.info("执行某段时间内的股票买卖交易（平均金叉死叉）");

        String buySellBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.begin.date");
        String buySellEndDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.end.date");
        Double closePriceStart = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.closePrice.start"));
        Double closePriceEnd = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.closePrice.end"));
        String xrBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.xr.begin.date");
        Integer useSingleOrUseAllMethod = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.useSingleOrUseAllMethod"));
        Integer judgeMethod = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.judgeMethod"));
        Integer backwardMonth = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.backward.month"));
        Integer averageDateNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.average.date.number"));
        Integer macdMultiplicationFactor = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.average.date.macd.multiplication.factor"));
        Integer closePriceMa5MultiplicationFactor = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.average.date.closePriceMa5.multiplication.factor"));
        Integer heiKinAshiMultiplicationFactor = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.average.date.heiKinAshi.multiplication.factor"));
        Integer kdMultiplicationFactor = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.average.date.kd.multiplication.factor"));
        Double successRateThreshold = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.success.rate.threshold"));
        Double percentageTopThreshold = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.percentageTop.threshold"));
        Double percentageBottomThreshold = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.percentageBottom.threshold"));
        Integer shippingSpaceControl = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.shippingSpaceControl"));
        String currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.current.closePrice.compareTo.someTime.highest.lowest.price.begin.date");
        Integer currentClosePriceCompareToSomeTimeHighestPrice = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.current.closePrice.compareTo.someTime.highest.price"));
        Integer currentClosePriceCompareToSomeTimeLowestPrice = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.current.closePrice.compareTo.someTime.lowest.price"));
        String maNotIncreasingLevel = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.ma.not.increasing.level");
        String maNotIncreasingDateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.ma.not.increasing.dateNumber");
        String maNotDecreasingLevel = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.ma.not.decreasing.level");
        String maNotDecreasingDateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.ma.not.decreasing.dateNumber");
        Boolean useMinFilterCondition = Boolean.parseBoolean(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.useMinFilterCondition"));
        Integer mandatoryStopLoss = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.mandatoryStopLoss"));
        Double mandatoryStopLossRate = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.mandatoryStopLossRate"));
        Integer macdTransactionNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.macd.transactionNumber"));
        Integer closePriceMa5TransactionNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.closePriceMa5.transactionNumber"));
        Integer heiKinAshiTransactionNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.heiKinAshi.transactionNumber"));
        Integer kdTransactionNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.kd.transactionNumber"));
        Integer holdStockNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.holdStockNumber"));
        Integer printFormat = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.averageGoldCrossDeadCross.print.format"));

        // 获取日期列表
        List<String> dateList = stockTransactionDataDao.findDistinctDateBetweenBeginDateAndEndDate(false, buySellBeginDate, buySellEndDate);

        if (null != dateList && dateList.size() > 0) {
            for (int i = 0; i < dateList.size(); i++) {
                String currentDate = dateList.get(i);
                String previousDate = DateUtil.dateToString(stockTransactionDataDao.findPreviousDateByDate(false, currentDate));
                String beginDate = DateUtil.dateToString(DateUtil.stepMonth(DateUtil.stringToDate(currentDate), -backwardMonth));

                /*************************************** 卖股票/买股票 ***********************************************/
                // 更新robot2_account表
                robot2AccountDao.updateRobotAccountBeforeSellOrBuy(currentDate);

                // 卖股票
                robot2StockTransactRecordDao.sellOrBuy(currentDate, mandatoryStopLoss, mandatoryStopLossRate);

                // 更新robot2_account表
                robot2AccountDao.updateRobotAccountAfterSellOrBuy(currentDate);

                // 如果所有账号都已经满仓，则跳过这个交易日
                if (isFullStockInRobot2Account(holdStockNumber)) {
                    logger.info("所有账号都是满仓，因此日期【" + currentDate + "】不做任何交易，直接跳过");

                    // 更新robot2_account表的total_assets字段
                    robot2AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                    continue;
                }

                /********************************** 选择最近平均值出现金叉或死叉的算法 ***********************************/
                // 计算所有算法的近期的平均值
                List<BaseCurrentDataAndPreviousDateAverage> currentDateAndPreviousDateAverageDifDeaList = stockTransactionDataDao.getAverageDifDeaByCurrentDateAndPreviousDate(false, currentDate, previousDate);
                List<BaseCurrentDataAndPreviousDateAverage> currentDateAndPreviousDateAverageClosePriceMa5List = stockTransactionDataDao.getAverageClosePriceMa5ByCurrentDateAndPreviousDate(false, currentDate, previousDate);
                List<BaseCurrentDataAndPreviousDateAverage> currentDateAndPreviousDateAverageHaClosePriceHaOpenPriceList = stockTransactionDataDao.getAverageHaClosePriceHaOpenPriceByCurrentDateAndPreviousDate(false, currentDate, previousDate);
                List<BaseCurrentDataAndPreviousDateAverage> currentDateAndPreviousDateAverageKDList = stockTransactionDataDao.getAverageKDByCurrentDateAndPreviousDate(false, currentDate, previousDate);

                BaseCurrentDataAndPreviousDateAverage currentDateAndPreviousDateAverageDifDea = currentDateAndPreviousDateAverageDifDeaList.get(0);
                BaseCurrentDataAndPreviousDateAverage currentDateAndPreviousDateAverageClosePriceMa5 = currentDateAndPreviousDateAverageClosePriceMa5List.get(0);
                BaseCurrentDataAndPreviousDateAverage currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice = currentDateAndPreviousDateAverageHaClosePriceHaOpenPriceList.get(0);
                BaseCurrentDataAndPreviousDateAverage currentDateAndPreviousDateAverageKD = currentDateAndPreviousDateAverageKDList.get(0);

                // 使用单一算法
                if (useSingleOrUseAllMethod.equals(UseSingleOrUseAllMethod.USE_SINGLE_METHOD)) {
                    logger.info("使用单一算法");
                    // 判断使用哪种算法的平均值
                    FilterMethod.SELECT_FILTER_METHOD_VALUE = 0.0;
                    FilterMethod.SELECT_FILTER_METHOD = null;
                    List<SuccessRateOrPercentagePojo> successRateOrPercentagePojoList = null;
                    Double value = null;

                    // MACD金叉状态
                    if (null != currentDateAndPreviousDateAverageDifDea
                            && null != currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过金叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelMACDGoldCrossDao.getMACDGoldCrossSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * macdMultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD金叉算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_GOLD_CROSS;
                            }
                        }
                        // 通过金叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelMACDGoldCrossDao.getMACDGoldCrossPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD金叉算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_GOLD_CROSS;
                            }
                        }
                    }
                    // MACD死叉状态
                    if (null != currentDateAndPreviousDateAverageDifDea
                            && null != currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelMACDDeadCrossDao.getMACDDeadCrossSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * macdMultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD死叉算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_DEAD_CROSS;
                            }
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelMACDDeadCrossDao.getMACDDeadCrossPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD死叉算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_DEAD_CROSS;
                            }
                        }
                    }
                    // close_price金叉ma5状态
                    if (null != currentDateAndPreviousDateAverageClosePriceMa5
                            && null != currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过金叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * closePriceMa5MultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price金叉ma5算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA;
                            }
                        }
                        // 通过金叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price金叉ma5算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA;
                            }
                        }
                    }
                    // close_price死叉ma5状态
                    if (null != currentDateAndPreviousDateAverageClosePriceMa5
                            && null != currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * closePriceMa5MultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price死叉ma5算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA;
                            }
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price死叉ma5算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA;
                            }
                        }
                    }
                    // hei_kin_ashi上升趋势状态
                    if (null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice
                            && null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过上升趋势成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiUpDownDao.getHeiKinAshiUpDownSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * heiKinAshiMultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi上升趋势算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_UP_DOWN;
                            }
                        }
                        // 通过上升趋势百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi上升趋势算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_UP_DOWN;
                            }
                        }
                    }
                    // hei_kin_ashi下降趋势状态
                    if (null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice
                            && null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * heiKinAshiMultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi下降趋势算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_DOWN_UP;
                            }
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi下降趋势算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_DOWN_UP;
                            }
                        }
                    }
                    // kd金叉状态
                    if (null != currentDateAndPreviousDateAverageKD
                            && null != currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过金叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelKDGoldCrossDao.getKdGoldCrossSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * kdMultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd金叉算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_GOLD_CROSS;
                            }
                        }
                        // 通过金叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelKDGoldCrossDao.getKdGoldCrossPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd金叉算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_GOLD_CROSS;
                            }
                        }
                    }
                    // kd死叉状态
                    if (null != currentDateAndPreviousDateAverageKD
                            && null != currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelKDDeadCrossDao.getKdDeadCrossSuccessRate(false, beginDate, currentDate, (int) (averageDateNumber.intValue() * kdMultiplicationFactor.doubleValue()));
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd死叉算法的成功率【" + value + "】");
                            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_DEAD_CROSS;
                            }
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelKDDeadCrossDao.getKdDeadCrossPercentage(false, beginDate, currentDate);
                            value = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd死叉算法的百分比【" + value + "】");
                            if (-1 == value.compareTo(percentageTopThreshold)
                                    && 1 == value.compareTo(percentageBottomThreshold)
                                    && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)) {
                                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_DEAD_CROSS;
                            }
                        }
                    }

                    // 如果不满足金叉死叉状态、成功率和百分比的阈值，则不做任何交易
                    if (null == FilterMethod.SELECT_FILTER_METHOD) {
                        logger.info("在使用单一算法的情况下，日期【" + currentDate + "】不是金叉状态或死叉状态，不做任何交易");

                        // 更新robot2_account表的total_assets字段
                        robot2AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                        continue;
                    }
                    if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)
                            && -1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(successRateThreshold)) {
                        logger.info("在使用单一算法，通过成功率来判断的情况下，日期【" + currentDate + "】的成功率【" +
                                FilterMethod.SELECT_FILTER_METHOD_VALUE + "】小于阈值【" + successRateThreshold + "】，不做任何交易");

                        // 更新robot2_account表的total_assets字段
                        robot2AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                        continue;
                    }
                    if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)
                            && null == FilterMethod.SELECT_FILTER_METHOD) {
                        logger.info("在使用单一算法，通过百分比来判断的情况下，日期【" + currentDate + "】的百分比【" +
                                FilterMethod.SELECT_FILTER_METHOD_VALUE + "】不在阈值【" + percentageTopThreshold
                                + "】和【" + percentageBottomThreshold + "】之间，不做任何交易");

                        // 更新robot2_account表的total_assets字段
                        robot2AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                        continue;
                    }

                    // 确定仓位
//                    int shippingSpace = 0;
//                    if(shippingSpaceControl.equals(ShippingSpaceControl.YES)){
//                        // 需要乘以100/percentageTopThreshold，否则最高仓位只有8成
//                        shippingSpace = (int) (Math.floor(FilterMethod.SELECT_FILTER_METHOD_VALUE.doubleValue() * (100 / percentageTopThreshold) / 10) + 1);
//                        if (shippingSpace > 10){
//                            shippingSpace = 10;
//                        }
//                        logger.info("在有仓位控制的情况下，仓位为【" + shippingSpace + "】");
//                    } else {
//                        shippingSpace = 10;
//                        logger.info("在没有仓位控制的情况下，仓位为【" + shippingSpace + "】");
//                    }

                    /*************************************** 按照条件，过滤股票 ***********************************/
                    // 清空robot2_stock_filter表
                    robot2StockFilterDao.truncateTableRobotStockFilter();

                    // 金叉做多的算法
                    if (1 == FilterMethod.SELECT_FILTER_METHOD % 2) {
                        // 根据date，从stock_transaction_data表中，向robot2_stock_filter表中插入股票代码
                        robot2StockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(currentDate);

                        if (!useMinFilterCondition) {
                            // 过滤条件：当前收盘价与某段时间最高价的百分比
//                            robot2StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeHighestPrice);

                            // 过滤条件：MA不单调递减，过滤股票
//                            String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot2StockFilterDao.filterByMANotDecreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }
                        }
                    }
                    // 死叉做空的算法
                    if (0 == FilterMethod.SELECT_FILTER_METHOD % 2) {
                        // 根据date，从stock_transaction_data表中，向robot_stock_filter表中插入股票代码
                        robot2StockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(currentDate);

                        if (!useMinFilterCondition) {
                            // 过滤条件：当前收盘价与某段时间最低价的百分比
//                            robot2StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeLowestPrice);

                            // 过滤条件：MA不单调递增，过滤股票
//                            String[] maLevelArray = maNotIncreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotIncreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot2StockFilterDao.filterByMANotIncreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }
                        }
                    }

                    // 只保留这个价格区间以内的股票
                    robot2StockFilterDao.filterByLessThanClosePrice(currentDate, closePriceStart, closePriceEnd);

                    // 过滤条件：删除除过权的股票
                    robot2StockFilterDao.filterByXr(xrBeginDate, currentDate);

                    /*************************************** 执行过滤算法 ***********************************/
                    // 过滤条件：MACD金叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.MACD_GOLD_CROSS)) {
                        robot2StockFilterDao.filterByMACDGoldCross(currentDate);
                    }
                    // 过滤条件：收盘价金叉五日均线
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA)) {
                        robot2StockFilterDao.filterByClosePriceGoldCrossMA5(currentDate);
                    }
                    // 过滤条件：平均K线从下跌趋势变为上涨趋势
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.HEI_KIN_ASHI_UP_DOWN)) {
                        robot2StockFilterDao.filterByHeiKinAshiUpDown(currentDate);
                    }
                    // 过滤条件：KD金叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.KD_GOLD_CROSS)) {
                        robot2StockFilterDao.filterByKDGoldCross(currentDate);
                    }
                    // 过滤条件：MACD死叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.MACD_DEAD_CROSS)) {
                        robot2StockFilterDao.filterByMACDDeadCross(currentDate);
                    }
                    // 过滤条件：收盘价死叉五日均线
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA)) {
                        robot2StockFilterDao.filterByClosePriceDeadCrossMA5(currentDate);
                    }
                    // 过滤条件：平均K线从上涨趋势变为下跌趋势
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.HEI_KIN_ASHI_DOWN_UP)) {
                        robot2StockFilterDao.filterByHeiKinAshiDownUp(currentDate);
                    }
                    // 过滤条件：KD死叉
                    if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.KD_DEAD_CROSS)) {
                        robot2StockFilterDao.filterByKDDeadCross(currentDate);
                    }

                    // 滤条件：这次金叉或死叉之前的那次死叉或金叉的持续天数如果小于transactionNumber，则这条记录被过滤掉
//                    robot2StockFilterDao.filterByTransactionNumberBeforeGoldCrossDeadCross(currentDate, FilterMethod.SELECT_FILTER_METHOD, macdTransactionNumber, closePriceMa5TransactionNumber, heiKinAshiTransactionNumber, kdTransactionNumber);

                    /*************************************** 买股票/卖股票 ******************************************/
                    if (null != FilterMethod.SELECT_FILTER_METHOD) {
                        // 买股票/卖股票
                        // 确定交易方向（做多/做空）
                        Integer direction;
                        if (0 == FilterMethod.SELECT_FILTER_METHOD % 2) {
                            direction = -1;
                        } else {
                            direction = 1;
                        }
                        robot2StockTransactRecordDao.buyOrSell(currentDate, backwardMonth, averageDateNumber, FilterMethod.SELECT_FILTER_METHOD, FilterMethod.SELECT_FILTER_METHOD_VALUE, direction, shippingSpaceControl, holdStockNumber);

                        // 更新robot2_account表的total_assets字段
                        robot2AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);
                    }

                    // 获取买卖建议
                    String printText = null;
                    if (printFormat.equals(RobotPrintBuySellSuggestionFormat.JSON)) {
                        // json方式
                        printText = super.printBuySellSuggestionByJson_averageGoldCrossDeadCross(currentDate);
                    }
                    if (printFormat.equals(RobotPrintBuySellSuggestionFormat.SIMPLE)) {
                        // 简单文本格式
                        printText = super.printBuySellSuggestionBySimple_averageGoldCrossDeadCross(currentDate);
                    }
                    logger.info("买卖建议：\n" + printText);
                }

                // 使用所有算法
                if (useSingleOrUseAllMethod.equals(UseSingleOrUseAllMethod.USE_ALL_METHOD)) {
                    logger.info("使用所有算法");

                    // 判断使用哪种算法的平均值
                    FilterMethod.SELECT_FILTER_METHOD_VALUE = 0.0;
                    FilterMethod.SELECT_FILTER_METHOD = null;
                    List<SuccessRateOrPercentagePojo> successRateOrPercentagePojoList = null;
                    Double difDeaGoldCrossSuccessRate = 0.0;
                    Double difDeaDeadCrossSuccessRate = 0.0;
                    Double closePriceMa5GoldCrossSuccessRate = 0.0;
                    Double closePriceMa5DeadCrossSuccessRate = 0.0;
                    Double haClosePriceHaOpenPriceUpDonwSuccessRate = 0.0;
                    Double haClosePriceHaOpenPriceDownUpSuccessRate = 0.0;
                    Double kdGoldCrossSuccessRate = 0.0;
                    Double kdDeadCrossSuccessRate = 0.0;
                    Double difDeaGoldCrossPercentage = 0.0;
                    Double difDeaDeadCrossPercentage = 0.0;
                    Double closePriceMa5GoldCrossPercentage = 0.0;
                    Double closePriceMa5DeadCrossPercentage = 0.0;
                    Double haClosePriceHaOpenPriceUpDownPercentage = 0.0;
                    Double haClosePriceHaOpenPriceDownUpPercentage = 0.0;
                    Double kdGoldCrossPercentage = 0.0;
                    Double kdDeadCrossPercentage = 0.0;
                    Double total = 0.0;

                    // MACD出现金叉
                    if (null != currentDateAndPreviousDateAverageDifDea
                            && null != currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过金叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelMACDGoldCrossDao.getMACDGoldCrossSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            difDeaGoldCrossSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD金叉算法的成功率【" + difDeaGoldCrossSuccessRate + "】");
                            currentDateAndPreviousDateAverageDifDea.setSuccessRateOrPercentage(difDeaGoldCrossSuccessRate);
                        }
                        // 通过金叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelMACDGoldCrossDao.getMACDGoldCrossPercentage(false, beginDate, currentDate);
                            difDeaGoldCrossPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD金叉算法的百分比【" + difDeaGoldCrossPercentage + "】");
                            currentDateAndPreviousDateAverageDifDea.setSuccessRateOrPercentage(difDeaGoldCrossPercentage);
                        }
                    }
                    // MACD出现死叉
                    if (null != currentDateAndPreviousDateAverageDifDea
                            && null != currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageDifDea.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelMACDDeadCrossDao.getMACDDeadCrossSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            difDeaDeadCrossSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD死叉算法的成功率【" + difDeaDeadCrossSuccessRate + "】");
                            currentDateAndPreviousDateAverageDifDea.setSuccessRateOrPercentage(difDeaGoldCrossSuccessRate);
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelMACDDeadCrossDao.getMACDDeadCrossPercentage(false, beginDate, currentDate);
                            difDeaDeadCrossPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("MACD死叉算法的百分比【" + difDeaDeadCrossPercentage + "】");
                            currentDateAndPreviousDateAverageDifDea.setSuccessRateOrPercentage(difDeaGoldCrossPercentage);
                        }
                    }
                    // close_price金叉ma5出现
                    if (null != currentDateAndPreviousDateAverageClosePriceMa5
                            && null != currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过金叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            closePriceMa5GoldCrossSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price金叉ma5算法的成功率【" + closePriceMa5GoldCrossSuccessRate + "】");
                            currentDateAndPreviousDateAverageClosePriceMa5.setSuccessRateOrPercentage(closePriceMa5GoldCrossSuccessRate);
                        }
                        // 通过金叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossPercentage(false, beginDate, currentDate);
                            closePriceMa5GoldCrossPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price金叉ma5算法的百分比【" + closePriceMa5GoldCrossPercentage + "】");
                            currentDateAndPreviousDateAverageClosePriceMa5.setSuccessRateOrPercentage(closePriceMa5GoldCrossPercentage);
                        }
                    }
                    // close_price死叉ma5出现
                    if (null != currentDateAndPreviousDateAverageClosePriceMa5
                            && null != currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageClosePriceMa5.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            closePriceMa5DeadCrossSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price死叉ma5算法的成功率【" + closePriceMa5DeadCrossSuccessRate + "】");
                            currentDateAndPreviousDateAverageClosePriceMa5.setSuccessRateOrPercentage(closePriceMa5GoldCrossSuccessRate);
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossPercentage(false, beginDate, currentDate);
                            closePriceMa5DeadCrossPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("close_price死叉ma5算法的百分比【" + closePriceMa5DeadCrossPercentage + "】");
                            currentDateAndPreviousDateAverageClosePriceMa5.setSuccessRateOrPercentage(closePriceMa5GoldCrossPercentage);
                        }
                    }
                    // hei_kin_ashi上升趋势出现
                    if (null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice
                            && null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过上升趋势成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiUpDownDao.getHeiKinAshiUpDownSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            haClosePriceHaOpenPriceUpDonwSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi上升趋势算法的成功率【" + haClosePriceHaOpenPriceUpDonwSuccessRate + "】");
                            currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.setSuccessRateOrPercentage(haClosePriceHaOpenPriceUpDonwSuccessRate);
                        }
                        // 通过上升趋势百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpPercentage(false, beginDate, currentDate);
                            haClosePriceHaOpenPriceUpDownPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi上升趋势算法的百分比【" + haClosePriceHaOpenPriceUpDownPercentage + "】");
                            currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.setSuccessRateOrPercentage(haClosePriceHaOpenPriceUpDownPercentage);
                        }
                    }
                    // hei_kin_ashi下降趋势出现
                    if (null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice
                            && null != currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            haClosePriceHaOpenPriceDownUpSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi下降趋势算法的成功率【" + haClosePriceHaOpenPriceDownUpSuccessRate + "】");
                            currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.setSuccessRateOrPercentage(haClosePriceHaOpenPriceUpDonwSuccessRate);
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpPercentage(false, beginDate, currentDate);
                            haClosePriceHaOpenPriceDownUpPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("hei_kin_ashi下降趋势算法的百分比【" + haClosePriceHaOpenPriceDownUpPercentage + "】");
                            currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.setSuccessRateOrPercentage(haClosePriceHaOpenPriceUpDownPercentage);
                        }
                    }
                    // kd金叉出现
                    if (null != currentDateAndPreviousDateAverageKD
                            && null != currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross().equals(CrossStatus.GOLD_CROSS)) {
                        // 通过金叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelKDGoldCrossDao.getKdGoldCrossSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            kdGoldCrossSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd金叉算法的成功率【" + kdGoldCrossSuccessRate + "】");
                            currentDateAndPreviousDateAverageKD.setSuccessRateOrPercentage(kdGoldCrossSuccessRate);
                        }
                        // 通过金叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelKDGoldCrossDao.getKdGoldCrossPercentage(false, beginDate, currentDate);
                            kdGoldCrossPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd金叉算法的百分比【" + kdGoldCrossPercentage + "】");
                            currentDateAndPreviousDateAverageKD.setSuccessRateOrPercentage(kdGoldCrossPercentage);
                        }
                    }
                    // kd死叉出现
                    if (null != currentDateAndPreviousDateAverageKD
                            && null != currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross()
                            && currentDateAndPreviousDateAverageKD.getGoldCrossOrDeadCross().equals(CrossStatus.DEAD_CROSS)) {
                        // 通过死叉成功率来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE)) {
                            successRateOrPercentagePojoList = modelKDDeadCrossDao.getKdDeadCrossSuccessRate(false, beginDate, currentDate, averageDateNumber);
                            kdDeadCrossSuccessRate = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd死叉算法的成功率【" + kdDeadCrossSuccessRate + "】");
                            currentDateAndPreviousDateAverageKD.setSuccessRateOrPercentage(kdGoldCrossSuccessRate);
                        }
                        // 通过死叉百分比来判断
                        if (judgeMethod.equals(JudgeMethod.GOLD_CROSS_DEAD_CROSS_PERCENTAGE)) {
                            successRateOrPercentagePojoList = modelKDDeadCrossDao.getKdDeadCrossPercentage(false, beginDate, currentDate);
                            kdDeadCrossPercentage = Double.valueOf((successRateOrPercentagePojoList.get(successRateOrPercentagePojoList.size() - 1)).getSuccessRateOrPercentage().toString());
                            logger.info("kd死叉算法的百分比【" + kdDeadCrossPercentage + "】");
                            currentDateAndPreviousDateAverageKD.setSuccessRateOrPercentage(kdGoldCrossPercentage);
                        }
                    }

                    // 确定仓位
                    SortedSet<BaseCurrentDataAndPreviousDateAverage> sortedSet = new TreeSet<BaseCurrentDataAndPreviousDateAverage>();
                    sortedSet.add(currentDateAndPreviousDateAverageDifDea);
                    sortedSet.add(currentDateAndPreviousDateAverageClosePriceMa5);
                    sortedSet.add(currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice);
                    sortedSet.add(currentDateAndPreviousDateAverageKD);
                    int shippingSpace = 0;
                    if (shippingSpaceControl.equals(ShippingSpaceControl.NO)) {
                        shippingSpace = 10;
                        logger.info("在没有仓位控制的情况下，仓位为【" + shippingSpace + "】");
                    }
                    if (shippingSpaceControl.equals(ShippingSpaceControl.YES_SUCCESS_RATE)) {
                        Double successRateOrPercentage = sortedSet.first().getSuccessRateOrPercentage();
                        shippingSpace = (int) (Double.valueOf(Math.ceil(successRateOrPercentage / 10) > 10 ? 10.0 : Math.ceil(successRateOrPercentage / 10)).intValue());
                        if (shippingSpace > 10) {
                            shippingSpace = 10;
                        }
                        logger.info("采用成功率算法，在有仓位控制的情况下，仓位为【" + shippingSpace + "】");
                    }
                    if (shippingSpaceControl.equals(ShippingSpaceControl.YES_PERCENTAGE)) {
                        Double successRateOrPercentage = sortedSet.first().getSuccessRateOrPercentage();
                        shippingSpace = (int) (Math.floor(successRateOrPercentage * (100 / percentageTopThreshold) / 10) + 1);
                        if (shippingSpace > 10) {
                            shippingSpace = 10;
                        }
                        logger.info("采用百分比算法，在有仓位控制的情况下，仓位为【" + shippingSpace + "】");
                    }
                    // 分配仓位
                    Iterator<BaseCurrentDataAndPreviousDateAverage> iterator = null;
                    Map<Integer, BaseCurrentDataAndPreviousDateAverage> mapWithShippingSpace = new HashMap();
                    for (int x = 1, y = 1; x <= shippingSpace; x++, y++) {
                        if ((x - 1) % sortedSet.size() == 0) {
                            iterator = sortedSet.iterator();
                            y = 1;
                        }
                        BaseCurrentDataAndPreviousDateAverage baseCurrentDataAndPreviousDateAverage = iterator.next();
                        if (null == baseCurrentDataAndPreviousDateAverage.getShippingSpace()) {
                            baseCurrentDataAndPreviousDateAverage.setShippingSpace(1);
                        } else {
                            baseCurrentDataAndPreviousDateAverage.setShippingSpace(baseCurrentDataAndPreviousDateAverage.getShippingSpace() + 1);
                        }
                        mapWithShippingSpace.put(y, baseCurrentDataAndPreviousDateAverage);
                    }

                    /*************************************** 按照条件，过滤股票 ***********************************/
                    Set<Integer> keySet = mapWithShippingSpace.keySet();
                    Iterator<Integer> keyIterator = keySet.iterator();
                    while (keyIterator.hasNext()) {
                        // 如果所有账号都已经满仓，则不再做任何交易
                        if (isFullStockInRobot2Account(holdStockNumber)) {
                            logger.info("此时所有账号都是满仓，不再做任何交易");
                            break;
                        }

                        Integer key = keyIterator.next();
                        BaseCurrentDataAndPreviousDateAverage baseCurrentDataAndPreviousDateAverage = mapWithShippingSpace.get(key);
                        FilterMethod.SELECT_FILTER_METHOD = baseCurrentDataAndPreviousDateAverage.getFilterMethod();
                        FilterMethod.SELECT_FILTER_METHOD_VALUE = baseCurrentDataAndPreviousDateAverage.getSuccessRateOrPercentage();
                        logger.info("算法【" + baseCurrentDataAndPreviousDateAverage.getFilterMethod() + "】的仓位为【" +
                                baseCurrentDataAndPreviousDateAverage.getShippingSpace() + "】");

                        // 清空robot2_stock_filter表
                        robot2StockFilterDao.truncateTableRobotStockFilter();

                        // 金叉做多的算法
                        if (1 == baseCurrentDataAndPreviousDateAverage.getFilterMethod() % 2) {
                            // 根据date，从stock_transaction_data表中，向robot2_stock_filter表中插入股票代码
                            robot2StockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(currentDate);

                            // 过滤条件：当前收盘价与某段时间最高价的百分比
                            robot2StockFilterDao
                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(
                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
                                            currentClosePriceCompareToSomeTimeHighestPrice);

                            // 过滤条件：MA不单调递减，过滤股票
                            String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
                            String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
                                String maLevel = maLevelArray[j].trim();
                                String dateNumber = dateNumberArray[j].trim();
                                robot2StockFilterDao.filterByMANotDecreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
                            }
                        }
                        // 死叉做空的算法
                        if (0 == baseCurrentDataAndPreviousDateAverage.getFilterMethod() % 2) {
                            // 根据date，从stock_transaction_data表中，向robot2_stock_filter表中插入股票代码
                            robot2StockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(currentDate);

                            // 过滤条件：当前收盘价与某段时间最低价的百分比
                            robot2StockFilterDao
                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(
                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
                                            currentClosePriceCompareToSomeTimeLowestPrice);

                            // 过滤条件：MA不单调递增，过滤股票
                            String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
                            String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
                                String maLevel = maLevelArray[j].trim();
                                String dateNumber = dateNumberArray[j].trim();
                                robot2StockFilterDao.filterByMANotIncreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
                            }
                        }
                        // 只保留这个价格区间以内的股票
                        robot2StockFilterDao.filterByLessThanClosePrice(currentDate, closePriceStart, closePriceEnd);

                        // 过滤条件：删除除过权的股票
                        robot2StockFilterDao.filterByXr(xrBeginDate, currentDate);

                        /*************************************** 执行过滤算法 ***********************************/
                        // 过滤条件：MACD金叉
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.MACD_GOLD_CROSS)) {
                            robot2StockFilterDao.filterByMACDGoldCross(currentDate);
                        }
                        // 过滤条件：收盘价金叉五日均线
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA)) {
                            robot2StockFilterDao.filterByClosePriceGoldCrossMA5(currentDate);
                        }
                        // 过滤条件：平均K线从下跌趋势变为上涨趋势
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.HEI_KIN_ASHI_UP_DOWN)) {
                            robot2StockFilterDao.filterByHeiKinAshiUpDown(currentDate);
                        }
                        // 过滤条件：KD金叉
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.KD_GOLD_CROSS)) {
                            robot2StockFilterDao.filterByKDGoldCross(currentDate);
                        }
                        // 过滤条件：MACD死叉
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.MACD_DEAD_CROSS)) {
                            robot2StockFilterDao.filterByMACDDeadCross(currentDate);
                        }
                        // 过滤条件：收盘价死叉五日均线
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA)) {
                            robot2StockFilterDao.filterByClosePriceDeadCrossMA5(currentDate);
                        }
                        // 过滤条件：平均K线从上涨趋势变为下跌趋势
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.HEI_KIN_ASHI_DOWN_UP)) {
                            robot2StockFilterDao.filterByHeiKinAshiDownUp(currentDate);
                        }
                        // 过滤条件：KD死叉
                        if (baseCurrentDataAndPreviousDateAverage.getFilterMethod().equals(FilterMethod.KD_DEAD_CROSS)) {
                            robot2StockFilterDao.filterByKDDeadCross(currentDate);
                        }

                        /*************************************** 买股票/卖股票 ******************************************/
                        // 买股票/卖股票
                        // 确定交易方向（做多/做空）
                        Integer direction;
                        if (0 == baseCurrentDataAndPreviousDateAverage.getFilterMethod() % 2) {
                            direction = -1;
                        } else {
                            direction = 1;
                        }
                        robot2StockTransactRecordDao.buyOrSell(currentDate, backwardMonth, averageDateNumber, FilterMethod.SELECT_FILTER_METHOD, FilterMethod.SELECT_FILTER_METHOD_VALUE, direction, shippingSpaceControl, percentageTopThreshold, baseCurrentDataAndPreviousDateAverage.getShippingSpace(), holdStockNumber);

                        // 更新robot2_account表的total_assets字段
                        robot2AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);
                    }

                    // 获取买卖建议
                    String printText = null;
                    if (printFormat.equals(RobotPrintBuySellSuggestionFormat.JSON)) {
                        // json方式
                        printText = super.printBuySellSuggestionByJson_averageGoldCrossDeadCross(currentDate);
                    }
                    if (printFormat.equals(RobotPrintBuySellSuggestionFormat.SIMPLE)) {
                        // 简单文本格式
                        printText = super.printBuySellSuggestionBySimple_averageGoldCrossDeadCross(currentDate);
                    }
                    logger.info("买卖建议：\n" + printText);
                }
                logger.info("-----------------------------");
            }
        }

        logger.info("执行某段时间内的股票买卖交易完成（平均金叉死叉百分比）");
    }

    /**
     * 卖股票/买股票
     */
    @Override
    public void sellOrBuy() {
        logger.info("卖股票/买股票");

        String sellDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.sell.stock.date");
        Integer mandatoryStopLoss = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.sell.stock.mandatoryStopLoss"));
        Double mandatoryStopLossRate = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.sell.stock.mandatoryStopLossRate"));

        robotStockTransactionRecordDao.sellOrBuy(sellDate, mandatoryStopLoss, mandatoryStopLossRate);
    }

    /**
     * 买股票/卖股票
     */
    @Override
    public void buyOrSell() {
        logger.info("买股票/卖股票");

        String buyDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.date");
        Integer backwardMonth = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.success.rate.backward.month"));
        Integer averageDateNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.success.rate.average.date.number"));
//        Integer successRateType = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.stock.success.rate.type"));
        Integer successRateType = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.shippingSpaceControl"));
        Integer shippingSpaceControl = FilterMethod.SELECT_FILTER_METHOD;
        Double successRate = FilterMethod.SELECT_FILTER_METHOD_VALUE;
        Integer holdStockNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.holdStockNumber"));

        // 确定交易方向（做多/做空）
        Integer direction;
        if (0 == FilterMethod.SELECT_FILTER_METHOD % 2) {
            direction = -1;
        } else {
            direction = 1;
        }
        // 买股票
        robotStockTransactionRecordDao.buyOrSell(buyDate, backwardMonth, averageDateNumber, successRateType,
                successRate, direction, shippingSpaceControl, holdStockNumber);
    }

    /**
     * 打印买卖建议
     */
    @Override
    public void printBuySellSuggestion() {
        logger.info("打印买卖建议");

        String printDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.print.day");
        Integer printFormat = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.print.format"));

        // 获取买卖建议
        String printText = null;
        if (printFormat.equals(RobotPrintBuySellSuggestionFormat.JSON)) {
            // json方式
            printText = super.printBuySellSuggestionByJson_successRate(printDate);
        }
        if (printFormat.equals(RobotPrintBuySellSuggestionFormat.SIMPLE)) {
            // 简单文本格式
            printText = super.printBuySellSuggestionBySimple_successRate(printDate);
        }
        System.out.println("买卖建议：\n" + printText);

        // 打印买卖建议
        File file;
        try {
            file = new File(PRINT_ROBOT_BUY_SELL_SUGGESTION);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(file);
            writer.write(printText);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
