package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 生成创建MACD图
 */
public class CreateMACDPictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateMACDPictureThread.class);

    private StockTransactionDataService stockTransactionDataService;

    public CreateMACDPictureThread() {
    }

    public CreateMACDPictureThread(
        StockTransactionDataService stockTransactionDataService) {
        this.stockTransactionDataService = stockTransactionDataService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始生成创建MACD图，调用的方法为【createMACDPicture】");

        stockTransactionDataService.createMACDPicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
