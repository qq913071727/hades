package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 月线级别交易记录
 * @author Administrator
 */
@Entity
@Table(name="STOCK_MONTH")
public class StockMonth implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
//    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private Integer id;

    /**
     * 股票代码
     */
    @Column(name="CODE_")
    private String code;

    /**
     * 表示某一个月的开始时间
     */
    @Column(name="BEGIN_DATE")
    private Date beginDate;

    /**
     * 表示某一个月的结束时间
     */
    @Column(name="END_DATE")
    private Date endDate;

    /**
     * 月线级别开盘价
     */
    @Column(name="OPEN_PRICE")
    private BigDecimal openPrice;

    /**
     * 月线级别收盘价
     */
    @Column(name="CLOSE_PRICE")
    private BigDecimal closePrice;

    /**
     * 月线级别最高价
     */
    @Column(name="HIGHEST_PRICE")
    private BigDecimal highestPrice;

    /**
     * 月线级别最低价
     */
    @Column(name="LOWEST_PRICE")
    private BigDecimal lowestPrice;

    /**
     * 月线级别成交量
     */
    @Column(name="VOLUME")
    private BigDecimal volume;

    /**
     * 月线级别成交额
     */
    @Column(name="TURNOVER")
    private BigDecimal turnover;

    /**
     * 5日均线
     */
    @Column(name="MA5")
    private BigDecimal ma5;

    /**
     * 10日均线
     */
    @Column(name="MA10")
    private BigDecimal ma10;

    /**
     * 20日均线
     */
    @Column(name="MA20")
    private BigDecimal ma20;

    /**
     * 60日均线
     */
    @Column(name="MA60")
    private BigDecimal ma60;

    /**
     * 120日均线
     */
    @Column(name="MA120")
    private BigDecimal ma120;

    /**
     * 250日均线
     */
    @Column(name="MA250")
    private BigDecimal ma250;

    /**
     * 计算KD时的指标
     */
    @Column(name="RSV")
    private BigDecimal rsv;

    /**
     * 计算KD时的指标
     */
    @Column(name="K")
    private BigDecimal k;

    /**
     * 计算KD时的指标
     */
    @Column(name="D")
    private BigDecimal d;

    /**
     * 计算MACD时的指标
     */
    @Column(name="EMA12")
    private BigDecimal ema12;

    /**
     * 计算MACD时的指标
     */
    @Column(name="EMA26")
    private BigDecimal ema26;

    /**
     * 计算MACD时的指标
     */
    @Column(name="DIF")
    private BigDecimal dif;

    /**
     * 计算MACD时的指标
     */
    @Column(name="DEA")
    private BigDecimal dea;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    public BigDecimal getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    public BigDecimal getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(BigDecimal highestPrice) {
        this.highestPrice = highestPrice;
    }

    public BigDecimal getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(BigDecimal lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getTurnover() {
        return turnover;
    }

    public void setTurnover(BigDecimal turnover) {
        this.turnover = turnover;
    }

    public BigDecimal getMa5() {
        return ma5;
    }

    public void setMa5(BigDecimal ma5) {
        this.ma5 = ma5;
    }

    public BigDecimal getMa10() {
        return ma10;
    }

    public void setMa10(BigDecimal ma10) {
        this.ma10 = ma10;
    }

    public BigDecimal getMa20() {
        return ma20;
    }

    public void setMa20(BigDecimal ma20) {
        this.ma20 = ma20;
    }

    public BigDecimal getMa60() {
        return ma60;
    }

    public void setMa60(BigDecimal ma60) {
        this.ma60 = ma60;
    }

    public BigDecimal getMa120() {
        return ma120;
    }

    public void setMa120(BigDecimal ma120) {
        this.ma120 = ma120;
    }

    public BigDecimal getMa250() {
        return ma250;
    }

    public void setMa250(BigDecimal ma250) {
        this.ma250 = ma250;
    }

    public BigDecimal getRsv() {
        return rsv;
    }

    public void setRsv(BigDecimal rsv) {
        this.rsv = rsv;
    }

    public BigDecimal getK() {
        return k;
    }

    public void setK(BigDecimal k) {
        this.k = k;
    }

    public BigDecimal getD() {
        return d;
    }

    public void setD(BigDecimal d) {
        this.d = d;
    }

    public BigDecimal getEma12() {
        return ema12;
    }

    public void setEma12(BigDecimal ema12) {
        this.ema12 = ema12;
    }

    public BigDecimal getEma26() {
        return ema26;
    }

    public void setEma26(BigDecimal ema26) {
        this.ema26 = ema26;
    }

    public BigDecimal getDif() {
        return dif;
    }

    public void setDif(BigDecimal dif) {
        this.dif = dif;
    }

    public BigDecimal getDea() {
        return dea;
    }

    public void setDea(BigDecimal dea) {
        this.dea = dea;
    }
}
