package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.EtfInfo;

import java.util.List;

public interface EtfInfoDao extends BaseDao {
	
	/**
	 * 通过ETF代码code_，查找EtfInfo对象
	 * @param code
	 * @return
	 */
	EtfInfo getEtfInfoObjectByCode(String code);
	
	/**
	 * 获得所有EtfInfo对象
	 * @return
	 */
	List<EtfInfo> getAllEtfInfo();
	
	/**
	 * 保存EtfInfo对象
	 * @param etfInfo
	 */
	void saveEtfInfo(EtfInfo etfInfo);
}
