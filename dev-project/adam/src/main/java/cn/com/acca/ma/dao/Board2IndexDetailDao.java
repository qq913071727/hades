package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Board2Index;
import cn.com.acca.ma.model.Board2IndexDetail;

import java.util.List;

public interface Board2IndexDetailDao {

    /**
     * 保存Board2IndexDetail对象
     * @param board2IndexDetail
     */
    void saveBoard2IndexDetail(Board2IndexDetail board2IndexDetail);

    /**
     * 保存Board2IndexDetail对象列表
     * @param board2IndexDetailList
     */
    void saveBoard2IndexDetailList(List<Board2IndexDetail> board2IndexDetailList);
}
