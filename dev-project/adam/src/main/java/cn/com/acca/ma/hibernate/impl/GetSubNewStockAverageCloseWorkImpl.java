package cn.com.acca.ma.hibernate.impl;

import cn.com.acca.ma.service.impl.WechatSubscriptionServiceImpl;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.jdbc.Work;

public class GetSubNewStockAverageCloseWorkImpl implements Work {

    private static Logger logger = LogManager.getLogger(GetSubNewStockAverageCloseWorkImpl.class);

    private String beginDate;
    private String endDate;
    private List<HashMap> list = new ArrayList<HashMap>();

    public GetSubNewStockAverageCloseWorkImpl() {
    }

    public GetSubNewStockAverageCloseWorkImpl(String beginDate, String endDate, List<HashMap> list) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.list = list;
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        DatabaseMetaData databaseMetaData=connection.getMetaData();
        Connection metaDataConnection=null;
        if(null!=databaseMetaData){
            metaDataConnection=databaseMetaData.getConnection();
        }
        OracleCallableStatement ocs=(OracleCallableStatement) metaDataConnection.prepareCall("{call PKG_MODEL_RECORD.FIND_SUB_NEW_STOCK_AVG_CLOSE(?,?,?)}");
        ocs.setString(1, beginDate);
        ocs.setString(2, endDate);
        ocs.registerOutParameter(3, OracleTypes.ARRAY,"T_SUBNEWSTOCKAVGCLOSE_ARRAY");
        ocs.execute();
        ARRAY array=ocs.getARRAY(3);
        Datum[] datas=(Datum[]) array.getOracleArray();
        if(null!=datas){
            for (int i = 0; i < datas.length; i++){
                if(datas[i]!=null&&((STRUCT) datas[i])!=null){

                    Datum[] boardResultAttributes = ((STRUCT) datas[i]).getOracleAttributes();
                    HashMap hashMap=new HashMap();
                    hashMap.put("subNewStockDate", boardResultAttributes[0].dateValue());
                    hashMap.put("subNewStockAvgCLose", boardResultAttributes[1].bigDecimalValue());
                    list.add(hashMap);
                }else{
                    logger.info("datas["+i+"] is null.");
                }
            }
        }
    }
}
