package cn.com.acca.ma.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 周线级别股票指数
 * @author Administrator
 */
@Entity
@Table(name = "STOCK_INDEX_WEEK")
public class StockIndexWeek {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
    private Integer id;

    /**
     * 指数代码
     */
    @Column(name="CODE_")
    private String code;

    /**
     * 本周交易的开始日期
     */
    @Column(name="BEGIN_DATE")
    private Date beginDate;

    /**
     * 本周交易的结束日期
     */
    @Column(name="END_DATE")
    private Date endDate;

    /**
     * 开盘价
     */
    @Column(name="OPEN_PRICE")
    private BigDecimal openPrice;

    /**
     * 收盘价
     */
    @Column(name="HIGHEST_PRICE")
    private BigDecimal highestPrice;

    /**
     * 最高价
     */
    @Column(name="CLOSE_PRICE")
    private BigDecimal closePrice;

    /**
     * 最低价
     */
    @Column(name="LOWEST_PRICE")
    private BigDecimal lowestPrice;

    /**
     * 涨跌额
     */
    @Column(name="CHANGE_AMOUNT")
    private BigDecimal changeAmount;

    /**
     * 涨跌幅。单位：%
     */
    @Column(name="CHANGE_RANGE")
    private BigDecimal changeRange;

    /**
     * 成交量
     */
    @Column(name="VOLUME")
    private BigDecimal volume;

    /**
     * 成交额
     */
    @Column(name="TURNOVER")
    private BigDecimal turnover;

    /**
     * HeiKinAshi本周开盘价
     */
    @Column(name="HA_INDEX_WEEK_OPEN_PRICE")
    private BigDecimal HAIndexWeekOpenPrice;

    /**
     * HeiKinAshi本周收盘价
     */
    @Column(name="HA_INDEX_WEEK_CLOSE_PRICE")
    private BigDecimal HAIndexWeekClosePrice;

    /**
     * HeiKinAshi本周最高价
     */
    @Column(name="HA_INDEX_WEEK_HIGHEST_PRICE")
    private BigDecimal HAIndexWeekHighestPrice;

    /**
     * HeiKinAshi本周最低价
     */
    @Column(name="HA_INDEX_WEEK_LOWEST_PRICE")
    private BigDecimal HAIndexWeekLowestPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(BigDecimal openPrice) {
        this.openPrice = openPrice;
    }

    public BigDecimal getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(BigDecimal highestPrice) {
        this.highestPrice = highestPrice;
    }

    public BigDecimal getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    public BigDecimal getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(BigDecimal lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public BigDecimal getChangeAmount() {
        return changeAmount;
    }

    public void setChangeAmount(BigDecimal changeAmount) {
        this.changeAmount = changeAmount;
    }

    public BigDecimal getChangeRange() {
        return changeRange;
    }

    public void setChangeRange(BigDecimal changeRange) {
        this.changeRange = changeRange;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getTurnover() {
        return turnover;
    }

    public void setTurnover(BigDecimal turnover) {
        this.turnover = turnover;
    }

    public BigDecimal getHAIndexWeekOpenPrice() {
        return HAIndexWeekOpenPrice;
    }

    public void setHAIndexWeekOpenPrice(BigDecimal HAIndexWeekOpenPrice) {
        this.HAIndexWeekOpenPrice = HAIndexWeekOpenPrice;
    }

    public BigDecimal getHAIndexWeekClosePrice() {
        return HAIndexWeekClosePrice;
    }

    public void setHAIndexWeekClosePrice(BigDecimal HAIndexWeekClosePrice) {
        this.HAIndexWeekClosePrice = HAIndexWeekClosePrice;
    }

    public BigDecimal getHAIndexWeekHighestPrice() {
        return HAIndexWeekHighestPrice;
    }

    public void setHAIndexWeekHighestPrice(BigDecimal HAIndexWeekHighestPrice) {
        this.HAIndexWeekHighestPrice = HAIndexWeekHighestPrice;
    }

    public BigDecimal getHAIndexWeekLowestPrice() {
        return HAIndexWeekLowestPrice;
    }

    public void setHAIndexWeekLowestPrice(BigDecimal HAIndexWeekLowestPrice) {
        this.HAIndexWeekLowestPrice = HAIndexWeekLowestPrice;
    }
}
