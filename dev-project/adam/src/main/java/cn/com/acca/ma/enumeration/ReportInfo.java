package cn.com.acca.ma.enumeration;

public enum ReportInfo {
	ADAM_REPORT("adam_report.pdf","Adam Report");
	
	private String reportName;
	private String reportTitle;
	
	ReportInfo(String reportName, String reportTitle) {
		this.reportName = reportName;
		this.reportTitle = reportTitle;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
}
