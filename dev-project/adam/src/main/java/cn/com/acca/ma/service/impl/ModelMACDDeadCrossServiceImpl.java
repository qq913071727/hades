package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelMACDDeadCross;
import cn.com.acca.ma.service.ModelMACDDeadCrossService;

import java.util.List;

public class ModelMACDDeadCrossServiceImpl extends BaseServiceImpl<ModelMACDDeadCrossServiceImpl, ModelMACDDeadCross> implements ModelMACDDeadCrossService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD死叉模型算法，根据endDate日期，向表MDL_MACD_DEAD_CROSS插入数据
     * @param multithreading
     */
    @Override
    public void writeModelMACDDeadCrossIncr(boolean multithreading) {
        String macdDeadCrossEndDate = PropertiesUtil.getValue(MODEL_MACD_DEAD_CROSS_PROPERTIES, "macd.dead.cross.end_date");
        modelMACDDeadCrossDao.writeModelMACDDeadCrossIncr(multithreading, macdDeadCrossEndDate);
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD死叉模型算法，向表MDL_MACD_DEAD_CROSS插入数据
     */
    @Override
    public void writeModelMACDDeadCross() {
        modelMACDDeadCrossDao.writeModelMACDDeadCross();
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
