package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 机器人--股票交易记录
 */
@Data
public class RobotStockTransactionRecord {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 机器人名字
     */
    private String robotName;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 买入日期
     */
    private Date buyDate;

    /**
     * 买入价格
     */
    private BigDecimal buyPrice;

    /**
     * 买入数量
     */
    private Integer buyAmount;

    /**
     * 卖出日期
     */
    private Date sellDate;

    /**
     * 卖出价格
     */
    private BigDecimal sellPrice;

    /**
     * 卖出数量
     */
    private Integer sellAmount;

    /**
     * 过滤类型/交易策略。1表示MACD金叉；2表示MACD死叉；3表示close_price金叉MA5；4表示close_price死叉MA5；5表示hei_kin_ashi从下跌趋势转为上涨趋势；6表示hei_kin_ashi从上涨趋势转为下跌趋势；7表示KD金叉；8表示KD死叉
     */
    private Integer filterType;

    /**
     * 交易方向。1表示做多；-1表示做空
     */
    private Integer direction;

    /**
     * 盈亏额
     */
    private BigDecimal profitAndLoss;

    /**
     * 盈亏率
     */
    private BigDecimal profitAndLossRate;

}
