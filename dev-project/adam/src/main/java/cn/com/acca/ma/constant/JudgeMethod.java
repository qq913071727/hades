package cn.com.acca.ma.constant;

/**
 * 在使用单一算法的条件下，出现了多个金叉或死叉的情况，此时使用哪种方法确定最后使用哪个算法。
 */
public class JudgeMethod {

    /**
     * 金叉死叉成功率
     */
    public static final Integer GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE = 1;

    /**
     * 金叉死叉百分比
     */
    public static final Integer GOLD_CROSS_DEAD_CROSS_PERCENTAGE = 2;
}
