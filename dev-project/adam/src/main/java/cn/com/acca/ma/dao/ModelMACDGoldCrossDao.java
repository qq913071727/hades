package cn.com.acca.ma.dao;

import java.util.Date;
import java.util.List;

import cn.com.acca.ma.model.ModelMACDGoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

public interface ModelMACDGoldCrossDao extends BaseDao {
	/*********************************************************************************************************************
	 * 
	 * 												按日期计算数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用日线级别MACD金叉模型算法，根据endDate日期，向表MDL_MACD_GOLD_CROSS插入数据
	 * @param multithreading
	 * @param endDate
	 */
	void writeModelMACDGoldCrossIncr(boolean multithreading, String endDate);
	
	/*********************************************************************************************************************
	 * 
	 * 												   	创建图表
	 * 
	 *********************************************************************************************************************/
	/**
	 * 从表MDL_MACD_GOLD_CROSS中获取PROFIT_LOSS大于零的记录
	 * @param multithreading
	 * @return
	 */
	List<ModelMACDGoldCross> getProfitMACDGoldCross(boolean multithreading);

	/**
	 * 从表MDL_MACD_GOLD_CROSS中获取PROFIT_LOSS小于零的记录
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List getLossMACDGoldCross(boolean multithreading, String beginDate, String endDate);

	/**
	 * 根据beginDate和endDate，查找macd金叉算法成功时，dif和dea的分布。profitLoss为true时表示盈利，为true时表示亏损
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param profitLoss
	 * @return
	 */
	List getMACDGoldCrossSuccessDifDea(boolean multithreading, String beginDate, String endDate, boolean profitLoss);

	/**
	 * 获取MACD金叉算法在开始时间到结束时间内成功率的数据
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param dateNumber
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List<SuccessRateOrPercentagePojo> getMACDGoldCrossSuccessRate(boolean multithreading, final String beginDate, final String endDate, Integer dateNumber);

	/**
	 * 获取MACD金叉算法在开始时间到结束时间内，金叉股票的百分比
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<SuccessRateOrPercentagePojo> getMACDGoldCrossPercentage(boolean multithreading, final String beginDate, final String endDate);

	/*********************************************************************************************************************
	 * 
	 * 												计算全部数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用日线级别MACD金叉模型算法，向表MDL_MACD_GOLD_CROSS插入数据
	 */
	void writeModelMACDGoldCross();

	/*********************************************************************************************************************
	 * 
	 * 										增量备份表MDL_MACD_GOLD_CROSS
	 * 
	 *********************************************************************************************************************/
	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_MACD_GOLD_CROSS中获取SELL_DATE字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<Date> getDateByCondition(String beginDate, String endDate);
	
	/**
	 * 从表MDL_MACD_GOLD_CROSS中获取date日的所有ModelMACDGoldCross对象
	 * @param date
	 * @return
	 */
	List<ModelMACDGoldCross> getModelMACDGoldCrossByDate(String date);
	
	/**
	 * 向表MDL_MACD_GOLD_CROSS中插入ModelMACDGoldCross对象
	 * @param modelMACDGoldCross
	 */
	void save(ModelMACDGoldCross modelMACDGoldCross);
}
