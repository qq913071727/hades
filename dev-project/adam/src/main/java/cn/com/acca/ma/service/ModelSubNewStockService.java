package cn.com.acca.ma.service;

public interface ModelSubNewStockService extends BaseService {

	/**
	 * 创建次新股收盘价平均值图
	 * @param multithreading
	 */
	void createSubNewStockAverageClosePicture(boolean multithreading);

	/*********************************************************************************************************************
	 *
	 * 												   	创建报告
	 *
	 *********************************************************************************************************************/
	/**
	 * 将视图V_SUB_NEW_STOCK中的数据打印出来
	 */
	void printSubNewStock();
}
