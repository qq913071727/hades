package cn.com.acca.ma.dao;

import java.util.Date;
import java.util.List;

import cn.com.acca.ma.model.StockIndex;
import cn.com.acca.ma.pojo.MaxAndMin;

public interface StockIndexDao extends BaseDao {
	/******************************************** 更新每天的数据 ********************************************************/
	void deleteStockIndexMyBatis();
	void writeStockIndexMyBatis(boolean multithreading);
	
	void deleteStockIndex();
	void writeStockIndex();

	/**
	 * 根据日期date，计算某一日所有指数的移动平均线数据
	 * @param multithreading
	 * @param date
	 */
	void writeStockIndexMAByDate(boolean multithreading, String date);

	/**
	 * 根据日期date，计算某一日所有指数的Hei Kin Ashi数据
	 * @param multithreading
	 * @param date
	 */
	void writeStockIndexHeiKinAshiByDate(boolean multithreading, String date);

	/**
	 * 根据日期date，计算所有指数某一日的乖离率
	 * @param multithreading
	 * @param date
	 */
	void writeStockIndexBiasByDate(boolean multithreading, String date);

	/**
	 * 更新表STOCK_INDEX中某一日的EMA15，EMA26，DIF和DEA字段
	 * @param date
	 */
	void writeStockIndexMACDByDate(String date);

	/**
	 * 更新表STOCK_INDEX中某一日的RSV，K和D字段
	 * @param date
	 */
	void writeStockIndexKDByDate(String date);
	
	/********************************************** 根据每日数据更新图表 *************************************************/
	/**
	 * 为创建28分化图表中的线图，获取数据
	 * @param multithreading
	 * @param beginTime
	 * @param endTime
	 * @param code
	 * @return
	 */
	List<StockIndex> getDataForLineChart(boolean multithreading, String beginTime, String endTime, String code);

	/**
	 * 为创建28分化图表中的柱状图，获取日期数据
	 * @param multithreading
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<Date> getDateForBarChart(boolean multithreading, String beginTime, String endTime);
	
	/**
     * 根据开始时间和结束时间，在STOCK_INDEX表中查找所有INDEX_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
	List<Date> getDateByCondition(String beginDate, String endDate);
	
	/**
	 * 根据日期获取StockIndex对象
	 * @param date
	 * @return
	 */
	List<StockIndex> getStockIndexByDate(String date);
	
	/**
	 * 保存StockIndex对象
	 * @param stockIndex
	 */
	void saveStockIndex(StockIndex stockIndex);

	/**
	 * 在开始时间和结束时间之间，查找某个指数的最大值和最小值
	 * @param stockIndexCode
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<MaxAndMin> getMaxMinClosePriceBetween(String stockIndexCode, String beginDate, String endDate);
	
	/*********************************************** 更新全部的数据 *****************************************************/
	/**
	 * 计算5日均线
	 */
	void writeStockIndexMA5();

	/**
	 * 计算10日均线
	 */
	void writeStockIndexMA10();

	/**
	 * 计算20日均线
	 */
	void writeStockIndexMA20();

	/**
	 * 计算60日均线
	 */
	void writeStockIndexMA60();

	/**
	 * 计算120日均线
	 */
	void writeStockIndexMA120();

	/**
	 * 计算250日均线
	 */
	void writeStockIndexMA250();

	/**
	 * 计算Hei Kin Ashi相关的字段
	 */
	void writeStockIndexHeiKinAshi();

	/**
	 * 计算所有指数的乖离率
	 */
	void writeStockIndexBias();

	/**
	 * 计算所有指数的MACD
	 */
	void writeStockIndexMACD();

	/**
	 * 计算所有指数的KD
	 */
	void writeStockIndexKD();

}
