package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 计算BOARD_INDEX表中某一日的FIVE_DAY_RATE字段和TEN_DAY_RATE字段
 */
public class WriteBoardIndexFiveAndTenDayRateByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteBoardIndexFiveAndTenDayRateByDateThread.class);

    private BoardIndexService boardIndexService;

    public WriteBoardIndexFiveAndTenDayRateByDateThread() {
    }

    public WriteBoardIndexFiveAndTenDayRateByDateThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，计算BOARD_INDEX表中某一日的FIVE_DAY_RATE字段和TEN_DAY_RATE字段，"
            + "调用的方法为【writeBoardIndexFiveAndTenDayRateByDate】");

        boardIndexService.writeBoardIndexFiveAndTenDayRateByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
