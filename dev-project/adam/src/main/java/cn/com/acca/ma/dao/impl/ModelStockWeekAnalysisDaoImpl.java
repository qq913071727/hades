package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelStockMonthAnalysisDao;
import cn.com.acca.ma.dao.ModelStockWeekAnalysisDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.jdbctemplate.impl.WriteModelStockMonthAnalysisByDateTransactionCallback;
import cn.com.acca.ma.jdbctemplate.impl.WriteModelStockWeekAnalysisByDateTransactionCallback;
import cn.com.acca.ma.jdbctemplate.util.JdbcTemplateUtil;
import cn.com.acca.ma.jpa.util.JpaUtil;
import cn.com.acca.ma.model.ModelStockMonthAnalysis;
import cn.com.acca.ma.model.ModelStockWeekAnalysis;
import org.hibernate.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelStockWeekAnalysisDaoImpl extends BaseDaoImpl<ModelStockWeekAnalysis> implements
        ModelStockWeekAnalysisDao {

    public ModelStockWeekAnalysisDaoImpl() {
        super();
    }

    /**
     * 计算model_stock_week_analysis表的全部数据
     */
    @Override
    public void writeModelStockWeekAnalysis() {
        logger.info("开始计算model_stock_week_analysis表的全部数据");

        transactionTemplate = JdbcTemplateUtil.getTransactionTemplate();
        jdbcTemplate = JdbcTemplateUtil.getJdbcTemplate();

        transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(TransactionStatus transactionStatus) {
                try {
                    jdbcTemplate
                            .execute("{call PKG_MODEL_STOCK_ANALYSIS.write_mdl_stock_week_analysis}");
                } catch (Throwable e) {
                    transactionStatus.setRollbackOnly();
                    // transactionStatus.rollbackToSavepoint(savepoint);
                }
                return null;
            }
        });
    }

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_STOCK_WEEK_ANALYSIS中获取END_DATE字段
     *
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Date> getDateByCondition(String beginDate, String endDate) {
        logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
                + "从表MDL_STOCK_WEEK_ANALYSIS中获取END_DATE字段");

        StringBuffer hql = new StringBuffer("select distinct t.endDate from ModelStockWeekAnalysis t " +
                "where t.endDate between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd')");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<Date> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 从表MDL_STOCK_WEEK_ANALYSIS中获取date日的所有ModelStockWeekAnalysis对象
     *
     * @param date
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<ModelStockWeekAnalysis> getModelStockWeekAnalysisByDate(String date) {
        logger.info("从表MDL_STOCK_WEEK_ANALYSIS中获取日期【" + date + "】的所有ModelStockWeekAnalysis对象");

        StringBuffer hql = new StringBuffer("select t from ModelStockWeekAnalysis t " +
                "where t.endDate=to_date(?,'yyyy-mm-dd') order by t.endDate asc");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, date);
        List<ModelStockWeekAnalysis> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 计算model_stock_week_analysis表的某一日的数据
     *
     * @param multithreading
     * @param beginDate
     * @param endDate
     */
    @Override
    public void writeModelStockWeekAnalysisByDate(boolean multithreading, String beginDate, String endDate) {
        logger.info("开始计算model_stock_week_analysis表在某段时间内【" + beginDate + "】和【" + endDate + "】的数据");

        TransactionTemplate newTransactionTemplate = null;
        JdbcTemplate newJdbcTemplate = null;
        if (multithreading) {
            newTransactionTemplate = JdbcTemplateUtil.newTransactionTemplate();
            newJdbcTemplate = JdbcTemplateUtil.newJdbcTemplate();
            WriteModelStockWeekAnalysisByDateTransactionCallback writeModelStockWeekAnalysisByDateTransactionCallback = new WriteModelStockWeekAnalysisByDateTransactionCallback(newJdbcTemplate, beginDate, endDate);
            newTransactionTemplate.execute(writeModelStockWeekAnalysisByDateTransactionCallback);
        } else {
            transactionTemplate = JdbcTemplateUtil.getTransactionTemplate();
            jdbcTemplate = JdbcTemplateUtil.getJdbcTemplate();
            WriteModelStockWeekAnalysisByDateTransactionCallback writeModelStockWeekAnalysisByDateTransactionCallback = new WriteModelStockWeekAnalysisByDateTransactionCallback(jdbcTemplate, beginDate, endDate);
            transactionTemplate.execute(writeModelStockWeekAnalysisByDateTransactionCallback);
        }
    }

    /**
     * 根据开始日期和结束日期，查找中间的日期，并去重
     * @param beginDate
     * @param endDate
     * @return
     */
//    @Override
//    public List<String> findDistinctDateBetweenBeginDateAndEndDate(boolean multithreading, String beginDate,
//        String endDate) {
//        logger.info("根据开始日期【" + beginDate + "】和结束日期【" + endDate + "】，查找中间的日期，并去重");
//
//        String sql = "select distinct b.date_ from ( "
//            + "select to_char(t.date_, 'yyyymmdd') date_ from mdl_stock_analysis t "
//            + "where t.date_ between to_date(:beginDate,'yyyy-mm-dd') and to_date(:endDate,'yyyy-mm-dd') order by t.date_ asc) b "
//            + "order by b.date_ asc";
//        Map map = new HashMap();
//        map.put("beginDate", beginDate);
//        map.put("endDate", endDate);
//
//        if (multithreading) {
//            NamedParameterJdbcTemplate newNamedParameterJdbcTemplate = JdbcTemplateUtil.newNamedParameterJdbcTemplate();
//            TransactionTemplate newTransactionTemplate = JdbcTemplateUtil.newTransactionTemplate();
//            return (List<String>) newTransactionTemplate.execute(new TransactionCallback<Object>() {
//                @Override
//                public List<String> doInTransaction(TransactionStatus transactionStatus) {
//                    try {
//                        return newNamedParameterJdbcTemplate.queryForList(sql, map, String.class);
//                    } catch (Throwable e) {
//                        e.printStackTrace();
//                        transactionStatus.setRollbackOnly();
//                        // transactionStatus.rollbackToSavepoint(savepoint);
//                    }
//                    return null;
//                }
//            });
//        } else {
//            namedParameterJdbcTemplate = JdbcTemplateUtil.getNamedParameterJdbcTemplate();
//            transactionTemplate = JdbcTemplateUtil.getTransactionTemplate();
//            return (List<String>) transactionTemplate.execute(new TransactionCallback<Object>() {
//                @Override
//                public List<String> doInTransaction(TransactionStatus transactionStatus) {
//                    try {
//                        return namedParameterJdbcTemplate.queryForList(sql, map, String.class);
//                    } catch (Throwable e) {
//                        e.printStackTrace();
//                        transactionStatus.setRollbackOnly();
//                        // transactionStatus.rollbackToSavepoint(savepoint);
//                    }
//                    return null;
//                }
//            });
//        }
//    }

    /**
     * 向表MDL_STOCK_WEEK_ANALYSIS中插入ModelStockWeekAnalysis对象
     *
     * @param modelStockWeekAnalysis
     */
    public void save(ModelStockWeekAnalysis modelStockWeekAnalysis) {
        logger.info("向表MDL_STOCK_WEEK_ANALYSIS中插入ModelStockWeekAnalysis对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.persist(modelStockWeekAnalysis);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 根据开始时间和结束时间，计算每个交易周、所有股票的平均收盘价和平均方差，并按时间升序排列
     *
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<Object> findAverageClosePriceAndAverageStandardDeviationByBeginDateAndEndDateOrderByDate(String beginDate, String endDate) {
        logger.info("开始根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，" +
                "计算每个交易周、所有股票的平均收盘价，并按时间升序排列");

        String stockWeekIndexString = new String("select siw.begin_date, siw.end_date from stock_index_week siw " +
                "where siw.begin_date>=to_date('" + beginDate + "','yyyy-mm-dd') and siw.end_date <= to_date('" + endDate + "','yyyy-mm-dd') " +
                "order by siw.begin_date asc");
        em = JpaUtil.currentEntityManager();
        em.getTransaction().begin();
        javax.persistence.Query stockWeekIndexQuery = em.createNativeQuery(stockWeekIndexString);
        List<Object> stockWeekIndexBeginDateAndEndDateList = stockWeekIndexQuery.getResultList();

        List<Object> resultList = new ArrayList<>();
        if (null != stockWeekIndexBeginDateAndEndDateList && stockWeekIndexBeginDateAndEndDateList.size() > 0) {
            for (int i = 0; i < stockWeekIndexBeginDateAndEndDateList.size(); i++) {
                Object object = stockWeekIndexBeginDateAndEndDateList.get(i);
                Object[] objectArray = (Object[]) object;
                Date stockWeekBeginDate = (Date) objectArray[0];
                Date stockWeekEndDate = (Date) objectArray[1];

                String queryClose = new String(
                        "select avg(sw.close_price), avg(mwsa.standard_deviation), max(sw.close_price), min(sw.close_price)," +
                                "max(mwsa.standard_deviation), min(mwsa.standard_deviation), ? " +
                                "from stock_week sw " +
                                "join mdl_stock_week_analysis mwsa on mwsa.code_=sw.code_ " +
                                "and mwsa.begin_date >= ?  and mwsa.end_date <= ? " +
                                "and mwsa.standard_deviation is not null " +
                                "where sw.begin_date >= ? and sw.end_date <= ?");

                em = JpaUtil.currentEntityManager();
                em.getTransaction().begin();
                javax.persistence.Query query = em.createNativeQuery(queryClose);
                query.setParameter(1, stockWeekEndDate);
                query.setParameter(2, stockWeekBeginDate);
                query.setParameter(3, stockWeekEndDate);
                query.setParameter(4, stockWeekBeginDate);
                query.setParameter(5, stockWeekEndDate);

                List<Object> list = query.getResultList();

                em.getTransaction().commit();
                em.close();

                resultList.addAll(list);
            }
            return resultList;
        }

        return null;
    }
}
