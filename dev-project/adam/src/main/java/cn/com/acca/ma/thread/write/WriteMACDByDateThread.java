package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 更新表STOCK_TRANSACTION_DATA中某一日的EMA15，EMA26，DIF和DEA字段
 */
public class WriteMACDByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteMACDByDateThread.class);

    private StockTransactionDataService stockTransactionDataService;

    public WriteMACDByDateThread() {
    }

    public WriteMACDByDateThread(StockTransactionDataService stockTransactionDataService) {
        this.stockTransactionDataService = stockTransactionDataService;
    }

    @Override
    public void run() {
        logger.info("启动线程，更新表STOCK_TRANSACTION_DATA中某一日的EMA15，EMA26，DIF和DEA字段，"
            + "调用的方法为【writeMACDByDate】");

        stockTransactionDataService.writeMACDByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
