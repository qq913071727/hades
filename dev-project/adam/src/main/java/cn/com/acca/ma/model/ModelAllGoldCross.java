package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 收盘价、所有均线，相互金叉时的模拟交易表
 */
@Entity
@Table(name = "MDL_ALL_GOLD_CROSS")
public class ModelAllGoldCross implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 *主键
	 */
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	/**
	 * 股票代码
	 */
	@Column(name = "STOCK_CODE")
	private String stockCode;

	/**
	 * 卖出日期
	 */
	@Column(name = "SELL_DATE")
	private Date sellDate;

	/**
	 * 卖出价格
	 */
	@Column(name = "SELL_PRICE")
	private BigDecimal sellPrice;

	/**
	 * 买入日期
	 */
	@Column(name = "BUY_DATE")
	private Date buyDate;

	/**
	 * 买入价格
	 */
	@Column(name = "BUY_PRICE")
	private BigDecimal buyPrice;

	/**
	 * 累计收益。单位：元
	 */
	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

	/**
	 * 本次交易盈亏百分比
	 */
	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

	/**
	 * 金叉类型。比如c_120表示收盘价金叉120日均线，60_120表示60日均线金叉120日均线
	 */
	@Column(name = "TYPE_")
	private String type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getAccumulativeProfitLoss() {
		return accumulativeProfitLoss;
	}

	public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
		this.accumulativeProfitLoss = accumulativeProfitLoss;
	}

	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
