package cn.com.acca.ma.common.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import cn.com.acca.ma.model.CommodityFutureDateContractData;
import cn.com.acca.ma.model.ForeignExchangeRecord;

public class DateUtil {
	/**
	 * 输入一个Date类型对象，将其转换为String字符串格式，如：20150117
	 * @param date
	 * @return String字符串格式，如：20150117
	 */
	@SuppressWarnings("deprecation")
	public static String dateToString(Date date){
		String realYear=null;
		String realMonth=null;
		String realDate=null;
		
		realYear=String.valueOf(date.getYear()+1900);
		
		if(String.valueOf(date.getMonth()+1).length()!=2){
			realMonth="0"+String.valueOf(date.getMonth()+1);
		}else{
			realMonth=String.valueOf(date.getMonth()+1);
		}
		
		if(String.valueOf(date.getDate()).length()!=2){
			realDate="0"+String.valueOf(date.getDate());
		}else{
			realDate=String.valueOf(date.getDate());
		}
		
		return realYear+realMonth+realDate;
	}
	
	/**
	 * 输入一个String字符串对象，将其转换为Date类型对象，如：Sat Jan 17 15:18:12 CST 2015。
	 * 这个String字符串的格式为：yyyy-MM-dd或yyyyMMdd。
	 * @param date
	 * @return Date类型对象，如：Sat Jan 17 15:18:12 CST 2015
	 */
	@SuppressWarnings("deprecation")
	public static Date stringToDate(String date){
		int year;
		int month;
		int day;

		date = date.replace("-", "");

		year=Integer.parseInt(date.substring(0,4))-1900;
		
		if("0".equals(date.substring(4,5))){
			month=Integer.parseInt(date.substring(5,6))-1;
		}else{
			month=Integer.parseInt(date.substring(4,6))-1;
		}
		
		if("0".equals(date.substring(6))){
			day=Integer.parseInt(date.substring(6));
		}else{
			day=Integer.parseInt(date.substring(6,8));
		}
		
		return new Date(year,month,day);
	}
	
	/**
	 * 获取Date类型对象的year变量，需要+1900
	 * @param date
	 * @return year变量
	 */
	@SuppressWarnings("deprecation")
	public static int getYearFromDate(Date date){
		return date.getYear()+1900;
	}
	
	/**
	 * 获取Date类型对象的month变量，需要+1
	 * @param date
	 * @return month变量
	 */
	@SuppressWarnings("deprecation")
	public static int getMonthFromDate(Date date){
		return date.getMonth()+1;
	}
	
	/**
	 * 获取Date类型对象的date变量，直接返回
	 * @param date
	 * @return date变量
	 */
	@SuppressWarnings("deprecation")
	public static int getDateFromDate(Date date){
		return date.getDate();
	}
	
	/**
	 * 判断两个日期是否是同一个星期
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameWeekend(String date1,String date2){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = null;
		Date d2 = null;
		try{
			d1 = format.parse(date1);
			d2 = format.parse(date2);
		}catch(Exception e){
			e.printStackTrace();
		}
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(d1);
		cal2.setTime(d2);
		int subYear = cal1.get(Calendar.YEAR)-cal2.get(Calendar.YEAR);
		//subYear==0,说明是同一年
		if(subYear == 0){
			if(cal1.get(Calendar.WEEK_OF_YEAR) == cal2.get(Calendar.WEEK_OF_YEAR))
		    return true;
		}
		//例子:cal1是"2005-1-1"，cal2是"2004-12-25"
		//java对"2004-12-25"处理成第52周
		// "2004-12-26"它处理成了第1周，和"2005-1-1"相同了
		//大家可以查一下自己的日历
		//处理的比较好
		//说明:java的一月用"0"标识，那么12月用"11"
		else if(subYear==1 && cal2.get(Calendar.MONTH)==11){
			if(cal1.get(Calendar.WEEK_OF_YEAR) == cal2.get(Calendar.WEEK_OF_YEAR))
		    return true;
		}
		//例子:cal1是"2004-12-31"，cal2是"2005-1-1"
		else if(subYear==-1 && cal1.get(Calendar.MONTH)==11){
			if(cal1.get(Calendar.WEEK_OF_YEAR) == cal2.get(Calendar.WEEK_OF_YEAR))
			return true;
		}
		return false;
	}
	
	/**
	 * 已知周数（一年中的第几周），求这一周星期五的日期
	 * @param year 年
	 * @param weekNumber 周数（一年中的第几周）
	 * @return 这一周星期五的日期
	 */
	public static Date fromYearAndWeekNumberToFriday(int year, int weekNumber){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.WEEK_OF_YEAR,weekNumber);//weekNumber为周数
		//Calendar cal1 = (Calendar)cal.clone();  
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);   
		cal.add(Calendar.DATE,cal.getActualMaximum(Calendar.DAY_OF_WEEK)-dayOfWeek-2);  
		cal.add(Calendar.DATE, 1);  
		Date d = cal.getTime();  
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
		return d;//sdf.format(d); 
	}

//	public static Date fromWeekNumberToFriday(int year, int weekNumber) {
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-w e");
//		LocalDate startDate = LocalDate.parse(weekNumber + " 2", formatter);
//		LocalDate endDate = LocalDate.parse(weekNumber + " 7", formatter);
//	}
	
	/**
	 * List<ForeignExchangeRecord>数组中的每个对象只有一个有效字段dateTime，
	 * 当截断年月日时会有重复，因此要去掉重复的，最后再以List<Date>类型返回。
	 * @param list
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static List<Date> timestampToDateWithoutDuplicate(List<ForeignExchangeRecord> list){
		Set<Date> set=new TreeSet<Date>();
		for(int i=0;i<list.size();i++){
			set.add(new Date(list.get(i).getDateTime().getYear(),list.get(i).getDateTime().getMonth(),list.get(i).getDateTime().getDate()));
		}
		List<Date> result=new ArrayList<Date>();
		Iterator<Date> itr=set.iterator();
		while(itr.hasNext()){
			result.add(itr.next());
		}
		return result;
	}

	/**
	 * List<CommodityFutureDateContractData>数组中的每个对象只有一个有效字段transactionDate，
	 * 当截断年月日时会有重复，因此要去掉重复的，最后再以List<Date>类型返回。
	 * @param list
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static List<Date> timestampToTransactionDateWithoutDuplicate(List<CommodityFutureDateContractData> list){
		Set<Date> set=new TreeSet<Date>();
		for(int i=0;i<list.size();i++){
			set.add(new Date(list.get(i).getTransactionDate().getYear(),list.get(i).getTransactionDate().getMonth(),list.get(i).getTransactionDate().getDate()));
		}
		List<Date> result=new ArrayList<Date>();
		Iterator<Date> itr=set.iterator();
		while(itr.hasNext()){
			result.add(itr.next());
		}
		return result;
	}

	/**
	 * 判断某个日期是星期几
	 * @param date
	 * @return
	 */
	public static Integer dayForWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		if (date != null) {
			cal.setTime(date);
		}
		int week = cal.get(Calendar.DAY_OF_WEEK);
		switch (week) {
			case 1:
				return Calendar.SUNDAY; // 星期日
			case 2:
				return Calendar.MONDAY; // 星期一
			case 3:
				return Calendar.TUESDAY; // 星期二
			case 4:
				return Calendar.WEDNESDAY; // 星期三
			case 5:
				return Calendar.THURSDAY; // 星期四
			case 6:
				return Calendar.FRIDAY; // 星期五
			case 7:
				return Calendar.SATURDAY;  // 星期六
		}
		return null;
	}

	/**
	 * 在给定的日期加上或减去指定月份后的日期
	 *
	 * @param sourceDate 原始时间
	 * @param month      要调整的月份，向前为负数，向后为正数
	 * @return
	 */
	public static Date stepMonth(Date sourceDate, int month) {
		Calendar c = Calendar.getInstance();
		c.setTime(sourceDate);
		c.add(Calendar.MONTH, month);

		return c.getTime();
	}

	/**
	 * 当前日期的前一天
	 * @param dateString
	 * @return
	 */
	public static String lastDate(String dateString){
		Date date = DateUtil.stringToDate(dateString);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		String lastDate = DateUtil.dateToString(calendar.getTime());
		return lastDate;
	}

	/**
	 * 当前日期的下一天
	 * @param currentDate
	 * @param dateNumber
	 * @return
	 */
	public static String nextDate(String currentDate, Integer dateNumber){
		Date date = DateUtil.stringToDate(currentDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, dateNumber);
		String lastDate = DateUtil.dateToString(calendar.getTime());
		return lastDate;
	}

	/**
	 * 当前日期的向前（year为负数）/向后（year为证书）year年
	 * @param dateString
	 * @param year
	 * @return
	 */
	public static String beforeYear(String dateString, int year){
		Date date = DateUtil.stringToDate(dateString);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, year);
		String lastYear = DateUtil.dateToString(calendar.getTime());
		return lastYear;
	}
}


