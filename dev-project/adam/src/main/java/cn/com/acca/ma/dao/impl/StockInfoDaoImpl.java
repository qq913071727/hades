package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.model.BoardIndex;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import cn.com.acca.ma.dao.StockInfoDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.StockInfo;

public class StockInfoDaoImpl extends BaseDaoImpl<StockInfoDaoImpl> implements StockInfoDao {

	public StockInfoDaoImpl() {
		super();
	}

	/**
	 * 通过股票代码stockCode，查找StockInfo对象
	 * @param stockCode
	 * @return
	 */
	public StockInfo getStockInfoObjectByStockCode(String stockCode) {
		logger.info("通过股票代码stockCode，查找StockInfo对象");
		
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		Criteria criteria=session.createCriteria(StockInfo.class);
		criteria.add(Restrictions.eq("code", stockCode.toUpperCase()));
		StockInfo result=(StockInfo) criteria.uniqueResult();
		session.getTransaction().commit();
		session.close();
		
		return result;
	}

	/**
	 * 获得所有StockInfo对象
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<StockInfo> getAllStockInfo(){
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		Criteria criteria=session.createCriteria(StockInfo.class);
		List<StockInfo> result=criteria.list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	/**
	 * 保存StockInfo对象
	 * @param stockInfo
	 */
	public void saveStockInfo(StockInfo stockInfo){
		logger.info("保存StockInfo对象");
		
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		session.save(stockInfo);
		session.getTransaction().commit();
		session.close();
	}
}
