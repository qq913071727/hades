package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建蜘蛛雷达图
 */
public class CreateSpiderWebPlotPictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateSpiderWebPlotPictureThread.class);

    private BoardIndexService boardIndexService;

    public CreateSpiderWebPlotPictureThread() {
    }

    public CreateSpiderWebPlotPictureThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，创建蜘蛛雷达图，调用的方法为【createSpiderWebPlotPicture】");

        boardIndexService.createSpiderWebPlotPicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
