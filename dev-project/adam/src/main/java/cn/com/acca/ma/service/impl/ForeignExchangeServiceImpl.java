package cn.com.acca.ma.service.impl;

import java.util.List;

import cn.com.acca.ma.service.ForeignExchangeService;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.common.util.StringUtil;
import cn.com.acca.ma.model.ForeignExchange;
import cn.com.acca.ma.common.util.CurrencyPartUtil;

public class ForeignExchangeServiceImpl extends BaseServiceImpl<ForeignExchangeServiceImpl,ForeignExchange> implements ForeignExchangeService {

	/**
	 * 向FOREIGN_EXCHANGE表中插入数据
	 */
	public void insertForeignExchange() {
		String currencies=PropertiesUtil.getValue(FOREIGN_EXCHANGE_PROPERTIES, "foreign_exchange.currencies");
		String[] currencyArray=currencies.split(",");
		for(String cur:currencyArray){
			ForeignExchange foreignExchange=new ForeignExchange();
			logger.info(StringUtil.removeBracket(cur.trim()));
			logger.info(CurrencyPartUtil.getDescription(StringUtil.removeBracket(cur.trim())));
			foreignExchange.setName(StringUtil.removeBracket(cur.trim()));
			foreignExchange.setDescription(CurrencyPartUtil.getDescription(StringUtil.removeBracket(cur.trim())));
			foreignExchangeDao.insertForeignExchange(foreignExchange);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public String listToString(List list) {
		// TODO Auto-generated method stub
		return null;
	}
}
