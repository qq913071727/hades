package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MDL_PERCENTAGE_MA_GOLD_CROSS")
public class ModelPercentageMaGoldCross implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "STOCK_CODE")
    private String stockCode;

    @Column(name = "TYPE_")
    private String type;

    @Column(name="SELL_DATE")
    private Date sellDate;

    @Column(name="SELL_PRICE")
    private BigDecimal sellPrice;

    @Column(name = "SELL_UP_DOWN_PERCENTAGE")
    private BigDecimal sellUpDownPercentage;

    @Column(name = "SELL_MA")
    private BigDecimal sellMa;

    @Column(name="BUY_DATE")
    private Date buyDate;

    @Column(name="BUY_PRICE")
    private BigDecimal buyPrice;

    @Column(name = "BUY_UP_DOWN_PERCENTAGE_MA")
    private BigDecimal buyUpDownPercentageMa;

    @Column(name = "BUY_MA")
    private BigDecimal buyMa;

    @Column(name = "ACCUMULATIVE_PROFIT_LOSS")
    private BigDecimal accumulativeProfitLoss;

    @Column(name = "PROFIT_LOSS")
    private BigDecimal profitLoss;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getSellUpDownPercentage() {
        return sellUpDownPercentage;
    }

    public void setSellUpDownPercentage(BigDecimal sellUpDownPercentage) {
        this.sellUpDownPercentage = sellUpDownPercentage;
    }

    public BigDecimal getSellMa() {
        return sellMa;
    }

    public void setSellMa(BigDecimal sellMa) {
        this.sellMa = sellMa;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getBuyUpDownPercentageMa() {
        return buyUpDownPercentageMa;
    }

    public void setBuyUpDownPercentageMa(BigDecimal buyUpDownPercentageMa) {
        this.buyUpDownPercentageMa = buyUpDownPercentageMa;
    }

    public BigDecimal getBuyMa() {
        return buyMa;
    }

    public void setBuyMa(BigDecimal buyMa) {
        this.buyMa = buyMa;
    }

    public BigDecimal getAccumulativeProfitLoss() {
        return accumulativeProfitLoss;
    }

    public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
        this.accumulativeProfitLoss = accumulativeProfitLoss;
    }

    public BigDecimal getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(BigDecimal profitLoss) {
        this.profitLoss = profitLoss;
    }
}
