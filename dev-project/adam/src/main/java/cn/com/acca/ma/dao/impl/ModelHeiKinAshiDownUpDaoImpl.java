package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelHeiKinAshiDownUpDao;
import cn.com.acca.ma.hibernate.impl.GetHeiKinAshiDownUpSuccessRateWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelHeiKinAshiDownUp;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelHeiKinAshiDownUpDaoImpl extends BaseDaoImpl<ModelHeiKinAshiDownUp> implements
        ModelHeiKinAshiDownUpDao {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     * @param multithreading
     * @param endDate
     */
    @Override
    public void writeModelHeiKinAshiDownUpIncr(boolean multithreading, String endDate) {
        logger.info("根据日期【" + endDate + "】，向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据");

        Session newSession = null;
        SQLQuery query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_H_K_A_DOWN_UP_INCR(?)}");
        } else {
            session= HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_H_K_A_DOWN_UP_INCR(?)}");
        }

        query.setString(0, endDate);
        query.executeUpdate();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        logger.info("根据日期【" + endDate + "】，向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据完成");
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     */
    @Override
    public void writeModelHeiKinAshiDownUp() {
        logger.info("开始向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_HEI_KIN_ASHI_DOWN_UP()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据完成");
    }

    /**
     * 获取hei_kin_ashi在开始时间到结束时间内，下跌趋势成功率的数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<SuccessRateOrPercentagePojo> getHeiKinAshiDownUpSuccessRate(boolean multithreading, final String beginDate, final String endDate, Integer dateNumber){
        logger.info("获取开始时间【" + beginDate + "】至【" + endDate + "】内，最近【" + dateNumber + "】天，hei_kin_ashi下跌趋势的成功率");

        final List<SuccessRateOrPercentagePojo> list=new ArrayList<SuccessRateOrPercentagePojo>();

        if (multithreading) {
            Session newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            newSession.doWork(new GetHeiKinAshiDownUpSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session=HibernateUtil.currentSession();
            session.beginTransaction();
            session.doWork(new GetHeiKinAshiDownUpSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            session.getTransaction().commit();
            session.close();
        }

        logger.info("获取一段时间内，最近" + dateNumber + "天，hei_kin_ashi下跌趋势的成功率完成");
        return list;
    }

    /**
     * 获取一段时间内，hei_kin_ashi下跌趋势的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SuccessRateOrPercentagePojo> getHeiKinAshiDownUpPercentage(boolean multithreading, String beginDate, String endDate){
        logger.info("获取开始时间【" + beginDate + "】至【" + endDate + "】内，hei_kin_ashi下跌趋势的百分比");

        String hql = "select t.date_ transactionDate, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_ " +
                "and t1.ha_close_price<t1.ha_open_price)/ " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 successRateOrPercentage " +
                "from stock_transaction_data_all t " +
                "where t.date_ between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd') " +
                "group by t.date_ order by t.date_ asc";

        Session newSession = null;
        Query query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        }

        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<SuccessRateOrPercentagePojo> list = query.list();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        return list;
    }

    /*********************************************************************************************************************
     *
     * 										增量备份表MDL_HEI_KIN_ASHI_DOWN_UP
     *
     *********************************************************************************************************************/
    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_HEI_KIN_ASHI_DOWN_UP中获取BUY_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Date> getDateByCondition(String beginDate, String endDate){
        logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
                + "从表MDL_HEI_KIN_ASHI_DOWN_UP中获取BUY_DATE字段");

        StringBuffer hql = new StringBuffer("select distinct t.buyDate from ModelHeiKinAshiDownUp t " +
                "where t.buyDate between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd')");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<Date> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 从表MDL_HEI_KIN_ASHI_DOWN_UP中获取date日的所有ModelHeiKinAshiDownUp对象
     * @param date
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<ModelHeiKinAshiDownUp> getModelHeiKinAshiDownUpByDate(String date){
        logger.info("从表MDL_HEI_KIN_ASHI_DOWN_UP中获取日期【" + date + "】的所有ModelHeiKinAshiDownUp对象");

        StringBuffer hql = new StringBuffer("select t from ModelHeiKinAshiDownUp t " +
                "where t.buyDate=to_date(?,'yyyy-mm-dd') order by t.buyDate asc");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, date);
        List<ModelHeiKinAshiDownUp> list=query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP中插入ModelHeiKinAshiDownUp对象
     * @param modelHeiKinAshiDownUp
     */
    public void save(ModelHeiKinAshiDownUp modelHeiKinAshiDownUp){
        logger.info("向表MDL_HEI_KIN_ASHI_DOWN_UP中插入ModelHeiKinAshiDownUp对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.persist(modelHeiKinAshiDownUp);
        session.getTransaction().commit();
        session.close();
    }
}
