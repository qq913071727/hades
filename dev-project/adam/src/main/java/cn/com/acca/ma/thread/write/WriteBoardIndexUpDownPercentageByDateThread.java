package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 计算BOARD_INDEX表中某一日的UP_DOWN_PERCENTAGE字段
 */
public class WriteBoardIndexUpDownPercentageByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteBoardIndexUpDownPercentageByDateThread.class);

    private BoardIndexService boardIndexService;

    public WriteBoardIndexUpDownPercentageByDateThread() {
    }

    public WriteBoardIndexUpDownPercentageByDateThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，计算BOARD_INDEX表中某一日的UP_DOWN_PERCENTAGE字段，"
            + "调用的方法为【writeBoardIndexUpDownPercentageByDate】");

        boardIndexService.writeBoardIndexUpDownPercentageByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
