package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 股票分析（周线级别）
 */
@Data
@Entity
@Table(name = "MDL_STOCK_WEEK_ANALYSIS")
public class ModelStockWeekAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
    private Integer id;

    /**
     * 开始日期
     */
    @Column(name="BEGIN_DATE")
    private Date beginDate;

    /**
     * 结束日期
     */
    @Column(name="END_DATE")
    private Date endDate;

    /**
     * 代码
     */
    @Column(name="CODE_")
    private String code;

    /**
     * 移动平均线的趋势
     */
    @Column(name="MA_TREND")
    private Integer maTrend;

    /**
     * MACD的趋势
     */
    @Column(name="MACD_TREND")
    private Integer macdTrend;

    /**
     * KD的趋势
     */
    @Column(name="KD_TREND")
    private Integer kdTrend;

    /**
     * 当周股价是涨是跌。1表示上涨，-1表示下跌，0表示平
     */
    @Column(name="UP_DOWN")
    private Integer upDown;

    /**
     * 将收盘价和均线从高到低排序。0表示close_price，1表示ma5，2表示ma10，3表示ma20，4表示ma60，5表示ma120，6表示ma250
     */
    @Column(name="CLOSE_PRICE_MA_ORDER")
    private String closePriceMaOrder;

    /**
     * 标准差
     */
    @Column(name="STANDARD_DEVIATION")
    private String standardDeviation;

}
