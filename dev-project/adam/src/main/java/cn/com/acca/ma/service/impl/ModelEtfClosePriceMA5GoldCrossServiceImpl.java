package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelEtfClosePriceMA5GoldCross;
import cn.com.acca.ma.service.ModelEtfClosePriceMA5GoldCrossService;

import java.util.List;

public class ModelEtfClosePriceMA5GoldCrossServiceImpl extends BaseServiceImpl<ModelEtfClosePriceMA5GoldCrossServiceImpl, ModelEtfClosePriceMA5GoldCross> implements
        ModelEtfClosePriceMA5GoldCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_ETF_CLOSE_PRICE_MA5_G_C中插入数据
     */
    @Override
    public void writeModelEtfClosePriceMA5GoldCross() {
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 0, "19970101", "20221231", 1);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 1, "19970101", "20221231", 2);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 0, "19970101", "20221231", 3);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 1, "19970101", "20221231", 4);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 0, "20110101", "20221231", 5);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 1, "20110101", "20221231", 6);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 0, "20110101", "20221231", 7);
        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 1, "20110101", "20221231", 8);
    }

    /*********************************************************************************************************************
     *
     * 												计算增量数据
     *
     *********************************************************************************************************************/
    /**
     * 增量地向表MDL_ETF_CLOSE_PRICE_MA5_G_C中插入数据
     */
    @Override
    public void writeModelEtfClosePriceMA5GoldCrossByDate() {
        String endDate = PropertiesUtil.getValue(MDL_ETF_CLOSE_PRICE_MA5_G_C_PROPERTIES, "etf.close.price.ma5.gold.cross.end_date");

        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 0, "19970101", endDate, 1);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 1, "19970101", endDate, 2);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 0, "19970101", endDate, 3);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 1, "19970101", endDate, 4);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 0, "20110101", endDate, 5);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 1, "20110101", endDate, 6);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(1, 0, "20110101", endDate, 7);
//        modelEtfClosePriceMA5GoldCrossDao.writeModelEtfClosePriceMA5GoldCross(0, 1, "20110101", endDate, 8);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
