package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelStockIndexMACDGoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelStockIndexMACDGoldCross;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import java.util.List;

public class ModelStockIndexMACDGoldCrossDaoImpl extends BaseDaoImpl<ModelStockIndexMACDGoldCross> implements ModelStockIndexMACDGoldCrossDao {

    public ModelStockIndexMACDGoldCrossDaoImpl() {
        super();
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD金叉模型算法，向表MDL_STOCK_INDEX_MACD_G_C插入数据
     */
    @Override
    public void writeModelStockIndexMACDGoldCross() {
        logger.info("开始使用日线级别MACD金叉模型算法，向表MDL_STOCK_INDEX_MACD_G_C插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_INDEX.CAL_MDL_STOCK_INDEX_MACD_G_C()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("使用日线级别MACD金叉模型算法，向表MDL_STOCK_INDEX_MACD_G_C插入数据完成");
    }

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_macd_g_c中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
    @Override
    public List<ModelStockIndexMACDGoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode) {
        logger.info("开始根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_macd_g_c中返回数据，并按照sell_date升序排列");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery("select t from ModelStockIndexMACDGoldCross t " +
                "where t.stockIndexCode=? and t.type=? " +
                "order by t.sellDate asc");
        query.setParameter(0, indexCode);
        query.setParameter(1, type);
        List<ModelStockIndexMACDGoldCross> modelStockIndexMACDGoldCrossList = query.list();
        session.getTransaction().commit();
        session.close();

        logger.info("根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_macd_g_c中返回数据，并按照sell_date升序排列完成");

        return modelStockIndexMACDGoldCrossList;
    }
}
