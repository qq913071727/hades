package cn.com.acca.ma.xmlmapper;

import cn.com.acca.ma.model.StockIndexWeek;
import cn.com.acca.ma.model.StockIndexWeekend;
import cn.com.acca.ma.pojo.StockWeekDatePojo;

import java.util.List;
import java.util.Map;

public interface StockIndexWeekMapper {
	/******************************************** 更新每周的数据 ********************************************************/
	/**
	 * 根据日期，更新STOCK_INDEX_WEEK表的基础数据
	 */
	void writeStockIndexWeekByDate(Map<String, String> map);

	/**
	 * 根据日期，更新STOCK_INDEX_WEEK表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekHeiKinAshiByDate(Map<String, String> map);

	/**************************************** 根据每周数据更新图表 *********************************************************/
	/**
	 * 为创建周线级别StockIndexWeek对象的Hei Kin Ashi图，获取数据
	 * @param map
	 * @return
	 */
	List<StockIndexWeek> getDataForLineChart(Map<String, String> map);

	/**
	 * 按照开始日期和结束日期分组，查找开始日期和结束日期之间的begin_date和end_date
	 * @param map
	 * @return
	 */
	List findBeginDateAndEndDateGroupByBeginDateAndEndDateOrderByBeginDate(Map map);

	/************************************************* 更新全部的数据 ***************************************************/
	/**
	 * 更新STOCK_INDEX_WEEK表的基础数据
	 */
	void writeStockIndexWeek();

	/**
	 * 更新STOCK_INDEX_WEEK表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekHeiKinAshi();

	/************************************* 增量备份表STOCK_INDEX_WEEK *************************************************/
	/**
	 * 根据开始日期beginDate和结束日期endDate获取StockIndexWeek对象
	 * @param map
	 * @return
	 */
	List<StockIndexWeek> getStockIndexWeekWithinDate(Map<String, String> map);

	/************************************* 增量回复表STOCK_INDEX_WEEK中的数据 ********************************************/
	/**
	 * 向表STOCK_INDEX_WEEKEND中插入StockIndexWeek对象
	 */
	void saveStockIndexWeek(Map<String, Object> map);

	/************************************* 查询 ********************************************/
	/**
	 * 根据某个日期，返回这个日期所属的周的前一周的开始日期和结束日期
	 * @param date
	 * @return
	 */
	StockIndexWeek findBeginDateAndEndDateInLastWeekByDate(String date);
}
