package cn.com.acca.ma.service;

public interface WechatSubscriptionService extends BaseService {

    /**
     * 先并行计算某一日的均线、MACD，再并行生成图片
     */
    void asyncHandleBasedOnBasicData();

    /**
     * 先并行计算某一日的均线、MACD，再并行生成图片
     */
    void asyncHandleBasedOnMovingAverage();

    /**
     * 先并行计算某一日的均线、MACD，再并行生成图片
     */
    void asyncHandleBasedOnMACD();

    /**
     * 先并行计算某一日的均线、MACD，再并行生成图片
     */
    void asyncHandleBasedOnStockIndex();

    /**
     * 先并行计算某一日的均线、MACD，再并行生成图片
     */
    void asyncHandleBasedOnBoardIndex();

    /**
     * 基基于周线级别数据的穿行处理：计算数据，生成图片
     */
    void handleBasedOnWeekData();

    /**
     * 生成微信订阅号报告
     */
    void generateWechatSubscriptionReport();

    /**
     * 生成微信订阅号报告之后的处理：计算数据
     */
    void calculateBasicDataAfterGeneratingWechatSubscriptionReport();
}
