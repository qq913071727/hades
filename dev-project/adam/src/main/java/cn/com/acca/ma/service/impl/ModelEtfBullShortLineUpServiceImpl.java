package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelBullShortLineUp;
import cn.com.acca.ma.model.ModelEtfBullShortLineUp;
import cn.com.acca.ma.service.ModelBullShortLineUpService;
import cn.com.acca.ma.service.ModelEtfBullShortLineUpService;

import java.util.List;

public class ModelEtfBullShortLineUpServiceImpl extends BaseServiceImpl<ModelEtfBullShortLineUpServiceImpl, ModelEtfBullShortLineUp> implements
        ModelEtfBullShortLineUpService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_ETF_BULL_SHORT_LINE_UP中插入数据
     */
    @Override
    public void writeModelEtfBullShortLineUp() {
        modelEtfBullShortLineUpDao.writeModelEtfBullShortLineUp();
    }

    /**
     * 增量地向表MDL_ETF_BULL_SHORT_LINE_UP中插入数据
     */
    @Override
    public void writeModelEtfBullShortLineUpIncr() {
        String date = PropertiesUtil
                .getValue(MODEL_ETF_BULL_SHORT_LINE_UP_PROPERTIES, "etf.bull.short.line.up.date");
        modelEtfBullShortLineUpDao.writeModelEtfBullShortLineUpIncr(date);
    }


    @Override
    public String listToString(List list) {
        return null;
    }
}
