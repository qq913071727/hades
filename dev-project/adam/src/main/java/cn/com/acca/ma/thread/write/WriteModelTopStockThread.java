package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.ModelTopStockService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 按照日期，向表MDL_TOP_STOCK中写入数据
 */
public class WriteModelTopStockThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteModelTopStockThread.class);

    private ModelTopStockService modelTopStockService;

    public WriteModelTopStockThread() {
    }

    public WriteModelTopStockThread(ModelTopStockService modelTopStockService) {
        this.modelTopStockService = modelTopStockService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始按照日期，向表MDL_TOP_STOCK中写入数据，"
            + "调用的方法为【writeModelTopStock】");

        modelTopStockService.writeModelTopStock(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
