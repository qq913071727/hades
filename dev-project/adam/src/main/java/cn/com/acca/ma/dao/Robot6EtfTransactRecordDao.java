package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Robot6StockTransactRecord;
import cn.com.acca.ma.pojo.BuySuggestion;
import cn.com.acca.ma.pojo.SellSuggestion;

import java.util.List;

public interface Robot6EtfTransactRecordDao extends BaseDao {

    /**
     * 卖股票/买股票
     * @param sellDate
     * @param mandatoryStopLoss
     * @param mandatoryStopLossRate
     * @param mdlEtfType
     */
    void sellOrBuy(String sellDate, Integer mandatoryStopLoss, Double mandatoryStopLossRate, Integer mdlEtfType);

    /**
     * 买股票。用于做多
     * @param buyDate
     * @param maxHoldEtfNumber
     * @param holdShippingSpaceType
     */
    void buyOrSell(String buyDate, Integer maxHoldEtfNumber, Integer holdShippingSpaceType);

    /**
     * 买股票。用于做多
     * @param buyDate
     * @param backwardMonth
     * @param averageDateNumber
     * @param successRateType
     * @param successRate
     * @param direction
     * @param shippingSpaceControl
     * @param percentageTopThreshold
     * @param shippingSpace
     */
    void buyOrSell(String buyDate, Integer backwardMonth, Integer averageDateNumber, Integer successRateType,
                   Double successRate, Integer direction, Integer shippingSpaceControl, Double percentageTopThreshold,
                   Integer shippingSpace);

    /**
     * 获取买入建议列表
     * 用于做多
     * @param buyDate
     * @return
     */
    List<Robot6StockTransactRecord> getBullRobotEtfTransactRecordListForBuy(String buyDate);

    /**
     * 获取卖出建议列表
     * 用于做多
     * @param sellDate
     * @return
     */
    List<Robot6StockTransactRecord> getBullRobotEtfTransactRecordListForSell(String sellDate);

    /**
     * 获取卖出建议列表
     * 用于做空
     * @param sellDate
     * @return
     */
    List<Robot6StockTransactRecord> getShortRobotEtfTransactRecordListForSell(String sellDate);

    /**
     * 获取买入建议列表
     * 用于做空
     * @param buyDate
     * @return
     */
    List<Robot6StockTransactRecord> getShortRobotEtfTransactRecordListForBuy(String buyDate);

    /**
     * 获取卖建议列表
     * @param sellDate
     * @return
     */
    List<SellSuggestion> getSellSuggestionList_etf12YearHighestAverageProfitRate(String sellDate);

    /**
     * 获取买建议列表
     * @param buyDate
     * @return
     */
    List<BuySuggestion> getBuySuggestionList_etf12YearHighestAverageProfitRate(String buyDate);
}
