package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.StockMonth;
import cn.com.acca.ma.service.StockMonthService;
import java.util.List;

public class StockMonthServiceImpl extends BaseServiceImpl<StockMonthServiceImpl, StockMonth> implements
    StockMonthService {

    /**************************************************************************************************
     *
     * 									       更新周线级别，所有月份的股票数据
     *
     **************************************************************************************************/
    /**
     * 计算月线级别，所有股票的基础数据
     */
    @Override
    public void writeStockMonth() {
        logger.info("计算月线级别，所有股票的基础数据");

        stockMonthDao.writeStockMonth();
    }

    /**
     * 计算月线级别，所有股票的移动平均线
     */
    @Override
    public void writeStockMonthMA() {
        logger.info("计算月线级别，所有股票的移动平均线");

        stockMonthDao.writeStockMonthMA();
    }

    /**
     * 计算月线级别，所有股票的的KD
     */
    @Override
    public void writeStockMonthKD() {
        logger.info("计算月线级别，所有股票的KD");

        stockMonthDao.writeStockMonthKD();
    }

    /**
     * 计算月线级别，所有股票的MACD
     */
    @Override
    public void writeStockMonthMACD() {
        logger.info("计算月线级别，所有股票的MACD");

        stockMonthDao.writeStockMonthMACD();
    }

    /**************************************************************************************************
     *
     * 									       更新周线级别，某一月的股票数据
     *
     **************************************************************************************************/
    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的基础数据
     */
    @Override
    public void writeStockMonthByDate() {
        logger.info("根据开始时间和结束时间，计算某一个月的所有股票的基础数据");

        String beginDate = PropertiesUtil
            .getValue(STOCK_MONTH_PROPERTIES, "stockMonth.beginDate");
        String endDate = PropertiesUtil.getValue(STOCK_MONTH_PROPERTIES, "stockMonth.endDate");

        stockMonthDao.writeStockMonthByDate(beginDate, endDate);
    }

    /**
     * 根据每个月的开始时间，计算某一个月的所有股票的移动平均线
     */
    @Override
    public void writeStockMonthMAByDate() {
        logger.info("根据每个月的开始时间，计算某一个月的所有股票的移动平均线");

        String currentMonthBeginDate = PropertiesUtil
            .getValue(STOCK_MONTH_PROPERTIES, "stockMonth.ma.currentMonthBeginDate");
        String currentMonthEndDate = PropertiesUtil
            .getValue(STOCK_MONTH_PROPERTIES, "stockMonth.ma.currentMonthEndDate");

        stockMonthDao.writeStockMonthMAByDate(currentMonthBeginDate, currentMonthEndDate);
    }

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的kd
     */
    @Override
    public void writeStockMonthKDByDate() {
        logger.info("根据开始时间和结束时间，计算某一个月的所有股票的kd");

        String beginDate = PropertiesUtil
            .getValue(STOCK_MONTH_PROPERTIES, "stockMonth.kd.beginDate");
        String endDate = PropertiesUtil.getValue(STOCK_MONTH_PROPERTIES, "stockMonth.kd.endDate");

        stockMonthDao.writeStockMonthKDByDate(beginDate, endDate);
    }

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的MACD
     */
    @Override
    public void writeStockMonthMACDByDate() {
        logger.info("根据开始时间和结束时间，计算某一个月的所有股票的MACD");

        String beginDate = PropertiesUtil
            .getValue(STOCK_MONTH_PROPERTIES, "stockMonth.macd.beginDate");
        String endDate = PropertiesUtil.getValue(STOCK_MONTH_PROPERTIES, "stockMonth.macd.endDate");

        stockMonthDao.writeStockMonthMACDByDate(beginDate, endDate);
    }


    @Override
    public String listToString(List list) {
        return null;
    }


}
