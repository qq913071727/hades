package cn.com.acca.ma.service;

public interface ModelClosePriceMA5DeadCrossService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    void writeModelClosePriceMA5DeadCross();

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    void writeModelClosePriceMA5DeadCrossIncr();
}
