package cn.com.acca.ma.constant;

public class RobotName {

    public static final String LAN_JING_LING = "蓝精灵";

    public static final String GE_GE_WU = "格格巫";

    public static final String HEI_MAO_JING_ZHANG = "黑猫警长";

    public static final String YI_ZHI_ER = "一只耳";
}
