package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.ModelStockIndexHeiKinAshiDownUp;
import cn.com.acca.ma.service.ModelStockIndexHeiKinAshiDownUpService;

import java.util.List;

public class ModelStockIndexHeiKinAshiDownUpServiceImpl extends BaseServiceImpl<ModelStockIndexHeiKinAshiDownUpServiceImpl, ModelStockIndexHeiKinAshiDownUp> implements
        ModelStockIndexHeiKinAshiDownUpService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_index_close_price小于等于ha_index_open_price时卖出，ha_index_close_price大于ha_index_open_price时买入
     * 向表mdl_stock_index_h_k_a_down_up插入数据
     */
    @Override
    public void writeModelStockIndexHeiKinAshiDownUp() {
        modelStockIndexHeiKinAshiDownUpDao.writeModelStockIndexHeiKinAshiDownUp();
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
