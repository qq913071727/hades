package cn.com.acca.ma.pojo;

import cn.com.acca.ma.model.RobotStockTransactionRecord;
import lombok.Data;

import java.util.List;

/**
 * 机器人买卖建议，json格式
 */
@Data
public class RobotBuySellSuggestionByJson_successRate {

    /**
     * 做多卖出建议列表
     */
    private List<RobotStockTransactionRecord> bullSellSuggestionList;

    /**
     * 做多买入建议列表
     */
    private List<RobotStockTransactionRecord> bullBuySuggestionList;

    /**
     * 做空卖出建议列表
     */
    private List<RobotStockTransactionRecord> shortSellSuggestionList;

    /**
     * 做空买入建议列表
     */
    private List<RobotStockTransactionRecord> shortBuySuggestionList;


}
