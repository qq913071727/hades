package cn.com.acca.ma.service;

public interface Robot3StockTransactRecordService extends BaseService {

    /**
     * 执行某段时间内的股票买卖交易（周线级别布林带突破上轨或跌破下轨）
     */
    void doBuyAndSellByBeginDateAndEndDate_weekBoll();

    /**
     * 卖股票/买股票
     */
    void sellOrBuy();

    /**
     * 买股票/卖股票
     */
    void buyOrSell();

    /**
     * 打印买卖建议
     */
    void printBuySellSuggestion();
}
