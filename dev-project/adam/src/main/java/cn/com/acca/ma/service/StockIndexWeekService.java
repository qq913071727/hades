package cn.com.acca.ma.service;

public interface StockIndexWeekService extends BaseService {
	/******************************************** 更新每周的数据 ********************************************************/
	/**
	 * 根据日期，更新STOCK_INDEX_WEEK表的基础数据
	 */
	void writeStockIndexWeekByDate();
	
	/**
	 * 根据日期，更新STOCK_INDEX_WEEK表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekHeiKinAshiByDate();
	
	/******************************************** 更新每周的图片 ********************************************************/
	/**
	 * 创建StockIndexWeek对象的Hei Kin Ashi图片
	 * @param multithreading
	 */
	void createStockIndexWeekHeiKinAshiPicture(boolean multithreading);
	
	/******************************************** 更新全部的数据 ********************************************************/
	/**
	 * 更新STOCK_INDEX_WEEK表的基础数据
	 */
	void writeStockIndexWeek();
	
	/**
	 * 更新STOCK_INDEX_WEEKEND表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekHeiKinAshi();
}
