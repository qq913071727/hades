package cn.com.acca.ma.service;

public interface ModelStockIndexHeiKinAshiDownUpService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_index_close_price小于等于ha_index_open_price时卖出，ha_index_close_price大于ha_index_open_price时买入
     * 向表mdl_stock_index_h_k_a_down_up插入数据
     */
    void writeModelStockIndexHeiKinAshiDownUp();
}
