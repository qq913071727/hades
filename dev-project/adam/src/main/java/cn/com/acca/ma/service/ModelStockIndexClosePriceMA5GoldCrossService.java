package cn.com.acca.ma.service;

public interface ModelStockIndexClosePriceMA5GoldCrossService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据
     */
    void writeModelStockIndexClosePriceMA5GoldCross();
}
