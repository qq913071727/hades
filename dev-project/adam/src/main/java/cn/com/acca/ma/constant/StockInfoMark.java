package cn.com.acca.ma.constant;

/**
 * stock_info表的mark字段
 */
public class StockInfoMark {

    /**
     * 融券
     */
    public static final String SECURITIES_LENDING = "R";
}
