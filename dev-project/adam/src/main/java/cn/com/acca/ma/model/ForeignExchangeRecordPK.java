package cn.com.acca.ma.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Id;

public class ForeignExchangeRecordPK {
	@Id
	private String currency;
	
	@Id
	private Timestamp dateTime;
	
	@Id
	private BigDecimal kLevel;
	
	public ForeignExchangeRecordPK(){
		
	}

	public ForeignExchangeRecordPK(String currency, Timestamp dateTime,BigDecimal kLevel) {
		super();
		this.currency = currency;
		this.dateTime = dateTime;
		this.kLevel = kLevel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((dateTime == null) ? 0 : dateTime.hashCode());
		result = prime * result + ((kLevel == null) ? 0 : kLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ForeignExchangeRecordPK other = (ForeignExchangeRecordPK) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (dateTime == null) {
			if (other.dateTime != null)
				return false;
		} else if (!dateTime.equals(other.dateTime))
			return false;
		if (kLevel == null) {
			if (other.kLevel != null)
				return false;
		} else if (!kLevel.equals(other.kLevel))
			return false;
		return true;
	}

}
