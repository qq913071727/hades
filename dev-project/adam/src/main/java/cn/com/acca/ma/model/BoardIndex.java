package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 板块每日交易
 */
@Entity
@Table(name="BOARD_INDEX")
public class BoardIndex implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_")
	private Integer id;

	/**
	 * 表示某一个板块的某一个日期
	 */
	@Column(name="DATE_")
	private Date date;

	/**
	 * 表示某一个板块的ID
	 */
	@Column(name="BOARD_ID")
	private BigDecimal boardId;

	/**
	 * 表示某一个板块的某一个日期时，股票的数量
	 */
	@Column(name="STOCK_NUMBER")
	private BigDecimal stockNumber;

	/**
	 * 表示某一个板块指数，在某一天，收盘价比前一天的收盘价是涨是跌。1：涨；0：平；-1：跌
	 */
	@Column(name="UP_DOWN")
	private BigDecimal upDown;

	/**
	 * 表示某一个板块指数，在某一天，上涨家数除以下跌家数
	 */
	@Column(name="UP_DOWN_RATE")
	private BigDecimal upDownRate;

	/**
	 * 表示某一个板块，在某一天，上涨了多少家
	 */
	@Column(name="UP_AMOUNT")
	private BigDecimal upAmount;

	/**
	 * 表示某一个板块，在某一天，下跌了多少家
	 */
	@Column(name="DOWN_AMOUNT")
	private BigDecimal downAmount;

	/**
	 * 表示某一个板块，在某一天的收盘价。版块内的股票的收盘价之和除以股票家数
	 */
	@Column(name="CLOSE_PRICE")
	private BigDecimal closePrice;

	/**
	 * 表示某一个板块，在某一天的收盘价。版块内的股票的开盘价之和除以股票家数
	 */
	@Column(name="OPEN_PRICE")
	private BigDecimal openPrice;

	/**
	 * 表示某一个板块，在某一天的收盘价。版块内的股票的最高价之和除以股票家数
	 */
	@Column(name="HIGHEST_PRICE")
	private BigDecimal highestPrice;

	/**
	 * 表示某一个板块，在某一天的收盘价。版块内的股票的最低价之和除以股票家数
	 */
	@Column(name="LOWEST_PRICE")
	private BigDecimal lowestPrice;

	/**
	 * 表示某一个板块，在某一天的成交量。版块内的股票的成交量之和除以股票家数
	 */
	@Column(name="AMOUNT")
	private BigDecimal amount;

	/**
	 * 表示某一个板块，在某一天的收盘价与5日前的收盘价的涨跌幅度，用百分比表示
	 */
	@Column(name="FIVE_DAY_RATE")
	private BigDecimal fiveDayRate;

	/**
	 * 表示某一个板块，在某一天的收盘价与10日前的收盘价的涨跌幅度，用百分比表示
	 */
	@Column(name="TEN_DAY_RATE")
	private BigDecimal tenDayRate;

	/**
	 * 表示某一个版块，当天涨跌的百分比。（当天收盘价-前一天收盘价）/前一天收盘价
	 */
	@Column(name="UP_DOWN_PERCENTAGE")
	private BigDecimal upDownPercentage;

	/**
	 * 表示某一个版块，在某一天内涨跌的排名
	 */
	@Column(name="UP_DOWN_RANK")
	private Integer upDownRank;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getBoardId() {
		return boardId;
	}

	public void setBoardId(BigDecimal boardId) {
		this.boardId = boardId;
	}

	public BigDecimal getUpAmount() {
		return upAmount;
	}

	public void setUpAmount(BigDecimal upAmount) {
		this.upAmount = upAmount;
	}

	public BigDecimal getDownAmount() {
		return downAmount;
	}

	public void setDownAmount(BigDecimal downAmount) {
		this.downAmount = downAmount;
	}

	public BigDecimal getUpDownRate() {
		return upDownRate;
	}

	public void setUpDownRate(BigDecimal upDownRate) {
		this.upDownRate = upDownRate;
	}

	public BigDecimal getUpDown() {
		return upDown;
	}

	public void setUpDown(BigDecimal upDown) {
		this.upDown = upDown;
	}

	public BigDecimal getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(BigDecimal stockNumber) {
		this.stockNumber = stockNumber;
	}

	public BigDecimal getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}

	public BigDecimal getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(BigDecimal openPrice) {
		this.openPrice = openPrice;
	}

	public BigDecimal getHighestPrice() {
		return highestPrice;
	}

	public void setHighestPrice(BigDecimal highestPrice) {
		this.highestPrice = highestPrice;
	}

	public BigDecimal getLowestPrice() {
		return lowestPrice;
	}

	public void setLowestPrice(BigDecimal lowestPrice) {
		this.lowestPrice = lowestPrice;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFiveDayRate() {
		return fiveDayRate;
	}

	public void setFiveDayRate(BigDecimal fiveDayRate) {
		this.fiveDayRate = fiveDayRate;
	}

	public BigDecimal getTenDayRate() {
		return tenDayRate;
	}

	public void setTenDayRate(BigDecimal tenDayRate) {
		this.tenDayRate = tenDayRate;
	}

	public BigDecimal getUpDownPercentage() {
		return upDownPercentage;
	}

	public void setUpDownPercentage(BigDecimal upDownPercentage) {
		this.upDownPercentage = upDownPercentage;
	}

	public Integer getUpDownRank() {
		return upDownRank;
	}

	public void setUpDownRank(Integer upDownRank) {
		this.upDownRank = upDownRank;
	}
}
