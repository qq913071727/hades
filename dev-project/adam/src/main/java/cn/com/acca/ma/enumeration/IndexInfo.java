package cn.com.acca.ma.enumeration;

public enum IndexInfo {
    SH_INDEX("上证指数", "0000001", "000001"),
    SZ_INDEX("深证成指", "1399001", "399001"),
    ZXB_INDEX("中小板指数", "1399005", "399005"),
    CYB_INDEX("创业板指数", "1399006", "399006");

    /**
     * 指数名称
     */
    private String name;

    /**
     * url参数
     */
    private String urlParam;

    /**
     * 代码
     */
    private String code;

    IndexInfo(String name, String urlParam, String code) {
        this.name = name;
        this.urlParam = urlParam;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlParam() {
        return urlParam;
    }

    public void setUrlParam(String urlParam) {
        this.urlParam = urlParam;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
