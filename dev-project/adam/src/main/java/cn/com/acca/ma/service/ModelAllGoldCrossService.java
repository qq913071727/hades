package cn.com.acca.ma.service;

public interface ModelAllGoldCrossService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_ALL_GOLD_CROSS中插入数据
     */
    void writeModelAllGoldCross();
}
