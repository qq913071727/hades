package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.Model;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;

import java.util.List;

public class ModelDaoImpl extends BaseDaoImpl<ModelDaoImpl> implements ModelDao {
	/**
	 * 获取所有的Model对象
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Model> findAll(){
		logger.info("获取所有的Model对象");
		
		String hql = "select t from Model t";
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql);
		List<Model> list = query.list();
		session.getTransaction().commit();
		session.close();
		
		return list;
	}
	
	/**
	 * 保存Model对象
	 * @param model
	 */
	public void saveModel(Model model){
		logger.info("保存Model对象");
		
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		session.save(model);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * 按照create_time列降序排列
	 * @return
	 */
	@Override
    public List<Model> orderByCreateTime() {
        logger.info("按照create_time列降序排列");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Model.class)
                .addOrder(Order.desc("createTime"))
                .setFirstResult(0)
                .setMaxResults(1);
        List<Model> modelList = criteria.list();
        session.getTransaction().commit();
        session.close();

        return modelList;
    }

    /**
     * 按照id列降序排列
     * @return
     */
    @Override
    public List<Model> orderById() {
        logger.info("按照id列降序排列");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Model.class)
                .addOrder(Order.desc("id"))
                .setFirstResult(0)
                .setMaxResults(1);
        List<Model> modelList = criteria.list();
        session.getTransaction().commit();
        session.close();

        return modelList;
    }

    /**
     * 删除TEMP_开头的四个表
     */
    @Override
    public void dropTableBeginWithTemp() {
        logger.info("删除TEMP_开头的四个表");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("drop table temp_robot_account");
        query.executeUpdate();
        query = session.createSQLQuery("drop table temp_robot_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("drop table temp_transaction_record");
        query.executeUpdate();
        query = session.createSQLQuery("drop table temp_robot_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将ROBOT_开头的四个表的数据导入到TEMP_开头的四个表中
     */
    @Override
    public void importIntoTableBeginWithTempFromTableBeginWithRobot() {
        logger.info("将robot_开头的四个表的数据导入到TEMP_开头的四个表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("create table temp_robot_account as select * from robot_account");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_stock_filter as select * from robot_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_transaction_record as select * from robot_stock_transaction_record");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_account_log as select * from robot_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将ROBOT2_开头的四个表的数据导入到TEMP_开头的四个表中
     */
    @Override
    public void importIntoTableBeginWithTempFromTableBeginWithRobot2() {
        logger.info("将robot2_开头的四个表的数据导入到TEMP_开头的四个表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("create table temp_robot_account as select * from robot2_account");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_stock_filter as select * from robot2_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_transaction_record as select * from robot2_stock_transact_record");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_account_log as select * from robot2_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将ROBOT3_开头的四个表的数据导入到TEMP_开头的四个表中
     */
    @Override
    public void importIntoTableBeginWithTempFromTableBeginWithRobot3() {
        logger.info("将robot3_开头的四个表的数据导入到TEMP_开头的四个表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("create table temp_robot_account as select * from robot3_account");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_stock_filter as select * from robot3_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_transaction_record as select * from robot3_stock_transact_record");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_account_log as select * from robot3_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将ROBOT4_开头的四个表的数据导入到TEMP_开头的四个表中
     */
    @Override
    public void importIntoTableBeginWithTempFromTableBeginWithRobot4() {
        logger.info("将robot4_开头的四个表的数据导入到TEMP_开头的四个表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("create table temp_robot_account as select * from robot4_account");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_stock_filter as select * from robot4_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_transaction_record as select * from robot4_stock_transact_record");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_account_log as select * from robot4_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将ROBOT5_开头的四个表的数据导入到TEMP_开头的四个表中
     */
    @Override
    public void importIntoTableBeginWithTempFromTableBeginWithRobot5() {
        logger.info("将robot5_开头的四个表的数据导入到TEMP_开头的四个表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("create table temp_robot_account as select * from robot5_account");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_stock_filter as select * from robot5_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_transaction_record as select * from robot5_stock_transact_record");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_account_log as select * from robot5_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将ROBOT6_开头的四个表的数据导入到TEMP_开头的四个表中
     */
    @Override
    public void importIntoTableBeginWithTempFromTableBeginWithRobot6() {
        logger.info("将robot6_开头的四个表的数据导入到TEMP_开头的四个表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("create table temp_robot_account as select * from robot6_account");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_stock_filter as select * from robot6_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_transaction_record as select * from robot6_stock_transact_record");
        query.executeUpdate();
        query = session.createSQLQuery("create table temp_robot_account_log as select * from robot6_account_log");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 将TEMP_开头的表中的数据导入到HIS_开头的表中
     * @param id
     */
    @Override
    public void insertIntoTableBeginWithHisFromTableBeginWithTemp(Integer id) {
        logger.info("将TEMP_开头的表中的数据导入到HIS_开头的表中");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("insert into his_robot_account(robot_name, hold_stock_number, stock_assets, capital_assets, total_assets, model_id) " +
                "select t.robot_name, t.hold_stock_number, t.stock_assets, t.capital_assets, t.total_assets, " + id + " from temp_robot_account t");
        query.executeUpdate();
        query = session.createSQLQuery("insert into his_robot_account_log(date_, robot_name, hold_stock_number, stock_assets, capital_assets, total_assets, model_id) " +
                "select t.date_, t.robot_name, t.hold_stock_number, t.stock_assets, t.capital_assets, t.total_assets, " + id + " from temp_robot_account_log t");
        query.executeUpdate();
        query = session.createSQLQuery("insert into his_robot_stock_filter(stock_code, model_id) select t.stock_code, " + id + " from temp_robot_stock_filter t");
        query.executeUpdate();
        query = session.createSQLQuery("insert into his_robot_transaction_record(robot_name, stock_code, buy_date, buy_price, buy_amount, sell_date, sell_price, sell_amount, filter_type, direction, profit_and_loss, profit_and_loss_rate, model_id) " +
                "select t.robot_name, t.stock_code, t.buy_date, t.buy_price, t.buy_amount, t.sell_date, t.sell_price, t.sell_amount, t.filter_type, t.direction, t.profit_and_loss, t.profit_and_loss_rate, " + id + " from temp_transaction_record t");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 删除或重置ROBOT_开头的表中的记录
     */
    @Override
    public void truncateOrResetTableBeginRobot(Integer initAssets) {
        logger.info("删除或重置ROBOT_开头的表中的记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("truncate table ROBOT_STOCK_TRANSACTION_RECORD");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot_account_log");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("update robot_account " +
                "set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=" + initAssets + ", TOTAL_ASSETS=" + initAssets);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 删除或重置ROBOT2_开头的表中的记录
     */
    @Override
    public void truncateOrResetTableBeginRobot2(Integer initAssets) {
        logger.info("删除或重置ROBOT2_开头的表中的记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("truncate table ROBOT2_STOCK_TRANSACT_RECORD");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot2_account_log");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot2_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("update robot2_account " +
                "set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=" + initAssets + ", TOTAL_ASSETS=" + initAssets);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 删除或重置ROBOT3_开头的表中的记录
     */
    @Override
    public void truncateOrResetTableBeginRobot3(Integer initAssets) {
        logger.info("删除或重置ROBOT3_开头的表中的记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("truncate table ROBOT3_STOCK_TRANSACT_RECORD");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot3_account_log");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot3_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("update robot3_account " +
                "set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=" + initAssets + ", TOTAL_ASSETS=" + initAssets);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 删除或重置ROBOT4_开头的表中的记录
     */
    @Override
    public void truncateOrResetTableBeginRobot4(Integer initAssets) {
        logger.info("删除或重置ROBOT4_开头的表中的记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("truncate table ROBOT4_STOCK_TRANSACT_RECORD");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot4_account_log");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot4_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("update robot4_account " +
                "set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=" + initAssets + ", TOTAL_ASSETS=" + initAssets);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 删除或重置ROBOT5_开头的表中的记录
     */
    @Override
    public void truncateOrResetTableBeginRobot5(Integer initAssets) {
        logger.info("删除或重置ROBOT5_开头的表中的记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("truncate table ROBOT5_STOCK_TRANSACT_RECORD");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot5_account_log");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot5_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("update robot5_account " +
                "set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=" + initAssets + ", TOTAL_ASSETS=" + initAssets);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 删除或重置ROBOT6_开头的表中的记录
     */
    @Override
    public void truncateOrResetTableBeginRobot6(Integer initAssets) {
        logger.info("删除或重置ROBOT6_开头的表中的记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("truncate table ROBOT6_STOCK_TRANSACT_RECORD");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot6_account_log");
        query.executeUpdate();
        query = session.createSQLQuery("truncate table robot6_stock_filter");
        query.executeUpdate();
        query = session.createSQLQuery("update robot6_account " +
                "set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=" + initAssets + ", TOTAL_ASSETS=" + initAssets);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
