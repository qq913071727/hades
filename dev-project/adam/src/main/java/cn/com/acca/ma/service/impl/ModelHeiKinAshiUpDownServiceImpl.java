package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelHeiKinAshiUpDown;
import cn.com.acca.ma.model.ModelMACDGoldCross;
import cn.com.acca.ma.service.ModelHeiKinAshiUpDownService;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

public class ModelHeiKinAshiUpDownServiceImpl extends BaseServiceImpl<ModelHeiKinAshiUpDownServiceImpl, ModelHeiKinAshiUpDown> implements
    ModelHeiKinAshiUpDownService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线ha_close_price大于ha_open_price时买入，ha_close_price小于等于ha_open_price时卖出，
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN插入数据
     * @param multithreading
     */
    @Override
    public void writeModelHeiKinAshiUpDownIncr(boolean multithreading) {
        String endDate = PropertiesUtil
            .getValue(MODEL_HEI_KIN_ASHI_UP_DOWN_PROPERTIES, "hei.kin.ashi.up.down.end_date");
        modelHeiKinAshiUpDownDao.writeModelHeiKinAshiUpDownIncr(multithreading, endDate);
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_close_price大于ha_open_price时买入，ha_close_price小于等于ha_open_price时卖出，
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN插入数据
     */
    @Override
    public void writeModelHeiKinAshiUpDown() {
        modelHeiKinAshiUpDownDao.writeModelHeiKinAshiUpDown();
    }
    @Override
    public String listToString(List list) {
        return null;
    }
}
