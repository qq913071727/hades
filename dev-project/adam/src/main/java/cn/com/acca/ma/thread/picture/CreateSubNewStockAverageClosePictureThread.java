package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.ModelSubNewStockService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建次新股收盘价平均值图
 */
public class CreateSubNewStockAverageClosePictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateSubNewStockAverageClosePictureThread.class);

    private ModelSubNewStockService modelSubNewStockService;

    public CreateSubNewStockAverageClosePictureThread() {
    }

    public CreateSubNewStockAverageClosePictureThread(ModelSubNewStockService modelSubNewStockService) {
        this.modelSubNewStockService = modelSubNewStockService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始生成创建次新股收盘价平均值图，调用的方法为【createSubNewStockAverageClosePicture】");

        modelSubNewStockService.createSubNewStockAverageClosePicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
