package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.enumeration.ExchangeBoardInfo;
import cn.com.acca.ma.model.StockTransactionData;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.List;

/**
 * 生产者线程，使用163接口，用于从网络上采集股票数据，并存储数据
 */
public class CollectStockTransactionDataThread_sohu extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CollectStockTransactionDataThread_sohu.class);

    private StockTransactionDataService stockTransactionDataService;

    /**
     * 股票代码的前缀
     */
    private String stockInfoCodePrefix;

    /**
     * 股票代码
     */
    private String code;

    /**
     * 开始日期
     */
    private String beginDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 前一日收盘价
     */
    private BigDecimal lastClosePrice;

    public CollectStockTransactionDataThread_sohu() {

    }

    public CollectStockTransactionDataThread_sohu(StockTransactionDataService stockTransactionDataService,
                                                  String stockInfoCodePrefix, String code, String beginDate,
                                                  String endDate, BigDecimal lastClosePrice) {
        this.stockTransactionDataService = stockTransactionDataService;
        this.stockInfoCodePrefix = stockInfoCodePrefix;
        this.code = code;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.lastClosePrice = lastClosePrice;
    }

    @Override
    public void run() {
        logger.debug("启动生产者线程。前缀为【" + stockInfoCodePrefix + "】，股票代码为【" + code + "】，"
                + "开始时间为【" + beginDate + "】，结束时间为【" + endDate + "】");

        Socket socket = null;
        OutputStream outputStream = null;
        try {
            this.handle();
        } finally {
            if (null != socket) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 从网络上获取数据，并存储到数据库中
     *
     * @return
     */
    private void handle() {
        String url = DATA_SOURCE_URL_PREFIX_SOHU + stockInfoCodePrefix
                + code + "&start=" + beginDate + "&end=" + endDate + "&stat=1&order=D&period=d";

        // 从网络上获取股票数据
        List<StockTransactionData> stockTransactionDataList = stockTransactionDataService.readStockTransactionDataList_sohu(url, code, lastClosePrice);
        // 更新stock_transaction_data表中的数据
        stockTransactionDataService.writeStockTransactionDataList(stockTransactionDataList);

        logger.debug(url);

        AbstractThread.stockTransactionDataCollectFinishAmount++;
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
