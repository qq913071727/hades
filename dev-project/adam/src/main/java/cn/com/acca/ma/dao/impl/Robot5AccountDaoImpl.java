package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Robot5AccountDao;

public class Robot5AccountDaoImpl extends BaseDaoImpl<Robot5AccountDao> implements
    Robot5AccountDao {

    public Robot5AccountDaoImpl() {
        super();
    }

    /**************************************** 买股票 ********************************************/
    /**
     * 根据当日持有股票的收盘价，在卖股票/买股票之前，更新robot5_account表
     * @param date
     */
    @Override
    public void updateRobotAccountBeforeSellOrBuy(String date){
        logger.info("更新robot5_account表的stock_assets、total_assets字段，"
            + "日期为【" + date + "】");

        String sql = "{call PKG_ROBOT5.update_robot_account_b_s_b('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 根据当日卖出/买入股票的收盘价，在卖股票/买股票之后，更新robot5_account表
     * @param date
     */
    @Override
    public void updateRobotAccountAfterSellOrBuy(String date) {
        logger.info("更新robot5_account表的hold_stock_number、stock_assets、capital_assets字段，"
            + "日期为【" + date + "】");

        String sql = "{call PKG_ROBOT5.update_robot_account_after_s_b('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 返回记录数
     * @return
     */
    @Override
    public Integer count() {
        logger.info("返回记录数");

        String sql = "select count(*) from robot5_account";

        return doSQLQueryInTransaction(sql, null);
    }

    /**
     * 列hold_stock_number求和
     * @return
     */
    @Override
    public Integer sumHoldStockNumber() {
        logger.info("列hold_stock_number求和");

        String sql = "select sum(hold_stock_number) from robot5_account";

        return doSQLQueryInTransaction(sql, null);
    }

    /**************************************** 买股票/卖股票  ********************************************/
    /**
     * 根据当日买入股票/卖出股票的收盘价，在买股票/卖股票之后，更新robot5_account表
     * @param date
     */
    @Override
    public void updateRobotAccountAfterBuyOrSell(String date) {
        logger.info("更新robot5_account表的hold_stock_number、stock_assets、capital_assets字段，"
            + "日期为【" + date + "】");

        String sql = "{call PKG_ROBOT5.update_robot_account_after_b_s('" + date + "')}";

        doSQLInTransaction(sql);
    }
}
