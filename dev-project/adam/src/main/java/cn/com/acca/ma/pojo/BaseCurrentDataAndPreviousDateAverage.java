package cn.com.acca.ma.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class BaseCurrentDataAndPreviousDateAverage implements Comparable {

    /**
     * 当前日期
     */
    @Id
    private String currentDate;

    /**
     * 金叉或死叉。1表示金叉，2表示死叉
     */
    private Integer goldCrossOrDeadCross;

    /**
     * 成功率或百分比
     */
    private Double successRateOrPercentage;

    /**
     * 仓位
     */
    private Integer shippingSpace;

    /**
     * 过滤方法编号
     */
    private Integer filterMethod;

    /**
     * 降序排列
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        BaseCurrentDataAndPreviousDateAverage currentDateAndPreviousDateAverageDifDea = (BaseCurrentDataAndPreviousDateAverage)o;
        return currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage());
//        BigDecimal result = currentDateAndPreviousDateAverageDifDea.getCurrentAverageDif().add(currentDateAndPreviousDateAverageDifDea.getCurrentAverageDea()).divide(new BigDecimal(2))
//                .subtract(this.getCurrentAverageDif().add(this.getCurrentAverageDea()).divide(new BigDecimal(2)));
//        if (currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage()) > 0){
//            return 1;
//        }
//        if (result.doubleValue() < 0){
//            return  -1;
//        }
//        if (result.doubleValue() == 0){
//            return 0;
//        }
//        return 0;
    }
}
