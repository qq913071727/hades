package cn.com.acca.ma.thread;

import cn.com.acca.ma.model.StockTransactionData;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public abstract class AbstractThread {

    /********************************** 静态属性、常量 *********************************/
    /**
     * 股票交易数据采集完成数量。没完成一只股票加一。如果是一只股票多日的数据，则也算做1。
     */
    public static int stockTransactionDataCollectFinishAmount = 0;

    /**
     * ETF交易数据采集完成数量。没完成一只股票加一。如果是一只ETF多日的数据，则也算做1。
     */
    public static int etfTransactionDataCollectFinishAmount = 0;

    /**
     * 从网络上获取股票数据的api接口，前半部分
     */
    protected static final String DATA_SOURCE_URL_PREFIX_163="http://quotes.money.163.com/service/chddata.html?code=";

    /**
     * sohu股票数据的api接口，前半部分
     */
    protected static final String DATA_SOURCE_URL_PREFIX_SOHU="https://q.stock.sohu.com/hisHq?code=";

    /**
     * 从网络上获取股票数据的api接口，后半部分
     */
    protected static final String DATA_SOURCE_URL_SUFFIX_163="&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP";

    /**
     * 从网络上获取股票数据的api接口，前半部分
     */
    protected static final String DATA_SOURCE_URL_PREFIX_126="http://img1.money.126.net/data/hs/kline/day/history/{year}/{prefix}{code}.json";

    /**
     * 获取程序当前路径，即根目录
     */
    protected static final String DIR=System.getProperty("user.dir");

    /**
     * bull-range-short-order.properties文件的路径
     */
    public static final String BULL_RANGE_SHORT_ORDER_PROPERTIES = DIR + "/src/main/resources/word/bull-range-short-order.xml";

    /********************************** 抽象方法 *********************************/
    /**
     * 创建子报告
     */
    public abstract void generateSubReport();

    /********************************** 具体方法 *********************************/
    /**
     * 根据code，从StockTransactionData列表中删除一个元素
     * @param stockTransactionDataList
     * @param code
     * @return
     */
    public List<StockTransactionData> removeByCode(List<StockTransactionData> stockTransactionDataList, String code){
        synchronized (stockTransactionDataList){
            for (StockTransactionData stockTransactionData : stockTransactionDataList){
                if (stockTransactionData.getCode().equals(code)){
                    stockTransactionDataList.remove(stockTransactionData);
                    break;
                }
            }
        }
        return stockTransactionDataList;
    }

    /**
     * 将List<XWPFRun>类型的对象拼接为String类型的对象
     * @param xwpfRunList
     * @return
     */
    protected String xwpfRunListToString(List<XWPFRun> xwpfRunList){
        if (null == xwpfRunList || xwpfRunList.size() == 0){
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        List<XWPFRun> newXwpfRunList = new ArrayList<>(xwpfRunList);
        for (XWPFRun xwpfRun : newXwpfRunList){
            stringBuffer.append(xwpfRun.text());
        }
        return stringBuffer.toString();
    }

    /**
     * 用XWPFRun类型的对象替换List<XWPFRun>类型对象的第一个元素，其余元素为null
     * @param xwpfRunList
     * @param xwpfRun
     * @return
     */
    protected List<XWPFRun> newXWPFRunList(List<XWPFRun> xwpfRunList, XWPFRun xwpfRun){
        if (null == xwpfRunList || xwpfRunList.size() == 0){
            return null;
        }
        List<XWPFRun> newXwpfRunList = new ArrayList<>(xwpfRunList);
        XWPFRun firstXwpfRun = newXwpfRunList.get(0);
        firstXwpfRun = xwpfRun;
        for (int i=1; i<newXwpfRunList.size(); i++){
            XWPFRun tempXwpfRun = newXwpfRunList.get(i);
            tempXwpfRun = null;
        }
        return newXwpfRunList;
    }
}
