package cn.com.acca.ma.xmlmapper;

import java.util.List;

import cn.com.acca.ma.model.ForeignExchangeRecord;

public interface ForeignExchangeRecordMapper {
	
	/**
	 * 向FOREIGN_EXCHANGE_RECORD表中插入数据，用于从csv文件插入数据
	 * @param foreignExchangeRecord
	 */
	void insertForeignExchangeRecord(ForeignExchangeRecord foreignExchangeRecord);
	
	/**
	 * 判断表FOREIGN_EXCHANGE_RECORD中是否有相同的记录
	 * @param foreignExchangeRecord
	 * @return
	 */
	boolean containForeignExchangeRecord(ForeignExchangeRecord foreignExchangeRecord);
	
	/**
	 * 计算5日简单移动平均线
	 */
	void calculateFiveMovingAverage();
	
	/**
	 * 计算10日简单移动平均线
	 */
	void calculateTenMovingAverage();
	
	/**
	 * 计算20日简单移动平均线
	 */
	void calculateTwentyMovingAverage();
	
	/**
	 * 计算60日简单移动平均线
	 */
	void calculateSixtyMovingAverage();
	
	/**
	 * 计算120日简单移动平均线
	 */
	void calculateOneHundredTwentyMovingAverage();
	
	/**
	 * 计算250日简单移动平均线
	 */
	void calculateTwoHundredFiftyMovingAverage();
	
	/**
	 * 根据开始日期beginDate和结束日期endDate，获取DATETIME字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<ForeignExchangeRecord> getDateByCondition(String beginDate,String endDate);
	
	/**
	 * 根据日期date获取所有ForeignExchangeRecord对象
	 * @param date
	 * @return
	 */
	List<ForeignExchangeRecord> getForeignExchangeRecordByDate(String date);
	
	/**
	 * 向FOREIGN_EXCHANGE_RECORD表中插入数据，用于Json文件恢复数据
	 * @param foreignExchangeRecord
	 */
	void restoreForeignExchangeRecord(ForeignExchangeRecord foreignExchangeRecord);
	
}
