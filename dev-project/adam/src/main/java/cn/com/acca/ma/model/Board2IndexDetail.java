package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 从网上爬到的板块数据（每一只股票），每个交易日都更新
 */
@Data
@Entity
@Table(name="BOARD2_INDEX_DETAIL")
public class Board2IndexDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
    private Integer id;

    /**
     * 股票名称
     */
    @Column(name="STOCK_NAME")
    private String stockName;

    /**
     * 股票代码
     */
    @Column(name="STOCK_CODE")
    private String stockCode;

    /**
     * 最新价
     */
    @Column(name="LATEST_CLOSE_PRICE")
    private BigDecimal latestClosePrice;

    /**
     * 涨跌幅
     */
    @Column(name="CHANGE_RANGE")
    private BigDecimal changeRange;

    /**
     * 关联原因
     */
    @Column(name="CORRELATION_CAUSE")
    private String correlationCause;

    /**
     * 所属其他概念
     */
    @Column(name="BELONG_OTHER_CONCEPT")
    private String belongOtherConcept;

    /**
     * 咨询次数
     */
    @Column(name="CONSULTATION_NUMBER")
    private Integer consultationNumber;

    /**
     * 交易日期，也是创建时间
     */
    @Column(name="TRANSACTION_DATE")
    private Date transactionDate;

    /**
     * board2_index表的id
     */
    @Column(name="board2_index_id")
    private Integer board2IndexId;
}
