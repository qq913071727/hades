package cn.com.acca.ma.service;

public interface BaseService {
	/**
	 * 创建MACD Gold Cross图表时，用于调整平均收盘价
	 * @param original
	 * @param max
	 * @param min
	 * @return
	 */
	double averageCloseAfterAdjust(double original, double max, double min);
	
	/**
	 * 创建MACD图表时，用于调整平均收盘价
	 * @param maxClose
	 * @param minClose
	 * @param maxDifDea
	 * @param minDifDea
	 * @param currentClose
	 * @return
	 */
	double adjustData(double maxClose, double minClose, double maxDifDea, double minDifDea, double currentClose);

	/**
	 * 在控制台打印字符串，并将字符串收集到StringBuffer中
	 * @param stringBuffer
	 * @param message
	 */
	void printlnAndCollectString(StringBuffer stringBuffer, String message);
}
