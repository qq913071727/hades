package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 更新stock_transaction_data表中某一日的KD指标
 */
public class WriteKDByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteKDByDateThread.class);

    private StockTransactionDataService stockTransactionDataService;

    public WriteKDByDateThread() {
    }

    public WriteKDByDateThread(StockTransactionDataService stockTransactionDataService) {
        this.stockTransactionDataService = stockTransactionDataService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始更新stock_transaction_data表中某一日的KD指标，"
            + "调用的方法为【writeKDByDate】");

        stockTransactionDataService.writeKDByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
