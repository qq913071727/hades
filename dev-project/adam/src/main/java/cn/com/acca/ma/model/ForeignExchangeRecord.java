package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="FOREIGN_EXCHANGE_RECORD")
public class ForeignExchangeRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CURRENCY")
	private String currency;
	
	@Id
	@Column(name="DATETIME")
	private Timestamp dateTime;
	
	@Id
	@Column(name="KLEVEL")
	private  Integer kLevel;
	
	@Column(name="OPEN")
	private BigDecimal open;
	
	@Column(name="HIGH")
	private BigDecimal high;
	
	@Column(name="LOW")
	private BigDecimal low;
	
	@Column(name="CLOSE")
	private BigDecimal close;
	
	@Column(name="VOLUME")
	private BigDecimal volume;
	
	@Column(name="FIVE")
	private BigDecimal five;
	
	@Column(name="TEN")
	private BigDecimal ten;
	
	@Column(name="TWENTY")
	private BigDecimal twenty;
	
	@Column(name="SIXTY")
	private BigDecimal sixty;
	
	@Column(name="ONE_HUNDRED_TWENTY")
	private BigDecimal oneHundredTwenty;
	
	@Column(name="TWO_HUNDRED_FIFTY")
	private BigDecimal twoHundredFifty;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public Integer getkLevel() {
		return kLevel;
	}

	public void setkLevel(Integer kLevel) {
		this.kLevel = kLevel;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public BigDecimal getClose() {
		return close;
	}

	public void setClose(BigDecimal close) {
		this.close = close;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getFive() {
		return five;
	}

	public void setFive(BigDecimal five) {
		this.five = five;
	}

	public BigDecimal getTen() {
		return ten;
	}

	public void setTen(BigDecimal ten) {
		this.ten = ten;
	}

	public BigDecimal getTwenty() {
		return twenty;
	}

	public void setTwenty(BigDecimal twenty) {
		this.twenty = twenty;
	}

	public BigDecimal getSixty() {
		return sixty;
	}

	public void setSixty(BigDecimal sixty) {
		this.sixty = sixty;
	}

	public BigDecimal getOneHundredTwenty() {
		return oneHundredTwenty;
	}

	public void setOneHundredTwenty(BigDecimal oneHundredTwenty) {
		this.oneHundredTwenty = oneHundredTwenty;
	}

	public BigDecimal getTwoHundredFifty() {
		return twoHundredFifty;
	}

	public void setTwoHundredFifty(BigDecimal twoHundredFifty) {
		this.twoHundredFifty = twoHundredFifty;
	}
	
	
}
