package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 最大值和最小值
 */
@Data
public class MaxAndMin {

    /**
     * 最大值
     */
    private BigDecimal max;

    /**
     * 最小值
     */
    private BigDecimal min;
}
