package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.HistoryRobotAccountLogDao;
import cn.com.acca.ma.model.HistoryRobotAccountLog;
import cn.com.acca.ma.model.RealStockTransactionRecord;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryRobotAccountLogDaoImpl extends BaseDaoImpl<HistoryRobotAccountLog> implements
        HistoryRobotAccountLogDao {

    public HistoryRobotAccountLogDaoImpl() {
        super();
    }

    /**
     * 根据开始日期和结束日期、账号名、模型id查找HistoryRobotAccountLog类型对象列表，并按照date的升序排列
     * @param beginDate
     * @param endDate
     * @param robotName
     * @param modelId
     * @param initAssets
     * @return
     */
    @Override
    public List<HistoryRobotAccountLog> findByDateBetweenAndRobotNameAndModelIdOrderByDateAsc(String beginDate, String endDate, String robotName, Integer modelId, Integer initAssets) {
        logger.info("根据开始日期【" + beginDate + "】和结束日期【" + endDate + "】、" +
                "账号名【" + robotName + "】、模型id【" + modelId + "】" +
                "查找HistoryRobotAccountLog类型对象列表，并按照date的升序排列");

        String sql = "select t.id_ id, t.date_ date_, t.robot_name robotName, " +
                    "t.hold_stock_number holdStockNumber, t.stock_assets stockAssets, " +
                    "t.capital_assets capitalAssets, t.total_assets totalAssets, " +
                    "t.model_id modelId, t.total_assets/" + initAssets/100 + "-100 profitRate " +
                "from his_robot_account_log t " +
                "where t.date_ between to_date(:beginDate, 'yyyy-mm-dd') and to_date(:endDate, 'yyyy-mm-dd') " +
                "and t.robot_name=:robotName " +
                "and t.model_id=:modelId " +
                "order by t.date_ asc";
        Map<String, Object> map = new HashMap();
        map.put("beginDate", beginDate);
        map.put("endDate", endDate);
        map.put("robotName", robotName);
        map.put("modelId", modelId);

        return doSQLQueryInTransaction(sql, map, HistoryRobotAccountLog.class);
    }
}
