package cn.com.acca.ma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * MACD死叉时的交易
 */
@Entity
@Table(name = "MDL_MACD_DEAD_CROSS")
public class ModelMACDDeadCross implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

    /**
     * 模型id
     */
	@Column(name = "MODEL_ID")
	private BigDecimal modelId;

    /**
     * 买入日期
     */
	@Column(name = "BUY_DATE")
	private Date buyDate;

    /**
     * 卖出日期
     */
	@Column(name = "SELL_DATE")
	private Date sellDate;

    /**
     * 股票代码
     */
	@Column(name = "STOCK_CODE")
	private String stockCode;

    /**
     * 买入价格
     */
	@Column(name = "BUY_PRICE")
	private BigDecimal buyPrice;

    /**
     * 卖出价格
     */
	@Column(name = "SELL_PRICE")
	private BigDecimal sellPrice;

    /**
     * 本次交易盈亏百分比
     */
	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

    /**
     * 累计收益。单位：元
     */
	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

    /**
     * 买入时的DIF
     */
	@Column(name = "BUY_DIF")
	private BigDecimal buyDif;

    /**
     * 买入时的DEA
     */
	@Column(name = "BUY_DEA")
	private BigDecimal buyDea;

    /**
     * 卖出时的DIF
     */
	@Column(name = "SELL_DIF")
	private BigDecimal sellDif;

    /**
     * 卖出时的DEA
     */
	@Column(name = "SELL_DEA")
	private BigDecimal sellDea;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getModelId() {
		return modelId;
	}

	public void setModelId(BigDecimal modelId) {
		this.modelId = modelId;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}

	public BigDecimal getAccumulativeProfitLoss() {
		return accumulativeProfitLoss;
	}

	public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
		this.accumulativeProfitLoss = accumulativeProfitLoss;
	}

	public BigDecimal getBuyDif() {
		return buyDif;
	}

	public void setBuyDif(BigDecimal buyDif) {
		this.buyDif = buyDif;
	}

	public BigDecimal getBuyDea() {
		return buyDea;
	}

	public void setBuyDea(BigDecimal buyDea) {
		this.buyDea = buyDea;
	}

	public BigDecimal getSellDif() {
		return sellDif;
	}

	public void setSellDif(BigDecimal sellDif) {
		this.sellDif = sellDif;
	}

	public BigDecimal getSellDea() {
		return sellDea;
	}

	public void setSellDea(BigDecimal sellDea) {
		this.sellDea = sellDea;
	}
}
