package cn.com.acca.ma.main;

import cn.com.acca.ma.service.ForeignExchangeRecordService;
import cn.com.acca.ma.service.ForeignExchangeService;
import cn.com.acca.ma.service.impl.ForeignExchangeRecordServiceImpl;
import cn.com.acca.ma.service.impl.ForeignExchangeServiceImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ForeignExchangeMain {
	private static Logger logger = LogManager.getLogger(ForeignExchangeMain.class);

	private static final ForeignExchangeService foreignExchangeService=new ForeignExchangeServiceImpl();
	private static final ForeignExchangeRecordService foreignExchangeRecordService=new ForeignExchangeRecordServiceImpl();

	public static void main(String[] args) {
		logger.info("StockMain程序开始运行......");

//		insertForeignExchange();
//		insertForeignExchangeRecord();
//		calculateMovingAverage();
	}
	
	private static void insertForeignExchange(){
		foreignExchangeService.insertForeignExchange();
	}
	
	private static void insertForeignExchangeRecord(){
		foreignExchangeRecordService.insertForeignExchangeRecord();
	}
	
	private static void calculateMovingAverage(){
		foreignExchangeRecordService.calculateMovingAverage();
	}
}
