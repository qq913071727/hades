package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.constant.HoldShippingSpaceType;
import cn.com.acca.ma.dao.Robot6EtfTransactRecordDao;
import cn.com.acca.ma.model.Robot6StockTransactRecord;
import cn.com.acca.ma.pojo.BuySuggestion;
import cn.com.acca.ma.pojo.SellSuggestion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Robot6EtfTransactRecordDaoImpl extends BaseDaoImpl<Robot6EtfTransactRecordDao> implements
        Robot6EtfTransactRecordDao {

    public Robot6EtfTransactRecordDaoImpl() {
        super();
    }

    /**
     * 卖股票/买股票
     * @param sellDate
     * @param mandatoryStopLoss
     * @param mandatoryStopLossRate
     * @param mdlEtfType
     */
    @Override
    public void sellOrBuy(String sellDate, Integer mandatoryStopLoss, Double mandatoryStopLossRate, Integer mdlEtfType) {
        logger.info("卖etf");

        String sql = "{call PKG_ROBOT6.sell_or_buy('" + sellDate + "', " + mandatoryStopLoss + ", " + mandatoryStopLossRate + ", " + mdlEtfType + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 买股票。用于做多
     * @param buyDate
     * @param maxHoldEtfNumber
     * @param holdShippingSpaceType
     */
    @Override
    public void buyOrSell(String buyDate, Integer maxHoldEtfNumber, Integer holdShippingSpaceType) {
        logger.info("买etf，日期为【" + buyDate + "】，最大持股数量为【" + maxHoldEtfNumber + "】，持仓方式为【" + holdShippingSpaceType + "】");

        String sql = null;
        if (holdShippingSpaceType.equals(HoldShippingSpaceType.AVERAGE_HOLD_SHIPPING_SPACE)){
            sql = "{call PKG_ROBOT6.buy_or_sell_avg_hold_space('" + buyDate + "', " + maxHoldEtfNumber + ")}";
        }
        if (holdShippingSpaceType.equals(HoldShippingSpaceType.MAX_HOLD_SHIPPING_SPACE)){
            sql = "{call PKG_ROBOT6.buy_or_sell_max_hold_space('" + buyDate + "', " + maxHoldEtfNumber + ")}";
        }

        doSQLInTransaction(sql);
    }

    /**
     * 买股票。用于做多
     * @param buyDate
     * @param backwardMonth
     * @param averageDateNumber
     * @param successRateType
     * @param successRate
     * @param direction
     * @param shippingSpaceControl
     * @param percentageTopThreshold
     * @param shippingSpace
     */
    @Override
    public void buyOrSell(String buyDate, Integer backwardMonth, Integer averageDateNumber, Integer successRateType,
                          Double successRate, Integer direction, Integer shippingSpaceControl,
                          Double percentageTopThreshold, Integer shippingSpace) {
        logger.info("买股票，日期为【" + buyDate + "】，向前月数为【" + backwardMonth + "】，平均成功率天数为【"
                + averageDateNumber + "】，成功率类型为【" + successRateType + "】");

        String sql = "{call PKG_ROBOT6.buy_or_sell('" + buyDate + "', " + backwardMonth + ", " + averageDateNumber + ", " +
                successRateType + ", " + successRate + ", " + direction + ", " + shippingSpaceControl + ", " +
                percentageTopThreshold + ", " + shippingSpace + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 获取买入建议列表
     * 用于做多
     * @param buyDate
     * @return
     */
    @Override
    public List<Robot6StockTransactRecord> getBullRobotEtfTransactRecordListForBuy(String buyDate) {
        logger.info("获取买入建议列表，日期【" + buyDate + "】，用于做多");

        String sql = "select * from robot6_stock_transact_record t "
                + "where t.buy_date=to_date(:buyDate,'yyyy-mm-dd') "
                + "and t.sell_date is null and t.sell_price is null and t.sell_amount is null "
                + "and t.direction=1";
        Map map = new HashMap();
        map.put("buyDate", buyDate);

        return (List<Robot6StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot6StockTransactRecord.class);
    }

    /**
     * 获取卖出建议列表
     * 用于做多
     * @param sellDate
     * @return
     */
    @Override
    public List<Robot6StockTransactRecord> getBullRobotEtfTransactRecordListForSell(String sellDate) {
        logger.info("获取卖出建议列表，日期【" + sellDate + "】，用于做多");

        String sql = "select * from robot6_stock_transact_record t "
                + "where t.sell_date=to_date(:sellDate,'yyyy-mm-dd') "
                + "and t.sell_date is not null and t.sell_price is not null and t.sell_amount is not null "
                + "and t.direction=1";
        Map map = new HashMap();
        map.put("sellDate", sellDate);

        return (List<Robot6StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot6StockTransactRecord.class);
    }

    /**
     * 获取卖出建议列表
     * 用于做空
     * @param sellDate
     * @return
     */
    @Override
    public List<Robot6StockTransactRecord> getShortRobotEtfTransactRecordListForSell(String sellDate) {
        logger.info("获取卖出建议列表，日期【" + sellDate + "】，用于做空");

        String sql = "select * from robot6_stock_transact_record t "
                + "where t.sell_date=to_date(:sellDate,'yyyy-mm-dd') "
                + "and t.buy_date is null and t.buy_price is null and t.buy_amount is null "
                + "and t.direction=-1";
        Map map = new HashMap();
        map.put("sellDate", sellDate);

        return (List<Robot6StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot6StockTransactRecord.class);
    }

    /**
     * 获取买入建议列表
     * 用于做空
     * @param buyDate
     * @return
     */
    @Override
    public List<Robot6StockTransactRecord> getShortRobotEtfTransactRecordListForBuy(String buyDate) {
        logger.info("获取买入建议列表，日期【" + buyDate + "】，用于做空");

        String sql = "select * from robot6_stock_transact_record t "
                + "where t.buy_date=to_date(:buyDate,'yyyy-mm-dd') "
                + "and t.buy_date is not null and t.buy_price is not null and t.buy_amount is not null "
                + "and t.direction=-1";
        Map map = new HashMap();
        map.put("buyDate", buyDate);

        return (List<Robot6StockTransactRecord>)doSQLQueryInTransaction(sql, map, Robot6StockTransactRecord.class);
    }

    /**
     * 获取卖建议列表
     * @param sellDate
     * @return
     */
    @Override
    public List<SellSuggestion> getSellSuggestionList_etf12YearHighestAverageProfitRate(String sellDate) {
        logger.info("获取卖出建议列表，日期【" + sellDate + "】");

        String sql = "select rstr.stock_code stockCode, si_.name_ stockName, rstr.sell_date sellDate, "
                + "rstr.sell_price sellPrice, rstr.sell_amount sellAmount, rstr.filter_type filterType, rstr.direction direction "
                + "from robot6_etf_transact_record rstr "
                + "join etf_info ei_ on ei_.code_=rstr.stock_code "
                + "where rstr.sell_date=to_date(:sellDate,'yyyy-mm-dd') "
                + "and (rstr.sell_date is not null and rstr.sell_price is not null and rstr.sell_amount is not null and rstr.direction=1) "
                + "or (rstr.buy_date is null and rstr.buy_price is null and rstr.buy_amount is null and rstr.direction=-1)";
        Map map = new HashMap();
        map.put("sellDate", sellDate);

        return (List<SellSuggestion>)doSQLQueryInTransaction(sql, map, SellSuggestion.class);
    }

    /**
     * 获取买建议列表
     * @param buyDate
     * @return
     */
    @Override
    public List<BuySuggestion> getBuySuggestionList_etf12YearHighestAverageProfitRate(String buyDate) {
        logger.info("获取买入建议列表，日期【" + buyDate + "】");

        String sql = "select rstr.stock_code stockCode, si_.name_ stockName, rstr.buy_date buyDate, "
                + "rstr.buy_price buyPrice, rstr.buy_amount buyAmount, rstr.filter_type filterType, rstr.direction direction "
                + "from robot6_etf_transact_record rstr "
                + "join etf_info ei_ on ei_.code_=rstr.stock_code "
                + "where rstr.buy_date=to_date(:buyDate,'yyyy-mm-dd') "
                + "and (rstr.sell_date is null and rstr.sell_price is null and rstr.sell_amount is null and rstr.direction=1)"
                + "or (rstr.buy_date is not null and rstr.buy_price is not null and rstr.buy_amount is not null and rstr.direction=-1)";
        Map map = new HashMap();
        map.put("buyDate", buyDate);

        return (List<BuySuggestion>)doSQLQueryInTransaction(sql, map, BuySuggestion.class);
    }
}
