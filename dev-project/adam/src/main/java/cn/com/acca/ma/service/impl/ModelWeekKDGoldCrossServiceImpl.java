package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.ModelWeekKDGoldCross;
import java.util.List;

import cn.com.acca.ma.service.ModelWeekKDGoldCrossService;

public class ModelWeekKDGoldCrossServiceImpl extends BaseServiceImpl<ModelWeekKDGoldCrossServiceImpl, ModelWeekKDGoldCross> implements
	ModelWeekKDGoldCrossService {
	
	/*********************************************************************************************************************
	 * 
	 * 												计算全部数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用周线级别KD金叉模型算法，向表MDL_WEEK_KD_GOLD_CROSS插入数据
	 */
	public void writeModelWeekKDGoldCross() {
		modelWeekKDGoldCrossDao.writeModelWeekKDGoldCross();
	}

	@Override
	public String listToString(List list) {
		// TODO Auto-generated method stub
		return null;
	}
}
