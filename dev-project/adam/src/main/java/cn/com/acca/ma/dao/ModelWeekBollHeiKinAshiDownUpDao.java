package cn.com.acca.ma.dao;

import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;

import java.util.List;

public interface ModelWeekBollHeiKinAshiDownUpDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_BOLL_HEI_KIN_ASHI_DOWN_UP中插入数据
     */
    void writeModelWeekBollHeiKinAshiDownUp();

    /**
     * 查询hei_kin_ashi下跌趋势交易收益率和最高价突破布林带上轨的百分比
     * @param beginDate
     * @param endDate
     */
    List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekHeiKinAshiDownUpProfitOrLossAndBollUp(String beginDate, String endDate);
}
