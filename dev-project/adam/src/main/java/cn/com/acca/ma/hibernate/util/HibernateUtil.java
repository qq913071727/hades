package cn.com.acca.ma.hibernate.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@SuppressWarnings("deprecation")
public class HibernateUtil {
	private static SessionFactory sessionFactory;
	static {
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch (Exception ex) {
			System.err.println("构造SessionFactory异常发生： " + ex.getMessage());
		}
	}

	public static Session currentSession() {
		Session session = sessionFactory.openSession();
		return session;
	}

	public static void closeSession(Session session) {
		if (null != session) {
			session.close();
		}
	}

	/**
	 * 创建一个新的SessionFactory，再返回一个新的Session
	 * @return
	 */
	public static Session newSession(){
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		return  sessionFactory.openSession();
	}

	/**
	 * 关闭SessionFactory
	 * @param session
	 */
	public static void closeNewSessionFactory(Session session){
		session.getSessionFactory().close();
	}
}
