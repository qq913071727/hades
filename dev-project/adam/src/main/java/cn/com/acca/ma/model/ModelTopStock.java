package cn.com.acca.ma.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MDL_TOP_STOCK")
public class ModelTopStock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private Integer id;

	@Column(name="DATE_")
	private Date date;

	@Column(name="PERIOD21")
	private String twentyOne;
	
	@Column(name="PERIOD34")
	private String thirtyFour;
	
	@Column(name="PERIOD55")
	private String fiftyFive;
	
	@Column(name="PERIOD89")
	private String eightyNine;
	
	@Column(name="PERIOD144")
	private String oneHundredAndFortyFour;
	
	@Column(name="PERIOD233")
	private String twoHundredAndThirtyThree;
	
	@Column(name="PERIOD337")
	private String threeHundredAndThirtySeven;
	
	@Column(name="PERIOD610")
	private String sixHundredAndTen;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTwentyOne() {
		return twentyOne;
	}

	public void setTwentyOne(String twentyOne) {
		this.twentyOne = twentyOne;
	}

	public String getThirtyFour() {
		return thirtyFour;
	}

	public void setThirtyFour(String thirtyFour) {
		this.thirtyFour = thirtyFour;
	}

	public String getFiftyFive() {
		return fiftyFive;
	}

	public void setFiftyFive(String fiftyFive) {
		this.fiftyFive = fiftyFive;
	}

	public String getEightyNine() {
		return eightyNine;
	}

	public void setEightyNine(String eightyNine) {
		this.eightyNine = eightyNine;
	}

	public String getOneHundredAndFortyFour() {
		return oneHundredAndFortyFour;
	}

	public void setOneHundredAndFortyFour(String oneHundredAndFortyFour) {
		this.oneHundredAndFortyFour = oneHundredAndFortyFour;
	}

	public String getTwoHundredAndThirtyThree() {
		return twoHundredAndThirtyThree;
	}

	public void setTwoHundredAndThirtyThree(String twoHundredAndThirtyThree) {
		this.twoHundredAndThirtyThree = twoHundredAndThirtyThree;
	}

	public String getThreeHundredAndThirtySeven() {
		return threeHundredAndThirtySeven;
	}

	public void setThreeHundredAndThirtySeven(String threeHundredAndThirtySeven) {
		this.threeHundredAndThirtySeven = threeHundredAndThirtySeven;
	}

	public String getSixHundredAndTen() {
		return sixHundredAndTen;
	}

	public void setSixHundredAndTen(String sixHundredAndTen) {
		this.sixHundredAndTen = sixHundredAndTen;
	}

	
}
