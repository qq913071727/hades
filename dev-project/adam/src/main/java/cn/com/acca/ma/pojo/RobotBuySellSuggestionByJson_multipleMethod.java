package cn.com.acca.ma.pojo;

import cn.com.acca.ma.model.Robot3StockTransactRecord;
import cn.com.acca.ma.model.Robot5StockTransactRecord;
import lombok.Data;

import java.util.List;

/**
 * 机器人买卖建议，json格式
 */
@Data
public class RobotBuySellSuggestionByJson_multipleMethod {

    /**
     * 做多卖出建议列表
     */
    private List<Robot5StockTransactRecord> bullSellSuggestionList;

    /**
     * 做多买入建议列表
     */
    private List<Robot5StockTransactRecord> bullBuySuggestionList;

    /**
     * 做空卖出建议列表
     */
    private List<Robot5StockTransactRecord> shortSellSuggestionList;

    /**
     * 做空买入建议列表
     */
    private List<Robot5StockTransactRecord> shortBuySuggestionList;


}
