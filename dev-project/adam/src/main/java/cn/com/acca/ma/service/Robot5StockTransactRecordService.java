package cn.com.acca.ma.service;

public interface Robot5StockTransactRecordService extends BaseService {

    /**
     * 执行某段时间内的股票买卖交易（多种方法一起使用）
     */
    void doBuyAndSellByBeginDateAndEndDate_multipleMethod();
}
