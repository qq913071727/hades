package cn.com.acca.ma.dao;

import java.util.Date;
import java.util.List;

import cn.com.acca.ma.model.Report;

public interface ReportDao extends BaseDao {

	/*********************************************************************************************************************
	 * 
	 * 									                            生成报告
	 * 
	 *********************************************************************************************************************/
	/**
	 * 获取某一日统计的股票样本总数
	 * @param stockDate 日期
	 * @return
	 */
	int getStockSampleSumByDate(String stockDate);
	
	/**
	 * 获取某一日上涨股票的家数
	 * @param stockDate 日期
	 * @return
	 */
	int getStockUpMumberByDate(String stockDate);
	
	/**
	 * 获取某一日持平股票的家数
	 * @param stockDate 日期
	 * @return
	 */
	int getStockMiddleMumberByDate(String stockDate);
	
	/**
	 * 获取某一日下跌股票的家数
	 * @param stockDate 日期
	 * @return
	 */
	int getStockDownMumberByDate(String stockDate);
	
	/**
	 * 如果REPORT表中有相同的报告记录，则删除
	 * @param report
	 */
	void deleteReport(Report report);
	
	/**
	 * 将报告对象保存到数据库
	 * @param report 报告对象
	 */
	void insertReport(Report report);
	
	/**
	 * 获取某一日多头排列的股票
	 * @param date
	 * @return 多头排列的股票的
	 */
	@SuppressWarnings("rawtypes")
	List getBullRank(String date);
	
	/**
	 * 获取某一日空头排列的股票
	 * @param date
	 * @return 空头排列的股票的
	 */
	@SuppressWarnings("rawtypes")
	List getShortOrder(String date);
	
	/**
	 * 查找日线级别发生反转的股票
	 * @param stockDate 日期
	 * @param percentage 下跌程度阈值
	 * @return
	 */
	String selectReverseStock(final String stockDate,final double percentage);
	
	/**
	 * 查找日线级别MACD金叉的股票
	 * @param macdDate 日期
	 * @param macdRate 下跌幅度
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List selectMACDGoldCross(final String macdDate,final String macdRate);
	
	/**
	 * 查询某一周MACD底背离的股票
	 * @param weekendMacdEndDeviateBeginDate
	 * @param weekendMacdEndDeviateEndDate
	 * @return 周线级别底背离股票的信息列表
	 */
	String findWeekendMacdEndDeviate(final String weekendMacdEndDeviateBeginDate,final String weekendMacdEndDeviateEndDate,final String reportWeekendMacdEndDeviateRate);
	
	/**
	 * 获取周线级别KD指标金叉的股票
	 * @param beginDate
	 * @param endDate
	 * @param crossPoint
	 * @return
	 */
	StringBuilder selectStockWeekendKDUp(final String beginDate,final String endDate,final String crossPoint);
	
	/**
	 * 查询某一日收盘价出现异常的股票
	 * @param stockDate 日期
	 * @param rate 倍数
	 * @return
	 */
	StringBuilder findAbnormalStock(final String stockDate, final int rate);
	
	/**
	 * 查询某一日MACD指标最低的stockNumber支股票
	 * @param stockDate 日期（结束日期）
	 * @param stockNumber 股票个数
	 * @param intervalDate stockDate之前intervalDate个交易日（开始日期）
	 * @return
	 */
	String findLowestMacdStock(final String stockDate, final int stockNumber, final int intervalDate);
	
	/**
	 * 查询某一周最低KD的stockNumber只股票
	 * @param beginDate
	 * @param endDate
	 * @param timeSpan
	 * @param stockNumber
	 * @return
	 */
	String findLowestKdStockWeek(String beginDate,String endDate,String timeSpan,int stockNumber);
	
	/**
	 * 查询某一日股票收盘价到达250日均线支撑的股票
	 * @param stockDate 日期
	 * @param nearRate 接近250日均线的比率
	 * @param downRate 之前下跌的比率
	 * @return
	 */
	StringBuilder selectStockWith250Support(final String stockDate,final double nearRate,final double downRate);
	
	/**
	 * 查询某一日股票收盘价到达120日均线支撑的股票
	 * @param stockDate 日期
	 * @param nearRate 接近120日均线的比率
	 * @param downRate 之前下跌的比率
	 * @return
	 */
	StringBuilder selectStockWith120Support(final String stockDate,final double nearRate,final double downRate);
	
	/**
	 * 查询某一段时间内上涨股票的前number名
	 * @param beginDate
	 * @param endDate
	 * @param number
	 * @return
	 */
	StringBuilder findTopStock(final String beginDate,final String endDate,final int number,final int topStockWordLimitNumber);
	
	/**
	 * 查询某一段时间内下跌股票的前number名
	 * @param beginDate
	 * @param endDate
	 * @param number
	 * @return
	 */
	StringBuilder findLastStock(final String beginDate,final String endDate,final int number);
	
	/*********************************************************************************************************************
	 * 
	 * 									                           增量备份表REPORT
	 * 
	 *********************************************************************************************************************/
	/**
	 * 从表REPORT中获取从开始时间beginDate到结束时间endDate
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<Date> getDateByCondition(String beginDate, String endDate);
	
	/**
	 * 根据日期获取Report对象
	 * @param date
	 * @return
	 */
	List<Report> getReportByDate(String date);
}
