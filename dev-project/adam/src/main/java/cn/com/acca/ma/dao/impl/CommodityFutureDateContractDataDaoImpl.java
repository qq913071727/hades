package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.dao.CommodityFutureDateContractDataDao;
import cn.com.acca.ma.dao.StockTransactionDataAllDao;
import cn.com.acca.ma.model.CommodityFutureDateContractData;
import cn.com.acca.ma.model.ForeignExchangeRecord;
import cn.com.acca.ma.mybatis.util.MyBatisUtil;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommodityFutureDateContractDataDaoImpl extends BaseDaoImpl<CommodityFutureDateContractDataDaoImpl> implements
        CommodityFutureDateContractDataDao {

    public CommodityFutureDateContractDataDaoImpl() {
    }

    /**
     * 根据开始日期beginDate和结束日期endDate，获取DATETIME字段
     * @param beginDate
     * @param endDate
     * @return
     */
    @Override
    public List<CommodityFutureDateContractData> getDateByCondition(String beginDate, String endDate) {
        logger.info("method getDateByCondition begin");

        sqlSession= MyBatisUtil.openSqlSession();

        Date tempBeginDate= DateUtil.stringToDate(beginDate);
        Date tempEndDate=DateUtil.stringToDate(endDate);
        long longBeginDate=tempBeginDate.getTime();
        long longEndDate=tempEndDate.getTime();

        Map<String,Object> param=new HashMap<String,Object>();
        param.put("beginDate", new Date(longBeginDate));
        param.put("endDate", new Date(longEndDate));
        List<CommodityFutureDateContractData> list=sqlSession.selectList("cn.com.acca.ma.xmlmapper.CommodityFutureDateContractDataMapper.getDateByCondition",param);
        sqlSession.commit();
        sqlSession.close();

        logger.info("method getDateByCondition finish");
        return list;
    }

    /**
     * 根据日期date获取所有CommodityFutureDateContractData对象
     * @param date
     * @return
     */
    public List<CommodityFutureDateContractData> getCommodityFutureDateContractDataByDate(String date){
        logger.info("method getCommodityFutureDateContractDataByDate begin");

        sqlSession=MyBatisUtil.openSqlSession();

        Date tempDate=DateUtil.stringToDate(date);
        long longDate=tempDate.getTime();

        Map<String,Object> param=new HashMap<String,Object>();
        param.put("date", new Date(longDate));
        List<CommodityFutureDateContractData> list=sqlSession.selectList("cn.com.acca.ma.xmlmapper.CommodityFutureDateContractDataMapper.getCommodityFutureDateContractDataByDate",param);
        sqlSession.commit();
        sqlSession.close();

        logger.info("method getCommodityFutureDateContractDataByDate finish");
        return list;
    }
}
