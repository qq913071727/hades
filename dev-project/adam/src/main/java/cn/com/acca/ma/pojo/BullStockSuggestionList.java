package cn.com.acca.ma.pojo;

import lombok.Data;

import java.util.List;

/**
 * 做多股票的买卖建议列表
 */
@Data
public class BullStockSuggestionList {

    /**
     * 应当买入的股票的列表（用于做多）
     */
    private List<StockInfoPojo> shouldBuyBullStockList;

    /**
     * 应当卖出的股票的列表（用于做多）
     */
    private List<StockInfoPojo> shouldSellBullStockList;
}
