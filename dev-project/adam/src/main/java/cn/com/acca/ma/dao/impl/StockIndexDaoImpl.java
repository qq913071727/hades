package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.pojo.MaxAndMin;
import com.alibaba.fastjson.JSON;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import cn.com.acca.ma.dao.StockIndexDao;
import cn.com.acca.ma.model.StockIndex;
import cn.com.acca.ma.mybatis.util.MyBatisUtil;
import cn.com.acca.ma.xmlmapper.StockIndexMapper;
import org.apache.ibatis.session.SqlSession;

public class StockIndexDaoImpl extends BaseDaoImpl<StockIndexDaoImpl> implements StockIndexDao {
	
	public StockIndexDaoImpl() {
		super();
	}

	/*********************************************************************************************************************
	 * 
	 * 									                            更新每天的数据
	 * 
	 *********************************************************************************************************************/
	/************************************* 下面两个方法是使用myBatis实现 ***********************************************/
	public void deleteStockIndexMyBatis(){
		logger.info("删除stock_index表中的全部数据");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexMapper = sqlSession.getMapper(StockIndexMapper.class);
		//首先删除STOCK_INDEX表中的全部数据
		stockIndexMapper.deleteStockIndexMyBatis();
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
	}
	
	@SuppressWarnings("deprecation")
	public void writeStockIndexMyBatis(boolean multithreading){
		logger.info("调用存储过程【PKG_INDEX.WRITE_STOCK_INDEX】，向表stock_index中插入数据");

		SqlSession newSqlSession = null;
		if (multithreading) {
			newSqlSession = MyBatisUtil.newSqlSession();
		} else {
			sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		}

		File file = new File(INDEX_DATE);
		FileReader fileReader = null;
		String line = null;
		BufferedReader bufferedReader = null;

		try {
			fileReader = new FileReader(file);
			//然后向表STOCK_INDEX中插入数据
			bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				StockIndex stockIndex = JSON.parseObject(line, StockIndex.class);

//				int year = Integer.parseInt(data[1].split("-")[0]) - 1900;
//				int month = Integer.parseInt(data[1].split("-")[1]) - 1;
//				int day = Integer.parseInt(data[1].split("-")[2]);
//
//				stockIndex.setCode(data[0]);
//				stockIndex.setDate(new Date(year, month, day));
//				stockIndex.setOpenPrice(new BigDecimal(Double.parseDouble(data[2])));
//				stockIndex.setHighestPrice(new BigDecimal(Double.parseDouble(data[3])));
//				stockIndex.setClosePrice(new BigDecimal(Double.parseDouble(data[4])));
//				stockIndex.setLowestPrice(new BigDecimal(Double.parseDouble(data[5])));
//				stockIndex.setVolume(new BigDecimal(Double.parseDouble(data[6])));

				Map<String, Object> param = new HashMap<>();
                param.put("code", stockIndex.getCode());
                param.put("date", new java.sql.Date(stockIndex.getDate().getYear(), stockIndex.getDate().getMonth(),
                		stockIndex.getDate().getDate()));
                param.put("openPrice", stockIndex.getOpenPrice());
                param.put("highestPrice", stockIndex.getHighestPrice());
                param.put("closePrice", stockIndex.getClosePrice());
                param.put("lowestPrice", stockIndex.getLowestPrice());
                param.put("changeAmount", stockIndex.getChangeAmount());
				param.put("changeRange", stockIndex.getChangeRange());
				param.put("volume", stockIndex.getVolume());
				param.put("turnover", stockIndex.getTurnover());

				if (multithreading){
					newSqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMyBatis", param);
					newSqlSession.commit();
				} else {
					sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMyBatis", param);
					sqlSession.commit();
				}
			}
			bufferedReader.close();
			fileReader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (multithreading){
			newSqlSession.close();
		} else {
			sqlSession.close();
		}
	}
	
	/**************************************** 下面两个方法是使用JPA实现 ***********************************************/
	public void deleteStockIndex(){
		logger.info("write stock index begin(delete from stock_index)");
		
		//首先删除STOCK_INDEX表中的全部数据
		em.getTransaction().begin();
		Query queryDeleteStockIndex = em.createQuery("delete from StockIndex t");
		queryDeleteStockIndex.executeUpdate();
		em.getTransaction().commit();
		em.close();
		
		logger.info("write stock index finish(delete from stock_index)");
	}

	@SuppressWarnings("deprecation")
	public void writeStockIndex() {
		logger.info("write stock index begin(write data to stock_index)");
		
		File file = new File(INDEX_DATE);
		FileReader fileReader = null;
		String line = null;
		BufferedReader bufferedReader = null;

		try {
			fileReader = new FileReader(file);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			//然后向表STOCK_INDEX中插入数据
			bufferedReader = new BufferedReader(fileReader);
			em.getTransaction().begin();
			while ((line = bufferedReader.readLine()) != null) {
				String[] data = line.split(",");
				StockIndex stockIndex = new StockIndex();

				int year = Integer.parseInt(data[1].split("-")[0]) - 1900;
				int month = Integer.parseInt(data[1].split("-")[1]) - 1;
				int day = Integer.parseInt(data[1].split("-")[2]);

				stockIndex.setCode(data[0]);
				stockIndex.setDate(new Date(year, month, day));
				stockIndex.setOpenPrice(new BigDecimal(Double.parseDouble(data[2])));
				stockIndex.setHighestPrice(new BigDecimal(Double.parseDouble(data[3])));
				stockIndex.setClosePrice(new BigDecimal(Double.parseDouble(data[4])));
				stockIndex.setLowestPrice(new BigDecimal(Double.parseDouble(data[5])));
				stockIndex.setVolume(new BigDecimal(Double.parseDouble(data[6])));

				Query queryIndex = em.createNativeQuery("{call pkg_index.WRITE_STOCK_INDEX(?,?,?,?,?,?,?)}");
				queryIndex.setParameter(1, stockIndex.getCode());
				queryIndex.setParameter(2, stockIndex.getDate());
				queryIndex.setParameter(3, stockIndex.getOpenPrice());
				queryIndex.setParameter(4, stockIndex.getHighestPrice());
				queryIndex.setParameter(5, stockIndex.getClosePrice());
				queryIndex.setParameter(6, stockIndex.getLowestPrice());
				queryIndex.setParameter(7, stockIndex.getVolume());
				queryIndex.executeUpdate();
			}
			
			em.getTransaction().commit();
			bufferedReader.close();
			fileReader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		em.close();
		logger.info("write stock index finish(write data to stock_index)");
	}

	/**
	 * 根据日期date，计算某一日所有指数的移动平均线数据
	 * @param multithreading
	 * @param date
	 */
	public void writeStockIndexMAByDate(boolean multithreading, String date){
		logger.info("开始计算日期【" + date + "】，所有指数的移动平均线数据");

		SqlSession newSqlSession = null;
		if (multithreading) {
			newSqlSession = MyBatisUtil.newSqlSession();
		} else {
			sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		}

		Map<String,String> map=new HashMap<String,String>();
		map.put("date", date);

		if (multithreading) {
			newSqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMAByDate",map);
			newSqlSession.commit();
			newSqlSession.close();
		} else {
			sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMAByDate",map);
			sqlSession.commit();
			sqlSession.close();
		}

		logger.info("计算日期【" + date + "】，所有指数的移动平均线数据完成");
	}

	/**
	 * 根据日期date，计算某一日所有指数的Hei Kin Ashi数据
	 * @param multithreading
	 * @param date
	 */
	public void writeStockIndexHeiKinAshiByDate(boolean multithreading, String date){
		logger.info("write StockIndex object with Hei Kin Ashi by date begin");

		SqlSession newSqlSession = null;
		if (multithreading) {
			newSqlSession = MyBatisUtil.newSqlSession();
		} else {
			sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		}

		Map<String,String> map=new HashMap<String,String>();
		map.put("date", date);

		if (multithreading) {
			newSqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexHeiKinAshiByDate",map);
			newSqlSession.commit();
			MyBatisUtil.closeNewSqlSessionFactory(newSqlSession);
		} else {
			sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexHeiKinAshiByDate",map);
			sqlSession.commit();
			sqlSession.close();
		}

		logger.info("write StockIndex object with Hei Kin Ashi by date finish");
	}

	/**
	 * 根据日期date，计算所有指数某一日的乖离率
	 * @param multithreading
	 * @param date
	 */
	public void writeStockIndexBiasByDate(boolean multithreading, String date){
		logger.info("开始计算所有指数在日期【" + date + "】的乖离率");

		SqlSession newSqlSession = null;
		if (multithreading) {
			newSqlSession = MyBatisUtil.newSqlSession();
		} else {
			sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		}

		Map<String,String> map=new HashMap<String,String>();
		map.put("date", date);

		if (multithreading) {
			newSqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexBiasByDate",map);
			newSqlSession.commit();
			newSqlSession.close();
		} else {
			sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexBiasByDate",map);
			sqlSession.commit();
			sqlSession.close();
		}

		logger.info("计算所有指数在日期【" + date + "】的乖离率完成");
	}

	/**
	 * 更新表STOCK_INDEX中某一日的EMA15，EMA26，DIF和DEA字段
	 * @param date
	 */
	@Override
	public void writeStockIndexMACDByDate(String date) {
		logger.info("更新表STOCK_INDEX中【" + date + "】的EMA15，EMA26，DIF和DEA字段");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("date", date);

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDEmaByDate", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDDifByDate", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDDeaByDate", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDByDate", param);
		sqlSession.commit();

		sqlSession.close();

		logger.info("更新表STOCK_INDEX中【" + date + "】的EMA15，EMA26，DIF和DEA字段完成");
	}

	/**
	 * 更新表STOCK_INDEX中某一日的RSV，K和D字段
	 * @param date
	 */
	@Override
	public void writeStockIndexKDByDate(String date) {
		logger.info("更新表STOCK_INDEX中【" + date + "】的RSV，K和D字段");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("date", date);

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDRsvByDate", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDKByDate", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDDByDate", param);
		sqlSession.commit();

		sqlSession.close();

		logger.info("更新表STOCK_INDEX中【" + date + "】的RSV，K和D字段完成");
	}
	
	/**************************************** 根据每日数据更新图表 *********************************************************/
	/**
	 * 为创建28分化图表中的线图，获取数据
	 * @param multithreading
	 * @param beginTime
	 * @param endTime
	 * @param code
	 * @return
	 */
	public List<StockIndex> getDataForLineChart(boolean multithreading, String beginTime, String endTime,String code){
		logger.info("get data for line chart begin");

		SqlSession newSqlSession = null;
		StockIndexMapper newStockIndexMapper = null;
		if (multithreading) {
			newSqlSession = MyBatisUtil.newSqlSession();
			newStockIndexMapper = newSqlSession.getMapper(StockIndexMapper.class);
		} else {
			sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
			stockIndexMapper = sqlSession.getMapper(StockIndexMapper.class);
		}

		Map<String,String> map=new HashMap<String,String>();
		map.put("beginTime", beginTime);
		map.put("endTime", endTime);
		map.put("code",code);
		
		//获取beginTime和endTime之间的日期
		List<StockIndex> list;
		if (multithreading) {
			list = newStockIndexMapper.getDataForLineChart(map);
			newSqlSession.commit();
			newSqlSession.close();
		} else {
			list = stockIndexMapper.getDataForLineChart(map);
			sqlSession.commit();// 提交会话，即事务提交
			sqlSession.close();// 关闭会话，释放资源
		}

		logger.info("get data for line chart finish");
		return list;
	}

	/**
	 * 为创建28分化图表中的柱状图，获取日期数据
	 * @param multithreading
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public List<Date> getDateForBarChart(boolean multithreading, String beginTime, String endTime){
		logger.info("get date for bar chart begin");

		SqlSession newSqlSession = null;
		StockIndexMapper newStockIndexMapper = null;
		if (multithreading) {
			newSqlSession = MyBatisUtil.newSqlSession();
			newStockIndexMapper = newSqlSession.getMapper(StockIndexMapper.class);
		} else {
			sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
			stockIndexMapper = sqlSession.getMapper(StockIndexMapper.class);
		}

		Map<String,String> map=new HashMap<String,String>();
		map.put("beginTime", beginTime);
		map.put("endTime", endTime);
		
		//获取beginTime和endTime之间的日期
		List<Date> list;
		if (multithreading) {
			list = newStockIndexMapper.getDateForBarChart(map);
			newSqlSession.commit();// 提交会话，即事务提交
			newSqlSession.close();// 关闭会话，释放资源
		} else {
			list = stockIndexMapper.getDateForBarChart(map);
			sqlSession.commit();// 提交会话，即事务提交
			sqlSession.close();// 关闭会话，释放资源
		}

		logger.info("get date for bar chart finish");
		return list;
	}
	
	/**
     * 根据开始时间和结束时间，在STOCK_INDEX表中查找所有INDEX_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
	public List<Date> getDateByCondition(String beginDate, String endDate){
		logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
			+ "在STOCK_INDEX表中查找所有DATE_字段");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexMapper = sqlSession.getMapper(StockIndexMapper.class);
		
		Map<String,String> map=new HashMap<String,String>();
		map.put("beginTime", beginDate);
		map.put("endTime", endDate);
		
		//获取beginTime和endTime之间的日期
		List<Date> list=stockIndexMapper.getDateByCondition(map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		return list;
	}
	
	/**
	 * 根据日期获取StockIndex对象
	 * @param date
	 * @return
	 */
	public List<StockIndex> getStockIndexByDate(String date){
		logger.info("据日期【" + date + "】获取StockIndex对象");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexMapper = sqlSession.getMapper(StockIndexMapper.class);
		
		Map<String,String> map=new HashMap<String,String>();
		map.put("date", date);
		
		//获取beginTime和endTime之间的日期
		List<StockIndex> list=stockIndexMapper.getStockIndexByDate(map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		return list;
	}
	
	/**
	 * 保存StockIndex对象
	 * @param stockIndex
	 */
	@SuppressWarnings("deprecation")
	public void saveStockIndex(StockIndex stockIndex){
		logger.info("保存StockIndex对象");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		Map<String, Object> param = new HashMap<String, Object>();
        param.put("code", stockIndex.getCode());
        param.put("date", new java.sql.Date(stockIndex.getDate().getYear(), stockIndex.getDate().getMonth(),
        		stockIndex.getDate().getDate()));
        param.put("openPrice", stockIndex.getOpenPrice());
        param.put("highestPrice", stockIndex.getHighestPrice());
        param.put("closePrice", stockIndex.getClosePrice());
        param.put("lowestPrice", stockIndex.getLowestPrice());
        param.put("changeAmount", stockIndex.getChangeAmount());
        param.put("changeRange", stockIndex.getChangeRange());
        param.put("volume", stockIndex.getVolume());
        param.put("turnover", stockIndex.getTurnover());
        param.put("ma5", stockIndex.getMa5());
        param.put("ma10", stockIndex.getMa10());
        param.put("ma20", stockIndex.getMa20());
        param.put("ma60", stockIndex.getMa60());
        param.put("ma120", stockIndex.getMa120());
        param.put("ma250", stockIndex.getMa250());
        param.put("haIndexOpenPrice",stockIndex.getHAIndexOpenPrice());
        param.put("haIndexHighestPrice",stockIndex.getHAIndexHighestPrice());
        param.put("haIndexClosePrice",stockIndex.getHAIndexClosePrice());
        param.put("haIndexLowestPrice",stockIndex.getHAIndexLowestPrice());
        param.put("bias5", stockIndex.getBias5());
		param.put("bias10", stockIndex.getBias10());
		param.put("bias20", stockIndex.getBias20());
		param.put("bias60", stockIndex.getBias60());
		param.put("bias120", stockIndex.getBias120());
		param.put("bias250", stockIndex.getBias250());
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.saveStockIndexMyBatis", param);
		sqlSession.commit();
		sqlSession.close();
	}

	/**
	 * 在开始时间和结束时间之间，查找某个指数的最大值和最小值
	 * @param stockIndexCode
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@Override
	public List<MaxAndMin> getMaxMinClosePriceBetween(String stockIndexCode, String beginDate, String endDate) {
		logger.info("在开始时间【" + beginDate + "】和结束时间【" + endDate + "】之间，查找某个指数【" + stockIndexCode + "】的最大值和最小值");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexMapper = sqlSession.getMapper(StockIndexMapper.class);

		Map<String,String> map=new HashMap<String,String>();
		map.put("stockIndexCode", stockIndexCode);
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);

		//获取beginDate和endDate之间的日期
		List<MaxAndMin> list=stockIndexMapper.getMaxMinClosePriceBetween(map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源

		return list;
	}

	/*********************************************** 更新全部的数据 *****************************************************/
	/**
	 * 计算5日均线
	 */
	@Override
	public void writeStockIndexMA5() {
		logger.info("开始计算所有指数的5日均线");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMA5");
		sqlSession.commit();
		sqlSession.close();

		logger.info("所有指数的5日均线计算完成");
	}

	/**
	 * 计算10日均线
	 */
	@Override
	public void writeStockIndexMA10() {
		logger.info("开始计算所有指数的10日均线");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMA10");
		sqlSession.commit();
		sqlSession.close();

		logger.info("所有指数的10日均线计算完成");
	}

	/**
	 * 计算20日均线
	 */
	@Override
	public void writeStockIndexMA20() {
		logger.info("开始计算所有指数的20日均线");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMA20");
		sqlSession.commit();
		sqlSession.close();

		logger.info("所有指数的20日均线计算完成");
	}

	/**
	 * 计算60日均线
	 */
	@Override
	public void writeStockIndexMA60() {
		logger.info("开始计算所有指数的60日均线");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMA60");
		sqlSession.commit();
		sqlSession.close();

		logger.info("所有指数的60日均线计算完成");
	}

	/**
	 * 计算120日均线
	 */
	@Override
	public void writeStockIndexMA120() {
		logger.info("开始计算所有指数的120日均线");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMA120");
		sqlSession.commit();
		sqlSession.close();

		logger.info("所有指数的120日均线计算完成");
	}

	/**
	 * 计算250日均线
	 */
	@Override
	public void writeStockIndexMA250() {
		logger.info("开始计算所有指数的250日均线");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMA250");
		sqlSession.commit();
		sqlSession.close();

		logger.info("所有指数的250日均线计算完成");
	}

	/**
	 * 计算Hei Kin Ashi相关的字段
	 */
	public void writeStockIndexHeiKinAshi(){
		logger.info("开始计算Hei Kin Ashi的所有相关字段");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexHeiKinAshi");
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("计算Hei Kin Ashi的所有相关字段完成");
	}

	/**
	 * 计算所有指数的乖离率
	 */
	public void writeStockIndexBias(){
		logger.info("开始计算所有指数的乖离率");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexBias");
		sqlSession.commit();
		sqlSession.close();

		logger.info("计算所有指数的乖离率完成");
	}

	/**
	 * 计算所有指数的MACD
	 */
	@Override
	public void writeStockIndexMACD() {
		logger.info("开始计算所有指数的MACD");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDInit");
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDEma");
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDDif");
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACDDea");
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexMACD");
		sqlSession.commit();

		sqlSession.close();

		logger.info("计算所有指数的MACD完成");
	}

	/**
	 * 计算所有指数的KD
	 */
	@Override
	public void writeStockIndexKD() {
		logger.info("开始计算所有指数的KD");

		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;

		Map<String, Object> param = new HashMap<String, Object>();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDInit", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDRsv", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDK", param);
		sqlSession.commit();

		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexMapper.writeStockIndexKDD", param);
		sqlSession.commit();

		sqlSession.close();

		logger.info("计算所有指数的KD完成");
	}
}
