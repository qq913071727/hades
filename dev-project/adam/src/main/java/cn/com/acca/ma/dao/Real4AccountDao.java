package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Real4Account;

import java.util.List;

public interface Real4AccountDao extends BaseDao {

    /**
     * 查找所有Real4Account类型对象
     *
     * @return
     */
    List<Real4Account> findAll();

}
