package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelGoldCrossDeadCrossAnalysis;

import java.util.List;

public interface ModelGoldCrossDeadCrossAnalysisDao extends BaseDao {

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的基础数据
     */
    void writeModelGoldCrossDeadCrossAnalysis();

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的百分比的均值
     */
    void writeModelGoldCrossDeadCrossAnalysisPercentMA();

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的成功率的均值
     */
    void writeModelGoldCrossDeadCrossAnalysisSuccessRateMA();

    /**
     * 根据date，查找最近两条记录，降序排列
     * @param date
     * @return
     */
    List<ModelGoldCrossDeadCrossAnalysis> findTwoByDateOrderByDateDesc(String date);

    /**
     * 查询各种算法状态已经持续了多少天。
     *   1表示MACD金叉，2表示MACD死叉，3表示收盘价金叉五日均线，4表示收盘价死叉五日均线
     *   5表示hei_kin_ashi处于上涨阶段，6表示hei_kin_ashi处于下跌阶段
     *   7表示KD金叉，8表示KD死叉
     *   返回值为0表示现在并不处于这种状态
     * @param type
     * @param date
     * @return
     */
    Integer findGoldCrossDeadCrossContinueDateCount(Integer type, String date);

    /**
     * 根据日期，计算表MDL_G_C_D_C_ANALYSIS的数据
     * @param beginDate
     * @param endDate
     */
    void writeModelGoldCrossDeadCrossAnalysisByDate(String beginDate, String endDate);

    /**
     * 根据date查找ModelGoldCrossDeadCrossAnalysis对象
     * @param date
     * @return
     */
    ModelGoldCrossDeadCrossAnalysis findByDate(String date);
}
