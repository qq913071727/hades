package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MDL_WEEK_KD_GOLD_CROSS")
public class ModelWeekKDGoldCross implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "MODEL_ID")
	private BigDecimal modelId;
	
	@Column(name = "STOCK_CODE")
	private String stockCode;
	
	@Column(name="WEEK_NUMBER")
	private BigDecimal weekNumber;
	
	@Column(name="BUY_BEGIN_DATE")
	private Date buyBeginDate;
	
	@Column(name="BUY_END_DATE")
	private Date buyEndDate;
	
	@Column(name="BUY_PRICE")
	private BigDecimal buyPrice;
	
	@Column(name="SELL_BEGIN_DATE")
	private Date sellBeginDate;
	
	@Column(name="SELL_END_DATE")
	private Date sellEndDate;
	
	@Column(name="SELL_PRICE")
	private BigDecimal sellPrice;
	
	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

	@Column(name = "BUY_K")
	private BigDecimal buyK;

	@Column(name = "BUY_D")
	private BigDecimal buyD;

	@Column(name = "SELL_K")
	private BigDecimal sellK;

	@Column(name = "SELL_D")
	private BigDecimal sellD;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getModelId() {
		return modelId;
	}

	public void setModelId(BigDecimal modelId) {
		this.modelId = modelId;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public BigDecimal getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(BigDecimal weekNumber) {
		this.weekNumber = weekNumber;
	}

	public Date getBuyBeginDate() {
		return buyBeginDate;
	}

	public void setBuyBeginDate(Date buyBeginDate) {
		this.buyBeginDate = buyBeginDate;
	}

	public Date getBuyEndDate() {
		return buyEndDate;
	}

	public void setBuyEndDate(Date buyEndDate) {
		this.buyEndDate = buyEndDate;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public Date getSellBeginDate() {
		return sellBeginDate;
	}

	public void setSellBeginDate(Date sellBeginDate) {
		this.sellBeginDate = sellBeginDate;
	}

	public Date getSellEndDate() {
		return sellEndDate;
	}

	public void setSellEndDate(Date sellEndDate) {
		this.sellEndDate = sellEndDate;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}

	public BigDecimal getAccumulativeProfitLoss() {
		return accumulativeProfitLoss;
	}

	public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
		this.accumulativeProfitLoss = accumulativeProfitLoss;
	}

	public BigDecimal getBuyK() {
		return buyK;
	}

	public void setBuyK(BigDecimal buyK) {
		this.buyK = buyK;
	}

	public BigDecimal getBuyD() {
		return buyD;
	}

	public void setBuyD(BigDecimal buyD) {
		this.buyD = buyD;
	}

	public BigDecimal getSellK() {
		return sellK;
	}

	public void setSellK(BigDecimal sellK) {
		this.sellK = sellK;
	}

	public BigDecimal getSellD() {
		return sellD;
	}

	public void setSellD(BigDecimal sellD) {
		this.sellD = sellD;
	}
	
	

}
