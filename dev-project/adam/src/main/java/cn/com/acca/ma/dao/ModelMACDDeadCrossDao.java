package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelClosePriceMA5DeadCross;
import cn.com.acca.ma.model.ModelMACDDeadCross;
import cn.com.acca.ma.model.ModelMACDGoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelMACDDeadCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD死叉模型算法，根据endDate日期，向表MDL_MACD_DEAD_CROSS插入数据
     * @param multithreading
     * @param endDate
     */
    void writeModelMACDDeadCrossIncr(boolean multithreading, String endDate);

    /*********************************************************************************************************************
     *
     * 												   	创建图表
     *
     *********************************************************************************************************************/
    /**
     * 获取MACD死叉算法在开始时间到结束时间内成功率的数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    @SuppressWarnings("rawtypes")
    List<SuccessRateOrPercentagePojo> getMACDDeadCrossSuccessRate(boolean multithreading, final String beginDate, final String endDate, Integer dateNumber);

    /**
     * 获取MACD死叉算法在开始时间到结束时间内，死叉股票的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<SuccessRateOrPercentagePojo> getMACDDeadCrossPercentage(boolean multithreading, final String beginDate, final String endDate);

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD死叉模型算法，向表MDL_MACD_DEAD_CROSS插入数据
     */
    void writeModelMACDDeadCross();

    /*********************************************************************************************************************
     *
     * 										增量备份表MDL_MACD_DEAD_CROSS
     *
     *********************************************************************************************************************/
    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_MACD_DEAD_CROSS中获取sell_date日的所有ModelMACDDeadCross对象
     * @param date
     * @return
     */
    List<ModelMACDDeadCross> getModelMACDDeadCrossByDate(String date);

    /**
     * 向表MDL_MACD_DEAD_CROSS中插入ModelMACDDeadCross对象
     * @param modelMACDDeadCross
     */
    void save(ModelMACDDeadCross modelMACDDeadCross);
}
