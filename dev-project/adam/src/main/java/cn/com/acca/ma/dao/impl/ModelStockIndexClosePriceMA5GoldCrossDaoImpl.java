package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelStockIndexClosePriceMA5GoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelStockIndexClosePriceMA5GoldCross;
import cn.com.acca.ma.model.ModelStockIndexMACDGoldCross;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import java.util.List;

public class ModelStockIndexClosePriceMA5GoldCrossDaoImpl extends BaseDaoImpl<ModelStockIndexClosePriceMA5GoldCrossDaoImpl> implements
        ModelStockIndexClosePriceMA5GoldCrossDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据
     */
    @Override
    public void writeModelStockIndexClosePriceMA5GoldCross() {
        logger.info("开始海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_INDEX.CAL_MDL_STOCK_INDEX_CP_MA5_GC()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据结束");
    }

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_c_p_ma5_g_c中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
    @Override
    public List<ModelStockIndexClosePriceMA5GoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode) {
        logger.info("开始根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_c_p_ma5_g_c中返回数据，并按照sell_date升序排列");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery("select t from ModelStockIndexClosePriceMA5GoldCross t " +
                "where t.stockIndexCode=? and t.type=? " +
                "order by t.sellDate asc");
        query.setParameter(0, indexCode);
        query.setParameter(1, type);
        List<ModelStockIndexClosePriceMA5GoldCross> modelStockIndexClosePriceMA5GoldCrossList = query.list();
        session.getTransaction().commit();
        session.close();

        logger.info("根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_c_p_ma5_g_c中返回数据，并按照sell_date升序排列完成");

        return modelStockIndexClosePriceMA5GoldCrossList;
    }
}
