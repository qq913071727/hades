package cn.com.acca.ma.service;

import cn.com.acca.ma.model.StockIndex;
import java.util.List;

public interface StockIndexService extends BaseService {
	/******************************************** 更新每天的数据 ********************************************************/
	/**
	 * 更新StockIndex对象某一日的数据
	 * 这个接口不能用了
	 * @param multithreading
	 */
	void writeStockIndexByDate(boolean multithreading);

	/**
	 * 调用baostock接口，更新StockIndex对象某一日的数据
	 */
	void writeStockIndexByDate_baoStock();

	/**
	 * 根据日期date，计算某一日所有指数的移动平均线数据
	 * @param multithreading
	 */
	void writeStockIndexMAByDate(boolean multithreading);

	/**
	 * 根据日期date，计算某一日所有指数的Hei Kin Ashi数据
	 * @param multithreading
	 */
	void writeStockIndexHeiKinAshiByDate(boolean multithreading);

	/**
	 * 根据日期date，计算所有指数某一日的乖离率
	 * @param multithreading
	 */
	void writeStockIndexBiasByDate(boolean multithreading);

	/**
	 * 更新表STOCK_INDEX中某一日的EMA15，EMA26，DIF和DEA字段
	 */
	void writeStockIndexMACDByDate();

	/**
	 * 更新表STOCK_INDEX中某一日的RSV，K和D字段
	 */
	void writeStockIndexKDByDate();
	
	/******************************************** 更新每天的图片 ********************************************************/
	/**
	 * 创建StockIndex对象的Hei Kin Ashi图片
	 * @param multithreading
	 */
	void createStockIndexHeiKinAshiPicture(boolean multithreading);
	
	/******************************************** 更新全部的数据 ********************************************************/
	void writeStockIndex(boolean multithreading);
	void writeIndexDataText(List<StockIndex> stockIndexList);
	List<StockIndex> readStockIndex(String url, String indexCode);

	/**
	 * 计算5、10、20、60、120、250日均线
	 */
	void writeStockIndexMA();
	
	/**
	 * 计算Hei Kin Ashi相关的字段
	 */
	void writeStockIndexHeiKinAshi();

	/**
	 * 计算所有指数的乖离率
	 */
	void writeStockIndexBias();

	/**
	 * 计算所有指数的MACD
	 */
	void writeStockIndexMACD();

	/**
	 * 计算所有指数的KD
	 */
	void writeStockIndexKD();
}
