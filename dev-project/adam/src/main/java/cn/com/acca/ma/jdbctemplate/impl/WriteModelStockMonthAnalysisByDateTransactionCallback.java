package cn.com.acca.ma.jdbctemplate.impl;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

public class WriteModelStockMonthAnalysisByDateTransactionCallback implements TransactionCallback {

    private static Logger logger = LogManager.getLogger(
        WriteModelStockMonthAnalysisByDateTransactionCallback.class);

    private JdbcTemplate jdbcTemplate;
    private String beginDate;
    private String endDate;

    public WriteModelStockMonthAnalysisByDateTransactionCallback() {
    }

    public WriteModelStockMonthAnalysisByDateTransactionCallback(JdbcTemplate jdbcTemplate, String beginDate, String endDate) {
        this.jdbcTemplate = jdbcTemplate;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public Object doInTransaction(TransactionStatus transactionStatus) {
        String sql = "{call PKG_MODEL_STOCK_ANALYSIS.write_mdl_s_m_a_by_date(p_begin_date => :p_begin_date, p_end_date => :p_end_date)}";
        jdbcTemplate.execute(sql,
            new CallableStatementCallback<List<Map<String, Object>>>() {
                @Override
                public List<Map<String, Object>> doInCallableStatement(
                    CallableStatement callableStatement) throws SQLException, DataAccessException {
                    List<Map<String, Object>> resultsMap = new ArrayList<>();
                    callableStatement.setString("p_begin_date", beginDate);
                    callableStatement.setString("p_end_date", endDate);
                    callableStatement.execute();
                    callableStatement.close();
                    return null;
                }
            });
        return null;
    }
}
