package cn.com.acca.ma.service;

import java.util.List;

import org.jfree.data.category.DefaultCategoryDataset;

import cn.com.acca.ma.model.BoardIndex;

public interface BoardIndexService extends BaseService {
	/**
	 * 计算BOARD_INDEX表中的数据
	 */
	void calculateBoardIndex();
	
	/**
	 * 计算BOARD_INDEX表中的FIVE_DAY_RATE字段和TEN_DAY_RATE字段
	 */
	void calculateAllBoardIndexFiveAndTenDayRate();

	/**
	 * 计算BOARD_INDEX表中某一日的FIVE_DAY_RATE字段和TEN_DAY_RATE字段
	 * @param multithreading
	 */
	void writeBoardIndexFiveAndTenDayRateByDate(boolean multithreading);
	
	/**
     * 计算BOARD_INDEX表的UP_DOWN_PERCENTAGE字段
     */
    void calculateBoardIndexUpDownPercentage();

	/**
	 * 计算BOARD_INDEX表中某一日的UP_DOWN_PERCENTAGE字段
	 * @param multithreading
	 */
	void writeBoardIndexUpDownPercentageByDate(boolean multithreading);
    
    /**
     * 计算BOARD_INDEX表的UP_DOWN_RANK字段
     */
    void calculateBoardIndexUpDownRank();

	/**
	 * 计算BOARD_INDEX表中某一日的UP_DOWN_RANK字段
	 * @param multithreading
	 */
	void writeBoardIndexUpDownRankByDate(boolean multithreading);

	/**
	 * 创建板块K线图
	 * @param multithreading
	 */
	void createBoardPicture(boolean multithreading);
	
	/**
	 * 返回从开始日期beginDate到结束日期endDate之间的BoardIndex数据
	 * @param beginDate
	 * @param endDate
	 * @return BoardIndex类型的List列表
	 */
	List<BoardIndex> getBoardIndexWithinDate(String beginDate,String endDate, String boardId);

	/**
	 * 根据开始时间，结束时间和板块ID绘制板块K线图
	 * @param multithreading
	 * @param boardBeginDate 开始时间
	 * @param boardEndDate 结束时间
	 * @param boardId 板块ID
	 * @param timeInterval 图片右边的间隔
	 */
	void paintBoardPicture(boolean multithreading, String boardBeginDate, String boardEndDate, String boardId, String timeInterval);

	/**
	 * 根据日期，计算board_index表中某一日的记录
	 * @param multithreading
	 */
	void writeBoardIndexByDate(boolean multithreading);
	
	/**
	 * 删除指定目录下的所有文件
	 * @param path
	 */
	void deleteAllFiles(String path);
	
	/**
	 * 查询某一天涨停的股票属于哪些板块
	 */
	void findLimitUpStockBoardByDate();

	/**
	 * 创建蜘蛛雷达图
	 * @param multithreading
	 */
	void createSpiderWebPlotPicture(boolean multithreading);

	/**
	 * 为创建蜘蛛雷达图而获取数据
	 * @param multithreading
	 * @return
	 */
	DefaultCategoryDataset getDefaultCategoryDatasetForSpiderWebPlotPicture(boolean multithreading);

	/**
	 * 创建板块收盘价图
	 * @param multithreading
	 */
	void createAllBoardIndexClosePicture(boolean multithreading);

	/**
	 * 根据开始日期和结束日期创建板块收盘价图。分别用来创建最近10年的图和半年的图
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param flag 创建最近10年图时，flag为true，图片名称中有all；创建半年图时，flag为false，图片名称中没有all
	 */
	void paintAllBoardIndexClosePicture(boolean multithreading, String beginDate, String endDate, boolean flag);

	/**
	 * 创建日线级别板块的5日和10的涨跌幅度图
	 * @param multithreading
	 */
	void createAllBoardIndexFiveAndTenDayRatePicture(boolean multithreading);

	/**
	 * 根据开始日期和结束日期创建所有板块的5日和10的涨跌幅度图。分别用来创建最近10年的图和半年的图
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param flag 创建最近10年图时，flag为true，图片名称中有all；创建半年图时，flag为false，图片名称中没有all
	 */
	void paintAllBoardIndexFiveAndTenDayRatePicture(boolean multithreading, String beginDate,String endDate,boolean flag);
	
	/**
	 * 实验图表
	 */
	void testPicture2();
	
	/**
	 * 实验图表
	 */
	void testPicture();
}
