package cn.com.acca.ma.service;

public interface StockIndexWeekendService extends BaseService {
	/******************************************** 更新每周的数据 ********************************************************/
	/**
	 * 根据日期，更新STOCK_INDEX_WEEKEND表的基础数据
	 */
	void writeStockIndexWeekendByDate();
	
	/**
	 * 根据日期，更新STOCK_INDEX_WEEKEND表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekendHeiKinAshiByDate();
	
	/******************************************** 更新每周的图片 ********************************************************/
	/**
	 * 创建StockIndexWeek对象的Hei Kin Ashi图片
	 */
	void createStockIndexWeekendHeiKinAshiPicture();
	
	/******************************************** 更新全部的数据 ********************************************************/
	/**
	 * 更新STOCK_INDEX_WEEKEND表的基础数据
	 */
	void writeStockIndexWeekend();
	
	/**
	 * 更新STOCK_INDEX_WEEK表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekHeiKinAshi();
}
