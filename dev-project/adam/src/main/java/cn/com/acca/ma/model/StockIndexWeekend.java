package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STOCK_INDEX_WEEKEND")
public class StockIndexWeekend implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="INDEX_CODE")
	private String indexCode;
	
	@Id
	@Column(name="INDEX_BEGIN_DATE")
	private Date indexBeginDate;
	
	@Id
	@Column(name="INDEX_END_DATE")
	private Date indexEndDate;
	
	@Column(name="INDEX_OPEN")
	private BigDecimal indexOpen;
	
	@Column(name="INDEX_CLOSE")
	private BigDecimal indexClose;
	
	@Column(name="INDEX_HIGH")
	private BigDecimal indexHigh;
	
	@Column(name="INDEX_LOW")
	private BigDecimal indexLow;
	
	@Column(name="AMOUNT")
	private BigDecimal amount;
	
	@Column(name="HA_INDEX_OPEN")
	private BigDecimal haIndexOpen;
	
	@Column(name="HA_INDEX_CLOSE")
	private BigDecimal haIndexClose;
	
	@Column(name="HA_INDEX_HIGH")
	private BigDecimal haIndexHigh;
	
	@Column(name="HA_INDEX_LOW")
	private BigDecimal haIndexLow;

	public String getIndexCode() {
		return indexCode;
	}

	public void setIndexCode(String indexCode) {
		this.indexCode = indexCode;
	}

	public Date getIndexBeginDate() {
		return indexBeginDate;
	}

	public void setIndexBeginDate(Date indexBeginDate) {
		this.indexBeginDate = indexBeginDate;
	}

	public Date getIndexEndDate() {
		return indexEndDate;
	}

	public void setIndexEndDate(Date indexEndDate) {
		this.indexEndDate = indexEndDate;
	}

	public BigDecimal getIndexOpen() {
		return indexOpen;
	}

	public void setIndexOpen(BigDecimal indexOpen) {
		this.indexOpen = indexOpen;
	}

	public BigDecimal getIndexClose() {
		return indexClose;
	}

	public void setIndexClose(BigDecimal indexClose) {
		this.indexClose = indexClose;
	}

	public BigDecimal getIndexHigh() {
		return indexHigh;
	}

	public void setIndexHigh(BigDecimal indexHigh) {
		this.indexHigh = indexHigh;
	}

	public BigDecimal getIndexLow() {
		return indexLow;
	}

	public void setIndexLow(BigDecimal indexLow) {
		this.indexLow = indexLow;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getHaIndexOpen() {
		return haIndexOpen;
	}

	public void setHaIndexOpen(BigDecimal haIndexOpen) {
		this.haIndexOpen = haIndexOpen;
	}

	public BigDecimal getHaIndexClose() {
		return haIndexClose;
	}

	public void setHaIndexClose(BigDecimal haIndexClose) {
		this.haIndexClose = haIndexClose;
	}

	public BigDecimal getHaIndexHigh() {
		return haIndexHigh;
	}

	public void setHaIndexHigh(BigDecimal haIndexHigh) {
		this.haIndexHigh = haIndexHigh;
	}

	public BigDecimal getHaIndexLow() {
		return haIndexLow;
	}

	public void setHaIndexLow(BigDecimal haIndexLow) {
		this.haIndexLow = haIndexLow;
	}

}
