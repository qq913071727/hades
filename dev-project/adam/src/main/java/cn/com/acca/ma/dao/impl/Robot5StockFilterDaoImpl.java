package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.constant.StockInfoMark;
import cn.com.acca.ma.dao.Robot5StockFilterDao;

public class Robot5StockFilterDaoImpl extends BaseDaoImpl<Robot5StockFilterDao> implements
        Robot5StockFilterDao {

    public Robot5StockFilterDaoImpl() {
        super();
    }

    /**
     * 清空robot5_stock_filter表
     */
    @Override
    public void truncateTableRobotStockFilter() {
        logger.info("清空robot5_stock_filter表");

        String sql = "truncate table robot5_stock_filter";

        doSQLInTransaction(sql);
    }

    /**
     * 不考虑做多还是做空，插入所有股票代码
     * @param date
     */
    public void insertAllStockBollByDate(String date) {
        logger.info("不考虑做多还是做空，插入所有股票代码");

        String sql = "insert into robot5_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_transaction_data_all t "
                + "where t.date_ = to_date('" + date + "', 'yyyy-mm-dd') "
                + "and t.code_ not in(select t2.stock_code from robot5_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null))";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：如果当前交易日和前n个交易日的收盘价都大于/小于布林带上轨/下轨，并且当前交易日的收盘价小于/大于前一个交易日的收盘价，则保留
     * @param date
     * @param dateNumber
     */
    @Override
    public void filterByUpDownBoll(String date, Integer dateNumber){
        logger.info("过滤条件：如果当前交易日【" + date + "】和前【" + (dateNumber - 1) + "】个交易日的收盘价都大于/小于布林带上轨/下轨，并且当前交易日的收盘价小于/大于前一个交易日的收盘价，则保留");

        String sql = "{call PKG_ROBOT5.filter_by_up_down_boll('" + date + "', " + dateNumber + ")}";

        doSQLInTransaction(sql);
    }
    
    /**
     * 过滤条件：如果上一周这只股票的最高价/最低价突破/跌破周线级别布林带上轨/下轨，则保留
     * @param date 
     */
    @Override
    public void filterByLastWeekUpDownBoll(String date){
        logger.info("过滤条件：如果上一周这只股票的最高价/最低价突破/跌破周线级别布林带上轨/下轨，则保留");

        String sql = "{call PKG_ROBOT5.filter_by_l_w_up_down_boll('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 根据date，从stock_transaction_data表中，向robot5_stock_filter表中插入股票代码
     * 用于做多
     * 注意：当日已经卖出的股票不能再买入
     *
     * @param date
     */
    @Override
    public void insertBullStockCodeFromStockTransactionDataByDate(String date) {
        logger.info("根据date【" + date + "】，从stock_transaction_data_all表中，向robot5_stock_filter表中插入股票代码。用于做多");

        String sql = "insert into robot5_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_transaction_data_all t "
                + "where t.date_ = to_date('" + date + "', 'yyyy-mm-dd') " +
                "and t.code_ not in(select t2.stock_code from robot5_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null) " +
                "or t2.sell_date=to_date('" + date + "','yyyy-mm-dd') " +
                "or t2.buy_date=to_date('" + date + "','yyyy-mm-dd'))";

        doSQLInTransaction(sql);
    }

    /**
     * 根据date，从stock_transaction_data表中，向robot5_stock_filter表中插入股票代码
     * 用于做空
     * 注意：当日已经还券的股票不能再融券
     *
     * @param date
     */
    @Override
    public void insertShortStockCodeFromStockTransactionDataByDate(String date) {
        logger.info("根据date【" + date + "】，从stock_transaction_data_all表中，向robot5_stock_filter表中插入股票代码。用于做空");

        String sql = "insert into robot5_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_transaction_data_all t "
                + "join stock_info si on si.code_=t.code_ and si.mark='R' "
                + "where t.date_ = to_date('" + date + "', 'yyyy-mm-dd') " +
                "and t.code_ not in(select t2.stock_code from robot5_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null) " +
                "or t2.sell_date=to_date('" + date + "','yyyy-mm-dd') " +
                "or t2.buy_date=to_date('" + date + "','yyyy-mm-dd'))";

        doSQLInTransaction(sql);
    }

    /**
     * 只插入收盘价跌破周线级别布林带下轨的股票记录
     * 用于做多
     *
     * @param beginDate
     * @param endDate
     */
    public void insertStockWeekBollDnByDate(String beginDate, String endDate) {
        logger.info("只插入收盘价跌破周线级别布林带下轨的股票记录");

        String sql = "insert into robot5_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_week t "
                + "where t.begin_date >= to_date('" + beginDate + "', 'yyyy-mm-dd') " +
                "and t.end_date <= to_date('" + endDate + "', 'yyyy-mm-dd') " +
                "and t.close_price <= t.dn_ " +
                "and t.code_ not in(select t2.stock_code from robot5_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null))";

        doSQLInTransaction(sql);
    }

    /**
     * 只插入收盘价突破周线级别布林带上轨的股票记录
     * 用于做空
     *
     * @param beginDate
     * @param endDate
     */
    public void insertStockWeekBollUpByDate(String beginDate, String endDate) {
        logger.info("只插入收盘价突破周线级别布林带上轨的股票记录");

        String sql = "insert into robot5_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_week t "
                + "join stock_info si on si.code_=t.code_ and si.mark='" + StockInfoMark.SECURITIES_LENDING + "' "
                + "where t.begin_date >= to_date('" + beginDate + "', 'yyyy-mm-dd') " +
                "and t.end_date <= to_date('" + endDate + "', 'yyyy-mm-dd') " +
                "and t.close_price >= t.up " +
                "and t.code_ not in(select t2.stock_code from robot5_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null))";

        doSQLInTransaction(sql);
    }

    /**
     * 只保留这个价格区间以内的股票
     *
     * @param date
     * @param closePriceStart
     * @param closePriceEnd
     */
    @Override
    public void filterByLessThanClosePrice(String date, Double closePriceStart, Double closePriceEnd) {
        logger.info("收盘价在【" + closePriceStart + "】和【" + closePriceEnd + "】价格区间以外的股票都被删除");

        String sql = "delete from robot5_stock_filter where stock_code not in("
                + "select t.code_ from stock_transaction_data_all t "
                + "where t.date_=to_date('" + date + "','yyyy-mm-dd') and t.close_price between " + closePriceStart + " and " + closePriceEnd + ")";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=阈值
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价小于年线的记录数（数量更多）/收盘价大于年线的记录数（数量更少）<=阈值
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public void filterByClosePriceGreatThenLessThenMA250(String beginDate, String endDate, Double rate) {
        logger.info("过滤条件：只选择震荡行情：日线级别，某一段时间内【" + beginDate + "】和【" + endDate + "】，" +
                "收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=" + rate);

        String sql = "{call PKG_ROBOT5.filter_by_c_p_gt_lt_ma250('" + beginDate + "', '" + endDate + "', " + rate + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除除过权的股票
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public void filterByXr(String beginDate, String endDate) {
        logger.info("过滤条件：删除除过权的股票，时间从【" + beginDate + "】至【" + endDate + "】");

        String sql = "{call PKG_ROBOT5.filter_by_xr('" + beginDate + "', '" + endDate + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：周线级别KD金叉
     *
     * @param date
     */
    @Override
    public void filterByWeekKDGoldCross(String date) {
        logger.info("过滤条件：周线级别KD金叉");

        String sql = "{call PKG_ROBOT5.filter_by_week_kd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MACD金叉
     *
     * @param date
     */
    @Override
    public void filterByMACDGoldCross(String date) {
        logger.info("过滤条件：MACD金叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_macd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MACD金叉
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByMACDDeadCross(String date) {
        logger.info("过滤条件：MACD死叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_macd_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，KD死叉的股票
     * 用于做多
     * @param date
     */
    @Override
    public void filterByStockWeekKDGoldCross(String date) {
        logger.info("过滤条件：删除周线级别，KD死叉的股票，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2 where t2.id_ in(select t.id_ from robot5_stock_filter t " +
                "join stock_week sw on sw.code_=t.stock_code " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and sw.k<sw.d)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，KD金叉的股票
     * 用于做空
     * @param date
     */
    @Override
    public void filterByStockWeekKDDeadCross(String date) {
        logger.info("过滤条件：删除周线级别，KD金叉的股票，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2 where t2.id_ in(select t.id_ from robot5_stock_filter t " +
                "join stock_week sw on sw.code_=t.stock_code " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and sw.k>sw.d)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周KD死叉的股票
     * @param date
     */
    @Override
    public void deleteLastWeekKDDeadCross(String date) {
        logger.info("过滤条件：删除周线级别，上一周KD死叉的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_last_week_kd_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周close_price死叉ma5的股票
     * @param date
     */
    @Override
    public void deleteLastWeekClosePriceDeadCrossMa5(String date) {
        logger.info("删除周线级别，上一周close_price死叉ma5的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_last_week_c_p_d_c_ma5('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周close_price金叉ma5的股票
     * @param date
     */
    @Override
    public void deleteLastWeekClosePriceGoldCrossMa5(String date) {
        logger.info("过滤条件：删除周线级别，上一周close_price金叉ma5的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_last_week_c_p_g_c_ma5('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价大于上上周hei_kin_ashi收盘价的股票
     * @param date
     */
    @Override
    public void deleteLastWeekHeiKinAshiClosePriceGreatThanLastTwoWeekHeiKinAshiClosePrice(String date) {
        logger.info("过滤条件：删除周线级别，上一周close_price金叉ma5的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_lw_hka_cp_gt_ltw_hka_cp('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价小于上上周hei_kin_ashi收盘价的股票
     * @param date
     */
    @Override
    public void deleteLastWeekHeiKinAshiClosePriceLessThanLastTwoWeekHeiKinAshiClosePrice(String date) {
        logger.info("过滤条件：删除周线级别，上一周close_price金叉ma5的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_lw_hka_cp_lt_ltw_hka_cp('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价大于上周hei_kin_ashi收盘价的股票
     * @param date
     */
    @Override
    public void deleteThisWeekHeiKinAshiClosePriceGreatThanLastWeekHeiKinAshiClosePrice(String date) {
        logger.info("过滤条件：删除周线级别，本周hei_kin_ashi收盘价大于上周hei_kin_ashi收盘价的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_tw_hka_cp_gt_lw_hka_cp('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价小于上周hei_kin_ashi收盘价的股票
     * @param date
     */
    @Override
    public void deleteThisWeekHeiKinAshiClosePriceLessThanLastWeekHeiKinAshiClosePrice(String date) {
        logger.info("过滤条件：删除周线级别，本周hei_kin_ashi收盘价小于上周hei_kin_ashi收盘价的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_tw_hka_cp_lt_lw_hka_cp('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价是阳线的股票
     * @param date
     */
    @Override
    public void deleteThisWeekHeiKinAshiClosePricePositiveLine(String date) {
        logger.info("过滤条件：删除周线级别， 本周hei_kin_ashi收盘价是阳线的股票，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2 where t2.id_ in(select t.id_ from robot5_stock_filter t " +
                "join stock_week sw on sw.code_=t.stock_code " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and sw.ha_week_close_price>sw.ha_week_open_price)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价是阴线的股票
     * @param date
     */
    @Override
    public void deleteThisWeekHeiKinAshiClosePriceNegativeLine(String date) {
        logger.info("过滤条件：删除周线级别，本周hei_kin_ashi收盘价是阴线的股票，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2 where t2.id_ in(select t.id_ from robot5_stock_filter t " +
                "join stock_week sw on sw.code_=t.stock_code " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and sw.ha_week_close_price<sw.ha_week_open_price)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价是阳线的股票
     * @param date
     */
    @Override
    public void deleteLastWeekHeiKinAshiClosePricePositiveLine(String date) {
        logger.info("过滤条件：删除周线级别，上一周hei_kin_ashi收盘价是阳线的股票，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2.stock_code not in(select rsf.stock_code from robot5_stock_filter rsf, stock_week sw, " +
                "(select t1.begin_date, t1.end_date from ( " +
                "select * from stock_index_week siw where siw.end_date<to_date('" + date + "','yyyy-mm-dd') order by siw.end_date desc) t1 " +
                "where rownum<=1) date_ " +
                "where sw.code_=rsf.stock_code and sw.begin_date>=date_.begin_date " +
                "and sw.end_date<=date_.end_date and sw.ha_week_open_price<sw.ha_week_close_price)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价是阴线的股票
     * @param date
     */
    @Override
    public void deleteLastWeekHeiKinAshiClosePriceNegativeLine(String date) {
        logger.info("过滤条件：删除周线级别，上一周hei_kin_ashi收盘价是阴线的股票，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2.stock_code not in(select rsf.stock_code from robot5_stock_filter rsf, stock_week sw, " +
                "(select t1.begin_date, t1.end_date from ( " +
                "select * from stock_index_week siw where siw.end_date<to_date('" + date + "','yyyy-mm-dd') order by siw.end_date desc) t1 " +
                "where rownum<=1) date_ " +
                "where sw.code_=rsf.stock_code and sw.begin_date>=date_.begin_date " +
                "and sw.end_date<=date_.end_date and sw.ha_week_open_price>sw.ha_week_close_price)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别，上一周KD金叉的股票
     * @param date
     */
    @Override
    public void deleteLastWeekKDGoldCross(String date) {
        logger.info("过滤条件：删除周线级别，上一周KD金叉的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_last_week_kd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别close_price死叉ma5
     * 用于做多
     * @param date
     */
    @Override
    public void filterByStockWeekClosePriceMa5GoldCross(String date) {
        logger.info("过滤条件：删除周线级别close_price死叉ma5，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2 where t2.id_ in(select t.id_ from robot5_stock_filter t " +
                "join stock_week sw on sw.code_=t.stock_code " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and sw.close_price<sw.ma5)";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除周线级别close_price金叉ma5
     * 用于做空
     * @param date
     */
    @Override
    public void filterByStockWeekClosePriceMa5DeadCross(String date) {
        logger.info("过滤条件：删除周线级别close_price金叉ma5，日期【" + date + "】");

        String sql = "delete from robot5_stock_filter t2 where t2.id_ in(select t.id_ from robot5_stock_filter t " +
                "join stock_week sw on sw.code_=t.stock_code " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and sw.close_price>sw.ma5)";

        doSQLInTransaction(sql);
    }

    /**
     * 删除本周close_price比上一周close_price大的股票
     * @param date
     */
    @Override
    public void deleteThisWeekClosePriceGreatThanLastWeekClosePrice(String date) {
        logger.info("删除本周close_price比上一周close_price大的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_t_w_cp_gt_l_w_cp('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 删除本周close_price比上一周close_price小的股票
     * @param date
     */
    @Override
    public void deleteThisWeekClosePriceLessThanLastWeekClosePrice(String date) {
        logger.info("删除本周close_price比上一周close_price小的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_t_w_cp_lt_l_w_cp('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除上周K比上上周K小的股票
     * @param date
     */
    @Override
    public void deleteLastWeekKDownThanLastTwoWeekK(String date) {
        logger.info("过滤条件：删除上周K比上上周K小的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_l_w_k_down_than_l_t_w_k('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除这周K比上周K小的股票
     * @param date
     */
    @Override
    public void deleteThisWeekKDownThanLastTwoWeekK(String date) {
        logger.info("过滤条件：删除这周K比上周K小的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_t_w_k_down_than_l_w_k('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除上周K比上上周K大的股票
     * @param date
     */
    @Override
    public void deleteLastWeekKUpThanLastTwoWeekK(String date) {
        logger.info("过滤条件：删除上周K比上上周K大的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_l_w_k_up_than_l_t_w_k('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除这周K比上周K大的股票
     * @param date
     */
    @Override
    public void deleteThisWeekKUpThanLastTwoWeekK(String date) {
        logger.info("过滤条件：删除这周K比上周K大的股票，日期【" + date + "】");

        String sql = "{call PKG_ROBOT5.delete_t_w_k_up_than_l_w_k('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：当前收盘价与某段时间最高价的百分比
     * 通常和filterByXr方法一起使用
     *
     * @param beginDate
     * @param date
     * @param percentage
     */
    @Override
    public void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(String beginDate, String date, Integer percentage) {
        logger.info("过滤条件：当前收盘价与某段时间最高价的百分比。beginDate为【" + beginDate + "】，"
                + "date为【" + date + "】，percentage为【" + percentage + "】");

        String sql = "{call PKG_ROBOT5.filter_p_o_c_c_p_c_t_s_t_h_p('" + beginDate + "', '" + date + "', " + percentage + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：当前收盘价与某段时间最低价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做空
     *
     * @param beginDate
     * @param date
     * @param percentage
     */
    @Override
    public void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(String beginDate, String date, Integer percentage) {
        logger.info("过滤条件：当前收盘价与某段时间最低价的百分比。beginDate为【" + beginDate + "】，"
                + "date为【" + date + "】，percentage为【" + percentage + "】");

        String sql = "{call PKG_ROBOT5.filter_p_o_c_c_p_c_t_s_t_l_p('" + beginDate + "', '" + date + "', " + percentage + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MA不单调递减，过滤股票
     * 用于做多
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    @Override
    public void filterByMANotDecreasing(String date, Integer maLevel, String dateNumber) {
        logger.info("过滤条件：MA不单调递减，过滤股票。date为【" + date + "】，maLevel为【" + maLevel + "】,"
                + "dateNumber为【" + dateNumber + "】");

        String sql = "{call PKG_ROBOT5.filter_by_ma_not_decreasing(" + maLevel + ", '" + date + "', " + dateNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MA不单调递增，过滤股票
     * 用于多空
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    @Override
    public void filterByMANotIncreasing(String date, Integer maLevel, String dateNumber) {
        logger.info("过滤条件：MA不单调递增，过滤股票。date为【" + date + "】，maLevel为【" + maLevel + "】,"
                + "dateNumber为【" + dateNumber + "】");

        String sql = "{call PKG_ROBOT5.filter_by_ma_not_increasing(" + maLevel + ", '" + date + "', " + dateNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留close_price金叉ma的股票
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByClosePriceGoldCrossMA5(String date) {
        logger.info("过滤条件：保留close_price金叉ma的股票。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_c_p_g_c_ma5('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留close_price死叉ma的股票
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByClosePriceDeadCrossMA5(String date) {
        logger.info("过滤条件：保留close_price死叉ma的股票。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_c_p_d_c_ma5('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：平均K线从下跌趋势变为上涨趋势
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByHeiKinAshiUpDown(String date) {
        logger.info("过滤条件：平均K线从下跌趋势变为上涨趋势。date为【" + date + "】");

        // 将当天除了ha_close_price大于ha_open_price以外的股票都删掉
        String closePriceGreatThanOpenPriceSql = "delete from robot5_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot5_stock_filter rsf on (std.code_=rsf.stock_code "
                + "and std.date_=to_date('" + date + "','yyyy-mm-dd') and std.ha_close_price >= std.ha_open_price))";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);

        // 将前一天除了ha_close_price小于ha_open_price以外的股票都删掉
        String closePriceLessThanOpenPriceSql = "delete from robot5_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot5_stock_filter rsf "
                + "on std.code_ = rsf.stock_code and std.date_ = "
                + "(select b.date_ from ( "
                + "select * from stock_transaction_data_all std1 "
                + "where std1.date_ < to_date('" + date + "', 'yyyy-mm-dd') order by std1.date_ desc) b "
                + "where rownum <= 1) "
                + "and std.ha_close_price <= std.ha_open_price)";
        doSQLInTransaction(closePriceLessThanOpenPriceSql);
    }

    /**
     * 过滤条件：平均K线从上涨趋势变为下跌趋势
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByHeiKinAshiDownUp(String date) {
        logger.info("过滤条件：平均K线从上涨趋势变为下跌趋势。date为【" + date + "】");

        // 将当天除了ha_close_price小于ha_open_price以外的股票都删掉
        String closePriceGreatThanOpenPriceSql = "delete from robot5_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot5_stock_filter rsf on (std.code_=rsf.stock_code "
                + "and std.date_=to_date('" + date + "','yyyy-mm-dd') and std.ha_close_price <= std.ha_open_price))";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);

        // 将前一天除了ha_close_price大于ha_open_price以外的股票都删掉
        String closePriceLessThanOpenPriceSql = "delete from robot5_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot5_stock_filter rsf "
                + "on std.code_ = rsf.stock_code and std.date_ = "
                + "(select b.date_ from ( "
                + "select * from stock_transaction_data_all std1 "
                + "where std1.date_ < to_date('" + date + "', 'yyyy-mm-dd') order by std1.date_ desc) b "
                + "where rownum <= 1) "
                + "and std.ha_close_price >= std.ha_open_price)";
        doSQLInTransaction(closePriceLessThanOpenPriceSql);
    }

    /**
     * 过滤条件：保留KD金叉的股票
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByKDGoldCross(String date) {
        logger.info("过滤条件：保留KD金叉的股票。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_kd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留KD死叉的股票
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByKDDeadCross(String date) {
        logger.info("过滤条件：保留KD死叉的股票。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_kd_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留所有金叉
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByAllGoldCross(String date) {
        logger.info("过滤条件：保留所有金叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_all_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留所有死叉
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByAllDeadCross(String date) {
        logger.info("过滤条件：保留所有死叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT5.filter_by_all_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留所有周线级别收盘价小于布林带中轨的股票
     * 用于做多
     * @param date
     */
    @Override
    public void filterWeekClosePriceDownBollMB(String date) {
        logger.info("过滤条件：保留所有周周线级别收盘价小于布林带中轨的股票");

        // 将当天除了ha_close_price小于ha_open_price以外的股票都删掉
        String closePriceGreatThanOpenPriceSql = "delete from robot5_stock_filter t where t.stock_code in " +
                "(select sw.code_ from stock_week sw " +
                "where sw.mb<sw.close_price " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date)";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);
    }

    /**
     * 过滤条件：保留所有周线级别收盘价大于布林带中轨的股票
     * 用于多空
     * @param date
     */
    @Override
    public void filterWeekClosePriceUpBollMB(String date) {
        logger.info("过滤条件：保留所有周周线级别收盘价大于布林带中轨的股票");

        // 将当天除了ha_close_price小于ha_open_price以外的股票都删掉
        String closePriceGreatThanOpenPriceSql = "delete from robot5_stock_filter t where t.stock_code in " +
                "(select sw.code_ from stock_week sw " +
                "where sw.mb>sw.close_price " +
                "and to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date)";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);
    }

    /**
     * 返回robot5_stock_filter表的记录数
     *
     * @return
     */
    @Override
    public Integer count() {
        logger.info("返回robot5_stock_filter表的记录数");

        String sql = "select count(*) from robot5_stock_filter";

        return doSQLQueryInTransaction(sql, null);
    }
}
