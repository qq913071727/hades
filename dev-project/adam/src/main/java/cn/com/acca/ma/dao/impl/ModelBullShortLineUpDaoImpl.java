package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelBullShortLineUpDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import org.hibernate.SQLQuery;

public class ModelBullShortLineUpDaoImpl extends BaseDaoImpl<ModelBullShortLineUpDaoImpl> implements
        ModelBullShortLineUpDao {

    /**
     * 海量地向表MDL_BULL_SHORT_LINE_UP中写入数据
     */
    @Override
    public void writeModelBullShortLineUp() {
        logger.info("开始海量地向表MDL_BULL_SHORT_LINE_UP中写入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_BULL_SHORT_LINE_UP()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("海量地向表MDL_BULL_SHORT_LINE_UP中写入数据结束");
    }

    /**
     * 增量地向表MDL_BULL_SHORT_LINE_UP中写入数据
     * @param date
     */
    @Override
    public void writeModelBullShortLineUpIncr(String date) {
        logger.info("开始增量地向表MDL_BULL_SHORT_LINE_UP中写入数据");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_B_S_LINE_UP_INCR(?)}");
        query.setParameter(0, date);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("增量地向表MDL_BULL_SHORT_LINE_UP中写入数据结束");
    }


}
