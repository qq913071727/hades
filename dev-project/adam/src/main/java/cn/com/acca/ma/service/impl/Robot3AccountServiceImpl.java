package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.Robot3Account;
import cn.com.acca.ma.model.RobotAccount;
import cn.com.acca.ma.service.Robot3AccountService;
import cn.com.acca.ma.service.RobotAccountService;

import java.util.List;

public class Robot3AccountServiceImpl extends BaseServiceImpl<Robot3AccountServiceImpl, Robot3Account> implements
        Robot3AccountService {

    public Robot3AccountServiceImpl() {
        super();
    }

    /**************************************** 卖股票/买股票 ********************************************/
    /**
     * 根据当日持有股票的收盘价，在卖股票/买股票之前，更新robot_account表
     */
    @Override
    public void updateRobotAccountBeforeSellOrBuy(){
        logger.info("更新robot_account表的stock_assets、total_assets字段");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.update.robot.account.before.sell.date");

        // 更新robot_account表
        robotAccountDao.updateRobotAccountBeforeSellOrBuy(date);
    }

    /**
     * 根据当日卖出/买入股票的收盘价，在卖股票/买股票之后，更新robot_account表
     */
    @Override
    public void updateRobotAccountAfterSellOrBuy() {
        logger.info("更新robot_account表的hold_stock_number、stock_assets、capital_assets字段");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.update.robot.account.after.sell.buy.date");

        // 更新robot_account表
        robotAccountDao.updateRobotAccountAfterSellOrBuy(date);
    }

    /**************************************** 买股票/卖股票 ********************************************/
    /**
     * 根据当日买入股票/卖出股票的收盘价，在买股票/卖股票之后，更新robot_account表
     */
    @Override
    public void updateRobotAccountAfterBuyOrSell() {
        logger.info("更新robot_account表的hold_stock_number、stock_assets、capital_assets字段");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.update.robot.account.after.buy.sell.date");

        // 更新robot_account表的total_assets字段
        robotAccountDao.updateRobotAccountAfterBuyOrSell(date);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
