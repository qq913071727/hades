package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建从开始日期至结束日期内MACD处于金叉状态（dif>dea）的股票的比率的图表
 */
public class CreateMACDGoldCrossStockRatePictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateMACDGoldCrossStockRatePictureThread.class);

    private StockTransactionDataService stockTransactionDataService;

    public CreateMACDGoldCrossStockRatePictureThread() {
    }

    public CreateMACDGoldCrossStockRatePictureThread(
        StockTransactionDataService stockTransactionDataService) {
        this.stockTransactionDataService = stockTransactionDataService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始生成从开始日期至结束日期内MACD处于金叉状态（dif>dea）的股票的比率的图表，"
            + "调用的方法为【createMACDGoldCrossStockRatePicture】");

        stockTransactionDataService.createMACDGoldCrossStockRatePicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
