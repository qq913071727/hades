package cn.com.acca.ma.service;

import java.util.Date;
import java.util.List;

public interface ModelClosePriceMA5GoldCrossService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     */
    void writeModelClosePriceMA5GoldCross();

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     */
    void writeModelClosePriceMA5GoldCrossIncr();

    /*********************************************************************************************************************
     *
     * 												创建图片
     *
     *********************************************************************************************************************/
    /**
     * 创建close_price金叉ma5的成功率图
     * @param multithreading
     */
    void createClosePriceMa5GoldCrossSuccessRatePicture(boolean multithreading);


}
