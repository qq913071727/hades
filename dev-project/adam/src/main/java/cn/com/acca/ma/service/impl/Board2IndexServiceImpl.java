package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.HttpClientUtil;
import cn.com.acca.ma.constant.Board2Type;
import cn.com.acca.ma.model.Board2Index;
import cn.com.acca.ma.model.Board2IndexDetail;
import cn.com.acca.ma.service.Board2IndexService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.*;

@Slf4j
public class Board2IndexServiceImpl extends BaseServiceImpl<Board2IndexServiceImpl, Board2Index> implements Board2IndexService {

    public Board2IndexServiceImpl() {
        super();
    }

    /**
     * 每天添加Board2Index对象
     */
    @Override
    public void writeBoard2IndexDaily() {
        log.info("每天添加Board2Index对象");

        int pageNo = 1;
        boolean hasData = true;

        String boardIndexDetailUrl = "http://stock.jrj.com.cn/concept/conceptdetail/conceptStock_#{boardIndexAbbr}.js";
        Document boardIndexDocument;
        String boardIndexDetailString;
        try {
            while (hasData){
                String boardIndexUrl = "http://stock.jrj.com.cn/action/concept/queryConceptHQ.jspa?sort=todaypl&order=desc&pn=" + pageNo + "&ps=30&_dc=";
                boardIndexDocument = Jsoup.connect(boardIndexUrl + new Date().getTime()).get();
                Elements bodyElements = boardIndexDocument.select("body");
                List<Node> nodeList = bodyElements.get(0).childNodes();
                String content = nodeList.get(0).toString();
                String jsonString = content.trim().replace("var _stock_concept=", "").replace(";", "");
                JSONObject jsonObject = JSON.parseObject(jsonString);
                JSONArray dataJSONArray = jsonObject.getJSONArray("data");
                if (null != dataJSONArray && dataJSONArray.size() == 0) {
                    return;
                }
                log.info(dataJSONArray.toString());
                if (null != dataJSONArray && dataJSONArray.size() > 0) {
                    List<Board2Index> board2IndexList = new ArrayList<>();
                    for (int i = 0; i < dataJSONArray.size(); i++) {
                        JSONArray jsonArray = dataJSONArray.getJSONArray(i);
                        Board2Index board2Index = new Board2Index();
                        board2Index.setBoardName(jsonArray.getString(2));
                        board2Index.setLimitUpNumber(jsonArray.getInteger(15));
                        board2Index.setLimitDownNumber(jsonArray.getInteger(16));
                        board2Index.setCurrentDayChangeRange(jsonArray.getDouble(9));
                        board2Index.setTransactionDate(new Date());
                        board2Index.setTransactionDate(DateUtil.stringToDate(DateUtil.dateToString(new Date())));
                        board2Index.setFiveMinuteChangeRange(jsonArray.getDouble(11));
                        board2Index.setFifteenMinuteChangeRange(jsonArray.getDouble(10));
                        board2Index.setFiveDayChangeRange(jsonArray.getDouble(12));
                        board2Index.setMajorCapitalInflow(jsonArray.getBigDecimal(8));
                        board2Index.setTopContributeStockName(jsonArray.getString(5));
                        board2Index.setTopContributeStockClosePrice(jsonArray.getDouble(6));
                        board2Index.setTopContributeStockChangeRange(jsonArray.getDouble(7));
                        board2IndexDao.saveBoard2Index(board2Index);
                        log.info(board2Index.toString());

                        String boardIndexAbbr = (String) jsonArray.get(1);
                        boardIndexDetailString = HttpClientUtil.get(boardIndexDetailUrl.replace("#{boardIndexAbbr}", boardIndexAbbr));
                        String boardIndexDetailJSONString = boardIndexDetailString.trim().replace("var conceptstockdata=", "").replace(";", "");
                        JSONObject boardIndexDetailJsonObject = JSON.parseObject(boardIndexDetailJSONString);
                        JSONArray board2IndexDetailDataJSONArray = boardIndexDetailJsonObject.getJSONArray("stockData");
                        if (null != board2IndexDetailDataJSONArray && board2IndexDetailDataJSONArray.size() > 0) {
                            for (Object object : board2IndexDetailDataJSONArray) {
                                JSONArray board2IndexDetailJSONArray = (JSONArray) object;
                                Board2IndexDetail board2IndexDetail = new Board2IndexDetail();
                                board2IndexDetail.setStockCode(board2IndexDetailJSONArray.getString(0));
                                board2IndexDetail.setStockName(board2IndexDetailJSONArray.getString(1));
                                board2IndexDetail.setCorrelationCause(board2IndexDetailJSONArray.getString(2));
                                JSONArray belongOtherConceptJSONArray = board2IndexDetailJSONArray.getJSONArray(3);
                                board2IndexDetail.setBelongOtherConcept(belongOtherConceptJSONArray.toJSONString());
                                board2IndexDetail.setTransactionDate(DateUtil.stringToDate(DateUtil.dateToString(new Date())));
                                board2IndexDetail.setBoard2IndexId(board2Index.getId());
                                board2IndexDetailDao.saveBoard2IndexDetail(board2IndexDetail);
                                log.info(board2IndexDetailJSONArray.toString());
                            }
                        }
                    }
                }
                pageNo++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void writeBoard2IndexDaily() {
//        log.info("每天添加Board2Index对象");
//        try {
//            Document document = Jsoup.connect("https://app.finance.ifeng.com/list/all_stock_cate.php").get();
//            log.debug(document.toString());
//
//            Elements mainElements = document.select("div[class='main']");
//            Elements tbodyElements = mainElements.select("tbody");
//            List<Board2Index> board2IndexList = new ArrayList<>();
//            if (null != tbodyElements && tbodyElements.size() > 0) {
//                for (Element element : tbodyElements) {
//                    List<Node> trNodeList = element.childNodes();
//                    for (int i = 2; i % 2 == 0 && i < trNodeList.size(); ) {
//                        List<Node> nodeList = trNodeList.get(i).childNodes();
//                        if (null != nodeList && nodeList.size() > 0) {
//                            Board2Index board2Index = new Board2Index();
//                            String boardName = nodeList.get(1).childNode(1).childNode(0).toString();
//                            board2Index.setBoardName(boardName);
//                            board2Index.setBoardType(Board2Type.INDUSTRY_BOARD);
//                            Double changeRange = Double.parseDouble(nodeList.get(3).childNode(0).childNode(0).toString().replace("%", ""));
//                            board2Index.setChangeRange(changeRange);
//                            String leadUpStockName = nodeList.get(5).childNode(0).childNode(0).toString();
//                            board2Index.setLeadUpStockName(leadUpStockName);
//                            Double leadUpStockChangeRange = Double.parseDouble(nodeList.get(5).childNode(2).childNode(0).toString().replace("%", ""));
//                            board2Index.setLeadUpStockChangeRange(leadUpStockChangeRange);
//                            String leadDownStockName = nodeList.get(7).childNode(0).childNode(0).toString();
//                            board2Index.setLeadDownStockName(leadDownStockName);
//                            Double leadDownStockChangeRange = Double.parseDouble(nodeList.get(7).childNode(2).childNode(0).toString().replace("%", ""));
//                            board2Index.setLeadDownStockChangeRange(leadDownStockChangeRange);
//                            Integer limitUpNumber = Integer.parseInt(nodeList.get(9).childNode(0).toString());
//                            board2Index.setLimitUpNumber(limitUpNumber);
//                            Integer limitDownNumber = Integer.parseInt(nodeList.get(11).childNode(0).toString());
//                            board2Index.setLimitDownNumber(limitDownNumber);
//                            Integer upNumber = Integer.parseInt(nodeList.get(13).childNode(0).toString());
//                            board2Index.setUpNumber(upNumber);
//                            Integer downNumber = Integer.parseInt(nodeList.get(15).childNode(0).toString());
//                            board2Index.setDownNumber(downNumber);
//                            Integer companyNumber = Integer.parseInt(nodeList.get(19).childNode(0).toString());
//                            board2Index.setCompanyNumber(companyNumber);
//                            board2Index.setTransactionDate(new Date());
//
//                            board2IndexList.add(board2Index);
//                        }
//                        i += 2;
//                    }
//                }
//            }
//            log.debug(board2IndexList.toString());
//
//            board2IndexDao.saveBoard2IndexList(board2IndexList);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (
//                IOException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
