package cn.com.acca.ma.dao;

public interface ProjectDao extends BaseDao {
	/**
	 * 收集数据库统计信息
	 */
	void gatherDatabaseStatistics();
}
