package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Robot3StockTransactRecord;
import cn.com.acca.ma.model.Robot5StockTransactRecord;
import cn.com.acca.ma.pojo.BuySuggestion;
import cn.com.acca.ma.pojo.ProfitAndLossRate;
import cn.com.acca.ma.pojo.SellSuggestion;

import java.util.List;

public interface Robot5StockTransactRecordDao extends BaseDao {

    /**
     * 卖股票/买股票
     * @param sellDate
     * @param mandatoryStopLoss
     * @param mandatoryStopLossRate
     * @param mandatoryStopProfit
     * @param mandatoryStopProfitRate
     */
    void sellOrBuy(String sellDate, Integer mandatoryStopLoss, Double mandatoryStopLossRate, Integer mandatoryStopProfit, Double mandatoryStopProfitRate);

    /**
     * 买股票/卖股票。同时用于做多和做空
     * @param buyDate
     * @param weekOrBool
     * @param thisMethodHoldStockNumber
     * @param holdStockNumber
     */
    void buyOrSell(String buyDate, Integer weekOrBool, Integer thisMethodHoldStockNumber, Integer holdStockNumber);

    /**
     * 获取卖出建议列表
     * 用于做多
     * @param sellDate
     * @return
     */
    List<Robot5StockTransactRecord> getBullRobotStockTransactRecordListForSell(String sellDate);

    /**
     * 获取买入建议列表
     * 用于做多
     * @param buyDate
     * @return
     */
    List<Robot5StockTransactRecord> getBullRobotStockTransactRecordListForBuy(String buyDate);

    /**
     * 获取卖出建议列表
     * 用于做空
     * @param sellDate
     * @return
     */
    List<Robot5StockTransactRecord> getShortRobotStockTransactRecordListForSell(String sellDate);

    /**
     * 获取买入建议列表
     * 用于做空
     * @param buyDate
     * @return
     */
    List<Robot5StockTransactRecord> getShortRobotStockTransactRecordListForBuy(String buyDate);

    /**
     * 获取卖建议列表
     * @param sellDate
     * @return
     */
    List<SellSuggestion> getSellSuggestionList_multipleMethod(String sellDate);

    /**
     * 获取买建议列表
     * @param buyDate
     * @return
     */
    List<BuySuggestion> getBuySuggestionList_multipleMethod(String buyDate);
    
    /**
     * 计算最近三个月，Robot3Main和Robot4Main的收益率的总和
     * @param currentDate
     * @return 
     */
    ProfitAndLossRate sumRobot3MainAndRobot4MainWithinLastThreeMonth(String currentDate);
}
