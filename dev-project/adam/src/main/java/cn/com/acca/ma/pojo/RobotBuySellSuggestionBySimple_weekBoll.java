package cn.com.acca.ma.pojo;

import java.util.List;

/**
 * 机器人买卖建议，简单文本格式
 */
public class RobotBuySellSuggestionBySimple_weekBoll {

    /**
     * 买建议列表
     */
    private List<BuySuggestion> buySuggestionList;

    /**
     * 卖建议列表
     */
    private List<SellSuggestion> sellSuggestionList;

    public List<BuySuggestion> getBuySuggestionList() {
        return buySuggestionList;
    }

    public void setBuySuggestionList(
        List<BuySuggestion> buySuggestionList) {
        this.buySuggestionList = buySuggestionList;
    }

    public List<SellSuggestion> getSellSuggestionList() {
        return sellSuggestionList;
    }

    public void setSellSuggestionList(
        List<SellSuggestion> sellSuggestionList) {
        this.sellSuggestionList = sellSuggestionList;
    }
}
