package cn.com.acca.ma.model;

import java.math.BigDecimal;

/**
 * 真实交易--账户
 */
public class Real3Account {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 持股数量
     */
    private Integer holdStockNumber;

    /**
     * 股票资产
     */
    private BigDecimal stockAssets;

    /**
     * 资金资产
     */
    private BigDecimal capitalAssets;

    /**
     * 总资产
     */
    private BigDecimal totalAssets;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHoldStockNumber() {
        return holdStockNumber;
    }

    public void setHoldStockNumber(Integer holdStockNumber) {
        this.holdStockNumber = holdStockNumber;
    }

    public BigDecimal getStockAssets() {
        return stockAssets;
    }

    public void setStockAssets(BigDecimal stockAssets) {
        this.stockAssets = stockAssets;
    }

    public BigDecimal getCapitalAssets() {
        return capitalAssets;
    }

    public void setCapitalAssets(BigDecimal capitalAssets) {
        this.capitalAssets = capitalAssets;
    }

    public BigDecimal getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(BigDecimal totalAssets) {
        this.totalAssets = totalAssets;
    }
}
