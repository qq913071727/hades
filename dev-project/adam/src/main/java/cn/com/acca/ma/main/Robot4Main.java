package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Robot4Main extends AbstractMain {

    private static Logger logger = LogManager.getLogger(Robot4Main.class);

    public static void main(String[] args) {
        logger.info("Robot4Main程序开始运行......");

        /******************************* 设置代理服务器的IP和port *******************************/
        setProxyProperties();

        /******************************* 收集数据库统计信息 *************************************/
//		gatherDatabaseStatistics();

        /************* 执行某段时间内的股票买卖交易（周线级金叉死叉结合日线级别买卖信号） ************/
        doBuyAndSellByBeginDateAndEndDate_week();

        /******************************* 将测试数据导入到历史数据表中 *****************************/
        importIntoHistoryDataTableFromRobot4();
    }
}
