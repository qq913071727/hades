package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockMonthAnalysis;
import cn.com.acca.ma.model.StockMonth;
import cn.com.acca.ma.model.StockWeek;
import java.util.Date;
import java.util.List;

public interface StockMonthDao extends BaseDao {

    /**************************************************************************************************
     *
     * 									       更新周线级别，所有月份的股票数据
     *
     **************************************************************************************************/
    /**
     * 计算月线级别，所有股票的基础数据
     */
    void writeStockMonth();

    /**
     * 计算月线级别，所有股票的移动平均线
     */
    void writeStockMonthMA();

    /**
     * 计算月线级别，所有股票的KD
     */
    void writeStockMonthKD();

    /**
     * 计算月线级别，所有股票的MACD
     */
    void writeStockMonthMACD();

    /**************************************************************************************************
     *
     * 									       更新周线级别，某一月的股票数据
     *
     **************************************************************************************************/
    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的基础数据
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthByDate(String beginDate, String endDate);

    /**
     * 根据每个月的开始时间和结束时间，计算某一个月的所有股票的移动平均线
     * @param currentMonthBeginDate
     * @param currentMonthEndDate
     */
    void writeStockMonthMAByDate(String currentMonthBeginDate, String currentMonthEndDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的kd
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthKDByDate(String beginDate, String endDate);

    /**
     * 根据开始时间和结束时间，计算某一个月的所有股票的MACD
     * @param beginDate
     * @param endDate
     */
    void writeStockMonthMACDByDate(String beginDate, String endDate);

    /**
     * 根据开始时间beginDate和结束时间endDate，从表STOCK_MONTH中获取END_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表STOCK_MONTH中获取date日的所有StockMonth对象
     * @param date
     * @return
     */
    List<StockMonth> getStockMonthByDate(String date);

    /**
     * 插入StockMonth对象
     * @param stockMonth
     */
    void saveStockMonth(StockMonth stockMonth);
}
