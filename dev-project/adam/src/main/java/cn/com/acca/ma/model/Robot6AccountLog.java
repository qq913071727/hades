package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 机器人6--账户日志表
 */
@Data
public class Robot6AccountLog {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 日期
     */
    private Date date;

    /**
     * 机器人名字
     */
    private String robotName;

    /**
     * 持股数量
     */
    private Integer holdStockNumber;

    /**
     * etf资产
     */
    private BigDecimal stockAssets;

    /**
     * 资金资产
     */
    private BigDecimal capitalAssets;

    /**
     * 总资产
     */
    private BigDecimal totalAssets;

}
