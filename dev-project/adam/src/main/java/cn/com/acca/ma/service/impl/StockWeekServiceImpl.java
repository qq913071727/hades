package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.StockWeek;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.com.acca.ma.pojo.StockWeekAverageClosePriceAndAllAverageMa;
import cn.com.acca.ma.pojo.StockWeekAverageBollPojo;
import cn.com.acca.ma.pojo.StockWeekClosePriceUpDnBollPercentagePojo;
import cn.com.acca.ma.pojo.StockWeekDatePojo;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.service.StockWeekService;

public class StockWeekServiceImpl extends BaseServiceImpl<StockWeekServiceImpl, StockWeek> implements
        StockWeekService {

    public StockWeekServiceImpl() {
        super();
    }

    /*********************************************************************************************************************
     *
     * 									                             更新某一周的数据
     *
     *********************************************************************************************************************/
    public void writeStockWeekByDate() {
        String allStockWeekBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.beginDate");
        String allStockWeekEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.endDate");
        stockWeekDao.writeStockWeekByDate(allStockWeekBeginDate, allStockWeekEndDate);
    }

    public void writeStockWeekMAByDate() {
        String beginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.ma.beginDate");
        String endDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.ma.endDate");
        stockWeekDao.writeStockWeekMAByDate(beginDate, endDate);
    }

    public void writeStockWeekKDByDate() {
        String allStockWeekKDBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.kd.beginDate");
        String allStockWeekKDEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.kd.endDate");
        stockWeekDao.writeStockWeekKDByDate(allStockWeekKDBeginDate, allStockWeekKDEndDate);
    }

    public void writeStockWeekUpDownByDate() {
        String allStockWeekUpdownBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.updown.beginDate");
        String allStockWeekUpdownEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.updown.endDate");
        stockWeekDao.writeStockWeekUpDownByDate(allStockWeekUpdownBeginDate, allStockWeekUpdownEndDate);
    }

    /**
     * 更新所有股票某一周的MACD数据
     */
    public void writeStockWeekMACDByDate() {
        String stockWeekMACDBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.macd.beginDate");
        String stockWeekMACDEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.macd.endDate");
        stockWeekDao.writeStockWeekMACDByDate(stockWeekMACDBeginDate, stockWeekMACDEndDate);
    }

    /**
     * 更新所有股票某一周的CHANGE_RANGE_EX_RIGHT字段
     */
    public void writeStockWeekChangeRangeExRightByDate() {
        String stockWeekChangeRangeExRightBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.changeRangeExRight.beginDate");
        String stockWeekChangeRangeExRightEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.changeRangeExRight.endDate");
        stockWeekDao.writeStockWeekChangeRangeExRightByDate(stockWeekChangeRangeExRightBeginDate, stockWeekChangeRangeExRightEndDate);
    }

    /**
     * 更新所有股票某一周的与布林带相关的字段
     */
    public void writeStockWeekBollByDate() {
        String stockWeekBollBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.boll.beginDate");
        String stockWeekBollEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.boll.endDate");
        stockWeekDao.writeStockWeekBollByDate(stockWeekBollBeginDate, stockWeekBollEndDate);
    }

    /**
     * 更新所有股票某一周的与hei_kin_ashi的字段
     */
    public void writeStockWeekHeiKinAshiByDate() {
        String stockWeekHeiKinAshiBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.heiKinAshi.beginDate");
        String stockWeekHeiKinAshiEndDate = PropertiesUtil.getValue(STOCK_WEEK_PROPERTIES, "stockWeek.heiKinAshi.endDate");
        stockWeekDao.writeStockWeekHeiKinAshiByDate(stockWeekHeiKinAshiBeginDate, stockWeekHeiKinAshiEndDate);
    }

    /*********************************************************************************************************************
     *
     * 									                           根据一周数据更新图表
     *
     *********************************************************************************************************************/
    @SuppressWarnings({"deprecation", "rawtypes"})
    public void createWeekKDPicture(boolean multithreading) {
        String weekKDBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.kd.beginDate");
        String weekKDEndDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.kd.endDate");

        // 获取某一段时间内平均价格中的最大价格和最小价格
        List maxMinList = stockTransactionDataDao.getMaxMinAverageCloseWeek(multithreading, weekKDBeginDate, weekKDEndDate);
        double maxClose = Double.parseDouble(((Object[]) maxMinList.get(0))[0].toString());
        double minClose = Double.parseDouble(((Object[]) maxMinList.get(0))[1].toString());

        // 获取数据
        List<String> dateList = stockWeekDao
                .getDateByConditionForKD(multithreading, weekKDBeginDate, weekKDEndDate);
        List<String> sameWeek = new ArrayList<String>();
        List<String> lastDayInWeek = new ArrayList<String>();
        List<Double> listK = new ArrayList<Double>();
        List<Double> listD = new ArrayList<Double>();
        List listAverageClose = new ArrayList();

        sameWeek.add(dateList.get(0));
        for (int i = 1; i < dateList.size(); i++) {
            if (true == DateUtil.isSameWeekend(dateList.get(i - 1), dateList.get(i))) {
                sameWeek.add(dateList.get(i));
            } else {
                lastDayInWeek.add(dateList.get(i - 1));
                listK.add(obtainWeekK(sameWeek.get(sameWeek.size() - 1), sameWeek.get(0)));
                listD.add(obtainWeekD(sameWeek.get(sameWeek.size() - 1), sameWeek.get(0)));
                sameWeek.clear();
                sameWeek.add(dateList.get(i));
            }
        }
        lastDayInWeek.add(dateList.get(dateList.size() - 1));
        listK.add(obtainWeekK(sameWeek.get(sameWeek.size() - 1), sameWeek.get(0)));
        listD.add(obtainWeekD(sameWeek.get(sameWeek.size() - 1), sameWeek.get(0)));
        logger.info(obtainWeekK(sameWeek.get(sameWeek.size() - 1), sameWeek.get(0)));
        logger.info(obtainWeekD(sameWeek.get(sameWeek.size() - 1), sameWeek.get(0)));
        logger.info(sameWeek.get(sameWeek.size() - 1));
        logger.info(sameWeek.get(0));
        sameWeek.clear();

        listAverageClose = stockWeekDao.getAverageCloseWeek(multithreading, weekKDBeginDate, weekKDEndDate);

        TimeSeries kIndex = new TimeSeries("K index", org.jfree.data.time.Day.class);
        TimeSeries dIndex = new TimeSeries("D index", org.jfree.data.time.Day.class);
        TimeSeries averageCloseIndex = new TimeSeries("average close", org.jfree.data.time.Day.class);
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();

        for (int i = 0; i < listK.size(); i++) {
            String[] str = lastDayInWeek.get(i).toString().split("-");
            int year = Integer.parseInt(str[0]);
            int month = Integer.parseInt(str[1]);
            int day = Integer.parseInt(str[2]);
            kIndex.add(new Day(day, month, year), listK.get(i));
            logger.info(new Day(day, month, year) + "		" + listK.get(i));
        }
        for (int i = 0; i < listD.size(); i++) {
            String[] str = lastDayInWeek.get(i).toString().split("-");
            int year = Integer.parseInt(str[0]);
            int month = Integer.parseInt(str[1]);
            int day = Integer.parseInt(str[2]);
            dIndex.add(new Day(day, month, year), listD.get(i));
            logger.info(new Day(day, month, year) + "		" + listD.get(i));
        }
        for (int i = 0; i < listAverageClose.size(); i++) {
            Object[] obj = (Object[]) listAverageClose.get(i);
            Date date = (Date) obj[0];
            int year = DateUtil.getYearFromDate(date);
            int month = DateUtil.getMonthFromDate(date);
            int day = DateUtil.getDateFromDate(date);
            averageCloseIndex.add(new Day(day, month, year), this.averageCloseAfterAdjust(Double.parseDouble(obj[1].toString()), maxClose, minClose));
        }

        timeSeriesCollection.addSeries(kIndex);
        timeSeriesCollection.addSeries(dIndex);
        timeSeriesCollection.addSeries(averageCloseIndex);

        // 创建图表
        JFreeChart jfreechart;
        jfreechart = ChartFactory.createTimeSeriesChart("K index & D index", "Date", "Index", timeSeriesCollection, true, true, true);
        jfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyitemrenderer;
            xylineandshaperenderer.setBaseShapesVisible(true);
            xylineandshaperenderer.setBaseShapesFilled(true);
            xylineandshaperenderer.setSeriesPaint(2, Color.BLACK);
        }
        DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
        dateaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

        try {
            FileOutputStream fos = new FileOutputStream(PICTURE_PATH + "kd_week/" + weekKDBeginDate + "-" + weekKDEndDate + PICTURE_FORMAT);
            // 将统计图标输出成JPG文件
            ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
                    1, // JPEG图片的质量，0~1之间
                    jfreechart, // 统计图标对象
                    imageWidth,// 宽
                    IMAGE_HEIGHT,// 高
                    null // ChartRenderingInfo 信息
            );
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double obtainWeekK(String weekBegin, String weekEnd) {
        return stockWeekDao.obtainWeekK(weekBegin, weekEnd);
    }

    public double obtainWeekD(String weekBegin, String weekEnd) {
        return stockWeekDao.obtainWeekD(weekBegin, weekEnd);
    }

    /**
     * 根据周线级别数据更新boll和收盘价的图表
     */
    @SuppressWarnings({"deprecation", "rawtypes"})
    public void createWeekBollClosePricePicture() {
        String weekBollBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.boll.beginDate");
        String weekBollEndDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.boll.endDate");

        // 根据开始日期和结束日期，按周分组，返回平均收盘价、平均上轨值、平均中轨值和平均下轨值
        List<StockWeekAverageBollPojo> stockWeekAverageBollPojoList = stockWeekDao.findAvgClosePriceAndAvgUPAndAvgMBAndAvgDNGroupByYearAndWeek(weekBollBeginDate, weekBollEndDate);

        TimeSeries averageClosePrice = new TimeSeries("average close price", org.jfree.data.time.Day.class);
        TimeSeries averageUP = new TimeSeries("average up", org.jfree.data.time.Day.class);
        TimeSeries averageMB = new TimeSeries("average  md", org.jfree.data.time.Day.class);
        TimeSeries averageDN = new TimeSeries("average  dn", org.jfree.data.time.Day.class);
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();

        for (int i = 0; i < stockWeekAverageBollPojoList.size(); i++) {
            StockWeekAverageBollPojo stockWeekAverageBollPojo = stockWeekAverageBollPojoList.get(i);
            averageClosePrice.addOrUpdate(new Day(stockWeekAverageBollPojo.getEndDate()), stockWeekAverageBollPojo.getAverageClosePrice());
            averageUP.addOrUpdate(new Day(stockWeekAverageBollPojo.getEndDate()), stockWeekAverageBollPojo.getAverageUP());
            averageMB.addOrUpdate(new Day(stockWeekAverageBollPojo.getEndDate()), stockWeekAverageBollPojo.getAverageMB());
            averageDN.addOrUpdate(new Day(stockWeekAverageBollPojo.getEndDate()), stockWeekAverageBollPojo.getAverageDN());
        }

        timeSeriesCollection.addSeries(averageClosePrice);
        timeSeriesCollection.addSeries(averageUP);
        timeSeriesCollection.addSeries(averageMB);
        timeSeriesCollection.addSeries(averageDN);

        // 创建图表
        JFreeChart jfreechart;
        jfreechart = ChartFactory.createTimeSeriesChart("boll and closePrice", "Date", "boll and closePrice", timeSeriesCollection, true, true, true);
        jfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyitemrenderer;
            xylineandshaperenderer.setBaseShapesVisible(true);
            xylineandshaperenderer.setBaseShapesFilled(true);
            xylineandshaperenderer.setSeriesPaint(2, Color.BLACK);
        }
        DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
        dateaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

        try {
            FileOutputStream fos = new FileOutputStream(PICTURE_PATH + "boll_week/" + weekBollBeginDate + "-" + weekBollEndDate + PICTURE_FORMAT);
            // 将统计图标输出成JPG文件
            ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
                    1, // JPEG图片的质量，0~1之间
                    jfreechart, // 统计图标对象
                    imageWidth,// 宽
                    IMAGE_HEIGHT,// 高
                    null // ChartRenderingInfo 信息
            );
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据周线级别数据更新收盘价突破上轨和下轨的股票数量的百分比
     */
    @SuppressWarnings({"deprecation", "rawtypes"})
    public void createWeekClosePriceUpDnBollPercentagePicture() {
        String weekBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.closePrice.upDn.boll.beginDate");
        String weekEndDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.closePrice.upDn.boll.endDate");

        // 根据开始日期和结束日期，按周分组，返回收盘价突破布林带上轨和下轨的股票的百分比
        List<StockWeekClosePriceUpDnBollPercentagePojo> stockWeekClosePriceUpDnBollPercentagePojoList = stockWeekDao.findClosePriceUpDonBollPercentage(weekBeginDate, weekEndDate);

        TimeSeries averageClosePrice = new TimeSeries("average close price", org.jfree.data.time.Day.class);
        TimeSeries closePriceUpBollPercentage = new TimeSeries("close price up boll percentage", org.jfree.data.time.Day.class);
        TimeSeries closePriceDnBollPercentage = new TimeSeries("close price dn boll percentage", org.jfree.data.time.Day.class);
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();

        for (int i = 0; i < stockWeekClosePriceUpDnBollPercentagePojoList.size(); i++) {
            StockWeekClosePriceUpDnBollPercentagePojo stockWeekClosePriceUpDnBollPercentagePojo = stockWeekClosePriceUpDnBollPercentagePojoList.get(i);
            averageClosePrice.add(new Day(stockWeekClosePriceUpDnBollPercentagePojo.getEndDate()), stockWeekClosePriceUpDnBollPercentagePojo.getAverageClosePirce());
            closePriceUpBollPercentage.add(new Day(stockWeekClosePriceUpDnBollPercentagePojo.getEndDate()), stockWeekClosePriceUpDnBollPercentagePojo.getUpBollPercentage());
            closePriceDnBollPercentage.add(new Day(stockWeekClosePriceUpDnBollPercentagePojo.getEndDate()), stockWeekClosePriceUpDnBollPercentagePojo.getDnBollPercentage());
        }

        timeSeriesCollection.addSeries(averageClosePrice);
        timeSeriesCollection.addSeries(closePriceUpBollPercentage);
        timeSeriesCollection.addSeries(closePriceDnBollPercentage);

        // 创建图表
        JFreeChart jfreechart;
        jfreechart = ChartFactory.createTimeSeriesChart("close price up and dn boll percentage", "Date", "percentage", timeSeriesCollection, true, true, true);
        jfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyitemrenderer;
            xylineandshaperenderer.setBaseShapesVisible(true);
            xylineandshaperenderer.setBaseShapesFilled(true);
            xylineandshaperenderer.setSeriesPaint(2, Color.BLACK);
        }
        DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
        dateaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

        try {
            FileOutputStream fos = new FileOutputStream(PICTURE_PATH + "close_price_up_dn_week_boll_percentage/" + weekBeginDate + "-" + weekEndDate + PICTURE_FORMAT);
            // 将统计图标输出成JPG文件
            ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
                    1, // JPEG图片的质量，0~1之间
                    jfreechart, // 统计图标对象
                    imageWidth,// 宽
                    IMAGE_HEIGHT,// 高
                    null // ChartRenderingInfo 信息
            );
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 周线级别所有均线和收盘价的折线图
     */
    @Override
    public void createWeekMAAndClosePricePicture() {
        String weekBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.ma.closePrice.beginDate");
        String weekEndDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.ma.closePrice.endDate");

        // 按照开始日期和结束日期分组，查找开始日期和结束日期之间的begin_date和end_date
        List<StockWeekDatePojo> beginDateEndDateList = stockIndexWeekDao.findBeginDateAndEndDateGroupByBeginDateAndEndDateOrderByBeginDate(weekBeginDate, weekEndDate);

        TimeSeries ma5TimeSeries = new TimeSeries("ma5", org.jfree.data.time.Day.class);
        TimeSeries ma10TimeSeries = new TimeSeries("ma10", org.jfree.data.time.Day.class);
        TimeSeries ma20TimeSeries = new TimeSeries("ma20", org.jfree.data.time.Day.class);
        TimeSeries ma60TimeSeries = new TimeSeries("ma60", org.jfree.data.time.Day.class);
        TimeSeries ma120TimeSeries = new TimeSeries("ma120", org.jfree.data.time.Day.class);
        TimeSeries ma250TimeSeries = new TimeSeries("ma250", org.jfree.data.time.Day.class);
        TimeSeries closePriceTimeSeries = new TimeSeries("close price", org.jfree.data.time.Day.class);
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();

        if (null != beginDateEndDateList && beginDateEndDateList.size() > 0) {
            for (StockWeekDatePojo stockWeekDatePojo : beginDateEndDateList) {
                Date beginDate = stockWeekDatePojo.getBeginDate();
                Date endDate = stockWeekDatePojo.getEndDate();

                List<StockWeekAverageClosePriceAndAllAverageMa> stockWeekAverageClosePriceAndAllAverageMaList = stockWeekDao.findAllAverageMaAndAverageClosePriceByBeginDateAndEndDate(DateUtil.dateToString(beginDate), DateUtil.dateToString(endDate));
                for (int i = 0; i < stockWeekAverageClosePriceAndAllAverageMaList.size(); i++) {
                    StockWeekAverageClosePriceAndAllAverageMa stockWeekAverageClosePriceAndAllAverageMa = stockWeekAverageClosePriceAndAllAverageMaList.get(i);
                    ma5TimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageMA5());
                    ma10TimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageMA10());
                    ma20TimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageMA20());
                    ma60TimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageMA60());
                    ma120TimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageMA120());
                    ma250TimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageMA250());
                    closePriceTimeSeries.add(new Day(stockWeekAverageClosePriceAndAllAverageMa.getEndDate()), stockWeekAverageClosePriceAndAllAverageMa.getAverageClosePrice());
                }
            }
        }

        timeSeriesCollection.addSeries(ma5TimeSeries);
        timeSeriesCollection.addSeries(ma10TimeSeries);
        timeSeriesCollection.addSeries(ma20TimeSeries);
        timeSeriesCollection.addSeries(ma60TimeSeries);
        timeSeriesCollection.addSeries(ma120TimeSeries);
        timeSeriesCollection.addSeries(ma250TimeSeries);
        timeSeriesCollection.addSeries(closePriceTimeSeries);

        // 创建图表
        JFreeChart jfreechart;
        jfreechart = ChartFactory.createTimeSeriesChart("所有的移动平均线和收盘价", "日期", "所有的移动平均线和收盘价", timeSeriesCollection, true, true, true);
        jfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyitemrenderer;
            xylineandshaperenderer.setBaseShapesVisible(true);
            xylineandshaperenderer.setBaseShapesFilled(true);
            xylineandshaperenderer.setSeriesPaint(2, Color.BLACK);
        }
        DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
        dateaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

        try {
            FileOutputStream fos = new FileOutputStream(PICTURE_PATH + "week_all_average_ma_and_all_close_price/" + weekBeginDate + "-" + weekEndDate + PICTURE_FORMAT);
            // 将统计图标输出成JPG文件
            ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
                    1, // JPEG图片的质量，0~1之间
                    jfreechart, // 统计图标对象
                    imageWidth,// 宽
                    IMAGE_HEIGHT,// 高
                    null // ChartRenderingInfo 信息
            );
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 周线级别hei_kin_ashi的折线图
     */
    @Override
    public void createWeekHeiKinAshiUpDownPicture() {
        String weekBeginDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.heiKinAshi.beginDate");
        String weekEndDate = PropertiesUtil.getValue(STOCK_WEEK_PICTURE_PROPERTIES, "stockWeek.heiKinAshi.endDate");

        // 按照开始日期和结束日期分组，查找开始日期和结束日期之间的平均ha_week_close_price、平均close_price和end_date
        List<StockWeek> stockWeekList = stockWeekDao.findAverageHaClosePriceAndAverageHaWeekClosePriceByBeginDateAndEndDateOrderByEndDateAsc(weekBeginDate, weekEndDate);

        TimeSeries averageHaClosePriceTimeSeries = new TimeSeries("平均hei_kin_ashi收盘价", org.jfree.data.time.Day.class);
        TimeSeries closePriceTimeSeries = new TimeSeries("平均收盘价", org.jfree.data.time.Day.class);
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();

        if (null != stockWeekList && stockWeekList.size() > 0) {
            for (StockWeek stockWeek : stockWeekList) {
                BigDecimal averageHaWeekClosePrice = stockWeek.getHaWeekClosePrice();
                BigDecimal averageClosePrice = stockWeek.getClosePrice();
                Date endDate = stockWeek.getEndDate();

                averageHaClosePriceTimeSeries.addOrUpdate(new Day(endDate), averageHaWeekClosePrice);
                closePriceTimeSeries.addOrUpdate(new Day(endDate), averageClosePrice);
            }
        }

        timeSeriesCollection.addSeries(averageHaClosePriceTimeSeries);
        timeSeriesCollection.addSeries(closePriceTimeSeries);

        // 创建图表
        JFreeChart jfreechart;
        jfreechart = ChartFactory.createTimeSeriesChart("周线级别所有的平均hei_kin_ashi收盘价和平均收盘价", "日期", "价格", timeSeriesCollection, true, true, true);
        jfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyitemrenderer;
            xylineandshaperenderer.setBaseShapesVisible(true);
            xylineandshaperenderer.setBaseShapesFilled(true);
            xylineandshaperenderer.setSeriesPaint(2, Color.BLACK);
        }
        DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
        dateaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

        try {
            FileOutputStream fos = new FileOutputStream(PICTURE_PATH + "week_all_average_ha_close_price_and_all_close_price/" + weekBeginDate + "-" + weekEndDate + PICTURE_FORMAT);
            // 将统计图标输出成JPG文件
            ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
                    1, // JPEG图片的质量，0~1之间
                    jfreechart, // 统计图标对象
                    imageWidth,// 宽
                    IMAGE_HEIGHT,// 高
                    null // ChartRenderingInfo 信息
            );
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*********************************************************************************************************************
     *
     * 									                           更新所有的数据
     *
     *********************************************************************************************************************/
    public void writeStockWeek() {
        stockWeekDao.writeStockWeek();
    }

    public void writeStockWeekMA() {
        stockWeekDao.writeStockWeekMA();
    }


    public void writeStockWeekKD() {
        stockWeekDao.writeStockWeekKD();
    }

    public void writeStockWeekUpDown() {
        stockWeekDao.writeStockWeekUpDown();
    }

    /**
     * 更新STOCK_WEEK表中与MACD相关的所有字段：EMA12，EMA26，D,K
     */
    public void writeStockWeekMACD() {
        stockWeekDao.writeStockWeekMACD();
    }

    /**
     * 更新STOCK_WEEK表中的CHANGE_RANGE_EX_RIGHT字段
     */
    public void writeStockWeekChangeRangeExRight() {
        stockWeekDao.writeStockWeekChangeRangeExRight();
    }

    /**
     * 更新STOCK_WEEK表中的和布林带相关的字段：mb、up、dn
     */
    public void writeStockWeekBoll() {
        stockWeekDao.writeStockWeekBoll();
    }

    /**
     * 更新STOCK_WEEK表中的hei_kin_ashi相关的字段
     */
    public void writeStockWeekHeiKinAshi() {
        stockWeekDao.writeStockWeekHeiKinAshi();
    }

    /**
     * 将List类型转换为String类型
     *
     * @param list
     * @return
     */
    @Override
    public String listToString(List list) {
        // TODO Auto-generated method stub
        return null;
    }
}
