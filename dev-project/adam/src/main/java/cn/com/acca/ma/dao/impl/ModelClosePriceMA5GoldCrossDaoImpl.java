package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelClosePriceMA5GoldCrossDao;
import cn.com.acca.ma.hibernate.impl.GetClosePriceMa5GoldCrossSuccessRateWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelClosePriceMA5GoldCross;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

public class ModelClosePriceMA5GoldCrossDaoImpl extends BaseDaoImpl<ModelClosePriceMA5GoldCrossDaoImpl> implements
    ModelClosePriceMA5GoldCrossDao {

    public ModelClosePriceMA5GoldCrossDaoImpl() {
        super();
    }

    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     */
    @Override
    public void writeModelClosePriceMA5GoldCross() {
        logger.info("开始海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_C_P_MA5_GOLD_CROSS()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据结束");
    }

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     * @param endDate
     */
    @Override
    public void writeModelClosePriceMA5GoldCrossIncr(String endDate) {
        logger.info("开始增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session
            .createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_C_P_MA5_G_C_INCR(?)}");
        query.setParameter(0, endDate);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据结束");
    }

    /**
     * 获取一段时间内，最近dateNumber天，close_price金叉ma5的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    @Override
    public List<SuccessRateOrPercentagePojo> getClosePriceMa5GoldCrossSuccessRate(boolean multithreading, String beginDate,
        String endDate, Integer dateNumber) {
        logger.info("获取开始时间【" + beginDate + "】至【" + endDate + "】内，最近【" + dateNumber + "】天，"
            + "close_price金叉ma5的成功率");

        final List<SuccessRateOrPercentagePojo> list=new ArrayList<SuccessRateOrPercentagePojo>();

        if (multithreading) {
            Session newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            newSession.doWork(new GetClosePriceMa5GoldCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            session.doWork(new GetClosePriceMa5GoldCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            session.getTransaction().commit();
            session.close();
        }

        logger.info("获取一段时间内，最近" + dateNumber + "天，close_price金叉ma5的成功率完成");
        return list;
    }

    /**
     * 获取一段时间内，close_price金叉ma5的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SuccessRateOrPercentagePojo> getClosePriceMa5GoldCrossPercentage(boolean multithreading, final String beginDate, final String endDate){
        logger.info("开始获取一段时间内【" + beginDate + "】至【" + endDate + "】，close_price金叉ma5的百分比");

        String hql = "select t.date_ transactionDate, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_ " +
                "and t1.close_price>t1.ma5)/ " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 successRateOrPercentage " +
                "from stock_transaction_data_all t " +
                "where t.date_ between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd') " +
                "group by t.date_ order by t.date_ asc";

        Session newSession = null;
        Query query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        }

        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<SuccessRateOrPercentagePojo> list = query.list();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        return list;
    }

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    @Override
    public List<Date> getDateByCondition(String beginDate, String endDate) {
        logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
            + "从表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中获取SELL_DATE字段");

        StringBuffer hql = new StringBuffer("select distinct t.sellDate from ModelClosePriceMA5GoldCross t " +
            "where t.sellDate between to_date(?, 'yyyy-mm-dd') and to_date(?, 'yyyy-mm-dd')");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<Date> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 从表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中获取sell_date日的所有ModelClosePriceMA5GoldCross对象
     * @param date
     * @return
     */
    @Override
    public List<ModelClosePriceMA5GoldCross> getModelClosePriceMA5GoldCrossByDate(String date) {
        logger.info("从表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中获取日期【" + date + "】的所有ModelClosePriceMA5GoldCross对象");

        StringBuffer hql = new StringBuffer("select t from ModelClosePriceMA5GoldCross t " +
            "where t.sellDate=to_date(?,'yyyy-mm-dd') order by t.sellDate asc");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, date);
        List<ModelClosePriceMA5GoldCross> list=query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入ModelClosePriceMA5GoldCross对象
     * @param modelClosePriceMA5GoldCross
     */
    public void save(ModelClosePriceMA5GoldCross modelClosePriceMA5GoldCross){
        logger.info("向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入ModelClosePriceMA5GoldCross对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.persist(modelClosePriceMA5GoldCross);
        session.getTransaction().commit();
        session.close();
    }
}
