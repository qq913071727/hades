package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建StockIndex对象的Hei Kin Ashi图片
 */
public class CreateStockIndexHeiKinAshiPictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateStockIndexHeiKinAshiPictureThread.class);

    private StockIndexService stockIndexService;

    public CreateStockIndexHeiKinAshiPictureThread() {
    }

    public CreateStockIndexHeiKinAshiPictureThread(StockIndexService stockIndexService) {
        this.stockIndexService = stockIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始创建StockIndex对象的Hei Kin Ashi图片，"
            + "调用的方法为【createStockIndexHeiKinAshiPicture】");

        stockIndexService.createStockIndexHeiKinAshiPicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
