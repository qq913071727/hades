package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 真实交易--股票交易记录
 */
@Data
public class Real4StockTransactionRecord {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 买入日期
     */
    private Date buyDate;

    /**
     * 买入价格
     */
    private BigDecimal buyPrice;

    /**
     * 买入数量
     */
    private Integer buyAmount;

    /**
     * 卖出日期
     */
    private Date sellDate;

    /**
     * 卖出价格
     */
    private BigDecimal sellPrice;

    /**
     * 卖出数量
     */
    private Integer sellAmount;

    /**
     * 交易算法
     */
    private Integer filterType;

    /**
     * 多空方向。1表示做多；-1表示做空
     */
    private Integer direction;

}
