package cn.com.acca.ma.constant;

/**
 * ModelMain类使用的常量
 */
public class ModelConstant {

    /**
     * 5日均线
     */
    public static final Integer MA5 = 5;

    /**
     * 10日均线
     */
    public static final Integer MA10 = 10;

    /**
     * 20日均线
     */
    public static final Integer MA20 = 20;

    /**
     * 60日均线
     */
    public static final Integer MA60 = 60;

    /**
     * 120日均线
     */
    public static final Integer MA120 = 120;

    /**
     * 250日均线
     */
    public static final Integer MA250 = 250;
}
