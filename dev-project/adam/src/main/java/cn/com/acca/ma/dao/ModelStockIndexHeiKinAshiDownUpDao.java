package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexHeiKinAshiDownUp;

import java.util.List;

public interface ModelStockIndexHeiKinAshiDownUpDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_index_close_price小于等于ha_index_open_price时卖出，ha_index_close_price大于ha_index_open_price时买入，
     * 向表mdl_stock_index_h_k_a_down_up插入数据
     */
    void writeModelStockIndexHeiKinAshiDownUp();

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
    List<ModelStockIndexHeiKinAshiDownUp> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
