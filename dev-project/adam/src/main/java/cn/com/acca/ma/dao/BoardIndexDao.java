package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.Board;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.com.acca.ma.model.BoardIndex;
import java.util.Map;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;

public interface BoardIndexDao extends BaseDao {
	/**
	 * 计算BOARD_INDEX表中的数据
	 */
	void calculateBoardIndex();
	
	/**
	 * 计算BOARD_INDEX表中的FIVE_DAY_RATE字段和TEN_DAY_RATE字段
	 */
	void calculateAllBoardIndexFiveAndTenDayRate();

	/**
	 * 算BOARD_INDEX表中某一日的FIVE_DAY_RATE字段和TEN_DAY_RATE字段
	 * @param multithreading
	 * @param boardDate
	 */
	void writeBoardIndexFiveAndTenDayRateByDate(boolean multithreading, String boardDate);
	
	/**
     * 计算BOARD_INDEX表的UP_DOWN_PERCENTAGE字段
     */
    void calculateBoardIndexUpDownPercentage();

	/**
	 * 计算BOARD_INDEX表中某一日的UP_DOWN_PERCENTAGE字段
	 * @param multithreading
	 * @param boardDate
	 */
	void writeBoardIndexUpDownPercentageByDate(boolean multithreading, String boardDate);
    
    /**
     * 计算BOARD_INDEX表的UP_DOWN_RANK字段
     */
    void calculateBoardIndexUpDownRank();

	/**
	 * 计算BOARD_INDEX表中某一日的UP_DOWN_RANK字段
	 * @param multithreading
	 * @param boardDate
	 */
	void writeBoardIndexUpDownRankByDate(boolean multithreading, String boardDate);
	
	/**
	 * 返回从开始日期beginDate到结束日期endDate之间的BoardIndex数据
	 * @param beginDate
	 * @param endDate
	 * @return oardIndex类型的List列表
	 */
	List<BoardIndex> getBoardIndexWithinDate(String beginDate, String endDate, String boardId);

	/**
	 * 根据日期，计算board_index表中某一日的记录
	 * @param multithreading
	 * @param boardDate
	 */
	void writeBoardIndexByDate(boolean multithreading, String boardDate);
	
	/**
	 * 查询某一天涨停的股票属于哪些板块
	 * @param boardDate
	 */
	void findLimitUpStockBoardByDate(String boardDate);

	/**
	 * 为绘制版块蜘蛛雷达图，获取相关数据
	 * @param multithreading
	 * @param boardDate 版块日期
	 * @param dateNumber 日期数量
	 * @param limitRate 判断股票是否涨停的标准
	 * @return 返回一个二维表，表示需要的数据
	 */
    @SuppressWarnings("rawtypes")
	List<HashMap> findDataForSpiderWebPlot(boolean multithreading, final Date boardDate, final BigDecimal dateNumber, final BigDecimal limitRate);
    
    /**
     *  根据开始时间和结束时间，在board_index表中查找所有board_date
     * @param beginDate
     * @param endDate
     * @return
     */
    List<String> getDateByCondition(String beginDate,String endDate);
    
    /**
     * BOARD_INDEX表中，某个交易日的全部记录
     * @param date
     * @return
     */
    List<BoardIndex> getBoardIndexsByDate(String date);

	/**
	 * 根据开始时间和结束时间，在board_index表中查找board_date
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<String> getDateByConditionForAllBoardClose(boolean multithreading, String beginDate, String endDate);

	/**
	 * 根据board_id和board_date查找某个板块在某个交易日的收盘价
	 * @param multithreading
	 * @param boardNumber
	 * @param boardList
	 * @param dateList
	 * @return
	 */
	TimeSeries[] getBoardCloseByDateAndId(boolean multithreading, Integer boardNumber, List<Board> boardList, List<String> dateList);

	/**
	 * 获取从开始日期beginDate至结束日期endDate，每个交易日所有板块FIVE_DAY_RATE字段和TEN_DAY_RATE字段的平均值
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List getFiveAndTenDayRateWithinDate(boolean multithreading, String beginDate,String endDate);
    
    /**
     * 根据版块ID，选在开始时间（beginDate）至结束时间（endDate）的FIVE_DAY_RATE字段
     * @param beginDate
     * @param endDate
     * @param boardId
     * @return
     */
    @SuppressWarnings("rawtypes")
	List getFiveDayRate(String beginDate,String endDate,String boardId);
    
    /**
     * 根据版块ID，选在开始时间（beginDate）至结束时间（endDate）的TEN_DAY_RATE字段
     * @param beginDate
     * @param endDate
     * @param boardId
     * @return
     */
    @SuppressWarnings("rawtypes")
	List getTenDayRate(String beginDate,String endDate,String boardId);
    
    /**
     * 持久化一个BoardIndex对象
     * @param boardIndex
     */
    void save(BoardIndex boardIndex);


}
