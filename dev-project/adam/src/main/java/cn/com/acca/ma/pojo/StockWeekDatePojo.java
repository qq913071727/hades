package cn.com.acca.ma.pojo;

import lombok.Data;

import java.util.Date;

/**
 * 这一周的开始日期和结束日期
 */
@Data
public class StockWeekDatePojo {

    /**
     * 开始日期
     */
    private Date beginDate;

    /**
     * 开始日期
     */
    private Date endDate;
}
