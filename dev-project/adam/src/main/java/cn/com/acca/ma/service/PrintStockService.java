package cn.com.acca.ma.service;

public interface PrintStockService extends BaseService {

    /**
     * 打印波段操作的股票，并发送邮件
     */
    void printBandOperationStockAndSendMail();

}
