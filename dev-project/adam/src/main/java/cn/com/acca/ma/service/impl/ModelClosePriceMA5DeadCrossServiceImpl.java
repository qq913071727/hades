package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelClosePriceMA5DeadCross;
import cn.com.acca.ma.model.ModelClosePriceMA5GoldCross;
import cn.com.acca.ma.service.ModelClosePriceMA5DeadCrossService;
import cn.com.acca.ma.service.ModelClosePriceMA5GoldCrossService;

import java.util.List;

public class ModelClosePriceMA5DeadCrossServiceImpl extends BaseServiceImpl<ModelClosePriceMA5DeadCrossServiceImpl, ModelClosePriceMA5DeadCross> implements
        ModelClosePriceMA5DeadCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    @Override
    public void writeModelClosePriceMA5DeadCross() {
        modelClosePriceMA5DeadCrossDao.writeModelClosePriceMA5DeadCross();
    }

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    @Override
    public void writeModelClosePriceMA5DeadCrossIncr() {
        String endDate = PropertiesUtil
                .getValue(MODEL_CLOSE_PRICE_MA5_DEAD_CROSS_PROPERTIES, "close.price.ma5.dead.cross.end_date");
        modelClosePriceMA5DeadCrossDao.writeModelClosePriceMA5DeadCrossIncr(endDate);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
