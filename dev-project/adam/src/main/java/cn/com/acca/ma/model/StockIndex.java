package cn.com.acca.ma.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 股票指数
 * @author Administrator
 */
@Data
@Entity
@Table(name="STOCK_INDEX")
public class StockIndex implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_")
	private Integer id;

	/**
	 * 日期
	 */
	@Column(name="DATE_")
	private Date date;

	/**
	 * 代码
	 */
	@Column(name="CODE_")
	private String code;

	/**
	 * 开盘价
	 */
	@Column(name="OPEN_PRICE")
	private BigDecimal openPrice;

	/**
	 * 收盘价
	 */
	@Column(name="HIGHEST_PRICE")
	private BigDecimal highestPrice;

	/**
	 * 最高价
	 */
	@Column(name="CLOSE_PRICE")
	private BigDecimal closePrice;

	/**
	 * 最低价
	 */
	@Column(name="LOWEST_PRICE")
	private BigDecimal lowestPrice;

	/**
	 * 涨跌额
	 */
	@Column(name="CHANGE_AMOUNT")
	private BigDecimal changeAmount;

	/**
	 * 涨跌幅。单位：%
	 */
	@Column(name="CHANGE_RANGE")
	private BigDecimal changeRange;

	/**
	 * 成交量
	 */
	@Column(name="VOLUME")
	private BigDecimal volume;

	/**
	 * 成交额
	 */
	@Column(name="TURNOVER")
	private BigDecimal turnover;

	/**
	 * 5日均线
	 */
	@Column(name="MA5")
	private BigDecimal ma5;

	/**
	 * 10日均线
	 */
	@Column(name="MA10")
	private BigDecimal ma10;

	/**
	 * 20日均线
	 */
	@Column(name="MA20")
	private BigDecimal ma20;

	/**
	 * 60日均线
	 */
	@Column(name="MA60")
	private BigDecimal ma60;

	/**
	 * 120日均线
	 */
	@Column(name="MA120")
	private BigDecimal ma120;

	/**
	 * 250日均线
	 */
	@Column(name="MA250")
	private BigDecimal ma250;

	/**
	 * HeiKinAshi开盘价
	 */
	@Column(name="HA_INDEX_OPEN_PRICE")
	private BigDecimal HAIndexOpenPrice;

	/**
	 * HeiKinAshi收盘价
	 */
	@Column(name="HA_INDEX_CLOSE_PRICE")
	private BigDecimal HAIndexClosePrice;

	/**
	 * HeiKinAshi最高价
	 */
	@Column(name="HA_INDEX_HIGHEST_PRICE")
	private BigDecimal HAIndexHighestPrice;

	/**
	 * HeiKinAshi最低价
	 */
	@Column(name="HA_INDEX_LOWEST_PRICE")
	private BigDecimal HAIndexLowestPrice;

	/**
	 * 5日乖离率
	 */
	@Column(name="BIAS5")
	private BigDecimal bias5;

	/**
	 * 10日乖离率
	 */
	@Column(name="BIAS10")
	private BigDecimal bias10;

	/**
	 * 20日乖离率
	 */
	@Column(name="BIAS20")
	private BigDecimal bias20;

	/**
	 * 60日乖离率
	 */
	@Column(name="BIAS60")
	private BigDecimal bias60;

	/**
	 * 120日乖离率
	 */
	@Column(name="BIAS120")
	private BigDecimal bias120;

	/**
	 * 250日乖离率
	 */
	@Column(name="BIAS250")
	private BigDecimal bias250;

	/**
	 * 计算MACD时的指标
	 */
	@Column(name="EMA12")
	private BigDecimal ema12;

	/**
	 * 计算MACD时的指标
	 */
	@Column(name="EMA26")
	private BigDecimal ema26;

	/**
	 * 计算MACD时的指标
	 */
	@Column(name="DIF")
	private BigDecimal dif;

	/**
	 * 计算MACD时的指标
	 */
	@Column(name="DEA")
	private BigDecimal dea;

	/**
	 * KD指标中的RSV
	 */
	@Column(name="RSV")
	private BigDecimal rsv;

	/**
	 * KD指标中的K
	 */
	@Column(name="K")
	private BigDecimal k;

	/**
	 * KD指标中的D
	 */
	@Column(name="D")
	private BigDecimal d;
}
