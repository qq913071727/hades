package cn.com.acca.ma.jpa.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hibernate.jdbc.Work;

/**
 * hibernate的work接口的实现类，可以作为其他实现类的积累
 */
public class FindCodeAndMaxDateAndLastClosePriceGroupByCodeWorkImpl implements Work {

    /**
     * 查询字符串
     */
    private String queryString;

    /**
     * 结果集
     */
    private ResultSet resultSet;

    public FindCodeAndMaxDateAndLastClosePriceGroupByCodeWorkImpl() {
    }

    public FindCodeAndMaxDateAndLastClosePriceGroupByCodeWorkImpl(String queryString) {
        this.queryString = queryString;
    }

    /**
     * 执行方法
     * @param connection
     * @throws SQLException
     */
    @Override
    public void execute(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(queryString);
        resultSet = preparedStatement.executeQuery();
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }
}
