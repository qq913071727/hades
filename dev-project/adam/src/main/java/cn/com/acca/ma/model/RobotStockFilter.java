package cn.com.acca.ma.model;

import lombok.Data;

/**
 * 机器人--股票过滤表
 */
@Data
public class RobotStockFilter {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 过滤类型/交易策略。1表示MACD金叉；2表示MACD死叉；3表示close_price金叉MA5；4表示close_price死叉MA5；5表示hei_kin_ashi从下跌趋势转为上涨趋势；6表示hei_kin_ashi从上涨趋势转为下跌趋势；7表示KD金叉；8表示KD死叉
     */
    private Integer filterType;

    /**
     * 交易方向。1表示做多；-1表示做空
     */
    private Integer direction;

}
