package cn.com.acca.ma.constant;

/**
 * 持仓方式
 */
public class HoldShippingSpaceType {

    /**
     * 10个股票平分仓位
     */
    public static final Integer AVERAGE_HOLD_SHIPPING_SPACE = 1;

    /**
     * 尽量填满所有仓位
     */
    public static final Integer MAX_HOLD_SHIPPING_SPACE = 2;
}
