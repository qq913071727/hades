package cn.com.acca.ma.service;

public interface ModelStockWeekAnalysisService extends BaseService {

    /**
     * 计算model_stock_week_analysis表的全部数据
     */
    void writeModelStockWeekAnalysis();

    /**
     * 计算model_stock_week_analysis表的某一日的数据
     * @param multithreading
     */
    void writeModelStockWeekAnalysisByDate(boolean multithreading);

    /**
     * 创建周线级别标准差和平均收盘价的折线图
     */
    void createModelStockWeekAnalysisStandardDeviationPictureAndAverageClosePrice();
}
