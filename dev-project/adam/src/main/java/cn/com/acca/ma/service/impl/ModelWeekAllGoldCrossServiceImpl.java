package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.constant.WeekMovingAverageGoldCrossType;
import cn.com.acca.ma.model.ModelWeekAllGoldCross;
import cn.com.acca.ma.service.ModelWeekAllGoldCrossService;

import java.util.List;

public class ModelWeekAllGoldCrossServiceImpl extends BaseServiceImpl<ModelWeekAllGoldCrossServiceImpl, ModelWeekAllGoldCross> implements
        ModelWeekAllGoldCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据
     */
    @Override
    public void writeModelWeekAllGoldCross() {
        for (int i = 0; i< WeekMovingAverageGoldCrossType.WEEK_MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.size(); i++){
            modelWeekAllGoldCrossDao.writeModelWeekAllGoldCross(WeekMovingAverageGoldCrossType.WEEK_MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.get(i));
        }
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
