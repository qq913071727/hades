package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.StockIndexWeekDao;
import cn.com.acca.ma.dao.StockIndexWeekendDao;
import cn.com.acca.ma.model.StockIndexWeek;
import cn.com.acca.ma.model.StockIndexWeekend;
import cn.com.acca.ma.model.StockWeek;
import cn.com.acca.ma.mybatis.util.MyBatisUtil;
import cn.com.acca.ma.pojo.StockWeekDatePojo;
import cn.com.acca.ma.xmlmapper.StockIndexMapper;
import cn.com.acca.ma.xmlmapper.StockIndexWeekMapper;
import cn.com.acca.ma.xmlmapper.StockIndexWeekendMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

public class StockIndexWeekDaoImpl extends BaseDaoImpl<StockIndexWeekDaoImpl> implements StockIndexWeekDao {
    /******************************************** 更新每周的数据 ********************************************************/
    /**
     * 根据日期，更新STOCK_INDEX_WEEK表的基础数据
     */
    public void writeStockIndexWeekByDate(String beginDate, String endDate) {
        logger.info("根据开始日期【" + beginDate + "】和结束日期【" + endDate + "】，"
                + "更新STOCK_INDEX_WEEK表的基础数据");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;

        Map<String, String> map = new HashMap<String, String>();
        map.put("beginTime", beginDate);
        map.put("endTime", endDate);

        sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekMapper.writeStockIndexWeekByDate", map);
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源
    }

    /**
     * 根据日期，更新STOCK_INDEX_WEEK表的Hei Kin Ashi数据
     */
    public void writeStockIndexWeekHeiKinAshiByDate(String beginDate, String endDate) {
        logger.info("根据日期【" + beginDate + "】和【" + endDate + "】，更新STOCK_INDEX_WEEK"
                + "表的Hei Kin Ashi数据");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;

        Map<String, String> map = new HashMap<String, String>();
        map.put("beginTime", beginDate);
        map.put("endTime", endDate);

        sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekMapper.writeStockIndexWeekHeiKinAshiByDate", map);
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源

        logger.info("根据日期【" + beginDate + "】和【" + endDate + "】，更新STOCK_INDEX_WEEK"
                + "表的Hei Kin Ashi数据完成");
    }

    /**************************************** 根据每周数据更新图表 *************************************************/
    /**
     * 为创建周线级别StockIndexWeek对象的Hei Kin Ashi图，获取数据
     *
     * @param multithreading
     * @param beginTime
     * @param endTime
     * @param code
     * @return
     */
    public List<StockIndexWeek> getDataForLineChart(boolean multithreading, String beginTime, String endTime, String code) {
        logger.info("get data for line chart begin");

        SqlSession newSqlSession = null;
        StockIndexWeekMapper newStockIndexWeekMapper = null;
        if (multithreading) {
            newSqlSession = MyBatisUtil.newSqlSession();// 打开会话，事务开始;
            newStockIndexWeekMapper = newSqlSession.getMapper(StockIndexWeekMapper.class);
        } else {
            sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
            stockIndexWeekMapper = sqlSession.getMapper(StockIndexWeekMapper.class);
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        map.put("code", code);

        //获取beginTime和endTime之间的日期
        List<StockIndexWeek> list;
        if (multithreading) {
            list = newStockIndexWeekMapper.getDataForLineChart(map);
            newSqlSession.commit();// 提交会话，即事务提交
            newSqlSession.close();// 关闭会话，释放资源
        } else {
            list = stockIndexWeekMapper.getDataForLineChart(map);
            sqlSession.commit();// 提交会话，即事务提交
            sqlSession.close();// 关闭会话，释放资源
        }

        logger.info("get data for line chart finish");
        return list;
    }

    /**
     * 按照开始日期和结束日期分组，查找开始日期和结束日期之间的begin_date和end_date
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    public List<StockWeekDatePojo> findBeginDateAndEndDateGroupByBeginDateAndEndDateOrderByBeginDate(String beginTime, String endTime) {
        logger.info("按照开始日期【" + beginTime + "】和结束日期【" + endTime + "】分组，查找开始日期和结束日期之间的begin_date和end_date");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
        stockIndexWeekMapper = sqlSession.getMapper(StockIndexWeekMapper.class);

        Map<String, String> map = new HashMap<String, String>();
        map.put("beginDate", beginTime);
        map.put("endDate", endTime);

        //获取beginDate和endDate之间的日期
        List list = stockIndexWeekMapper.findBeginDateAndEndDateGroupByBeginDateAndEndDateOrderByBeginDate(map);
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源

        logger.info("按照开始日期【" + beginTime + "】和结束日期【" + endTime + "】分组，查找开始日期和结束日期之间的begin_date和end_date完成");

        return list;
    }

    /*********************************************** 更新全部的数据 ********************************************************/
    /**
     * 更新STOCK_INDEX_WEEK表的基础数据
     */
    public void writeStockIndexWeek() {
        logger.info("开始计算STOCK_INDEX_WEEK表的基本数据");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
        sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekMapper.writeStockIndexWeek");
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源

        logger.info("STOCK_INDEX_WEEK表的基本数据计算完成");
    }

    /**
     * 更新STOCK_INDEX_WEEK表的Hei Kin Ashi数据
     */
    public void writeStockIndexWeekHeiKinAshi() {
        logger.info("write hei kin ashi data of table STOCK_INDEX_WEEK begin");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
        sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekMapper.writeStockIndexWeekHeiKinAshi");
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源

        logger.info("write hei kin ashi data of table STOCK_INDEX_WEEK finish");
    }

    /************************************* 增量备份表STOCK_INDEX_WEEK *************************************************/
    /**
     * 根据开始日期beginDate和结束日期endDate获取StockIndexWeek对象
     */
    public List<StockIndexWeek> getStockIndexWeekWithinDate(String beginDate, String endDate) {
        logger.info("根据开始日期【" + beginDate + "】和结束日期【" + endDate + "】获取StockIndexWeek对象");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
        stockIndexWeekMapper = sqlSession.getMapper(StockIndexWeekMapper.class);

        Map<String, String> map = new HashMap<String, String>();
        map.put("beginDate", beginDate);
        map.put("endDate", endDate);

        //获取beginDate和endDate之间的日期
        List<StockIndexWeek> list = stockIndexWeekMapper.getStockIndexWeekWithinDate(map);
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源

        return list;
    }

    /************************************* 增量回复表STOCK_INDEX_WEEK中的数据 ********************************************/
    /**
     * 向表STOCK_INDEX_WEEK中插入StockIndexWeek对象
     */
    public void saveStockIndexWeek(StockIndexWeek stockIndexWeek) {
        logger.info("向表STOCK_INDEX_WEEK中插入StockIndexWeek对象");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
        stockIndexWeekMapper = sqlSession.getMapper(StockIndexWeekMapper.class);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", stockIndexWeek.getCode());
        map.put("beginDate", stockIndexWeek.getBeginDate());
        map.put("endDate", stockIndexWeek.getEndDate());
        map.put("openPrice", stockIndexWeek.getOpenPrice());
        map.put("closePrice", stockIndexWeek.getClosePrice());
        map.put("highestPrice", stockIndexWeek.getHighestPrice());
        map.put("lowestPrice", stockIndexWeek.getLowestPrice());
        map.put("changeAmount", stockIndexWeek.getChangeAmount());
        map.put("changeRange", stockIndexWeek.getChangeRange());
        map.put("volume", stockIndexWeek.getVolume());
        map.put("turnover", stockIndexWeek.getTurnover());
        map.put("HAIndexWeekOpenPrice", stockIndexWeek.getHAIndexWeekOpenPrice());
        map.put("HAIndexWeekClosePrice", stockIndexWeek.getHAIndexWeekClosePrice());
        map.put("HAIndexWeekHighestPrice", stockIndexWeek.getHAIndexWeekHighestPrice());
        map.put("HAIndexWeekLowestPrice", stockIndexWeek.getHAIndexWeekLowestPrice());

        //获取beginDate和endDate之间的日期
        stockIndexWeekMapper.saveStockIndexWeek(map);
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源
    }

    /************************************* 查询 ********************************************/
    /**
     * 根据某个日期，返回这个日期所属的周的前一周的开始日期和结束日期
     *
     * @param date
     * @return
     */
    public StockWeekDatePojo findBeginDateAndEndDateInLastWeekByDate(String date) {
        logger.info("根据某个日期，返回这个日期【" + date + "】所属的周的开始日期和结束日期");

        sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
        stockIndexWeekMapper = sqlSession.getMapper(StockIndexWeekMapper.class);

        //获取beginDate和endDate之间的日期
        StockIndexWeek stockIndexWeek = stockIndexWeekMapper.findBeginDateAndEndDateInLastWeekByDate(date);
        StockWeekDatePojo stockWeekDatePojo = new StockWeekDatePojo();
        stockWeekDatePojo.setBeginDate(stockIndexWeek.getBeginDate());
        stockWeekDatePojo.setEndDate(stockIndexWeek.getEndDate());
        sqlSession.commit();// 提交会话，即事务提交
        sqlSession.close();// 关闭会话，释放资源

        return stockWeekDatePojo;
    }
}
