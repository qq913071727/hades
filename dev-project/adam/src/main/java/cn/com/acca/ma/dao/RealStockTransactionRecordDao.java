package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.RealStockTransactionRecord;
import cn.com.acca.ma.model.RobotStockTransactionRecord;
import cn.com.acca.ma.pojo.BuySuggestion;
import cn.com.acca.ma.pojo.SellSuggestion;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface RealStockTransactionRecordDao extends BaseDao {

    /**
     * 计算MACD的成功率
     * @param date
     * @param backwardMonth
     * @param averageDateNumber
     * @return
     */
    List<HashMap> calculateMACDSuccessRate(String date, Integer backwardMonth, Integer averageDateNumber);

    /**
     * 查找real_stock_transaction_record表中，sell_date、sell_price、sell_amount列都为空的记录
     */
    List<RealStockTransactionRecord> findBySellDateNullAndSellPriceNullAndSellAmountNull();

    /**
     * 查找real_stock_transaction_record表中，buy_date、buy_price、buy_amount列都为空的记录
     */
    List<RealStockTransactionRecord> findByBuyDateNullAndBuyPriceNullAndBuyAmountNull();

}
