package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelWeekBollHeiKinAshiDownUp;
import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;
import cn.com.acca.ma.service.ModelWeekBollHeiKinAshiDownUpService;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ModelWeekBollHeiKinAshiDownUpServiceImpl extends BaseServiceImpl<ModelWeekBollHeiKinAshiDownUpServiceImpl, ModelWeekBollHeiKinAshiDownUp> implements
        ModelWeekBollHeiKinAshiDownUpService {

    /**
     * 海量地向表MDL_WEEK_BOLL_MACD_GOLD_CROSS中插入数据
     */
    @Override
    public void writeModelWeekBollHeiKinAshiDownUp() {
        modelWeekBollHeiKinAshiDownUpDao.writeModelWeekBollHeiKinAshiDownUp();
    }

    /**
     * 创建周线级别最高价突破布林带上轨的百分比和hei_kin_ashi下跌趋势交易收益率的散点图，x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨
     */
    @Override
    public void createWeekHeighestPriceUpBollPercentAndHeiKinAshiDownUpProfitScatterPlotPicture() {
        String beginDate = PropertiesUtil.getValue(MODEL_WEEK_BOLL_PICTURE_PROPERTIES, "week.boll.scatter.begin_date");
        String endDate = PropertiesUtil.getValue(MODEL_WEEK_BOLL_PICTURE_PROPERTIES, "week.boll.scatter.end_date");

        // 获取某一段时间的收益率和最高价/最低价突破/跌破布林带上轨/下轨的比率
        List<ProfitOrLossAndWeekBollUpOrDnPercentage> profitOrLossAndWeekBollUpOrDnPercentageList = modelWeekBollHeiKinAshiDownUpDao.findWeekHeiKinAshiDownUpProfitOrLossAndBollUp(beginDate, endDate);
        // 用于存储需要最终显示的数据
        double[][] data = new double[2][];
        data[0] = new double[profitOrLossAndWeekBollUpOrDnPercentageList.size()];
        data[1] = new double[profitOrLossAndWeekBollUpOrDnPercentageList.size()];

        for (int i = 0; i < profitOrLossAndWeekBollUpOrDnPercentageList.size(); i++) {
            ProfitOrLossAndWeekBollUpOrDnPercentage profitOrLossAndWeekBollUpOrDnPercentage = profitOrLossAndWeekBollUpOrDnPercentageList.get(i);
            data[0][i] = profitOrLossAndWeekBollUpOrDnPercentage.getProfitOrLoss();
            data[1][i] = profitOrLossAndWeekBollUpOrDnPercentage.getWeekBollUpOrDnPercentage();
        }

        DefaultXYDataset xyDataset = new DefaultXYDataset();
        xyDataset.addSeries("Profit or loss and week boll up or dn percentage", data);

        JFreeChart chart = ChartFactory.createScatterPlot("profit or loss and week boll up or dn percentage",
                "Profit or loss", "week boll up or dn percentage", xyDataset, PlotOrientation.VERTICAL, true, false, false);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setNoDataMessage("NO DATA");
        plot.setDomainZeroBaselineVisible(true);
        plot.setRangeZeroBaselineVisible(true);

        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesOutlinePaint(0, Color.black);
        renderer.setUseOutlinePaint(true);

        // x axis
        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRange(true);

        // Y axis
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRange(true);

        XYTextAnnotation textAnnotation = new XYTextAnnotation("123", 370, 25); // r value
        textAnnotation.setPaint(Color.BLUE);
        textAnnotation.setToolTipText("Correlation Coefficient");

        plot.addAnnotation(textAnnotation);

        try {
            FileOutputStream fos = new FileOutputStream(PICTURE_PATH + "profit_or_loss_and_week_boll_up_or_dn_percentage/" + beginDate + "-" + endDate + "_hei_kin_ashi_down_up" + PICTURE_FORMAT);
            // 将统计图标输出成JPG文件
            ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
                    1, // JPEG图片的质量，0~1之间
                    chart, // 统计图标对象
                    imageWidth, // 宽
                    IMAGE_HEIGHT, // 高
                    null // ChartRenderingInfo 信息
            );
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
