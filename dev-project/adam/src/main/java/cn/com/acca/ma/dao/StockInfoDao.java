package cn.com.acca.ma.dao;

import java.util.List;

import cn.com.acca.ma.model.StockInfo;

public interface StockInfoDao extends BaseDao {
	
	/**
	 * 通过股票代码stockCode，查找StockInfo对象
	 * @param stockCode
	 * @return
	 */
	StockInfo getStockInfoObjectByStockCode(String stockCode);
	
	/**
	 * 获得所有StockInfo对象
	 * @return
	 */
	List<StockInfo> getAllStockInfo();
	
	/**
	 * 保存StockInfo对象
	 * @param stockInfo
	 */
	void saveStockInfo(StockInfo stockInfo);
}
