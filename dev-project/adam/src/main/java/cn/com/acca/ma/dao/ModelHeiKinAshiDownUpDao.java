package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelHeiKinAshiDownUp;
import cn.com.acca.ma.model.ModelHeiKinAshiUpDown;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelHeiKinAshiDownUpDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入，
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     * @param multithreading
     * @param endDate
     */
    void writeModelHeiKinAshiDownUpIncr(boolean multithreading, String endDate);

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入，
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     */
    void writeModelHeiKinAshiDownUp();

    /**
     * 获取一段时间内，最近dateNumber天，hei_kin_ashi下跌趋势的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    List<SuccessRateOrPercentagePojo> getHeiKinAshiDownUpSuccessRate(boolean multithreading, String beginDate, String endDate, Integer dateNumber);

    /**
     * 获取一段时间内，hei_kin_ashi下跌趋势的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<SuccessRateOrPercentagePojo> getHeiKinAshiDownUpPercentage(boolean multithreading, String beginDate, String endDate);

    /*********************************************************************************************************************
     *
     * 										增量备份表MDL_HEI_KIN_ASHI_DOWN_UP
     *
     *********************************************************************************************************************/
    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_HEI_KIN_ASHI_DOWN_UP中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_HEI_KIN_ASHI_DOWN_UP中获取date日的所有ModelHeiKinAshiDownUp对象
     * @param date
     * @return
     */
    List<ModelHeiKinAshiDownUp> getModelHeiKinAshiDownUpByDate(String date);

    /**
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP中插入ModelHeiKinAshiDownUp对象
     * @param modelHeiKinAshiDownUp
     */
    void save(ModelHeiKinAshiDownUp modelHeiKinAshiDownUp);
}
