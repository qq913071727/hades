package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建板块收盘价图
 */
public class CreateAllBoardIndexClosePictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateAllBoardIndexClosePictureThread.class);

    private BoardIndexService boardIndexService;

    public CreateAllBoardIndexClosePictureThread() {
    }

    public CreateAllBoardIndexClosePictureThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始创建板块收盘价图，调用的方法为【createAllBoardIndexClosePicture】");

        boardIndexService.createAllBoardIndexClosePicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     *
     */
    @Override
    public void generateSubReport() {

    }
}
