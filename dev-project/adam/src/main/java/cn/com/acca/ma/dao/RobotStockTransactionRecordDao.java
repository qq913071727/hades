package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.RobotStockTransactionRecord;
import cn.com.acca.ma.pojo.BuySuggestion;
import cn.com.acca.ma.pojo.SellSuggestion;
import java.util.List;

public interface RobotStockTransactionRecordDao extends BaseDao {

    /**
     * 卖股票/买股票
     * @param sellDate
     * @param mandatoryStopLoss
     * @param mandatoryStopLossRate
     */
    void sellOrBuy(String sellDate, Integer mandatoryStopLoss, Double mandatoryStopLossRate);

    /**
     * 买股票/卖股票。同时用于做多和做空
     * @param buyDate
     * @param backwardMonth
     * @param averageDateNumber
     * @param successRateType
     * @param successRate
     * @param direction
     * @param shippingSpaceControl
     * @param holdStockNumber
     */
    void buyOrSell(String buyDate, Integer backwardMonth, Integer averageDateNumber, Integer successRateType,
             Double successRate, Integer direction, Integer shippingSpaceControl, Integer holdStockNumber);

    /**
     * 买股票/卖股票。同时用于做多和做空
     * @param buyDate
     * @param backwardMonth
     * @param averageDateNumber
     * @param successRateType
     * @param successRate
     * @param direction
     * @param shippingSpaceControl
     * @param percentageTopThreshold
     * @param shippingSpace
     */
    void buyOrSell(String buyDate, Integer backwardMonth, Integer averageDateNumber, Integer successRateType,
                   Double successRate, Integer direction, Integer shippingSpaceControl, Double percentageTopThreshold,
                   Integer shippingSpace);

    /**
     * 获取买入建议列表
     * 用于做多
     * @param buyDate
     * @return
     */
    List<RobotStockTransactionRecord> getBullRobotStockTransactionRecordListForBuy(String buyDate);

    /**
     * 获取卖出建议列表
     * 用于做多
     * @param sellDate
     * @return
     */
    List<RobotStockTransactionRecord> getBullRobotStockTransactionRecordListForSell(String sellDate);

    /**
     * 获取卖出建议列表
     * 用于做空
     * @param sellDate
     * @return
     */
    List<RobotStockTransactionRecord> getShortRobotStockTransactionRecordListForSell(String sellDate);

    /**
     * 获取买入建议列表
     * 用于做空
     * @param buyDate
     * @return
     */
    List<RobotStockTransactionRecord> getShortRobotStockTransactionRecordListForBuy(String buyDate);

    /**
     * 获取卖建议列表
     * @param sellDate
     * @return
     */
    List<SellSuggestion> getSellSuggestionList(String sellDate);

    /**
     * 获取买建议列表
     * @param buyDate
     * @return
     */
    List<BuySuggestion> getBuySuggestionList(String buyDate);
}
