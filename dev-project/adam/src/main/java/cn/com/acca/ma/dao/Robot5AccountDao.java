package cn.com.acca.ma.dao;

public interface Robot5AccountDao extends BaseDao {

    /**************************************** 卖股票/买股票 ********************************************/
    /**
     * 根据当日持有股票的收盘价，在卖股票/买股票之前，更新robot5_account表
     * @param date
     */
    void updateRobotAccountBeforeSellOrBuy(String date);

    /**
     * 根据当日卖出/买入股票的收盘价，在卖股票/买股票之后，更新robot5_account表
     * @param date
     */
    void updateRobotAccountAfterSellOrBuy(String date);

    /**
     * 返回记录数
     * @return
     */
    Integer count();

    /**
     * 列hold_stock_number求和
     * @return
     */
    Integer sumHoldStockNumber();

    /**************************************** 买股票/卖股票 ********************************************/
    /**
     * 根据当日买入股票/卖出股票的收盘价，在买股票/卖股票之后，更新robot5_account表
     * @param date
     */
    void updateRobotAccountAfterBuyOrSell(String date);

}
