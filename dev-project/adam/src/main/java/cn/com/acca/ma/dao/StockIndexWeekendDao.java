package cn.com.acca.ma.dao;

import java.util.List;
import java.util.Map;

import cn.com.acca.ma.model.StockIndexWeekend;

public interface StockIndexWeekendDao extends BaseDao {
	/******************************************** 更新每周的数据 ********************************************************/
	/**
	 * 根据日期，更新STOCK_INDEX_WEEKEND表的基础数据
	 */
	void writeStockIndexWeekendByDate(String beginDate,String endDate);
	
	/**
	 * 根据日期，更新STOCK_INDEX_WEEKEND表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekendHeiKinAshiByDate(String beginDate,String endDate);
	
	/********************************************** 根据每周数据更新图表 *************************************************/
	/**
	 * 为创建周线级别StockIndexWeekend对象的Hei Kin Ashi图，获取数据
	 */
	List<StockIndexWeekend> getDataForLineChart(String beginTime,String endTime,String code);
	
	/****************************************** 更新全部的数据 **********************************************************/
	/**
	 * 更新STOCK_INDEX_WEEKEND表的基础数据
	 */
	void writeStockIndexWeekend();
	
	/**
	 * 更新STOCK_INDEX_WEEKEND表的Hei Kin Ashi数据
	 */
	void writeStockIndexWeekendHeiKinAshi();
	
	/************************************* 增量备份表STOCK_INDEX_WEEKEND *************************************************/
	/**
	 * 根据开始日期beginDate和结束日期endDate获取StockIndexWeekend对象
	 */
	List<StockIndexWeekend> getStockIndexWeekendWithinDate(String beginDate, String endDate);
	
	/************************************* 增量回复表STOCK_INDEX_WEEKEND中的数据 ********************************************/
	/**
	 * 向表STOCK_INDEX_WEEKEND中插入StockIndexWeekend对象
	 */
	void saveStockIndexWeekend(StockIndexWeekend stockIndexWeekend);
}
