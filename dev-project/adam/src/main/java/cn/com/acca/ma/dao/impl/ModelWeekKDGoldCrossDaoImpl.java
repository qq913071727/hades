package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.model.ModelWeekKDGoldCross;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import cn.com.acca.ma.dao.ModelWeekKDGoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;

public class ModelWeekKDGoldCrossDaoImpl extends BaseDaoImpl<ModelWeekKDGoldCrossDaoImpl> implements
	ModelWeekKDGoldCrossDao {

	public ModelWeekKDGoldCrossDaoImpl() {
		super();
	}

	/**
	 * 使用周线级别KD金叉模型算法，向表MDL_WEEK_KD_GOLD_CROSS插入数据
	 */
	public void writeModelWeekKDGoldCross() {
		logger.info("开始周线级别KD金叉算法");
		
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_WEEK_KD_GOLD_CROSS()}");
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
		
		logger.info("周线级别KD金叉算法结束");
	}

	/*********************************************************************************************************************
	 *
	 * 										增量备份表MDL_WEEK_KD_GOLD_CROSS
	 *
	 *********************************************************************************************************************/
	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_WEEK_KD_GOLD_CROSS中获取sell_end_date字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Date> getDateByCondition(String beginDate, String endDate){
		logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
			+ "从表MDL_WEEK_KD_GOLD_CROSS中获取sell_date字段");

		StringBuffer hql = new StringBuffer("select distinct t.sellEndDate from ModelWeekKDGoldCross t " +
			"where t.sellEndDate between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd')");
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql.toString());
		query.setParameter(0, beginDate);
		query.setParameter(1, endDate);
		List<Date> list = query.list();
		session.getTransaction().commit();
		session.close();

		return list;
	}

	/**
	 * 从表MDL_WEEK_KD_GOLD_CROSS中获取date日的所有ModelWeekKDGoldCross对象
	 * @param date
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ModelWeekKDGoldCross> getModelWeekKDGoldCrossByDate(String date){
		logger.info("从表MDL_WEEK_KD_GOLD_CROSS中获取日期【" + date + "】的所有ModelWeekKDGoldCross对象");

		StringBuffer hql = new StringBuffer("select t from ModelWeekKDGoldCross t " +
			"where t.sellEndDate=to_date(?,'yyyy-mm-dd') order by t.sellEndDate asc");
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql.toString());
		query.setParameter(0, date);
		List<ModelWeekKDGoldCross> list=query.list();
		session.getTransaction().commit();
		session.close();

		return list;
	}

	/**
	 * 向表MDL_WEEK_KD_GOLD_CROSS中插入ModelWeekKDGoldCross对象
	 * @param modelWeekKDGoldCross
	 */
	public void save(ModelWeekKDGoldCross modelWeekKDGoldCross){
		logger.info("向表MDL_WEEK_KD_GOLD_CROSS中插入ModelWeekKDGoldCross对象");

		session = HibernateUtil.currentSession();
		session.beginTransaction();
		session.persist(modelWeekKDGoldCross);
		session.getTransaction().commit();
		session.close();
	}
}
