package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 周线级别收盘价突破布林带上轨和下轨的股票数量的百分比
 */
@Data
public class StockWeekClosePriceUpDnBollPercentagePojo {

    /**
     * 一周的开始日期
     */
    private Date beginDate;

    /**
     * 一周的结束日期
     */
    private Date endDate;

    /**
     * 平均收盘价
     */
    private BigDecimal averageClosePirce;

    /**
     * 突破上轨的股票数量的百分比
     */
    private BigDecimal upBollPercentage;

    /**
     * 突破下轨的股票数量的百分比
     */
    private BigDecimal dnBollPercentage;


}
