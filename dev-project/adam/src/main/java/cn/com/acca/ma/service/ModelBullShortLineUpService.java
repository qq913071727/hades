package cn.com.acca.ma.service;

public interface ModelBullShortLineUpService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_BULL_SHORT_LINE_UP中插入数据
     */
    void writeModelBullShortLineUp();

    /**
     * 增量地向表MDL_BULL_SHORT_LINE_UP中插入数据
     */
    void writeModelBullShortLineUpIncr();
}
