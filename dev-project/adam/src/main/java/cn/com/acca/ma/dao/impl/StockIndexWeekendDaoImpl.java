package cn.com.acca.ma.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.dao.StockIndexWeekendDao;
import cn.com.acca.ma.model.StockIndexWeekend;
import cn.com.acca.ma.mybatis.util.MyBatisUtil;
import cn.com.acca.ma.pojo.StockWeekDatePojo;
import cn.com.acca.ma.xmlmapper.StockIndexWeekendMapper;

public class StockIndexWeekendDaoImpl extends BaseDaoImpl<StockIndexWeekendDaoImpl> implements StockIndexWeekendDao {
	/******************************************** 更新每周的数据 ********************************************************/
	/**
	 * 根据日期，更新STOCK_INDEX_WEEKEND表的基础数据
	 */
	public void writeStockIndexWeekendByDate(String beginDate,String endDate){
		logger.info("write basic data of table STOCK_INDEX_WEEKEND begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		
		Map<String,String> map=new HashMap<String,String>();
		map.put("beginTime", beginDate);
		map.put("endTime", endDate);
		
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekendMapper.writeStockIndexWeekendByDate",map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("write basic data of table STOCK_INDEX_WEEKEND finish");
	}
	
	/**
	 * 根据日期，更新STOCK_INDEX_WEEKEND表的Hei Kin Ashi数据
	 */
	public void writeStockIndexWeekendHeiKinAshiByDate(String beginDate,String endDate){
		logger.info("write hei kin ashi data of table STOCK_INDEX_WEEKEND begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		
		Map<String,String> map=new HashMap<String,String>();
		map.put("beginTime", beginDate);
		map.put("endTime", endDate);
		
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekendMapper.writeStockIndexWeekendHeiKinAshiByDate",map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("write hei kin ashi data of table STOCK_INDEX_WEEKEND finish");
	}
	
	/**************************************** 根据每周数据更新图表 *************************************************/
	/**
	 * 为创建周线级别StockIndexWeekend对象的Hei Kin Ashi图，获取数据
	 */
	public List<StockIndexWeekend> getDataForLineChart(String beginTime,String endTime,String code){
		logger.info("get data for line chart begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexWeekendMapper = sqlSession.getMapper(StockIndexWeekendMapper.class);
		
		Map<String,String> map=new HashMap<String,String>();
		map.put("beginTime", beginTime);
		map.put("endTime", endTime);
		map.put("code",code);
		
		//获取beginTime和endTime之间的日期
		List<StockIndexWeekend> list=stockIndexWeekendMapper.getDataForLineChart(map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("get data for line chart finish");
		return list;
	}
	
	/*********************************************** 更新全部的数据 ********************************************************/
	/**
	 * 更新STOCK_INDEX_WEEKEND表的基础数据
	 */
	public void writeStockIndexWeekend(){
		logger.info("write basic data of table STOCK_INDEX_WEEKEND begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekendMapper.writeStockIndexWeekend");
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("write basic data of table STOCK_INDEX_WEEKEND finish");
	}
	
	/**
	 * 更新STOCK_INDEX_WEEKEND表的Hei Kin Ashi数据
	 */
	public void writeStockIndexWeekendHeiKinAshi(){
		logger.info("write hei kin ashi data of table STOCK_INDEX_WEEKEND begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		sqlSession.insert("cn.com.acca.ma.xmlmapper.StockIndexWeekendMapper.writeStockIndexWeekendHeiKinAshi");
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("write hei kin ashi data of table STOCK_INDEX_WEEKEND finish");
	}
	
	/************************************* 增量备份表STOCK_INDEX_WEEKEND *************************************************/
	/**
	 * 根据开始日期beginDate和结束日期endDate获取StockIndexWeekend对象
	 */
	public List<StockIndexWeekend> getStockIndexWeekendWithinDate(String beginDate, String endDate){
		logger.info("get StockIndexWeekend object with date begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexWeekendMapper = sqlSession.getMapper(StockIndexWeekendMapper.class);
		
		Map<String,String> map=new HashMap<String,String>();
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		
		//获取beginDate和endDate之间的日期
		List<StockIndexWeekend> list=stockIndexWeekendMapper.getStockIndexWeekendWithinDate(map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("get StockIndexWeekend object within date finish");
		return list;
	}

	/************************************* 增量回复表STOCK_INDEX_WEEKEND中的数据 ********************************************/
	/**
	 * 向表STOCK_INDEX_WEEKEND中插入StockIndexWeekend对象
	 */
	public void saveStockIndexWeekend(StockIndexWeekend stockIndexWeekend){
		logger.info("insert StockIndexWeekend object with date begin");
		
		sqlSession = MyBatisUtil.openSqlSession();// 打开会话，事务开始;
		stockIndexWeekendMapper = sqlSession.getMapper(StockIndexWeekendMapper.class);
		
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("indexCode", stockIndexWeekend.getIndexCode());
		map.put("amount", stockIndexWeekend.getAmount());
		map.put("haIndexClose", stockIndexWeekend.getHaIndexClose());
		map.put("haIndexHigh", stockIndexWeekend.getHaIndexHigh());
		map.put("haIndexLow", stockIndexWeekend.getHaIndexLow());
		map.put("haIndexOpen", stockIndexWeekend.getHaIndexOpen());
		map.put("indexBeginDate", stockIndexWeekend.getIndexBeginDate());
		map.put("indexClose", stockIndexWeekend.getIndexClose());
		map.put("indexEndDate", stockIndexWeekend.getIndexEndDate());
		map.put("indexHigh", stockIndexWeekend.getIndexHigh());
		map.put("indexLow", stockIndexWeekend.getIndexLow());
		map.put("indexOpen", stockIndexWeekend.getIndexOpen());
		
		//获取beginDate和endDate之间的日期
		stockIndexWeekendMapper.saveStockIndexWeekend(map);
		sqlSession.commit();// 提交会话，即事务提交
		sqlSession.close();// 关闭会话，释放资源
		
		logger.info("insert StockIndexWeekend object within date finish");
	}
}
