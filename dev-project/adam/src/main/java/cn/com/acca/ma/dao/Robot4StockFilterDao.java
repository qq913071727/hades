package cn.com.acca.ma.dao;

public interface Robot4StockFilterDao extends BaseDao {

    /**
     * 清空robot4_stock_filter表
     */
    void truncateTableRobotStockFilter();

    /**
     * 只保留这个价格区间以内的股票
     * @param date
     * @param closePriceStart
     * @param closePriceEnd
     */
    void filterByLessThanClosePrice(String date, Double closePriceStart, Double closePriceEnd);

    /**
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=阈值
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价小于年线的记录数（数量更多）/收盘价大于年线的记录数（数量更少）<=阈值
     * @param beginDate
     * @param endDate
     * @param rate
     */
    void filterByClosePriceGreatThenLessThenMA250(String beginDate, String endDate, Double rate);

    /**
     * 根据date，从stock_transaction_data表中，向robot4_stock_filter表中插入股票代码
     * 用于做多
     * @param date
     */
    void insertBullStockCodeFromStockTransactionDataByDate(String date);

    /**
     * 根据date，从stock_transaction_data表中，向robot4_stock_filter表中插入股票代码
     * 用于做空
     * @param date
     */
    void insertShortStockCodeFromStockTransactionDataByDate(String date);

    /**
     * 只插入收盘价跌破周线级别布林带上轨的股票记录
     * 用于做多
     * @param beginDate
     * @param endDate
     */
    void insertStockWeekBollDnByDate(String beginDate, String endDate);

    /**
     * 只插入收盘价突破周线级别布林带上轨的股票记录
     * 用于做空
     * @param beginDate
     * @param endDate
     */
    void insertStockWeekBollUpByDate(String beginDate, String endDate);

    /**
     * 过滤条件：MACD金叉
     * 用于做多
     * @param date
     */
    void filterByMACDGoldCross(String date);

    /**
     * 过滤条件：MACD死叉
     * 用于做空
     * @param date
     */
    void filterByMACDDeadCross(String date);

    /**
     * 过滤条件：删除除过权的股票
     * @param beginDate
     * @param endDate
     */
    void filterByXr(String beginDate, String endDate);

    /**
     * 过滤条件：删除周线级别，KD死叉的股票
     * @param date
     */
    void filterByWeekKDGoldCross(String date);

    /**
     * 过滤条件：删除周线级别，KD金叉的股票
     * @param date
     */
    void filterByStockWeekKDGoldCross(String date);

    /**
     * 过滤条件：周线级别KD死叉
     * 用于做空
     * @param date
     */
    void filterByStockWeekKDDeadCross(String date);

    /**
     * 过滤条件：删除周线级别，上一周KD死叉的股票
     * @param date
     */
    void deleteLastWeekKDDeadCross(String date);

    /**
     * 过滤条件：删除周线级别，上一周KD金叉的股票
     * @param date
     */
    void deleteLastWeekKDGoldCross(String date);

    /**
     * 过滤条件：删除周线级别close_price死叉ma5
     * 用于做多
     * @param date
     */
    void filterByStockWeekClosePriceMa5GoldCross(String date);

    /**
     * 过滤条件：删除周线级别close_price金叉ma5
     * 用于做空
     * @param date
     */
    void filterByStockWeekClosePriceMa5DeadCross(String date);

    /**
     * 过滤条件：删除周线级别，上一周close_price死叉ma5的股票
     * @param date
     */
    void deleteLastWeekClosePriceDeadCrossMa5(String date);

    /**
     * 过滤条件：删除周线级别，上一周close_price金叉ma5的股票
     * @param date
     */
    void deleteLastWeekClosePriceGoldCrossMa5(String date);

    /**
     * 删除本周close_price比上一周close_price大的股票
     * @param date
     */
    void deleteThisWeekClosePriceGreatThanLastWeekClosePrice(String date);

    /**
     * 删除本周close_price比上一周close_price小的股票
     * @param date
     */
    void deleteThisWeekClosePriceLessThanLastWeekClosePrice(String date);

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价大于上上周hei_kin_ashi收盘价的股票
     * @param date
     */
    void deleteLastWeekHeiKinAshiClosePriceGreatThanLastTwoWeekHeiKinAshiClosePrice(String date);

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价小于上上周hei_kin_ashi收盘价的股票
     * @param date
     */
    void deleteLastWeekHeiKinAshiClosePriceLessThanLastTwoWeekHeiKinAshiClosePrice(String date);

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价大于上周hei_kin_ashi收盘价的股票
     * @param date
     */
    void deleteThisWeekHeiKinAshiClosePriceGreatThanLastWeekHeiKinAshiClosePrice(String date);

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价小于上周hei_kin_ashi收盘价的股票
     * @param date
     */
    void deleteThisWeekHeiKinAshiClosePriceLessThanLastWeekHeiKinAshiClosePrice(String date);

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价是阳线的股票
     * @param date
     */
    void deleteThisWeekHeiKinAshiClosePricePositiveLine(String date);

    /**
     * 过滤条件：删除周线级别，本周hei_kin_ashi收盘价是阴线的股票
     * @param date
     */
    void deleteThisWeekHeiKinAshiClosePriceNegativeLine(String date);

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价是阳线的股票
     * @param date
     */
    void deleteLastWeekHeiKinAshiClosePricePositiveLine(String date);

    /**
     * 过滤条件：删除周线级别，上一周hei_kin_ashi收盘价是阴线的股票
     * @param date
     */
    void deleteLastWeekHeiKinAshiClosePriceNegativeLine(String date);

    /**
     * 过滤条件：删除上周K比上上周K小的股票
     * @param date
     */
    void deleteLastWeekKDownThanLastTwoWeekK(String date);

    /**
     * 过滤条件：删除这周K比上周K小的股票
     * @param date
     */
    void deleteThisWeekKDownThanLastTwoWeekK(String date);

    /**
     * 过滤条件：删除上周K比上上周K大的股票
     * @param date
     */
    void deleteLastWeekKUpThanLastTwoWeekK(String date);

    /**
     * 过滤条件：删除这周K比上周K大的股票
     * @param date
     */
    void deleteThisWeekKUpThanLastTwoWeekK(String date);

    /**
     * 过滤条件：当前收盘价与某段时间最高价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做多
     * @param beginDate
     * @param date
     * @param percentage
     */
    void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(String beginDate, String date, Integer percentage);

    /**
     * 过滤条件：当前收盘价与某段时间最低价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做空
     * @param beginDate
     * @param date
     * @param percentage
     */
    void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(String beginDate, String date, Integer percentage);

    /**
     * 过滤条件：MA不单调递减，过滤股票
     * 用于做多
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    void filterByMANotDecreasing(String date, Integer maLevel, String dateNumber);

    /**
     * 过滤条件：MA不单调递增，过滤股票
     * 用于做空
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    void filterByMANotIncreasing(String date, Integer maLevel, String dateNumber);

    /**
     * 过滤条件：保留close_price金叉ma的股票
     * 用于做多
     * @param date
     */
    void filterByClosePriceGoldCrossMA5(String date);

    /**
     * 过滤条件：保留close_price死叉ma的股票
     * 用于做空
     * @param date
     */
    void filterByClosePriceDeadCrossMA5(String date);

    /**
     * 过滤条件：平均K线从下跌趋势变为上涨趋势
     * 用于做多
     * @param date
     */
    void filterByHeiKinAshiUpDown(String date);

    /**
     * 过滤条件：平均K线从上涨趋势变为下跌趋势
     * 用于做空
     * @param date
     */
    void filterByHeiKinAshiDownUp(String date);

    /**
     * 过滤条件：保留KD金叉的股票
     * 用于做多
     * @param date
     */
    void filterByKDGoldCross(String date);

    /**
     * 过滤条件：保留KD死叉的股票
     * 用于做空
     * @param date
     */
    void filterByKDDeadCross(String date);

    /**
     * 过滤条件：保留所有金叉
     * 用于做多
     * @param date
     */
    void filterByAllGoldCross(String date);

    /**
     * 过滤条件：保留所有死叉
     * 用于做空
     * @param date
     */
    void filterByAllDeadCross(String date);

    /**
     * 过滤条件：保留所有周线级别收盘价小于布林带中轨的股票
     * 用于做多
     * @param date
     */
    void filterWeekClosePriceDownBollMB(String date);

    /**
     * 过滤条件：保留所有周线级别收盘价大于布林带中轨的股票
     * 用于多空
     * @param date
     */
    void filterWeekClosePriceUpBollMB(String date);

    /**
     * 返回robot4_stock_filter表的记录数
     * @return
     */
    Integer count();
}
