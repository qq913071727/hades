/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.com.acca.ma.jfreechart.util;

import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;

public class MySpriderWebPlotTest {
    //带刻度蜘蛛网
    public static void main(String args[]) {
        JFrame jf = new JFrame();
        jf.add(erstelleSpinnenDiagramm());
        jf.pack();
        jf.setVisible(true);
    }
    public static JPanel erstelleSpinnenDiagramm() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        String group1 = "apple ";
        dataset.addValue(5, group1, "w1");
        dataset.addValue(6, group1, "w2");
        dataset.addValue(4, group1, "w3");
        dataset.addValue(2, group1, "w4");
        dataset.addValue(5, group1, "w5");
        dataset.addValue(5, group1, "w6");
        dataset.addValue(5, group1, "w7");
        dataset.addValue(8, group1, "w8");
        dataset.addValue(5, group1, "w9");
        dataset.addValue(6, group1, "w10");
        dataset.addValue(4, group1, "w11");
        dataset.addValue(2, group1, "w12");
        dataset.addValue(5, group1, "w13");
        dataset.addValue(5, group1, "w14");
        dataset.addValue(5, group1, "w15");
        dataset.addValue(8, group1, "w16");
        dataset.addValue(5, group1, "w17");
        dataset.addValue(6, group1, "w18");
        dataset.addValue(4, group1, "w19");
        dataset.addValue(2, group1, "w20");
        dataset.addValue(5, group1, "w21");
        dataset.addValue(5, group1, "w22");
        dataset.addValue(5, group1, "w23");
        dataset.addValue(8, group1, "w24");
        dataset.addValue(5, group1, "w25");
        dataset.addValue(6, group1, "w26");
        dataset.addValue(4, group1, "w27");
        dataset.addValue(2, group1, "w28");
        dataset.addValue(5, group1, "w29");
        dataset.addValue(5, group1, "w30");
        /*dataset.addValue(5, group1, "w31");
        dataset.addValue(8, group1, "w32");
        dataset.addValue(5, group1, "w33");
        dataset.addValue(6, group1, "w34");
        dataset.addValue(4, group1, "w35");
        dataset.addValue(2, group1, "w36");
        dataset.addValue(5, group1, "w37");
        dataset.addValue(5, group1, "w38");
        dataset.addValue(5, group1, "w39");
        dataset.addValue(8, group1, "w40");
        dataset.addValue(5, group1, "w41");
        dataset.addValue(6, group1, "w42");
        dataset.addValue(4, group1, "w43");
        dataset.addValue(2, group1, "w44");
        dataset.addValue(5, group1, "w45");
        dataset.addValue(5, group1, "w46");
        dataset.addValue(5, group1, "w47");
        dataset.addValue(8, group1, "w48");
        dataset.addValue(5, group1, "w49");
        dataset.addValue(6, group1, "w50");
        dataset.addValue(4, group1, "w51");
        dataset.addValue(2, group1, "w52");
        dataset.addValue(5, group1, "w53");
        dataset.addValue(5, group1, "w54");
        dataset.addValue(5, group1, "w55");
        dataset.addValue(8, group1, "w56");
        dataset.addValue(5, group1, "w57");
        dataset.addValue(6, group1, "w58");
        dataset.addValue(4, group1, "w59");
        dataset.addValue(2, group1, "w60");
        dataset.addValue(5, group1, "w61");
        dataset.addValue(5, group1, "w62");
        dataset.addValue(5, group1, "w63");
        dataset.addValue(8, group1, "w64");*/
        
        
        
        
        MySpiderWebPlot spiderwebplot = new MySpiderWebPlot(dataset);
        JFreeChart jfreechart = new JFreeChart("Test", TextTitle.DEFAULT_FONT,spiderwebplot, false);
        LegendTitle legendtitle = new LegendTitle(spiderwebplot);
        legendtitle.setPosition(RectangleEdge.BOTTOM);
        jfreechart.addSubtitle(legendtitle);
        
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        return chartpanel;
    }
    
    
    //带刻度雷达图
    /*public static void main(String args[]) {
        JFrame jf = new JFrame();
        jf.add(erstelleSpinnenDiagramm());
        jf.pack();
        jf.setVisible(true);
    }
    public static JPanel erstelleSpinnenDiagramm() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String group1 = "apple ";
        
        dataset.addValue(5, group1, "w1");
        dataset.addValue(6, group1, "w2");
        dataset.addValue(4, group1, "w3");
        dataset.addValue(2, group1, "w4");
        dataset.addValue(5, group1, "w5");
        dataset.addValue(5, group1, "w6");
        dataset.addValue(5, group1, "w7");
        dataset.addValue(8, group1, "w8");
        MySpiderWebPlot spiderwebplot = new MySpiderWebPlot(dataset);
        JFreeChart jfreechart = new JFreeChart("Test", TextTitle.DEFAULT_FONT,spiderwebplot, false);
        LegendTitle legendtitle = new LegendTitle(spiderwebplot);
        legendtitle.setPosition(RectangleEdge.BOTTOM);
        jfreechart.addSubtitle(legendtitle);
        
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        return chartpanel;
    }*/
    
    
    
    //蜘蛛网
    /*public static void main(String args[]) {
        JFrame jf = new JFrame();
        jf.add(erstelleSpinnenDiagramm());
        jf.pack();
        jf.setVisible(true);
    }
    public static JPanel erstelleSpinnenDiagramm() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String group1 = "apple ";
        
        dataset.addValue(5, group1, "w1");
        dataset.addValue(6, group1, "w2");
        dataset.addValue(4, group1, "w3");
        dataset.addValue(2, group1, "w4");
        dataset.addValue(5, group1, "w5");
        dataset.addValue(5, group1, "w6");
        dataset.addValue(5, group1, "w7");
        dataset.addValue(8, group1, "w8");
        String group2 = "orange ";
        dataset.addValue(3, group2, "w1");
        dataset.addValue(3, group2, "w2");
        dataset.addValue(4, group2, "w3");
        dataset.addValue(7, group2, "w4");
        dataset.addValue(4, group2, "w5");
        dataset.addValue(5, group2, "w6");
        dataset.addValue(3, group2, "w7");
        dataset.addValue(3, group2, "w8");
        String group3 = "banana ";
        dataset.addValue(4, group3, "w1");
        dataset.addValue(5, group3, "w2");
        dataset.addValue(2, group3, "w3");
        dataset.addValue(5, group3, "w4");
        dataset.addValue(6, group3, "w5");
        dataset.addValue(6, group3, "w6");
        dataset.addValue(4, group3, "w7");
        dataset.addValue(4, group3, "w8");
        SpiderWebPlot spiderwebplot = new SpiderWebPlot(dataset);
        JFreeChart jfreechart = new JFreeChart("Test", TextTitle.DEFAULT_FONT,spiderwebplot, false);
        LegendTitle legendtitle = new LegendTitle(spiderwebplot);
        legendtitle.setPosition(RectangleEdge.BOTTOM);
        jfreechart.addSubtitle(legendtitle);
        
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        return chartpanel;
    }*/
    
    
    //画雷达图
    /*public static void main(String args[]) {
        JFrame jf = new JFrame();
        jf.add(erstelleSpinnenDiagramm());
        jf.pack();
        jf.setVisible(true);
    }
    public static JPanel erstelleSpinnenDiagramm() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String group1 = "apple ";
        
        dataset.addValue(5, group1, "w1");
        dataset.addValue(6, group1, "w2");
        dataset.addValue(4, group1, "w3");
        dataset.addValue(2, group1, "w4");
        dataset.addValue(5, group1, "w5");
        dataset.addValue(5, group1, "w6");
        dataset.addValue(5, group1, "w7");
        dataset.addValue(8, group1, "w8");
    
        SpiderWebPlot spiderwebplot = new SpiderWebPlot(dataset);
        
        JFreeChart jfreechart = new JFreeChart("Test", TextTitle.DEFAULT_FONT,spiderwebplot, false);
        LegendTitle legendtitle = new LegendTitle(spiderwebplot);
        legendtitle.setPosition(RectangleEdge.BOTTOM);
        jfreechart.addSubtitle(legendtitle);
        
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        return chartpanel;
    }*/
}
