package cn.com.acca.ma.model;

import java.util.Date;

import javax.persistence.Id;

public class StockMovingAveragePK {
	@Id
	private Date stockDate;
	@Id
	private String stockCode;
	
	public StockMovingAveragePK() {
	}
	
	public StockMovingAveragePK(Date stockDate, String stockCode) {
		super();
		this.stockDate = stockDate;
		this.stockCode = stockCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((stockCode == null) ? 0 : stockCode.hashCode());
		result = prime * result
				+ ((stockDate == null) ? 0 : stockDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockMovingAveragePK other = (StockMovingAveragePK) obj;
		if (stockCode == null) {
			if (other.stockCode != null)
				return false;
		} else if (!stockCode.equals(other.stockCode))
			return false;
		if (stockDate == null) {
			if (other.stockDate != null)
				return false;
		} else if (!stockDate.equals(other.stockDate))
			return false;
		return true;
	}
}
