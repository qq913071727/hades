package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexKDGoldCross;

import java.util.List;

public interface ModelStockIndexKDGoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用KD金叉模型算法，向表MDL_STOCK_INDEX_KD_GOLD_CROSS插入数据
     */
    void writeModelStockIndexKDGoldCross();

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_kd_gold_cross中返回数据，并按照sell_date升序排列
     *
     * @param type
     * @param indexCode
     * @return
     */
    List<ModelStockIndexKDGoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
