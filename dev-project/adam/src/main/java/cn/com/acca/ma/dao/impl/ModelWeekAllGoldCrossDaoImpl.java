package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelWeekAllGoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelWeekAllGoldCross;
import org.hibernate.SQLQuery;

public class ModelWeekAllGoldCrossDaoImpl extends BaseDaoImpl<ModelWeekAllGoldCross> implements
        ModelWeekAllGoldCrossDao {

    public ModelWeekAllGoldCrossDaoImpl() {
        super();
    }

    /**
     * 海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据
     * @param type
     */
    @Override
    public void writeModelWeekAllGoldCross(String type) {
        logger.info("开始海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据，参数type为【" + type + "】");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_WEEK_ALL_GOLD_CROSS(?)}");
        query.setParameter(0, type);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据结束");
    }

}
