package cn.com.acca.ma.pojo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 周线级别布林带平均值
 */
@Data
public class StockWeekAverageBollPojo {

    /**
     * 一周的开始日期
     */
    private Date beginDate;

    /**
     * 一周的结束日期
     */
    private Date endDate;

    /**
     * 平均收盘价
     */
    private BigDecimal averageClosePrice;

    /**
     * 平均上轨值
     */
    private BigDecimal averageUP;

    /**
     * 平均中轨值
     */
    private BigDecimal averageMB;

    /**
     * 平均下轨值
     */
    private BigDecimal averageDN;

}
