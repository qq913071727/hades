package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.ModelMACDGoldCrossService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 使用日线级别MACD金叉模型算法，根据endDate日期，向表MDL_MACD_GOLD_CROSS插入数据
 */
public class WriteModelMACDGoldCrossIncrThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteModelMACDGoldCrossIncrThread.class);

    private ModelMACDGoldCrossService modelMACDGoldCrossService;

    public WriteModelMACDGoldCrossIncrThread() {
    }

    public WriteModelMACDGoldCrossIncrThread(ModelMACDGoldCrossService modelMACDGoldCrossService) {
        this.modelMACDGoldCrossService = modelMACDGoldCrossService;
    }

    @Override
    public void run() {
        logger.info("启动线程，使用日线级别MACD金叉模型算法，根据endDate日期，向表MDL_MACD_GOLD_CROSS插入数据，"
            + "调用的方法为【writeModelMACDGoldCrossIncr】");

        modelMACDGoldCrossService.writeModelMACDGoldCrossIncr(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
