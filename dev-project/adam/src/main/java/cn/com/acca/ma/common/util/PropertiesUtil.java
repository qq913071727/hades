package cn.com.acca.ma.common.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.List;

public class PropertiesUtil {

    /**
     * 获取指定properties文件中的value值
     *
     * @param file properties文件的路径和文件名
     * @param key  properties文件中的key
     * @return properties文件中的value
     */
    public static String getValue(String file, String key) {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.setEncoding("utf-8");
        config.setFileName(file);
        try {
            config.load();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return config.getProperty(key).toString();
    }

    /**
     * 获取指定properties文件中的value列表
     * @param file properties文件的路径和文件名
     * @param key properties文件中的key
     * @return properties文件中的value列表
     */
    public static List getList(String file, String key) {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.setEncoding("utf-8");
        config.setFileName(file);
        try {
            config.load();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return config.getList(key);
    }

    /**
     * 设置指定properties文件中的value值
     *
     * @param file  properties文件的路径和文件名
     * @param key   properties文件中的key
     * @param value properties文件中的value
     */
    public static void setValue(String file, String key, String value) {
        try {
            PropertiesConfiguration config = new PropertiesConfiguration();
            config.setEncoding("utf-8");
            config.setFileName(file);
            config.setProperty(key, value);
            config.save();
        } catch (ConfigurationException e1) {
            e1.printStackTrace();
        }
    }
}
