package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelStockAnalysis;
import cn.com.acca.ma.model.ModelStockMonthAnalysis;
import cn.com.acca.ma.service.ModelStockAnalysisService;
import cn.com.acca.ma.service.ModelStockMonthAnalysisService;
import java.util.List;

public class ModelStockMonthAnalysisServiceImpl extends BaseServiceImpl<ModelStockMonthAnalysisServiceImpl, ModelStockMonthAnalysis> implements
    ModelStockMonthAnalysisService {

    /**
     * 计算model_stock_analysis表的全部数据
     */
    @Override
    public void writeModelStockMonthAnalysis() {
        modelStockMonthAnalysisDao.writeModelStockMonthAnalysis();
    }

    /**
     * 计算model_stock_month_analysis表的某一日的数据
     * @param multithreading
     */
    @Override
    public void writeModelStockMonthAnalysisByDate(boolean multithreading){
        String beginDate = PropertiesUtil.getValue(MODEL_STOCK_MONTH_ANALYSIS, "stock.month.analysis.beginDate");
        String endDate = PropertiesUtil.getValue(MODEL_STOCK_MONTH_ANALYSIS, "stock.month.analysis.endDate");
        modelStockMonthAnalysisDao.writeModelStockMonthAnalysisByDate(multithreading, beginDate, endDate);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}

