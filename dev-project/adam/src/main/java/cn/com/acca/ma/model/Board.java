package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 板块信息表
 */
@Entity
@Table(name="BOARD")
public class Board implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 表示板块ID
	 */
	@Id
	@Column(name="ID")
	// 如果添加了下面这个注释，则在恢复数据时，id字段不再是之前备份的，而是程序自己加上去的序号，其他的实体类不能注释这个
//	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private BigDecimal id;

	/**
	 * 表示板块名称
	 */
	@Column(name="NAME")
	private String name;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "board")  
	private List<StockInfo> stockCode=new ArrayList<StockInfo>();

	/**
	 * 表示板块中股票的数量
	 */
	@Column(name="STOCK_AMOUNT")
	private BigDecimal stockAmount;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<StockInfo> getStockCode() {
		return stockCode;
	}

	public void setStockCode(List<StockInfo> stockCode) {
		this.stockCode = stockCode;
	}

	public BigDecimal getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(BigDecimal stockAmount) {
		this.stockAmount = stockAmount;
	}
}
