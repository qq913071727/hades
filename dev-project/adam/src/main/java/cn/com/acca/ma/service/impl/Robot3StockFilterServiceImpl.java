package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.constant.FilterMethod;
import cn.com.acca.ma.model.Robot3StockFilter;
import cn.com.acca.ma.model.RobotStockFilter;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import cn.com.acca.ma.service.Robot3StockFilterService;
import cn.com.acca.ma.service.RobotStockFilterService;

import java.util.List;

public class Robot3StockFilterServiceImpl extends BaseServiceImpl<Robot3StockFilterServiceImpl, Robot3StockFilter> implements
        Robot3StockFilterService {

    public Robot3StockFilterServiceImpl() {
        super();
    }

    /*********************************************************************************************************************
     *
     * 									      RobotMain类过滤股票
     *
     *********************************************************************************************************************/
    /**
     * 先清空robot_stock_filter表，再向其添加数据
     * 用于做多
     */
    public void initBullRobotStockFilter(){
        logger.info("先清空robot_stock_filter表，再向其添加数据。用于做多");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.insert.date");

        // 清空robot_stock_filter表
        robotStockFilterDao.truncateTableRobotStockFilter();

        // 根据date，从stock_transaction_data表中，向robot_stock_filter表中插入股票代码
        robotStockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(date);
    }

    /**
     * 先清空robot_stock_filter表，再向其添加数据
     * 用于做空
     */
    public void initShortRobotStockFilter(){
        logger.info("先清空robot_stock_filter表，再向其添加数据。用于做空");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.insert.date");

        // 清空robot_stock_filter表
        robotStockFilterDao.truncateTableRobotStockFilter();

        // 根据date，从stock_transaction_data表中，向robot_stock_filter表中插入股票代码
        robotStockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(date);
    }

    /**
     * 只保留收盘价在某个价格之下的股票
     */
    @Override
    public void filterByLessThanClosePrice() {
        logger.info("只保留收盘价在某个价格之下的股票");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.less.than.closePrice.date");
        Double closePriceStart = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.closePrice.start"));
        Double closePriceEnd = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.closePrice.end"));

        // 只保留收盘价在某个价格之下的股票
        robotStockFilterDao.filterByLessThanClosePrice(date, closePriceStart, closePriceEnd);
    }

    /**
     * 过滤条件：删除除过权的股票
     */
    @Override
    public void filterByXr() {
        logger.info("过滤条件：删除除过权的股票");

        String beginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.xr.begin.date");
        String endDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.xr.end.date");

        // 过滤条件：删除除过权的股票
        robotStockFilterDao.filterByXr(beginDate, endDate);
    }

    /**
     * 过滤条件：周线级别KD金叉
     */
    @Override
    public void filterByWeekKDGoldCross() {
        logger.info("过滤条件：周线级别KD金叉");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.week.kd.gold.cross.date");

        // 过滤条件：周线级别KD金叉
        robotStockFilterDao.filterByWeekKDGoldCross(date);
    }

    // 下面的方法由于多线程同时访问一个数据库，导致运行效率不高，或出现内存泄漏等问题，因此放弃这种方案
//	public void initStockTransactionDataListInModelConstant(){
//		logger.info("开始初始化Model.stockTransactionDataList对象");
//
//		String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.initStockTransactionDataList.date");
//
//		ModelGlobalVariable.renewStockTransactionDataList();
//
//		ModelGlobalVariable.stockTransactionDataList.addAll(stockTransactionDataDao.findByDate(date));
//	}

    // 下面的方法由于多线程同时访问一个数据库，导致运行效率不高，或出现内存泄漏等问题，因此放弃这种方案
//    public void filterByMANotDecreasing(){
//        logger.info("按照条件：MA不单调递减，过滤股票");
//
//        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.initStockTransactionDataList.date");
//        Integer maLevel = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.level"));
//        String dateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.dateNumber");
//        Integer judgeThreadIsFinishInterval = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.judge.thread.isFinish.interval"));
//        Integer threadNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.threadPool.threadNumber"));
//
//        ModelGlobalVariable.modelThreadFinishNumber = 0;
//        // 创建线程池
//        ExecutorService executorService = Executors.newFixedThreadPool(threadNumber);
//        Integer stockTransactionDataListSize = ModelGlobalVariable.stockTransactionDataList.size();
//        for (int i = 0; i < ModelGlobalVariable.stockTransactionDataList.size(); i++) {
////			if (((ThreadPoolExecutor)executorService).getActiveCount() % threadNumber == 0) {
////				try {
////					Thread.sleep(1000);
////				} catch (InterruptedException e) {
////					e.printStackTrace();
////				}
////			}
//            StockTransactionData stockTransactionData = ModelGlobalVariable.stockTransactionDataList.get(i);
//            String code = stockTransactionData.getCode();
//            executorService.execute(new FilterByMANotDecreasingThread(code, maLevel, date, Integer.parseInt(dateNumber), this.stockTransactionDataDao));
////			Thread thread = new Thread(new FilterByMANotDecreasingThread(code, maLevel, date, Integer.parseInt(dateNumber), this.stockTransactionDataDao));
////			thread.start();
//        }
//
//        while (true){
//            if (ModelGlobalVariable.modelThreadFinishNumber == stockTransactionDataListSize){
//                break;
//            } else {
//                try {
//
//                    Thread.sleep(judgeThreadIsFinishInterval);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        // 关闭线程池
//        executorService.shutdown();
//        System.out.println(ModelGlobalVariable.stockTransactionDataList);
//    }

    /**
     * 过滤条件：当前收盘价与某段时间最高价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做多
     */
    @Override
    public void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice() {
        logger.info("过滤条件：当前收盘价与某段时间最高价的百分比");

        String beginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.current.closePrice.compareTo.someTime.highest.lowest.price.begin.date");
        String endDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.current.closePrice.compareTo.someTime.highest.lowest.price.end.date");
        Integer percentage = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.current.closePrice.compareTo.someTime.highest.price"));

        // 过滤条件：当前收盘价与某段时间最高价的百分比
        robotStockFilterDao.filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(beginDate, endDate, percentage);
    }

    /**
     * 过滤条件：当前收盘价与某段时间最低价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做空
     */
    @Override
    public void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice() {
        logger.info("过滤条件：当前收盘价与某段时间最低价的百分比");

        String beginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.current.closePrice.compareTo.someTime.highest.lowest.price.begin.date");
        String endDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.current.closePrice.compareTo.someTime.highest.lowest.price.end.date");
        Integer percentage = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.current.closePrice.compareTo.someTime.lowest.price"));

        // 过滤条件：当前收盘价与某段时间最低价的百分比
        robotStockFilterDao.filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(beginDate, endDate, percentage);
    }

    /**
     * 过滤条件：MA不单调递减，过滤股票
     * 用于做多
     */
    public void filterByMANotDecreasing(){
        logger.info("过滤条件：MA不单调递减，过滤股票。用于做多");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.increasing.date");
        String maLevelString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.increasing.level");
        String dateNumberString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.increasing.dateNumber");

        String[] maLevelArray = maLevelString.replace("[", "").replace("]", "").split(",");
        String[] dateNumberArray = dateNumberString.replace("[", "").replace("]", "").split(",");

        for (int i=0; i<maLevelArray.length && i<dateNumberArray.length; i++){
            String maLevel = maLevelArray[i].trim();
            String dateNumber = dateNumberArray[i].trim();

            // 过滤条件：MA不单调递减，过滤股票
            logger.info("开始过滤均线【" + maLevel + "】，【" + dateNumber +"】日内不单调递减");
            robotStockFilterDao.filterByMANotDecreasing(date, Integer.parseInt(maLevel), dateNumber);
        }
    }

    /**
     * 过滤条件：MA不单调递增，过滤股票
     * 用于做空
     */
    public void filterByMANotIncreasing(){
        logger.info("过滤条件：MA不单调递增，过滤股票。用于做空");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.increasing.date");
        String maLevelString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.increasing.level");
        String dateNumberString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.ma.not.decreasing.increasing.dateNumber");

        String[] maLevelArray = maLevelString.replace("[", "").replace("]", "").split(",");
        String[] dateNumberArray = dateNumberString.replace("[", "").replace("]", "").split(",");

        for (int i=0; i<maLevelArray.length && i<dateNumberArray.length; i++){
            String maLevel = maLevelArray[i].trim();
            String dateNumber = dateNumberArray[i].trim();

            // 过滤条件：MA不单调递增，过滤股票
            logger.info("开始过滤均线【" + maLevel + "】，【" + dateNumber +"】日内不单调递增");
            robotStockFilterDao.filterByMANotIncreasing(date, Integer.parseInt(maLevel), dateNumber);
        }
    }

    /**
     * 选择最近成功率最高的那个算法
     * @param multithreading
     */
    @Override
    public void selectHighestSuccessRateStrategy(boolean multithreading){
        logger.info("选择最近成功率最高的那个算法");

        String beginDate = PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.all.transaction.strategy.success.rate.begin_date");
        String endDate = PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.all.transaction.strategy.success.rate.end_date");
        Integer dateNumber = Integer.parseInt(PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.all.transaction.strategy.success.rate.date_number"));
        Double threshold = Double.parseDouble(PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.all.transaction.strategy.success.rate.threshold"));

        // 计算所有算法的近期的成功率
        List<SuccessRateOrPercentagePojo> macdGoldCrossSuccessRateList = modelMACDGoldCrossDao.getMACDGoldCrossSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> macdDeadCrossSuccessRateList = modelMACDDeadCrossDao.getMACDDeadCrossSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> closePriceMa5GoldCrossSuccessRateList = modelClosePriceMA5GoldCrossDao.getClosePriceMa5GoldCrossSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> closePriceMa5DeadCrossSuccessRateList = modelClosePriceMA5DeadCrossDao.getClosePriceMa5DeadCrossSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> heiKinAshiUpDownSuccessRateList = modelHeiKinAshiUpDownDao.getHeiKinAshiUpDownSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> heiKinAshiDonwUpSuccessRateList = modelHeiKinAshiDownUpDao.getHeiKinAshiDownUpSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> kdGoldCrossSuccessRateList = modelKDGoldCrossDao.getKdGoldCrossSuccessRate(multithreading, beginDate, endDate, dateNumber);
        List<SuccessRateOrPercentagePojo> kdDeadCrossSuccessRateList = modelKDDeadCrossDao.getKdDeadCrossSuccessRate(multithreading, beginDate, endDate, dateNumber);

        // 判断哪种算法的成功率最高，默认为MACD金叉算法
        if (null != macdGoldCrossSuccessRateList && macdGoldCrossSuccessRateList.size() > 0){
            Double value = Double.valueOf(macdGoldCrossSuccessRateList.get(macdGoldCrossSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("MACD金叉算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_GOLD_CROSS;
            }
        }
        if (null != macdDeadCrossSuccessRateList && macdDeadCrossSuccessRateList.size() > 0){
            Double value = Double.valueOf(macdDeadCrossSuccessRateList.get(macdDeadCrossSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("MACD死叉算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.MACD_DEAD_CROSS;
            }
        }
        if (null != closePriceMa5GoldCrossSuccessRateList && closePriceMa5GoldCrossSuccessRateList.size() > 0){
            Double value = Double.valueOf(closePriceMa5GoldCrossSuccessRateList.get(closePriceMa5GoldCrossSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("close_price金叉ma5算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA;
            }
        }
        if (null != closePriceMa5DeadCrossSuccessRateList && closePriceMa5DeadCrossSuccessRateList.size() > 0){
            Double value = Double.valueOf(closePriceMa5DeadCrossSuccessRateList.get(closePriceMa5DeadCrossSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("close_price死叉ma5算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA;
            }
        }
        if (null != heiKinAshiUpDownSuccessRateList && heiKinAshiUpDownSuccessRateList.size() > 0){
            Double value = Double.valueOf(heiKinAshiUpDownSuccessRateList.get(heiKinAshiUpDownSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("hei kin ashi上升趋势算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_UP_DOWN;
            }
        }
        if (null != heiKinAshiDonwUpSuccessRateList && heiKinAshiDonwUpSuccessRateList.size() > 0){
            Double value = Double.valueOf(heiKinAshiDonwUpSuccessRateList.get(heiKinAshiDonwUpSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("hei kin ashi下跌趋势算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.HEI_KIN_ASHI_DOWN_UP;
            }
        }
        if (null != kdGoldCrossSuccessRateList && kdGoldCrossSuccessRateList.size() > 0){
            Double value = Double.valueOf(kdGoldCrossSuccessRateList.get(kdGoldCrossSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("KD金叉算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_GOLD_CROSS;
            }
        }
        if (null != kdDeadCrossSuccessRateList && kdDeadCrossSuccessRateList.size() > 0){
            Double value = Double.valueOf(kdDeadCrossSuccessRateList.get(kdDeadCrossSuccessRateList.size() - 1).getSuccessRateOrPercentage().toString());
            logger.info("KD死叉算法的成功率【" + value + "】");
            if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(value)){
                FilterMethod.SELECT_FILTER_METHOD_VALUE = value;
                FilterMethod.SELECT_FILTER_METHOD = FilterMethod.KD_DEAD_CROSS;
            }
        }

        // 如果最大的过滤算法成功率比阈值低，则不做任何交易
        if (-1 == FilterMethod.SELECT_FILTER_METHOD_VALUE.compareTo(threshold)){
            logger.info("所有的过滤算法中最大的成功率是【" + FilterMethod.SELECT_FILTER_METHOD_VALUE
                    + "】，对应的过滤算法是【" + FilterMethod.SELECT_FILTER_METHOD + "】，小于阈值【"
                    + threshold + "】，因此不做任何交易");
            FilterMethod.SELECT_FILTER_METHOD = null;
        } else {
            logger.info("采用的过滤算法【" + FilterMethod.SELECT_FILTER_METHOD + "】，其成功率【"
                    + FilterMethod.SELECT_FILTER_METHOD_VALUE + "】");
        }
    }

    /**
     * 过滤条件：使用最近成功率最高的那个算法过滤股票
     */
    public void filterByHighestSuccessRateStrategy() {
        logger.info("过滤条件：使用最近成功率最高的那个算法过滤股票");

        String endDate = PropertiesUtil
                .getValue(MODEL_FILTER_PROPERTIES, "filter.all.transaction.strategy.success.rate.end_date");

        // 执行过滤算法
        // 过滤条件：MACD金叉
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.MACD_GOLD_CROSS)){
            robotStockFilterDao.filterByMACDGoldCross(endDate);
        }
        // 过滤条件：收盘价金叉五日均线
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.CLOSE_PRICE_GOLD_CROSS_MA)){
            robotStockFilterDao.filterByClosePriceGoldCrossMA5(endDate);
        }
        // 过滤条件：平均K线从下跌趋势变为上涨趋势
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.HEI_KIN_ASHI_UP_DOWN)) {
            robotStockFilterDao.filterByHeiKinAshiUpDown(endDate);
        }
        // 过滤条件：KD金叉
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.KD_GOLD_CROSS)) {
            robotStockFilterDao.filterByKDGoldCross(endDate);
        }
        // 过滤条件：MACD死叉
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.MACD_DEAD_CROSS)){
            robotStockFilterDao.filterByMACDDeadCross(endDate);
        }
        // 过滤条件：收盘价死叉五日均线
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.CLOSE_PRICE_DEAD_CROSS_MA)){
            robotStockFilterDao.filterByClosePriceDeadCrossMA5(endDate);
        }
        // 过滤条件：平均K线从上涨趋势变为下跌趋势
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.HEI_KIN_ASHI_DOWN_UP)) {
            robotStockFilterDao.filterByHeiKinAshiDownUp(endDate);
        }
        // 过滤条件：KD死叉
        if (FilterMethod.SELECT_FILTER_METHOD.equals(FilterMethod.KD_DEAD_CROSS)) {
            robotStockFilterDao.filterByKDDeadCross(endDate);
        }
    }

    /**
     * 过滤条件：MACD金叉
     */
    @Override
    public void filterByMACDGoldCross() {
        logger.info("过滤条件：MACD金叉");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.macd.gold.cross.date");

        // 过滤条件：MACD金叉
        robotStockFilterDao.filterByMACDGoldCross(date);
    }

    /**
     * 过滤条件：收盘价金叉五日均线
     */
    @Override
    public void filterByClosePriceGoldCrossMA5() {
        logger.info("过滤条件：收盘价金叉五日均线");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.closePrice.goldCross.ma5.date");

        // 过滤条件：收盘价金叉五日均线
        robotStockFilterDao.filterByClosePriceGoldCrossMA5(date);
    }

    /**
     * 过滤条件：平均K线从下跌趋势变为上涨趋势
     */
    @Override
    public void filterByHeiKinAshiUp() {
        logger.info("过滤条件：平均K线从下跌趋势变为上涨趋势");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.heiKinAshi.up.date");

        // 过滤条件：平均K线从下跌趋势变为上涨趋势
        robotStockFilterDao.filterByHeiKinAshiUpDown(date);
    }

    /**
     * 过滤条件：KD金叉
     */
    @Override
    public void filterByKDGoldCross() {
        logger.info("过滤条件：KD金叉");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.kd.gold.cross.date");

        // 过滤条件：MACD金叉
        robotStockFilterDao.filterByKDGoldCross(date);
    }


    @Override
    public String listToString(List list) {
        return null;
    }

}
