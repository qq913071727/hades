package cn.com.acca.ma.hibernate.impl;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.Datum;
import oracle.sql.NUMBER;
import oracle.sql.STRUCT;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.jdbc.Work;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FIndGoldCrossDeadCrossDateCountWorkImpl implements Work {

    private static Logger logger = LogManager.getLogger(FIndGoldCrossDeadCrossDateCountWorkImpl.class);

    private Integer type;
    private String date;
    private List<Integer> numberList = new ArrayList<>();

    public FIndGoldCrossDeadCrossDateCountWorkImpl() {
    }

    public FIndGoldCrossDeadCrossDateCountWorkImpl(Integer type, String date, List<Integer> numberList) {
        this.type = type;
        this.date = date;
        this.numberList = numberList;
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        Connection metaDataConnection = null;
        if (null != databaseMetaData) {
            metaDataConnection = databaseMetaData.getConnection();
        }
        OracleCallableStatement ocs = (OracleCallableStatement) metaDataConnection.prepareCall("{call PKG_MODEL_G_C_D_C_ANALYSIS.find_g_c_d_c_c_date_count(?,?,?)}");
        ocs.setInt(1, type);
        ocs.setString(2, date);
        ocs.registerOutParameter(3, OracleTypes.NUMBER);
        ocs.execute();
        NUMBER num = ocs.getNUMBER(3);
        this.numberList.add(Integer.valueOf(num.intValue()));
    }
}
