package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.enumeration.ExchangeBoardInfo;
import cn.com.acca.ma.model.StockTransactionData;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 生产者线程，使用126接口，用于从网络上采集股票数据，并存储数据
 */
public class CollectStockTransactionDataThread_126 extends AbstractThread implements Runnable {
    private static Logger logger = LogManager.getLogger(CollectStockTransactionDataThread_126.class);

    private StockTransactionDataService stockTransactionDataService;

    /**
     * 股票代码的前缀
     */
    private String stockInfoCodePrefix;

    /**
     * 股票代码
     */
    private String code;

    /**
     * 开始时间
     */
    private String beginDate;

    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 前收盘
     */
    private BigDecimal lastClosePrice;

    public CollectStockTransactionDataThread_126(){

    }

    public CollectStockTransactionDataThread_126(StockTransactionDataService stockTransactionDataService,
        String stockInfoCodePrefix, String code, String beginDate, String endDate, BigDecimal lastClosePrice){

        this.stockTransactionDataService = stockTransactionDataService;
        this.stockInfoCodePrefix = stockInfoCodePrefix;
        this.code = code;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.lastClosePrice = lastClosePrice;
    }

    @Override
    public void run() {
        logger.debug("启动生产者线程。前缀为【" + stockInfoCodePrefix + "】，股票代码为【" + code + "】，"
            + "日期为【" + beginDate + "】至【" + endDate + "】");

        Socket socket = null;
        OutputStream outputStream = null;
        try {
            this.handle();
        } finally {
            if (null != socket) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 从网络上获取数据，并存储到数据库中
     * @return
     */
    private void handle(){
        // 截取年份
        String year = this.beginDate.substring(0, 4);

        String[] shStockCodePrefixArray = ExchangeBoardInfo.SH_PREFIX.getCodePrefixArray();
        for (String codePrefix : shStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_126.replace("{year}", year)
                    .replace("{prefix}", ExchangeBoardInfo.SH_PREFIX.getUrlParamPrefix())
                    .replace("{code}", code);

                // 从网络上获取股票数据
                List<StockTransactionData> stockTransactionDataList = stockTransactionDataService.readStockTransactionDataList_126(url, code, this.beginDate, this.endDate, this.lastClosePrice);
                // 将数据存储到数据库
                stockTransactionDataService.writeStockTransactionDataList(stockTransactionDataList);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] kcbStockCodePrefixArray = ExchangeBoardInfo.KCB_PREFIX.getCodePrefixArray();
        for (String codePrefix : kcbStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_126.replace("{year}", year)
                    .replace("{prefix}", ExchangeBoardInfo.KCB_PREFIX.getUrlParamPrefix())
                    .replace("{code}", code);

                // 从网络上获取股票数据
                List<StockTransactionData> stockTransactionDataList = stockTransactionDataService.readStockTransactionDataList_126(url, code, this.beginDate, this.endDate, this.lastClosePrice);
                // 将数据存储到数据库
                stockTransactionDataService.writeStockTransactionDataList(stockTransactionDataList);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] szStockCodePrefixArray = ExchangeBoardInfo.SZ_PREFIX.getCodePrefixArray();
        for (String codePrefix : szStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_126.replace("{year}", year)
                    .replace("{prefix}", ExchangeBoardInfo.SZ_PREFIX.getUrlParamPrefix())
                    .replace("{code}", code);

                // 从网络上获取股票数据
                List<StockTransactionData> stockTransactionDataList = stockTransactionDataService.readStockTransactionDataList_126(url, code, this.beginDate, this.endDate, this.lastClosePrice);
                // 将数据存储到数据库
                stockTransactionDataService.writeStockTransactionDataList(stockTransactionDataList);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] zxbStockCodePrefixArray = ExchangeBoardInfo.ZXB_PREFIX.getCodePrefixArray();
        for (String codePrefix : zxbStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_126.replace("{year}", year)
                    .replace("{prefix}", ExchangeBoardInfo.ZXB_PREFIX.getUrlParamPrefix())
                    .replace("{code}", code);

                // 从网络上获取股票数据
                List<StockTransactionData> stockTransactionDataList = stockTransactionDataService.readStockTransactionDataList_126(url, code, this.beginDate, this.endDate, this.lastClosePrice);
                // 将数据存储到数据库
                stockTransactionDataService.writeStockTransactionDataList(stockTransactionDataList);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] cybStockCodePrefixArray = ExchangeBoardInfo.CYB_PREFIX.getCodePrefixArray();
        for (String codePrefix : cybStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_126.replace("{year}", year)
                    .replace("{prefix}", ExchangeBoardInfo.CYB_PREFIX.getUrlParamPrefix())
                    .replace("{code}", code);

                // 从网络上获取股票数据
                List<StockTransactionData> stockTransactionDataList = stockTransactionDataService.readStockTransactionDataList_126(url, code, this.beginDate, this.endDate, this.lastClosePrice);
                // 将数据存储到数据库
                stockTransactionDataService.writeStockTransactionDataList(stockTransactionDataList);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
