//package cn.com.acca.ma.pojo;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.util.Date;
//
///**
// * 当前日期和前一日平均k和平均d
// */
////@Data
////@Entity
//public class CurrentDateAndPreviousDateAverageKD extends BaseCurrentDataAndPreviousDateAverage implements Comparable {
//
//    /**
//     * 当前日期
//     */
////    @Id
//    private String currentDate;
//
//    /**
//     * 前一日
//     */
////    private String previousDate;
//
//    /**
//     * 当前日期k和d的平均值
//     */
////    private BigDecimal averageKD;
//
//    /**
//     * 当前日期的平均k
//     */
////    private BigDecimal currentAverageK;
//
//    /**
//     * 当前日期的平均d
//     */
////    private BigDecimal currentAverageD;
//
//    /**
//     * 前一日的平均k
//     */
////    private BigDecimal previousAverageK;
//
//    /**
//     * 前一日的平均d
//     */
////    private BigDecimal previousAverageD;
//
//    /**
//     * 金叉或死叉。1表示金叉，2表示死叉
//     */
//    private Integer goldCrossOrDeadCross;
//
//    /**
//     * 降序排列
//     * @param o
//     * @return
//     */
//    @Override
//    public int compareTo(Object o) {
//        CurrentDateAndPreviousDateAverageKD currentDateAndPreviousDateAverageKD = (CurrentDateAndPreviousDateAverageKD)o;
//        return currentDateAndPreviousDateAverageKD.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage());
////        BigDecimal result = currentDateAndPreviousDateAverageDifDea.getCurrentAverageDif().add(currentDateAndPreviousDateAverageDifDea.getCurrentAverageDea()).divide(new BigDecimal(2))
////                .subtract(this.getCurrentAverageDif().add(this.getCurrentAverageDea()).divide(new BigDecimal(2)));
////        if (currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage()) > 0){
////            return 1;
////        }
////        if (result.doubleValue() < 0){
////            return  -1;
////        }
////        if (result.doubleValue() == 0){
////            return 0;
////        }
////        return 0;
//    }
//}
