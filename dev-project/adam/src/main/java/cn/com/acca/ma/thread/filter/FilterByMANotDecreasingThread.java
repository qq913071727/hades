// 下面的类由于多线程同时访问一个数据库，导致运行效率不高，或出现内存泄漏等问题，因此放弃这种方案

//package cn.com.acca.ma.thread.filter;
//
//import cn.com.acca.ma.constant.ModelConstant;
//import cn.com.acca.ma.dao.StockTransactionDataDao;
//import cn.com.acca.ma.globalvariable.ModelGlobalVariable;
//import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
//import cn.com.acca.ma.model.StockTransactionData;
//import cn.com.acca.ma.service.StockTransactionDataService;
//import cn.com.acca.ma.thread.AbstractThread;
//import cn.com.acca.ma.thread.write.writeBoardIndexFiveAndTenDayRateByDateThread;
//import java.math.BigDecimal;
//import java.util.List;
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//
///**
// * 按照条件：MA不单调递减，过滤股票
// */
//public class FilterByMANotDecreasingThread extends AbstractThread implements Runnable {
//
//    private static Logger logger = LogManager.getLogger(FilterByMANotDecreasingThread.class);
//
//    private String code;
//    private Integer maLevel;
//    private String date;
//    private Integer rownum;
//    private StockTransactionDataDao stockTransactionDataDao;
//
//    public FilterByMANotDecreasingThread() {
//    }
//
//    public FilterByMANotDecreasingThread(String code, Integer maLevel, String date, Integer rownum,
//        StockTransactionDataDao stockTransactionDataDao) {
//        this.code = code;
//        this.maLevel = maLevel;
//        this.date = date;
//        this.rownum = rownum;
//        this.stockTransactionDataDao = stockTransactionDataDao;
//    }
//
//    @Override
//    public void run() {
//        logger.info("启动线程，按照条件：MA不单调递减，过滤股票");
//
//        List<StockTransactionData> stockTransactionDataList = this.stockTransactionDataDao.findByCodeAndDateOrderByDateDescLtqRownum(this.code, this.date, this.rownum, Boolean.TRUE);
//
//        if (maLevel.equals(ModelConstant.MA5) && null != stockTransactionDataList.get(0).getMa5()){
//            BigDecimal currentMa5 = stockTransactionDataList.get(0).getMa5();
//            boolean increasing = true;
//            for (int i=1; i<stockTransactionDataList.size(); i++){
//                StockTransactionData stockTransactionData = stockTransactionDataList.get(i);
//                if (null != stockTransactionData.getMa5() && currentMa5.compareTo(stockTransactionData.getMa5()) == -1){
//                    increasing = false;
//                    break;
//                }
//                currentMa5 = stockTransactionData.getMa5();
//            }
//            if (!increasing){
//                this.removeByCode(ModelGlobalVariable.stockTransactionDataList, code);
//            }
//        }
//
//        if (maLevel.equals(ModelConstant.MA10) && null != stockTransactionDataList.get(0).getMa10()){
//            BigDecimal currentMa10 = stockTransactionDataList.get(0).getMa10();
//            boolean increasing = true;
//            for (int i=1; i<stockTransactionDataList.size(); i++){
//                StockTransactionData stockTransactionData = stockTransactionDataList.get(i);
//                if (null != stockTransactionData.getMa10() && currentMa10.compareTo(stockTransactionData.getMa10()) == -1){
//                    increasing = false;
//                    break;
//                }
//                currentMa10 = stockTransactionData.getMa10();
//            }
//            if (!increasing){
//                this.removeByCode(ModelGlobalVariable.stockTransactionDataList, code);
//            }
//        }
//
//        if (maLevel.equals(ModelConstant.MA20) && null != stockTransactionDataList.get(0).getMa20()){
//            BigDecimal currentMa20 = stockTransactionDataList.get(0).getMa20();
//            boolean increasing = true;
//            for (int i=1; i<stockTransactionDataList.size(); i++){
//                StockTransactionData stockTransactionData = stockTransactionDataList.get(i);
//                if (null != stockTransactionData.getMa20() && currentMa20.compareTo(stockTransactionData.getMa20()) == -1){
//                    increasing = false;
//                    break;
//                }
//                currentMa20 = stockTransactionData.getMa20();
//            }
//            if (!increasing){
//                this.removeByCode(ModelGlobalVariable.stockTransactionDataList, code);
//            }
//        }
//
//        if (maLevel.equals(ModelConstant.MA60) && null != stockTransactionDataList.get(0).getMa60()){
//            BigDecimal currentMa60 = stockTransactionDataList.get(0).getMa60();
//            boolean increasing = true;
//            for (int i=1; i<stockTransactionDataList.size(); i++){
//                StockTransactionData stockTransactionData = stockTransactionDataList.get(i);
//                if (null != stockTransactionData.getMa60() && currentMa60.compareTo(stockTransactionData.getMa60()) == -1){
//                    increasing = false;
//                    break;
//                }
//                currentMa60 = stockTransactionData.getMa60();
//            }
//            if (!increasing){
//                this.removeByCode(ModelGlobalVariable.stockTransactionDataList, code);
//            }
//        }
//
//        if (maLevel.equals(ModelConstant.MA120) && null != stockTransactionDataList.get(0).getMa120()){
//            BigDecimal currentMa120 = stockTransactionDataList.get(0).getMa120();
//            boolean increasing = true;
//            for (int i=1; i<stockTransactionDataList.size(); i++){
//                StockTransactionData stockTransactionData = stockTransactionDataList.get(i);
//                if (null != stockTransactionData.getMa120() && currentMa120.compareTo(stockTransactionData.getMa120()) == -1){
//                    increasing = false;
//                    break;
//                }
//                currentMa120 = stockTransactionData.getMa5();
//            }
//            if (!increasing){
//                this.removeByCode(ModelGlobalVariable.stockTransactionDataList, code);
//            }
//        }
//
//        if (maLevel.equals(ModelConstant.MA250) && null != stockTransactionDataList.get(0).getMa250()){
//            BigDecimal currentMa250 = stockTransactionDataList.get(0).getMa250();
//            boolean increasing = true;
//            for (int i=1; i<stockTransactionDataList.size(); i++){
//                StockTransactionData stockTransactionData = stockTransactionDataList.get(i);
//                if (null != stockTransactionData.getMa250() && currentMa250.compareTo(stockTransactionData.getMa250()) == -1){
//                    increasing = false;
//                    break;
//                }
//                currentMa250 = stockTransactionData.getMa250();
//            }
//            if (!increasing){
//                this.removeByCode(ModelGlobalVariable.stockTransactionDataList, code);
//            }
//        }
//
//        synchronized (AbstractThread.class){
//            ModelGlobalVariable.modelThreadFinishNumber++;
//            logger.info("已经完成了【" + ModelGlobalVariable.modelThreadFinishNumber
//                + "】个FilterByMANotDecreasingThread线程");
//        }
//    }
//
//    @Override
//    public void generateSubReport() {
//
//    }
//}
