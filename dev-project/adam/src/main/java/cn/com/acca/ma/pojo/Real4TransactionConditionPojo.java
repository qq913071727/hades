package cn.com.acca.ma.pojo;

import cn.com.acca.ma.model.Board;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Real4TransactionConditionPojo {

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 调用接口时的股票代码参数
     */
    private String urlParam;

    /**
     * 交易日期
     */
    private Date transactionDate;

    /**
     * 从数据库中查找到的开盘价
     */
    private BigDecimal openPriceFromDB;

    /**
     * 从数据库中查找到的收盘价
     */
    private BigDecimal closePriceFromDB;

    /**
     * 从数据库中查找到的最高价
     */
    private BigDecimal highestPriceFromDB;

    /**
     * 从数据库中查找到的最低价
     */
    private BigDecimal lowestPriceFromDB;

    /**
     * 前一个交易日的收盘价
     */
    private BigDecimal lastClosePrice;

    /**
     * 前一天的收盘价
     */
    private BigDecimal previousClosePrice;

    /**
     * 周线级别的收盘价
     */
    private BigDecimal weekClosePrice;

    /**
     * 周线级别的ma5
     */
    private BigDecimal weekMa5;

    /**
     * 周线级别的开始日期
     */
    private Date weekBeginDate;

    /**
     * 周线级别的结束日期
     */
    private Date weekEndDate;

    /**
     * 前一个交易日的ha_open_price
     */
    private BigDecimal lastHaOpenPrice;

    /**
     * 前一个交易日的ha_close_price
     */
    private BigDecimal lastHaClosePrice;

    /**
     * 最近8日内的最高收盘价
     */
    private BigDecimal highestPriceWith8Day;

    /**
     * 最近8日内的最低收盘价
     */
    private BigDecimal lowestPriceWith8Day;

    /**
     * macd金叉收盘价
     */
    private BigDecimal macdGoldCrossClosePrice;

    /**
     * macd死叉收盘价
     */
    private BigDecimal macdDeadCrossClosePrice;

    /**
     * 收盘价金叉ma5时的收盘价
     */
    private BigDecimal cPMa5GCClosePrice;

    /**
     * 收盘价死叉ma5时的收盘价
     */
    private BigDecimal cPMa5DCClosePrice;

    /**
     * kei_kin_ashi处于上涨趋势时的kei_kin_ashi开盘价
     */
    private BigDecimal hKAUpHKAOpenPrice;

    /**
     * kei_kin_ashi处于下跌趋势时的kei_kin_ashi开盘价
     */
    private BigDecimal hKADownHKAOpenPrice;

    /**
     * kd金叉时的RSV
     */
    private BigDecimal kdGoldCrossRsv;

    /**
     * kd死叉时的RSV
     */
    private BigDecimal kdDeadCrossRsv;

    /**
     * 表示股票名称
     */
    private String stockName;

    /**
     * R表示融资融券
     */
    private String mark;
}
