package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Robot2Main extends AbstractMain {

    private static Logger logger = LogManager.getLogger(Robot2Main.class);

    public static void main(String[] args) {
        logger.info("Robot2Main程序开始运行......");

        /******************************* 设置代理服务器的IP和port *******************************/
        setProxyProperties();

        /******************************* 收集数据库统计信息 *************************************/
//		gatherDatabaseStatistics();

        /*********************** 执行某段时间内的股票买卖交易（平均金叉死叉） ********************/
        doBuyAndSellByBeginDateAndEndDate_averageGoldCrossDeadCross();

        /******************************* 将测试数据导入到历史数据表中 *****************************/
//        importIntoHistoryDataTableFromRobot2();
    }
}
