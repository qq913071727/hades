package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.WeekDate;

import java.util.Date;

public interface WeekDateDao extends BaseDao {

    /**
     * 查找开始日期和结束日期在date之间的记录
     * @param date
     * @return
     */
    WeekDate findByDateBetween(Date date);
}
