package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelEtfKDGoldCross;
import cn.com.acca.ma.model.ModelStockIndexKDGoldCross;
import cn.com.acca.ma.service.ModelEtfKDGoldCrossService;
import cn.com.acca.ma.service.ModelStockIndexKDGoldCrossService;

import java.util.List;

public class ModelEtfKDGoldCrossServiceImpl extends BaseServiceImpl<ModelEtfKDGoldCrossServiceImpl, ModelEtfKDGoldCross> implements
        ModelEtfKDGoldCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用KD金叉模型算法，向表MDL_ETF_KD_GOLD_CROSS插入数据
     */
    @Override
    public void writeModelEtfKDGoldCross() {
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 0, "19970101", "20221231", 1);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 1, "19970101", "20221231", 2);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 0, "19970101", "20221231", 3);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 1, "19970101", "20221231", 4);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 0, "20110101", "20221231", 5);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 1, "20110101", "20221231", 6);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 0, "20110101", "20221231", 7);
        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 1, "20110101", "20221231", 8);
    }

    /*********************************************************************************************************************
     *
     * 												计算增量数据
     *
     *********************************************************************************************************************/
    /**
     * 使用KD金叉模型算法，向表MDL_ETF_KD_GOLD_CROSS插入数据
     */
    @Override
    public void writeModelEtfKDGoldCrossByDate() {
        String endDate = PropertiesUtil.getValue(MDL_ETF_KD_GOLD_CROSS_PROPERTIES, "etf.kd.gold.cross.end_date");

        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 0, "19970101", endDate, 1);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 1, "19970101", endDate, 2);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 0, "19970101", endDate, 3);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 1, "19970101", endDate, 4);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 0, "20110101", endDate, 5);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 1, "20110101", endDate, 6);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(1, 0, "20110101", endDate, 7);
//        modelEtfKDGoldCrossDao.writeModelEtfKDGoldCross(0, 1, "20110101", endDate, 8);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
