package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建日线级别板块的5日和10的涨跌幅度图
 */
public class CreateAllBoardIndexFiveAndTenDayRatePictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateClosePictureThread.class);

    private BoardIndexService boardIndexService;

    public CreateAllBoardIndexFiveAndTenDayRatePictureThread() {
    }

    public CreateAllBoardIndexFiveAndTenDayRatePictureThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始创建日线级别板块的5日和10的涨跌幅度图，调用的方法为【createClosePicture】");

        boardIndexService.createAllBoardIndexFiveAndTenDayRatePicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
