package cn.com.acca.ma.pojo;

import cn.com.acca.ma.model.Robot4StockTransactRecord;
import lombok.Data;

import java.util.List;

/**
 * 机器人买卖建议，json格式
 */
@Data
public class RobotBuySellSuggestionByJson_weekKD {

    /**
     * 做多卖出建议列表
     */
    private List<Robot4StockTransactRecord> bullSellSuggestionList;

    /**
     * 做多买入建议列表
     */
    private List<Robot4StockTransactRecord> bullBuySuggestionList;

    /**
     * 做空卖出建议列表
     */
    private List<Robot4StockTransactRecord> shortSellSuggestionList;

    /**
     * 做空买入建议列表
     */
    private List<Robot4StockTransactRecord> shortBuySuggestionList;


}
