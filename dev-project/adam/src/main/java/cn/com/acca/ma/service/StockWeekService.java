package cn.com.acca.ma.service;

public interface StockWeekService extends BaseService {
	/************************************************** 更新某一周的数据 ******************************************************/
	void writeStockWeekByDate();
	void writeStockWeekMAByDate();
	void writeStockWeekKDByDate();
	void writeStockWeekUpDownByDate();
	
	/**
	 * 更新所有股票某一周的MACD数据
	 */
	void writeStockWeekMACDByDate();

	/**
	 * 更新所有股票某一周的CHANGE_RANGE_EX_RIGHT字段
	 */
	void writeStockWeekChangeRangeExRightByDate();

	/**
	 * 更新所有股票某一周的与布林带相关的字段
	 */
	void writeStockWeekBollByDate();

	/**
	 * 更新所有股票某一周的与hei_kin_ashi的字段
	 */
	void writeStockWeekHeiKinAshiByDate();

	/******************************************** 根据一周数据更新KD图表 **********************************************************/
	void createWeekKDPicture(boolean multithreading);
	double obtainWeekK(String weekBegin,String weekEnd);
	double obtainWeekD(String weekBegin,String weekEnd);

	/**
	 * 根据周线级别数据更新boll和收盘价的图表
	 */
	void createWeekBollClosePricePicture();

	/**
	 * 根据周线级别数据更新收盘价突破上轨和下轨的股票数量的百分比
	 */
	void createWeekClosePriceUpDnBollPercentagePicture();

	/**
	 * 周线级别所有均线和收盘价的折线图
	 */
	void createWeekMAAndClosePricePicture();

	/**
	 * 周线级别hei_kin_ashi的折线图
	 */
	void createWeekHeiKinAshiUpDownPicture();
	
	/**
	 * 返回经过调整之后的平均收盘价
	 * @param original
	 * @param max
	 * @param min
	 * @return
	 */
	double averageCloseAfterAdjust(double original, double max, double min);
	
	/*************************************************** 更新所有的数据 *********************************************************/
	void writeStockWeek();
	void writeStockWeekMA();
	void writeStockWeekKD();
	void writeStockWeekUpDown();
	
	/**
	 * 更新STOCK_WEEK表中与MACD相关的所有字段：EMA12，EMA26，D,K
	 */
	void writeStockWeekMACD();

	/**
	 * 更新STOCK_WEEK表中的CHANGE_RANGE_EX_RIGHT字段
	 */
	void writeStockWeekChangeRangeExRight();

	/**
	 * 更新STOCK_WEEK表中的和布林带相关的字段：mb、up、dn
	 */
	void writeStockWeekBoll();

	/**
	 * 更新STOCK_WEEK表中的hei_kin_ashi相关的字段
	 */
	void writeStockWeekHeiKinAshi();
}
