package cn.com.acca.ma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 收盘价金叉牛熊线的交易
 */
@Entity
@Table(name = "MDL_BULL_SHORT_LINE_UP")
public class ModelBullShortLineUp {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_")
    private Integer id;

    /**
     * 股票代码
     */
    @Column(name = "STOCK_CODE")
    private String stockCode;

    /**
     * 买入日期
     */
    @Column(name = "BUY_DATE")
    private Date buyDate;

    /**
     * 买入价格
     */
    @Column(name = "BUY_PRICE")
    private BigDecimal buyPrice;

    /**
     * 卖出日期
     */
    @Column(name = "SELL_DATE")
    private Date sellDate;

    /**
     * 卖出价格
     */
    @Column(name = "SELL_PRICE")
    private BigDecimal sellPrice;

    /**
     * 买入时的牛熊线
     */
    @Column(name = "B_S_L_WHEN_BUY")
    private String bullShortLineWhenBuy;

    /**
     * 买入时的牛熊线价格
     */
    @Column(name = "B_S_L_PRICE_WHEN_BUY")
    private BigDecimal bullShortLinePriceWhenBuy;

    /**
     * 卖出时的牛熊线
     */
    @Column(name = "B_S_L_WHEN_SELL")
    private String bullShortLineWhenSell;

    /**
     * 卖出时的牛熊线价格
     */
    @Column(name = "B_S_L_PRICE_WHEN_SELL")
    private BigDecimal bullShortLinePriceWhenSell;

    /**
     * 累计收益。单位：元
     */
    @Column(name = "ACCUMULATIVE_PROFIT_LOSS")
    private BigDecimal accumulativeProfitLoss;

    /**
     * 本次交易盈亏百分比
     */
    @Column(name = "PROFIT_LOSS")
    private BigDecimal profitLoss;
}
