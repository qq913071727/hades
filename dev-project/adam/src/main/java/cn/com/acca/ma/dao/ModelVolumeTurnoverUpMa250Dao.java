package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelKDGoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelVolumeTurnoverUpMa250Dao extends BaseDao {
	/**
	 * 使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据
	 */
	void writeModelVolumeTurnoverUpMa250();

	/**
	 * 根据日期endDate，使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据
	 * @param endDate
	 */
	void writeModelVolumeTurnoverUpMa250Incr(String endDate);

}
