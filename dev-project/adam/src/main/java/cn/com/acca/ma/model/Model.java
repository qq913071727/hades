package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name="MODEL")
public class Model {

    /**
     * 主键
     */
    @Id
	@Column(name="ID")
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

    /**
     * 模型名称0
     */
	@Column(name="NAME")
	private String name;

    /**
     * 创建时间
     */
	@Column(name="CREATE_TIME")
	private Date createTime;

    /**
     * 测试开始时间
     */
    @Column(name="BEGIN_TIME")
    private Date beginTime;

    /**
     * 测试结束时间
     */
    @Column(name="END_TIME")
    private Date endTime;

    /**
     * 算法描述
     */
	@Column(name="ALGORITHM_DESCRIPTION")
	private String algorithmDescription;

    /**
     * 是否有仓位控制。1表示有，-1表示没有
     */
    @Column(name="S_POSITION_CONTROL")
    private Integer sPositionControl;

    /**
     * 平均年收益率
     */
    @Column(name="AVERAGE_ANNUAL_RETURN_RATE")
    private BigDecimal averageAnnualReturnRate;

    /**
     * 成功率计算时间。单位：天
     */
    @Column(name="SUCCESS_RATE_CALCULATE_TIME")
    private Integer successRateCalculateTime;

    /**
     * 成功率阈值。单位：百分比
     */
    @Column(name="SUCCESS_RATE_THRESHOLD")
    private Integer successRateThreshold;

    /**
     * 是否做多。1表示是，-1表示否
     */
    @Column(name="BUYING_LONG")
    private Integer buyingLong;

    /**
     * 是否做空。1表示是，-1表示否
     */
    @Column(name="SHORT_SELLING")
    private Integer shortSelling;

    /**
     * 1表示使用单一算法，2表示使用所有算法
     */
    @Column(name="P_SINGLE_OR_ALL_METHOD")
    private Integer pSingleOrAllMethod;

    /**
     * 在使用单一算法的条件下，出现了多个金叉或死叉的情况，此时使用哪种方法确定最后使用哪个算法。
     * 1表示通过金叉死叉成功率来判断，2表示通过金叉死叉百分比来判断
     */
    @Column(name="P_JUDGE_METHOD")
    private Integer pJudgeMethod;

    /**
     * 是否控制仓位。0表示不控制，1表示控制（成功率），2表示控制（百分比）
     */
    @Column(name="P_SHIPPING_SPACE_CONTROL")
    private Integer pShippingSpaceControl;

    /**
     * 当百分比小于这个阈值时才进行买卖交易（单位：%）
     */
    @Column(name="P_PERCENTAGE_TOP_THRESHOLD")
    private Integer pPercentageTopThreshold;

    /**
     * 当百分比大于这个阈值时才进行买卖交易（单位：%）
     */
    @Column(name="P_PERCENTAGE_BOTTOM_THRESHOLD")
    private Integer pPercentageBottomThreshold;

    /**
     * 如果为true（数据库列为1），只使用最少的过滤条件。如果为false（数据库列为2），使用所有过滤条件
     */
    @Column(name="P_USE_MIN_FILTER_CONDITION")
    private Integer pUseMinFilterCondition;

    /**
     * 是否有强制止损。0表示没有，1表示有
     */
    @Column(name="MANDATORY_STOP_LOSS")
    private Integer mandatoryStopLoss;

    /**
     * 强制止损率。当损失超过这个比率时，立刻平仓
     */
    @Column(name="MANDATORY_STOP_LOSS_RATE")
    private Integer mandatoryStopLossRate;
    
    /**
     * 是否有强制止盈。0表示没有，1表示有
     */
    @Column(name="MANDATORY_STOP_PROFIT")
    private Integer mandatoryStopProfit;

    /**
     * 强制止盈率。当损失超过这个比率时，立刻平仓
     */
    @Column(name="MANDATORY_STOP_PROFIT_RATE")
    private Integer mandatoryStopProfitRate;

    /**
     * 1表示旧的成功率算法，2表示新的百分比算法
     */
    @Column(name="TYPE_")
    private Integer type;

    /**
     * 每个账号的持股数量
     */
    @Column(name="HOLD_STOCK_NUMBER_PER_ACCOUNT")
    private Integer holdStockNumberPerAccount;

    /**
     * 账号数量
     */
    @Column(name="ACCOUNT_NUMBER")
    private Integer accountNumber;

    /**
     * 是否有ma120单调不递增。-1表示没有，如果不是-1则表示是多少天
     */
    @Column(name="ma120_not_increasing")
    private Integer ma120NotIncreasing;

    /**
     * 是否有ma120单调不递减。-1表示没有，如果不是-1则表示是多少天
     */
    @Column(name="ma120_not_decreasing")
    private Integer ma120NotDecreasing;

    /**
     * 是否有ma250单调不递增。-1表示没有，如果不是-1则表示是多少天
     */
    @Column(name="ma250_not_increasing")
    private Integer ma250NotIncreasing;

    /**
     * 是否有ma250单调不递减。-1表示没有，如果不是-1则表示是多少天
     */
    @Column(name="ma250_not_decreasing")
    private Integer ma250NotDecreasing;

    /**
     * 收盘价距离布林带上下轨的百分比
     */
    @Column(name="c_p_percentage_from_up_dn")
    private Double closePricePercentageFromUpDn;

    /**
     * 交易数量(macd)
     */
    @Column(name="macd_transaction_number")
    private Integer macdTransactionNumber;

    /**
     * 交易数量(close_price ma5)
     */
    @Column(name="C_P_MA5_TRANSACTION_NUMBER")
    private Integer cpMa5TransactionNumber;

    /**
     * 交易数量(hei_kin_ashi)
     */
    @Column(name="H_K_A_TRANSACTION_NUMBER")
    private Integer heiKinAshiTransactionNumber;

    /**
     * 交易数量(kd)
     */
    @Column(name="KD_TRANSACTION_NUMBER")
    private Integer kdTransactionNumber;
}
