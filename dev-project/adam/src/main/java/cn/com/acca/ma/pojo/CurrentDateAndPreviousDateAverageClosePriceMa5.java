//package cn.com.acca.ma.pojo;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.util.Date;
//
///**
// * 当前日期和前一日平均close_price和平均ma5
// */
////@Data
////@Entity
//public class CurrentDateAndPreviousDateAverageClosePriceMa5 extends BaseCurrentDataAndPreviousDateAverage implements Comparable {
//
//    /**
//     * 当前日期
//     */
////    @Id
//    private String currentDate;
//
//    /**
//     * 前一日
//     */
////    private String previousDate;
//
//    /**
//     * 当前日期close_price和ma5的平均值
//     */
////    private BigDecimal averageClosePriceMa5;
//
//    /**
//     * 当前日期的平均close_price
//     */
////    private BigDecimal currentAverageClosePrice;
//
//    /**
//     * 当前日期的平均ma5
//     */
////    private BigDecimal currentAverageMa5;
//
//    /**
//     * 前一日的平均close_price
//     */
////    private BigDecimal previousAverageClosePrice;
//
//    /**
//     * 前一日的平均ma5
//     */
////    private BigDecimal previousAverageMa5;
//
//    /**
//     * 金叉或死叉。1表示金叉，2表示死叉
//     */
//    private Integer goldCrossOrDeadCross;
//
//    /**
//     * 降序排列
//     * @param o
//     * @return
//     */
//    @Override
//    public int compareTo(Object o) {
//        CurrentDateAndPreviousDateAverageClosePriceMa5 currentDateAndPreviousDateAverageClosePriceMa5 = (CurrentDateAndPreviousDateAverageClosePriceMa5)o;
//        return currentDateAndPreviousDateAverageClosePriceMa5.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage());
////        BigDecimal result = currentDateAndPreviousDateAverageDifDea.getCurrentAverageDif().add(currentDateAndPreviousDateAverageDifDea.getCurrentAverageDea()).divide(new BigDecimal(2))
////                .subtract(this.getCurrentAverageDif().add(this.getCurrentAverageDea()).divide(new BigDecimal(2)));
////        if (currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage()) > 0){
////            return 1;
////        }
////        if (result.doubleValue() < 0){
////            return  -1;
////        }
////        if (result.doubleValue() == 0){
////            return 0;
////        }
////        return 0;
//    }
//}
