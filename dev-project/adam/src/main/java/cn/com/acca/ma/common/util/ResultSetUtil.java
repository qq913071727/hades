package cn.com.acca.ma.common.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * jdbc的ResultSet的工具类
 */
public class ResultSetUtil {

    /**
     * 将ResultSet类型对象转换为List类型对象，其中每个元素是一个Object[]
     * @param resultSet
     * @return
     */
    public static List convertList(ResultSet resultSet){
        List list = new ArrayList();
        try {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();//获取键名
            int columnCount = resultSetMetaData.getColumnCount();//获取行的数量
            while (resultSet.next()) {
                Object[] objectArray = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    Object object = resultSet.getObject(i);//获取键名及值
                    objectArray[i - 1] = object;
                }
                list.add(objectArray);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return list;
    }
}
