package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class WechatSubscriptionMain extends AbstractMain {

    private static Logger logger = LogManager.getLogger(WechatSubscriptionMain.class);

    public static void main(String[] args) {
        logger.info("WechatSubscriptionMain程序开始运行......");

        /*************************************** 设置代理服务器的IP和port ***********************************************/
        setProxyProperties();

        /*************************************** 收集数据库统计信息  ***********************************************/
//		gatherDatabaseStatistics();

        /*********************************************** 恢复数据库表 **************************************************/
//		restoreTableIncremental();

        /*********************************** 从网络上获取数据，并插入到数据库中 ***************************************/
//		insertStockTransactionData_163();
//		insertStockTransactionData_126();

        /********* 补充使用126接口没有的字段。使用126接口有些字段是空的，只能之后再使用163的接口将确实的字段补齐 ***********/
//		supplementDataBy163Api();

        /********* 从网络上获取数据，以json格式存储在ALL_RECORDS_IN_JSON.json文件中；然后再解析文件，并插入到数据库中 *************/
//		 readAndWriteAllRecordsByJson();
//		 insertAllRecordsFromJson();

        /***************************** 基于基础数据的并行处理：计算数据，生成图片 *********************************/
        asyncHandleBasedOnBasicData();

        /***************************** 基于移动平均线的并行处理：计算数据，生成图片 *********************************/
//        asyncHandleBasedOnMovingAverage();

        /***************************** 基于MACD的并行处理：计算数据，生成图片 *********************************/
//        asyncHandleBasedOnMACD();

        /***************************** 基于stock_index表的并行处理：计算数据，生成图片 *********************************/
//        asyncHandleBasedOnStockIndex();

        /***************************** 基于board_index的并行处理：计算数据，生成图片 *********************************/
//        asyncHandleBasedOnBoardIndex();

        /***************************** 基于周线级别数据的穿行处理：计算数据，生成图片 *********************************/
//        handleBasedOnWeekData();

        /*************************************** 生成微信订阅号报告 *******************************************/
//        generateWechatSubscriptionReport();

        /***************************** 生成微信订阅号报告之后的处理：计算数据 *********************************/
//        calculateBasicDataAfterGeneratingWechatSubscriptionReport();

        /***************************************** 打印文本报告 **********************************************************/
//		 printSubNewStock();

        /************************************ 删除stock_transaction_data表中的记录 **********************************/
//		deleteStockTransactionDataByDate();

        /************************************************* 备份项目代码 ***********************************************/
//		backupCode();

        /***************************************** 备份数据库/表 （海量和增量） *****************************************/
//		backupDatabaseIncremental();
//		backupDatabaseMass();

        logger.info("WechatSubscriptionMain程序执行完毕。");
    }


}
