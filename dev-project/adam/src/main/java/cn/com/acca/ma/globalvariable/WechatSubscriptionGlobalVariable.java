package cn.com.acca.ma.globalvariable;

/**
 * 微信订阅号全局变量
 */
public class WechatSubscriptionGlobalVariable {

    /**
     * 微信订阅号相关任务的线程的完成数量
     */
    public static Integer wechatSubscriptionThreadFinishNumber = 0;

}
