package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelStockIndexHeiKinAshiDownUpDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelStockIndexHeiKinAshiDownUp;
import cn.com.acca.ma.model.ModelStockIndexMACDGoldCross;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import java.util.List;

public class ModelStockIndexHeiKinAshiDownUpDaoImpl extends BaseDaoImpl<ModelStockIndexHeiKinAshiDownUp> implements
        ModelStockIndexHeiKinAshiDownUpDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_index_close_price小于等于ha_index_open_price时卖出，ha_index_close_price大于ha_index_open_price时买入
     * 向表mdl_stock_index_h_k_a_down_up插入数据
     */
    @Override
    public void writeModelStockIndexHeiKinAshiDownUp() {
        logger.info("开始向表mdl_stock_index_h_k_a_down_up插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_INDEX.CAL_MDL_STOCK_INDEX_H_K_A_D_U()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("向表mdl_stock_index_h_k_a_down_up插入数据完成");
    }

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
    @Override
    public List<ModelStockIndexHeiKinAshiDownUp> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode) {
        logger.info("开始根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery("select t from ModelStockIndexHeiKinAshiDownUp t " +
                "where t.stockIndexCode=? and t.type=? " +
                "order by t.sellDate asc");
        query.setParameter(0, indexCode);
        query.setParameter(1, type);
        List<ModelStockIndexHeiKinAshiDownUp> modelStockIndexHeiKinAshiDownUpList = query.list();
        session.getTransaction().commit();
        session.close();

        logger.info("根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列完成");

        return modelStockIndexHeiKinAshiDownUpList;
    }
}
