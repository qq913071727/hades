package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelWeekKDGoldCross;
import java.util.Date;
import java.util.List;

public interface ModelWeekKDGoldCrossDao extends BaseDao {
	/**
	 * 使用周线级别KD金叉模型算法，向表MDL_WEEK_KD_GOLD_CROSS插入数据
	 */
	void writeModelWeekKDGoldCross();

	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_WEEK_KD_GOLD_CROSS中获取sell_end_date字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<Date> getDateByCondition(String beginDate, String endDate);

	/**
	 * 从表MDL_WEEK_KD_GOLD_CROSS中获取date日的所有ModelKDGoldCross对象
	 * @param date
	 * @return
	 */
	List<ModelWeekKDGoldCross> getModelWeekKDGoldCrossByDate(String date);

	/**
	 * 向表MDL_WEEK_KD_GOLD_CROSS中插入ModelWeekKDGoldCross对象
	 * @param modelWeekKDGoldCross
	 */
	void save(ModelWeekKDGoldCross modelWeekKDGoldCross);
}
