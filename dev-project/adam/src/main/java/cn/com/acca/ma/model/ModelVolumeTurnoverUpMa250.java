package cn.com.acca.ma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MDL_VOLUME_TURNOVER_UP_MA250")
public class ModelVolumeTurnoverUpMa250 implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "STOCK_CODE")
	private String stockCode;

	@Column(name = "BUY_DATE")
	private Date buyDate;

	@Column(name = "BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name = "BUY_VOLUME")
	private BigDecimal buyVolume;

	@Column(name = "BUY_VOLUME_MA250")
	private BigDecimal buyVolumeMa250;

	@Column(name = "BUY_TURNOVER")
	private BigDecimal buyTurnover;

	@Column(name = "BUY_TURNOVER_MA250")
	private BigDecimal buyTurnoverMa250;

	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

	@Column(name = "SELL_DATE")
	private Date sellDate;

	@Column(name = "SELL_PRICE")
	private BigDecimal sellPrice;

	@Column(name = "SELL_VOLUME")
	private BigDecimal sellVolume;

	@Column(name = "SELL_VOLUME_MA250")
	private BigDecimal sellVolumeMa250;

	@Column(name = "SELL_TURNOVER")
	private BigDecimal sellTurnover;

	@Column(name = "SELL_TURNOVER_MA250")
	private BigDecimal sellTurnoverMa250;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getBuyVolume() {
		return buyVolume;
	}

	public void setBuyVolume(BigDecimal buyVolume) {
		this.buyVolume = buyVolume;
	}

	public BigDecimal getBuyVolumeMa250() {
		return buyVolumeMa250;
	}

	public void setBuyVolumeMa250(BigDecimal buyVolumeMa250) {
		this.buyVolumeMa250 = buyVolumeMa250;
	}

	public BigDecimal getBuyTurnover() {
		return buyTurnover;
	}

	public void setBuyTurnover(BigDecimal buyTurnover) {
		this.buyTurnover = buyTurnover;
	}

	public BigDecimal getBuyTurnoverMa250() {
		return buyTurnoverMa250;
	}

	public void setBuyTurnoverMa250(BigDecimal buyTurnoverMa250) {
		this.buyTurnoverMa250 = buyTurnoverMa250;
	}

	public BigDecimal getAccumulativeProfitLoss() {
		return accumulativeProfitLoss;
	}

	public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
		this.accumulativeProfitLoss = accumulativeProfitLoss;
	}

	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public BigDecimal getSellVolume() {
		return sellVolume;
	}

	public void setSellVolume(BigDecimal sellVolume) {
		this.sellVolume = sellVolume;
	}

	public BigDecimal getSellVolumeMa250() {
		return sellVolumeMa250;
	}

	public void setSellVolumeMa250(BigDecimal sellVolumeMa250) {
		this.sellVolumeMa250 = sellVolumeMa250;
	}

	public BigDecimal getSellTurnover() {
		return sellTurnover;
	}

	public void setSellTurnover(BigDecimal sellTurnover) {
		this.sellTurnover = sellTurnover;
	}

	public BigDecimal getSellTurnoverMa250() {
		return sellTurnoverMa250;
	}

	public void setSellTurnoverMa250(BigDecimal sellTurnoverMa250) {
		this.sellTurnoverMa250 = sellTurnoverMa250;
	}
}
