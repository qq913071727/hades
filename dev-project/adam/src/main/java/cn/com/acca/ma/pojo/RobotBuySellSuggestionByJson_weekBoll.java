package cn.com.acca.ma.pojo;

import cn.com.acca.ma.model.Robot3StockTransactRecord;
import cn.com.acca.ma.model.RobotStockTransactionRecord;
import lombok.Data;

import java.util.List;

/**
 * 机器人买卖建议，json格式
 */
@Data
public class RobotBuySellSuggestionByJson_weekBoll {

    /**
     * 做多卖出建议列表
     */
    private List<Robot3StockTransactRecord> bullSellSuggestionList;

    /**
     * 做多买入建议列表
     */
    private List<Robot3StockTransactRecord> bullBuySuggestionList;

    /**
     * 做空卖出建议列表
     */
    private List<Robot3StockTransactRecord> shortSellSuggestionList;

    /**
     * 做空买入建议列表
     */
    private List<Robot3StockTransactRecord> shortBuySuggestionList;


}
