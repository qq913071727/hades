package cn.com.acca.ma.main;

/**
 * 优先级别：bug性>算法性>功能性
 * 技术选型核心原则：成本要远远小于投入
 * <p>
 * 紧急型：
 * 1.搜集统计信息的存储过程GATHER_DATEBASE_STATISTICS执行之后，原本快的存储过程反而变慢了。
 * <p>
 * bug型：
 * 1.FIND_STOP_STOCK和FIND_LAST_STOCK可能还有问题，可能还需要优化。
 * <p>
 * 研究型：
 * 1.存储过程PKG_MODEL_RECORD.CAL_MDL_KD_GOLD_CROSS还缺少相应的按日期计算并插入记录的存储过程
 * 2.ModelMACDGoldCrossServiceImpl.java,ModelMACDGoldCrossDaoImpl.java类中创建MACD金叉散点图虽然做了，但是好像没有多大的用处。
 * 可以考虑对其进行改进
 * 3.表MDL_TOP_STOCK的计算间隔可以进行改进。可以按照斐波那契数列21, 34, 55, 89, 144, 233，377，610来设计数据库的列。
 * 之后用存储过程WRITE_TOP_STOCK计算数据，再用类ModelTopStockServiceImpl中的方法createTopStockUpDownPicture创建图片。
 * 这样就可以确定列的周期应该如何设计，在这个基础上再进行研究应该更有效果。
 * 4.表STOCK_INDEX中增加了heikinashi列，不过做出来的图似乎用处不大，可以考虑指数周线级别和个股周线级别的heikinashi。
 * 5.存储过程PKG_WEEKEND.WRITE_WEEKEND_BOLL是计算周线级别BOLL带的，还没有做完。
 * <p>
 * 优化型：
 * 存储过程WRITE_TOP_STOCK																	计算一年的数据需要9个小时，无法容忍
 * 存储过程pkg_weekend.FIND_WEEKEND_MACD_END_DEVIATE											00:44:40
 * 存储过程PKG_WEEKEND.WRITE_WEEKEND_KD_BY_DATE_RSV(beginDate varchar2,endDate varchar2)     00:13:26
 * 存储过程pkg_moving_average.FIND_LOWEST_MACD_STOCK											00:08:22
 * 存储过程pkg_moving_average.WRITE_MOVING_AVERAGE_BY_DATE(stockDate in varchar2)            00:08:02
 * 类ReportDaoImpl的方法findLowestKdStockWeekend												00:06:07
 * <p>
 * 功能型：
 * 1.ProjectServiceImpl.java文件中用json文件备份和回复。 备份还没做完，只做了几个表的备份，其余的还没有做。
 * 2.有个图表已经可以创建了，没有问题，但是似乎report报告里没有加进去。
 * 此外，生成report部分的设计有问题，应当将其划分为多个独立的小模块，再通过配置决定这次生成的报告中是否需要有这个模块。
 * 3.由于每天都要修改大量的配置文件，需要写个方法，减轻工作量。这个应当单独创建的项目
 */

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class StockMain extends AbstractMain {

    private static Logger logger = LogManager.getLogger(StockMain.class);

    public static void main(String[] args) {
        logger.info("StockMain程序开始运行......");

        /*************************************** 设置代理服务器的IP和port ***********************************************/
        setProxyProperties();

        /*************************************** 收集数据库统计信息  ***********************************************/
//		gatherDatabaseStatistics();

        /*********************************************** 恢复数据库表 **************************************************/
//		restoreTableIncremental();

        /*********************************** 从网络上获取数据，并插入到数据库中 ***************************************/
//		insertStockTransactionData_sohu();
//		writeStockIndexByDate_baoStock();
//		writeEtfTransactionDataByDate_sohu();
        // 方法insertStockTransactionData_sohu无法使用时，就是用下面这个方法
//        insertStockTransactionData_baoStock(); // 如果这个方法有问题了，就用pycharm打开程序执行，可以看到报错，通常可以手工删除无用的记录即可

        /*insertStockTransactionData_163();*/ // 接口用不了了
        /*insertStockTransactionData_126();*/ // 接口用不了了

        /********* 补充使用126接口没有的字段。使用126接口有些字段是空的，只能之后再使用163的接口将确实的字段补齐 ***********/
        /*supplementDataBy163Api();*/ // 接口用不了了

        /********* 从网络上获取数据，以json格式存储在ALL_RECORDS_IN_JSON.json文件中；然后再解析文件，并插入到数据库中 *************/
//		 readAndWriteAllRecordsByJson();
//		 insertAllRecordsFromJson();

        /********************************************* 更新每天的数据 ******************************************************/
        // 下面的方法全都重构好了
        writeMovingAverageByDate();
        writeChangeRangeExRightByDate();
        writeMACDByDate();
        writeKDByDate();
        writeHaByDate();
        writeBollByDate();
        writeBiasByDate();
        writeVarianceByDate();
        writeVolumeMaByDate();
        writeTurnoverMaByDate();
        // stock_transaction_data表的up_down字段已经在获取数据时就计算好了，所以现在这个方法用不着了。
        /*writeUpDownByDate();*/
        /*calVolatilityByDate();*/
        // 下面这两个个方法也不用了
        /*calUpDownPercentageByDate();*/
        /*writeStockIndexByDate();*/
        writeStockTransactionDataAllByDate();
        writeStockIndexMAByDate();
        writeStockIndexBiasByDate();
        writeStockIndexHeiKinAshiByDate();
        writeStockIndexMACDByDate();
        writeStockIndexKDByDate();
        writeEtfTransactionDataMAByDate();
        writeEtfTransactionDataBiasByDate();
        writeEtfTransactionDataHeiKinAshiByDate();
        writeEtfTransactionDataMACDByDate();
        writeEtfTransactionDataKDByDate();
        writeEtfTransactionDataCorrelationByDate();
        writeBoardIndexByDate();
        writeBoardIndexFiveAndTenDayRateByDate();
        writeBoardIndexUpDownPercentageByDate();
        writeBoardIndexUpDownRankByDate();
        writeModelMACDGoldCrossIncr();
        writeModelMACDDeadCrossIncr();
        writeModelClosePriceMA5GoldCrossIncr();
        writeModelClosePriceMA5DeadCrossIncr();
        writeModelHeiKinAshiUpDownIncr();
        writeModelHeiKinAshiDownUpIncr();
        writeModelKDGoldCrossIncr();
        writeModelKDDeadCrossIncr();
        writeModelBullShortLineUpIncr();
        writeModelVolumeTurnoverUpMa250Incr();
        writeModelEtfMACDGoldCrossByDate();
        writeModelEtfClosePriceMA5GoldCrossByDate();
        writeModelEtfHeiKinAshiDownUpByDate();
        writeModelEtfKDGoldCrossByDate();
        writeModelEtfBullShortLineUpIncr();
        writeModelStockAnalysisByDate();
        writeModelTopStockDetailByDate();
        writeModelTopStockDetailMAByDate();
        writeModelGoldCrossDeadCrossAnalysisByDate();
        // 下面这个方法也不用了
        /*writeModelTopStock();*/

        /******************************************** 根据每日数据更新图表 **************************************************/
        // 下面的方法全都重构好了
//		createMACDPicture();
//		createClosePriceGoldCrossMa5Picture();
//		createHeiKinAshiUpDownPicture();
//		createKDPicture();

//		createMovingAverageBullRankShortOrderPicture();
//		createMACDGoldCrossStockRatePicture();
//		createClosePicture(true);
//		createStockIndexHeiKinAshiPicture();
//		createDifferentiationPicture();
//		createStockLimitUpAndDownPicture();
//		createSpiderWebPlotPicture();
//		createBoardPicture();
//		createAllBoardIndexClosePicture();
//		createStockBigBoardUpDownPercentagePicture();
//		createAllBoardIndexFiveAndTenDayRatePicture();
//		createMACDGoldCrossScatterPlotPicture();
//		createMACDSuccessDidDeaScatterPlotPicture();
//		createMACDSuccessRatePicture();
//		createModelStockAnalysisStandardDeviationPictureAndAverageClosePrice();
//		createClosePriceMa5GoldCrossSuccessRatePicture();
//		createTopStockUpDownPicture();
//		createSubNewStockAvgClosePicture();

        /************************************************ 更新某一周的数据 ********************************************/
        // 下面的方法全都重构好了
//		writeStockWeekByDate();
//		writeStockWeekMAByDate();
//		writeStockWeekKDByDate();
//		writeStockWeekUpDownByDate();
//		writeStockWeekMACDByDate();
//		writeStockWeekChangeRangeExRightByDate();
//		writeStockWeekBollByDate();
//        writeStockWeekHeiKinAshiByDate();
//		writeStockIndexWeekByDate();
//		writeStockIndexWeekHeiKinAshiByDate();
//        writeModelStockWeekAnalysisByDate();

        /******************************************** 根据某一周数据更新图表 *********************************************/
        // 下面的方法全都重构好了
//		 createWeekKDPicture();
//		createWeekBollClosePricePicture();
//        createWeekClosePriceUpDnBollPercentagePicture();
//		createWeekMAAndClosePricePicture();
//        createWeekHeiKinAshiUpDownPicture();

//        createModelStockWeekAnalysisStandardDeviationPictureAndAverageClosePrice();
//		 createStockIndexWeekHeiKinAshiPicture();

        /************************************************ 更新某一月的数据 ********************************************/
//		writeStockMonthByDate();
//		writeStockMonthMAByDate();
//		writeStockMonthKDByDate();
//		writeStockMonthMACDByDate();
//		writeModelStockMonthAnalysisByDate();

        /********************************************** 更新所有的数据 **************************************************/
        // 下面的方法全都重构好了
//		 calculateMovingAverage();
//		 writeChangeRangeExRight();
//		 writeMACD();
//		 writeKD();
//		 writeHa();
//		writeBoll();
//		writeBias();
//		writeVariance();
//        writeVolumeMa();
//        writeTurnoverMa();
        // stock_transaction_data表的up_down字段已经在获取数据时就计算好了，所以现在这个方法用不着了。
        /*writeUpDown();*/
//		 calculateUpDownPercentage();
        /*calculateVolatility();*/
//		 writeStockWeek();
//		 writeStockWeekMA();
//		 writeStockWeekKD();
//		 writeStockWeekUpDown();
//		 writeStockWeekMACD();
//		 writeStockWeekChangeRangeExRight();
//		 writeStockWeekBoll();
//         writeStockWeekHeiKinAshi();
//		 writeStockMonth();
//		 writeStockMonthMA();
//		 writeStockMonthKD();
//		 writeStockMonthMACD();
        // 下面这一个方法已经无法使用了
        /*writeStockIndex();*/
//		 writeStockIndexMA();
//		 writeStockIndexBias();
//		 writeStockIndexHeiKinAshi();
//        writeStockIndexMACD();
//        writeStockIndexKD();
//        writeStockIndexWeek();
//        writeStockIndexWeekHeiKinAshi();
//        writeEtfTransactionDataMA();
//        writeEtfTransactionDataBias();
//        writeEtfTransactionDataHeiKinAshi();
//        writeEtfTransactionDataMACD();
//        writeEtfTransactionDataKD();
//		writeEtfTransactionDataCorrelation();
//        calculateBoardIndex();
//        calculateAllBoardIndexFiveAndTenDayRate();
//        calculateBoardIndexUpDownPercentage();
//        calculateBoardIndexUpDownRank();
//        writeModelWeekKDGoldCross();
//        writeModelMACDGoldCross();
//        writeModelMACDDeadCross();
//        writeModelClosePriceMA5GoldCross();
//        writeModelClosePriceMA5DeadCross();
//        writeModelHeiKinAshiUpDown();
//        writeModelHeiKinAshiDownUp();
//        writeModelKDGoldCross();
//        writeModelKDDeadCross();
//        writeModelBullShortLineUp();
//        writeModelVolumeTurnoverUpMa250();
//        writeModelStockIndexMACDGoldCross();
//        writeModelStockIndexClosePriceMA5GoldCross();
//        writeModelStockIndexHeiKinAshiDownUp();
//        writeModelStockIndexKDGoldCross();
//        writeModelEtfMACDGoldCross();
//        writeModelEtfClosePriceMA5GoldCross();
//        writeModelEtfHeiKinAshiDownUp();
//        writeModelEtfKDGoldCross();
//        writeModelEtfBullShortLineUp();
//        writeModelPercentageMaGoldCross();
//        writeModelAllGoldCross();
//        writeModelStockAnalysis();
//        writeModelGoldCrossDeadCrossAnalysis();
//        writeModelGoldCrossDeadCrossAnalysisPercentMA();
//        writeModelGoldCrossDeadCrossAnalysisSuccessRateMA();
//        writeModelWeekAllGoldCross();
//        writeModelWeekBollMacdGoldCross();
//        writeModelWeekBollClosePriceMa5GoldCross();
//        writeModelWeekBollHeiKinAshiUpDown();
//        writeModelWeekBollKDGoldCross();
//        writeModelWeekBollMacdDeadCross();
//        writeModelWeekBollClosePriceMa5DeadCross();
//        writeModelWeekBollHeiKinAshiDownUp();
//        writeModelWeekBollKDDeadCross();
//        writeModelStockWeekAnalysis();
//        writeModelStockMonthAnalysis();

        /***************************************** 打印文本报告 **********************************************************/
//		 printSubNewStock();
        printBandOperationStockAndSendMail();

        /************************************************** 生成报告 *****************************************************/
//		 generateReport();

        /************************************ 删除stock_transaction_data表中的记录 **********************************/
//		deleteStockTransactionDataByDate();

        /************************************************** 实验 *****************************************************/
//		test();

        /************************************************* 备份项目代码 ***********************************************/
//		backupCode();

        /***************************************** 备份数据库/表（海量和增量） *****************************************/
//		backupDatabaseIncremental();
//		backupDatabaseMass();

        /***************************************** 使操作系统进入睡眠状态 *****************************************/
//        makeOperationSystemSleep();

        logger.info("StockMain程序执行完毕。");
    }
}
