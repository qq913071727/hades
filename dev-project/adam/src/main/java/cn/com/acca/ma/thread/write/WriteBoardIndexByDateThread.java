package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 根据日期，计算board_index表中某一日的记录
 */
public class WriteBoardIndexByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteBoardIndexByDateThread.class);

    private BoardIndexService boardIndexService;

    public WriteBoardIndexByDateThread() {
    }

    public WriteBoardIndexByDateThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，根据日期，计算board_index表中某一日的记录，"
            + "调用的方法为【writeBoardIndexByDate】");

        boardIndexService.writeBoardIndexByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
