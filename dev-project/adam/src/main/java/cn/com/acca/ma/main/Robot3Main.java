package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Robot3Main extends AbstractMain {

    private static Logger logger = LogManager.getLogger(Robot3Main.class);

    public static void main(String[] args) {
        logger.info("Robot3Main程序开始运行......");

        /******************************* 设置代理服务器的IP和port *******************************/
        setProxyProperties();

        /******************************* 收集数据库统计信息 *************************************/
//		gatherDatabaseStatistics();

        /************* 执行某段时间内的股票买卖交易（周线级别布林带突破上轨或跌破下轨） ************/
        doBuyAndSellByBeginDateAndEndDate_weekBoll();

        /******************************* 将测试数据导入到历史数据表中 *****************************/
//        importIntoHistoryDataTableFromRobot3();
    }
}
