package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建上证股票，深证股票，中小板股票和创业板股票日线级别平均涨跌幅度图
 */
public class CreateStockBigBoardUpDownPercentagePictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateStockBigBoardUpDownPercentagePictureThread.class);

    private StockTransactionDataService stockTransactionDataService;

    public CreateStockBigBoardUpDownPercentagePictureThread() {
    }

    public CreateStockBigBoardUpDownPercentagePictureThread(StockTransactionDataService stockTransactionDataService) {
        this.stockTransactionDataService = stockTransactionDataService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始生成上证股票，深证股票，中小板股票和创业板股票日线级别平均涨跌幅度图，"
            + "调用的方法为【createStockBigBoardUpDownPercentagePicture】");

        stockTransactionDataService.createStockBigBoardUpDownPercentagePicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
