package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexKDGoldCross;

import java.util.List;

public interface ModelEtfKDGoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用KD金叉模型算法，向表MDL_ETF_KD_GOLD_CROSS插入数据
     * @param validateMA120NotDecreasing
     * @param validateMA250NotDecreasing
     * @param defaultBeginDate
     * @param endDate
     * @param type
     */
    void writeModelEtfKDGoldCross(Integer validateMA120NotDecreasing, Integer validateMA250NotDecreasing, String defaultBeginDate,
                                  String endDate, Integer type);

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_kd_gold_cross中返回数据，并按照sell_date升序排列
     *
     * @param type
     * @param indexCode
     * @return
     */
//    List<ModelStockIndexKDGoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
