package cn.com.acca.ma.dao;

import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;

import java.util.List;

public interface ModelWeekBollHeiKinAshiUpDownDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_BOLL_H_K_A_UP_DOWN中插入数据
     */
    void writeModelWeekBollHeiKinAshiUpDown();

    /**
     * 查询hei_kin_ashi上涨趋势交易收益率和最高价突破布林带下轨的百分比
     * @param beginDate
     * @param endDate
     */
    List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekHeiKinAshiUpDownProfitOrLossAndBollDn(String beginDate, String endDate);
}
