package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 期货交易记录（具体合约）
 */
@Data
@Entity
@Table(name="C_F_DATE_CONTRACT_DATA")
public class CommodityFutureDateContractData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private Integer id;

    /**
     * 代码
     */
    @Column(name="CODE")
    private String code;

    /**
     * 日期
     */
    @Column(name="TRANSACTION_DATE")
    private Date transactionDate;

    /**
     * 名称
     */
    @Column(name="NAME")
    private String name;

    /**
     * 开盘价
     */
    @Column(name="OPEN_PRICE")
    private BigDecimal openPrice;

    /**
     * 收盘价
     */
    @Column(name="CLOSE_PRICE")
    private BigDecimal closePrice;

    /**
     * 最高价
     */
    @Column(name="HIGHEST_PRICE")
    private BigDecimal highestPrice;

    /**
     * 最低价
     */
    @Column(name="LOWEST_PRICE")
    private BigDecimal lowestPrice;

    /**
     * 前收盘
     */
    @Column(name="LAST_CLOSE_PRICE")
    private BigDecimal lastClosePrice;

    /**
     * 成交量
     */
    @Column(name="VOLUME")
    private BigDecimal volume;

    /**
     * 成交额
     */
    @Column(name="TURNOVER")
    private BigDecimal turnover;

    /**
     * 持仓量
     */
    @Column(name="OPEN_INTEREST")
    private BigDecimal openInterest;

    /**
     * 内盘
     */
    @Column(name="BUYING")
    private BigDecimal buying;

    /**
     * 外盘
     */
    @Column(name="SELLING")
    private BigDecimal selling;

    /**
     * 涨跌额
     */
    @Column(name="RISING_AND_FALLING_AMOUNT")
    private BigDecimal risingAndFallingAmount;

    /**
     * 涨跌幅
     */
    @Column(name="PRICE_CHANGE")
    private BigDecimal priceChange;

    /**
     * 5日均线
     */
    @Column(name="MA5")
    private BigDecimal ma5;

    /**
     * 10日均线
     */
    @Column(name="MA10")
    private BigDecimal ma10;

    /**
     * 20日均线
     */
    @Column(name="MA20")
    private BigDecimal ma20;

    /**
     * 60日均线
     */
    @Column(name="MA60")
    private BigDecimal ma60;

    /**
     * 120日均线
     */
    @Column(name="MA120")
    private BigDecimal ma120;

    /**
     * 250日均线
     */
    @Column(name="MA250")
    private BigDecimal ma250;

    /**
     * 计算MACD时的指标
     */
    @Column(name="EMA12")
    private BigDecimal ema12;

    /**
     * 计算MACD时的指标
     */
    @Column(name="EMA26")
    private BigDecimal ema26;

    /**
     * 计算MACD时的指标
     */
    @Column(name="DIF")
    private BigDecimal dif;

    /**
     * 计算MACD时的指标
     */
    @Column(name="DEA")
    private BigDecimal dea;

    /**
     * KD指标中的RSV
     */
    @Column(name="RSV")
    private BigDecimal rsv;

    /**
     * KD指标中的K
     */
    @Column(name="K")
    private BigDecimal k;

    /**
     * KD指标中的D
     */
    @Column(name="D")
    private BigDecimal d;

    /**
     * HeiKinAshi开盘价
     */
    @Column(name="HA_OPEN_PRICE")
    private BigDecimal haOpenPrice;

    /**
     * HeiKinAshi收盘价
     */
    @Column(name="HA_CLOSE_PRICE")
    private BigDecimal haClosePrice;

    /**
     * HeiKinAshi最高价
     */
    @Column(name="HA_HIGHEST_PRICE")
    private BigDecimal haHighestPrice;

    /**
     * HeiKinAshi最低价
     */
    @Column(name="HA_LOWEST_PRICE")
    private BigDecimal haLowestPrice;

    /**
     * 布林带上轨
     */
    @Column(name="UP")
    private BigDecimal up;

    /**
     * 布林带中轨
     */
    @Column(name="MB")
    private BigDecimal mb;

    /**
     * 布林带下轨
     */
    @Column(name="DN_")
    private BigDecimal dn;

    /**
     * 5日乖离率
     */
    @Column(name="BIAS5")
    private BigDecimal bias5;

    /**
     * 10日乖离率
     */
    @Column(name="BIAS10")
    private BigDecimal bias10;

    /**
     * 20日乖离率
     */
    @Column(name="BIAS20")
    private BigDecimal bias20;

    /**
     * 60日乖离率
     */
    @Column(name="BIAS60")
    private BigDecimal bias60;

    /**
     * 120日乖离率
     */
    @Column(name="BIAS120")
    private BigDecimal bias120;

    /**
     * 250日乖离率
     */
    @Column(name="BIAS250")
    private BigDecimal bias250;

    /**
     * 5日方差
     */
    @Column(name="VARIANCE5")
    private BigDecimal variance5;

    /**
     * 10日方差
     */
    @Column(name="VARIANCE10")
    private BigDecimal variance10;

    /**
     * 20日方差
     */
    @Column(name="VARIANCE20")
    private BigDecimal variance20;

    /**
     * 60日方差
     */
    @Column(name="VARIANCE60")
    private BigDecimal variance60;

    /**
     * 120日方差
     */
    @Column(name="VARIANCE120")
    private BigDecimal variance120;

    /**
     * 250日方差
     */
    @Column(name="VARIANCE250")
    private BigDecimal variance250;

    /**
     * 统一相对价格指数
     */
    @Column(name="UNITE_RELATIVE_PRICE_INDEX")
    private BigDecimal uniteRelativePriceIndex;

    /**
     * 创建时间
     */
    @Column(name="CREATE_TIME")
    private Date createTime;
}
