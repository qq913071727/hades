package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.hibernate.impl.GetSubNewStockAverageCloseWorkImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import cn.com.acca.ma.dao.ModelSubNewStockDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;

public class ModelSubNewStockDaoImpl extends BaseDaoImpl<Object> implements
		ModelSubNewStockDao {

    /**
     * 计算次新股收盘价的平均值
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
	@SuppressWarnings("rawtypes")
	public List<HashMap> getSubNewStockAverageClose(boolean multithreading, final String beginDate, final String endDate) {
		logger.info("get sub new stock average close begin");
		
		final List<HashMap> list = new ArrayList<HashMap>();

		Session newSession;
		if (multithreading){
		    newSession = HibernateUtil.newSession();
		    newSession.beginTransaction();
            newSession.doWork(new GetSubNewStockAverageCloseWorkImpl(beginDate, endDate, list));
            newSession.getTransaction().commit();
			HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            session.doWork(new GetSubNewStockAverageCloseWorkImpl(beginDate, endDate, list));
            session.getTransaction().commit();
            session.close();
        }

		logger.info("get sub new stock average close finish");
		return list;
	}

//	class GetSubNewStockAverageCloseWorkImpl implements Work{
//
//	    private String beginDate;
//	    private String endDate;
//        private List<HashMap> list = new ArrayList<HashMap>();
//
//        public GetSubNewStockAverageCloseWorkImpl() {
//        }
//
//        public GetSubNewStockAverageCloseWorkImpl(String beginDate, String endDate, List<HashMap> list) {
//            this.beginDate = beginDate;
//            this.endDate = endDate;
//            this.list = list;
//        }
//
//        @Override
//        public void execute(Connection connection) throws SQLException {
//            DatabaseMetaData databaseMetaData=connection.getMetaData();
//            Connection metaDataConnection=null;
//            if(null!=databaseMetaData){
//                metaDataConnection=databaseMetaData.getConnection();
//            }
//            OracleCallableStatement ocs=(OracleCallableStatement) metaDataConnection.prepareCall("{call PKG_MODEL_RECORD.FIND_SUB_NEW_STOCK_AVG_CLOSE(?,?,?)}");
//            ocs.setString(1, beginDate);
//            ocs.setString(2, endDate);
//            ocs.registerOutParameter(3, OracleTypes.ARRAY,"T_SUBNEWSTOCKAVGCLOSE_ARRAY");
//            ocs.execute();
//            ARRAY array=ocs.getARRAY(3);
//            Datum[] datas=(Datum[]) array.getOracleArray();
//            if(null!=datas){
//                for (int i = 0; i < datas.length; i++){
//                    if(datas[i]!=null&&((STRUCT) datas[i])!=null){
//
//                        Datum[] boardResultAttributes = ((STRUCT) datas[i]).getOracleAttributes();
//                        HashMap hashMap=new HashMap();
//                        hashMap.put("subNewStockDate", boardResultAttributes[0].dateValue());
//                        hashMap.put("subNewStockAvgCLose", boardResultAttributes[1].bigDecimalValue());
//                        list.add(hashMap);
//                    }else{
//                        logger.info("datas["+i+"] is null.");
//                    }
//                }
//            }
//        }
//    }

	/*********************************************************************************************************************
	 *
	 * 												   	创建报告
	 *
	 *********************************************************************************************************************/
	/**
	 * 将视图V_SUB_NEW_STOCK中的数据打印出来
	 */
	public void printSubNewStock(){
		logger.info("print sub new stock data begin");

		session= HibernateUtil.currentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.PRINT_V_SUB_NEW_STOCK()}");
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();

		logger.info("print sub new stock data finish");
	}
}
