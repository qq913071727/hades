package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelAllGoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelAllGoldCross;

import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

public class ModelAllGoldCrossDaoImpl extends BaseDaoImpl<ModelAllGoldCross> implements
	ModelAllGoldCrossDao {

	public ModelAllGoldCrossDaoImpl() {
		super();
	}

	/**
	 * 海量地向表MDL_ALL_GOLD_CROSS中插入数据
	 * @param type
	 */
	@Override
	public void writeModelAllGoldCross(String type) {
		logger.info("开始海量地向表MDL_ALL_GOLD_CROSS中插入数据，参数type为【" + type + "】");

		session= HibernateUtil.currentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_ALL_GOLD_CROSS(?)}");
		query.setParameter(0, type);
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();

		logger.info("海量地向表MDL_ALL_GOLD_CROSS中插入数据结束");
	}

	/**
	 * 查找所有ModelAllGoldCross类型对象
	 */
	@Override
	public List<ModelAllGoldCross> findAll() {
		logger.info("开始查找所有ModelAllGoldCross类型对象");

		session= HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createSQLQuery("select * from mdl_all_gold_cross t");
		List<ModelAllGoldCross> modelAllGoldCrossList = query.list();
		session.getTransaction().commit();
		session.close();

		logger.info("查找所有ModelAllGoldCross类型对象完成");

		return modelAllGoldCrossList;
	}

	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_ALL_GOLD_CROSS中获取SELL_DATE字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@Override
	public List<Date> getDateByCondition(String beginDate, String endDate) {
		logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
			+ "从表MDL_ALL_GOLD_CROSS中获取SELL_DATE字段");

		StringBuffer hql = new StringBuffer("select distinct t.sellDate from ModelAllGoldCross t " +
			"where t.sellDate between to_date(?, 'yyyy-mm-dd') and to_date(?, 'yyyy-mm-dd')");
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql.toString());
		query.setParameter(0, beginDate);
		query.setParameter(1, endDate);
		List<Date> list = query.list();
		session.getTransaction().commit();
		session.close();

		return list;
	}

	/**
	 * 从表MDL_ALL_GOLD_CROSS中获取sell_date日的所有ModelAllGoldCross对象
	 * @param date
	 * @return
	 */
	@Override
	public List<ModelAllGoldCross> getModelAllGoldCrossByDate(String date) {
		logger.info("从表MDL_ALL_GOLD_CROSS中获取日期【" + date + "】的所有ModelAllGoldCross对象");

		StringBuffer hql = new StringBuffer("select t from ModelAllGoldCross t " +
			"where t.sellDate=to_date(?,'yyyy-mm-dd') order by t.sellDate asc");
		session = HibernateUtil.currentSession();
		session.beginTransaction();
		Query query = session.createQuery(hql.toString());
		query.setParameter(0, date);
		List<ModelAllGoldCross> list=query.list();
		session.getTransaction().commit();
		session.close();

		return list;
	}

	/**
	 * 向表MDL_ALL_GOLD_CROSS中插入ModelAllGoldCross对象
	 * @param modelAllGoldCross
	 */
	public void save(ModelAllGoldCross modelAllGoldCross){
		logger.info("向表MDL_ALL_GOLD_CROSS中插入ModelAllGoldCross对象");

		session = HibernateUtil.currentSession();
		session.beginTransaction();
		session.persist(modelAllGoldCross);
		session.getTransaction().commit();
		session.close();
	}

}
