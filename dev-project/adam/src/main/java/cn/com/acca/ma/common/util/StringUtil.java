package cn.com.acca.ma.common.util;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

import java.io.UnsupportedEncodingException;

public class StringUtil {
	
	/**
	 * 按照格式（format），转换String字符串（表示日期）
	 * @param dateString String类型，表示日期的字符串
	 * @param format 格式
	 * @return 如果dateString为20150117，format为-，则返回值为2015-01-17
	 */
	public static String convertDateStringByFormat(String dateString,String format){
		return dateString.substring(0,4)+format+dateString.substring(4,6)+format+dateString.substring(6,8);
	}

	/**
	 * 删除字符串中的括号
	 * @param str
	 * @return
	 */
	public static String removeBracket(String str){
		return str.replace("[", "").replace("]", "").trim();
	}

	public static String zhs16gbkToUtf8(byte[] bytes){
		CharsetDetector detector = new CharsetDetector();
		detector.setText(bytes);
		CharsetMatch match = detector.detect();

		if (match.getName().equals("UTF-8") || match.getName().equals("GBK")) {
			try {
				return new String(bytes, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		return null;
	}
}
