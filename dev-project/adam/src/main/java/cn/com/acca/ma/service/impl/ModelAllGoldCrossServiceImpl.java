package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.constant.MovingAverageGoldCrossType;
import cn.com.acca.ma.model.ModelAllGoldCross;
import cn.com.acca.ma.service.ModelAllGoldCrossService;
import java.util.List;

public class ModelAllGoldCrossServiceImpl extends BaseServiceImpl<ModelAllGoldCrossServiceImpl, ModelAllGoldCross> implements
    ModelAllGoldCrossService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_ALL_GOLD_CROSS中插入数据
     */
    @Override
    public void writeModelAllGoldCross() {
        for (int i = 0; i< MovingAverageGoldCrossType.MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.size(); i++){
            modelAllGoldCrossDao.writeModelAllGoldCross(MovingAverageGoldCrossType.MOVING_AVERAGE_GOLD_CROSS_TYPE_LIST.get(i));
        }
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
