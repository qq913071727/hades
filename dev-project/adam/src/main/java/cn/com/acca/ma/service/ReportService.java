package cn.com.acca.ma.service;

public interface ReportService extends BaseService {
	
	/**
	 * 将报告对象保存到数据库
	 */
	void storeReport();
	/**
	 * 生成报告
	 */
	void createReport();
	
	/**
	 * 设置字体
	 */
	void setFont();
	
	/**
	 * 设置标题
	 */
	void writeTitle();
	
	/**
	 * 写日期
	 */
	void writeDate();
	
	/**
	 * 写入股票样本总数
	 */
	void writeStockSampleSumByDate();
	
	/**
	 * 写入股票上涨的家数
	 */
	void writeStockUpNumberByDate();
	
	/**
	 * 写入股票持平的家数
	 */
	void writeStockMiddleNumberByDate();
	
	/**
	 * 写入股票下跌的家数
	 */
	void writeStockDownNumberByDate();
	
	/**
	 * 在报告上绘制28分化图
	 */
	void print28DifferentiationPicture();
	
	/**
	 * 写入多头排列的股票的数量，股票名称/股票代码
	 */
	void writeBullRank();
	
	/**
	 * 写入空头排列的股票的数量，股票名称/股票代码
	 */
	void writeShortOrder();
	
	/**
	 * 在报告上绘制多空排列图（最近10年和最近半年）
	 */
	void printBullRankShortOrderPicture();
	
	/**
	 * 在报告上绘制收盘价图STOCK_CLOSE（最近10年和最近半年）
	 */
	void printClosePicture();
	
	/**
	 * 在报告上绘制日线级别MACD图
	 */
	void printDayMacdPicture();
	
	/**
	 * 在报告上绘制日线级别的蜘蛛雷达图
	 */
	void printSpiderWebPicture();
	
	/**
	 * 在报告上绘制周线级别KD指标图
	 */
	void printWeekendKDPicture();
	
	/**
	 * 在报告上绘制日线级别各个板块的K线图
	 */
	void printBoardIndexPicture();
	
	/**
	 * 在报告上绘制从开始日期至结束日期的板块收盘价图。分别绘制最近10年的图和半年的图
	 */
	void printAllBoardIndexClosePicture();
	
	/**
	 * 在报告上绘制从开始日期至结束日期的板块5日和10日涨跌图。分别绘制最近10年的图和半年的图
	 */
	void printAllBoardIndexFiveAndTenDayRatePicture();
	
	/**
	 * 在报告上绘制从开始日期至结束日期的股票涨停跌停数量图（折线图）
	 */
	void printStockLimitUpAndDownPicture();
	
	/**
	 * 写入日线级别发生反转的股票
	 */
	void writeReverseStock();
	
	/**
	 * 写入日线级别MACD金叉的股票
	 */
	void writeMACDUpBelowZero();
	
	/**
	 * 写入周线级别MACD底背离的股票的信息
	 */
	void writeWeekendMacdEndDeviate();
	
	/**
	 * 写入周线级别KD指标金叉的股票的信息
	 */
	void writeStockWeekendKDUp();
	
	/**
	 * 写入日线级别异常股票的信息
	 */
	void writeAbnormalStock();
	
	/**
	 * 写入日线级别MACD最低的股票的信息
	 */
	void writeLowestMacdStock();
	
    /**
     * 写入某一周最低KD的stockNumber只股票
     */
	void writeLowestKdStockWeekend();
	
	/**
	 * 写入日线级别股票收盘价到达250日均线支撑的股票
	 */
	void writeStockWith250Support();
	
	/**
	 * 写入日线级别股票收盘价到达120日均线支撑的股票
	 */
	void writeStockWith120Support();
	
	/**
	 * 写入日线级别某一段时间内上涨股票的前number名
	 */
	public void writeTopStock();
	
	/**
	 * 写入日线级别某一段时间内上涨股票的前number名
	 */
	public void writeLastStock();
}
