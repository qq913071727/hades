package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MDL_MACD_GOLD_CROSS")
public class ModelMACDGoldCross implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "MODEL_ID")
	private BigDecimal modelId;

	@Column(name = "BUY_DATE")
	private Date buyDate;

	@Column(name = "SELL_DATE")
	private Date sellDate;

	@Column(name = "STOCK_CODE")
	private String stockCode;

	@Column(name = "BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name = "SELL_PRICE")
	private BigDecimal sellPrice;

	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

	@Column(name = "BUY_DIF")
	private BigDecimal buyDif;

	@Column(name = "BUY_DEA")
	private BigDecimal buyDea;

	@Column(name = "SELL_DIF")
	private BigDecimal sellDif;

	@Column(name = "SELL_DEA")
	private BigDecimal sellDea;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getModelId() {
		return modelId;
	}

	public void setModelId(BigDecimal modelId) {
		this.modelId = modelId;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public BigDecimal getProfitLoss() {
		return profitLoss;
	}

	public void setProfitLoss(BigDecimal profitLoss) {
		this.profitLoss = profitLoss;
	}

	public BigDecimal getAccumulativeProfitLoss() {
		return accumulativeProfitLoss;
	}

	public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
		this.accumulativeProfitLoss = accumulativeProfitLoss;
	}

	public BigDecimal getBuyDif() {
		return buyDif;
	}

	public void setBuyDif(BigDecimal buyDif) {
		this.buyDif = buyDif;
	}

	public BigDecimal getBuyDea() {
		return buyDea;
	}

	public void setBuyDea(BigDecimal buyDea) {
		this.buyDea = buyDea;
	}

	public BigDecimal getSellDif() {
		return sellDif;
	}

	public void setSellDif(BigDecimal sellDif) {
		this.sellDif = sellDif;
	}

	public BigDecimal getSellDea() {
		return sellDea;
	}

	public void setSellDea(BigDecimal sellDea) {
		this.sellDea = sellDea;
	}
}
