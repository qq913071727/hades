package cn.com.acca.ma.service;

import cn.com.acca.ma.model.EtfTransactionData;
import cn.com.acca.ma.model.StockTransactionData;

import java.math.BigDecimal;
import java.util.List;

public interface EtfTransactionDataService extends BaseService {

    /********************************************** 从网络上获取数据，并插入到数据库中 **********************************************/
    /**
     * 调用sohu的接口，向etf_transaction_data表中插入数据
     */
    void insertEtfTransactionData_sohu();

    /**
     * 从网络中获取ETF(sohu)
     * @param url
     * @param codeNum
     * @param lastClosePrice
     * @return
     */
    List<EtfTransactionData> readEtfTransactionDataList_sohu(String url, String codeNum, BigDecimal lastClosePrice);

    /**
     * 将从网络中读取的数据写入到表ETF_TRANSACTION_DATA中。
     * @param etfTransactionDataList
     */
    void writeEtfTransactionDataList(List<EtfTransactionData> etfTransactionDataList);

    /******************************************** 更新每天的数据 ********************************************************/
    /**
     * 根据日期date，计算某一日所有ETF的移动平均线数据
     */
    void writeEtfTransactionDataMAByDate();

    /**
     * 根据日期date，计算某一日所有ETF的Hei Kin Ashi数据
     */
    void writeEtfTransactionDataHeiKinAshiByDate();

    /**
     * 根据日期date，计算所有ETF某一日的乖离率
     */
    void writeEtfTransactionDataBiasByDate();

    /**
     * 更新表ETF_TRANSACTION_DATA中某一日的EMA15，EMA26，DIF和DEA字段
     */
    void writeEtfTransactionDataMACDByDate();

    /**
     * 更新表ETF_TRANSACTION_DATA中某一日的RSV，K和D字段
     */
    void writeEtfTransactionDataKDByDate();

    /**
     * 更新表ETF_TRANSACTION_DATA中某一日的correlation***_with_avg_c_p字段
     */
    void writeEtfTransactionDataCorrelationByDate();

    /******************************************** 更新全部的数据 ********************************************************/
    /**
     * 更新last_close_price字段
     */
    void writeLastClosePrice();

    /**
     * 计算5、10、20、60、120、250日均线
     */
    void writeEtfTransactionDataMA();

    /**
     * 计算所有ETF的乖离率
     */
    void writeEtfTransactionDataBias();

    /**
     * 计算Hei Kin Ashi相关的字段
     */
    void writeEtfTransactionDataHeiKinAshi();

    /**
     * 计算所有ETF的MACD
     */
    void writeEtfTransactionDataMACD();

    /**
     * 计算所有ETF的KD
     */
    void writeEtfTransactionDataKD();

    /**
     * 计算所有ETF的correlation5_with***字段
     */
    void writeEtfTransactionDataCorrelation();
}
