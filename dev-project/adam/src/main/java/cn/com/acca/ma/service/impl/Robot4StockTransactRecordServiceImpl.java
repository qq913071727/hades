package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.constant.FilterMethod;
import cn.com.acca.ma.constant.RobotPrintBuySellSuggestionFormat;
import cn.com.acca.ma.constant.WeekGoldCrossDeadCrossMethod;
import cn.com.acca.ma.model.Robot4StockTransactRecord;
import cn.com.acca.ma.pojo.StockWeekAverageHeiKinAshiClosePrice;
import cn.com.acca.ma.pojo.StockWeekAverageKD;
import cn.com.acca.ma.pojo.StockWeekAverageClosePriceAndMa5;
import cn.com.acca.ma.pojo.StockWeekDatePojo;
import cn.com.acca.ma.service.Robot4StockTransactRecordService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class Robot4StockTransactRecordServiceImpl extends BaseServiceImpl<Robot4StockTransactRecordServiceImpl, Robot4StockTransactRecord> implements
        Robot4StockTransactRecordService {

    /**
     * 执行某段时间内的股票买卖交易（周线级金叉死叉结合日线级别买卖信号）
     */
    @Override
    public void doBuyAndSellByBeginDateAndEndDate_week() {
        logger.info("执行某段时间内的股票买卖交易（周线级金叉死叉结合日线级别买卖信号）");

        String weekKdBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.beginDate");
        String weekKdEndDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.endDate");
        Integer goldCrossDeadCrossMethod = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.goldCross.deadCross.method"));
        Integer mandatoryStopLoss = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.mandatoryStopLoss"));
        Double mandatoryStopLossRate = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.mandatoryStopLossRate"));
        Integer mandatoryStopProfit = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.mandatoryStopProfit"));
        Double mandatoryStopProfitRate = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.mandatoryStopProfitRate"));
        Double top = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.top"));
        Double bottom = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.bottom"));
        Double closePriceStart = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.closePrice.start"));
        Double closePriceEnd = Double.parseDouble(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.closePrice.end"));
        String xrBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.xr.begin.date");
        String currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.current.closePrice.compareTo.someTime.highest.lowest.price.begin.date");
        Integer currentClosePriceCompareToSomeTimeHighestPrice = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.current.closePrice.compareTo.someTime.highest.price"));
        Integer currentClosePriceCompareToSomeTimeLowestPrice = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.current.closePrice.compareTo.someTime.lowest.price"));
        String maNotDecreasingLevel = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.ma.not.decreasing.level");
        String maNotDecreasingDateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.ma.not.decreasing.dateNumber");
        String maNotIncreasingLevel = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.ma.not.increasing.level");
        String maNotIncreasingDateNumber = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.ma.not.increasing.dateNumber");
        Integer backwardMonth = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.success.rate.backward.month"));
        Integer averageDateNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.success.rate.average.date.number"));
        Integer shippingSpaceControl = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.shippingSpaceControl"));
        Integer holdStockNumber = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.holdStockNumber"));
        Integer printFormat = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.buy.sell.week.kd.print.format"));

        // 获取日期列表
        List<String> dateList = stockTransactionDataDao.findDistinctDateBetweenBeginDateAndEndDate(false, weekKdBeginDate, weekKdEndDate);
        // 记录交易天数
        int transactionDateNumber = 0;
        if (null != dateList && dateList.size() > 0) {
            for (int i = 0; i < dateList.size(); i++) {
                String currentDate = dateList.get(i);

                /*************************************** 卖股票/买股票 ***********************************************/
                // 更新robot4_account表
                robot4AccountDao.updateRobotAccountBeforeSellOrBuy(currentDate);

                // 卖股票
                robot4StockTransactRecordDao.sellOrBuy(currentDate, mandatoryStopLoss, mandatoryStopLossRate, mandatoryStopProfit, mandatoryStopProfitRate);

                // 更新robot4_account表
                robot4AccountDao.updateRobotAccountAfterSellOrBuy(currentDate);

                // 如果所有账号都已经满仓，则跳过这个交易日
                if (isFullStockInRobot4Account(holdStockNumber)) {
                    logger.info("所有账号都是满仓，因此日期【" + currentDate + "】不做任何交易，直接跳过");

                    // 更新robot4_account表的total_assets字段
                    robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                    continue;
                }

                /********************************** 确定是做多，还是做空。判断使用哪种算法 ***********************************/
                Boolean goldCross = false;
                Boolean deadCross = false;

                // 如果当前日期是周末，则买股票，否则跳过这一天
                List<StockWeekDatePojo> stockWeekDatePojoList = stockIndexWeekDao.findBeginDateAndEndDateGroupByBeginDateAndEndDateOrderByBeginDate(weekKdBeginDate, weekKdEndDate);
                boolean isWeekend = false;
                if (null != stockWeekDatePojoList && stockWeekDatePojoList.size() > 0) {
                    for (StockWeekDatePojo stockWeekDatePojo : stockWeekDatePojoList) {
                        Date endDate = stockWeekDatePojo.getEndDate();
                        if (endDate.equals(DateUtil.stringToDate(currentDate))) {
                            isWeekend = true;
                            logger.info("日期【" + DateUtil.dateToString(endDate) + "】是本周最后一个交易日，准备买入/融券");
                            break;
                        }
                    }

                    if (isWeekend) {
                        /********************************** 判断使用哪个方法决定操作方向 ***********************************/
                        // 查找包括日期date在内的之前的两周记录的平均K和D，并按照begin_date升序排列
                        List<StockWeekAverageKD> theStockWeekAverageKDList = stockWeekDao.findThisTwoStockWeekAverageKDByDateOrderByBeginDateAsc(currentDate);
                        // 本周
                        StockWeekAverageKD thisWeekStockWeekAverageKD = theStockWeekAverageKDList.get(0);
                        // 上周
                        StockWeekAverageKD lastWeekStockWeekAverageKD = theStockWeekAverageKDList.get(1);
                        double averageKD = (thisWeekStockWeekAverageKD.getAverageK().doubleValue() + thisWeekStockWeekAverageKD.getAverageD().doubleValue()) / 2;
                        if (averageKD <= top && averageKD >= bottom) {
                            goldCrossDeadCrossMethod = WeekGoldCrossDeadCrossMethod.KD_GOLD_CROSS_DEAD_CROSS;
                            logger.info("averageKD是【" + averageKD + "】，使用KD金叉/死叉来判断操作方向");
                        } else {
                            goldCrossDeadCrossMethod = WeekGoldCrossDeadCrossMethod.HEI_KIN_ASHI_UP_DOWN;
                            logger.info("averageKD是【" + averageKD + "】，使用hei_kin_ashi上涨/下跌来判断操作方向");
                        }

                        /****************************************** 判断操作方向 *******************************************/
                        // close_price和ma5金叉死叉
                        List<StockWeekAverageClosePriceAndMa5> stockWeekAverageClosePriceAndMa5List;
                        StockWeekAverageClosePriceAndMa5 lastFirstStockWeekAverageClosePriceAndMa5;
                        StockWeekAverageClosePriceAndMa5 lastSecondStockWeekAverageClosePriceAndMa5;
                        if (goldCrossDeadCrossMethod.equals(WeekGoldCrossDeadCrossMethod.CLOSE_PRICE_MA5_GOLD_CROSS_DEAD_CROSS)) {
                            // 上周与上上周
//                          stockWeekAverageClosePriceAndMa5List = stockWeekDao.findLastTwoStockWeekAverageClosePriceAndMA5ByDateOrderByBeginDateAsc(currentDate);
                            // 本周与上周
                            stockWeekAverageClosePriceAndMa5List = stockWeekDao.findThisTwoStockWeekAverageClosePriceAndMA5ByDateOrderByBeginDateAsc(currentDate);

                            if (null != stockWeekAverageClosePriceAndMa5List && stockWeekAverageClosePriceAndMa5List.size() == 2) {
                                lastFirstStockWeekAverageClosePriceAndMa5 = stockWeekAverageClosePriceAndMa5List.get(0);
                                lastSecondStockWeekAverageClosePriceAndMa5 = stockWeekAverageClosePriceAndMa5List.get(1);

//                        // 如果上个星期是金叉
//                        if (lastFirstStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue() >= lastFirstStockWeekAverageClosePriceAndMa5.getAverageMA5().doubleValue()) {
//                            goldCross = true;
//                            logger.info("上个星期出现了金叉");
//                        }
//                        // 如果上个星期是死叉
//                        if (lastFirstStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue() <= lastFirstStockWeekAverageClosePriceAndMa5.getAverageMA5().doubleValue()) {
//                            deadCross = true;
//                            logger.info("上个星期出现了死叉");
//                        }

                                // 如果上个星期比上上个星期高
//                        if (lastSecondStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue() <= lastFirstStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue()) {
//                            goldCross = true;
//                            logger.info("上个星期比上上个星期高");
//                        }
                                // 如果上个星期比上上个星期低
//                        if (lastSecondStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue() >= lastFirstStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue()) {
//                            deadCross = true;
//                            logger.info("上个星期比上上个星期低");
//                        }

                                // 本周平均close_price比上周平均close_price上涨了
                                if (lastSecondStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue() <= lastFirstStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue()) {
                                    goldCross = true;
                                    logger.info("本周平均close_price比上周平均close_price上涨了");
                                }
                                // 本周平均close_price比上周平均close_price下跌了
                                if (lastSecondStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue() >= lastFirstStockWeekAverageClosePriceAndMa5.getAverageClosePrice().doubleValue()) {
                                    deadCross = true;
                                    logger.info("本周平均close_price比上周平均close_price下跌了");
                                }
                            } else {
                                logger.warn("没有前两个星期的记录或者不足两个星期，从下一个日期开始处理");

                                // 更新robot4_account表的total_assets字段
                                robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                                continue;
                            }
                        }

                        // hei_kin_ashi上涨下跌
                        List<StockWeekAverageHeiKinAshiClosePrice> stockWeekAverageHeiKinAshiClosePriceList;
                        StockWeekAverageHeiKinAshiClosePrice lastFirstStockWeekAverageHeiKinAshiClosePrice = null;
                        StockWeekAverageHeiKinAshiClosePrice lastSecondStockWeekAverageHeiKinAshiClosePrice = null;
                        if (goldCrossDeadCrossMethod.equals(WeekGoldCrossDeadCrossMethod.HEI_KIN_ASHI_UP_DOWN)) {
                            // 上周与上上周
//                          stockWeekAverageHeiKinAshiClosePriceList = stockWeekDao.findLastTwoStockWeekAverageHeiKinAshiClosePriceByDateOrderByBeginDateAsc(currentDate);
                            // 本周与上周
                            stockWeekAverageHeiKinAshiClosePriceList = stockWeekDao.findThisTwoStockWeekAverageHeiKinAshiClosePriceByDateOrderByBeginDateAsc(currentDate);

                            if (null != stockWeekAverageHeiKinAshiClosePriceList && stockWeekAverageHeiKinAshiClosePriceList.size() == 2) {
                                lastFirstStockWeekAverageHeiKinAshiClosePrice = stockWeekAverageHeiKinAshiClosePriceList.get(0);
                                lastSecondStockWeekAverageHeiKinAshiClosePrice = stockWeekAverageHeiKinAshiClosePriceList.get(1);

                                // 上一周hei_kin_ashi收盘价大于上上周hei_kin_ashi收盘价
//                                if (lastSecondStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue() <= lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue()) {
//                                    goldCross = true;
//                                    logger.info("上一周hei_kin_ashi收盘价大于上上周hei_kin_ashi收盘价");
//                                }
                                // 上一周hei_kin_ashi收盘价小于上上周hei_kin_ashi收盘价
//                                if (lastSecondStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue() >= lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue()) {
//                                    deadCross = true;
//                                    logger.info("上一周hei_kin_ashi收盘价小于上上周hei_kin_ashi收盘价");
//                                }

                                // 如果上个星期是阳线
//                        if (lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiOpenPrice().doubleValue() <= lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue()) {
//                            goldCross = true;
//                            logger.info("上个星期是阳线");
//                        }
                                // 如果上个星期是阴线
//                        if (lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiOpenPrice().doubleValue() >= lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue()) {
//                            deadCross = true;
//                            logger.info("上个星期是阴线");
//                        }

                                // 本周hei_kin_ashi是阳线
                                if (lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue() >= lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiOpenPrice().doubleValue()) {
                                    goldCross = true;
                                    logger.info("本周hei_kin_ashi是阳线");
                                }
                                // 本周hei_kin_ashi是阴线
                                if (lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiClosePrice().doubleValue() <= lastFirstStockWeekAverageHeiKinAshiClosePrice.getAverageHeiKinAshiOpenPrice().doubleValue()) {
                                    deadCross = true;
                                    logger.info("本周hei_kin_ashi是阴线");
                                }
                            } else {
                                logger.warn("没有前两个星期的记录或者不足两个星期，从下一个日期开始处理");

                                // 更新robot4_account表的total_assets字段
                                robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                                continue;
                            }
                        }

                        // kd金叉死叉
                        List<StockWeekAverageKD> stockWeekAverageKDList;
                        StockWeekAverageKD lastFirstStockWeekAverageKD = null;
                        StockWeekAverageKD lastSecondStockWeekAverageKD = null;
                        if (goldCrossDeadCrossMethod.equals(WeekGoldCrossDeadCrossMethod.KD_GOLD_CROSS_DEAD_CROSS)) {
                            // 上周与上上周
//                          stockWeekAverageKDList = stockWeekDao.findLastTwoStockWeekAverageKDByDateOrderByBeginDateAsc(currentDate);
                            // 本周与上周
                            stockWeekAverageKDList = stockWeekDao.findThisTwoStockWeekAverageKDByDateOrderByBeginDateAsc(currentDate);

                            if (null != stockWeekAverageKDList && stockWeekAverageKDList.size() == 2) {
                                lastFirstStockWeekAverageKD = stockWeekAverageKDList.get(0);
                                lastSecondStockWeekAverageKD = stockWeekAverageKDList.get(1);

//                        // 如果上个星期是金叉
//                        if (lastFirstStockWeekAverageKD.getAverageK().doubleValue() >= lastFirstStockWeekAverageKD.getAverageD().doubleValue()) {
//                            goldCross = true;
//                            logger.info("上个星期出现了金叉");
//                        }
//                        // 如果上个星期是死叉
//                        if (lastFirstStockWeekAverageKD.getAverageK().doubleValue() <= lastFirstStockWeekAverageKD.getAverageD().doubleValue()) {
//                            deadCross = true;
//                            logger.info("上个星期出现了死叉");
//                        }

                                // 如果上个星期比上上个星期高
//                        if (lastSecondStockWeekAverageKD.getAverageK().doubleValue() <= lastFirstStockWeekAverageKD.getAverageK().doubleValue()) {
//                            goldCross = true;
//                            logger.info("上个星期出现了金叉");
//                        }
                                // 如果上个星期比上上个星期低
//                        if (lastSecondStockWeekAverageKD.getAverageK().doubleValue() >= lastFirstStockWeekAverageKD.getAverageK().doubleValue()) {
//                            deadCross = true;
//                            logger.info("上个星期出现了死叉");
//                        }

                                // 如果这个星期比上个星期高
                                if (lastSecondStockWeekAverageKD.getAverageK().doubleValue() <= lastFirstStockWeekAverageKD.getAverageK().doubleValue()) {
                                    goldCross = true;
                                    logger.info("这个星期比上个星期高");
                                }
                                // 如果这个星期比上个星期低
                                if (lastSecondStockWeekAverageKD.getAverageK().doubleValue() >= lastFirstStockWeekAverageKD.getAverageK().doubleValue()) {
                                    deadCross = true;
                                    logger.info("这个星期比上个星期低");
                                }
                            } else {
                                logger.warn("没有前两个星期的记录或者不足两个星期，从下一个日期开始处理");

                                // 更新robot4_account表的total_assets字段
                                robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                                continue;
                            }
                        }

                        /*************************************** 按照条件，过滤股票 ***********************************/
                        // 清空robot4_stock_filter表
                        robot4StockFilterDao.truncateTableRobotStockFilter();

                        // close_price金叉死叉ma5
                        if (goldCrossDeadCrossMethod.equals(WeekGoldCrossDeadCrossMethod.CLOSE_PRICE_MA5_GOLD_CROSS_DEAD_CROSS)) {
                            if (goldCross) {
                                logger.info("满足交易条件：close_price金叉ma5出现");
                                logger.info("操作方向：做多。交易天数为：【" + ++transactionDateNumber + "】");

                                // 向表robot4_stock_filter中插入可以做多的股票的代码
                                robot4StockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(currentDate);

                                // 保留所有周线级别收盘价小于布林带中轨的股票
//                                      robot4StockFilterDao.filterWeekClosePriceDownBollMB(currentDate);

                                // 删除周线级别，上一周close_price死叉ma5的股票
//                                      robot4StockFilterDao.deleteLastWeekClosePriceDeadCrossMa5(currentDate);
                                // 删除本周周线级别close_price死叉ma5
//                                      robot4StockFilterDao.filterByStockWeekClosePriceMa5GoldCross(currentDate);
                                // 删除本周close_price比上一周close_price小的股票
                                robot4StockFilterDao.deleteThisWeekClosePriceLessThanLastWeekClosePrice(currentDate);
                                // 删除周线级别，KD死叉的股票
//                                      robot4StockFilterDao.filterByStockWeekKDGoldCross(currentDate);
                                // 删除周线级别，上一周KD死叉的股票
//                                      robot4StockFilterDao.deleteLastWeekKDDeadCross(currentDate);
                                // 删除上周K比上上周K小的股票
//                                      robot4StockFilterDao.deleteLastWeekKDownThanLastTwoWeekK(currentDate);

                                // 过滤条件：当前收盘价与某段时间最高价的百分比
//                              robot4StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeHighestPrice);

                                // 过滤条件：MA不单调递减，过滤股票
//                            String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot4StockFilterDao.filterByMANotDecreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }

                                // 保留所有金叉的股票
                                robot4StockFilterDao.filterByAllGoldCross(currentDate);
                            } else if (deadCross) {
                                logger.info("满足交易条件：close_price死叉ma5出现");
                                logger.info("操作方向：做空。交易天数为：【" + ++transactionDateNumber + "】");

                                // 向表robot4_stock_filter中插入可以做空的股票的代码
                                robot4StockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(currentDate);

                                // 保留所有周线级别收盘价大于布林带中轨的股票
//                                      robot4StockFilterDao.filterWeekClosePriceUpBollMB(currentDate);

                                // 删除周线级别，上一周close_price金叉ma5的股票
//                                      robot4StockFilterDao.deleteLastWeekClosePriceGoldCrossMa5(currentDate);
                                // 删除本周周线级别close_price金叉ma5
//                                      robot4StockFilterDao.filterByStockWeekClosePriceMa5DeadCross(currentDate);
                                // 删除本周close_price比上一周close_price大的股票
                                robot4StockFilterDao.deleteThisWeekClosePriceGreatThanLastWeekClosePrice(currentDate);
                                // 删除周线级别，KD金叉的股票
//                                      robot4StockFilterDao.filterByStockWeekKDDeadCross(currentDate);
                                // 删除周线级别，上一周KD金叉的股票
//                                      robot4StockFilterDao.deleteLastWeekKDGoldCross(currentDate);
                                // 删除上周K比上上周K大的股票1n
//                                      robot4StockFilterDao.deleteLastWeekKUpThanLastTwoWeekK(currentDate);

                                // 过滤条件：当前收盘价与某段时间最低价的百分比
//                              robot4StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeLowestPrice);

                                // 过滤条件：MA不单调递增，过滤股票
//                            String[] maLevelArray = maNotIncreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotIncreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot4StockFilterDao.filterByMANotIncreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }

                                // 保留所有死叉的股票
                                robot4StockFilterDao.filterByAllDeadCross(currentDate);
                            } else {
                                logger.warn("不做处理，因为不满足条件");

                                // 更新robot4_account表的total_assets字段
                                robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                                continue;
                            }
                        }

                        // hei_kin_ashi上涨/下跌
                        if (goldCrossDeadCrossMethod.equals(WeekGoldCrossDeadCrossMethod.HEI_KIN_ASHI_UP_DOWN)) {
                            if (goldCross) {
                                logger.info("满足交易条件：hei_kin_ashi收盘价上涨出现");
                                logger.info("操作方向：做多。交易天数为：【" + ++transactionDateNumber + "】");

                                // 向表robot4_stock_filter中插入可以做多的股票的代码
                                robot4StockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(currentDate);

                                // 保留所有周线级别收盘价小于布林带中轨的股票
//                                      robot4StockFilterDao.filterWeekClosePriceDownBollMB(currentDate);

                                // 删除周线级别，上一周close_price死叉ma5的股票
//                                      robot4StockFilterDao.deleteLastWeekClosePriceDeadCrossMa5(currentDate);
                                // 删除周线级别close_price死叉ma5
//                                      robot4StockFilterDao.filterByStockWeekClosePriceMa5GoldCross(currentDate);
                                // 删除周线级别上一周hei_kin_ashi收盘价小于上上周hei_kin_ashi收盘价的股票
//                                      robot4StockFilterDao.deleteLastWeekHeiKinAshiClosePriceLessThanLastTwoWeekHeiKinAshiClosePrice(currentDate);
                                // 删除周线级别，上一周hei_kin_ashi收盘价是阴线的股票
//                                      robot4StockFilterDao.deleteLastWeekHeiKinAshiClosePriceNegativeLine(currentDate);
                                // 删除周线级别，本周hei_kin_ashi收盘价小于上周hei_kin_ashi收盘价的股票
//                                      robot4StockFilterDao.deleteThisWeekHeiKinAshiClosePriceLessThanLastWeekHeiKinAshiClosePrice(currentDate);
                                // 删除周线级别，本周hei_kin_ashi收盘价是阴线的股票
                                robot4StockFilterDao.deleteThisWeekHeiKinAshiClosePriceNegativeLine(currentDate);
                                // 删除周线级别，KD死叉的股票
//                                      robot4StockFilterDao.filterByStockWeekKDGoldCross(currentDate);
                                // 删除周线级别，上一周KD死叉的股票
//                                      robot4StockFilterDao.deleteLastWeekKDDeadCross(currentDate);
                                // 删除上周K比上上周K小的股票
//                                      robot4StockFilterDao.deleteLastWeekKDownThanLastTwoWeekK(currentDate);

                                // 过滤条件：当前收盘价与某段时间最高价的百分比
//                              robot4StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeHighestPrice);

                                // 过滤条件：MA不单调递减，过滤股票
//                            String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot4StockFilterDao.filterByMANotDecreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }

                                // 保留所有金叉的股票
                                robot4StockFilterDao.filterByAllGoldCross(currentDate);
                            } else if (deadCross) {
                                logger.info("满足交易条件：hei_kin_ashi收盘价下跌出现");
                                logger.info("操作方向：做空。交易天数为：【" + ++transactionDateNumber + "】");

                                // 向表robot4_stock_filter中插入可以做空的股票的代码
                                robot4StockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(currentDate);

                                // 保留所有周线级别收盘价大于布林带中轨的股票
//                                      robot4StockFilterDao.filterWeekClosePriceUpBollMB(currentDate);

                                // 删除周线级别，上一周close_price金叉ma5的股票
//                                      robot4StockFilterDao.deleteLastWeekClosePriceGoldCrossMa5(currentDate);
                                // 删除周线级别close_price金叉ma5
//                                      robot4StockFilterDao.filterByStockWeekClosePriceMa5DeadCross(currentDate);
                                // 删除周线级别上一周hei_kin_ashi收盘价大于上上周hei_kin_ashi收盘价的股票
//                                      robot4StockFilterDao.deleteLastWeekHeiKinAshiClosePriceGreatThanLastTwoWeekHeiKinAshiClosePrice(currentDate);
                                // 删除周线级别，上一周hei_kin_ashi收盘价是阳线的股票
//                                      robot4StockFilterDao.deleteLastWeekHeiKinAshiClosePricePositiveLine(currentDate);
                                // 删除周线级别，本周hei_kin_ashi收盘价大于上周hei_kin_ashi收盘价的股票
//                                      robot4StockFilterDao.deleteThisWeekHeiKinAshiClosePriceGreatThanLastWeekHeiKinAshiClosePrice(currentDate);
                                // 删除周线级别，本周hei_kin_ashi收盘价是阳线的股票
                                robot4StockFilterDao.deleteThisWeekHeiKinAshiClosePricePositiveLine(currentDate);
                                // 删除周线级别，KD金叉的股票
//                                      robot4StockFilterDao.filterByStockWeekKDDeadCross(currentDate);
                                // 删除周线级别，上一周KD金叉的股票
//                                      robot4StockFilterDao.deleteLastWeekKDGoldCross(currentDate);
                                // 删除上周K比上上周K大的股票1n
//                                      robot4StockFilterDao.deleteLastWeekKUpThanLastTwoWeekK(currentDate);

                                // 过滤条件：当前收盘价与某段时间最低价的百分比
//                              robot4StockFilterDao
//                                      .filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(
//                                              currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                              currentClosePriceCompareToSomeTimeLowestPrice);

                                // 过滤条件：MA不单调递增，过滤股票
//                            String[] maLevelArray = maNotIncreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotIncreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot4StockFilterDao.filterByMANotIncreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }

                                // 保留所有死叉的股票
                                robot4StockFilterDao.filterByAllDeadCross(currentDate);
                            } else {
                                logger.warn("不做处理，因为不满足条件");

                                // 更新robot4_account表的total_assets字段
                                robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                                continue;
                            }
                        }

                        // kd金叉/死叉
                        if (goldCrossDeadCrossMethod.equals(WeekGoldCrossDeadCrossMethod.KD_GOLD_CROSS_DEAD_CROSS)) {
                            if (goldCross) {
                                logger.info("操作方向：做多。交易天数为：【" + ++transactionDateNumber + "】");

                                // 向表robot4_stock_filter中插入可以做多的股票的代码
                                robot4StockFilterDao.insertBullStockCodeFromStockTransactionDataByDate(currentDate);

                                // 保留所有周线级别收盘价小于布林带中轨的股票
//                                          robot4StockFilterDao.filterWeekClosePriceDownBollMB(currentDate);

                                // 删除周线级别close_price死叉ma5
//                                          robot4StockFilterDao.filterByStockWeekClosePriceMa5GoldCross(currentDate);
                                // 删除周线级别，上一周close_price死叉ma5的股票
//                                          robot4StockFilterDao.deleteLastWeekClosePriceDeadCrossMa5(currentDate);
                                // 删除周线级别KD，上一周死叉的股票
//                                          robot4StockFilterDao.deleteLastWeekKDDeadCross(currentDate);
                                // 删除周线级别，KD死叉的股票
//                                          robot4StockFilterDao.filterByStockWeekKDGoldCross(currentDate);
                                // 删除上周K比上上周K小的股票
//                                          robot4StockFilterDao.deleteLastWeekKDownThanLastTwoWeekK(currentDate);
                                // 删除这周K比上周K小的股票
                                robot4StockFilterDao.deleteThisWeekKDownThanLastTwoWeekK(currentDate);

                                // 过滤条件：当前收盘价与某段时间最高价的百分比
//                                  robot4StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeHighestPrice);

                                // 过滤条件：MA不单调递减，过滤股票
//                            String[] maLevelArray = maNotDecreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotDecreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot4StockFilterDao.filterByMANotDecreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }

                                // 保留所有金叉的股票
                                robot4StockFilterDao.filterByAllGoldCross(currentDate);
                            } else if (deadCross) {
                                logger.info("操作方向：做空。交易天数为：【" + ++transactionDateNumber + "】");

                                // 向表robot4_stock_filter中插入可以做空的股票的代码
                                robot4StockFilterDao.insertShortStockCodeFromStockTransactionDataByDate(currentDate);

                                // 保留所有周线级别收盘价大于布林带中轨的股票
//                                          robot4StockFilterDao.filterWeekClosePriceUpBollMB(currentDate);

                                // 删除周线级别close_price金叉ma5
//                                          robot4StockFilterDao.filterByStockWeekClosePriceMa5DeadCross(currentDate);
                                // 删除周线级别，上一周close_price金叉ma5的股票
//                                          robot4StockFilterDao.deleteLastWeekClosePriceGoldCrossMa5(currentDate);
                                // 删除周线级别，上一周KD金叉的股票
//                                          robot4StockFilterDao.deleteLastWeekKDGoldCross(currentDate);
                                // 删除周线级别，KD金叉的股票
//                                          robot4StockFilterDao.filterByStockWeekKDDeadCross(currentDate);
                                // 删除上周K比上上周K大的股票
//                                          robot4StockFilterDao.deleteLastWeekKUpThanLastTwoWeekK(currentDate);
                                // 删除这周K比上周K大的股票
                                robot4StockFilterDao.deleteThisWeekKUpThanLastTwoWeekK(currentDate);

                                // 过滤条件：当前收盘价与某段时间最低价的百分比
//                                  robot4StockFilterDao
//                                    .filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(
//                                            currentClosePriceCompareToSomeTimeHighestLowestPriceBeginDate, currentDate,
//                                            currentClosePriceCompareToSomeTimeLowestPrice);

                                // 过滤条件：MA不单调递增，过滤股票
//                            String[] maLevelArray = maNotIncreasingLevel.replace("[", "").replace("]", "").split(",");
//                            String[] dateNumberArray = maNotIncreasingDateNumber.replace("[", "").replace("]", "").split(",");
//                            for (int j = 0; j < maLevelArray.length && j < dateNumberArray.length; j++) {
//                                String maLevel = maLevelArray[j].trim();
//                                String dateNumber = dateNumberArray[j].trim();
//                                robot4StockFilterDao.filterByMANotIncreasing(currentDate, Integer.parseInt(maLevel), dateNumber);
//                            }
                                // 保留所有死叉的股票
                                robot4StockFilterDao.filterByAllDeadCross(currentDate);
                            } else {
                                logger.warn("不做处理，因为不满足条件");

                                // 更新robot4_account表的total_assets字段
                                robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                                continue;
                            }
                        }

                        // 只保留这个价格区间以内的股票
                        robot4StockFilterDao.filterByLessThanClosePrice(currentDate, closePriceStart, closePriceEnd);

                        // 过滤条件：删除除过权的股票
                        robot4StockFilterDao.filterByXr(xrBeginDate, weekKdEndDate);

                    }
                } else {
                    logger.warn("没有前两个星期的记录或者不足两个星期，从下一个日期开始处理");

                    // 更新robot4_account表的total_assets字段
                    robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);

                    continue;
                }

                /*************************************** 买股票/卖股票 ******************************************/
                // 如果当前日期是周末，则买股票，否则跳过这一天
                if (isWeekend) {
                    // 买股票/卖股票
                    // 确定交易方向（做多/做空）
                    Integer direction = null;
                    if (goldCross) {
                        direction = 1;
                    }
                    if (deadCross) {
                        direction = -1;
                    }
                    robot4StockTransactRecordDao.buyOrSell(currentDate, backwardMonth, averageDateNumber, FilterMethod.SELECT_FILTER_METHOD, FilterMethod.SELECT_FILTER_METHOD_VALUE, direction, shippingSpaceControl, holdStockNumber);

                    // 更新robot4_account表的total_assets字段
                    robot4AccountDao.updateRobotAccountAfterBuyOrSell(currentDate);
                }

                // 获取买卖建议
                String printText = null;
                if (printFormat.equals(RobotPrintBuySellSuggestionFormat.JSON)) {
                    // json方式
                    printText = super.printBuySellSuggestionByJson_weekKD(currentDate);
                }
                if (printFormat.equals(RobotPrintBuySellSuggestionFormat.SIMPLE)) {
                    // 简单文本格式
                    printText = super.printBuySellSuggestionBySimple_weekKD(currentDate);
                }
                logger.info("买卖建议：\n" + printText);
            }
        }
    }

    /**
     * 卖股票/买股票
     */
    @Override
    public void sellOrBuy() {

    }

    /**
     * 买股票/卖股票
     */
    @Override
    public void buyOrSell() {

    }

    /**
     * 打印买卖建议
     */
    @Override
    public void printBuySellSuggestion() {

    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
