package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockMonthAnalysis;
import cn.com.acca.ma.model.ModelTopStock;
import java.util.Date;
import java.util.List;

public interface ModelStockMonthAnalysisDao extends BaseDao {

    /**
     * 计算model_stock_month_analysis表的全部数据
     */
    void writeModelStockMonthAnalysis();

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_STOCK_MONTH_ANALYSIS中获取END_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_STOCK_MONTH_ANALYSIS中获取date日的所有ModelStockMonthAnalysis对象
     * @param date
     * @return
     */
    List<ModelStockMonthAnalysis> getModelStockMonthAnalysisByDate(String date);

    /**
     * 计算model_stock_month_analysis表的某一日的数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     */
    void writeModelStockMonthAnalysisByDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 根据开始日期和结束日期，查找中间的日期，并去重
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
//    List<String> findDistinctDateBetweenBeginDateAndEndDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 向表MDL_STOCK_MONTH_ANALYSIS中插入ModelStockMonthAnalysis对象
     * @param modelStockMonthAnalysis
     */
    void save(ModelStockMonthAnalysis modelStockMonthAnalysis);
}
