package cn.com.acca.ma.main;

import cn.com.acca.ma.constant.FilterMethod;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class RobotMain extends AbstractMain {

    private static Logger logger = LogManager.getLogger(RobotMain.class);

    public static void main(String[] args) {
        logger.info("RobotMain程序开始运行......");

        /******************************* 设置代理服务器的IP和port *******************************/
        setProxyProperties();

        /******************************* 收集数据库统计信息 *************************************/
//		gatherDatabaseStatistics();

        /*************************** 执行某段时间内的股票买卖交易（成功率） *************************/
        doBuyAndSellByBeginDateAndEndDate_successRate();

        /******************************* 将测试数据导入到历史数据表中 *****************************/
        importIntoHistoryDataTableFromRobot();

        /********************************* 执行某一日的股票买卖交易 ******************************/
//        doBuyAndSellByDate();

    }

    /**
     * 执行某一日的股票买卖交易
     */
    private static void doBuyAndSellByDate(){
        /*************************************** 卖股票/买股票 ***********************************************/
        updateRobotAccountBeforeSellOrBuy();
        sellOrBuy();
        updateRobotAccountAfterSellOrBuy();

        /********************************** 选择最近成功率最高的算法 ***********************************/
        selectHighestSuccessRateStrategy();

        /*************************************** 按照条件，过滤股票 ***********************************/
        if (null != FilterMethod.SELECT_FILTER_METHOD) {
            // 金叉做多的算法
            if (1 == FilterMethod.SELECT_FILTER_METHOD % 2){
                initBullRobotStockFilter();
                filterByLessThanClosePrice();
                filterByXr();
                filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice();
                filterByMANotDecreasing();
                filterByHighestSuccessRateStrategy();
            }
            // 死叉做空的算法
            if (0 == FilterMethod.SELECT_FILTER_METHOD % 2){
                initShortRobotStockFilter();
                filterByLessThanClosePrice();
                filterByXr();
                filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice();
                filterByMANotIncreasing();
                filterByHighestSuccessRateStrategy();
            }

            /*filterByWeekKDGoldCross();*/ // 这个方法失败了，以后不用了
            /* 在使用最高成功率算法后，下面4个方法不再使用 */
            /*filterByMACDGoldCross();
            filterByClosePriceGoldCrossMA5();
            filterByHeiKinAshiUp();
            filterByKDGoldCross();*/
        }

        /*************************************** 买股票/卖股票 ******************************************/
        if (null != FilterMethod.SELECT_FILTER_METHOD) {
            buyOrSell();
            updateRobotAccountAfterBuyOrSell();
            printBuySellSuggestion();
        }

        logger.info("RobotMain程序执行完毕。");
    }
}
