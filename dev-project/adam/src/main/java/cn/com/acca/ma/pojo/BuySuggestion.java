package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 买建议
 */
@Data
public class BuySuggestion {

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 买入日期
     */
    private Date buyDate;

    /**
     * 买入价格
     */
    private BigDecimal buyPrice;

    /**
     * 买入数量
     */
    private Integer buyAmount;

    /**
     * 过滤类型
     */
    private Integer filterType;

    /**
     * 多空方向。1表示做多；-1表示做空
     */
    private Integer direction;

}
