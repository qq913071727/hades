package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * etf信息
 * @author Administrator
 */
@Data
@Entity
@Table(name="ETF_INFO")
public class EtfInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @Column(name="ID_")
    private Integer id;

    /**
     * 表示股票代码
     */
    @Column(name="CODE_")
    private String code;

    /**
     * 表示股票名称
     */
    @Column(name="NAME_")
    private String name;

    /**
     * 有的etf由于震荡区间太小，没有交易价值。是否可用。1表示可用，0表示不可用
     */
    @Column(name="AVAILABLE")
    private Integer available;
    
    /**
     * 趋势类型。空表示无法确定类型，1表示和股票正相关，2表示和股票不相关
     */
    @Column(name="TENDENCY_TYPE")
    private Integer tendencyType;
}
