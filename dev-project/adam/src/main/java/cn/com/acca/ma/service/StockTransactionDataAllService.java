package cn.com.acca.ma.service;

public interface StockTransactionDataAllService extends BaseService  {

    /**
     * 从表stock_transaction_data向表stock_transaction_data_all中写数据
     * @param multithreading
     */
    void writeStockTransactionDataAllByDate(boolean multithreading);


}
