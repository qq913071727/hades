package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.Robot2Account;
import cn.com.acca.ma.service.Robot2AccountService;

import java.util.List;

public class Robot2AccountServiceImpl extends BaseServiceImpl<Robot2AccountServiceImpl, Robot2Account> implements
        Robot2AccountService {

    public Robot2AccountServiceImpl() {
        super();
    }

    /**************************************** 卖股票/买股票 ********************************************/
    /**
     * 根据当日持有股票的收盘价，在卖股票/买股票之前，更新robot2_account表
     */
    @Override
    public void updateRobotAccountBeforeSellOrBuy(){
        logger.info("更新robot2_account表的stock_assets、total_assets字段");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.update.robot.account.before.sell.date");

        // 更新robot_account表
        robot2AccountDao.updateRobotAccountBeforeSellOrBuy(date);
    }

    /**
     * 根据当日卖出/买入股票的收盘价，在卖股票/买股票之后，更新robot2_account表
     */
    @Override
    public void updateRobotAccountAfterSellOrBuy() {
        logger.info("更新robot2_account表的hold_stock_number、stock_assets、capital_assets字段");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.update.robot.account.after.sell.buy.date");

        // 更新robot2_account表
        robot2AccountDao.updateRobotAccountAfterSellOrBuy(date);
    }

    /**************************************** 买股票/卖股票 ********************************************/
    /**
     * 根据当日买入股票/卖出股票的收盘价，在买股票/卖股票之后，更新robot2_account表
     */
    @Override
    public void updateRobotAccountAfterBuyOrSell() {
        logger.info("更新robot2_account表的hold_stock_number、stock_assets、capital_assets字段");

        String date = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.update.robot.account.after.buy.sell.date");

        // 更新robot2_account表的total_assets字段
        robot2AccountDao.updateRobotAccountAfterBuyOrSell(date);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
