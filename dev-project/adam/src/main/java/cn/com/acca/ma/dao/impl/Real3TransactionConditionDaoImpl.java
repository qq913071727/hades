package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Real3TransactionConditionDao;
import cn.com.acca.ma.model.Real3TransactionCondition;

import java.util.List;

public class Real3TransactionConditionDaoImpl extends BaseDaoImpl<Real3TransactionConditionDao> implements
    Real3TransactionConditionDao {

    public Real3TransactionConditionDaoImpl() {
        super();
    }

    /**
     * 向real_transaction_condition表中写数据。
     * ma120NotDecreasing为1时表示判断120均线是否不单调递减，ma120NotDecreasingDateNumber表示120日均线多少天内不单调递减；
     * ma250NotDecreasing为1时表示判断250均线是否不单调递减，ma250NotDecreasingDateNumber表示250日均线多少天内不单调递减。
     * ma120NotIncreasing为1时表示判断120均线是否不单调递增，ma120NotIncreasingDateNumber表示120日均线多少天内不单调递增；
     * ma250NotIncreasing为1时表示判断250均线是否不单调递增，ma250NotIncreasingDateNumber表示250日均线多少天内不单调递增。
     * @param xrBeginDate
     * @param date
     * @param lessThanPercentage
     * @param moreThanPercentage
     * @param closePriceStart
     * @param closePriceEnd
     * @param ma120NotDecreasing
     * @param ma120NotDecreasingDateNumber
     * @param ma250NotDecreasing
     * @param ma250NotDecreasingDateNumber
     * @param ma120NotIncreasing
     * @param ma120NotIncreasingDateNumber
     * @param ma250NotIncreasing
     * @param ma250NotIncreasingDateNumber
     */
    @Override
    public void writeReal3TransactionCondition(String xrBeginDate, String date, /*Integer lessThanPercentage,
                                              Integer moreThanPercentage, Double closePriceStart, Double closePriceEnd,*/
                                              Integer ma120NotDecreasing, Integer ma120NotDecreasingDateNumber,
                                              Integer ma250NotDecreasing, Integer ma250NotDecreasingDateNumber,
                                              Integer ma120NotIncreasing, Integer ma120NotIncreasingDateNumber,
                                              Integer ma250NotIncreasing, Integer ma250NotIncreasingDateNumber) {
        logger.info("开始向real3_transaction_condition表中写数据，日期为【" + date + "】");

        String sql = "{call PKG_REAL3_TRANSACTION.write_real3_transac_cond('" + xrBeginDate + "', '" + date + "', "
//                + lessThanPercentage + ", " + moreThanPercentage + ", " + closePriceStart + ", " + closePriceEnd + ", "
                + ma120NotDecreasing + ", " + ma120NotDecreasingDateNumber + ", "
                + ma250NotDecreasing + ", " + ma250NotDecreasingDateNumber + ", "
                + ma120NotIncreasing + ", " + ma120NotIncreasingDateNumber + ", "
                + ma250NotIncreasing + ", " + ma250NotIncreasingDateNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 清空real3_transaction_condition表中的数据
     */
    @Override
    public void truncateTableReal3TransactionCondition() {
        logger.info("开始清空real3_transaction_condition表中的数据");

        String sql = "truncate table real3_transaction_condition";

        doSQLInTransaction(sql);
    }

    /**
     * 根据stock_code字段查找Real3TransactionCondition类型对象
     * @param stockCode
     * @return
     */
    @Override
    public Real3TransactionCondition findByStockCode(String stockCode) {
        logger.info("根据stock_code字段查找Real3TransactionCondition类型对象");

        String sql = "select * "
            + "from real3_transaction_condition t "
            + "where t.stock_code ='" + stockCode + "'";

        List list = doSQLQueryInTransaction(sql, null, Real3TransactionCondition.class);
        if (list.size() == 0){
            logger.warn("表real3_transaction_condition中没有code为【" + stockCode + "】的记录，这支"
                + "股票应在已经停牌了");
            return null;
        } else {
            return (Real3TransactionCondition) doSQLQueryInTransaction(sql, null, Real3TransactionCondition.class).get(0);
        }
    }

    /**
     * 根据条件查找记录：未除权、120日和250日均线不单调递减
     * 如果allFilterCondition为true，则使用所有过滤条件；否则只是有价格区间条件
     * @return
     */
    @Override
    public List<Real3TransactionCondition> findByCondition(boolean allFilterCondition) {
        logger.info("根据条件查找记录：未除权、120日和250日均线不单调递减");

        String sql;
        if (allFilterCondition){
            sql = "select * from real3_transaction_condition t " +
                    "where t.is_xr=0 " +
                    "and t.is_ma_1_2_0_not_decreasing=1 and t.is_ma_2_5_0_not_decreasing=1 " +
                    "and t.stock_code not in(select t1.stock_code from real3_stock_transaction_record t1 " +
                    "where t1.direction=1 and t1.sell_date is null) " +
                    "union " +
                    "select * from real3_transaction_condition t " +
                    "where t.is_xr=0 " +
                    "and t.is_ma_1_2_0_not_increasing=1 and t.is_ma_2_5_0_not_increasing=1 " +
                    "and t.stock_code not in(select t1.stock_code from real3_stock_transaction_record t1 " +
                    "where t1.direction=-1 and t1.buy_date is null)";
        } else {
            sql = "select * from real3_transaction_condition t " +
                    "where t.stock_code not in(select t1.stock_code from real3_stock_transaction_record t1 " +
                    "where t1.direction=1 and t1.sell_date is null) " +
                    "union " +
                    "select * from real3_transaction_condition t " +
                    "where t.stock_code not in(select t1.stock_code from real3_stock_transaction_record t1 " +
                    "where t1.direction=-1 and t1.buy_date is null)";
        }

        return (List<Real3TransactionCondition>) doSQLQueryInTransaction(sql, null, Real3TransactionCondition.class);
    }
}
