package cn.com.acca.ma.thread.picture;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.BoardIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 创建板块K线图
 */
public class CreateBoardPictureThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CreateBoardPictureThread.class);

    private BoardIndexService boardIndexService;

    public CreateBoardPictureThread() {
    }

    public CreateBoardPictureThread(BoardIndexService boardIndexService) {
        this.boardIndexService = boardIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始创建板块K线图，调用的方法为【createBoardPicture】");

        boardIndexService.createBoardPicture(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
