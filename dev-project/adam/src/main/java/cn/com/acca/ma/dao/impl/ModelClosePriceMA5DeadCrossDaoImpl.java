package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelClosePriceMA5DeadCrossDao;
import cn.com.acca.ma.hibernate.impl.GetClosePriceMa5DeadCrossSuccessRateWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelClosePriceMA5DeadCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ModelClosePriceMA5DeadCrossDaoImpl extends BaseDaoImpl<ModelClosePriceMA5DeadCrossDaoImpl> implements
        ModelClosePriceMA5DeadCrossDao {

    public ModelClosePriceMA5DeadCrossDaoImpl() {
        super();
    }

    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    @Override
    public void writeModelClosePriceMA5DeadCross() {
        logger.info("开始海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_C_P_MA5_DEAD_CROSS()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据结束");
    }

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     * @param endDate
     */
    @Override
    public void writeModelClosePriceMA5DeadCrossIncr(String endDate) {
        logger.info("开始增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session
                .createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_C_P_MA5_D_C_INCR(?)}");
        query.setParameter(0, endDate);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据结束");
    }

    /**
     * 获取一段时间内，最近dateNumber天，close_price死叉ma5的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    @Override
    public List<SuccessRateOrPercentagePojo> getClosePriceMa5DeadCrossSuccessRate(boolean multithreading, String beginDate,
                                                     String endDate, Integer dateNumber) {
        logger.info("获取开始时间【" + beginDate + "】至【" + endDate + "】内，最近【" + dateNumber + "】天，"
                + "close_price死叉ma5的成功率");

        final List<SuccessRateOrPercentagePojo> list=new ArrayList<SuccessRateOrPercentagePojo>();

        if (multithreading) {
            Session newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            newSession.doWork(new GetClosePriceMa5DeadCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            session.doWork(new GetClosePriceMa5DeadCrossSuccessRateWorkImpl(beginDate, endDate, dateNumber, list));
            session.getTransaction().commit();
            session.close();
        }

        logger.info("获取一段时间内，最近" + dateNumber + "天，close_price死叉ma5的成功率完成");
        return list;
    }

    /**
     * 获取一段时间内，close_price死叉ma5的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SuccessRateOrPercentagePojo> getClosePriceMa5DeadCrossPercentage(boolean multithreading, final String beginDate, final String endDate){
        logger.info("开始获取一段时间内【" + beginDate + "】至【" + endDate + "】，close_price死叉ma5的百分比");

        String hql = "select t.date_ transactionDate, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_ " +
                "and t1.close_price<t1.ma5)/ " +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 successRateOrPercentage " +
                "from stock_transaction_data_all t " +
                "where t.date_ between to_date(?,'yyyy-mm-dd') and to_date(?,'yyyy-mm-dd') " +
                "group by t.date_ order by t.date_ asc";

        Session newSession = null;
        Query query;
        if (multithreading) {
            newSession = HibernateUtil.newSession();
            newSession.beginTransaction();
            query = newSession.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        } else {
            session = HibernateUtil.currentSession();
            session.beginTransaction();
            query = session.createSQLQuery(hql).addEntity(SuccessRateOrPercentagePojo.class);
        }

        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<SuccessRateOrPercentagePojo> list = query.list();

        if (multithreading) {
            newSession.getTransaction().commit();
            HibernateUtil.closeNewSessionFactory(newSession);
        } else {
            session.getTransaction().commit();
            session.close();
        }

        return list;
    }

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    @Override
    public List<Date> getDateByCondition(String beginDate, String endDate) {
        logger.info("根据开始时间【" + beginDate + "】和结束时间【" + endDate + "】，"
                + "从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取BUY_DATE字段");

        StringBuffer hql = new StringBuffer("select distinct t.buyDate from ModelClosePriceMA5DeadCross t " +
                "where t.buyDate between to_date(?, 'yyyy-mm-dd') and to_date(?, 'yyyy-mm-dd')");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        List<Date> list = query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取buy_date日的所有ModelClosePriceMA5DeadCross对象
     * @param date
     * @return
     */
    @Override
    public List<ModelClosePriceMA5DeadCross> getModelClosePriceMA5DeadCrossByDate(String date) {
        logger.info("从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取日期【" + date + "】的所有ModelClosePriceMA5DeadCross对象");

        StringBuffer hql = new StringBuffer("select t from ModelClosePriceMA5DeadCross t " +
                "where t.buyDate=to_date(?,'yyyy-mm-dd') order by t.buyDate asc");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createQuery(hql.toString());
        query.setParameter(0, date);
        List<ModelClosePriceMA5DeadCross> list=query.list();
        session.getTransaction().commit();
        session.close();

        return list;
    }

    /**
     * 向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入ModelClosePriceMA5DeadCross对象
     * @param modelClosePriceMA5DeadCross
     */
    public void save(ModelClosePriceMA5DeadCross modelClosePriceMA5DeadCross){
        logger.info("向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入ModelClosePriceMA5DeadCross对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.persist(modelClosePriceMA5DeadCross);
        session.getTransaction().commit();
        session.close();
    }

}
