package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelAllGoldCross;
import cn.com.acca.ma.model.ModelMACDGoldCross;
import cn.com.acca.ma.model.ModelTopStock;
import java.util.Date;
import java.util.List;

public interface ModelAllGoldCrossDao extends BaseDao {

	/**
	 * 海量地向表MDL_ALL_GOLD_CROSS中插入数据
	 * @param type
	 */
	void writeModelAllGoldCross(String type);

	/**
	 * 查找所有ModelAllGoldCross类型对象
	 * @return
	 */
	List<ModelAllGoldCross> findAll();

	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_ALL_GOLD_CROSS中获取SELL_DATE字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<Date> getDateByCondition(String beginDate, String endDate);

	/**
	 * 从表MDL_ALL_GOLD_CROSS中获取sell_date日的所有ModelAllGoldCross对象
	 * @param date
	 * @return
	 */
	List<ModelAllGoldCross> getModelAllGoldCrossByDate(String date);

	/**
	 * 向表MDL_ALL_GOLD_CROSS中插入ModelAllGoldCross对象
	 * @param modelAllGoldCross
	 */
	void save(ModelAllGoldCross modelAllGoldCross);
}
