package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelHeiKinAshiDownUp;
import cn.com.acca.ma.service.ModelHeiKinAshiDownUpService;

import java.util.List;

public class ModelHeiKinAshiDownUpServiceImpl extends BaseServiceImpl<ModelHeiKinAshiDownUpServiceImpl, ModelHeiKinAshiDownUp> implements
        ModelHeiKinAshiDownUpService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     * @param multithreading
     */
    @Override
    public void writeModelHeiKinAshiDownUpIncr(boolean multithreading) {
        String endDate = PropertiesUtil
                .getValue(MODEL_HEI_KIN_ASHI_DOWN_UP_PROPERTIES, "hei.kin.ashi.down.up.end_date");
        modelHeiKinAshiDownUpDao.writeModelHeiKinAshiDownUpIncr(multithreading, endDate);
    }

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     */
    @Override
    public void writeModelHeiKinAshiDownUp() {
        modelHeiKinAshiDownUpDao.writeModelHeiKinAshiDownUp();
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
