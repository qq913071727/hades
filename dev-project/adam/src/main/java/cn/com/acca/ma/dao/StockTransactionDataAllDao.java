package cn.com.acca.ma.dao;

public interface StockTransactionDataAllDao extends BaseDao {

    /**
     * 根据开始日期和结束日期，从表stock_transaction_data向表stock_transaction_data_all中写数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     */
    void writeStockTransactionDataAllByDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 删除stock_transaction_data_all表中，开始时间和结束时间之间的记录
     * @param beginDate
     * @param endDate
     */
    void deleteWithDate(String beginDate, String endDate);
}
