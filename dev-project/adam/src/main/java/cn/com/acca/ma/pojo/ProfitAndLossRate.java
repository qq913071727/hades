package cn.com.acca.ma.pojo;

import java.math.BigDecimal;
import lombok.Data;

/**
 * Robot3Main和Robot4Main的收益率总和
 * @author Administrator
 */
@Data
public class ProfitAndLossRate {
    
    /**
     * Robot3Main的收益率总和
     */
    private BigDecimal robot3MainProfitAndLossRate;
    
    /**
     * Robot4Main的收益率总和
     */
    private BigDecimal robot4MainProfitAndLossRate;
    
}
