package cn.com.acca.ma.constant;

/**
 * 使用单一算法，还是使用所有算法
 */
public class UseSingleOrUseAllMethod {

    /**
     * 使用单一算法
     */
    public static final Integer USE_SINGLE_METHOD = 1;

    /**
     * 使用所有算法
     */
    public static final Integer USE_ALL_METHOD = 2;
}
