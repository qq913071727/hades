package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexHeiKinAshiDownUp;

import java.util.List;

public interface ModelEtfHeiKinAshiDownUpDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入，
     * 向表mdl_etf_hei_kin_ashi_down_up插入数据
     * @param validateMA120NotDecreasing
     * @param validateMA250NotDecreasing
     * @param defaultBeginDate
     * @param endDate
     * @param type
     */
    void writeModelEtfHeiKinAshiDownUp(Integer validateMA120NotDecreasing, Integer validateMA250NotDecreasing, String defaultBeginDate,
                                       String endDate, Integer type);

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
//    List<ModelStockIndexHeiKinAshiDownUp> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
