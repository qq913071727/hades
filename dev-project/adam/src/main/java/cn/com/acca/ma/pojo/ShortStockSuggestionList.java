package cn.com.acca.ma.pojo;

import lombok.Data;

import java.util.List;

/**
 * 做空股票的卖买建议列表
 */
@Data
public class ShortStockSuggestionList {

    /**
     * 应当卖出的股票的列表（用于做空）
     */
    private List<StockInfoPojo> shouldSellShortStockList;

    /**
     * 应当买入的股票的列表（用于做空）
     */
    private List<StockInfoPojo> shouldBuyShortStockList;
}
