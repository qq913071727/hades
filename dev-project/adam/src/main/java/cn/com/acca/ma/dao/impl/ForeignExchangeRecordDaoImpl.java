package cn.com.acca.ma.dao.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.dao.ForeignExchangeRecordDao;
import cn.com.acca.ma.model.ForeignExchangeRecord;
import cn.com.acca.ma.mybatis.util.MyBatisUtil;
import cn.com.acca.ma.xmlmapper.ForeignExchangeRecordMapper;

public class ForeignExchangeRecordDaoImpl extends BaseDaoImpl<ForeignExchangeRecordDaoImpl> implements ForeignExchangeRecordDao {

	/**
	 * 向表FOREIGN_EXCHANGE_RECORD表中插入记录，用于从csv文件插入数据
	 * @param foreignExchangeRecord
	 */
	public void insertForeignExchangeRecord(ForeignExchangeRecord foreignExchangeRecord){
		logger.info("method insertForeignExchangeRecord begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("currency", foreignExchangeRecord.getCurrency());
		param.put("dateTime", foreignExchangeRecord.getDateTime());
		param.put("kLevel", foreignExchangeRecord.getkLevel());
		param.put("open", foreignExchangeRecord.getOpen());
		param.put("high", foreignExchangeRecord.getHigh());
		param.put("low", foreignExchangeRecord.getLow());
		param.put("close", foreignExchangeRecord.getClose());
		param.put("volume", foreignExchangeRecord.getVolume());		
		sqlSession.insert("cn.com.acca.ma.xmlmapper.ForeignExchangeRecordMapper.insertForeignExchangeRecord",param);
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method insertForeignExchangeRecord finish");
	}
	
	/**
	 * 判断表FOREIGN_EXCHANGE_RECORD中是否有相同的记录
	 * @param foreignExchangeRecord
	 * @return
	 */
	public boolean containForeignExchangeRecord(ForeignExchangeRecord foreignExchangeRecord){
		logger.info("method containForeignExchangeRecord begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("currency", foreignExchangeRecord.getCurrency());
		param.put("dateTime", foreignExchangeRecord.getDateTime());
		param.put("kLevel", foreignExchangeRecord.getkLevel());
		param.put("open", foreignExchangeRecord.getOpen());
		param.put("high", foreignExchangeRecord.getHigh());
		param.put("low", foreignExchangeRecord.getLow());
		param.put("close", foreignExchangeRecord.getClose());
		param.put("volume", foreignExchangeRecord.getVolume());
		List<ForeignExchangeRecord> list=sqlSession.selectList("cn.com.acca.ma.xmlmapper.ForeignExchangeRecordMapper.containForeignExchangeRecord",param);
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method containForeignExchangeRecord finish");
		if(list.size()==0){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的5日简单移动平均线
	 */
	public void calculateFiveMovingAverage(){
		logger.info("method calculateFiveMovingAverage begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		foreignExchangeRecordMapper = sqlSession.getMapper(ForeignExchangeRecordMapper.class);
		foreignExchangeRecordMapper.calculateFiveMovingAverage();
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method calculateFiveMovingAverage finish");
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的10日简单移动平均线
	 */
	public void calculateTenMovingAverage(){
		logger.info("method calculateTenMovingAverage begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		foreignExchangeRecordMapper = sqlSession.getMapper(ForeignExchangeRecordMapper.class);
		foreignExchangeRecordMapper.calculateTenMovingAverage();
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method calculateTenMovingAverage finish");
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的20日简单移动平均线
	 */
	public void calculateTwentyMovingAverage(){
		logger.info("method calculateTwentyMovingAverage begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		foreignExchangeRecordMapper = sqlSession.getMapper(ForeignExchangeRecordMapper.class);
		foreignExchangeRecordMapper.calculateTwentyMovingAverage();
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method calculateTwentyMovingAverage finish");
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的60日简单移动平均线
	 */
	public void calculateSixtyMovingAverage(){
		logger.info("method calculateSixtyMovingAverage begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		foreignExchangeRecordMapper = sqlSession.getMapper(ForeignExchangeRecordMapper.class);
		foreignExchangeRecordMapper.calculateSixtyMovingAverage();
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method calculateSixtyMovingAverage finish");
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的120日简单移动平均线
	 */
	public void calculateOneHundredTwentyMovingAverage(){
		logger.info("method calculateOneHundredTwentyMovingAverage begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		foreignExchangeRecordMapper = sqlSession.getMapper(ForeignExchangeRecordMapper.class);
		foreignExchangeRecordMapper.calculateOneHundredTwentyMovingAverage();
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method calculateOneHundredTwentyMovingAverage finish");
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的250日简单移动平均线
	 */
	public void calculateTwoHundredFiftyMovingAverage(){
		logger.info("method calculateTwoHundredFiftyMovingAverage begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		foreignExchangeRecordMapper = sqlSession.getMapper(ForeignExchangeRecordMapper.class);
		foreignExchangeRecordMapper.calculateTwoHundredFiftyMovingAverage();
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method calculateTwoHundredFiftyMovingAverage finish");
	}
	
	/**
	 * 根据开始日期beginDate和结束日期endDate，获取DATETIME字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	public List<ForeignExchangeRecord> getDateByCondition(String beginDate,String endDate){
		logger.info("method getDateByCondition begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		
		Date tempBeginDate=DateUtil.stringToDate(beginDate);
		Date tempEndDate=DateUtil.stringToDate(endDate);
		long longBeginDate=tempBeginDate.getTime();
		long longEndDate=tempEndDate.getTime();
		
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("beginDate", new Timestamp(longBeginDate));
		param.put("endDate", new Timestamp(longEndDate));
		List<ForeignExchangeRecord> list=sqlSession.selectList("cn.com.acca.ma.xmlmapper.ForeignExchangeRecordMapper.getDateByCondition",param);
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method getDateByCondition finish");
		return list;
	}
	
	/**
	 * 根据日期date获取所有ForeignExchangeRecord对象
	 * @param date
	 * @return
	 */
	public List<ForeignExchangeRecord> getForeignExchangeRecordByDate(String date){
		logger.info("method getForeignExchangeRecordByDate begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		
		Date tempDate=DateUtil.stringToDate(date);
		long longDate=tempDate.getTime();
		
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("date", new Timestamp(longDate));
		List<ForeignExchangeRecord> list=sqlSession.selectList("cn.com.acca.ma.xmlmapper.ForeignExchangeRecordMapper.getForeignExchangeRecordByDate",param);
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method getForeignExchangeRecordByDate finish");
		return list;
	}

	/**
	 * 向FOREIGN_EXCHANGE_RECORD表中插入数据，用于Json文件恢复数据
	 * @param foreignExchangeRecord
	 */
	public void restoreForeignExchangeRecord(ForeignExchangeRecord foreignExchangeRecord){
		logger.info("method restoreForeignExchangeRecord begin");
		
		sqlSession=MyBatisUtil.openSqlSession();
		Map<String,Object> param=new HashMap<String,Object>();
		param.put("currency", foreignExchangeRecord.getCurrency());
		param.put("dateTime", foreignExchangeRecord.getDateTime());
		param.put("kLevel", foreignExchangeRecord.getkLevel());
		param.put("open", foreignExchangeRecord.getOpen());
		param.put("high", foreignExchangeRecord.getHigh());
		param.put("low", foreignExchangeRecord.getLow());
		param.put("close", foreignExchangeRecord.getClose());
		param.put("volume", foreignExchangeRecord.getVolume());
		param.put("five", foreignExchangeRecord.getFive());
		param.put("ten", foreignExchangeRecord.getTen());
		param.put("twenty", foreignExchangeRecord.getTwenty());
		param.put("sixty", foreignExchangeRecord.getSixty());
		param.put("oneHundredTwenty", foreignExchangeRecord.getOneHundredTwenty());
		param.put("twoHundredFifty", foreignExchangeRecord.getTwoHundredFifty());	
		sqlSession.insert("cn.com.acca.ma.xmlmapper.ForeignExchangeRecordMapper.insertForeignExchangeRecord",param);
		sqlSession.commit();
		sqlSession.close();
		
		logger.info("method restoreForeignExchangeRecord finish");
	}
	
}
