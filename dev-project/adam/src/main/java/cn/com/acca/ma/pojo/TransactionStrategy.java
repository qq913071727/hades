package cn.com.acca.ma.pojo;

/**
 * 交易策略
 */
public class TransactionStrategy {

    /**
     * 金叉死叉成功率算法
     */
    public static final Integer GOLD_CROSS_DEAD_CROSS_SUCCESS_RATE = 1;

    /**
     * 金叉死叉百分比算法
     */
    public static final Integer GOLD_CROSS_DEAD_CROSS_PERCENTAGE = 2;
}
