package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.model.EtfTransactionData;
import cn.com.acca.ma.service.EtfTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.List;

/**
 * 生产者线程，使用sohu接口，用于从网络上采集ETF数据，并存储数据
 */
public class CollectEtfTransactionDataThread_sohu extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(CollectEtfTransactionDataThread_sohu.class);

    private EtfTransactionDataService etfTransactionDataService;

    /**
     * ETF代码的前缀
     */
    private String etfInfoCodePrefix;

    /**
     * ETF代码
     */
    private String code;

    /**
     * 开始日期
     */
    private String beginDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 前一日收盘价
     */
    private BigDecimal lastClosePrice;

    public CollectEtfTransactionDataThread_sohu() {

    }

    public CollectEtfTransactionDataThread_sohu(EtfTransactionDataService etfTransactionDataService,
                                                String etfInfoCodePrefix, String code, String beginDate,
                                                String endDate, BigDecimal lastClosePrice) {
        this.etfTransactionDataService = etfTransactionDataService;
        this.etfInfoCodePrefix = etfInfoCodePrefix;
        this.code = code;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.lastClosePrice = lastClosePrice;
    }

    @Override
    public void run() {
        logger.debug("启动生产者线程。前缀为【" + etfInfoCodePrefix + "】，ETF代码为【" + code + "】，"
                + "开始时间为【" + beginDate + "】，结束时间为【" + endDate + "】");

        Socket socket = null;
        OutputStream outputStream = null;
        try {
            this.handle();
        } finally {
            if (null != socket) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 从网络上获取数据，并存储到数据库中
     *
     * @return
     */
    private void handle() {
        String url = DATA_SOURCE_URL_PREFIX_SOHU + etfInfoCodePrefix
                + code + "&start=" + beginDate + "&end=" + endDate + "&stat=1&order=D&period=d";

        // 从网络上获取ETF数据
        List<EtfTransactionData> etfTransactionDataList = etfTransactionDataService.readEtfTransactionDataList_sohu(url, code, lastClosePrice);
        // 更新etf_transaction_data表中的数据
        etfTransactionDataService.writeEtfTransactionDataList(etfTransactionDataList);

        logger.debug(url);

        AbstractThread.etfTransactionDataCollectFinishAmount++;
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
