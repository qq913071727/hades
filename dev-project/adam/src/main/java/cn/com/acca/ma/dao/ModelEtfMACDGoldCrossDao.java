package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexMACDGoldCross;

import java.util.List;

public interface ModelEtfMACDGoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用日线级别MACD金叉模型算法，向表MDL_ETF_MACD_GOLD_CROSS插入数据
     *
     * @param validateMA120NotDecreasing
     * @param validateMA250NotDecreasing
     * @param defaultBeginDate
     * @param endDate
     * @param type
     */
    void writeModelEtfMACDGoldCross(Integer validateMA120NotDecreasing, Integer validateMA250NotDecreasing, String defaultBeginDate,
                                    String endDate, Integer type);

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和etf代码，从表mdl_stock_index_macd_g_c中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
//    List<ModelStockIndexMACDGoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
