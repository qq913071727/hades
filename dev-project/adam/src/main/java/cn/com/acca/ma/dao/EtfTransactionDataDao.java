package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.EtfTransactionData;

import java.util.Date;
import java.util.List;

public interface EtfTransactionDataDao extends BaseDao {

    /**
     * 查找所有ETF某一日的前收盘
     * @param date
     * @return
     */
    List findLastClosePrice(Date date);

    /**
     * 将从网络中（或.json文件中）读取的数据写入到表ETF_TRANSACTION_DATA中。
     * @param etfTransactionDataList
     */
    void writeEtfTransactionDataList(List<EtfTransactionData> etfTransactionDataList);

    /*********************************************************************************************************************
     *
     * 									                             更新每天的数据
     *
     *********************************************************************************************************************/
    /**
     * 根据日期date，计算某一日所有ETF的移动平均线数据
     * @param date
     */
    void writeEtfTransactionDataMAByDate(String date);

    /**
     * 根据日期date，计算某一日所有ETF的Hei Kin Ashi数据
     * @param date
     */
    void writeEtfTransactionDataHeiKinAshiByDate(String date);

    /**
     * 根据日期date，计算所有ETF某一日的乖离率
     * @param date
     */
    void writeEtfTransactionDataBiasByDate(String date);

    /**
     * 更新表ETF_TRANSACTION_DATA中某一日的EMA15，EMA26，DIF和DEA字段
     * @param date
     */
    void writeEtfTransactionDataMACDByDate(String date);

    /**
     * 更新表ETF_TRANSACTION_DATA中某一日的RSV，K和D字段
     * @param date
     */
    void writeEtfTransactionDataKDByDate(String date);

    /**
     * 更新表ETF_TRANSACTION_DATA中某段时间的correlation***_with_avg_c_p字段
     * @param beginDate
     * @param endDate
     */
    void writeEtfTransactionDataCorrelationByDate(String beginDate, String endDate);

    /*********************************************************************************************************************
     *
     * 									               更新全部的数据
     *
     *********************************************************************************************************************/
    /**
     * 更新last_close_price字段
     */
    void writeLastClosePrice();
    /**
     * 计算5日均线
     */
    void writeEtfTransactionDataMA5();

    /**
     * 计算10日均线
     */
    void writeEtfTransactionDataMA10();

    /**
     * 计算20日均线
     */
    void writeEtfTransactionDataMA20();

    /**
     * 计算60日均线
     */
    void writeEtfTransactionDataMA60();

    /**
     * 计算120日均线
     */
    void writeEtfTransactionDataMA120();

    /**
     * 计算250日均线
     */
    void writeEtfTransactionDataMA250();

    /**
     * 计算所有ETF的乖离率
     */
    void writeEtfTransactionDataBias();

    /**
     * 计算Hei Kin Ashi相关的字段
     */
    void writeEtfTransactionDataHeiKinAshi();

    /**
     * 计算所有ETF的MACD
     */
    void writeEtfTransactionDataMACD();

    /**
     * 计算所有ETF的KD
     */
    void writeEtfTransactionDataKD();

    /**
     * 计算所有ETF的correlation5_with***字段
     */
    void writeEtfTransactionDataCorrelation();

    /*********************************************************************************************************************
     *
     * 									                 Robot6Main类过滤股票
     *
     *********************************************************************************************************************/
    /**
     * 根据开始日期和结束日期，查找中间的日期，并去重
     * @param beginDate
     * @param endDate
     * @return
     */
    List<String> findDistinctDateBetweenBeginDateAndEndDate(String beginDate, String endDate);

    /**
     * 查找日期date的etf的代码，并且其最小日期小于minDate
     * @param minDate
     * @param date
     * @return
     */
    List<String> findDistinctDateByDateAndLessThanMinDate(String minDate, String date);

    /**
     * 查找日期date之前的最近一个交易日的日期
     * @param date
     * @return
     */
    String findLatestDate(String date);

    /**
     * 查找日期date之后的最近一个交易日的日期
     * @param date
     * @return
     */
    String findNextDate(String date);

    /**************************************** 根据每日数据更新图表 *************************************************************/
    /**
     * 获取某一段时间内平均价格中的最大价格和最小价格
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getMaxMinAverageCloseWeek(boolean multithreading, String beginDate, String endDate);

    /**
     * 获取从开始日期至结束日期，每个交易日所有ETF的平均价格（所有ETF的收盘价之和/参加交易的ETF的数量）
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    @SuppressWarnings("rawtypes")
    List getAverageCloseWithDate(boolean multithreading, String beginDate, String endDate);
}
