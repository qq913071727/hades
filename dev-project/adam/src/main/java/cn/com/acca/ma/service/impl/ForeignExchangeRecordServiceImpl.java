package cn.com.acca.ma.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.List;

import com.csvreader.CsvReader;

import cn.com.acca.ma.common.util.CurrencyPartUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.common.util.StringUtil;
import cn.com.acca.ma.model.ForeignExchangeRecord;
import cn.com.acca.ma.service.ForeignExchangeRecordService;

@SuppressWarnings("rawtypes")
public class ForeignExchangeRecordServiceImpl extends BaseServiceImpl<ForeignExchangeRecordServiceImpl,ForeignExchangeRecord> implements ForeignExchangeRecordService {

	/**
	 * 向FOREIGN_EXCHANGE表中插入数据
	 */
	@SuppressWarnings("deprecation")
	public void insertForeignExchangeRecord() {
		String currencies=PropertiesUtil.getValue(FOREIGN_EXCHANGE_PROPERTIES, "foreign_exchange.currencies");
		String[] currencyArray=currencies.split(",");
		for(String cur:currencyArray){
			File path=new File(FOREIGN_EXCHANGE_DATA_PATH+"/"+StringUtil.removeBracket(cur));
			File[] file=path.listFiles();
			for(int i=0;i<file.length;i++){
				try {
					CsvReader reader = new CsvReader(file[i].getAbsolutePath(),',',Charset.forName("UTF-8"));
					reader.readHeaders(); // 跳过表头,如果需要表头的话,不要写这句。
					
					while(reader.readRecord()){ //逐行读入除表头的数据
						String[] value=reader.getValues();
						ForeignExchangeRecord foreignExchangeRecord=new ForeignExchangeRecord();
						
						// value表示日期和时间，如01.01.2004 08:00:00.000
						String dates=value[0].split(" ")[0];			//日期（日.月.年），如29.03.2013
						String time=value[0].split(" ")[1];				//时间（小时:分钟:秒:毫秒），如23:00:00.000

						String date=dates.split("\\.")[0];				//日
						String month=dates.split("\\.")[1];				//月
						String year=dates.split("\\.")[2];				//年
						String hour=time.split(":")[0];					//小时
						String minute=time.split(":")[1];				//分钟
						String second=time.split(":")[2].split("\\.")[0];	//秒
						String nano=time.split(":")[2].split("\\.")[1];	//毫秒
						
						Timestamp dateTime=new Timestamp(Integer.parseInt(year)-1900,Integer.parseInt(month)-1,Integer.parseInt(date),
								Integer.parseInt(hour),Integer.parseInt(minute),Integer.parseInt(second),Integer.parseInt(nano));
						foreignExchangeRecord.setDateTime(dateTime);
						foreignExchangeRecord.setCurrency(StringUtil.removeBracket(cur));
						foreignExchangeRecord.setDateTime(dateTime);
						foreignExchangeRecord.setkLevel(CurrencyPartUtil.getFileMark(file[i].getName()));
						foreignExchangeRecord.setOpen(new BigDecimal(value[1]));
						foreignExchangeRecord.setHigh(new BigDecimal(value[2]));
						foreignExchangeRecord.setLow(new BigDecimal(value[3]));
						foreignExchangeRecord.setClose(new BigDecimal(value[4]));
						foreignExchangeRecord.setVolume(new BigDecimal(value[5]));
						
						logger.info(file[i].getName());
						logger.info(foreignExchangeRecord.getCurrency());
						logger.info(foreignExchangeRecord.getDateTime());
						logger.info(foreignExchangeRecord.getkLevel());
						logger.info(file[i].getName());
						
						if(false==foreignExchangeRecordDao.containForeignExchangeRecord(foreignExchangeRecord)){
							logger.info("*************************************");
							foreignExchangeRecordDao.insertForeignExchangeRecord(foreignExchangeRecord);
						}
					}
					reader.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的简单移动平均线
	 */
	public void calculateMovingAverage(){
		foreignExchangeRecordDao.calculateFiveMovingAverage();
		foreignExchangeRecordDao.calculateTenMovingAverage();
		foreignExchangeRecordDao.calculateTwentyMovingAverage();
		foreignExchangeRecordDao.calculateSixtyMovingAverage();
		foreignExchangeRecordDao.calculateOneHundredTwentyMovingAverage();
		foreignExchangeRecordDao.calculateTwoHundredFiftyMovingAverage();
	}

	@Override
	public String listToString(List list) {
		// TODO Auto-generated method stub
		return null;
	}

}
