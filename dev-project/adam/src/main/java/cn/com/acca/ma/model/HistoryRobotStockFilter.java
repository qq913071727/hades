package cn.com.acca.ma.model;

import lombok.Data;

/**
 * 机器人--股票过滤表（历史数据）
 */
@Data
public class HistoryRobotStockFilter {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 模型id
     */
    private Integer modelId;

}
