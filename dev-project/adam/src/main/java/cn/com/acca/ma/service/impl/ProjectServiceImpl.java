package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.*;
import cn.com.acca.ma.enumeration.TableName;
import cn.com.acca.ma.model.*;
import cn.com.acca.ma.service.ProjectService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class ProjectServiceImpl extends BaseServiceImpl<ProjectServiceImpl, StockMovingAverage> implements ProjectService {
	
	/*****************************************************************************************************************
	 * 
	 * 									设置代理服务器的IP和port
	 * 
	 ******************************************************************************************************************/
	/**
	 * 如果程序是在公司环境运行，则设置代理服务器；否则直接跳过
	 */
	@SuppressWarnings("rawtypes")
	public void setProxyProperties(){
		// 如果是在家中环境运行，则直接返回
		if(this.isHomeComputer()==true){
			return;
		}
		
		logger.info("开始设置代理服务器的IP和端口......");
		
		// IP
		String proxyHost=null;
		// port
		String proxyPort=null;
		
		try {
			// 获取IP和端口
			SAXReader saxReader = new SAXReader();
		    Document document = saxReader.read(new File(STOCK_RECORD_XML));
			Element rootElt = document.getRootElement();
			for(Iterator it=rootElt.elementIterator();it.hasNext();){     
		        Element element = (Element) it.next();
		        if(element.attribute("id").getValue().equals("proxyProperties")){
		        	for(Iterator iterator=element.elementIterator();iterator.hasNext();){ 
			        	Element elt=(Element) iterator.next();
			        	if(elt.attributeValue("key").equals("http.proxyHost")){
			        		proxyHost=elt.attributeValue("value");
			        		logger.info("代理服务器的IP为：" + proxyHost);
			        	}
			        	if(elt.attributeValue("key").equals("http.proxyPort")){
			        		proxyPort=elt.attributeValue("value");
			        		logger.info("代理服务器的端口为：" + proxyPort);
			        	}
		        	}
		        }
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		// 设置http代理
		System.getProperties().setProperty("socksProxySet", "true");
		System.getProperties().setProperty("http.proxyHost", proxyHost);
		System.getProperties().setProperty("http.proxyPort", proxyPort);
	}
	
	/**
	 * 判断程序是在家里运行，还是在公司环境运行。 默认是在家中运行。
	 */
	@SuppressWarnings("rawtypes")
	public boolean isHomeComputer(){
		logger.info("开始判断是在家中环境，还是公司环境......");
		
		InetAddress inetAddress=null;
		// 公司电脑的IP
		String companyIP=null;
		// 当前的IP
		String currentIP=null;
		
		try {
			// 获取当前的IP
			inetAddress = InetAddress.getLocalHost();
			currentIP=inetAddress.getHostAddress().toString();
			logger.info("本地的IP为：" + currentIP);
		
			// 获取公司电脑的IP
			SAXReader saxReader = new SAXReader();
		    Document document = saxReader.read(new File(STOCK_RECORD_XML));
			Element rootElement = document.getRootElement();
			for(Iterator it=rootElement.elementIterator();it.hasNext();){     
		        Element element = (Element) it.next();
		        if(element.attribute("id").getValue().equals("proxyProperties")){
		        	for(Iterator iterator=element.elementIterator();iterator.hasNext();){ 
			        	Element elt=(Element) iterator.next();
			        	if(elt.attributeValue("key").equals("companyIP")){
			        		companyIP=elt.attributeValue("value");
			        		logger.info("公司环境的IP为：" + companyIP);
			        	}
		        	}
		        }
			}
			
			// 判断现在程序是在家运行，还是在公司运行
			if(currentIP.equals(companyIP)){
				// 在公司环境运行
				logger.info("程序是在公司环境运行");
				return false;
			}else{
				// 在家中的环境运行
				logger.info("程序是在家中环境运行");
				return true;
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		logger.info("程序是在家中环境运行");
		return true;
	}
	
	/*****************************************************************************************************************
	 * 
	 * 										收集数据库统计信息 
	 * 
	 * ****************************************************************************************************************/
	/**
	 * 如果当天是星期五，则收集数据库统计信息
	 * 收集数据库统计信息
	 */
	public void gatherDatabaseStatistics(){

//		if (DateUtil.dayForWeek(new Date()).equals(Calendar.FRIDAY)){
//			logger.info("今天是星期五，开始收集数据库的统计信息......");
//
//			projectDao.gatherDatabaseStatistics();
//		}
//		logger.info("今天不是星期五，不收集数据库的统计信息");

		logger.info("开始收集数据库的统计信息");
		projectDao.gatherDatabaseStatistics();
	}
	
	/*********************************************************************************************************************
	 * 
	 * 									                          备份项目代码
	 * 
	 *********************************************************************************************************************/
	/**
	 * 备份项目代码，并删除备份目录下的target目录
	 */
	public void backupCode(){
		logger.info("开始备份项目代码");
		
		String targetDir=CODE_BACKUP_DIR+"adam_"+DateUtil.getYearFromDate(new Date())+"_"+DateUtil.getMonthFromDate(new Date())+"_"+DateUtil.getDateFromDate(new Date());
		new File(targetDir).mkdir();
		try {
			FileUtils.copyDirectory(new File(DIR), new File(targetDir));
			FileUtils.deleteDirectory(new File(targetDir+"/target"));

			logger.info("代码备份完成。备份路径为：" + targetDir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// 旧的代码，现在已经不再使用
//		String targetDir=BACKUP_DIR+"adam_"+DateUtil.getYearFromDate(new Date())+"_"+DateUtil.getMonthFromDate(new Date())+"_"+DateUtil.getDateFromDate(new Date());
//		new File(targetDir).mkdir();
//		logger.info("cmd /c start xcopy /S /D "+DIR+" "+targetDir);
//		try {
//			Runtime.getRuntime().exec("cmd /c start xcopy /S /D "+DIR+" "+targetDir);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	/*********************************************************************************************************************
	 *
	 * 									            使操作系统进入睡眠状态
	 *
	 *********************************************************************************************************************/
	/**
	 * 使操作系统进入睡眠状态
	 */
	@Override
	public void makeOperationSystemSleep() {
		logger.info("开始使操作系统进入睡眠状态");

		try {
			Runtime.getRuntime().exec("rundll32.exe powrprof.dll,SetSuspendState");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		logger.info("使操作系统进入睡眠状态完成");
	}
	/*********************************************************************************************************************
	 * 
	 * 									                          备份数据库/表 （海量和增量）
	 * 
	 *********************************************************************************************************************/
	/**
	 * 海量备份数据库，每次只备份一部分
	 */
	public void backupDatabaseMass() {
		logger.info("海量备份数据库，每次只备份一部分，将其导出为.dmp文件");

		String[] tableNameArray = PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.tables")
			.replace("[", "").replace("]", "").split(",");
		String oversizeTables = PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.tables.oversizeTables")
				.replace("[", "").replace("]", "").trim();

		// 大小正常的dmp文件
		if (null != tableNameArray && tableNameArray.length > 0) {
			for (int i = 0; i < tableNameArray.length; i++) {
				if (StringUtils.isNotEmpty(tableNameArray[i].trim())){
					String tableName = tableNameArray[i].trim();
					String CMD = "exp scott/tiger@ADAM file=" + DUMP_BACKUP_DIR + tableName
							+ ".dmp log=" + tableName + ".log tables="
							+ tableName;
					try {
						Runtime.getRuntime().exec("cmd /c start " + CMD);
						logger.info("海量备份数据库完成。备份命令：" + CMD);
						Thread.sleep(30000);
					} catch (IOException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		// 过大的dmp文件
		try {
			Runtime.getRuntime().exec("cmd /c start " + oversizeTables);
			logger.info("海量备份数据库完成。备份命令：" + oversizeTables);
			Thread.sleep(30000);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 增量备份数据库的各个表
	 * 注意：由于各个表的表结构不同，在备份和恢复时，也是采取的不同策略。
	 * 每个方法的注解中都详细地写清楚了备份和恢复的策略，因此，需要仔细阅读每个方法的注解才能了解具体的策略。
	 * @param multithreading
	 */
	public void backupDatabaseIncrementalByJson(boolean multithreading){
		logger.info("开始使用json增量备份数据库");

		String tableNames=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.table_name");
		String dayBeginDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.day.begin_date");
		String dayEndDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.day.end_date");
		String weekBeginDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.week.begin_date");
		String weekEndDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.week.end_date");
		String monthBeginDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.month.begin_date");
		String monthEndDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "backup.database.month.end_date");

		String[] tableNameArray=tableNames.replace("[", "").replace("]", "").replace(" ", "").split(",");
		for(int i=0;i<tableNameArray.length;i++){
			if(TableName.STOCK_TRANSACTION_DATA.getName().toString().equals(tableNameArray[i])){
				// 增量备份表STOCK_TRANSACTION_DATA
				this.backupStockTransactionDataIncrementalByJson(multithreading, dayBeginDate, dayEndDate);
			}
			if(TableName.STOCK_INDEX.getName().trim().equals(tableNameArray[i])){
				// 增量备份表STOCK_INDEX
				this.backupStockIndexIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.BOARD_INDEX.getName().trim().equals(tableNameArray[i])){
				// 增量备份表BOARD_INDEX
				this.backupBoardIndexIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.REPORT.getName().trim().equals(tableNameArray[i])){
				// 增量备份表REPORT
				this.backupReportIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.STOCK_WEEK.getName().trim().equals(tableNameArray[i])){
				// 增量备份表STOCK_WEEK
				this.backupStockWeekIncrementalByJson(weekBeginDate, weekEndDate);
			}
			if(TableName.STOCK_INDEX_WEEK.getName().trim().equals(tableNameArray[i])){
				// 增量备份表STOCK_INDEX_WEEK
				this.backupStockIndexWeekIncrementalByJson(weekBeginDate, weekEndDate);
			}

			if(TableName.MDL_ALL_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_ALL_GOLD_CROSS
				this.backupModelAllGoldCrossIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.MDL_CLOSE_PRICE_MA5_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_CLOSE_PRICE_MA5_GOLD_CROSS
				this.backupModelClosePriceMA5GoldCrossIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.MDL_CLOSE_PRICE_MA5_DEAD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_CLOSE_PRICE_MA5_DEAD_CROSS
				this.backupModelClosePriceMA5DeadCrossIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.MDL_MACD_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_MACD_GOLD_CROSS
				this.backupModelMACDGoldCrossIncrementalByJson(dayBeginDate,dayEndDate);
			}
			if(TableName.MDL_MACD_DEAD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_MACD_DEAD_CROSS
				this.backupModelMACDDeadCrossIncrementalByJson(dayBeginDate,dayEndDate);
			}
			if(TableName.MDL_HEI_KIN_ASHI_UP_DOWN.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_HEI_KIN_ASHI_UP_DOWN
				this.backupModelHeiKinAshiUpDownIncrementalByJson(dayBeginDate,dayEndDate);
			}
			if(TableName.MDL_HEI_KIN_ASHI_DOWN_UP.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_HEI_KIN_ASHI_DOWN_UP
				this.backupModelHeiKinAshiDownUpIncrementalByJson(dayBeginDate,dayEndDate);
			}
			if (TableName.MDL_KD_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_KD_GOLD_CROSS
				this.backupModelKDGoldCrossIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if (TableName.MDL_KD_DEAD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_KD_DEAD_CROSS
				this.backupModelKDDeadCrossIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if (TableName.MDL_TOP_STOCK.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_TOP_STOCK
				this.backupModelTopStockIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if (TableName.MDL_TOP_STOCK_DETAIL.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_TOP_STOCK_DETAIL
				this.backupModelTopStockDetailIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if (TableName.MDL_STOCK_ANALYSIS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_STOCK_ANALYSIS
				this.backupModelStockAnalysisIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if (TableName.MDL_STOCK_MONTH_ANALYSIS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_STOCK_MONTH_ANALYSIS
				this.backupModelStockMonthAnalysisIncrementalByJson(monthBeginDate, monthEndDate);
			}
			if (TableName.STOCK_MONTH.getName().trim().equals(tableNameArray[i])){
				// 增量备份表STOCK_MONTH
				this.backupStockMonthIncrementalByJson(monthBeginDate, monthEndDate);
			}

			if (TableName.MDL_WEEK_KD_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MDL_WEEK_KD_GOLD_CROSS
				this.backupModelWeekKDGoldCrossIncrementalByJson(weekBeginDate, weekEndDate);
			}

			if(TableName.STOCK_INFO.getName().trim().equals(tableNameArray[i])){
				// 增量备份表STOCK_INFO
				this.backupStockInfoIncrementalByJson();
			}
			if(TableName.BOARD.getName().trim().equals(tableNameArray[i])){
				// 增量备份表BOARD（其实是一次全部备份）
				this.backupBoardIncrementalByJson();
			}
			if(TableName.MODEL.getName().trim().equals(tableNameArray[i])){
				// 增量备份表MODEL
				this.backupModelIncrementalByJson();
			}
			if(TableName.FOREIGN_EXCHANGE.getName().trim().equals(tableNameArray[i])){
				// 增量备份表FOREIGN_EXCHANGE（其实是一次全部备份）
				this.backupForeignExchangeIncrementalByJson();
			}
			if(TableName.FOREIGN_EXCHANGE_RECORD.getName().trim().equals(tableNameArray[i])){
				// 增量备份表FOREIGN_EXCHANGE_RECORD
				this.backupForeignExchangeRecord(dayBeginDate,dayEndDate);
			}

			if(TableName.C_F_DATE_CONTRACT_DATA.getName().trim().equals(tableNameArray[i])){
				// 增量备份表C_F_DATE_CONTRACT_DATA
				this.backupCommodityFutureDateContractDataRecord(dayBeginDate,dayEndDate);
			}
		}

		logger.info("使用json增量备份数据库结束");
	}

	/**
	 * 增量备份表STOCK_TRANSACTION_DATA
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 */
	public void backupStockTransactionDataIncrementalByJson(boolean multithreading, String beginDate,String endDate){
		logger.info("开始增量备份表STOCK_TRANSACTION_DATA，时间为【" + beginDate + "】至【" + endDate + "】");
		
		// 获取从开始时间beginDate到结束时间endDate内，STOCK_TRANSACTION_DATA表中的交易日期
		List<String> dateList=stockTransactionDataDao.getDateByCondition(multithreading, beginDate, endDate, false);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日STOCK_TRANSACTION_DATA表中的所有交易记录
			List<StockTransactionData> stockList=stockTransactionDataDao.getStocksByDate(dateList.get(i));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.STOCK_TRANSACTION_DATA+"\\"+TableName.STOCK_TRANSACTION_DATA+"-"+DateUtil.dateToString(stockList.get(0).getDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter(file, true);
			} catch (IOException e2) {
				e2.printStackTrace();
			}	
			// 向json文件中写入对象
			for(int j=0;j<stockList.size();j++){
				try {
					jsonString = objectMapper.writeValueAsString(stockList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				
				fileWriter.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);
			// 删除json文件
			FileUtil.deleteFile(path);
		}
	}
	
	/**
	 * 增量备份表STOCK_WEEK
	 * 每只股票的周线级别的记录的BEGIN_DATE不一定是星期一，END_DATE也不一定是星期五。
	 * 备份时是用END_DATE字段为标准，每个END_DATE字段值相同的为一个zip文件。
	 * 有时多个zip文件一起构成了某一周的所有记录。
	 * @param beginDate
	 * @param endDate
	 */
	public void backupStockWeekIncrementalByJson(String beginDate,String endDate){
		logger.info("开始增量备份表STOCK_WEEK，时间为【" + beginDate + "】至【" + endDate + "】");
		
		// 获取从开始时间beginDate到结束时间endDate内，STOCK_WEEKEND表中的日期
		List<Date> dateList= stockWeekDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日STOCK_WEEK表中的所有记录
			List<StockWeek> stockWeekList= stockWeekDao.getStockWeeksByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.STOCK_WEEK+"\\"+TableName.STOCK_WEEK+"-"+DateUtil.dateToString(((StockWeek)stockWeekList.get(0)).getEndDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter(file, true);
			} catch (IOException e2) {
				e2.printStackTrace();
			}	
			// 向json文件中写入对象
			for(int j=0;j<stockWeekList.size();j++){
				try {
					jsonString = objectMapper.writeValueAsString(stockWeekList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				
				fileWriter.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);
			// 删除json文件
			FileUtil.deleteFile(path);
		}
	}
	
	/**
	 * 增量备份表STOCK_INDEX
	 * @param beginDate
	 * @param endDate
	 */
	public void backupStockIndexIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表STOCK_INDEX，时间为【" + beginDate + "】至【" + endDate + "】");
		
		// 获取从开始时间beginDate到结束时间endDate内，STOCK_INDEX表中的日期
		List<Date> dateList=stockIndexDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日STOCK_INDEX表中的所有记录
			List<StockIndex> stockIndexList=stockIndexDao.getStockIndexByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.STOCK_INDEX+"\\"+TableName.STOCK_INDEX+"-"+DateUtil.dateToString(((StockIndex)stockIndexList.get(0)).getDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter(file, true);

				// 向json文件中写入对象
				for(int j=0;j<stockIndexList.size();j++){
					jsonString = objectMapper.writeValueAsString(stockIndexList.get(j));
					fileWriter.append(jsonString+"\n");
				}
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
		
	}
	
	/**
	 * 增量备份表STOCK_INDEX_WEEK
	 * @param beginDate
	 * @param endDate
	 */
	public void backupStockIndexWeekIncrementalByJson(String beginDate, String endDate){
		logger.info("增量备份表STOCK_INDEX_WEEK，时间为【" + beginDate + "】至【" + endDate + "】");
		
		// 根据开始日期beginDate和结束日期endDate，获取STOCK_INDEX_WEEK表中的所有记录
		List<StockIndexWeek> stockIndexWeekList = stockIndexWeekDao.getStockIndexWeekWithinDate(beginDate, endDate);
		// Json文件的路径和文件名
		String path = new String(JSON_DATA_BACKUP_DIR + TableName.STOCK_INDEX_WEEK + "\\" + TableName.STOCK_INDEX_WEEK + "-" + beginDate + "-" + endDate + ".json");
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		File file = new File(path);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file, true);
			// 向json文件中写入对象
			for (int j = 0; j < stockIndexWeekList.size(); j++) {
				jsonString = objectMapper.writeValueAsString(stockIndexWeekList.get(j));
				fileWriter.append(jsonString + "\n");
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ZipCompressor zipCompressor = new ZipCompressor(path.replace(".json",".zip"));
		// 将json文件压缩为zip文件
		zipCompressor.compress(path);

		// 删除json文件
		FileUtil.deleteFile(path);
		logger.info("删除文件【" + path + "】");
	}

	/**
	 * 增量备份表BOARD_INDEX
	 * @param beginDate
	 * @param endDate
	 */
	public void backupBoardIndexIncrementalByJson(String beginDate, String endDate){
		logger.info("增量备份表BOARD_INDEX，时间为【" + beginDate + "】至【" + endDate + "】");
		
		// 获取从开始时间beginDate到结束时间endDate内，BOARD_INDEX表中的日期
		List<String> dateList=boardIndexDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日BOARD_INDEX表中的所有记录
			List<BoardIndex> boardIndexList=boardIndexDao.getBoardIndexsByDate(dateList.get(i));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.BOARD_INDEX+"\\"+TableName.BOARD_INDEX+"-"+DateUtil.dateToString(boardIndexList.get(0).getDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter(file, true);
				// 向json文件中写入对象
				for(int j=0;j<boardIndexList.size();j++) {
					jsonString = objectMapper.writeValueAsString(boardIndexList.get(j));
					fileWriter.append(jsonString + "\n");
				}
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					fileWriter.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}
		
	/**
	 * 增量备份表REPORT
	 * @param beginDate
	 * @param endDate
	 */
	public void backupReportIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表REPORT，时间为【" + beginDate + "】至【" + endDate + "】");
		
		// 获取从开始时间beginDate到结束时间endDate内，REPORT表中的日期
		List<Date> dateList=reportDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日REPORT表中的所有记录
			List<Report> reportList=reportDao.getReportByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.REPORT+"\\"+TableName.REPORT+"-"+DateUtil.dateToString(reportList.get(0).getReportDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			try {
				fileWriter = new FileWriter(file, true);
				// 向json文件中写入对象
				for(int j=0;j<reportList.size();j++){
					jsonString = objectMapper.writeValueAsString(reportList.get(j));
					fileWriter.append(jsonString+"\n");
				}
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} finally {
				try {
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path +"】");
		}
	}
	
	/**
	 * 增量备份表MDL_MACD_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelMACDGoldCrossIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_MACD_GOLD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_MACD_GOLD_CROSS表中的日期
		List<Date> dateList=modelMACDGoldCrossDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_MACD_GOLD_CROSS表中的所有记录
			List<ModelMACDGoldCross> modelMACDGoldCrossList=modelMACDGoldCrossDao.getModelMACDGoldCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_MACD_GOLD_CROSS+"\\"+TableName.MDL_MACD_GOLD_CROSS+"-"+DateUtil.dateToString(modelMACDGoldCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelMACDGoldCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelMACDGoldCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_MACD_DEAD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelMACDDeadCrossIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_MACD_DEAD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_MACD_DEAD_CROSS表中的日期
		List<Date> dateList=modelMACDDeadCrossDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_MACD_DEAD_CROSS表中的所有记录
			List<ModelMACDDeadCross> modelMACDDeadCrossList=modelMACDDeadCrossDao.getModelMACDDeadCrossByDate((DateUtil.dateToString(dateList.get(i))));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_MACD_DEAD_CROSS+"\\"+TableName.MDL_MACD_DEAD_CROSS+"-"+DateUtil.dateToString(modelMACDDeadCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelMACDDeadCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelMACDDeadCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_HEI_KIN_ASHI_UP_DOWN
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelHeiKinAshiUpDownIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_HEI_KIN_ASHI_UP_DOWN，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_HEI_KIN_ASHI_UP_DOWN表中的日期
		List<Date> dateList=modelHeiKinAshiUpDownDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_HEI_KIN_ASHI_UP_DOWN表中的所有记录
			List<ModelHeiKinAshiUpDown> modelHeiKinAshiUpDownList=modelHeiKinAshiUpDownDao.getModelHeiKinAshiUpDownByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_HEI_KIN_ASHI_UP_DOWN+"\\"+TableName.MDL_HEI_KIN_ASHI_UP_DOWN+"-"+DateUtil.dateToString(modelHeiKinAshiUpDownList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelHeiKinAshiUpDownList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelHeiKinAshiUpDownList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_HEI_KIN_ASHI_DOWN_UP
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelHeiKinAshiDownUpIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_HEI_KIN_ASHI_DOWN_UP，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_HEI_KIN_ASHI_DOWN_UP表中的日期
		List<Date> dateList=modelHeiKinAshiDownUpDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_HEI_KIN_ASHI_DOWN_UP表中的所有记录
			List<ModelHeiKinAshiDownUp> modelHeiKinAshiDownUpList=modelHeiKinAshiDownUpDao.getModelHeiKinAshiDownUpByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_HEI_KIN_ASHI_DOWN_UP+"\\"+TableName.MDL_HEI_KIN_ASHI_DOWN_UP+"-"+DateUtil.dateToString(modelHeiKinAshiDownUpList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelHeiKinAshiDownUpList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelHeiKinAshiDownUpList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_TOP_STOCK
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelTopStockIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_TOP_STOCK，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_TOP_STOCK表中的日期
		List<Date> dateList=modelTopStockDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_TOP_STOCK表中的所有记录
			List<ModelTopStock> modelTopStockList=modelTopStockDao.getModelTopStockByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_TOP_STOCK+"\\"+TableName.MDL_TOP_STOCK+"-"+DateUtil.dateToString(modelTopStockList.get(0).getDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelTopStockList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelTopStockList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_TOP_STOCK_DETAIL
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelTopStockDetailIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_TOP_STOCK_DETAIL，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_TOP_STOCK_DETAIL表中的日期
		List<Date> dateList=modelTopStockDetailDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_TOP_STOCK_DETAIL表中的所有记录
			List<ModelTopStockDetail> modelTopStockDetailList=modelTopStockDetailDao.getModelTopStockDetailByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_TOP_STOCK_DETAIL+"\\"+TableName.MDL_TOP_STOCK_DETAIL+"-"+DateUtil.dateToString(modelTopStockDetailList.get(0).getDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelTopStockDetailList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelTopStockDetailList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_STOCK_ANALYSIS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelStockAnalysisIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_STOCK_ANALYSIS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_TOP_STOCK表中的日期
		List<Date> dateList = modelStockAnalysisDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_STOCK_ANALYSIS表中的所有记录
			List<ModelStockAnalysis> modelStockAnalysisList= modelStockAnalysisDao.getModelStockAnalysisByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_STOCK_ANALYSIS+"\\"+TableName.MDL_STOCK_ANALYSIS+"-"+DateUtil.dateToString(modelStockAnalysisList.get(0).getDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelStockAnalysisList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelStockAnalysisList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_STOCK_MONTH_ANALYSIS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelStockMonthAnalysisIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_STOCK_MONTH_ANALYSIS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_STOCK_MONTH_ANALYSIS表中的日期
		List<Date> dateList = modelStockMonthAnalysisDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_STOCK_MONTH_ANALYSIS表中的所有记录
			List<ModelStockMonthAnalysis> modelStockMonthAnalysisList= modelStockMonthAnalysisDao.getModelStockMonthAnalysisByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_STOCK_MONTH_ANALYSIS+"\\"+TableName.MDL_STOCK_MONTH_ANALYSIS+"-"+DateUtil.dateToString(modelStockMonthAnalysisList.get(0).getEndDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelStockMonthAnalysisList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelStockMonthAnalysisList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表STOCK_MONTH
	 * @param beginDate
	 * @param endDate
	 */
	public void backupStockMonthIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表STOCK_MONTH，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，STOCK_MONTH表中的日期
		List<Date> dateList = stockMonthDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日STOCK_MONTH表中的所有记录
			List<StockMonth> stockMonthList= stockMonthDao.getStockMonthByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.STOCK_MONTH+"\\"+TableName.STOCK_MONTH+"-"+DateUtil.dateToString(stockMonthList.get(0).getEndDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<stockMonthList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(stockMonthList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_WEEK_KD_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelWeekKDGoldCrossIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_WEEK_KD_GOLD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_WEEK_KD_GOLD_CROSS表中的日期
		List<Date> dateList = modelWeekKDGoldCrossDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_WEEK_KD_GOLD_CROSS表中的所有记录
			List<ModelWeekKDGoldCross> modelWeekKDGoldCrossList = modelWeekKDGoldCrossDao.getModelWeekKDGoldCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_WEEK_KD_GOLD_CROSS+"\\"+TableName.MDL_WEEK_KD_GOLD_CROSS+"-"+DateUtil.dateToString(modelWeekKDGoldCrossList.get(0).getSellEndDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelWeekKDGoldCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelWeekKDGoldCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_KD_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelKDGoldCrossIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_KD_GOLD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_KD_GOLD_CROSS表中的日期
		List<Date> dateList = modelKDGoldCrossDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_KD_GOLD_CROSS表中的所有记录
			List<ModelKDGoldCross> modelKDGoldCrossList = modelKDGoldCrossDao.getModelKDGoldCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_KD_GOLD_CROSS+"\\"+TableName.MDL_KD_GOLD_CROSS+"-"+DateUtil.dateToString(modelKDGoldCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelKDGoldCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelKDGoldCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_KD_DEAD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelKDDeadCrossIncrementalByJson(String beginDate,String endDate){
		logger.info("增量备份表MDL_KD_DEAD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_KD_DEAD_CROSS表中的日期
		List<Date> dateList = modelKDDeadCrossDao.getDateByCondition(beginDate, endDate);
		for(int i=0;i<dateList.size();i++){
			// 根据交易日期，获取那一日MDL_KD_GOLD_CROSS表中的所有记录
			List<ModelKDDeadCross> modelKDDeadCrossList = modelKDDeadCrossDao.getModelKDDeadCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_KD_DEAD_CROSS+"\\"+TableName.MDL_KD_DEAD_CROSS+"-"+DateUtil.dateToString(modelKDDeadCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelKDDeadCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelKDDeadCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}
	
	/**
	 * 增量备份表FOREIGN_EXCHANGE_RECORD
	 * @param beginDate
	 * @param endDate
	 */
	public void backupForeignExchangeRecord(String beginDate,String endDate){
		logger.info("增量备份表FOREIGN_EXCHANGE_RECORD，时间为【" + beginDate + "】至【" + endDate +"】");
		
		// 获取从开始时间beginDate到结束时间endDate内，FOREIGN_EXCHANGE_RECORD表中的日期
		List<ForeignExchangeRecord> foreignExchangeRecordTimestampList=foreignExchangeRecordDao.getDateByCondition(beginDate, endDate);
		// List<ForeignExchangeRecord>数组中的每个对象只有一个有效字段dateTime，当截断年月日时会有重复，因此要去掉重复的，最后再以List<Date>类型返回。
		List<Date>dateTimeList=DateUtil.timestampToDateWithoutDuplicate(foreignExchangeRecordTimestampList);
		for(int i=0;i<dateTimeList.size();i++){
			// 根据交易日期，获取那一日FOREIGN_EXCHANGE_RECORD表中的所有记录
			List<ForeignExchangeRecord> foreignExchangeRecordList=foreignExchangeRecordDao.getForeignExchangeRecordByDate(DateUtil.dateToString(dateTimeList.get(i)));
			if(foreignExchangeRecordList.size()!=0){
				// Json文件的路径和文件名
				String fileNamePart=DateUtil.dateToString(foreignExchangeRecordList.get(0).getDateTime()).replace(":", "-").replace(".", "-");
				String path=new String(JSON_DATA_BACKUP_DIR + TableName.FOREIGN_EXCHANGE_RECORD+"\\"+TableName.FOREIGN_EXCHANGE_RECORD+"-"+fileNamePart+".json");
				String jsonString=null;
				ObjectMapper objectMapper=new ObjectMapper();
				File file = new File(path);
				FileWriter fileWriter = null;
				try {
					fileWriter = new FileWriter(file, true);
				} catch (IOException e2) {
					e2.printStackTrace();
				}	
				// 向json文件中写入对象
				for(int j=0;j<foreignExchangeRecordList.size();j++){
					try {
						jsonString = objectMapper.writeValueAsString(foreignExchangeRecordList.get(j));
						fileWriter.append(jsonString+"\n");
					} catch (JsonGenerationException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				try {
					
					fileWriter.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
				// 将json文件压缩为zip文件
				zipCompressor.compress(path);
				// 删除json文件
				FileUtil.deleteFile(path);
			}
		}
		
		logger.info("backup table FOREIGN_EXCHANGE_RECORD incremental by json finish");
	}

	/**
	 * C_F_DATE_CONTRACT_DATA
	 * @param beginDate
	 * @param endDate
	 */
	public void backupCommodityFutureDateContractDataRecord(String beginDate,String endDate){
		logger.info("增量备份表C_F_DATE_CONTRACT_DATA，时间为【" + beginDate + "】至【" + endDate +"】");

		// 获取从开始时间beginDate到结束时间endDate内，C_F_DATE_CONTRACT_DATA表中的日期
		List<CommodityFutureDateContractData> commodityFutureDateContractDataList=commodityFutureDateContractDataDao.getDateByCondition(beginDate, endDate);
		// List<CommodityFutureDateContractData>数组中的每个对象只有一个有效字段transactionDate，当截断年月日时会有重复，因此要去掉重复的，最后再以List<Date>类型返回。
		List<Date>dateTimeList=DateUtil.timestampToTransactionDateWithoutDuplicate(commodityFutureDateContractDataList);
		for(int i=0;i<dateTimeList.size();i++){
			// 根据交易日期，获取那一日C_F_DATE_CONTRACT_DATA表中的所有记录
			List<CommodityFutureDateContractData> _commodityFutureDateContractDataList=commodityFutureDateContractDataDao.getCommodityFutureDateContractDataByDate(DateUtil.dateToString(dateTimeList.get(i)));
			if(_commodityFutureDateContractDataList.size()!=0){
				// Json文件的路径和文件名
				String fileNamePart=DateUtil.dateToString(_commodityFutureDateContractDataList.get(0).getTransactionDate()).replace(":", "-").replace(".", "-");
				String path=new String(JSON_DATA_BACKUP_DIR + TableName.C_F_DATE_CONTRACT_DATA+"\\"+TableName.C_F_DATE_CONTRACT_DATA+"-"+fileNamePart+".json");
				String jsonString=null;
				ObjectMapper objectMapper=new ObjectMapper();
				File file = new File(path);
				FileWriter fileWriter = null;
				try {
					fileWriter = new FileWriter(file, true);
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				// 向json文件中写入对象
				for(int j=0;j<_commodityFutureDateContractDataList.size();j++){
					try {
						jsonString = objectMapper.writeValueAsString(_commodityFutureDateContractDataList.get(j));
						fileWriter.append(jsonString+"\n");
					} catch (JsonGenerationException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				try {

					fileWriter.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
				// 将json文件压缩为zip文件
				zipCompressor.compress(path);
				// 删除json文件
				FileUtil.deleteFile(path);
			}
		}

		logger.info("backup table C_F_DATE_CONTRACT_DATA incremental by json finish");
	}

	/**
	 * 增量备份表MDL_CLOSE_PRICE_MA5_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelClosePriceMA5GoldCrossIncrementalByJson(String beginDate, String endDate){
		logger.info("增量备份表MDL_CLOSE_PRICE_MA5_GOLD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_CLOSE_PRICE_MA5_GOLD_CROSS表中的日期
		List<Date> dateList = modelClosePriceMA5GoldCrossDao.getDateByCondition(beginDate, endDate);
		for (int i = 0; i < dateList.size(); i++) {
			// 根据交易日期，获取那一日MDL_CLOSE_PRICE_MA5_GOLD_CROSS表中的所有记录
			List<ModelClosePriceMA5GoldCross> modelClosePriceMA5GoldCrossList = modelClosePriceMA5GoldCrossDao.getModelClosePriceMA5GoldCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_CLOSE_PRICE_MA5_GOLD_CROSS+"\\"+TableName.MDL_CLOSE_PRICE_MA5_GOLD_CROSS+"-"+DateUtil.dateToString(modelClosePriceMA5GoldCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelClosePriceMA5GoldCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelClosePriceMA5GoldCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_CLOSE_PRICE_MA5_DEAD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelClosePriceMA5DeadCrossIncrementalByJson(String beginDate, String endDate){
		logger.info("增量备份表MDL_CLOSE_PRICE_MA5_DEAD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_CLOSE_PRICE_MA5_DEAD_CROSS表中的日期
		List<Date> dateList = modelClosePriceMA5DeadCrossDao.getDateByCondition(beginDate, endDate);
		for (int i = 0; i < dateList.size(); i++) {
			// 根据交易日期，获取那一日MDL_CLOSE_PRICE_MA5_DEAD_CROSS表中的所有记录
			List<ModelClosePriceMA5DeadCross> modelClosePriceMA5DeadCrossList = modelClosePriceMA5DeadCrossDao.getModelClosePriceMA5DeadCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_CLOSE_PRICE_MA5_DEAD_CROSS+"\\"+TableName.MDL_CLOSE_PRICE_MA5_DEAD_CROSS+"-"+DateUtil.dateToString(modelClosePriceMA5DeadCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelClosePriceMA5DeadCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelClosePriceMA5DeadCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}

	/**
	 * 增量备份表MDL_ALL_GOLD_CROSS
	 * @param beginDate
	 * @param endDate
	 */
	public void backupModelAllGoldCrossIncrementalByJson(String beginDate, String endDate){
		logger.info("增量备份表MDL_ALL_GOLD_CROSS，时间为【" + beginDate + "】至【" + endDate + "】");

		// 获取从开始时间beginDate到结束时间endDate内，MDL_ALL_GOLD_CROSS表中的日期
		List<Date> dateList = modelAllGoldCrossDao.getDateByCondition(beginDate, endDate);
		for (int i = 0; i < dateList.size(); i++) {
			// 根据交易日期，获取那一日MDL_ALL_GOLD_CROSS表中的所有记录
			List<ModelAllGoldCross> modelAllGoldCrossList = modelAllGoldCrossDao.getModelAllGoldCrossByDate(DateUtil.dateToString(dateList.get(i)));
			// Json文件的路径和文件名
			String path=new String(JSON_DATA_BACKUP_DIR + TableName.MDL_ALL_GOLD_CROSS+"\\"+TableName.MDL_ALL_GOLD_CROSS+"-"+DateUtil.dateToString(modelAllGoldCrossList.get(0).getSellDate())+".json");
			String jsonString=null;
			ObjectMapper objectMapper=new ObjectMapper();
			File file = new File(path);
			FileWriter fileWriter = null;
			// 向json文件中写入对象
			for(int j=0;j<modelAllGoldCrossList.size();j++){
				try {
					fileWriter = new FileWriter(file, true);
					jsonString = objectMapper.writeValueAsString(modelAllGoldCrossList.get(j));
					fileWriter.append(jsonString+"\n");
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						fileWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			ZipCompressor zipCompressor=new ZipCompressor(path.replace(".json", ".zip"));
			// 将json文件压缩为zip文件
			zipCompressor.compress(path);

			// 删除json文件
			FileUtil.deleteFile(path);
			logger.info("删除文件【" + path + "】");
		}
	}
	
	/**
	 * 增量备份表BOARD
	 */
	public void backupBoardIncrementalByJson(){
		logger.info("增量备份表BOARD");
		
		// 获取BOARD表中的所有记录
		List<Board> boardList = boardDao.getAllBoard(Boolean.FALSE);
		// Json文件的路径和文件名
		String path = new String(JSON_DATA_BACKUP_DIR + TableName.BOARD + "\\" + TableName.BOARD + ".json");
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		File file = new File(path);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file, true);
			// 向json文件中写入对象
			for (int j = 0; j < boardList.size(); j++) {
				jsonString = objectMapper.writeValueAsString(boardList.get(j));
				fileWriter.append(jsonString + "\n");
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ZipCompressor zipCompressor = new ZipCompressor(path.replace(".json", ".zip"));
		// 将json文件压缩为zip文件
		zipCompressor.compress(path);

		// 删除json文件
		FileUtil.deleteFile(path);
		logger.info("删除文件【" + path + "】");
	}
	
	/**
	 * 增量备份表STOCK_INFO
	 */
	public void backupStockInfoIncrementalByJson(){
		logger.info("增量备份表STOCK_INFO");
		
		// 获取STOCK_INFO表中的所有记录
		List<StockInfo> stockInfoList=stockInfoDao.getAllStockInfo();
		// Json文件的路径和文件名
		String path = new String(JSON_DATA_BACKUP_DIR + TableName.STOCK_INFO + "\\" + TableName.STOCK_INFO + ".json");
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		File file = new File(path);
		FileWriter fileWriter = null;
		// 向json文件中写入对象
		try {
			fileWriter = new FileWriter(file, true);
			for (int j = 0; j < stockInfoList.size(); j++) {
				jsonString = objectMapper.writeValueAsString(stockInfoList.get(j));
				fileWriter.append(jsonString + "\n");
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ZipCompressor zipCompressor = new ZipCompressor(path.replace(".json", ".zip"));
		// 将json文件压缩为zip文件
		zipCompressor.compress(path);

		// 删除json文件
		FileUtil.deleteFile(path);
		logger.info("删除文件【" + path + "】");
	}
	
	/**
	 * 增量备份表MODEL
	 */
	public void backupModelIncrementalByJson(){
		logger.info("增量备份表MODEL");
		
		// 获取MODEL表中的所有记录
		List<Model> modelList = modelDao.findAll();
		// Json文件的路径和文件名
		String path = new String(JSON_DATA_BACKUP_DIR + TableName.MODEL + "\\" + TableName.MODEL + ".json");
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		File file = new File(path);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file, true);
			// 向json文件中写入对象
			for (int j = 0; j < modelList.size(); j++) {
				jsonString = objectMapper.writeValueAsString(modelList.get(j));
				fileWriter.append(jsonString + "\n");
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ZipCompressor zipCompressor = new ZipCompressor(path.replace(".json", ".zip"));
		// 将json文件压缩为zip文件
		zipCompressor.compress(path);

		// 删除json文件
		FileUtil.deleteFile(path);
		logger.info("删除文件【" + path + "】");
	}
	
	/**
	 * 增量备份表FOREIGN_EXCHANGE
	 */
	public void backupForeignExchangeIncrementalByJson(){
		logger.info("backup table FOREIGN_EXCHANGE incremental by json begin");
		
		// 获取FOREIGN_EXCHANGE表中的所有记录
		List<ForeignExchange> foreignExchangeList=foreignExchangeDao.getAllForeignExchange();
		// Json文件的路径和文件名
		String path = new String(JSON_DATA_BACKUP_DIR + TableName.FOREIGN_EXCHANGE + "\\" + TableName.FOREIGN_EXCHANGE + ".json");
		String jsonString = null;
		ObjectMapper objectMapper = new ObjectMapper();
		File file = new File(path);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file, true);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		// 向json文件中写入对象
		for (int j = 0; j < foreignExchangeList.size(); j++) {
			try {
				jsonString = objectMapper.writeValueAsString(foreignExchangeList.get(j));
				fileWriter.append(jsonString + "\n");
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {

			fileWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		ZipCompressor zipCompressor = new ZipCompressor(path.replace(".json", ".zip"));
		// 将json文件压缩为zip文件
		zipCompressor.compress(path);
		// 删除json文件
		FileUtil.deleteFile(path);
		
		logger.info("backup table FOREIGN_EXCHANGE incremental by json finish");
	}
	
	/*********************************************************************************************************************
	 * 
	 * 									                         使用json文件恢复数据库表
	 * 
	 *********************************************************************************************************************/
	/**
	 * 恢复数据库的各个表中的数据
	 */
	public void restoreTableIncrementalByJson(){
		logger.info("开始恢复数据库的各个表中的数据");
		
		String tableNames=PropertiesUtil.getValue(BACKUP_PROPERTIES, "restore.database.table_name");
		String dayBeginDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "restore.database.day.begin_date");
		String dayEndDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "restore.database.day.end_date");
		String weekBeginDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "restore.database.week.begin_date");
		String weekEndDate=PropertiesUtil.getValue(BACKUP_PROPERTIES, "restore.database.week.end_date");

		String[] tableNameArray=tableNames.replace("[", "").replace("]", "").replace(" ", "").split(",");
		for(int i=0;i<tableNameArray.length;i++){
			if(TableName.STOCK_TRANSACTION_DATA.getName().trim().equals(tableNameArray[i])){
				// 向表STOCK_TRANSACTION_DATA中插入数据
				this.restoreStockTransactionDataIncrementalByJson(dayBeginDate, dayEndDate);
			}
			if(TableName.STOCK_INDEX.getName().trim().equals(tableNameArray[i])){
				// 向表STOCK_INDEX中插入数据
				this.restoreStockIndexIncrementalByJson();
			}
			if(TableName.BOARD_INDEX.getName().trim().equals(tableNameArray[i])){
				// 向表BOARD_INDEX中插入数据
				this.restoreBoardIndexIncrementalByJson();
			}
			if(TableName.REPORT.getName().trim().equals(tableNameArray[i])){
				// 向表REPORT中插入数据
				this.restoreReportIncrementalByJson();
			}
			if(TableName.STOCK_WEEK.getName().trim().equals(tableNameArray[i])){
				// 向表STOCK_WEEK中插入数据
				this.restoreStockWeekIncrementalByJson(weekBeginDate, weekEndDate);
			}
			if(TableName.STOCK_MONTH.getName().trim().equals(tableNameArray[i])){
				// 向表STOCK_MONTH中插入数据
				this.restoreStockMonthIncrementalByJson();
			}
			if(TableName.STOCK_INDEX_WEEK.getName().trim().equals(tableNameArray[i])){
				// 向表STOCK_INDEX_WEEK中插入数据
				this.restoreStockIndexWeekIncrementalByJson();
			}

			if(TableName.MDL_ALL_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_ALL_GOLD_CROSS中插入数据
				this.restoreModelAllGoldCrossIncrementalByJson();
			}
			if(TableName.MDL_CLOSE_PRICE_MA5_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
				this.restoreModelClosePriceMA5GoldCrossIncrementalByJson();
			}
			if(TableName.MDL_CLOSE_PRICE_MA5_DEAD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
				this.restoreModelClosePriceMA5DeadCrossIncrementalByJson();
			}
			if(TableName.MDL_MACD_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_MACD_GOLD_CROSS中插入数据
				this.restoreModelMACDGoldCrossIncrementalByJson();
			}
			if(TableName.MDL_MACD_DEAD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_MACD_DEAD_CROSS中插入数据
				this.restoreModelMACDDeadCrossIncrementalByJson();
			}
			if(TableName.MDL_HEI_KIN_ASHI_UP_DOWN.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_HEI_KIN_ASHI_UP_DOWN中插入数据
				this.restoreModelHeiKinAshiUpDownIncrementalByJson();
			}
			if(TableName.MDL_HEI_KIN_ASHI_DOWN_UP.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_HEI_KIN_ASHI_DOWN_UP中插入数据
				this.restoreModelHeiKinAshiDownUpIncrementalByJson();
			}
			if(TableName.MDL_KD_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_KD_GOLD_CROSS中插入数据
				this.restoreModelKDGoldCrossIncrementalByJson();
			}
			if(TableName.MDL_KD_DEAD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_KD_DEAD_CROSS中插入数据
				this.restoreModelKDDeadCrossIncrementalByJson();
			}
			if(TableName.MDL_TOP_STOCK.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_TOP_STOCK中插入数据
				this.restoreModelTopStockIncrementalByJson();
			}
			if(TableName.MDL_TOP_STOCK_DETAIL.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_TOP_STOCK_DETAIL中插入数据
				this.restoreModelTopStockDetailIncrementalByJson();
			}
			if(TableName.MDL_STOCK_ANALYSIS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_STOCK_ANALYSIS中插入数据
				this.restoreModelStockAnalysisIncrementalByJson();
			}
			if(TableName.MDL_STOCK_MONTH_ANALYSIS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_STOCK_MONTH_ANALYSIS中插入数据
				this.restoreModelStockMonthAnalysisIncrementalByJson();
			}

			if(TableName.MDL_WEEK_KD_GOLD_CROSS.getName().trim().equals(tableNameArray[i])){
				// 向表MDL_WEEK_KD_GOLD_CROSS中插入数据
				this.restoreModelWeekKDGoldCrossIncrementalByJson();
			}

			if(TableName.STOCK_INFO.getName().trim().equals(tableNameArray[i])){
				// 向表STOCK_INFO中插入数据
				this.restoreStockInfoIncrementalByJson();
			}
			if(TableName.BOARD.getName().trim().equals(tableNameArray[i])){
				// 向表BOARD中插入数据
				this.restoreBoardIncrementalByJson();
			}
			if(TableName.MODEL.getName().trim().equals(tableNameArray[i])){
				// 向表MODEL中插入数据
				this.restoreModelIncrementalByJson();
			}
			if(TableName.FOREIGN_EXCHANGE.getName().trim().equals(tableNameArray[i])){
				// 向表FOREIGN_EXCHANGE中插入数据
				this.restoreForeignExchangeIncrementalByJson();
			}
			if(TableName.FOREIGN_EXCHANGE_RECORD.getName().trim().equals(tableNameArray[i])){
				// 向表FOREIGN_EXCHANGE_RECORD中插入数据
				this.restoreForeignExchangeRecordByJson();
			}
		}

		logger.info("使用json恢复数据库的各个表中的数据完成");
	}
	
	/**
	 * 恢复表STOCK_TRANSACTION_DATA中的数据
	 * @param beginTime
	 * @param endTime
	 */
	public void restoreStockTransactionDataIncrementalByJson(String beginTime,String endTime){
		logger.info("开始恢复表STOCK_TRANSACTION_DATA中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.STOCK_TRANSACTION_DATA);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			// 当文件的后缀为zip，并且时间在beginTime和endTime之间时，才将这个文件解压
			String fileExtension=FilenameUtils.getExtension(file.getName());
			int fileDate=Integer.parseInt(file.getName().split("-")[1].substring(0,8));
			int fileBeginDate=Integer.parseInt(beginTime);
			int fileEndDate=Integer.parseInt(endTime);
			try {
				if(fileExtension.equals("zip") && fileDate>=fileBeginDate && fileDate<=fileEndDate){
					// 将每一个zip文件解压为json文件
					ZipDecompressor.unZipFiles(file, path);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			int fileDate=Integer.parseInt(file.getName().split("-")[1].substring(0,8));
			int fileBeginDate=Integer.parseInt(beginTime);
			int fileEndDate=Integer.parseInt(endTime);
			if (file.getName().endsWith(".json") && fileDate>=fileBeginDate && fileDate<=fileEndDate) {
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    StockTransactionData stockTransactionData=new StockTransactionData();
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					try {
						while((s = br.readLine())!=null){
							stockTransactionData=mapper.readValue(s,StockTransactionData.class);
							stockTransactionDataDao.save(stockTransactionData);
						}
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
			}
		}
	}
	
	/**
	 * 恢复表STOCK_WEEK中的数据
	 * @param beginDate
	 * @param endDate
	 */
	public void restoreStockWeekIncrementalByJson(String beginDate,String endDate){
		logger.info("恢复表STOCK_WEEK中的数据，日期为【" + beginDate + "】至【" + endDate + "】");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.STOCK_WEEK);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			// 当文件的后缀为zip，并且时间在beginTime和endTime之间时，才将这个文件解压
//			String fileExtension=FilenameUtils.getExtension(file.getName());
//			int fileDate=Integer.parseInt(file.getName().split("-")[1].substring(0,8));
//			int fileBeginDate=Integer.parseInt(beginDate);
//			int fileEndDate=Integer.parseInt(endDate);
			try {
//				if(fileExtension.equals("zip") && fileDate>=fileBeginDate && fileDate<=fileEndDate){
					// 将每一个zip文件解压为json文件
					ZipDecompressor.unZipFiles(file, path);
//				}
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
//			int fileDate=Integer.parseInt(file.getName().split("-")[1].substring(0,8));
//			int fileBeginDate=Integer.parseInt(beginDate);
//			int fileEndDate=Integer.parseInt(endDate);
			if (file.getName().endsWith(".json") /*&& fileDate>=fileBeginDate && fileDate<=fileEndDate*/) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					StockWeek stockWeek = new StockWeek();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while ((s = bufferedReader.readLine()) != null) {
						stockWeek = mapper.readValue(s, StockWeek.class);
						stockWeekDao.saveStockWeek(stockWeek);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除了文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表STOCK_MONTH中的数据
	 */
	public void restoreStockMonthIncrementalByJson(){
		logger.info("恢复表STOCK_MONTH中的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.STOCK_MONTH);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			// 当文件的后缀为zip，并且时间在beginTime和endTime之间时，才将这个文件解压
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					StockMonth stockMonth = new StockMonth();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while ((s = bufferedReader.readLine()) != null) {
						stockMonth = mapper.readValue(s, StockMonth.class);
						stockMonthDao.saveStockMonth(stockMonth);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除了文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表STOCK_INDEX中的数据
	 */
	public void restoreStockIndexIncrementalByJson(){
		logger.info("恢复表STOCK_INDEX中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.STOCK_INDEX);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    StockIndex stockIndex=new StockIndex();
					fileReader = new FileReader(file);
					BufferedReader bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null){
						stockIndex=mapper.readValue(s,StockIndex.class);
						stockIndexDao.saveStockIndex(stockIndex);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表STOCK_INDEX_WEEK中的数据
	 */
	public void restoreStockIndexWeekIncrementalByJson(){
		logger.info("恢复表STOCK_INDEX_WEEK中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.STOCK_INDEX_WEEK);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			// 当文件的后缀为zip，并且时间在beginTime和endTime之间时，才将这个文件解压
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    StockIndexWeek stockIndexWeek=new StockIndexWeek();
				    fileReader = new FileReader(file);
				    bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null){
						stockIndexWeek=mapper.readValue(s,StockIndexWeek.class);
						stockIndexWeekDao.saveStockIndexWeek(stockIndexWeek);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表BOARD_INDEX中的数据
	 */
	public void restoreBoardIndexIncrementalByJson() {
		logger.info("恢复表BOARD_INDEX中的数据");

		String path = new String(JSON_DATA_BACKUP_DIR + TableName.BOARD_INDEX);
		File fPath = new File(path);
		File[] files = fPath.listFiles();
		for (File file : files) {
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;

		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					BoardIndex boardIndex = new BoardIndex();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while ((s = bufferedReader.readLine()) != null) {
						boardIndex = mapper.readValue(s, BoardIndex.class);
						boardIndexDao.save(boardIndex);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表REPORT中的数据
	 */
	public void restoreReportIncrementalByJson(){
		logger.info("恢复表REPORT中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.REPORT);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    Report report=new Report();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null){
						report=mapper.readValue(s,Report.class);
						reportDao.insertReport(report);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_CLOSE_PRICE_MA5_GOLD_CROSS的数据
	 */
	public void restoreModelClosePriceMA5GoldCrossIncrementalByJson(){
		logger.info("恢复表MDL_CLOSE_PRICE_MA5_GOLD_CROSS的数据");

		String path = new String(JSON_DATA_BACKUP_DIR + TableName.MDL_CLOSE_PRICE_MA5_GOLD_CROSS);
		File fPath = new File(path);
		File[] files = fPath.listFiles();
		for (File file : files) {
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelClosePriceMA5GoldCross modelClosePriceMA5GoldCross = new ModelClosePriceMA5GoldCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelClosePriceMA5GoldCross = mapper.readValue(s, ModelClosePriceMA5GoldCross.class);
						modelClosePriceMA5GoldCrossDao.save(modelClosePriceMA5GoldCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_CLOSE_PRICE_MA5_DEAD_CROSS的数据
	 */
	public void restoreModelClosePriceMA5DeadCrossIncrementalByJson(){
		logger.info("恢复表MDL_CLOSE_PRICE_MA5_DEAD_CROSS的数据");

		String path = new String(JSON_DATA_BACKUP_DIR + TableName.MDL_CLOSE_PRICE_MA5_DEAD_CROSS);
		File fPath = new File(path);
		File[] files = fPath.listFiles();
		for (File file : files) {
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelClosePriceMA5DeadCross modelClosePriceMA5DeadCross = new ModelClosePriceMA5DeadCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelClosePriceMA5DeadCross = mapper.readValue(s, ModelClosePriceMA5DeadCross.class);
						modelClosePriceMA5DeadCrossDao.save(modelClosePriceMA5DeadCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_ALL_GOLD_CROSS的数据
	 */
	public void restoreModelAllGoldCrossIncrementalByJson(){
		logger.info("恢复表MDL_ALL_GOLD_CROSS的数据");

		String path = new String(JSON_DATA_BACKUP_DIR + TableName.MDL_ALL_GOLD_CROSS);
		File fPath = new File(path);
		File[] files = fPath.listFiles();
		for (File file : files) {
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelAllGoldCross modelAllGoldCross = new ModelAllGoldCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelAllGoldCross = mapper.readValue(s, ModelAllGoldCross.class);
						modelAllGoldCrossDao.save(modelAllGoldCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表MDL_MACD_GOLD_CROSS的数据
	 */
	public void restoreModelMACDGoldCrossIncrementalByJson(){
		logger.info("恢复表MDL_MACD_GOLD_CROSS的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_MACD_GOLD_CROSS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    ModelMACDGoldCross modelMACDGoldCross=new ModelMACDGoldCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelMACDGoldCross = mapper.readValue(s, ModelMACDGoldCross.class);
						modelMACDGoldCrossDao.save(modelMACDGoldCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_MACD_DEAD_CROSS的数据
	 */
	public void restoreModelMACDDeadCrossIncrementalByJson(){
		logger.info("恢复表MDL_MACD_DEAD_CROSS的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_MACD_DEAD_CROSS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelMACDDeadCross modelMACDDeadCross=new ModelMACDDeadCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelMACDDeadCross = mapper.readValue(s, ModelMACDDeadCross.class);
						modelMACDDeadCrossDao.save(modelMACDDeadCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_HEI_KIN_ASHI_UP_DOWN的数据
	 */
	public void restoreModelHeiKinAshiUpDownIncrementalByJson(){
		logger.info("恢复表MDL_HEI_KIN_ASHI_UP_DOWN的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_HEI_KIN_ASHI_UP_DOWN);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelHeiKinAshiUpDown modelHeiKinAshiUpDown=new ModelHeiKinAshiUpDown();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelHeiKinAshiUpDown = mapper.readValue(s, ModelHeiKinAshiUpDown.class);
						modelHeiKinAshiUpDownDao.save(modelHeiKinAshiUpDown);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_HEI_KIN_ASHI_DOWN_UP的数据
	 */
	public void restoreModelHeiKinAshiDownUpIncrementalByJson(){
		logger.info("恢复表MDL_HEI_KIN_ASHI_DOWN_UP的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_HEI_KIN_ASHI_DOWN_UP);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelHeiKinAshiDownUp modelHeiKinAshiDownUp=new ModelHeiKinAshiDownUp();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelHeiKinAshiDownUp = mapper.readValue(s, ModelHeiKinAshiDownUp.class);
						modelHeiKinAshiDownUpDao.save(modelHeiKinAshiDownUp);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_TOP_STOCK的数据
	 */
	public void restoreModelTopStockIncrementalByJson(){
		logger.info("恢复表MDL_TOP_STOCK的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_TOP_STOCK);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelTopStock modelTopStock = new ModelTopStock();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelTopStock = mapper.readValue(s, ModelTopStock.class);
						modelTopStockDao.save(modelTopStock);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_TOP_STOCK_DETAIL的数据
	 */
	public void restoreModelTopStockDetailIncrementalByJson(){
		logger.info("恢复表MDL_TOP_STOCK_DETAIL的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_TOP_STOCK_DETAIL);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelTopStockDetail modelTopStockDetail = new ModelTopStockDetail();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelTopStockDetail = mapper.readValue(s, ModelTopStockDetail.class);
						modelTopStockDetailDao.save(modelTopStockDetail);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 向表MDL_STOCK_ANALYSIS中插入数据
	 */
	public void restoreModelStockAnalysisIncrementalByJson(){
		logger.info("恢复表MDL_STOCK_ANALYSIS的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_STOCK_ANALYSIS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelStockAnalysis modelStockAnalysis = new ModelStockAnalysis();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelStockAnalysis = mapper.readValue(s, ModelStockAnalysis.class);
						modelStockAnalysisDao.save(modelStockAnalysis);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 向表MDL_STOCK_MONTH_ANALYSIS中插入数据
	 */
	public void restoreModelStockMonthAnalysisIncrementalByJson(){
		logger.info("恢复表MDL_STOCK_MONTH_ANALYSIS的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_STOCK_MONTH_ANALYSIS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelStockMonthAnalysis modelStockMonthAnalysis = new ModelStockMonthAnalysis();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelStockMonthAnalysis = mapper.readValue(s, ModelStockMonthAnalysis.class);
						modelStockMonthAnalysisDao.save(modelStockMonthAnalysis);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_WEEK_KD_GOLD_CROSS的数据
	 */
	public void restoreModelWeekKDGoldCrossIncrementalByJson(){
		logger.info("恢复表MDL_WEEK_KD_GOLD_CROSS的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_WEEK_KD_GOLD_CROSS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelWeekKDGoldCross modelWeekKDGoldCross = new ModelWeekKDGoldCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelWeekKDGoldCross = mapper.readValue(s, ModelWeekKDGoldCross.class);
						modelWeekKDGoldCrossDao.save(modelWeekKDGoldCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_KD_GOLD_CROSS的数据
	 */
	public void restoreModelKDGoldCrossIncrementalByJson(){
		logger.info("恢复表MDL_KD_GOLD_CROSS的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_KD_GOLD_CROSS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelKDGoldCross modelKDGoldCross = new ModelKDGoldCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelKDGoldCross = mapper.readValue(s, ModelKDGoldCross.class);
						modelKDGoldCrossDao.save(modelKDGoldCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表MDL_KD_DEAD_CROSS的数据
	 */
	public void restoreModelKDDeadCrossIncrementalByJson(){
		logger.info("恢复表MDL_KD_DEAD_CROSS的数据");

		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MDL_KD_DEAD_CROSS);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ModelKDDeadCross modelKDDeadCross = new ModelKDDeadCross();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						modelKDDeadCross = mapper.readValue(s, ModelKDDeadCross.class);
						modelKDDeadCrossDao.save(modelKDDeadCross);
					}
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}

	/**
	 * 恢复表FOREIGN_EXCHANGE_RECORD的数据
	 */
	public void restoreForeignExchangeRecordByJson(){
		logger.info("恢复表FOREIGN_EXCHANGE_RECORD的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.FOREIGN_EXCHANGE_RECORD);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
					ForeignExchangeRecord fer=new ForeignExchangeRecord();
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					try {
						while((s = br.readLine())!=null){
							fer=mapper.readValue(s,ForeignExchangeRecord.class);
							foreignExchangeRecordDao.restoreForeignExchangeRecord(fer);
						}
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
			}
		}
		
		logger.info("insert the data of table FOREIGN_EXCHANGE_RECORD incremental by json finish");
	}
	
	/**
	 * 恢复表BOARD中的数据
	 */
	public void restoreBoardIncrementalByJson(){
		logger.info("恢复表BOARD中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.BOARD);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    Board b=new Board();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null) {
						b = mapper.readValue(s, Board.class);
						boardDao.saveBoard(b);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表STOCK_INFO中的数据
	 */
	public void restoreStockInfoIncrementalByJson(){
		logger.info("恢复表STOCK_INFO中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.STOCK_INFO);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    StockInfo stockInfo=new StockInfo();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
						while((s = bufferedReader.readLine())!=null){
							stockInfo=mapper.readValue(s,StockInfo.class);
							stockInfoDao.saveStockInfo(stockInfo);
						}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() +"】");
			}
		}
	}
	
	/**
	 * 恢复表MODEL中的数据
	 */
	public void restoreModelIncrementalByJson(){
		logger.info("恢复表MODEL中的数据");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.MODEL);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				FileReader fileReader = null;
				BufferedReader bufferedReader = null;
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    Model model=new Model();
					fileReader = new FileReader(file);
					bufferedReader = new BufferedReader(fileReader);
					while((s = bufferedReader.readLine())!=null){
						model=mapper.readValue(s,Model.class);
						modelDao.saveModel(model);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						fileReader.close();
					} catch (IOException e) {
								e.printStackTrace();
					}
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
				logger.info("删除文件【" + file.toString() + "】");
			}
		}
	}
	
	/**
	 * 恢复表FOREIGN_EXCHANGE中的数据
	 */
	public void restoreForeignExchangeIncrementalByJson(){
		logger.info("insert the data of table FOREIGN_EXCHANGE incremental by json begin");
		
		String path=new String(JSON_DATA_BACKUP_DIR+TableName.FOREIGN_EXCHANGE);
		File fPath=new File(path);
		File[] files=fPath.listFiles();
		for(File file:files){
			try {
				// 将每一个zip文件解压为json文件
				ZipDecompressor.unZipFiles(file, path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File deletePath = new File(path);
		File[] deleteFile = deletePath.listFiles();
		for (File file : deleteFile) {
			if (file.getName().endsWith(".json")) {
				try {
					String s;
					ObjectMapper mapper = new ObjectMapper();
				    ForeignExchange fe=new ForeignExchange();
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					try {
						while((s = br.readLine())!=null){
							fe=mapper.readValue(s,ForeignExchange.class);
							foreignExchangeDao.insertForeignExchange(fe);
						}
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}

				// 删除json文件
				FileUtil.deleteFile(file.toString());
			}
		}
		
		logger.info("insert the data of table FOREIGN_EXCHANGE incremental by json finish");
	}

	/*****************************************************************************************************************
	 *
	 * 									发送邮件
	 *
	 ******************************************************************************************************************/
	/**
	 * 发送邮件
	 */
	@Override
	public void sendEMail(){
        logger.info("开始发送邮件");

        // 参数
        String mailHost = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.host");
        String mailTransportProtocol = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.transport.protocol");
        String mailSmtpAuth = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.smtp.auth");
		String mailUsername = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.username");
        String mailPassword = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.password");
        String mailSubject = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.realTransactionMain.subject");

		Properties prop = new Properties();
		// 邮件服务器
		prop.setProperty("mail.host", mailHost);
		// 邮件发送协议
		prop.setProperty("mail.transport.protocol", mailTransportProtocol);
		// 需要验证用户名密码
		prop.setProperty("mail.smtp.auth", mailSmtpAuth);

		Transport ts = null;
		try {
			//使用JavaMail发送邮件的5个步骤
			//1.txt、创建定义整个应用程序所需的环境信息的Session对象
            Session session = Session.getDefaultInstance(prop, new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        //发件人邮件用户名、授权码
                        return new PasswordAuthentication(mailUsername, mailPassword);
                    }
                }
            );

			//开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
			session.setDebug(true);

			//2、通过session得到transport对象
			ts = session.getTransport();

			//3、使用邮箱的用户名和授权码连上邮件服务器
			ts.connect(mailHost, mailUsername, mailPassword);

			//4，创建邮件
			//4-1.txt，创建邮件对象
			MimeMessage message = new MimeMessage(session);

			//4-2，指明邮件的发件人
			message.setFrom(new InternetAddress(mailUsername));

			//4-3，指明邮件的收件人
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(mailUsername));

			//4-4，邮件标题
			message.setSubject(mailSubject + "_" + DateUtil.dateToString(new Date()));

			//4-5，邮件文本内容
            File file = new File(PRINT_REAL_BUY_SELL_SUGGESTION);
            FileReader fileReader = new FileReader(file);
            char[] buf = new char[128];
            int len;
            StringBuffer stringBuffer = new StringBuffer();
            while ((len = fileReader.read(buf)) != -1) {
                stringBuffer.append(new String(buf, 0, len));
            }
            message.setContent(stringBuffer.toString(), "text/html;charset=UTF-8");

			//4-6，发送邮件
			ts.sendMessage(message, message.getAllRecipients());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//5，关闭连接
			try {
				ts.close();
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}

        logger.info("发送邮件结束");
	}
	
	@Override
	public String listToString(List list) {
		// TODO Auto-generated method stub
		return null;
	}

}
