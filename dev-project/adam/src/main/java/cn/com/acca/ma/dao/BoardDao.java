package cn.com.acca.ma.dao;

import java.math.BigDecimal;
import java.util.List;

import cn.com.acca.ma.model.Board;

public interface BoardDao {

	/**
	 * 通过board_id查找board_name
	 * @param multithreading
	 * @param id
	 * @return
	 */
	String findBoardNameById(boolean multithreading, BigDecimal id);

	/**
	 * 获取BOARD表中ID列的所有值
	 * @param multithreading
	 * @return
	 */
	List<BigDecimal> getAllBoardId(boolean multithreading);

	/**
	 * 获取所有的板块
	 * @param multithreading
	 * @return
	 */
	List<Board> getAllBoard(boolean multithreading);
	
	/**
	 * 保存Board对象
	 * @param board
	 */
	void saveBoard(Board board);
}
