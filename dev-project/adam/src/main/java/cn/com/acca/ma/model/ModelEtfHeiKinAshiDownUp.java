package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 平均K线下跌趋势的交易
 */
@Data
@Entity
@Table(name = "mdl_etf_hei_kin_ashi_down_up")
public class ModelEtfHeiKinAshiDownUp implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    /**
     * 指数代码
     */
    @Column(name = "ETF_CODE")
    private String etfCode;

    /**
     * 卖出日期
     */
    @Column(name = "SELL_DATE")
    private Date sellDate;

    /**
     * 卖出价格
     */
    @Column(name = "SELL_PRICE")
    private BigDecimal sellPrice;

    /**
     * 买入日期
     */
    @Column(name = "BUY_DATE")
    private Date buyDate;

    /**
     * 买入价格
     */
    @Column(name = "BUY_PRICE")
    private BigDecimal buyPrice;

    /**
     * 累计收益。单位：元
     */
    @Column(name = "ACCUMULATIVE_PROFIT_LOSS")
    private BigDecimal accumulativeProfitLoss;

    /**
     * 本次交易盈亏百分比
     */
    @Column(name = "PROFIT_LOSS")
    private BigDecimal profitLoss;

    /**
     * 类型。1表示所有交易记录，时间是1997年1月1日至今。2表示所有交易记录，120、250日均线单调不减，时间是1997年1月1日至今。3表示所有交易记录，120日均线单调不减，时间是1997年1月1日至今。4表示所有交易记录，250日均线单调不减，时间是1997年1月1日至今。5表示所有交易记录，时间是2011年1月1日至今。6表示所有交易记录，120、250日均线单调不减，时间是2011年1月1日至今。7表示所有交易记录，120日均线单调不减，时间是2011年1月1日至今。8表示所有交易记录，250日均线单调不减，时间是2011年1月1日至今。
     */
    @Column(name = "TYPE_")
    private Integer type;

}
