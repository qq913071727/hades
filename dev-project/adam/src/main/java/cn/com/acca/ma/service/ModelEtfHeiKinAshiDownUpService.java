package cn.com.acca.ma.service;

public interface ModelEtfHeiKinAshiDownUpService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表mdl_etf_hei_kin_ashi_down_up插入数据
     */
    void writeModelEtfHeiKinAshiDownUp();

    /*********************************************************************************************************************
     *
     * 												计算增量数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表mdl_etf_hei_kin_ashi_down_up插入数据
     */
    void writeModelEtfHeiKinAshiDownUpByDate();
}
