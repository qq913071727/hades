package cn.com.acca.ma.dao;

public interface ModelEtfBullShortLineUpDao extends BaseDao {

    /**
     * 海量地向表MDL_ETF_BULL_SHORT_LINE_UP中写入数据
     */
    void writeModelEtfBullShortLineUp();

    /**
     * 增量地向表MDL_ETF_BULL_SHORT_LINE_UP中写入数据
     * @param date
     */
    void writeModelEtfBullShortLineUpIncr(String date);
}
