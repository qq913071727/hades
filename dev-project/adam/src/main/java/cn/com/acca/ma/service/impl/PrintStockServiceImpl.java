package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.service.PrintStockService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class PrintStockServiceImpl extends BaseServiceImpl<PrintStockServiceImpl, Object> implements PrintStockService {

    /**
     * 打印波段操作的股票，并发送邮件
     */
    @Override
    public void printBandOperationStockAndSendMail() {
        String date = PropertiesUtil.getValue(PRINT_PROPERTIES, "print.bond.operation.stock.date");
        // 发邮件的参数
        String mailHost = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.host");
        String mailTransportProtocol = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.transport.protocol");
        String mailSmtpAuth = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.smtp.auth");
        String mailUsername = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.username");
        String mailPassword = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.password");
        String mailSubject = PropertiesUtil.getValue(PROJECT_PROPERTIES, "mail.bondOperationStock.subject");
        stockTransactionDataDao.findBandOperationStockAndSendMail(date, mailHost, mailTransportProtocol, mailSmtpAuth, mailUsername, mailPassword, mailSubject);
    }

    @Override
    public String listToString(List list) {
        return null;
    }
}
