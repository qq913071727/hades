package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.dao.*;
import cn.com.acca.ma.dao.impl.*;
import cn.com.acca.ma.model.*;
import cn.com.acca.ma.pojo.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.lowagie.text.Image;

import cn.com.acca.ma.service.BaseService;

public abstract class BaseServiceImpl<T, E> implements BaseService {

    protected Class<T> entityClass;
    protected String className;

    /**
     * 线程池
     */
    protected ExecutorService executorService = Executors.newFixedThreadPool(Integer.parseInt(PropertiesUtil.getValue(THREAD_PROPERTIES, "thread.pool.threadNumber")));

    /**
     * Logger类的实例，用于记录日志
     */
    protected Logger logger;

    /********************************************* 股票用到的成员变量和方法 **********************************************/
    /**
     * StockTransactionDataDao类的实例
     */
    protected StockTransactionDataDao stockTransactionDataDao = new StockTransactionDataDaoImpl();

    /**
     * StockTransactionDataAllDao类的实例
     */
    protected StockTransactionDataAllDao stockTransactionDataAllDao = new StockTransactionDataAllDaoImpl();

    /**
     * EtfInfoDao类的实例
     */
    protected EtfInfoDao etfInfoDao = new EtfInfoDaoImpl();

    /**
     * EtfTransactionDataDao类的实例
     */
    protected EtfTransactionDataDao etfTransactionDataDao = new EtfTransactionDataDaoImpl();

    /**
     * StockCodeDao类的实例
     */
    protected StockInfoDao stockInfoDao = new StockInfoDaoImpl();

    /**
     * StockIndexDao类的实例
     */
    protected StockIndexDao stockIndexDao = new StockIndexDaoImpl();

    /**
     * StockIndexWeekDao类的实例
     */
    protected StockIndexWeekDao stockIndexWeekDao = new StockIndexWeekDaoImpl();

    /**
     * StockIndexWeekendDao类的实例
     */
    protected StockIndexWeekendDao stockIndexWeekendDao = new StockIndexWeekendDaoImpl();

    /**
     * StockWeekendDao类的实例
     */
    protected StockWeekDao stockWeekDao = new StockWeekDaoImpl();

    /**
     * StockMonthDao类的实例
     */
    protected StockMonthDao stockMonthDao = new StockMonthDaoImpl();

    /**
     * BoardIndexDao类的实例
     */
    protected BoardIndexDao boardIndexDao = new BoardIndexDaoImpl();

    /**
     * Board2IndexDao类的实例
     */
    protected Board2IndexDao board2IndexDao = new Board2IndexDaoImpl();

    /**
     * Board2IndexDetailDao类的实例
     */
    protected Board2IndexDetailDao board2IndexDetailDao = new Board2IndexDetailDaoImpl();

    /**
     * BoardDao类的实例
     */
    protected BoardDao boardDao = new BoardDaoImpl();

    /**
     * ReportDao类的实例
     */
    protected ReportDao reportDao = new ReportDaoImpl();

    /**
     * ModelDao类的实例
     */
    protected ModelDao modelDao = new ModelDaoImpl();

    /**
     * ModelAllGoldCrossDao类的实例
     */
    protected ModelAllGoldCrossDao modelAllGoldCrossDao = new ModelAllGoldCrossDaoImpl();

    /**
     * ModelWeekAllGoldCrossDao类的实例
     */
    protected ModelWeekAllGoldCrossDao modelWeekAllGoldCrossDao = new ModelWeekAllGoldCrossDaoImpl();

    /**
     * ModelMACDGoldCrossDao类的实例
     */
    protected ModelMACDGoldCrossDao modelMACDGoldCrossDao = new ModelMACDGoldCrossDaoImpl();

    /**
     * ModelMACDDeadCrossDao类的实例
     */
    protected ModelMACDDeadCrossDao modelMACDDeadCrossDao = new ModelMACDDeadCrossDaoImpl();

    /**
     * ModelKDGoldCrossDao类的实例
     */
    protected ModelKDGoldCrossDao modelKDGoldCrossDao = new ModelKDGoldCrossDaoImpl();

    /**
     * ModelKDDeadCrossDao类的实例
     */
    protected ModelKDDeadCrossDao modelKDDeadCrossDao = new ModelKDDeadCrossDaoImpl();

    /**
     * ModelVolumeTurnoverUpMa250Dao类的实例
     */
    protected ModelVolumeTurnoverUpMa250Dao modelVolumeTurnoverUpMa250Dao = new ModelVolumeTurnoverUpMa250DaoImpl();

    /**
     * ModelUpDownPercentageMaGoldCrossDao类的实例
     */
    protected ModelPercentageMaGoldCrossDao modelPercentageMaGoldCrossDao = new ModelPercentageMaGoldCrossDaoImpl();

    /**
     * ModelWeekKDGoldCrossDao类的实例
     */
    protected ModelWeekKDGoldCrossDao modelWeekKDGoldCrossDao = new ModelWeekKDGoldCrossDaoImpl();

    /**
     * ModelClosePriceMA5GoldCrossDao类的实例
     */
    protected ModelClosePriceMA5GoldCrossDao modelClosePriceMA5GoldCrossDao = new ModelClosePriceMA5GoldCrossDaoImpl();

    /**
     * ModelClosePriceMA5DeadCrossDao类的实例
     */
    protected ModelClosePriceMA5DeadCrossDao modelClosePriceMA5DeadCrossDao = new ModelClosePriceMA5DeadCrossDaoImpl();

    /**
     * ModelHeiKinAshiUpDownDao类的实例
     */
    protected ModelHeiKinAshiUpDownDao modelHeiKinAshiUpDownDao = new ModelHeiKinAshiUpDownDaoImpl();

    /**
     * ModelHeiKinAshiDownUpDao类的实例
     */
    protected ModelHeiKinAshiDownUpDao modelHeiKinAshiDownUpDao = new ModelHeiKinAshiDownUpDaoImpl();

    /**
     * ModelStockIndexMACDGoldCrossDao类的实例
     */
    protected ModelStockIndexMACDGoldCrossDao modelStockIndexMACDGoldCrossDao = new ModelStockIndexMACDGoldCrossDaoImpl();

    /**
     * ModelStockIndexClosePriceMA5GoldCrossDao类的实例
     */
    protected ModelStockIndexClosePriceMA5GoldCrossDao modelStockIndexClosePriceMA5GoldCrossDao = new ModelStockIndexClosePriceMA5GoldCrossDaoImpl();

    /**
     * ModelStockIndexHeiKinAshiDownUpDao类的实例
     */
    protected ModelStockIndexHeiKinAshiDownUpDao modelStockIndexHeiKinAshiDownUpDao = new ModelStockIndexHeiKinAshiDownUpDaoImpl();

    /**
     * ModelStockIndexKDGoldCrossDao类的实例
     */
    protected ModelStockIndexKDGoldCrossDao modelStockIndexKDGoldCrossDao = new ModelStockIndexKDGoldCrossDaoImpl();

    /**
     * ModelEtfMACDGoldCrossDao类的实例
     */
    protected ModelEtfMACDGoldCrossDao modelEtfMACDGoldCrossDao = new ModelEtfMACDGoldCrossDaoImpl();

    /**
     * ModelEtfClosePriceMA5GoldCrossDao类的实例
     */
    protected ModelEtfClosePriceMA5GoldCrossDao modelEtfClosePriceMA5GoldCrossDao = new ModelEtfClosePriceMA5GoldCrossDaoImpl();

    /**
     * ModelEtfHeiKinAshiDownUpDao类的实例
     */
    protected ModelEtfHeiKinAshiDownUpDao modelEtfHeiKinAshiDownUpDao = new ModelEtfHeiKinAshiDownUpDaoImpl();

    /**
     * ModelEtfKDGoldCrossDao类的实例
     */
    protected ModelEtfKDGoldCrossDao modelEtfKDGoldCrossDao = new ModelEtfKDGoldCrossDaoImpl();

    /**
     * ModelTopStockDao类的实例
     */
    protected ModelTopStockDao modelTopStockDao = new ModelTopStockDaoImpl();

    /**
     * ModelTopStockDetailDao类的实例
     */
    protected ModelTopStockDetailDao modelTopStockDetailDao = new ModelTopStockDetailDaoImpl();

    /**
     * ModelSubNewStockDao类的实例
     */
    protected ModelSubNewStockDao modelSubNewStockDao = new ModelSubNewStockDaoImpl();

    /**
     * ModelStockAnalysisDao类的实例
     */
    protected ModelStockAnalysisDao modelStockAnalysisDao = new ModelStockAnalysisDaoImpl();

    /**
     * ModelBullShortLineUpDao类的实例
     */
    protected ModelBullShortLineUpDao modelBullShortLineUpDao = new ModelBullShortLineUpDaoImpl();

    /**
     * ModelEtfBullShortLineUpDao类的实例
     */
    protected ModelEtfBullShortLineUpDao modelEtfBullShortLineUpDao = new ModelEtfBullShortLineUpDaoImpl();

    /**
     * ModelStockWeekAnalysisDao类的实例
     */
    protected ModelStockWeekAnalysisDao modelStockWeekAnalysisDao = new ModelStockWeekAnalysisDaoImpl();

    /**
     * ModelStockMonthAnalysisDao类的实例
     */
    protected ModelStockMonthAnalysisDao modelStockMonthAnalysisDao = new ModelStockMonthAnalysisDaoImpl();

    /**
     * RobotStockFilterDao类的实例
     */
    protected RobotStockFilterDao robotStockFilterDao = new RobotStockFilterDaoImpl();

    /**
     * RobotStockTransactionRecordDao类的实例
     */
    protected RobotStockTransactionRecordDao robotStockTransactionRecordDao = new RobotStockTransactionRecordDaoImpl();

    /**
     * RobotAccountDao类的实例
     */
    protected RobotAccountDao robotAccountDao = new RobotAccountDaoImpl();

    /**
     * Robot3AccountDao类的实例
     */
    protected Robot3AccountDao robot3AccountDao = new Robot3AccountDaoImpl();

    /**
     * Robot3StockFilterDao类的实例
     */
    protected Robot3StockFilterDao robot3StockFilterDao = new Robot3StockFilterDaoImpl();

    /**
     * Robot3StockTransactRecordDao类的实例
     */
    protected Robot3StockTransactRecordDao robot3StockTransactRecordDao = new Robot3StockTransactRecordDaoImpl();

    /**
     * Robot2AccountDao类的实例
     */
    protected Robot2AccountDao robot2AccountDao = new Robot2AccountDaoImpl();

    /**
     * Robot2StockFilterDao类的实例
     */
    protected Robot2StockFilterDao robot2StockFilterDao = new Robot2StockFilterDaoImpl();

    /**
     * Robot2StockTransactRecordDao类的实例
     */
    protected Robot2StockTransactRecordDao robot2StockTransactRecordDao = new Robot2StockTransactRecordDaoImpl();

    /**
     * Robot4AccountDao类的实例
     */
    protected Robot4AccountDao robot4AccountDao = new Robot4AccountDaoImpl();

    /**
     * Robot4StockFilterDao类的实例
     */
    protected Robot4StockFilterDao robot4StockFilterDao = new Robot4StockFilterDaoImpl();

    /**
     * Robot4StockTransactRecordDao类的实例
     */
    protected Robot4StockTransactRecordDao robot4StockTransactRecordDao = new Robot4StockTransactRecordDaoImpl();

    /**
     * Robot5AccountDao类的实例
     */
    protected Robot5AccountDao robot5AccountDao = new Robot5AccountDaoImpl();

    /**
     * Robot5StockFilterDao类的实例
     */
    protected Robot5StockFilterDao robot5StockFilterDao = new Robot5StockFilterDaoImpl();

    /**
     * Robot6EtfTransactRecordDao类的实例
     */
    protected Robot6EtfTransactRecordDao robot6EtfTransactRecordDao = new Robot6EtfTransactRecordDaoImpl();

    /**
     * Robot6AccountDao类的实例
     */
    protected Robot6AccountDao robot6AccountDao = new Robot6AccountDaoImpl();

    /**
     * Robot6EtfFilterDao类的实例
     */
    protected Robot6EtfFilterDao robot6EtfFilterDao = new Robot6EtfFilterDaoImpl();

    /**
     * Robot5StockTransactRecordDao类的实例
     */
    protected Robot5StockTransactRecordDao robot5StockTransactRecordDao = new Robot5StockTransactRecordDaoImpl();

    /**
     * RealTransactionConditionDao类的实例
     */
    protected RealTransactionConditionDao realTransactionConditionDao = new RealTransactionConditionDaoImpl();

    /**
     * RealStockTransactionRecordDao类的实例
     */
    protected RealStockTransactionRecordDao realStockTransactionRecordDao = new RealStockTransactionRecordDaoImpl();

    /**
     * Real3TransactionConditionDao类的实例
     */
    protected Real3TransactionConditionDao real3TransactionConditionDao = new Real3TransactionConditionDaoImpl();

    /**
     * Real3StockTransactionRecordDao类的实例
     */
    protected Real3StockTransactionRecordDao real3StockTransactionRecordDao = new Real3StockTransactionRecordDaoImpl();

    /**
     * Real4TransactionConditionDao类的实例
     */
    protected Real4TransactionConditionDao real4TransactionConditionDao = new Real4TransactionConditionDaoImpl();

    /**
     * Real4StockTransactionRecordDao类的实例
     */
    protected Real4StockTransactionRecordDao real4StockTransactionRecordDao = new Real4StockTransactionRecordDaoImpl();

    /**
     * Real4AccountDao类的实例
     */
    protected Real4AccountDao real4AccountDao = new Real4AccountDaoImpl();

    /**
     * ProjectDao类的实例
     */
    protected ProjectDao projectDao = new ProjectDaoImpl();

    /**
     * HistoryRobotAccountLogDao类的实例
     */
    protected HistoryRobotAccountLogDao historyRobotAccountLogDao = new HistoryRobotAccountLogDaoImpl();

    /**
     * ModelWeekBollMACDGoldCrossDao类的实例
     */
    protected ModelWeekBollMACDGoldCrossDao modelWeekBollMACDGoldCrossDao = new ModelWeekBollMACDGoldCrossDaoImpl();

    /**
     * ModelWeekBollMACDDeadCrossDao类的实例
     */
    protected ModelWeekBollMACDDeadCrossDao modelWeekBollMACDDeadCrossDao = new ModelWeekBollMACDDeadCrossDaoImpl();

    /**
     * ModelWeekBollClosePriceMA5GoldCrossDao类的实例
     */
    protected ModelWeekBollClosePriceMA5GoldCrossDao modelWeekBollClosePriceMA5GoldCrossDao = new ModelWeekBollClosePriceMA5GoldCrossDaoImpl();

    /**
     * ModelWeekBollClosePriceMA5DeadCrossDao类的实例
     */
    protected ModelWeekBollClosePriceMA5DeadCrossDao modelWeekBollClosePriceMA5DeadCrossDao = new ModelWeekBollClosePriceMA5DeadCrossDaoImpl();

    /**
     * ModelWeekBollHeiKinAshiUpDownDao类的实例
     */
    protected ModelWeekBollHeiKinAshiUpDownDao modelWeekBollHeiKinAshiUpDownDao = new ModelWeekBollHeiKinAshiUpDownDaoImpl();

    /**
     * ModelWeekBollHeiKinAshiDownUpDao类的实例
     */
    protected ModelWeekBollHeiKinAshiDownUpDao modelWeekBollHeiKinAshiDownUpDao = new ModelWeekBollHeiKinAshiDownUpDaoImpl();

    /**
     * ModelWeekBollKDGoldCrossDao类的实例
     */
    protected ModelWeekBollKDGoldCrossDao modelWeekBollKDGoldCrossDao = new ModelWeekBollKDGoldCrossDaoImpl();

    /**
     * ModelWeekBollKDDeadCrossDao类的实例
     */
    protected ModelWeekBollKDDeadCrossDao modelWeekBollKDDeadCrossDao = new ModelWeekBollKDDeadCrossDaoImpl();

    /**
     * ModelGoldCrossDeadCrossAnalysisDao
     */
    protected ModelGoldCrossDeadCrossAnalysisDao modelGoldCrossDeadCrossAnalysisDao = new ModelGoldCrossDeadCrossAnalysisDaoImpl();

    /**
     * WeekDateDao
     */
    protected WeekDateDao weekDateDao = new WeekDateDaoImpl();

    protected CommodityFutureDateContractDataDao commodityFutureDateContractDataDao = new CommodityFutureDateContractDataDaoImpl();

    /**
     * Report类的实例，用于将报告的数据存储到数据库（表STOCK_REPORT）
     */
    protected Report report;

    /**
     * 获取程序当前路径，即根目录
     */
    protected static final String DIR = System.getProperty("user.dir");

    /**
     * 从网络上获取数据时，如果发生异常，则将异常信息存储到这个文件中
     */
    protected static final String CONNECTION_EXCEPTION_FILE =
            DIR + "/src/main/resources/ConnectionException.txt";

    /**
     * 设置代理和获取数据（从网络上获取数据，并保存到文件中）
     */
    protected static final String STOCK_RECORD_XML = DIR + "/src/main/resources/stock-record.xml";

    /**
     * 用于STOCK_TRANSACTION_DATA表相关操作的属性文件
     */
    protected static final String STOCK_RECORD_PROPERTIES =
            DIR + "/src/main/resources/stock-record.properties";

    /**
     * 用于绘制STOCK_TRANSACTION_DATA表相关图片的属性文件
     */
    protected static final String STOCK_RECORD_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/stock-record-picture.properties";

    /**
     * 用于ETF_TRANSACTION_DATA表相关操作的属性文件
     */
    protected static final String ETF_TRANSACTION_DATA_PROPERTIES =
            DIR + "/src/main/resources/etf-transaction-data.properties";

    /**
     * 用于MDL_ETF_MACD_GOLD_CROSS表相关操作的属性文件
     */
    protected static final String MDL_ETF_MACD_GOLD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-etf-macd-gold-cross.properties";

    /**
     * 用于MDL_ETF_CLOSE_PRICE_MA5_G_C表相关操作的属性文件
     */
    protected static final String MDL_ETF_CLOSE_PRICE_MA5_G_C_PROPERTIES =
            DIR + "/src/main/resources/model-etf-close-price-ma5-gold-cross.properties";

    /**
     * 用于MDL_ETF_HEI_KIN_ASHI_DOWN_UP表相关操作的属性文件
     */
    protected static final String MDL_ETF_HEI_KIN_ASHI_DOWN_UP_PROPERTIES =
            DIR + "/src/main/resources/model-etf-hei-kin-ashi-down-up.properties";

    /**
     * 用于MDL_ETF_KD_GOLD_CROSS表相关操作的属性文件
     */
    protected static final String MDL_ETF_KD_GOLD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-etf-kd-gold-cross.properties";

    /**
     * 用于STOCK_WEEK表相关操作的属性文件
     */
    protected static final String STOCK_WEEK_PROPERTIES =
            DIR + "/src/main/resources/stock-week.properties";

    /**
     * 用于绘制STOCK_WEEK表相关图片的属性文件
     */
    protected static final String STOCK_WEEK_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/stock-week-picture.properties";

    /**
     * 用于STOCK_MONTH表相关操作的属性文件
     */
    protected static final String STOCK_MONTH_PROPERTIES =
            DIR + "/src/main/resources/stock-month.properties";

    /**
     * 用于STOCK_INDEX表相关操作的属性文件
     */
    protected static final String STOCK_INDEX_PROPERTIES =
            DIR + "/src/main/resources/stock-index.properties";

    /**
     * 用于STOCK_INDEX表相关图片的属性文件
     */
    protected static final String STOCK_INDEX_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/stock-index-picture.properties";

    /**
     * 用于STOCK_INDEX_WEEKEND表相关操作的属性文件
     */
    protected static final String STOCK_INDEX_WEEK_PROPERTIES =
            DIR + "/src/main/resources/stock-index-week.properties";

    /**
     * 用于STOCK_INDEX_WEEKEND表相关图片的属性文件
     */
    protected static final String STOCK_INDEX_WEEK_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/stock-index-week-picture.properties";

    /**
     * 用于STOCK_INDEX_WEEKEND表相关操作的属性文件
     */
    protected static final String STOCK_INDEX_WEEKEND_PROPERTIES =
            DIR + "/src/main/resources/stock-week.properties";

    /**
     * 用于BOARD_INDEX表相关操作的属性文件
     */
    protected static final String BOARD_INDEX_PROPERTIES =
            DIR + "/src/main/resources/board-index.properties";

    /**
     * 用于板块BOARD相关图表的属性文件
     */
    protected static final String BOARD_INDEX_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/board-index-picture.properties";

    /**
     * 用于生成报告时使用的属性文件
     */
    protected static final String REPORT_PROPERTIES = DIR + "/src/main/resources/report.properties";

    /**
     * 用于生成报告中的图标时使用的属性文件
     */
    protected static final String REPORT_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/report-picture.properties";

    /**
     * 模型算法的属性文件
     */
    protected static final String MODEL_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-picture.properties";

    /**
     * 四个指数、四个模型算法的属性文件
     */
    protected static final String MODEL_STOCK_INDEX_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-stock-index-picture.properties";

    /**
     * 使用日线级别在21日，34日，55日，89日，144日，233日，377日和610日内，上涨的股票的百分之多少的模型算法的属性文件
     */
    protected static final String MODEL_GOLD_CROSS_DEAD_CROSS_ANALYSIS_PROPERTIES =
            DIR + "/src/main/resources/model-gold-cross-dead-cross-analysis.properties";

    /**
     * 用于日线级别MACD金叉模型算法的属性文件
     */
    protected static final String MODEL_MACD_GOLD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-macd-gold-cross.properties";

    /**
     * 用于日线级别MACD死叉模型算法的属性文件
     */
    protected static final String MODEL_MACD_DEAD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-macd-dead-cross.properties";

    /**
     * 用于日线级别KD金叉模型算法的属性文件
     */
    protected static final String MODEL_KD_GOLD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-kd-gold-cross.properties";

    /**
     * 用于日线级别使用成交量/成交额突破MA250模型算法模型算法的属性文件
     */
    protected static final String MODEL_VOLUME_TURNOVER_UP_MA250_PROPERTIES =
            DIR + "/src/main/resources/model-volume-turnover-up-ma250.properties";

    /**
     * 用于日线级别KD死叉模型算法的属性文件
     */
    protected static final String MODEL_KD_DEAD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-kd-dead-cross.properties";

    /**
     * 用于平均K线上涨趋势的属性文件
     */
    protected static final String MODEL_HEI_KIN_ASHI_UP_DOWN_PROPERTIES =
            DIR + "/src/main/resources/model-hei-kin-ashi-up-down.properties";

    /**
     * 用于平均K线下跌趋势的属性文件
     */
    protected static final String MODEL_HEI_KIN_ASHI_DOWN_UP_PROPERTIES =
            DIR + "/src/main/resources/model-hei-kin-ashi-down-up.properties";

    /**
     * 用于日线级别close_price金叉ma5模型算法的属性文件
     */
    protected static final String MODEL_CLOSE_PRICE_MA5_GOLD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-close-price-ma5-gold-cross.properties";

    /**
     * 用于日线级别close_price死叉ma5模型算法的属性文件
     */
    protected static final String MODEL_CLOSE_PRICE_MA5_DEAD_CROSS_PROPERTIES =
            DIR + "/src/main/resources/model-close-price-ma5-dead-cross.properties";

    /**
     * 用于日线级别close_price金叉牛熊线模型算法的属性文件
     */
    protected static final String MODEL_BULL_SHORT_LINE_UP_PROPERTIES =
            DIR + "/src/main/resources/model-bull-short-line-up.properties";

    /**
     * 用于日线级别close_price金叉牛熊线模型算法的属性文件
     */
    protected static final String MODEL_ETF_BULL_SHORT_LINE_UP_PROPERTIES =
            DIR + "/src/main/resources/model-etf-bull-short-line-up.properties";

    /**
     * 用于日线级别close_price金叉ma5模型算法相关图片的属性文件
     */
    protected static final String MODEL_CLOSE_PRICE_MA5_GOLD_CROSS_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-close-price-ma5-gold-cross-picture.properties";

    /**
     * 用于日线级别MACD金叉散点图的属性文件
     */
    protected static final String MODEL_MACD_GOLD_CROSS_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-macd-gold-cross-picture.properties";

    /**
     * 用于mdl_week_boll_*表做图的属性文件
     */
    protected static final String MODEL_WEEK_BOLL_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-week-boll-picture.properties";

    /**
     * 用于日线级别在21日，34日，55日，89日，144日，233日，377日和610日内，上涨前n名的股票模型算法的属性文件
     */
    protected static final String MODEL_TOP_STOCK_PROPERTIES =
            DIR + "/src/main/resources/model-top-stock.properties";

    /**
     * 使用日线级别在21日，34日，55日，89日，144日，233日，377日和610日内，上涨的股票的百分之多少的模型算法的属性文件
     */
    protected static final String MODEL_TOP_STOCK_DETAIL_PROPERTIES =
            DIR + "/src/main/resources/model-top-stock-detail.properties";

    /**
     * 使用日线级别在21日，34日，55日，89日，144日，233日，377日和610日内，上涨的股票的百分之多少的图表的属性文件
     */
    protected static final String MODEL_TOP_STOCK_DETAIL_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-top-stock-detail-picture.properties";

    /**
     * 用于日线级别在21日，34日，55日，89日，144日，233日，377日和610日内，每一日平均上涨百分比的图形的属性文件
     */
    protected static final String MODEL_TOP_STOCK_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-top-stock-picture.properties";

    /**
     * 用于日线级别次新股平均价格图形的属性文件
     */
    protected static final String MODEL_SUB_NEW_STOCK_PICTURE_PROPERTIES =
            DIR + "/src/main/resources/model-sub-new-stock-picture.properties";

    /**
     * 用于日线级别股票分析的属性文件
     */
    protected static final String MODEL_STOCK_ANALYSIS =
            DIR + "/src/main/resources/model-stock-analysis.properties";

    /**
     * 用于日线级别股票分析的图形的属性文件
     */
    protected static final String MODEL_STOCK_ANALYSIS_PICTURE =
            DIR + "/src/main/resources/model-stock-analysis-picture.properties";

    /**
     * 用于周线级别股票分析的属性文件
     */
    protected static final String MODEL_STOCK_WEEK_ANALYSIS =
            DIR + "/src/main/resources/model-stock-week-analysis.properties";

    /**
     * 用于周线级别股票分析的图形的属性文件
     */
    protected static final String MODEL_STOCK_WEEK_ANALYSIS_PICTURE =
            DIR + "/src/main/resources/model-stock-week-analysis-picture.properties";

    /**
     * 用于月线级别股票分析的属性文件
     */
    protected static final String MODEL_STOCK_MONTH_ANALYSIS =
            DIR + "/src/main/resources/model-stock-month-analysis.properties";

    /**
     * 使用模型过滤股票的配置文件
     */
    protected static final String MODEL_FILTER_PROPERTIES =
            DIR + "/src/main/resources/model-filter.properties";

    /**
     * 用于备份的配置文件
     */
    protected static final String BACKUP_PROPERTIES = DIR + "/src/main/resources/backup.properties";

    /**
     * 用于多线程的配置文件
     */
    protected static final String THREAD_PROPERTIES = DIR + "/src/main/resources/thread.properties";

    /**
     * 用于和工程相关的配置文件
     */
    protected static final String PROJECT_PROPERTIES = DIR + "/src/main/resources/project.properties";

    /**
     * 用于打印的配置文件
     */
    protected static final String PRINT_PROPERTIES = DIR + "/src/main/resources/print.properties";

    /**
     * 将数据库的数据以json文件的格式进行备份的目录
     */
    protected static final String JSON_DATA_BACKUP_DIR = "/mywork/gitcode-repository/backup/ADAM/json_data/";

    /**
     * 备份项目代码的目录
     */
    protected static final String CODE_BACKUP_DIR = "/mywork/gitcode-repository/backup/ADAM/code/";

    /**
     * 备份数据库dmp文件的目录
     */
    protected static final String DUMP_BACKUP_DIR = "C:/mywork/gitcode-repository/backup/ADAM/dump_file/";

    /**
     * 从网络上获取数据，让后将其存储在这个JSON文件中
     */
    protected static final String ALL_RECORDS_IN_JSON =
            DIR + "/src/main/resources/output/ALL_RECORDS_IN_JSON.json";

    /**
     * 用于存储图表的目录，不包括文件名
     */
    protected static final String PICTURE_PATH = DIR + "/src/main/resources/picture/";

    /**
     * 表示创建的图片的格式
     */
    protected static final String PICTURE_FORMAT = ".jpeg";

    /**
     * 从网络上获取四种指数的数据，将其存储在index_data.txt中，再删除STOCK_INDEX表中的数据，最后将index_data.txt中的数据插入到STOCK_INDEX表中
     */
    protected static final String INDEX_DATE = DIR + "/src/main/resources/output/index_data.txt";

    /**
     * 从网络上获取股票数据的api接口，前半部分
     */
    protected static final String DATA_SOURCE_URL_PREFIX_163 = "http://quotes.money.163.com/service/chddata.html?code=";

    /**
     * 从网络上获取股票的实时交易数据，前半部分
     */
    protected static final String HQ_SINAJS_URL_PREFIX = "https://stock.xueqiu.com/v5/stock/realtime/quotec.json?symbol=";

    /**
     * pdf报告的路径和文件名称(前缀)
     */
    protected static final String REPORT_PREFFIX = DIR + "/src/main/resources/report/adam_report_";

    /**
     * pdf报告的路径和文件名称(后缀)
     */
    protected static final String REPORT_SUFFIX = ".pdf";

    /**
     * 打印机器人买卖建议的文件
     */
    protected static final String PRINT_ROBOT_BUY_SELL_SUGGESTION =
            DIR + "/src/main/resources/output/PRINT_ROBOT_BUY_SELL_SUGGESTION.txt";

    /**
     * 打印真实交易买卖建议的文件
     */
    protected static final String PRINT_REAL_BUY_SELL_SUGGESTION =
            DIR + "/src/main/resources/output/PRINT_REAL_BUY_SELL_SUGGESTION.txt";

    /**
     * 表示报告中的图片
     */
    protected static Image jpg = null;

    /**
     * 生成在目录中的图片的宽度
     */
    protected static final int imageWidth = 1900;

    /**
     * 生成在目录中的图片的高度
     */
    protected static final int IMAGE_HEIGHT = 1300;

    /**
     * pdf报告中图片的宽度
     */
    protected static final int reportImageWidth = 500;

    /**
     * pdf报告中图片的高度
     */
    protected static final int reportImageHeigh = 350;

    /********************************************* 外汇用到的成员变量和方法 **********************************************/
    /**
     * ForeignExchangeRecordDao类的实例
     */
    protected ForeignExchangeRecordDao foreignExchangeRecordDao = new ForeignExchangeRecordDaoImpl();

    /**
     * ForeignExchangeDao类的实例
     */
    protected ForeignExchangeDao foreignExchangeDao = new ForeignExchangeDaoImpl();

    /**
     * 用于FOREIGN_EXCHANGE表相关操作的属性文件
     */
    protected static final String FOREIGN_EXCHANGE_PROPERTIES =
            DIR + "/src/main/resources/foreign-exchange.properties";

    /**
     * 用于存储货币对csv文件的路径
     */
    protected static final String FOREIGN_EXCHANGE_DATA_PATH =
            DIR + "/src/main/resources/foreign_exchange_data";

    /********************************************* 构造函数 **********************************************/
    @SuppressWarnings("unchecked")
    public BaseServiceImpl() {
        entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
        className = entityClass.getName();
        logger = Logger.getLogger(entityClass);
    }

    /********************************************* 实现接口方法 **********************************************/
    /**
     * 创建MACD Gold Cross图表时，采用归一化方法，调整平均收盘价
     *
     * @param original
     * @param max
     * @param min
     * @return
     */
    public double averageCloseAfterAdjust(double original, double max, double min) {
        return (original - min) / (max - min) * 100;
    }

    /**
     * 创建MACD图表时，用于调整平均收盘价
     *
     * @param maxClose
     * @param minClose
     * @param maxDifDea
     * @param minDifDea
     * @param currentClose
     * @return
     */
    public double adjustData(double maxClose, double minClose, double maxDifDea,
                             double minDifDea, double currentClose) {
        return (maxDifDea - minDifDea) / (maxClose - minClose) * (currentClose - minClose);
    }

    /**
     * 在控制台打印字符串，并将字符串收集到StringBuffer中
     *
     * @param stringBuffer
     * @param message
     */
    public void printlnAndCollectString(StringBuffer stringBuffer, String message) {
        if (null != message) {
            System.out.println(message);
            stringBuffer.append(message).append("<br>");
        } else {
            System.out.println();
            stringBuffer.append("<br>");
        }
    }

    /********************************************* 定义的抽象方法 **********************************************/
    /**
     * 将List类型转换为String类型
     *
     * @param list
     * @return
     */
    public abstract String listToString(List list);

    /********************************************* 通用的方法 **********************************************/
    /**
     * 以json格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionByJson_successRate(String printDate) {
        logger.info("以json方式，打印买卖建议，日期为【" + printDate + "】");

        // 做多
        // 查找卖出建议和买入建议
        List<RobotStockTransactionRecord> bullSellSuggestionList = robotStockTransactionRecordDao
                .getBullRobotStockTransactionRecordListForSell(printDate);
        List<RobotStockTransactionRecord> bullBuySuggestionList = robotStockTransactionRecordDao
                .getBullRobotStockTransactionRecordListForBuy(printDate);

        // 做空
        // 查找买入建议和卖出建议
        List<RobotStockTransactionRecord> shortSellSuggestionList = robotStockTransactionRecordDao
                .getShortRobotStockTransactionRecordListForSell(printDate);
        List<RobotStockTransactionRecord> shortBuySuggestionList = robotStockTransactionRecordDao
                .getShortRobotStockTransactionRecordListForBuy(printDate);

        RobotBuySellSuggestionByJson_successRate robotBuySellSuggestionByJsonSuccessRate = new RobotBuySellSuggestionByJson_successRate();
        robotBuySellSuggestionByJsonSuccessRate.setBullSellSuggestionList(bullSellSuggestionList);
        robotBuySellSuggestionByJsonSuccessRate.setBullBuySuggestionList(bullBuySuggestionList);
        robotBuySellSuggestionByJsonSuccessRate.setShortSellSuggestionList(shortSellSuggestionList);
        robotBuySellSuggestionByJsonSuccessRate.setShortBuySuggestionList(shortBuySuggestionList);

        // 格式化为json字符串
        String prettyJson = JSON
                .toJSONString(robotBuySellSuggestionByJsonSuccessRate, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return prettyJson;
    }

    /**
     * 以简单文本格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionBySimple_successRate(String printDate) {
        logger.info("以简单文本格式，打印买卖建议，日期为【" + printDate + "】");

        // 查找卖出建议和买入建议
        List<SellSuggestion> sellSuggestionList = robotStockTransactionRecordDao
                .getSellSuggestionList(printDate);
        List<BuySuggestion> buySuggestionList = robotStockTransactionRecordDao
                .getBuySuggestionList(printDate);

        RobotBuySellSuggestionBySimple_successRate robotBuySellSuggestionBySimpleSuccessRate = new RobotBuySellSuggestionBySimple_successRate();
        robotBuySellSuggestionBySimpleSuccessRate.setSellSuggestionList(sellSuggestionList);
        robotBuySellSuggestionBySimpleSuccessRate.setBuySuggestionList(buySuggestionList);

        // 下面这个格式可能还需要改
        // 格式化为简单文本格式
        String simpleString = JSON
                .toJSONString(robotBuySellSuggestionBySimpleSuccessRate, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return simpleString;
    }

    /**
     * 以json格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionByJson_averageGoldCrossDeadCross(String printDate) {
        logger.info("以json方式，打印买卖建议，日期为【" + printDate + "】");

        // 做多
        // 查找卖出建议和买入建议
        List<Robot2StockTransactRecord> bullSellSuggestionList = robot2StockTransactRecordDao
                .getBullRobotStockTransactRecordListForSell(printDate);
        List<Robot2StockTransactRecord> bullBuySuggestionList = robot2StockTransactRecordDao
                .getBullRobotStockTransactRecordListForBuy(printDate);

        // 做空
        // 查找买入建议和卖出建议
        List<Robot2StockTransactRecord> shortSellSuggestionList = robot2StockTransactRecordDao
                .getShortRobotStockTransactRecordListForSell(printDate);
        List<Robot2StockTransactRecord> shortBuySuggestionList = robot2StockTransactRecordDao
                .getShortRobotStockTransactRecordListForBuy(printDate);

        RobotBuySellSuggestionByJson_averageGoldCrossDeadCross robotBuySellSuggestionByJsonAverageGoldCrossDeadCross = new RobotBuySellSuggestionByJson_averageGoldCrossDeadCross();
        robotBuySellSuggestionByJsonAverageGoldCrossDeadCross.setBullSellSuggestionList(bullSellSuggestionList);
        robotBuySellSuggestionByJsonAverageGoldCrossDeadCross.setBullBuySuggestionList(bullBuySuggestionList);
        robotBuySellSuggestionByJsonAverageGoldCrossDeadCross.setShortSellSuggestionList(shortSellSuggestionList);
        robotBuySellSuggestionByJsonAverageGoldCrossDeadCross.setShortBuySuggestionList(shortBuySuggestionList);

        // 格式化为json字符串
        String prettyJson = JSON
                .toJSONString(robotBuySellSuggestionByJsonAverageGoldCrossDeadCross, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return prettyJson;
    }

    /**
     * 以简单文本格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionBySimple_averageGoldCrossDeadCross(String printDate) {
        logger.info("以简单文本格式，打印买卖建议，日期为【" + printDate + "】");

        // 查找卖出建议和买入建议
        List<SellSuggestion> sellSuggestionList = robot2StockTransactRecordDao
                .getSellSuggestionList_averageGoldCrossDeadCross(printDate);
        List<BuySuggestion> buySuggestionList = robot2StockTransactRecordDao
                .getBuySuggestionList_averageGoldCrossDeadCross(printDate);

        RobotBuySellSuggestionBySimple_averageGoldCrossDeadCross robotBuySellSuggestionBySimpleAverageGoldCrossDeadCross = new RobotBuySellSuggestionBySimple_averageGoldCrossDeadCross();
        robotBuySellSuggestionBySimpleAverageGoldCrossDeadCross.setSellSuggestionList(sellSuggestionList);
        robotBuySellSuggestionBySimpleAverageGoldCrossDeadCross.setBuySuggestionList(buySuggestionList);

        // 下面这个格式可能还需要改
        // 格式化为简单文本格式
        String simpleString = JSON
                .toJSONString(robotBuySellSuggestionBySimpleAverageGoldCrossDeadCross, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return simpleString;
    }

    /**
     * 以json格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionByJson_weekBoll(String printDate) {
        logger.info("以json方式，打印买卖建议，日期为【" + printDate + "】");

        // 做多
        // 查找卖出建议和买入建议
        List<Robot3StockTransactRecord> bullSellSuggestionList = robot3StockTransactRecordDao
                .getBullRobotStockTransactRecordListForSell(printDate);
        List<Robot3StockTransactRecord> bullBuySuggestionList = robot3StockTransactRecordDao
                .getBullRobotStockTransactRecordListForBuy(printDate);

        // 做空
        // 查找买入建议和卖出建议
        List<Robot3StockTransactRecord> shortSellSuggestionList = robot3StockTransactRecordDao
                .getShortRobotStockTransactRecordListForSell(printDate);
        List<Robot3StockTransactRecord> shortBuySuggestionList = robot3StockTransactRecordDao
                .getShortRobotStockTransactRecordListForBuy(printDate);

        RobotBuySellSuggestionByJson_weekBoll robotBuySellSuggestionByJson_weekBoll = new RobotBuySellSuggestionByJson_weekBoll();
        robotBuySellSuggestionByJson_weekBoll.setBullSellSuggestionList(bullSellSuggestionList);
        robotBuySellSuggestionByJson_weekBoll.setBullBuySuggestionList(bullBuySuggestionList);
        robotBuySellSuggestionByJson_weekBoll.setShortSellSuggestionList(shortSellSuggestionList);
        robotBuySellSuggestionByJson_weekBoll.setShortBuySuggestionList(shortBuySuggestionList);

        // 格式化为json字符串
        String prettyJson = JSON
                .toJSONString(robotBuySellSuggestionByJson_weekBoll, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return prettyJson;
    }

    /**
     * 以简单文本格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionBySimple_weekBoll(String printDate) {
        logger.info("以简单文本格式，打印买卖建议，日期为【" + printDate + "】");

        // 查找卖出建议和买入建议
        List<SellSuggestion> sellSuggestionList = robot3StockTransactRecordDao
                .getSellSuggestionList_weekBoll(printDate);
        List<BuySuggestion> buySuggestionList = robot3StockTransactRecordDao
                .getBuySuggestionList_weekBoll(printDate);

        RobotBuySellSuggestionBySimple_weekBoll robotBuySellSuggestionBySimple_weekBoll = new RobotBuySellSuggestionBySimple_weekBoll();
        robotBuySellSuggestionBySimple_weekBoll.setSellSuggestionList(sellSuggestionList);
        robotBuySellSuggestionBySimple_weekBoll.setBuySuggestionList(buySuggestionList);

        // 下面这个格式可能还需要改
        // 格式化为简单文本格式
        String simpleString = JSON
                .toJSONString(robotBuySellSuggestionBySimple_weekBoll, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return simpleString;
    }

    /**
     * 以json格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionByJson_weekKD(String printDate) {
        logger.info("以json方式，打印买卖建议，日期为【" + printDate + "】");

        // 做多
        // 查找卖出建议和买入建议
        List<Robot4StockTransactRecord> bullSellSuggestionList = robot4StockTransactRecordDao
                .getBullRobotStockTransactRecordListForSell(printDate);
        List<Robot4StockTransactRecord> bullBuySuggestionList = robot4StockTransactRecordDao
                .getBullRobotStockTransactRecordListForBuy(printDate);

        // 做空
        // 查找买入建议和卖出建议
        List<Robot4StockTransactRecord> shortSellSuggestionList = robot4StockTransactRecordDao
                .getShortRobotStockTransactRecordListForSell(printDate);
        List<Robot4StockTransactRecord> shortBuySuggestionList = robot4StockTransactRecordDao
                .getShortRobotStockTransactRecordListForBuy(printDate);

        RobotBuySellSuggestionByJson_weekKD robotBuySellSuggestionByJson_weekKD = new RobotBuySellSuggestionByJson_weekKD();
        robotBuySellSuggestionByJson_weekKD.setBullSellSuggestionList(bullSellSuggestionList);
        robotBuySellSuggestionByJson_weekKD.setBullBuySuggestionList(bullBuySuggestionList);
        robotBuySellSuggestionByJson_weekKD.setShortSellSuggestionList(shortSellSuggestionList);
        robotBuySellSuggestionByJson_weekKD.setShortBuySuggestionList(shortBuySuggestionList);

        // 格式化为json字符串
        String prettyJson = JSON
                .toJSONString(robotBuySellSuggestionByJson_weekKD, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return prettyJson;
    }

    /**
     * 以简单文本格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionBySimple_weekKD(String printDate) {
        logger.info("以简单文本格式，打印买卖建议，日期为【" + printDate + "】");

        // 查找卖出建议和买入建议
        List<SellSuggestion> sellSuggestionList = robot4StockTransactRecordDao
                .getSellSuggestionList_weekKD(printDate);
        List<BuySuggestion> buySuggestionList = robot4StockTransactRecordDao
                .getBuySuggestionList_weekKD(printDate);

        RobotBuySellSuggestionBySimple_weekKD robotBuySellSuggestionBySimple_weekKD = new RobotBuySellSuggestionBySimple_weekKD();
        robotBuySellSuggestionBySimple_weekKD.setSellSuggestionList(sellSuggestionList);
        robotBuySellSuggestionBySimple_weekKD.setBuySuggestionList(buySuggestionList);

        // 下面这个格式可能还需要改
        // 格式化为简单文本格式
        String simpleString = JSON
                .toJSONString(robotBuySellSuggestionBySimple_weekKD, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return simpleString;
    }

    /**
     * 以json格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionByJson_multipleMethod(String printDate) {
        logger.info("以json方式，打印买卖建议，日期为【" + printDate + "】");

        // 做多
        // 查找卖出建议和买入建议
        List<Robot5StockTransactRecord> bullSellSuggestionList = robot5StockTransactRecordDao
                .getBullRobotStockTransactRecordListForSell(printDate);
        List<Robot5StockTransactRecord> bullBuySuggestionList = robot5StockTransactRecordDao
                .getBullRobotStockTransactRecordListForBuy(printDate);

        // 做空
        // 查找买入建议和卖出建议
        List<Robot5StockTransactRecord> shortSellSuggestionList = robot5StockTransactRecordDao
                .getShortRobotStockTransactRecordListForSell(printDate);
        List<Robot5StockTransactRecord> shortBuySuggestionList = robot5StockTransactRecordDao
                .getShortRobotStockTransactRecordListForBuy(printDate);

        RobotBuySellSuggestionByJson_multipleMethod robotBuySellSuggestionByJson_multipleMethod = new RobotBuySellSuggestionByJson_multipleMethod();
        robotBuySellSuggestionByJson_multipleMethod.setBullSellSuggestionList(bullSellSuggestionList);
        robotBuySellSuggestionByJson_multipleMethod.setBullBuySuggestionList(bullBuySuggestionList);
        robotBuySellSuggestionByJson_multipleMethod.setShortSellSuggestionList(shortSellSuggestionList);
        robotBuySellSuggestionByJson_multipleMethod.setShortBuySuggestionList(shortBuySuggestionList);

        // 格式化为json字符串
        String prettyJson = JSON
                .toJSONString(robotBuySellSuggestionByJson_multipleMethod, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return prettyJson;
    }

    /**
     * 以简单文本格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionBySimple_multipleMethod(String printDate) {
        logger.info("以简单文本格式，打印买卖建议，日期为【" + printDate + "】");

        // 查找卖出建议和买入建议
        List<SellSuggestion> sellSuggestionList = robot5StockTransactRecordDao
                .getSellSuggestionList_multipleMethod(printDate);
        List<BuySuggestion> buySuggestionList = robot5StockTransactRecordDao
                .getBuySuggestionList_multipleMethod(printDate);

        RobotBuySellSuggestionBySimple_multipleMethod robotBuySellSuggestionBySimple_multipleMethod = new RobotBuySellSuggestionBySimple_multipleMethod();
        robotBuySellSuggestionBySimple_multipleMethod.setSellSuggestionList(sellSuggestionList);
        robotBuySellSuggestionBySimple_multipleMethod.setBuySuggestionList(buySuggestionList);

        // 下面这个格式可能还需要改
        // 格式化为简单文本格式
        String simpleString = JSON
                .toJSONString(robotBuySellSuggestionBySimple_multipleMethod, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return simpleString;
    }

    /**
     * 以json格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionByJson_etf12YearHighestAverageProfitRate(String printDate) {
        logger.info("以json方式，打印买卖建议，日期为【" + printDate + "】");

        // 做多
        // 查找卖出建议和买入建议
        List<Robot6StockTransactRecord> bullSellSuggestionList = robot6EtfTransactRecordDao
                .getBullRobotEtfTransactRecordListForSell(printDate);
        List<Robot6StockTransactRecord> bullBuySuggestionList = robot6EtfTransactRecordDao
                .getBullRobotEtfTransactRecordListForBuy(printDate);

        // 做空
        // 查找买入建议和卖出建议
        List<Robot6StockTransactRecord> shortSellSuggestionList = robot6EtfTransactRecordDao
                .getShortRobotEtfTransactRecordListForSell(printDate);
        List<Robot6StockTransactRecord> shortBuySuggestionList = robot6EtfTransactRecordDao
                .getShortRobotEtfTransactRecordListForBuy(printDate);

        RobotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate robotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate = new RobotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate();
        robotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate.setBullSellSuggestionList(bullSellSuggestionList);
        robotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate.setBullBuySuggestionList(bullBuySuggestionList);
        robotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate.setShortSellSuggestionList(shortSellSuggestionList);
        robotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate.setShortBuySuggestionList(shortBuySuggestionList);

        // 格式化为json字符串
        String prettyJson = JSON
                .toJSONString(robotBuySellSuggestionByJson_etf12YearHighestAverageProfitRate, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return prettyJson;
    }

    /**
     * 以简单文本格式，返回买卖建议
     *
     * @param printDate
     */
    protected String printBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate(String printDate) {
        logger.info("以简单文本格式，打印买卖建议，日期为【" + printDate + "】");

        // 查找卖出建议和买入建议
        List<SellSuggestion> sellSuggestionList = robot6EtfTransactRecordDao
                .getSellSuggestionList_etf12YearHighestAverageProfitRate(printDate);
        List<BuySuggestion> buySuggestionList = robot6EtfTransactRecordDao
                .getBuySuggestionList_etf12YearHighestAverageProfitRate(printDate);

        RobotBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate robotBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate = new RobotBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate();
        robotBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate.setSellSuggestionList(sellSuggestionList);
        robotBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate.setBuySuggestionList(buySuggestionList);

        // 下面这个格式可能还需要改
        // 格式化为简单文本格式
        String simpleString = JSON
                .toJSONString(robotBuySellSuggestionBySimple_etf12YearHighestAverageProfitRate, SerializerFeature.PrettyFormat,
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteDateUseDateFormat);

        return simpleString;
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullStockInRobotAccount(Integer holdStockNumber) {
        Integer rowNumber = robotAccountDao.count();
        Integer sumHoldStockNumber = robotAccountDao.sumHoldStockNumber();
        if (holdStockNumber.intValue() * rowNumber.intValue() == sumHoldStockNumber.intValue()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullStockInRobot2Account(Integer holdStockNumber) {
        Integer rowNumber = robot2AccountDao.count();
        Integer sumHoldStockNumber = robot2AccountDao.sumHoldStockNumber();
        if (holdStockNumber.intValue() * rowNumber.intValue() == sumHoldStockNumber.intValue()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullStockInRobot3Account(Integer holdStockNumber) {
        Integer rowNumber = robot3AccountDao.count();
        Integer sumHoldStockNumber = robot3AccountDao.sumHoldStockNumber();
        if (holdStockNumber.intValue() * rowNumber.intValue() == sumHoldStockNumber.intValue()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullStockInRobot4Account(Integer holdStockNumber) {
        Integer rowNumber = robot4AccountDao.count();
        Integer sumHoldStockNumber = robot4AccountDao.sumHoldStockNumber();
        if (holdStockNumber.intValue() * rowNumber.intValue() == sumHoldStockNumber.intValue()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullStockInRobot5Account(Integer holdStockNumber) {
        Integer rowNumber = robot5AccountDao.count();
        Integer sumHoldStockNumber = robot5AccountDao.sumHoldStockNumber();
        if (holdStockNumber.intValue() * rowNumber.intValue() == sumHoldStockNumber.intValue()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullEtfInRobot6Account(Integer holdEtfNumber) {
        Integer rowNumber = robot6AccountDao.count();
        Integer sumHoldEtfNumber = robot6AccountDao.sumHoldEtfNumber();
        if (holdEtfNumber.intValue() * rowNumber.intValue() == sumHoldEtfNumber.intValue()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * 判断现在是否是满仓
     *
     * @return
     */
    protected Boolean isFullStockInReal4Account(Integer holdStockNumber) {
        Real4Account real4Account = real4AccountDao.findAll().get(0);
        if (real4Account.getHoldStockNumber().equals(holdStockNumber)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
