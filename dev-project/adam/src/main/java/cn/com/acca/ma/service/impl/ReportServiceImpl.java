package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.model.StockTransactionData;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.common.util.StringUtil;
import cn.com.acca.ma.enumeration.ReportInfo;
import cn.com.acca.ma.model.Board;
import cn.com.acca.ma.model.Report;
import cn.com.acca.ma.model.StockMovingAverage;
import cn.com.acca.ma.service.ReportService;

public class ReportServiceImpl extends BaseServiceImpl<ReportServiceImpl,StockMovingAverage> implements ReportService {
	private Document document = new Document();
	private PdfWriter writer;
	
	// 报告生成的日期
	private String reportGenerateDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.generate.date");
	
	// 计算某一日股票样本总数，上涨家数，持平家数和下跌家数的日期
	private String reportStockSampleSum=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stock.sample.sum");
	private String reportStockUpNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stock.up.number");
	private String reportStockMiddleNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stock.middle.number");
	private String reportStockDownNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stock.down.number");
	
	// 计算某一日股票多头排列和空头排列家数的日期
	private String reportStockBullRankNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stock.bull.rank.number");
	private String reportStockShortOrderNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stock.short.order.number");
	
	// 28分化图的开始日期和结束日期
	private String reportPicture28BeginDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.28.beginDate");
	private String reportPicture28EndDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.28.endDate");
	
	// 多空排列图的开始日期和结束日期
	private String reportPictureTenYearBullRankShortOrderBeginDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.tenYear.bullRank.shortOrder.beginDate");
	private String reportPictureTenYearBullRankShortOrderEndDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.tenYear.bullRank.shortOrder.endDate");
	private String reportPictureHalfYearBullRankShortOrderBeginDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.halfYear.bullRank.shortOrder.beginDate");
	private String reportPictureHalfYearBullRankShortOrderEndDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.halfYear.bullRank.shortOrder.endDate");
	
	// 收盘价STOCK_CLOSE图的开始日期和结束日期
	private String reportPictureTenYearCloseBeginDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.tenYear.close.beginDate");
	private String reportPictureTenYearCloseEndDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.tenYear.close.endDate");
	private String reportPictureHalfYearCloseBeginDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.halfYear.close.beginDate");
	private String reportPictureHalfYearCloseEndDate = PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES,"reportPicture.halfYear.close.endDate");
	
	// 日线级别反转股票的日期和下跌幅度
	private String reportReverseStockDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.reverse.stock.date");
	private String reportReverseStockRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.reverse.stock.rate");
	
	// 日线级别MACD图的开始日期和结束日期
	private String reportPictureDayMacdBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.day.macd.beginDate");
	private String reportPictureDayMacdEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.day.macd.endDate");
	
	// 日线级别蜘蛛雷达图的日期，天数和阈值
	private String reportPictureSpiderWebDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.spiderWeb.date");
	private String reportPictureSpiderWebDateNumber=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.spiderWeb.dateNumber");
	private String reportPictureSpiderWebLimitRate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.spiderWeb.limitRate");
	
	// 周线级别KD指标图的开始日期和结束日期
	private String reportPictureWeekendKDBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.weekend.kd.beginDate");
	private String reportPictureWeekendKDEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.weekend.kd.endDate");
	// 日线级别各个板块的K线图
	
	private String reportPictureBoardIndexBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.beginDate");
	private String reportPictureBoardIndexEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.endDate");
	
	// 日线级别板块收盘价图的开始日期和结束日期（最近10年的图和半年的图）
	private String reportPictureAllBoardIndexTenYearCloseBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.tenYear.close.beginDate");
	private String reportPictureAllBoardIndexTenYearCloseEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.tenYear.close.endDate");
	private String reportPictureAllBoardIndexHalfYearCloseBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.halfYear.close.beginDate");
	private String reportPictureAllBoardIndexHalfYearCloseEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.halfYear.close.endDate");
	
	// 日线级别板块5日和10日涨跌图的开始日期和结束日期（最近10年的图和半年的图）
	private String reportPictureAllBoardIndexTenYearFiveAndTenDayRateBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.tenYear.fiveAndTenDayRate.beginDate");
	private String reportPictureAllBoardIndexTenYearFiveAndTenDayRateEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.tenYear.fiveAndTenDayRate.endDate");
	private String reportPictureAllBoardIndexHalfYearFiveAndTenDayRateBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.halfYear.fiveAndTenDayRate.beginDate");
	private String reportPictureAllBoardIndexHalfYearFiveAndTenDayRateEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.boardIndex.halfYear.fiveAndTenDayRate.endDate");
	
	// 日线级别板块股票涨停跌停数量图的开始日期和结束日期
	private String reportPictureLimitUpDownBeginDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.limitUpDown.beginDate");
	private String reportPictureLimitUpDownEndDate=PropertiesUtil.getValue(REPORT_PICTURE_PROPERTIES, "reportPicture.limitUpDown.endDate");
	
	// 计算日线级别MACD金叉的日期和下跌幅度
	private String reportMacdUpBelowDate = PropertiesUtil.getValue(REPORT_PROPERTIES, "report.macd.up.below.date");
	private String reportMacdUpBelowRate = PropertiesUtil.getValue(REPORT_PROPERTIES, "report.macd.up.below.rate");
	
	// 计算某一周MACD是否底背离的日期和日期阈值
	private String reportWeekendMacdEndDeviateBeginDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.weekend.macd.end.deviate.beginDate");
	private String reportWeekendMacdEndDeviateEndDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.weekend.macd.end.deviate.endDate");
	private String reportWeekendMacdEndDeviateRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.weekend.macd.end.deviate.rate");
	
	// 计算某一周KD指标是否金叉的日期和阈值
	private String reportWeekendKDUpBeginDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.weekend.kd.up.beginDate");
	private String reportWeekendKDUpEndDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.weekend.kd.up.endDate");
	private String reportWeekendKDUpCrossPoint=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.weekend.kd.up.crossPoint");
	
	// 查找日线级别异常股票的日期和倍数
	private String reportAbnormalStockDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.abnormal.stock.date");
	private String reportAbnormalStockRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.abnormal.stock.rate");
	
	// 查找日线级别MACD最低的股票的日期和个数
	private String reportLowestMacdStockDate=PropertiesUtil.getValue(REPORT_PROPERTIES,"report.lowest.macd.stock.date");
	private String reportLowestMacdStockNumber=PropertiesUtil.getValue(REPORT_PROPERTIES,"report.lowest.macd.stock.number");
	private String reportLowestMacdStockInterval=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.lowest.macd.stock.interval");
	
	// 计算某一周KD指标最低的股票的开始日期，结束日期，时间跨度和股票个数
    private String reportLowestKdStockWeekBeginDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stockWeek.lowest.kd.beginDate");
    private String reportLowestKdStockWeekEndDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stockWeek.lowest.kd.endDate");
    private String reportLowestKdStockWeekTimeSpan=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stockWeek.lowest.kd.timeSpan");
    private String reportLowestKdStockWeekStockNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.stockWeek.lowest.kd.number");
	
    // 查找日线级别股票收盘价到达250日均线时的日期，临近率和下跌率
	private String reportYearLineSupportDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.yearLine.support.date");
	private String reportYearLineSupportNearRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.yearLine.support.nearRate");
	private String reportYearLineSupportDownRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.yearLine.support.downRate");
	
	// 查找日线级别股票收盘价到达120日均线时的日期，临近率和下跌率
	private String reportHalfYearLineSupportDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.halfYearLine.support.date");
	private String reportHalfYearLineSupportNearRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.halfYearLine.support.nearRate");
	private String reportHalfYearLineSupportDownRate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.halfYearLine.support.downRate");
	
	// 查找日线级别股票某一段时间内上涨股票的前number名和下跌股票的前number名
	private String reportTopStockBeginDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.top.stock.beginDate");
	private String reportTopStockEndDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.top.stock.endDate");
	private String reportTopStockNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.top.stock.number");
	private String reportTopStockWordLimtNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.top.word.limit.num");
	private String reportLastStockBeginDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.last.stock.beginDate");
	private String reportLastStockEndDate=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.last.stock.endDate");
	private String reportLastStockNumber=PropertiesUtil.getValue(REPORT_PROPERTIES, "report.last.stock.number");
	
	/**
	 * 中文字体
	 */
	private BaseFont bfChinese;
	
	/**
	 * 标题字体
	 */
	private Font titleFont;
	
	/**
	 * 段落字体
	 */
	private Font paragraphFont;
	
	/**
	 * 标题
	 */
	private Paragraph title;
	
	/**
	 * 段落内容
	 */
	private Paragraph paragraphContent;
	
	/**
	 * 将报告对象保存到数据库
	 */
	@SuppressWarnings("unchecked")
	public void storeReport(){
		report=new Report();
		
		// 设置报告生成的日期
		report.setReportDate(DateUtil.stringToDate(reportGenerateDate));
		// 设置报告的名称
		report.setReportName(ReportInfo.ADAM_REPORT.getReportName());
		// 设置报告的标题
		report.setReportTitle(ReportInfo.ADAM_REPORT.getReportTitle());
		
		// 设置股票样本总数
		report.setStockSampleSum(BigDecimal.valueOf(reportDao.getStockSampleSumByDate(reportStockSampleSum)));
		// 设置股票上涨家数
		report.setStockUpNumber(BigDecimal.valueOf(reportDao.getStockUpMumberByDate(reportStockUpNumber)));
		// 设置股票持平的家数
		report.setStockMiddleNumber(BigDecimal.valueOf(reportDao.getStockMiddleMumberByDate(reportStockMiddleNumber)));
		// 设置股票下跌的家数
		report.setStockDownNumber(BigDecimal.valueOf(reportDao.getStockDownMumberByDate(reportStockDownNumber)));
		// 设置28分化图开始的日期
		report.setDifferentiationBeginDate(DateUtil.stringToDate(reportPicture28BeginDate));
		// 设置28分化图结束的日期
		report.setDifferentiationEndDate(DateUtil.stringToDate(reportPicture28EndDate));
		
		// 设置多头排列的股票的家数
		report.setBullRankNumber(BigDecimal.valueOf(reportDao.getBullRank(reportStockBullRankNumber).size()));
		// 设置多头排列的股票的信息
		report.setBullRankStockInfo(this.listToString(reportDao.getBullRank(reportStockBullRankNumber)));
		// 设置空头排列的股票的家数
		report.setShortOrderNumber(BigDecimal.valueOf(reportDao.getShortOrder(reportStockShortOrderNumber).size()));
		// 设置空头排列的股票的信息
		report.setShortOrderStockInfo(this.listToString(reportDao.getShortOrder(reportStockShortOrderNumber)));
		// 设置多空排列图的开始日期（最近10年）
		report.setTenYearBullRankShortOrderBeginDate(DateUtil.stringToDate(reportPictureTenYearBullRankShortOrderBeginDate));
		// 设置多空排列图的结束日期（最近10年）
		report.setTenYearBullRankShortOrderEndDate(DateUtil.stringToDate(reportPictureTenYearBullRankShortOrderEndDate));
		// 设置多空排列图的开始日期（最近半年）
		report.setHalfYearBullRankShortOrderBeginDate(DateUtil.stringToDate(reportPictureHalfYearBullRankShortOrderBeginDate));
		// 设置多空排列图的结束日期（最近半年）
		report.setHalfYearBullRankShortOrderEndDate(DateUtil.stringToDate(reportPictureHalfYearBullRankShortOrderEndDate));
		
		// 设置收盘价图STOCK_CLOSE的开始时间（最近10年）
		report.setTenYearCloseBeginDate(DateUtil.stringToDate(reportPictureTenYearCloseBeginDate));
		// 设置收盘价图STOCK_CLOSE的结束时间（最近10年）
		report.setTenYearCloseEndDate(DateUtil.stringToDate(reportPictureTenYearCloseEndDate));
		// 设置收盘价图STOCK_CLOSE的开始时间（最近半年）
		report.setHalfYearCloseBeginDate(DateUtil.stringToDate(reportPictureHalfYearCloseBeginDate));
		// 设置收盘价图STOCK_CLOSE的结束时间（最近半年）
		report.setHalfYearCloseEndDate(DateUtil.stringToDate(reportPictureHalfYearCloseEndDate));
		
		// 设置日线级别MACD图的开始时间
		report.setDayMacdBeginDate(DateUtil.stringToDate(reportPictureDayMacdBeginDate));
		// 设置日线级别MACD图的结束时间
		report.setDayMacdEndDate(DateUtil.stringToDate(reportPictureDayMacdEndDate));
		
		// 设置日线级别蜘蛛雷达图的时间
		report.setSpiderWebDate(DateUtil.stringToDate(reportPictureSpiderWebDate));
		// 设置日线级别蜘蛛雷达图的天数
		report.setSpiderWebDateNumber(new BigDecimal(reportPictureSpiderWebDateNumber));
		// 设置日线级别蜘蛛雷达图的判断股票是否涨停的阈值
		report.setSpiderWebLimitRate(new BigDecimal(reportPictureSpiderWebLimitRate));
		
		// 设置周线级别KD指标图的开始时间
		report.setWeekednKDBeginDate(DateUtil.stringToDate(reportPictureWeekendKDBeginDate));
		// 设置周线级别KD指标图的结束时间
		report.setWeekednKDEndDate(DateUtil.stringToDate(reportPictureWeekendKDEndDate));
		
		// 设置日线级别各个板块的K线图的开始日期
		report.setBoardIndexBeginDate(DateUtil.stringToDate(reportPictureBoardIndexBeginDate));
		// 设置日线级别各个板块的K线图的结束日期
		report.setBoardIndexEndDate(DateUtil.stringToDate(reportPictureBoardIndexEndDate));
		
		// 设置日线级别板块收盘价图的开始日期（最近10年）
		report.setBoardTenCloseEnginDate(DateUtil.stringToDate(reportPictureAllBoardIndexTenYearCloseBeginDate));
		// 设置日线级别板块收盘价图的结束日期（最近10年）
		report.setBoardTenCloseEndDate(DateUtil.stringToDate(reportPictureAllBoardIndexTenYearCloseEndDate));
		// 设置日线级别板块收盘价图的开始日期（半年）
		report.setBoardHalfCloseEnginDate(DateUtil.stringToDate(reportPictureAllBoardIndexHalfYearCloseBeginDate));
		// 设置日线级别板块收盘价图的结束日期（半年）
		report.setBoardHalfCloseEnginDate(DateUtil.stringToDate(reportPictureAllBoardIndexHalfYearCloseEndDate));
		
		// 设置日线级别板块5日和10日涨跌图的开始日期（最近10年）
		report.setTenYearFiveTenDayRateBeginDate(DateUtil.stringToDate(reportPictureAllBoardIndexTenYearFiveAndTenDayRateBeginDate));
		// 设置日线级别板块5日和10日涨跌图的结束日期（最近10年）
		report.setTenYearFiveTenDayRateEndDate(DateUtil.stringToDate(reportPictureAllBoardIndexTenYearFiveAndTenDayRateEndDate));
		// 设置日线级别板块5日和10日涨跌图的开始日期（半年）
		report.setHalfYearFiveTenDayRateBeginDate(DateUtil.stringToDate(reportPictureAllBoardIndexHalfYearFiveAndTenDayRateBeginDate));
		// 设置日线级别板块5日和10日涨跌图的结束日期（半年）
		report.setHalfYearFiveTenDayRateEndDate(DateUtil.stringToDate(reportPictureAllBoardIndexHalfYearFiveAndTenDayRateEndDate));
		
		// 设置日线级别股票涨停跌停数量图的开始日期
		report.setLimitUpDownBeginDate(DateUtil.stringToDate(reportPictureLimitUpDownBeginDate));
		// 设置日线级别股票涨停跌停数量图的结束日期
		report.setLimitUpDownEndDate(DateUtil.stringToDate(reportPictureLimitUpDownEndDate));
		
		// 设置日线级别反转股票的日期
		report.setReverseStockDate(DateUtil.stringToDate(reportReverseStockDate));
		// 设置日线级别反转股票的下跌幅度
		report.setReverseStockRate(new Double(reportReverseStockRate));
		// 设置日线级别反转股票
//		report.setReverseStock(reportDao.selectReverseStock(reportReverseStockDate, Double.parseDouble(reportReverseStockRate)));
		
		// 设置日线级别MACD金叉时的日期
		report.setMacdUpBelowZeroDate(DateUtil.stringToDate(reportMacdUpBelowDate));
		// 设置日线级别MACD金叉时的下跌幅度
		report.setMacdUpBelowZeroRate(new BigDecimal((reportMacdUpBelowRate)));
		// 设置日线级别MACD金叉的股票信息
		report.setMacdUpBelowZeroStock(reportDao.selectMACDGoldCross(reportMacdUpBelowDate, reportMacdUpBelowRate).toString().replace("[","").replace("]", ""));
		
		// 设置某一周MACD底背离的股票信息
		report.setWeekendMacdEndDeviate(reportDao.findWeekendMacdEndDeviate(reportWeekendMacdEndDeviateBeginDate, reportWeekendMacdEndDeviateEndDate,reportWeekendMacdEndDeviateRate));
		// 设置查找周线级别MACD底背离的股票时，使用的日期阈值
		report.setWeekendMacdEndDeviateRate(DateUtil.stringToDate(reportWeekendMacdEndDeviateRate));
		
		// 设置某一周KD指标金叉的开始日期
		report.setWeekendKDUpBeginDate(DateUtil.stringToDate(reportWeekendKDUpBeginDate));
		// 设置某一周KD指标金叉的结束日期
		report.setWeekendKDUpEndDate(DateUtil.stringToDate(reportWeekendKDUpEndDate));
		// 设置某一周KD指标金叉的阈值
		report.setWeekendKDUpCrossPoint(new BigDecimal(reportWeekendKDUpCrossPoint));
		// 设置某一周KD指标金叉时的股票信息
		report.setWeekendKDUpStock(reportDao.selectStockWeekendKDUp(reportWeekendKDUpBeginDate, reportWeekendKDUpEndDate, reportWeekendKDUpCrossPoint).toString());

		// 由于five_day_volatility等列没有数据，下面这个暂时先不做了
		// 设置日线级别异常股票的日期
//		report.setAbnormalStockDate(DateUtil.stringToDate(reportAbnormalStockDate));
//		// 设置日线级别异常股票的倍数
//		report.setAbnormalStockRate(new BigDecimal(reportAbnormalStockRate));
//		// 设置日线级别异常股票的信息
//		report.setAbnormalStock(reportDao.findAbnormalStock(reportAbnormalStockDate, Integer.parseInt(reportAbnormalStockRate)).toString());
		
		// 设置日线级别MACD最低的股票的日期
		report.setLowestMacdStockDate(DateUtil.stringToDate(reportLowestMacdStockDate));
		// 设置日线级别MACD最低的股票的个数
		report.setLowestMacdStockNumber(new BigDecimal(reportLowestMacdStockNumber));
		// 设置日线级别MACD最低的股票
		report.setLowestMacdStock(reportDao.findLowestMacdStock(reportLowestMacdStockDate, Integer.valueOf(reportLowestMacdStockNumber), Integer.valueOf(reportLowestMacdStockInterval)));
		
		// 设置某一周KD指标最低的股票的开始日期
        report.setLowestKdWeekendBeginDate(DateUtil.stringToDate(reportLowestKdStockWeekBeginDate));
        // 设置某一周KD指标最低的股票的结束日期
        report.setLowestKdWeekendEndDate(DateUtil.stringToDate(reportLowestKdStockWeekEndDate));
     // 设置某一周KD指标最低的股票的时间跨度
        report.setLowestKdWeekendTimeSpan(DateUtil.stringToDate(reportLowestKdStockWeekTimeSpan));
        // 设置某一周KD指标最低的股票的股票数量
        report.setLowestKdWeekendStockNumber(new BigDecimal(reportLowestKdStockWeekStockNumber));
        // 设置某一周KD指标最低的股票的股票
        report.setLowestKdWeekendStock(reportDao.findLowestKdStockWeek(reportLowestKdStockWeekBeginDate, reportLowestKdStockWeekEndDate, reportLowestKdStockWeekTimeSpan, Integer.parseInt(reportLowestKdStockWeekStockNumber)));
		
        // 设置日线级别股票收盘价到达250日均线时的日期
        report.setYearLineSupportDate(DateUtil.stringToDate(reportYearLineSupportDate));
        // 设置日线级别股票收盘价到达250日均线时的临近率
        report.setYearLineSupportNearRate(new BigDecimal(reportYearLineSupportNearRate));
        // 设置日线级别股票收盘价到达250日均线时的下跌率
        report.setYearLineSupportDownRate(new BigDecimal(reportYearLineSupportDownRate));
        
	    // 设置日线级别股票收盘价到达120日均线时的日期
        report.setHalfYearLineSupportDate(DateUtil.stringToDate(reportHalfYearLineSupportDate));
        // 设置日线级别股票收盘价到达120日均线时的临近率
        report.setHalfYearLineSupportNearRate(new BigDecimal(reportHalfYearLineSupportNearRate));
        // 设置日线级别股票收盘价到达120日均线时的下跌率
        report.setHalfYearLineSupportDownRate(new BigDecimal(reportHalfYearLineSupportDownRate));
        
        // 设置日线级别从开始日期到结束日期内上涨幅度最多股票的开始日期
        report.setTopStockBeginDate(DateUtil.stringToDate(reportTopStockBeginDate));
        // 设置日线级别从开始日期到结束日期内上涨幅度最多股票的结束日期
        report.setTopStockEndDate(DateUtil.stringToDate(reportTopStockEndDate));
        // 设置日线级别从开始日期到结束日期内上涨幅度最多股票的股票个数
        report.setTopStockNumber(new BigDecimal(reportTopStockNumber));
        // 设置日线级别从开始日期到结束日期内上涨幅度最多股票的一字涨停板不能超过的数量
        report.setTopStockWordLimitNumber(new BigDecimal(reportTopStockWordLimtNumber));
        // 设置日线级别从开始日期到结束日期内下跌幅度最多股票的开始日期
        report.setLastStockBeginDate(DateUtil.stringToDate(reportLastStockBeginDate));
        // 设置日线级别从开始日期到结束日期内下跌幅度最多股票的结束日期
        report.setLastStockEndDate(DateUtil.stringToDate(reportLastStockEndDate));
        // 设置日线级别从开始日期到结束日期内下跌幅度最多股票的股票个数
        report.setLastStockNumber(new BigDecimal(reportLastStockNumber));
        
		// 如果这条报告记录存在，则删除
		reportDao.deleteReport(report);
		// 插入报告记录
		reportDao.insertReport(report);
	}
	
	/**
	 * 生成报告
	 */
	public void createReport() {
		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(REPORT_PREFFIX + reportGenerateDate + REPORT_SUFFIX));
			writer.setStrictImageSequence(true);
			
			// 打开文档
            document.open();
            
            // 设置字体
            this.setFont();
            // 设置标题
            this.writeTitle();
            // 写日期
            this.writeDate();
            
            // 写股票样本总数
            this.writeStockSampleSumByDate();
            // 写入某一日股票上涨家数
            this.writeStockUpNumberByDate();
            // 写入某一日股票持平家数
            this.writeStockMiddleNumberByDate();
            // 写入某一日股票下跌家数
            this.writeStockDownNumberByDate();
            // 在报告上绘制28分化图
            this.print28DifferentiationPicture();
            
            // 写入多头排列的股票的数量，股票名称/股票代码
            this.writeBullRank();
            // 写入空头排列的股票的数量，股票名称/股票代码
            this.writeShortOrder();
            // 在报告上绘制多空排列图（最近10年的图和半年的图）
            this.printBullRankShortOrderPicture();
            
            // 在报告上绘制收盘价图STOCK_CLOSE
            this.printClosePicture();
            
            // 在报告上绘制日线级别MACD图
            this.printDayMacdPicture();
            
            // 在报告上绘制日线级别蜘蛛雷达图
            this.printSpiderWebPicture();
            
            // 在报告上绘制周线级别KD指标图
            this.printWeekendKDPicture();
            
            // 在报告上绘制日线级别各个板块的K线图
            this.printBoardIndexPicture();
            
            // 在报告上绘制日线级别板块收盘价图（最近10年的图和半年的图）
            this.printAllBoardIndexClosePicture();
            
            // 在报告上绘制日线级别板块5日和10涨跌图（最近10年的图和半年的图）
            this.printAllBoardIndexFiveAndTenDayRatePicture();
            
            // 在报告上绘制日线级别股票涨停跌停数量图
            this.printStockLimitUpAndDownPicture();
            
            // 写入日线级别反转的股票
            this.writeReverseStock();
            
            // 写入日线级别MACD金叉的股票的信息
            this.writeMACDUpBelowZero();
            
            // 写入周线级别MACD底背离的股票的信息
            this.writeWeekendMacdEndDeviate();
            
            // 写入周线级别KD指标金叉的股票的信息
        	this.writeStockWeekendKDUp();
        	
        	// 写入日线级别异常股票的信息
        	this.writeAbnormalStock();
        	
        	// 写入日下级别MACD最低的股票信息
        	this.writeLowestMacdStock();
        	
        	// 写入周线级别KD指标最低的股票
            this.writeLowestKdStockWeekend();
            
            // 写入日线级别股票收盘价到达250日均线支撑的股票
            this.writeStockWith250Support();
            
            // 写入日线级别股票收盘价到达120日均线支撑的股票
            this.writeStockWith120Support();
            
            // 写入日线级别某一段时间内上涨股票的前number名
            this.writeTopStock();
            
            // 写入日线级别某一段时间内下跌股票的前number名
            this.writeLastStock();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

        // 关闭文档
        document.close();
	}
	
	/**
	 * 设置字体
	 */
	public void setFont(){
		try {
			// 设置中文字体，解决windows10下的字体问题
//			bfChinese = BaseFont.createFont("STSong-Light","UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
			bfChinese = BaseFont.createFont("/WINDOWS/Fonts/simhei.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 设置标题的中文字体
		titleFont = new Font(bfChinese,32,Font.NORMAL,Color.black);
		// 设置段落的中文字体
		paragraphFont=new Font(bfChinese,12,Font.NORMAL,Color.black);
	}
	
	/**
	 * 设置标题
	 */
	public void writeTitle(){
		try {
			title=new Paragraph(report.getReportTitle(),titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写日期
	 */
	public void writeDate(){
		try {
			document.add(new Paragraph("日期："+StringUtil.convertDateStringByFormat(DateUtil.dateToString(report.getReportDate()),"-"),paragraphFont));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入股票样本总数
	 */
	public void writeStockSampleSumByDate(){
		paragraphContent=new Paragraph("股票样本总数："+report.getStockSampleSum(),paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入股票上涨的家数
	 */
	public void writeStockUpNumberByDate(){
		paragraphContent=new Paragraph("股票上涨家数："+report.getStockUpNumber(),paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 写入股票持平的家数
	 */
	public void writeStockMiddleNumberByDate() {
		paragraphContent=new Paragraph("股票持平家数："+report.getStockMiddleNumber(),paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 写入股票下跌的家数
	 */
	public void writeStockDownNumberByDate() {
		paragraphContent=new Paragraph("股票下跌家数："+report.getStockDownNumber(),paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 在报告上绘制28分化图
	 */
	public void print28DifferentiationPicture() {
		try {
			// 设置25分化图的标题
			paragraphContent=new Paragraph("28分化图：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "28_differentiation/" + reportPicture28BeginDate + "-" + reportPicture28EndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入多头排列的股票的数量，股票名称/股票代码
	 */
	public void writeBullRank(){
		// 在报告中输出的内容
		String outputContent="股票多头排列的数量："+report.getBullRankNumber()+"\n"+"分别是：";
		outputContent+=report.getBullRankStockInfo();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入空头排列的股票的数量，股票名称/股票代码
	 */
	public void writeShortOrder() {
		// 在报告中输出的内容
		String outputContent="股票空头排列的数量："+report.getShortOrderNumber()+"\n"+"分别是：";
		outputContent+=report.getShortOrderStockInfo();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 将List类型转换为String类型
	 * @param list
	 * @return
	 */
	public String listToString(List list){
		String outputContent=new String();
		for (int i = 0; i < list.size(); i++) {
			String stockCode = ((StockTransactionData) list.get(i)).getCode();
			String stockName=stockInfoDao.getStockInfoObjectByStockCode(stockCode).getName();
			outputContent += stockCode + "_" + stockName + ",";
		}
		// 去掉字符串最后的逗号
		// 注意：如果股票的多头排列或空头排列的数量为0的话，那么list的size方法返回的就是0，即outputContent.length为0
		if(null!=outputContent&&outputContent.length()!=0){
			outputContent.substring(0, outputContent.length()-1);
		}
		logger.info(outputContent);
		return outputContent;
	}
	
	/**
	 * 在报告上绘制多空排列图（最近10年和最近半年）
	 */
	public void printBullRankShortOrderPicture(){
		// 在报告上绘制多空排列图（最近10年）
		try {
			// 设置多空排列图的标题
			paragraphContent=new Paragraph("多空排列图（最近10年）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "bull_range_short_order/" + reportPictureTenYearBullRankShortOrderBeginDate + "-" + reportPictureTenYearBullRankShortOrderEndDate + "_all" + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		// 在报告上绘制多空排列图（最近半年）
		try {
			// 设置多空排列图的标题
			paragraphContent=new Paragraph("多空排列图（最近半年）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "bull_range_short_order/" + reportPictureHalfYearBullRankShortOrderBeginDate + "-" + reportPictureHalfYearBullRankShortOrderEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制收盘价图STOCK_CLOSE（最近10年和最近半年）
	 */
	public void printClosePicture(){
		// 在报告上绘制多空排列图（最近10年）
		try {
			// 设置收盘价图STOCK_CLOSE的标题
			paragraphContent=new Paragraph("收盘价图STOCK_CLOSE（最近10年）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "close/" + reportPictureTenYearCloseBeginDate + "-" + reportPictureTenYearCloseEndDate + "_all" + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		// 在报告上绘制多空排列图（最近半年）
		try {
			// 设置收盘价图STOCK_CLOSE的标题
			paragraphContent=new Paragraph("收盘价图STOCK_CLOSE（最近半年）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "close/" + reportPictureHalfYearCloseBeginDate + "-" + reportPictureHalfYearCloseEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制日线级别MACD图
	 */
	public void printDayMacdPicture(){
		try {
			// 设置日线级别MACD图的标题
			paragraphContent=new Paragraph("日线级别MACD图：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "macd/" + reportPictureDayMacdBeginDate + "-" + reportPictureDayMacdEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制日线级别的蜘蛛雷达图
	 */
	public void printSpiderWebPicture(){
		try {
			// 设置日线级别的蜘蛛雷达图的标题
			paragraphContent=new Paragraph("日线级别的蜘蛛雷达图：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "spider_web/" + reportPictureSpiderWebDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制周线级别KD指标图
	 */
	public void printWeekendKDPicture(){
		try {
			// 设置周线级别KD指标图的标题
			paragraphContent=new Paragraph("周线级别KD指标图：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "kd_week/" + reportPictureWeekendKDBeginDate + "-" + reportPictureWeekendKDEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制日线级别各个板块的K线图
	 */
	public void printBoardIndexPicture(){
		List<Board> boardList = boardDao.getAllBoard(Boolean.FALSE);
		List<Image> jpgList=new ArrayList<Image>();
		try {
			// 设置日线级别各个板块的K线图
			paragraphContent=new Paragraph("日线级别各个板块的K线图：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			for(int i=0;i<boardList.size();i++){
				Image jpg=Image.getInstance(PICTURE_PATH + "board_index/" + reportPictureBoardIndexBeginDate + "-" + reportPictureBoardIndexEndDate + "_"+ boardList.get(i).getName() + PICTURE_FORMAT);
				// 设置图像的宽和高
				jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
				jpgList.add(jpg);
			}
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			for(int i=0;i<jpgList.size();i++){
				document.add(jpgList.get(i));
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制从开始日期至结束日期的板块收盘价图。分别绘制最近10年的图和半年的图
	 */
	public void printAllBoardIndexClosePicture(){
		// 在报告上绘制从开始日期至结束日期的板块收盘价图（最近10年的图）
		try {
			// 设置日线级别板块收盘价图（最近10年的图）的标题
			paragraphContent=new Paragraph("日线级别板块收盘价图（最近10年的图）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "board_index_close/" + reportPictureAllBoardIndexTenYearCloseBeginDate + "-" + reportPictureAllBoardIndexTenYearCloseEndDate + "_all"  + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		// 在报告上绘制从开始日期至结束日期的板块收盘价图（半年的图）
		try {
			// 设置日线级别板块收盘价图（半年的图）的标题
			paragraphContent=new Paragraph("日线级别板块收盘价图（半年的图）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "board_index_close/" + reportPictureAllBoardIndexHalfYearCloseBeginDate + "-" + reportPictureAllBoardIndexHalfYearCloseEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制从开始日期至结束日期的板块5日和10日涨跌图。分别绘制最近10年的图和半年的图
	 */
	public void printAllBoardIndexFiveAndTenDayRatePicture(){
		// 在报告上绘制从开始日期至结束日期的板块5日和10日涨跌图（最近10年的图）
		try {
			// 设置日线级别的板块5日和10日涨跌图（最近10年的图）的标题
			paragraphContent=new Paragraph("日线级别的板块5日和10日涨跌图（最近10年的图）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "board_index_five_ten_day_rate/" + reportPictureAllBoardIndexTenYearFiveAndTenDayRateBeginDate + "-" + reportPictureAllBoardIndexTenYearFiveAndTenDayRateEndDate + "_all"  + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		// 在报告上绘制从开始日期至结束日期的板块5日和10日涨跌图（半年的图）
		try {
			// 设置日线级别的板块5日和10日涨跌图（半年的图）的标题
			paragraphContent=new Paragraph("日线级别的板块5日和10日涨跌图（半年的图）：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "board_index_five_ten_day_rate/" + reportPictureAllBoardIndexHalfYearFiveAndTenDayRateBeginDate + "-" + reportPictureAllBoardIndexHalfYearFiveAndTenDayRateEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 在报告上绘制从开始日期至结束日期的股票涨停跌停数量图（折线图）
	 */
	public void printStockLimitUpAndDownPicture(){
		// 在报告上绘制从开始日期至结束日期的股票涨停跌停数量图（折线图）
		try {
			// 设置日线级别的股票涨停跌停数量图的标题
			paragraphContent=new Paragraph("日线级别的股票涨停跌停数量图：",paragraphFont);
			paragraphContent.setAlignment(Element.ALIGN_CENTER);
			
			jpg = Image.getInstance(PICTURE_PATH + "stock_up_down_limit_number/" + reportPictureLimitUpDownBeginDate + "-" + reportPictureLimitUpDownEndDate + PICTURE_FORMAT);
			// 设置图像的宽和高
			jpg.scaleAbsolute(reportImageWidth, reportImageHeigh);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			document.add(paragraphContent);
			document.add(jpg);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入日线级别发生反转的股票
	 */
	public void writeReverseStock(){
		// 在报告中输出的内容
		String outputContent="日线级别反转的股票：\n";
		outputContent+=report.getReverseStock();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 查找日线级别MACD金叉的股票
	 */
	public void writeMACDUpBelowZero() {
		// 在报告中输出的内容
		String outputContent="日线级别MACD金叉的股票：\n";
		outputContent+=report.getMacdUpBelowZeroStock().toString();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入周线级别MACD底背离的股票的信息
	 */
	public void writeWeekendMacdEndDeviate(){
		// 在报告中输出的内容
		String outputContent="周线级别MACD底背离的股票：\n";
		String[] stockWeekendArray=report.getWeekendMacdEndDeviate().split(",");
		for(int i=0;i<stockWeekendArray.length-1;i++){
			outputContent+=stockWeekendArray[i].split("&")[0]+",";
			outputContent+=StringUtil.convertDateStringByFormat(stockWeekendArray[i].split("&")[1],"-")+",";
			outputContent+=StringUtil.convertDateStringByFormat(stockWeekendArray[i].split("&")[2],"-")+"\n";
		}
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 写入周线级别KD指标金叉的股票的信息
	 */
	public void writeStockWeekendKDUp(){
		// 在报告中输出的内容
		String outputContent="周线级别KD指标金叉的股票：\n";
		outputContent+=report.getWeekendKDUpStock();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入日线级别异常股票的信息
	 */
	public void writeAbnormalStock(){
		// 在报告中输出的内容
		String outputContent="日线级别异常的股票：\n";
		outputContent+=report.getAbnormalStock();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 写入日线级别MACD最低的股票的信息
	 */
	public void writeLowestMacdStock(){
		// 在报告中输出的内容
		String outputContent="日线级别MACD最低的股票：\n";
		outputContent+=report.getLowestMacdStock();
		
		paragraphContent=new Paragraph(outputContent,paragraphFont);
		try {
			document.add(paragraphContent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * 查询某一周最低KD的stockNumber只股票
     */
	public void writeLowestKdStockWeekend(){
        // 在报告中输出的内容
        String outputContent="周线级别KD指标最低的股票：\n";
        outputContent+=report.getLowestKdWeekendStock().toString();
    
        paragraphContent=new Paragraph(outputContent,paragraphFont);
        try {
            document.add(paragraphContent);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * 写入日线级别股票收盘价到达250日均线支撑的股票
	 */
	public void writeStockWith250Support(){
		// 在报告中输出的内容
        String outputContent="日线级别股票收盘价到达250日均线支撑的股票：\n";
        outputContent+=reportDao.selectStockWith250Support(reportYearLineSupportDate,Double.parseDouble(reportYearLineSupportNearRate),Double.parseDouble(reportYearLineSupportDownRate)).toString();
    
        paragraphContent=new Paragraph(outputContent,paragraphFont);
        try {
            document.add(paragraphContent);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 写入日线级别股票收盘价到达120日均线支撑的股票
	 */
	public void writeStockWith120Support(){
		// 在报告中输出的内容
        String outputContent="日线级别股票收盘价到达120日均线支撑的股票：\n";
        outputContent+=reportDao.selectStockWith120Support(reportHalfYearLineSupportDate,Double.parseDouble(reportHalfYearLineSupportNearRate),Double.parseDouble(reportHalfYearLineSupportDownRate)).toString();
    
        paragraphContent=new Paragraph(outputContent,paragraphFont);
        try {
            document.add(paragraphContent);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 写入日线级别某一段时间内上涨股票的前number名
	 */
	public void writeTopStock(){
		// 在报告中输出的内容
        String outputContent="日线级别某一段时间内上涨股票的前"+reportTopStockNumber+"名：\n";
        outputContent+=reportDao.findTopStock(reportTopStockBeginDate, reportTopStockEndDate, Integer.parseInt(reportTopStockNumber), Integer.parseInt(reportTopStockWordLimtNumber)); 
        
        paragraphContent=new Paragraph(outputContent,paragraphFont);
        try {
            document.add(paragraphContent);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 写入日线级别某一段时间内下跌股票的前number名
	 */
	public void writeLastStock(){
		// 在报告中输出的内容
        String outputContent="日线级别某一段时间内下跌股票的前"+reportLastStockNumber+"名：\n";
        outputContent+=reportDao.findLastStock(reportLastStockBeginDate, reportLastStockEndDate, Integer.parseInt(reportLastStockNumber));
    
        paragraphContent=new Paragraph(outputContent,paragraphFont);
        try {
            document.add(paragraphContent);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
	}
}
