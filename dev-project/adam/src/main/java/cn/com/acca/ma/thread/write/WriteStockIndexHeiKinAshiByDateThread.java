package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 根据日期date，计算某一日所有指数的Hei Kin Ashi数据
 */
public class WriteStockIndexHeiKinAshiByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteStockIndexHeiKinAshiByDateThread.class);

    private StockIndexService stockIndexService;

    public WriteStockIndexHeiKinAshiByDateThread() {
    }

    public WriteStockIndexHeiKinAshiByDateThread(StockIndexService stockIndexService) {
        this.stockIndexService = stockIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始根据日期date，计算某一日所有指数的Hei Kin Ashi数据，"
            + "调用的方法为【writeStockIndexHeiKinAshiByDate】");

        stockIndexService.writeStockIndexHeiKinAshiByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
