package cn.com.acca.ma.common.util;

import cn.com.acca.ma.model.StockInfo;
import cn.com.acca.ma.model.StockTransactionData;
import cn.com.acca.ma.pojo.Real4TransactionConditionPojo;
import cn.com.acca.ma.pojo.StockInfoPojo;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 模型装换类
 */
public class ModelConverter {

    /**
     * 将StockInfo类型的对象转换为StockInfoPojo类型的对象，除了amount属性
     * @param stockInfo
     * @return
     */
    public static StockInfoPojo stockInfoToStockInfoPojo(StockInfo stockInfo){
        if (null != stockInfo){
            StockInfoPojo stockInfoPojo = new StockInfoPojo();
            stockInfoPojo.setId(stockInfo.getId());
            stockInfoPojo.setBoard(stockInfo.getBoard());
            stockInfoPojo.setCode(stockInfo.getCode());
            stockInfoPojo.setMark(stockInfo.getMark());
            stockInfoPojo.setName(stockInfo.getName());
            stockInfoPojo.setUrlParam(stockInfo.getUrlParam());
            return stockInfoPojo;
        }
        return null;
    }

    /**
     * 将Real4TransactionConditionPojo类型的对象转换为StockInfoPojo类型的对象，除了amount属性
     * @param real4TransactionConditionPojo
     * @return
     */
    public static StockInfoPojo real4TransactionConditionPojoToStockInfoPojo(Real4TransactionConditionPojo real4TransactionConditionPojo){
        if (null != real4TransactionConditionPojo){
            StockInfoPojo stockInfoPojo = new StockInfoPojo();
//            stockInfoPojo.setId(real4TransactionConditionPojo.getId());
//            stockInfoPojo.setBoard(stockInfo.getBoard());
            stockInfoPojo.setCode(real4TransactionConditionPojo.getStockCode());
            stockInfoPojo.setMark(real4TransactionConditionPojo.getMark());
            stockInfoPojo.setName(real4TransactionConditionPojo.getStockName());
//            stockInfoPojo.setUrlParam(stockInfo.getUrlParam());
            return stockInfoPojo;
        }
        return null;
    }

    public static StockTransactionData mapToStockTransactionData(Map map){
        if (null != map){
            StockTransactionData stockTransactionData = new StockTransactionData();
            stockTransactionData.setId(Integer.valueOf(map.get("ID_").toString()));
            stockTransactionData.setDate(DateUtil.stringToDate(map.get("DATE_").toString()));
            stockTransactionData.setCode((String) map.get("CODE_"));
            stockTransactionData.setOpenPrice((BigDecimal) map.get("OPEN_PRICE"));
            stockTransactionData.setClosePrice((BigDecimal) map.get("CLOSE_PRICE"));
            stockTransactionData.setHighestPrice((BigDecimal) map.get("HIGHEST_PRICE"));
            stockTransactionData.setLowestPrice((BigDecimal) map.get("LOWEST_PRICE"));
            stockTransactionData.setLastClosePrice((BigDecimal) map.get("LAST_CLOSE_PRICE"));
            stockTransactionData.setChangeAmount((BigDecimal) map.get("CHANGE_AMOUNT"));
            stockTransactionData.setChangeRange((BigDecimal) map.get("CHANGE_RANGE"));
            stockTransactionData.setChangeRangeExRight((BigDecimal) map.get("CHANGE_RANGE_EX_RIGHT"));
            stockTransactionData.setUpDown((BigDecimal) map.get("UP_DOWN"));
            stockTransactionData.setTurnoverRate((BigDecimal) map.get("TURNOVER_RATE"));
            stockTransactionData.setVolume((BigDecimal) map.get("VOLUME"));
            stockTransactionData.setTurnover((BigDecimal) map.get("TURNOVER"));
            stockTransactionData.setTotalMarketValue((BigDecimal) map.get("TOTAL_MARKET_VALUE"));
            stockTransactionData.setCirculationMarketValue((BigDecimal) map.get("CIRCULATION_MARKET_VALUE"));
            stockTransactionData.setTransactionNumber((BigDecimal) map.get("TRANSACTION_NUMBER"));
            stockTransactionData.setMa5((BigDecimal) map.get("MA5"));
            stockTransactionData.setMa10((BigDecimal) map.get("MA10"));
            stockTransactionData.setMa20((BigDecimal) map.get("MA20"));
            stockTransactionData.setMa60((BigDecimal) map.get("MA60"));
            stockTransactionData.setMa120((BigDecimal) map.get("MA120"));
            stockTransactionData.setMa250((BigDecimal) map.get("MA250"));
            stockTransactionData.setEma12((BigDecimal)map.get("EMA12"));
            stockTransactionData.setEma26((BigDecimal)map.get("EMA26"));
            stockTransactionData.setDif((BigDecimal)map.get("DIF"));
            stockTransactionData.setDea((BigDecimal)map.get("DEA"));
            stockTransactionData.setFiveDayVolatility((BigDecimal)map.get("FIVE_DAY_VOLATILITY"));
            stockTransactionData.setTenDayVolatility((BigDecimal)map.get("TEN_DAY_VOLATILITY"));
            stockTransactionData.setTwentyDayVolatility((BigDecimal)map.get("TWENTY_DAY_VOLATILITY"));
            stockTransactionData.setTwoHundredFifty((BigDecimal)map.get("TWO_HUNDRED_FIFTY"));
            stockTransactionData.setRsv((BigDecimal)map.get("RSV"));
            stockTransactionData.setK((BigDecimal)map.get("K"));
            stockTransactionData.setD((BigDecimal)map.get("D"));
            stockTransactionData.setHaOpenPrice((BigDecimal)map.get("HA_OPEN_PRICE"));
            stockTransactionData.setHaClosePrice((BigDecimal)map.get("HA_CLOSE_PRICE"));
            stockTransactionData.setHaHighestPrice((BigDecimal)map.get("HA_HIGHEST_PRICE"));
            stockTransactionData.setHaLowestPrice((BigDecimal)map.get("HA_LOWEST_PRICE"));
            return stockTransactionData;
        }
        return null;
    }
}
