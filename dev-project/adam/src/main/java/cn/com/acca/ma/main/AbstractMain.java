package cn.com.acca.ma.main;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.dao.impl.ReportDaoImpl;
import cn.com.acca.ma.service.*;
import cn.com.acca.ma.service.impl.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;

public abstract class AbstractMain {

    private static Logger logger = LogManager.getLogger(AbstractMain.class);

    protected static StockTransactionDataService stockTransactionDataService = new StockTransactionDataServiceImpl();
    protected static StockTransactionDataAllService stockTransactionDataAllService = new StockTransactionDataAllServiceImpl();
    protected static EtfTransactionDataService etfTransactionDataService = new EtfTransactionDataServiceImpl();
    protected static StockWeekService stockWeekService = new StockWeekServiceImpl();
    protected static StockMonthService stockMonthService = new StockMonthServiceImpl();
    protected static StockIndexService stockIndexService = new StockIndexServiceImpl();
    protected static StockIndexWeekService stockIndexWeekService = new StockIndexWeekServiceImpl();
    protected static PrintStockService printStockService = new PrintStockServiceImpl();
    protected static BoardIndexService boardIndexService = new BoardIndexServiceImpl();
    protected static Board2IndexService board2IndexService = new Board2IndexServiceImpl();
    protected static ReportService reportService = new ReportServiceImpl();
    protected static ProjectService projectService = new ProjectServiceImpl();
    protected static ModelService modelService = new ModelServiceImpl();
    protected static ModelMACDGoldCrossService modelMACDGoldCrossService = new ModelMACDGoldCrossServiceImpl();
    protected static ModelMACDDeadCrossService modelMACDDeadCrossService = new ModelMACDDeadCrossServiceImpl();
    protected static ModelStockIndexMACDGoldCrossService modelStockIndexMACDGoldCrossService = new ModelStockIndexMACDGoldCrossServiceImpl();
    protected static ModelStockIndexClosePriceMA5GoldCrossService modelStockIndexClosePriceMA5GoldCrossService = new ModelStockIndexClosePriceMA5GoldCrossServiceImpl();
    protected static ModelStockIndexHeiKinAshiDownUpService modelStockIndexHeiKinAshiDownUpService = new ModelStockIndexHeiKinAshiDownUpServiceImpl();
    protected static ModelStockIndexKDGoldCrossService modelStockIndexKDGoldCrossService = new ModelStockIndexKDGoldCrossServiceImpl();
    protected static ModelEtfMACDGoldCrossService modelEtfMACDGoldCrossService = new ModelEtfMACDGoldCrossServiceImpl();
    protected static ModelEtfClosePriceMA5GoldCrossService modelEtfClosePriceMA5GoldCrossService = new ModelEtfClosePriceMA5GoldCrossServiceImpl();
    protected static ModelEtfHeiKinAshiDownUpService modelEtfHeiKinAshiDownUpService = new ModelEtfHeiKinAshiDownUpServiceImpl();
    protected static ModelEtfKDGoldCrossService modelEtfKDGoldCrossService = new ModelEtfKDGoldCrossServiceImpl();
    protected static ModelKDGoldCrossService modelKDGoldCrossService = new ModelKDGoldCrossServiceImpl();
    protected static ModelKDDeadCrossService modelKDDeadCrossService = new ModelKDDeadCrossServiceImpl();

    protected static ModelVolumeTurnoverUpMa250Service modelVolumeTurnoverUpMa250Service = new ModelVolumeTurnoverUpMa250ServiceImpl();
    protected static ModelWeekKDGoldCrossService modelWeekKDGoldCrossService = new ModelWeekKDGoldCrossServiceImpl();
    protected static ModelAllGoldCrossService modelAllGoldCrossService = new ModelAllGoldCrossServiceImpl();
    protected static ModelWeekAllGoldCrossService modelWeekAllGoldCrossService = new ModelWeekAllGoldCrossServiceImpl();
    protected static ModelClosePriceMA5GoldCrossService modelClosePriceMA5GoldCrossService = new ModelClosePriceMA5GoldCrossServiceImpl();
    protected static ModelClosePriceMA5DeadCrossService modelClosePriceMA5DeadCrossService = new ModelClosePriceMA5DeadCrossServiceImpl();
    protected static ModelHeiKinAshiUpDownService modelHeiKinAshiUpDownService = new ModelHeiKinAshiUpDownServiceImpl();
    protected static ModelHeiKinAshiDownUpService modelHeiKinAshiDownUpService = new ModelHeiKinAshiDownUpServiceImpl();
    protected static ModelTopStockService modelTopStockService = new ModelTopStockServiceImpl();
    protected static ModelTopStockDetailService modelTopStockDetailService = new ModelTopStockDetailServiceImpl();
    protected static ModelEtfBullShortLineUpService modelEtfBullShortLineUpService = new ModelEtfBullShortLineUpServiceImpl();
    protected static ModelBullShortLineUpService modelBullShortLineUpService = new ModelBullShortLineUpServiceImpl();
    protected static ModelPercentageMaGoldCrossService modelPercentageMaGoldCrossService = new ModelPercentageMaGoldCrossServiceImpl();
    protected static ModelSubNewStockService modelSubNewStockService = new ModelSubNewStockServiceImpl();
    protected static ModelStockAnalysisService modelStockAnalysisService = new ModelStockAnalysisServiceImpl();
    protected static ModelStockMonthAnalysisService modelStockMonthAnalysisService = new ModelStockMonthAnalysisServiceImpl();
    protected static ModelStockWeekAnalysisService modelStockWeekAnalysisService = new ModelStockWeekAnalysisServiceImpl();
    protected static WechatSubscriptionService wechatSubscriptionService = new WechatSubscriptionServiceImpl();
    protected static RobotStockFilterService robotStockFilterService = new RobotStockFilterServiceImpl();
    protected static RobotStockTransactionRecordService robotStockTransactionRecordService = new RobotStockTransactionRecordServiceImpl();
    protected static RobotAccountService robotAccountService = new RobotAccountServiceImpl();
    protected static Robot2StockFilterService robot2StockFilterService = new Robot2StockFilterServiceImpl();
    protected static Robot2StockTransactRecordService robot2StockTransactRecordService = new Robot2StockTransactRecordServiceImpl();
    protected static Robot2AccountService robot2AccountService = new Robot2AccountServiceImpl();
    protected static Robot3AccountService robot3AccountService = new Robot3AccountServiceImpl();
    protected static Robot3StockFilterService robot3StockFilterService = new Robot3StockFilterServiceImpl();
    protected static Robot3StockTransactRecordService robot3StockTransactRecordService = new Robot3StockTransactRecordServiceImpl();
    protected static Robot4StockTransactRecordService robot4StockTransactRecordService = new Robot4StockTransactRecordServiceImpl();
    protected static Robot5StockTransactRecordService robot5StockTransactRecordService = new Robot5StockTransactRecordServiceImpl();
    protected static Robot6EtfTransactRecordService robot6StockTransactRecordService = new Robot6EtfTransactRecordServiceImpl();
    protected static RealTransactionConditionService realTransactionConditionService = new RealTransactionConditionServiceImpl();
    protected static RealStockTransactionRecordService realStockTransactionRecordService = new RealStockTransactionRecordServiceImpl();
    protected static Real3TransactionConditionService real3TransactionConditionService = new Real3TransactionConditionServiceImpl();
    protected static Real3StockTransactionRecordService real3StockTransactionRecordService = new Real3StockTransactionRecordServiceImpl();
    protected static Real4TransactionConditionService real4TransactionConditionService = new Real4TransactionConditionServiceImpl();
    protected static Real4StockTransactionRecordService real4StockTransactionRecordService = new Real4StockTransactionRecordServiceImpl();
    protected static ModelWeekBollMACDGoldCrossService modelWeekBollMACDGoldCrossService = new ModelWeekBollMACDGoldCrossServiceImpl();
    protected static ModelWeekBollMACDDeadCrossService modelWeekBollMACDDeadCrossService = new ModelWeekBollMACDDeadCrossServiceImpl();
    protected static ModelWeekBollClosePriceMA5GoldCrossService modelWeekBollClosePriceMA5GoldCrossService = new ModelWeekBollClosePriceMA5GoldCrossServiceImpl();
    protected static ModelWeekBollClosePriceMA5DeadCrossService modelWeekBollClosePriceMA5DeadCrossService = new ModelWeekBollClosePriceMA5DeadCrossServiceImpl();
    protected static ModelWeekBollHeiKinAshiUpDownService modelWeekBollHeiKinAshiUpDownService = new ModelWeekBollHeiKinAshiUpDownServiceImpl();
    protected static ModelWeekBollHeiKinAshiDownUpService modelWeekBollHeiKinAshiDownUpService = new ModelWeekBollHeiKinAshiDownUpServiceImpl();
    protected static ModelWeekBollKDGoldCrossService modelWeekBollKDGoldCrossService = new ModelWeekBollKDGoldCrossServiceImpl();
    protected static ModelWeekBollKDDeadCrossService modelWeekBollKDDeadCrossService = new ModelWeekBollKDDeadCrossServiceImpl();
    protected static ModelGoldCrossDeadCrossAnalysisService modelGoldCrossDeadCrossAnalysisService = new ModelGoldCrossDeadCrossAnalysisServiceImpl();

    /*****************************************************************************************************************
     *
     * 如果程序是在公司环境运行，则设置代理服务器；否则直接跳过
     *
     ******************************************************************************************************************/
    public static void setProxyProperties() {
        projectService.setProxyProperties();
    }

    /*****************************************************************************************************************
     *
     * 										收集数据库统计信息
     *
     * ****************************************************************************************************************/
    /**
     * 收集数据库统计信息
     */
    public static void gatherDatabaseStatistics() {
        projectService.gatherDatabaseStatistics();
    }

    /*******************************************************************************************************************
     *
     * 恢复数据库中各个表的数据
     *
     *******************************************************************************************************************/
    public static void restoreTableIncremental() {
        projectService.restoreTableIncrementalByJson();
    }

    /*******************************************************************************************************************
     *
     * 从网络上获取数据，并插入到数据库中
     *
     *******************************************************************************************************************/
    public static void insertStockTransactionData_sohu(){
        stockTransactionDataService.insertStockTransactionData_sohu();
    }

    public static void insertStockTransactionData_baoStock(){
        stockTransactionDataService.insertStockTransactionData_baoStock();
    }

    public static void writeEtfTransactionDataByDate_sohu(){
        etfTransactionDataService.insertEtfTransactionData_sohu();
    }

    public static void insertStockTransactionData_163() {
        stockTransactionDataService.insertStockTransactionData_163();
    }

    public static void insertStockTransactionData_126() {
        stockTransactionDataService.insertStockTransactionData_126();
    }

    /*******************************************************************************************************************
     *
     * 补充使用126接口没有的字段。使用126接口有些字段是空的，只能之后再使用163的接口将确实的字段补齐
     *
     ********************************************************************************************************************/
    public static void supplementDataBy163Api() {
        stockTransactionDataService.supplementDataBy163Api();
    }

    /*******************************************************************************************************************
     *
     * 从网络上获取数据，以json格式存储在ALL_RECORDS_IN_JSON.json文件中；然后再解析文件，并插入到数据库中
     *
     ********************************************************************************************************************/
    public static void readAndWriteAllRecordsByJson() {
        stockTransactionDataService.readAndWriteAllRecordsByJson();
    }

    public static void insertAllRecordsFromJson() {
        stockTransactionDataService.insertAllRecordsFromJson();
    }

    /*******************************************************************************************************************
     *
     * 更新每天的数据
     *
     *******************************************************************************************************************/
    public static void writeMovingAverageByDate() {
        stockTransactionDataService.writeMovingAverageByDate(Boolean.FALSE);
    }

    public static void writeChangeRangeExRightByDate() {
        stockTransactionDataService.writeChangeRangeExRightByDate();
    }

    /**
     * stock_transaction_data表的up_down字段已经在获取数据时就计算好了，所以现在这个方法用不着了。
     */
    public static void writeUpDownByDate() {
        stockTransactionDataService.writeUpDownByDate();
    }

    public static void writeMACDByDate() {
        stockTransactionDataService.writeMACDByDate(Boolean.FALSE);
    }

    public static void calUpDownPercentageByDate() {
        stockTransactionDataService.calUpDownPercentageByDate();
    }

    public static void calVolatilityByDate() {
        stockTransactionDataService.calVolatilityByDate();
    }

    public static void writeKDByDate() {
        stockTransactionDataService.writeKDByDate(Boolean.FALSE);
    }

    public static void writeHaByDate() {
        stockTransactionDataService.writeHaByDate(Boolean.FALSE);
    }

    public static void writeBollByDate() {
        stockTransactionDataService.writeBollByDate(Boolean.FALSE);
    }

    public static void writeBiasByDate(){
        stockTransactionDataService.writeBiasByDate(Boolean.FALSE);
    }

    public static void writeVarianceByDate(){
        stockTransactionDataService.writeVarianceByDate(Boolean.FALSE);
    }

    public static void writeVolumeMaByDate(){
        stockTransactionDataService.writeVolumeMaByDate(Boolean.FALSE);
    }

    public static void writeTurnoverMaByDate(){
        stockTransactionDataService.writeTurnoverMaByDate(Boolean.FALSE);
    }

    public static void writeStockTransactionDataAllByDate() {
        stockTransactionDataAllService.writeStockTransactionDataAllByDate(Boolean.FALSE);
    }

    public static void writeStockIndexByDate() {
        stockIndexService.writeStockIndexByDate(Boolean.FALSE);
    }

    public static void writeStockIndexByDate_baoStock() {
        stockIndexService.writeStockIndexByDate_baoStock();
    }

    public static void writeStockIndexMAByDate() {
        stockIndexService.writeStockIndexMAByDate(Boolean.FALSE);
    }

    public static void writeStockIndexBiasByDate() {
        stockIndexService.writeStockIndexBiasByDate(Boolean.FALSE);
    }

    public static void writeStockIndexHeiKinAshiByDate() {
        stockIndexService.writeStockIndexHeiKinAshiByDate(Boolean.FALSE);
    }

    public static void writeStockIndexMACDByDate() {
        stockIndexService.writeStockIndexMACDByDate();
    }

    public static void writeStockIndexKDByDate() {
        stockIndexService.writeStockIndexKDByDate();
    }

    public static void writeEtfTransactionDataMAByDate() {
        etfTransactionDataService.writeEtfTransactionDataMAByDate();
    }

    public static void writeEtfTransactionDataBiasByDate() {
        etfTransactionDataService.writeEtfTransactionDataBiasByDate();
    }

    public static void writeEtfTransactionDataHeiKinAshiByDate() {
        etfTransactionDataService.writeEtfTransactionDataHeiKinAshiByDate();
    }

    public static void writeEtfTransactionDataMACDByDate() {
        etfTransactionDataService.writeEtfTransactionDataMACDByDate();
    }

    public static void writeEtfTransactionDataKDByDate() {
        etfTransactionDataService.writeEtfTransactionDataKDByDate();
    }

    public static void writeEtfTransactionDataCorrelationByDate(){
        etfTransactionDataService.writeEtfTransactionDataCorrelationByDate();
    }

    public static void writeBoardIndexByDate() {
        boardIndexService.writeBoardIndexByDate(Boolean.FALSE);
    }

    public static void writeBoardIndexFiveAndTenDayRateByDate() {
        boardIndexService.writeBoardIndexFiveAndTenDayRateByDate(Boolean.FALSE);
    }

    public static void writeBoardIndexUpDownPercentageByDate() {
        boardIndexService.writeBoardIndexUpDownPercentageByDate(Boolean.FALSE);
    }

    public static void writeBoardIndexUpDownRankByDate() {
        boardIndexService.writeBoardIndexUpDownRankByDate(Boolean.FALSE);
    }

    public static void writeModelMACDGoldCrossIncr() {
        modelMACDGoldCrossService.writeModelMACDGoldCrossIncr(Boolean.FALSE);
    }

    public static void writeModelMACDDeadCrossIncr() {
        modelMACDDeadCrossService.writeModelMACDDeadCrossIncr(Boolean.FALSE);
    }

    public static void writeModelKDGoldCrossIncr() {
        modelKDGoldCrossService.writeModelKDGoldCrossIncr();
    }

    public static void writeModelKDDeadCrossIncr() {
        modelKDDeadCrossService.writeModelKDDeadCrossIncr();
    }

    public static void writeModelEtfMACDGoldCrossByDate() {
        modelEtfMACDGoldCrossService.writeModelEtfMACDGoldCrossByDate();
    }

    public static void writeModelVolumeTurnoverUpMa250Incr() {
        modelVolumeTurnoverUpMa250Service.writeModelVolumeTurnoverUpMa250Incr();
    }

    public static void writeModelEtfClosePriceMA5GoldCrossByDate() {
        modelEtfClosePriceMA5GoldCrossService.writeModelEtfClosePriceMA5GoldCrossByDate();
    }

    public static void writeModelEtfHeiKinAshiDownUpByDate() {
        modelEtfHeiKinAshiDownUpService.writeModelEtfHeiKinAshiDownUpByDate();
    }

    public static void writeModelEtfKDGoldCrossByDate() {
        modelEtfKDGoldCrossService.writeModelEtfKDGoldCrossByDate();
    }

    public static void writeModelEtfBullShortLineUpIncr(){
        modelEtfBullShortLineUpService.writeModelEtfBullShortLineUpIncr();
    }

    public static void writeModelBullShortLineUpIncr(){
        modelBullShortLineUpService.writeModelBullShortLineUpIncr();
    }

    public static void writeModelTopStockByDate() {
        modelTopStockService.writeModelTopStock(Boolean.FALSE);
    }

    public static void writeModelTopStockDetailByDate() {
        modelTopStockDetailService.writeModelTopStockDetailByDate(Boolean.FALSE);
    }

    public static void writeModelTopStockDetailMAByDate() {
        modelTopStockDetailService.writeModelTopStockDetailMAByDate();
    }

    public static void writeModelGoldCrossDeadCrossAnalysisByDate() {
        modelGoldCrossDeadCrossAnalysisService.writeModelGoldCrossDeadCrossAnalysisByDate();
    }

    public static void writeModelStockAnalysisByDate() {
        modelStockAnalysisService.writeModelStockAnalysisByDate(Boolean.FALSE);
    }

    public static void writeModelWeekBollMacdGoldCross() {
        modelWeekBollMACDGoldCrossService.writeModelWeekBollMACDGoldCross();
    }

    public static void writeModelWeekBollClosePriceMa5GoldCross() {
        modelWeekBollClosePriceMA5GoldCrossService.writeModelWeekBollClosePriceMA5GoldCross();
    }

    public static void writeModelWeekBollHeiKinAshiUpDown() {
        modelWeekBollHeiKinAshiUpDownService.writeModelWeekBollHeiKinAshiUpDown();
    }

    public static void writeModelWeekBollKDGoldCross() {
        modelWeekBollKDGoldCrossService.writeModelWeekBollKDGoldCross();
    }

    public static void writeModelWeekBollMacdDeadCross() {
        modelWeekBollMACDDeadCrossService.writeModelWeekBollMACDDeadCross();
    }

    public static void writeModelWeekBollClosePriceMa5DeadCross() {
        modelWeekBollClosePriceMA5DeadCrossService.writeModelWeekBollClosePriceMA5DeadCross();
    }

    public static void writeModelWeekBollHeiKinAshiDownUp() {
        modelWeekBollHeiKinAshiDownUpService.writeModelWeekBollHeiKinAshiDownUp();
    }

    public static void writeModelWeekBollKDDeadCross() {
        modelWeekBollKDDeadCrossService.writeModelWeekBollKDDeadCross();
    }

    public static void writeModelPercentageMaGoldCross() {
        modelPercentageMaGoldCrossService.writeModelPercentageMaGoldCross();
    }

    public static void writeModelBullShortLineUp(){
        modelBullShortLineUpService.writeModelBullShortLineUp();
    }

    public static void writeModelVolumeTurnoverUpMa250(){
        modelVolumeTurnoverUpMa250Service.writeModelVolumeTurnoverUpMa250();
    }

    /*******************************************************************************************************************
     *
     * 根据每日数据更新图表
     *
     *******************************************************************************************************************/
    public static void createMovingAverageBullRankShortOrderPicture() {
        stockTransactionDataService.createMovingAverageBullRankShortOrderPicture(Boolean.FALSE);
    }

    public static void createMACDPicture() {
        stockTransactionDataService.createMACDPicture(Boolean.FALSE);
    }

    public static void createClosePicture(boolean tenYear) {
        stockTransactionDataService.createClosePicture(Boolean.FALSE, tenYear);
    }

    public static void createKDPicture() {
        stockTransactionDataService.createKDPicture(Boolean.FALSE);
    }

    public static void createClosePriceGoldCrossMa5Picture() {
        stockTransactionDataService.createClosePriceGoldCrossMa5Picture(Boolean.FALSE);
    }

    public static void createHeiKinAshiUpDownPicture() {
        stockTransactionDataService.createHeiKinAshiUpDownPicture(Boolean.FALSE);
    }

    public static void createDifferentiationPicture() {
        stockTransactionDataService.createDifferentiationPicture(Boolean.FALSE);
    }

    public static void createBoardPicture() {
        boardIndexService.createBoardPicture(Boolean.FALSE);
    }

    public static void createSpiderWebPlotPicture() {
        boardIndexService.createSpiderWebPlotPicture(Boolean.FALSE);
    }

    public static void createStockIndexHeiKinAshiPicture() {
        stockIndexService.createStockIndexHeiKinAshiPicture(Boolean.FALSE);
    }

    public static void createAllBoardIndexClosePicture() {
        boardIndexService.createAllBoardIndexClosePicture(Boolean.FALSE);
    }

    public static void createStockLimitUpAndDownPicture() {
        stockTransactionDataService.createStockLimitUpAndDownPicture(Boolean.FALSE);
    }

    public static void createAllBoardIndexFiveAndTenDayRatePicture() {
        boardIndexService.createAllBoardIndexFiveAndTenDayRatePicture(Boolean.FALSE);
    }

    public static void createMACDGoldCrossStockRatePicture() {
        stockTransactionDataService.createMACDGoldCrossStockRatePicture(Boolean.FALSE);
    }

    public static void createStockBigBoardUpDownPercentagePicture() {
        stockTransactionDataService.createStockBigBoardUpDownPercentagePicture(Boolean.FALSE);
    }

    public static void createMACDGoldCrossScatterPlotPicture() {
        modelMACDGoldCrossService.createMACDGoldCrossScatterPlotPicture(Boolean.FALSE);
    }

    public static void createMACDSuccessDidDeaScatterPlotPicture() {
        modelMACDGoldCrossService.createMACDSuccessDidDeaScatterPlotPicture(Boolean.FALSE, Boolean.TRUE);
        modelMACDGoldCrossService.createMACDSuccessDidDeaScatterPlotPicture(Boolean.FALSE, Boolean.FALSE);
    }

    public static void createMACDSuccessRatePicture() {
        modelMACDGoldCrossService.createMACDSuccessRatePicture(Boolean.FALSE);
    }

    public static void createModelStockAnalysisStandardDeviationPictureAndAverageClosePrice() {
        modelStockAnalysisService.createModelStockAnalysisStandardDeviationPictureAndAverageClosePrice();
    }

    public static void createClosePriceMa5GoldCrossSuccessRatePicture() {
        modelClosePriceMA5GoldCrossService.createClosePriceMa5GoldCrossSuccessRatePicture(Boolean.FALSE);
    }

    public static void createTopStockUpDownPicture() {
        modelTopStockService.createTopStockUpDownPicture(Boolean.FALSE);
    }

    public static void createSubNewStockAvgClosePicture() {
        modelSubNewStockService.createSubNewStockAverageClosePicture(Boolean.FALSE);
    }

    /******************************************************************************************************************
     *
     * 更新某一周的数据
     *
     *******************************************************************************************************************/
    public static void writeStockWeekByDate() {
        stockWeekService.writeStockWeekByDate();
    }

    public static void writeStockWeekMAByDate() {
        stockWeekService.writeStockWeekMAByDate();
    }

    public static void writeStockWeekKDByDate() {
        stockWeekService.writeStockWeekKDByDate();
    }

    public static void writeStockWeekUpDownByDate() {
        stockWeekService.writeStockWeekUpDownByDate();
    }

    public static void writeStockWeekMACDByDate() {
        stockWeekService.writeStockWeekMACDByDate();
    }

    public static void writeStockWeekChangeRangeExRightByDate() {
        stockWeekService.writeStockWeekChangeRangeExRightByDate();
    }

    public static void writeStockWeekBollByDate() {
        stockWeekService.writeStockWeekBollByDate();
    }

    public static void writeStockWeekHeiKinAshiByDate() {
        stockWeekService.writeStockWeekHeiKinAshiByDate();
    }

    public static void writeStockIndexWeekByDate() {
        stockIndexWeekService.writeStockIndexWeekByDate();
    }

    public static void writeStockIndexWeekHeiKinAshiByDate() {
        stockIndexWeekService.writeStockIndexWeekHeiKinAshiByDate();
    }

    public static void writeModelStockWeekAnalysisByDate(){
        modelStockWeekAnalysisService.writeModelStockWeekAnalysisByDate(Boolean.FALSE);
    }

    /******************************************************************************************************************
     *
     * 根据某一周数据更新图表
     *
     ******************************************************************************************************************/
    public static void createWeekKDPicture() {
        stockWeekService.createWeekKDPicture(Boolean.FALSE);
    }

    public static void createWeekBollClosePricePicture() {
        stockWeekService.createWeekBollClosePricePicture();
    }

    public static void createWeekClosePriceUpDnBollPercentagePicture(){
        stockWeekService.createWeekClosePriceUpDnBollPercentagePicture();
    }

    public static void createWeekMAAndClosePricePicture(){
        stockWeekService.createWeekMAAndClosePricePicture();
    }

    public static void createWeekHeiKinAshiUpDownPicture(){
        stockWeekService.createWeekHeiKinAshiUpDownPicture();
    }

    public static void createModelStockWeekAnalysisStandardDeviationPictureAndAverageClosePrice(){
        modelStockWeekAnalysisService.createModelStockWeekAnalysisStandardDeviationPictureAndAverageClosePrice();
    }

    public static void createStockIndexWeekHeiKinAshiPicture() {
        stockIndexWeekService.createStockIndexWeekHeiKinAshiPicture(Boolean.FALSE);
    }

    /******************************************************************************************************************
     *
     * 更新某一月的数据
     *
     *******************************************************************************************************************/
    public static void writeStockMonthByDate() {
        stockMonthService.writeStockMonthByDate();
    }

    public static void writeStockMonthMAByDate() {
        stockMonthService.writeStockMonthMAByDate();
    }

    public static void writeStockMonthKDByDate() {
        stockMonthService.writeStockMonthKDByDate();
    }

    public static void writeStockMonthMACDByDate() {
        stockMonthService.writeStockMonthMACDByDate();
    }

    public static void writeModelStockMonthAnalysisByDate() {
        modelStockMonthAnalysisService.writeModelStockMonthAnalysisByDate(Boolean.FALSE);
    }

    /*******************************************************************************************************************
     *
     * 更新所有的数据
     *
     *******************************************************************************************************************/
    public static void calculateMovingAverage() {
        stockTransactionDataService.calculateMovingAverage();
    }

    public static void writeChangeRangeExRight() {
        stockTransactionDataService.writeChangeRangeExRight();
    }

    /**
     * stock_transaction_data表的up_down字段已经在获取数据时就计算好了，所以现在这个方法用不着了。
     */
    public static void writeUpDown() {
        stockTransactionDataService.writeUpDown();
    }

    public static void writeMACD() {
        stockTransactionDataService.writeMACD();
    }

    public static void calculateUpDownPercentage() {
        stockTransactionDataService.calculateUpDownPercentage();
    }

    public static void calculateVolatility() {
        stockTransactionDataService.calculateVolatility();
    }

    public static void writeKD() {
        stockTransactionDataService.writeKD();
    }

    public static void writeHa() {
        stockTransactionDataService.writeHa();
    }

    public static void writeBoll() {
        stockTransactionDataService.writeBoll();
    }

    public static void writeBias(){
        stockTransactionDataService.writeBias();
    }

    public static void writeVariance(){
        stockTransactionDataService.writeVariance();
    }

    public static void writeVolumeMa(){
        stockTransactionDataService.writeVolumeMa();
    }

    public static void writeTurnoverMa(){
        stockTransactionDataService.writeTurnoverMa();
    }

    public static void writeStockWeek() {
        stockWeekService.writeStockWeek();
    }

    public static void writeStockWeekMA() {
        stockWeekService.writeStockWeekMA();
    }

    public static void writeStockWeekKD() {
        stockWeekService.writeStockWeekKD();
    }

    public static void writeStockWeekUpDown() {
        stockWeekService.writeStockWeekUpDown();
    }

    public static void writeStockWeekMACD() {
        stockWeekService.writeStockWeekMACD();
    }

    public static void writeStockWeekChangeRangeExRight() {
        stockWeekService.writeStockWeekChangeRangeExRight();
    }

    public static void writeStockWeekBoll() {
        stockWeekService.writeStockWeekBoll();
    }

    public static void writeStockWeekHeiKinAshi() {
        stockWeekService.writeStockWeekHeiKinAshi();
    }

    public static void writeStockMonth() {
        stockMonthService.writeStockMonth();
    }

    public static void writeStockMonthMA() {
        stockMonthService.writeStockMonthMA();
    }

    public static void writeStockMonthKD() {
        stockMonthService.writeStockMonthKD();
    }

    public static void writeStockMonthMACD() {
        stockMonthService.writeStockMonthMACD();
    }

    public static void writeStockIndex() {
        stockIndexService.writeStockIndex(Boolean.FALSE);
    }

    public static void writeStockIndexMA() {
        stockIndexService.writeStockIndexMA();
    }

    public static void writeStockIndexHeiKinAshi() {
        stockIndexService.writeStockIndexHeiKinAshi();
    }

    public static void writeStockIndexBias() {
        stockIndexService.writeStockIndexBias();
    }

    public static void writeStockIndexMACD() {
        stockIndexService.writeStockIndexMACD();
    }

    public static void writeStockIndexKD() {
        stockIndexService.writeStockIndexKD();
    }

    public static void writeStockIndexWeek() {
        stockIndexWeekService.writeStockIndexWeek();
    }

    public static void writeStockIndexWeekHeiKinAshi() {
        stockIndexWeekService.writeStockIndexWeekHeiKinAshi();
    }

    public static void writeLastClosePrice() {
        etfTransactionDataService.writeLastClosePrice();
    }

    public static void writeEtfTransactionDataMA() {
        etfTransactionDataService.writeEtfTransactionDataMA();
    }

    public static void writeEtfTransactionDataBias() {
        etfTransactionDataService.writeEtfTransactionDataBias();
    }

    public static void writeEtfTransactionDataHeiKinAshi() {
        etfTransactionDataService.writeEtfTransactionDataHeiKinAshi();
    }

    public static void writeEtfTransactionDataMACD() {
        etfTransactionDataService.writeEtfTransactionDataMACD();
    }

    public static void writeEtfTransactionDataKD() {
        etfTransactionDataService.writeEtfTransactionDataKD();
    }

    public static void writeEtfTransactionDataCorrelation() {
        etfTransactionDataService.writeEtfTransactionDataCorrelation();
    }

    public static void calculateBoardIndex() {
        boardIndexService.calculateBoardIndex();
    }

    public static void calculateAllBoardIndexFiveAndTenDayRate() {
        boardIndexService.calculateAllBoardIndexFiveAndTenDayRate();
    }

    public static void calculateBoardIndexUpDownPercentage() {
        boardIndexService.calculateBoardIndexUpDownPercentage();
    }

    public static void calculateBoardIndexUpDownRank() {
        boardIndexService.calculateBoardIndexUpDownRank();
    }

    public static void writeModelWeekKDGoldCross() {
        modelWeekKDGoldCrossService.writeModelWeekKDGoldCross();
    }

    public static void writeModelMACDGoldCross() {
        modelMACDGoldCrossService.writeModelMACDGoldCross();
    }

    public static void writeModelMACDDeadCross() {
        modelMACDDeadCrossService.writeModelMACDDeadCross();
    }

    public static void writeModelClosePriceMA5GoldCross() {
        modelClosePriceMA5GoldCrossService.writeModelClosePriceMA5GoldCross();
    }

    public static void writeModelClosePriceMA5GoldCrossIncr() {
        modelClosePriceMA5GoldCrossService.writeModelClosePriceMA5GoldCrossIncr();
    }

    public static void writeModelClosePriceMA5DeadCross() {
        modelClosePriceMA5DeadCrossService.writeModelClosePriceMA5DeadCross();
    }

    public static void writeModelClosePriceMA5DeadCrossIncr() {
        modelClosePriceMA5DeadCrossService.writeModelClosePriceMA5DeadCrossIncr();
    }

    public static void writeModelHeiKinAshiUpDown() {
        modelHeiKinAshiUpDownService.writeModelHeiKinAshiUpDown();
    }

    public static void writeModelHeiKinAshiUpDownIncr() {
        modelHeiKinAshiUpDownService.writeModelHeiKinAshiUpDownIncr(Boolean.FALSE);
    }

    public static void writeModelHeiKinAshiDownUp() {
        modelHeiKinAshiDownUpService.writeModelHeiKinAshiDownUp();
    }

    public static void writeModelHeiKinAshiDownUpIncr() {
        modelHeiKinAshiDownUpService.writeModelHeiKinAshiDownUpIncr(Boolean.FALSE);
    }

    public static void writeModelKDGoldCross() {
        modelKDGoldCrossService.writeModelKDGoldCross();
    }

    public static void writeModelKDDeadCross() {
        modelKDDeadCrossService.writeModelKDDeadCross();
    }

    public static void writeModelStockIndexMACDGoldCross() {
        modelStockIndexMACDGoldCrossService.writeModelStockIndexMACDGoldCross();
    }

    public static void writeModelStockIndexClosePriceMA5GoldCross() {
        modelStockIndexClosePriceMA5GoldCrossService.writeModelStockIndexClosePriceMA5GoldCross();
    }

    public static void writeModelStockIndexHeiKinAshiDownUp() {
        modelStockIndexHeiKinAshiDownUpService.writeModelStockIndexHeiKinAshiDownUp();
    }

    public static void writeModelStockIndexKDGoldCross() {
        modelStockIndexKDGoldCrossService.writeModelStockIndexKDGoldCross();
    }

    public static void writeModelEtfMACDGoldCross() {
        modelEtfMACDGoldCrossService.writeModelEtfMACDGoldCross();
    }

    public static void writeModelEtfClosePriceMA5GoldCross() {
        modelEtfClosePriceMA5GoldCrossService.writeModelEtfClosePriceMA5GoldCross();
    }

    public static void writeModelEtfHeiKinAshiDownUp(){
        modelEtfHeiKinAshiDownUpService.writeModelEtfHeiKinAshiDownUp();
    }

    public static void writeModelEtfKDGoldCross() {
        modelEtfKDGoldCrossService.writeModelEtfKDGoldCross();
    }

    public static void writeModelEtfBullShortLineUp() {
        modelEtfBullShortLineUpService.writeModelEtfBullShortLineUp();
    }

    public static void writeModelAllGoldCross() {
        modelAllGoldCrossService.writeModelAllGoldCross();
    }

    public static void writeModelWeekAllGoldCross() {
        modelWeekAllGoldCrossService.writeModelWeekAllGoldCross();
    }

    public static void writeModelStockAnalysis() {
        modelStockAnalysisService.writeModelStockAnalysis();
    }

    public static void writeModelGoldCrossDeadCrossAnalysis() {
        modelGoldCrossDeadCrossAnalysisService.writeModelGoldCrossDeadCrossAnalysis();
    }

    public static void writeModelGoldCrossDeadCrossAnalysisPercentMA() {
        modelGoldCrossDeadCrossAnalysisService.writeModelGoldCrossDeadCrossAnalysisPercentMA();
    }

    public static void writeModelGoldCrossDeadCrossAnalysisSuccessRateMA() {
        modelGoldCrossDeadCrossAnalysisService.writeModelGoldCrossDeadCrossAnalysisSuccessRateMA();
    }

    public static void writeModelStockWeekAnalysis() {
        modelStockWeekAnalysisService.writeModelStockWeekAnalysis();
    }

    public static void writeModelStockMonthAnalysis() {
        modelStockMonthAnalysisService.writeModelStockMonthAnalysis();
    }

    /*********************************************************************************************************************
     *
     * 打印文本报告
     *
     *********************************************************************************************************************/
    public static void printSubNewStock() {
        modelSubNewStockService.printSubNewStock();
    }

    public static void printBandOperationStockAndSendMail() {
        printStockService.printBandOperationStockAndSendMail();
    }

    /*******************************************************************************************************************
     *
     * 生成报告
     *
     ******************************************************************************************************************/
    public static void generateReport() {
        reportService.storeReport();
        reportService.createReport();
    }

    /*******************************************************************************************************************
     *
     * 删除stock_transaction_data表中的记录
     *
     ******************************************************************************************************************/
    public static void deleteStockTransactionDataByDate() {
        stockTransactionDataService.deleteStockTransactionDataByDate();
    }

    /*******************************************************************************************************************
     *
     * 实验
     *
     *******************************************************************************************************************/
    public static void test() {
        System.out.println(new ReportDaoImpl().selectStockWeekendKDUp("20160307", "20160311", "30"));
    }

    /*******************************************************************************************************************
     *
     * 实验室
     *
     *******************************************************************************************************************/
    public static void createAllTransactionStrategyGoldCrossSuccessRatePicture() {
        modelService.createAllTransactionStrategyGoldCrossSuccessRatePicture(false);
    }

    public static void createAllTransactionStrategyDeadCrossSuccessRatePicture() {
        modelService.createAllTransactionStrategyDeadCrossSuccessRatePicture(false);
    }

    public static void createModelTopStockDetailPicture() {
        modelTopStockDetailService.createModelTopStockDetailPicture();
    }

    public static void createAllTransactionStrategyGoldCrossPercentagePicture() {
        modelService.createAllTransactionStrategyGoldCrossPercentagePicture(false);
    }

    public static void createAllTransactionStrategyDeadCrossPercentagePicture() {
        modelService.createAllTransactionStrategyDeadCrossPercentagePicture(false);
    }

    public static void createRobotAccountLogProfitRatePicture() {
        modelService.createRobotAccountLogProfitRatePicture(false);
    }

    public static void createRobotEtfAccountLogProfitRatePicture() {
        modelService.createRobotEtfAccountLogProfitRatePicture(false);
    }

    public static void createWeekBollUpDnPercentAndGoldCrossDeadCrossProfitScatterPlotPicture(){
        modelWeekBollMACDGoldCrossService.createWeekLowestPriceDnBollPercentAndMACDGoldCrossProfitScatterPlotPicture();
        modelWeekBollMACDDeadCrossService.createWeekHeighestPriceUpBollPercentAndMACDDeadCrossProfitScatterPlotPicture();
        modelWeekBollClosePriceMA5GoldCrossService.createWeekLowestPriceDnBollPercentAndClosePriceGoldCrossMA5ProfitScatterPlotPicture();
        modelWeekBollClosePriceMA5DeadCrossService.createWeekHeighestPriceUpBollPercentAndClosePriceDeadCrossMA5ProfitScatterPlotPicture();
        modelWeekBollHeiKinAshiUpDownService.createWeekLowestPriceDnBollPercentAndHeiKinAshiUpDownProfitScatterPlotPicture();
        modelWeekBollHeiKinAshiDownUpService.createWeekHeighestPriceUpBollPercentAndHeiKinAshiDownUpProfitScatterPlotPicture();
        modelWeekBollKDGoldCrossService.createWeekLowestPriceDnBollPercentAndKDGoldCrossProfitScatterPlotPicture();
        modelWeekBollKDDeadCrossService.createWeekHeighestPriceUpBollPercentAndKDDeadCrossProfitScatterPlotPicture();
    }

    public static void createModelStockIndexProfitRatePicture(){
        modelStockIndexMACDGoldCrossService.createModelStockIndexMACDGoldCrossProfitRatePicture();
    }

    public static void writeBoard2IndexDaily(){
        board2IndexService.writeBoard2IndexDaily();
    }

    /*******************************************************************************************************************
     *
     * 备份项目代码，并删除备份目录下的target目录
     *
     *******************************************************************************************************************/
    public static void backupCode() {
        projectService.backupCode();
    }

    /*******************************************************************************************************************
     *
     * 备份数据库/表（海量和增量）
     *
     *******************************************************************************************************************/
    public static void backupDatabaseMass() {
        projectService.backupDatabaseMass();
    }

    public static void backupDatabaseIncremental() {
        projectService.backupDatabaseIncrementalByJson(Boolean.FALSE);
    }

    /*******************************************************************************************************************
     *
     * 使操作系统进入睡眠状态
     *
     *******************************************************************************************************************/
    public static void makeOperationSystemSleep(){
        projectService.makeOperationSystemSleep();
    }

    /*******************************************************************************************************************
     *
     * 基于基础数据的并行处理：计算数据，生成图片
     *
     *******************************************************************************************************************/
    public static void asyncHandleBasedOnBasicData() {
        wechatSubscriptionService.asyncHandleBasedOnBasicData();
    }

    /*******************************************************************************************************************
     *
     * 基于移动平均线的并行处理：计算数据，生成图片
     *
     *******************************************************************************************************************/
    public static void asyncHandleBasedOnMovingAverage() {
        wechatSubscriptionService.asyncHandleBasedOnMovingAverage();
    }

    /*******************************************************************************************************************
     *
     * 基于MACD的并行处理：计算数据，生成图片
     *
     *******************************************************************************************************************/
    public static void asyncHandleBasedOnMACD() {
        wechatSubscriptionService.asyncHandleBasedOnMACD();
    }

    /*******************************************************************************************************************
     *
     * 基于stock_index表的并行处理：计算数据，生成图片
     *
     *******************************************************************************************************************/
    public static void asyncHandleBasedOnStockIndex() {
        wechatSubscriptionService.asyncHandleBasedOnStockIndex();
    }

    /*******************************************************************************************************************
     *
     * 基于board_index的并行处理：计算数据，生成图片
     *
     *******************************************************************************************************************/
    public static void asyncHandleBasedOnBoardIndex() {
        wechatSubscriptionService.asyncHandleBasedOnBoardIndex();
    }

    /*******************************************************************************************************************
     *
     * 基基于周线级别数据的穿行处理：计算数据，生成图片
     *
     *******************************************************************************************************************/
    public static void handleBasedOnWeekData() {
        wechatSubscriptionService.handleBasedOnWeekData();
    }

    /*******************************************************************************************************************
     *
     * 生成微信订阅号报告
     *
     *******************************************************************************************************************/
    public static void generateWechatSubscriptionReport() {
        wechatSubscriptionService.generateWechatSubscriptionReport();
    }

    /*******************************************************************************************************************
     *
     * 生成微信订阅号报告之后的处理：计算数据
     *
     *******************************************************************************************************************/
    public static void calculateBasicDataAfterGeneratingWechatSubscriptionReport() {
        wechatSubscriptionService.calculateBasicDataAfterGeneratingWechatSubscriptionReport();
    }

    /*******************************************************************************************************************
     *
     * RobotMain类--卖股票
     *
     *******************************************************************************************************************/
    public static void preCalculateTransactionCondition_real() {
        realTransactionConditionService.preCalculateTransactionCondition_real();
    }

    public static void preCalculateTransactionCondition_real3() {
        real3TransactionConditionService.preCalculateTransactionCondition_real3();
    }

    public static void preCalculateTransactionCondition_real4() {
        real4TransactionConditionService.preCalculateTransactionCondition_real4();
    }

    public static void realTimeJudgeConditionAndGiveSuggestion() {
        realStockTransactionRecordService.realTimeJudgeConditionAndGiveSuggestion();
    }

    public static void doBuyAndSellByBeginDateAndEndDate_successRate() {
        robotStockTransactionRecordService.doBuyAndSellByBeginDateAndEndDate_successRate();
    }

    public static void doBuyAndSellByBeginDateAndEndDate_averageGoldCrossDeadCross() {
        robot2StockTransactRecordService.doBuyAndSellByBeginDateAndEndDate_averageGoldCrossDeadCross();
    }

    public static void doBuyAndSellByBeginDateAndEndDate_weekBoll() {
        robot3StockTransactRecordService.doBuyAndSellByBeginDateAndEndDate_weekBoll();
    }

    public static void doBuyAndSellByBeginDateAndEndDate_week() {
        robot4StockTransactRecordService.doBuyAndSellByBeginDateAndEndDate_week();
    }

    public static void doBuyAndSellByBeginDateAndEndDate_multipleMethod(){
        robot5StockTransactRecordService.doBuyAndSellByBeginDateAndEndDate_multipleMethod();
    }

    public static void doBuyAndSellByBeginDateAndEndDate_etf12YearHighestAverageProfitRate() {
        robot6StockTransactRecordService.doBuyAndSellByBeginDateAndEndDate_etf12YearHighestAverageProfitRate();
    }

    public static void importIntoHistoryDataTableFromRobot() {
        modelService.importIntoHistoryDataTableFromRobot();
    }

    public static void importIntoHistoryDataTableFromRobot2() {
        modelService.importIntoHistoryDataTableFromRobot2();
    }

    public static void importIntoHistoryDataTableFromRobot3() {
        modelService.importIntoHistoryDataTableFromRobot3();
    }

    public static void importIntoHistoryDataTableFromRobot4() {
        modelService.importIntoHistoryDataTableFromRobot4();
    }

    public static void importIntoHistoryDataTableFromRobot5() {
        modelService.importIntoHistoryDataTableFromRobot5();
    }

    public static void importIntoHistoryDataTableFromRobot6() {
        modelService.importIntoHistoryDataTableFromRobot6();
    }

    public static void updateRobotAccountBeforeSellOrBuy() {
        robotAccountService.updateRobotAccountBeforeSellOrBuy();
    }

    public static void sellOrBuy() {
        robotStockTransactionRecordService.sellOrBuy();
    }

    public static void updateRobotAccountAfterSellOrBuy() {
        robotAccountService.updateRobotAccountAfterSellOrBuy();
    }

    /*******************************************************************************************************************
     *
     * RobotMain类--过滤股票
     *
     *******************************************************************************************************************/
    public static void initBullRobotStockFilter() {
        robotStockFilterService.initBullRobotStockFilter();
    }

    public static void initShortRobotStockFilter() {
        robotStockFilterService.initShortRobotStockFilter();
    }

    public static void filterByLessThanClosePrice() {
        robotStockFilterService.filterByLessThanClosePrice();
    }

    public static void filterByXr() {
        robotStockFilterService.filterByXr();
    }

    public static void filterByWeekKDGoldCross() {
        robotStockFilterService.filterByWeekKDGoldCross();
    }

    public static void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice() {
        robotStockFilterService.filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice();
    }

    public static void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice() {
        robotStockFilterService.filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice();
    }

    public static void filterByMANotDecreasing() {
        robotStockFilterService.filterByMANotDecreasing();
    }

    public static void filterByMANotIncreasing() {
        robotStockFilterService.filterByMANotIncreasing();
    }

    public static void selectHighestSuccessRateStrategy() {
        robotStockFilterService.selectHighestSuccessRateStrategy(false);
    }

    public static void filterByHighestSuccessRateStrategy() {
        robotStockFilterService.filterByHighestSuccessRateStrategy();
    }

    public static void filterByMACDGoldCross() {
        robotStockFilterService.filterByMACDGoldCross();
    }

    public static void filterByClosePriceGoldCrossMA5() {
        robotStockFilterService.filterByClosePriceGoldCrossMA5();
    }

    public static void filterByHeiKinAshiUp() {
        robotStockFilterService.filterByHeiKinAshiUp();
    }

    public static void filterByKDGoldCross() {
        robotStockFilterService.filterByKDGoldCross();
    }

    /*******************************************************************************************************************
     *
     * RobotMain类--买股票
     *
     *******************************************************************************************************************/
    public static void buyOrSell() {
        robotStockTransactionRecordService.buyOrSell();
    }

    public static void updateRobotAccountAfterBuyOrSell() {
        robotAccountService.updateRobotAccountAfterBuyOrSell();
    }

    public static void printBuySellSuggestion() {
        robotStockTransactionRecordService.printBuySellSuggestion();
    }

    /*******************************************************************************************************************
     *
     * RealTransactionMain类--发送邮件
     *
     *******************************************************************************************************************/
    public static void sendEMail() {
        projectService.sendEMail();
    }

    /*******************************************************************************************************************
     *
     * RealTransactionMain类--定时地判断买卖条件，给出交易建议，保存在文件中，发送邮件
     *
     *******************************************************************************************************************/
    public static void realTimeJudgeConditionAndGiveSuggestionAndSendEMail_real() {
        logger.info("开始定时地判断买卖条件，给出交易建议，保存在文件中，发送邮件");

        final String MODEL_FILTER_PROPERTIES =
                System.getProperty("user.dir") + "/src/main/resources/model-filter.properties";

        // 参数
        String beginDateString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.schedule.begin.date");
        String endDateString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.schedule.end.date");
        Integer interval = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real.transaction.schedule.interval"));

        Integer beginHour = Integer.parseInt(beginDateString.split(":")[0]);
        Integer beginMinute = Integer.parseInt(beginDateString.split(":")[1]);
        Integer beginSecond = Integer.parseInt(beginDateString.split(":")[2]);
        Date beginDate = new Date();
        beginDate.setHours(beginHour);
        beginDate.setMinutes(beginMinute);
        beginDate.setSeconds(beginSecond);
        Integer endHour = Integer.parseInt(endDateString.split(":")[0]);
        Integer endMinute = Integer.parseInt(endDateString.split(":")[1]);
        Integer endSecond = Integer.parseInt(endDateString.split(":")[2]);
        Date endDate = new Date();
        endDate.setHours(endHour);
        endDate.setMinutes(endMinute);
        endDate.setSeconds(endSecond);

        while (true) {
            if (beginDate.compareTo(new Date()) == 1) {
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (new Date().compareTo(endDate) == 1) {
                break;
            } else {
                realStockTransactionRecordService.realTimeJudgeConditionAndGiveSuggestion();
                projectService.sendEMail();
            }

        }
    }

    /*******************************************************************************************************************
     *
     * Real3TransactionMain类--定时地判断买卖条件，给出交易建议，保存在文件中，发送邮件
     *
     *******************************************************************************************************************/
    public static void realTimeJudgeConditionAndGiveSuggestionAndSendEMail_real3() {
        logger.info("开始定时地判断买卖条件，给出交易建议，保存在文件中，发送邮件");

        final String MODEL_FILTER_PROPERTIES =
                System.getProperty("user.dir") + "/src/main/resources/model-filter.properties";

        // 参数
        String beginDateString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real3.transaction.schedule.begin.date");
        String endDateString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real3.transaction.schedule.end.date");
        Integer interval = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real3.transaction.schedule.interval"));

        Integer beginHour = Integer.parseInt(beginDateString.split(":")[0]);
        Integer beginMinute = Integer.parseInt(beginDateString.split(":")[1]);
        Integer beginSecond = Integer.parseInt(beginDateString.split(":")[2]);
        Date beginDate = new Date();
        beginDate.setHours(beginHour);
        beginDate.setMinutes(beginMinute);
        beginDate.setSeconds(beginSecond);
        Integer endHour = Integer.parseInt(endDateString.split(":")[0]);
        Integer endMinute = Integer.parseInt(endDateString.split(":")[1]);
        Integer endSecond = Integer.parseInt(endDateString.split(":")[2]);
        Date endDate = new Date();
        endDate.setHours(endHour);
        endDate.setMinutes(endMinute);
        endDate.setSeconds(endSecond);

        while (true) {
            if (beginDate.compareTo(new Date()) == 1) {
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (new Date().compareTo(endDate) == 1) {
                break;
            } else {
                real3StockTransactionRecordService.realTimeJudgeConditionAndGiveSuggestion();
                projectService.sendEMail();
            }

        }
    }

    /*******************************************************************************************************************
     *
     * Real4TransactionMain类--定时地判断买卖条件，给出交易建议，保存在文件中，发送邮件
     *
     *******************************************************************************************************************/
    public static void realTimeJudgeConditionAndGiveSuggestionAndSendEMail_real4() {
        logger.info("开始定时地判断买卖条件，给出交易建议，保存在文件中，发送邮件");

        final String MODEL_FILTER_PROPERTIES =
                System.getProperty("user.dir") + "/src/main/resources/model-filter.properties";

        // 参数
        String beginDateString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real4.transaction.schedule.begin.date");
        String endDateString = PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real4.transaction.schedule.end.date");
        Integer interval = Integer.parseInt(PropertiesUtil.getValue(MODEL_FILTER_PROPERTIES, "filter.real4.transaction.schedule.interval"));

        Integer beginHour = Integer.parseInt(beginDateString.split(":")[0]);
        Integer beginMinute = Integer.parseInt(beginDateString.split(":")[1]);
        Integer beginSecond = Integer.parseInt(beginDateString.split(":")[2]);
        Date beginDate = new Date();
        beginDate.setHours(beginHour);
        beginDate.setMinutes(beginMinute);
        beginDate.setSeconds(beginSecond);
        Integer endHour = Integer.parseInt(endDateString.split(":")[0]);
        Integer endMinute = Integer.parseInt(endDateString.split(":")[1]);
        Integer endSecond = Integer.parseInt(endDateString.split(":")[2]);
        Date endDate = new Date();
        endDate.setHours(endHour);
        endDate.setMinutes(endMinute);
        endDate.setSeconds(endSecond);

        while (true) {
            if (beginDate.compareTo(new Date()) == 1) {
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (new Date().compareTo(endDate) == 1) {
                break;
            } else {
                real4StockTransactionRecordService.realTimeJudgeConditionAndGiveSuggestion();
                projectService.sendEMail();
            }

        }
    }
}
