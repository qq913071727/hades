package cn.com.acca.ma.model;

import java.util.Date;

import javax.persistence.Id;

public class StockIndexPK {
	@Id
	private Date indexDate;
	@Id
	private String indexCode;
	
	public StockIndexPK() {
	}
	
	public StockIndexPK(Date indexDate, String indexCode) {
		super();
		this.indexDate = indexDate;
		this.indexCode = indexCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((indexCode == null) ? 0 : indexCode.hashCode());
		result = prime * result
				+ ((indexDate == null) ? 0 : indexDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockIndexPK other = (StockIndexPK) obj;
		if (indexCode == null) {
			if (other.indexCode != null)
				return false;
		} else if (!indexCode.equals(other.indexCode))
			return false;
		if (indexDate == null) {
			if (other.indexDate != null)
				return false;
		} else if (!indexDate.equals(other.indexDate))
			return false;
		return true;
	}
}
