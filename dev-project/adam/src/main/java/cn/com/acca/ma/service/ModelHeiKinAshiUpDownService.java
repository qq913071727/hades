package cn.com.acca.ma.service;

import cn.com.acca.ma.model.ModelHeiKinAshiUpDown;
import cn.com.acca.ma.model.ModelMACDGoldCross;
import java.util.Date;
import java.util.List;

public interface ModelHeiKinAshiUpDownService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线ha_close_price大于ha_open_price时买入，ha_close_price小于等于ha_open_price时卖出，
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN插入数据
     * @param multithreading
     */
    void writeModelHeiKinAshiUpDownIncr(boolean multithreading);

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_close_price大于ha_open_price时买入，ha_close_price小于等于ha_open_price时卖出，
     * 向表MDL_HEI_KIN_ASHI_UP_DOWN插入数据
     */
    void writeModelHeiKinAshiUpDown();


}
