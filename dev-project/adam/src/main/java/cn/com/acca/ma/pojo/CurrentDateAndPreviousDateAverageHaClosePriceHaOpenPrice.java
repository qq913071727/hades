//package cn.com.acca.ma.pojo;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.util.Date;
//
///**
// * 当前日期和前一日平均ha_close_price和平均ha_open_price
// */
////@Data
////@Entity
//public class CurrentDateAndPreviousDateAverageHaClosePriceHaOpenPrice extends BaseCurrentDataAndPreviousDateAverage implements Comparable {
//
//    /**
//     * 当前日期
//     */
////    @Id
//    private String currentDate;
//
//    /**
//     * 前一日
//     */
////    private String previousDate;
//
//    /**
//     * 当前日期ha_close_price和ha_open_price的平均值
//     */
////    private BigDecimal averageHaClosePriceHaOpenPrice;
//
//    /**
//     * 当前日期的平均ha_close_price
//     */
////    private BigDecimal currentAverageHaClosePrice;
//
//    /**
//     * 当前日期的平均ha_open_price
//     */
////    private BigDecimal currentAverageHaOpenPrice;
//
//    /**
//     * 前一日的平均ha_close_price
//     */
////    private BigDecimal previousAverageHaClosePrice;
//
//    /**
//     * 前一日的平均ha_open_price
//     */
////    private BigDecimal previousAverageHaOpenPrice;
//
//    /**
//     * 上涨趋势或下跌趋势。1表示金叉，2表示死叉
//     */
//    private Integer upDownOrDownUp;
//
//    /**
//     * 降序排列
//     * @param o
//     * @return
//     */
//    @Override
//    public int compareTo(Object o) {
//        CurrentDateAndPreviousDateAverageHaClosePriceHaOpenPrice currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice = (CurrentDateAndPreviousDateAverageHaClosePriceHaOpenPrice)o;
//        return currentDateAndPreviousDateAverageHaClosePriceHaOpenPrice.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage());
////        BigDecimal result = currentDateAndPreviousDateAverageDifDea.getCurrentAverageDif().add(currentDateAndPreviousDateAverageDifDea.getCurrentAverageDea()).divide(new BigDecimal(2))
////                .subtract(this.getCurrentAverageDif().add(this.getCurrentAverageDea()).divide(new BigDecimal(2)));
////        if (currentDateAndPreviousDateAverageDifDea.getSuccessRateOrPercentage().compareTo(this.getSuccessRateOrPercentage()) > 0){
////            return 1;
////        }
////        if (result.doubleValue() < 0){
////            return  -1;
////        }
////        if (result.doubleValue() == 0){
////            return 0;
////        }
////        return 0;
//    }
//}
