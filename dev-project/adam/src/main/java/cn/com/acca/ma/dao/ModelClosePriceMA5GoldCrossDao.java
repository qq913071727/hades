package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelAllGoldCross;
import cn.com.acca.ma.model.ModelClosePriceMA5GoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelClosePriceMA5GoldCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     */
    void writeModelClosePriceMA5GoldCross();

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
     * @param endDate
     */
    void writeModelClosePriceMA5GoldCrossIncr(String endDate);

    /**
     * 获取一段时间内，最近dateNumber天，close_price金叉ma5的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    List<SuccessRateOrPercentagePojo> getClosePriceMa5GoldCrossSuccessRate(boolean multithreading, String beginDate, String endDate, Integer dateNumber);

    /**
     * 获取一段时间内，close_price金叉ma5的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<SuccessRateOrPercentagePojo> getClosePriceMa5GoldCrossPercentage(boolean multithreading, final String beginDate, final String endDate);

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中获取sell_date日的所有ModelClosePriceMA5GoldCross对象
     * @param date
     * @return
     */
    List<ModelClosePriceMA5GoldCross> getModelClosePriceMA5GoldCrossByDate(String date);

    /**
     * 向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入ModelClosePriceMA5GoldCross对象
     * @param modelClosePriceMA5GoldCross
     */
    void save(ModelClosePriceMA5GoldCross modelClosePriceMA5GoldCross);
}
