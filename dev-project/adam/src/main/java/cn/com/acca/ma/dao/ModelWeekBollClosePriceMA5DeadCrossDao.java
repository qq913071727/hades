package cn.com.acca.ma.dao;

import cn.com.acca.ma.pojo.ProfitOrLossAndWeekBollUpOrDnPercentage;

import java.util.List;

public interface ModelWeekBollClosePriceMA5DeadCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_WEEK_BOLL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    void writeModelWeekBollClosePriceMA5DeadCross();

    /**
     * 查询close_price死叉MA5交易收益率和最高价突破布林带上轨的百分比
     * @param beginDate
     * @param endDate
     */
    List<ProfitOrLossAndWeekBollUpOrDnPercentage> findWeekClosePriceDeadCrossMA5ProfitOrLossAndBollUp(String beginDate, String endDate);
}
