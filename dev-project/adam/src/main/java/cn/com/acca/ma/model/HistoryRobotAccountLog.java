package cn.com.acca.ma.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 机器人--账户日志表（历史数据）
 */
@Data
public class HistoryRobotAccountLog {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 日期
     */
    private Date date_;

    /**
     * 机器人名字
     */
    private String robotName;

    /**
     * 持股数量
     */
    private Integer holdStockNumber;

    /**
     * 股票资产
     */
    private BigDecimal stockAssets;

    /**
     * 资金资产
     */
    private BigDecimal capitalAssets;

    /**
     * 总资产
     */
    private BigDecimal totalAssets;

    /**
     * 收益率
     */
    private BigDecimal profitRate;

    /**
     * 模型id
     */
    private Integer modelId;

}
