package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 更新StockIndex对象某一日的数据
 */
public class WriteStockIndexByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteStockIndexByDateThread.class);

    private StockIndexService stockIndexService;

    public WriteStockIndexByDateThread() {
    }

    public WriteStockIndexByDateThread(StockIndexService stockIndexService) {
        this.stockIndexService = stockIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始更新StockIndex对象某一日的数据，调用的方法为【writeMovingAverageByDate】");

        stockIndexService.writeStockIndexByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }

}
