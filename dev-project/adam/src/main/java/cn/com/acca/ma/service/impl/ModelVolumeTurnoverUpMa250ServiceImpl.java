package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelKDGoldCross;
import cn.com.acca.ma.model.ModelVolumeTurnoverUpMa250;
import cn.com.acca.ma.service.ModelKDGoldCrossService;
import cn.com.acca.ma.service.ModelVolumeTurnoverUpMa250Service;

import java.util.List;

public class ModelVolumeTurnoverUpMa250ServiceImpl extends BaseServiceImpl<ModelVolumeTurnoverUpMa250ServiceImpl, ModelVolumeTurnoverUpMa250> implements
		ModelVolumeTurnoverUpMa250Service {

	/*********************************************************************************************************************
	 *
	 * 												按日期计算数据
	 *
	 *********************************************************************************************************************/
	/**
	 * 根据日期endDate，使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据
	 */
	public void writeModelVolumeTurnoverUpMa250Incr() {
		String endDate = PropertiesUtil
			.getValue(MODEL_VOLUME_TURNOVER_UP_MA250_PROPERTIES, "volume.turnover.up.ma250.end_date");
		modelVolumeTurnoverUpMa250Dao.writeModelVolumeTurnoverUpMa250Incr(endDate);
	}
	
	/*********************************************************************************************************************
	 * 
	 * 												计算全部数据
	 * 
	 *********************************************************************************************************************/
	/**
	 * 使用成交量/成交额突破MA250模型算法，向表MDL_VOLUME_TURNOVER_UP_MA250插入数据
	 */
	public void writeModelVolumeTurnoverUpMa250() {
		modelVolumeTurnoverUpMa250Dao.writeModelVolumeTurnoverUpMa250();
	}

	@Override
	public String listToString(List list) {
		return null;
	}
}
