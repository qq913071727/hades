package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.RealStockTransactionRecord;
import cn.com.acca.ma.model.RealTransactionCondition;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface RealTransactionConditionDao extends BaseDao {

    /**
     * 向real_transaction_condition表中写数据。
     * ma120NotDecreasing为1时表示判断120均线是否不单调递减，ma120NotDecreasingDateNumber表示120日均线多少天内不单调递减；
     * ma250NotDecreasing为1时表示判断250均线是否不单调递减，ma250NotDecreasingDateNumber表示250日均线多少天内不单调递减。
     * ma120NotIncreasing为1时表示判断120均线是否不单调递增，ma120NotIncreasingDateNumber表示120日均线多少天内不单调递增；
     * ma250NotIncreasing为1时表示判断250均线是否不单调递增，ma250NotIncreasingDateNumber表示250日均线多少天内不单调递增。
     * @param xrBeginDate
     * @param date
     * @param lessThanPercentage
     * @param moreThanPercentage
     * @param closePriceStart
     * @param closePriceEnd
     * @param ma120NotDecreasing
     * @param ma120NotDecreasingDateNumber
     * @param ma250NotDecreasing
     * @param ma250NotDecreasingDateNumber
     * @param ma120NotIncreasing
     * @param ma120NotIncreasingDateNumber
     * @param ma250NotIncreasing
     * @param ma250NotIncreasingDateNumber
     */
    void writeRealTransactionCondition(String xrBeginDate, String date, /*Integer lessThanPercentage,
                                       Integer moreThanPercentage, Double closePriceStart, Double closePriceEnd,*/
                                       Integer ma120NotDecreasing, Integer ma120NotDecreasingDateNumber,
                                       Integer ma250NotDecreasing, Integer ma250NotDecreasingDateNumber,
                                       Integer ma120NotIncreasing, Integer ma120NotIncreasingDateNumber,
                                       Integer ma250NotIncreasing, Integer ma250NotIncreasingDateNumber);

    /**
     * 清空real_transaction_condition表中的数据
     */
    void truncateTableRealTransactionCondition();

    /**
     * 根据stock_code字段查找RealTransactionCondition类型对象
     * @param stockCode
     * @return
     */
    RealTransactionCondition findByStockCode(String stockCode);

    /**
     * 根据条件查找记录：未除权、120日和250日均线不单调递减
     * 如果allFilterCondition为true，则使用所有过滤条件；否则只是有价格区间条件
     * @param allFilterCondition
     * @return
     */
    List<RealTransactionCondition> findByCondition(boolean allFilterCondition);

}
