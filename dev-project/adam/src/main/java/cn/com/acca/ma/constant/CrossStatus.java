package cn.com.acca.ma.constant;

/**
 * 金叉或死叉/上涨趋势或下跌趋势
 */
public class CrossStatus {

    /**
     * 金叉状态
     */
    public static final Integer GOLD_CROSS = 1;

    /**
     * 死叉状态
     */
    public static final Integer DEAD_CROSS = 2;

}
