package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelKDGoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelKDGoldCrossDao extends BaseDao {
	/**
	 * 使用KD金叉模型算法，向表MDL_KD_GOLD_CROSS插入数据
	 */
	void writeModelKDGoldCross();

	/**
	 * 根据日期endDate，使用KD金叉模型算法，向表MDL_KD_GOLD_CROSS插入数据
	 * @param endDate
	 */
	void writeModelKDGoldCrossIncr(String endDate);

	/**
	 * 获取一段时间内，最近dateNumber天，kd金叉的成功率
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @param dateNumber
	 * @return
	 */
	List<SuccessRateOrPercentagePojo> getKdGoldCrossSuccessRate(boolean multithreading, String beginDate, String endDate, Integer dateNumber);

	/**
	 * 获取一段时间内，kd金叉的百分比
	 * @param multithreading
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<SuccessRateOrPercentagePojo> getKdGoldCrossPercentage(boolean multithreading, String beginDate, String endDate);

	/**
	 * 根据开始时间beginDate和结束时间endDate，从表MDL_KD_GOLD_CROSS中获取sell_date字段
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<Date> getDateByCondition(String beginDate, String endDate);

	/**
	 * 从表MDL_KD_GOLD_CROSS中获取date日的所有ModelKDGoldCross对象
	 * @param date
	 * @return
	 */
	List<ModelKDGoldCross> getModelKDGoldCrossByDate(String date);

	/**
	 * 向表MDL_KD_GOLD_CROSS中插入ModelKDGoldCross对象
	 * @param modelKDGoldCross
	 */
	void save(ModelKDGoldCross modelKDGoldCross);
}
