package cn.com.acca.ma.pojo;

import cn.com.acca.ma.model.Board;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@Data
public class StockInfoPojo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 表示股票代码
     */
    private String code;

    /**
     * 表示股票名称
     */
    private String name;

    /**
     * 表示股票所属的板块
     */
    private Board board;

    /**
     * 获取股票实时交易数据的url中的参数
     */
    private String urlParam;

    /**
     * R表示融资融券
     */
    private String mark;

    /**
     * 买入/卖出数量
     */
    private Integer amount;

    /**
     * 算法
     */
    private Integer filterType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockInfoPojo that = (StockInfoPojo) o;
//        return Objects.equals(id, that.id) && Objects.equals(code, that.code) && Objects.equals(name, that.name) && Objects.equals(board, that.board) && Objects.equals(urlParam, that.urlParam) && Objects.equals(mark, that.mark) && Objects.equals(amount, that.amount);
        return code.equals(that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, board, urlParam, mark, amount, filterType);
    }
}
