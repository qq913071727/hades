package cn.com.acca.ma.dao;

public interface ModelPercentageMaGoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 使用percentage*_ma*金叉模型算法，海量地向表MDL_PERCENTAGE_MA_GOLD_CROSS插入数据
     */
    void writeModelPercentageMaGoldCross();
}
