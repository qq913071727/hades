package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.common.util.DateUtil;
import cn.com.acca.ma.dao.ModelGoldCrossDeadCrossAnalysisDao;
import cn.com.acca.ma.hibernate.impl.FIndGoldCrossDeadCrossDateCountWorkImpl;
import cn.com.acca.ma.hibernate.impl.FindDataForSpiderWebPlotWorkImpl;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelGoldCrossDeadCrossAnalysis;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModelGoldCrossDeadCrossAnalysisDaoImpl extends BaseDaoImpl<ModelGoldCrossDeadCrossAnalysis> implements
        ModelGoldCrossDeadCrossAnalysisDao {

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的基础数据
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysis() {
        logger.info("开始计算MDL_G_C_D_C_ANALYSIS表的基础数据");

        StringBuffer hql = new StringBuffer("insert into mdl_g_c_d_c_analysis(DATE_, MACD_G_C_PERCENT, " +
                "MACD_D_C_PERCENT, CLOSE_PRICE_G_C_MA5_PERCENT, CLOSE_PRICE_D_C_MA5_PERCENT, " +
                "HEI_KIN_ASHI_U_D_PERCENT, HEI_KIN_ASHI_D_U_PERCENT, KD_G_C_PERCENT, KD_D_C_PERCENT) " +
                "select * from( " +
                "select t.date_ DATE_, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.dif>t1.dea and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 MACD_G_C_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.dif<t1.dea and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 MACD_D_C_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.close_price>t1.ma5 and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 CLOSE_PRICE_G_C_MA5_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.close_price<t1.ma5 and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 CLOSE_PRICE_D_C_MA5_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.ha_close_price>t1.ha_open_price and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 HEI_KIN_ASHI_U_D_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.ha_close_price<t1.ha_open_price and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 HEI_KIN_ASHI_D_U_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.k>t1.d and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 KD_G_C_PERCENT, " +
                "(select count(*) from stock_transaction_data_all t1 where t1.k<t1.d and t1.date_=t.date_)/" +
                "(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 KD_D_C_PERCENT " +
                "from stock_transaction_data_all t " +
                "group by t.date_ " +
                "order by t.date_ asc)");
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery(hql.toString());
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的百分比的均值
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysisPercentMA() {
        logger.info("开始计算MDL_G_C_D_C_ANALYSIS表的百分比的均值");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("{call PKG_MODEL_G_C_D_C_ANALYSIS.WRITE_MDL_G_C_D_C_A_PERCENT_MA()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 计算MDL_G_C_D_C_ANALYSIS表的成功率的均值
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysisSuccessRateMA() {
        logger.info("开始计算MDL_G_C_D_C_ANALYSIS表的成功率的均值");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("{call PKG_MODEL_G_C_D_C_ANALYSIS.WRITE_MDL_G_C_D_C_A_S_R_MA()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 根据date，查找最近两条记录，降序排列
     *
     * @param date
     * @return
     */
    @Override
    public List<ModelGoldCrossDeadCrossAnalysis> findTwoByDateOrderByDateDesc(String date) {
        logger.info("根据date【" + date + "】，查找记录");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("select * from (select * from MDL_G_C_D_C_ANALYSIS t where t.date_<=to_date(?, 'yyyy-mm-dd') order by t.date_ desc) where rownum<=2").addEntity(ModelGoldCrossDeadCrossAnalysis.class);
        query.setParameter(0, date);
        List<ModelGoldCrossDeadCrossAnalysis> modelGoldCrossDeadCrossAnalysisList = query.list();
        session.getTransaction().commit();
        session.close();
        return modelGoldCrossDeadCrossAnalysisList;
    }

    /**
     * 查询各种算法状态已经持续了多少天
     * 1表示MACD金叉，2表示MACD死叉，3表示收盘价金叉五日均线，4表示收盘价死叉五日均线
     * 5表示hei_kin_ashi处于上涨阶段，6表示hei_kin_ashi处于下跌阶段
     * 7表示KD金叉，8表示KD死叉
     * 返回值为0表示现在并不处于这种状态
     *
     * @param type
     * @param date
     * @return
     */
    @Override
    public Integer findGoldCrossDeadCrossContinueDateCount(Integer type, String date) {
        logger.info("查询各种算法状态已经持续了多少天");

        final List<Integer> numberList = new ArrayList<>();
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.doWork(new FIndGoldCrossDeadCrossDateCountWorkImpl(type, date, numberList) {
        });
        session.getTransaction().commit();
        session.close();

        return numberList.get(0);
    }

    /**
     * 根据日期，计算表MDL_G_C_D_C_ANALYSIS的数据
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public void writeModelGoldCrossDeadCrossAnalysisByDate(String beginDate, String endDate) {
        logger.info("开始根据日期【" + beginDate + "】和【" + endDate + "】，计算表MDL_G_C_D_C_ANALYSIS的数据");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("{call PKG_MODEL_G_C_D_C_ANALYSIS.write_g_c_d_c_a_by_date(?, ?)}");
        query.setParameter(0, beginDate);
        query.setParameter(1, endDate);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 根据date查找ModelGoldCrossDeadCrossAnalysis对象
     * @param date
     * @return
     */
    @Override
    public ModelGoldCrossDeadCrossAnalysis findByDate(String date) {
        logger.info("根据date【" + date + "】查找ModelGoldCrossDeadCrossAnalysis对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Query query = session.createSQLQuery("select * from MDL_G_C_D_C_ANALYSIS t where t.date_=to_date(?, 'yyyy-mm-dd')").addEntity(ModelGoldCrossDeadCrossAnalysis.class);;
        query.setParameter(0, date);
        ModelGoldCrossDeadCrossAnalysis modelGoldCrossDeadCrossAnalysis = (ModelGoldCrossDeadCrossAnalysis) query.uniqueResult();
        session.getTransaction().commit();
        session.close();

        return modelGoldCrossDeadCrossAnalysis;
    }
}
