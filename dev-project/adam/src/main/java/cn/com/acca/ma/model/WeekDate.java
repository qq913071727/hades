package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 一周日期表
 */
@Data
@Entity
@Table(name = "WEEK_DATE")
public class WeekDate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    /**
     * 一周的开始日期
     */
    @Column(name = "begin_date")
    private Date beginDate;

    /**
     * 一周的结束日期
     */
    @Column(name = "end_date")
    private Date endDate;
}
