package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.Board2IndexDao;
import cn.com.acca.ma.dao.Board2IndexDetailDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.Board2Index;
import cn.com.acca.ma.model.Board2IndexDetail;

import java.util.List;

public class Board2IndexDetailDaoImpl extends BaseDaoImpl<Board2IndexDetailDaoImpl> implements Board2IndexDetailDao {

    public Board2IndexDetailDaoImpl() {
        super();
    }

    /**
     * 保存Board2IndexDetail对象
     *
     * @param board2IndexDetail
     */
    @Override
    public void saveBoard2IndexDetail(Board2IndexDetail board2IndexDetail) {
        logger.info("保存Board2IndexDetail对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.save(board2IndexDetail);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * 保存Board2IndexDetail对象列表
     *
     * @param board2IndexDetailList
     */
    @Override
    public void saveBoard2IndexDetailList(List<Board2IndexDetail> board2IndexDetailList) {
        logger.info("保存Board2IndexDetail对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        for (Board2IndexDetail board2IndexDetail : board2IndexDetailList) {
            session.save(board2IndexDetail);
        }
        session.getTransaction().commit();
        session.close();
    }
}
