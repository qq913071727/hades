package cn.com.acca.ma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="FOREIGN_EXCHANGE")
public class ForeignExchange {
	@Id
	@Column(name="NAME")
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
