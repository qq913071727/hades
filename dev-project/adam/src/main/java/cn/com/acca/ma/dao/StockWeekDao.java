package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.StockWeek;
import cn.com.acca.ma.pojo.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface StockWeekDao extends BaseDao {
	/*************************************************** 更新某一周的数据 ****************************************************/
	void writeStockWeekByDate(String beginDate,String endDate);
	void writeStockWeekMAByDate(String beginDate, String endDate);
	void writeStockWeekKDByDate(String beginDate,String endDate);
	void writeStockWeekUpDownByDate(String allStockWeekUpdownBeginDate, String allStockWeekUpdownEndDate);
	
	/**
	 * 更新所有股票某一周的MACD数据
	 * @param stockWeekBeginDate
	 * @param stockWeekEndDate
	 */
	void writeStockWeekMACDByDate(String stockWeekBeginDate,String stockWeekEndDate);

	/**
	 * 更新所有股票某一周的CHANGE_RANGE_EX_RIGHT字段
	 * @param stockWeekBeginDate
	 * @param stockWeekEndDate
	 */
	void writeStockWeekChangeRangeExRightByDate(String stockWeekBeginDate,String stockWeekEndDate);

	/**
	 * 更新所有股票某一周的与布林带相关的字段
	 * @param stockWeekBeginDate
	 * @param stockWeekEndDate
	 */
	void writeStockWeekBollByDate(String stockWeekBeginDate,String stockWeekEndDate);

	/**
	 * 更新所有股票某一周的与hei_kin_ashi的字段
	 * @param stockWeekBeginDate
	 * @param stockWeekEndDate
	 */
	void writeStockWeekHeiKinAshiByDate(String stockWeekBeginDate,String stockWeekEndDate);
	
	/**************************************** 根据一周数据更新KD图表 *************************************************/
	List<String> getDateByConditionForKD(boolean multithreading, String beginDate,String endDate);
	double obtainWeekK(String weekBegin,String weekEnd);
	double obtainWeekD(String weekBegin,String weekEnd);

	/**
	 * 为创建周线级别KD图表，获取周线级别每一周的所有股票的平均收盘价
	 * @param multithreading
	 * @param weekBegin
	 * @param weekEnd
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List getAverageCloseWeek(boolean multithreading, String weekBegin,String weekEnd);
	
	/**
	 * 之前本打算使用的算法，现在已经弃用
	 * @param weekBegin
	 * @param weekEnd
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List getOldAverageCloseWeek(String weekBegin,String weekEnd);

	/**
	 * 根据开始日期和结束日期，按周分组，返回平均收盘价、平均上轨值、平均中轨值和平均下轨值
	 * @param weekBeginDate
	 * @param weekEndDate
	 * @return
	 */
	List<StockWeekAverageBollPojo> findAvgClosePriceAndAvgUPAndAvgMBAndAvgDNGroupByYearAndWeek(String weekBeginDate,String weekEndDate);

	/**
	 * 根据开始日期和结束日期，按周分组，返回收盘价突破布林带上轨和下轨的股票的百分比
	 * @param weekBeginDate
	 * @param weekEndDate
	 * @return
	 */
	List<StockWeekClosePriceUpDnBollPercentagePojo> findClosePriceUpDonBollPercentage(String weekBeginDate, String weekEndDate);

	/**
	 * 查找beginDate和endDate之间的，所有MA和收盘价
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<StockWeekAverageClosePriceAndAllAverageMa> findAllAverageMaAndAverageClosePriceByBeginDateAndEndDate(String beginDate, String endDate);

	/**
	 * 根据开始时间和结束时间，查找ha_week_close_price的平均值，并按照end_date升序排列
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<StockWeek> findAverageHaClosePriceAndAverageHaWeekClosePriceByBeginDateAndEndDateOrderByEndDateAsc(String beginDate, String endDate);

	/******************************************** 更新所有的数据 ********************************************************/
	void writeStockWeek();
	void writeStockWeekMA();
	void writeStockWeekKD();
	void writeStockWeekUpDown();
	
	/**
	 * 更新STOCK_WEEK表中与MACD相关的所有字段：EMA12，EMA26，D,K
	 */
	void writeStockWeekMACD();

	/**
	 * 更新STOCK_WEEK表中的CHANGE_RANGE_EX_RIGHT字段
	 */
	void writeStockWeekChangeRangeExRight();

	/**
	 * 更新STOCK_WEEK表中的和布林带相关的字段：mb、up、dn
	 */
	void writeStockWeekBoll();

	/**
	 * 更新STOCK_WEEK表中的hei_kin_ashi相关的字段
	 */
	void writeStockWeekHeiKinAshi();
	
	/************************************** 增量备份STOCK_WEEK表 *****************************************************/
	/**
     * 根据开始时间和结束时间，在STOCK_WEEK表中查找所有STOCK_WEEK_END_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
	List<Date> getDateByCondition(String beginDate, String endDate);
	
	/**
	 * 根据结束时间endDate，在STOCK_WEEKEND表中查找所有STOCK_WEEKEND对象
	 * @param endDate
	 * @return
	 */
	List<StockWeek> getStockWeeksByDate(String endDate);
	
	/**
	 * 插入StockWeek对象
	 * @param stockWeek
	 */
	void saveStockWeek(StockWeek stockWeek);

	/**
	 * 查询周线级别，收盘价突破布林带上轨和下轨的股票的数量
	 * @param beginDate
	 * @param endDate
	 * @param closePricePercentageFromUpDn
	 * @return
	 */
	StockWeekBollUpDnCount findStockWeekBollUpDnCount(Date beginDate, Date endDate, Double closePricePercentageFromUpDn);

	/**
	 * 查找日期date之前的两周记录的平均K和D，并按照begin_date升序排列
	 * @param date
	 * @return
	 */
	List<StockWeekAverageKD> findLastTwoStockWeekAverageKDByDateOrderByBeginDateAsc(String date);

	/**
	 * 查找包括日期date在内的之前的两周记录的平均K和D，并按照begin_date升序排列
	 * @param date
	 * @return
	 */
	List<StockWeekAverageKD> findThisTwoStockWeekAverageKDByDateOrderByBeginDateAsc(String date);

	/**
	 * 查找日期date之前的两周记录的平均hei_kin_ashi收盘价，并按照begin_date升序排列
	 * @param date
	 * @return
	 */
	List<StockWeekAverageHeiKinAshiClosePrice> findLastTwoStockWeekAverageHeiKinAshiClosePriceByDateOrderByBeginDateAsc(String date);

	/**
	 * 查找包括日期date在内的之前的两周记录的平均hei_kin_ashi收盘价，并按照begin_date升序排列
	 * @param date
	 * @return
	 */
	List<StockWeekAverageHeiKinAshiClosePrice> findThisTwoStockWeekAverageHeiKinAshiClosePriceByDateOrderByBeginDateAsc(String date);

	/**
	 * 查找日期date之前的两周记录的平均close_price和MA5，并按照begin_date升序排列
	 * @param date
	 * @return
	 */
	List<StockWeekAverageClosePriceAndMa5> findLastTwoStockWeekAverageClosePriceAndMA5ByDateOrderByBeginDateAsc(String date);

	/**
	 * 查找包括日期date在内的两周记录的平均close_price和MA5，并按照begin_date升序排列
	 * @param date
	 * @return
	 */
	List<StockWeekAverageClosePriceAndMa5> findThisTwoStockWeekAverageClosePriceAndMA5ByDateOrderByBeginDateAsc(String date);

	/**
	 * 根据股票代码和时间，查找这只股票在这一周的交易记录
	 * @param code
	 * @param date
	 * @return
	 */
	StockWeek findByCodeAndBeginDateAndEndDate(String code, String date);

	/**
	 * 根据开始时间和结束时间，删除记录
	 * @param beginDate
	 * @param endDate
	 */
	void deleteByBeginDateAndEndDate(Date beginDate, Date endDate);
}
