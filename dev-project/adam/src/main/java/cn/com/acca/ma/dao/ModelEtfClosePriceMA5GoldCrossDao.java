package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockIndexClosePriceMA5GoldCross;

import java.util.List;

public interface ModelEtfClosePriceMA5GoldCrossDao extends BaseDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_ETF_CLOSE_PRICE_MA5_G_C中插入数据
     * @param validateMA120NotDecreasing
     * @param validateMA250NotDecreasing
     * @param defaultBeginDate
     * @param endDate
     * @param type
     */
    void writeModelEtfClosePriceMA5GoldCross(Integer validateMA120NotDecreasing, Integer validateMA250NotDecreasing, String defaultBeginDate,
                                             String endDate, Integer type);

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和etf代码，从表MDL_ETF_CLOSE_PRICE_MA5_G_C中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
//    List<ModelStockIndexClosePriceMA5GoldCross> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode);
}
