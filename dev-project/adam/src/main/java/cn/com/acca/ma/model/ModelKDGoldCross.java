package cn.com.acca.ma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * KD金叉时的交易
 */
@Entity
@Table(name = "MDL_KD_GOLD_CROSS")
public class ModelKDGoldCross implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    /**
     * 股票代码
     */
    @Column(name = "STOCK_CODE")
    private String stockCode;

    /**
     * 卖出日期
     */
    @Column(name="SELL_DATE")
    private Date sellDate;

    /**
     * 卖出价格
     */
    @Column(name="SELL_PRICE")
    private BigDecimal sellPrice;

    /**
     * 卖出时的K
     */
    @Column(name = "SELL_K")
    private BigDecimal sellK;

    /**
     * 卖出时的D
     */
    @Column(name = "SELL_D")
    private BigDecimal sellD;

    /**
     * 买入日期
     */
    @Column(name="BUY_DATE")
    private Date buyDate;

    /**
     * 买入价格
     */
    @Column(name="BUY_PRICE")
    private BigDecimal buyPrice;

    /**
     * 买入时的K
     */
    @Column(name = "BUY_K")
    private BigDecimal buyK;

    /**
     * 买入时的D
     */
    @Column(name = "BUY_D")
    private BigDecimal buyD;

    /**
     * 累计收益。单位：元
     */
    @Column(name = "ACCUMULATIVE_PROFIT_LOSS")
    private BigDecimal accumulativeProfitLoss;

    /**
     * 本次交易盈亏百分比
     */
    @Column(name = "PROFIT_LOSS")
    private BigDecimal profitLoss;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getSellK() {
        return sellK;
    }

    public void setSellK(BigDecimal sellK) {
        this.sellK = sellK;
    }

    public BigDecimal getSellD() {
        return sellD;
    }

    public void setSellD(BigDecimal sellD) {
        this.sellD = sellD;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getBuyK() {
        return buyK;
    }

    public void setBuyK(BigDecimal buyK) {
        this.buyK = buyK;
    }

    public BigDecimal getBuyD() {
        return buyD;
    }

    public void setBuyD(BigDecimal buyD) {
        this.buyD = buyD;
    }

    public BigDecimal getAccumulativeProfitLoss() {
        return accumulativeProfitLoss;
    }

    public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
        this.accumulativeProfitLoss = accumulativeProfitLoss;
    }

    public BigDecimal getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(BigDecimal profitLoss) {
        this.profitLoss = profitLoss;
    }
}
