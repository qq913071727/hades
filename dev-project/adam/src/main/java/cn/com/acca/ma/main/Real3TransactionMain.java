package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Real3TransactionMain extends AbstractMain {

    private static Logger logger = LogManager.getLogger(Real3TransactionMain.class);

    public static void main(String[] args) {
        logger.info("Real3TransactionMain程序开始运行......");

        /************************ 设置代理服务器的IP和port ********************************/
        setProxyProperties();

        /****************************** 收集数据库统计信息 ********************************/
//		gatherDatabaseStatistics();

        /************************** 预先计算股票的买卖条件 ********************************/
        preCalculateTransactionCondition_real3();

        /****************************** 发送邮件 ****************************************/
//        sendEMail();

        /************* 定时地判断买卖条件，给出交易建议，并保存在文件中，发送邮件 *****************/
        realTimeJudgeConditionAndGiveSuggestionAndSendEMail_real3();

        logger.info("RealTransactionMain程序执行完毕。");
    }
}
