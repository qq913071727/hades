package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelKDDeadCross;
import cn.com.acca.ma.model.ModelKDGoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelKDDeadCrossDao extends BaseDao {

    /**
     * 使用周线级别KD金叉模型算法，向表MDL_KD_DEAD_CROSS插入数据
     */
    void writeModelKDDeadCross();

    /**
     * 根据日期endDate，使用KD死叉模型算法，向表MDL_KD_DEAD_CROSS插入数据
     * @param endDate
     */
    void writeModelKDDeadCrossIncr(String endDate);

    /**
     * 获取一段时间内，最近dateNumber天，kd死叉的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    List<SuccessRateOrPercentagePojo> getKdDeadCrossSuccessRate(boolean multithreading, String beginDate, String endDate, Integer dateNumber);

    /**
     * 获取一段时间内，kd死叉的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<SuccessRateOrPercentagePojo> getKdDeadCrossPercentage(boolean multithreading, String beginDate, String endDate);

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_KD_DEAD_CROSS中获取sell_date字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_KD_DEAD_CROSS中获取date日的所有ModelKDDeadCross对象
     * @param date
     * @return
     */
    List<ModelKDDeadCross> getModelKDDeadCrossByDate(String date);

    /**
     * 向表MDL_KD_DEAD_CROSS中插入ModelKDDeadCross对象
     * @param modelKDDeadCross
     */
    void save(ModelKDDeadCross modelKDDeadCross);
}
