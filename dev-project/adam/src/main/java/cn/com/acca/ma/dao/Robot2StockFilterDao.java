package cn.com.acca.ma.dao;

public interface Robot2StockFilterDao extends BaseDao {

    /**
     * 清空robot2_stock_filter表
     */
    void truncateTableRobotStockFilter();

    /**
     * 只保留这个价格区间以内的股票
     *
     * @param date
     * @param closePriceStart
     * @param closePriceEnd
     */
    void filterByLessThanClosePrice(String date, Double closePriceStart, Double closePriceEnd);

    /**
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=阈值
     *
     * @param beginDate
     * @param endDate
     * @param rate
     */
    void filterByClosePriceGreatThenLessThenMA250(String beginDate, String endDate, Double rate);

    /**
     * 根据date，从stock_transaction_data表中，向robot2_stock_filter表中插入股票代码
     * 用于做多
     *
     * @param date
     */
    void insertBullStockCodeFromStockTransactionDataByDate(String date);

    /**
     * 根据date，从stock_transaction_data表中，向robot2_stock_filter表中插入股票代码
     * 用于做空
     *
     * @param date
     */
    void insertShortStockCodeFromStockTransactionDataByDate(String date);

    /**
     * 只插入收盘价跌破周线级别布林带上轨的股票记录
     * 用于做多
     *
     * @param beginDate
     * @param endDate
     */
    void insertStockWeekBollDnByDate(String beginDate, String endDate);

    /**
     * 只插入收盘价突破周线级别布林带上轨的股票记录
     * 用于做空
     *
     * @param beginDate
     * @param endDate
     */
    void insertStockWeekBollUpByDate(String beginDate, String endDate);

    /**
     * 过滤条件：MACD金叉
     * 用于做多
     *
     * @param date
     */
    void filterByMACDGoldCross(String date);

    /**
     * 过滤条件：MACD死叉
     * 用于做空
     *
     * @param date
     */
    void filterByMACDDeadCross(String date);

    /**
     * 过滤条件：删除除过权的股票
     *
     * @param beginDate
     * @param endDate
     */
    void filterByXr(String beginDate, String endDate);

    /**
     * 过滤条件：周线级别KD金叉
     *
     * @param date
     */
    void filterByWeekKDGoldCross(String date);

    /**
     * 过滤条件：当前收盘价与某段时间最高价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做多
     *
     * @param beginDate
     * @param date
     * @param percentage
     */
    void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(String beginDate, String date, Integer percentage);

    /**
     * 过滤条件：当前收盘价与某段时间最低价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做空
     *
     * @param beginDate
     * @param date
     * @param percentage
     */
    void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(String beginDate, String date, Integer percentage);

    /**
     * 过滤条件：MA不单调递减，过滤股票
     * 用于做多
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    void filterByMANotDecreasing(String date, Integer maLevel, String dateNumber);

    /**
     * 过滤条件：MA不单调递增，过滤股票
     * 用于做空
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    void filterByMANotIncreasing(String date, Integer maLevel, String dateNumber);

    /**
     * 过滤条件：收盘价金叉五日均线
     * 用于做多
     *
     * @param date
     */
    void filterByClosePriceGoldCrossMA5(String date);

    /**
     * 过滤条件：收盘价死叉五日均线
     * 用于做空
     *
     * @param date
     */
    void filterByClosePriceDeadCrossMA5(String date);

    /**
     * 过滤条件：平均K线从下跌趋势变为上涨趋势
     * 用于做多
     *
     * @param date
     */
    void filterByHeiKinAshiUpDown(String date);

    /**
     * 过滤条件：平均K线从上涨趋势变为下跌趋势
     * 用于做空
     *
     * @param date
     */
    void filterByHeiKinAshiDownUp(String date);

    /**
     * 过滤条件：KD金叉
     * 用于做多
     *
     * @param date
     */
    void filterByKDGoldCross(String date);

    /**
     * 过滤条件：KD死叉
     * 用于做空
     *
     * @param date
     */
    void filterByKDDeadCross(String date);

    /**
     * 过滤条件：保留所有金叉
     * 用于做多
     *
     * @param date
     */
    void filterByAllGoldCross(String date);

    /**
     * 过滤条件：保留所有死叉
     * 用于做空
     *
     * @param date
     */
    void filterByAllDeadCross(String date);

    /**
     * 过滤条件：这次金叉或死叉之前的那次死叉或金叉的持续天数如果小于transactionNumber，则这条记录被过滤掉
     *
     * @param date
     * @param filterMethod
     * @param macdTransactionNumber
     * @param closePriceMa5TransactionNumber
     * @param heiKinAshiTransactionNumber
     * @param kdTransactionNumber
     */
    void filterByTransactionNumberBeforeGoldCrossDeadCross(String date, Integer filterMethod, Integer macdTransactionNumber, Integer closePriceMa5TransactionNumber, Integer heiKinAshiTransactionNumber, Integer kdTransactionNumber);

    /**
     * 返回robot2_stock_filter表的记录数
     *
     * @return
     */
    Integer count();
}
