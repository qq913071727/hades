package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class AllMain extends AbstractMain {

    private static Logger logger = LogManager.getLogger(AllMain.class);

    public static void main(String[] args) {
        logger.info("AllMain程序开始运行......");

        StockMain.main(null);
//        RealTransactionMain.main(null);

//        RobotMain.main(null);
//        Robot2Main.main(null);
//        Robot3Main.main(null);
//        Robot4Main.main(null);
//        Robot5Main.main(null);
//        Robot6Main.main(null);
//        LaboratoryMain.main(null);

        logger.info("AllMain程序执行完毕。");
    }
}
