package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Robot6Main extends AbstractMain {

    private static Logger logger = LogManager.getLogger(Robot6Main.class);

    public static void main(String[] args) {
        logger.info("Robot6Main程序开始运行......");

        /******************************* 设置代理服务器的IP和port *******************************/
        setProxyProperties();

        /******************************* 收集数据库统计信息 *************************************/
//		gatherDatabaseStatistics();

        /************* 执行某段时间内的股票买卖交易（etf四种算法最近12年中平均收益率最高的） ************/
        doBuyAndSellByBeginDateAndEndDate_etf12YearHighestAverageProfitRate();

        /******************************* 将测试数据导入到历史数据表中 *****************************/
//        importIntoHistoryDataTableFromRobot6();

        /***************************************** 使操作系统进入睡眠状态 *****************************************/
//        makeOperationSystemSleep();

        logger.info("Robot6Main程序执行完毕。");
    }
}
