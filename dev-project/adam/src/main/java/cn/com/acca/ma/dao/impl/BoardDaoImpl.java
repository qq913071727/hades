package cn.com.acca.ma.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import cn.com.acca.ma.dao.BoardDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.Board;
import org.hibernate.Session;

public class BoardDaoImpl extends BaseDaoImpl<BoardDaoImpl> implements BoardDao {

	public BoardDaoImpl() {
		super();
	}

	/**
	 * 通过board_id查找board_name
	 * @param id： board_id
	 * @return board_name
	 */
	public String findBoardNameById(boolean multithreading, BigDecimal id) {
		logger.info("find board name by id begin");

		Session newSession = null;
		Board board;
		if (multithreading) {
			newSession = HibernateUtil.newSession();
			newSession.beginTransaction();
			board = (Board) newSession.load(Board.class, id);
		} else {
			session = HibernateUtil.currentSession();
			session.beginTransaction();
			board = (Board) session.load(Board.class, id);
		}

		String boardName = board.getName();

		if (multithreading) {
			newSession.getTransaction().commit();
			HibernateUtil.closeNewSessionFactory(newSession);
		} else {
			session.getTransaction().commit();
			session.close();
		}

		logger.info("find board name by id finish");
		return boardName;
	}

	/**
	 * 获取BOARD表中ID列的所有值
	 * @param multithreading
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<BigDecimal> getAllBoardId(boolean multithreading) {
		logger.info("开始获取BOARD表中ID列的所有值");

		Session newSession = null;
		List<BigDecimal> allBoardId;
		if (multithreading) {
			newSession= HibernateUtil.newSession();
			newSession.beginTransaction();
			allBoardId = newSession.createQuery("select t.id from Board t").list();
			newSession.getTransaction().commit();
			HibernateUtil.closeNewSessionFactory(newSession);
		} else {
			session= HibernateUtil.currentSession();
			session.beginTransaction();
			allBoardId = session.createQuery("select t.id from Board t").list();
			session.getTransaction().commit();
			session.close();
		}

		logger.info("获取BOARD表中ID列的所有值完成");
		return allBoardId;
	}

	/**
	 * 获取所有的板块
	 * @param multithreading
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Board> getAllBoard(boolean multithreading){
		logger.info("get all board begin");

		Session newSession = null;
		List<Board> boardList;
		if (multithreading) {
			newSession = HibernateUtil.newSession();
			newSession.beginTransaction();
			boardList = newSession.createQuery("from Board t").list();
			newSession.getTransaction().commit();
			HibernateUtil.closeNewSessionFactory(newSession);
		} else {
			session = HibernateUtil.currentSession();
			session.beginTransaction();
			boardList = session.createQuery("from Board t").list();
			session.getTransaction().commit();
			session.close();
		}

		logger.info("get all board finish");
		return boardList;
	}
	
	/**
	 * 保存Board对象
	 * @param board
	 */
	public void saveBoard(Board board){
		logger.info("保存Board对象");
		
		session= HibernateUtil.currentSession();
		session.beginTransaction();
		session.save(board);
		session.getTransaction().commit();
		session.close();
	}
}
