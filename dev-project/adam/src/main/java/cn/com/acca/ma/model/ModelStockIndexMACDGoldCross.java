package cn.com.acca.ma.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "MDL_STOCK_INDEX_MACD_G_C")
public class ModelStockIndexMACDGoldCross implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "BUY_DATE")
	private Date buyDate;

	@Column(name = "SELL_DATE")
	private Date sellDate;

	@Column(name = "STOCK_INDEX_CODE")
	private String stockIndexCode;

	@Column(name = "BUY_PRICE")
	private BigDecimal buyPrice;

	@Column(name = "SELL_PRICE")
	private BigDecimal sellPrice;

	@Column(name = "PROFIT_LOSS")
	private BigDecimal profitLoss;

	@Column(name = "ACCUMULATIVE_PROFIT_LOSS")
	private BigDecimal accumulativeProfitLoss;

	@Column(name = "BUY_DIF")
	private BigDecimal buyDif;

	@Column(name = "BUY_DEA")
	private BigDecimal buyDea;

	@Column(name = "SELL_DIF")
	private BigDecimal sellDif;

	@Column(name = "SELL_DEA")
	private BigDecimal sellDea;

	/**
	 * 类型。1表示所有交易记录，时间是1997年1月1日至今。2表示所有交易记录，120、250日均线单调不减，时间是1997年1月1日至今。3表示所有交易记录，120日均线单调不减，时间是1997年1月1日至今。4表示所有交易记录，250日均线单调不减，时间是1997年1月1日至今。5表示所有交易记录，时间是2011年1月1日至今。6表示所有交易记录，120、250日均线单调不减，时间是2011年1月1日至今。7表示所有交易记录，120日均线单调不减，时间是2011年1月1日至今。8表示所有交易记录，250日均线单调不减，时间是2011年1月1日至今。
	 */
	@Column(name = "TYPE_")
	private Integer type;

}
