package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelStockMonthAnalysis;
import cn.com.acca.ma.model.ModelStockWeekAnalysis;

import java.util.Date;
import java.util.List;

public interface ModelStockWeekAnalysisDao extends BaseDao {

    /**
     * 计算model_stock_week_analysis表的全部数据
     */
    void writeModelStockWeekAnalysis();

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_STOCK_WEEK_ANALYSIS中获取END_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_STOCK_WEEK_ANALYSIS中获取date日的所有ModelStockWeekAnalysis对象
     * @param date
     * @return
     */
    List<ModelStockWeekAnalysis> getModelStockWeekAnalysisByDate(String date);

    /**
     * 计算model_stock_week_analysis表的某一日的数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     */
    void writeModelStockWeekAnalysisByDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 根据开始日期和结束日期，查找中间的日期，并去重
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
//    List<String> findDistinctDateBetweenBeginDateAndEndDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 向表MDL_STOCK_WEEK_ANALYSIS中插入ModelStockWeekAnalysis对象
     * @param modelStockWeekAnalysis
     */
    void save(ModelStockWeekAnalysis modelStockWeekAnalysis);

    /**
     * 根据开始时间和结束时间，计算每个交易周、所有股票的平均收盘价和平均方差，并按时间升序排列
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Object> findAverageClosePriceAndAverageStandardDeviationByBeginDateAndEndDateOrderByDate(String beginDate, String endDate);
}
