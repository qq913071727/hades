package cn.com.acca.ma.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MDL_TOP_STOCK_DETAIL")
public class ModelTopStockDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
    private Integer id;

    @Column(name="DATE_")
    private Date date;

    @Column(name="CODE_")
    private String code;

    @Column(name="NAME_")
    private String name;

    @Column(name="BOARD_NAME")
    private String boardName;

    @Column(name="UP_DOWN_PERCENTAGE_21")
    private Double upDownPercentage21;

    @Column(name="UP_DOWN_PERCENTAGE_34")
    private Double upDownPercentage34;

    @Column(name="UP_DOWN_PERCENTAGE_55")
    private Double upDownPercentage55;

    @Column(name="UP_DOWN_PERCENTAGE_89")
    private Double upDownPercentage89;

    @Column(name="UP_DOWN_PERCENTAGE_144")
    private Double upDownPercentage144;

    @Column(name="UP_DOWN_PERCENTAGE_233")
    private Double upDownPercentage233;

    @Column(name="UP_DOWN_PERCENTAGE_377")
    private Double upDownPercentage377;

    @Column(name="UP_DOWN_PERCENTAGE_610")
    private Double upDownPercentage610;

    @Column(name="PERCENTAGE_21_MA5")
    private Double percentage21Ma5;

    @Column(name="PERCENTAGE_34_MA5")
    private Double percentage34Ma5;

    @Column(name="PERCENTAGE_55_MA5")
    private Double percentage55Ma5;

    @Column(name="PERCENTAGE_89_MA5")
    private Double percentage89Ma5;

    @Column(name="PERCENTAGE_144_MA5")
    private Double percentage144Ma5;

    @Column(name="PERCENTAGE_233_MA5")
    private Double percentage233Ma5;

    @Column(name="PERCENTAGE_377_MA5")
    private Double percentage377Ma5;

    @Column(name="PERCENTAGE_610_MA5")
    private Double percentage610Ma5;

    @Column(name="PERCENTAGE_21_MA10")
    private Double percentage21Ma10;

    @Column(name="PERCENTAGE_34_MA10")
    private Double percentage34Ma10;

    @Column(name="PERCENTAGE_55_MA10")
    private Double percentage55Ma10;

    @Column(name="PERCENTAGE_89_MA10")
    private Double percentage89Ma10;

    @Column(name="PERCENTAGE_144_MA10")
    private Double percentage144Ma10;

    @Column(name="PERCENTAGE_233_MA10")
    private Double percentage233Ma10;

    @Column(name="PERCENTAGE_377_MA10")
    private Double percentage377Ma10;

    @Column(name="PERCENTAGE_610_MA10")
    private Double percentage610Ma10;

    @Column(name="PERCENTAGE_21_MA20")
    private Double percentage21Ma20;

    @Column(name="PERCENTAGE_34_MA20")
    private Double percentage34Ma20;

    @Column(name="PERCENTAGE_55_MA20")
    private Double percentage55Ma20;

    @Column(name="PERCENTAGE_89_MA20")
    private Double percentage89Ma20;

    @Column(name="PERCENTAGE_144_MA20")
    private Double percentage144Ma20;

    @Column(name="PERCENTAGE_233_MA20")
    private Double percentage233Ma20;

    @Column(name="PERCENTAGE_377_MA20")
    private Double percentage377Ma20;

    @Column(name="PERCENTAGE_610_MA20")
    private Double percentage610Ma20;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public Double getUpDownPercentage21() {
        return upDownPercentage21;
    }

    public void setUpDownPercentage21(Double upDownPercentage21) {
        this.upDownPercentage21 = upDownPercentage21;
    }

    public Double getUpDownPercentage34() {
        return upDownPercentage34;
    }

    public void setUpDownPercentage34(Double upDownPercentage34) {
        this.upDownPercentage34 = upDownPercentage34;
    }

    public Double getUpDownPercentage55() {
        return upDownPercentage55;
    }

    public void setUpDownPercentage55(Double upDownPercentage55) {
        this.upDownPercentage55 = upDownPercentage55;
    }

    public Double getUpDownPercentage89() {
        return upDownPercentage89;
    }

    public void setUpDownPercentage89(Double upDownPercentage89) {
        this.upDownPercentage89 = upDownPercentage89;
    }

    public Double getUpDownPercentage144() {
        return upDownPercentage144;
    }

    public void setUpDownPercentage144(Double upDownPercentage144) {
        this.upDownPercentage144 = upDownPercentage144;
    }

    public Double getUpDownPercentage233() {
        return upDownPercentage233;
    }

    public void setUpDownPercentage233(Double upDownPercentage233) {
        this.upDownPercentage233 = upDownPercentage233;
    }

    public Double getUpDownPercentage377() {
        return upDownPercentage377;
    }

    public void setUpDownPercentage377(Double upDownPercentage377) {
        this.upDownPercentage377 = upDownPercentage377;
    }

    public Double getUpDownPercentage610() {
        return upDownPercentage610;
    }

    public void setUpDownPercentage610(Double upDownPercentage610) {
        this.upDownPercentage610 = upDownPercentage610;
    }

    public Double getPercentage21Ma5() {
        return percentage21Ma5;
    }

    public void setPercentage21Ma5(Double percentage21Ma5) {
        this.percentage21Ma5 = percentage21Ma5;
    }

    public Double getPercentage34Ma5() {
        return percentage34Ma5;
    }

    public void setPercentage34Ma5(Double percentage34Ma5) {
        this.percentage34Ma5 = percentage34Ma5;
    }

    public Double getPercentage55Ma5() {
        return percentage55Ma5;
    }

    public void setPercentage55Ma5(Double percentage55Ma5) {
        this.percentage55Ma5 = percentage55Ma5;
    }

    public Double getPercentage89Ma5() {
        return percentage89Ma5;
    }

    public void setPercentage89Ma5(Double percentage89Ma5) {
        this.percentage89Ma5 = percentage89Ma5;
    }

    public Double getPercentage144Ma5() {
        return percentage144Ma5;
    }

    public void setPercentage144Ma5(Double percentage144Ma5) {
        this.percentage144Ma5 = percentage144Ma5;
    }

    public Double getPercentage233Ma5() {
        return percentage233Ma5;
    }

    public void setPercentage233Ma5(Double percentage233Ma5) {
        this.percentage233Ma5 = percentage233Ma5;
    }

    public Double getPercentage377Ma5() {
        return percentage377Ma5;
    }

    public void setPercentage377Ma5(Double percentage377Ma5) {
        this.percentage377Ma5 = percentage377Ma5;
    }

    public Double getPercentage610Ma5() {
        return percentage610Ma5;
    }

    public void setPercentage610Ma5(Double percentage610Ma5) {
        this.percentage610Ma5 = percentage610Ma5;
    }

    public Double getPercentage21Ma10() {
        return percentage21Ma10;
    }

    public void setPercentage21Ma10(Double percentage21Ma10) {
        this.percentage21Ma10 = percentage21Ma10;
    }

    public Double getPercentage34Ma10() {
        return percentage34Ma10;
    }

    public void setPercentage34Ma10(Double percentage34Ma10) {
        this.percentage34Ma10 = percentage34Ma10;
    }

    public Double getPercentage55Ma10() {
        return percentage55Ma10;
    }

    public void setPercentage55Ma10(Double percentage55Ma10) {
        this.percentage55Ma10 = percentage55Ma10;
    }

    public Double getPercentage89Ma10() {
        return percentage89Ma10;
    }

    public void setPercentage89Ma10(Double percentage89Ma10) {
        this.percentage89Ma10 = percentage89Ma10;
    }

    public Double getPercentage144Ma10() {
        return percentage144Ma10;
    }

    public void setPercentage144Ma10(Double percentage144Ma10) {
        this.percentage144Ma10 = percentage144Ma10;
    }

    public Double getPercentage233Ma10() {
        return percentage233Ma10;
    }

    public void setPercentage233Ma10(Double percentage233Ma10) {
        this.percentage233Ma10 = percentage233Ma10;
    }

    public Double getPercentage377Ma10() {
        return percentage377Ma10;
    }

    public void setPercentage377Ma10(Double percentage377Ma10) {
        this.percentage377Ma10 = percentage377Ma10;
    }

    public Double getPercentage610Ma10() {
        return percentage610Ma10;
    }

    public void setPercentage610Ma10(Double percentage610Ma10) {
        this.percentage610Ma10 = percentage610Ma10;
    }

    public Double getPercentage21Ma20() {
        return percentage21Ma20;
    }

    public void setPercentage21Ma20(Double percentage21Ma20) {
        this.percentage21Ma20 = percentage21Ma20;
    }

    public Double getPercentage34Ma20() {
        return percentage34Ma20;
    }

    public void setPercentage34Ma20(Double percentage34Ma20) {
        this.percentage34Ma20 = percentage34Ma20;
    }

    public Double getPercentage55Ma20() {
        return percentage55Ma20;
    }

    public void setPercentage55Ma20(Double percentage55Ma20) {
        this.percentage55Ma20 = percentage55Ma20;
    }

    public Double getPercentage89Ma20() {
        return percentage89Ma20;
    }

    public void setPercentage89Ma20(Double percentage89Ma20) {
        this.percentage89Ma20 = percentage89Ma20;
    }

    public Double getPercentage144Ma20() {
        return percentage144Ma20;
    }

    public void setPercentage144Ma20(Double percentage144Ma20) {
        this.percentage144Ma20 = percentage144Ma20;
    }

    public Double getPercentage233Ma20() {
        return percentage233Ma20;
    }

    public void setPercentage233Ma20(Double percentage233Ma20) {
        this.percentage233Ma20 = percentage233Ma20;
    }

    public Double getPercentage377Ma20() {
        return percentage377Ma20;
    }

    public void setPercentage377Ma20(Double percentage377Ma20) {
        this.percentage377Ma20 = percentage377Ma20;
    }

    public Double getPercentage610Ma20() {
        return percentage610Ma20;
    }

    public void setPercentage610Ma20(Double percentage610Ma20) {
        this.percentage610Ma20 = percentage610Ma20;
    }
}
