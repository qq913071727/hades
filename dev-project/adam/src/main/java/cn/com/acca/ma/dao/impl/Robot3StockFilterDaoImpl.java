package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.constant.StockInfoMark;
import cn.com.acca.ma.dao.Robot3StockFilterDao;
import cn.com.acca.ma.dao.RobotStockFilterDao;
import cn.com.acca.ma.pojo.AverageClosePriceAndMaBullShortLineByBias;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Robot3StockFilterDaoImpl extends BaseDaoImpl<Robot3StockFilterDao> implements
        Robot3StockFilterDao {

    public Robot3StockFilterDaoImpl() {
        super();
    }

    /**
     * 判断当前交易日，判断股票的平均收盘价是否在牛熊线之上。true表示是，false表示否
     * @param date
     * @return
     */
    @Override
    public boolean isUpBullShortLine(String date) {
        logger.info("判断当前交易日[" + date + "]，判断股票的平均收盘价是否在牛熊线之上。true表示是，false表示否");

        String sql = "select to_char(t.date_, 'yyyy-mm-dd') date_, avg(t.close_price) averageClosePrice, avg(t.ma5) averageMa5, " +
                "avg(t.ma10) averageMa10, avg(t.ma20) averageMa20, avg(t.ma60) averageMa60, avg(t.ma120) averageMa120, avg(t.ma250) averageMa250, " +
                "avg(t.bias5) averageBias5, avg(t.bias10) averageBias10, avg(t.bias20) averageBias20, avg(t.bias60) averageBias60, " +
                "avg(t.bias120) averageBias120, avg(t.bias250) averageBias250 " +
                "from stock_transaction_data_all t " +
                "where t.date_ = to_date(:date_,'yyyy-mm-dd') group by t.date_";

        Map<String, Object> map = new HashMap();
        map.put("date_", date);

        List<AverageClosePriceAndMaBullShortLineByBias> averageClosePriceAndMaBullShortLineByBiasList = doSQLQueryInTransaction(sql, map, AverageClosePriceAndMaBullShortLineByBias.class);
        if (null != averageClosePriceAndMaBullShortLineByBiasList && averageClosePriceAndMaBullShortLineByBiasList.size() > 0){
            AverageClosePriceAndMaBullShortLineByBias averageClosePriceAndMaBullShortLineByBias = averageClosePriceAndMaBullShortLineByBiasList.get(0);
            double topLimit = 10;
            double bottomList = -15;
            double bullShortLine;
            BigDecimal averageClosePrice = averageClosePriceAndMaBullShortLineByBias.getAverageClosePrice();
            BigDecimal averageBias5 = averageClosePriceAndMaBullShortLineByBias.getAverageBias5();
            BigDecimal averageBias10 = averageClosePriceAndMaBullShortLineByBias.getAverageBias10();
            BigDecimal averageBias20 = averageClosePriceAndMaBullShortLineByBias.getAverageBias20();
            BigDecimal averageBias60 = averageClosePriceAndMaBullShortLineByBias.getAverageBias60();
            BigDecimal averageBias120 = averageClosePriceAndMaBullShortLineByBias.getAverageBias120();
            BigDecimal averageBias250 = averageClosePriceAndMaBullShortLineByBias.getAverageBias250();

            if (averageBias250.longValue() <= topLimit && averageBias250.longValue() >= bottomList) {
                bullShortLine = averageClosePriceAndMaBullShortLineByBias.getAverageMa250().doubleValue();
            } else if (averageBias120.longValue() <= topLimit && averageBias120.longValue() >= bottomList) {
                bullShortLine = averageClosePriceAndMaBullShortLineByBias.getAverageMa120().doubleValue();
            } else if (averageBias60.longValue() <= topLimit && averageBias60.longValue() >= bottomList) {
                bullShortLine = averageClosePriceAndMaBullShortLineByBias.getAverageMa60().doubleValue();
            } else if (averageBias20.longValue() <= topLimit && averageBias20.longValue() >= bottomList) {
                bullShortLine = averageClosePriceAndMaBullShortLineByBias.getAverageMa20().doubleValue();
            } else if (averageBias10.longValue() <= topLimit && averageBias10.longValue() >= bottomList) {
                bullShortLine = averageClosePriceAndMaBullShortLineByBias.getAverageMa10().doubleValue();
            } else if (averageBias5.longValue() <= topLimit && averageBias5.longValue() >= bottomList) {
                bullShortLine = averageClosePriceAndMaBullShortLineByBias.getAverageMa5().doubleValue();
            } else {
                bullShortLine = averageClosePrice.doubleValue();
            }

            if (averageClosePrice.doubleValue() >= bullShortLine) {
                logger.info("当前股票市场整体态势：牛市");
                return true;
            } else {
                logger.info("当前股票市场整体态势：熊市");
                return false;
            }
        } else {
            logger.info("当前股票市场整体态势无法判断，按照牛市处理");
            return true;
        }
    }

    /**
     * 清空robot3_stock_filter表
     */
    @Override
    public void truncateTableRobotStockFilter() {
        logger.info("清空robot3_stock_filter表");

        String sql = "truncate table robot3_stock_filter";

        doSQLInTransaction(sql);
    }

    /**
     * 根据date，从stock_transaction_data表中，向robot3_stock_filter表中插入股票代码
     * 用于做多
     * 注意：当日已经卖出的股票不能再买入
     *
     * @param date
     */
    @Override
    public void insertBullStockCodeFromStockTransactionDataByDate(String date) {
        logger.info("根据date【" + date + "】，从stock_transaction_data_all表中，向robot3_stock_filter表中插入股票代码。用于做多");

        String sql = "insert into robot3_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_transaction_data_all t "
                + "where t.date_ = to_date('" + date + "', 'yyyy-mm-dd') " +
                "and t.code_ not in(select t2.stock_code from robot3_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null) " +
                "or t2.sell_date=to_date('" + date + "','yyyy-mm-dd') " +
                "or t2.buy_date=to_date('" + date + "','yyyy-mm-dd'))";

        doSQLInTransaction(sql);
    }

    /**
     * 根据date，从stock_transaction_data表中，向robot3_stock_filter表中插入股票代码
     * 用于做空
     * 注意：当日已经还券的股票不能再融券
     *
     * @param date
     */
    @Override
    public void insertShortStockCodeFromStockTransactionDataByDate(String date) {
        logger.info("根据date【" + date + "】，从stock_transaction_data_all表中，向robot3_stock_filter表中插入股票代码。用于做空");

        String sql = "insert into robot3_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_transaction_data_all t "
                + "join stock_info si on si.code_=t.code_ and si.mark='R' "
                + "where t.date_ = to_date('" + date + "', 'yyyy-mm-dd') " +
                "and t.code_ not in(select t2.stock_code from robot3_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null) " +
                "or t2.sell_date=to_date('" + date + "','yyyy-mm-dd') " +
                "or t2.buy_date=to_date('" + date + "','yyyy-mm-dd'))";

        doSQLInTransaction(sql);
    }

    /**
     * 只插入收盘价跌破周线级别布林带下轨的股票记录
     * 用于做多
     *
     * @param beginDate
     * @param endDate
     */
    public void insertStockWeekBollDnByDate(String beginDate, String endDate) {
        logger.info("只插入收盘价跌破周线级别布林带下轨的股票记录");

        String sql = "insert into robot3_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_week t "
                + "where t.begin_date >= to_date('" + beginDate + "', 'yyyy-mm-dd') " +
                "and t.end_date <= to_date('" + endDate + "', 'yyyy-mm-dd') " +
                "and t.close_price <= t.dn_ " +
                "and t.code_ not in(select t2.stock_code from robot3_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null))";

        doSQLInTransaction(sql);
    }

    /**
     * 只插入收盘价突破周线级别布林带上轨的股票记录
     * 用于做空
     *
     * @param beginDate
     * @param endDate
     */
    public void insertStockWeekBollUpByDate(String beginDate, String endDate) {
        logger.info("只插入收盘价突破周线级别布林带上轨的股票记录");

        String sql = "insert into robot3_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_week t "
                + "join stock_info si on si.code_=t.code_ and si.mark='" + StockInfoMark.SECURITIES_LENDING + "' "
                + "where t.begin_date >= to_date('" + beginDate + "', 'yyyy-mm-dd') " +
                "and t.end_date <= to_date('" + endDate + "', 'yyyy-mm-dd') " +
                "and t.close_price >= t.up " +
                "and t.code_ not in(select t2.stock_code from robot3_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null))";

        doSQLInTransaction(sql);
    }

    /**
     * 不考虑做多还是做空，插入所有股票代码
     * @param date
     */
    public void insertAllStockBollByDate(String date) {
        logger.info("不考虑做多还是做空，插入所有股票代码");

        String sql = "insert into robot3_stock_filter(stock_code) "
                + "select distinct t.code_ "
                + "from stock_transaction_data_all t "
                + "where t.date_ = to_date('" + date + "', 'yyyy-mm-dd') "
                + "and t.code_ not in(select t2.stock_code from robot3_stock_transact_record t2 " +
                "where (t2.buy_date is null and t2.buy_price is null and t2.buy_amount is null) " +
                "or (t2.sell_date is null and t2.sell_price is null and t2.sell_amount is null))";

        doSQLInTransaction(sql);
    }

    /**
     * 只保留这个价格区间以内的股票
     *
     * @param date
     * @param closePriceStart
     * @param closePriceEnd
     */
    @Override
    public void filterByLessThanClosePrice(String date, Double closePriceStart, Double closePriceEnd) {
        logger.info("收盘价在【" + closePriceStart + "】和【" + closePriceEnd + "】价格区间以外的股票都被删除");

        String sql = "delete from robot3_stock_filter where stock_code not in("
                + "select t.code_ from stock_transaction_data_all t "
                + "where t.date_=to_date('" + date + "','yyyy-mm-dd') and t.close_price between " + closePriceStart + " and " + closePriceEnd + ")";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=阈值
     * 过滤条件：选择震荡行情：日线级别，某一段时间内，收盘价小于年线的记录数（数量更多）/收盘价大于年线的记录数（数量更少）<=阈值
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public void filterByClosePriceGreatThenLessThenMA250(String beginDate, String endDate, Double rate) {
        logger.info("过滤条件：只选择震荡行情：日线级别，某一段时间内【" + beginDate + "】和【" + endDate + "】，" +
                "收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=" + rate);

        String sql = "{call PKG_ROBOT3.filter_by_c_p_gt_lt_ma250('" + beginDate + "', '" + endDate + "', " + rate + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除除过权的股票
     *
     * @param beginDate
     * @param endDate
     */
    @Override
    public void filterByXr(String beginDate, String endDate) {
        logger.info("过滤条件：删除除过权的股票，时间从【" + beginDate + "】至【" + endDate + "】");

        String sql = "{call PKG_ROBOT3.filter_by_xr('" + beginDate + "', '" + endDate + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：周线级别KD金叉
     *
     * @param date
     */
    @Override
    public void filterByWeekKDGoldCross(String date) {
        logger.info("过滤条件：周线级别KD金叉");

        String sql = "{call PKG_ROBOT3.filter_by_week_kd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MACD金叉
     *
     * @param date
     */
    @Override
    public void filterByMACDGoldCross(String date) {
        logger.info("过滤条件：MACD金叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_macd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MACD金叉
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByMACDDeadCross(String date) {
        logger.info("过滤条件：MACD死叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_macd_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：当前收盘价与某段时间最高价的百分比
     * 通常和filterByXr方法一起使用
     *
     * @param beginDate
     * @param date
     * @param percentage
     */
    @Override
    public void filterByPercentageOfCurrentClosePriceCompareToSomeTimeHighestPrice(String beginDate, String date, Integer percentage) {
        logger.info("过滤条件：当前收盘价与某段时间最高价的百分比。beginDate为【" + beginDate + "】，"
                + "date为【" + date + "】，percentage为【" + percentage + "】");

        String sql = "{call PKG_ROBOT3.filter_p_o_c_c_p_c_t_s_t_h_p('" + beginDate + "', '" + date + "', " + percentage + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：当前收盘价与某段时间最低价的百分比
     * 通常和filterByXr方法一起使用
     * 用于做空
     *
     * @param beginDate
     * @param date
     * @param percentage
     */
    @Override
    public void filterByPercentageOfCurrentClosePriceCompareToSomeTimeLowestPrice(String beginDate, String date, Integer percentage) {
        logger.info("过滤条件：当前收盘价与某段时间最低价的百分比。beginDate为【" + beginDate + "】，"
                + "date为【" + date + "】，percentage为【" + percentage + "】");

        String sql = "{call PKG_ROBOT3.filter_p_o_c_c_p_c_t_s_t_l_p('" + beginDate + "', '" + date + "', " + percentage + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MA不单调递减，过滤股票
     * 用于做多
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    @Override
    public void filterByMANotDecreasing(String date, Integer maLevel, String dateNumber) {
        logger.info("过滤条件：MA不单调递减，过滤股票。date为【" + date + "】，maLevel为【" + maLevel + "】,"
                + "dateNumber为【" + dateNumber + "】");

        String sql = "{call PKG_ROBOT3.filter_by_ma_not_decreasing(" + maLevel + ", '" + date + "', " + dateNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：MA不单调递增，过滤股票
     * 用于多空
     *
     * @param date
     * @param maLevel
     * @param dateNumber
     */
    @Override
    public void filterByMANotIncreasing(String date, Integer maLevel, String dateNumber) {
        logger.info("过滤条件：MA不单调递增，过滤股票。date为【" + date + "】，maLevel为【" + maLevel + "】,"
                + "dateNumber为【" + dateNumber + "】");

        String sql = "{call PKG_ROBOT3.filter_by_ma_not_increasing(" + maLevel + ", '" + date + "', " + dateNumber + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：收盘价金叉五日均线
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByClosePriceGoldCrossMA5(String date) {
        logger.info("过滤条件：收盘价金叉五日均线。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_c_p_g_c_ma5('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：收盘价死叉五日均线
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByClosePriceDeadCrossMA5(String date) {
        logger.info("过滤条件：收盘价死叉五日均线。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_c_p_d_c_ma5('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：平均K线从下跌趋势变为上涨趋势
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByHeiKinAshiUpDown(String date) {
        logger.info("过滤条件：平均K线从下跌趋势变为上涨趋势。date为【" + date + "】");

        // 将当天除了ha_close_price大于ha_open_price以外的股票都删掉
        String closePriceGreatThanOpenPriceSql = "delete from robot3_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot3_stock_filter rsf on (std.code_=rsf.stock_code "
                + "and std.date_=to_date('" + date + "','yyyy-mm-dd') and std.ha_close_price >= std.ha_open_price))";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);

        // 将前一天除了ha_close_price小于ha_open_price以外的股票都删掉
        String closePriceLessThanOpenPriceSql = "delete from robot3_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot3_stock_filter rsf "
                + "on std.code_ = rsf.stock_code and std.date_ = "
                + "(select b.date_ from ( "
                + "select * from stock_transaction_data_all std1 "
                + "where std1.date_ < to_date('" + date + "', 'yyyy-mm-dd') order by std1.date_ desc) b "
                + "where rownum <= 1) "
                + "and std.ha_close_price <= std.ha_open_price)";
        doSQLInTransaction(closePriceLessThanOpenPriceSql);
    }

    /**
     * 过滤条件：平均K线从上涨趋势变为下跌趋势
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByHeiKinAshiDownUp(String date) {
        logger.info("过滤条件：平均K线从上涨趋势变为下跌趋势。date为【" + date + "】");

        // 将当天除了ha_close_price小于ha_open_price以外的股票都删掉
        String closePriceGreatThanOpenPriceSql = "delete from robot3_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot3_stock_filter rsf on (std.code_=rsf.stock_code "
                + "and std.date_=to_date('" + date + "','yyyy-mm-dd') and std.ha_close_price <= std.ha_open_price))";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);

        // 将前一天除了ha_close_price大于ha_open_price以外的股票都删掉
        String closePriceLessThanOpenPriceSql = "delete from robot3_stock_filter t where t.stock_code not in ("
                + "select std.code_ "
                + "from stock_transaction_data_all std "
                + "join robot3_stock_filter rsf "
                + "on std.code_ = rsf.stock_code and std.date_ = "
                + "(select b.date_ from ( "
                + "select * from stock_transaction_data_all std1 "
                + "where std1.date_ < to_date('" + date + "', 'yyyy-mm-dd') order by std1.date_ desc) b "
                + "where rownum <= 1) "
                + "and std.ha_close_price >= std.ha_open_price)";
        doSQLInTransaction(closePriceLessThanOpenPriceSql);
    }

    /**
     * 过滤条件：KD金叉
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByKDGoldCross(String date) {
        logger.info("过滤条件：KD金叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_kd_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：KD死叉
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByKDDeadCross(String date) {
        logger.info("过滤条件：KD死叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_kd_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留所有金叉
     * 用于做多
     *
     * @param date
     */
    @Override
    public void filterByAllGoldCross(String date) {
        logger.info("过滤条件：保留所有金叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_all_gold_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：保留所有死叉
     * 用于做空
     *
     * @param date
     */
    @Override
    public void filterByAllDeadCross(String date) {
        logger.info("过滤条件：保留所有死叉。date为【" + date + "】");

        String sql = "{call PKG_ROBOT3.filter_by_all_dead_cross('" + date + "')}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：如果当前交易日和前n个交易日的收盘价都大于/小于布林带上轨/下轨，并且当前交易日的收盘价小于/大于前一个交易日的收盘价，则保留
     * @param date
     * @param dateNumber
     * @param operationDirection
     */
    @Override
    public void filterByUpDownBoll(String date, Integer dateNumber, Integer operationDirection){
        logger.info("过滤条件：如果当前交易日【" + date + "】和前【" + (dateNumber - 1) + "】个交易日的收盘价都大于/小于布林带上轨/下轨，并且当前交易日的收盘价小于/大于前一个交易日的收盘价，则保留");

        String sql = "{call PKG_ROBOT3.filter_by_up_down_boll('" + date + "', " + dateNumber + ", " + operationDirection + ")}";

        doSQLInTransaction(sql);
    }

    /**
     * 过滤条件：删除某一天所在的那周的kd平均值在kdAverageTop和kdAverageBottom之外的股票
     * @param date
     * @param kdAverageTop
     * @param kdAverageBottom
     */
    @Override
    public void filterByStockWeekKDBetween(String date, Integer kdAverageTop, Integer kdAverageBottom){
        logger.info("过滤条件：删除某一天【" + date + "】所在的那周的kd平均值在【" + kdAverageBottom + "】和【" + kdAverageTop + "】之外的股票");

        String closePriceGreatThanOpenPriceSql = "delete from robot3_stock_filter rsf where rsf.stock_code in( " +
                "select sw.code_ from stock_week sw where to_date('" + date + "','yyyy-mm-dd') between sw.begin_date and sw.end_date " +
                "and (sw.k+sw.d)/2 between " + kdAverageBottom + " and " + kdAverageTop + ")";
        doSQLInTransaction(closePriceGreatThanOpenPriceSql);
    }

    /**
     * 返回robot_stock_filter表的记录数
     *
     * @return
     */
    @Override
    public Integer count() {
        logger.info("返回robot3_stock_filter表的记录数");

        String sql = "select count(*) from robot3_stock_filter";

        return doSQLQueryInTransaction(sql, null);
    }
}
