package cn.com.acca.ma.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 交易记录表（全部数据）
 */
@Data
@Entity
@Table(name="STOCK_TRANSACTION_DATA_ALL")
public class StockTransactionDataAll implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID_")
    private Integer id;

    /**
     * 日期
     */
    @Column(name="DATE_")
    private Date date;

    /**
     * 股票代码
     */
    @Column(name="CODE_")
    private String code;

    /**
     * 开盘价
     */
    @Column(name="OPEN_PRICE")
    private BigDecimal openPrice;

    /**
     * 收盘价
     */
    @Column(name="CLOSE_PRICE")
    private BigDecimal closePrice;

    /**
     * 最高价
     */
    @Column(name="HIGHEST_PRICE")
    private BigDecimal highestPrice;

    /**
     * 最低价
     */
    @Column(name="LOWEST_PRICE")
    private BigDecimal lowestPrice;

    /**
     * 前收盘
     */
    @Column(name="LAST_CLOSE_PRICE")
    private BigDecimal lastClosePrice;

    /**
     * 今日股价的涨跌额
     */
    @Column(name="CHANGE_AMOUNT")
    private BigDecimal changeAmount;

    /**
     * 今日股价的涨跌幅。单位：%
     */
    @Column(name="CHANGE_RANGE")
    private BigDecimal changeRange;

    /**
     * 今日股价是否涨跌。1：涨，-1：跌，0：平
     */
    @Column(name="UP_DOWN")
    private BigDecimal upDown;

    /**
     * 换手率
     */
    @Column(name="TURNOVER_RATE")
    private BigDecimal turnoverRate;

    /**
     * 成交量
     */
    @Column(name="VOLUME")
    private BigDecimal volume;

    /**
     * 成交额
     */
    @Column(name="TURNOVER")
    private BigDecimal turnover;

    /**
     * 总市值
     */
    @Column(name="TOTAL_MARKET_VALUE")
    private BigDecimal totalMarketValue;

    /**
     * 流通市值
     */
    @Column(name="CIRCULATION_MARKET_VALUE")
    private BigDecimal circulationMarketValue;

    /**
     * 成交笔数
     */
    @Column(name="TRANSACTION_NUMBER")
    private BigDecimal transactionNumber;

    /**
     * 5日均线
     */
    @Column(name="MA5")
    private BigDecimal ma5;

    /**
     * 10日均线
     */
    @Column(name="MA10")
    private BigDecimal ma10;

    /**
     * 20日均线
     */
    @Column(name="MA20")
    private BigDecimal ma20;

    /**
     * 60日均线
     */
    @Column(name="MA60")
    private BigDecimal ma60;

    /**
     * 120日均线
     */
    @Column(name="MA120")
    private BigDecimal ma120;

    /**
     * 250日均线
     */
    @Column(name="MA250")
    private BigDecimal ma250;

    /**
     * 计算MACD时的指标
     */
    @Column(name="EMA12")
    private BigDecimal ema12;

    /**
     * 计算MACD时的指标
     */
    @Column(name="EMA26")
    private BigDecimal ema26;

    /**
     * 计算MACD时的指标
     */
    @Column(name="DIF")
    private BigDecimal dif;

    /**
     * 计算MACD时的指标
     */
    @Column(name="DEA")
    private BigDecimal dea;

    /**
     * MACD动能柱
     */
    @Column(name="MACD")
    private BigDecimal MACD;

    /**
     * 股价5日内震荡幅度。计算方法为：将5日内涨跌百分比的绝对值相加，再除以5。单位：%
     */
    @Column(name="FIVE_DAY_VOLATILITY")
    private BigDecimal fiveDayVolatility;

    /**
     * 股价10日内震荡幅度。计算方法为：将10日内涨跌百分比的绝对值相加，再除以10。单位：%
     */
    @Column(name="TEN_DAY_VOLATILITY")
    private BigDecimal tenDayVolatility;

    /**
     * 股价20日内震荡幅度。计算方法为：将20日内涨跌百分比的绝对值相加，再除以20。单位：%
     */
    @Column(name="TWENTY_DAY_VOLATILITY")
    private BigDecimal twentyDayVolatility;

    /**
     * 股价250日内震荡幅度。计算方法为：将250日内涨跌百分比的绝对值相加，再除以250。单位：%
     */
    @Column(name="TWO_HUNDRED_FIFTY")
    private BigDecimal twoHundredFifty;

    /**
     * KD指标中的RSV
     */
    @Column(name="RSV")
    private BigDecimal rsv;

    /**
     * KD指标中的K
     */
    @Column(name="K")
    private BigDecimal k;

    /**
     * KD指标中的D
     */
    @Column(name="D")
    private BigDecimal d;

    /**
     * HeiKinAshi开盘价
     */
    @Column(name="HA_OPEN_PRICE")
    private BigDecimal haOpenPrice;

    /**
     * HeiKinAshi收盘价
     */
    @Column(name="HA_CLOSE_PRICE")
    private BigDecimal haClosePrice;

    /**
     * HeiKinAshi最高价
     */
    @Column(name="HA_HIGHEST_PRICE")
    private BigDecimal haHighestPrice;

    /**
     * HeiKinAshi最低价
     */
    @Column(name="HA_LOWEST_PRICE")
    private BigDecimal haLowestPrice;

    /**
     * 布林带上轨
     */
    @Column(name="UP")
    private BigDecimal up;

    /**
     * 布林带中轨
     */
    @Column(name="MB")
    private BigDecimal mb;

    /**
     * 布林带下轨
     */
    @Column(name="DN_")
    private BigDecimal dn;

    /**
     * 5日乖离率
     */
    @Column(name="BIAS5")
    private BigDecimal bias5;

    /**
     * 10日乖离率
     */
    @Column(name="BIAS10")
    private BigDecimal bias10;

    /**
     * 20日乖离率
     */
    @Column(name="BIAS20")
    private BigDecimal bias20;

    /**
     * 60日乖离率
     */
    @Column(name="BIAS60")
    private BigDecimal bias60;

    /**
     * 120日乖离率
     */
    @Column(name="BIAS120")
    private BigDecimal bias120;

    /**
     * 250日乖离率
     */
    @Column(name="BIAS250")
    private BigDecimal bias250;

    /**
     * 5日方差
     */
    @Column(name="VARIANCE5")
    private BigDecimal variance5;

    /**
     * 10日方差
     */
    @Column(name="VARIANCE10")
    private BigDecimal variance10;

    /**
     * 20日方差
     */
    @Column(name="VARIANCE20")
    private BigDecimal variance20;

    /**
     * 60日方差
     */
    @Column(name="VARIANCE60")
    private BigDecimal variance60;

    /**
     * 120日方差
     */
    @Column(name="VARIANCE120")
    private BigDecimal variance120;

    /**
     * 250日方差
     */
    @Column(name="VARIANCE250")
    private BigDecimal variance250;
}
