package cn.com.acca.ma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 收盘价死叉5日均线的交易
 */
@Entity
@Table(name = "MDL_CLOSE_PRICE_MA5_DEAD_CROSS")
public class ModelClosePriceMA5DeadCross {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    /**
     * 股票代码
     */
    @Column(name = "STOCK_CODE")
    private String stockCode;

    /**
     * 买入日期
     */
    @Column(name = "BUY_DATE")
    private Date buyDate;

    /**
     * 买入价格
     */
    @Column(name = "BUY_PRICE")
    private BigDecimal buyPrice;

    /**
     * 买入时的5日均线
     */
    @Column(name = "BUY_MA5")
    private BigDecimal buyMA5;

    /**
     * 累计收益。单位：元
     */
    @Column(name = "ACCUMULATIVE_PROFIT_LOSS")
    private BigDecimal accumulativeProfitLoss;

    /**
     * 本次交易盈亏百分比
     */
    @Column(name = "PROFIT_LOSS")
    private BigDecimal profitLoss;

    /**
     * 卖出日期
     */
    @Column(name = "SELL_DATE")
    private Date sellDate;

    /**
     * 卖出价格
     */
    @Column(name = "SELL_PRICE")
    private BigDecimal sellPrice;

    /**
     * 卖出时的5日均线
     */
    @Column(name = "SELL_MA5")
    private BigDecimal sellMA5;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getBuyMA5() {
        return buyMA5;
    }

    public void setBuyMA5(BigDecimal buyMA5) {
        this.buyMA5 = buyMA5;
    }

    public BigDecimal getAccumulativeProfitLoss() {
        return accumulativeProfitLoss;
    }

    public void setAccumulativeProfitLoss(BigDecimal accumulativeProfitLoss) {
        this.accumulativeProfitLoss = accumulativeProfitLoss;
    }

    public BigDecimal getProfitLoss() {
        return profitLoss;
    }

    public void setProfitLoss(BigDecimal profitLoss) {
        this.profitLoss = profitLoss;
    }

    public Date getSellDate() {
        return sellDate;
    }

    public void setSellDate(Date sellDate) {
        this.sellDate = sellDate;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getSellMA5() {
        return sellMA5;
    }

    public void setSellMA5(BigDecimal sellMA5) {
        this.sellMA5 = sellMA5;
    }
}
