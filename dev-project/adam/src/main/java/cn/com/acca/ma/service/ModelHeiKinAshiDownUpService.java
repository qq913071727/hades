package cn.com.acca.ma.service;

public interface ModelHeiKinAshiDownUpService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												按日期计算数据
     *
     *********************************************************************************************************************/
    /**
     * 根据endDate日期，平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     * @param multithreading
     */
    void writeModelHeiKinAshiDownUpIncr(boolean multithreading);

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线，ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入
     * 向表MDL_HEI_KIN_ASHI_DOWN_UP插入数据
     */
    void writeModelHeiKinAshiDownUp();
}
