package cn.com.acca.ma.service;

public interface ForeignExchangeRecordService {
	/**
	 * 向FOREIGN_EXCHANGE表中插入数据
	 */
	void insertForeignExchangeRecord();
	
	/**
	 * 计算FOREIGN_EXCHANGE_RECORD表的简单移动平均线
	 */
	void calculateMovingAverage();
}
