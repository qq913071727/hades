package cn.com.acca.ma.dao;

import java.util.List;

import cn.com.acca.ma.model.ForeignExchange;

public interface ForeignExchangeDao extends BaseDao {
	/**
	 * 向FOREIGN_EXCHANGE表中插入数据
	 * @param foreignExchange
	 */
	void insertForeignExchange(ForeignExchange foreignExchange);
	
	/**
	 * 获取所有ForeignExchange对象
	 * @return
	 */
	List<ForeignExchange> getAllForeignExchange();
}
