package cn.com.acca.ma.service;

public interface ModelEtfBullShortLineUpService extends BaseService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_ETF_BULL_SHORT_LINE_UP中插入数据
     */
    void writeModelEtfBullShortLineUp();

    /**
     * 增量地向表MDL_ETF_BULL_SHORT_LINE_UP中插入数据
     */
    void writeModelEtfBullShortLineUpIncr();
}
