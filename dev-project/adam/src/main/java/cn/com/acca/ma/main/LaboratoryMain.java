package cn.com.acca.ma.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LaboratoryMain extends AbstractMain {

    private static Logger logger = LogManager.getLogger(LaboratoryMain.class);

    public static void main(String[] args) {
        logger.info("LaboratoryMain程序开始运行......");

//        createAllTransactionStrategyGoldCrossSuccessRatePicture();
//        createAllTransactionStrategyDeadCrossSuccessRatePicture();
//        createModelTopStockDetailPicture();
//        createAllTransactionStrategyGoldCrossPercentagePicture();
//        createAllTransactionStrategyDeadCrossPercentagePicture();
//        createRobotAccountLogProfitRatePicture();
        createRobotEtfAccountLogProfitRatePicture();
//        createWeekBollUpDnPercentAndGoldCrossDeadCrossProfitScatterPlotPicture();
//        createModelStockIndexProfitRatePicture();

//        writeBoard2IndexDaily();

        /***************************************** 使操作系统进入睡眠状态 *****************************************/
//        makeOperationSystemSleep();

        logger.info("LaboratoryMain程序执行完毕。");
    }
}
