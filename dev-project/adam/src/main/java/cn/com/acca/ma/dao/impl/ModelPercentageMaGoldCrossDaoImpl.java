package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelPercentageMaGoldCrossDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import org.hibernate.SQLQuery;

public class ModelPercentageMaGoldCrossDaoImpl extends BaseDaoImpl<ModelPercentageMaGoldCrossDaoImpl> implements
    ModelPercentageMaGoldCrossDao {

    public ModelPercentageMaGoldCrossDaoImpl() {
        super();
    }

    /**
     * 使用percentage*_ma*金叉模型算法，海量地向表MDL_PERCENTAGE_MA_GOLD_CROSS插入数据
     */
    @Override
    public void writeModelPercentageMaGoldCross() {
        logger.info("使用percentage*_ma*金叉模型算法，海量地向表MDL_PERCENTAGE_MA_GOLD_CROSS插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_RECORD.CAL_MDL_PERCENTAGE_MA_G_C()}");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("使用percentage*_ma*金叉模型算法，海量地向表MDL_PERCENTAGE_MA_GOLD_CROSS插入数据完成");
    }

}
