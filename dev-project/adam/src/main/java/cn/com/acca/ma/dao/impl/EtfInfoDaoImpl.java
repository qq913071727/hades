package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.EtfInfoDao;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.EtfInfo;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class EtfInfoDaoImpl extends BaseDaoImpl<EtfInfoDaoImpl> implements EtfInfoDao {

    public EtfInfoDaoImpl() {
        super();
    }

    /**
     * 通过ETF代码code_，查找EtfInfo对象
     *
     * @param code
     * @return
     */
    public EtfInfo getEtfInfoObjectByCode(String code) {
        logger.info("通过ETF代码code_，查找EtfInfo对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(EtfInfo.class);
        criteria.add(Restrictions.eq("code_", code.toUpperCase()));
        EtfInfo result = (EtfInfo) criteria.uniqueResult();
        session.getTransaction().commit();
        session.close();

        return result;
    }

    /**
     * 获得所有EtfInfo对象
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<EtfInfo> getAllEtfInfo() {
        session = HibernateUtil.currentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(EtfInfo.class);
        List<EtfInfo> result = criteria.list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    /**
     * 保存EtfInfo对象
     *
     * @param etfInfo
     */
    public void saveEtfInfo(EtfInfo etfInfo) {
        logger.info("保存EtfInfo对象");

        session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.save(etfInfo);
        session.getTransaction().commit();
        session.close();
    }
}
