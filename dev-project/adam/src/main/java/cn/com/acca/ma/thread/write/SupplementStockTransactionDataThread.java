package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.enumeration.ExchangeBoardInfo;
import cn.com.acca.ma.model.StockTransactionData;
import cn.com.acca.ma.service.StockTransactionDataService;
import cn.com.acca.ma.thread.AbstractThread;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 生产者线程，使用163接口，用于从网络上采集股票数据，弥补之前由于使用126接口导致有些列是空的问题
 */
public class SupplementStockTransactionDataThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(SupplementStockTransactionDataThread.class);

    private StockTransactionDataService stockTransactionDataService;

    /**
     * 股票代码的前缀
     */
    private String stockInfoCodePrefix;

    /**
     * StockTransactionData对象
     */
    private StockTransactionData stockTransactionData;

    /**
     * 股票代码
     */
    private String code;

    /**
     * 开始时间
     */
    private String beginDate;

    /**
     * 结束时间
     */
    private String endDate;

    public SupplementStockTransactionDataThread(){

    }

    public SupplementStockTransactionDataThread(StockTransactionDataService stockTransactionDataService,
        String stockInfoCodePrefix, StockTransactionData stockTransactionData, String code, String beginDate, String endDate){

        this.stockTransactionDataService = stockTransactionDataService;
        this.stockInfoCodePrefix = stockInfoCodePrefix;
        this.stockTransactionData = stockTransactionData;
        this.code = code;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public void run() {
        logger.debug("启动生产者线程。前缀为【" + stockInfoCodePrefix + "】，股票代码为【" + code + "】，"
            + "日期为【" + beginDate + "】至【" + endDate + "】");

        Socket socket = null;
        OutputStream outputStream = null;
        try {
            this.handle();
        } finally {
            if (null != socket) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 从网络上获取数据，并存储到数据库中
     * @return
     */
    private void handle(){

        String[] shStockCodePrefixArray = ExchangeBoardInfo.SH_PREFIX.getCodePrefixArray();
        for (String codePrefix : shStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_163 + ExchangeBoardInfo.SH_PREFIX.getUrlParamPrefix()
                    + code + "&start=" + beginDate + "&end=" + endDate
                    + DATA_SOURCE_URL_SUFFIX_163;

                // 从网络上获取股票数据
                StockTransactionData supplementStockTransactionData = stockTransactionDataService.readStockTransactionDataList_163(url, code).get(0);
                // 更新stock_transaction_data表中的数据
                this.stockTransactionData.setTurnoverRate(supplementStockTransactionData.getTurnoverRate());
                this.stockTransactionData.setTurnover(supplementStockTransactionData.getTurnover());
                this.stockTransactionData.setTotalMarketValue(supplementStockTransactionData.getTotalMarketValue());
                this.stockTransactionData.setCirculationMarketValue(supplementStockTransactionData.getCirculationMarketValue());
                stockTransactionDataService.updateStockTransactionData(this.stockTransactionData);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] kcbStockCodePrefixArray = ExchangeBoardInfo.KCB_PREFIX.getCodePrefixArray();
        for (String codePrefix : kcbStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_163 + ExchangeBoardInfo.KCB_PREFIX.getUrlParamPrefix()
                    + code + "&start=" + beginDate + "&end=" + endDate
                    + DATA_SOURCE_URL_SUFFIX_163;

                // 从网络上获取股票数据
                StockTransactionData supplementStockTransactionData = stockTransactionDataService.readStockTransactionDataList_163(url, code).get(0);
                // 更新stock_transaction_data表中的数据
                this.stockTransactionData.setTurnoverRate(supplementStockTransactionData.getTurnoverRate());
                this.stockTransactionData.setTurnover(supplementStockTransactionData.getTurnover());
                this.stockTransactionData.setTotalMarketValue(supplementStockTransactionData.getTotalMarketValue());
                this.stockTransactionData.setCirculationMarketValue(supplementStockTransactionData.getCirculationMarketValue());
                stockTransactionDataService.updateStockTransactionData(this.stockTransactionData);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] szStockCodePrefixArray = ExchangeBoardInfo.SZ_PREFIX.getCodePrefixArray();
        for (String codePrefix : szStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_163 + ExchangeBoardInfo.SZ_PREFIX.getUrlParamPrefix()
                    + code + "&start=" + beginDate + "&end=" + endDate
                    + DATA_SOURCE_URL_SUFFIX_163;

                // 从网络上获取股票数据
                StockTransactionData supplementStockTransactionData = stockTransactionDataService.readStockTransactionDataList_163(url, code).get(0);
                // 更新stock_transaction_data表中的数据
                this.stockTransactionData.setTurnoverRate(supplementStockTransactionData.getTurnoverRate());
                this.stockTransactionData.setTurnover(supplementStockTransactionData.getTurnover());
                this.stockTransactionData.setTotalMarketValue(supplementStockTransactionData.getTotalMarketValue());
                this.stockTransactionData.setCirculationMarketValue(supplementStockTransactionData.getCirculationMarketValue());
                stockTransactionDataService.updateStockTransactionData(this.stockTransactionData);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] zxbStockCodePrefixArray = ExchangeBoardInfo.ZXB_PREFIX.getCodePrefixArray();
        for (String codePrefix : zxbStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_163 + ExchangeBoardInfo.ZXB_PREFIX.getUrlParamPrefix()
                    + code + "&start=" + beginDate + "&end=" + endDate
                    + DATA_SOURCE_URL_SUFFIX_163;

                // 从网络上获取股票数据
                StockTransactionData supplementStockTransactionData = stockTransactionDataService.readStockTransactionDataList_163(url, code).get(0);
                // 更新stock_transaction_data表中的数据
                this.stockTransactionData.setTurnoverRate(supplementStockTransactionData.getTurnoverRate());
                this.stockTransactionData.setTurnover(supplementStockTransactionData.getTurnover());
                this.stockTransactionData.setTotalMarketValue(supplementStockTransactionData.getTotalMarketValue());
                this.stockTransactionData.setCirculationMarketValue(supplementStockTransactionData.getCirculationMarketValue());
                stockTransactionDataService.updateStockTransactionData(this.stockTransactionData);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }

        String[] cybStockCodePrefixArray = ExchangeBoardInfo.CYB_PREFIX.getCodePrefixArray();
        for (String codePrefix : cybStockCodePrefixArray){
            if (codePrefix.equals(stockInfoCodePrefix)){
                String url = DATA_SOURCE_URL_PREFIX_163 + ExchangeBoardInfo.CYB_PREFIX.getUrlParamPrefix()
                    + code + "&start=" + beginDate + "&end=" + endDate
                    + DATA_SOURCE_URL_SUFFIX_163;

                // 从网络上获取股票数据
                StockTransactionData supplementStockTransactionData = stockTransactionDataService.readStockTransactionDataList_163(url, code).get(0);
                // 更新stock_transaction_data表中的数据
                this.stockTransactionData.setTurnoverRate(supplementStockTransactionData.getTurnoverRate());
                this.stockTransactionData.setTurnover(supplementStockTransactionData.getTurnover());
                this.stockTransactionData.setTotalMarketValue(supplementStockTransactionData.getTotalMarketValue());
                this.stockTransactionData.setCirculationMarketValue(supplementStockTransactionData.getCirculationMarketValue());
                stockTransactionDataService.updateStockTransactionData(this.stockTransactionData);

                logger.debug(url);

                AbstractThread.stockTransactionDataCollectFinishAmount++;
                break;
            }
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
