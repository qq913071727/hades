package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockTransactionDataAllService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 根据开始日期和结束日期，从表stock_transaction_data向表stock_transaction_data_all中写数据
 */
public class WriteStockTransactionDataAllByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteStockTransactionDataAllByDateThread.class);

    private StockTransactionDataAllService stockTransactionDataAllService;

    public WriteStockTransactionDataAllByDateThread() {
    }

    public WriteStockTransactionDataAllByDateThread(StockTransactionDataAllService stockTransactionDataAllService) {
        this.stockTransactionDataAllService = stockTransactionDataAllService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始根据开始日期和结束日期，"
            + "从表stock_transaction_data向表stock_transaction_data_all中写数据，"
            + "调用的方法为【writeStockTransactionDataAllByDate】");

        stockTransactionDataAllService.writeStockTransactionDataAllByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
