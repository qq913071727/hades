package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelClosePriceMA5DeadCross;
import cn.com.acca.ma.model.ModelClosePriceMA5GoldCross;
import cn.com.acca.ma.pojo.SuccessRateOrPercentagePojo;

import java.util.Date;
import java.util.List;

public interface ModelClosePriceMA5DeadCrossDao extends BaseDao {

    /**
     * 海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     */
    void writeModelClosePriceMA5DeadCross();

    /**
     * 增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
     * @param endDate
     */
    void writeModelClosePriceMA5DeadCrossIncr(String endDate);

    /**
     * 获取一段时间内，最近dateNumber天，close_price金死ma5的成功率
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @param dateNumber
     * @return
     */
    List<SuccessRateOrPercentagePojo> getClosePriceMa5DeadCrossSuccessRate(boolean multithreading, String beginDate, String endDate, Integer dateNumber);

    /**
     * 获取一段时间内，close_price金叉ma5的百分比
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List<SuccessRateOrPercentagePojo> getClosePriceMa5DeadCrossPercentage(boolean multithreading, final String beginDate, final String endDate);

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取SELL_DATE字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中获取sell_date日的所有ModelClosePriceMA5DeadCross对象
     * @param date
     * @return
     */
    List<ModelClosePriceMA5DeadCross> getModelClosePriceMA5DeadCrossByDate(String date);

    /**
     * 向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入ModelClosePriceMA5DeadCross对象
     * @param modelClosePriceMA5DeadCross
     */
    void save(ModelClosePriceMA5DeadCross modelClosePriceMA5DeadCross);
}
