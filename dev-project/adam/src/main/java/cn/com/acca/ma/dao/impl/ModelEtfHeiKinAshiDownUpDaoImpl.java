package cn.com.acca.ma.dao.impl;

import cn.com.acca.ma.dao.ModelEtfHeiKinAshiDownUpDao;
import cn.com.acca.ma.model.ModelEtfHeiKinAshiDownUp;
import cn.com.acca.ma.hibernate.util.HibernateUtil;
import cn.com.acca.ma.model.ModelStockIndexHeiKinAshiDownUp;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import java.util.List;

public class ModelEtfHeiKinAshiDownUpDaoImpl extends BaseDaoImpl<ModelEtfHeiKinAshiDownUp> implements
        ModelEtfHeiKinAshiDownUpDao {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 平均K线ha_close_price小于等于ha_open_price时卖出，ha_close_price大于ha_open_price时买入，
     * 向表mdl_etf_hei_kin_ashi_down_up插入数据
     * @param validateMA120NotDecreasing
     * @param validateMA250NotDecreasing
     * @param defaultBeginDate
     * @param endDate
     * @param type
     */
    @Override
    public void writeModelEtfHeiKinAshiDownUp(Integer validateMA120NotDecreasing, Integer validateMA250NotDecreasing, String defaultBeginDate,
                                              String endDate, Integer type) {
        logger.info("开始向表mdl_etf_hei_kin_ashi_down_up插入数据");

        session= HibernateUtil.currentSession();
        session.beginTransaction();
        SQLQuery query = session.createSQLQuery("{call PKG_MODEL_ETF.CAL_MDL_ETF_H_K_A_DOWN_UP(?, ?, ?, ?, ?)}");
        query.setParameter(0, validateMA120NotDecreasing);
        query.setParameter(1, validateMA250NotDecreasing);
        query.setParameter(2, defaultBeginDate);
        query.setParameter(3, endDate);
        query.setParameter(4, type);
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();

        logger.info("向表mdl_etf_hei_kin_ashi_down_up插入数据完成");
    }

    /*********************************************************************************************************************
     *
     * 												创建图表
     *
     *********************************************************************************************************************/
    /**
     * 根据type和指数代码，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列
     * @param type
     * @param indexCode
     * @return
     */
//    @Override
//    public List<ModelStockIndexHeiKinAshiDownUp> findByTypeAndIndexCodeOrderBySellDateAsc(Integer type, String indexCode) {
//        logger.info("开始根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列");
//
//        session= HibernateUtil.currentSession();
//        session.beginTransaction();
//        Query query = session.createQuery("select t from ModelStockIndexHeiKinAshiDownUp t " +
//                "where t.stockIndexCode=? and t.type=? " +
//                "order by t.sellDate asc");
//        query.setParameter(0, indexCode);
//        query.setParameter(1, type);
//        List<ModelStockIndexHeiKinAshiDownUp> modelStockIndexHeiKinAshiDownUpList = query.list();
//        session.getTransaction().commit();
//        session.close();
//
//        logger.info("根据type:【" + type + "】和指数代码:【" + indexCode + "】，从表mdl_stock_index_h_k_a_up_down中返回数据，并按照sell_date升序排列完成");
//
//        return modelStockIndexHeiKinAshiDownUpList;
//    }
}
