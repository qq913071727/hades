package cn.com.acca.ma.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 卖建议
 */
@Data
public class SellSuggestion {

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 卖出日期
     */
    private Date sellDate;

    /**
     * 卖出价格
     */
    private BigDecimal sellPrice;

    /**
     * 卖出数量
     */
    private Integer sellAmount;

    /**
     * 过滤类型
     */
    private Integer filterType;

    /**
     * 多空方向。1表示做多；-1表示做空
     */
    private Integer direction;

}
