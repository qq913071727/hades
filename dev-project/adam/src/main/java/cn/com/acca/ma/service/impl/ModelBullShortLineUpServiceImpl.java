package cn.com.acca.ma.service.impl;

import cn.com.acca.ma.common.util.PropertiesUtil;
import cn.com.acca.ma.model.ModelBullShortLineUp;
import cn.com.acca.ma.model.ModelClosePriceMA5DeadCross;
import cn.com.acca.ma.service.ModelBullShortLineUpService;
import cn.com.acca.ma.service.ModelClosePriceMA5DeadCrossService;

import java.util.List;

public class ModelBullShortLineUpServiceImpl extends BaseServiceImpl<ModelBullShortLineUpServiceImpl, ModelBullShortLineUp> implements
        ModelBullShortLineUpService {

    /*********************************************************************************************************************
     *
     * 												计算全部数据
     *
     *********************************************************************************************************************/
    /**
     * 海量地向表MDL_BULL_SHORT_LINE_UP中插入数据
     */
    @Override
    public void writeModelBullShortLineUp() {
        modelBullShortLineUpDao.writeModelBullShortLineUp();
    }

    /**
     * 增量地向表MDL_BULL_SHORT_LINE_UP中插入数据
     */
    @Override
    public void writeModelBullShortLineUpIncr() {
        String date = PropertiesUtil
                .getValue(MODEL_BULL_SHORT_LINE_UP_PROPERTIES, "bull.short.line.up.date");
        modelBullShortLineUpDao.writeModelBullShortLineUpIncr(date);
    }


    @Override
    public String listToString(List list) {
        return null;
    }
}
