package cn.com.acca.ma.service;

public interface RobotStockTransactionRecordService extends BaseService {

    /**
     * 执行某段时间内的股票买卖交易（成功率）
     */
    void doBuyAndSellByBeginDateAndEndDate_successRate();

    /**
     * 卖股票/买股票
     */
    void sellOrBuy();

    /**
     * 买股票/卖股票
     */
    void buyOrSell();

    /**
     * 打印买卖建议
     */
    void printBuySellSuggestion();
}
