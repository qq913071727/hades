package cn.com.acca.ma.thread.write;

import cn.com.acca.ma.globalvariable.WechatSubscriptionGlobalVariable;
import cn.com.acca.ma.service.StockIndexService;
import cn.com.acca.ma.thread.AbstractThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 根据日期date，计算所有指数某一日的乖离率
 */
public class WriteStockIndexBiasByDateThread extends AbstractThread implements Runnable {

    private static Logger logger = LogManager.getLogger(WriteStockIndexBiasByDateThread.class);

    private StockIndexService stockIndexService;

    public WriteStockIndexBiasByDateThread() {
    }

    public WriteStockIndexBiasByDateThread(StockIndexService stockIndexService) {
        this.stockIndexService = stockIndexService;
    }

    @Override
    public void run() {
        logger.info("启动线程，开始根据日期date，计算所有指数某一日的乖离率，"
            + "调用的方法为【writeStockIndexBiasByDate】");

        stockIndexService.writeStockIndexBiasByDate(Boolean.TRUE);

        synchronized (AbstractThread.class){
            WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber++;
            logger.info("已经完成了【" + WechatSubscriptionGlobalVariable.wechatSubscriptionThreadFinishNumber + "】个线程");
        }
    }

    /**
     * 创建子报告
     */
    @Override
    public void generateSubReport() {

    }
}
