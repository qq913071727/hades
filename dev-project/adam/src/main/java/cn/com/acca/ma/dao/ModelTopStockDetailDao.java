package cn.com.acca.ma.dao;

import cn.com.acca.ma.model.ModelTopStock;
import cn.com.acca.ma.model.ModelTopStockDetail;
import java.util.Date;
import java.util.List;

public interface ModelTopStockDetailDao extends BaseDao {

    /**
     * 按照日期，向表MDL_TOP_STOCK_DETAIL中写入数据
     * @param multithreading
     * @param beginDate
     * @param endDate
     */
    void writeModelTopStockDetailByDate(boolean multithreading, String beginDate, String endDate);

    /**
     * 根据开始日期beginDate和结束日期endDate计算表MDL_TOP_STOCK_DETAIL中的percentage_*_ma*字段
     * @param beginDate
     * @param endDate
     */
    void writeModelTopStockDetailMAByDate(String beginDate, String endDate);

    /**
     * 根据开始时间beginDate和结束时间endDate，从表MDL_TOP_STOCK_DETAIL中获取DATE_字段
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Date> getDateByCondition(String beginDate, String endDate);

    /**
     * 从表MDL_TOP_STOCK_DETAIL中获取date日的所有ModelTopStockDetail对象
     * @param date
     * @return
     */
    List<ModelTopStockDetail> getModelTopStockDetailByDate(String date);

    /**
     * 向表MDL_TOP_STOCK_DETAIL中插入ModelTopStockDetail对象
     * @param modelTopStockDetail
     */
    void save(ModelTopStockDetail modelTopStockDetail);

    /**
     * 根据开始时间和结束时间，查找code，并去重
     * @param beginDate
     * @param endDate
     * @return
     */
    List<String> findDistinctCodeBetweenDate(String beginDate, String endDate);

    /**
     * 查询某段时间内，up_down_percentage_21、percentage_21_ma5、percentage_21_ma10、percentage_21_ma20、close_price的平均值
     * @param beginDate
     * @param endDate
     * @return
     */
    List<Object> findDateAndAvgBetweenGroupByOrderBy(String beginDate, String endDate);

    /**
     * 获取某一段时间内up_down_percentage_21中的最大价格和最小价格
     * @param multithreading
     * @param beginDate
     * @param endDate
     * @return
     */
    List getMaxMinAverageUpDownPercentage21Week(boolean multithreading, String beginDate, String endDate);

    /**
     * 根据code，查找ModelTopStockDetail列表，并按照时间升序排列
     * @param code
     * @return
     */
    List<ModelTopStockDetail> findByCodeOrderByDateAsc(String code);
}
