package cn.com.acca.ma.enumeration;

public enum CurrencyPart {
	AUDUSD("AUDUSD","澳大利亚元兑美元"),
	EURUSD("EURUSD","欧元兑美元"),
	GBPUSD("GBPUSD","英镑兑美元"),
	NZDUSD("NZDUSD","新西兰元兑美元"),
	USDCAD("USDCAD","美元兑加拿大元"),
	USDCHF("USDCHF","美元兑瑞士法郎"),
	USDJPY("USDJPY","美元兑日元"),
	USDNOK("USDNOK","美元兑挪威克朗"),
	USDSEK("USDSEK","美元兑瑞典克朗"),
	USDSGD("USDSGD","美元兑新加坡元");
	
	private String name;
	private String description;
	
	private CurrencyPart(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
}
