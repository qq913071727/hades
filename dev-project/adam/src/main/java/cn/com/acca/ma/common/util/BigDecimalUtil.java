package cn.com.acca.ma.common.util;

import java.math.BigDecimal;

/**
 * BigDecimal的工具类
 */
public class BigDecimalUtil {

    /**
     * 将字符串类型的科学计数法转换为BigDecimal类型对象
     * @param param
     * @return
     */
    public static BigDecimal convertScientificNotation(String param){
        if (null == param){
            return null;
        }
        if (param.contains("E+")){
            String[] paramArray = param.split("E\\+");
            int tenNumber = 0;
            char[] charArray = paramArray[1].toCharArray();
            for (int i=0; i<charArray.length; i++){
                if (charArray[i] == 0){
                    continue;
                }else {
                    tenNumber = Integer.parseInt(param.substring(i));
                    break;
                }
            }
            BigDecimal result = new BigDecimal(Double.parseDouble(paramArray[0]) * tenNumber * 10);
            return result;
        }else {
            return null;
        }
    }
}
