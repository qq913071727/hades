package cn.com.acca.ma.service;

import cn.com.acca.ma.model.StockTransactionData;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.TimeSeriesCollection;

import cn.com.acca.ma.model.StockInfo;
import cn.com.acca.ma.model.StockMovingAverage;

public interface StockTransactionDataService extends BaseService {
    /********************************************** 从网络上获取数据，并插入到数据库中 **********************************************/
    /**
     * 调用sohu的接口，向stock_transaction_data表中插入数据
     */
    void insertStockTransactionData_sohu();

    /**
     * 调用baostock的python程序，向stock_transaction_data表中插入数据
     */
    void insertStockTransactionData_baoStock();

    /**
     * 从网络上获取数据，并插入到数据库中，分别调用方法：readStockTransactionDataList, writeStockTransactionData
     * 和readStockCode。
     */
    void insertStockTransactionData_163();

    /**
     * 调用126的接口获取股票交易数据，并插入到数据库
     */
    void insertStockTransactionData_126();

    /**
     * 补充使用126接口没有的字段。使用126接口有些字段是空的，只能之后再使用163的接口将确实的字段补齐
     */
    void supplementDataBy163Api();

    /**
     * 从网络中获取数据(sohu)
     *
     * @param url
     * @param codeNum
     * @param lastClosePrice
     * @return
     */
    List<StockTransactionData> readStockTransactionDataList_sohu(String url, String codeNum, BigDecimal lastClosePrice);

    /**
     * 从网络中获取数据(163)
     *
     * @param url
     * @param codeNum
     * @return
     */
    List<StockTransactionData> readStockTransactionDataList_163(String url, String codeNum);

    /**
     * 从网络中获取数据(126)
     *
     * @param url
     * @param codeNum
     * @param beginDate
     * @param endDate
     * @param lastClosePrice
     * @return
     */
    List<StockTransactionData> readStockTransactionDataList_126(String url, String codeNum, String beginDate, String endDate, BigDecimal lastClosePrice);

    /**
     * 将从网络中读取的数据写入到表STOCK_TRANSACTION_DATA中。
     *
     * @param stockTransactionDataList
     */
    void writeStockTransactionDataList(List<StockTransactionData> stockTransactionDataList);

    /**
     * 将从网络中读取数据，更新STOCK_TRANSACTION_DATA表中的数据。
     *
     * @param stockTransactionData
     */
    void updateStockTransactionData(StockTransactionData stockTransactionData);

    /**
     * 从表STOCK_INFO中读取数据
     *
     * @return
     */
    List<StockInfo> readStockInfo();

    /******************************* 从网络上获取数据，存储在.json文件中；然后再解析文件，并插入到数据库中 ***************************/
    /**
     * 从网络上获取数据，存储在.json文件中；然后再解析文件，并插入到数据库中
     */
    void readAndWriteAllRecordsByJson();

    /**
     * 将从网络上获取的数据存储到.json文件中
     *
     * @param stockTransactionDataList
     */
    void storeAllRecordsInJson(List<StockTransactionData> stockTransactionDataList);

    /**
     * 清空ALL_RECORDS_IN_JSON.json文件的内容
     */
    void emptyJsonFile();

    /**
     * 将.json文件中的数据插入数据库
     */
    void insertAllRecordsFromJson();

    /************************************************** 更新每天的数据 ************************************************************/
    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的FIVE，TEN，TWENTY，SIXTY和ONE_HUNDRED_TWENTY字段
     *
     * @param multithreading
     */
    void writeMovingAverageByDate(boolean multithreading);

    /**
     * 更新date日，更新stock_transaction_data表的change_range_ex_right字段的值
     */
    void writeChangeRangeExRightByDate();

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的UP_DOWN字段
     */
    void writeUpDownByDate();

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的EMA15，EMA26，DIF和DEA字段
     *
     * @param multithreading
     */
    void writeMACDByDate(boolean multithreading);

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的TODAY_UP_DOWN_PERCENTAGE字段
     */
    void calUpDownPercentageByDate();

    /**
     * 更新表STOCK_TRANSACTION_DATA中某一日的FIVE_DAY_VOLATILITY，TEN_DAY_VOLATILITY和TWENTY_DAY_VOLATILITY字段
     */
    void calVolatilityByDate();

    /**
     * 更新stock_transaction_data表中某一日的KD指标
     *
     * @param multithreading
     */
    void writeKDByDate(boolean multithreading);

    /**
     * 更新stock_transaction_data表中某一日的HA_OPEN_PRICE、HA_CLOSE_PRICE、HA_HIGHEST_PRICE、HA_LOWEST_PRICE
     *
     * @param multithreading
     */
    void writeHaByDate(boolean multithreading);

    /**
     * 更新stock_transaction_data_all表中某一日的boll相关字段
     *
     * @param multithreading
     */
    void writeBollByDate(boolean multithreading);

    /**
     * 更新stock_transaction_data_all表中某一日的bias相关字段
     *
     * @param multithreading
     */
    void writeBiasByDate(boolean multithreading);

    /**
     * 更新stock_transaction_data_all表中某一日的方差相关字段
     *
     * @param multithreading
     */
    void writeVarianceByDate(boolean multithreading);

    /**
     * 更新stock_transaction_data_all表中某一日的成交量移动平均线相关字段
     *
     * @param multithreading
     */
    void writeVolumeMaByDate(boolean multithreading);

    /**
     * 更新stock_transaction_data_all表中某一日的成交额移动平均线相关字段
     *
     * @param multithreading
     */
    void writeTurnoverMaByDate(boolean multithreading);

    /************************************************** 根据每日数据更新图表 *******************************************************/
    /**
     * 创建移动平均线多头排列和空头排列图
     *
     * @param multithreading
     */
    void createMovingAverageBullRankShortOrderPicture(boolean multithreading);

    /**
     * 根据开始日期和结束日期，创建移动平均线多头排列和空头排列图。分别用来创建最近10年的图和半年的图
     *
     * @param multithreading
     * @param beginDate      开始时间
     * @param endDate        结束时间
     * @param flag           创建最近10年图时，flag为true，图片名称中有all；创建半年图时，flag为false，图片名称中没有all
     */
    void paintMovingAverageBullRankShortOrderPicture(boolean multithreading, String beginDate, String endDate, boolean flag);

    /**
     * 创建MACD图
     *
     * @param multithreading
     */
    void createMACDPicture(boolean multithreading);

    /**
     * 创建股票收盘价图
     *
     * @param multithreading
     * @param tenYear        是否创建最近十年的图表
     */
    void createClosePicture(boolean multithreading, boolean tenYear);

    /**
     * 创建KD图
     *
     * @param multithreading
     */
    void createKDPicture(boolean multithreading);

    /**
     * 创建close_price金叉MA5图
     *
     * @param multithreading
     */
    void createClosePriceGoldCrossMa5Picture(boolean multithreading);

    /**
     * 创建hei_kin_ashi上升趋势图
     *
     * @param multithreading
     */
    void createHeiKinAshiUpDownPicture(boolean multithreading);

    /**
     * 根据开始日期和结束日期，创建股票收盘价图图。分别用来创建最近10年的图和半年的图
     *
     * @param multithreading
     * @param beginDate      开始时间
     * @param endDate        结束时间
     * @param flag           创建最近10年图时，flag为true，图片名称中有all；创建半年图时，flag为false，图片名称中没有all
     */
    void paintClosePicture(boolean multithreading, String beginDate, String endDate, boolean flag);

    /**
     * 创建28分化图表
     *
     * @param multithreading
     */
    void createDifferentiationPicture(boolean multithreading);

    /**
     * 获取线图数据集DataSet
     *
     * @param multithreading
     * @return
     */
    DefaultCategoryDataset getLineDataSet(boolean multithreading);

    /**
     * 获取柱状图数据集DataSet
     *
     * @param multithreading
     * @return
     */
    DefaultCategoryDataset getBarDataSet(boolean multithreading);

    /**
     * 创建股票涨停跌停数量图（折线图）
     *
     * @param multithreading
     */
    void createStockLimitUpAndDownPicture(boolean multithreading);

    /**
     * 为创建股票涨跌停数量图（折线图）而获取数据
     *
     * @param multithreading
     * @return
     */
    TimeSeriesCollection getDatasetForStockLimitUpAndDownPicture(boolean multithreading);

    /**
     * 获取从开始日期至结束日期内MACD处于金叉状态（dif>dea）的股票的比率
     *
     * @param multithreading
     */
    void createMACDGoldCrossStockRatePicture(boolean multithreading);

    /**
     * 创建上证股票，深证股票，中小板股票和创业板股票日线级别平均涨跌幅度图
     *
     * @param multithreading
     */
    void createStockBigBoardUpDownPercentagePicture(boolean multithreading);

    /*********************************** 更新所有的数据 ********************************************/
    void calculateMovingAverage();

    void writeChangeRangeExRight();

    void writeUpDown();

    void writeMACD();

    void calculateUpDownPercentage();

    void calculateVolatility();

    void writeKD();

    void writeHa();

    void writeBoll();

    void writeBias();

    void writeVariance();

    void writeVolumeMa();

    void writeTurnoverMa();

    /**
     * 根据开始日期和结束日期，删除表stock_transaction_data_all中的记录
     */
    void deleteStockTransactionDataByDate();

    /*********************************** 实验 ********************************************/
    void test();


}
