???create or replace function scott.FNC_HA_MAX_VALUE(p_one   in number,
                                            p_two   in number,
                                            p_three in number)
    return number is
    result number;
    /*求三个数中的最大值*/
begin
    if p_one < p_two then
        result := p_two;
    else
        result := p_one;
    end if;
    if result < p_three then
        result := p_three;
    end if;

    return(result);
end FNC_HA_MAX_VALUE;
/

