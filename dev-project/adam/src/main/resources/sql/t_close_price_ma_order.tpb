???create or replace type body scott.T_CLOSE_PRICE_MA_ORDER is

  member function get_value return number is
  begin
    return value_;
  end get_value;

  member procedure set_value(p_value number) is
  begin
    value_ := p_value;
  end set_value;

  member function get_name return varchar2 is
  begin
    return name_;
  end get_name;

  member procedure set_name(p_name varchar2) is
  begin
    name_ := p_name;
  end set_name;

  member function get_label return number is
  begin
    return label;
  end get_label;

  member procedure set_label(p_label number) is
  begin
    label := p_label;
  end set_label;
end;
/

