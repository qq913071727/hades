???create or replace function scott.fnc_judge_by_kd_gold_cross(p_date       in varchar2,
                                                        p_stock_code in varchar2)
  return number is
  /** 判断某只股票在某一日，KD是否是金叉。如果是金叉，则返回1，如果不是金叉，则返回-1 **/
  result number;
  -- 当日的k
  v_current_k number;
  -- 当日的d
  v_current_d number;
  -- 前一日的k
  v_last_k number;
  -- 前一日的d
  v_last_d number;
begin
  begin
    -- 计算当日的k、d
    select t.k, t.d
      into v_current_k, v_current_d
      from stock_transaction_data_all t
     where t.code_ = p_stock_code
       and t.date_ = to_date(p_date, 'yyyy-mm-dd');

    -- 计算前一日的k、d
    select b.k, b.d
      into v_last_k, v_last_d
      from (select *
              from stock_transaction_data_all t
             where t.code_ = p_stock_code
               and t.date_ < to_date(p_date, 'yyyy-mm-dd')
             order by t.date_ desc) b
     where rownum <= 1;
  exception
    when no_data_found then
      DBMS_OUTPUT.put_line('代码为【' || p_stock_code || '】的股票，
            在【' || p_date || '】或者前一天没有dif和dea');
      result := -1;
      return(result);
  end;

  -- 判断是否是金叉
  if v_last_k<= v_last_d and v_current_k >= v_current_d then
    result := 1;
    return(result);
  else
    result := -1;
    return(result);
  end if;

end fnc_judge_by_kd_gold_cross;
/

