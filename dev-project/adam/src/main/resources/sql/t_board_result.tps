???create or replace type scott.T_BOARD_RESULT as object(
       board_date date,
       board_id number,
       stock_amount number,

       member function get_goard_date return date,
       member procedure set_board_date(param_board_date date),
       member function get_board_id return number,
       member procedure set_board_id(param_board_id number),
       member function get_stock_amount return number,
       member procedure set_stock_amount(param_stock_amount in number)
)
/

