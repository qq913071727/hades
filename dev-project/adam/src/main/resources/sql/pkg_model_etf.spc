???create or replace package scott.PKG_MODEL_ETF is

  /*-------------------- 海量地向表MDL_ETF_MACD_GOLD_CROSS中插入数据 ---------------------*/
  procedure CAL_MDL_ETF_MACD_GOLD_CROSS(p_validate_120_not_decreasing in number,
                                        p_validate_250_not_decreasing in number,
                                        p_default_begin_date          in varchar2,
                                        p_end_date                    in varchar2,
                                        p_type                        in number);

  /*-------------------- 海量地向表MDL_ETF_C_P_MA5_G_C中插入数据 -----------------------*/
  procedure CAL_MDL_ETF_CLOSE_PRICE_MA5_GC(p_validate_120_not_decreasing in number,
                                           p_validate_250_not_decreasing in number,
                                           p_default_begin_date          in varchar2,
                                           p_end_date                    in varchar2,
                                           p_type                        in number);

  /*-------------------- 海量地向表MDL_ETF_HEI_KIN_ASHI_DOWN_UP中插入数据 ---------------------*/
  procedure CAL_MDL_ETF_H_K_A_DOWN_UP(p_validate_120_not_decreasing in number,
                                      p_validate_250_not_decreasing in number,
                                      p_default_begin_date          in varchar2,
                                      p_end_date                    in varchar2,
                                      p_type                        in number);

  /*---------------------- 海量地向表MDL_ETF_KD_GOLD_CROSS中插入数据 -----------------------*/
  procedure CAL_MDL_ETF_KD_GOLD_CROSS(p_validate_120_not_decreasing in number,
                                      p_validate_250_not_decreasing in number,
                                      p_default_begin_date          in varchar2,
                                      p_end_date                    in varchar2,
                                      p_type                        in number);

  /*------------------- 海量地向表MDL_ETF_BULL_SHORT_LINE_UP中插入数据 --------------------*/
  procedure CAL_MDL_ETF_B_S_LINE_UP;

  /*------------------- 增量地向表MDL_ETF_BULL_SHORT_LINE_UP中插入数据 --------------------*/
  procedure CAL_MDL_ETF_B_S_LINE_UP_INCR(p_date in varchar2);
end PKG_MODEL_ETF;
/

