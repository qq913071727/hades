???create or replace package body scott.pkg_interval is

  /*--------------------------------------------------------------------------------------------------------------------*/
  /*---------------------------------- 死叉/金叉持续天数与之后的金叉/死叉获利的交易次数的关系 --------------------------*/
  /*--------------------------------------------------------------------------------------------------------------------*/
  /*----------------- macd死叉持续天数与之后的金叉获利的交易次数的关系 ----------------*/
  procedure find_macd_g_c_interval_t_n(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_macd_gold_cross类型的变量
    row_mdl_macd mdl_macd_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_gold_cross表和mdl_macd_dead_cross表的合并表中，查找code，并去重
    cursor cur_macd_distinct_code is
      select distinct mdl_macd.stock_code
        from (select t.*
                from mdl_macd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_macd_by_code is
      select *
        from (select t.*
                from mdl_macd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd
       order by mdl_macd.buy_date desc, mdl_macd.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_macd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_macd in cur_macd_by_code loop

        -- 如果当前记录是金叉交易，并且收益是正
        if row_mdl_macd.buy_date < row_mdl_macd.sell_date and
           row_mdl_macd.profit_loss > 0 then
          v_gold_cross := 1;
          v_dead_cross := -1;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_macd.buy_date > row_mdl_macd.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_macd_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_macd.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_macd_g_c_interval_t_n;

  /*----------------- macd金叉持续天数与之后的死叉获利的交易次数的关系 ----------------*/
  procedure find_macd_d_c_interval_t_n(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_macd_dead_cross类型的变量
    row_mdl_macd mdl_macd_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_dead_cross表和mdl_macd_gold_cross表的合并表中，查找code，并去重
    cursor cur_macd_distinct_code is
      select distinct mdl_macd.stock_code
        from (select t.*
                from mdl_macd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd;
    -- 在合并表中查找某一只股票，并按卖出时间降序排列
    cursor cur_macd_by_code is
      select *
        from (select t.*
                from mdl_macd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd
       order by mdl_macd.sell_date desc, mdl_macd.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_macd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_macd in cur_macd_by_code loop

        -- 如果当前记录是死叉交易，并且收益是正
        if row_mdl_macd.sell_date < row_mdl_macd.buy_date and
           row_mdl_macd.profit_loss > 0 then
          v_gold_cross := -1;
          v_dead_cross := 1;
        end if;

        -- 如果当前记录是金叉交易
        if v_dead_cross = 1 and v_gold_cross = -1 and
           row_mdl_macd.sell_date > row_mdl_macd.buy_date then
          v_gold_cross := 1;
          v_dead_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_macd_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_macd.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_macd_d_c_interval_t_n;

  /*----------- close_price死叉ma5持续天数与之后的金叉获利的交易次数的关系 ------------*/
  procedure find_c_p_ma5_g_c_interval_t_n(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_macd_gold_cross类型的变量
    row_mdl_c_p_ma5 mdl_close_price_ma5_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_close_price_ma5_gold_cross表和mdl_close_price_ma5_dead_cross表的合并表中，查找code，并去重
    cursor cur_c_p_ma5_distinct_code is
      select distinct mdl_c_p_ma5.stock_code
        from (select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_c_p_ma5_by_code is
      select *
        from (select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5
       order by mdl_c_p_ma5.buy_date desc, mdl_c_p_ma5.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_c_p_ma5_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_c_p_ma5 in cur_c_p_ma5_by_code loop

        -- 如果当前记录是金叉交易，并且收益是正
        if row_mdl_c_p_ma5.buy_date < row_mdl_c_p_ma5.sell_date and
           row_mdl_c_p_ma5.profit_loss > 0 then
          v_gold_cross := 1;
          v_dead_cross := -1;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_c_p_ma5.buy_date > row_mdl_c_p_ma5.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_close_price_ma5_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_c_p_ma5.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_c_p_ma5_g_c_interval_t_n;

  /*----------- close_price金叉ma5持续天数与之后的死叉获利的交易次数的关系 ------------*/
  procedure find_c_p_ma5_d_c_interval_t_n(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_close_price_ma5_dead_cross类型的变量
    row_mdl_c_p_ma5 mdl_close_price_ma5_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_dead_cross表和mdl_macd_gold_cross表的合并表中，查找code，并去重
    cursor cur_c_p_ma5_distinct_code is
      select distinct mdl_c_p_ma5.stock_code
        from (select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5;
    -- 在合并表中查找某一只股票，并按卖出时间降序排列
    cursor cur_c_p_ma5_by_code is
      select *
        from (select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5
       order by mdl_c_p_ma5.sell_date desc, mdl_c_p_ma5.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_c_p_ma5_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_c_p_ma5 in cur_c_p_ma5_by_code loop

        -- 如果当前记录是死叉交易，并且收益是正
        if row_mdl_c_p_ma5.sell_date < row_mdl_c_p_ma5.buy_date and
           row_mdl_c_p_ma5.profit_loss > 0 then
          v_gold_cross := -1;
          v_dead_cross := 1;
        end if;

        -- 如果当前记录是金叉交易
        if v_dead_cross = 1 and v_gold_cross = -1 and
           row_mdl_c_p_ma5.sell_date > row_mdl_c_p_ma5.buy_date then
          v_gold_cross := 1;
          v_dead_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_close_price_ma5_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_c_p_ma5.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_c_p_ma5_d_c_interval_t_n;

  /*--------- hei_kin_ashi下跌趋势持续天数与之后的上涨趋势获利的交易次数的关系  -------*/
  procedure find_h_k_a_u_d_interval_t_n(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了上升趋势。1表示出现了上升趋势
    v_up_down number := -1;
    -- 表示是否出现了下跌趋势。1表示出现了下跌趋势
    v_down_up number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_hei_kin_ashi_up_down类型的变量
    row_mdl_h_k_a_up_down mdl_hei_kin_ashi_up_down%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_hei_kin_ashi_up_down表和mdl_hei_kin_ashi_down_up表的合并表中，查找code，并去重
    cursor cur_hka_up_down_distinct_code is
      select distinct mdl_h_k_a_up_down.stock_code
        from (select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_up_down;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_hka_up_down_by_code is
      select *
        from (select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_up_down
       order by mdl_h_k_a_up_down.buy_date  desc,
                mdl_h_k_a_up_down.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_hka_up_down_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_h_k_a_up_down in cur_hka_up_down_by_code loop

        -- 如果当前记录是上升趋势交易，并且收益是正
        if row_mdl_h_k_a_up_down.buy_date < row_mdl_h_k_a_up_down.sell_date and
           row_mdl_h_k_a_up_down.profit_loss > 0 then
          v_up_down := 1;
          v_down_up := -1;
        end if;

        -- 如果当前记录是下跌趋势交易
        if v_up_down = 1 and v_down_up = -1 and
           row_mdl_h_k_a_up_down.buy_date > row_mdl_h_k_a_up_down.sell_date then
          v_down_up := 1;
          v_up_down := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_hei_kin_ashi_down_up t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_h_k_a_up_down.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_h_k_a_u_d_interval_t_n;

  /*--------- hei_kin_ashi上涨趋势持续天数与之后的下跌趋势获利的交易次数的关系  -------*/
  procedure find_h_k_a_d_u_interval_t_n(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了上升趋势。1表示出现了上升趋势
    v_up_down number := -1;
    -- 表示是否出现了下跌趋势。1表示出现了下跌趋势
    v_down_up number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_hei_kin_ashi_down_up类型的变量
    row_mdl_h_k_a_down_up mdl_hei_kin_ashi_down_up%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_hei_kin_ashi_down_up表和mdl_hei_kin_ashi_up_down表的合并表中，查找code，并去重
    cursor cur_hka_down_up_distinct_code is
      select distinct mdl_h_k_a_down_up.stock_code
        from (select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_down_up;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_hka_down_up_by_code is
      select *
        from (select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_down_up
       order by mdl_h_k_a_down_up.sell_date desc,
                mdl_h_k_a_down_up.buy_date  desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_hka_down_up_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_h_k_a_down_up in cur_hka_down_up_by_code loop

        -- 如果当前记录是下跌趋势交易，并且收益是正
        if row_mdl_h_k_a_down_up.sell_date < row_mdl_h_k_a_down_up.buy_date and
           row_mdl_h_k_a_down_up.profit_loss > 0 then
          v_down_up := 1;
          v_up_down := -1;
        end if;

        -- 如果当前记录是下跌趋势交易
        if v_down_up = 1 and v_up_down = -1 and
           row_mdl_h_k_a_down_up.sell_date > row_mdl_h_k_a_down_up.buy_date then
          v_down_up := -1;
          v_up_down := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_hei_kin_ashi_up_down t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_h_k_a_down_up.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_h_k_a_d_u_interval_t_n;

  /*------------------- kd死叉持续天数与之后的金叉获利的交易次数的关系 ------------------*/
  procedure find_kd_g_c_interval_t_n(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_kd_gold_cross类型的变量
    row_mdl_kd mdl_kd_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_kd_gold_cross表和mdl_kd_dead_cross表的合并表中，查找code，并去重
    cursor cur_kd_distinct_code is
      select distinct mdl_kd.stock_code
        from (select t.*
                from mdl_kd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_kd_by_code is
      select *
        from (select t.*
                from mdl_kd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd
       order by mdl_kd.buy_date desc, mdl_kd.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_kd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_kd in cur_kd_by_code loop

        -- 如果当前记录是金叉交易，并且收益是正
        if row_mdl_kd.buy_date < row_mdl_kd.sell_date and
           row_mdl_kd.profit_loss > 0 then
          v_gold_cross := 1;
          v_dead_cross := -1;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_kd.buy_date > row_mdl_kd.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_kd_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_kd.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_kd_g_c_interval_t_n;

  /*--------- kd死叉获利的交易之前的那个金叉持续了多久，按照持续天数做分组统计 --------*/
  procedure find_kd_d_c_interval_t_n(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 表mdl_kd_dead_cross类型的变量
    row_mdl_kd mdl_kd_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_t_n(interval_date, amount) values(:1, :2)';
    v_sql_group_by varchar2(256) := 'select t1.interval_date, count(*) from interval_g_c_d_c_t_n t1 group by t1.interval_date having count(*)>=:1 order by t1.interval_date asc';
    -- 在mdl_kd_gold_cross表和mdl_kd_dead_cross表的合并表中，查找code，并去重
    cursor cur_kd_distinct_code is
      select distinct mdl_kd.stock_code
        from (select t.*
                from mdl_kd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_kd_by_code is
      select *
        from (select t.*
                from mdl_kd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd
       order by mdl_kd.sell_date desc, mdl_kd.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_t_n purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_t_n不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_t_n(
                                       -- 持续n天
                                       interval_date number,
                                       -- 有多少股票持续了n天
                                       amount number)';

    for i in cur_kd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_kd in cur_kd_by_code loop

        -- 如果当前记录是死叉交易，并且收益是正
        if row_mdl_kd.sell_date < row_mdl_kd.buy_date and
           row_mdl_kd.profit_loss > 0 then
          v_gold_cross := -1;
          v_dead_cross := 1;
        end if;

        -- 如果当前记录是金叉交易
        if v_gold_cross = -1 and v_dead_cross = 1 and
           row_mdl_kd.sell_date > row_mdl_kd.buy_date then
          v_dead_cross := -1;
          v_gold_cross := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_kd_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_kd.id;

          execute immediate v_sql_insert
            using v_interval_date, 1;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_kd_d_c_interval_t_n;

  /*--------------------------------------------------------------------------------------------------------------------*/
  /*----------------------- 金叉/死叉获利的交易之前的那个死叉/死叉的持续天数与这次死叉/金叉的成功率的关系 --------------*/
  /*--------------------------------------------------------------------------------------------------------------------*/
  /*--------- macd金叉获利的交易之前的那个macd死叉的持续天数与这次金叉的成功率的关系 --------*/
  procedure find_macd_g_c_interval_s_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_macd_gold_cross类型的变量
    row_mdl_macd mdl_macd_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_gold_cross表和mdl_macd_dead_cross表的合并表中，查找code，并去重
    cursor cur_macd_distinct_code is
      select distinct mdl_macd.stock_code
        from (select t.*
                from mdl_macd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_macd_by_code is
      select *
        from (select t.*
                from mdl_macd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd
       order by mdl_macd.buy_date desc, mdl_macd.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_macd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_macd in cur_macd_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_macd.buy_date < row_mdl_macd.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_macd.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_macd.buy_date > row_mdl_macd.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_macd_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_macd.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_macd_g_c_interval_s_r;

  /*--------- macd死叉获利的交易之前的那个macd金叉的持续天数与这次死叉的成功率的关系 --------*/
  procedure find_macd_d_c_interval_s_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_macd_dead_cross类型的变量
    row_mdl_macd mdl_macd_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_dead_cross表和mdl_macd_gold_cross表的合并表中，查找code，并去重
    cursor cur_macd_distinct_code is
      select distinct mdl_macd.stock_code
        from (select t.*
                from mdl_macd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd;
    -- 在合并表中查找某一只股票，并按卖出时间降序排列
    cursor cur_macd_by_code is
      select *
        from (select t.*
                from mdl_macd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd
       order by mdl_macd.sell_date desc, mdl_macd.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_macd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_macd in cur_macd_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_macd.sell_date < row_mdl_macd.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_macd.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_dead_cross = 1 and v_gold_cross = -1 and
           row_mdl_macd.sell_date > row_mdl_macd.buy_date then
          v_gold_cross := 1;
          v_dead_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_macd_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_macd.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_macd_d_c_interval_s_r;

  /*--------- close_price金叉ma5获利的交易之前的那个close_price死叉ma5的持续天数与这次金叉的成功率的关系 --------*/
  procedure find_c_p_ma5_g_c_interval_s_r(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_macd_gold_cross类型的变量
    row_mdl_c_p_ma5 mdl_close_price_ma5_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_close_price_ma5_gold_cross表和mdl_close_price_ma5_dead_cross表的合并表中，查找code，并去重
    cursor cur_c_p_ma5_distinct_code is
      select distinct mdl_c_p_ma5.stock_code
        from (select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_c_p_ma5_by_code is
      select *
        from (select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5
       order by mdl_c_p_ma5.buy_date desc, mdl_c_p_ma5.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_c_p_ma5_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_c_p_ma5 in cur_c_p_ma5_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_c_p_ma5.buy_date < row_mdl_c_p_ma5.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_c_p_ma5.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_c_p_ma5.buy_date > row_mdl_c_p_ma5.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_close_price_ma5_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_c_p_ma5.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_c_p_ma5_g_c_interval_s_r;

  /*--------- close_price死叉ma5获利的交易之前的那个close_price金叉ma5的持续天数与这次死叉的成功率的关系 --------*/
  procedure find_c_p_ma5_d_c_interval_s_r(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_close_price_ma5_dead_cross类型的变量
    row_mdl_c_p_ma5 mdl_close_price_ma5_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_dead_cross表和mdl_macd_gold_cross表的合并表中，查找code，并去重
    cursor cur_c_p_ma5_distinct_code is
      select distinct mdl_c_p_ma5.stock_code
        from (select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5;
    -- 在合并表中查找某一只股票，并按卖出时间降序排列
    cursor cur_c_p_ma5_by_code is
      select *
        from (select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5
       order by mdl_c_p_ma5.sell_date desc, mdl_c_p_ma5.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_c_p_ma5_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_c_p_ma5 in cur_c_p_ma5_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_c_p_ma5.sell_date < row_mdl_c_p_ma5.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_c_p_ma5.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_dead_cross = 1 and v_gold_cross = -1 and
           row_mdl_c_p_ma5.sell_date > row_mdl_c_p_ma5.buy_date then
          v_gold_cross := 1;
          v_dead_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_close_price_ma5_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_c_p_ma5.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_c_p_ma5_d_c_interval_s_r;

  /*--------- hei_kin_ashi上升趋势获利的交易之前的那个hei_kin_ashi下跌趋势的持续天数与这次上升趋势的成功率的关系 --------*/
  procedure find_h_k_a_u_d_interval_s_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了上升趋势。1表示出现了上升趋势
    v_up_down number := -1;
    -- 表示是否出现了下跌趋势。1表示出现了下跌趋势
    v_down_up number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_hei_kin_ashi_up_down类型的变量
    row_mdl_h_k_a_up_down mdl_hei_kin_ashi_up_down%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_hei_kin_ashi_up_down表和mdl_hei_kin_ashi_down_up表的合并表中，查找code，并去重
    cursor cur_hka_up_down_distinct_code is
      select distinct mdl_h_k_a_up_down.stock_code
        from (select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_up_down;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_hka_up_down_by_code is
      select *
        from (select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_up_down
       order by mdl_h_k_a_up_down.buy_date  desc,
                mdl_h_k_a_up_down.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_hka_up_down_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_h_k_a_up_down in cur_hka_up_down_by_code loop

        -- 如果当前记录是上升趋势交易
        if row_mdl_h_k_a_up_down.buy_date < row_mdl_h_k_a_up_down.sell_date then
          v_up_down         := 1;
          v_down_up         := -1;
          v_previous_profit := row_mdl_h_k_a_up_down.profit_loss;
        end if;

        -- 如果当前记录是下跌趋势交易
        if v_up_down = 1 and v_down_up = -1 and
           row_mdl_h_k_a_up_down.buy_date > row_mdl_h_k_a_up_down.sell_date then
          v_down_up := 1;
          v_up_down := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_hei_kin_ashi_down_up t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_h_k_a_up_down.id;

          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_h_k_a_u_d_interval_s_r;

  /*--------- hei_kin_ashi下跌趋势获利的交易之前的那个hei_kin_ashi上升趋势的持续天数与这次下跌趋势的成功率的关系 --------*/
  procedure find_h_k_a_d_u_interval_s_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了上升趋势。1表示出现了上升趋势
    v_up_down number := -1;
    -- 表示是否出现了下跌趋势。1表示出现了下跌趋势
    v_down_up number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_hei_kin_ashi_down_up类型的变量
    row_mdl_h_k_a_down_up mdl_hei_kin_ashi_down_up%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_hei_kin_ashi_down_up表和mdl_hei_kin_ashi_up_down表的合并表中，查找code，并去重
    cursor cur_hka_down_up_distinct_code is
      select distinct mdl_h_k_a_down_up.stock_code
        from (select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_down_up;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_hka_down_up_by_code is
      select *
        from (select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_h_k_a_down_up
       order by mdl_h_k_a_down_up.sell_date desc,
                mdl_h_k_a_down_up.buy_date  desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_hka_down_up_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_h_k_a_down_up in cur_hka_down_up_by_code loop

        -- 如果当前记录是下跌趋势交易
        if row_mdl_h_k_a_down_up.sell_date < row_mdl_h_k_a_down_up.buy_date then
          v_down_up         := 1;
          v_up_down         := -1;
          v_previous_profit := row_mdl_h_k_a_down_up.profit_loss;
        end if;

        -- 如果当前记录是上涨趋势交易
        if v_down_up = 1 and v_up_down = -1 and
           row_mdl_h_k_a_down_up.sell_date > row_mdl_h_k_a_down_up.buy_date then
          v_down_up := -1;
          v_up_down := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_hei_kin_ashi_up_down t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_h_k_a_down_up.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_h_k_a_d_u_interval_s_r;

  /*--------- kd金叉获利的交易之前的那个kd死叉的持续天数与这次金叉的成功率的关系 --------*/
  procedure find_kd_g_c_interval_s_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_kd_gold_cross类型的变量
    row_mdl_kd mdl_kd_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_kd_gold_cross表和mdl_kd_dead_cross表的合并表中，查找code，并去重
    cursor cur_kd_distinct_code is
      select distinct mdl_kd.stock_code
        from (select t.*
                from mdl_kd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_kd_by_code is
      select *
        from (select t.*
                from mdl_kd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd
       order by mdl_kd.buy_date desc, mdl_kd.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_kd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_kd in cur_kd_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_kd.buy_date < row_mdl_kd.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_kd.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_kd.buy_date > row_mdl_kd.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_kd_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_kd.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_kd_g_c_interval_s_r;

  /*--------- kd死叉获利的交易之前的那个kd金叉的持续天数与这次死叉的成功率的关系 --------*/
  procedure find_kd_d_c_interval_s_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_kd_dead_cross类型的变量
    row_mdl_kd mdl_kd_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_s_r(interval_date, profit_or_loss) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select count(*) from interval_g_c_d_c_s_r t2 where t2.profit_or_loss=1 and t2.interval_date=t1.interval_date)/count(*)*100 from interval_g_c_d_c_s_r t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_s_r t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_kd_dead_cross表和mdl_kd_gold_cross表的合并表中，查找code，并去重
    cursor cur_kd_distinct_code is
      select distinct mdl_kd.stock_code
        from (select t.*
                from mdl_kd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd;
    -- 在合并表中查找某一只股票，并按卖出时间降序排列
    cursor cur_kd_by_code is
      select *
        from (select t.*
                from mdl_kd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd
       order by mdl_kd.sell_date desc, mdl_kd.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_s_r purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_s_r不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_s_r(
                                       -- 持续n天
                                       interval_date number,
                                       -- 1表示这次交易是赚钱的，并且持续了n天；-1表示这次交易是赔钱的，并且持续了n天
                                       profit_or_loss number)';

    for i in cur_kd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_kd in cur_kd_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_kd.sell_date < row_mdl_kd.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_kd.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_dead_cross = 1 and v_gold_cross = -1 and
           row_mdl_kd.sell_date > row_mdl_kd.buy_date then
          v_gold_cross := 1;
          v_dead_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_kd_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_kd.id;

          -- 插入记录
          if v_previous_profit > 0 then
            execute immediate v_sql_insert
              using v_interval_date, 1;
          end if;
          if v_previous_profit < 0 then
            execute immediate v_sql_insert
              using v_interval_date, -1;
          end if;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_kd_d_c_interval_s_r;

  /*--------------------------------------------------------------------------------------------------------------------*/
  /*------------------------------------ 金叉/死叉之前的死叉持续天数与金叉/死叉平均收益率的关系 --------------------------*/
  /*--------------------------------------------------------------------------------------------------------------------*/
  /*------------------ macd金叉之前的死叉持续天数与金叉平均收益率的关系 ----------------*/
  procedure find_macd_g_c_interval_p_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_macd_gold_cross类型的变量
    row_mdl_macd mdl_macd_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_gold_cross表和mdl_macd_dead_cross表的合并表中，查找code，并去重
    cursor cur_macd_distinct_code is
      select distinct mdl_macd.stock_code
        from (select t.*
                from mdl_macd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_macd_by_code is
      select *
        from (select t.*
                from mdl_macd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd
       order by mdl_macd.buy_date desc, mdl_macd.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_macd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_macd in cur_macd_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_macd.buy_date < row_mdl_macd.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_macd.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_macd.buy_date > row_mdl_macd.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_macd_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_macd.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_macd_g_c_interval_p_r;

  /*----------------- macd死叉之前的金叉持续天数与死叉平均收益率的关系 ----------------*/
  procedure find_macd_d_c_interval_p_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_macd_dead_cross类型的变量
    row_mdl_macd mdl_macd_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_macd_gold_cross表和mdl_macd_dead_cross表的合并表中，查找code，并去重
    cursor cur_macd_distinct_code is
      select distinct mdl_macd.stock_code
        from (select t.*
                from mdl_macd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_macd_by_code is
      select *
        from (select t.*
                from mdl_macd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_macd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_macd
       order by mdl_macd.sell_date desc, mdl_macd.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_macd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_macd in cur_macd_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_macd.sell_date < row_mdl_macd.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_macd.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_gold_cross = -1 and v_dead_cross = 1 and
           row_mdl_macd.sell_date > row_mdl_macd.buy_date then
          v_dead_cross := -1;
          v_gold_cross := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_macd_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_macd.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_macd_d_c_interval_p_r;

  /*------------------ close_price金叉ma5之前的死叉持续天数与金叉平均收益率的关系 -----------------*/
  procedure find_cp_ma5_gc_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_close_price_ma5_gold_cross类型的变量
    row_mdl_c_p_ma5 mdl_close_price_ma5_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_close_price_ma5_gold_cross表和mdl_close_price_ma5_dead_cross表的合并表中，查找code，并去重
    cursor cur_c_p_ma5_distinct_code is
      select distinct mdl_c_p_ma5.stock_code
        from (select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_c_p_ma5_by_code is
      select *
        from (select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5
       order by mdl_c_p_ma5.buy_date desc, mdl_c_p_ma5.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_c_p_ma5_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_c_p_ma5 in cur_c_p_ma5_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_c_p_ma5.buy_date < row_mdl_c_p_ma5.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_c_p_ma5.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_c_p_ma5.buy_date > row_mdl_c_p_ma5.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_close_price_ma5_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_c_p_ma5.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_cp_ma5_gc_interval_p_r;

  /*------------------ close_price死叉ma5之前的金叉持续天数与死叉平均收益率的关系 -----------------*/
  procedure find_cp_ma5_dc_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_close_price_ma5_dead_cross类型的变量
    row_mdl_c_p_ma5 mdl_close_price_ma5_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_close_price_ma5_dead_cross表和mdl_close_price_ma5_gold_cross表的合并表中，查找code，并去重
    cursor cur_c_p_ma5_distinct_code is
      select distinct mdl_c_p_ma5.stock_code
        from (select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_c_p_ma5_by_code is
      select *
        from (select t.*
                from mdl_close_price_ma5_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_close_price_ma5_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_c_p_ma5
       order by mdl_c_p_ma5.sell_date desc, mdl_c_p_ma5.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_c_p_ma5_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_c_p_ma5 in cur_c_p_ma5_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_c_p_ma5.sell_date < row_mdl_c_p_ma5.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_c_p_ma5.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_gold_cross = -1 and v_dead_cross = 1 and
           row_mdl_c_p_ma5.sell_date > row_mdl_c_p_ma5.buy_date then
          v_dead_cross := -1;
          v_gold_cross := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_close_price_ma5_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_c_p_ma5.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_cp_ma5_dc_interval_p_r;

  /*------------------ hei_kin_ashi上升趋势之前的下跌趋势持续天数与上升趋势平均收益率的关系 ----------------*/
  procedure find_h_k_a_u_d_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_hei_kin_ashi_up_down类型的变量
    row_mdl_hka_up_down mdl_hei_kin_ashi_up_down%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_hei_kin_ashi_up_down表和mdl_hei_kin_ashi_down_up表的合并表中，查找code，并去重
    cursor cur_hka_up_down_distinct_code is
      select distinct mdl_hka_up_down.stock_code
        from (select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_hka_up_down;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_hka_up_down_by_code is
      select *
        from (select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_hka_up_down
       order by mdl_hka_up_down.buy_date  desc,
                mdl_hka_up_down.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_hka_up_down_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_hka_up_down in cur_hka_up_down_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_hka_up_down.buy_date < row_mdl_hka_up_down.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_hka_up_down.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_hka_up_down.buy_date > row_mdl_hka_up_down.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_hei_kin_ashi_down_up t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_hka_up_down.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_h_k_a_u_d_interval_p_r;

  /*------------------ hei_kin_ashi下跌趋势之前的上升趋势持续天数与下跌趋势平均收益率的关系 ----------------*/
  procedure find_h_k_a_d_u_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_hei_kin_ashi_down_up类型的变量
    row_mdl_hka_down_up mdl_hei_kin_ashi_down_up%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_hei_kin_ashi_up_down表和mdl_hei_kin_ashi_down_up表的合并表中，查找code，并去重
    cursor cur_hka_down_up_distinct_code is
      select distinct mdl_hka_down_up.stock_code
        from (select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_hka_down_up;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_hka_down_up_by_code is
      select *
        from (select t.*
                from mdl_hei_kin_ashi_down_up t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_hei_kin_ashi_up_down t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_hka_down_up
       order by mdl_hka_down_up.sell_date desc,
                mdl_hka_down_up.buy_date  desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_hka_down_up_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_hka_down_up in cur_hka_down_up_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_hka_down_up.sell_date < row_mdl_hka_down_up.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_hka_down_up.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_gold_cross = -1 and v_dead_cross = 1 and
           row_mdl_hka_down_up.sell_date > row_mdl_hka_down_up.buy_date then
          v_dead_cross := -1;
          v_gold_cross := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_hei_kin_ashi_up_down t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_hka_down_up.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_h_k_a_d_u_interval_p_r;

  /*------------------ kd金叉之前的死叉持续天数与金叉平均收益率的关系 ----------------*/
  procedure find_kd_g_c_interval_p_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_kd_gold_cross类型的变量
    row_mdl_kd mdl_kd_gold_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_kd_gold_cross表和mdl_kd_dead_cross表的合并表中，查找code，并去重
    cursor cur_kd_distinct_code is
      select distinct mdl_kd.stock_code
        from (select t.*
                from mdl_kd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_kd_by_code is
      select *
        from (select t.*
                from mdl_kd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd
       order by mdl_kd.buy_date desc, mdl_kd.sell_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_kd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_kd in cur_kd_by_code loop

        -- 如果当前记录是金叉交易
        if row_mdl_kd.buy_date < row_mdl_kd.sell_date then
          v_gold_cross      := 1;
          v_dead_cross      := -1;
          v_previous_profit := row_mdl_kd.profit_loss;
        end if;

        -- 如果当前记录是死叉交易
        if v_gold_cross = 1 and v_dead_cross = -1 and
           row_mdl_kd.buy_date > row_mdl_kd.sell_date then
          v_dead_cross := 1;
          v_gold_cross := -1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_kd_dead_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.sell_date
             and si_.date_ <= t.buy_date
           where t.id = row_mdl_kd.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_kd_g_c_interval_p_r;

  /*----------------- kd死叉之前的金叉持续天数与死叉平均收益率的关系 ----------------*/
  procedure find_kd_d_c_interval_p_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor) is
    -- 股票代码
    v_code varchar2(24);
    -- 表示是否出现了金叉。1表示出现了金叉
    v_gold_cross number := -1;
    -- 表示是否出现了死叉。1表示出现了死叉
    v_dead_cross number := -1;
    -- 间隔了多少天
    v_interval_date number;
    -- 上一次操作的收益率
    v_previous_profit number;
    -- 表mdl_kd_dead_cross类型的变量
    row_mdl_kd mdl_kd_dead_cross%rowtype;
    -- sql语句
    v_sql_insert   varchar2(256) := 'insert into interval_g_c_d_c_profit(interval_date, profit) values(:1, :2)';
    v_sql_group_by varchar2(512) := 'select t1.interval_date, (select avg(t2.profit) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date) from interval_g_c_d_c_profit t1 group by t1.interval_date having (select count(*) from interval_g_c_d_c_profit t2 where t2.interval_date=t1.interval_date)>=:1 order by t1.interval_date asc';
    -- 在mdl_kd_gold_cross表和mdl_kd_dead_cross表的合并表中，查找code，并去重
    cursor cur_kd_distinct_code is
      select distinct mdl_kd.stock_code
        from (select t.*
                from mdl_kd_dead_cross t
               where t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_gold_cross t
               where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd;
    -- 在合并表中查找某一只股票，并按买入时间降序排列
    cursor cur_kd_by_code is
      select *
        from (select t.*
                from mdl_kd_dead_cross t
               where t.stock_code = v_code
                 and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.buy_date <= to_date(p_end_date, 'yyyy-mm-dd')
              union all
              select t.*
                from mdl_kd_gold_cross t
               where t.stock_code = v_code
                 and t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd')
                 and t.sell_date <= to_date(p_end_date, 'yyyy-mm-dd')) mdl_kd
       order by mdl_kd.sell_date desc, mdl_kd.buy_date desc;
  begin
    -- 删除临时表
    begin
      execute immediate 'drop table interval_g_c_d_c_profit purge';
    exception
      when others then
        pkg_tool.DEBUG('表interval_g_c_d_c_profit不存在');
    end;

    -- 创建临时表
    execute immediate 'create table interval_g_c_d_c_profit(
                                       -- 之前的死叉持续的天数
                                       interval_date number,
                                       -- 现在的金叉的收益
                                       profit number)';

    for i in cur_kd_distinct_code loop
      v_code := i.stock_code;
      for row_mdl_kd in cur_kd_by_code loop

        -- 如果当前记录是死叉交易
        if row_mdl_kd.sell_date < row_mdl_kd.buy_date then
          v_gold_cross      := -1;
          v_dead_cross      := 1;
          v_previous_profit := row_mdl_kd.profit_loss;
        end if;

        -- 如果当前记录是金叉交易
        if v_gold_cross = -1 and v_dead_cross = 1 and
           row_mdl_kd.sell_date > row_mdl_kd.buy_date then
          v_dead_cross := -1;
          v_gold_cross := 1;
          -- 计算间隔了多少天
          select count(si_.id_)
            into v_interval_date
            from mdl_kd_gold_cross t
            join stock_index si_
              on si_.code_ = '000001'
             and si_.date_ >= t.buy_date
             and si_.date_ <= t.sell_date
           where t.id = row_mdl_kd.id;

          -- 插入记录
          execute immediate v_sql_insert
            using v_interval_date, v_previous_profit;
        end if;
      end loop;
    end loop;
    commit;

    -- 分组统计
    open p_g_c_d_c_interval_cursor for v_sql_group_by
      using p_date_num;
  end find_kd_d_c_interval_p_r;

end pkg_interval;
/

