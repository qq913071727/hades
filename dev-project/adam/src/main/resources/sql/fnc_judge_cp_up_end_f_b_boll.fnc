???create or replace function scott.fnc_judge_cp_up_end_f_b_boll(p_date       in varchar2,
                                                       p_stock_code in varchar2)
  return number is
  /* fuc_judge_close_price_up_end_from_bottom_boll。
  判断是否满足条件：如果前一交易日收盘价大于布林带下轨，并且当前交易日收盘价小于前一交易日收盘价。如果是，则返回1，如果不是，则返回-1 */
  result number;
  -- 当前交易记录的收盘价
  v_current_close_price number;
  -- 当前交易记录的布林带下轨
  v_current_dn number;
  -- 前一个交易记录的收盘价
  v_last_close_price number;
  -- 前一个交易记录的布林带下轨
  v_last_dn number;
begin
  begin
    -- 当前交易记录的收盘价、布林带下轨
    select t.close_price, t.dn_
      into v_current_close_price, v_current_dn
      from stock_transaction_data_all t
     where t.code_ = p_stock_code
       and t.date_ = to_date(p_date, 'yyyy-mm-dd');

    -- 前一个交易记录的收盘价
    select close_price, dn_
      into v_last_close_price, v_last_dn
      from (select *
              from stock_transaction_data_all t
             where t.code_ = p_stock_code
               and t.date_ < to_date(p_date, 'yyyy-mm-dd')
             order by t.date_ desc)
     where rownum <= 1;
  exception
    when no_data_found then
      DBMS_OUTPUT.put_line('代码为【' || p_stock_code || '】的股票，
            在【' || p_date || '】或者前一天没有记录');
      result := -1;
      return(result);
  end;

  -- 判断是否满足条件：如果前一交易日收盘价大于布林带下轨，并且当前交易日收盘价小于前一交易日收盘价。如果是，则返回1，如果不是，则返回-1
  if v_last_close_price > v_last_dn and
     v_current_close_price < v_last_close_price then
    result := 1;
    return(result);
  else
    result := -1;
    return(result);
  end if;

end fnc_judge_cp_up_end_f_b_boll;
/

