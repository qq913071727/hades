???create or replace package body scott.PKG_BOARD_INDEX is

/*------------------------------------------------ calculate the columns of table board_index ----------------------------------------------*/
procedure CALCULATE_BOARD_INDEX as
begin
     declare
     -- 表示stock_transaction_data表中的date_字段
     v_date date;
     -- 表示stock_code表中的board_id字段
     v_board_id number;
     -- 用来记录上涨家数
     v_up_number number;
     -- 用来记录下跌家数
     v_down_number number;
     -- 表示股票的家数
     v_stock_number number;
     -- 表示板块指数的收盘价
     v_close_price number;
     -- 表示板块指数的开盘价
     v_open_price number;
     -- 表示板块指数的最高价
     v_highest_price number;
     -- 表示板块指数的最低价
     v_lowest_price number;
     -- 表示板块指数的成交量
     v_volume number;
     -- 表示某个板块第一天的board_close
     v_init_close_price number;
     -- 表示前一天的board_close
     v_pre_close_price number;
     -- stock_moving_average表中的所有日期，按升序排列
     cursor cur_all_stock_date is select distinct t.date_ from stock_transaction_data t order by t.date_ asc;
     -- 查找stock_moving_average表中某一天的所有版块
     cursor cur_all_board_by_date is select distinct t2.board_id from stock_transaction_data t1,stock_info t2
            where t1.date_=v_date and t1.code_=lower(t2.code_);
     -- 查找stock_moving_average表中某一天，某一个版块的股票信息
     cursor cur_stock_by_date_and_board is select t1.date_,t1.code_,t1.close_price,t1.open_price,t1.highest_price,t1.lowest_price,t2.board_id,t1.volume
            from stock_transaction_data t1,stock_info t2 where t1.date_=v_date and t1.code_=lower(t2.code_) and t2.board_id=v_board_id;
     -- 返回全部board_id
     cursor cur_all_board is select distinct t.board_id from board_index t order by t.board_id asc;
      -- 返回某个板块所有交易日的收盘价
     cursor cur_single_board_close_price is select * from board_index t where t.board_id=v_board_id order by t.date_ asc;
     begin
          -- 计算board_date,board_id,up_down_rate,stock_number,up_amount,down_amount,board_close,board_amount
          for i in cur_all_stock_date loop
              v_date:=i.date_;
              for j in cur_all_board_by_date loop
                  v_board_id:=j.board_id;
                  -- 清零，用于重新计算某一天，某个板块的上涨家数和下跌家数
                  v_up_number:=0;
                  v_down_number:=0;
                  v_stock_number:=0;
                  v_close_price:=0;
                  v_open_price:=0;
                  v_highest_price:=0;
                  v_lowest_price:=0;
                  v_volume:=0;
                  for k in cur_stock_by_date_and_board loop
                      if k.close_price>k.open_price then
                          v_up_number:=v_up_number+1;
                      end if;
                      if k.close_price<k.open_price then
                          v_down_number:=v_down_number+1;
                      end if;
                      v_stock_number:=v_stock_number+1;
                      v_close_price:=v_close_price+k.close_price;
                      v_open_price:=v_open_price+k.open_price;
                      v_highest_price:=v_highest_price+k.highest_price;
                      v_lowest_price:=v_lowest_price+k.lowest_price;
                      v_volume:=v_volume+k.volume;
                  end loop;
                  insert into board_index(date_,board_id,up_down_rate,stock_number,up_amount,down_amount,close_price,open_price,highest_price,lowest_price,amount)
                  values(v_date,v_board_id,decode(v_down_number,0,v_up_number,round(v_up_number/v_down_number,2)),v_stock_number,v_up_number,v_down_number,round(v_close_price/v_stock_number,2),round(v_open_price/v_stock_number,2),round(v_highest_price/v_stock_number,2),round(v_lowest_price/v_stock_number,2),round(v_volume/v_stock_number,2));
                  commit;
              end loop;
          end loop;

          -- 计算up_down
          for i in cur_all_board loop
              v_board_id:=i.board_id;
              select t.board_id into v_init_close_price from board_index t where t.board_id=v_board_id and rownum<=1 order by t.date_ asc;
              v_pre_close_price:=v_init_close_price;
              for j in cur_single_board_close_price loop
                  if v_pre_close_price=j.close_price then
                     update board_index t set t.up_down=0 where t.board_id=j.board_id and t.date_=j.date_;
                  elsif v_pre_close_price<j.close_price then
                     update board_index t set t.up_down=1 where t.board_id=j.board_id and t.date_=j.date_;
                  else
                     update board_index t set t.up_down=-1 where t.board_id=j.board_id and t.date_=j.date_;
                  end if;
                  commit;
                  v_pre_close_price:=j.close_price;
              end loop;
          end loop;
     end;
end CALCULATE_BOARD_INDEX;



/*-----------------------------------------------------------------------*/
procedure CALCULATE_BOARD_INDEX_BY_DATE(p_date in varchar2) as
begin
     declare
     -- 表示版块ID
     v_board_id number;
     -- 表示某一个交易日，上涨的家数
     v_up_number number;
     -- 表示某一个交易日，下跌的家数
     v_down_number number;
     -- 表示某一个交易日，某个版块的股票总数
     v_stock_number number;
     -- 表示某一个交易日，某个板块指数的收盘价
     v_close_price number;
     -- 表示某一个交易日，某个板块指数的开盘价
     v_open_price number;
     -- 表示某一个交易日，某个板块指数的最高价
     v_highest_price number;
     -- 表示某一个交易日，某个板块指数的最低价
     v_lowest_price number;
     -- 表示某一个交易日，某个板块指数的成交量
     v_amount number;
     -- 用于记录某一个版块，在最近两天的交易记录
     type type_array is array(3) of number;
     type_array_last_two_record type_array:=type_array();
     -- 获取某一个交易日中的所有版块ID。注意：由于有些股票暂时还没有划分到任何版块，所以这些股票的BOARD_ID为NULL
     cursor cur_all_board_id_by_date is select distinct t2.board_id from stock_transaction_data t1,stock_info t2 where t1.date_=to_date(p_date,'yyyy-mm-dd') and t1.code_=lower(t2.code_) and t2.board_id is not null;
     -- 获取某一个交易日,某一个版块的所有股票记录
     cursor cur_stock_by_date_and_board_id is select distinct t2.board_id,t1.date_,t1.up_down,t1.close_price,t1.open_price,t1.highest_price,t1.lowest_price,t1.volume
                                        from (
                                             select * from stock_transaction_data t where t.date_=to_date(p_date,'yyyy-mm-dd')
                                             ) t1,stock_info t2
                                        where t1.code_=lower(t2.code_) and t2.board_id=v_board_id;
     -- 获得某个交易日的所有版块ID
     cursor cur_all_board_id is select distinct t.board_id from board_index t where t.date_=to_date(p_date,'yyyy-mm-dd');
     -- 获取某个板块，从某一天开始最近的两个交易记录
     cursor cur_last2record_by_date_and_id is select * from (
                                                    select * from board_index t
                                                    where t.date_<=to_date(p_date,'yyyy-mm-dd') and t.board_id=v_board_id
                                                    order by t.date_ desc)
                                                  where rownum<=2;
     begin
          -- 计算board_date,board_id,up_down_rate,stock_number,up_amount,down_amount,board_close,board_amount
          for i in cur_all_board_id_by_date loop
              v_board_id:=i.board_id;
              -- 清零，用于计算某个交易日，下一个版块的数据
              v_up_number:=0;
              v_down_number:=0;
              v_stock_number:=0;
              v_close_price:=0;
              v_open_price:=0;
              v_highest_price:=0;
              v_lowest_price:=0;
              v_amount:=0;
              for j in cur_stock_by_date_and_board_id loop
                  if j.up_down=1 then
                     v_up_number:=v_up_number+1;
                  end if;
                  if j.up_down<1 then
                     v_down_number:=v_down_number+1;
                  end if;
                  v_stock_number:=v_stock_number+1;
                  v_close_price:=v_close_price+j.close_price;
                  v_open_price:=v_open_price+j.open_price;
                  v_highest_price:=v_highest_price+j.highest_price;
                  v_lowest_price:=v_lowest_price+j.lowest_price;
                  v_amount:=v_amount+j.volume;
              end loop;
              insert into board_index(board_id,date_,up_down_rate,stock_number,up_amount,down_amount,close_price,open_price,highest_price,lowest_price,amount)
              values(v_board_id,to_date(p_date,'yyyy-mm-dd'),decode(v_down_number,0,v_up_number,round(v_up_number/v_down_number,2)),v_stock_number,v_up_number,v_down_number,round(v_close_price/v_stock_number,2),round(v_open_price/v_stock_number,2),round(v_highest_price/v_stock_number,2),round(v_lowest_price/v_stock_number,2),round(v_amount/v_stock_number,2));
              commit;
          end loop;

          -- 计算UP_DOWN列
          for i in cur_all_board_id loop
              v_board_id:=i.board_id;
              type_array_last_two_record:=type_array(3);
              for j in cur_last2record_by_date_and_id loop
                  type_array_last_two_record(type_array_last_two_record.count):=j.close_price;
                  type_array_last_two_record.extend;
              end loop;
              if type_array_last_two_record(1)>type_array_last_two_record(2) then
                  update board_index t set t.up_down=1 where t.date_=to_date(p_date,'yyyy-mm-dd') and t.board_id=v_board_id;
                  commit;
              else
                  update board_index t set t.up_down=-1 where t.date_=to_date(p_date,'yyyy-mm-dd') and t.board_id=v_board_id;
                  commit;
              end if;
          end loop;
     end;
end CALCULATE_BOARD_INDEX_BY_DATE;



/*-----------------------------------------------------------------------*/
procedure CAL_FIVE_DAY_RATE as
    -- 表示板块的ID
    v_board_id number;
    -- 表示板块的日期
    v_date date;
    -- 用于计数，5个交易日之后才开始计算
    v_num number;
    -- 表示当前交易日的收盘价
    v_current_close_price number;
    -- 表示5天前交易日的收盘价
    v_last_five_close_price number;
    -- 表示所有版块的ID
    cursor cur_all_board_id is select distinct t.board_id from board_index t;
    -- 表示某个板块的交易记录，按照board_date升序排列
    cursor cur_single_board_index is select * from board_index t where t.board_id=v_board_id order by t.date_ asc;
begin
    for i in cur_all_board_id loop
        v_board_id:=i.board_id;
        v_num:=0;
        for j in cur_single_board_index loop
            v_num:=v_num+1;
            v_date:=j.date_;
            -- 最开始的5个交易日不计算five_day_rate
            if v_num>=6 then
                -- 获取当前交易日的收盘价
                select t.close_price into v_current_close_price from board_index t where t.board_id=v_board_id and t.date_=j.date_;
                -- 获取5天前交易日的收盘价
                select o.close_price into v_last_five_close_price from
                (
                    select * from
                    (
                           select * from (
                                  select * from board_index t where t.board_id=v_board_id and t.date_<=v_date order by t.date_ desc
                           ) where rownum<=6
                    ) t order by t.date_ asc
                ) o where rownum<=1;
                -- 更新five_day_rate字段
                update board_index t set t.five_day_rate=(v_current_close_price-v_last_five_close_price)/v_last_five_close_price*100
                       where t.board_id=i.board_id and t.date_=v_date;
                commit;
            end if;
        end loop;
        -- 为了计算下一个板块的five_day_rate，将v_num清零
        v_num:=0;
    end loop;
    null;
end CAL_FIVE_DAY_RATE;




/*-----------------------------------------------------------------------*/
procedure CAL_FIVE_DAY_RATE_BY_DATE(p_date in varchar2) as
    -- 表示板块的ID
    v_board_id number;
    -- 表示当前交易日的收盘价
    v_current_close_price number;
    -- 表示5天前交易日的收盘价
    v_last_five_close_price number;
    -- 表示某个交易日的所有版块的ID
    cursor cur_all_board_id is select distinct t.board_id from board_index t where t.date_=to_date(p_date,'yyyy-mm-dd');
begin
    for i in cur_all_board_id loop
        v_board_id:=i.board_id;
        -- 获取当前交易日的收盘价
        select t.close_price into v_current_close_price from BOARD_INDEX t where t.board_id=v_board_id and t.date_=to_date(p_date,'yyyy-mm-dd');
        -- 获取5天前交易日的收盘价
        select b.close_price into v_last_five_close_price from
        (
            select * from
            (
                   select * from (
                          select * from BOARD_INDEX t where t.board_id=v_board_id and t.date_<=to_date(p_date,'yyyy-mm-dd') order by t.date_ desc
                   ) where rownum<=6
            )o order by o.date_ asc
        )b where rownum<=1;
        -- 更新five_day_rate字段
        update board_index t set t.five_day_rate=(v_current_close_price-v_last_five_close_price)/v_last_five_close_price*100
               where t.date_=to_date(p_date,'yyyy-mm-dd') and t.board_id=v_board_id;
        commit;
    end loop;
end CAL_FIVE_DAY_RATE_BY_DATE;




/*-----------------------------------------------------------------------*/
procedure CAL_TEN_DAY_RATE as
    -- 表示板块的ID
    v_board_id number;
    -- 表示板块的日期
    v_date date;
    -- 用于计数，11个交易日之后才开始计算
    v_num number;
    -- 表示当前交易日的收盘价
    v_current_close_price number;
    -- 表示11天前交易日的收盘价
    v_last_ten_close_price number;
    -- 表示所有版块的ID
    cursor cur_all_board_id is select distinct t.board_id from board_index t;
    -- 表示某个板块的交易记录，按照board_date升序排列
    cursor cur_single_board_index is select * from board_index t where t.board_id=v_board_id order by t.date_ asc;
begin
    for i in cur_all_board_id loop
        v_board_id:=i.board_id;
        v_num:=0;
        for j in cur_single_board_index loop
            v_num:=v_num+1;
            v_date:=j.date_;
            -- 最开始的11个交易日不计算five_day_rate
            if v_num>=11 then
                -- 获取当前交易日的收盘价
                select t.close_price into v_current_close_price from board_index t where t.board_id=v_board_id and t.date_=j.date_;
                -- 获取5天前交易日的收盘价
                select o.close_price into v_last_ten_close_price from
                (
                    select * from
                    (
                           select * from (
                                  select * from board_index t where t.board_id=v_board_id and t.date_<=v_date order by t.date_ desc
                           ) where rownum<=11
                    ) t order by t.date_ asc
                ) o where rownum<=1;
                -- 更新five_day_rate字段
                update board_index t set t.ten_day_rate=(v_current_close_price-v_last_ten_close_price)/v_last_ten_close_price*100
                       where t.board_id=i.board_id and t.date_=v_date;
                commit;
            end if;
        end loop;
        -- 为了计算下一个板块的five_day_rate，将v_num清零
        v_num:=0;
    end loop;
    null;
end CAL_TEN_DAY_RATE;




/*-----------------------------------------------------------------------*/
procedure CAL_TEN_DAY_RATE_BY_DATE(p_date in varchar2) as
    -- 表示板块的ID
    v_board_id number;
    -- 表示当前交易日的收盘价
    v_current_close_price number;
    -- 表示10天前交易日的收盘价
    v_last_ten_close_price number;
    -- 表示某个交易日的所有版块的ID
    cursor cur_all_board_id is select distinct t.board_id from board_index t where t.date_=to_date(p_date,'yyyy-mm-dd');
begin
    for i in cur_all_board_id loop
        v_board_id:=i.board_id;
        -- 获取当前交易日的收盘价
        select t.close_price into v_current_close_price from BOARD_INDEX t where t.board_id=v_board_id and t.date_=to_date(p_date,'yyyy-mm-dd');
        -- 获取5天前交易日的收盘价
        select b.close_price into v_last_ten_close_price from
        (
            select * from
            (
                   select * from (
                          select * from BOARD_INDEX t where t.board_id=v_board_id and t.date_<=to_date(p_date,'yyyy-mm-dd') order by t.date_ desc
                   ) where rownum<=11
            )o order by o.date_ asc
        )b where rownum<=1;
        -- 更新ten_day_rate字段
        update board_index t set t.ten_day_rate=(v_current_close_price-v_last_ten_close_price)/v_last_ten_close_price*100
               where t.date_=to_date(p_date,'yyyy-mm-dd') and t.board_id=v_board_id;
        commit;
    end loop;
end CAL_TEN_DAY_RATE_BY_DATE;




/*------- calculate up or down percentage of the close of this day against the close of the last day ----------*/
procedure CAL_UP_DOWN_PERCENTAGE as
    -- 用于存储版块的ID
    v_board_id number;
    -- 用于存储某个版块第一条记录的日期
    v_board_first_date date;
    result number;
    -- 获取所有版块的ID
    cursor cur_board_id is select distinct t.id from board t;
    -- 获取某个版块的所有记录，并按升序排列
    cursor cur_all_board_index_by_id is select * from board_index t where t.board_id=v_board_id order by t.date_ asc;
begin
    for i in cur_board_id loop
        v_board_id:=i.id;
        for j in cur_all_board_index_by_id loop
            -- 获取某个版块的第一条记录
            select * into v_board_first_date from(
                   select t.date_ from board_index t where t.board_id=v_board_id order by t.date_ asc
            ) where rownum<=1;
            -- 任何版块的第一条记录都不计算
            if v_board_first_date<>j.date_ then
                result:=SCOTT.fnc_cal_up_down_percent(to_char(j.date_,'yyyy-mm-dd'),v_board_id);
            end if;
        end loop;
    end loop;
end CAL_UP_DOWN_PERCENTAGE;


/*select t.board_id,(max(t.board_close)-min(t.board_close))/min(t.board_close)*100 from BOARD_INDEX t
where t.board_date between to_date('20120109','yyyy-mm-dd') and to_date('20120305','yyyy-mm-dd')
--to_date('20121205','yyyy-mm-dd') and to_date('20130218','yyyy-mm-dd')
--to_date('20081110','yyyy-mm-dd') and to_date('20090805','yyyy-mm-dd')
--to_date('20140722','yyyy-mm-dd') and to_date('20150625','yyyy-mm-dd')
group by t.board_id order by (max(t.board_close)-min(t.board_close))/min(t.board_close) desc*/


/*-----------------------------------------------------------------------*/
procedure CAL_UP_DOWN_PERCENTAGE_BY_DATE(p_date in varchar2) as
    -- 这个字段没有实际意义，只是为了接收方法fnc_cal_up_down_percent的返回值
    temp number;
    -- 获取所有版块的ID
    cursor cur_board_id is select distinct t.id from board t;
begin
    for i in cur_board_id loop
        temp:=SCOTT.fnc_cal_up_down_percent(p_date,i.id);
    end loop;
end CAL_UP_DOWN_PERCENTAGE_BY_DATE;




/*--------------------------------- calculate UP_DOWN_RANK field --------------------------------------*/
procedure CAL_UP_DOWN_RANK as
    -- 这个字段没有实际意义，只是为了接收方法fnc_cal_up_down_percent的返回值
    temp number;
    -- 获取BOARD_INDEX表中的所有board_date字段
    cursor cur_all_board_date is select distinct t.date_ from board_index t order by t.date_ asc;
begin
    for i in cur_all_board_date loop
        temp:=SCOTT.fnc_cal_up_down_percent_bydate(to_char(i.date_,'yyyy-mm-dd'));
    end loop;
end CAL_UP_DOWN_RANK;




/*-----------------------------------------------------------------------*/
procedure CAL_UP_DOWN_RANK_BY_DATE(p_date in varchar2) as
    -- 这个字段没有实际意义，只是为了接收方法fnc_cal_up_down_percent的返回值
    temp number;
begin
    temp:=SCOTT.fnc_cal_up_down_percent_bydate(p_date);
end CAL_UP_DOWN_RANK_BY_DATE;





/*-----------------------------------------------------------------------*/
procedure FIND_LIMITUP_STOCKBOARD_BYDATE(p_date in varchar2) as
begin
     declare
     -- 表示股票代码stock_code
     v_stock_code number;
     -- 某一日，涨停的股票的总数
     v_stock_limit_up_number number;
     -- 表示版块ID
     v_board_id number;
     -- 用于打印股票名称
     v_varchar2_for_print varchar2(255);
     -- 创建UTL_FILE.file_type对象，用于读写文件
     file_handle UTL_FILE.file_type;
     -- 表示某一天中，涨停股票记录，包括：stock_date,stock_code,stock_name,id,board_name,stock_amount,today_up_down_percentage
     cursor cur_all_stock_limit_up_by_date is select t3.id as board_id,count(*) as stock_amount--distinct t1.stock_date,t1.stock_code,t2.stock_name,t3.id,t3.board_name,t3.stock_amount,t1.today_up_down_percentage
                                    from stock_transaction_data t1,stock_info t2,board t3
                                    where t1.date_=to_date(p_date,'yyyy-mm-dd') and t1.change_range>0.09 and t1.code_=lower(t2.code_) and t2.board_id=t3.id
                                    group by t3.id;
     -- 表示某一日，某一个版块，涨停的股票的名称
     cursor cur_limit_up_stock_name is select distinct t2.name_ from stock_transaction_data t1,stock_info t2,board t3
                                where t1.date_=to_date(p_date,'yyyy-mm-dd') and t1.change_range>0.09 and t1.code_=lower(t2.code_)
                                and t2.board_id=t3.id and t3.id=v_board_id;
     begin
          file_handle := UTL_FILE.FOPEN('TXTDIR','SELECT_STOCK_LIMIT_UP.properties','w');
          -- 计算某一天中，涨停股票的个数
          select count(t.code_) into v_stock_limit_up_number from STOCK_TRANSACTION_DATA t where t.date_=to_date(p_date,'yyyy-mm-dd') and t.change_range>0.09;
          -- 输出日期
          UTL_FILE.PUT_LINE(file_handle,'date='||p_date);
          -- 输出某一日涨停股票的家数
          UTL_FILE.PUT_LINE(file_handle,'limit_up_number='||v_stock_limit_up_number);

          for i in cur_all_stock_limit_up_by_date loop
              v_board_id:=i.board_id;
              v_varchar2_for_print:=null;
              -- 输出涨停版块的ID和涨停家数
              UTL_FILE.PUT_LINE(file_handle,'board_id_and_stock_amount='||i.board_id||','||i.stock_amount);
              for j in cur_limit_up_stock_name loop
                  if v_varchar2_for_print is not null then
                     v_varchar2_for_print:=v_varchar2_for_print||','||j.name_;
                  else
                      v_varchar2_for_print:=v_varchar2_for_print||j.name_;
                  end if;
              end loop;
              -- 输出某一个版块，某一日，涨停的股票名称
              UTL_FILE.PUT_LINE(file_handle,'stock_names='||v_varchar2_for_print);
          end loop;
     end;
end FIND_LIMITUP_STOCKBOARD_BYDATE;


procedure FIND_DATA_FOR_SPIDER_WEB_PLOT(p_date in varchar2,p_date_number in number,p_limit_rate in number,p_board_result_array out T_BOARD_RESULT_ARRAY) as
     -- 表示BOARD表的ID字段
     v_board_id number;
     -- 表示BOARD表的DATE_字段
     v_date date;
     -- 表示T_BOARD_RESULT_ARRAY类型的对象
     type_board_result T_BOARD_RESULT;
     -- 返回boadr_id
     cursor cur_board_ids is select distinct t.id from board t;
     -- 返回从boarDate开始的dateNumber个日期
     cursor cur_last_several_board_date is select * from (
                select t.date_ from board_index t where t.date_<=to_date(p_date,'yyyy-mm-dd') and t.board_id=v_board_id order by t.date_ desc
            ) where rownum<=p_date_number;
     -- 返回某一日所有版块的涨停股票的信息
     /*cursor cur_stock_limit_number_of_board_by_date is select t3.id as board_id,count(*) as stock_amount
                                    from stock_transaction_data t1,stock_info t2,board t3
                                    where t1.date_=to_date(v_date, 'yyyy-mm-dd') and t1.change_range>p_limit_rate and t3.id=v_board_id and t1.code_=lower(t2.code_) and t2.board_id=t3.id
                                    group by t3.id;*/

     cursor cur_board_stock_limit_by_date is select t3.id as board_id, count(t3.id) as stock_amount
                                    from stock_transaction_data t1,stock_info t2,board t3
                                    where t1.date_=v_date and t1.change_range>p_limit_rate and t3.id=v_board_id and t1.code_=lower(t2.code_) and t2.board_id=t3.id
                                    group by t3.id;
begin
     p_board_result_array:=T_BOARD_RESULT_ARRAY();
     for i in cur_board_ids loop
         v_board_id:=i.id;
         for j in cur_last_several_board_date loop
             v_date:=j.date_;
             for m in cur_board_stock_limit_by_date loop
                 type_board_result:=T_BOARD_RESULT(v_date,m.board_id,m.stock_amount);
                 p_board_result_array.Extend;
                 p_board_result_array(p_board_result_array.count):=type_board_result;
                 dbms_output.put_line(m.board_id||'   '||v_date||'   '||m.stock_amount);
             end loop;
         end loop;
     end loop;
end FIND_DATA_FOR_SPIDER_WEB_PLOT;



end PKG_BOARD_INDEX;
/

