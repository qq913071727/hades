???create or replace type scott.T_CLOSE_PRICE_MA_ORDER as object
(
  value_ number,
  name_  varchar2(20),
  label  number,

  member function get_value return number,
  member procedure set_value(p_value number),

  member function get_name return varchar2,
  member procedure set_name(p_name varchar2),

  member function get_label return number,
  member procedure set_label(p_label number)
)not final
/

