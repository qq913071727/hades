???create or replace package scott.PKG_MODEL_INDEX as
  type tp_cur_top_stock_date is ref cursor;
  -- 海量地向表MDL_STOCK_INDEX_MACD_G_C中插入数据
  procedure CAL_MDL_STOCK_INDEX_MACD_G_C;

  -- 海量地向表MDL_STOCK_INDEX_C_P_MA5_G_C中插入数据
  procedure CAL_MDL_STOCK_INDEX_CP_MA5_GC;

  -- 海量地向表MDL_HEI_KIN_ASHI_DOWN_UP中插入数据
  procedure CAL_MDL_STOCK_INDEX_H_K_A_D_U;

  -- 海量地向表MDL_STOCK_INDEX_KD_GOLD_CROSS中插入数据
  procedure CAL_MDL_STOCK_INDEX_KD_G_C;

end PKG_MODEL_INDEX;
/

