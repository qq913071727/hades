???create or replace package scott.PKG_WEEK is
  -- calculate the basic data such as open price, close price and so on
  procedure WRITE_STOCK_WEEK;

  -- 计算简单移动平均线
  PROCEDURE WRITE_MA5;
  PROCEDURE WRITE_MA10;
  PROCEDURE WRITE_MA20;
  PROCEDURE WRITE_MA60;
  PROCEDURE WRITE_MA120;
  PROCEDURE WRITE_MA250;

  -- 计算某一个周，所有股票的简单移动平均线
  PROCEDURE WRITE_MA_BY_DATE(p_current_week_begin_date in varchar2,
                             p_current_week_end_date   in varchar2);

  -- calculate KD
  procedure WRITE_WEEK_KD_INIT;
  procedure WRITE_WEEK_KD_RSV;
  procedure WRITE_WEEK_KD_K;
  procedure WRITE_WEEK_KD_D;

  -- calculate up_down
  procedure WRITE_WEEK_UP_DOWN;
  procedure WRITE_WEEK_UP_DOWN_BY_DATE(p_begin_date in varchar2,
                                       p_end_date   in varchar2);

  -- calculate the basic data such as open price, close price and so on by date
  procedure WRITE_STOCK_WEEK_BY_DATE(p_begin_date varchar2,
                                     p_end_date   varchar2);

  -- calculate KD by date
  procedure WRITE_WEEK_KD_BY_DATE_RSV(p_begin_date varchar2,
                                      p_end_date   varchar2);
  procedure WRITE_WEEK_KD_BY_DATE_K(p_begin_date varchar2,
                                    p_end_date   varchar2);
  procedure WRITE_WEEK_KD_BY_DATE_D(p_begin_date varchar2,
                                    p_end_date   varchar2);

  -- 计算MACD
  procedure WRITE_WEEK_MACD_INIT;
  procedure WRITE_WEEK_MACD_EMA;
  procedure WRITE_WEEK_MACD_DIF;
  procedure WRITE_WEEK_MACD_DEA;
  procedure WRITE_WEEK_MACD;
  procedure WRITE_WEEK_MACD_EMA_BY_DATE(p_stock_week_begin_date in varchar2,
                                        p_stock_week_end_date   in varchar2);
  procedure WRITE_WEEK_MACD_DIF_BY_DATE(p_stock_week_begin_date in varchar2,
                                        p_stock_week_end_date   in varchar2);
  procedure WRITE_WEEK_MACD_DEA_BY_DATE(p_stock_week_begin_date in varchar2,
                                        p_stock_week_end_date   in varchar2);
  procedure WRITE_WEEK_MACD_BY_DATE(p_stock_week_begin_date in varchar2,
                                    p_stock_week_end_date   in varchar2);

  -- WRITE_WEEK_CHANGE_RANGE_EX_RIGHT
  procedure WRITE_WEEK_C_R_E_R;
  -- WRITE_WEEK_CHANGE_RANGE_EX_RIGHT_BY_DATE
  procedure WRITE_WEEK_C_R_E_R_BY_DATE(p_stock_week_begin_date in varchar2,
                                       p_stock_week_end_date   in varchar2);

  /*--------------------- 计算stock_week表中的布林带 -----------------------*/
  procedure CAL_BOLL;

  /*-------------- 计算某一周stock_week表中的布林带，必须在计算完均线后运行 -----------------*/
  procedure CAL_BOLL_BY_DATE(p_begin_date varchar2, p_end_date varchar2);

  -- 计算表STOCK_WEEK的所有Hei Kin Ashi字段
  procedure CAL_STOCK_WEEK_HA;

  -- 按照日期，计算表STOCK_WEEK的Hei Kin Ashi记录
  procedure CAL_STOCK_WEEK_HA_BY_DATE(p_begin_date in varchar2,
                                      p_end_date   in varchar2);

  procedure FIND_WEEK_MACD_END_DEVIATE(p_stock_week_begin_date in varchar2,
                                       p_stock_week_end_date   in varchar2,
                                       p_rate_week_date        in varchar2,
                                       p_stock_week_result     out clob);

  -- select the gold cross of kd index in weekend scale
  procedure SELECT_WEEK_KD_UP(p_begin_date              in varchar2,
                              p_end_date                in varchar2,
                              p_cross_point             in number,
                              p_stock_week_result_array out T_STOCK_WEEK_RESULT_ARRAY);
end PKG_WEEK;
/

