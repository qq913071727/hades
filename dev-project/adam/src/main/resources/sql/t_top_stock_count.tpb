???create or replace type body scott.T_TOP_STOCK_COUNT is

  member function get_code return varchar2 is
  begin
    return v_code;
  end get_code;

  member procedure set_code(p_code varchar2) is
  begin
    v_code := p_code;
  end set_code;

  member function get_count return number is
  begin
    return v_count;
  end get_count;

  member procedure set_count(p_count number) is
  begin
    v_count := p_count;
  end set_count;

end;
/

