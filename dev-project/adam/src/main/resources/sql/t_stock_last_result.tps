???create or replace type scott.T_STOCK_LAST_RESULT as object
(
       v_stock_code varchar2(100),
       v_stock_name varchar2(100),
       v_board_name varchar2(100),
       v_rate number,

       member function get_stock_code return varchar2,
       member function get_stock_name return varchar2,
       member function get_board_name return varchar2,
       member function get_rate return number,
       member procedure set_stock_code(p_stock_code varchar2),
       member procedure set_stock_name(p_stock_name varchar2),
       member procedure set_board_name(p_board_name varchar2),
       member procedure set_rate(p_rate number)
)not final
/

