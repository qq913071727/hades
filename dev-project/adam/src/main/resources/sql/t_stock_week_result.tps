???create or replace type scott.T_STOCK_WEEK_RESULT as object
(
       stock_week_code varchar2(10),
       stock_week_begin_date date,
       stock_week_end_date date,

       member function get_stock_week_code return varchar2,
       member procedure set_stock_week_code(param_stock_week_code varchar2),
       member function get_stock_week_begin_date return date,
       member procedure set_stock_week_begin_date(param_stock_week_begin_date date),
       member function get_stock_week_end_date return date,
       member procedure set_stock_week_end_date(param_stock_week_end_date date)
)
/

