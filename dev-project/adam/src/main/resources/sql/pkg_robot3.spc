???create or replace package scott.PKG_ROBOT3 is

  /****************************************** 卖股票/买股票 **********************************************/
  -- 根据当日持有股票的收盘价，在卖股票/买股票之前，更新robot3_account表
  -- 游标名称update_robot_account_before_sell_or_buy
  procedure update_robot_account_b_s_b(p_date in varchar2);

  -- 卖股票/买股票，只修改robot3_stock_transact_record表中的记录
  /* p_mandatory_stop_loss等于1，表示有强制止损；p_mandatory_stop_loss等于0，表示没有强制止损 */
  procedure sell_or_buy(p_date                     in varchar2,
                        p_mandatory_stop_loss      in number,
                        p_mandatory_stop_loss_rate in number);

  -- 根据当日卖出/买入股票的收盘价，在卖股票/买股票之后，更新robot3_account表
  procedure update_robot_account_after_s_b(p_date in varchar2);

  /***************************** 买股票之前的过滤，确定哪些股票能买 *********************************/
  -- 过滤条件：MACD金叉。用于做多
  procedure filter_by_macd_gold_cross(p_date in varchar2);

  -- 过滤条件：MACD死叉。用于做空
  procedure filter_by_macd_dead_cross(p_date in varchar2);

  -- 过滤条件：删除除过权的股票
  procedure filter_by_xr(p_begin_date in varchar2, p_end_date in varchar2);

  -- 过滤条件：周线级别KD金叉
  procedure filter_by_week_kd_gold_cross(p_date in varchar2);

  -- 过滤条件：当前收盘价与某段时间最高价的百分比
  -- filter_by_percentage_of_current_close_price_compare_to_some_time_highest_price
  procedure filter_p_o_c_c_p_c_t_s_t_h_p(p_begin_date in varchar2,
                                         p_date       in varchar2,
                                         p_percentage in number);

  -- 过滤条件：当前收盘价与某段时间最低价的百分比
  -- filter_by_percentage_of_current_close_price_compare_to_some_time_lowest_price
  procedure filter_p_o_c_c_p_c_t_s_t_l_p(p_begin_date in varchar2,
                                         p_date       in varchar2,
                                         p_percentage in number);

  -- 过滤条件：MA不单调递减，过滤股票
  PROCEDURE filter_by_ma_not_decreasing(p_ma_level in number,
                                        p_date     in varchar2,
                                        p_rownum   in number);

  -- 过滤条件：MA不单调递增，过滤股票
  PROCEDURE filter_by_ma_not_increasing(p_ma_level in number,
                                        p_date     in varchar2,
                                        p_rownum   in number);

  -- 过滤条件：收盘价金叉五日均线。用于做多
  -- filter_by_close_price_gold_cross_ma5
  procedure filter_by_c_p_g_c_ma5(p_date in varchar2);

  -- 过滤条件：收盘价金叉五日均线。用于做空
  -- filter_by_close_price_dead_cross_ma5
  procedure filter_by_c_p_d_c_ma5(p_date in varchar2);

  -- 过滤条件：KD金叉。用于做多
  procedure filter_by_kd_gold_cross(p_date in varchar2);

  -- 过滤条件：KD死叉。用于做空
  procedure filter_by_kd_dead_cross(p_date in varchar2);

  -- 过滤条件：保留所有金叉。用于做多
  procedure filter_by_all_gold_cross(p_date in varchar2);

  -- 过滤条件：保留所有死叉。用于做空
  procedure filter_by_all_dead_cross(p_date in varchar2);

  -- 过滤条件：日线级别，某一段时间内，收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=p_rate
  procedure filter_by_c_p_gt_lt_ma250(p_begin_date in varchar2,
                                      p_end_date   in varchar2,
                                      p_rate       in number);

  -- 过滤条件：如果当前交易日和前n个交易日的收盘价都大于/小于布林带上轨/下轨，并且当前交易日的收盘价小于/大于前一个交易日的收盘价，则保留
  procedure filter_by_up_down_boll(p_date                in varchar2,
                                   p_date_number         in number,
                                   p_operation_direction in number);

  /****************************************** 买股票/卖股票 **********************************************/
  -- 买股票/卖股票，只向robot_stock_transaction_record表中插入记录，只更新robot_account表的hold_stock_number字段
  procedure buy_or_sell(p_buy_date                   in varchar2,
                        p_backward_month             in number,
                        p_average_date_number        in number,
                        p_success_rate_type          in number,
                        p_success_rate_or_percentage in number,
                        p_direction                  in number,
                        p_shipment_space_control     in number,
                        p_percentage_top_threshold   in number,
                        p_shipping_space             in number,
                        p_hold_stock_number          in number,
                        p_operation_direction        in number);

  -- 根据当日买入股票/卖出股票的收盘价，在买股票/卖股票之后，更新robot_account表
  -- 存储过程名称：update_robot_account_after_buy_or_sell
  procedure update_robot_account_after_b_s(p_date in varchar2);

end PKG_ROBOT3;
/

