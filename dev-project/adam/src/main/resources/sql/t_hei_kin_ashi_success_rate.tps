???create or replace type scott.T_HEI_KIN_ASHI_SUCCESS_RATE as object
(
       date_ date,
       success_rate number,

       member function getDate_ return date,
       member procedure setDate(paramDate_ date),
       member function getSuccessRate return number,
       member procedure setSuccessRate(paramSuccessRate number)
)
/

