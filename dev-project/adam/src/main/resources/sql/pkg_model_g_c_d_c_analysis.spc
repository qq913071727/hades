???create or replace package scott.PKG_MODEL_G_C_D_C_ANALYSIS is

  -- 计算mdl_g_c_d_c_analysis表中的所有百分比的均值
  procedure WRITE_MDL_G_C_D_C_A_PERCENT_MA;

  -- 计算mdl_g_c_d_c_analysis表中的所有成功率的均值
  procedure WRITE_MDL_G_C_D_C_A_S_R_MA;

  -- 查询各种算法状态已经持续了多少天
  /* 1表示MACD金叉，2表示MACD死叉，3表示收盘价金叉五日均线，4表示收盘价死叉五日均线
  5表示hei_kin_ashi处于上涨阶段，6表示hei_kin_ashi处于下跌阶段
  7表示KD金叉，8表示KD死叉
  返回值为0表示现在并不处于这种状态 */
  -- 全名：find_gold_cross_dead_cross_continue_date_count
  procedure find_g_c_d_c_c_date_count(p_type in number,
                                      p_date in varchar2,
                                      p_num  out number);

  -- 根据日期，计算表MDL_G_C_D_C_ANALYSIS的数据
  procedure write_g_c_d_c_a_by_date(p_begin_date in varchar2,
                                    p_end_date   in varchar2);

end PKG_MODEL_G_C_D_C_ANALYSIS;
/

