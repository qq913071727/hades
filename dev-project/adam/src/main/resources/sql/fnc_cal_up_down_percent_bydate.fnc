???create or replace function scott.FNC_CAL_UP_DOWN_PERCENT_BYDATE(p_date in varchar2) return number is
    result number;
    -- 用于记录排名
    v_rank number;
    -- 获取某一日BOARD_INDEX表的记录，并按UP_DOWN_PERCENTAGE降序排列
    cursor cur_all_board_index_by_date is select * from board_index t where t.date_=to_date(p_date,'yyyy-mm-dd')
        order by t.up_down_percentage desc;
    --cursor cur_all_board_index_id is select * from board_index t where t.
begin
    v_rank:=0;
    for i in cur_all_board_index_by_date loop
        v_rank:=v_rank+1;
        update board_index t set t.up_down_rank=v_rank where t.board_id=i.board_id and t.date_=i.date_;
        commit;
    end loop;
    return(result);
end FNC_CAL_UP_DOWN_PERCENT_BYDATE;
/

