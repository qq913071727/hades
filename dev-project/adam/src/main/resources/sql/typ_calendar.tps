???create or replace type scott.typ_calendar as object(
       -- 表示股票代码
       stockCode varchar2(8),

       -- 表示方法
       member function format(
              curday date:=sysdate,
              fmtlen pls_integer:=8
       )return typ_calendar
)
/

