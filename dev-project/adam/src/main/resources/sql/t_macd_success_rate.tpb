???create or replace type body scott.T_MACD_SUCCESS_RATE is

       member function getDate_ return date is
       begin
            return date_;
       end getDate_;


       member procedure setDate_(paramDate_ date) is
       begin
            date_:=paramDate_;
       end setDate_;

       member function getSuccessRate return number is
       begin
            return success_rate;
       end getSuccessRate;

       member procedure setSuccessRate(paramSuccessRate number) is
       begin
            success_rate:=paramSuccessRate;
       end setSuccessRate;

end;
/

