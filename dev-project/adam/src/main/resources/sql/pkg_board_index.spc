???create or replace package scott.PKG_BOARD_INDEX is

       procedure CALCULATE_BOARD_INDEX;
       procedure CALCULATE_BOARD_INDEX_BY_DATE(p_date in varchar2);

       procedure CAL_FIVE_DAY_RATE;
       procedure CAL_FIVE_DAY_RATE_BY_DATE(p_date in varchar2);

       procedure CAL_TEN_DAY_RATE;
       procedure CAL_TEN_DAY_RATE_BY_DATE(p_date in varchar2);

       procedure CAL_UP_DOWN_PERCENTAGE;
       procedure CAL_UP_DOWN_PERCENTAGE_BY_DATE(p_date in varchar2);

       procedure CAL_UP_DOWN_RANK;
       procedure CAL_UP_DOWN_RANK_BY_DATE(p_date in varchar2);

       procedure FIND_LIMITUP_STOCKBOARD_BYDATE(p_date in varchar2);
       procedure FIND_DATA_FOR_SPIDER_WEB_PLOT(p_date in varchar2,p_date_number in number,p_limit_rate in number,p_board_result_array out T_BOARD_RESULT_ARRAY);

end PKG_BOARD_INDEX;
/

