???create or replace type body scott.T_STOCK_LAST_RESULT is
       member function get_stock_code return varchar2 is
       begin
            return v_stock_code;
       end get_stock_code;

       member procedure set_stock_code(p_stock_code varchar2) is
       begin
            v_stock_code:=p_stock_code;
       end set_stock_code;

       member function get_stock_name return varchar2 is
       begin
            return v_stock_name;
       end get_stock_name;

       member procedure set_stock_name(p_stock_name varchar2) is
       begin
            v_stock_name:=p_stock_name;
       end set_stock_name;

       member function get_board_name return varchar2 is
       begin
            return v_board_name;
       end get_board_name;

       member procedure set_board_name(p_board_name varchar2) is
       begin
            v_board_name:=p_board_name;
       end set_board_name;

       member function get_rate return number is
       begin
            return v_rate;
       end get_rate;

       member procedure set_rate(p_rate number) is
       begin
            v_rate:=p_rate;
       end set_rate;
end;
/

