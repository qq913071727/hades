???create or replace package body scott.PKG_MODEL_RECORD is

  /*---------------------------------- calculate MACD's gold cross model --------------------------------------*/
  procedure CAL_MDL_MACD_GOLD_CROSS as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断MACD是否发生了金叉或死叉
    v_first_stock stock_transaction_data%rowtype;
    -- 表示两天记录中的第二天，用于判断MACD是否发生了金叉或死叉
    v_second_stock stock_transaction_data%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_DIF,BUY_DEA,SELL_DIF,SELL_DEA
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_dif    number;
    v_temp_stock_buy_dea    number;
    v_temp_stock_sell_dif   number;
    v_temp_stock_sell_dea   number;
    -- 用来判断现在是否是dif>dea的阶段，从而可以判断死叉
    v_start_track_gold_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found  number;
    v_death_cross_found number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t; -- where t.code_ like '000%' or t.code_ like '300%' or t.code_ like '600%' or t.code_ like '603%';
    /*
    stock_transaction_data表中一共有code_：3963
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '000%'; 完成 -- 452
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '001%'; 完成 -- 6
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '002%'; 完成 -- 962
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '003%'; 完成 -- 1
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '300%'; 完成 -- 853
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '600%'; 完成 -- 809
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '601%'; 完成 -- 201
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '603%'; 完成 -- 510
    select count(distinct t.code_) from stock_transaction_data t where t.code_ like '688%'; 完成 -- 169
            */
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = lower(v_stock_code)
       order by t.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    v_model_id := 1;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是dif>dea，那么接下来只有可能先出现死叉，所以要判断：j.dif<j.dea，也就是说如果第一段数据是dif>dea，则将其忽略
        -- 之后在确认了金叉后，需要确认死叉，所以引入了表示变量v_start_track_gold_cross_stage，当其为1时，表示接下来只可能出现死叉
        if j.dif < j.dea or
           (j.dif > j.dea and v_start_track_gold_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.dif < v_first_stock.dea and
             v_second_stock.dif > v_second_stock.dea and
             v_start_track_gold_cross_stage = 0 then
            v_temp_stock_code      := v_second_stock.code_;
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_dif   := v_second_stock.dif;
            v_temp_stock_buy_dea   := v_second_stock.dea;
            v_gold_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_gold_cross_stage设置为1
            v_start_track_gold_cross_stage := 1;
          
            dbms_output.put_line('gold cross   ' || j.date_);
          end if;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.dif > v_first_stock.dea and
             v_second_stock.dif < v_second_stock.dea and
             v_start_track_gold_cross_stage = 1 then
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_dif   := v_second_stock.dif;
            v_temp_stock_sell_dea   := v_second_stock.dea;
            v_death_cross_found     := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_gold_cross_stage设置为0
            v_start_track_gold_cross_stage := 0;
          
            dbms_output.put_line('death cross   ' || j.date_);
          end if;
          -- 插入数据。要求这只股票在开始时间和结束时间内没有停牌或除权的情况
          if v_gold_cross_found = 1 and v_death_cross_found = 1 and
             v_model_id <> 0 and v_temp_stock_buy_date is not null and
             v_temp_stock_sell_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_MACD_GOLD_CROSS
              (model_id,
               buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_dif,
               buy_dea,
               sell_dif,
               sell_dea)
            values
              (v_model_id,
               v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_dif,
               v_temp_stock_buy_dea,
               v_temp_stock_sell_dif,
               v_temp_stock_sell_dea);
          
            v_gold_cross_found  := 0;
            v_death_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_MACD_GOLD_CROSS;

  /*---------------------------------- calculate MACD's gold cross model incrementaly ------------------------------------*/
  procedure CAL_MDL_MACD_GOLD_CROSS_INCR(p_end_date in varchar2) as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断MACD是否发生了金叉或死叉
    v_first_stock stock_transaction_data%rowtype;
    -- 表示两天记录中的第二天，用于判断MACD是否发生了金叉或死叉
    v_second_stock stock_transaction_data%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_DIF,BUY_DEA,SELL_DIF,SELL_DEA
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_dif    number;
    v_temp_stock_buy_dea    number;
    v_temp_stock_sell_dif   number;
    v_temp_stock_sell_dea   number;
    -- 用来判断现在是否是dif>dea的阶段，从而可以判断死叉
    v_start_track_gold_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found  number := 0;
    v_death_cross_found number := 0;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录。
    -- 1997-01-01是stock_transaction_data表中最早的交易记录。
    -- buy_date和sell_date效果一样
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t1
       where t1.date_ >
             (select nvl(max(t.buy_date),
                         to_date('1997-01-01', 'yyyy-mm-dd'))
                from mdl_macd_gold_cross t
               where t.stock_code = lower(v_stock_code))
         and t1.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
         and t1.code_ = lower(v_stock_code)
       order by t1.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    v_model_id := 1;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是dif>dea，那么接下来只有可能先出现死叉，所以要判断：j.dif<j.dea，也就是说如果第一段数据是dif>dea，则将其忽略
        -- 之后在确认了金叉后，需要确认死叉，所以引入了表示变量v_start_track_gold_cross_stage，当其为1时，表示接下来只可能出现死叉
        if j.dif < j.dea or
           (j.dif > j.dea and v_start_track_gold_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.dif < v_first_stock.dea and
             v_second_stock.dif > v_second_stock.dea and
             v_start_track_gold_cross_stage = 0 then
            v_temp_stock_code      := v_second_stock.code_;
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_dif   := v_second_stock.dif;
            v_temp_stock_buy_dea   := v_second_stock.dea;
            v_gold_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_gold_cross_stage设置为1
            v_start_track_gold_cross_stage := 1;
          
          end if;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.dif > v_first_stock.dea and
             v_second_stock.dif < v_second_stock.dea and
             v_start_track_gold_cross_stage = 1 then
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_dif   := v_second_stock.dif;
            v_temp_stock_sell_dea   := v_second_stock.dea;
            v_death_cross_found     := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_gold_cross_stage设置为0
            v_start_track_gold_cross_stage := 0;
          
          end if;
          -- 插入数据
          if v_gold_cross_found = 1 and v_death_cross_found = 1 and
             v_model_id is not null and v_temp_stock_buy_date is not null and
             v_temp_stock_sell_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss := round((v_temp_stock_sell_price -
                                        v_temp_stock_buy_price) /
                                        v_temp_stock_buy_price,
                                        4) * 100;
            -- 获取最近一次交易后的accumulative_profit_loss
            begin
              select t.accumulative_profit_loss
                into v_init_last_acc_profit_loss
                from (select *
                        from mdl_macd_gold_cross t
                       where t.stock_code = v_stock_code
                       order by t.buy_date desc) t
               where rownum <= 1;
            exception
              when no_data_found then
                dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_macd_gold_cross表中没有记录');
            end;
            -- 计算本次交易后的accumulative_profit_loss
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_MACD_GOLD_CROSS
              (model_id,
               buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_dif,
               buy_dea,
               sell_dif,
               sell_dea)
            values
              (v_model_id,
               v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_dif,
               v_temp_stock_buy_dea,
               v_temp_stock_sell_dif,
               v_temp_stock_sell_dea);
          
            v_gold_cross_found  := 0;
            v_death_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_MACD_GOLD_CROSS_INCR;

  /*---------------------------------- calculate MACD's dead cross model --------------------------------------*/
  procedure CAL_MDL_MACD_DEAD_CROSS as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断MACD是否发生了金叉或死叉
    v_first_stock stock_transaction_data%rowtype;
    -- 表示两天记录中的第二天，用于判断MACD是否发生了金叉或死叉
    v_second_stock stock_transaction_data%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_DIF,BUY_DEA,SELL_DIF,SELL_DEA
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_dif    number;
    v_temp_stock_buy_dea    number;
    v_temp_stock_sell_dif   number;
    v_temp_stock_sell_dea   number;
    -- 用来判断现在是否是dif>dea的阶段，从而可以判断死叉
    v_start_track_dead_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found number;
    v_dead_cross_found number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    /*where t.code_ not like '000%' and t.code_ not like '001%' and t.code_ not like '002%'
    and t.code_ not like '003%' and t.code_ not like '300%' and t.code_ not like '600%'
    and t.code_ not like '601%' and t.code_ not like '603%' and t.code_ not like '688%';*/
    /*
    select distinct t.code_ from stock_transaction_data_all t;                         -- 4194
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '000%'; -- 完成 450
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '001%'; -- 完成 6
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '002%'; -- 完成 961
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '003%'; -- 完成 38
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '300%'; -- 完成 915
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '600%'; -- 完成 801
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '601%'; -- 完成 207
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '603%'; -- 完成 513
    select count(distinct t.code_) from stock_transaction_data_all t where t.code_ like '688%'; -- 完成 235
    -- 其他 68
            */
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = lower(v_stock_code)
       order by t.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    v_model_id := 2;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_dead_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是dif<dea，那么接下来只有可能先出现金叉，所以要判断：j.dif>j.dea，也就是说如果第一段数据是dif<dea，则将其忽略
        -- 之后在确认了死叉后，需要确认金叉，所以引入了表示变量v_start_track_dead_cross_stage，当其为1时，表示接下来只可能出现金叉
        if j.dif > j.dea or
           (j.dif < j.dea and v_start_track_dead_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.dif > v_first_stock.dea and
             v_second_stock.dif < v_second_stock.dea and
             v_start_track_dead_cross_stage = 0 then
            v_temp_stock_code       := v_second_stock.code_;
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_dif   := v_second_stock.dif;
            v_temp_stock_sell_dea   := v_second_stock.dea;
            v_gold_cross_found      := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_dead_cross_stage设置为1
            v_start_track_dead_cross_stage := 1;
          
            dbms_output.put_line('dead cross   ' || j.date_);
          end if;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.dif < v_first_stock.dea and
             v_second_stock.dif > v_second_stock.dea and
             v_start_track_dead_cross_stage = 1 then
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_dif   := v_second_stock.dif;
            v_temp_stock_buy_dea   := v_second_stock.dea;
            v_dead_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_dead_cross_stage设置为0
            v_start_track_dead_cross_stage := 0;
          
            dbms_output.put_line('gold cross   ' || j.date_);
          end if;
          -- 插入数据。要求这只股票在开始时间和结束时间内没有停牌或除权的情况
          if v_gold_cross_found = 1 and v_dead_cross_found = 1 and
             v_model_id <> 0 and v_temp_stock_sell_date is not null and
             v_temp_stock_buy_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_sell_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_MACD_DEAD_CROSS
              (model_id,
               buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_dif,
               buy_dea,
               sell_dif,
               sell_dea)
            values
              (v_model_id,
               v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_dif,
               v_temp_stock_buy_dea,
               v_temp_stock_sell_dif,
               v_temp_stock_sell_dea);
            commit;
          
            v_gold_cross_found := 0;
            v_dead_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
  
  end CAL_MDL_MACD_DEAD_CROSS;

  /*---------------------------------- calculate MACD's dead cross model incrementaly ------------------------------------*/
  procedure CAL_MDL_MACD_DEAD_CROSS_INCR(p_end_date in varchar2) as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断MACD是否发生了金叉或死叉
    v_first_stock stock_transaction_data%rowtype;
    -- 表示两天记录中的第二天，用于判断MACD是否发生了金叉或死叉
    v_second_stock stock_transaction_data%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_DIF,BUY_DEA,SELL_DIF,SELL_DEA
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_dif    number;
    v_temp_stock_buy_dea    number;
    v_temp_stock_sell_dif   number;
    v_temp_stock_sell_dea   number;
    -- 用来判断现在是否是dif<dea的阶段，从而可以判断金叉
    v_start_track_dead_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found number := 0;
    v_dead_cross_found number := 0;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录。
    -- 1997-01-01是stock_transaction_data_all表中最早的交易记录。
    -- buy_date和sell_date效果一样
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t1
       where t1.date_ >
             (select nvl(max(t.sell_date),
                         to_date('1997-01-01', 'yyyy-mm-dd'))
                from mdl_macd_dead_cross t
               where t.stock_code = lower(v_stock_code))
         and t1.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
         and t1.code_ = lower(v_stock_code)
       order by t1.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    v_model_id := 2;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_dead_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是dif<dea，那么接下来只有可能先出现金叉，所以要判断：j.dif>j.dea，也就是说如果第一段数据是dif<dea，则将其忽略
        -- 之后在确认了死叉后，需要确认金叉，所以引入了表示变量v_start_track_dead_cross_stage，当其为1时，表示接下来只可能出现金叉
        if j.dif > j.dea or
           (j.dif < j.dea and v_start_track_dead_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.dif < v_first_stock.dea and
             v_second_stock.dif > v_second_stock.dea and
             v_start_track_dead_cross_stage = 1 then
            v_temp_stock_code      := v_second_stock.code_;
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_dif   := v_second_stock.dif;
            v_temp_stock_buy_dea   := v_second_stock.dea;
            v_gold_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_dead_cross_stage设置为0
            v_start_track_dead_cross_stage := 0;
          
          end if;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.dif > v_first_stock.dea and
             v_second_stock.dif < v_second_stock.dea and
             v_start_track_dead_cross_stage = 0 then
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_dif   := v_second_stock.dif;
            v_temp_stock_sell_dea   := v_second_stock.dea;
            v_dead_cross_found      := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_dead_cross_stage设置为1
            v_start_track_dead_cross_stage := 1;
          
          end if;
          -- 插入数据
          if v_gold_cross_found = 1 and v_dead_cross_found = 1 and
             v_model_id is not null and v_temp_stock_buy_date is not null and
             v_temp_stock_sell_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss := round((v_temp_stock_sell_price -
                                        v_temp_stock_buy_price) /
                                        v_temp_stock_sell_price,
                                        4) * 100;
            -- 获取最近一次交易后的accumulative_profit_loss
            begin
              select t.accumulative_profit_loss
                into v_init_last_acc_profit_loss
                from (select *
                        from mdl_macd_dead_cross t
                       where t.stock_code = v_stock_code
                       order by t.buy_date desc) t
               where rownum <= 1;
            exception
              when no_data_found then
                dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_macd_dead_cross表中没有记录');
            end;
            -- 计算本次交易后的accumulative_profit_loss
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_MACD_DEAD_CROSS
              (model_id,
               buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_dif,
               buy_dea,
               sell_dif,
               sell_dea)
            values
              (v_model_id,
               v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_dif,
               v_temp_stock_buy_dea,
               v_temp_stock_sell_dif,
               v_temp_stock_sell_dea);
          
            v_gold_cross_found := 0;
            v_dead_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_MACD_DEAD_CROSS_INCR;

  /*---------------------------------- 海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据 --------------------------------------*/
  procedure CAL_MDL_C_P_MA5_GOLD_CROSS as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_MA5,SELL_MA5
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_ma5    number;
    v_temp_stock_sell_ma5   number;
    -- 1表示close_price>ma5的阶段，2表示close_price<=ma5的阶段
    v_start_track_gold_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
    
      for j in cur_single_stock loop
        if j.close_price > j.ma5 then
          if v_start_track_gold_cross_stage = 0 then
            -- 买入
            v_temp_stock_code              := j.code_;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_buy_ma5           := j.ma5;
            v_start_track_gold_cross_stage := 1;
          else
            continue;
          end if;
        end if;
      
        if j.close_price <= j.ma5 then
          if v_start_track_gold_cross_stage = 1 then
            -- 卖出
            v_temp_stock_sell_date      := j.date_;
            v_temp_stock_sell_price     := j.close_price;
            v_temp_stock_sell_ma5       := j.ma5;
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
            -- 插入记录
            insert into MDL_CLOSE_PRICE_MA5_GOLD_CROSS
              (buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_ma5,
               sell_ma5)
            values
              (v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_ma5,
               v_temp_stock_sell_ma5);
          
            -- 重置为0，表示还没有买入
            v_start_track_gold_cross_stage := 0;
          else
            continue;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_C_P_MA5_GOLD_CROSS;

  /*---------------------------------- 增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据 ------------------------------------*/
  procedure CAL_MDL_C_P_MA5_G_C_INCR(p_end_date in varchar2) as
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_MA5,SELL_MA5
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_ma5    number;
    v_temp_stock_sell_ma5   number;
    -- 1表示close_price>ma5的阶段，0表示close_price<=ma5的阶段
    v_start_track_gold_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 从某一只股票在mdl_close_price_ma5_gold_cross表中最后的卖出日期，到参数p_end_date之间的交易记录，
    -- 并按升序排列。这段时间内可能存在先是close_price金叉ma5，然后close_price死叉ma5。
    -- buy_date和sell_date效果一样
    cursor cur_c_p_gold_dead_cross_ma5 is
      select *
        from stock_transaction_data_all std
       where std.code_ = v_stock_code
         and std.date_ >
             (select max(mcpmgc.sell_date)
                from mdl_close_price_ma5_gold_cross mcpmgc
               where mcpmgc.stock_code = v_stock_code)
         and std.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
       order by std.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_start_track_gold_cross_stage := 0;
      -- 查找最近的accumulative_profit_loss
      begin
        select b.accumulative_profit_loss
          into v_init_last_acc_profit_loss
          from (select *
                  from mdl_close_price_ma5_gold_cross t
                 where t.stock_code = v_stock_code
                 order by t.sell_date desc) b
         where rownum <= 1;
      exception
        when no_data_found then
          dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_close_price_ma5_gold_cross表中没有记录');
      end;
    
      for j in cur_c_p_gold_dead_cross_ma5 loop
        if j.close_price > j.ma5 then
          -- 买入
          if v_start_track_gold_cross_stage = 0 then
            v_temp_stock_code              := j.code_;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_buy_ma5           := j.ma5;
            v_start_track_gold_cross_stage := 1;
          else
            continue;
          end if;
        end if;
      
        if j.close_price <= j.ma5 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_temp_stock_sell_date  := j.date_;
            v_temp_stock_sell_price := j.close_price;
            v_temp_stock_sell_ma5   := j.ma5;
          
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_CLOSE_PRICE_MA5_GOLD_CROSS
              (buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_ma5,
               sell_ma5)
            values
              (v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_ma5,
               v_temp_stock_sell_ma5);
          
            v_start_track_gold_cross_stage := 0;
          else
            continue;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_C_P_MA5_G_C_INCR;

  /*---------------------------------- 海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据 --------------------------------------*/
  procedure CAL_MDL_C_P_MA5_DEAD_CROSS as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_MA5,SELL_MA5
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_ma5    number;
    v_temp_stock_sell_ma5   number;
    -- 1表示close_price>ma5的阶段，2表示close_price<=ma5的阶段
    v_start_track_dead_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_init_last_acc_profit_loss    := 100;
      v_start_track_dead_cross_stage := 0;
    
      for j in cur_single_stock loop
        if j.close_price < j.ma5 then
          if v_start_track_dead_cross_stage = 0 then
            -- 卖出
            v_temp_stock_code              := j.code_;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_stock_sell_ma5          := j.ma5;
            v_start_track_dead_cross_stage := 1;
          else
            continue;
          end if;
        end if;
      
        if j.close_price >= j.ma5 then
          if v_start_track_dead_cross_stage = 1 then
            -- 买入
            v_temp_stock_buy_date       := j.date_;
            v_temp_stock_buy_price      := j.close_price;
            v_temp_stock_buy_ma5        := j.ma5;
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_sell_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
            -- 插入记录
            insert into MDL_CLOSE_PRICE_MA5_DEAD_CROSS
              (buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_ma5,
               sell_ma5)
            values
              (v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_ma5,
               v_temp_stock_sell_ma5);
          
            -- 重置为0，表示还没有买入
            v_start_track_dead_cross_stage := 0;
          else
            continue;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_C_P_MA5_DEAD_CROSS;

  /*---------------------------------- 增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据 ------------------------------------*/
  procedure CAL_MDL_C_P_MA5_D_C_INCR(p_end_date in varchar2) as
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_MA5,SELL_MA5
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_ma5    number;
    v_temp_stock_sell_ma5   number;
    -- 1表示close_price>ma5的阶段，0表示close_price<=ma5的阶段
    v_start_track_dead_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 从某一只股票在mdl_close_price_ma5_dead_cross表中最后的买出日期，到参数p_end_date之间的交易记录，
    -- 并按升序排列。这段时间内可能存在先是close_price死叉ma5，然后close_price金叉ma5。
    -- buy_date和sell_date效果一样
    cursor cur_c_p_gold_dead_cross_ma5 is
      select *
        from stock_transaction_data_all std
       where std.code_ = v_stock_code
         and std.date_ >
             (select max(mcpmdc.buy_date)
                from mdl_close_price_ma5_dead_cross mcpmdc
               where mcpmdc.stock_code = v_stock_code)
         and std.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
       order by std.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_start_track_dead_cross_stage := 0;
      -- 查找最近的accumulative_profit_loss
      begin
        select b.accumulative_profit_loss
          into v_init_last_acc_profit_loss
          from (select *
                  from mdl_close_price_ma5_dead_cross t
                 where t.stock_code = v_stock_code
                 order by t.buy_date desc) b
         where rownum <= 1;
      exception
        when no_data_found then
          dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_close_price_ma5_dead_cross表中没有记录');
      end;
    
      for j in cur_c_p_gold_dead_cross_ma5 loop
        if j.close_price < j.ma5 then
          -- 买入
          if v_start_track_dead_cross_stage = 0 then
            v_temp_stock_code              := j.code_;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_stock_sell_ma5          := j.ma5;
            v_start_track_dead_cross_stage := 1;
          else
            continue;
          end if;
        end if;
      
        if j.close_price >= j.ma5 then
          -- 卖出
          if v_start_track_dead_cross_stage = 1 then
            v_temp_stock_buy_date  := j.date_;
            v_temp_stock_buy_price := j.close_price;
            v_temp_stock_buy_ma5   := j.ma5;
          
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_sell_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_CLOSE_PRICE_MA5_DEAD_CROSS
              (buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_ma5,
               sell_ma5)
            values
              (v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_ma5,
               v_temp_stock_sell_ma5);
          
            v_start_track_dead_cross_stage := 0;
          else
            continue;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_C_P_MA5_D_C_INCR;

  /*---------------------------------- 海量地向表MDL_HEI_KIN_ASHI_UP_DOWN中插入数据 --------------------------------------*/
  procedure CAL_MDL_HEI_KIN_ASHI_UP_DOWN as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_h_k_a_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                := i.code_;
      v_init_last_acc_profit_loss := 100;
      v_start_track_h_k_a_stage   := 0;
    
      for j in cur_single_stock loop
      
        if j.ha_close_price <= j.ha_open_price then
          -- 卖出
          if v_start_track_h_k_a_stage = 1 then
            v_start_track_h_k_a_stage   := 0;
            v_temp_stock_sell_date      := j.date_;
            v_temp_stock_sell_price     := j.close_price;
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_hei_kin_ashi_up_down
              (stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.ha_close_price > j.ha_open_price then
          -- 买入
          if v_start_track_h_k_a_stage != 1 then
            v_start_track_h_k_a_stage := 1;
            v_temp_stock_buy_date     := j.date_;
            v_temp_stock_buy_price    := j.close_price;
            v_temp_stock_code         := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_HEI_KIN_ASHI_UP_DOWN;

  /*---------------------------------- 增量地向表CAL_MDL_HEI_KIN_ASHI_UP_DOWN中插入数据 ------------------------------------*/
  procedure CAL_MDL_H_K_A_UP_DOWN_INCR(p_end_date in varchar2) as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,UY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_MA5,SELL_MA5
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 1表示ha_close_price阳线的阶段，0表示ha_close_price阴线的阶段
    v_start_track_gold_cross_stage number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 从某一只股票在mdl_hei_kin_ashi_up_down表中最后的卖出日期，到参数p_end_date之间的交易记录，
    -- 并按升序排列。这段时间内可能存在先是ha_close_price阳线，然后ha_close_price阴线。
    -- buy_date和sell_date效果一样
    cursor cur_hei_kin_ashi_up_down is
      select *
        from stock_transaction_data_all std
       where std.code_ = v_stock_code
         and std.date_ >
             (select max(mhkaud.sell_date)
                from mdl_hei_kin_ashi_up_down mhkaud
               where mhkaud.stock_code = v_stock_code)
         and std.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
       order by std.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_start_track_gold_cross_stage := 0;
    
      -- 查找最近的accumulative_profit_loss
      begin
        select b.accumulative_profit_loss
          into v_init_last_acc_profit_loss
          from (select *
                  from mdl_hei_kin_ashi_up_down t
                 where t.stock_code = v_stock_code
                 order by t.sell_date desc) b
         where rownum <= 1;
      exception
        when no_data_found then
          dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_hei_kin_ashi_up_down表中没有记录');
      end;
    
      for j in cur_hei_kin_ashi_up_down loop
        if j.ha_close_price > j.ha_open_price then
          -- 买入
          if v_start_track_gold_cross_stage = 0 then
            v_temp_stock_code              := j.code_;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_start_track_gold_cross_stage := 1;
          else
            continue;
          end if;
        end if;
      
        if j.ha_close_price <= j.ha_open_price then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_temp_stock_sell_date  := j.date_;
            v_temp_stock_sell_price := j.close_price;
          
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into mdl_hei_kin_ashi_up_down
              (buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss)
            values
              (v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss);
          
            v_start_track_gold_cross_stage := 0;
          else
            continue;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_H_K_A_UP_DOWN_INCR;

  /*---------------------------------- 海量地向表MDL_HEI_KIN_ASHI_DOWN_UP中插入数据 --------------------------------------*/
  procedure CAL_MDL_HEI_KIN_ASHI_DOWN_UP as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_h_k_a_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                := i.code_;
      v_init_last_acc_profit_loss := 100;
      v_start_track_h_k_a_stage   := 0;
    
      for j in cur_single_stock loop
      
        if j.ha_close_price >= j.ha_open_price then
          -- 买入
          if v_start_track_h_k_a_stage = 1 then
            v_start_track_h_k_a_stage   := 0;
            v_temp_stock_buy_date       := j.date_;
            v_temp_stock_buy_price      := j.close_price;
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_sell_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_hei_kin_ashi_down_up
              (stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.ha_close_price < j.ha_open_price then
          -- 卖出
          if v_start_track_h_k_a_stage != 1 then
            v_start_track_h_k_a_stage := 1;
            v_temp_stock_sell_date    := j.date_;
            v_temp_stock_sell_price   := j.close_price;
            v_temp_stock_code         := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_HEI_KIN_ASHI_DOWN_UP;

  /*---------------------------------- 增量地向表CAL_MDL_HEI_KIN_ASHI_DOWN_UP中插入数据 ------------------------------------*/
  procedure CAL_MDL_H_K_A_DOWN_UP_INCR(p_end_date in varchar2) as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,UY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_MA5,SELL_MA5
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 1表示ha_close_price阳线的阶段，0表示ha_close_price阴线的阶段
    v_start_track_dead_cross_stage number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 从某一只股票在mdl_hei_kin_ashi_down_up表中最后的买入日期，到参数p_end_date之间的交易记录，
    -- 并按升序排列。这段时间内可能存在先是ha_close_price阴线，然后ha_close_price阳线。
    -- buy_date和sell_date效果一样
    cursor cur_hei_kin_ashi_down_up is
      select *
        from stock_transaction_data_all std
       where std.code_ = v_stock_code
         and std.date_ >
             (select max(mhkadu.buy_date)
                from mdl_hei_kin_ashi_down_up mhkadu
               where mhkadu.stock_code = v_stock_code)
         and std.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
       order by std.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_start_track_dead_cross_stage := 0;
    
      -- 查找最近的accumulative_profit_loss
      begin
        select b.accumulative_profit_loss
          into v_init_last_acc_profit_loss
          from (select *
                  from mdl_hei_kin_ashi_down_up t
                 where t.stock_code = v_stock_code
                 order by t.buy_date desc) b
         where rownum <= 1;
      exception
        when no_data_found then
          dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_hei_kin_ashi_down_up表中没有记录');
      end;
    
      for j in cur_hei_kin_ashi_down_up loop
        if j.ha_close_price < j.ha_open_price then
          -- 卖出
          if v_start_track_dead_cross_stage = 0 then
            v_temp_stock_code              := j.code_;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_start_track_dead_cross_stage := 1;
          else
            continue;
          end if;
        end if;
      
        if j.ha_close_price >= j.ha_open_price then
          -- 买入
          if v_start_track_dead_cross_stage = 1 then
            v_temp_stock_buy_date  := j.date_;
            v_temp_stock_buy_price := j.close_price;
          
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_sell_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into mdl_hei_kin_ashi_down_up
              (buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss)
            values
              (v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss);
          
            v_start_track_dead_cross_stage := 0;
          else
            continue;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_H_K_A_DOWN_UP_INCR;

  /*---------------------------------- 海量地向表MDL_KD_GOLD_CROSS中插入数据 --------------------------------------*/
  procedure CAL_MDL_KD_GOLD_CROSS as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,K,D
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_k      number;
    v_temp_stock_sell_k     number;
    v_temp_stock_buy_d      number;
    v_temp_stock_sell_d     number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_kd_g_c_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                := i.code_;
      v_init_last_acc_profit_loss := 100;
      v_start_track_kd_g_c_stage  := 0;
    
      for j in cur_single_stock loop
      
        if j.k <= j.d then
          -- 卖出
          if v_start_track_kd_g_c_stage = 1 then
            v_start_track_kd_g_c_stage  := 0;
            v_temp_stock_sell_date      := j.date_;
            v_temp_stock_sell_price     := j.close_price;
            v_temp_stock_sell_k         := j.k;
            v_temp_stock_sell_d         := j.d;
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_kd_gold_cross
              (stock_code,
               sell_date,
               sell_price,
               sell_k,
               sell_d,
               buy_date,
               buy_price,
               buy_k,
               buy_d,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_k,
               v_temp_stock_sell_d,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_k,
               v_temp_stock_buy_d,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.k > j.d then
          -- 买入
          if v_start_track_kd_g_c_stage != 1 then
            v_start_track_kd_g_c_stage := 1;
            v_temp_stock_buy_date      := j.date_;
            v_temp_stock_buy_price     := j.close_price;
            v_temp_stock_buy_k         := j.k;
            v_temp_stock_buy_d         := j.d;
            v_temp_stock_code          := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_KD_GOLD_CROSS;

  /*---------------------------------- 增量地向表MDL_KD_GOLD_CROSS中插入数据 ------------------------------------*/
  procedure CAL_MDL_KD_GOLD_CROSS_INCR(p_end_date in varchar2) as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    -- v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断KD是否发生了金叉或死叉
    v_first_stock stock_transaction_data%rowtype;
    -- 表示两天记录中的第二天，用于判断KD是否发生了金叉或死叉
    v_second_stock stock_transaction_data%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_K,BUY_D,SELL_K,SELL_D
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_k      number;
    v_temp_stock_buy_d      number;
    v_temp_stock_sell_k     number;
    v_temp_stock_sell_d     number;
    -- 用来判断现在是否是k>d的阶段，从而可以判断死叉
    v_start_track_gold_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found  number := 0;
    v_death_cross_found number := 0;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录。
    -- 1997-01-01是stock_transaction_data表_all中最早的交易记录。
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t1
       where t1.date_ >
             (select nvl(max(t.sell_date),
                         to_date('1997-01-01', 'yyyy-mm-dd'))
                from mdl_kd_gold_cross t
               where t.stock_code = lower(v_stock_code))
         and t1.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
         and t1.code_ = lower(v_stock_code)
       order by t1.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    -- v_model_id := 1;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是k>d，那么接下来只有可能先出现死叉，所以要判断：j.k<j.d，也就是说如果第一段数据是k>d，则将其忽略
        -- 之后在确认了金叉后，需要确认死叉，所以引入了表示变量v_start_track_gold_cross_stage，当其为1时，表示接下来只可能出现死叉
        if j.k < j.d or (j.k > j.d and v_start_track_gold_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.k < v_first_stock.d and
             v_second_stock.k > v_second_stock.d and
             v_start_track_gold_cross_stage = 0 then
            v_temp_stock_code      := v_second_stock.code_;
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_k     := v_second_stock.k;
            v_temp_stock_buy_d     := v_second_stock.d;
            v_gold_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_gold_cross_stage设置为1
            v_start_track_gold_cross_stage := 1;
          
          end if;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.k > v_first_stock.d and
             v_second_stock.k < v_second_stock.d and
             v_start_track_gold_cross_stage = 1 then
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_k     := v_second_stock.k;
            v_temp_stock_sell_d     := v_second_stock.d;
            v_death_cross_found     := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_gold_cross_stage设置为0
            v_start_track_gold_cross_stage := 0;
          
          end if;
          -- 插入数据
          if v_gold_cross_found = 1 and v_death_cross_found = 1 and
            /*v_model_id is not null and*/
             v_temp_stock_buy_date is not null and
             v_temp_stock_sell_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss := round((v_temp_stock_sell_price -
                                        v_temp_stock_buy_price) /
                                        v_temp_stock_buy_price,
                                        4) * 100;
            -- 获取最近一次交易后的accumulative_profit_loss
            begin
              select t.accumulative_profit_loss
                into v_init_last_acc_profit_loss
                from (select *
                        from mdl_kd_gold_cross t
                       where t.stock_code = v_stock_code
                       order by t.sell_date desc) t
               where rownum <= 1;
            exception
              when no_data_found then
                dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_macd_gold_cross表中没有记录');
            end;
            -- 计算本次交易后的accumulative_profit_loss
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_KD_GOLD_CROSS
              ( /*model_id,*/buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_k,
               buy_d,
               sell_k,
               sell_d)
            values
              ( /*v_model_id,*/v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_k,
               v_temp_stock_buy_d,
               v_temp_stock_sell_k,
               v_temp_stock_sell_d);
            commit;
          
            v_gold_cross_found  := 0;
            v_death_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
  end CAL_MDL_KD_GOLD_CROSS_INCR;

  /*---------------------------------- 海量地向表MDL_KD_DEAD_CROSS中插入数据 --------------------------------------*/
  procedure CAL_MDL_KD_DEAD_CROSS as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,K,D
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_k      number;
    v_temp_stock_sell_k     number;
    v_temp_stock_buy_d      number;
    v_temp_stock_sell_d     number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_kd_d_c_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                := i.code_;
      v_init_last_acc_profit_loss := 100;
      v_start_track_kd_d_c_stage  := 0;
    
      for j in cur_single_stock loop
      
        if j.k >= j.d then
          -- 买入
          if v_start_track_kd_d_c_stage = 1 then
            v_start_track_kd_d_c_stage  := 0;
            v_temp_stock_buy_date       := j.date_;
            v_temp_stock_buy_price      := j.close_price;
            v_temp_stock_buy_k          := j.k;
            v_temp_stock_buy_d          := j.d;
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_sell_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_kd_dead_cross
              (stock_code,
               sell_date,
               sell_price,
               sell_k,
               sell_d,
               buy_date,
               buy_price,
               buy_k,
               buy_d,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_k,
               v_temp_stock_sell_d,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_k,
               v_temp_stock_buy_d,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.k < j.d then
          -- 卖出
          if v_start_track_kd_d_c_stage != 1 then
            v_start_track_kd_d_c_stage := 1;
            v_temp_stock_sell_date     := j.date_;
            v_temp_stock_sell_price    := j.close_price;
            v_temp_stock_sell_k        := j.k;
            v_temp_stock_sell_d        := j.d;
            v_temp_stock_code          := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_KD_DEAD_CROSS;

  /*---------------------------------- 增量地向表MDL_KD_DEAD_CROSS中插入数据 ------------------------------------*/
  procedure CAL_MDL_KD_DEAD_CROSS_INCR(p_end_date in varchar2) as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    -- v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断KD是否发生了金叉或死叉
    v_first_stock stock_transaction_data%rowtype;
    -- 表示两天记录中的第二天，用于判断KD是否发生了金叉或死叉
    v_second_stock stock_transaction_data%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_K,BUY_D,SELL_K,SELL_D
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_k      number;
    v_temp_stock_buy_d      number;
    v_temp_stock_sell_k     number;
    v_temp_stock_sell_d     number;
    -- 用来判断现在是否是k<d的阶段，从而可以判断死叉
    v_start_track_dead_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found number := 0;
    v_dead_cross_found number := 0;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录。
    -- 1997-01-01是stock_transaction_data表_all中最早的交易记录。
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t1
       where t1.date_ >
             (select nvl(max(t.buy_date),
                         to_date('1997-01-01', 'yyyy-mm-dd'))
                from mdl_kd_dead_cross t
               where t.stock_code = lower(v_stock_code))
         and t1.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
         and t1.code_ = lower(v_stock_code)
       order by t1.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    -- v_model_id := 1;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_dead_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是k<d，那么接下来只有可能先出现金叉，所以要判断：j.k>j.d，也就是说如果第一段数据是k<d，则将其忽略
        -- 之后在确认了死叉后，需要确认金叉，所以引入了表示变量v_start_track_dead_cross_stage，当其为1时，表示接下来只可能出现金叉
        if j.k > j.d or (j.k < j.d and v_start_track_dead_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.k > v_first_stock.d and
             v_second_stock.k < v_second_stock.d and
             v_start_track_dead_cross_stage = 0 then
            v_temp_stock_code       := v_second_stock.code_;
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_k     := v_second_stock.k;
            v_temp_stock_sell_d     := v_second_stock.d;
            v_gold_cross_found      := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_dead_cross_stage设置为1
            v_start_track_dead_cross_stage := 1;
          
          end if;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.k < v_first_stock.d and
             v_second_stock.k > v_second_stock.d and
             v_start_track_dead_cross_stage = 1 then
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_k     := v_second_stock.k;
            v_temp_stock_buy_d     := v_second_stock.d;
            v_dead_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_dead_cross_stage设置为0
            v_start_track_dead_cross_stage := 0;
          
          end if;
          -- 插入数据
          if v_gold_cross_found = 1 and v_dead_cross_found = 1 and
            /*v_model_id is not null and*/
             v_temp_stock_sell_date is not null and
             v_temp_stock_buy_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss := round((v_temp_stock_sell_price -
                                        v_temp_stock_buy_price) /
                                        v_temp_stock_sell_price,
                                        4) * 100;
            -- 获取最近一次交易后的accumulative_profit_loss
            begin
              select t.accumulative_profit_loss
                into v_init_last_acc_profit_loss
                from (select *
                        from mdl_kd_dead_cross t
                       where t.stock_code = v_stock_code
                       order by t.buy_date desc) t
               where rownum <= 1;
            exception
              when no_data_found then
                dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在mdl_macd_dead_cross表中没有记录');
            end;
            -- 计算本次交易后的accumulative_profit_loss
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
          
            insert into MDL_KD_DEAD_CROSS
              ( /*model_id,*/buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_k,
               buy_d,
               sell_k,
               sell_d)
            values
              ( /*v_model_id,*/v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_k,
               v_temp_stock_buy_d,
               v_temp_stock_sell_k,
               v_temp_stock_sell_d);
          
            v_gold_cross_found := 0;
            v_dead_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_KD_DEAD_CROSS_INCR;

  /*----------- 海量地向表MDL_BULL_SHORT_LINE_UP中插入数据 --------------*/
  procedure CAL_MDL_BULL_SHORT_LINE_UP as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 判断哪根均线作为牛熊线时，乖离率的阈值
    v_bias_top_threshold    number := 10;
    v_bias_bottom_threshold number := -15;
    -- 当前交易日期的牛熊线
    v_cur_bull_short_line varchar2(11);
    -- 当前交易日期的牛熊线的收盘价
    v_cur_bull_short_line_price number;
    -- 前一个交易日期的牛熊线
    v_last_bull_short_line varchar2(11);
    -- 前一个交易日期的牛熊线的收盘价
    v_last_bull_short_line_price number;
    -- 当前记录
    row_current_stda stock_transaction_data_all%rowtype;
    -- 前一条记录
    row_last_stda stock_transaction_data_all%rowtype;
    -- 是否是某只股票的第一条记录，1表示是，-1表示否
    v_is_first_row number := 1;
    -- 表示是否出现了金叉，1表示是，-1表示否
    v_is_gold_cross number := -1;
    -- 表示是否出现了死叉，1表示是，-1表示否
    v_is_dead_cross number := -1;
    -- 买入价格、买入日期、卖出价格、卖出日期
    v_buy_price  number;
    v_buy_date   date;
    v_sell_price number;
    v_sell_date  date;
    -- 买入时牛熊线和牛熊线价格，卖出时牛熊线和牛熊线价格
    v_b_s_l_when_buy        varchar2(11);
    v_b_s_l_price_when_buy  number;
    v_b_s_l_when_sell       varchar2(11);
    v_b_s_l_price_when_sell number;
    -- 收益率
    v_profit_loss number;
    -- 累计收益率
    v_init_last_acc_profit_loss number;
    -- 所有股票的stock_code
    cursor cur_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    --where t.code_ = '600971';
    -- 某一只股票的全部交易记录，按升序排列，ma250和bias250不能为空
    cursor cur_stock_transaction_data_all is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
      --and t.date_ >= to_date('2008-1-30', 'yyyy-mm-dd')
       order by t.date_ asc;
  begin
    for i in cur_stock_code loop
      v_stock_code := i.code_;
    
      -- 重置
      v_is_first_row              := 1;
      v_init_last_acc_profit_loss := 100;
    
      for j in cur_stock_transaction_data_all loop
      
        -- 如果是第一条记录，则跳过
        if v_is_first_row = 1 then
          v_is_first_row := -1;
          row_last_stda  := j;
          continue;
        else
          row_current_stda := j;
        end if;
      
        -- 判断前一个交易日期的牛熊线
        if row_last_stda.bias250 <= v_bias_top_threshold and
           row_last_stda.bias250 >= v_bias_bottom_threshold then
          v_last_bull_short_line       := 'ma250';
          v_last_bull_short_line_price := row_last_stda.ma250;
        elsif row_last_stda.bias120 <= v_bias_top_threshold and
              row_last_stda.bias120 >= v_bias_bottom_threshold then
          v_last_bull_short_line       := 'ma120';
          v_last_bull_short_line_price := row_last_stda.ma120;
        elsif row_last_stda.bias60 <= v_bias_top_threshold and
              row_last_stda.bias60 >= v_bias_bottom_threshold then
          v_last_bull_short_line       := 'ma60';
          v_last_bull_short_line_price := row_last_stda.ma60;
        elsif row_last_stda.bias20 <= v_bias_top_threshold and
              row_last_stda.bias20 >= v_bias_bottom_threshold then
          v_last_bull_short_line       := 'ma20';
          v_last_bull_short_line_price := row_last_stda.ma20;
        elsif row_last_stda.bias10 <= v_bias_top_threshold and
              row_last_stda.bias10 >= v_bias_bottom_threshold then
          v_last_bull_short_line       := 'ma10';
          v_last_bull_short_line_price := row_last_stda.ma10;
        elsif row_last_stda.bias5 <= v_bias_top_threshold and
              row_last_stda.bias5 >= v_bias_bottom_threshold then
          v_last_bull_short_line       := 'ma5';
          v_last_bull_short_line_price := row_last_stda.ma5;
        else
          v_last_bull_short_line       := 'ma2';
          v_last_bull_short_line_price := row_last_stda.last_close_price;
        end if;
      
        -- 判断当前交易日期的牛熊线
        if row_current_stda.bias250 <= v_bias_top_threshold and
           row_current_stda.bias250 >= v_bias_bottom_threshold then
          v_cur_bull_short_line       := 'ma250';
          v_cur_bull_short_line_price := row_current_stda.ma250;
        elsif row_current_stda.bias120 <= v_bias_top_threshold and
              row_current_stda.bias120 >= v_bias_bottom_threshold then
          v_cur_bull_short_line       := 'ma120';
          v_cur_bull_short_line_price := row_current_stda.ma120;
        elsif row_current_stda.bias60 <= v_bias_top_threshold and
              row_current_stda.bias60 >= v_bias_bottom_threshold then
          v_cur_bull_short_line       := 'ma60';
          v_cur_bull_short_line_price := row_current_stda.ma60;
        elsif row_current_stda.bias20 <= v_bias_top_threshold and
              row_current_stda.bias20 >= v_bias_bottom_threshold then
          v_cur_bull_short_line       := 'ma20';
          v_cur_bull_short_line_price := row_current_stda.ma20;
        elsif row_current_stda.bias10 <= v_bias_top_threshold and
              row_current_stda.bias10 >= v_bias_bottom_threshold then
          v_cur_bull_short_line       := 'ma10';
          v_cur_bull_short_line_price := row_current_stda.ma10;
        elsif row_current_stda.bias5 <= v_bias_top_threshold and
              row_current_stda.bias5 >= v_bias_bottom_threshold then
          v_cur_bull_short_line       := 'ma5';
          v_cur_bull_short_line_price := row_current_stda.ma5;
        else
          v_cur_bull_short_line       := 'ma2';
          v_cur_bull_short_line_price := row_current_stda.last_close_price;
        end if;
      
        -- 在金叉还没出现的情况下，前一个交易日，收盘价是否在牛熊线之下，当前交易日，收盘价是否在牛熊线之上
        if v_is_gold_cross = -1 and
           row_last_stda.close_price < v_last_bull_short_line_price and
           row_current_stda.close_price >= v_cur_bull_short_line_price then
          v_is_gold_cross        := 1;
          v_buy_price            := row_current_stda.close_price;
          v_buy_date             := row_current_stda.date_;
          v_b_s_l_when_buy       := v_cur_bull_short_line;
          v_b_s_l_price_when_buy := v_cur_bull_short_line_price;
        end if;
      
        -- 必须在金叉已经出现的情况下，才判断当前交易日，收盘价是否在牛熊线之上，当前交易日，收盘价是否在牛熊线之下
        if v_is_gold_cross = 1 and v_is_dead_cross = -1 and
           row_last_stda.close_price >= v_last_bull_short_line_price and
           row_current_stda.close_price < v_cur_bull_short_line_price then
          v_is_dead_cross         := 1;
          v_sell_price            := row_current_stda.close_price;
          v_sell_date             := row_current_stda.date_;
          v_b_s_l_when_sell       := v_cur_bull_short_line;
          v_b_s_l_price_when_sell := v_cur_bull_short_line_price;
        end if;
      
        -- 插入记录
        if v_is_gold_cross = 1 and v_is_dead_cross = 1 then
          v_profit_loss               := (v_sell_price - v_buy_price) /
                                         v_buy_price * 100;
          v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                         (1 + v_profit_loss / 100);
          insert into mdl_bull_short_line_up
            (stock_code,
             buy_date,
             buy_price,
             sell_date,
             sell_price,
             b_s_l_when_buy,
             b_s_l_price_when_buy,
             b_s_l_when_sell,
             b_s_l_price_when_sell,
             profit_loss,
             accumulative_profit_loss)
          values
            (row_current_stda.code_,
             v_buy_date,
             v_buy_price,
             v_sell_date,
             v_sell_price,
             v_b_s_l_when_buy,
             v_b_s_l_price_when_buy,
             v_b_s_l_when_sell,
             v_b_s_l_price_when_sell,
             v_profit_loss,
             v_init_last_acc_profit_loss);
        
          -- 重置
          v_is_gold_cross := -1;
          v_is_dead_cross := -1;
        end if;
      
        -- 将当前记录赋值给前一条记录
        row_last_stda := row_current_stda;
      end loop;
    
    end loop;
    commit;
  end CAL_MDL_BULL_SHORT_LINE_UP;

  /*----------- 增量地向表MDL_BULL_SHORT_LINE_UP中插入数据 --------------*/
  procedure CAL_MDL_B_S_LINE_UP_INCR(p_date in varchar2) as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 表示某只股票的最大卖出日期
    v_max_sell_date date;
    -- 判断哪根均线作为牛熊线时，乖离率的阈值
    v_bias_top_threshold    number := 10;
    v_bias_bottom_threshold number := -15;
    -- 表示出现金叉之前的一个交易日
    v_gold_cross_previous_stda stock_transaction_data_all%rowtype;
    -- 表示出现金叉的交易日
    v_gold_cross_current_stda stock_transaction_data_all%rowtype;
    -- 表示出现死叉之前的一个交易日
    v_dead_cross_previous_stda stock_transaction_data_all%rowtype;
    -- 表示出现死叉的交易日
    v_dead_cross_current_stda stock_transaction_data_all%rowtype;
    -- 金叉之前的牛熊线
    v_g_c_previous_b_s_line number;
    -- 金叉时的牛熊线
    v_g_c_current_b_s_line number;
    -- 死叉之前的牛熊线
    --v_d_c_previous_b_s_line number;
    -- 死叉时的牛熊线
    --v_d_c_current_b_s_line number;
    -- 金叉是否已经出现，1表示出现，-1表示未出现
    v_gold_cross number := -1;
    -- 死叉是否已经出现，1表示出现，-1表示未出现
    v_dead_cross number := -1;
    -- 买入价格、买入日期、卖出价格、卖出日期
    v_buy_price  number;
    v_buy_date   date;
    v_sell_price number;
    v_sell_date  date;
    -- 买入时牛熊线和牛熊线价格，卖出时牛熊线和牛熊线价格
    v_b_s_l_when_buy        varchar2(11);
    v_b_s_l_price_when_buy  number;
    v_b_s_l_when_sell       varchar2(11);
    v_b_s_l_price_when_sell number;
    v_temp_b_s_l            varchar2(11);
    --v_temp_b_s_l_price      number;
    -- 收益率
    v_profit_loss number;
    -- 累计收益率
    v_init_last_acc_profit_loss number;
    -- 所有股票的stock_code
    cursor cur_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 某只股票，在上次交易日期后的记录，按日期升序排列
    cursor cur_later_transaction_stda is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
         and t.date_ > v_max_sell_date
         and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
       order by t.date_ asc;
  begin
    for i in cur_stock_code loop
      v_stock_code := i.code_;
    
      -- 重置
      v_init_last_acc_profit_loss := 100;
    
      -- 某只股票的最大卖出日期
      begin
        select max(t.sell_date)
          into v_max_sell_date
          from mdl_bull_short_line_up t
         where t.stock_code = v_stock_code;
      exception
        when no_data_found then
          dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在MDL_BULL_SHORT_LINE_UP表中没有最大的卖出日期');
          continue;
      end;
    
      -- 重置
      v_gold_cross_previous_stda := null;
      v_gold_cross_current_stda  := null;
      v_dead_cross_previous_stda := null;
      v_dead_cross_current_stda  := null;
      v_gold_cross               := -1;
      v_dead_cross               := -1;
    
      for j in cur_later_transaction_stda loop
        -- 如果是第一条记录
        if v_gold_cross_previous_stda.code_ is null then
          v_gold_cross_previous_stda := j;
          continue;
        end if;
      
        -- 判断前一天的牛熊线
        if v_gold_cross_previous_stda.bias250 < v_bias_top_threshold and
           v_bias_bottom_threshold < v_gold_cross_previous_stda.bias250 then
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.ma250;
        elsif v_gold_cross_previous_stda.bias120 < v_bias_top_threshold and
              v_bias_bottom_threshold < v_gold_cross_previous_stda.bias120 then
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.ma120;
        elsif v_gold_cross_previous_stda.bias60 < v_bias_top_threshold and
              v_bias_bottom_threshold < v_gold_cross_previous_stda.bias60 then
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.ma60;
        elsif v_gold_cross_previous_stda.bias20 < v_bias_top_threshold and
              v_bias_bottom_threshold < v_gold_cross_previous_stda.bias20 then
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.ma20;
        elsif v_gold_cross_previous_stda.bias10 < v_bias_top_threshold and
              v_bias_bottom_threshold < v_gold_cross_previous_stda.bias10 then
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.ma10;
        elsif v_gold_cross_previous_stda.bias5 < v_bias_top_threshold and
              v_bias_bottom_threshold < v_gold_cross_previous_stda.bias5 then
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.ma5;
        else
          v_g_c_previous_b_s_line := v_gold_cross_previous_stda.close_price;
        end if;
      
        -- 判断当前的牛熊线
        if j.bias250 < v_bias_top_threshold and
           v_bias_bottom_threshold < j.bias250 then
          v_g_c_current_b_s_line := j.ma250;
          v_temp_b_s_l           := 'ma250';
        elsif j.bias120 < v_bias_top_threshold and
              v_bias_bottom_threshold < j.bias120 then
          v_g_c_current_b_s_line := j.ma120;
          v_temp_b_s_l           := 'ma120';
        elsif j.bias60 < v_bias_top_threshold and
              v_bias_bottom_threshold < j.bias60 then
          v_g_c_current_b_s_line := j.ma60;
          v_temp_b_s_l           := 'm60';
        elsif j.bias20 < v_bias_top_threshold and
              v_bias_bottom_threshold < j.bias20 then
          v_g_c_current_b_s_line := j.ma20;
          v_temp_b_s_l           := 'ma20';
        elsif j.bias10 < v_bias_top_threshold and
              v_bias_bottom_threshold < j.bias10 then
          v_g_c_current_b_s_line := j.ma10;
          v_temp_b_s_l           := 'ma10';
        elsif j.bias5 < v_bias_top_threshold and
              v_bias_bottom_threshold < j.bias5 then
          v_g_c_current_b_s_line := j.ma5;
          v_temp_b_s_l           := 'ma5';
        else
          v_g_c_current_b_s_line := j.last_close_price;
          v_temp_b_s_l           := 'ma2';
        end if;
      
        -- 判断金叉是否出现
        if v_gold_cross = -1 and v_dead_cross = -1 then
          if v_gold_cross_previous_stda.close_price <
             v_g_c_previous_b_s_line and
             j.close_price >= v_g_c_current_b_s_line then
            v_gold_cross           := 1;
            v_buy_price            := j.close_price;
            v_buy_date             := j.date_;
            v_b_s_l_when_buy       := v_temp_b_s_l;
            v_b_s_l_price_when_buy := v_g_c_current_b_s_line;
          end if;
        end if;
      
        -- 判断金叉、死叉是否出现
        if v_gold_cross = 1 and v_dead_cross = -1 then
          if v_gold_cross_previous_stda.close_price >=
             v_g_c_previous_b_s_line and
             j.close_price < v_g_c_current_b_s_line then
            v_dead_cross            := 1;
            v_sell_price            := j.close_price;
            v_sell_date             := j.date_;
            v_b_s_l_when_sell       := v_temp_b_s_l;
            v_b_s_l_price_when_sell := v_g_c_current_b_s_line;
          
            v_profit_loss               := (v_sell_price - v_buy_price) /
                                           v_buy_price * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_profit_loss / 100);
            -- 插入记录
            insert into mdl_bull_short_line_up
              (stock_code,
               buy_date,
               buy_price,
               sell_date,
               sell_price,
               b_s_l_when_buy,
               b_s_l_price_when_buy,
               b_s_l_when_sell,
               b_s_l_price_when_sell,
               profit_loss,
               accumulative_profit_loss)
            values
              (j.code_,
               v_buy_date,
               v_buy_price,
               v_sell_date,
               v_sell_price,
               v_b_s_l_when_buy,
               v_b_s_l_price_when_buy,
               v_b_s_l_when_sell,
               v_b_s_l_price_when_sell,
               v_profit_loss,
               v_init_last_acc_profit_loss);
          
            -- 重置
            v_gold_cross := 1;
            v_dead_cross := 1;
          
          end if;
        end if;
      
        v_gold_cross_previous_stda := j;
      
      end loop;
    end loop;
    commit;
  end CAL_MDL_B_S_LINE_UP_INCR;

  /*----------- 海量地向表MDL_UP_DOWN_PERCENTAGE_MA_GOLD_CROSS中插入数据 --------------*/
  procedure CAL_MDL_PERCENTAGE_MA_G_C as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE
    v_temp_stock_code            varchar2(100);
    v_temp_stock_buy_date        date;
    v_temp_stock_sell_date       date;
    v_temp_stock_buy_price       number;
    v_temp_stock_sell_price      number;
    v_temp_stock_buy_percentage  number;
    v_temp_stock_sell_percentage number;
    v_temp_stock_buy_ma          number;
    v_temp_stock_sell_ma         number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_p_ma_g_c_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from mdl_top_stock_detail t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from mdl_top_stock_detail t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    -- up_down_percentage_21_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_21 <= j.percentage_21_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_21;
            v_temp_stock_sell_ma         := j.percentage_21_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_21_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_21 > j.percentage_21_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_21;
            v_temp_stock_buy_ma         := j.percentage_21_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_21_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_21 <= j.percentage_21_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_21;
            v_temp_stock_sell_ma         := j.percentage_21_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_21_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_21 > j.percentage_21_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_21;
            v_temp_stock_buy_ma         := j.percentage_21_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_21_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_21 <= j.percentage_21_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_21;
            v_temp_stock_sell_ma         := j.percentage_21_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_21_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_21 > j.percentage_21_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_21;
            v_temp_stock_buy_ma         := j.percentage_21_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_34_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_34 <= j.percentage_34_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_34;
            v_temp_stock_sell_ma         := j.percentage_34_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_34_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_34 > j.percentage_34_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_34;
            v_temp_stock_buy_ma         := j.percentage_34_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_34_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_34 <= j.percentage_34_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_34;
            v_temp_stock_sell_ma         := j.percentage_34_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_34_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_34 > j.percentage_34_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_34;
            v_temp_stock_buy_ma         := j.percentage_34_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_34_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_34 <= j.percentage_34_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_34;
            v_temp_stock_sell_ma         := j.percentage_34_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_34_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_34 > j.percentage_34_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_34;
            v_temp_stock_buy_ma         := j.percentage_34_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_55_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_55 <= j.percentage_55_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_55;
            v_temp_stock_sell_ma         := j.percentage_55_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_55_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_55 > j.percentage_55_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_55;
            v_temp_stock_buy_ma         := j.percentage_55_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_55_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_55 <= j.percentage_55_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_55;
            v_temp_stock_sell_ma         := j.percentage_55_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_55_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_55 > j.percentage_55_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_55;
            v_temp_stock_buy_ma         := j.percentage_55_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_55_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_55 <= j.percentage_55_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_55;
            v_temp_stock_sell_ma         := j.percentage_55_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_55_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_55 > j.percentage_55_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_55;
            v_temp_stock_buy_ma         := j.percentage_55_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_89_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_89 <= j.percentage_89_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_89;
            v_temp_stock_sell_ma         := j.percentage_89_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_89_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_89 > j.percentage_89_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_89;
            v_temp_stock_buy_ma         := j.percentage_89_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_89_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_89 <= j.percentage_89_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_89;
            v_temp_stock_sell_ma         := j.percentage_89_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_89_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_89 > j.percentage_89_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_89;
            v_temp_stock_buy_ma         := j.percentage_89_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_89_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_89 <= j.percentage_89_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_89;
            v_temp_stock_sell_ma         := j.percentage_89_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_89_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_89 > j.percentage_89_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_89;
            v_temp_stock_buy_ma         := j.percentage_89_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_144_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_144 <= j.percentage_144_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_144;
            v_temp_stock_sell_ma         := j.percentage_144_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_144_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_144 > j.percentage_144_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_144;
            v_temp_stock_buy_ma         := j.percentage_144_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_144_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_144 <= j.percentage_144_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_144;
            v_temp_stock_sell_ma         := j.percentage_144_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_144_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_144 > j.percentage_144_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_144;
            v_temp_stock_buy_ma         := j.percentage_144_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_144_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_144 <= j.percentage_144_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_144;
            v_temp_stock_sell_ma         := j.percentage_144_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_144_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_144 > j.percentage_144_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_144;
            v_temp_stock_buy_ma         := j.percentage_144_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_233_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_233 <= j.percentage_233_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_233;
            v_temp_stock_sell_ma         := j.percentage_233_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_233_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_233 > j.percentage_233_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_233;
            v_temp_stock_buy_ma         := j.percentage_233_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_233_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_233 <= j.percentage_233_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_233;
            v_temp_stock_sell_ma         := j.percentage_233_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_233_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_233 > j.percentage_233_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_233;
            v_temp_stock_buy_ma         := j.percentage_233_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_233_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_233 <= j.percentage_233_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_233;
            v_temp_stock_sell_ma         := j.percentage_233_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_233_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_233 > j.percentage_233_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_233;
            v_temp_stock_buy_ma         := j.percentage_233_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_377_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_377 <= j.percentage_377_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_377;
            v_temp_stock_sell_ma         := j.percentage_377_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_377_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_377 > j.percentage_377_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_377;
            v_temp_stock_buy_ma         := j.percentage_377_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_377_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_377 <= j.percentage_377_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_377;
            v_temp_stock_sell_ma         := j.percentage_377_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_377_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_377 > j.percentage_377_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_377;
            v_temp_stock_buy_ma         := j.percentage_377_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_377_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_377 <= j.percentage_377_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_377;
            v_temp_stock_sell_ma         := j.percentage_377_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_377_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_377 > j.percentage_377_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_377;
            v_temp_stock_buy_ma         := j.percentage_377_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_610_ma5
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_610 <= j.percentage_610_ma5 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_610;
            v_temp_stock_sell_ma         := j.percentage_610_ma5;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_610_ma5',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_610 > j.percentage_610_ma5 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_610;
            v_temp_stock_buy_ma         := j.percentage_610_ma5;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_610_ma10
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_610 <= j.percentage_610_ma10 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_610;
            v_temp_stock_sell_ma         := j.percentage_610_ma10;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_610_ma10',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_610 > j.percentage_610_ma10 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_610;
            v_temp_stock_buy_ma         := j.percentage_610_ma10;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  
    -- up_down_percentage_610_ma20
    for i in cur_all_stock_code loop
      v_stock_code                 := i.code_;
      v_init_last_acc_profit_loss  := 100;
      v_start_track_p_ma_g_c_stage := 0;
    
      for j in cur_single_stock loop
      
        if j.up_down_percentage_610 <= j.percentage_610_ma20 then
          -- 卖出
          if v_start_track_p_ma_g_c_stage = 1 then
            v_start_track_p_ma_g_c_stage := 0;
            v_temp_stock_sell_date       := j.date_;
            select t.close_price
              into v_temp_stock_sell_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_sell_percentage := j.up_down_percentage_610;
            v_temp_stock_sell_ma         := j.percentage_610_ma20;
            v_temp_profit_loss           := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
            v_init_last_acc_profit_loss  := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          
            -- 插入记录
            insert into mdl_percentage_ma_gold_cross
              (stock_code,
               type_,
               sell_date,
               sell_price,
               sell_up_down_percentage,
               sell_ma,
               buy_date,
               buy_price,
               buy_up_down_percentage,
               buy_ma,
               accumulative_profit_loss,
               profit_loss)
            values
              (v_temp_stock_code,
               'up_down_percentage_610_ma20',
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_sell_percentage,
               v_temp_stock_sell_ma,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_temp_stock_buy_percentage,
               v_temp_stock_buy_ma,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if j.up_down_percentage_610 > j.percentage_610_ma20 then
          -- 买入
          if v_start_track_p_ma_g_c_stage != 1 then
            v_start_track_p_ma_g_c_stage := 1;
            v_temp_stock_buy_date        := j.date_;
            select t.close_price
              into v_temp_stock_buy_price
              from stock_transaction_data_all t
             where t.code_ = j.code_
               and t.date_ = j.date_;
            v_temp_stock_buy_percentage := j.up_down_percentage_610;
            v_temp_stock_buy_ma         := j.percentage_610_ma20;
            v_temp_stock_code           := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_PERCENTAGE_MA_G_C;

  /*---------------------------------- calculate weekend KD's gold cross model --------------------------------------*/
  procedure CAL_MDL_WEEK_KD_GOLD_CROSS as
    -- 表示股票的STOCK_CODE
    v_stock_code varchar2(100);
    -- 由于KD指标的初值均是50，所以用这个变量表示跳过初值所在的记录及之前的记录。1表示已经跳过，0表示还没有跳过
    v_skip_kd_init_value number;
    -- 表示K,D指标。当K<D时，用于存储K,D值；当K>D时，赋值为0
    v_temp_k number;
    v_temp_d number;
    -- 临时变量，用于存储buy_begin_date,buy_end_date,buy_k,buy_d,buy_price
    v_temp_buy_begin_date date;
    v_temp_buy_end_date   date;
    v_temp_buy_k          number;
    v_temp_buy_d          number;
    v_temp_buy_price      number;
    -- 表示是否已经购买了某只股票。没有购买或者已经卖了为0，购买了还没有卖为1
    v_is_buy number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data t;
    -- 获得某一只股票的所有交易记录
    cursor cur_single_stock_week is
      select *
        from stock_week t
       where t.code_ = lower(v_stock_code)
       order by t.begin_date asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code := i.code_;
      -- 为每只股票的KD初值变量赋初值
      v_skip_kd_init_value := 0;
      -- 为每只股票的K，D指标赋初值
      v_temp_k := 0;
      v_temp_d := 0;
      -- 表示某只购票没有购买，或者已经卖了
      v_is_buy := 0;
      -- 初始化 v_init_last_acc_profit_loss
      v_init_last_acc_profit_loss := 100;
      for j in cur_single_stock_week loop
        -- 跳过KD指标为初值的记录及以前的记录
        if v_skip_kd_init_value = 0 and j.k is not null and j.d is not null and
           j.k <> 50 and j.d <> 50 then
          v_skip_kd_init_value := 1;
        end if;
        -- 当某只股票还没有买，并且K小于D时，保留K,D指标
        if v_skip_kd_init_value = 1 and v_is_buy = 0 and j.k < j.d then
          v_temp_k := j.k;
          v_temp_d := j.d;
        end if;
        -- 当某只股票还没有买，并且K大于D时，保留buy_begin_date,buy_end_date,buy_k,buy_d,buy_price，
        -- 并给v_temp_k,v_temp_d和v_is_buy重新赋值
        if v_skip_kd_init_value = 1 and v_is_buy = 0 and v_temp_k <> 0 and
           v_temp_d <> 0 and j.k > j.d then
          -- 当K大于D时，赋值为0
          v_temp_k := 0;
          v_temp_d := 0;
          -- 保存购买某只股票时的buy_begin_date,buy_end_date,buy_k,buy_d,buy_price
          v_temp_buy_begin_date := j.begin_date;
          v_temp_buy_end_date   := j.end_date;
          v_temp_buy_k          := j.k;
          v_temp_buy_d          := j.d;
          v_temp_buy_price      := j.close_price;
          -- 表示某只购票已经卖了
          v_is_buy := 1;
        end if;
        if v_skip_kd_init_value = 1 and v_is_buy = 1 and j.k < j.d then
          -- 计算profit_loss
          v_temp_profit_loss := round((j.close_price - v_temp_buy_price) /
                                      v_temp_buy_price,
                                      4) * 100;
          -- 计算accumulative_profit_loss
          v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                         (1 + v_temp_profit_loss / 100);
          dbms_output.put_line('v_temp_profit_loss:   ' ||
                               v_temp_profit_loss);
          dbms_output.put_line('v_init_last_acc_profit_loss:   ' ||
                               v_init_last_acc_profit_loss);
          -- 向表MDL_KD_GOLD_CROSS插入记录
          insert into MDL_WEEK_KD_GOLD_CROSS
            (week_number,
             stock_code,
             model_id,
             accumulative_profit_loss,
             buy_begin_date,
             buy_d,
             buy_end_date,
             buy_k,
             buy_price,
             profit_loss,
             sell_begin_date,
             sell_d,
             sell_end_date,
             sell_k,
             sell_price)
          values
            (j.number_,
             j.code_,
             2,
             v_init_last_acc_profit_loss,
             v_temp_buy_begin_date,
             v_temp_buy_d,
             v_temp_buy_end_date,
             v_temp_buy_k,
             v_temp_buy_price,
             v_temp_profit_loss,
             j.begin_date,
             j.d,
             j.end_date,
             j.k,
             j.close_price);
          -- 表示某只股票已经卖出
          v_is_buy := 0;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_WEEK_KD_GOLD_CROSS;

  /*---------------------------------- 海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据 --------------------------------------*/
  procedure CAL_MDL_WEEK_ALL_GOLD_CROSS(p_type in varchar2) as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_BEGIN_DATE,BUY_END_DATA,SELL_BEGIN_DATE,BUY_END_DATE,BUY_PRICE,SELL_PRICE
    v_temp_stock_code            varchar2(100);
    v_temp_stock_buy_begin_date  date;
    v_temp_stock_buy_end_date    date;
    v_temp_stock_sell_begin_date date;
    v_temp_stock_sell_end_date   date;
    v_temp_stock_buy_price       number;
    v_temp_stock_sell_price      number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_gold_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 除权的次数
    ex_number number := 0;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_week t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_week t
       where t.code_ = v_stock_code
       order by t.end_date asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
    
      for j in cur_single_stock loop
      
        -------------------------------- 120周均线金叉250周均线 ---------------------------------
        if p_type = '120_250' and j.ma120 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '120_250' and j.ma120 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 60周均线金叉250周均线 ---------------------------------
        if p_type = '60_250' and j.ma60 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '60_250' and j.ma60 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 20周均线金叉250周均线 ---------------------------------
        if p_type = '20_250' and j.ma20 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '20_250' and j.ma20 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10周均线金叉250周均线 ---------------------------------
        if p_type = '10_250' and j.ma10 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_250' and j.ma10 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5周均线金叉250周均线 ---------------------------------
        if p_type = '5_250' and j.ma5 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_250' and j.ma5 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉250周均线 ---------------------------------
        if p_type = 'c_250' and j.close_price <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_250' and j.close_price > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 60周均线金叉120周均线 ---------------------------------
        if p_type = '60_120' and j.ma60 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '60_120' and j.ma60 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 20周均线金叉120周均线 ---------------------------------
        if p_type = '20_120' and j.ma20 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '20_120' and j.ma20 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10周均线金叉120周均线 ---------------------------------
        if p_type = '10_120' and j.ma10 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_120' and j.ma10 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5周均线金叉120周均线 ---------------------------------
        if p_type = '5_120' and j.ma5 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_120' and j.ma5 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉120周均线 ---------------------------------
        if p_type = 'c_120' and j.close_price <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_120' and j.close_price > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 20周均线金叉60周均线 ---------------------------------
        if p_type = '20_60' and j.ma20 <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '20_60' and j.ma20 > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10周均线金叉60周均线 ---------------------------------
        if p_type = '10_60' and j.ma10 <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_60' and j.ma10 > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5周均线金叉60周均线 ---------------------------------
        if p_type = '5_60' and j.ma5 <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_60' and j.ma5 > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉60周均线 ---------------------------------
        if p_type = 'c_60' and j.close_price <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_60' and j.close_price > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10周均线金叉20周均线 ---------------------------------
        if p_type = '10_20' and j.ma10 <= j.ma20 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_20' and j.ma10 > j.ma20 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5周均线金叉20周均线 ---------------------------------
        if p_type = '5_20' and j.ma5 <= j.ma20 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_20' and j.ma5 > j.ma20 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉20周均线 ---------------------------------
        if p_type = 'c_20' and j.close_price <= j.ma20 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_20' and j.close_price > j.ma20 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5周均线金叉10周均线 ---------------------------------
        if p_type = '5_10' and j.ma5 <= j.ma10 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_10' and j.ma5 > j.ma10 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉10周均线 ---------------------------------
        if p_type = 'c_10' and j.close_price <= j.ma10 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_10' and j.close_price > j.ma10 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉5周均线 ---------------------------------
        if p_type = 'c_5' and j.close_price <= j.ma5 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_begin_date   := j.begin_date;
            v_temp_stock_sell_end_date     := j.end_date;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data_all t
             where t.date_ between v_temp_stock_buy_begin_date and
                   v_temp_stock_sell_end_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_week_all_gold_cross
              (type_,
               stock_code,
               sell_begin_date,
               sell_end_date,
               sell_price,
               buy_begin_date,
               buy_end_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_begin_date,
               v_temp_stock_sell_end_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_begin_date,
               v_temp_stock_buy_end_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_5' and j.close_price > j.ma5 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_begin_date    := j.begin_date;
            v_temp_stock_buy_end_date      := j.end_date;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_WEEK_ALL_GOLD_CROSS;

  /*---------------------------------- 海量地向表MDL_VOLUME_TURNOVER_UP_MA250中插入数据 --------------------------------------*/
  procedure CAL_MDL_V_T_UP_MA250 as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE等
    v_temp_stock_code              varchar2(100);
    v_temp_stock_buy_date          date;
    v_temp_stock_sell_date         date;
    v_temp_stock_buy_volume        number;
    v_temp_stock_sell_volume       number;
    v_temp_stock_buy_volume_ma250  number;
    v_temp_stock_sell_volume_ma250 number;
    v_temp_stock_buy_turnover      number;
    v_temp_stock_sell_turnover     number;
    v_temp_stock_buy_t_ma250       number;
    v_temp_stock_sell_t_ma250      number;
    v_temp_stock_buy_price         number;
    v_temp_stock_sell_price        number;
    -- 1表示turnover>turnover_ma250或volume>volume_ma250的阶段，2表示turnover<=turnover_ma250或volume<=volume_ma250的阶段
    v_start_track_gold_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
    
      for j in cur_single_stock loop
        if j.volume > j.volume_ma250 or j.turnover > j.turnover_ma250 and
           v_start_track_gold_cross_stage = 0 then
          -- 买入
          v_temp_stock_code              := j.code_;
          v_temp_stock_buy_date          := j.date_;
          v_temp_stock_buy_price         := j.close_price;
          v_temp_stock_buy_volume        := j.volume;
          v_temp_stock_buy_turnover      := j.turnover;
          v_temp_stock_buy_volume_ma250  := j.volume_ma250;
          v_temp_stock_buy_t_ma250       := j.turnover_ma250;
          v_start_track_gold_cross_stage := 1;
        end if;
      
        if j.volume <= j.volume_ma250 or j.turnover <= j.turnover_ma250 and
           v_start_track_gold_cross_stage = 1 then
          -- 卖出
          v_temp_stock_sell_date         := j.date_;
          v_temp_stock_sell_price        := j.close_price;
          v_temp_stock_sell_volume       := j.volume;
          v_temp_stock_sell_turnover     := j.turnover;
          v_temp_stock_sell_volume_ma250 := j.volume_ma250;
          v_temp_stock_sell_t_ma250      := j.turnover_ma250;
          v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                  v_temp_stock_buy_price) /
                                                  v_temp_stock_buy_price,
                                                  4) * 100;
          v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                            (1 + v_temp_profit_loss / 100);
          -- 插入记录
          insert into MDL_VOLUME_TURNOVER_UP_MA250
            (stock_code,
             buy_date,
             buy_price,
             buy_volume,
             buy_turnover,
             buy_volume_ma250,
             buy_turnover_ma250,
             sell_date,
             sell_price,
             sell_volume,
             sell_turnover,
             sell_volume_ma250,
             sell_turnover_ma250,
             profit_loss,
             accumulative_profit_loss)
          values
            (v_temp_stock_code,
             v_temp_stock_buy_date,
             v_temp_stock_buy_price,
             v_temp_stock_buy_volume,
             v_temp_stock_buy_turnover,
             v_temp_stock_buy_volume_ma250,
             v_temp_stock_buy_t_ma250,
             v_temp_stock_sell_date,
             v_temp_stock_sell_price,
             v_temp_stock_sell_volume,
             v_temp_stock_sell_turnover,
             v_temp_stock_sell_volume_ma250,
             v_temp_stock_sell_t_ma250,
             v_temp_profit_loss,
             v_init_last_acc_profit_loss);
        
          -- 重置为0，表示还没有买入
          v_start_track_gold_cross_stage := 0;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_V_T_UP_MA250;

  /*---------------------------------- 增量地向表MDL_VOLUME_TURNOVER_UP_MA250中插入数据 ------------------------------------*/
  procedure CAL_MDL_V_T_UP_MA250_INCR(p_end_date in varchar2) as
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE等
    v_temp_stock_code              varchar2(100);
    v_temp_stock_buy_date          date;
    v_temp_stock_sell_date         date;
    v_temp_stock_buy_price         number;
    v_temp_stock_sell_price        number;
    v_temp_stock_buy_turnover      number;
    v_temp_stock_sell_turnover     number;
    v_temp_stock_buy_volume        number;
    v_temp_stock_sell_volume       number;
    v_temp_stock_buy_volume_ma250  number;
    v_temp_stock_sell_volume_ma250 number;
    v_temp_stock_buy_t_ma250       number;
    v_temp_stock_sell_t_ma250      number;
    -- 1表示turnover>turnover_ma250或volume>volume_ma250的阶段，2表示turnover<=turnover_ma250或volume<=volume_ma250的阶段
    v_start_track_gold_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t;
    -- 从某一只股票在MDL_VOLUME_TURNOVER_UP_MA250表中最后的卖出日期，到参数p_end_date之间的交易记录，
    -- 并按升序排列。这段时间内可能存在先是turnover/volume金叉turnover_ma250/volume_ma250，然后turnover/volume死叉turnover_ma250/volume_ma250。
    -- buy_date和sell_date效果一样
    cursor cur_v_t_gold_dead_cross_ma5 is
      select *
        from stock_transaction_data_all std
       where std.code_ = v_stock_code
         and std.date_ >
             (select max(mvtum.sell_date)
                from MDL_VOLUME_TURNOVER_UP_MA250 mvtum
               where mvtum.stock_code = v_stock_code)
         and std.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
       order by std.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_start_track_gold_cross_stage := 0;
      -- 查找最近的accumulative_profit_loss
      begin
        select b.accumulative_profit_loss
          into v_init_last_acc_profit_loss
          from (select *
                  from mdl_close_price_ma5_gold_cross t
                 where t.stock_code = v_stock_code
                 order by t.sell_date desc) b
         where rownum <= 1;
      exception
        when no_data_found then
          dbms_output.put_line('stock_code为【' || v_stock_code || '】，
                                                            在MDL_VOLUME_TURNOVER_UP_MA250表中没有记录');
      end;
    
      for j in cur_v_t_gold_dead_cross_ma5 loop
        if j.volume > j.volume_ma250 or j.turnover > j.turnover_ma250 and
           v_start_track_gold_cross_stage = 0 then
          -- 买入
          v_temp_stock_code              := j.code_;
          v_temp_stock_buy_date          := j.date_;
          v_temp_stock_buy_price         := j.close_price;
          v_temp_stock_buy_volume        := j.volume;
          v_temp_stock_buy_turnover      := j.turnover;
          v_temp_stock_buy_volume_ma250  := j.volume_ma250;
          v_temp_stock_buy_t_ma250       := j.turnover_ma250;
          v_start_track_gold_cross_stage := 1;
        end if;
      
        if j.volume <= j.volume_ma250 or j.turnover <= j.turnover_ma250 and
           v_start_track_gold_cross_stage = 1 then
          -- 卖出
          v_temp_stock_sell_date         := j.date_;
          v_temp_stock_sell_price        := j.close_price;
          v_temp_stock_sell_volume       := j.volume;
          v_temp_stock_sell_turnover     := j.turnover;
          v_temp_stock_sell_volume_ma250 := j.volume_ma250;
          v_temp_stock_sell_t_ma250      := j.turnover_ma250;
        
          v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                               v_temp_stock_buy_price) /
                                               v_temp_stock_buy_price,
                                               4) * 100;
          v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                         (1 + v_temp_profit_loss / 100);
        
          insert into MDL_VOLUME_TURNOVER_UP_MA250
            (stock_code,
             buy_date,
             buy_price,
             buy_volume,
             buy_turnover,
             buy_volume_ma250,
             buy_turnover_ma250,
             sell_date,
             sell_price,
             sell_volume,
             sell_turnover,
             sell_volume_ma250,
             sell_turnover_ma250,
             profit_loss,
             accumulative_profit_loss)
          values
            (v_temp_stock_code,
             v_temp_stock_buy_date,
             v_temp_stock_buy_price,
             v_temp_stock_buy_volume,
             v_temp_stock_buy_turnover,
             v_temp_stock_buy_volume_ma250,
             v_temp_stock_buy_t_ma250,
             v_temp_stock_sell_date,
             v_temp_stock_sell_price,
             v_temp_stock_sell_volume,
             v_temp_stock_sell_turnover,
             v_temp_stock_sell_volume_ma250,
             v_temp_stock_sell_t_ma250,
             v_temp_profit_loss,
             v_init_last_acc_profit_loss);
        
          v_start_track_gold_cross_stage := 0;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_V_T_UP_MA250_INCR;

  /*---------------------------------- 海量地计算所有的金叉类型 --------------------------------------*/
  -- p_type为120_250表示120日均线金叉250日均线
  procedure CAL_MDL_ALL_GOLD_CROSS(p_type in varchar2) as
    -- 表示CODE_
    v_stock_code varchar2(100);
    -- 作为临时变量，表示CODE_,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    -- 如果为1，则表示已经买入股票了；如果为0，则表示还没有买入股票
    v_start_track_gold_cross_stage number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 除权的次数
    ex_number number := 0;
    -- 用于获取所有的CODE_
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data t;
    -- 用于根据CODE_获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data t
       where t.code_ = v_stock_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_stock_code                   := i.code_;
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
    
      for j in cur_single_stock loop
      
        -------------------------------- 120日均线金叉250日均线 ---------------------------------
        if p_type = '120_250' and j.ma120 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '120_250' and j.ma120 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 60日均线金叉250日均线 ---------------------------------
        if p_type = '60_250' and j.ma60 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '60_250' and j.ma60 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 20日均线金叉250日均线 ---------------------------------
        if p_type = '20_250' and j.ma20 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '20_250' and j.ma20 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10日均线金叉250日均线 ---------------------------------
        if p_type = '10_250' and j.ma10 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_250' and j.ma10 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5日均线金叉250日均线 ---------------------------------
        if p_type = '5_250' and j.ma5 <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_250' and j.ma5 > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉250日均线 ---------------------------------
        if p_type = 'c_250' and j.close_price <= j.ma250 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_250' and j.close_price > j.ma250 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 60日均线金叉120日均线 ---------------------------------
        if p_type = '60_120' and j.ma60 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '60_120' and j.ma60 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 20日均线金叉120日均线 ---------------------------------
        if p_type = '20_120' and j.ma20 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '20_120' and j.ma20 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10日均线金叉120日均线 ---------------------------------
        if p_type = '10_120' and j.ma10 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_120' and j.ma10 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5日均线金叉120日均线 ---------------------------------
        if p_type = '5_120' and j.ma5 <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_120' and j.ma5 > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉120日均线 ---------------------------------
        if p_type = 'c_120' and j.close_price <= j.ma120 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_120' and j.close_price > j.ma120 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 20日均线金叉60日均线 ---------------------------------
        if p_type = '20_60' and j.ma20 <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '20_60' and j.ma20 > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10日均线金叉60日均线 ---------------------------------
        if p_type = '10_60' and j.ma10 <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_60' and j.ma10 > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5日均线金叉60日均线 ---------------------------------
        if p_type = '5_60' and j.ma5 <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_60' and j.ma5 > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉60日均线 ---------------------------------
        if p_type = 'c_60' and j.close_price <= j.ma60 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_60' and j.close_price > j.ma60 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 10日均线金叉20日均线 ---------------------------------
        if p_type = '10_20' and j.ma10 <= j.ma20 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '10_20' and j.ma10 > j.ma20 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5日均线金叉20日均线 ---------------------------------
        if p_type = '5_20' and j.ma5 <= j.ma20 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_20' and j.ma5 > j.ma20 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉20日均线 ---------------------------------
        if p_type = 'c_20' and j.close_price <= j.ma20 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_20' and j.close_price > j.ma20 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- 5日均线金叉10日均线 ---------------------------------
        if p_type = '5_10' and j.ma5 <= j.ma10 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = '5_10' and j.ma5 > j.ma10 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉10日均线 ---------------------------------
        if p_type = 'c_10' and j.close_price <= j.ma10 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_10' and j.close_price > j.ma10 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      
        -------------------------------- close_price金叉5日均线 ---------------------------------
        if p_type = 'c_5' and j.close_price <= j.ma5 then
          -- 卖出
          if v_start_track_gold_cross_stage = 1 then
            v_start_track_gold_cross_stage := 0;
            v_temp_stock_sell_date         := j.date_;
            v_temp_stock_sell_price        := j.close_price;
            v_temp_profit_loss             := round((v_temp_stock_sell_price -
                                                    v_temp_stock_buy_price) /
                                                    v_temp_stock_buy_price,
                                                    4) * 100;
            v_init_last_acc_profit_loss    := v_init_last_acc_profit_loss *
                                              (1 + v_temp_profit_loss / 100);
          
            -- 如果买入时间到卖出时间之间存在除权的情况，则不记录这次交易
            select count(*)
              into ex_number
              from stock_transaction_data t
             where t.date_ between v_temp_stock_buy_date and
                   v_temp_stock_sell_date
               and t.code_ = v_stock_code
               and t.change_range_ex_right <= -11;
            if ex_number > 0 then
              continue;
            end if;
          
            -- 插入记录
            insert into mdl_all_gold_cross
              (type_,
               stock_code,
               sell_date,
               sell_price,
               buy_date,
               buy_price,
               accumulative_profit_loss,
               profit_loss)
            values
              (p_type,
               v_temp_stock_code,
               v_temp_stock_sell_date,
               v_temp_stock_sell_price,
               v_temp_stock_buy_date,
               v_temp_stock_buy_price,
               v_init_last_acc_profit_loss,
               v_temp_profit_loss);
          else
            continue;
          end if;
        end if;
      
        if p_type = 'c_5' and j.close_price > j.ma5 then
          -- 买入
          if v_start_track_gold_cross_stage != 1 then
            v_start_track_gold_cross_stage := 1;
            v_temp_stock_buy_date          := j.date_;
            v_temp_stock_buy_price         := j.close_price;
            v_temp_stock_code              := j.code_;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_ALL_GOLD_CROSS;

  /*--------------------------------------------- write table MDL_TOP_STOCK -------------------------------------------------*/
  procedure WRITE_MDL_TOP_STOCK(p_begin_date        in varchar2,
                                p_end_date          in varchar2,
                                p_number_percentage in number) as
    -- 表示STOCK_DATE字段
    v_stock_date date;
    -- 表示游标中的行数
    v_row_num number;
    -- 表示在某个时间范围之内，最大的日期
    v_max_date date;
    -- 表示在某个时间范围之内，最小的日期
    v_min_date             date;
    stock_top_result_array T_STOCK_TOP_RESULT_ARRAY;
    -- 用于记录最近21, 34, 55, 89, 144, 233，377，610个交易日内的涨幅最大的股票
    v_top_stock_period21_set  CLOB;
    v_top_stock_period34_set  CLOB;
    v_top_stock_period55_set  CLOB;
    v_top_stock_period89_set  CLOB;
    v_top_stock_period144_set CLOB;
    v_top_stock_period233_set CLOB;
    v_top_stock_period377_set CLOB;
    v_top_stock_period610_set CLOB;
    -- v_top_stock_period610_set varchar2(5120);
    -- 股票数量
    v_number number;
    -- 表示表STOCK_MOVING_AVERAGE中的所有STOCK_DATE字段，不能有重复，并且升序排列
    cursor cur_all_stock_date is
      select distinct t.date_
        from stock_transaction_data t
       where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
  begin
    for i in cur_all_stock_date loop
      v_stock_date              := i.date_;
      stock_top_result_array    := T_STOCK_TOP_RESULT_ARRAY();
      v_top_stock_period21_set  := '';
      v_top_stock_period34_set  := '';
      v_top_stock_period55_set  := '';
      v_top_stock_period89_set  := '';
      v_top_stock_period144_set := '';
      v_top_stock_period233_set := '';
      v_top_stock_period377_set := '';
      v_top_stock_period610_set := '';
    
      -- 计算日期v_stock_date之前有多少个交易日
      select count(*)
        into v_row_num
        from (select distinct t.date_
                from stock_transaction_data t
               where t.date_ <= v_stock_date
               order by t.date_ desc);
    
      -- 计算21日内涨幅最大的股票
      if v_row_num >= 21 then
        -- 获取在某个时间范围之内，最大的日期和最小的日期
        select max(date_), min(date_)
          into v_max_date, v_min_date
          from (select distinct t.date_
                  from stock_transaction_data t
                 where t.date_ <= v_stock_date
                 order by t.date_ desc)
         where rownum <= 21;
      
        -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
        -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
        select count(distinct t.code_)
          into v_number
          from stock_transaction_data_all t
         where t.date_ between v_min_date and v_max_date;
        v_number := ceil(v_number * p_number_percentage);
      
        -- 获取涨幅最大的股票
        SCOTT.pkg_stock_transaction_data.FIND_TOP_STOCK(to_char(v_min_date,
                                                                'yyyy-mm-dd'),
                                                        to_char(v_max_date,
                                                                'yyyy-mm-dd'),
                                                        v_number,
                                                        3,
                                                        stock_top_result_array);
      
        -- 将涨幅最大的股票存储在字符串中
        for i in 1 .. stock_top_result_array.count loop
          v_top_stock_period21_set := v_top_stock_period21_set || i || '&' || stock_top_result_array(i)
                                     .get_stock_code() || '&' || stock_top_result_array(i)
                                     .get_stock_name() || '&' || stock_top_result_array(i)
                                     .get_board_name() || '&' || stock_top_result_array(i)
                                     .get_rate() || CHR(10);
        end loop;
      end if;
    
      -- 计算34日内涨幅最大的股票
      if v_row_num >= 34 then
        -- 获取在某个时间范围之内，最大的日期和最小的日期
        select max(date_), min(date_)
          into v_max_date, v_min_date
          from (select distinct t.date_
                  from stock_transaction_data t
                 where t.date_ <= v_stock_date
                 order by t.date_ desc)
         where rownum <= 34;
      
        -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
        -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
        select count(distinct t.code_)
          into v_number
          from stock_transaction_data_all t
         where t.date_ between v_min_date and v_max_date;
        v_number := ceil(v_number * p_number_percentage);
      
        -- 获取涨幅最大的股票
        SCOTT.pkg_stock_transaction_data.FIND_TOP_STOCK(to_char(v_min_date,
                                                                'yyyy-mm-dd'),
                                                        to_char(v_max_date,
                                                                'yyyy-mm-dd'),
                                                        v_number,
                                                        3,
                                                        stock_top_result_array);
      
        -- 将涨幅最大的股票存储在字符串中
        for i in 1 .. stock_top_result_array.count loop
          v_top_stock_period34_set := v_top_stock_period34_set || i || '&' || stock_top_result_array(i)
                                     .get_stock_code() || '&' || stock_top_result_array(i)
                                     .get_stock_name() || '&' || stock_top_result_array(i)
                                     .get_board_name() || '&' || stock_top_result_array(i)
                                     .get_rate() || CHR(10);
        end loop;
      end if;
    
      -- 计算55日内涨幅最大的股票
      if v_row_num >= 55 then
        -- 获取在某个时间范围之内，最大的日期和最小的日期
        select max(date_), min(date_)
          into v_max_date, v_min_date
          from (select distinct t.date_
                  from stock_transaction_data t
                 where t.date_ <= v_stock_date
                 order by t.date_ desc)
         where rownum <= 55;
      
        -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
        -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
        select count(distinct t.code_)
          into v_number
          from stock_transaction_data_all t
         where t.date_ between v_min_date and v_max_date;
        v_number := ceil(v_number * p_number_percentage);
      
        -- 获取涨幅最大的股票
        SCOTT.pkg_stock_transaction_data.FIND_TOP_STOCK(to_char(v_min_date,
                                                                'yyyy-mm-dd'),
                                                        to_char(v_max_date,
                                                                'yyyy-mm-dd'),
                                                        v_number,
                                                        3,
                                                        stock_top_result_array);
      
        -- 将涨幅最大的股票存储在字符串中
        for i in 1 .. stock_top_result_array.count loop
          v_top_stock_period55_set := v_top_stock_period55_set || i || '&' || stock_top_result_array(i)
                                     .get_stock_code() || '&' || stock_top_result_array(i)
                                     .get_stock_name() || '&' || stock_top_result_array(i)
                                     .get_board_name() || '&' || stock_top_result_array(i)
                                     .get_rate() || CHR(10);
        end loop;
      end if;
    
    -- 计算89日内涨幅最大的股票
    /*if v_row_num >= 89 then
                                                                                                                                                                                                                              -- 获取在某个时间范围之内，最大的日期和最小的日期
                                                                                                                                                                                                                              select max(stock_date), min(stock_date)
                                                                                                                                                                                                                                into v_max_date, v_min_date
                                                                                                                                                                                                                                from (select distinct t.stock_date
                                                                                                                                                                                                                                        from stock_moving_average t
                                                                                                                                                                                                                                       where t.stock_date <= v_stock_date
                                                                                                                                                                                                                                       order by t.stock_date desc)
                                                                                                                                                                                                                               where rownum <= 89;
                    
                                                                                                                                                                                                                              -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                                                                                                                                                                                                                              -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                                                                                                                                                                                                                              select count(distinct t.code_)
                                                                                                                                                                                                                                into v_number
                                                                                                                                                                                                                                from stock_transaction_data_all t
                                                                                                                                                                                                                               where t.date_ between v_min_date and v_max_date;
                                                                                                                                                                                                                              v_number := ceil(v_number * p_number_percentage);
                    
                                                                                                                                                                                                                              -- 获取涨幅最大的股票
                                                                                                                                                                                                                              scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      to_char(v_max_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      v_number,
                                                                                                                                                                                                                                                                      3,
                                                                                                                                                                                                                                                                      stock_top_result_array);
                    
                                                                                                                                                                                                                              -- 将涨幅最大的股票存储在字符串中
                                                                                                                                                                                                                              for i in 1 .. stock_top_result_array.count loop
                                                                                                                                                                                                                                  v_top_stock_period89_set := v_top_stock_period89_set || i || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                             .getStockCode() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                             .getStockName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                             .getBoardName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                             .getRate() || CHR(10);
                                                                                                                                                                                                                              end loop;
                                                                                                                                                                                                                          end if;
                    
                                                                                                                                                                                                                          -- 计算144日内涨幅最大的股票
                                                                                                                                                                                                                          if v_row_num >= 144 then
                                                                                                                                                                                                                              -- 获取在某个时间范围之内，最大的日期和最小的日期
                                                                                                                                                                                                                              select max(stock_date), min(stock_date)
                                                                                                                                                                                                                                into v_max_date, v_min_date
                                                                                                                                                                                                                                from (select distinct t.stock_date
                                                                                                                                                                                                                                        from stock_moving_average t
                                                                                                                                                                                                                                       where t.stock_date <= v_stock_date
                                                                                                                                                                                                                                       order by t.stock_date desc)
                                                                                                                                                                                                                               where rownum <= 144;
                    
                                                                                                                                                                                                                              -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                                                                                                                                                                                                                              -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                                                                                                                                                                                                                              select count(distinct t.code_)
                                                                                                                                                                                                                                into v_number
                                                                                                                                                                                                                                from stock_transaction_data_all t
                                                                                                                                                                                                                               where t.date_ between v_min_date and v_max_date;
                                                                                                                                                                                                                              v_number := ceil(v_number * p_number_percentage);
                    
                                                                                                                                                                                                                              -- 获取涨幅最大的股票
                                                                                                                                                                                                                              scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      to_char(v_max_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      v_number,
                                                                                                                                                                                                                                                                      3,
                                                                                                                                                                                                                                                                      stock_top_result_array);
                    
                                                                                                                                                                                                                              -- 将涨幅最大的股票存储在字符串中
                                                                                                                                                                                                                              for i in 1 .. stock_top_result_array.count loop
                                                                                                                                                                                                                                  v_top_stock_period144_set := v_top_stock_period144_set || i || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockCode() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getBoardName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getRate() || CHR(10);
                                                                                                                                                                                                                              end loop;
                                                                                                                                                                                                                          end if;
                    
                                                                                                                                                                                                                          -- 计算233日内涨幅最大的股票
                                                                                                                                                                                                                          if v_row_num >= 233 then
                                                                                                                                                                                                                              -- 获取在某个时间范围之内，最大的日期和最小的日期
                                                                                                                                                                                                                              select max(stock_date), min(stock_date)
                                                                                                                                                                                                                                into v_max_date, v_min_date
                                                                                                                                                                                                                                from (select distinct t.stock_date
                                                                                                                                                                                                                                        from stock_moving_average t
                                                                                                                                                                                                                                       where t.stock_date <= v_stock_date
                                                                                                                                                                                                                                       order by t.stock_date desc)
                                                                                                                                                                                                                               where rownum <= 233;
                    
                                                                                                                                                                                                                              -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                                                                                                                                                                                                                              -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                                                                                                                                                                                                                              select count(distinct t.code_)
                                                                                                                                                                                                                                into v_number
                                                                                                                                                                                                                                from stock_transaction_data_all t
                                                                                                                                                                                                                               where t.date_ between v_min_date and v_max_date;
                                                                                                                                                                                                                              v_number := ceil(v_number * p_number_percentage);
                    
                                                                                                                                                                                                                              -- 获取涨幅最大的股票
                                                                                                                                                                                                                              scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      to_char(v_max_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      v_number,
                                                                                                                                                                                                                                                                      3,
                                                                                                                                                                                                                                                                      stock_top_result_array);
                    
                                                                                                                                                                                                                              -- 将涨幅最大的股票存储在字符串中
                                                                                                                                                                                                                              for i in 1 .. stock_top_result_array.count loop
                                                                                                                                                                                                                                  v_top_stock_period233_set := v_top_stock_period233_set || i || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockCode() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getBoardName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getRate() || CHR(10);
                                                                                                                                                                                                                              end loop;
                                                                                                                                                                                                                          end if;
                    
                                                                                                                                                                                                                          -- 计算377日内涨幅最大的股票
                                                                                                                                                                                                                          if v_row_num >= 377 then
                                                                                                                                                                                                                              -- 获取在某个时间范围之内，最大的日期和最小的日期
                                                                                                                                                                                                                              select max(stock_date), min(stock_date)
                                                                                                                                                                                                                                into v_max_date, v_min_date
                                                                                                                                                                                                                                from (select distinct t.stock_date
                                                                                                                                                                                                                                        from stock_moving_average t
                                                                                                                                                                                                                                       where t.stock_date <= v_stock_date
                                                                                                                                                                                                                                       order by t.stock_date desc)
                                                                                                                                                                                                                               where rownum <= 377;
                    
                                                                                                                                                                                                                              -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                                                                                                                                                                                                                              -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                                                                                                                                                                                                                              select count(distinct t.code_)
                                                                                                                                                                                                                                into v_number
                                                                                                                                                                                                                                from stock_transaction_data_all t
                                                                                                                                                                                                                               where t.date_ between v_min_date and v_max_date;
                                                                                                                                                                                                                              v_number := ceil(v_number * p_number_percentage);
                    
                                                                                                                                                                                                                              -- 获取涨幅最大的股票
                                                                                                                                                                                                                              scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      to_char(v_max_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      v_number,
                                                                                                                                                                                                                                                                      3,
                                                                                                                                                                                                                                                                      stock_top_result_array);
                    
                                                                                                                                                                                                                              -- 将涨幅最大的股票存储在字符串中
                                                                                                                                                                                                                              for i in 1 .. stock_top_result_array.count loop
                                                                                                                                                                                                                                  v_top_stock_period377_set := v_top_stock_period377_set || i || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockCode() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getBoardName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getRate() || CHR(10);
                                                                                                                                                                                                                              end loop;
                                                                                                                                                                                                                          end if;
                    
                                                                                                                                                                                                                          -- 计算610日内涨幅最大的股票
                                                                                                                                                                                                                          if v_row_num >= 610 then
                                                                                                                                                                                                                              -- 获取在某个时间范围之内，最大的日期和最小的日期
                                                                                                                                                                                                                              select max(stock_date), min(stock_date)
                                                                                                                                                                                                                                into v_max_date, v_min_date
                                                                                                                                                                                                                                from (select distinct t.stock_date
                                                                                                                                                                                                                                        from stock_moving_average t
                                                                                                                                                                                                                                       where t.stock_date <= v_stock_date
                                                                                                                                                                                                                                       order by t.stock_date desc)
                                                                                                                                                                                                                               where rownum <= 610;
                    
                                                                                                                                                                                                                              -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                                                                                                                                                                                                                              -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                                                                                                                                                                                                                              select count(distinct t.code_)
                                                                                                                                                                                                                                into v_number
                                                                                                                                                                                                                                from stock_transaction_data_all t
                                                                                                                                                                                                                               where t.date_ between v_min_date and v_max_date;
                                                                                                                                                                                                                              v_number := ceil(v_number * p_number_percentage);
                    
                                                                                                                                                                                                                              -- 获取涨幅最大的股票
                                                                                                                                                                                                                              scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      to_char(v_max_date,
                                                                                                                                                                                                                                                                              'yyyy-mm-dd'),
                                                                                                                                                                                                                                                                      v_number,
                                                                                                                                                                                                                                                                      3,
                                                                                                                                                                                                                                                                      stock_top_result_array);
                    
                                                                                                                                                                                                                              -- 将涨幅最大的股票存储在字符串中
                                                                                                                                                                                                                              for i in 1 .. stock_top_result_array.count loop
                                                                                                                                                                                                                                  v_top_stock_period610_set := v_top_stock_period610_set || i || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockCode() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getStockName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getBoardName() || '&' || stock_top_result_array(i)
                                                                                                                                                                                                                                                              .getRate() || CHR(10);
                                                                                                                                                                                                                              end loop;
                                                                                                                                                                                                                          end if;*/
    
    -- 插入数据
    /*if v_max_date is not null then
                                                                                                                                                                                                                            insert into mdl_top_stock
                                                                                                                                                                                                                              (date_,
                                                                                                                                                                                                                               period21,
                                                                                                                                                                                                                               period34,
                                                                                                                                                                                                                               period55,
                                                                                                                                                                                                                               period89,
                                                                                                                                                                                                                               period144,
                                                                                                                                                                                                                               period233,
                                                                                                                                                                                                                               period337,
                                                                                                                                                                                                                               period610)
                                                                                                                                                                                                                            values
                                                                                                                                                                                                                              (v_max_date,
                                                                                                                                                                                                                               v_top_stock_period21_set,
                                                                                                                                                                                                                               v_top_stock_period34_set,
                                                                                                                                                                                                                               v_top_stock_period55_set,
                                                                                                                                                                                                                               v_top_stock_period89_set,
                                                                                                                                                                                                                               v_top_stock_period144_set,
                                                                                                                                                                                                                               v_top_stock_period233_set,
                                                                                                                                                                                                                               v_top_stock_period377_set,
                                                                                                                                                                                                                               v_top_stock_period610_set);
                                                                                                                                                                                                                            commit;
                                                                                                                                                                                                                          end if;*/
    end loop;
  end WRITE_MDL_TOP_STOCK;

  /*--------------------------------------------- write table MDL_TOP_STOCK_DETAIL -------------------------------------------------*/
  procedure WRITE_MDL_TOP_STOCK_DETAIL(p_begin_date in varchar2,
                                       p_end_date   in varchar2) as
    -- 表示date_字段
    v_stock_date date;
    -- 某一只股票的交易记录数
    v_stock_transaction_data_num number;
    -- 股票代码
    v_stock_code varchar2(50);
    -- 当前日期的股票收盘价
    v_current_close_price number;
    -- 第一个日期的股票收盘价
    v_first_close_price number;
    -- 用于记录最近21, 34, 55, 89, 144, 233，377，610个交易日内的涨幅最大的股票
    v_period21_up_down_percentage  number;
    v_period34_up_down_percentage  number;
    v_period55_up_down_percentage  number;
    v_period89_up_down_percentage  number;
    v_period144_up_down_percentage number;
    v_period233_up_down_percentage number;
    v_period377_up_down_percentage number;
    v_period610_up_down_percentage number;
    -- 表示股票名称是否存在，1表示存在
    v_stock_name_exist number;
    -- 股票名称
    v_stock_name varchar2(50);
    -- 表示板块名称是否存在，1表示存在
    v_board_name_exist number;
    -- 板块名称
    v_board_name varchar2(50);
    -- 表示表stock_transaction_data_all中的所有date_字段，不能有重复，并且升序排列
    cursor cur_distinct_date is
      select distinct t.date_
        from stock_transaction_data t
       where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 在某一日的所有股票的代码
    cursor cur_stock_by_date is
      select distinct t.code_
        from stock_transaction_data_all t
       where t.date_ = v_stock_date;
  begin
    for i in cur_distinct_date loop
      v_stock_date := i.date_;
      for j in cur_stock_by_date loop
        v_stock_code := j.code_;
        -- 重置变量
        v_period21_up_down_percentage  := null;
        v_period34_up_down_percentage  := null;
        v_period55_up_down_percentage  := null;
        v_period89_up_down_percentage  := null;
        v_period144_up_down_percentage := null;
        v_period233_up_down_percentage := null;
        v_period377_up_down_percentage := null;
        v_period610_up_down_percentage := null;
        v_stock_transaction_data_num   := null;
        v_current_close_price          := null;
        v_first_close_price            := null;
        v_stock_name_exist             := null;
        v_stock_name                   := null;
        v_board_name_exist             := null;
        v_board_name                   := null;
      
        -- 计算某只股票在某个日期后的交易记录数
        select count(*)
          into v_stock_transaction_data_num
          from stock_transaction_data_all t
         where t.code_ = v_stock_code
           and t.date_ <= v_stock_date;
      
        -- 计算v_current_close_price
        select t1.close_price
          into v_current_close_price
          from (select *
                  from stock_transaction_data_all t
                 where t.code_ = v_stock_code
                   and t.date_ <= v_stock_date
                 order by t.date_ desc) t1
         where rownum <= 1;
      
        -- 计算period_type为21时的up_down_percentage
        if v_stock_transaction_data_num >= 21 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 21) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period21_up_down_percentage := (v_current_close_price -
                                           v_first_close_price) /
                                           v_first_close_price * 100;
        end if;
      
        -- 计算period_type为34时的up_down_percentage
        if v_stock_transaction_data_num >= 34 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 34) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period34_up_down_percentage := (v_current_close_price -
                                           v_first_close_price) /
                                           v_first_close_price * 100;
        end if;
      
        -- 计算period_type为55时的up_down_percentage
        if v_stock_transaction_data_num >= 55 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 55) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period55_up_down_percentage := (v_current_close_price -
                                           v_first_close_price) /
                                           v_first_close_price * 100;
        end if;
      
        -- 计算period_type为89时的up_down_percentage
        if v_stock_transaction_data_num >= 89 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 89) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period89_up_down_percentage := (v_current_close_price -
                                           v_first_close_price) /
                                           v_first_close_price * 100;
        end if;
      
        -- 计算period_type为144时的up_down_percentage
        if v_stock_transaction_data_num >= 144 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 144) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period144_up_down_percentage := (v_current_close_price -
                                            v_first_close_price) /
                                            v_first_close_price * 100;
        end if;
      
        -- 计算period_type为233时的up_down_percentage
        if v_stock_transaction_data_num >= 233 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 233) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period233_up_down_percentage := (v_current_close_price -
                                            v_first_close_price) /
                                            v_first_close_price * 100;
        end if;
      
        -- 计算period_type为377时的up_down_percentage
        if v_stock_transaction_data_num >= 377 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 377) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period377_up_down_percentage := (v_current_close_price -
                                            v_first_close_price) /
                                            v_first_close_price * 100;
        end if;
      
        -- 计算period_type为610时的up_down_percentage
        if v_stock_transaction_data_num >= 610 then
          -- 计算v_first_close_price
          select t2.close_price
            into v_first_close_price
            from (select *
                    from (select *
                            from (select *
                                    from (select *
                                            from stock_transaction_data_all t
                                           where t.code_ = v_stock_code
                                             and t.date_ <= v_stock_date
                                           order by t.date_ desc)
                                   where rownum <= 610) t1
                           order by t1.date_ asc)
                   where rownum <= 1) t2;
          v_period610_up_down_percentage := (v_current_close_price -
                                            v_first_close_price) /
                                            v_first_close_price * 100;
        end if;
      
        -- 查询股票名称
        select count(*)
          into v_stock_name_exist
          from stock_info t
         where t.code_ = v_stock_code;
        if v_stock_name_exist = 1 then
          select t.name_
            into v_stock_name
            from stock_info t
           where t.code_ = v_stock_code;
        end if;
        -- 查询板块名称
        select count(*)
          into v_board_name_exist
          from stock_info si
          join board b
            on si.board_id = b.id
         where si.code_ = v_stock_code;
        if v_board_name_exist = 1 then
          select b.name
            into v_board_name
            from stock_info si
            join board b
              on si.board_id = b.id
           where si.code_ = v_stock_code;
        end if;
      
        -- 向表mdl_top_stock_detail中插入数据
        insert into mdl_top_stock_detail
          (date_,
           code_,
           name_,
           board_name,
           up_down_percentage_21,
           up_down_percentage_34,
           up_down_percentage_55,
           up_down_percentage_89,
           up_down_percentage_144,
           up_down_percentage_233,
           up_down_percentage_377,
           up_down_percentage_610)
        values
          (v_stock_date,
           v_stock_code,
           v_stock_name,
           v_board_name,
           v_period21_up_down_percentage,
           v_period34_up_down_percentage,
           v_period55_up_down_percentage,
           v_period89_up_down_percentage,
           v_period144_up_down_percentage,
           v_period233_up_down_percentage,
           v_period377_up_down_percentage,
           v_period610_up_down_percentage);
        commit;
      end loop;
    end loop;
  end WRITE_MDL_TOP_STOCK_DETAIL;

  /*--------------- 计算表MDL_TOP_STOCK_DETAIL中所有percentage_*_ma*列的值 -----------------------*/
  procedure WRITE_MDL_TOP_STOCK_DETAIL_MA(p_begin_date in varchar2,
                                          p_end_date   in varchar2) as
    -- 日期
    v_date date;
    -- code_字段
    v_code varchar2(20);
    -- 记录数量
    v_record_number number;
    -- percentage_*_ma*列
    v_percentage_21_ma5   number;
    v_percentage_34_ma5   number;
    v_percentage_55_ma5   number;
    v_percentage_89_ma5   number;
    v_percentage_144_ma5  number;
    v_percentage_233_ma5  number;
    v_percentage_377_ma5  number;
    v_percentage_610_ma5  number;
    v_percentage_21_ma10  number;
    v_percentage_34_ma10  number;
    v_percentage_55_ma10  number;
    v_percentage_89_ma10  number;
    v_percentage_144_ma10 number;
    v_percentage_233_ma10 number;
    v_percentage_377_ma10 number;
    v_percentage_610_ma10 number;
    v_percentage_21_ma20  number;
    v_percentage_34_ma20  number;
    v_percentage_55_ma20  number;
    v_percentage_89_ma20  number;
    v_percentage_144_ma20 number;
    v_percentage_233_ma20 number;
    v_percentage_377_ma20 number;
    v_percentage_610_ma20 number;
    -- 查询日期p_begin_date和p_end_date之间的所有日期，并按升序排列
    cursor cur_date is
      select distinct t.date_
        from mdl_top_stock_detail t
       where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 查询日期v_date的所有记录
    cursor cur_mdl_top_stock_detail is
      select * from mdl_top_stock_detail t where t.date_ = v_date;
  begin
    for i in cur_date loop
      v_date := i.date_;
      for j in cur_mdl_top_stock_detail loop
        v_code := j.code_;
      
        ---------------- 计算percentage_*_ma5相关列的值 -----------------
        -- 计算percentage_21_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_21 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_21)
            into v_percentage_21_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_34_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_34 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_34)
            into v_percentage_34_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_55_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_55 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_55)
            into v_percentage_55_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_89_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_89 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_89)
            into v_percentage_89_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_144_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_144 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_144)
            into v_percentage_144_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_233_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_233 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_233)
            into v_percentage_233_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_377_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_377 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_377)
            into v_percentage_377_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_610_ma5列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_610 is not null;
        if v_record_number >= 5 then
          select avg(t2.up_down_percentage_610)
            into v_percentage_610_ma5
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 5;
        end if;
      
        -- 计算percentage_21_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_21 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_21)
            into v_percentage_21_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_34_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_34 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_34)
            into v_percentage_34_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_55_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_55 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_55)
            into v_percentage_55_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_89_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_89 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_89)
            into v_percentage_89_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_144_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_144 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_144)
            into v_percentage_144_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_233_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_233 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_233)
            into v_percentage_233_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_377_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_377 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_377)
            into v_percentage_377_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_610_ma10列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_610 is not null;
        if v_record_number >= 10 then
          select avg(t2.up_down_percentage_610)
            into v_percentage_610_ma10
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 10;
        end if;
      
        -- 计算percentage_21_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_21 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_21)
            into v_percentage_21_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_34_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_34 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_34)
            into v_percentage_34_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_55_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_55 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_55)
            into v_percentage_55_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_89_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_89 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_89)
            into v_percentage_89_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_144_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_144 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_144)
            into v_percentage_144_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_233_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_233 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_233)
            into v_percentage_233_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_377_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_377 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_377)
            into v_percentage_377_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 计算percentage_610_ma20列的值
        select count(*)
          into v_record_number
          from mdl_top_stock_detail t
         where t.code_ = v_code
           and t.date_ <= v_date
           and t.up_down_percentage_610 is not null;
        if v_record_number >= 20 then
          select avg(t2.up_down_percentage_610)
            into v_percentage_610_ma20
            from (select *
                    from mdl_top_stock_detail t
                   where t.code_ = v_code
                     and t.date_ <= v_date
                   order by t.date_ desc) t2
           where rownum <= 20;
        end if;
      
        -- 更新记录
        update mdl_top_stock_detail t
           set t.percentage_21_ma5   = v_percentage_21_ma5,
               t.percentage_21_ma10  = v_percentage_21_ma10,
               t.percentage_21_ma20  = v_percentage_21_ma20,
               t.percentage_34_ma5   = v_percentage_34_ma5,
               t.percentage_34_ma10  = v_percentage_34_ma10,
               t.percentage_34_ma20  = v_percentage_34_ma20,
               t.percentage_55_ma5   = v_percentage_55_ma5,
               t.percentage_55_ma10  = v_percentage_55_ma10,
               t.percentage_55_ma20  = v_percentage_55_ma20,
               t.percentage_89_ma5   = v_percentage_89_ma5,
               t.percentage_89_ma10  = v_percentage_89_ma10,
               t.percentage_89_ma20  = v_percentage_89_ma20,
               t.percentage_144_ma5  = v_percentage_144_ma5,
               t.percentage_144_ma10 = v_percentage_144_ma10,
               t.percentage_144_ma20 = v_percentage_144_ma20,
               t.percentage_233_ma5  = v_percentage_233_ma5,
               t.percentage_233_ma10 = v_percentage_233_ma10,
               t.percentage_233_ma20 = v_percentage_233_ma20,
               t.percentage_377_ma5  = v_percentage_377_ma5,
               t.percentage_377_ma10 = v_percentage_377_ma10,
               t.percentage_377_ma20 = v_percentage_377_ma20,
               t.percentage_610_ma5  = v_percentage_610_ma5,
               t.percentage_610_ma10 = v_percentage_610_ma10,
               t.percentage_610_ma20 = v_percentage_610_ma20
         where t.code_ = v_code
           and t.date_ = v_date;
      end loop;
    end loop;
    commit;
  end WRITE_MDL_TOP_STOCK_DETAIL_MA;

  /*--------------------------------------------- write table MDL_TOP_STOCK_DETAIL -------------------------------------------------*/
  /*procedure WRITE_MDL_TOP_STOCK_DETAIL2(p_begin_date        in varchar2,
                                        p_end_date          in varchar2,
                                        p_number_percentage in number) as
    -- 表示STOCK_DATE字段
    v_stock_date date;
    -- 表示游标中的行数
    v_row_num number;
    -- 表示在某个时间范围之内，最大的日期
    v_max_date date;
    -- 表示在某个时间范围之内，最小的日期
    v_min_date             date;
    stock_top_result_array T_STOCK_TOP_RESULT_ARRAY;
    -- 用于记录最近21, 34, 55, 89, 144, 233，377，610个交易日内的涨幅最大的股票
    v_top_stock_period21_set  CLOB;
    v_top_stock_period34_set  CLOB;
    v_top_stock_period55_set  CLOB;
    v_top_stock_period89_set  CLOB;
    v_top_stock_period144_set CLOB;
    v_top_stock_period233_set CLOB;
    v_top_stock_period377_set CLOB;
    v_top_stock_period610_set CLOB;
    -- v_top_stock_period610_set varchar2(5120);
    -- 股票数量
    v_number number;
    -- 表示表stock_transaction_data中的所有date_字段，不能有重复，并且升序排列
    cursor cur_all_stock_date is
      select distinct t.date_
        from stock_transaction_data t
       where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
  begin
    for i in cur_all_stock_date loop
      v_stock_date              := i.date_;
      stock_top_result_array    := T_STOCK_TOP_RESULT_ARRAY();
      v_top_stock_period21_set  := '';
      v_top_stock_period34_set  := '';
      v_top_stock_period55_set  := '';
      v_top_stock_period89_set  := '';
      v_top_stock_period144_set := '';
      v_top_stock_period233_set := '';
      v_top_stock_period377_set := '';
      v_top_stock_period610_set := '';
  
      -- 计算日期v_stock_date之前有多少个交易日
      select count(*)
        into v_row_num
        from (select distinct t.date_
                from stock_transaction_data t
               where t.date_ <= v_stock_date
               order by t.date_ desc);
  
      -- 计算21日内涨幅最大的股票
      if v_row_num >= 21 then
        -- 获取在某个时间范围之内，最大的日期和最小的日期
        select max(date_), min(date_)
          into v_max_date, v_min_date
          from (select distinct t.date_
                  from stock_transaction_data t
                 where t.date_ <= v_stock_date
                 order by t.date_ desc)
         where rownum <= 21;
  
        -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
        -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
        select count(distinct t.code_)
          into v_number
          from stock_transaction_data_all t
         where t.date_ between v_min_date and v_max_date;
        v_number := ceil(v_number * p_number_percentage);
  
        -- 获取涨幅最大的股票
        SCOTT.pkg_stock_transaction_data.FIND_TOP_STOCK(to_char(v_min_date,
                                                                   'yyyy-mm-dd'),
                                                           to_char(v_max_date,
                                                                   'yyyy-mm-dd'),
                                                           v_number,
                                                           3,
                                                           stock_top_result_array);
  
        -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
        for i in 1 .. stock_top_result_array.count loop
          insert into mdl_top_stock_detail
            (date_,
             code_,
             name_,
             board_name,
             up_down_percentage,
             period_type)
          values
            (v_stock_date,
             stock_top_result_array(i).get_stock_code(),
             stock_top_result_array(i).get_stock_name(),
             stock_top_result_array(i).get_board_name(),
             stock_top_result_array(i).get_rate(),
             21);
          commit;
        end loop;
      end if;
  
      -- 计算34日内涨幅最大的股票
      if v_row_num >= 34 then
        -- 获取在某个时间范围之内，最大的日期和最小的日期
        select max(date_), min(date_)
          into v_max_date, v_min_date
          from (select distinct t.date_
                  from stock_transaction_data t
                 where t.date_ <= v_stock_date
                 order by t.date_ desc)
         where rownum <= 34;
  
        -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
        -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
        select count(distinct t.code_)
          into v_number
          from stock_transaction_data_all t
         where t.date_ between v_min_date and v_max_date;
        v_number := ceil(v_number * p_number_percentage);
  
        -- 获取涨幅最大的股票
        SCOTT.pkg_stock_transaction_data.FIND_TOP_STOCK(to_char(v_min_date,
                                                                   'yyyy-mm-dd'),
                                                           to_char(v_max_date,
                                                                   'yyyy-mm-dd'),
                                                           v_number,
                                                           3,
                                                           stock_top_result_array);
  
        -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
        for i in 1 .. stock_top_result_array.count loop
          insert into mdl_top_stock_detail
            (date_,
             code_,
             name_,
             board_name,
             up_down_percentage,
             period_type)
          values
            (v_stock_date,
             stock_top_result_array(i).get_stock_code(),
             stock_top_result_array(i).get_stock_name(),
             stock_top_result_array(i).get_board_name(),
             stock_top_result_array(i).get_rate(),
             34);
          commit;
        end loop;
      end if;
  
      -- 计算55日内涨幅最大的股票
      if v_row_num >= 55 then
        -- 获取在某个时间范围之内，最大的日期和最小的日期
        select max(date_), min(date_)
          into v_max_date, v_min_date
          from (select distinct t.date_
                  from stock_transaction_data t
                 where t.date_ <= v_stock_date
                 order by t.date_ desc)
         where rownum <= 55;
  
        -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
        -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
        select count(distinct t.code_)
          into v_number
          from stock_transaction_data_all t
         where t.date_ between v_min_date and v_max_date;
        v_number := ceil(v_number * p_number_percentage);
  
        -- 获取涨幅最大的股票
        SCOTT.pkg_stock_transaction_data.FIND_TOP_STOCK(to_char(v_min_date,
                                                                   'yyyy-mm-dd'),
                                                           to_char(v_max_date,
                                                                   'yyyy-mm-dd'),
                                                           v_number,
                                                           3,
                                                           stock_top_result_array);
  
        -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
        for i in 1 .. stock_top_result_array.count loop
          insert into mdl_top_stock_detail
            (date_,
             code_,
             name_,
             board_name,
             up_down_percentage,
             period_type)
          values
            (v_stock_date,
             stock_top_result_array(i).get_stock_code(),
             stock_top_result_array(i).get_stock_name(),
             stock_top_result_array(i).get_board_name(),
             stock_top_result_array(i).get_rate(),
             55);
          commit;
        end loop;
      end if;
  
    -- 计算89日内涨幅最大的股票
    if v_row_num >= 89 then
                          -- 获取在某个时间范围之内，最大的日期和最小的日期
                          select max(stock_date), min(stock_date)
                            into v_max_date, v_min_date
                            from (select distinct t.stock_date
                                    from stock_moving_average t
                                   where t.stock_date <= v_stock_date
                                   order by t.stock_date desc)
                           where rownum <= 89;
  
                          -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                          -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                          select count(distinct t.code_)
                            into v_number
                            from stock_transaction_data_all t
                           where t.date_ between v_min_date and v_max_date;
                          v_number := ceil(v_number * p_number_percentage);
  
                          -- 获取涨幅最大的股票
                          scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                          'yyyy-mm-dd'),
                                                                  to_char(v_max_date,
                                                                          'yyyy-mm-dd'),
                                                                  v_number,
                                                                  3,
                                                                  stock_top_result_array);
  
                          -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
                          for i in 1 .. stock_top_result_array.count loop
                              insert into mdl_top_stock_detail(date_, code_, name_, board_name, up_down_percentage, period_type)
                                values(v_stock_date, stock_top_result_array(i).get_stock_code(),
                                     stock_top_result_array(i).get_stock_name(), stock_top_result_array(i).get_board_name(),
                                     stock_top_result_array(i).get_rate(), 89);
                                commit;
                          end loop;
                      end if;
  
                      -- 计算144日内涨幅最大的股票
                      if v_row_num >= 144 then
                          -- 获取在某个时间范围之内，最大的日期和最小的日期
                          select max(stock_date), min(stock_date)
                            into v_max_date, v_min_date
                            from (select distinct t.stock_date
                                    from stock_moving_average t
                                   where t.stock_date <= v_stock_date
                                   order by t.stock_date desc)
                           where rownum <= 144;
  
                          -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                          -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                          select count(distinct t.code_)
                            into v_number
                            from stock_transaction_data_all t
                           where t.date_ between v_min_date and v_max_date;
                          v_number := ceil(v_number * p_number_percentage);
  
                          -- 获取涨幅最大的股票
                          scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                          'yyyy-mm-dd'),
                                                                  to_char(v_max_date,
                                                                          'yyyy-mm-dd'),
                                                                  v_number,
                                                                  3,
                                                                  stock_top_result_array);
  
                          -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
                          for i in 1 .. stock_top_result_array.count loop
                              insert into mdl_top_stock_detail(date_, code_, name_, board_name, up_down_percentage, period_type)
                                values(v_stock_date, stock_top_result_array(i).get_stock_code(),
                                     stock_top_result_array(i).get_stock_name(), stock_top_result_array(i).get_board_name(),
                                     stock_top_result_array(i).get_rate(), 144);
                                commit;
                          end loop;
                      end if;
  
                      -- 计算233日内涨幅最大的股票
                      if v_row_num >= 233 then
                          -- 获取在某个时间范围之内，最大的日期和最小的日期
                          select max(stock_date), min(stock_date)
                            into v_max_date, v_min_date
                            from (select distinct t.stock_date
                                    from stock_moving_average t
                                   where t.stock_date <= v_stock_date
                                   order by t.stock_date desc)
                           where rownum <= 233;
  
                          -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                          -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                          select count(distinct t.code_)
                            into v_number
                            from stock_transaction_data_all t
                           where t.date_ between v_min_date and v_max_date;
                          v_number := ceil(v_number * p_number_percentage);
  
                          -- 获取涨幅最大的股票
                          scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                          'yyyy-mm-dd'),
                                                                  to_char(v_max_date,
                                                                          'yyyy-mm-dd'),
                                                                  v_number,
                                                                  3,
                                                                  stock_top_result_array);
  
                          -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
                          for i in 1 .. stock_top_result_array.count loop
                              insert into mdl_top_stock_detail(date_, code_, name_, board_name, up_down_percentage, period_type)
                                values(v_stock_date, stock_top_result_array(i).get_stock_code(),
                                     stock_top_result_array(i).get_stock_name(), stock_top_result_array(i).get_board_name(),
                                     stock_top_result_array(i).get_rate(), 233);
                                commit;
                          end loop;
                      end if;
  
                      -- 计算377日内涨幅最大的股票
                      if v_row_num >= 377 then
                          -- 获取在某个时间范围之内，最大的日期和最小的日期
                          select max(stock_date), min(stock_date)
                            into v_max_date, v_min_date
                            from (select distinct t.stock_date
                                    from stock_moving_average t
                                   where t.stock_date <= v_stock_date
                                   order by t.stock_date desc)
                           where rownum <= 377;
  
                          -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                          -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                          select count(distinct t.code_)
                            into v_number
                            from stock_transaction_data_all t
                           where t.date_ between v_min_date and v_max_date;
                          v_number := ceil(v_number * p_number_percentage);
  
                          -- 获取涨幅最大的股票
                          scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                          'yyyy-mm-dd'),
                                                                  to_char(v_max_date,
                                                                          'yyyy-mm-dd'),
                                                                  v_number,
                                                                  3,
                                                                  stock_top_result_array);
  
                          -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
                          for i in 1 .. stock_top_result_array.count loop
                              insert into mdl_top_stock_detail(date_, code_, name_, board_name, up_down_percentage, period_type)
                                values(v_stock_date, stock_top_result_array(i).get_stock_code(),
                                     stock_top_result_array(i).get_stock_name(), stock_top_result_array(i).get_board_name(),
                                     stock_top_result_array(i).get_rate(), 377);
                                commit;
                          end loop;
                      end if;
  
                      -- 计算610日内涨幅最大的股票
                      if v_row_num >= 610 then
                          -- 获取在某个时间范围之内，最大的日期和最小的日期
                          select max(stock_date), min(stock_date)
                            into v_max_date, v_min_date
                            from (select distinct t.stock_date
                                    from stock_moving_average t
                                   where t.stock_date <= v_stock_date
                                   order by t.stock_date desc)
                           where rownum <= 610;
  
                          -- 查找开始时间p_begin_date和结束时间p_end_date之间的股票数量，
                          -- 然后与p_number_percentage相乘，再取整数，这个就是要使用的股票数量
                          select count(distinct t.code_)
                            into v_number
                            from stock_transaction_data_all t
                           where t.date_ between v_min_date and v_max_date;
                          v_number := ceil(v_number * p_number_percentage);
  
                          -- 获取涨幅最大的股票
                          scott.pkg_moving_average.FIND_TOP_STOCK(to_char(v_min_date,
                                                                          'yyyy-mm-dd'),
                                                                  to_char(v_max_date,
                                                                          'yyyy-mm-dd'),
                                                                  v_number,
                                                                  3,
                                                                  stock_top_result_array);
  
                          -- 按照涨幅，将股票存储在mdl_top_stock_detail表中
                          for i in 1 .. stock_top_result_array.count loop
                              insert into mdl_top_stock_detail(date_, code_, name_, board_name, up_down_percentage, period_type)
                                values(v_stock_date, stock_top_result_array(i).get_stock_code(),
                                     stock_top_result_array(i).get_stock_name(), stock_top_result_array(i).get_board_name(),
                                     stock_top_result_array(i).get_rate(), 610);
                                commit;
                          end loop;
                      end if;
    end loop;
  end WRITE_MDL_TOP_STOCK_DETAIL2;*/

  /*---------------------------------- 判断临时表是否存在，如果不存在，则创建 ----------------------------------------*/
  procedure CREATE_TEMP_MDL_TOP_STOCK is
    -- 用于判断临时表是否存在
    v_temp_table_num number;
    -- 用于存储创建临时表的字符串
    v_temp_table varchar2(200);
  begin
    -- 判断临时表是否存在，如果不存在，则创建
    select count(*)
      into v_temp_table_num
      from user_tables
     where table_name = 'TEMP_MDL_TOP_STOCK';
    if v_temp_table_num = 0 then
      -- 创建临时表temp_top_stock（会话级别）
      v_temp_table := 'CREATE GLOBAL TEMPORARY TABLE TEMP_MDL_TOP_STOCK(
                    id_ NUMBER generated by default as identity (maxvalue 100000000000000000000000000 cycle order),
                    stock_ranking number(4),
                    stock_date date,
                    stock_code varchar2(100),
                    stock_name varchar2(100),
                    board_name varchar2(100),
                    up_down_percentage number)
                ON COMMIT PRESERVE ROWS';
      execute immediate v_temp_table;
    end if;
  end CREATE_TEMP_MDL_TOP_STOCK;

  /*----------------------------------- 向临时表TEMP_MDL_TOP_STOCK中插入数据 ----------------------------------------*/
  procedure INSERT_TEMP_MDL_TOP_STOCK(p_begin_date in varchar2,
                                      p_end_date   in varchar2) is
    -- 根据结束日期，获取top_stock_date字段和period21字段
    cursor cur_top_stock_date is
      select t.date_, t.period21
        from mdl_top_stock t
       where t.date_ = to_date(p_begin_date, 'yyyy-mm-dd')
          or t.date_ = to_date(p_end_date, 'yyyy-mm-dd');
    rt_date_period cur_top_stock_date%rowtype;
  
    -- 存储过程dbms_lob.read用到的参数
    v_buffer varchar2(32767);
    v_amount number;
    v_offset number := 1;
  
    -- 字符串数组，每个元素代表一行
    t_line_split t_str_split;
    -- 字符串数组，每个元素代表一列
    t_column_split t_str_split;
    -- 用于存储至临时表temp_top_stock
    v_stock_ranking      number(10);
    v_stock_date         date;
    v_stock_code         varchar2(100);
    v_stock_name         varchar2(100);
    v_board_name         varchar2(100);
    v_up_down_percentage number;
  begin
    -- 判断临时表是否存在，如果不存在，则创建
    CREATE_TEMP_MDL_TOP_STOCK;
  
    open cur_top_stock_date;
    loop
      fetch cur_top_stock_date
        into rt_date_period;
      exit when cur_top_stock_date%notfound;
    
      -- 将clob类型转换为varchar2类型
      -- 注意：此处要先用dbms_lob获取第二个参数，否则会报错：”ORA-06533下标超出数量“
      v_amount := dbms_lob.getlength(rt_date_period.period21);
      dbms_lob.read(rt_date_period.period21, v_amount, v_offset, v_buffer);
    
      -- 使用换行符（CHR(10)）将字符串切割为字符串数组，每个元素代表一行
      t_line_split := fnc_split(v_buffer, CHR(10));
    
      for i in 1 .. t_line_split.count loop
        -- 使用字符'&'将每一行切割为字符串数组，每个元素代表一列
        t_column_split := fnc_split(t_line_split(i), '&');
      
        -- 向临时表temp_top_stock中插入数据
        v_stock_ranking      := t_column_split(1);
        v_stock_date         := rt_date_period.date_;
        v_stock_code         := t_column_split(2);
        v_stock_name         := t_column_split(3);
        v_board_name         := t_column_split(4);
        v_up_down_percentage := t_column_split(5);
      
        execute immediate 'insert into TEMP_MDL_TOP_STOCK values(:1,:2,:3,:4,:5,:6)'
          using v_stock_ranking, v_stock_date, v_stock_code, v_stock_name, v_board_name, v_up_down_percentage;
        commit;
      end loop;
    end loop;
  end INSERT_TEMP_MDL_TOP_STOCK;

  /*------------------------------------ 将视图V_SUB_NEW_STOCK中的内容打印出来 ----------------------------------------*/
  procedure PRINT_V_SUB_NEW_STOCK is
    -- 创建结构化的数组，用于存储视图v_sub_new_stock中查询到的数据
    type type_v_sub_new_stock_record is record(
      sys_date   varchar2(20),
      stock_code varchar2(20),
      amount     number);
    type type_v_sub_new_stock_table is table of type_v_sub_new_stock_record;
    tp_v_sub_new_stock type_v_sub_new_stock_table;
  
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  begin
    file_handle := UTL_FILE.FOPEN('TXTDIR',
                                  'PRINT_V_SUB_NEW_STOCK.txt',
                                  'w');
  
    execute immediate 'select * from v_sub_new_stock' bulk collect
      into tp_v_sub_new_stock;
  
    for i in 1 .. tp_v_sub_new_stock.count loop
      UTL_FILE.PUT_LINE(file_handle,
                        tp_v_sub_new_stock(i)
                        .sys_date || '   ' || tp_v_sub_new_stock(i)
                        .stock_code || '   ' || tp_v_sub_new_stock(i).amount);
    end loop;
  end PRINT_V_SUB_NEW_STOCK;

  /*----------------------------------- 查询某段时间内次新股的平均价格 ---------------------------------------------*/
  procedure FIND_SUB_NEW_STOCK_AVG_CLOSE(p_begin_date              in varchar2,
                                         p_end_date                in varchar2,
                                         p_sub_new_stock_avg_close out T_SUBNEWSTOCKAVGCLOSE_ARRAY) is
    -- 创建引用游标，用于查询开始时间到结束时间内，所有的交易日期，不能有重复
    cur_stock_date sys_refcursor;
    vc_stock_date  varchar2(500) := 'select distinct t.date_ stock_date
    from stock_transaction_data t
   where t.date_ between to_date(:1, ''yyyy-mm-dd'') and
         to_date(:2, ''yyyy-mm-dd'')
   order by t.date_ asc';
  
    -- 创建typ_stock_date_result类型的record和varray，用于接收开始时间到结束时间内的交易日期
    type typ_stock_date_result is record(
      stock_date date);
    type typ_stock_date_results is varray(1000) of typ_stock_date_result;
    stock_date_results typ_stock_date_results;
  
    -- 用于限制每次fetch的记录数量
    CN_BATCH_SIZE_CONSTANT pls_integer := 1000;
  
    -- 用于查询某个交易日，所有次新股的平均价格
    v_stock_date               date;
    v_avg_stock_close          number;
    vc_sub_new_stock_avg_close varchar2(1500) := 'select t2.date_ stock_date, avg(t2.close_price) avg_stock_close
    from (select "SYS_DATE", "STOCK_CODE", "AMOUNT"
            from (select to_char(:1, ''yyyy-mm-dd'') as sys_date,
                         t1.code_ as stock_code,
                         count(t1.date_) as amount
                    from stock_transaction_data t1
                   where t1.code_ in
                         (select t2.code_
                            from stock_transaction_data t2
                           where t2.date_ = :2)
                   group by t1.code_
                   order by count(t1.date_) asc)
           where rownum <= 30
           order by amount asc) t1,
         stock_transaction_data t2
   where t1.STOCK_CODE = t2.code_
     and t2.date_ = :3
   group by t2.date_
   order by t2.date_ desc';
  
    -- 创建t_sub_new_stock类型的TYPE，用于存储某个交易日的日期和所有此次新股的平均价格
    t_sub_new_stock SCOTT.t_sub_new_stock;
  begin
    -- 初始化T_SUBNEWSTOCKAVGCLOSE_ARRAY类型的数组
    p_sub_new_stock_avg_close := T_SUBNEWSTOCKAVGCLOSE_ARRAY();
  
    -- 将当前session缓存cursor的数量设置为200
    execute immediate 'alter session set session_cached_cursors=200';
  
    open cur_stock_date for vc_stock_date
      using p_begin_date, p_end_date;
    loop
      -- 返回开始时间到结束时间内的交易日期
      fetch cur_stock_date bulk collect
        into stock_date_results limit CN_BATCH_SIZE_CONSTANT;
    
      for i in 1 .. stock_date_results.count loop
        -- 查询某个交易日，所有次新股的平均价格
        execute immediate vc_sub_new_stock_avg_close
          into v_stock_date, v_avg_stock_close
          using stock_date_results(i).stock_date, stock_date_results(i).stock_date, stock_date_results(i).stock_date;
        -- 存储某个交易日的日期和所有此次新股的平均价格
        t_sub_new_stock := SCOTT.t_sub_new_stock(v_stock_date,
                                                 v_avg_stock_close);
        p_sub_new_stock_avg_close.Extend;
        p_sub_new_stock_avg_close(p_sub_new_stock_avg_close.count) := t_sub_new_stock;
      end loop;
      exit when stock_date_results.count < CN_BATCH_SIZE_CONSTANT;
    
    end loop;
    close cur_stock_date;
  
    /*for i in cur_stock_date loop
      v_stock_date := i.stock_date;
      for j in cur_sub_new_stock_avg_close loop
        t_sub_new_stock := scott.t_sub_new_stock(j.stock_date,
                                                 j.avg_stock_close);
        p_sub_new_stock_avg_close.Extend;
        p_sub_new_stock_avg_close(p_sub_new_stock_avg_close.count) := t_sub_new_stock;
        dbms_output.put_line(j.stock_date);
        dbms_output.put_line(j.avg_stock_close);
      end loop;
    end loop;*/
  end FIND_SUB_NEW_STOCK_AVG_CLOSE;

  /*------------------------------------- 查找MDL_TOP_STOCK表中版块分布的数据 ----------------------------------------*/
  procedure FIND_TOP_STOCK_BOARD(p_date in varchar2) is
    -- 创建结构化的数组，用于存储从临时表temp_top_stock中查询到的数据
    type type_group_by_board_record is record(
      board_name varchar2(100),
      amount     number);
    type type_group_by_board_table is table of type_group_by_board_record;
    tp_group_by_board type_group_by_board_table;
  begin
    -- 判断临时表是否存在，如果不存在，则创建
    CREATE_TEMP_MDL_TOP_STOCK;
  
    -- 向临时表TEMP_MDL_TOP_STOCK中插入数据
    INSERT_TEMP_MDL_TOP_STOCK(p_date, p_date);
  
    -- 从临时表temp_top_stock中获取数据，存储到数组type_top_stock中
    execute immediate 'select board_name,count(stock_code) amount
                      from temp_mdl_top_stock t
                      where t.stock_date=to_date(' ||
                      p_date ||
                      ',''yyyy-mm-dd'')
                      group by board_name
                      order by count(stock_code) desc' bulk
                      collect
      into tp_group_by_board;
  
    for i in 1 .. tp_group_by_board.count loop
      dbms_output.put_line('---' || tp_group_by_board(i).board_name ||
                           '---' || tp_group_by_board(i).amount);
    end loop;
  end FIND_TOP_STOCK_BOARD;

  /*-------------------------------------- 查找某段时间内MACD金叉算法的成功率 --------------------------------------------*/
  procedure FIND_MACD_G_C_SUCCESS_RATE(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_number             in number,
                                       p_macd_success_rate_array out T_MACD_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取buy_date字段，去除重复
    /*cursor cur_buy_date is
    select distinct t.buy_date
      from mdl_macd_gold_cross t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.buy_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date; --mdl_macd_gold_cross.buy_date%type;
  
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_macd_rate number;
  
    -- 定义T_MACD_SUCCESS_RATE类型的变量
    t_macd_success_rate SCOTT.T_MACD_SUCCESS_RATE;
  begin
    p_macd_success_rate_array := T_MACD_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_macd_gold_cross t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_macd_gold_cross t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_macd_rate
        from dual;
    
      -- 保存buy_date和v_profit_macd_rate至p_macd_success_rate_array
      t_macd_success_rate := SCOTT.t_macd_success_rate(v_date,
                                                       v_profit_macd_rate);
      p_macd_success_rate_array.Extend;
      p_macd_success_rate_array(p_macd_success_rate_array.count) := t_macd_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_macd_rate);
    
    end loop;
    close cur_date;
  end FIND_MACD_G_C_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内close_price金叉ma5算法的成功率 --------------------------------------------*/
  procedure FIND_C_P_MA5_G_C_SUCCESS_RATE(p_begin_date                 in varchar2,
                                          p_end_date                   in varchar2,
                                          p_date_number                in number,
                                          p_c_p_ma5_success_rate_array out T_C_P_MA5_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取buy_date字段，去除重复
    /*cursor cur_buy_date is
    select distinct t.buy_date
      from mdl_close_price_ma5_gold_cross t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.buy_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date;
  
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_c_p_ma5_g_c_rate number;
  
    -- 定义T_CLOSE_PRICE_MA5_GOLD_CROSS_SUCCESS_RATE类型的变量
    t_close_price_ma5_success_rate SCOTT.T_CLOSE_PRICE_MA5_SUCCESS_RATE;
  begin
    p_c_p_ma5_success_rate_array := T_C_P_MA5_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_close_price_ma5_gold_cross t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_close_price_ma5_gold_cross t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_c_p_ma5_g_c_rate
        from dual;
    
      -- 保存buy_date和v_profit_macd_rate至p_macd_success_rate_array
      t_close_price_ma5_success_rate := SCOTT.t_close_price_ma5_success_rate(v_date,
                                                                             v_profit_c_p_ma5_g_c_rate);
      p_c_p_ma5_success_rate_array.Extend;
      p_c_p_ma5_success_rate_array(p_c_p_ma5_success_rate_array.count) := t_close_price_ma5_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_c_p_ma5_g_c_rate);
    
    end loop;
    close cur_date;
  end FIND_C_P_MA5_G_C_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内ha_close_price上涨趋势算法的成功率 --------------------------------------------*/
  procedure FIND_H_K_A_UPDOWN_SUCCESS_RATE(p_begin_date               in varchar2,
                                           p_end_date                 in varchar2,
                                           p_date_number              in number,
                                           p_h_k_a_success_rate_array out T_H_K_A_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取buy_date字段，去除重复
    /*cursor cur_buy_date is
    select distinct t.buy_date
      from mdl_hei_kin_ashi_up_down t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.buy_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date;
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_h_k_a_up_down_rate number;
    -- 定义T_HEI_KIN_ASHI_UP_DOWN_SUCCESS_RATE类型的变量
    t_hei_kin_ashi_success_rate SCOTT.T_HEI_KIN_ASHI_SUCCESS_RATE;
  begin
    p_h_k_a_success_rate_array := T_H_K_A_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_hei_kin_ashi_up_down t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_hei_kin_ashi_up_down t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_h_k_a_up_down_rate
        from dual;
    
      -- 保存buy_date和v_profit_macd_rate至p_macd_success_rate_array
      t_hei_kin_ashi_success_rate := SCOTT.t_hei_kin_ashi_success_rate(v_date,
                                                                       v_profit_h_k_a_up_down_rate);
      p_h_k_a_success_rate_array.Extend;
      p_h_k_a_success_rate_array(p_h_k_a_success_rate_array.count) := t_hei_kin_ashi_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_h_k_a_up_down_rate);
    
    end loop;
    close cur_date;
  end FIND_H_K_A_UPDOWN_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内日线级别kd金叉算法的成功率 --------------------------------------------*/
  procedure FIND_KD_G_C_SUCCESS_RATE(p_begin_date            in varchar2,
                                     p_end_date              in varchar2,
                                     p_date_number           in number,
                                     p_kd_success_rate_array out T_KD_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取buy_date字段，去除重复
    /*cursor cur_buy_date is
    select distinct t.buy_date
      from mdl_kd_gold_cross t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.buy_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date;
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_mdl_kd_g_c_rate number;
    -- 定义T_KD_GOLD_CROSS_SUCCESS_RATE类型的变量
    t_kd_success_rate SCOTT.T_KD_SUCCESS_RATE;
  begin
    p_kd_success_rate_array := T_KD_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_kd_gold_cross t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_kd_gold_cross t2
                     where t2.buy_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_mdl_kd_g_c_rate
        from dual;
    
      -- 保存buy_date和v_profit_mdl_kd_gold_cross_rate至p_mdl_kd_gold_cross_success_rate_array
      t_kd_success_rate := SCOTT.t_kd_success_rate(v_date,
                                                   v_profit_mdl_kd_g_c_rate);
      p_kd_success_rate_array.Extend;
      p_kd_success_rate_array(p_kd_success_rate_array.count) := t_kd_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_mdl_kd_g_c_rate);
    
    end loop;
    close cur_date;
  end FIND_KD_G_C_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内MACD死叉算法的成功率 --------------------------------------------*/
  procedure FIND_MACD_D_C_SUCCESS_RATE(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_number             in number,
                                       p_macd_success_rate_array out T_MACD_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取sell_date字段，去除重复
    /*cursor cur_sell_date is
    select distinct t.sell_date
      from mdl_macd_dead_cross t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.sell_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date; --mdl_macd_dead_cross.sell_date%type;
  
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_macd_rate number;
  
    -- 定义T_MACD_SUCCESS_RATE类型的变量
    t_macd_success_rate SCOTT.T_MACD_SUCCESS_RATE;
  begin
    p_macd_success_rate_array := T_MACD_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_macd_dead_cross t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_macd_dead_cross t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_macd_rate
        from dual;
    
      -- 保存sell_date和v_profit_macd_rate至p_macd_success_rate_array
      t_macd_success_rate := SCOTT.t_macd_success_rate(v_date,
                                                       v_profit_macd_rate);
      p_macd_success_rate_array.Extend;
      p_macd_success_rate_array(p_macd_success_rate_array.count) := t_macd_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_macd_rate);
    
    end loop;
    close cur_date;
  end FIND_MACD_D_C_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内close_price死叉ma5算法的成功率 --------------------------------------------*/
  procedure FIND_C_P_MA5_D_C_SUCCESS_RATE(p_begin_date                 in varchar2,
                                          p_end_date                   in varchar2,
                                          p_date_number                in number,
                                          p_c_p_ma5_success_rate_array out T_C_P_MA5_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取sell_date字段，去除重复
    /*cursor cur_sell_date is
    select distinct t.sell_date
      from mdl_close_price_ma5_dead_cross t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.sell_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date;
  
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_c_p_ma5_d_c_rate number;
  
    -- 定义T_CLOSE_PRICE_MA5_DEAD_CROSS_SUCCESS_RATE类型的变量
    t_close_price_ma5_success_rate SCOTT.T_CLOSE_PRICE_MA5_SUCCESS_RATE;
  begin
    p_c_p_ma5_success_rate_array := T_C_P_MA5_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_close_price_ma5_dead_cross t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_close_price_ma5_dead_cross t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_c_p_ma5_d_c_rate
        from dual;
    
      -- 保存sell_date和v_profit_macd_rate至p_macd_success_rate_array
      t_close_price_ma5_success_rate := SCOTT.t_close_price_ma5_success_rate(v_date,
                                                                             v_profit_c_p_ma5_d_c_rate);
      p_c_p_ma5_success_rate_array.Extend;
      p_c_p_ma5_success_rate_array(p_c_p_ma5_success_rate_array.count) := t_close_price_ma5_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_c_p_ma5_d_c_rate);
    
    end loop;
    close cur_date;
  end FIND_C_P_MA5_D_C_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内ha_close_price下跌趋势算法的成功率 --------------------------------------------*/
  procedure FIND_H_K_A_DOWNUP_SUCCESS_RATE(p_begin_date               in varchar2,
                                           p_end_date                 in varchar2,
                                           p_date_number              in number,
                                           p_h_k_a_success_rate_array out T_H_K_A_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取sell_date字段，去除重复
    /*cursor cur_sell_date is
    select distinct t.sell_date
      from mdl_hei_kin_ashi_down_up t
     where t.buy_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.buy_date < to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date >= to_date(p_begin_date, 'yyyy-mm-dd') and
           t.sell_date < to_date(p_end_date, 'yyyy-mm-dd')
     order by t.sell_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date;
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_h_k_a_down_up_rate number;
    -- 定义T_HEI_KIN_ASHI_DOWN_UP_SUCCESS_RATE类型的变量
    t_hei_kin_ashi_success_rate SCOTT.T_HEI_KIN_ASHI_SUCCESS_RATE;
  begin
    p_h_k_a_success_rate_array := T_H_K_A_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_hei_kin_ashi_down_up t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_hei_kin_ashi_down_up t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_h_k_a_down_up_rate
        from dual;
    
      -- 保存sell_date和v_profit_macd_rate至t_macd_success_rate_array
      t_hei_kin_ashi_success_rate := SCOTT.t_hei_kin_ashi_success_rate(v_date,
                                                                       v_profit_h_k_a_down_up_rate);
      p_h_k_a_success_rate_array.Extend;
      p_h_k_a_success_rate_array(p_h_k_a_success_rate_array.count) := t_hei_kin_ashi_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_h_k_a_down_up_rate);
    
    end loop;
    close cur_date;
  end FIND_H_K_A_DOWNUP_SUCCESS_RATE;

  /*-------------------------------------- 查找某段时间内日线级别kd死叉算法的成功率 --------------------------------------------*/
  procedure FIND_KD_D_C_SUCCESS_RATE(p_begin_date            in varchar2,
                                     p_end_date              in varchar2,
                                     p_date_number           in number,
                                     p_kd_success_rate_array out T_KD_SUCCESS_RATE_ARRAY) is
    -- 根据开始时间和结束时间获取sell_date字段，去除重复
    /*cursor cur_sell_date is
    select distinct t.sell_date
      from mdl_kd_dead_cross t
     where t.buy_date between to_date(p_begin_date, 'yyyy-mm-dd') and
           to_date(p_end_date, 'yyyy-mm-dd')
       and t.sell_date between to_date(p_begin_date, 'yyyy-mm-dd') and
           to_date(p_end_date, 'yyyy-mm-dd')
     order by t.sell_date asc;*/
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ < to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
    -- 日期
    v_date date;
    -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
    v_profit_mdl_kd_d_c_rate number;
    -- 定义T_KD_DEAD_CROSS_SUCCESS_RATE类型的变量
    t_kd_success_rate SCOTT.T_KD_SUCCESS_RATE;
  begin
    p_kd_success_rate_array := T_KD_SUCCESS_RATE_ARRAY();
    open cur_date;
    loop
      fetch cur_date
        into v_date;
      exit when cur_date%notfound;
    
      -- 开始时间和结束时间范围内，获利交易的次数除以全部交易的次数
      select round((select count(*)
                      from mdl_kd_dead_cross t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date
                       and t2.profit_loss > 0) /
                   (select count(*)
                      from mdl_kd_dead_cross t2
                     where t2.sell_date in
                           (select *
                              from (select distinct t.date_
                                      from stock_transaction_data_all t
                                     where t.date_ <= v_date
                                     order by t.date_ desc) b
                             where rownum <= p_date_number)
                       and t2.buy_date <= v_date
                       and t2.sell_date <= v_date) * 100,
                   2)
        into v_profit_mdl_kd_d_c_rate
        from dual;
    
      -- 保存sell_date和v_profit_mdl_kd_dead_cross_rate至p_mdl_kd_dead_cross_success_rate_array
      t_kd_success_rate := SCOTT.t_kd_success_rate(v_date,
                                                   v_profit_mdl_kd_d_c_rate);
      p_kd_success_rate_array.Extend;
      p_kd_success_rate_array(p_kd_success_rate_array.count) := t_kd_success_rate;
      dbms_output.put_line(v_date);
      dbms_output.put_line(v_profit_mdl_kd_d_c_rate);
    
    end loop;
    close cur_date;
  end FIND_KD_D_C_SUCCESS_RATE;
end PKG_MODEL_RECORD;
/

