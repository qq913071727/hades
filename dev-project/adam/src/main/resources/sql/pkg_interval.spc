???create or replace package scott.pkg_interval is

  /*--------------------------------------------------------------------------------------------------------------------*/
  /*---------------------------------- 死叉/金叉持续天数与之后的金叉/死叉获利的交易次数的关系 --------------------------*/
  /*--------------------------------------------------------------------------------------------------------------------*/
  /*----------------- macd死叉持续天数与之后的金叉获利的交易次数的关系 ----------------*/
  procedure find_macd_g_c_interval_t_n(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor);

  /*----------------- macd死叉持续天数与之后的金叉获利的交易次数的关系 ----------------*/
  procedure find_macd_d_c_interval_t_n(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor);

  /*----------- close_price死叉ma5持续天数与之后的金叉获利的交易次数的关系 ------------*/
  procedure find_c_p_ma5_g_c_interval_t_n(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor);

  /*----------- close_price金叉ma5持续天数与之后的死叉获利的交易次数的关系 ------------*/
  procedure find_c_p_ma5_d_c_interval_t_n(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- hei_kin_ashi下跌趋势持续天数与之后的上涨趋势获利的交易次数的关系  -------*/
  procedure find_h_k_a_u_d_interval_t_n(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- hei_kin_ashi上涨趋势持续天数与之后的下跌趋势获利的交易次数的关系  -------*/
  procedure find_h_k_a_d_u_interval_t_n(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------------------- kd死叉持续天数与之后的金叉获利的交易次数的关系 ------------------*/
  procedure find_kd_g_c_interval_t_n(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------------------- kd金叉持续天数与之后的死叉获利的交易次数的关系 ------------------*/
  procedure find_kd_d_c_interval_t_n(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------------------------------------------------------------------------------------------------------------------*/
  /*----------------------- 金叉/死叉获利的交易之前的那个死叉/死叉的持续天数与这次死叉/金叉的成功率的关系 --------------*/
  /*--------------------------------------------------------------------------------------------------------------------*/
  /*--------- macd金叉获利的交易之前的那个macd死叉的持续天数与这次金叉的成功率的关系 --------*/
  procedure find_macd_g_c_interval_s_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- macd死叉获利的交易之前的那个macd金叉的持续天数与这次死叉的成功率的关系 --------*/
  procedure find_macd_d_c_interval_s_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- close_price金叉ma5获利的交易之前的那个close_price死叉ma5的持续天数与这次金叉的成功率的关系 --------*/
  procedure find_c_p_ma5_g_c_interval_s_r(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- close_price死叉ma5获利的交易之前的那个close_price金叉ma5的持续天数与这次死叉的成功率的关系 --------*/
  procedure find_c_p_ma5_d_c_interval_s_r(p_begin_date              in varchar2,
                                          p_end_date                in varchar2,
                                          p_date_num                in number,
                                          p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------ hei_kin_ashi上升趋势获利的交易之前的那个hei_kin_ashi下跌趋势的持续天数与这次上升趋势的成功率的关系 -----*/
  procedure find_h_k_a_u_d_interval_s_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------ hei_kin_ashi下跌趋势获利的交易之前的那个hei_kin_ashi上升趋势的持续天数与这次下跌趋势的成功率的关系 -----*/
  procedure find_h_k_a_d_u_interval_s_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- kd金叉获利的交易之前的那个kd死叉的持续天数与这次金叉的成功率的关系 --------*/
  procedure find_kd_g_c_interval_s_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------- kd死叉获利的交易之前的那个kd金叉的持续天数与这次死叉的成功率的关系 --------*/
  procedure find_kd_d_c_interval_s_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor);

  /*--------------------------------------------------------------------------------------------------------------------*/
  /*------------------------------------ 金叉/死叉之前的死叉持续天数与金叉/死叉平均收益率的关系 --------------------------*/
  /*--------------------------------------------------------------------------------------------------------------------*/
  /*------------------ macd金叉之前的死叉持续天数与金叉平均收益率的关系 -----------------*/
  procedure find_macd_g_c_interval_p_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor);

  /*----------------- macd死叉之前的金叉持续天数与死叉平均收益率的关系 ----------------*/
  procedure find_macd_d_c_interval_p_r(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_num                in number,
                                       p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------------------ close_price金叉ma5之前的死叉持续天数与金叉平均收益率的关系 -----------------*/
  procedure find_cp_ma5_gc_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------------------ close_price死叉ma5之前的金叉持续天数与死叉平均收益率的关系 -----------------*/
  procedure find_cp_ma5_dc_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------ hei_kin_ashi上升趋势之前的下跌趋势持续天数与上升趋势平均收益率的关系 -----*/
  procedure find_h_k_a_u_d_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------ hei_kin_ashi下跌趋势之前的上升趋势持续天数与下跌趋势平均收益率的关系 -----*/
  procedure find_h_k_a_d_u_interval_p_r(p_begin_date              in varchar2,
                                        p_end_date                in varchar2,
                                        p_date_num                in number,
                                        p_g_c_d_c_interval_cursor out sys_refcursor);

  /*------------------ kd金叉之前的死叉持续天数与金叉平均收益率的关系 -----------------*/
  procedure find_kd_g_c_interval_p_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor);

  /*----------------- kd死叉之前的金叉持续天数与死叉平均收益率的关系 ----------------*/
  procedure find_kd_d_c_interval_p_r(p_begin_date              in varchar2,
                                     p_end_date                in varchar2,
                                     p_date_num                in number,
                                     p_g_c_d_c_interval_cursor out sys_refcursor);

end pkg_interval;
/

