???create or replace procedure scott.WriteStocks(stockDate in date, code in nvarchar2, stockOpen in number,
stockHigh in number, stockClose in number, stockLow in number, stockAmount in number) is
begin
  INSERT into STOCK_MOVING_AVERAGE(STOCK_DATE,STOCK_CODE,STOCK_OPEN,STOCK_HIGH,STOCK_CLOSE,STOCK_LOW,STOCK_AMOUNT)
  values(stockDate,code,stockOpen,stockHigh,stockClose,stockLow,stockAmount);
end WriteStocks;
/

