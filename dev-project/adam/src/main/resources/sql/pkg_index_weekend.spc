???create or replace package scott.PKG_INDEX_WEEKEND is
    -- 计算表STOCK_INDEX_WEEKEND的所有记录
    procedure CAL_STK_IDX_WKD;

    -- 计算表STOCK_INDEX_WEEKEND的所有Hei Kin Ashi字段
    procedure CAL_STK_IDX_WKD_HA;

    -- 按照日期，计算表STOCK_INDEX_WEEKEND的所有记录
    procedure CAL_STK_IDX_WKD_BD(p_begin_date in varchar2,
                                 p_end_date   in varchar2);

    -- 按照日期，计算表STOCK_INDEX_WEEKEND的Hei Kin Ashi记录
    procedure CAL_STK_IDX_WKD_HA_BD(p_begin_date in varchar2,
                                    p_end_date   in varchar2);

end PKG_INDEX_WEEKEND;
/

