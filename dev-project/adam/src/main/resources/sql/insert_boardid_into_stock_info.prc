???create or replace procedure scott.INSERT_BOARDID_INTO_STOCK_INFO is
begin
    declare
  cursor cur_stock_info is select * from stock_info;
  cursor cur_board is select * from board;
  begin
         for i in cur_stock_info loop
             for j in cur_board loop
                 if i.url_param=j.name then
                     update stock_info set board_id=j.id where code_=i.code_;
                 end if;
             end loop;
         end loop;
         null;
  end;
end INSERT_BOARDID_INTO_STOCK_INFO;
/

