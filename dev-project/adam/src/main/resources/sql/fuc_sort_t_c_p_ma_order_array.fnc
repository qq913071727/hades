???create or replace function scott.FUC_SORT_T_C_P_MA_ORDER_ARRAY(t_close_price_ma_order_array in SCOTT.T_CLOSE_PRICE_MA_ORDER_ARRAY)
  return SCOTT.t_close_price_ma_order_array as
  -- 临时类型数组
  t_c_p_ma_order_array_temp SCOTT.t_close_price_ma_order_array;
  -- 临时类型
  t_c_p_ma_order_temp SCOTT.t_close_price_ma_order;
begin
  -- 初始化
  t_c_p_ma_order_array_temp := t_close_price_ma_order_array;

  -- 冒泡算法，降序排列
  for i in 1 .. (t_c_p_ma_order_array_temp.count - 1) loop
    for j in 1 .. (t_c_p_ma_order_array_temp.count - i) loop
      if t_c_p_ma_order_array_temp(j).get_value < t_c_p_ma_order_array_temp(j + 1).get_value then
        t_c_p_ma_order_temp := t_c_p_ma_order_array_temp(j);
        t_c_p_ma_order_array_temp(j) := t_c_p_ma_order_array_temp(j + 1);
        t_c_p_ma_order_array_temp(j + 1) := t_c_p_ma_order_temp;
      end if;
    end loop;
  end loop;

  return(t_c_p_ma_order_array_temp);
end FUC_SORT_T_C_P_MA_ORDER_ARRAY;
/

