???create or replace type scott.T_STOCK_RESULT as object(
       code_ varchar2(100),
       date_ date,

       member function get_code return varchar2,
       member function get_date return date,
       member procedure set_code(p_stock_code varchar2),
       member procedure set_date(p_stock_date date)
)not final
/

