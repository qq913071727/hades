???create or replace type scott.T_STOCK_WEEKEND_RESULT as object
(
       stockWeekendCode varchar2(10),
       stockWeekendBeginDate date,
       stockWeekendEndDate date,

       member function getStockWeekendCode return varchar2,
       member procedure setStockWeekendCode(paramStockWeekendCode varchar2),
       member function getStockWeekendBeginDate return date,
       member procedure setStockWeekendBeginDate(paramStockWeekendBeginDate date),
       member function getStockWeekendEndDate return date,
       member procedure setStockWeekendEndDate(paramStockWeekendEndDate date)
)
/

