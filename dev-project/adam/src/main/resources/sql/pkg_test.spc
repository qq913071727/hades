???create or replace package scott.PKG_TEST is

  -- 向mdl_stock_top_detail表中导入数据
  /*procedure IMPORT_MDL_STOCK_TOP_DETAIL(p_date_number in number, p_period in number);*/

  -- 计算MACD金叉、close_price金叉MA5、hei_kin_ashi上涨趋势、KD金叉算法在周线级别KD金叉时的成功率
  /*procedure CAL_SUCCESS_RATE_WITH_W_KD_G_C;*/

  /*--------------------- 根据ma的形态和金叉死叉的方式来计算交易数据 -----------------------*/
  procedure CAL_MA_PATTERN_OVERSOLD(p_ma_pattern                 varchar2,
                                    p_gold_cross_dead_cross_type varchar2,
                                    p_direction                  number);

  /*--------------- 选择某段时间内，4中算法中（金叉/死叉），成功率最高的股票 ---------------*/
  procedure filter_by_max_success_rate(p_need_stock_number          number,
                                       p_macd_begin_date            varchar,
                                       p_macd_end_date              varchar,
                                       p_close_price_ma5_begin_date varchar,
                                       p_close_price_ma5_end_date   varchar,
                                       p_hei_kin_ashi_begin_date    varchar,
                                       p_hei_kin_ashi_end_date      varchar,
                                       p_kd_begin_date              varchar,
                                       p_kd_end_date                varchar);



  /*--------------------- 海龟交易法则 ---------------------*/
  procedure turtle;

  /*--------------------- 反趋势交易 ---------------------*/
  procedure countertrend;
end PKG_TEST;
/

