???create or replace package scott.pkg_real4_transaction is

  -- 向real4_transaction_condition表中写数据
  procedure write_real4_transac_cond(p_begin_date in varchar2,
                                     p_date       in varchar2,
                                     /*p_less_than_percentage         in number,
                                                                                                                                                                                     p_more_than_percentage         in number,
                                                                                                                                                                                     p_close_price_start            in number,
                                                                                                                                                                                     p_close_price_end              in number,*/
                                     p_ma120_not_decrea             in number,
                                     p_ma120_not_decrea_date_number in number,
                                     p_ma250_not_decrea             in number,
                                     p_ma250_not_decrea_date_number in number,
                                     p_ma120_not_increa             in number,
                                     p_ma120_not_increa_date_number in number,
                                     p_ma250_not_increa             in number,
                                     p_ma250_not_increa_date_number in number);

end pkg_real4_transaction;
/

