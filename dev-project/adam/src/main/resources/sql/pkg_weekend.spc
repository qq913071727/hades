???create or replace package scott.PKG_WEEKEND is
       -- calculate the basic data such as open price, close price and so on
       procedure WRITE_STOCK_WEEK;

       -- calculate KD
       procedure WRITE_WEEKEND_KD_INIT;
       procedure WRITE_WEEKEND_KD_RSV;
       procedure WRITE_WEEKEND_KD_K;
       procedure WRITE_WEEKEND_KD_D;

       -- calculate up_down
       procedure WRITE_WEEKEND_UP_DOWN;
       procedure WRITE_WEEKEND_UP_DOWN_BY_DATE(weekendBeginDate in varchar2,weekendEndDate in varchar2);

       -- calculate the basic data such as open price, close price and so on by date
       procedure WRITE_STOCK_WEEKEND_BY_DATE(beginDate varchar2,endDate varchar2);

       -- calculate KD by date
       procedure WRITE_WEEKEND_KD_BY_DATE_RSV(beginDate varchar2,endDate varchar2);
       procedure WRITE_WEEKEND_KD_BY_DATE_K(beginDate varchar2,endDate varchar2);
       procedure WRITE_WEEKEND_KD_BY_DATE_D(beginDate varchar2,endDate varchar2);

       -- 计算MACD
       procedure WRITE_WEEKEND_MACD_INIT;
       procedure WRITE_WEEKEND_MACD_EMA;
       procedure WRITE_WEEKEND_MACD_DIF;
       procedure WRITE_WEEKEND_MACD_DEA;
       procedure WRITE_WEEKEND_MACD_EMA_BY_DATE(stockWeekendBeginDate in varchar2,stockWeekendEndDate in varchar2);
       procedure WRITE_WEEKEND_MACD_DIF_BY_DATE(stockWeekendBeginDate in varchar2,stockWeekendEndDate in varchar2);
       procedure WRITE_WEEKEND_MACD_DEA_BY_DATE(stockWeekendBeginDate in varchar2,stockWeekendEndDate in varchar2);

       procedure FIND_WEEKEND_MACD_END_DEVIATE(stockWeekendBeginDate in varchar2,stockWeekendEndDate in varchar2,rateWeekendDate in varchar2,stockWeekendResult out clob);

       -- select the gold cross of kd index in weekend scale
       procedure SELECT_WEEKEND_KD_UP(beginDate in varchar2,endDate in varchar2,crossPoint in number,stockWeekendResultArray out T_STOCK_WEEKEND_RESULT_ARRAY);
end PKG_WEEKEND;
/

