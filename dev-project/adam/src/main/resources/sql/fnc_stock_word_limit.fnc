???create or replace function scott.FNC_STOCK_WORD_LIMIT(p_stock_code     in varchar2,
                                                p_begin_date     in varchar2,
                                                p_end_date       in varchar2,
                                                p_word_limit_num number)
    return number is
    /* 通常某只股票在长期停牌后，一旦复牌会出现连续上涨的情况。此时表现为一字涨停板和极低的成交量。
    * 此外新股刚刚开始上市交易时也会出现这种情况
    * 而这两种情况使得很难进行买卖交易，因此应该放弃。
    * 这个函数的作用是判断某一只股票在某段时间内是否出现p_word_limit_num次一字涨停板。如果是，则返回1；否则，返回-1
    * 判断标准是开盘价，收盘价，最高价，最低价都一样
    * 这个函数通常与函数FNC_STOCK_STOP一起使用 */
    result number;
    -- 表示表STOCK_MOVING_AVERAGE类型的记录
    t_stock stock_moving_average%rowtype;
    -- 表示出现的一字涨停板的次数
    v_word_limit_num number;
    -- 获得某只股票在某段时间内涨停的记录，按降序排列
    cursor cur_word_limit_stock is
        select *
          from stock_moving_average t
         where t.stock_date between to_date(p_begin_date, 'yyyy-mm-dd') and
               to_date(p_end_date, 'yyyy-mm-dd')
           and t.today_up_down_percentage >= 9.9
           and t.stock_code = p_stock_code
         order by t.stock_date desc;
begin
    -- 初始化
    v_word_limit_num := 0;
    result           := -1;

    for i in cur_word_limit_stock loop
        t_stock := i;
        -- 判断某只股票在某个交易日是否是一字涨停板。判断方法：开盘价，收盘价，最高价，最低价都一样
        if i.stock_open = i.stock_close and i.stock_open = i.stock_high and
           i.stock_open = i.stock_low then
            v_word_limit_num := v_word_limit_num + 1;
            -- 如果某只股票在某个交易日内一字涨停板的次数到达了p_word_limit_num，则返回1
            if v_word_limit_num = p_word_limit_num then
                result := 1;
            end if;
        end if;
    end loop;
    return(result);
end FNC_STOCK_WORD_LIMIT;
/

