???create or replace type body scott.T_SUB_NEW_STOCK is
       member function getSubNewStockDate return date is
       begin
            return sub_new_stock_date;
       end getSubNewStockDate;

       member procedure setSubNewStockDate(paramSubNewStockDate date) is
       begin
            sub_new_stock_date:=paramSubNewStockDate;
       end setSubNewStockDate;

       member function getSubNewStockAvgClose return number is
       begin
            return sub_new_stock_avg_close;
       end getSubNewStockAvgClose;

       member procedure setSubNewStockAvgClose(paramSubNewStockAvgClose number) is
       begin
            sub_new_stock_avg_close:=paramSubNewStockAvgClose;
       end setSubNewStockAvgClose;
end;
/

