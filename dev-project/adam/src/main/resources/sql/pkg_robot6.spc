???create or replace package scott.PKG_ROBOT6 is

  /****************************************** 卖etf **********************************************/
  -- 根据当日持有etf的收盘价，在卖etf之前，更新robot6_account表
  -- 游标名称update_robot_account_before_sell_or_buy
  procedure update_robot_account_b_s_b(p_date in varchar2);

  -- 卖etf，只修改robot6_stock_transact_record表中的记录
  /* p_mandatory_stop_loss等于1，表示有强制止损；p_mandatory_stop_loss等于0，表示没有强制止损 */
  procedure sell_or_buy(p_date                     in varchar2,
                        p_mandatory_stop_loss      in number,
                        p_mandatory_stop_loss_rate in number,
                        p_mdl_etf_type             in number);

  -- 根据当日卖etf的收盘价，在卖etf之后，更新robot6_account表
  procedure update_robot_account_after_s_b(p_date in varchar2);

  /***************************** 买etf之前的过滤，确定哪些etf能买 *********************************/
  -- 过滤条件：删除除过权的etf
  procedure filter_by_xr(p_begin_date in varchar2, p_end_date in varchar2);

  -- 过滤条件：MA不单调递减，过滤股票
  PROCEDURE filter_by_ma_not_decreasing(p_ma_level in number,
                                        p_date     in varchar2,
                                        p_rownum   in number);

  -- 更新robot6_stock_filter表：direction为1；filter_type为交易算法；accumulative_profit_rate累计收益。单位：元
  PROCEDURE update_robot_stock_filter(p_date in varchar2);

  -- 过滤条件：从robot6_stock_filter表中删除那些不是交易日的记录
  PROCEDURE filter_by_buy_date(p_buy_date     in varchar2,
                               p_mdl_etf_type in number);

  /****************************************** 买etf **********************************************/
  -- 买股票，只向robot6_stock_transaction_record表中插入记录，只更新robot6_account表的hold_stock_number字段
  -- 10个股票平分仓位
  procedure buy_or_sell_avg_hold_space(p_buy_date            in varchar2,
                                       p_max_hold_etf_number in number);

  -- 买股票，只向robot6_stock_transaction_record表中插入记录，只更新robot6_account表的hold_stock_number字段
  -- 尽量填满所有仓位
  procedure buy_or_sell_max_hold_space(p_buy_date            in varchar2,
                                       p_max_hold_etf_number in number);

  -- 根据当日买入etf的收盘价，在买etf之后，更新robot_account表
  -- 存储过程名称：update_robot_account_after_buy_or_sell
  procedure update_robot_account_after_b_s(p_date in varchar2);
end PKG_ROBOT6;
/

