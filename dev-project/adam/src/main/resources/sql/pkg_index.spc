???create or replace package scott.PKG_INDEX is

  -- 插入上证指数，深证成指，中小板指数和创业板指数（只包括基础数据）
  procedure WRITE_STOCK_INDEX(p_code          in nvarchar2,
                              p_date          in date,
                              p_open_price    in number,
                              p_highest_price in number,
                              p_close_price   in number,
                              p_lowest_price  in number,
                              p_change_amount in number,
                              p_change_range  in number,
                              p_volume        in number,
                              p_turnover      in number);

  -- 插入上证指数，深证成指，中小板指数和创业板指数（包括基础数据和Hei Kin Ashi数据）
  procedure SAVE_STOCK_INDEX(p_code                 in nvarchar2,
                             p_date                 in date,
                             p_open_price           in number,
                             p_highest_price        in number,
                             p_close_price          in number,
                             p_lowest_price         in number,
                             p_change_amount        in number,
                             p_change_range         in number,
                             p_volume               in number,
                             p_turnover             in number,
                             p_ma5                  in number,
                             p_ma10                 in number,
                             p_ma20                 in number,
                             p_ma60                 in number,
                             p_ma120                in number,
                             p_ma250                in number,
                             ha_index_open_price    in number,
                             ha_index_highest_price in number,
                             ha_index_close_price   in number,
                             ha_index_lowest_price  in number,
                             p_bias5                in number,
                             p_bias10               in number,
                             p_bias20               in number,
                             p_bias60               in number,
                             p_bias120              in number,
                             p_bias250              in number);

  -- 计算简单移动平均线
  PROCEDURE WRITE_FIVE;
  PROCEDURE WRITE_TEN;
  PROCEDURE WRITE_TWENTY;
  PROCEDURE WRITE_SIXTY;
  PROCEDURE WRITE_ONEHUNDREDTWENTY;
  PROCEDURE WRITE_TWOHUNDREDFIFTY;

  -- 计算上证指数，深证成指，中小板指数和创业板指数的hei kin ashi的开盘价，收盘价，最高价和最低价
  procedure WRITE_STOCK_INDEX_HA;

  -- 计算所有指数的乖离率
  procedure WRITE_STOCK_INDEX_BIAS;

  -- 计算某一天所有指数的简单移动平均线
  PROCEDURE WRITE_STOCK_INDEX_MA_BY_DATE(p_date in varchar2);

  -- 按日期，计算上证指数，深证成指，中小板指数和创业板指数的hei kin ashi的开盘价，收盘价，最高价和最低价
  procedure WRITE_STOCK_INDEX_HA_BY_DATE(p_date in varchar2);

  -- 按照日期，计算所有指数在某一日的乖离率
  procedure WRITE_STOCK_INDEX_BIAS_BY_DATE(p_date in varchar2);

  -- 计算MACD
  procedure WRITE_MACD_INIT;
  procedure WRITE_MACD_EMA;
  procedure WRITE_MACD_DIF;
  procedure WRITE_MACD_DEA;
  procedure WRITE_MACD;
  procedure WRITE_MACD_EMA_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_DIF_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_DEA_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_BY_DATE(p_date in varchar2);

  -- 计算KD
  procedure WRITE_KD_INIT;
  procedure WRITE_KD_RSV;
  procedure WRITE_KD_K;
  procedure WRITE_KD_D;

  -- 计算每日的kd
  procedure WRITE_KD_BY_DATE_RSV(p_date varchar2);
  procedure WRITE_KD_BY_DATE_K(p_date varchar2);
  procedure WRITE_KD_BY_DATE_D(p_date varchar2);

-- 海量地向表MDL_STOCK_INDEX_MACD_G_C中插入数据
-- procedure CAL_MDL_STOCK_INDEX_MACD_G_C;
end PKG_INDEX;
/

