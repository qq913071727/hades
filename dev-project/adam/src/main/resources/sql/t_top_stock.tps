???create or replace type scott.T_TOP_STOCK as object
(
  v_code  varchar2(100),
  v_name  varchar2(100),
  v_board_name varchar2(100),
  v_up_down_percentage number,

  member function get_code return varchar2,
  member function get_name return varchar2,
  member function get_board_name return varchar2,
  member function get_up_down_percentage return number,

  member procedure set_code(p_code varchar2),
  member procedure set_name(p_name varchar2),
  member procedure set_board_name(p_board_name varchar2),
  member procedure set_up_down_percentage(p_up_down_percentage number)
)
/

