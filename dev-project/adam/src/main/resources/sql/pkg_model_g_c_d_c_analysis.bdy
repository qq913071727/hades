???create or replace package body scott.PKG_MODEL_G_C_D_C_ANALYSIS is

  /*----------------- 计算mdl_g_c_d_c_analysis表中的所有百分比的均值 ----------------------*/
  procedure WRITE_MDL_G_C_D_C_A_PERCENT_MA is
    -- 日期数量
    v_date_count number;
    -- 百分比均价
    v_macd_g_c_percent_ma5     NUMBER;
    v_macd_g_c_percent_ma10    NUMBER;
    v_macd_g_c_percent_ma20    NUMBER;
    v_macd_d_c_percent_ma5     NUMBER;
    v_macd_d_c_percent_ma10    NUMBER;
    v_macd_d_c_percent_ma20    NUMBER;
    v_c_p_g_c_ma5_percent_ma5  NUMBER;
    v_c_p_g_c_ma5_percent_ma10 NUMBER;
    v_c_p_g_c_ma5_percent_ma20 NUMBER;
    v_c_p_d_c_ma5_percent_ma5  NUMBER;
    v_c_p_d_c_ma5_percent_ma10 NUMBER;
    v_c_p_d_c_ma5_percent_ma20 NUMBER;
    v_h_k_a_u_d_percent_ma5    NUMBER;
    v_h_k_a_u_d_percent_ma10   NUMBER;
    v_h_k_a_u_d_percent_ma20   NUMBER;
    v_h_k_a_d_u_percent_ma5    NUMBER;
    v_h_k_a_d_u_percent_ma10   NUMBER;
    v_h_k_a_d_u_percent_ma20   NUMBER;
    v_kd_g_c_percent_ma5       NUMBER;
    v_kd_g_c_percent_ma10      NUMBER;
    v_kd_g_c_percent_ma20      NUMBER;
    v_kd_d_c_percent_ma5       NUMBER;
    v_kd_d_c_percent_ma10      NUMBER;
    v_kd_d_c_percent_ma20      NUMBER;
    -- mdl_g_c_d_c_analysis表类型的变量
    row_mdl_g_c_d_c_analysis mdl_g_c_d_c_analysis%rowtype;
    -- mdl_g_c_d_c_analysis表按照date_字段降序排列
    cursor cur_analysis_desc is
      select * from mdl_g_c_d_c_analysis t order by t.date_ desc;
  begin
    -- 计算日期的数量
    select count(distinct t.date_)
      into v_date_count
      from mdl_g_c_d_c_analysis t;

    open cur_analysis_desc;
    loop
      fetch cur_analysis_desc
        into row_mdl_g_c_d_c_analysis;
      exit when cur_analysis_desc%notfound;

      -- 重置
      v_macd_g_c_percent_ma5     := null;
      v_macd_g_c_percent_ma10    := null;
      v_macd_g_c_percent_ma20    := null;
      v_macd_d_c_percent_ma5     := null;
      v_macd_d_c_percent_ma10    := null;
      v_macd_d_c_percent_ma20    := null;
      v_c_p_g_c_ma5_percent_ma5  := null;
      v_c_p_g_c_ma5_percent_ma10 := null;
      v_c_p_g_c_ma5_percent_ma20 := null;
      v_c_p_d_c_ma5_percent_ma5  := null;
      v_c_p_d_c_ma5_percent_ma10 := null;
      v_c_p_d_c_ma5_percent_ma20 := null;
      v_h_k_a_u_d_percent_ma5    := null;
      v_h_k_a_u_d_percent_ma10   := null;
      v_h_k_a_u_d_percent_ma20   := null;
      v_h_k_a_d_u_percent_ma5    := null;
      v_h_k_a_d_u_percent_ma10   := null;
      v_h_k_a_d_u_percent_ma20   := null;
      v_kd_g_c_percent_ma5       := null;
      v_kd_g_c_percent_ma10      := null;
      v_kd_g_c_percent_ma20      := null;
      v_kd_d_c_percent_ma5       := null;
      v_kd_d_c_percent_ma10      := null;
      v_kd_d_c_percent_ma20      := null;

      -- ma5
      if v_date_count >= 5 then
        select avg(t1.macd_g_c_percent),
               avg(t1.macd_d_c_percent),
               avg(t1.close_price_g_c_ma5_percent),
               avg(t1.close_price_d_c_ma5_percent),
               avg(t1.hei_kin_ashi_u_d_percent),
               avg(t1.hei_kin_ashi_d_u_percent),
               avg(t1.kd_g_c_percent),
               avg(t1.kd_d_c_percent)
          into v_macd_g_c_percent_ma5,
               v_macd_d_c_percent_ma5,
               v_c_p_g_c_ma5_percent_ma5,
               v_c_p_d_c_ma5_percent_ma5,
               v_h_k_a_u_d_percent_ma5,
               v_h_k_a_d_u_percent_ma5,
               v_kd_g_c_percent_ma5,
               v_kd_d_c_percent_ma5
          from (select *
                  from mdl_g_c_d_c_analysis t
                 where t.date_ <= row_mdl_g_c_d_c_analysis.date_
                 order by t.date_ desc) t1
         where rownum <= 5;
      end if;

      -- ma10
      if v_date_count >= 10 then
        select avg(t1.macd_g_c_percent),
               avg(t1.macd_d_c_percent),
               avg(t1.close_price_g_c_ma5_percent),
               avg(t1.close_price_d_c_ma5_percent),
               avg(t1.hei_kin_ashi_u_d_percent),
               avg(t1.hei_kin_ashi_d_u_percent),
               avg(t1.kd_g_c_percent),
               avg(t1.kd_d_c_percent)
          into v_macd_g_c_percent_ma10,
               v_macd_d_c_percent_ma10,
               v_c_p_g_c_ma5_percent_ma10,
               v_c_p_d_c_ma5_percent_ma10,
               v_h_k_a_u_d_percent_ma10,
               v_h_k_a_d_u_percent_ma10,
               v_kd_g_c_percent_ma10,
               v_kd_d_c_percent_ma10
          from (select *
                  from mdl_g_c_d_c_analysis t
                 where t.date_ <= row_mdl_g_c_d_c_analysis.date_
                 order by t.date_ desc) t1
         where rownum <= 10;
      end if;

      -- ma20
      if v_date_count >= 20 then
        select avg(t1.macd_g_c_percent),
               avg(t1.macd_d_c_percent),
               avg(t1.close_price_g_c_ma5_percent),
               avg(t1.close_price_d_c_ma5_percent),
               avg(t1.hei_kin_ashi_u_d_percent),
               avg(t1.hei_kin_ashi_d_u_percent),
               avg(t1.kd_g_c_percent),
               avg(t1.kd_d_c_percent)
          into v_macd_g_c_percent_ma20,
               v_macd_d_c_percent_ma20,
               v_c_p_g_c_ma5_percent_ma20,
               v_c_p_d_c_ma5_percent_ma20,
               v_h_k_a_u_d_percent_ma20,
               v_h_k_a_d_u_percent_ma20,
               v_kd_g_c_percent_ma20,
               v_kd_d_c_percent_ma20
          from (select *
                  from mdl_g_c_d_c_analysis t
                 where t.date_ <= row_mdl_g_c_d_c_analysis.date_
                 order by t.date_ desc) t1
         where rownum <= 20;
      end if;

      -- 更新记录
      -- 至少有5条记录
      if v_date_count >= 5 then
        update mdl_g_c_d_c_analysis t
           set t.macd_g_c_percent_ma5          = v_macd_g_c_percent_ma5,
               t.macd_d_c_percent_ma5          = v_macd_d_c_percent_ma5,
               t.c_p_g_c_ma5_percent_ma5       = v_c_p_g_c_ma5_percent_ma5,
               t.c_p_d_c_ma5_percent_ma5       = v_c_p_d_c_ma5_percent_ma5,
               t.hei_kin_ashi_u_d_percent_ma5  = v_h_k_a_u_d_percent_ma5,
               t.hei_kin_ashi_d_u_percent_ma5  = v_h_k_a_d_u_percent_ma5,
               t.kd_g_c_percent_ma5            = v_kd_g_c_percent_ma5,
               t.kd_d_c_percent_ma5            = v_kd_d_c_percent_ma5,
               t.macd_g_c_percent_ma10         = v_macd_g_c_percent_ma10,
               t.macd_d_c_percent_ma10         = v_macd_d_c_percent_ma10,
               t.c_p_g_c_ma5_percent_ma10      = v_c_p_g_c_ma5_percent_ma10,
               t.c_p_d_c_ma5_percent_ma10      = v_c_p_d_c_ma5_percent_ma10,
               t.hei_kin_ashi_u_d_percent_ma10 = v_h_k_a_u_d_percent_ma10,
               t.hei_kin_ashi_d_u_percent_ma10 = v_h_k_a_d_u_percent_ma10,
               t.kd_g_c_percent_ma10           = v_kd_g_c_percent_ma10,
               t.kd_d_c_percent_ma10           = v_kd_d_c_percent_ma10,
               t.macd_g_c_percent_ma20         = v_macd_g_c_percent_ma20,
               t.macd_d_c_percent_ma20         = v_macd_d_c_percent_ma20,
               t.c_p_g_c_ma5_percent_ma20      = v_c_p_g_c_ma5_percent_ma20,
               t.c_p_d_c_ma5_percent_ma20      = v_c_p_d_c_ma5_percent_ma20,
               t.hei_kin_ashi_u_d_percent_ma20 = v_h_k_a_u_d_percent_ma20,
               t.hei_kin_ashi_d_u_percent_ma20 = v_h_k_a_d_u_percent_ma20,
               t.kd_g_c_percent_ma20           = v_kd_g_c_percent_ma20,
               t.kd_d_c_percent_ma20           = v_kd_d_c_percent_ma20
         where t.id_ = row_mdl_g_c_d_c_analysis.id_;
      end if;

      v_date_count := v_date_count - 1;

    end loop;
    close cur_analysis_desc;
    commit;
  end WRITE_MDL_G_C_D_C_A_PERCENT_MA;

  /*----------------- 计算mdl_g_c_d_c_analysis表中的所有成功率的均值 ----------------------*/
  procedure WRITE_MDL_G_C_D_C_A_S_R_MA is
    -- 日期数量
    v_date_count number;
    -- 最大日起
    v_max_date date;
    -- 最小日期
    v_min_date date;
    -- 成功率均价
    v_macd_g_c_s_r_ma5     NUMBER;
    v_macd_g_c_s_r_ma10    NUMBER;
    v_macd_g_c_s_r_ma20    NUMBER;
    v_macd_d_c_s_r_ma5     NUMBER;
    v_macd_d_c_s_r_ma10    NUMBER;
    v_macd_d_c_s_r_ma20    NUMBER;
    v_c_p_g_c_ma5_s_r_ma5  NUMBER;
    v_c_p_g_c_ma5_s_r_ma10 NUMBER;
    v_c_p_g_c_ma5_s_r_ma20 NUMBER;
    v_c_p_d_c_ma5_s_r_ma5  NUMBER;
    v_c_p_d_c_ma5_s_r_ma10 NUMBER;
    v_c_p_d_c_ma5_s_r_ma20 NUMBER;
    v_h_k_a_u_d_s_r_ma5    NUMBER;
    v_h_k_a_u_d_s_r_ma10   NUMBER;
    v_h_k_a_u_d_s_r_ma20   NUMBER;
    v_h_k_a_d_u_s_r_ma5    NUMBER;
    v_h_k_a_d_u_s_r_ma10   NUMBER;
    v_h_k_a_d_u_s_r_ma20   NUMBER;
    v_kd_g_c_s_r_ma5       NUMBER;
    v_kd_g_c_s_r_ma10      NUMBER;
    v_kd_g_c_s_r_ma20      NUMBER;
    v_kd_d_c_s_r_ma5       NUMBER;
    v_kd_d_c_s_r_ma10      NUMBER;
    v_kd_d_c_s_r_ma20      NUMBER;
    -- 记录的数量
    v_num_macd_g_c_s_r_ma5     NUMBER;
    v_num_macd_g_c_s_r_ma10    NUMBER;
    v_num_macd_g_c_s_r_ma20    NUMBER;
    v_num_macd_d_c_s_r_ma5     NUMBER;
    v_num_macd_d_c_s_r_ma10    NUMBER;
    v_num_macd_d_c_s_r_ma20    NUMBER;
    v_num_c_p_g_c_ma5_s_r_ma5  NUMBER;
    v_num_c_p_g_c_ma5_s_r_ma10 NUMBER;
    v_num_c_p_g_c_ma5_s_r_ma20 NUMBER;
    v_num_c_p_d_c_ma5_s_r_ma5  NUMBER;
    v_num_c_p_d_c_ma5_s_r_ma10 NUMBER;
    v_num_c_p_d_c_ma5_s_r_ma20 NUMBER;
    v_num_h_k_a_u_d_s_r_ma5    NUMBER;
    v_num_h_k_a_u_d_s_r_ma10   NUMBER;
    v_num_h_k_a_u_d_s_r_ma20   NUMBER;
    v_num_h_k_a_d_u_s_r_ma5    NUMBER;
    v_num_h_k_a_d_u_s_r_ma10   NUMBER;
    v_num_h_k_a_d_u_s_r_ma20   NUMBER;
    v_num_kd_g_c_s_r_ma5       NUMBER;
    v_num_kd_g_c_s_r_ma10      NUMBER;
    v_num_kd_g_c_s_r_ma20      NUMBER;
    v_num_kd_d_c_s_r_ma5       NUMBER;
    v_num_kd_d_c_s_r_ma10      NUMBER;
    v_num_kd_d_c_s_r_ma20      NUMBER;
    -- mdl_g_c_d_c_analysis表的所有日期，按降序排列
    cursor cur_analysis_desc is
      select * from mdl_g_c_d_c_analysis t order by t.date_ desc;
    -- mdl_g_c_d_c_analysis表类型的变量
    row_mdl_g_c_d_c_analysis mdl_g_c_d_c_analysis%rowtype;
  begin
    -- 计算日期的数量
    select count(distinct t.date_)
      into v_date_count
      from mdl_g_c_d_c_analysis t;

    open cur_analysis_desc;
    loop
      fetch cur_analysis_desc
        into row_mdl_g_c_d_c_analysis;
      exit when cur_analysis_desc%notfound;

      -- 重置
      /*v_macd_g_c_s_r_ma5     := null;
      v_macd_g_c_s_r_ma10    := null;
      v_macd_g_c_s_r_ma20    := null;
      v_macd_d_c_s_r_ma5     := null;
      v_macd_d_c_s_r_ma10    := null;
      v_macd_d_c_s_r_ma20    := null;
      v_c_p_g_c_ma5_s_r_ma5  := null;
      v_c_p_g_c_ma5_s_r_ma10 := null;
      v_c_p_g_c_ma5_s_r_ma20 := null;
      v_c_p_d_c_ma5_s_r_ma5  := null;
      v_c_p_d_c_ma5_s_r_ma10 := null;
      v_c_p_d_c_ma5_s_r_ma20 := null;
      v_h_k_a_u_d_s_r_ma5    := null;
      v_h_k_a_u_d_s_r_ma10   := null;
      v_h_k_a_u_d_s_r_ma20   := null;
      v_h_k_a_d_u_s_r_ma5    := null;
      v_h_k_a_d_u_s_r_ma10   := null;
      v_h_k_a_d_u_s_r_ma20   := null;
      v_kd_g_c_s_r_ma5       := null;
      v_kd_g_c_s_r_ma10      := null;
      v_kd_g_c_s_r_ma20      := null;
      v_kd_d_c_s_r_ma5       := null;
      v_kd_d_c_s_r_ma10      := null;
      v_kd_d_c_s_r_ma20      := null;*/

      -- 如果抛出异常
      -- ma5
      if v_date_count >= 5 then
        -- 求最大日期和最小日期
        select max(t1.date_), min(t1.date_)
          into v_max_date, v_min_date
          from (select *
                  from mdl_g_c_d_c_analysis t
                 where t.date_ <= row_mdl_g_c_d_c_analysis.date_
                 order by t.date_ desc) t1
         where rownum <= 5;
        -- 计算5日均值
        select count(*)
          into v_num_macd_g_c_s_r_ma5
          from mdl_macd_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_macd_g_c_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_macd_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_macd_g_c_s_r_ma5 * 100
            into v_macd_g_c_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_macd_d_c_s_r_ma5
          from mdl_macd_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_macd_d_c_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_macd_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_macd_d_c_s_r_ma5 * 100
            into v_macd_d_c_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_c_p_g_c_ma5_s_r_ma5
          from mdl_close_price_ma5_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_c_p_g_c_ma5_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_close_price_ma5_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_c_p_g_c_ma5_s_r_ma5 * 100
            into v_c_p_g_c_ma5_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_c_p_d_c_ma5_s_r_ma5
          from mdl_close_price_ma5_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_c_p_d_c_ma5_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_close_price_ma5_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_c_p_d_c_ma5_s_r_ma5 * 100
            into v_c_p_d_c_ma5_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_h_k_a_u_d_s_r_ma5
          from mdl_hei_kin_ashi_up_down t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_h_k_a_u_d_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_hei_kin_ashi_up_down t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_h_k_a_u_d_s_r_ma5 * 100
            into v_h_k_a_u_d_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_h_k_a_d_u_s_r_ma5
          from mdl_hei_kin_ashi_down_up t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_h_k_a_d_u_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_hei_kin_ashi_down_up t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_h_k_a_d_u_s_r_ma5 * 100
            into v_h_k_a_d_u_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_kd_g_c_s_r_ma5
          from mdl_kd_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_kd_g_c_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_kd_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_kd_g_c_s_r_ma5 * 100
            into v_kd_g_c_s_r_ma5
            from dual;
        end if;

        select count(*)
          into v_num_kd_d_c_s_r_ma5
          from mdl_kd_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_kd_d_c_s_r_ma5 > 0 then
          select (select count(*)
                    from mdl_kd_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_kd_d_c_s_r_ma5 * 100
            into v_kd_d_c_s_r_ma5
            from dual;
        end if;
      end if;

      -- ma10
      if v_date_count >= 10 then
        -- 求最大日期和最小日期
        select max(t1.date_), min(t1.date_)
          into v_max_date, v_min_date
          from (select *
                  from mdl_g_c_d_c_analysis t
                 where t.date_ <= row_mdl_g_c_d_c_analysis.date_
                 order by t.date_ desc) t1
         where rownum <= 10;
        -- 计算10日均值
        select count(*)
          into v_num_macd_g_c_s_r_ma10
          from mdl_macd_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_macd_g_c_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_macd_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_macd_g_c_s_r_ma10 * 100
            into v_macd_g_c_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_macd_d_c_s_r_ma10
          from mdl_macd_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_macd_d_c_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_macd_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_macd_d_c_s_r_ma10 * 100
            into v_macd_d_c_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_c_p_g_c_ma5_s_r_ma10
          from mdl_close_price_ma5_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_c_p_g_c_ma5_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_close_price_ma5_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_c_p_g_c_ma5_s_r_ma10 * 100
            into v_c_p_g_c_ma5_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_c_p_d_c_ma5_s_r_ma10
          from mdl_close_price_ma5_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_c_p_d_c_ma5_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_close_price_ma5_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_c_p_d_c_ma5_s_r_ma10 * 100
            into v_c_p_d_c_ma5_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_h_k_a_u_d_s_r_ma10
          from mdl_hei_kin_ashi_up_down t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_h_k_a_u_d_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_hei_kin_ashi_up_down t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_h_k_a_u_d_s_r_ma10 * 100
            into v_h_k_a_u_d_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_h_k_a_d_u_s_r_ma10
          from mdl_hei_kin_ashi_down_up t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_h_k_a_d_u_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_hei_kin_ashi_down_up t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_h_k_a_d_u_s_r_ma10 * 100
            into v_h_k_a_d_u_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_kd_g_c_s_r_ma10
          from mdl_kd_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_kd_g_c_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_kd_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_kd_g_c_s_r_ma10 * 100
            into v_kd_g_c_s_r_ma10
            from dual;
        end if;

        select count(*)
          into v_num_kd_d_c_s_r_ma10
          from mdl_kd_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_kd_d_c_s_r_ma10 > 0 then
          select (select count(*)
                    from mdl_kd_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_kd_d_c_s_r_ma10 * 100
            into v_kd_d_c_s_r_ma10
            from dual;
        end if;
      end if;

      -- ma20
      if v_date_count >= 20 then
        -- 求最大日期和最小日期
        select max(t1.date_), min(t1.date_)
          into v_max_date, v_min_date
          from (select *
                  from mdl_g_c_d_c_analysis t
                 where t.date_ <= row_mdl_g_c_d_c_analysis.date_
                 order by t.date_ desc) t1
         where rownum <= 20;
        -- 计算20日均值
        select count(*)
          into v_num_macd_g_c_s_r_ma20
          from mdl_macd_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_macd_g_c_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_macd_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_macd_g_c_s_r_ma20 * 100
            into v_macd_g_c_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_macd_d_c_s_r_ma20
          from mdl_macd_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_macd_d_c_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_macd_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_macd_d_c_s_r_ma20 * 100
            into v_macd_d_c_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_c_p_g_c_ma5_s_r_ma20
          from mdl_close_price_ma5_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_c_p_g_c_ma5_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_close_price_ma5_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_c_p_g_c_ma5_s_r_ma20 * 100
            into v_c_p_g_c_ma5_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_c_p_d_c_ma5_s_r_ma20
          from mdl_close_price_ma5_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_c_p_d_c_ma5_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_close_price_ma5_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_c_p_d_c_ma5_s_r_ma20 * 100
            into v_c_p_d_c_ma5_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_h_k_a_u_d_s_r_ma20
          from mdl_hei_kin_ashi_up_down t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_h_k_a_u_d_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_hei_kin_ashi_up_down t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_h_k_a_u_d_s_r_ma20 * 100
            into v_h_k_a_u_d_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_h_k_a_d_u_s_r_ma20
          from mdl_hei_kin_ashi_down_up t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_h_k_a_d_u_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_hei_kin_ashi_down_up t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_h_k_a_d_u_s_r_ma20 * 100
            into v_h_k_a_d_u_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_kd_g_c_s_r_ma20
          from mdl_kd_gold_cross t
         where t.buy_date between v_min_date and v_max_date;
        if v_num_kd_g_c_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_kd_gold_cross t
                   where t.buy_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_kd_g_c_s_r_ma20 * 100
            into v_kd_g_c_s_r_ma20
            from dual;
        end if;

        select count(*)
          into v_num_kd_d_c_s_r_ma20
          from mdl_kd_dead_cross t
         where t.sell_date between v_min_date and v_max_date;
        if v_num_kd_d_c_s_r_ma20 > 0 then
          select (select count(*)
                    from mdl_kd_dead_cross t
                   where t.sell_date between v_min_date and v_max_date
                     and t.profit_loss > 0) / v_num_kd_d_c_s_r_ma20 * 100
            into v_kd_d_c_s_r_ma20
            from dual;
        end if;
      end if;

      -- 更新记录
      -- 至少有5条记录
      if v_date_count >= 5 then
        update mdl_g_c_d_c_analysis t
           set t.macd_g_c_success_rate_ma5     = v_macd_g_c_s_r_ma5,
               t.macd_d_c_success_rate_ma5     = v_macd_d_c_s_r_ma5,
               t.c_p_g_c_ma5_success_rate_ma5  = v_c_p_g_c_ma5_s_r_ma5,
               t.c_p_d_c_ma5_success_rate_ma5  = v_c_p_d_c_ma5_s_r_ma5,
               t.hei_kin_ashi_u_d_s_r_ma5      = v_h_k_a_u_d_s_r_ma5,
               t.hei_kin_ashi_d_u_s_r_ma5      = v_h_k_a_d_u_s_r_ma5,
               t.kd_g_c_success_rate_ma5       = v_kd_g_c_s_r_ma5,
               t.kd_d_c_success_rate_ma5       = v_kd_d_c_s_r_ma5,
               t.macd_g_c_success_rate_ma10    = v_macd_g_c_s_r_ma10,
               t.macd_d_c_success_rate_ma10    = v_macd_d_c_s_r_ma10,
               t.c_p_g_c_ma5_success_rate_ma10 = v_c_p_g_c_ma5_s_r_ma10,
               t.c_p_d_c_ma5_success_rate_ma10 = v_c_p_d_c_ma5_s_r_ma10,
               t.hei_kin_ashi_u_d_s_r_ma10     = v_h_k_a_u_d_s_r_ma10,
               t.hei_kin_ashi_d_u_s_r_ma10     = v_h_k_a_d_u_s_r_ma10,
               t.kd_g_c_success_rate_ma10      = v_kd_g_c_s_r_ma10,
               t.kd_d_c_success_rate_ma10      = v_kd_d_c_s_r_ma10,
               t.macd_g_c_success_rate_ma20    = v_macd_g_c_s_r_ma20,
               t.macd_d_c_success_rate_ma20    = v_macd_d_c_s_r_ma20,
               t.c_p_g_c_ma5_success_rate_ma20 = v_c_p_g_c_ma5_s_r_ma20,
               t.c_p_d_c_ma5_success_rate_ma20 = v_c_p_d_c_ma5_s_r_ma20,
               t.hei_kin_ashi_u_d_s_r_ma20     = v_h_k_a_u_d_s_r_ma20,
               t.hei_kin_ashi_d_u_s_r_ma20     = v_h_k_a_d_u_s_r_ma20,
               t.kd_g_c_success_rate_ma20      = v_kd_g_c_s_r_ma20,
               t.kd_d_c_success_rate_ma20      = v_kd_d_c_s_r_ma20
         where t.id_ = row_mdl_g_c_d_c_analysis.id_;
      end if;

      v_date_count := v_date_count - 1;

    end loop;
    close cur_analysis_desc;
    commit;
  end WRITE_MDL_G_C_D_C_A_S_R_MA;

  /*---------------------- 查询各种算法状态已经持续了多少天 ---------------------------*/
  /* 1表示MACD金叉，2表示MACD死叉，3表示收盘价金叉五日均线，4表示收盘价死叉五日均线
  5表示hei_kin_ashi处于上涨阶段，6表示hei_kin_ashi处于下跌阶段
  7表示KD金叉，8表示KD死叉
  返回值为0表示现在并不处于这种状态 */
  -- 全名：find_gold_cross_dead_cross_continue_date_count
  procedure find_g_c_d_c_c_date_count(p_type in number,
                                      p_date in varchar2,
                                      p_num  out number) is
    -- mdl_g_c_d_c_analysis表的行
    row_mdl_g_c_d_c_analysis mdl_g_c_d_c_analysis%rowtype;
    -- 某个日期之后的记录，降序排列
    cursor cur_by_date_order_by_desc is
      select *
        from mdl_g_c_d_c_analysis t
       where t.date_ <= to_date(p_date, 'yyyy-mm-dd')
       order by t.date_ desc;
  begin
    p_num := 0;
    for row_mdl_g_c_d_c_analysis in cur_by_date_order_by_desc loop
      -- MACD金叉
      if p_type = 1 then
        if row_mdl_g_c_d_c_analysis.macd_g_c_percent_ma5 >
           row_mdl_g_c_d_c_analysis.macd_g_c_percent_ma10 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- MACD死叉
      if p_type = 2 then
        if row_mdl_g_c_d_c_analysis.macd_d_c_percent_ma5 <
           row_mdl_g_c_d_c_analysis.macd_d_c_percent_ma10 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- 收盘价金叉五日均线
      if p_type = 3 then
        if row_mdl_g_c_d_c_analysis.c_p_g_c_ma5_percent_ma5 >
           row_mdl_g_c_d_c_analysis.c_p_g_c_ma5_percent_ma20 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- 收盘价死叉五日均线
      if p_type = 4 then
        if row_mdl_g_c_d_c_analysis.c_p_d_c_ma5_percent_ma5 <
           row_mdl_g_c_d_c_analysis.c_p_d_c_ma5_percent_ma20 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- hei_kin_ashi处于上涨阶段
      if p_type = 5 then
        if row_mdl_g_c_d_c_analysis.hei_kin_ashi_u_d_percent_ma5 >
           row_mdl_g_c_d_c_analysis.hei_kin_ashi_u_d_percent_ma10 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- hei_kin_ashi处于下跌阶段
      if p_type = 6 then
        if row_mdl_g_c_d_c_analysis.hei_kin_ashi_d_u_percent_ma5 <
           row_mdl_g_c_d_c_analysis.hei_kin_ashi_d_u_percent_ma10 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- KD金叉
      if p_type = 7 then
        if row_mdl_g_c_d_c_analysis.kd_g_c_percent_ma5 >
           row_mdl_g_c_d_c_analysis.kd_g_c_percent_ma10 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
      -- KD死叉
      if p_type = 8 then
        if row_mdl_g_c_d_c_analysis.kd_d_c_percent_ma5 <
           row_mdl_g_c_d_c_analysis.kd_d_c_percent_ma10 then
          p_num := p_num + 1;
        else
          return;
        end if;
      end if;
    end loop;
  end find_g_c_d_c_c_date_count;

  /*-------------------- 根据日期，计算表MDL_G_C_D_C_ANALYSIS的数据 -----------------------*/
  procedure write_g_c_d_c_a_by_date(p_begin_date in varchar2,
                                    p_end_date   in varchar2) is
    -- 最大日起
    v_max_date date;
    -- 最小日期
    v_min_date date;
    -- 所有金叉死叉的百分比
    v_macd_g_c_percent            number;
    v_macd_d_c_percent            number;
    v_close_price_g_c_ma5_percent number;
    v_close_price_d_c_ma5_percent number;
    v_h_k_a_up_down_percent       number;
    v_h_k_a_down_up_percent       number;
    v_kd_g_c_percent              number;
    v_kd_d_c_percent              number;
    -- 所有金叉死叉的百分比的5日均值、10日均值、20日均值
    v_macd_g_c_percent_ma5       number;
    v_macd_d_c_percent_ma5       number;
    v_c_p_g_c_ma5_percent_ma5    number;
    v_c_p_d_c_ma5_percent_ma5    number;
    v_h_k_a_up_down_percent_ma5  number;
    v_h_k_a_down_up_percent_ma5  number;
    v_kd_g_c_percent_ma5         number;
    v_kd_d_c_percent_ma5         number;
    v_macd_g_c_percent_ma10      number;
    v_macd_d_c_percent_ma10      number;
    v_c_p_g_c_ma5_percent_ma10   number;
    v_c_p_d_c_ma5_percent_ma10   number;
    v_h_k_a_up_down_percent_ma10 number;
    v_h_k_a_down_up_percent_ma10 number;
    v_kd_g_c_percent_ma10        number;
    v_kd_d_c_percent_ma10        number;
    v_macd_g_c_percent_ma20      number;
    v_macd_d_c_percent_ma20      number;
    v_c_p_g_c_ma5_percent_ma20   number;
    v_c_p_d_c_ma5_percent_ma20   number;
    v_h_k_a_up_down_percent_ma20 number;
    v_h_k_a_down_up_percent_ma20 number;
    v_kd_g_c_percent_ma20        number;
    v_kd_d_c_percent_ma20        number;
    -- 所有成功率的5日均值、10日均值、20日均值
    v_macd_g_c_s_r_ma5     NUMBER;
    v_macd_g_c_s_r_ma10    NUMBER;
    v_macd_g_c_s_r_ma20    NUMBER;
    v_macd_d_c_s_r_ma5     NUMBER;
    v_macd_d_c_s_r_ma10    NUMBER;
    v_macd_d_c_s_r_ma20    NUMBER;
    v_c_p_g_c_ma5_s_r_ma5  NUMBER;
    v_c_p_g_c_ma5_s_r_ma10 NUMBER;
    v_c_p_g_c_ma5_s_r_ma20 NUMBER;
    v_c_p_d_c_ma5_s_r_ma5  NUMBER;
    v_c_p_d_c_ma5_s_r_ma10 NUMBER;
    v_c_p_d_c_ma5_s_r_ma20 NUMBER;
    v_h_k_a_u_d_s_r_ma5    NUMBER;
    v_h_k_a_u_d_s_r_ma10   NUMBER;
    v_h_k_a_u_d_s_r_ma20   NUMBER;
    v_h_k_a_d_u_s_r_ma5    NUMBER;
    v_h_k_a_d_u_s_r_ma10   NUMBER;
    v_h_k_a_d_u_s_r_ma20   NUMBER;
    v_kd_g_c_s_r_ma5       NUMBER;
    v_kd_g_c_s_r_ma10      NUMBER;
    v_kd_g_c_s_r_ma20      NUMBER;
    v_kd_d_c_s_r_ma5       NUMBER;
    v_kd_d_c_s_r_ma10      NUMBER;
    v_kd_d_c_s_r_ma20      NUMBER;
    -- stock_transaction_data表类型
    row_stock_transaction_data stock_transaction_data%rowtype;
    -- 所有日期
    cursor cur_date is
      select distinct t.date_
        from stock_transaction_data_all t
       where t.date_ >= to_date(p_begin_date, 'yyyy-mm-dd')
         and t.date_ <= to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ asc;
  begin
    for row_stock_transaction_data in cur_date loop

      -- 当日的百分比
      select (select count(*)
                from stock_transaction_data_all t1
               where t1.dif > t1.dea
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.dif < t1.dea
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.close_price > t1.ma5
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.close_price < t1.ma5
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.ha_close_price > t1.ha_open_price
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.ha_close_price < t1.ha_open_price
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.k > t1.d
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100,
             (select count(*)
                from stock_transaction_data_all t1
               where t1.k < t1.d
                 and t1.date_ = row_stock_transaction_data.date_) /
             (select count(*)
                from stock_transaction_data_all t1
               where t1.date_ = row_stock_transaction_data.date_) * 100
        into v_macd_g_c_percent,
             v_macd_d_c_percent,
             v_close_price_g_c_ma5_percent,
             v_close_price_d_c_ma5_percent,
             v_h_k_a_up_down_percent,
             v_h_k_a_down_up_percent,
             v_kd_g_c_percent,
             v_kd_d_c_percent
        from dual;

      -- 百分比的5日均值、10日均值、20日均值
      select (select avg(t.macd_g_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.macd_g_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.macd_g_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.macd_d_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.macd_d_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.macd_d_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.close_price_g_c_ma5_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.close_price_g_c_ma5_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.close_price_g_c_ma5_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.close_price_d_c_ma5_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.close_price_d_c_ma5_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.close_price_d_c_ma5_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.hei_kin_ashi_u_d_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.hei_kin_ashi_u_d_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.hei_kin_ashi_u_d_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.hei_kin_ashi_d_u_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.hei_kin_ashi_d_u_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.hei_kin_ashi_d_u_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.kd_g_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.kd_g_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.kd_g_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20),
             (select avg(t.kd_d_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 5),
             (select avg(t.kd_d_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 10),
             (select avg(t.kd_d_c_percent)
                from (select *
                        from mdl_g_c_d_c_analysis t1
                       where t1.date_ < row_stock_transaction_data.date_
                       order by t1.date_ desc) t
               where rownum <= 20)
        into v_macd_g_c_percent_ma5,
             v_macd_g_c_percent_ma10,
             v_macd_g_c_percent_ma20,
             v_macd_d_c_percent_ma5,
             v_macd_d_c_percent_ma10,
             v_macd_d_c_percent_ma20,
             v_c_p_g_c_ma5_percent_ma5,
             v_c_p_g_c_ma5_percent_ma10,
             v_c_p_g_c_ma5_percent_ma20,
             v_c_p_d_c_ma5_percent_ma5,
             v_c_p_d_c_ma5_percent_ma10,
             v_c_p_d_c_ma5_percent_ma20,
             v_h_k_a_up_down_percent_ma5,
             v_h_k_a_up_down_percent_ma10,
             v_h_k_a_up_down_percent_ma20,
             v_h_k_a_down_up_percent_ma5,
             v_h_k_a_down_up_percent_ma10,
             v_h_k_a_down_up_percent_ma20,
             v_kd_g_c_percent_ma5,
             v_kd_g_c_percent_ma10,
             v_kd_g_c_percent_ma20,
             v_kd_d_c_percent_ma5,
             v_kd_d_c_percent_ma10,
             v_kd_d_c_percent_ma20
        from dual;

      -- 成功率的五日均值
      -- ma5
      -- 求最大日期和最小日期
      select max(t1.date_), min(t1.date_)
        into v_max_date, v_min_date
        from (select *
                from stock_index t
               where t.date_ <= row_stock_transaction_data.date_
                 and t.code_ = '000001'
               order by t.date_ desc) t1
       where rownum <= 5;
      -- 计算5日均值
      select (select count(*)
                from mdl_macd_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_macd_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_macd_g_c_s_r_ma5
        from dual;

      select (select count(*)
                from mdl_macd_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_macd_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_macd_d_c_s_r_ma5
        from dual;

      select (select count(*)
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_c_p_g_c_ma5_s_r_ma5
        from dual;

      select (select count(*)
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_c_p_d_c_ma5_s_r_ma5
        from dual;

      begin
        select (select count(*)
                  from mdl_hei_kin_ashi_up_down t
                 where t.buy_date between v_min_date and v_max_date
                   and t.profit_loss > 0) /
               (select count(*)
                  from mdl_hei_kin_ashi_up_down t
                 where t.buy_date between v_min_date and v_max_date) * 100
          into v_h_k_a_u_d_s_r_ma5
          from dual;
      exception
        when others then
          v_h_k_a_u_d_s_r_ma5 := null;
      end;

      begin
        select (select count(*)
                  from mdl_hei_kin_ashi_down_up t
                 where t.sell_date between v_min_date and v_max_date
                   and t.profit_loss > 0) /
               (select count(*)
                  from mdl_hei_kin_ashi_down_up t
                 where t.sell_date between v_min_date and v_max_date) * 100
          into v_h_k_a_d_u_s_r_ma5
          from dual;
      exception
        when others then
          v_h_k_a_d_u_s_r_ma5 := null;
      end;

      select (select count(*)
                from mdl_kd_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_kd_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_kd_g_c_s_r_ma5
        from dual;

      select (select count(*)
                from mdl_kd_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_kd_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_kd_d_c_s_r_ma5
        from dual;

      -- ma10
      -- 求最大日期和最小日期
      select max(t1.date_), min(t1.date_)
        into v_max_date, v_min_date
        from (select *
                from stock_index t
               where t.date_ <= row_stock_transaction_data.date_
                 and t.code_ = '000001'
               order by t.date_ desc) t1
       where rownum <= 10;
      -- 计算10日均值
      select (select count(*)
                from mdl_macd_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_macd_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_macd_g_c_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_macd_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_macd_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_macd_d_c_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_c_p_g_c_ma5_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_c_p_d_c_ma5_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_h_k_a_u_d_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_h_k_a_d_u_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_kd_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_kd_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_kd_g_c_s_r_ma10
        from dual;

      select (select count(*)
                from mdl_kd_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_kd_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_kd_d_c_s_r_ma10
        from dual;

      -- ma20
      -- 求最大日期和最小日期
      select max(t1.date_), min(t1.date_)
        into v_max_date, v_min_date
        from (select *
                from stock_index t
               where t.date_ <= row_stock_transaction_data.date_
                 and t.code_ = '000001'
               order by t.date_ desc) t1
       where rownum <= 20;
      -- 计算20日均值
      select (select count(*)
                from mdl_macd_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_macd_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_macd_g_c_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_macd_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_macd_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_macd_d_c_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_close_price_ma5_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_c_p_g_c_ma5_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_close_price_ma5_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_c_p_d_c_ma5_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_hei_kin_ashi_up_down t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_h_k_a_u_d_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_hei_kin_ashi_down_up t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_h_k_a_d_u_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_kd_gold_cross t
               where t.buy_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_kd_gold_cross t
               where t.buy_date between v_min_date and v_max_date) * 100
        into v_kd_g_c_s_r_ma20
        from dual;

      select (select count(*)
                from mdl_kd_dead_cross t
               where t.sell_date between v_min_date and v_max_date
                 and t.profit_loss > 0) /
             (select count(*)
                from mdl_kd_dead_cross t
               where t.sell_date between v_min_date and v_max_date) * 100
        into v_kd_d_c_s_r_ma20
        from dual;

      -- 插入记录
      insert into mdl_g_c_d_c_analysis
        (date_,
         macd_g_c_percent,
         macd_d_c_percent,
         close_price_g_c_ma5_percent,
         close_price_d_c_ma5_percent,
         hei_kin_ashi_u_d_percent,
         hei_kin_ashi_d_u_percent,
         kd_g_c_percent,
         kd_d_c_percent,
         macd_g_c_percent_ma5,
         macd_g_c_percent_ma10,
         macd_g_c_percent_ma20,
         macd_d_c_percent_ma5,
         macd_d_c_percent_ma10,
         macd_d_c_percent_ma20,
         c_p_g_c_ma5_percent_ma5,
         c_p_g_c_ma5_percent_ma10,
         c_p_g_c_ma5_percent_ma20,
         c_p_d_c_ma5_percent_ma5,
         c_p_d_c_ma5_percent_ma10,
         c_p_d_c_ma5_percent_ma20,
         hei_kin_ashi_u_d_percent_ma5,
         hei_kin_ashi_u_d_percent_ma10,
         hei_kin_ashi_u_d_percent_ma20,
         hei_kin_ashi_d_u_percent_ma5,
         hei_kin_ashi_d_u_percent_ma10,
         hei_kin_ashi_d_u_percent_ma20,
         kd_g_c_percent_ma5,
         kd_g_c_percent_ma10,
         kd_g_c_percent_ma20,
         kd_d_c_percent_ma5,
         kd_d_c_percent_ma10,
         kd_d_c_percent_ma20,
         macd_g_c_success_rate_ma5,
         macd_g_c_success_rate_ma10,
         macd_g_c_success_rate_ma20,
         macd_d_c_success_rate_ma5,
         macd_d_c_success_rate_ma10,
         macd_d_c_success_rate_ma20,
         c_p_g_c_ma5_success_rate_ma5,
         c_p_g_c_ma5_success_rate_ma10,
         c_p_g_c_ma5_success_rate_ma20,
         c_p_d_c_ma5_success_rate_ma5,
         c_p_d_c_ma5_success_rate_ma10,
         c_p_d_c_ma5_success_rate_ma20,
         hei_kin_ashi_u_d_s_r_ma5,
         hei_kin_ashi_u_d_s_r_ma10,
         hei_kin_ashi_u_d_s_r_ma20,
         hei_kin_ashi_d_u_s_r_ma5,
         hei_kin_ashi_d_u_s_r_ma10,
         hei_kin_ashi_d_u_s_r_ma20,
         kd_g_c_success_rate_ma5,
         kd_g_c_success_rate_ma10,
         kd_g_c_success_rate_ma20,
         kd_d_c_success_rate_ma5,
         kd_d_c_success_rate_ma10,
         kd_d_c_success_rate_ma20)
      values
        (row_stock_transaction_data.date_,
         v_macd_g_c_percent,
         v_macd_d_c_percent,
         v_close_price_g_c_ma5_percent,
         v_close_price_d_c_ma5_percent,
         v_h_k_a_up_down_percent,
         v_h_k_a_down_up_percent,
         v_kd_g_c_percent,
         v_kd_d_c_percent,
         v_macd_g_c_percent_ma5,
         v_macd_g_c_percent_ma10,
         v_macd_g_c_percent_ma20,
         v_macd_d_c_percent_ma5,
         v_macd_d_c_percent_ma10,
         v_macd_d_c_percent_ma20,
         v_c_p_g_c_ma5_percent_ma5,
         v_c_p_g_c_ma5_percent_ma10,
         v_c_p_g_c_ma5_percent_ma20,
         v_c_p_d_c_ma5_percent_ma5,
         v_c_p_d_c_ma5_percent_ma10,
         v_c_p_d_c_ma5_percent_ma20,
         v_h_k_a_up_down_percent_ma5,
         v_h_k_a_up_down_percent_ma10,
         v_h_k_a_up_down_percent_ma20,
         v_h_k_a_down_up_percent_ma5,
         v_h_k_a_down_up_percent_ma10,
         v_h_k_a_down_up_percent_ma20,
         v_kd_g_c_percent_ma5,
         v_kd_g_c_percent_ma10,
         v_kd_g_c_percent_ma20,
         v_kd_d_c_percent_ma5,
         v_kd_d_c_percent_ma10,
         v_kd_d_c_percent_ma20,
         v_macd_g_c_s_r_ma5,
         v_macd_g_c_s_r_ma10,
         v_macd_g_c_s_r_ma20,
         v_macd_d_c_s_r_ma5,
         v_macd_d_c_s_r_ma10,
         v_macd_d_c_s_r_ma20,
         v_c_p_g_c_ma5_s_r_ma5,
         v_c_p_g_c_ma5_s_r_ma10,
         v_c_p_g_c_ma5_s_r_ma20,
         v_c_p_d_c_ma5_s_r_ma5,
         v_c_p_d_c_ma5_s_r_ma10,
         v_c_p_d_c_ma5_s_r_ma20,
         v_h_k_a_u_d_s_r_ma5,
         v_h_k_a_u_d_s_r_ma10,
         v_h_k_a_u_d_s_r_ma20,
         v_h_k_a_d_u_s_r_ma5,
         v_h_k_a_d_u_s_r_ma10,
         v_h_k_a_d_u_s_r_ma20,
         v_kd_g_c_s_r_ma5,
         v_kd_g_c_s_r_ma10,
         v_kd_g_c_s_r_ma20,
         v_kd_d_c_s_r_ma5,
         v_kd_d_c_s_r_ma10,
         v_kd_d_c_s_r_ma20);
      commit;
    end loop;
  end write_g_c_d_c_a_by_date;

end PKG_MODEL_G_C_D_C_ANALYSIS;
/

