???create or replace function scott.FNC_EXTRACT_CONTRACT_CODE(p_code in varchar2)
  return varchar2 is
  -- 方法返回值
  v_result varchar2(50);
begin
  select upper(REGEXP_SUBSTR(substr(p_code, INSTR(p_code, '.') + 1),
                             '[A-Za-z]+'))
    into v_result
    from dual;
  return(v_result);
end FNC_EXTRACT_CONTRACT_CODE;
/

