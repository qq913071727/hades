???create or replace type scott.T_TOP_STOCK_COUNT as object
(
  v_code  varchar2(100),
  v_count number,

  member function get_code return varchar2,
  member function get_count return number,

  member procedure set_code(p_code varchar2),
  member procedure set_count(p_count number)
)
/

