???create or replace type body scott.T_STOCK_RESULT as
       member function get_code return varchar2 is
       begin
            return code_;
       end get_code;

       member procedure set_code(p_stock_code varchar2) is
       begin
            code_:=p_stock_code;
       end set_code;

       member function get_date return date is
       begin
            return date_;
       end get_date;

       member procedure set_date(p_stock_date date) is
       begin
            date_:=p_stock_date;
       end set_date;
end;
/

