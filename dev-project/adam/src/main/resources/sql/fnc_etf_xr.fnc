???create or replace function scott.FNC_ETF_XR(p_etf_code in varchar2,
                                        p_begin_date in varchar2,
                                        p_end_date   in varchar2)
  return number is
  /* 通常在某只股票除权之后，股票价格会大幅度下降，此时很多指标不再准确。由于数据库只有不复权的数据，
  * 因此在遇到一段时间内出现除权现象的股票时应当放弃。
  * 这个函数的作用是判断某一段时间内，某只股票是否出现了除权的情况。
  * 2020年8月24日之前，所有股票以下跌幅度超过11%为标准；2020年8月24日之后，创业板以22%为标准，其他板块
  * 的股票仍然是11%。
  * 如果出现除权，则返回1；否则返回-1 */

  -- 从这一天开始，创业板的涨跌停限制为20%
  v_judgement_date date := to_date('2020-08-24','yyyy-mm-dd');
  -- 方法返回值
  v_result number := -1;
  -- 表示某一只etf在某一段时间内升序排列
  cursor cur_etf_by_date_asc is
    select *
      from etf_transaction_data t
     where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
           to_date(p_end_date, 'yyyy-mm-dd')
       and t.code_ = p_etf_code
     order by t.date_ asc;
  -- 前一天etf的收盘价
  v_last_close_price number := -1;
  -- 表示某一段时间内某只etf最大的下跌幅度
  v_down_percentage number;
begin
  -- 判断某一段时间内，某只etf是否有除权
  for i in cur_etf_by_date_asc loop

    -- 如果是第一天，则跳过
    if v_last_close_price = -1 then
      v_last_close_price := i.close_price;
      continue;
    end if;

    v_down_percentage := (i.close_price - v_last_close_price) /
                         v_last_close_price * 100;

    /*select min(t.change_range_ex_right) into v_down_percentage from stock_transaction_data_all t
    where t.code_ = p_stock_code
          and t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and to_date(p_end_date, 'yyyy-mm-dd');*/
    -- 在2020-08-24之前，并且当下跌幅度大于-11%，就表示有可能是除权
    if to_date(p_end_date,'yyyy-mm-dd') < v_judgement_date and v_down_percentage < -11 then
      v_result := 1;
      return(v_result);
    end if;
    -- 在2020-08-24之后，如果etf代码以300开头，并且下跌幅度大于22%，则表示有可能是除权
    if to_date(p_end_date,'yyyy-mm-dd') >= v_judgement_date and substr(p_etf_code, 1, 3) = '300'
       and v_down_percentage < -22 then
       v_result := 1;
       return(v_result);
    end if;
    -- 在2020-08-24之后，如果etf代码不是300开头，并且下跌幅度大于11%，则表示有可能是除权
    if to_date(p_end_date,'yyyy-mm-dd') >= v_judgement_date and substr(p_etf_code, 1, 3) != '300'
       and v_down_percentage < -11 then
       v_result := 1;
       return(v_result);
    end if;

    v_last_close_price := i.close_price;
  end loop;

  return(v_result);
end FNC_ETF_XR;
/

