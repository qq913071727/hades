???create or replace type scott.T_SUB_NEW_STOCK as object
(
       sub_new_stock_date date,
       sub_new_stock_avg_close number,

       member function getSubNewStockDate return date,
       member function getSubNewStockAvgClose return number,
       member procedure setSubNewStockDate(paramSubNewStockDate date),
       member procedure setSubNewStockAvgClose(paramSubNewStockAvgClose number)
)not final
/

