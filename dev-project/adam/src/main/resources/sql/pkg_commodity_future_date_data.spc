???create or replace package scott.PKG_COMMODITY_FUTURE_DATE_DATA is

  -- 计算日下级别全部last_close_price、rising_and_falling_amount和price_change字段
  PROCEDURE WRITE_BASIC_DATA;
  -- 计算某一天所有期货的last_close_price、rising_and_falling_amount和price_change字段
  PROCEDURE WRITE_BASIC_DATA_BY_DATE(p_date in varchar2);

  -- 计算统一相对价格指数
  PROCEDURE CAL_UNITE_RELATIVE_PRICE_INDEX;
  -- 计算某一天的统一相对价格指数
  PROCEDURE CAL_U_R_PRICE_INDEX_BY_DATE(p_date in varchar2);

  -- 计算简单移动平均线
  PROCEDURE CALCULATE_FIVE;
  PROCEDURE CALCULATE_TEN;
  PROCEDURE CALCULATE_TWENTY;
  PROCEDURE CALCULATE_SIXTY;
  PROCEDURE CALCULATE_ONEHUNDREDTWENTY;
  PROCEDURE CALCULATE_TWOHUNDREDFIFTY;
  -- 计算某一天所有期货的简单移动平均线
  PROCEDURE WRITE_MOVING_AVERAGE_BY_DATE(p_date in varchar2);

  -- 计算MACD
  procedure WRITE_MACD_INIT;
  procedure WRITE_MACD_EMA;
  procedure WRITE_MACD_DIF;
  procedure WRITE_MACD_DEA;
  procedure WRITE_MACD;
  -- 计算每日的MACD
  procedure WRITE_MACD_EMA_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_DIF_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_DEA_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_BY_DATE(p_date in varchar2);

  -- 计算KD
  procedure WRITE_KD_INIT;
  procedure WRITE_KD_RSV;
  procedure WRITE_KD_K;
  procedure WRITE_KD_D;
  -- 计算每日的kd
  procedure WRITE_KD_BY_DATE_RSV(p_date varchar2);
  procedure WRITE_KD_BY_DATE_K(p_date varchar2);
  procedure WRITE_KD_BY_DATE_D(p_date varchar2);

  -- 计算所有期货的ha
  procedure WRITE_HA;
  -- 计算某一日所有期货的ha
  procedure WRITE_HA_BY_DATE(p_date varchar2);

  -- 计算commodity_future_date_data表中的布林带
  procedure CAL_BOLL;
  -- 计算某一日commodity_future_date_data表中的布林带，必须在计算完均线后运行
  procedure CAL_BOLL_BY_DATE(p_date varchar2);

  -- 计算所有期货的乖离率
  procedure WRITE_BIAS;
  -- 按照日期，计算所有期货在某一日的乖离率
  procedure WRITE_BIAS_BY_DATE(p_date in varchar2);

  -- 计算所有期货的方差
  procedure WRITE_VARIANCE;
  -- 按照日期，计算所有期货在某一日的的方差
  procedure WRITE_VARIANCE_BY_DATE(p_date in varchar2);

end PKG_COMMODITY_FUTURE_DATE_DATA;
/

