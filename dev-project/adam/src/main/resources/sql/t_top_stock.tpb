???create or replace type body scott.T_TOP_STOCK is

  member function get_code return varchar2 is
  begin
    return v_code;
  end get_code;

  member procedure set_code(p_code varchar2) is
  begin
    v_code := p_code;
  end set_code;

  member function get_name return varchar2 is
  begin
    return v_name;
  end get_name;

  member procedure set_name(p_name varchar2) is
  begin
    v_name := p_name;
  end set_name;

  member function get_board_name return varchar2 is
  begin
    return v_board_name;
  end get_board_name;

  member procedure set_board_name(p_board_name varchar2) is
  begin
    v_board_name := p_board_name;
  end set_board_name;

  member function get_up_down_percentage return number is
  begin
    return v_up_down_percentage;
  end get_up_down_percentage;

  member procedure set_up_down_percentage(p_up_down_percentage number) is
  begin
    v_up_down_percentage := p_up_down_percentage;
  end set_up_down_percentage;

end;
/

