???create or replace package body scott.PKG_ETF is

  /*------------- 插入ETF_TRANSACTION_DATA（只包括基础数据） --------------------*/
  procedure WRITE_ETF_TRANSACTION_DATA(p_code          in nvarchar2,
                                       p_date          in date,
                                       p_open_price    in number,
                                       p_highest_price in number,
                                       p_close_price   in number,
                                       p_lowest_price  in number,
                                       p_change_amount in number,
                                       p_change_range  in number,
                                       p_volume        in number,
                                       p_turnover      in number) is
  begin
    INSERT into ETF_TRANSACTION_DATA
      (CODE_,
       DATE_,
       OPEN_PRICE,
       HIGHEST_PRICE,
       CLOSE_PRICE,
       LOWEST_PRICE,
       CHANGE_AMOUNT,
       CHANGE_RANGE,
       VOLUME,
       TURNOVER)
    values
      (p_code,
       p_date,
       p_open_price,
       p_highest_price,
       p_close_price,
       p_lowest_price,
       p_change_amount,
       p_change_range,
       p_volume,
       p_turnover);
    commit;
  end WRITE_ETF_TRANSACTION_DATA;

  /*----------- 插入上ETF_TRANSACTION_DATA（包括基础数据和Hei Kin Ashi数据） --------*/
  procedure SAVE_ETF_TRANSACTION_DATA(p_code           in nvarchar2,
                                      p_date           in date,
                                      p_open_price     in number,
                                      p_highest_price  in number,
                                      p_close_price    in number,
                                      p_lowest_price   in number,
                                      p_change_amount  in number,
                                      p_change_range   in number,
                                      p_volume         in number,
                                      p_turnover       in number,
                                      p_ma5            in number,
                                      p_ma10           in number,
                                      p_ma20           in number,
                                      p_ma60           in number,
                                      p_ma120          in number,
                                      p_ma250          in number,
                                      ha_open_price    in number,
                                      ha_highest_price in number,
                                      ha_close_price   in number,
                                      ha_lowest_price  in number,
                                      p_bias5          in number,
                                      p_bias10         in number,
                                      p_bias20         in number,
                                      p_bias60         in number,
                                      p_bias120        in number,
                                      p_bias250        in number) is
  begin
    INSERT into ETF_TRANSACTION_DATA
      (CODE_,
       DATE_,
       OPEN_PRICE,
       HIGHEST_PRICE,
       CLOSE_PRICE,
       LOWEST_PRICE,
       CHANGE_AMOUNT,
       CHANGE_RANGE,
       VOLUME,
       TURNOVER,
       MA5,
       MA10,
       MA20,
       MA60,
       MA120,
       MA250,
       HA_OPEN_PRICE,
       HA_HIGHEST_PRICE,
       HA_CLOSE_PRICE,
       HA_LOWEST_PRICE,
       BIAS5,
       BIAS10,
       BIAS20,
       BIAS60,
       BIAS120,
       BIAS250)
    values
      (p_code,
       p_date,
       p_open_price,
       p_highest_price,
       p_close_price,
       p_lowest_price,
       p_change_amount,
       p_change_range,
       p_volume,
       p_turnover,
       p_ma5,
       p_ma10,
       p_ma20,
       p_ma60,
       p_ma120,
       p_ma250,
       ha_open_price,
       ha_highest_price,
       ha_close_price,
       ha_lowest_price,
       p_bias5,
       p_bias10,
       p_bias20,
       p_bias60,
       p_bias120,
       p_bias250);
    commit;
  end SAVE_ETF_TRANSACTION_DATA;
  
  /*-------------------------------- 计算简单移动平均线(MA5) -----------------------------*/
  procedure write_last_close_price is
    -- 代码
    v_code varchar2(50);
    -- 表示是否是第一条记录
    v_first boolean:=true;
    -- 上一条记录的last_close_price字段
    v_last_close_price number;
    -- 查询所有的code
    cursor cur_code is select distinct t.code_ from etf_transaction_data t;
    -- 某一个ETF的全部记录，升序排列
    cursor cur_etf is select * from etf_transaction_data t where t.code_=v_code order by t.date_ asc;
  begin
    for i in cur_code loop
      v_code := i.code_;
      
      -- 重置
      v_last_close_price := null;
      v_first :=true;
      
      for j in cur_etf loop
        if v_first = true then
          -- 如果是这个ETF的第一条记录，则跳过
          v_last_close_price := j.close_price;
          v_first := false;
          continue;
        else
          -- 更新
          update etf_transaction_data t set t.last_close_price=v_last_close_price
          where t.code_=v_code and t.date_=j.date_;
          
          -- 设置上一条交易记录的收盘价
          v_last_close_price := j.close_price;
        end if;
      end loop;
    end loop;
    commit;
  end write_last_close_price;

  /*-------------------------------- 计算简单移动平均线(MA5) -----------------------------*/
  procedure WRITE_FIVE is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示5天收盘价的和
      v_five_sum number := 0;
      -- 表示5天收盘价的平均值
      v_five_average number := 0;
      -- 定义一个含有5个数值型数据的数组
      type type_array is varray(5) of number;
      array_five type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_etf_t_d_code is
        select distinct t.code_
          from ETF_TRANSACTION_DATA t
         order by t.code_ asc;
      cursor cur_all_etf_t_d_c_p is
        select t.close_price, t.date_
          from etf_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_etf_t_d_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_five := type_array();
        for j in cur_all_etf_t_d_c_p loop
          array_five.extend; -- 扩展数组，扩展一个元素
          array_five(array_five.count) := j.close_price;
          if mod(array_five.count, 5) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_five_sum := 0;
            for x in 1 .. array_five.count loop
              -- 求5天收盘价的和
              v_five_sum := v_five_sum + array_five(x);
            end loop;
            -- dbms_output.put_line('v_five_sum'||'  :  '||v_five_sum);
            -- 删除数组中的第一个元素，将其与4个元素向前挪一位，并删除下标为5的元素
            for y in 1 .. array_five.count - 1 loop
              array_five(y) := array_five(y + 1);
            end loop;
            array_five.trim;
            -- 5天收盘价的平均值
            v_five_average := v_five_sum / 5;
            -- 向所有记录的FIVE列插入5天收盘价的平均值
            update etf_transaction_data t
            -- set t.ma5 = round(v_five_average, 2)
               set t.ma5 = v_five_average
             where t.code_ = v_code
               and t.date_ = j.date_;
            -- dbms_output.put_line('v_five_average'||'  :  '||v_five_average);
          end if;
        end loop;
      end loop;
    end;
  end WRITE_FIVE;

  /*-------------------------------- 计算简单移动平均线(MA10) -----------------------------*/
  procedure WRITE_TEN is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示10天收盘价的和
      v_ten_sum number := 0;
      -- 表示10天收盘价的平均值
      v_ten_average number := 0;
      -- 定义一个含有10个数值型数据的数组
      type type_array is varray(10) of number;
      array_ten type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_etf_t_d_code is
        select distinct t.code_
          from etf_transaction_data t
         order by t.code_ asc;
      cursor cur_all_etf_t_d_c_p is
        select t.close_price, t.date_
          from etf_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_etf_t_d_code loop
        v_code := i.code_;
        dbms_output.put_line('**************   v_code' || '  :  ' ||
                             v_code);
        array_ten := type_array();
        for j in cur_all_etf_t_d_c_p loop
          array_ten.extend; -- 扩展数组，扩展一个元素
          array_ten(array_ten.count) := j.close_price;
          if mod(array_ten.count, 10) = 0 then
            dbms_output.put_line('j.date_' || '  :  ' || j.date_);
            v_ten_sum := 0;
            for x in 1 .. array_ten.count loop
              -- 求10天收盘价的和
              v_ten_sum := v_ten_sum + array_ten(x);
            end loop;
            dbms_output.put_line('ten_sum' || '  :  ' || v_ten_sum);
            -- 删除数组中的第一个元素，将其与9个元素向前挪一位，并删除下标为10的元素
            for y in 1 .. array_ten.count - 1 loop
              array_ten(y) := array_ten(y + 1);
            end loop;
            array_ten.trim;
            -- 10天收盘价的平均值
            v_ten_average := v_ten_sum / 10;
            -- 向所有记录的FIVE列插入10天收盘价的平均值
            update etf_transaction_data t
            --set t.ma10 = round(v_ten_average, 2)
               set t.ma10 = v_ten_average
             where t.code_ = v_code
               and t.date_ = j.date_;
            dbms_output.put_line('v_ten_average' || '  :  ' ||
                                 v_ten_average);
          end if;
        end loop;
      end loop;
    end;
  end WRITE_TEN;

  /*-------------------------------- 计算简单移动平均线(MA20) -----------------------------*/
  procedure WRITE_TWENTY is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示20天收盘价的和
      v_twenty_sum number := 0;
      -- 表示20天收盘价的平均值
      v_twenty_average number := 0;
      -- 定义一个含有20个数值型数据的数组
      type type_array is varray(20) of number;
      array_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_etf_t_d_code is
        select distinct t.code_
          from etf_transaction_data t
         order by t.code_ asc;
      cursor cur_all_etf_t_d_c_p is
        select t.close_price, t.date_
          from etf_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_etf_t_d_code loop
        v_code := i.code_;
        dbms_output.put_line('**************   v_code' || '  :  ' ||
                             v_code);
        array_twenty := type_array();
        for j in cur_all_etf_t_d_c_p loop
          array_twenty.extend; -- 扩展数组，扩展一个元素
          array_twenty(array_twenty.count) := j.close_price;
          if mod(array_twenty.count, 20) = 0 then
            dbms_output.put_line('j.date_' || '  :  ' || j.date_);
            v_twenty_sum := 0;
            for x in 1 .. array_twenty.count loop
              -- 求20天收盘价的和
              v_twenty_sum := v_twenty_sum + array_twenty(x);
            end loop;
            dbms_output.put_line('v_twenty_sum' || '  :  ' || v_twenty_sum);
            -- 删除数组中的第一个元素，将其与19个元素向前挪一位，并删除下标为20的元素
            for y in 1 .. array_twenty.count - 1 loop
              array_twenty(y) := array_twenty(y + 1);
            end loop;
            array_twenty.trim;
            -- 20天收盘价的平均值
            v_twenty_average := v_twenty_sum / 20;
            -- 向所有记录的FIVE列插入20天收盘价的平均值
            update etf_transaction_data t
            --set t.ma20 = round(v_twenty_average, 2)
               set t.ma20 = v_twenty_average
             where t.code_ = v_code
               and t.date_ = j.date_;
            dbms_output.put_line('v_twenty_average' || '  :  ' ||
                                 v_twenty_average);
          end if;
        end loop;
      end loop;
    end;
  end WRITE_TWENTY;

  /*-------------------------------- 计算简单移动平均线(MA60) -----------------------------*/
  procedure WRITE_SIXTY is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示60天收盘价的和
      v_sixty_sum number := 0;
      -- 表示60天收盘价的平均值
      v_sixty_average number := 0;
      -- 定义一个含有60个数值型数据的数组
      type type_array is varray(60) of number;
      array_sixty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_etf_t_d_code is
        select distinct t.code_
          from etf_transaction_data t
         order by t.code_ asc;
      cursor cur_all_etf_t_d_c_p is
        select t.close_price, t.date_
          from etf_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_etf_t_d_code loop
        v_code := i.code_;
        dbms_output.put_line('**************   v_code' || '  :  ' ||
                             v_code);
        array_sixty := type_array();
        for j in cur_all_etf_t_d_c_p loop
          array_sixty.extend; -- 扩展数组，扩展一个元素
          array_sixty(array_sixty.count) := j.close_price;
          if mod(array_sixty.count, 60) = 0 then
            dbms_output.put_line('j.date_' || '  :  ' || j.date_);
            v_sixty_sum := 0;
            for x in 1 .. array_sixty.count loop
              -- 求60天收盘价的和
              v_sixty_sum := v_sixty_sum + array_sixty(x);
            end loop;
            dbms_output.put_line('v_sixty_sum' || '  :  ' || v_sixty_sum);
            -- 删除数组中的第一个元素，将其与59个元素向前挪一位，并删除下标为60的元素
            for y in 1 .. array_sixty.count - 1 loop
              array_sixty(y) := array_sixty(y + 1);
            end loop;
            array_sixty.trim;
            -- 60天收盘价的平均值
            v_sixty_average := v_sixty_sum / 60;
            -- 向所有记录的FIVE列插入60天收盘价的平均值
            update etf_transaction_data t
            --set t.ma60 = round(v_sixty_average, 2)
               set t.ma60 = v_sixty_average
             where t.code_ = v_code
               and t.date_ = j.date_;
            dbms_output.put_line('v_sixty_average' || '  :  ' ||
                                 v_sixty_average);
          end if;
        end loop;
      end loop;
    end;
  end WRITE_SIXTY;

  /*-------------------------------- 计算简单移动平均线(MA120) -----------------------------*/
  procedure WRITE_ONEHUNDREDTWENTY is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示120天收盘价的和
      v_one_hundred_twenty_sum number := 0;
      -- 表示120天收盘价的平均值
      v_one_hundred_twenty_average number := 0;
      -- 定义一个含有120个数值型数据的数组
      type type_array is varray(120) of number;
      array_one_hundred_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_etf_t_d_code is
        select distinct t.code_
          from etf_transaction_data t
         order by t.code_ asc;
      cursor cur_all_etf_t_d_c_p is
        select t.close_price, t.date_
          from etf_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_etf_t_d_code loop
        v_code := i.code_;
        dbms_output.put_line('**************   v_code' || '  :  ' ||
                             v_code);
        array_one_hundred_twenty := type_array();
        for j in cur_all_etf_t_d_c_p loop
          array_one_hundred_twenty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_twenty(array_one_hundred_twenty.count) := j.close_price;
          if mod(array_one_hundred_twenty.count, 120) = 0 then
            dbms_output.put_line('j.date_' || '  :  ' || j.date_);
            v_one_hundred_twenty_sum := 0;
            for x in 1 .. array_one_hundred_twenty.count loop
              -- 求120天收盘价的和
              v_one_hundred_twenty_sum := v_one_hundred_twenty_sum +
                                          array_one_hundred_twenty(x);
            end loop;
            dbms_output.put_line('v_one_hundred_twenty_sum' || '  :  ' ||
                                 v_one_hundred_twenty_sum);
            -- 删除数组中的第一个元素，将其与119个元素向前挪一位，并删除下标为120的元素
            for y in 1 .. array_one_hundred_twenty.count - 1 loop
              array_one_hundred_twenty(y) := array_one_hundred_twenty(y + 1);
            end loop;
            array_one_hundred_twenty.trim;
            -- 120天收盘价的平均值
            v_one_hundred_twenty_average := v_one_hundred_twenty_sum / 120;
            -- 向所有记录的FIVE列插入120天收盘价的平均值
            update etf_transaction_data t
            --set t.ma120 = round(v_one_hundred_twenty_average, 2)
               set t.ma120 = v_one_hundred_twenty_average
             where t.code_ = v_code
               and t.date_ = j.date_;
            dbms_output.put_line('v_one_hundred_twenty_average' || '  :  ' ||
                                 v_one_hundred_twenty_average);
          end if;
        end loop;
      end loop;
    end;
  end WRITE_ONEHUNDREDTWENTY;

  /*-------------------------------- 计算简单移动平均线(MA250) -----------------------------*/
  procedure WRITE_TWOHUNDREDFIFTY is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示250天收盘价的和
      v_one_hundred_fifty_sum number := 0;
      -- 表示250天收盘价的平均值
      v_one_hundred_fifty_average number := 0;
      -- 定义一个含有250个数值型数据的数组
      type type_array is varray(250) of number;
      array_one_hundred_fifty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_etf_t_d_code is
        select distinct t.code_
          from etf_transaction_data t
         order by t.code_ asc;
      cursor cur_all_etf_t_d_c_p is
        select t.close_price, t.date_
          from etf_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_etf_t_d_code loop
        v_code := i.code_;
        dbms_output.put_line('**************   v_code' || '  :  ' ||
                             v_code);
        array_one_hundred_fifty := type_array();
        for j in cur_all_etf_t_d_c_p loop
          array_one_hundred_fifty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_fifty(array_one_hundred_fifty.count) := j.close_price;
          if mod(array_one_hundred_fifty.count, 250) = 0 then
            dbms_output.put_line('j.date_' || '  :  ' || j.date_);
            v_one_hundred_fifty_sum := 0;
            for x in 1 .. array_one_hundred_fifty.count loop
              -- 求250天收盘价的和
              v_one_hundred_fifty_sum := v_one_hundred_fifty_sum +
                                         array_one_hundred_fifty(x);
            end loop;
            dbms_output.put_line('v_one_hundred_fifty_sum' || '  :  ' ||
                                 v_one_hundred_fifty_sum);
            -- 删除数组中的第一个元素，将其与249个元素向前挪一位，并删除下标为250的元素
            for y in 1 .. array_one_hundred_fifty.count - 1 loop
              array_one_hundred_fifty(y) := array_one_hundred_fifty(y + 1);
            end loop;
            array_one_hundred_fifty.trim;
            -- 250天收盘价的平均值
            v_one_hundred_fifty_average := v_one_hundred_fifty_sum / 250;
            -- 向所有记录的FIVE列插入250天收盘价的平均值
            update etf_transaction_data t
            --set t.ma250 = round(v_one_hundred_fifty_average, 2)
               set t.ma250 = v_one_hundred_fifty_average
             where t.code_ = v_code
               and t.date_ = j.date_;
            dbms_output.put_line('v_one_hundred_fifty_average' || '  :  ' ||
                                 v_one_hundred_fifty_average);
          end if;
        end loop;
      end loop;
    end;
  end WRITE_TWOHUNDREDFIFTY;

  /*---- 计算ETF_TRANSACTION_DATA的hei kin ashi的开盘价，收盘价，最高价和最低价 ----*/
  procedure WRITE_ETF_TRANSACTION_DATA_HA is
    -- 代码
    v_code varchar2(10);
    -- 日期
    v_date date;
    -- 获取所有的CODE
    cursor cur_all_code is
      select distinct t.code_ etf_transaction_data_code
        from ETF_TRANSACTION_DATA t;
    -- 查询某个具体的etf的第一条交易记录
    cursor cur_first_etf_transaction_data is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
               order by t.date_ asc)
       where rownum <= 1;
    -- 查询某个etf除了最早的一条记录外的其他记录，并按升序排列
    cursor cur_later_etf_transaction_data is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_date
       order by t.date_ asc;
    -- 定义表etf_transaction_data结构的游标变量
    first_etf_transaction_data etf_transaction_data%rowtype;
    later_etf_transaction_data etf_transaction_data%rowtype;
    pre_etf_transaction_data   etf_transaction_data%rowtype;
    -- 用于计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价的变量
    v_ha_open_price    number;
    v_ha_close_price   number;
    v_ha_highest_price number;
    v_ha_lowest_price  number;
  begin
    open cur_all_code;
    loop
      -- 获取每个etf的code_字段
      fetch cur_all_code
        into v_code;
      exit when cur_all_code%notfound;
    
      -- 先计算每个etf的第一条记录
      open cur_first_etf_transaction_data;
      fetch cur_first_etf_transaction_data
        into first_etf_transaction_data;
      exit when cur_first_etf_transaction_data%notfound;
    
      -- 计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价
      v_ha_open_price  := (first_etf_transaction_data.open_price +
                          first_etf_transaction_data.close_price) / 2;
      v_ha_close_price := (first_etf_transaction_data.open_price +
                          first_etf_transaction_data.close_price +
                          first_etf_transaction_data.highest_price +
                          first_etf_transaction_data.lowest_price) / 4;
      if first_etf_transaction_data.highest_price > v_ha_open_price then
        v_ha_highest_price := first_etf_transaction_data.highest_price;
      else
        v_ha_highest_price := v_ha_open_price;
      end if;
      if first_etf_transaction_data.lowest_price < v_ha_open_price then
        v_ha_lowest_price := first_etf_transaction_data.lowest_price;
      else
        v_ha_lowest_price := v_ha_open_price;
      end if;
      -- 保存hei kin ashi平均K线开盘价，收盘价，最高价和最低价
      update etf_transaction_data t
         set t.ha_open_price    = v_ha_open_price,
             t.ha_close_price   = v_ha_close_price,
             t.ha_highest_price = v_ha_highest_price,
             t.ha_lowest_price  = v_ha_lowest_price
       where t.code_ = first_etf_transaction_data.code_
         and t.date_ = first_etf_transaction_data.date_;
      commit;
      v_date := first_etf_transaction_data.date_;
      close cur_first_etf_transaction_data;
    
      -- 再计算每个etf的其他记录
      open cur_later_etf_transaction_data;
      loop
        fetch cur_later_etf_transaction_data
          into later_etf_transaction_data;
        exit when cur_later_etf_transaction_data%notfound;
      
        -- 前一条记录
        select *
          into pre_etf_transaction_data
          from (select *
                  from etf_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= v_date
                 order by t.date_ desc)
         where rownum <= 1;
      
        -- 计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        v_ha_open_price  := (pre_etf_transaction_data.ha_open_price +
                            pre_etf_transaction_data.ha_close_price) / 2;
        v_ha_close_price := (later_etf_transaction_data.open_price +
                            later_etf_transaction_data.close_price +
                            later_etf_transaction_data.highest_price +
                            later_etf_transaction_data.lowest_price) / 4;
        if later_etf_transaction_data.highest_price > v_ha_open_price then
          v_ha_highest_price := later_etf_transaction_data.highest_price;
        else
          v_ha_highest_price := v_ha_open_price;
        end if;
        if later_etf_transaction_data.lowest_price < v_ha_open_price then
          v_ha_lowest_price := later_etf_transaction_data.lowest_price;
        else
          v_ha_lowest_price := v_ha_open_price;
        end if;
        -- 保存hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        update etf_transaction_data t
           set t.ha_open_price    = v_ha_open_price,
               t.ha_close_price   = v_ha_close_price,
               t.ha_highest_price = v_ha_highest_price,
               t.ha_lowest_price  = v_ha_lowest_price
         where t.code_ = later_etf_transaction_data.code_
           and t.date_ = later_etf_transaction_data.date_;
        commit;
      
        -- 把这次的etf留给下一次迭代使用
        v_date := later_etf_transaction_data.date_;
      end loop;
      close cur_later_etf_transaction_data;
    
    end loop;
    close cur_all_code;
  end WRITE_ETF_TRANSACTION_DATA_HA;

  /*---------------- 计算所有etf的乖离率(WRITE_ETF_TRANSACTION_DATA_BIAS) ----------------*/
  procedure WRITE_ETF_TRANSACT_DATA_BIAS is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 5日乖离率
      v_bias5 number := null;
      -- 10日乖离率
      v_bias10 number := null;
      -- 20日乖离率
      v_bias20 number := null;
      -- 60日乖离率
      v_bias60 number := null;
      -- 120日乖离率
      v_bias120 number := null;
      -- 250日乖离率
      v_bias250 number := null;
      -- 交易日期
      v_date date;
      -- 获取etf的code_，排除重复
      cursor cur_all_code is
        select distinct t.code_ from ETF_TRANSACTION_DATA t;
      -- 根据v_code，获取这个指数的所有数据，并按照升序排列
      cursor cur_single_etf_t_d is
        select *
          from ETF_TRANSACTION_DATA t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_code loop
        v_code := i.code_;
        for j in cur_single_etf_t_d loop
          v_date := j.date_;
          -- 5日乖离率
          if j.ma5 is not null then
            v_bias5 := (j.close_price - j.ma5) / j.ma5 * 100;
          end if;
          -- 10日乖离率
          if j.ma10 is not null then
            v_bias10 := (j.close_price - j.ma10) / j.ma10 * 100;
          end if;
          -- 20日乖离率
          if j.ma20 is not null then
            v_bias20 := (j.close_price - j.ma20) / j.ma20 * 100;
          end if;
          -- 60日乖离率
          if j.ma60 is not null then
            v_bias60 := (j.close_price - j.ma60) / j.ma60 * 100;
          end if;
          -- 120日乖离率
          if j.ma120 is not null then
            v_bias120 := (j.close_price - j.ma120) / j.ma120 * 100;
          end if;
          -- 250日乖离率
          if j.ma250 is not null then
            v_bias250 := (j.close_price - j.ma250) / j.ma250 * 100;
          end if;
        
          -- 更新
          update etf_transaction_data t
             set t.bias5   = v_bias5,
                 t.bias10  = v_bias10,
                 t.bias20  = v_bias20,
                 t.bias60  = v_bias60,
                 t.bias120 = v_bias120,
                 t.bias250 = v_bias250
           where t.code_ = v_code
             and t.date_ = v_date;
        end loop;
        commit;
      end loop;
    end;
  end WRITE_ETF_TRANSACT_DATA_BIAS;

  /*-- 计算某一天所有ETF_TRANSACTION_DATA的简单移动平均线(WRITE_ETF_TRANSACTION_DATA_MA_BY_DATE) --*/
  procedure WRITE_ETF_T_D_MA_BY_DATE(p_date in varchar2) is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 表示5天收盘价的平均值
      v_five_average number := 0;
      -- 表示10天收盘价的平均值
      v_ten_average number := 0;
      -- 表示20天收盘价的平均值
      v_twenty_average number := 0;
      -- 表示60天收盘价的平均值
      v_sixty_average number := 0;
      -- 表示120天收盘价的平均值
      v_one_hundred_twenty_average number := 0;
      -- 表示250天收盘价的平均值
      v_two_hundred_fifty_average number := 0;
      -- 表示所有天收盘价的平均值
      --infiniteAverage number:=0;
      -- define cursor section.返回全部code_字段
      cursor cur_all_code is
        select distinct t.code_
          from etf_transaction_data t
         order by t.code_ asc;
    begin
      for i in cur_all_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
      
        -- 更新所有etf某一天的5日均线
        select avg(d.close_price)
          into v_five_average
          from (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                         order by t.date_ desc)
                 where rownum <= 5) d;
        update etf_transaction_data t
        --set t.ma5 = round(v_five_average, 2)
           set t.ma5 = v_five_average
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
        -- 更新所有etf某一天的10日均线
        select avg(d.close_price)
          into v_ten_average
          from (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                         order by t.date_ desc)
                 where rownum <= 10) d;
        update etf_transaction_data t
        --set t.ma10 = round(v_ten_average, 2)
           set t.ma10 = v_ten_average
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
        -- 更新所有etf某一天的20日均线
        select avg(d.close_price)
          into v_twenty_average
          from (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                         order by t.date_ desc)
                 where rownum <= 20) d;
        update etf_transaction_data t
        --set t.ma20 = round(v_twenty_average, 2)
           set t.ma20 = v_twenty_average
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
        -- 更新所有etf某一天的60日均线
        select avg(d.close_price)
          into v_sixty_average
          from (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                         order by t.date_ desc)
                 where rownum <= 60) d;
        update etf_transaction_data t
        --set t.ma60 = round(v_sixty_average, 2)
           set t.ma60 = v_sixty_average
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
        -- 更新所有etf某一天的120日均线
        select avg(d.close_price)
          into v_one_hundred_twenty_average
          from (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                         order by t.date_ desc)
                 where rownum <= 120) d;
        update etf_transaction_data t
        --set t.ma120 = round(v_one_hundred_twenty_average, 2)
           set t.ma120 = v_one_hundred_twenty_average
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
        -- 更新所有etf某一天的250日均线
        select avg(d.close_price)
          into v_two_hundred_fifty_average
          from (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                         order by t.date_ desc)
                 where rownum <= 250) d;
        update etf_transaction_data t
        --set t.ma250 = round(v_two_hundred_fifty_average, 2)
           set t.ma250 = v_two_hundred_fifty_average
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
      -- 更新所有股票某一天的所有日均线
      /*select avg(d.stock_close) into infiniteAverage from(
                                                                                                                                                                                                     select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') order by t.stock_date desc
                                                                                                                                                                                              ) d;
                                                                                                                                                                                              update stock_moving_average t set t.infinite=round(infiniteAverage,2) where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
                                                                                                                                                                                              */
      end loop;
    end;
  end WRITE_ETF_T_D_MA_BY_DATE;

  /*-- 按日期，计算ETF_TRANSACTION_DATA的hei kin ashi的开盘价，收盘价，最高价和最低价(WRITE_ETF_TRANSACTION_DATA_HA_BY_DATE) --*/
  procedure WRITE_ETF_T_D_HA_BY_DATE(p_date in varchar2) is
    -- 获取所有的CODE
    cursor cur_all_etf_t_d_code is
      select distinct t.code_ etf_t_d_code from etf_transaction_data t;
    -- 表示CODE类型的变量
    v_code varchar2(10);
  
    -- 查询某个etf在日期p_date之前的那一条记录
    cursor cur_all_etf_t_d is
      select *
        from (select *
                from (select *
                        from etf_transaction_data t
                       where t.code_ = v_code
                         and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                       order by t.date_ desc)
               where rownum <= 2) t2
       where t2.date_ <>
             (select t1.date_
                from etf_transaction_data t1
               where t1.code_ = v_code
                 and t1.date_ = to_date(p_date, 'yyyy-mm-dd'));
    -- 定义表etf_transaction_data结构的游标变量
    all_etf_transaction_data etf_transaction_data%rowtype;
  
    -- 查询某个具体的etf的某一日交易记录
    cursor cur_later_etf_transaction_data is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    -- 定义表etf_transaction_data结构的游标变量
    later_etf_transaction_data etf_transaction_data%rowtype;
  
    -- 用于计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价的变量
    v_ha_open_price    number;
    v_ha_close_price   number;
    v_ha_highest_price number;
    v_ha_lowest_price  number;
  begin
    open cur_all_etf_t_d_code;
    loop
      -- 获取每个etf的code_字段
      fetch cur_all_etf_t_d_code
        into v_code;
      exit when cur_all_etf_t_d_code%notfound;
    
      open cur_all_etf_t_d;
      open cur_later_etf_transaction_data;
      loop
        fetch cur_all_etf_t_d
          into all_etf_transaction_data;
        exit when cur_all_etf_t_d%notfound;
      
        fetch cur_later_etf_transaction_data
          into later_etf_transaction_data;
        exit when cur_later_etf_transaction_data%notfound;
      
        -- 计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        v_ha_open_price  := (all_etf_transaction_data.ha_open_price +
                            all_etf_transaction_data.ha_close_price) / 2;
        v_ha_close_price := (later_etf_transaction_data.open_price +
                            later_etf_transaction_data.close_price +
                            later_etf_transaction_data.highest_price +
                            later_etf_transaction_data.lowest_price) / 4;
        if later_etf_transaction_data.highest_price > v_ha_open_price then
          v_ha_highest_price := later_etf_transaction_data.highest_price;
        else
          v_ha_highest_price := v_ha_open_price;
        end if;
        if later_etf_transaction_data.lowest_price < v_ha_open_price then
          v_ha_lowest_price := later_etf_transaction_data.lowest_price;
        else
          v_ha_lowest_price := v_ha_open_price;
        end if;
        -- 保存hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        update etf_transaction_data t
           set t.ha_open_price    = v_ha_open_price,
               t.ha_close_price   = v_ha_close_price,
               t.ha_highest_price = v_ha_highest_price,
               t.ha_lowest_price  = v_ha_lowest_price
         where t.code_ = later_etf_transaction_data.code_
           and t.date_ = later_etf_transaction_data.date_;
        commit;
      end loop;
      close cur_all_etf_t_d;
      close cur_later_etf_transaction_data;
    
    end loop;
    close cur_all_etf_t_d_code;
  end WRITE_ETF_T_D_HA_BY_DATE;

  /*---- 按照日期，计算所有etf在某一日的乖离率(WRITE_ETF_TRANSACTION_DATA_BIAS_BY_DATE) ----*/
  procedure WRITE_ETF_T_D_BIAS_BY_DATE(p_date in varchar2) is
    -- 表示CODE_类型的变量
    v_code varchar2(10);
    -- 5日乖离率
    v_bias5 number := null;
    -- 10日乖离率
    v_bias10 number := null;
    -- 20日乖离率
    v_bias20 number := null;
    -- 60日乖离率
    v_bias60 number := null;
    -- 120日乖离率
    v_bias120 number := null;
    -- 250日乖离率
    v_bias250 number := null;
  
    row_etf_transaction_data etf_transaction_data%rowtype;
    -- 获取所有etf的code_，排除重复的
    cursor cur_all_etf_t_d_code is
      select distinct t.code_ from etf_transaction_data t;
    -- 某个etf某一天的交易记录
    cursor cur_single_etf_t_d is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
  begin
    open cur_all_etf_t_d_code;
    loop
      -- 获取每个etf的code_字段
      fetch cur_all_etf_t_d_code
        into v_code;
      exit when cur_all_etf_t_d_code%notfound;
    
      open cur_single_etf_t_d;
      loop
        -- 获取每个etf在某一日的交易记录
        fetch cur_single_etf_t_d
          into row_etf_transaction_data;
        exit when cur_single_etf_t_d%notfound;
      
        -- 计算5日乖离率、10日乖离率、20日乖离率、60日乖离率、120日乖离率、250日乖离率
        v_bias5   := (row_etf_transaction_data.close_price -
                     row_etf_transaction_data.ma5) /
                     row_etf_transaction_data.ma5 * 100;
        v_bias10  := (row_etf_transaction_data.close_price -
                     row_etf_transaction_data.ma10) /
                     row_etf_transaction_data.ma10 * 100;
        v_bias20  := (row_etf_transaction_data.close_price -
                     row_etf_transaction_data.ma20) /
                     row_etf_transaction_data.ma20 * 100;
        v_bias60  := (row_etf_transaction_data.close_price -
                     row_etf_transaction_data.ma60) /
                     row_etf_transaction_data.ma60 * 100;
        v_bias120 := (row_etf_transaction_data.close_price -
                     row_etf_transaction_data.ma120) /
                     row_etf_transaction_data.ma120 * 100;
        v_bias250 := (row_etf_transaction_data.close_price -
                     row_etf_transaction_data.ma250) /
                     row_etf_transaction_data.ma250 * 100;
      
        -- 更新某个etf在某一日的5日乖离率、10日乖离率、20日乖离率、60日乖离率、120日乖离率、250日乖离率
        update etf_transaction_data t
           set t.bias5   = v_bias5,
               t.bias10  = v_bias10,
               t.bias20  = v_bias20,
               t.bias60  = v_bias60,
               t.bias120 = v_bias120,
               t.bias250 = v_bias250
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
      end loop;
      close cur_single_etf_t_d;
    
      commit;
    
    end loop;
    close cur_all_etf_t_d_code;
  end WRITE_ETF_T_D_BIAS_BY_DATE;

  /*----------------------- 计算所有ETF_TRANSACTION_DATA的MACD(INIT) -----------------------*/
  procedure WRITE_MACD_INIT as
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取每只etf第一个交易日的日期
    cursor cur_first_etf_t_d_date is
      select min(t.date_) as date_
        from etf_transaction_data t
       where t.code_ = v_code;
  begin
    -- 初始化每只etf第一个交易日的ema12,ema26,dif和dea字段
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      for j in cur_first_etf_t_d_date loop
        update etf_transaction_data t
           set t.ema12 = t.close_price,
               t.ema26 = t.close_price,
               t.dif   = 0,
               t.dea   = 0
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
      end loop;
    end loop;
  end WRITE_MACD_INIT;

  /*----------------------- 计算所有ETF_TRANSACTION_DATA的MACD(EMA) -----------------------*/
  procedure WRITE_MACD_EMA as
    v_pre_ema12  number;
    v_pre_ema26  number;
    v_pre_dif    number;
    v_pre_dea    number;
    v_first_date date;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取每只etf第一个交易日的日期
    cursor cur_first_etf_t_d_date is
      select min(t.date_) as date_
        from etf_transaction_data t
       where t.code_ = v_code;
    -- 根据v_code选出某只etf的除第一天以外的全部交易记录，按升序排列
    cursor cur_all_etf_t_d is
      select distinct *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_first_date
       order by t.date_ asc;
  begin
    -- 计算每只etf其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      -- 用记录是第一个交易日的字段初始化相关变量
      for x in cur_first_etf_t_d_date loop
        select t.ema12, t.ema26, t.dif, t.dea
          into v_pre_ema12, v_pre_ema26, v_pre_dif, v_pre_dea
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = x.date_;
      end loop;
    
      select min(t.date_)
        into v_first_date
        from etf_transaction_data t
       where t.code_ = v_code;
      for j in cur_all_etf_t_d loop
        -- 对于其余交易日，更新ema12,ema26,dif和dea字段
        update etf_transaction_data t
           set t.ema12 = v_pre_ema12 * 11 / 13 + j.close_price * 2 / 13,
               t.ema26 = v_pre_ema26 * 25 / 27 + j.close_price * 2 / 27
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
        -- 用于计算下一个交易日时使用
        select t.ema12
          into v_pre_ema12
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
        select t.ema26
          into v_pre_ema26
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
      end loop;
    end loop;
  end WRITE_MACD_EMA;

  /*----------------------- 计算所有ETF_TRANSACTION_DATA的MACD(DIF) -----------------------*/
  procedure WRITE_MACD_DIF as
    v_pre_ema12  number;
    v_pre_ema26  number;
    v_pre_dif    number;
    v_pre_dea    number;
    v_first_date date;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取每只etf第一个交易日的日期
    cursor cur_first_etf_t_d_date is
      select min(t.date_) as date_
        from etf_transaction_data t
       where t.code_ = v_code;
    -- 根据v_code选出某只etf的除第一天以外的全部交易记录，按升序排列
    cursor cur_all_etf_t_d is
      select distinct *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_first_date
       order by t.date_ asc;
  begin
    -- 计算每只etf其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      -- 用记录是第一个交易日的字段初始化相关变量
      for x in cur_first_etf_t_d_date loop
        select t.ema12, t.ema26, t.dif, t.dea
          into v_pre_ema12, v_pre_ema26, v_pre_dif, v_pre_dea
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = x.date_;
      end loop;
      select min(t.date_)
        into v_first_date
        from etf_transaction_data t
       where t.code_ = v_code;
      for j in cur_all_etf_t_d loop
        update etf_transaction_data t
           set t.dif = t.ema12 - t.ema26
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
        select t.dif
          into v_pre_dif
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
      end loop;
    end loop;
  end WRITE_MACD_DIF;

  /*----------------------- 计算所有ETF_TRANSACTION_DATA的MACD(DEA) -----------------------*/
  procedure WRITE_MACD_DEA as
    v_pre_ema12  number;
    v_pre_ema26  number;
    v_pre_dif    number;
    v_pre_dea    number;
    v_first_date date;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取每只etf第一个交易日的日期
    cursor cur_first_etf_t_d_date is
      select min(t.date_) as date_
        from etf_transaction_data t
       where t.code_ = v_code;
    -- 根据v_code选出某只etf的除第一天以外的全部交易记录，按升序排列
    cursor cur_all_etf_t_d is
      select distinct *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_first_date
       order by t.date_ asc;
  begin
    -- 计算每只etf其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      -- 用记录是第一个交易日的字段初始化相关变量
      for x in cur_first_etf_t_d_date loop
        select t.ema12, t.ema26, t.dif, t.dea
          into v_pre_ema12, v_pre_ema26, v_pre_dif, v_pre_dea
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = x.date_;
      end loop;
      select min(t.date_)
        into v_first_date
        from etf_transaction_data t
       where t.code_ = v_code;
      for j in cur_all_etf_t_d loop
        update etf_transaction_data t
           set t.dea = v_pre_dea * 8 / 10 + t.dif * 2 / 10
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
        select t.dea
          into v_pre_dea
          from etf_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
      end loop;
    end loop;
  end WRITE_MACD_DEA;

  /*----------------------- 计算所有ETF_TRANSACTION_DATA的MACD(macd) -----------------------*/
  procedure WRITE_MACD as
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
  begin
    -- 计算每只etf其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      update etf_transaction_data t
         set t.macd = 2 * (t.dif - t.dea)
       where t.dif is not null
         and t.dea is not null;
    end loop;
    commit;
  end WRITE_MACD;

  /*------------------- 计算所有ETF_TRANSACTION_DATA的某一日的MACD(EMA) -------------------*/
  procedure WRITE_MACD_EMA_BY_DATE(p_date in varchar2) as
    v_pre_ema12 number;
    v_pre_ema26 number;
    v_pre_dea   number;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取某只etf最近两天的记录
    cursor cur_single_etf_t_d is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      for j in cur_single_etf_t_d loop
        if j.date_ != to_date(p_date, 'yyyy-mm-dd') then
          v_pre_ema12 := j.ema12;
          v_pre_ema26 := j.ema26;
          v_pre_dea   := j.dea;
          update etf_transaction_data t
             set t.ema12 = v_pre_ema12 * 11 / 13 + t.close_price * 2 / 13,
                 t.ema26 = v_pre_ema26 * 25 / 27 + t.close_price * 2 / 27
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_MACD_EMA_BY_DATE;

  /*------------------- 计算所有ETF_TRANSACTION_DATA的某一日的MACD(DIF) -------------------*/
  procedure WRITE_MACD_DIF_BY_DATE(p_date in varchar2) as
    v_pre_ema12 number;
    v_pre_ema26 number;
    v_pre_dea   number;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取某只etf最近两天的记录
    cursor cur_single_etf_t_d is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      for j in cur_single_etf_t_d loop
        if j.date_ != to_date(p_date, 'yyyy-mm-dd') then
          v_pre_ema12 := j.ema12;
          v_pre_ema26 := j.ema26;
          v_pre_dea   := j.dea;
          update etf_transaction_data t
             set t.dif = t.ema12 - t.ema26
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_MACD_DIF_BY_DATE;

  /*------------------- 计算所有ETF_TRANSACTION_DATA的某一日的MACD(DEA) -------------------*/
  procedure WRITE_MACD_DEA_BY_DATE(p_date in varchar2) as
    v_pre_ema12 number;
    v_pre_ema26 number;
    v_pre_dea   number;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取某只etf最近两天的记录
    cursor cur_single_etf_t_d is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      for j in cur_single_etf_t_d loop
        if j.date_ != to_date(p_date, 'yyyy-mm-dd') then
          v_pre_ema12 := j.ema12;
          v_pre_ema26 := j.ema26;
          v_pre_dea   := j.dea;
          update etf_transaction_data t
             set t.dea = v_pre_dea * 8 / 10 + t.dif * 2 / 10
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_MACD_DEA_BY_DATE;

  /*------------------- 计算所有ETF_TRANSACTION_DATA的某一日的MACD(macd) -------------------*/
  procedure WRITE_MACD_BY_DATE(p_date in varchar2) as
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      update etf_transaction_data t
         set t.macd = 2 * (t.dif - t.dea)
       where t.code_ = v_code
         and t.dif is not null
         and t.dea is not null
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    end loop;
    commit;
  end WRITE_MACD_BY_DATE;

  /*----------------------------- 计算日线级别所有etf的KD指标(INIT) ------------------------*/
  procedure WRITE_KD_INIT as
    -- 表示某只指数的code_字段
    v_code varchar2(10);
    -- 用于计算是否是第9个交易周
    num number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 查询某只etf最初的8个交易周的记录，并按生序排列
    cursor cur_single_etf_t_d_k is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
               order by t.date_ asc)
       where rownum <= 8;
  begin
    -- 初始化每只etf第一个交易日的K和D字段
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_etf_t_d_k loop
        num := num + 1;
        if num = 8 then
          -- 若无前一日K值与D值，则可分别用50来代替
          update etf_transaction_data t
             set t.k = 50, t.d = 50
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_INIT;

  /*----------------------------- 计算日线级别所有etf的KD指标(RSV) ------------------------*/
  procedure WRITE_KD_RSV as
    -- 表示某只etf的code_字段
    v_code varchar2(10);
    -- 9日内最高价
    v_nine_day_highest_price number;
    -- 9日内最低价
    v_nine_day_lowest_price number;
    -- 计算etf时使用，用于表示日期的累积
    num number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取某只etf所有的日线级别的交易记录，并按升序排列
    cursor cur_single_etf_t_d is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
       order by t.date_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_etf_t_d loop
        num := num + 1;
        if num >= 9 then
          -- 计算9日内最高价和最低价
          select max(highest_price), min(lowest_price)
            into v_nine_day_highest_price, v_nine_day_lowest_price
            from (select *
                    from etf_transaction_data t
                   where t.code_ = v_code
                     and t.date_ <= j.date_
                   order by t.date_ desc)
           where rownum <= 9;
          -- 计算rsv
          if (v_nine_day_highest_price - v_nine_day_lowest_price) = 0 then
            update etf_transaction_data t
               set t.rsv = 50
             where t.code_ = v_code
               and t.date_ = j.date_;
          else
            update etf_transaction_data t
               set t.rsv =
                   (t.close_price - v_nine_day_lowest_price) /
                   (v_nine_day_highest_price - v_nine_day_lowest_price) * 100
             where t.code_ = v_code
               and t.date_ = j.date_;
          end if;
        end if;
      end loop;
    end loop;
  end WRITE_KD_RSV;

  /*----------------------------- 计算日线级别所有etf的KD指标(K) ------------------------*/
  procedure WRITE_KD_K as
    -- 表示某只etf的CODE_字段
    v_code varchar2(10);
    -- 计算etf时使用，用于表示日期的累积
    num number;
    -- 表示KD指标的K
    v_temp_k number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取某只etf所有的日线级别的交易记录，并按升序排列
    cursor cur_single_etf_t_d is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
       order by t.date_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_etf_t_d loop
        num := num + 1;
        if num = 8 then
          v_temp_k := j.k;
        end if;
        if num >= 9 then
          -- 计算K
          update etf_transaction_data t
             set t.k = 2 / 3 * v_temp_k + 1 / 3 * t.rsv
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
          select t.k
            into v_temp_k
            from etf_transaction_data t
           where t.code_ = v_code
             and t.date_ = j.date_;
        end if;
      
      end loop;
    end loop;
  end WRITE_KD_K;

  /*----------------------------- 计算日线级别所有etf的KD指标(D) ------------------------*/
  procedure WRITE_KD_D as
    -- 表示某只etf的code_字段
    v_code varchar2(10);
    -- 计算etf时使用，用于表示日期的累积
    num number;
    -- 表示KD指标的D
    v_temp_d number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 获取某只etf所有的日线级别的交易记录，并按升序排列
    cursor cur_single_etf_t_d is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
       order by t.date_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_etf_t_d loop
        num := num + 1;
        if num = 8 then
          v_temp_d := j.d;
        end if;
        if num >= 9 then
          -- 计算D
          update etf_transaction_data t
             set t.d = 2 / 3 * v_temp_d + 1 / 3 * j.k
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
          select t.d
            into v_temp_d
            from etf_transaction_data t
           where t.code_ = v_code
             and t.date_ = j.date_;
        end if;
      end loop;
    end loop;
  end WRITE_KD_D;

  /*------------------------ 计算日线级别，某一日，所有指数的KD指标(RSV) --------------------*/
  procedure WRITE_KD_BY_DATE_RSV(p_date varchar2) as
    -- 表示某只指数的code_字段
    v_code varchar2(10);
    -- 9日内最高价
    v_nine_day_highest_price number;
    -- 9日内最低价
    v_nine_day_lowest_price number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      -- 计算9日内最高价和最低价
      select max(highest_price), min(lowest_price)
        into v_nine_day_highest_price, v_nine_day_lowest_price
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 9;
      -- 计算某只股票某一日的RSV
      if (v_nine_day_highest_price - v_nine_day_lowest_price) = 0 then
        update etf_transaction_data t
           set t.rsv = 50
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      else
        update etf_transaction_data t
           set t.rsv =
               (t.close_price - v_nine_day_lowest_price) /
               (v_nine_day_highest_price - v_nine_day_lowest_price) * 100
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      end if;
      commit;
    end loop;
  end WRITE_KD_BY_DATE_RSV;

  /*------------------------ 计算日线级别，某一日，所有指数的KD指标(K) --------------------*/
  procedure WRITE_KD_BY_DATE_K(p_date varchar2) as
    -- 表示某只指数的code_字段
    v_code varchar2(10);
    -- 表示前一日K值
    v_temp_k number;
    -- 用于计数
    v_num number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 按照日期参数，获取某只指数最近两天的日线级别的交易记录，并按降序排列
    cursor cur_single_etf_t_d is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2
       order by date_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      v_num  := 0;
      for j in cur_single_etf_t_d loop
        v_num := v_num + 1;
        if v_num = 1 then
          v_temp_k := j.k;
        end if;
        if v_num = 2 then
          update etf_transaction_data t
             set t.k = 2 / 3 * v_temp_k + 1 / 3 * t.rsv
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_BY_DATE_K;

  /*------------------------ 计算日线级别，某一日，所有指数的KD指标(D) --------------------*/
  procedure WRITE_KD_BY_DATE_D(p_date varchar2) as
    -- 表示某只etf的code_字段
    v_code varchar2(10);
    -- 表示前一日K值
    v_temp_d number;
    -- 用于计数
    v_num number;
    -- define cursor section.返回全部code_
    cursor cur_all_etf_t_d_code is
      select distinct t.code_
        from etf_transaction_data t
       order by t.code_ asc;
    -- 按照日期参数，获取某只etf最近两天的日线级别的交易记录，并按降序排列
    cursor cur_single_etf_t_d is
      select *
        from (select *
                from etf_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2
       order by date_ asc;
  begin
    for i in cur_all_etf_t_d_code loop
      v_code := i.code_;
      v_num  := 0;
      for j in cur_single_etf_t_d loop
        v_num := v_num + 1;
        if v_num = 1 then
          v_temp_d := j.d;
        end if;
        if v_num = 2 then
          update etf_transaction_data t
             set t.d = 2 / 3 * v_temp_d + 1 / 3 * t.k
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_BY_DATE_D;

  ------------------------ calculate etf_transaction_data's MACD's gold cross model ----------------------------
  /*procedure CAL_MDL_STOCK_INDEX_MACD_G_C as
    -- 表示字段MODEL_ID，这个存储过程中是1，在下面初始化
    v_model_id number;
    -- 表示STOCK_CODE
    v_stock_code varchar2(100);
    -- 表示STOCK_DATE
    v_stock_date date;
    -- 表示两天记录中的第一天，用于判断MACD是否发生了金叉或死叉
    v_first_stock_index stock_index%rowtype;
    -- 表示两天记录中的第二天，用于判断MACD是否发生了金叉或死叉
    v_second_stock_index stock_index%rowtype;
    -- 作为临时变量，表示STOCK_CODE,BUY_DATE,SELL_DATE,BUY_PRICE,SELL_PRICE,BUY_DIF,BUY_DEA,SELL_DIF,SELL_DEA
    v_temp_stock_code       varchar2(100);
    v_temp_stock_buy_date   date;
    v_temp_stock_sell_date  date;
    v_temp_stock_buy_price  number;
    v_temp_stock_sell_price number;
    v_temp_stock_buy_dif    number;
    v_temp_stock_buy_dea    number;
    v_temp_stock_sell_dif   number;
    v_temp_stock_sell_dea   number;
    -- 用来判断现在是否是dif>dea的阶段，从而可以判断死叉
    v_start_track_gold_cross_stage number;
    -- 用来表示金叉和死叉是否发现了
    v_gold_cross_found  number;
    v_death_cross_found number;
    -- 每只股票初始的accumulative_profit_loss值为100（表示百分之一百），之后代表上一次交易的累计盈亏百分比
    v_init_last_acc_profit_loss number;
    -- 表示字段PROFIT_LOSS的临时变量
    v_temp_profit_loss number;
    -- 用于获取所有的STOCK_CODE
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data_all t; -- where t.code_ like '000%' or t.code_ like '300%' or t.code_ like '600%' or t.code_ like '603%';
    -- 用于根据STOCK_CODE获取某一只股票的所有交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data_all t
       where t.code_ = lower(v_stock_code)
       order by t.date_ asc;
    -- 用于获取某一只股票两天的交易记录，用于判断MACD是否发生了金叉或死叉
    cursor cur_single_stock_two_record is
      select *
        from (select *
                from stock_transaction_data_all t
               where t.date_ >= v_stock_date
                 and t.code_ = lower(v_stock_code)
               order by t.date_ asc)
       where rownum <= 2;
  begin
    -- initialize variables
    v_model_id := 1;
  
    for i in cur_all_stock_code loop
      v_stock_code                   := lower(i.code_);
      v_init_last_acc_profit_loss    := 100;
      v_start_track_gold_cross_stage := 0;
      for j in cur_single_stock loop
        v_stock_date := j.date_;
        -- 如果最早的数据是dif>dea，那么接下来只有可能先出现死叉，所以要判断：j.dif<j.dea，也就是说如果第一段数据是dif>dea，则将其忽略
        -- 之后在确认了金叉后，需要确认死叉，所以引入了表示变量v_start_track_gold_cross_stage，当其为1时，表示接下来只可能出现死叉
        if j.dif < j.dea or
           (j.dif > j.dea and v_start_track_gold_cross_stage = 1) then
          -- 分别将两天的记录赋给相应的变量
          for x in cur_single_stock_two_record loop
            if cur_single_stock_two_record%rowcount = 1 then
              v_first_stock := x;
            elsif cur_single_stock_two_record%rowcount = 2 then
              v_second_stock := x;
            end if;
          end loop;
          -- 如果出现了金叉，给临时变量赋值
          if v_first_stock.dif < v_first_stock.dea and
             v_second_stock.dif > v_second_stock.dea and
             v_start_track_gold_cross_stage = 0 then
            v_temp_stock_code      := v_second_stock.code_;
            v_temp_stock_buy_date  := v_second_stock.date_;
            v_temp_stock_buy_price := v_second_stock.close_price;
            v_temp_stock_buy_dif   := v_second_stock.dif;
            v_temp_stock_buy_dea   := v_second_stock.dea;
            v_gold_cross_found     := 1;
            -- 当金叉发生后，只可能发生死叉，因此要将v_start_track_gold_cross_stage设置为1
            v_start_track_gold_cross_stage := 1;
  
            dbms_output.put_line('gold cross   ' || j.date_);
          end if;
          -- 如果出现了死叉，给临时变量赋值
          if v_first_stock.dif > v_first_stock.dea and
             v_second_stock.dif < v_second_stock.dea and
             v_start_track_gold_cross_stage = 1 then
            v_temp_stock_sell_date  := v_second_stock.date_;
            v_temp_stock_sell_price := v_second_stock.close_price;
            v_temp_stock_sell_dif   := v_second_stock.dif;
            v_temp_stock_sell_dea   := v_second_stock.dea;
            v_death_cross_found     := 1;
            -- 当死叉发生后，只可能发生金叉，因此要将v_start_track_gold_cross_stage设置为0
            v_start_track_gold_cross_stage := 0;
  
            dbms_output.put_line('death cross   ' || j.date_);
          end if;
          -- 插入数据。要求这只股票在开始时间和结束时间内没有停牌或除权的情况
          if v_gold_cross_found = 1 and v_death_cross_found = 1 and
             v_model_id <> 0 and v_temp_stock_buy_date is not null and
             v_temp_stock_sell_date is not null and
             v_temp_stock_code is not null then
            v_temp_profit_loss          := round((v_temp_stock_sell_price -
                                                 v_temp_stock_buy_price) /
                                                 v_temp_stock_buy_price,
                                                 4) * 100;
            v_init_last_acc_profit_loss := v_init_last_acc_profit_loss *
                                           (1 + v_temp_profit_loss / 100);
  
            insert into MDL_MACD_GOLD_CROSS
              (model_id,
               buy_date,
               sell_date,
               buy_price,
               sell_price,
               profit_loss,
               stock_code,
               accumulative_profit_loss,
               buy_dif,
               buy_dea,
               sell_dif,
               sell_dea)
            values
              (v_model_id,
               v_temp_stock_buy_date,
               v_temp_stock_sell_date,
               v_temp_stock_buy_price,
               v_temp_stock_sell_price,
               v_temp_profit_loss,
               v_temp_stock_code,
               v_init_last_acc_profit_loss,
               v_temp_stock_buy_dif,
               v_temp_stock_buy_dea,
               v_temp_stock_sell_dif,
               v_temp_stock_sell_dea);
  
            v_gold_cross_found  := 0;
            v_death_cross_found := 0;
          end if;
        end if;
      end loop;
    end loop;
    commit;
  end CAL_MDL_STOCK_INDEX_MACD_G_C;*/

  /*------------------------ 海量地计算correlation5_with***列的值 --------------------*/
  procedure WRITE_CORRELATION5 as
    -- 表示code_字段
    v_code varchar2(100);
    -- 相关系数
    v_correlation5_with000001   number;
    v_correlation5_with399001   number;
    v_correlation5_with399005   number;
    v_correlation5_with399006   number;
    v_correlation5_with_avg_c_p number;
    -- etf记录数
    v_etf_count number;
    -- 查询所有etf的code_
    cursor cur_distinct_etf_code is
      select distinct t.code_
        from etf_transaction_data t
        join etf_info ei
          on substr(ei.code_, 3, 8) = t.code_
         and ei.tendency_type = 2;
    -- 根据code_字段，查询某个etf的全部记录，并按照降序排列
    cursor cur_single_etf_desc is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
       order by t.date_ desc;
  begin
    for i in cur_distinct_etf_code loop
      v_code := i.code_;
    
      for j in cur_single_etf_desc loop
      
        -- 计算相关系数
        SELECT /*CORR(etd.close_price, si_000001.close_price),
                                                                                               CORR(etd.close_price, si_399001.close_price),
                                                                                               CORR(etd.close_price, si_399005.close_price),
                                                                                               CORR(etd.close_price, si_399006.close_price),*/
         CORR(etd.close_price, stda.close_price), count(etd.close_price)
          into /*v_correlation5_with000001,
                                                                                                                                                                               v_correlation5_with399001,
                                                                                                                                                                               v_correlation5_with399005,
                                                                                                                                                                               v_correlation5_with399006,*/
               v_correlation5_with_avg_c_p,
               v_etf_count
          FROM (select *
                  from (select *
                          from etf_transaction_data t
                         where t.code_ = v_code
                           and t.date_ <= j.date_
                         order by t.date_ desc) t2
                 where rownum <= 5) etd,
               /*(select *
                  from (select *
                          from stock_index t1
                         where t1.code_ = 000001
                           and t1.date_ <= j.date_
                         order by t1.date_ desc) t2
                 where rownum <= 5) si_000001,
               (select *
                  from (select *
                          from stock_index t1
                         where t1.code_ = 399001
                           and t1.date_ <= j.date_
                         order by t1.date_ desc) t2
                 where rownum <= 5) si_399001,
               (select *
                  from (select *
                          from stock_index t1
                         where t1.code_ = 399005
                           and t1.date_ <= j.date_
                         order by t1.date_ desc) t2
                 where rownum <= 5) si_399005,
               (select *
                  from (select *
                          from stock_index t1
                         where t1.code_ = 399006
                           and t1.date_ <= j.date_
                         order by t1.date_ desc) t2
                 where rownum <= 5) si_399006,*/
               (select *
                  from (select t1.date_ date_,
                               avg(t1.close_price) close_price
                          from stock_transaction_data_all t1
                         where t1.date_ <= j.date_
                         group by t1.date_
                         order by t1.date_ desc) t2
                 where rownum <= 5) stda
         where /*etd.date_ = si_000001.date_
                                                                                           and etd.date_ = si_399001.date_
                                                                                           and etd.date_ = si_399005.date_
                                                                                           and etd.date_ = si_399006.date_
                                                                                           and*/
         etd.date_ = stda.date_;
      
        -- 如果有5条记录匹配，则更新记录
        if v_etf_count = 5 then
          update etf_transaction_data t
             set /*t.correlation5_with000001   = v_correlation5_with000001,
                 t.correlation5_with399001   = v_correlation5_with399001,
                 t.correlation5_with399005   = v_correlation5_with399005,
                 t.correlation5_with399006   = v_correlation5_with399006,*/ t.correlation5_with_avg_c_p = v_correlation5_with_avg_c_p
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
        else
          exit;
        end if;
      
      end loop;
    end loop;
  end WRITE_CORRELATION5;

  /*------------------------ 海量地计算correlation250_with***列的值 --------------------*/
  procedure WRITE_CORRELATION250 as
    -- 表示code_字段
    v_code varchar2(100);
    -- 相关系数
    v_correlation5_with000001     number;
    v_correlation5_with399001     number;
    v_correlation5_with399005     number;
    v_correlation5_with399006     number;
    v_correlation250_with_avg_c_p number;
    -- etf记录数
    v_etf_count number;
    -- 计算etf_transaction_data表的相关系数的开始时间
    v_etf_begin_date date;
    -- 查询所有etf的code_
    cursor cur_distinct_etf_code is
      select t.code_ code_, ei.name_, avg(t.turnover)
        from ETF_TRANSACTION_DATA t
        join etf_info ei
          on substr(ei.code_, 3, 8) = t.code_
         and ei.available = 1
         and ei.tendency_type = 2
       group by t.code_, ei.name_
      having avg(t.turnover) > 1000
       order by avg(t.turnover) desc;
    -- 根据code_字段，查询某个etf的全部记录，并按照降序排列
    cursor cur_single_etf_desc is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
       order by t.date_ desc;
  begin
    for i in cur_distinct_etf_code loop
      v_code := i.code_;
    
      for j in cur_single_etf_desc loop
      
        -- 计算etf_transaction_data表的相关系数的开始时间和记录数
        select min(t2.date_), count(*)
          into v_etf_begin_date, v_etf_count
          from (select *
                  from etf_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= j.date_
                 order by t.date_ desc) t2
         where rownum <= 250;
      
        -- 如果有250条记录匹配
        if v_etf_count >= 250 then
          -- 计算相关系数
          SELECT /*CORR(etd.close_price, si_000001.close_price),
                                                                                                             CORR(etd.close_price, si_399001.close_price),
                                                                                                             CORR(etd.close_price, si_399005.close_price),
                                                                                                             CORR(etd.close_price, si_399006.close_price),*/
           CORR(etd.close_price, stda.close_price)
            into /*v_correlation5_with000001,
                                                                                                                                                                                             v_correlation5_with399001,
                                                                                                                                                                                             v_correlation5_with399005,
                                                                                                                                                                                             v_correlation5_with399006,*/
                 v_correlation250_with_avg_c_p
            FROM (select *
                    from etf_transaction_data t
                   where t.code_ = v_code
                     and t.date_ between v_etf_begin_date and j.date_
                   order by t.date_ desc) etd,
                 /*(select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 000001
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_000001,
                 (select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 399001
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_399001,
                 (select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 399005
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_399005,
                 (select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 399006
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_399006,*/
                 (select t1.date_ date_, avg(t1.close_price) close_price
                    from stock_transaction_data_all t1
                   where t1.date_ between v_etf_begin_date and j.date_
                   group by t1.date_) stda
           where /*etd.date_ = si_000001.date_
                                                                                                         and etd.date_ = si_399001.date_
                                                                                                         and etd.date_ = si_399005.date_
                                                                                                         and etd.date_ = si_399006.date_
                                                                                                         and*/
           etd.date_ = stda.date_;
        
          -- 更新记录
          update etf_transaction_data t
             set /*t.correlation5_with000001   = v_correlation5_with000001,
                 t.correlation5_with399001   = v_correlation5_with399001,
                 t.correlation5_with399005   = v_correlation5_with399005,
                 t.correlation5_with399006   = v_correlation5_with399006,*/ t.correlation250_with_avg_c_p = v_correlation250_with_avg_c_p
           where t.code_ = v_code
             and t.date_ = j.date_;
        else
          exit;
        end if;
      
      end loop;
      commit;
    end loop;
  end WRITE_CORRELATION250;

  /*------------------------ 增量地计算correlation250_with***列的值 --------------------*/
  procedure WRITE_CORRELATION250_BY_DATE(p_begin_date varchar2,
                                         p_end_date   varchar2) as
    -- 表示code_字段
    v_code varchar2(100);
    -- 相关系数
    v_correlation5_with000001     number;
    v_correlation5_with399001     number;
    v_correlation5_with399005     number;
    v_correlation5_with399006     number;
    v_correlation250_with_avg_c_p number;
    -- etf记录数
    v_etf_count number;
    -- 计算etf_transaction_data表的相关系数的开始时间
    v_etf_begin_date date;
    -- 查询所有etf的code_
    cursor cur_distinct_etf_code is
      select t.code_ code_, ei.name_, avg(t.turnover)
        from ETF_TRANSACTION_DATA t
        join etf_info ei
          on substr(ei.code_, 3, 8) = t.code_
         and ei.available = 1
         and ei.tendency_type = 2
       group by t.code_, ei.name_
      having avg(t.turnover) > 1000
       order by avg(t.turnover) desc;
    -- 根据code_字段，查询某个etf的全部记录，并按照降序排列
    cursor cur_single_etf_desc is
      select *
        from etf_transaction_data t
       where t.code_ = v_code
         and t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
       order by t.date_ desc;
  begin
    for i in cur_distinct_etf_code loop
      v_code := i.code_;
    
      for j in cur_single_etf_desc loop
      
        -- 计算etf_transaction_data表的相关系数的开始时间和记录数
        select min(t2.date_), count(*)
          into v_etf_begin_date, v_etf_count
          from (select *
                  from etf_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= j.date_
                 order by t.date_ desc) t2
         where rownum <= 250;
      
        -- 如果有250条记录匹配
        if v_etf_count >= 250 then
          -- 计算相关系数
          SELECT /*CORR(etd.close_price, si_000001.close_price),
                                                                                                             CORR(etd.close_price, si_399001.close_price),
                                                                                                             CORR(etd.close_price, si_399005.close_price),
                                                                                                             CORR(etd.close_price, si_399006.close_price),*/
           CORR(etd.close_price, stda.close_price)
            into /*v_correlation5_with000001,
                                                                                                                                                                                             v_correlation5_with399001,
                                                                                                                                                                                             v_correlation5_with399005,
                                                                                                                                                                                             v_correlation5_with399006,*/
                 v_correlation250_with_avg_c_p
            FROM (select *
                    from etf_transaction_data t
                   where t.code_ = v_code
                     and t.date_ between v_etf_begin_date and j.date_
                   order by t.date_ desc) etd,
                 /*(select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 000001
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_000001,
                 (select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 399001
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_399001,
                 (select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 399005
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_399005,
                 (select *
                    from (select *
                            from stock_index t1
                           where t1.code_ = 399006
                             and t1.date_ <= j.date_
                           order by t1.date_ desc) t2
                   where rownum <= 5) si_399006,*/
                 (select t1.date_ date_, avg(t1.close_price) close_price
                    from stock_transaction_data_all t1
                   where t1.date_ between v_etf_begin_date and j.date_
                   group by t1.date_) stda
           where /*etd.date_ = si_000001.date_
                                                                                                         and etd.date_ = si_399001.date_
                                                                                                         and etd.date_ = si_399005.date_
                                                                                                         and etd.date_ = si_399006.date_
                                                                                                         and*/
           etd.date_ = stda.date_;
        
          -- 更新记录
          update etf_transaction_data t
             set /*t.correlation5_with000001   = v_correlation5_with000001,
                 t.correlation5_with399001   = v_correlation5_with399001,
                 t.correlation5_with399005   = v_correlation5_with399005,
                 t.correlation5_with399006   = v_correlation5_with399006,*/ t.correlation250_with_avg_c_p = v_correlation250_with_avg_c_p
           where t.code_ = v_code
             and t.date_ = j.date_;
        else
          exit;
        end if;
      
      end loop;
      commit;
    end loop;
  end WRITE_CORRELATION250_BY_DATE;

end PKG_ETF;
/

