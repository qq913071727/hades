???create or replace package scott.PKG_TOOL is

  --  收集数据库统计信息
  procedure GATHER_DATEBASE_STATISTICS;

  -- 对STOCK_INFO表的数据进行清洗。去掉code_字段前两个字符，如SH、SZ。
  procedure CLEAN_STOCK_INFO_TABLE;

  -- 计算表STOCK_TRANSACTION_DATA的字段last_close_price的值
  procedure FILL_LAST_C_P_IN_S_T_D;

  -- 打印日志到日志文件
  procedure DEBUG(message in varchar2);

end PKG_TOOL;
/

