???create or replace package scott.PKG_MODEL_RECORD as
  type tp_cur_top_stock_date is ref cursor;

  -- 海量地向表MDL_MACD_GOLD_CROSS中插入数据
  procedure CAL_MDL_MACD_GOLD_CROSS;
  -- 增量地向表MDL_MACD_GOLD_CROSS中插入数据
  procedure CAL_MDL_MACD_GOLD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_MACD_DEAD_CROSS中插入数据
  procedure CAL_MDL_MACD_DEAD_CROSS;
  -- 增量地向表MDL_MACD_DEAD_CROSS中插入数据
  procedure CAL_MDL_MACD_DEAD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
  procedure CAL_MDL_C_P_MA5_GOLD_CROSS;
  -- 增量地向表MDL_CLOSE_PRICE_MA5_GOLD_CROSS中插入数据
  procedure CAL_MDL_C_P_MA5_G_C_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
  procedure CAL_MDL_C_P_MA5_DEAD_CROSS;
  -- 增量地向表MDL_CLOSE_PRICE_MA5_DEAD_CROSS中插入数据
  procedure CAL_MDL_C_P_MA5_D_C_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_HEI_KIN_ASHI_UP_DOWN中插入数据
  procedure CAL_MDL_HEI_KIN_ASHI_UP_DOWN;
  -- 增量地向表MDL_HEI_KIN_ASHI_UP_DOWN中插入数据
  procedure CAL_MDL_H_K_A_UP_DOWN_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_HEI_KIN_ASHI_DOWN_UP中插入数据
  procedure CAL_MDL_HEI_KIN_ASHI_DOWN_UP;
  -- 增量地向表MDL_HEI_KIN_ASHI_DOWN_UP中插入数据
  procedure CAL_MDL_H_K_A_DOWN_UP_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_KD_GOLD_CROSS中插入数据
  procedure CAL_MDL_KD_GOLD_CROSS;
  -- 增量地向表MDL_KD_GOLD_CROSS中插入数据
  procedure CAL_MDL_KD_GOLD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_KD_DEAD_CROSS中插入数据
  procedure CAL_MDL_KD_DEAD_CROSS;
  -- 增量地向表MDL_KD_DEAD_CROSS中插入数据
  procedure CAL_MDL_KD_DEAD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_UP_DOWN_PERCENTAGE_MA_GOLD_CROSS中插入数据
  procedure CAL_MDL_PERCENTAGE_MA_G_C;

  -- 海量地向表MDL_BULL_SHORT_LINE_UP中插入数据
  procedure CAL_MDL_BULL_SHORT_LINE_UP;
  -- 增量地向表MDL_BULL_SHORT_LINE_UP中插入数据
  procedure CAL_MDL_B_S_LINE_UP_INCR(p_date in varchar2);

  -- 海量地向表MDL_WEEK_KD_GOLD_CROSS中插入数据
  procedure CAL_MDL_WEEK_KD_GOLD_CROSS;

  -- 海量地向表MDL_WEEK_ALL_GOLD_CROSS中插入数据
  procedure CAL_MDL_WEEK_ALL_GOLD_CROSS(p_type in varchar2);

  -- 海量地向表MDL_VOLUME_TURNOVER_UP_MA250中插入数据
  procedure CAL_MDL_V_T_UP_MA250;
  -- 增量地向表MDL_VOLUME_TURNOVER_UP_MA250中插入数据
  procedure CAL_MDL_V_T_UP_MA250_INCR(p_end_date in varchar2);

  -- 海量地计算所有的金叉类型
  -- p_type为120_250表示120日均线金叉250日均线
  procedure CAL_MDL_ALL_GOLD_CROSS(p_type in varchar2);

  -- 海量地向表MDL_TOP_STOCK中插入数据
  procedure WRITE_MDL_TOP_STOCK(p_begin_date        in varchar2,
                                p_end_date          in varchar2,
                                p_number_percentage in number);

  -- 海量地向表MDL_TOP_STOCK_DETAIL中插入数据
  procedure WRITE_MDL_TOP_STOCK_DETAIL(p_begin_date in varchar2,
                                       p_end_date   in varchar2);

  -- 计算表MDL_TOP_STOCK_DETAIL中所有percentage_*_ma*列的值
  procedure WRITE_MDL_TOP_STOCK_DETAIL_MA(p_begin_date in varchar2,
                                          p_end_date   in varchar2);

  -- 海量地向表MDL_TOP_STOCK_DETAIL中插入数据
  /*procedure WRITE_MDL_TOP_STOCK_DETAIL2(p_begin_date        in varchar2,
  p_end_date          in varchar2,
  p_number_percentage in number);*/

  -- 判断临时表是否存在，如果不存在，则创建
  procedure CREATE_TEMP_MDL_TOP_STOCK;

  -- 向临时表TEMP_MDL_TOP_STOCK中插入数据
  procedure INSERT_TEMP_MDL_TOP_STOCK(p_begin_date in varchar2,
                                      p_end_date   in varchar2);

  -- 将视图V_SUB_NEW_STOCK中的内容打印出来
  procedure PRINT_V_SUB_NEW_STOCK;

  -- 查询某段时间内次新股的平均价格
  procedure FIND_SUB_NEW_STOCK_AVG_CLOSE(p_begin_date              in varchar2,
                                         p_end_date                in varchar2,
                                         p_sub_new_stock_avg_close out T_SUBNEWSTOCKAVGCLOSE_ARRAY);

  -- 查找MDL_TOP_STOCK表中版块分布的数据
  procedure FIND_TOP_STOCK_BOARD(p_date in varchar2);

  -- 查找某段时间内MACD金叉算法的成功率
  procedure FIND_MACD_G_C_SUCCESS_RATE(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_number             in number,
                                       p_macd_success_rate_array out T_MACD_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内close_price金叉ma5算法的成功率
  procedure FIND_C_P_MA5_G_C_SUCCESS_RATE(p_begin_date                 in varchar2,
                                          p_end_date                   in varchar2,
                                          p_date_number                in number,
                                          p_c_p_ma5_success_rate_array out T_C_P_MA5_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内ha_close_price上涨趋势算法的成功率
  procedure FIND_H_K_A_UPDOWN_SUCCESS_RATE(p_begin_date               in varchar2,
                                           p_end_date                 in varchar2,
                                           p_date_number              in number,
                                           p_h_k_a_success_rate_array out T_H_K_A_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内日线级别kd金叉算法的成功率
  procedure FIND_KD_G_C_SUCCESS_RATE(p_begin_date            in varchar2,
                                     p_end_date              in varchar2,
                                     p_date_number           in number,
                                     p_kd_success_rate_array out T_KD_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内MACD死叉算法的成功率
  procedure FIND_MACD_D_C_SUCCESS_RATE(p_begin_date              in varchar2,
                                       p_end_date                in varchar2,
                                       p_date_number             in number,
                                       p_macd_success_rate_array out T_MACD_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内close_price死叉ma5算法的成功率
  procedure FIND_C_P_MA5_D_C_SUCCESS_RATE(p_begin_date                 in varchar2,
                                          p_end_date                   in varchar2,
                                          p_date_number                in number,
                                          p_c_p_ma5_success_rate_array out T_C_P_MA5_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内ha_close_price下跌趋势算法的成功率
  procedure FIND_H_K_A_DOWNUP_SUCCESS_RATE(p_begin_date               in varchar2,
                                           p_end_date                 in varchar2,
                                           p_date_number              in number,
                                           p_h_k_a_success_rate_array out T_H_K_A_SUCCESS_RATE_ARRAY);

  -- 查找某段时间内日线级别kd金叉算法的成功率
  procedure FIND_KD_D_C_SUCCESS_RATE(p_begin_date            in varchar2,
                                     p_end_date              in varchar2,
                                     p_date_number           in number,
                                     p_kd_success_rate_array out T_KD_SUCCESS_RATE_ARRAY);

end PKG_MODEL_RECORD;
/

