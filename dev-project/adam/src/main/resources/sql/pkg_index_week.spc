???create or replace package scott.PKG_INDEX_WEEK is
  -- 计算表STOCK_INDEX_WEEK的所有记录
  procedure CALCULATE_STOCK_INDEX_WEEK;

  -- 计算表STOCK_INDEX_WEEK的所有Hei Kin Ashi字段
  procedure CALCULATE_STOCK_INDEX_WEEK_HA;

  -- 按照日期，计算表STOCK_INDEX_WEEK的所有记录
  procedure CAL_STOCK_INDEX_WEEK_BY_DATE(p_begin_date in varchar2,
                                         p_end_date   in varchar2);

  -- 按照日期，计算表STOCK_INDEX_WEEK的Hei Kin Ashi记录
  procedure CAL_STOCKINDEXWEEK_HA_BY_DATE(p_begin_date in varchar2,
                                          p_end_date   in varchar2);

end PKG_INDEX_WEEK;
/

