???create or replace function scott.FNC_STOCK_STOP(p_stock_code in varchar2,
                                          p_begin_date in varchar2,
                                          p_end_date   in varchar2)
    return number is
    /* 通常某只股票在长期停牌后，一旦复牌会出现连续上涨的情况。此时表现为一字涨停板和极低的成交量。
    * 而这两种情况使得很难进行买卖交易，因此应该放弃。
    * 这个函数的作用是判断某只股票在某个交易时间内是否有停牌的情况。如果有，返回1；否则，返回-1
    * 判断标准是某段时间内某只股票的交易次数是否与某段时间内市场上的交易天数相等
    * 这个函数通常与函数FNC_STOCK_WORD_LIMIT一起使用 */
    result number;
    -- 表示某段时间内某只股票的交易次数
    v_stock_num number;
    -- 表示某段时间内市场上的交易天数
    v_stock_record_by_date number;
begin
    -- 计算某段时间内某只股票的交易次数
    select count(distinct t.stock_date)
      into v_stock_num
      from stock_moving_average t
     where t.stock_date between to_date(p_begin_date, 'yyyy-mm-dd') and
           to_date(p_end_date, 'yyyy-mm-dd')
       and t.stock_code = p_stock_code;

    -- 计算某段时间内市场上的交易天数
    select count(distinct t.stock_date)
      into v_stock_record_by_date
      from stock_moving_average t
     where t.stock_date between to_date(p_begin_date, 'yyyy-mm-dd') and
           to_date(p_end_date, 'yyyy-mm-dd');

    -- 如果某段时间内某只股票的交易次数不等于某段时间内市场上的交易天数，则表示这只股票在这段时间内有停牌的情况，返回1；
    -- 否则返回-1.
    if v_stock_num <> v_stock_record_by_date then
        result := 1;
    else
        result := -1;
    end if;

    return(result);
end FNC_STOCK_STOP;
/

