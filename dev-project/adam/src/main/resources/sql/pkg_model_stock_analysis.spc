???create or replace package scott.PKG_MODEL_STOCK_ANALYSIS is

  -- 计算model_stock_analysis表的全部数据
  procedure write_mdl_stock_analysis;

  -- 计算model_stock_analysis表再某一日的数据
  procedure write_mdl_s_a_by_date(p_date in varchar2);

  -- 计算model_stock_week_analysis表的全部数据
  procedure write_mdl_stock_week_analysis;

  -- 计算model_stock_week_analysis表再某一周的数据
  procedure write_mdl_s_w_a_by_date(p_begin_date in varchar2,
                                    p_end_date   in varchar2);

  -- 计算model_stock_month_analysis表的全部数据
  procedure write_mdl_stock_month_analysis;

  -- 计算model_stock_month_analysis表再某一月的数据
  procedure write_mdl_s_m_a_by_date(p_begin_date in varchar2,
                                    p_end_date   in varchar2);

end PKG_MODEL_STOCK_ANALYSIS;
/

