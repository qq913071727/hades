???create or replace package scott.pkg_moving_average is
       -- 计算简单移动平均线
       PROCEDURE CALCULATE_FIVE;
       PROCEDURE CALCULATE_TEN;
       PROCEDURE CALCULATE_TWENTY;
       PROCEDURE CALCULATE_SIXTY;
       PROCEDURE CALCULATE_ONEHUNDREDTWENTY;
       --PROCEDURE CALCULATE_INFINITE;

       -- 插入股票记录
       procedure WRITE_STOCKS(stockDate in date, code in nvarchar2, stockOpen in number,
       stockHigh in number, stockClose in number, stockLow in number, stockAmount in number);

       -- 计算某一天所有股票的简单移动平均线
       PROCEDURE WRITE_MOVING_AVERAGE_BY_DATE(stockDate in varchar2);

       -- 计算某一日有哪些股票处于多头排列
       procedure SELECT_BULL_RANK_STOCK(stockDate in varchar2,num out number);

       TYPE MYCURSOR IS REF CURSOR;
       PROCEDURE TEST(stockDate in varchar2);

       -- 选择股票
       procedure SELECT_REVERSE_STOCK(stockDate in varchar2,percentage in number,resultStockCode out T_TYPE_ARRAY);
       procedure SELECT_STOCK_3(stockDate in varchar2,percentage in number);
       procedure SELECT_MACD_UP_BELOW_ZERO(stockDate in varchar2,rate in number,stockMacdResult out T_TYPE_ARRAY);

       -- 计算所有股票当天的收盘价是否大于前一天的收盘价
       procedure WRITE_UP_DOWN;
       procedure WRITE_UP_DOWN_BY_DATE(stockDate in varchar2);

       -- 计算MACD
       procedure WRITE_MACD_INIT;
       procedure WRITE_MACD_EMA;
       procedure WRITE_MACD_DIF;
       procedure WRITE_MACD_DEA;
       procedure WRITE_MACD_EMA_BY_DATE(stockDate in varchar2);
       procedure WRITE_MACD_DIF_BY_DATE(stockDate in varchar2);
       procedure WRITE_MACD_DEA_BY_DATE(stockDate in varchar2);

       procedure CALCULATE_UP_DOWN_PERCENTAGE;
       procedure CALCULATE_VOLATILITY;
       procedure CALCULATE_CORRELATION_RATE(stockCode in varchar2);
       procedure CAL_UP_DOWN_PERCENTAGE_BY_DATE(stockDate in varchar2);
       procedure CAL_VOLATILITY_BY_DATE(stockDate in varchar2);

       procedure FIND_ABNORMAL_STOCK(stockDate in varchar2,rate in number,stockResultArray out T_STOCK_RESULT_ARRAY);

end pkg_moving_average;
/

