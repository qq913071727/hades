???create or replace package scott.PKG_C_F_WEEK_CONTRACT_DATA is

  -- 计算期货周线级别的基础数据
  procedure WRITE_COMMODITY_FUTURE_WEEK;

  -- 根据日期，计算周线级别的基础数据
  procedure WRITE_WEEK_BY_DATE(p_begin_date varchar2, p_end_date varchar2);

  -- 计算简单移动平均线
  PROCEDURE WRITE_MA5;
  PROCEDURE WRITE_MA10;
  PROCEDURE WRITE_MA20;
  PROCEDURE WRITE_MA60;
  PROCEDURE WRITE_MA120;
  PROCEDURE WRITE_MA250;

  -- 计算某一个周，所有期货的简单移动平均线
  PROCEDURE WRITE_MA_BY_DATE(p_current_week_begin_date in varchar2,
                             p_current_week_end_date   in varchar2);

  -- 计算KD
  procedure WRITE_WEEK_KD_INIT;
  procedure WRITE_WEEK_KD_RSV;
  procedure WRITE_WEEK_KD_K;
  procedure WRITE_WEEK_KD_D;

  -- 算某一个周的KD
  procedure WRITE_WEEK_KD_BY_DATE_RSV(p_begin_date varchar2,
                                      p_end_date   varchar2);
  procedure WRITE_WEEK_KD_BY_DATE_K(p_begin_date varchar2,
                                    p_end_date   varchar2);
  procedure WRITE_WEEK_KD_BY_DATE_D(p_begin_date varchar2,
                                    p_end_date   varchar2);

  -- 计算MACD
  procedure WRITE_WEEK_MACD_INIT;
  procedure WRITE_WEEK_MACD_EMA;
  procedure WRITE_WEEK_MACD_DIF;
  procedure WRITE_WEEK_MACD_DEA;
  procedure WRITE_WEEK_MACD;
  procedure WRITE_WEEK_MACD_EMA_BY_DATE(p_week_begin_date in varchar2,
                                        p_week_end_date   in varchar2);
  procedure WRITE_WEEK_MACD_DIF_BY_DATE(p_week_begin_date in varchar2,
                                        p_week_end_date   in varchar2);
  procedure WRITE_WEEK_MACD_DEA_BY_DATE(p_week_begin_date in varchar2,
                                        p_week_end_date   in varchar2);
  procedure WRITE_WEEK_MACD_BY_DATE(p_week_begin_date in varchar2,
                                    p_week_end_date   in varchar2);

  /*---------------- 计算commodity_future_week_data表中的布林带 ------------------*/
  procedure CAL_BOLL;

  /*---- 计算某一周commodity_future_week_data表中的布林带，必须在计算完均线后运行 ----*/
  procedure CAL_BOLL_BY_DATE(p_begin_date varchar2, p_end_date varchar2);

  -- 计算表commodity_future_week_data的所有Hei Kin Ashi字段
  procedure CAL_WEEK_HA;

  -- 按照日期，计算表commodity_future_week_data的Hei Kin Ashi记录
  procedure CAL_WEEK_HA_BY_DATE(p_begin_date in varchar2,
                                p_end_date   in varchar2);

end PKG_C_F_WEEK_CONTRACT_DATA;
/

