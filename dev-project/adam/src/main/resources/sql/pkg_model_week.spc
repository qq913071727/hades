???create or replace package scott.PKG_MODEL_WEEK is

  /*------------------- 计算周线级别布林带跌破下轨后，MACD金叉的算法 ---------------------*/
  procedure CAL_MDL_WEEK_BOLL_MACD_G_C;

  /*--------------- 计算周线级别布林带跌破下轨后，close_price金叉ma5的算法 -----------------*/
  procedure CAL_MDL_WEEK_BOLL_C_P_MA5_G_C;

  /*--------------- 计算周线级别布林带跌破下轨后，hei_kin_ashi上升趋势的算法 ---------------*/
  procedure CAL_MDL_WEEK_BOLL_H_K_A_U_D;

  /*--------------- 计算周线级别布林带跌破下轨后，kd金叉的算法 ---------------*/
  procedure CAL_MDL_WEEK_BOLL_KD_G_C;

  /*------------------- 计算周线级别布林带跌破下轨后，MACD死叉的算法 ---------------------*/
  procedure CAL_MDL_WEEK_BOLL_MACD_D_C;

  /*--------------- 计算周线级别布林带跌破下轨后，close_price死叉ma5的算法 -----------------*/
  procedure CAL_MDL_WEEK_BOLL_C_P_MA5_D_C;

  /*--------------- 计算周线级别布林带跌破下轨后，hei_kin_ashi下降趋势的算法 ---------------*/
  procedure CAL_MDL_WEEK_BOLL_H_K_A_D_U;

  /*--------------- 计算周线级别布林带跌破下轨后，kd死叉的算法 ---------------*/
  procedure CAL_MDL_WEEK_BOLL_KD_D_C;

  /*-- 查询周线级别最高价突破布林带上轨的百分比和MACD死叉交易收益率的散点图，
  x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨，
  全称find_week_heighest_price_up_boll_percent_and_macd_dead_cross_profit --*/
  procedure find_w_h_p_u_b_p_a_macd_d_c_p(p_begin_date          in varchar2,
                                          p_end_date            in varchar2,
                                          p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最低价跌破布林带下轨的百分比和MACD金叉交易收益率的散点图，
  x轴是收益率，y轴是(前一周最低价-布林带下轨)/布林带下轨，
  全称find_week_lowest_price_dn_boll_percent_and_macd_gold_cross_profit --*/
  procedure find_w_l_p_d_b_p_a_macd_g_c_p(p_begin_date          in varchar2,
                                          p_end_date            in varchar2,
                                          p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最高价突破布林带上轨的百分比和close_price死叉ma5交易收益率的散点图，
  x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨，
  全称find_week_heighest_price_up_boll_percent_and_close_price_dead_cross_ma5_profit --*/
  procedure find_w_h_p_u_b_p_a_c_p_d_c_5_p(p_begin_date          in varchar2,
                                           p_end_date            in varchar2,
                                           p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最低价跌破布林带下轨的百分比和close_price金叉ma5交易收益率的散点图，
  x轴是收益率，y轴是(前一周最低价-布林带下轨)/布林带下轨，
  全称find_week_lowest_price_dn_boll_percent_and_close_price_gold_cross_ma5_profit --*/
  procedure find_w_l_p_d_b_p_a_c_p_g_c_5_p(p_begin_date          in varchar2,
                                           p_end_date            in varchar2,
                                           p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最高价突破布林带上轨的百分比和hei_kin_ashi下跌趋势交易收益率的散点图，
  x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨，
  全称find_week_heighest_price_up_boll_percent_and_hei_kin_ashi_down_up_profit --*/
  procedure find_w_h_p_u_b_p_a_h_k_a_d_u_p(p_begin_date          in varchar2,
                                           p_end_date            in varchar2,
                                           p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最低价跌破布林带下轨的百分比和hei_kin_ashi上涨趋势交易收益率的散点图，
  x轴是收益率，y轴是(前一周最低价-布林带下轨)/布林带下轨，
  全称find_week_lowest_price_dn_boll_percent_and_hei_kin_ashi_up_down_profit --*/
  procedure find_w_l_p_d_b_p_a_h_k_a_u_d_p(p_begin_date          in varchar2,
                                           p_end_date            in varchar2,
                                           p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最高价突破布林带上轨的百分比和KD死叉交易收益率的散点图，
  x轴是收益率，y轴是(前一周最高价-布林带上轨)/布林带上轨，
  全称find_week_heighest_price_up_boll_percent_and_kd_dead_cross_profit --*/
  procedure find_w_h_p_u_b_p_a_kd_d_c_p(p_begin_date          in varchar2,
                                        p_end_date            in varchar2,
                                        p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);

  /*-- 查询周线级别最低价跌破布林带下轨的百分比和KD金叉交易收益率的散点图，
  x轴是收益率，y轴是(前一周最低价-布林带下轨)/布林带下轨，
  全称find_week_lowest_price_dn_boll_percent_and_kd_gold_cross_profit --*/
  procedure find_w_l_p_d_b_p_a_kd_g_c_p(p_begin_date          in varchar2,
                                        p_end_date            in varchar2,
                                        p_t_w_b_u_p_l_p_array out T_WEEK_BOLL_U_D_P_L_P_ARRAY);
end PKG_MODEL_WEEK;
/

