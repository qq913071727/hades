???CREATE OR REPLACE PROCEDURE SCOTT.asynchronized(i_tjrq IN NUMBER, x out number, y out number) is

BEGIN
  --SYS.DBMS_OUTPUT.put_line(m);
  sys.DBMS_JOB.submit(job       => x,
                      what      => 'begin
                                   pkg_stock_transaction_data.WRITE_MOVING_AVERAGE_BY_DATE(20210122);
                                   end;',
                      next_date => TO_DATE('24-01-2021 23:18:00', 'dd-mm-yyyy hh24:mi:ss')
                      -- interval  => 'trunc(sysdate+1)',
                      -- no_parse  => FALSE
                      );

  sys.DBMS_JOB.submit(job       => y,
                      what      => 'begin
                                   pkg_stock_transaction_data.WRITE_MACD_EMA_BY_DATE(20210122);
                                   end;',
                      next_date => TO_DATE('24-01-2021 23:18:00', 'dd-mm-yyyy hh24:mi:ss')
                      -- interval  => 'trunc(sysdate+1)',
                      -- no_parse  => FALSE
                      );
  COMMIT;

  /*sys.DBMS_JOB.submit(job       => x,
                      what      => 'for i in  10001 .. 100000 loop
    insert into model(id, name) values (i, i);
    commit;
  end loop;'
                      -- next_date => TO_DATE('24-01-2021 22:50:00', 'dd-mm-yyyy hh24:mi:ss')
                      -- interval  => 'trunc(sysdate+1)',
                      -- no_parse  => FALSE
                      );

  sys.DBMS_JOB.submit(job       => y,
                      what      => 'for i in  1 .. 10000 loop
    insert into model(id, name) values (i, i);
    commit;
  end loop;'
                      -- next_date => TO_DATE('24-01-2021 22:50:00', 'dd-mm-yyyy hh24:mi:ss')
                      -- interval  => 'trunc(sysdate+1)',
                      -- no_parse  => FALSE
                      );
  COMMIT;*/
END;


-- SCOTT.WRITE_MACD_DIF_BY_DATE(20210124);
-- SCOTT.WRITE_MACD_DEA_BY_DATE(20210124);
/

