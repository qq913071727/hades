???create or replace package body scott.PKG_U_R_PRICE_INDEX is

  ---------------------------------- 计算基础数据 ----------------------------------
  procedure write_basic_data is
    -- 交易日期
    v_transaction_date date;
    -- 代码
    v_code varchar(50);
    -- 期货品种数量
    v_count number;
    -- 统一相对价格指数的总数
    v_sum number;
    -- 统一相对价格指数的平均数
    v_avg number;
    -- commodity_future_date_data类型的变量
    row_commodity_future_date_data commodity_future_date_data%rowtype;
    -- 所有交易日期
    cursor cur_transaction_date is
      select distinct t.transaction_date
        from commodity_future_date_data t
       order by t.transaction_date asc;
    -- 所有期货信息
    cursor cur_commodity_future_info is
      select * from commodity_future_info;
  begin
    for x in cur_transaction_date loop
      v_transaction_date := x.transaction_date;
      v_count            := 0;
      v_sum              := 0;
      v_avg              := 0;

      for y in cur_commodity_future_info loop
        v_code := y.code;

        -- 某个交易日，某个期货的所有记录
        begin
          select *
            into row_commodity_future_date_data
            from commodity_future_date_data t
           where t.transaction_date = v_transaction_date
             and t.code = v_code;
        exception
          when no_data_found then
            -- 如果当前交易日没有交易记录，则先查询上一个交易日的记录，如果还没有则说明还没有上市交易
            begin
              select *
                into row_commodity_future_date_data
                from (select *
                        from commodity_future_date_data t
                       where t.code = v_code
                         and t.transaction_date <= v_transaction_date
                       order by t.transaction_date desc)
               where rownum <= 1;
            exception
              when no_data_found then
                continue;
            end;
        end;

        -- 计算指数的总数
        v_sum := v_sum +
                 row_commodity_future_date_data.unite_relative_price_index;
        -- 计算总数量
        v_count := v_count + 1;

      end loop;

      -- 计算平均值
      v_avg := v_sum / v_count;

      -- 插入记录
      insert into unite_relative_price_index_d
        (CODE, TRANSACTION_DATE, NAME, CLOSE_PRICE)
      values
        ('URPID',
         v_transaction_date,
         CONVERT('统一相对价格指数（日线级别）', 'UTF8'),
         v_avg);

    end loop;
    commit;
  end write_basic_data;

  ---------------------------------- 计算某一天的基础数据 ----------------------------------
  procedure write_basic_data_by_date(p_date in varchar2) is
    -- 代码
    v_code varchar(50);
    -- 期货品种数量
    v_count number := 0;
    -- 统一相对价格指数的总数
    v_sum number := 0;
    -- 统一相对价格指数的平均数
    v_avg number := 0;
    -- commodity_future_date_data类型的变量
    row_commodity_future_date_data commodity_future_date_data%rowtype;
    -- 所有期货信息
    cursor cur_commodity_future_info is
      select * from commodity_future_info;
  begin

    for y in cur_commodity_future_info loop
      v_code := y.code;
      -- 某个交易日，某个期货的所有记录
      begin
        select *
          into row_commodity_future_date_data
          from commodity_future_date_data t
         where t.transaction_date = to_date(p_date, 'yyyy-mm-dd')
           and t.code = v_code;
      exception
        when no_data_found then
          -- 如果当前交易日没有交易记录，则先查询上一个交易日的记录，如果还没有则说明还没有上市交易
          begin
            select *
              into row_commodity_future_date_data
              from (select *
                      from commodity_future_date_data t
                     where t.code = v_code
                       and t.transaction_date <=
                           to_date(p_date, 'yyyy-mm-dd')
                     order by t.transaction_date desc)
             where rownum <= 1;
          exception
            when no_data_found then
              continue;
          end;
      end;

      -- 计算指数的总数
      v_sum := v_sum +
               row_commodity_future_date_data.unite_relative_price_index;
      -- 计算总数量
      v_count := v_count + 1;

    end loop;

    -- 计算平均值
    v_avg := v_sum / v_count;

    -- 插入记录
    insert into unite_relative_price_index_d
      (CODE, TRANSACTION_DATE, NAME, CLOSE_PRICE)
    values
      ('URPID',
       to_date(p_date, 'yyyy-mm-dd'),
       '统一相对价格指数（日线级别）',
       v_avg);
    commit;
  end write_basic_data_by_date;

end PKG_U_R_PRICE_INDEX;
/

