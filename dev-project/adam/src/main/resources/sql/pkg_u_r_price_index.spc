???create or replace package scott.PKG_U_R_PRICE_INDEX is

  -- 计算基础数据
  procedure write_basic_data;
  -- 计算某一天的基础数据
  procedure write_basic_data_by_date(p_date in varchar2);

end PKG_U_R_PRICE_INDEX;
/

