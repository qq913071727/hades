???create or replace package scott.PKG_MODEL_COMMODITY_FUTURE is

  -- 海量地向表MDL_C_F_DATE_MACD_GOLD_CROSS中插入数据
  procedure CAL_MDL_MACD_GOLD_CROSS;
  -- 增量地向表MDL_C_F_DATE_MACD_GOLD_CROSS中插入数据
  procedure CAL_MDL_MACD_GOLD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_MACD_DEAD_CROSS中插入数据
  procedure CAL_MDL_MACD_DEAD_CROSS;
  -- 增量地向表MDL_C_F_DATE_MACD_DEAD_CROSS中插入数据
  procedure CAL_MDL_MACD_DEAD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_C_P_MA_G_C中插入数据
  procedure CAL_MDL_CF_DATE_CP_MA_GC;
  -- 增量地向表MDL_C_F_DATE_C_P_MA_G_C中插入数据
  procedure CAL_MDL_CF_DATE_CP_MA_GC_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_C_P_MA_D_C中插入数据
  procedure CAL_MDL_CF_DATE_CP_MA_DC;
  -- 增量地向表MDL_C_F_DATE_C_P_MA_D_C中插入数据
  procedure CAL_MDL_CF_DATE_CP_MA_DC_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_HEI_KIN_ASHI_UP中插入数据
  procedure CAL_MDL_C_F_DATE_HKA_UP;
  -- 增量地向表MDL_C_F_DATE_HEI_KIN_ASHI_UP中插入数据
  procedure CAL_MDL_C_F_DATE_HKA_UP_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_HEI_KIN_ASHI_DOWN中插入数据
  procedure CAL_MDL_C_F_DATE_HKA_DOWN;
  -- 增量地向表MDL_C_F_DATE_HEI_KIN_ASHI_DOWN中插入数据
  procedure CAL_MDL_C_F_DATE_HKA_DOWN_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_KD_GOLD_CROSS中插入数据
  procedure CAL_MDL_C_F_KD_GOLD_CROSS;
  -- 增量地向表MDL_C_F_DATE_KD_GOLD_CROSS中插入数据
  procedure CAL_MDL_C_F_KD_GOLD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_KD_DEAD_CROSS中插入数据
  procedure CAL_MDL_C_F_KD_DEAD_CROSS;
  -- 增量地向表MDL_C_F_KD_DEAD_CROSS中插入数据
  procedure CAL_MDL_C_F_KD_DEAD_CROSS_INCR(p_end_date in varchar2);

  -- 海量地向表MDL_C_F_DATE_TURN_POINT_UP中插入数据
  procedure CAL_MDL_C_F_DATE_TURN_POINT_UP;

end PKG_MODEL_COMMODITY_FUTURE;
/

