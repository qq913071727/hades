???CREATE OR REPLACE PACKAGE BODY SCOTT.pkg_stock_transaction_data as

  /***********************************************************************************************************************************
  
                                                              procedure
  
  ***********************************************************************************************************************************/

  /* ----------------------------- insert the records of stocks ------------------------------------------------ */
  -- 这个存储过程不再使用了
  procedure WRITE_STOCK_TRANSACTION_DATA(p_date                     in date,
                                         p_code                     in varchar2,
                                         p_open_price               in number,
                                         p_close_price              in number,
                                         p_highest_price            in number,
                                         p_lowest_price             in number,
                                         p_change_amount            in number,
                                         p_change_range             in number,
                                         p_turnover_rate            in number,
                                         p_volume                   in number,
                                         p_turnover                 in number,
                                         p_total_market_value       in number,
                                         p_circulation_market_value in number) is
  begin
    INSERT into STOCK_TRANSACTION_DATA
      (date_,
       code_,
       open_price,
       close_price,
       highest_price,
       lowest_price,
       change_amount,
       change_range,
       turnover_rate,
       volume,
       turnover,
       total_market_value,
       circulation_market_value)
    values
      (p_date,
       p_code,
       p_open_price,
       p_close_price,
       p_highest_price,
       p_lowest_price,
       p_change_amount,
       p_change_range,
       p_turnover_rate,
       p_volume,
       p_turnover,
       p_total_market_value,
       p_circulation_market_value);
  end WRITE_STOCK_TRANSACTION_DATA;

  /*---------------------------------------------------- calculate five moving average -------------------------------------------------*/
  procedure CALCULATE_FIVE is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示5天收盘价的和
      v_five_sum number := 0;
      -- 表示5天收盘价的平均值
      v_five_average number := 0;
      -- 定义一个含有5个数值型数据的数组
      type type_array is varray(5) of number;
      array_five type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_close_price is
        select std.close_price, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_five := type_array();
        for j in cur_all_stock_close_price loop
          array_five.extend; -- 扩展数组，扩展一个元素
          array_five(array_five.count) := j.close_price;
          if mod(array_five.count, 5) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_five_sum := 0;
            for x in 1 .. array_five.count loop
              -- 求5天收盘价的和
              v_five_sum := v_five_sum + array_five(x);
            end loop;
            -- dbms_output.put_line('v_five_sum'||'  :  '||v_five_sum);
            -- 删除数组中的第一个元素，将其与4个元素向前挪一位，并删除下标为5的元素
            for y in 1 .. array_five.count - 1 loop
              array_five(y) := array_five(y + 1);
            end loop;
            array_five.trim;
            -- 5天收盘价的平均值
            v_five_average := v_five_sum / 5;
            -- 向所有记录的FIVE列插入5天收盘价的平均值
            update stock_transaction_data std
               set std.ma5 = round(v_five_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_five_average'||'  :  '||v_five_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CALCULATE_FIVE;
  -- end procedure

  /*---------------------------------------------------- calculate ten moving average -------------------------------------------------*/
  procedure CALCULATE_TEN is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示10天收盘价的和
      v_ten_sum number := 0;
      -- 表示10天收盘价的平均值
      v_ten_average number := 0;
      -- 定义一个含有10个数值型数据的数组
      type type_array is varray(10) of number;
      array_ten type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_close_price is
        select std.close_price, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_ten := type_array();
        for j in cur_all_stock_close_price loop
          array_ten.extend; -- 扩展数组，扩展一个元素
          array_ten(array_ten.count) := j.close_price;
          if mod(array_ten.count, 10) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_ten_sum := 0;
            for x in 1 .. array_ten.count loop
              -- 求10天收盘价的和
              v_ten_sum := v_ten_sum + array_ten(x);
            end loop;
            -- dbms_output.put_line('ten_sum'||'  :  '||v_ten_sum);
            -- 删除数组中的第一个元素，将其与9个元素向前挪一位，并删除下标为10的元素
            for y in 1 .. array_ten.count - 1 loop
              array_ten(y) := array_ten(y + 1);
            end loop;
            array_ten.trim;
            -- 10天收盘价的平均值
            v_ten_average := v_ten_sum / 10;
            -- 向所有记录的FIVE列插入10天收盘价的平均值
            update stock_transaction_data std
               set std.ma10 = round(v_ten_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_ten_average'||'  :  '||v_ten_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CALCULATE_TEN;
  -- end procedure

  /*---------------------------------------------------- calculate twenty moving average -------------------------------------------------*/
  procedure CALCULATE_TWENTY is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示20天收盘价的和
      v_twenty_sum number := 0;
      -- 表示20天收盘价的平均值
      v_twenty_average number := 0;
      -- 定义一个含有20个数值型数据的数组
      type type_array is varray(20) of number;
      array_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_al_sStock_close_price is
        select std.close_price, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_twenty := type_array();
        for j in cur_al_sStock_close_price loop
          array_twenty.extend; -- 扩展数组，扩展一个元素
          array_twenty(array_twenty.count) := j.close_price;
          if mod(array_twenty.count, 20) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_twenty_sum := 0;
            for x in 1 .. array_twenty.count loop
              -- 求20天收盘价的和
              v_twenty_sum := v_twenty_sum + array_twenty(x);
            end loop;
            -- dbms_output.put_line('v_twenty_sum'||'  :  '||v_twenty_sum);
            -- 删除数组中的第一个元素，将其与19个元素向前挪一位，并删除下标为20的元素
            for y in 1 .. array_twenty.count - 1 loop
              array_twenty(y) := array_twenty(y + 1);
            end loop;
            array_twenty.trim;
            -- 20天收盘价的平均值
            v_twenty_average := v_twenty_sum / 20;
            -- 向所有记录的FIVE列插入20天收盘价的平均值
            update stock_transaction_data std
               set std.ma20 = round(v_twenty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_twenty_average'||'  :  '||v_twenty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CALCULATE_TWENTY;
  -- end procedure

  /*---------------------------------------------------- calculate sixty moving average -------------------------------------------------*/
  procedure CALCULATE_SIXTY is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示60天收盘价的和
      v_sixty_sum number := 0;
      -- 表示60天收盘价的平均值
      v_sixty_average number := 0;
      -- 定义一个含有60个数值型数据的数组
      type type_array is varray(60) of number;
      array_sixty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_close_price is
        select std.close_price, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_sixty := type_array();
        for j in cur_all_stock_close_price loop
          array_sixty.extend; -- 扩展数组，扩展一个元素
          array_sixty(array_sixty.count) := j.close_price;
          if mod(array_sixty.count, 60) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_sixty_sum := 0;
            for x in 1 .. array_sixty.count loop
              -- 求60天收盘价的和
              v_sixty_sum := v_sixty_sum + array_sixty(x);
            end loop;
            -- dbms_output.put_line('v_sixty_sum'||'  :  '||v_sixty_sum);
            -- 删除数组中的第一个元素，将其与59个元素向前挪一位，并删除下标为60的元素
            for y in 1 .. array_sixty.count - 1 loop
              array_sixty(y) := array_sixty(y + 1);
            end loop;
            array_sixty.trim;
            -- 60天收盘价的平均值
            v_sixty_average := v_sixty_sum / 60;
            -- 向所有记录的FIVE列插入60天收盘价的平均值
            update stock_transaction_data std
               set std.ma60 = round(v_sixty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_sixty_average'||'  :  '||v_sixty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CALCULATE_SIXTY;
  -- end procedure

  /*---------------------------------------------------- calculate one hundred twenty moving average -------------------------------------------------*/
  procedure CALCULATE_ONEHUNDREDTWENTY is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示120天收盘价的和
      v_one_hundred_twenty_sum number := 0;
      -- 表示120天收盘价的平均值
      v_one_hundred_twenty_average number := 0;
      -- 定义一个含有120个数值型数据的数组
      type type_array is varray(120) of number;
      array_one_hundred_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_close_price is
        select std.close_price, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_one_hundred_twenty := type_array();
        for j in cur_all_stock_close_price loop
          array_one_hundred_twenty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_twenty(array_one_hundred_twenty.count) := j.close_price;
          if mod(array_one_hundred_twenty.count, 120) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_one_hundred_twenty_sum := 0;
            for x in 1 .. array_one_hundred_twenty.count loop
              -- 求120天收盘价的和
              v_one_hundred_twenty_sum := v_one_hundred_twenty_sum +
                                          array_one_hundred_twenty(x);
            end loop;
            -- dbms_output.put_line('v_one_hundred_twenty_sum'||'  :  '||v_one_hundred_twenty_sum);
            -- 删除数组中的第一个元素，将其与119个元素向前挪一位，并删除下标为120的元素
            for y in 1 .. array_one_hundred_twenty.count - 1 loop
              array_one_hundred_twenty(y) := array_one_hundred_twenty(y + 1);
            end loop;
            array_one_hundred_twenty.trim;
            -- 120天收盘价的平均值
            v_one_hundred_twenty_average := v_one_hundred_twenty_sum / 120;
            -- 向所有记录的FIVE列插入120天收盘价的平均值
            update stock_transaction_data std
               set std.ma120 = round(v_one_hundred_twenty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_one_hundred_twenty_average'||'  :  '||v_one_hundred_twenty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CALCULATE_ONEHUNDREDTWENTY;
  -- end procedure

  /*---------------------------------------------------- calculate one hundred fifty moving average -------------------------------------------------*/
  procedure CALCULATE_TWOHUNDREDFIFTY is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示250天收盘价的和
      v_one_hundred_fifty_sum number := 0;
      -- 表示250天收盘价的平均值
      v_one_hundred_fifty_average number := 0;
      -- 定义一个含有250个数值型数据的数组
      type type_array is varray(250) of number;
      array_one_hundred_fifty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_close_price is
        select std.close_price, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_one_hundred_fifty := type_array();
        for j in cur_all_stock_close_price loop
          array_one_hundred_fifty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_fifty(array_one_hundred_fifty.count) := j.close_price;
          if mod(array_one_hundred_fifty.count, 250) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_one_hundred_fifty_sum := 0;
            for x in 1 .. array_one_hundred_fifty.count loop
              -- 求250天收盘价的和
              v_one_hundred_fifty_sum := v_one_hundred_fifty_sum +
                                         array_one_hundred_fifty(x);
            end loop;
            -- dbms_output.put_line('v_one_hundred_fifty_sum'||'  :  '||v_one_hundred_fifty_sum);
            -- 删除数组中的第一个元素，将其与249个元素向前挪一位，并删除下标为250的元素
            for y in 1 .. array_one_hundred_fifty.count - 1 loop
              array_one_hundred_fifty(y) := array_one_hundred_fifty(y + 1);
            end loop;
            array_one_hundred_fifty.trim;
            -- 250天收盘价的平均值
            v_one_hundred_fifty_average := v_one_hundred_fifty_sum / 250;
            -- 向所有记录的FIVE列插入250天收盘价的平均值
            update stock_transaction_data std
               set std.ma250 = round(v_one_hundred_fifty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_one_hundred_fifty_average'||'  :  '||v_one_hundred_fifty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CALCULATE_TWOHUNDREDFIFTY;
  -- end procedure

  /*---------------------------------------------- calculate infinite moving average -----------------------------------------
  procedure CALCULATE_INFINITE is
          begin
            ------------------------------------------ standard declare section ---------------------------------------------------
            declare
            -- 表示stock_code
            stockCode varchar2(10);
            -- 表示所有交易日收盘价的和
            infiniteSum number:=0;
            -- 表示所有交易日收盘价的平均值
            infiniteAverage number:=0;
            -- 定义一个含有无限个数值型数据的数组
            type type_array is table of number;
            arrayInfinite type_array:=type_array();
            -- define cursor section.返回全部stock_code
            cursor allStockCode is select distinct t.stock_code from stock_moving_average t order by t.stock_code asc;
            cursor allStockClose is select t.stock_close,t.stock_date from stock_moving_average t where t.stock_code=stockCode order by t.stock_date asc;
            ------------------------------------------ standard declare section ---------------------------------------------------
  
            begin
              --------------------------------- standard for loop section --------------------------------------------------------
              for i in allStockCode loop
                  stockCode:=i.stock_code;
                  dbms_output.put_line('**************   stockCode'||'  :  '||stockCode);
                  arrayInfinite:=type_array();
                  infiniteSum:=0;
                  infiniteAverage:=0;
                  for j in allStockClose loop
                      arrayInfinite.extend;-- 扩展数组，扩展一个元素
                      arrayInfinite(arrayInfinite.count):=j.stock_close;
                      -- 求所有天收盘价的和
                      infiniteSum:=infiniteSum+arrayInfinite(arrayInfinite.count);
                      dbms_output.put_line('infiniteSum'||'  :  '||infiniteSum);
                      -- 所有天收盘价的平均值
                      infiniteAverage:=infiniteSum/arrayInfinite.count;
                      -- 向所有记录的FIVE列插入120天收盘价的平均值
                      update stock_moving_average t set t.infinite=round(infiniteAverage,2) where t.stock_code=stockCode and t.stock_date=j.stock_date;
                      dbms_output.put_line('infiniteAverage'||'  :  '||infiniteAverage);
                  end loop;
              end loop;
            end;
          end CALCULATE_INFINITE;
          -- end procedure
  */

  /*---------------------------------------------------- write moving average by date -------------------------------------------------*/
  procedure WRITE_MOVING_AVERAGE_BY_DATE(p_date in varchar2) is
    -- 表示stock_code
    v_code varchar2(10);
    -- 表示5天收盘价的平均值
    v_five_average number := 0;
    -- 表示10天收盘价的平均值
    v_ten_average number := 0;
    -- 表示20天收盘价的平均值
    v_twenty_average number := 0;
    -- 表示60天收盘价的平均值
    v_sixty_average number := 0;
    -- 表示120天收盘价的平均值
    v_one_hundred_twenty_average number := 0;
    -- 表示250天收盘价的平均值
    v_two_hundred_fifty_average number := 0;
    -- 表示所有天收盘价的平均值
    --infiniteAverage number:=0;
    -- 用于判断某只股票的记录数是否可以计算均线
    v_average_num number;
    -- 计算某只股票，在某个交易日之前的所有交易记录
    cursor cur_stock_row_num_by_date is
      select t.code_, count(*) row_num
        from stock_transaction_data t
       where t.date_ <= to_date(p_date, 'yyyy-mm-dd')
       group by t.code_;
  begin
    for i in cur_stock_row_num_by_date loop
      v_code        := i.code_;
      v_average_num := i.row_num;
    
      /*select
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=5 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=10 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=20 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=60 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=120 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=250 order by t.stock_date desc))
             into fiveAverage,tenAverage,twentyAverage,sixtyAverage,oneHundredTwentyAverage,twoHundredFiftyAverage
      from dual;
      update stock_moving_average t
      set t.five=round(fiveAverage,2),t.ten=round(tenAverage,2),t.twenty=round(twentyAverage,2),
          t.sixty=round(sixtyAverage,2),t.one_hundred_twenty=round(oneHundredTwentyAverage,2),
          t.two_hundred_fifty=round(twoHundredFiftyAverage,2)
      where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
      commit;*/
    
      -- 每只股票都重置如下变量
      v_five_average               := null;
      v_ten_average                := null;
      v_twenty_average             := null;
      v_sixty_average              := null;
      v_one_hundred_twenty_average := null;
      v_two_hundred_fifty_average  := null;
    
      -- 更新所有股票某一天的5日均线
      if v_average_num >= 5 then
        select avg(d.close_price)
          into v_five_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 5;
      end if;
    
      -- 更新所有股票某一天的10日均线
      if v_average_num >= 10 then
        select avg(d.close_price)
          into v_ten_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 10;
      end if;
    
      -- 更新所有股票某一天的20日均线
      if v_average_num >= 20 then
        select avg(d.close_price)
          into v_twenty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 20;
      end if;
    
      -- 更新所有股票某一天的60日均线
      if v_average_num >= 60 then
        select avg(d.close_price)
          into v_sixty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 60;
      end if;
    
      -- 更新所有股票某一天的120日均线
      if v_average_num >= 120 then
        select avg(d.close_price)
          into v_one_hundred_twenty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 120;
      end if;
    
      -- 更新所有股票某一天的250日均线
      if v_average_num >= 250 then
        select avg(d.close_price)
          into v_two_hundred_fifty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 250;
      end if;
    
      update stock_transaction_data t
         set t.ma5   = nvl2(v_five_average, round(v_five_average, 2), null),
             t.ma10  = nvl2(v_ten_average, round(v_ten_average, 2), null),
             t.ma20  = nvl2(v_twenty_average,
                            round(v_twenty_average, 2),
                            null),
             t.ma60  = nvl2(v_sixty_average, round(v_sixty_average, 2), null),
             t.ma120 = nvl2(v_one_hundred_twenty_average,
                            round(v_one_hundred_twenty_average, 2),
                            null),
             t.ma250 = nvl2(v_two_hundred_fifty_average,
                            round(v_two_hundred_fifty_average, 2),
                            null)
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    
    -- 更新所有股票某一天的所有日均线
    /*select avg(d.stock_close) into infiniteAverage from(
                                                                                                                                                                                                                                                                     select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') order by t.stock_date desc
                                                                                                                                                                                                                                                              ) d;
                                                                                                                                                                                                                                                              update stock_moving_average t set t.infinite=round(infiniteAverage,2) where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
                                                                                                                                                                                                                                                              */
    end loop;
    commit;
  end WRITE_MOVING_AVERAGE_BY_DATE;

  /*procedure WRITE_MOVING_AVERAGE_BY_DATE(p_date in varchar2) is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
    -- 表示stock_code
    v_code varchar2(10);
    -- 表示5天收盘价的平均值
    v_five_average number:=0;
    -- 表示10天收盘价的平均值
    v_ten_average number:=0;
    -- 表示20天收盘价的平均值
    v_twenty_average number:=0;
    -- 表示60天收盘价的平均值
    v_sixty_average number:=0;
    -- 表示120天收盘价的平均值
    v_one_hundred_twenty_average number:=0;
    -- 表示250天收盘价的平均值
    v_two_hundred_fifty_average number:=0;
    -- 表示所有天收盘价的平均值
    --infiniteAverage number:=0;
    -- define cursor section.返回全部code_字段
    cursor cur_all_code is select distinct std.code_ from stock_transaction_data std;
    ------------------------------------------ standard declare section ---------------------------------------------------
  
      begin
      --------------------------------- standard for loop section --------------------------------------------------------
        for i in cur_all_code loop
            v_code:=i.code_;
            -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
  
            -- 更新所有股票某一天的5日均线
            select avg(d.close_price) into v_five_average from(
                   select * from(
                          select * from stock_transaction_data std
                          where std.code_=v_code and std.date_<=to_date(p_date,'yyyy-mm-dd') order by std.date_ desc
                   ) where rownum<=5
            ) d;
            update stock_transaction_data std set std.ma5=round(v_five_average,2) where std.code_=v_code and std.date_=to_date(p_date,'yyyy-mm-dd');
  
            -- 更新所有股票某一天的10日均线
            select avg(d.close_price) into v_ten_average from(
                   select * from(
                          select * from stock_transaction_data std
                          where std.code_=v_code and std.date_<=to_date(p_date,'yyyy-mm-dd') order by std.date_ desc
                   ) where rownum<=10
            ) d;
            update stock_transaction_data std set std.ma10=round(v_ten_average,2) where std.code_=v_code and std.date_=to_date(p_date,'yyyy-mm-dd');
  
            -- 更新所有股票某一天的20日均线
            select avg(d.close_price) into v_twenty_average from(
                   select * from(
                          select * from stock_transaction_data std
                          where std.code_=v_code and std.date_<=to_date(p_date,'yyyy-mm-dd') order by std.date_ desc
                   ) where rownum<=20
            ) d;
            update stock_transaction_data std set std.ma20=round(v_twenty_average,2) where std.code_=v_code and std.date_=to_date(p_date,'yyyy-mm-dd');
  
            -- 更新所有股票某一天的60日均线
            select avg(d.close_price) into v_sixty_average from(
                   select * from(
                          select * from stock_transaction_data std
                          where std.code_=v_code and std.date_<=to_date(p_date,'yyyy-mm-dd') order by std.date_ desc
                   ) where rownum<=60
            ) d;
            update stock_transaction_data std set std.ma60=round(v_sixty_average,2) where std.code_=v_code and std.date_=to_date(p_date,'yyyy-mm-dd');
  
            -- 更新所有股票某一天的120日均线
            select avg(d.close_price) into v_one_hundred_twenty_average from(
                   select * from(
                          select * from stock_transaction_data std
                          where std.code_=v_code and std.date_<=to_date(p_date,'yyyy-mm-dd') order by std.date_ desc
                   ) where rownum<=120
            ) d;
            update stock_transaction_data std set std.ma120=round(v_one_hundred_twenty_average,2) where std.code_=v_code and std.date_=to_date(p_date,'yyyy-mm-dd');
  
            -- 更新所有股票某一天的250日均线
            select avg(d.close_price) into v_two_hundred_fifty_average from(
                   select * from(
                          select * from stock_transaction_data std
                          where std.code_=v_code and std.date_<=to_date(p_date,'yyyy-mm-dd') order by std.date_ desc
                   ) where rownum<=250
            ) d;
            update stock_transaction_data std set std.ma250=round(v_two_hundred_fifty_average,2) where std.code_=v_code and std.date_=to_date(p_date,'yyyy-mm-dd');
  
            -- 更新所有股票某一天的所有日均线
            \*select avg(d.stock_close) into infiniteAverage from(
                   select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') order by t.stock_date desc
            ) d;
            update stock_moving_average t set t.infinite=round(infiniteAverage,2) where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
            *\
        end loop;
    end;
  end WRITE_MOVING_AVERAGE_BY_DATE;*/

  /*------------------------------------------------ select bull rank stock ------------------------------------------------------*/
  procedure SELECT_BULL_RANK_STOCK(p_date in varchar2, p_num out number) as
    -- define cursor section.返回全部在某一日均线成多头排列的股票
    cursor cur_bull_rank_stock_code is
      select distinct *
        from stock_transaction_data t
       where t.close_price >= t.ma5
         and t.close_price >= t.ma10
         and t.close_price >= t.ma20
         and t.close_price >= t.ma60
         and t.close_price >= t.ma120
         and t.ma5 >= t.ma10
         and t.ma5 >= t.ma20
         and t.ma5 >= t.ma60
         and t.ma5 >= t.ma120
         and t.ma10 >= t.ma20
         and t.ma10 >= t.ma60
         and t.ma10 >= t.ma120
         and t.ma20 >= t.ma60
         and t.ma20 >= t.ma120
         and t.ma60 >= t.ma120
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
  begin
    p_num := 0;
    for i in cur_bull_rank_stock_code loop
      dbms_output.put_line('code_   ' || i.code_);
      p_num := p_num + 1;
    end loop;
  end SELECT_BULL_RANK_STOCK;

  /*------------------------------------------------ select stock by three conditions ------------------------------------------------------*/
  procedure SELECT_REVERSE_STOCK(p_date              in varchar2,
                                 p_percentage        in number,
                                 p_result_stock_code out T_TYPE_ARRAY) as
    -- 表示stock_code
    v_code varchar2(10);
    -- 表示某只股票历史上的最高收盘价
    v_max_stock_close number;
    -- 表示当前股票某一日的收盘价
    v_current_stock_close number;
    -- 表示当前股票某一日的120均线价格
    v_current_stock_120 number;
    -- 用于计算某只股票的120均线是否在20个交易日内不是单调递减的
    v_num number;
    -- 定义一个字符型数据的数组，用于存储被选中的股票的代码。TYPE_ARRAY的定义在Type目录中。
    --type_array_stock_code type_array:=type_array(3000);
    -- define cursor section.返回全部在某一日均线成多头排列的股票
    cursor cur_all_stock_code is
      select distinct *
        from stock_transaction_data t
       where t.date_ = to_date(p_date, 'yyyy-mm-dd');
    -- define cursor section.返回全部在某一支股票在某一日之后（包括某一日）的20个交易日内的交易记录
    cursor cur_top_twenty_stock_close is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc) b
       where rownum <= 20;
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  begin
    --------------------------------- standard for loop section --------------------------------------------------------
    file_handle         := UTL_FILE.FOPEN('TXTDIR',
                                          'SELECT_REVERSE_STOCK.txt',
                                          'w');
    p_result_stock_code := t_type_array(51200);
  
    for i in cur_all_stock_code loop
      v_code := i.code_;
      -- 查询某只股票历史上的最高收盘价
      select max(t.close_price)
        into v_max_stock_close
        from stock_transaction_data t
       where t.code_ = v_code;
      -- 查询某只股票在某一日的收盘价格和120均线价格
      select t.close_price, t.ma120
        into v_current_stock_close, v_current_stock_120
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    
      v_num := 0;
      -- 计算某只股票的某一日的收盘价格是否是其历史最高价的百分子：1-percentage
      if (v_max_stock_close - v_current_stock_close) / v_max_stock_close >
         p_percentage then
        for j in cur_top_twenty_stock_close loop
          -- 判断某只股票的120均线是否在20个交易日内不是单调递减的
          if v_current_stock_120 < j.ma120 then
            exit;
          end if;
        
          v_current_stock_120 := j.ma120;
          p_result_stock_code.extend;
          v_num := 1 + v_num;
        
          if v_num = 20 then
            p_result_stock_code(p_result_stock_code.count) := j.code_;
            --dbms_output.put_line('j.stock_code'||'  :  '||j.stock_code);
            UTL_FILE.PUT_LINE(file_handle, j.code_);
          end if;
        end loop;
      end if;
    end loop;
    UTL_FILE.FCLOSE(file_handle);
  end SELECT_REVERSE_STOCK;
  -- end procedure

  /*------------------------------------------------ select stock by three conditions ------------------------------------------------------*/
  /*procedure SELECT_STOCK_3(stockDate in varchar2,percentage in number) as
  begin
      ------------------------------------------ standard declare section ---------------------------------------------------
      declare
      -- 表示stock_code
      stockCode varchar2(10);
      -- 表示某只股票历史上的最高收盘价
      maxStockClose number;
      -- 表示当前股票某一日的收盘价
      currentStockClose number;
      -- 表示当前股票某一日的120均线价格
      currentStockOneHundredTwenty number;
      -- 用于计算某只股票的120均线是否在20个交易日内不是单调递减的
      num number;
      -- 定义一个字符型数据的数组，用于存储被选中的股票的代码。TYPE_ARRAY的定义在Type目录中。
      arrayStockCode t_type_array:=t_type_array();
      -- define cursor section.返回全部在某一日均线成多头排列的股票
      cursor allStockCode is select distinct * from stock_moving_average t
              where t.stock_close>=t.five and t.stock_close>=t.ten and t.stock_close>=t.twenty and t.stock_close>=t.sixty and t.stock_close>=t.one_hundred_twenty
              and t.five>=t.ten and t.five>=t.twenty and t.five>=t.sixty and t.five>=t.one_hundred_twenty
              and t.ten>=t.twenty and t.ten>=t.sixty and t.ten>=t.one_hundred_twenty
              and t.twenty>=t.sixty and t.twenty>=t.one_hundred_twenty
              and t.sixty>=t.one_hundred_twenty
              and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
      -- define cursor section.返回全部在某一支股票在某一日之后（包括某一日）的20个交易日内的交易记录
      cursor topTwentyStockClose is select * from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd')) b where rownum<=20 order by b.stock_date desc;
      -- 创建UTL_FILE.file_type对象，用于读写文件
      file_handle UTL_FILE.file_type;
      ------------------------------------------ standard declare section ---------------------------------------------------
  
      begin
          --------------------------------- standard for loop section --------------------------------------------------------
          file_handle := UTL_FILE.FOPEN('TXTDIR','SELECT_STOCK_3.txt','w');
  
          for i in allStockCode loop
              stockCode:=i.stock_code;
              -- 查询某只股票历史上的最高收盘价
              select max(t.stock_close) into maxStockClose from stock_moving_average t where t.stock_code=stockCode;
              -- 查询某只股票在某一日的收盘价格和120均线价格
              select t.stock_close,t.one_hundred_twenty into currentStockClose,currentStockOneHundredTwenty from stock_moving_average t where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
  
              num:=0;
              -- 计算某只股票的某一日的收盘价格是否是其历史最高价的百分子：1-percentage
              if (maxStockClose-currentStockClose)/maxStockClose>percentage then
                  for j in topTwentyStockClose loop
                      -- 判断某只股票的120均线是否在20个交易日内不是单调递减的
                      if currentStockOneHundredTwenty<j.one_hundred_twenty then
                          exit;
                      end if;
  
                      currentStockOneHundredTwenty:=j.one_hundred_twenty;
                      arrayStockCode.extend;
                      num:=1+num;
  
                      if num=20 then
                          arrayStockCode(arrayStockCode.count):=j.stock_code;
                          dbms_output.put_line('j.stock_code'||'  :  '||j.stock_code);
                          UTL_FILE.PUT_LINE(file_handle,j.stock_code);
                      end if;
                  end loop;
              end if;
          end loop;
          UTL_FILE.FCLOSE(file_handle);
      end;
  end SELECT_STOCK_3;*/

  /*------------------------------ judge whether stock is supported by 250 moving average ----------------------------------*/
  procedure SELECT_STOCK_WITH_250_SUPPORT(p_date               in varchar2,
                                          p_near_rate          in number,
                                          p_down_rate          in number,
                                          p_stock_result_array out T_STOCK_RESULT_ARRAY) as
    -- 表示某只股票历史上的最高收盘价
    v_max_stock_close number;
    -- T_STOCK_RESULT类型对象。表示出现异动的股票
    type_stock_result T_STOCK_RESULT;
    -- 获取某一日的所有股票stock_code
    cursor cur_max_stock_close is
      select max(o.close_price) as max_stock_close,
             o.code_,
             (select b.ma250
                from stock_transaction_data b
               where b.date_ = to_date(p_date, 'yyyy-mm-dd')
                 and b.code_ = o.code_) as two_hundred_fifty,
             (select c.close_price
                from stock_transaction_data c
               where c.date_ = to_date(p_date, 'yyyy-mm-dd')
                 and c.code_ = o.code_) as stock_close
        from stock_transaction_data o
       where o.code_ in
             (select t.code_
                from stock_transaction_data t
               where t.date_ = to_date(p_date, 'yyyy-mm-dd'))
       group by o.code_;
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  
  begin
    file_handle          := UTL_FILE.FOPEN('TXTDIR',
                                           'SELECT_STOCK_WITH_250_SUPPORT.txt',
                                           'w');
    p_stock_result_array := T_STOCK_RESULT_ARRAY();
  
    for i in cur_max_stock_close loop
      v_max_stock_close := i.max_stock_close;
    
      if (v_max_stock_close - i.stock_close) / v_max_stock_close >
         p_down_rate then
        if i.two_hundred_fifty is not null then
          if abs(i.stock_close - i.two_hundred_fifty) / i.two_hundred_fifty <=
             p_near_rate then
            dbms_output.put_line(i.code_);
          
            type_stock_result := T_STOCK_RESULT(i.code_,
                                                to_date(p_date, 'yyyy-mm-dd'));
            p_stock_result_array.extend;
            p_stock_result_array(p_stock_result_array.count) := type_stock_result;
          
            UTL_FILE.PUT_LINE(file_handle,
                              p_stock_result_array(p_stock_result_array.count)
                              .get_code() || '   ' || p_stock_result_array(p_stock_result_array.count)
                              .get_date());
          end if;
        end if;
      end if;
    end loop;
  end SELECT_STOCK_WITH_250_SUPPORT;

  /*------------------------------ judge whether stock is supported by 120 moving average ----------------------------------*/
  procedure SELECT_STOCK_WITH_120_SUPPORT(p_date               in varchar2,
                                          p_near_rate          in number,
                                          p_down_rate          in number,
                                          p_stock_result_array out T_STOCK_RESULT_ARRAY) as
    -- 表示某只股票历史上的最高收盘价
    v_max_stock_close number;
    -- T_STOCK_RESULT类型对象。表示出现异动的股票
    type_stock_result T_STOCK_RESULT;
    -- 获取某一日的所有股票stock_code
    cursor cur_max_stock_close is
      select max(o.close_price) as max_stock_close,
             o.code_,
             (select b.ma120
                from stock_transaction_data b
               where b.date_ = to_date(p_date, 'yyyy-mm-dd')
                 and b.code_ = o.code_) as one_hundred_twenty,
             (select c.close_price
                from stock_transaction_data c
               where c.date_ = to_date(p_date, 'yyyy-mm-dd')
                 and c.code_ = o.code_) as close_price
        from stock_transaction_data o
       where o.code_ in
             (select t.code_
                from stock_transaction_data t
               where t.date_ = to_date(p_date, 'yyyy-mm-dd'))
       group by o.code_;
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  
  begin
    file_handle          := UTL_FILE.FOPEN('TXTDIR',
                                           'SELECT_STOCK_WITH_120_SUPPORT.txt',
                                           'w');
    p_stock_result_array := T_STOCK_RESULT_ARRAY();
  
    for i in cur_max_stock_close loop
      v_max_stock_close := i.max_stock_close;
    
      if (v_max_stock_close - i.close_price) / v_max_stock_close >
         p_down_rate then
        if i.one_hundred_twenty is not null then
          if abs(i.close_price - i.one_hundred_twenty) /
             i.one_hundred_twenty <= p_near_rate then
            dbms_output.put_line(i.code_);
          
            type_stock_result := T_STOCK_RESULT(i.code_,
                                                to_date(p_date, 'yyyy-mm-dd'));
            p_stock_result_array.extend;
            p_stock_result_array(p_stock_result_array.count) := type_stock_result;
          
            UTL_FILE.PUT_LINE(file_handle,
                              p_stock_result_array(p_stock_result_array.count)
                              .get_code() || '   ' || p_stock_result_array(p_stock_result_array.count)
                              .get_date());
          end if;
        end if;
      end if;
    end loop;
  end SELECT_STOCK_WITH_120_SUPPORT;

  /*------------------------------ judge if MACD of the stock is the gold cross by date -----------------------------------------*/
  procedure SELECT_MACD_GOLD_CROSS(p_date              in varchar2,
                                   p_rate              in number,
                                   p_stock_macd_result out T_TYPE_ARRAY) as
    -- 定义一个stock_transaction_data%rowtype类型的变量
    row_stock_record stock_transaction_data%rowtype;
    -- 定义一个含有5个数值型数据的数组
    type type_array is varray(3) of stock_transaction_data%rowtype;
    type_array_dif_dea type_array := type_array();
    -- 表示code_
    v_stock_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取某只股票某一日及之前一日的记录
    cursor cur_last_two_day_stock_info is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_stock_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  begin
    p_stock_macd_result := SCOTT.T_TYPE_ARRAY();
    file_handle         := UTL_FILE.FOPEN('TXTDIR',
                                          'SELECT_MACD_UP_BELOW_ZERO.txt',
                                          'w');
  
    for i in cur_all_stock_code loop
      v_stock_code       := i.code_;
      type_array_dif_dea := type_array();
      for j in cur_last_two_day_stock_info loop
        row_stock_record.dif   := j.dif;
        row_stock_record.dea   := j.dea;
        row_stock_record.code_ := j.code_;
        type_array_dif_dea.extend;
        type_array_dif_dea(type_array_dif_dea.count) := row_stock_record;
      end loop;
      -- 由于有些股票是在p_date之后才上市交易，因此没有记录，即type_array_dif_dea.count!=2，所以此处要先判断一下
      if type_array_dif_dea.count = 2 then
        if type_array_dif_dea(1).dif > type_array_dif_dea(1).dea and type_array_dif_dea(2)
           .dif < type_array_dif_dea(2).dea then
          if (type_array_dif_dea(1)
             .dif + type_array_dif_dea(1).dea + type_array_dif_dea(2).dif + type_array_dif_dea(2).dea) / 4 <
             p_rate then
            dbms_output.put_line(type_array_dif_dea(1).code_);
            p_stock_macd_result.extend;
            p_stock_macd_result(p_stock_macd_result.count) := type_array_dif_dea(1)
                                                              .code_;
            UTL_FILE.PUT_LINE(file_handle, type_array_dif_dea(1).code_);
          end if;
        end if;
      end if;
    end loop;
    UTL_FILE.FCLOSE(file_handle);
  end SELECT_MACD_GOLD_CROSS;

  /*-------------------------------------- judge whether the close of stock is up or down --------------------------------------------*/
  /*-- stock_transaction_data表的up_down字段已经在获取数据时就计算好了，所以现在这个方法用不着了。 ---*/
  procedure WRITE_UP_DOWN as
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      stockCode varchar2(10);
      -- 表示前一天的stock_close
      preStockClose number;
      -- 表示某只股票第一天的stock_close
      initStockClose number;
      -- define cursor section.返回全部stock_code。注意表STOCK_CODE的字段stock_code是大写的，所以要用函数lower
      cursor allStockCode is
        select distinct t.stock_code
          from stock_moving_average t
         order by t.stock_code desc;
      -- 返回某只股票所有交易日的收盘价
      cursor singleStockClose is
        select *
          from stock_moving_average t
         where t.stock_code = stockCode
         order by t.stock_date asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      for i in allStockCode loop
        stockCode := lower(i.stock_code);
        select t.stock_close
          into initStockClose
          from stock_moving_average t
         where t.stock_code = stockCode
           and rownum <= 1
         order by t.stock_date asc;
        preStockClose := initStockClose;
        for j in singleStockClose loop
          if preStockClose = j.stock_close then
            --dbms_output.put_line('stockCode'||'  :  '||stockCode);
            update stock_moving_average t
               set t.up_down = 0
             where t.stock_code = j.stock_code
               and t.stock_date = j.stock_date;
          elsif preStockClose < j.stock_close then
            update stock_moving_average t
               set t.up_down = 1
             where t.stock_code = j.stock_code
               and t.stock_date = j.stock_date;
            --dbms_output.put_line('stockCode'||'  :  '||stockCode);
          else
            update stock_moving_average t
               set t.up_down = -1
             where t.stock_code = j.stock_code
               and t.stock_date = j.stock_date;
            --dbms_output.put_line('stockCode'||'  :  '||stockCode);
          end if;
          preStockClose := j.stock_close;
        end loop;
      end loop;
    end;
  end WRITE_UP_DOWN;

  /*------------------------------ judge whether the close of stock is up or down according the given date ------------------------------------*/
  procedure WRITE_UP_DOWN_BY_DATE(stockDate in varchar2) as
    -- 表示stock_code
    stockCode varchar2(10);
    -- 表示当前股票某一日的收盘价
    currentStockClose number;
    -- 表示当前股票某一日的日期
    currentStockDate Date;
    -- define cursor section.返回全部stock_code。
    cursor allStockCode is
      select distinct t.stock_code
        from stock_moving_average t
       order by t.stock_code asc;
    -- 返回某只股票从某一日开始最近两天的交易记录
    cursor twoStock is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date <= to_date(stockDate, 'yyyy-mm-dd')
         and rownum <= 2
       order by t.stock_date desc;
  begin
    for i in allStockCode loop
      stockCode := i.stock_code;
      -- 返回某只股票某一日的收盘价和日期
      select t.stock_close, t.stock_date
        into currentStockClose, currentStockDate
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date <= to_date(stockDate, 'yyyy-mm-dd')
         and rownum <= 1
       order by t.stock_date desc;
      for j in twoStock loop
        if currentStockDate != j.stock_date then
          if j.stock_close > currentStockClose then
            update stock_moving_average t
               set t.up_down = -1
             where t.stock_code = i.stock_code
               and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
          elsif j.stock_close < currentStockClose then
            update stock_moving_average t
               set t.up_down = 1
             where t.stock_code = i.stock_code
               and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
          else
            update stock_moving_average t
               set t.up_down = 0
             where t.stock_code = i.stock_code
               and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
          end if;
        end if;
      end loop;
    end loop;
  end WRITE_UP_DOWN_BY_DATE;

  /*------------------------------------------------------ write MACD of all stocks ------------------------------------------------------*/
  procedure WRITE_MACD_INIT as
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取每只股票第一个交易日的日期
    cursor cur_first_stock_date is
      select min(t.date_) as date_
        from stock_transaction_data t
       where t.code_ = v_code;
  begin
    -- 初始化每只股票第一个交易日的ema12,ema26,dif和dea字段
    for i in cur_all_stock_code loop
      v_code := i.code_;
      for j in cur_first_stock_date loop
        update stock_transaction_data t
           set t.ema12 = t.close_price,
               t.ema26 = t.close_price,
               t.dif   = 0,
               t.dea   = 0
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
      end loop;
    end loop;
  end WRITE_MACD_INIT;

  procedure WRITE_MACD_EMA as
    v_pre_ema12  number;
    v_pre_ema26  number;
    v_pre_dif    number;
    v_pre_dea    number;
    v_first_date date;
    -- 表示stock_code
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取每只股票第一个交易日的日期
    cursor cur_first_stock_date is
      select min(t.date_) as date_
        from stock_transaction_data t
       where t.code_ = v_code;
    -- 根据v_code选出某只股票的除第一天以外的全部交易记录，按升序排列
    cursor cur_all_stock is
      select distinct *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_first_date
       order by t.date_ asc;
  begin
    -- 计算每只股票其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_stock_code loop
      v_code := i.code_;
      -- 用记录是第一个交易日的字段初始化相关变量
      for x in cur_first_stock_date loop
        select t.ema12, t.ema26, t.dif, t.dea
          into v_pre_ema12, v_pre_ema26, v_pre_dif, v_pre_dea
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = x.date_;
      end loop;
    
      select min(t.date_)
        into v_first_date
        from stock_transaction_data t
       where t.code_ = v_code;
      for j in cur_all_stock loop
        -- 对于其余交易日，更新ema12,ema26,dif和dea字段
        update stock_transaction_data t
           set t.ema12 = v_pre_ema12 * 11 / 13 + j.close_price * 2 / 13,
               t.ema26 = v_pre_ema26 * 25 / 27 + j.close_price * 2 / 27
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
        -- 用于计算下一个交易日时使用
        select t.ema12
          into v_pre_ema12
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
        select t.ema26
          into v_pre_ema26
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
      end loop;
    end loop;
  end WRITE_MACD_EMA;

  procedure WRITE_MACD_DIF as
    v_pre_ema12  number;
    v_pre_ema26  number;
    v_pre_dif    number;
    v_pre_dea    number;
    v_first_date date;
    -- 表示stock_code
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取每只股票第一个交易日的日期
    cursor cur_first_stock_date is
      select min(t.date_) as date_
        from stock_transaction_data t
       where t.code_ = v_code;
    -- 根据v_code选出某只股票的除第一天以外的全部交易记录，按升序排列
    cursor cur_all_stock is
      select distinct *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_first_date
       order by t.date_ asc;
  begin
    -- 计算每只股票其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_stock_code loop
      v_code := i.code_;
      -- 用记录是第一个交易日的字段初始化相关变量
      for x in cur_first_stock_date loop
        select t.ema12, t.ema26, t.dif, t.dea
          into v_pre_ema12, v_pre_ema26, v_pre_dif, v_pre_dea
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = x.date_;
      end loop;
      select min(t.date_)
        into v_first_date
        from stock_transaction_data t
       where t.code_ = v_code;
      for j in cur_all_stock loop
        update stock_transaction_data t
           set t.dif = t.ema12 - t.ema26
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
        select t.dif
          into v_pre_dif
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
      end loop;
    end loop;
  end WRITE_MACD_DIF;

  procedure WRITE_MACD_DEA as
    v_pre_ema12  number;
    v_pre_ema26  number;
    v_pre_dif    number;
    v_pre_dea    number;
    v_first_date date;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取每只股票第一个交易日的日期
    cursor cur_first_stock_date is
      select min(t.date_) as date_
        from stock_transaction_data t
       where t.code_ = v_code;
    -- 根据v_code选出某只股票的除第一天以外的全部交易记录，按升序排列
    cursor cur_all_stock is
      select distinct *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_first_date
       order by t.date_ asc;
  begin
    -- 计算每只股票其余交易日的ema12,ema26,dif和dea字段
    for i in cur_all_stock_code loop
      v_code := i.code_;
      -- 用记录是第一个交易日的字段初始化相关变量
      for x in cur_first_stock_date loop
        select t.ema12, t.ema26, t.dif, t.dea
          into v_pre_ema12, v_pre_ema26, v_pre_dif, v_pre_dea
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = x.date_;
      end loop;
      select min(t.date_)
        into v_first_date
        from stock_transaction_data t
       where t.code_ = v_code;
      for j in cur_all_stock loop
        update stock_transaction_data t
           set t.dea = v_pre_dea * 8 / 10 + t.dif * 2 / 10
         where t.code_ = v_code
           and t.date_ = j.date_;
        commit;
        select t.dea
          into v_pre_dea
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = j.date_;
      end loop;
    end loop;
  end WRITE_MACD_DEA;

  /*-------------------------------- write MACD of all stocks by date -----------------------------------------------*/
  procedure WRITE_MACD as
  begin
    update stock_transaction_data t
       set t.macd = 2 * (t.dif - t.dea)
     where t.dif is not null
       and t.dea is not null;
    commit;
  end WRITE_MACD;

  /*-------------------------------- write MACD of all stocks by date -----------------------------------------------*/
  procedure WRITE_MACD_EMA_BY_DATE(p_date in varchar2) as
    v_pre_ema12 number;
    v_pre_ema26 number;
    v_pre_dea   number;
    -- 表示code_
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
    -- 获取某只股票最近两天的记录
    cursor cur_single_stock is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      for j in cur_single_stock loop
        if j.date_ != to_date(p_date, 'yyyy-mm-dd') then
          v_pre_ema12 := j.ema12;
          v_pre_ema26 := j.ema26;
          v_pre_dea   := j.dea;
          update stock_transaction_data t
             set t.ema12 = v_pre_ema12 * 11 / 13 + t.close_price * 2 / 13,
                 t.ema26 = v_pre_ema26 * 25 / 27 + t.close_price * 2 / 27
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_MACD_EMA_BY_DATE;

  procedure WRITE_MACD_DIF_BY_DATE(p_date in varchar2) as
    v_pre_ema12 number;
    v_pre_ema26 number;
    v_pre_dea   number;
    -- 表示stock_code
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
    -- 获取某只股票最近两天的记录
    cursor cur_single_stock is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      for j in cur_single_stock loop
        if j.date_ != to_date(p_date, 'yyyy-mm-dd') then
          v_pre_ema12 := j.ema12;
          v_pre_ema26 := j.ema26;
          v_pre_dea   := j.dea;
          update stock_transaction_data t
             set t.dif = t.ema12 - t.ema26
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_MACD_DIF_BY_DATE;

  procedure WRITE_MACD_DEA_BY_DATE(p_date in varchar2) as
    v_pre_ema12 number;
    v_pre_ema26 number;
    v_pre_dea   number;
    -- 表示stock_code
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
    -- 获取某只股票最近两天的记录
    cursor cur_single_stock is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      for j in cur_single_stock loop
        if j.date_ != to_date(p_date, 'yyyy-mm-dd') then
          v_pre_ema12 := j.ema12;
          v_pre_ema26 := j.ema26;
          v_pre_dea   := j.dea;
          update stock_transaction_data t
             set t.dea = v_pre_dea * 8 / 10 + t.dif * 2 / 10
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_MACD_DEA_BY_DATE;

  procedure WRITE_MACD_BY_DATE(p_date in varchar2) as
    -- 表示stock_code
    v_code varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      update stock_transaction_data t
         set t.macd = 2 * (t.dif - t.dea)
       where t.code_ = v_code
         and t.dif is not null
         and t.dea is not null
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    end loop;
    commit;
  end WRITE_MACD_BY_DATE;

  /*--------------------------------- 计算日线级别所有股票的KD指标 ----------------------------*/
  procedure WRITE_KD_INIT as
    -- 表示某只股票的code_字段
    v_code varchar2(10);
    -- 用于计算是否是第9个交易周
    num number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 查询某只股票最初的8个交易周的记录，并按生序排列
    cursor cur_single_stock_k is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
               order by t.date_ asc)
       where rownum <= 8;
  begin
    -- 初始化每只股票第一个交易日的K和D字段
    for i in cur_all_stock_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_stock_k loop
        num := num + 1;
        if num = 8 then
          -- 若无前一日K值与D值，则可分别用50来代替
          update stock_transaction_data t
             set t.k = 50, t.d = 50
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_INIT;

  procedure WRITE_KD_RSV as
    -- 表示某只股票的code_字段
    v_code varchar2(10);
    -- 9日内最高价
    v_nine_day_highest_price number;
    -- 9日内最低价
    v_nine_day_lowest_price number;
    -- 计算指标时使用，用于表示日期的累积
    num number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取某只股票所有的日线级别的交易记录，并按升序排列
    cursor cur_single_stock is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_stock loop
        num := num + 1;
        if num >= 9 then
          -- 计算9日内最高价和最低价
          select max(highest_price), min(lowest_price)
            into v_nine_day_highest_price, v_nine_day_lowest_price
            from (select *
                    from stock_transaction_data t
                   where t.code_ = v_code
                     and t.date_ <= j.date_
                   order by t.date_ desc)
           where rownum <= 9;
          -- 计算rsv
          if (v_nine_day_highest_price - v_nine_day_lowest_price) = 0 then
            update stock_transaction_data t
               set t.rsv =
                   (select t1.rsv
                      from (select *
                              from stock_transaction_data t
                             where t.code_ = v_code
                               and t.date_ < j.date_
                             order by t.date_ desc) t1
                     where rownum <= 1)
             where t.code_ = v_code
               and t.date_ = j.date_;
          else
            update stock_transaction_data t
               set t.rsv =
                   (t.close_price - v_nine_day_lowest_price) /
                   (v_nine_day_highest_price - v_nine_day_lowest_price) * 100
             where t.code_ = v_code
               and t.date_ = j.date_;
          end if;
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_RSV;

  procedure WRITE_KD_K as
    -- 表示某只股票的CODE_字段
    v_code varchar2(10);
    -- 计算指标时使用，用于表示日期的累积
    num number;
    -- 表示KD指标的K
    v_temp_k number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取某只股票所有的日线级别的交易记录，并按升序排列
    cursor cur_single_stock is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_stock loop
        num := num + 1;
        if num = 8 then
          v_temp_k := j.k;
        end if;
        if num >= 9 then
          -- 计算K
          update stock_transaction_data t
             set t.k = 2 / 3 * v_temp_k + 1 / 3 * t.rsv
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
          select t.k
            into v_temp_k
            from stock_transaction_data t
           where t.code_ = v_code
             and t.date_ = j.date_;
        end if;
      
      end loop;
    end loop;
  end WRITE_KD_K;

  procedure WRITE_KD_D as
    -- 表示某只股票的code_字段
    v_code varchar2(10);
    -- 计算指标时使用，用于表示日期的累积
    num number;
    -- 表示KD指标的D
    v_temp_d number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 获取某只股票所有的日线级别的交易记录，并按升序排列
    cursor cur_single_stock is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      num    := 0;
      for j in cur_single_stock loop
        num := num + 1;
        if num = 8 then
          v_temp_d := j.d;
        end if;
        if num >= 9 then
          -- 计算D
          update stock_transaction_data t
             set t.d = 2 / 3 * v_temp_d + 1 / 3 * j.k
           where t.code_ = v_code
             and t.date_ = j.date_;
          commit;
          select t.d
            into v_temp_d
            from stock_transaction_data t
           where t.code_ = v_code
             and t.date_ = j.date_;
        end if;
      end loop;
    end loop;
  end WRITE_KD_D;

  ---------------------------------- 计算日线级别，某一日，所有股票的KD指标 ----------------------------
  procedure WRITE_KD_BY_DATE_RSV(p_date varchar2) as
    -- 表示某只股票的code_字段
    v_code varchar2(10);
    -- 9日内最高价
    v_nine_day_highest_price number;
    -- 9日内最低价
    v_nine_day_lowest_price number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      -- 计算9日内最高价和最低价
      select max(highest_price), min(lowest_price)
        into v_nine_day_highest_price, v_nine_day_lowest_price
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 9;
      -- 计算某只股票某一日的RSV
      update stock_transaction_data t
         set t.rsv =
             (t.close_price - v_nine_day_lowest_price) /
             (v_nine_day_highest_price - v_nine_day_lowest_price) * 100
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      commit;
    end loop;
  end WRITE_KD_BY_DATE_RSV;

  procedure WRITE_KD_BY_DATE_K(p_date varchar2) as
    -- 表示某只股票的code_字段
    v_code varchar2(10);
    -- 表示前一日K值
    v_temp_k number;
    -- 用于计数
    v_num number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
    -- 按照日期参数，获取某只股票最近两天的日线级别的交易记录，并按降序排列
    cursor cur_single_stock is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2
       order by date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      v_num  := 0;
      for j in cur_single_stock loop
        v_num := v_num + 1;
        if v_num = 1 then
          v_temp_k := j.k;
        end if;
        if v_num = 2 then
          update stock_transaction_data t
             set t.k = 2 / 3 * v_temp_k + 1 / 3 * t.rsv
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_BY_DATE_K;

  procedure WRITE_KD_BY_DATE_D(p_date varchar2) as
    -- 表示某只股票的code_字段
    v_code varchar2(10);
    -- 表示前一日K值
    v_temp_d number;
    -- 用于计数
    v_num number;
    -- define cursor section.返回全部code_
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
    -- 按照日期参数，获取某只股票最近两天的日线级别的交易记录，并按降序排列
    cursor cur_single_stock is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 2
       order by date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      v_num  := 0;
      for j in cur_single_stock loop
        v_num := v_num + 1;
        if v_num = 1 then
          v_temp_d := j.d;
        end if;
        if v_num = 2 then
          update stock_transaction_data t
             set t.d = 2 / 3 * v_temp_d + 1 / 3 * t.k
           where t.code_ = v_code
             and t.date_ = to_date(p_date, 'yyyy-mm-dd');
          commit;
        end if;
      end loop;
    end loop;
  end WRITE_KD_BY_DATE_D;

  /*------------------------------ 计算所有股票的ha -----------------------------------------*/
  procedure WRITE_HA as
    -- 代码
    v_code varchar2(10);
    -- 日期
    v_date date;
    -- 获取所有的CODE
    cursor cur_all_code is
      select distinct t.code_ code from stock_transaction_data t;
    -- 查询某个具体的指数的第一条交易记录
    cursor cur_fist_stda is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
               order by t.date_ asc)
       where rownum <= 1;
    -- 查询某个股票除了最早的一条记录外的其他记录，并按升序排列
    cursor cur_later_stda is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ > v_date
       order by t.date_ asc;
    -- 定义表stock_transaction_data_all结构的游标变量
    first_stda stock_transaction_data_all%rowtype;
    later_stda stock_transaction_data_all%rowtype;
    pre_stda   stock_transaction_data_all%rowtype;
    -- 用于计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价的变量
    v_ha_open_price    number;
    v_ha_close_price   number;
    v_ha_highest_price number;
    v_ha_lowest_price  number;
  begin
    open cur_all_code;
    loop
      -- 获取每个股票的code_字段
      fetch cur_all_code
        into v_code;
      exit when cur_all_code%notfound;
    
      -- 先计算每个股票的第一条记录
      open cur_fist_stda;
      fetch cur_fist_stda
        into first_stda;
      exit when cur_fist_stda%notfound;
    
      -- 计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价
      v_ha_open_price  := (first_stda.open_price + first_stda.close_price) / 2;
      v_ha_close_price := (first_stda.open_price + first_stda.close_price +
                          first_stda.highest_price +
                          first_stda.lowest_price) / 4;
      if first_stda.highest_price > v_ha_open_price then
        v_ha_highest_price := first_stda.highest_price;
      else
        v_ha_highest_price := v_ha_open_price;
      end if;
      if first_stda.lowest_price < v_ha_open_price then
        v_ha_lowest_price := first_stda.lowest_price;
      else
        v_ha_lowest_price := v_ha_open_price;
      end if;
      -- 保存hei kin ashi平均K线开盘价，收盘价，最高价和最低价
      update stock_transaction_data t
         set t.ha_open_price    = v_ha_open_price,
             t.ha_close_price   = v_ha_close_price,
             t.ha_highest_price = v_ha_highest_price,
             t.ha_lowest_price  = v_ha_lowest_price
       where t.code_ = first_stda.code_
         and t.date_ = first_stda.date_;
      commit;
      v_date := first_stda.date_;
      close cur_fist_stda;
    
      -- 再计算每个股票的其他记录
      open cur_later_stda;
      loop
        fetch cur_later_stda
          into later_stda;
        exit when cur_later_stda%notfound;
      
        -- 前一条记录
        select *
          into pre_stda
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= v_date
                 order by t.date_ desc)
         where rownum <= 1;
      
        -- 计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        v_ha_open_price  := (pre_stda.ha_open_price +
                            pre_stda.ha_close_price) / 2;
        v_ha_close_price := (later_stda.open_price + later_stda.close_price +
                            later_stda.highest_price +
                            later_stda.lowest_price) / 4;
        if later_stda.highest_price > v_ha_open_price then
          v_ha_highest_price := later_stda.highest_price;
        else
          v_ha_highest_price := v_ha_open_price;
        end if;
        if later_stda.lowest_price < v_ha_open_price then
          v_ha_lowest_price := later_stda.lowest_price;
        else
          v_ha_lowest_price := v_ha_open_price;
        end if;
        -- 保存hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        update stock_transaction_data t
           set t.ha_open_price    = v_ha_open_price,
               t.ha_close_price   = v_ha_close_price,
               t.ha_highest_price = v_ha_highest_price,
               t.ha_lowest_price  = v_ha_lowest_price
         where t.code_ = later_stda.code_
           and t.date_ = later_stda.date_;
        commit;
      
        -- 把这次的数据留给下一次迭代使用
        v_date := later_stda.date_;
      end loop;
      close cur_later_stda;
    
    end loop;
    close cur_all_code;
  end WRITE_HA;

  /*------------------------------ 计算某一日所有股票的ha -----------------------------------------*/
  procedure WRITE_HA_BY_DATE(p_date varchar2) as
    -- 获取所有的CODE
    cursor cur_all_code is
      select distinct t.code_ code from stock_transaction_data_all t;
    -- 表示CODE类型的变量
    v_code varchar2(10);
  
    -- 查询某个指数在日期p_date之前的那一条记录
    cursor cur_all_std is
      select *
        from (select *
                from (select *
                        from stock_transaction_data t
                       where t.code_ = v_code
                         and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                       order by t.date_ desc)
               where rownum <= 2) t2
       where t2.date_ <>
             (select t1.date_
                from stock_transaction_data t1
               where t1.code_ = v_code
                 and t1.date_ = to_date(p_date, 'yyyy-mm-dd'));
    -- 定义表stock_transaction_data结构的游标变量
    all_std stock_transaction_data%rowtype;
  
    -- 查询某个具体的股票的某一日交易记录
    cursor cur_later_std is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    -- 定义表stock_transaction_data结构的游标变量
    later_std stock_transaction_data%rowtype;
  
    -- 用于计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价的变量
    v_ha_open_price    number;
    v_ha_close_price   number;
    v_ha_highest_price number;
    v_ha_lowest_price  number;
  begin
    open cur_all_code;
    loop
      -- 获取每个股票的code_字段
      fetch cur_all_code
        into v_code;
      exit when cur_all_code%notfound;
    
      open cur_all_std;
      open cur_later_std;
      loop
        fetch cur_all_std
          into all_std;
        exit when cur_all_std%notfound;
      
        fetch cur_later_std
          into later_std;
        exit when cur_later_std%notfound;
      
        -- 计算hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        v_ha_open_price  := (all_std.ha_open_price + all_std.ha_close_price) / 2;
        v_ha_close_price := (later_std.open_price + later_std.close_price +
                            later_std.highest_price +
                            later_std.lowest_price) / 4;
        if later_std.highest_price > v_ha_open_price then
          v_ha_highest_price := later_std.highest_price;
        else
          v_ha_highest_price := v_ha_open_price;
        end if;
        if later_std.lowest_price < v_ha_open_price then
          v_ha_lowest_price := later_std.lowest_price;
        else
          v_ha_lowest_price := v_ha_open_price;
        end if;
        -- 保存hei kin ashi平均K线开盘价，收盘价，最高价和最低价
        update stock_transaction_data t
           set t.ha_open_price    = v_ha_open_price,
               t.ha_close_price   = v_ha_close_price,
               t.ha_highest_price = v_ha_highest_price,
               t.ha_lowest_price  = v_ha_lowest_price
         where t.code_ = later_std.code_
           and t.date_ = later_std.date_;
        commit;
      end loop;
      close cur_all_std;
      close cur_later_std;
    
    end loop;
    close cur_all_code;
  end WRITE_HA_BY_DATE;

  /*---------------------------------------------------- 计算所有股票的乖离率 -------------------------------------------------*/
  procedure WRITE_BIAS is
  begin
    declare
      -- 表示code_
      v_code varchar2(10);
      -- 5日乖离率
      v_bias5 number := null;
      -- 10日乖离率
      v_bias10 number := null;
      -- 20日乖离率
      v_bias20 number := null;
      -- 60日乖离率
      v_bias60 number := null;
      -- 120日乖离率
      v_bias120 number := null;
      -- 250日乖离率
      v_bias250 number := null;
      -- 交易日期
      v_date date;
      -- 获取股票的code_，排除重复
      cursor cur_all_stock_code is
        select distinct t.code_ from stock_transaction_data t;
      -- 根据v_code，获取这个股票的所有数据，并按照升序排列
      cursor cur_single_stock is
        select *
          from stock_transaction_data t
         where t.code_ = v_code
         order by t.date_ asc;
    begin
      for i in cur_all_stock_code loop
        v_code := i.code_;
        for j in cur_single_stock loop
          v_date := j.date_;
          -- 5日乖离率
          if j.ma5 is not null then
            v_bias5 := (j.close_price - j.ma5) / j.ma5 * 100;
          else
            v_bias5 := null;
          end if;
          -- 10日乖离率
          if j.ma10 is not null then
            v_bias10 := (j.close_price - j.ma10) / j.ma10 * 100;
          else
            v_bias10 := null;
          end if;
          -- 20日乖离率
          if j.ma20 is not null then
            v_bias20 := (j.close_price - j.ma20) / j.ma20 * 100;
          else
            v_bias20 := null;
          end if;
          -- 60日乖离率
          if j.ma60 is not null then
            v_bias60 := (j.close_price - j.ma60) / j.ma60 * 100;
          else
            v_bias60 := null;
          end if;
          -- 120日乖离率
          if j.ma120 is not null then
            v_bias120 := (j.close_price - j.ma120) / j.ma120 * 100;
          else
            v_bias120 := null;
          end if;
          -- 250日乖离率
          if j.ma250 is not null then
            v_bias250 := (j.close_price - j.ma250) / j.ma250 * 100;
          else
            v_bias250 := null;
          end if;
        
          -- 更新
          update stock_transaction_data t
             set t.bias5   = v_bias5,
                 t.bias10  = v_bias10,
                 t.bias20  = v_bias20,
                 t.bias60  = v_bias60,
                 t.bias120 = v_bias120,
                 t.bias250 = v_bias250
           where t.code_ = v_code
             and t.date_ = v_date;
        end loop;
        commit;
      end loop;
    end;
  end WRITE_BIAS;

  /*------------------------ 按照日期，计算所有股票在某一日的乖离率 -----------------------*/
  procedure WRITE_BIAS_BY_DATE(p_date in varchar2) is
    -- 表示CODE_类型的变量
    v_code varchar2(10);
    -- 5日乖离率
    v_bias5 number := null;
    -- 10日乖离率
    v_bias10 number := null;
    -- 20日乖离率
    v_bias20 number := null;
    -- 60日乖离率
    v_bias60 number := null;
    -- 120日乖离率
    v_bias120 number := null;
    -- 250日乖离率
    v_bias250 number := null;
  
    row_stock_transaction_data stock_transaction_data%rowtype;
    -- 获取所有股票的code_，排除重复的
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data t;
    -- 某个股票某一天的交易记录
    cursor cur_single_stock is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
  begin
    open cur_all_stock_code;
    loop
      -- 获取每个股票的code_字段
      fetch cur_all_stock_code
        into v_code;
      exit when cur_all_stock_code%notfound;
    
      open cur_single_stock;
      loop
        -- 获取每个股票在某一日的交易记录
        fetch cur_single_stock
          into row_stock_transaction_data;
        exit when cur_single_stock%notfound;
      
        -- 计算5日乖离率、10日乖离率、20日乖离率、60日乖离率、120日乖离率、250日乖离率
        if row_stock_transaction_data.ma5 is not null then
          v_bias5 := (row_stock_transaction_data.close_price -
                     row_stock_transaction_data.ma5) /
                     row_stock_transaction_data.ma5 * 100;
        else
          v_bias5 := null;
        end if;
        if row_stock_transaction_data.ma10 is not null then
          v_bias10 := (row_stock_transaction_data.close_price -
                      row_stock_transaction_data.ma10) /
                      row_stock_transaction_data.ma10 * 100;
        else
          v_bias10 := null;
        end if;
        if row_stock_transaction_data.ma20 is not null then
          v_bias20 := (row_stock_transaction_data.close_price -
                      row_stock_transaction_data.ma20) /
                      row_stock_transaction_data.ma20 * 100;
        else
          v_bias20 := null;
        end if;
        if row_stock_transaction_data.ma60 is not null then
          v_bias60 := (row_stock_transaction_data.close_price -
                      row_stock_transaction_data.ma60) /
                      row_stock_transaction_data.ma60 * 100;
        else
          v_bias60 := null;
        end if;
        if row_stock_transaction_data.ma120 is not null then
          v_bias120 := (row_stock_transaction_data.close_price -
                       row_stock_transaction_data.ma120) /
                       row_stock_transaction_data.ma120 * 100;
        else
          v_bias120 := null;
        end if;
        if row_stock_transaction_data.ma250 is not null then
          v_bias250 := (row_stock_transaction_data.close_price -
                       row_stock_transaction_data.ma250) /
                       row_stock_transaction_data.ma250 * 100;
        else
          v_bias250 := null;
        end if;
      
        -- 更新某个股票在某一日的5日乖离率、10日乖离率、20日乖离率、60日乖离率、120日乖离率、250日乖离率
        update stock_transaction_data t
           set t.bias5   = v_bias5,
               t.bias10  = v_bias10,
               t.bias20  = v_bias20,
               t.bias60  = v_bias60,
               t.bias120 = v_bias120,
               t.bias250 = v_bias250
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
      end loop;
      close cur_single_stock;
    
      commit;
    
    end loop;
    close cur_all_stock_code;
  end WRITE_BIAS_BY_DATE;

  /*------------------------------ 计算所有股票的方差 -----------------------------------------*/
  procedure WRITE_VARIANCE is
    -- 股票code
    v_code varchar2(10);
    -- 股票date
    v_date date;
    -- 方差
    v_variance5   number;
    v_variance10  number;
    v_variance20  number;
    v_variance60  number;
    v_variance120 number;
    v_variance250 number;
    -- 计算方差时用到的变量
    v_sum5   number;
    v_sum10  number;
    v_sum20  number;
    v_sum60  number;
    v_sum120 number;
    v_sum250 number;
    -- 均线
    v_ma5   number;
    v_ma10  number;
    v_ma20  number;
    v_ma60  number;
    v_ma120 number;
    v_ma250 number;
    -- 需要计算的记录数
    v_rownum number;
    -- 所有股票的code
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data t;
    -- 某一只股票的全部交易日期，并降序排列
    cursor cur_std_by_code is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
       order by t.date_ desc;
    -- 某只股票、在某个日期之前的全部记录，按日期降序排列
    cursor cur_std_by_code_date is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= v_date
               order by t.date_ desc)
       where rownum <= v_rownum;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
    
      for j in cur_std_by_code loop
        v_date := j.date_;
      
        v_ma5   := j.ma5;
        v_ma10  := j.ma10;
        v_ma20  := j.ma20;
        v_ma60  := j.ma60;
        v_ma120 := j.ma120;
        v_ma250 := j.ma250;
      
        -- 重置
        v_variance5   := null;
        v_variance10  := null;
        v_variance20  := null;
        v_variance60  := null;
        v_variance120 := null;
        v_variance250 := null;
        v_sum5        := 0;
        v_sum10       := 0;
        v_sum20       := 0;
        v_sum60       := 0;
        v_sum120      := 0;
        v_sum250      := 0;
      
        -- 计算variance5
        if v_ma5 is not null then
          v_rownum := 5;
          for x in cur_std_by_code_date loop
            v_sum5 := v_sum5 + power(x.close_price - v_ma5, 2);
          end loop;
          v_variance5 := 1 / (v_rownum - 1) * v_sum5;
        end if;
      
        -- 计算variance10
        if v_ma10 is not null then
          v_rownum := 10;
          for x in cur_std_by_code_date loop
            v_sum10 := v_sum10 + power(x.close_price - v_ma10, 2);
          end loop;
          v_variance10 := 1 / (v_rownum - 1) * v_sum10;
        end if;
      
        -- 计算variance20
        if v_ma20 is not null then
          v_rownum := 20;
          for x in cur_std_by_code_date loop
            v_sum20 := v_sum20 + power(x.close_price - v_ma20, 2);
          end loop;
          v_variance20 := 1 / (v_rownum - 1) * v_sum20;
        end if;
      
        -- 计算variance60
        if v_ma60 is not null then
          v_rownum := 60;
          for x in cur_std_by_code_date loop
            v_sum60 := v_sum60 + power(x.close_price - v_ma60, 2);
          end loop;
          v_variance60 := 1 / (v_rownum - 1) * v_sum60;
        end if;
      
        -- 计算variance120
        if v_ma120 is not null then
          v_rownum := 120;
          for x in cur_std_by_code_date loop
            v_sum120 := v_sum120 + power(x.close_price - v_ma120, 2);
          end loop;
          v_variance120 := 1 / (v_rownum - 1) * v_sum120;
        end if;
      
        -- 计算variance250
        if v_ma250 is not null then
          v_rownum := 250;
          for x in cur_std_by_code_date loop
            v_sum250 := v_sum250 + power(x.close_price - v_ma250, 2);
          end loop;
          v_variance250 := 1 / (v_rownum - 1) * v_sum250;
        end if;
      
        -- 更新
        update stock_transaction_data t
           set t.variance5   = v_variance5,
               t.variance10  = v_variance10,
               t.variance20  = v_variance20,
               t.variance60  = v_variance60,
               t.variance120 = v_variance120,
               t.variance250 = v_variance250
         where t.code_ = v_code
           and t.date_ = v_date;
      end loop;
    end loop;
    commit;
  end WRITE_VARIANCE;

  /*------------------------------ 按照日期，计算所有股票在某一日的的方差 -----------------------------------------*/
  procedure WRITE_VARIANCE_BY_DATE(p_date in varchar2) is
    -- 股票code
    v_code varchar2(10);
    -- 股票date
    --v_date date;
    -- 方差
    v_variance5   number;
    v_variance10  number;
    v_variance20  number;
    v_variance60  number;
    v_variance120 number;
    v_variance250 number;
    -- 计算方差时用到的变量
    v_sum5   number;
    v_sum10  number;
    v_sum20  number;
    v_sum60  number;
    v_sum120 number;
    v_sum250 number;
    -- 均线
    v_ma5   number;
    v_ma10  number;
    v_ma20  number;
    v_ma60  number;
    v_ma120 number;
    v_ma250 number;
    -- 需要计算的记录数
    v_rownum number;
    -- stock_transaction_data类型的变量
    row_stock_transaction_data stock_transaction_data%rowtype;
    -- 所有股票的code
    cursor cur_all_stock_code is
      select distinct t.code_ from stock_transaction_data t;
    -- 某只股票、在某个日期之前的全部记录，按日期降序排列
    cursor cur_std_by_code_date is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= v_rownum;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
    
      begin
        select *
          into row_stock_transaction_data
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      exception
        when NO_DATA_FOUND then
          dbms_output.put_line('STOCK_TRANSACTION_DATA表中，没有code_为【' ||
                               v_code || '】，date_为【' || p_date || '】时的记录');
          continue;
      end;
    
      v_ma5   := row_stock_transaction_data.ma5;
      v_ma10  := row_stock_transaction_data.ma10;
      v_ma20  := row_stock_transaction_data.ma20;
      v_ma60  := row_stock_transaction_data.ma60;
      v_ma120 := row_stock_transaction_data.ma120;
      v_ma250 := row_stock_transaction_data.ma250;
    
      -- 重置
      v_variance5   := null;
      v_variance10  := null;
      v_variance20  := null;
      v_variance60  := null;
      v_variance120 := null;
      v_variance250 := null;
      v_sum5        := 0;
      v_sum10       := 0;
      v_sum20       := 0;
      v_sum60       := 0;
      v_sum120      := 0;
      v_sum250      := 0;
    
      -- 计算variance5
      if v_ma5 is not null then
        v_rownum := 5;
        for x in cur_std_by_code_date loop
          v_sum5 := v_sum5 + power(x.close_price - v_ma5, 2);
        end loop;
        v_variance5 := 1 / (v_rownum - 1) * v_sum5;
      end if;
    
      -- 计算variance10
      if v_ma10 is not null then
        v_rownum := 10;
        for x in cur_std_by_code_date loop
          v_sum10 := v_sum10 + power(x.close_price - v_ma10, 2);
        end loop;
        v_variance10 := 1 / (v_rownum - 1) * v_sum10;
      end if;
    
      -- 计算variance20
      if v_ma20 is not null then
        v_rownum := 20;
        for x in cur_std_by_code_date loop
          v_sum20 := v_sum20 + power(x.close_price - v_ma20, 2);
        end loop;
        v_variance20 := 1 / (v_rownum - 1) * v_sum20;
      end if;
    
      -- 计算variance60
      if v_ma60 is not null then
        v_rownum := 60;
        for x in cur_std_by_code_date loop
          v_sum60 := v_sum60 + power(x.close_price - v_ma60, 2);
        end loop;
        v_variance60 := 1 / (v_rownum - 1) * v_sum60;
      end if;
    
      -- 计算variance120
      if v_ma120 is not null then
        v_rownum := 120;
        for x in cur_std_by_code_date loop
          v_sum120 := v_sum120 + power(x.close_price - v_ma120, 2);
        end loop;
        v_variance120 := 1 / (v_rownum - 1) * v_sum120;
      end if;
    
      -- 计算variance250
      if v_ma250 is not null then
        v_rownum := 250;
        for x in cur_std_by_code_date loop
          v_sum250 := v_sum250 + power(x.close_price - v_ma250, 2);
        end loop;
        v_variance250 := 1 / (v_rownum - 1) * v_sum250;
      end if;
    
      -- 更新
      update stock_transaction_data t
         set t.variance5   = v_variance5,
             t.variance10  = v_variance10,
             t.variance20  = v_variance20,
             t.variance60  = v_variance60,
             t.variance120 = v_variance120,
             t.variance250 = v_variance250
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    end loop;
    commit;
  end WRITE_VARIANCE_BY_DATE;

  /*------------------------------ judge if MACD of the stock is the gold fork by date -----------------------------------------*/
  procedure SELECT_MACD_UP_BELOW_ZERO(p_date              in varchar2,
                                      p_rate              in number,
                                      p_stock_macd_result out T_TYPE_ARRAY) as
    -- 定义一个stock_transaction_data%rowtype类型的变量
    row_stock_record stock_transaction_data%rowtype;
    -- 定义一个含有5个数值型数据的数组
    type type_array is varray(3) of stock_transaction_data%rowtype;
    array_dif_dea type_array := type_array();
    -- 表示code_字段
    v_code varchar2(10);
    -- define cursor section.返回全部code_字段
    cursor cur_all_stock_code is
      select distinct std.code_
        from stock_transaction_data std
       order by std.code_ asc;
    -- 获取某只股票某一日及之前一日的记录
    cursor cur_last_two_day_stock_info is
      select *
        from (select *
                from stock_transaction_data std
               where std.code_ = v_code
                 and std.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by std.date_ desc)
       where rownum <= 2;
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  begin
    p_stock_macd_result := T_TYPE_ARRAY();
    file_handle         := UTL_FILE.FOPEN('TXTDIR',
                                          'SELECT_MACD_UP_BELOW_ZERO.txt',
                                          'w');
  
    for i in cur_all_stock_code loop
      v_code        := i.code_;
      array_dif_dea := type_array();
      for j in cur_last_two_day_stock_info loop
        row_stock_record.dif   := j.dif;
        row_stock_record.dea   := j.dea;
        row_stock_record.code_ := j.code_;
        array_dif_dea.extend;
        array_dif_dea(array_dif_dea.count) := row_stock_record;
      end loop;
      -- 由于有些股票是在p_date之后才上市交易，因此没有记录，即array_dif_dea.count!=2，所以此处要先判断一下
      if array_dif_dea.count = 2 then
        if array_dif_dea(1).dif > array_dif_dea(1).dea and array_dif_dea(2)
           .dif < array_dif_dea(2).dea then
          if (array_dif_dea(1)
             .dif + array_dif_dea(1).dea + array_dif_dea(2).dif + array_dif_dea(2).dea) / 4 <
             p_rate then
            dbms_output.put_line(array_dif_dea(1).code_);
            p_stock_macd_result.extend;
            p_stock_macd_result(p_stock_macd_result.count) := array_dif_dea(1)
                                                              .code_;
            UTL_FILE.PUT_LINE(file_handle, array_dif_dea(1).code_);
          end if;
        end if;
      end if;
    end loop;
    UTL_FILE.FCLOSE(file_handle);
  end SELECT_MACD_UP_BELOW_ZERO;

  /*-------------------------------- calculate WRITE_CHANGE_RANGE_EX_RIGHT----------------------------------------*/
  procedure WRITE_CHANGE_RANGE_EX_RIGHT as
    -- 表示股票代码
    v_code varchar2(10);
    -- 用来记录某只股票的第一条记录
    row_stock_transaction_data stock_transaction_data%rowtype;
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       order by t.code_ asc;
    -- 返回某一只股票除第一条记录以外的其他记录，并按生序排列
    cursor cur_single_stock_other_record is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ != row_stock_transaction_data.date_
       order by t.date_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
    
      -- 计算某一只股票change_range_ex_right字段的值
      select *
        into row_stock_transaction_data
        from (select *
                from stock_transaction_data t
               where t.code_ = i.code_
               order by t.date_ asc)
       where rownum <= 1;
      for j in cur_single_stock_other_record loop
        update stock_transaction_data t
           set t.change_range_ex_right = round((j.close_price -
                                               row_stock_transaction_data.close_price) /
                                               row_stock_transaction_data.close_price,
                                               4) * 100
         where t.code_ = i.code_
           and t.date_ = j.date_;
        row_stock_transaction_data.close_price := j.close_price;
        commit;
      end loop;
    end loop;
  end WRITE_CHANGE_RANGE_EX_RIGHT;

  /*-------------------------------- calculate WRITE_CHANGE_RANGE_EX_RIGHT_BY_DATE----------------------------------------*/
  procedure WRITE_C_R_E_R_BY_DATE(p_date in varchar2) as
    -- 表示股票代码
    v_code varchar2(10);
    -- 当日的收盘价
    v_current_close_price number;
    -- 前一日收盘价
    v_last_close_price number;
    -- define cursor section.返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data_all t
       order by t.code_ asc;
  begin
    for i in cur_all_stock_code loop
      v_code := i.code_;
      -- 当日的收盘价
      begin
        select t.close_price
          into v_current_close_price
          from stock_transaction_data t
         where t.code_ = v_code
           and t.date_ = to_date(p_date, 'yyyy-mm-dd');
      
      exception
        when NO_DATA_FOUND then
          dbms_output.put_line('STOCK_TRANSACTION_DATA表中，没有code_为【' ||
                               v_code || '】，date_为【' || p_date || '】时的记录');
          continue;
      end;
    
      -- 前一日收盘价
      begin
        select b.close_price
          into v_last_close_price
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ < to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) b
         where rownum <= 1;
      
      exception
        when NO_DATA_FOUND then
          dbms_output.put_line('STOCK_TRANSACTION_DATA表中，没有code_为【' ||
                               v_code || '】，date_为【' || p_date || '】时的记录');
          continue;
      end;
    
      -- 更新change_range_ex_right字段
      update stock_transaction_data
         set change_range_ex_right = round((v_current_close_price -
                                           v_last_close_price) /
                                           v_last_close_price,
                                           4) * 100
       where code_ = i.code_
         and date_ = to_date(p_date, 'yyyy-mm-dd');
    
    end loop;
    commit;
  end WRITE_C_R_E_R_BY_DATE;

  /*--------------------- 计算stock_transaction_data_all表中的布林带 -----------------------*/
  procedure CAL_BOLL is
    -- 股票代码
    v_code varchar(50);
    -- 布林带中轨、上轨、下轨
    v_mb number;
    v_up number;
    v_dn number;
    -- 计算标准差时使用
    v_sum number := 0;
    -- 记录数量
    v_num number;
    -- 某只股票的一条交易记录
    row_stock      stock_transaction_data_all%rowtype;
    row_stock_desc stock_transaction_data_all%rowtype;
    -- 周线级别所有股票的code_
    cursor cur_stock_code is
      select distinct t.code_ from stock_transaction_data t;
    -- 某一只股票的全部交易记录，升序排列
    cursor cur_single_stock is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.ma20 is not null
       order by t.date_ asc;
    -- 某只股票，在某日之后的交易记录，降序排列
    cursor cur_stock_desc is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = row_stock.code_
                 and t.date_ <= row_stock.date_
               order by t.date_ desc)
       where rownum <= 20;
  begin
    open cur_stock_code;
    loop
      fetch cur_stock_code
        into v_code;
      exit when cur_stock_code%notfound;
    
      open cur_single_stock;
      loop
        fetch cur_single_stock
          into row_stock;
        exit when cur_single_stock%notfound;
      
        -- 初始化
        v_up  := 0;
        v_dn  := 0;
        v_sum := 0;
      
        -- 中轨
        select avg(t1.close_price)
          into v_mb
          from (select *
                  from stock_transaction_data t
                 where t.code_ = row_stock.code_
                   and t.date_ <= row_stock.date_
                 order by t.date_ desc) t1
         where rownum <= 20;
        -- v_mb := row_stock.ma20;
      
        -- 如果交易次数不够20，则返回
        select count(*)
          into v_num
          from stock_transaction_data t
         where t.code_ = row_stock.code_
           and t.date_ <= row_stock.date_;
        if v_num < 20 then
          continue;
        end if;
      
        open cur_stock_desc;
        loop
          fetch cur_stock_desc
            into row_stock_desc;
          exit when cur_stock_desc%notfound;
          v_sum := v_sum + power(row_stock_desc.close_price - v_mb, 2);
        end loop;
        close cur_stock_desc;
      
        -- 上轨、下轨
        v_up := v_mb + 2 * sqrt(v_sum / 20);
        v_dn := v_mb - 2 * sqrt(v_sum / 20);
      
        -- 更新记录
        update stock_transaction_data
           set mb = v_mb, up = v_up, dn_ = v_dn
         where code_ = row_stock.code_
           and date_ = row_stock.date_;
      
      end loop;
      close cur_single_stock;
    end loop;
    close cur_stock_code;
    commit;
  end CAL_BOLL;

  /*------- 计算某一日stock_transaction_data_all表中的布林带，必须在计算完均线后运行 ------*/
  procedure CAL_BOLL_BY_DATE(p_date varchar2) is
    -- 股票代码
    v_code varchar(50);
    -- 布林带中轨、上轨、下轨
    v_mb number;
    v_up number;
    v_dn number;
    -- 计算标准差时使用
    v_sum number := 0;
    -- 记录数量
    v_num number;
    -- 某只股票的一条交易记录
    row_stock_desc stock_transaction_data%rowtype;
    -- 股票的code_
    cursor cur_stock_code is
      select distinct t.code_
        from stock_transaction_data t
       where t.date_ = to_date(p_date, 'yyyy-mm-dd');
    -- 某一只股票的全部交易记录，降序排列
    /*cursor cur_single_stock is
    select *
      from stock_transaction_data t
     where t.code_ = v_code
       and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
       and t.ma20 is not null
     order by t.date_ desc;*/
    -- 某只股票，在某日之后的交易记录，降序排列
    cursor cur_stock_desc is
      select *
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc)
       where rownum <= 20;
  begin
    open cur_stock_code;
    loop
      fetch cur_stock_code
        into v_code;
      exit when cur_stock_code%notfound;
    
      /*open cur_single_stock;
      loop
        fetch cur_single_stock
          into row_stock;
        exit when cur_single_stock%notfound;*/
    
      -- 如果交易次数不够20，则返回
      select count(*)
        into v_num
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ <= to_date(p_date, 'yyyy-mm-dd');
      if v_num < 20 then
        continue;
      end if;
    
      -- 初始化
      v_up  := 0;
      v_dn  := 0;
      v_sum := 0;
    
      -- 中轨
      select avg(t1.close_price)
        into v_mb
        from (select *
                from stock_transaction_data t
               where t.code_ = v_code
                 and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
               order by t.date_ desc) t1
       where rownum <= 20;
      -- v_mb := row_stock.ma20;
    
      open cur_stock_desc;
      loop
        fetch cur_stock_desc
          into row_stock_desc;
        exit when cur_stock_desc%notfound;
        v_sum := v_sum + power(row_stock_desc.close_price - v_mb, 2);
      end loop;
      close cur_stock_desc;
    
      -- 上轨、下轨
      v_up := v_mb + 2 * sqrt(v_sum / 20);
      v_dn := v_mb - 2 * sqrt(v_sum / 20);
    
      -- 更新记录
      update stock_transaction_data
         set mb = v_mb, up = v_up, dn_ = v_dn
       where code_ = v_code
         and date_ = to_date(p_date, 'yyyy-mm-dd');
    
      /*end loop;
      close cur_single_stock;*/
    end loop;
    close cur_stock_code;
    commit;
  end CAL_BOLL_BY_DATE;

  /*----------------- calculate the volatility of stock close price with five day,ten day and twenty day -----------------------*/
  procedure CALCULATE_VOLATILITY as
    -- 表示stock_code
    stockCode varchar2(10);
    -- 表示stock_date
    stockDate date;
    -- 表示五日内波动的总数
    fiveDayVolatilitySum number;
    -- 表示十日内波动的总数
    tenDayVolatilitySum number;
    -- 表示二十五日内波动的总数
    twentyDayVolatilitySum number;
    -- 计算五日内波动时的前一条记录
    initFiveDayStockRecord stock_moving_average%rowtype;
    -- 计算十日内波动时的前一条记录
    initTenDayStockRecord stock_moving_average%rowtype;
    -- 计算二十日内波动时的前一条记录
    initTwentyDayStockRecord stock_moving_average%rowtype;
    -- 返回全部stock_code
    cursor allStockCode is
      select distinct t.stock_code
        from stock_moving_average t
       order by t.stock_code asc;
    -- 返回全部stock_date
    cursor allStockDate is
      select distinct t.stock_date
        from stock_moving_average t
       where t.stock_code = stockCode
       order by t.stock_date desc;
    -- 用于获取从指定日期开始五日内的记录，并按降序排列
    cursor nextFiveRecord is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date < initFiveDayStockRecord.stock_date
         and rownum <= 5
       order by t.stock_date desc;
    -- 用于获取从指定日期开始十日内的记录，并按降序排列
    cursor nextTenRecord is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date < initTenDayStockRecord.stock_date
         and rownum <= 10
       order by t.stock_date desc;
    -- 用于获取从指定日期开始二十日内的记录，并按降序排列
    cursor nextTwentyRecord is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date < initTwentyDayStockRecord.stock_date
         and rownum <= 20
       order by t.stock_date desc;
  begin
    --- 计算字段five_day_volatility
    fiveDayVolatilitySum := 0;
    for i in allStockCode loop
      stockCode := i.stock_code;
      for j in allStockDate loop
        stockDate := j.stock_date;
        select *
          into initFiveDayStockRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date <= stockDate
           and rownum <= 1
         order by t.stock_date desc;
        for x in nextFiveRecord loop
          fiveDayVolatilitySum := fiveDayVolatilitySum +
                                  abs((initFiveDayStockRecord.stock_close -
                                      x.stock_close) / x.stock_close);
          if nextFiveRecord%ROWCOUNT = 5 then
            update stock_moving_average t
               set t.five_day_volatility = round(fiveDayVolatilitySum / 5, 4) * 100
             where t.stock_code = stockCode
               and t.stock_date = stockDate;
            commit;
          end if;
          initFiveDayStockRecord.stock_close := x.stock_close;
        end loop;
        fiveDayVolatilitySum := 0;
      end loop;
    end loop;
  
    --- 计算字段ten_day_volatility
    tenDayVolatilitySum := 0;
    for i in allStockCode loop
      stockCode := i.stock_code;
      for j in allStockDate loop
        stockDate := j.stock_date;
        select *
          into initTenDayStockRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date <= stockDate
           and rownum <= 1
         order by t.stock_date desc;
        for x in nextTenRecord loop
          tenDayVolatilitySum := tenDayVolatilitySum +
                                 abs((initTenDayStockRecord.stock_close -
                                     x.stock_close) / x.stock_close);
          if nextTenRecord%ROWCOUNT = 10 then
            update stock_moving_average t
               set t.ten_day_volatility = round(tenDayVolatilitySum / 10, 4) * 100
             where t.stock_code = stockCode
               and t.stock_date = stockDate;
            commit;
          end if;
          initTenDayStockRecord.stock_close := x.stock_close;
        end loop;
        tenDayVolatilitySum := 0;
      end loop;
    end loop;
  
    --- 计算字段twenty_day_volatility
    twentyDayVolatilitySum := 0;
    for i in allStockCode loop
      stockCode := i.stock_code;
      for j in allStockDate loop
        stockDate := j.stock_date;
        select *
          into initTwentyDayStockRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date <= stockDate
           and rownum <= 1
         order by t.stock_date desc;
        for x in nextTwentyRecord loop
          twentyDayVolatilitySum := twentyDayVolatilitySum +
                                    abs((initTwentyDayStockRecord.stock_close -
                                        x.stock_close) / x.stock_close);
          if nextTwentyRecord%ROWCOUNT = 20 then
            update stock_moving_average t
               set t.twenty_day_volatility = round(twentyDayVolatilitySum / 20,
                                                   4) * 100
             where t.stock_code = stockCode
               and t.stock_date = stockDate;
            commit;
          end if;
          initTwentyDayStockRecord.stock_close := x.stock_close;
        end loop;
        twentyDayVolatilitySum := 0;
      end loop;
    end loop;
  end CALCULATE_VOLATILITY;

  /*------------------------- calculate the correlation rate of a certain stock with several index -------------------------------*/
  procedure CALCULATE_CORRELATION_RATE(stockCode in varchar2) as
    -- 用于写文件的变量
    fHandle utl_file.file_type;
    -- 表示上证指数，深证成指，中小板指数和创业板指数
    shIndexCode  constant varchar2(8) := 'sh000001';
    szIndexCode  constant varchar2(8) := 'sz399001';
    zxbIndexCode constant varchar2(8) := 'sz399005';
    cybIndexCode constant varchar2(8) := 'sz399006';
    -- 指定股票的最近10天的数据
    cursor lastFiveStockInfo is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and rownum <= 10
       order by t.stock_date desc;
    lastFiveUpNumber number;
    -- 上证指数，深证成指，中小板指数和创业板指数的最近10天的数据
    cursor lastFiveShIndexInfo is
      select *
        from stock_index t
       where t.code_ = shIndexCode
         and rownum <= 10
       order by t.date_ desc;
    cursor lastFiveSzIndexInfo is
      select *
        from stock_index t
       where t.code_ = szIndexCode
         and rownum <= 10
       order by t.date_ desc;
    cursor lastFiveZxbIndexInfo is
      select *
        from stock_index t
       where t.code_ = zxbIndexCode
         and rownum <= 10
       order by t.date_ desc;
    cursor lastFiveCybIndexInfo is
      select *
        from stock_index t
       where t.code_ = cybIndexCode
         and rownum <= 10
       order by t.date_ desc;
  begin
    for i in lastFiveStockInfo loop
      if i.up_down = 1 then
        lastFiveUpNumber := lastFiveUpNumber + 1;
      end if;
    end loop;
  end CALCULATE_CORRELATION_RATE;

  /*------------------------------------------------ 计算某一日所有股票的涨跌幅度 ------------------------------------------------*/
  procedure CAL_UP_DOWN_PERCENTAGE_BY_DATE(stockDate in varchar2) as
    -- 表示stock_code
    stockCode varchar2(10);
    -- define cursor section.返回全部stock_code
    cursor allStockCode is
      select distinct t.stock_code
        from stock_moving_average t
       order by t.stock_code asc;
    -- 表示某一只股票某一天的记录
    cursor singleStockByDate is
      select *
        from stock_moving_average t
       where t.stock_date = to_date(stockDate, 'yyyy-mm-dd')
         and t.stock_code = stockCode;
    -- 表示某一只股票某一天之前的一天的记录
    cursor singleStockLastDate is
      select *
        from stock_moving_average t
       where t.stock_date < to_date(stockDate, 'yyyy-mm-dd')
         and t.stock_code = stockCode
         and rownum <= 1
       order by t.stock_date desc;
  begin
    for i in allStockCode loop
      stockCode := i.stock_code;
      for j in singleStockByDate loop
        for x in singleStockLastDate loop
          update stock_moving_average t
             set t.today_up_down_percentage = round((j.stock_close -
                                                    x.stock_close) /
                                                    x.stock_close,
                                                    4) * 100
           where t.stock_code = stockCode
             and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
          commit;
        end loop;
      end loop;
    end loop;
  end CAL_UP_DOWN_PERCENTAGE_BY_DATE;

  /*------------ 计算某一日所有股票价格的 -----------*/
  procedure CAL_VOLATILITY_BY_DATE(stockDate in varchar2) as
    -- 表示stock_code
    stockCode varchar2(10);
    -- 表示五日内波动的总数
    fiveDayVolatilitySum number;
    -- 表示十日内波动的总数
    tenDayVolatilitySum number;
    -- 表示二十五日内波动的总数
    twentyDayVolatilitySum number;
    -- 计算五日内波动时的前一条记录
    initFiveDayStockRecord stock_moving_average%rowtype;
    -- 计算十日内波动时的前一条记录
    initTenDayStockRecord stock_moving_average%rowtype;
    -- 计算二十日内波动时的前一条记录
    initTwentyDayStockRecord stock_moving_average%rowtype;
    --  由于存在停牌的股票，所以stockCount(用于记录某一日，某一只股票的数量)等于0时，表示该停牌
    stockCount number;
    -- 返回全部stock_code
    cursor allStockCode is
      select distinct t.stock_code
        from stock_moving_average t
       order by t.stock_code asc;
    -- 用于返回某日之后5天的记录（不包括某日）
    cursor lastFiveRecordByDate is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date < to_date(stockDate, 'yyyy-mm-dd')
         and rownum <= 5
       order by t.stock_date desc;
    -- 用于返回某日之后10天的记录（不包括某日）
    cursor lastTenRecordByDate is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date < to_date(stockDate, 'yyyy-mm-dd')
         and rownum <= 10
       order by t.stock_date desc;
    -- 用于返回某日之后20天的记录（不包括某日）
    cursor lastTwentyRecordByDate is
      select *
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date < to_date(stockDate, 'yyyy-mm-dd')
         and rownum <= 20
       order by t.stock_date desc;
  begin
    --- 计算字段five_day_volatility
    fiveDayVolatilitySum := 0;
    for i in allStockCode loop
      stockCode := i.stock_code;
      select count(*)
        into stockCount
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
      if stockCount <> 0 then
        select *
          into initFiveDayStockRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
        for j in lastFiveRecordByDate loop
          fiveDayVolatilitySum := fiveDayVolatilitySum +
                                  abs((initFiveDayStockRecord.stock_close -
                                      j.stock_close) / j.stock_close);
          if lastFiveRecordByDate%ROWCOUNT = 5 then
            update stock_moving_average t
               set t.five_day_volatility = round(fiveDayVolatilitySum / 5, 4) * 100
             where t.stock_code = stockCode
               and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
            commit;
          end if;
          initFiveDayStockRecord.stock_close := j.stock_close;
        end loop;
        fiveDayVolatilitySum := 0;
      end if;
    end loop;
  
    --- 计算字段ten_day_volatility
    tenDayVolatilitySum := 0;
    for i in allStockCode loop
      stockCode := i.stock_code;
      select count(*)
        into stockCount
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
      if stockCount <> 0 then
        select *
          into initTenDayStockRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
        for j in lastTenRecordByDate loop
          tenDayVolatilitySum := tenDayVolatilitySum +
                                 abs((initTenDayStockRecord.stock_close -
                                     j.stock_close) / j.stock_close);
          if lastTenRecordByDate%ROWCOUNT = 10 then
            update stock_moving_average t
               set t.ten_day_volatility = round(tenDayVolatilitySum / 10, 4) * 100
             where t.stock_code = stockCode
               and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
            commit;
          end if;
          initTenDayStockRecord.stock_close := j.stock_close;
        end loop;
        tenDayVolatilitySum := 0;
      end if;
    end loop;
  
    --- 计算字段twenty_day_volatility
    twentyDayVolatilitySum := 0;
    for i in allStockCode loop
      stockCode := i.stock_code;
      select count(*)
        into stockCount
        from stock_moving_average t
       where t.stock_code = stockCode
         and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
      if stockCount <> 0 then
        select *
          into initTwentyDayStockRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
        for j in lastTwentyRecordByDate loop
          twentyDayVolatilitySum := twentyDayVolatilitySum +
                                    abs((initTwentyDayStockRecord.stock_close -
                                        j.stock_close) / j.stock_close);
          if lastTwentyRecordByDate%ROWCOUNT = 20 then
            update stock_moving_average t
               set t.twenty_day_volatility = round(twentyDayVolatilitySum / 20,
                                                   4) * 100
             where t.stock_code = stockCode
               and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
            commit;
          end if;
          initTwentyDayStockRecord.stock_close := j.stock_close;
        end loop;
        twentyDayVolatilitySum := 0;
      end if;
    end loop;
  end CAL_VOLATILITY_BY_DATE;

  /*---------------------------------------------------- calculate five moving average -------------------------------------------------*/
  procedure CAL_VOLUME_MA5 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示5天成交量的和
      v_five_sum number := 0;
      -- 表示5天成交量的平均值
      v_five_average number := 0;
      -- 定义一个含有5个数值型数据的数组
      type type_array is varray(5) of number;
      array_five type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_volume is
        select std.volume, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_five := type_array();
        for j in cur_all_stock_volume loop
          array_five.extend; -- 扩展数组，扩展一个元素
          array_five(array_five.count) := j.volume;
          if mod(array_five.count, 5) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_five_sum := 0;
            for x in 1 .. array_five.count loop
              -- 求5天成交量的和
              v_five_sum := v_five_sum + array_five(x);
            end loop;
            -- dbms_output.put_line('v_five_sum'||'  :  '||v_five_sum);
            -- 删除数组中的第一个元素，将其与4个元素向前挪一位，并删除下标为5的元素
            for y in 1 .. array_five.count - 1 loop
              array_five(y) := array_five(y + 1);
            end loop;
            array_five.trim;
            -- 5天成交量的平均值
            v_five_average := v_five_sum / 5;
            -- 向所有记录的FIVE列插入5天成交量的平均值
            update stock_transaction_data std
               set std.volume_ma5 = round(v_five_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_five_average'||'  :  '||v_five_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_VOLUME_MA5;
  -- end procedure

  /*---------------------------------------------------- calculate ten moving average -------------------------------------------------*/
  procedure CAL_VOLUME_MA10 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示10天成交量的和
      v_ten_sum number := 0;
      -- 表示10天成交量的平均值
      v_ten_average number := 0;
      -- 定义一个含有10个数值型数据的数组
      type type_array is varray(10) of number;
      array_ten type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_volume is
        select std.volume, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_ten := type_array();
        for j in cur_all_stock_volume loop
          array_ten.extend; -- 扩展数组，扩展一个元素
          array_ten(array_ten.count) := j.volume;
          if mod(array_ten.count, 10) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_ten_sum := 0;
            for x in 1 .. array_ten.count loop
              -- 求10天成交量的和
              v_ten_sum := v_ten_sum + array_ten(x);
            end loop;
            -- dbms_output.put_line('ten_sum'||'  :  '||v_ten_sum);
            -- 删除数组中的第一个元素，将其与9个元素向前挪一位，并删除下标为10的元素
            for y in 1 .. array_ten.count - 1 loop
              array_ten(y) := array_ten(y + 1);
            end loop;
            array_ten.trim;
            -- 10天成交量的平均值
            v_ten_average := v_ten_sum / 10;
            -- 向所有记录的FIVE列插入10天成交量的平均值
            update stock_transaction_data std
               set std.volume_ma10 = round(v_ten_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_ten_average'||'  :  '||v_ten_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_VOLUME_MA10;
  -- end procedure

  /*---------------------------------------------------- calculate twenty moving average -------------------------------------------------*/
  procedure CAL_VOLUME_MA20 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示20天成交量的和
      v_twenty_sum number := 0;
      -- 表示20天成交量的平均值
      v_twenty_average number := 0;
      -- 定义一个含有20个数值型数据的数组
      type type_array is varray(20) of number;
      array_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_al_stock_volume is
        select std.volume, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_twenty := type_array();
        for j in cur_al_stock_volume loop
          array_twenty.extend; -- 扩展数组，扩展一个元素
          array_twenty(array_twenty.count) := j.volume;
          if mod(array_twenty.count, 20) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_twenty_sum := 0;
            for x in 1 .. array_twenty.count loop
              -- 求20天成交量的和
              v_twenty_sum := v_twenty_sum + array_twenty(x);
            end loop;
            -- dbms_output.put_line('v_twenty_sum'||'  :  '||v_twenty_sum);
            -- 删除数组中的第一个元素，将其与19个元素向前挪一位，并删除下标为20的元素
            for y in 1 .. array_twenty.count - 1 loop
              array_twenty(y) := array_twenty(y + 1);
            end loop;
            array_twenty.trim;
            -- 20天成交量的平均值
            v_twenty_average := v_twenty_sum / 20;
            -- 向所有记录的FIVE列插入20天成交量的平均值
            update stock_transaction_data std
               set std.volume_ma20 = round(v_twenty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_twenty_average'||'  :  '||v_twenty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_VOLUME_MA20;
  -- end procedure

  /*---------------------------------------------------- calculate sixty moving average -------------------------------------------------*/
  procedure CAL_VOLUME_MA60 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示60天成交量的和
      v_sixty_sum number := 0;
      -- 表示60天成交量的平均值
      v_sixty_average number := 0;
      -- 定义一个含有60个数值型数据的数组
      type type_array is varray(60) of number;
      array_sixty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_volume is
        select std.volume, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_sixty := type_array();
        for j in cur_all_stock_volume loop
          array_sixty.extend; -- 扩展数组，扩展一个元素
          array_sixty(array_sixty.count) := j.volume;
          if mod(array_sixty.count, 60) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_sixty_sum := 0;
            for x in 1 .. array_sixty.count loop
              -- 求60天收盘价的和
              v_sixty_sum := v_sixty_sum + array_sixty(x);
            end loop;
            -- dbms_output.put_line('v_sixty_sum'||'  :  '||v_sixty_sum);
            -- 删除数组中的第一个元素，将其与59个元素向前挪一位，并删除下标为60的元素
            for y in 1 .. array_sixty.count - 1 loop
              array_sixty(y) := array_sixty(y + 1);
            end loop;
            array_sixty.trim;
            -- 60天成交量的平均值
            v_sixty_average := v_sixty_sum / 60;
            -- 向所有记录的FIVE列插入60天成交量的平均值
            update stock_transaction_data std
               set std.volume_ma60 = round(v_sixty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_sixty_average'||'  :  '||v_sixty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_VOLUME_MA60;
  -- end procedure

  /*---------------------------------------------------- calculate one hundred twenty moving average -------------------------------------------------*/
  procedure CAL_VOLUME_MA120 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示120天成交量的和
      v_one_hundred_twenty_sum number := 0;
      -- 表示120天成交量的平均值
      v_one_hundred_twenty_average number := 0;
      -- 定义一个含有120个数值型数据的数组
      type type_array is varray(120) of number;
      array_one_hundred_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_volume is
        select std.volume, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_one_hundred_twenty := type_array();
        for j in cur_all_stock_volume loop
          array_one_hundred_twenty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_twenty(array_one_hundred_twenty.count) := j.volume;
          if mod(array_one_hundred_twenty.count, 120) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_one_hundred_twenty_sum := 0;
            for x in 1 .. array_one_hundred_twenty.count loop
              -- 求120天成交量的和
              v_one_hundred_twenty_sum := v_one_hundred_twenty_sum +
                                          array_one_hundred_twenty(x);
            end loop;
            -- dbms_output.put_line('v_one_hundred_twenty_sum'||'  :  '||v_one_hundred_twenty_sum);
            -- 删除数组中的第一个元素，将其与119个元素向前挪一位，并删除下标为120的元素
            for y in 1 .. array_one_hundred_twenty.count - 1 loop
              array_one_hundred_twenty(y) := array_one_hundred_twenty(y + 1);
            end loop;
            array_one_hundred_twenty.trim;
            -- 120天成交量的平均值
            v_one_hundred_twenty_average := v_one_hundred_twenty_sum / 120;
            -- 向所有记录的FIVE列插入120天成交量的平均值
            update stock_transaction_data std
               set std.volume_ma120 = round(v_one_hundred_twenty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_one_hundred_twenty_average'||'  :  '||v_one_hundred_twenty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_VOLUME_MA120;
  -- end procedure

  /*---------------------------------------------------- calculate one hundred fifty moving average -------------------------------------------------*/
  procedure CAL_VOLUME_MA250 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示250天成交量的和
      v_one_hundred_fifty_sum number := 0;
      -- 表示250天成交量的平均值
      v_one_hundred_fifty_average number := 0;
      -- 定义一个含有250个数值型数据的数组
      type type_array is varray(250) of number;
      array_one_hundred_fifty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_volume is
        select std.volume, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_one_hundred_fifty := type_array();
        for j in cur_all_stock_volume loop
          array_one_hundred_fifty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_fifty(array_one_hundred_fifty.count) := j.volume;
          if mod(array_one_hundred_fifty.count, 250) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_one_hundred_fifty_sum := 0;
            for x in 1 .. array_one_hundred_fifty.count loop
              -- 求250天收盘价的和
              v_one_hundred_fifty_sum := v_one_hundred_fifty_sum +
                                         array_one_hundred_fifty(x);
            end loop;
            -- dbms_output.put_line('v_one_hundred_fifty_sum'||'  :  '||v_one_hundred_fifty_sum);
            -- 删除数组中的第一个元素，将其与249个元素向前挪一位，并删除下标为250的元素
            for y in 1 .. array_one_hundred_fifty.count - 1 loop
              array_one_hundred_fifty(y) := array_one_hundred_fifty(y + 1);
            end loop;
            array_one_hundred_fifty.trim;
            -- 250天收成交量的平均值
            v_one_hundred_fifty_average := v_one_hundred_fifty_sum / 250;
            -- 向所有记录的FIVE列插入250天成交量的平均值
            update stock_transaction_data std
               set std.volume_ma250 = round(v_one_hundred_fifty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_one_hundred_fifty_average'||'  :  '||v_one_hundred_fifty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_VOLUME_MA250;
  -- end procedure

  /*---------------------------------------------------- write moving average by date -------------------------------------------------*/
  procedure WRITE_VOLUME_MA_BY_DATE(p_date in varchar2) is
    -- 表示stock_code
    v_code varchar2(10);
    -- 表示5天成交量的平均值
    v_five_average number := 0;
    -- 表示10天成交量的平均值
    v_ten_average number := 0;
    -- 表示20天成交量的平均值
    v_twenty_average number := 0;
    -- 表示60天成交量的平均值
    v_sixty_average number := 0;
    -- 表示120天成交量的平均值
    v_one_hundred_twenty_average number := 0;
    -- 表示250天成交量的平均值
    v_two_hundred_fifty_average number := 0;
    -- 表示所有天成交量的平均值
    --infiniteAverage number:=0;
    -- 用于判断某只股票的记录数是否可以计算均线
    v_average_num number;
    -- 计算某只股票，在某个交易日之前的所有交易记录
    cursor cur_stock_row_num_by_date is
      select t.code_, count(*) row_num
        from stock_transaction_data t
       where t.date_ <= to_date(p_date, 'yyyy-mm-dd')
       group by t.code_;
  begin
    for i in cur_stock_row_num_by_date loop
      v_code        := i.code_;
      v_average_num := i.row_num;
    
      /*select
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=5 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=10 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=20 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=60 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=120 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=250 order by t.stock_date desc))
             into fiveAverage,tenAverage,twentyAverage,sixtyAverage,oneHundredTwentyAverage,twoHundredFiftyAverage
      from dual;
      update stock_moving_average t
      set t.five=round(fiveAverage,2),t.ten=round(tenAverage,2),t.twenty=round(twentyAverage,2),
          t.sixty=round(sixtyAverage,2),t.one_hundred_twenty=round(oneHundredTwentyAverage,2),
          t.two_hundred_fifty=round(twoHundredFiftyAverage,2)
      where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
      commit;*/
    
      -- 每只股票都重置如下变量
      v_five_average               := null;
      v_ten_average                := null;
      v_twenty_average             := null;
      v_sixty_average              := null;
      v_one_hundred_twenty_average := null;
      v_two_hundred_fifty_average  := null;
    
      -- 更新所有股票某一天的5日均线
      if v_average_num >= 5 then
        select avg(d.volume)
          into v_five_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 5;
      end if;
    
      -- 更新所有股票某一天的10日均线
      if v_average_num >= 10 then
        select avg(d.volume)
          into v_ten_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 10;
      end if;
    
      -- 更新所有股票某一天的20日均线
      if v_average_num >= 20 then
        select avg(d.volume)
          into v_twenty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 20;
      end if;
    
      -- 更新所有股票某一天的60日均线
      if v_average_num >= 60 then
        select avg(d.volume)
          into v_sixty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 60;
      end if;
    
      -- 更新所有股票某一天的120日均线
      if v_average_num >= 120 then
        select avg(d.volume)
          into v_one_hundred_twenty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 120;
      end if;
    
      -- 更新所有股票某一天的250日均线
      if v_average_num >= 250 then
        select avg(d.volume)
          into v_two_hundred_fifty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 250;
      end if;
    
      update stock_transaction_data t
         set t.volume_ma5   = nvl2(v_five_average,
                                   round(v_five_average, 2),
                                   null),
             t.volume_ma10  = nvl2(v_ten_average,
                                   round(v_ten_average, 2),
                                   null),
             t.volume_ma20  = nvl2(v_twenty_average,
                                   round(v_twenty_average, 2),
                                   null),
             t.volume_ma60  = nvl2(v_sixty_average,
                                   round(v_sixty_average, 2),
                                   null),
             t.volume_ma120 = nvl2(v_one_hundred_twenty_average,
                                   round(v_one_hundred_twenty_average, 2),
                                   null),
             t.volume_ma250 = nvl2(v_two_hundred_fifty_average,
                                   round(v_two_hundred_fifty_average, 2),
                                   null)
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    
    -- 更新所有股票某一天的所有日均线
    /*select avg(d.stock_close) into infiniteAverage from(
                                                                                                                                                                                                                                                                 select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') order by t.stock_date desc
                                                                                                                                                                                                                                                          ) d;
                                                                                                                                                                                                                                                          update stock_moving_average t set t.infinite=round(infiniteAverage,2) where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
                                                                                                                                                                                                                                                          */
    end loop;
    commit;
  end WRITE_VOLUME_MA_BY_DATE;

  /*---------------------------------------------------- calculate five moving average -------------------------------------------------*/
  procedure CAL_TURNOVER_MA5 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示5天成交额的和
      v_five_sum number := 0;
      -- 表示5天成交额的平均值
      v_five_average number := 0;
      -- 定义一个含有5个数值型数据的数组
      type type_array is varray(5) of number;
      array_five type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_turnover is
        select std.turnover, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_five := type_array();
        for j in cur_all_stock_turnover loop
          array_five.extend; -- 扩展数组，扩展一个元素
          array_five(array_five.count) := j.turnover;
          if mod(array_five.count, 5) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_five_sum := 0;
            for x in 1 .. array_five.count loop
              -- 求5天成交额的和
              v_five_sum := v_five_sum + array_five(x);
            end loop;
            -- dbms_output.put_line('v_five_sum'||'  :  '||v_five_sum);
            -- 删除数组中的第一个元素，将其与4个元素向前挪一位，并删除下标为5的元素
            for y in 1 .. array_five.count - 1 loop
              array_five(y) := array_five(y + 1);
            end loop;
            array_five.trim;
            -- 5天成交额的平均值
            v_five_average := v_five_sum / 5;
            -- 向所有记录的FIVE列插入5天成交额的平均值
            update stock_transaction_data std
               set std.turnover_ma5 = round(v_five_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_five_average'||'  :  '||v_five_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_TURNOVER_MA5;
  -- end procedure

  /*---------------------------------------------------- calculate ten moving average -------------------------------------------------*/
  procedure CAL_TURNOVER_MA10 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示10天成交额的和
      v_ten_sum number := 0;
      -- 表示10天成交额的平均值
      v_ten_average number := 0;
      -- 定义一个含有10个数值型数据的数组
      type type_array is varray(10) of number;
      array_ten type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_turnover is
        select std.turnover, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_ten := type_array();
        for j in cur_all_stock_turnover loop
          array_ten.extend; -- 扩展数组，扩展一个元素
          array_ten(array_ten.count) := j.turnover;
          if mod(array_ten.count, 10) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_ten_sum := 0;
            for x in 1 .. array_ten.count loop
              -- 求10天成交额的和
              v_ten_sum := v_ten_sum + array_ten(x);
            end loop;
            -- dbms_output.put_line('ten_sum'||'  :  '||v_ten_sum);
            -- 删除数组中的第一个元素，将其与9个元素向前挪一位，并删除下标为10的元素
            for y in 1 .. array_ten.count - 1 loop
              array_ten(y) := array_ten(y + 1);
            end loop;
            array_ten.trim;
            -- 10天成交额的平均值
            v_ten_average := v_ten_sum / 10;
            -- 向所有记录的FIVE列插入10天成交额的平均值
            update stock_transaction_data std
               set std.turnover_ma10 = round(v_ten_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_ten_average'||'  :  '||v_ten_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_TURNOVER_MA10;
  -- end procedure

  /*---------------------------------------------------- calculate twenty moving average -------------------------------------------------*/
  procedure CAL_TURNOVER_MA20 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示20天成交额的和
      v_twenty_sum number := 0;
      -- 表示20天成交额的平均值
      v_twenty_average number := 0;
      -- 定义一个含有20个数值型数据的数组
      type type_array is varray(20) of number;
      array_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_al_stock_turnover is
        select std.turnover, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_twenty := type_array();
        for j in cur_al_stock_turnover loop
          array_twenty.extend; -- 扩展数组，扩展一个元素
          array_twenty(array_twenty.count) := j.turnover;
          if mod(array_twenty.count, 20) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_twenty_sum := 0;
            for x in 1 .. array_twenty.count loop
              -- 求20天成交额的和
              v_twenty_sum := v_twenty_sum + array_twenty(x);
            end loop;
            -- dbms_output.put_line('v_twenty_sum'||'  :  '||v_twenty_sum);
            -- 删除数组中的第一个元素，将其与19个元素向前挪一位，并删除下标为20的元素
            for y in 1 .. array_twenty.count - 1 loop
              array_twenty(y) := array_twenty(y + 1);
            end loop;
            array_twenty.trim;
            -- 20天成交额的平均值
            v_twenty_average := v_twenty_sum / 20;
            -- 向所有记录的FIVE列插入20天成交额的平均值
            update stock_transaction_data std
               set std.turnover_ma20 = round(v_twenty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_twenty_average'||'  :  '||v_twenty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_TURNOVER_MA20;
  -- end procedure

  /*---------------------------------------------------- calculate sixty moving average -------------------------------------------------*/
  procedure CAL_TURNOVER_MA60 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示60天成交额的和
      v_sixty_sum number := 0;
      -- 表示60天成交额的平均值
      v_sixty_average number := 0;
      -- 定义一个含有60个数值型数据的数组
      type type_array is varray(60) of number;
      array_sixty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_turnover is
        select std.turnover, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_sixty := type_array();
        for j in cur_all_stock_turnover loop
          array_sixty.extend; -- 扩展数组，扩展一个元素
          array_sixty(array_sixty.count) := j.turnover;
          if mod(array_sixty.count, 60) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_sixty_sum := 0;
            for x in 1 .. array_sixty.count loop
              -- 求60天成交额的和
              v_sixty_sum := v_sixty_sum + array_sixty(x);
            end loop;
            -- dbms_output.put_line('v_sixty_sum'||'  :  '||v_sixty_sum);
            -- 删除数组中的第一个元素，将其与59个元素向前挪一位，并删除下标为60的元素
            for y in 1 .. array_sixty.count - 1 loop
              array_sixty(y) := array_sixty(y + 1);
            end loop;
            array_sixty.trim;
            -- 60天成交额的平均值
            v_sixty_average := v_sixty_sum / 60;
            -- 向所有记录的FIVE列插入60天成交额的平均值
            update stock_transaction_data std
               set std.turnover_ma60 = round(v_sixty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_sixty_average'||'  :  '||v_sixty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_TURNOVER_MA60;
  -- end procedure

  /*---------------------------------------------------- calculate one hundred twenty moving average -------------------------------------------------*/
  procedure CAL_TURNOVER_MA120 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示120天成交额的和
      v_one_hundred_twenty_sum number := 0;
      -- 表示120天成交额的平均值
      v_one_hundred_twenty_average number := 0;
      -- 定义一个含有120个数值型数据的数组
      type type_array is varray(120) of number;
      array_one_hundred_twenty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_turnover is
        select std.turnover, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_one_hundred_twenty := type_array();
        for j in cur_all_stock_turnover loop
          array_one_hundred_twenty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_twenty(array_one_hundred_twenty.count) := j.turnover;
          if mod(array_one_hundred_twenty.count, 120) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_one_hundred_twenty_sum := 0;
            for x in 1 .. array_one_hundred_twenty.count loop
              -- 求120天成交额的和
              v_one_hundred_twenty_sum := v_one_hundred_twenty_sum +
                                          array_one_hundred_twenty(x);
            end loop;
            -- dbms_output.put_line('v_one_hundred_twenty_sum'||'  :  '||v_one_hundred_twenty_sum);
            -- 删除数组中的第一个元素，将其与119个元素向前挪一位，并删除下标为120的元素
            for y in 1 .. array_one_hundred_twenty.count - 1 loop
              array_one_hundred_twenty(y) := array_one_hundred_twenty(y + 1);
            end loop;
            array_one_hundred_twenty.trim;
            -- 120天成交额的平均值
            v_one_hundred_twenty_average := v_one_hundred_twenty_sum / 120;
            -- 向所有记录的FIVE列插入120天成交额的平均值
            update stock_transaction_data std
               set std.turnover_ma120 = round(v_one_hundred_twenty_average,
                                              2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_one_hundred_twenty_average'||'  :  '||v_one_hundred_twenty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_TURNOVER_MA120;
  -- end procedure

  /*---------------------------------------------------- calculate one hundred fifty moving average -------------------------------------------------*/
  procedure CAL_TURNOVER_MA250 is
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      v_code varchar2(10);
      -- 表示250天成交额的和
      v_one_hundred_fifty_sum number := 0;
      -- 表示250天成交额的平均值
      v_one_hundred_fifty_average number := 0;
      -- 定义一个含有250个数值型数据的数组
      type type_array is varray(250) of number;
      array_one_hundred_fifty type_array := type_array();
      -- define cursor section.返回全部stock_code
      cursor cur_all_stock_code is
        select distinct std.code_ from stock_transaction_data std;
      cursor cur_all_stock_turnover is
        select std.turnover, std.date_
          from stock_transaction_data std
         where std.code_ = v_code
         order by std.date_ asc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in cur_all_stock_code loop
        v_code := i.code_;
        -- dbms_output.put_line('**************   v_code'||'  :  '||v_code);
        array_one_hundred_fifty := type_array();
        for j in cur_all_stock_turnover loop
          array_one_hundred_fifty.extend; -- 扩展数组，扩展一个元素
          array_one_hundred_fifty(array_one_hundred_fifty.count) := j.turnover;
          if mod(array_one_hundred_fifty.count, 250) = 0 then
            -- dbms_output.put_line('j.date_'||'  :  '||j.date_);
            v_one_hundred_fifty_sum := 0;
            for x in 1 .. array_one_hundred_fifty.count loop
              -- 求250天成交额的和
              v_one_hundred_fifty_sum := v_one_hundred_fifty_sum +
                                         array_one_hundred_fifty(x);
            end loop;
            -- dbms_output.put_line('v_one_hundred_fifty_sum'||'  :  '||v_one_hundred_fifty_sum);
            -- 删除数组中的第一个元素，将其与249个元素向前挪一位，并删除下标为250的元素
            for y in 1 .. array_one_hundred_fifty.count - 1 loop
              array_one_hundred_fifty(y) := array_one_hundred_fifty(y + 1);
            end loop;
            array_one_hundred_fifty.trim;
            -- 250天收成交额的平均值
            v_one_hundred_fifty_average := v_one_hundred_fifty_sum / 250;
            -- 向所有记录的FIVE列插入250天成交额的平均值
            update stock_transaction_data std
               set std.turnover_ma250 = round(v_one_hundred_fifty_average, 2)
             where std.code_ = v_code
               and std.date_ = j.date_;
            -- dbms_output.put_line('v_one_hundred_fifty_average'||'  :  '||v_one_hundred_fifty_average);
          end if;
        end loop;
      end loop;
      commit;
    end;
  end CAL_TURNOVER_MA250;
  -- end procedure

  /*---------------------------------------------------- write moving average by date -------------------------------------------------*/
  procedure WRITE_TURNOVER_MA_BY_DATE(p_date in varchar2) is
    -- 表示stock_code
    v_code varchar2(10);
    -- 表示5天成交额的平均值
    v_five_average number := 0;
    -- 表示10天成交额的平均值
    v_ten_average number := 0;
    -- 表示20天成交额的平均值
    v_twenty_average number := 0;
    -- 表示60天成交额的平均值
    v_sixty_average number := 0;
    -- 表示120天成交额的平均值
    v_one_hundred_twenty_average number := 0;
    -- 表示250天成交额的平均值
    v_two_hundred_fifty_average number := 0;
    -- 表示所有天成交额的平均值
    --infiniteAverage number:=0;
    -- 用于判断某只股票的记录数是否可以计算均线
    v_average_num number;
    -- 计算某只股票，在某个交易日之前的所有交易记录
    cursor cur_stock_row_num_by_date is
      select t.code_, count(*) row_num
        from stock_transaction_data t
       where t.date_ <= to_date(p_date, 'yyyy-mm-dd')
       group by t.code_;
  begin
    for i in cur_stock_row_num_by_date loop
      v_code        := i.code_;
      v_average_num := i.row_num;
    
      /*select
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=5 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=10 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=20 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=60 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=120 order by t.stock_date desc)),
             (select avg(stock_close) from(select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') and rownum<=250 order by t.stock_date desc))
             into fiveAverage,tenAverage,twentyAverage,sixtyAverage,oneHundredTwentyAverage,twoHundredFiftyAverage
      from dual;
      update stock_moving_average t
      set t.five=round(fiveAverage,2),t.ten=round(tenAverage,2),t.twenty=round(twentyAverage,2),
          t.sixty=round(sixtyAverage,2),t.one_hundred_twenty=round(oneHundredTwentyAverage,2),
          t.two_hundred_fifty=round(twoHundredFiftyAverage,2)
      where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
      commit;*/
    
      -- 每只股票都重置如下变量
      v_five_average               := null;
      v_ten_average                := null;
      v_twenty_average             := null;
      v_sixty_average              := null;
      v_one_hundred_twenty_average := null;
      v_two_hundred_fifty_average  := null;
    
      -- 更新所有股票某一天的5日均线
      if v_average_num >= 5 then
        select avg(d.turnover)
          into v_five_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 5;
      end if;
    
      -- 更新所有股票某一天的10日均线
      if v_average_num >= 10 then
        select avg(d.turnover)
          into v_ten_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 10;
      end if;
    
      -- 更新所有股票某一天的20日均线
      if v_average_num >= 20 then
        select avg(d.turnover)
          into v_twenty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 20;
      end if;
    
      -- 更新所有股票某一天的60日均线
      if v_average_num >= 60 then
        select avg(d.turnover)
          into v_sixty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 60;
      end if;
    
      -- 更新所有股票某一天的120日均线
      if v_average_num >= 120 then
        select avg(d.turnover)
          into v_one_hundred_twenty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 120;
      end if;
    
      -- 更新所有股票某一天的250日均线
      if v_average_num >= 250 then
        select avg(d.turnover)
          into v_two_hundred_fifty_average
          from (select *
                  from stock_transaction_data t
                 where t.code_ = v_code
                   and t.date_ <= to_date(p_date, 'yyyy-mm-dd')
                 order by t.date_ desc) d
         where rownum <= 250;
      end if;
    
      update stock_transaction_data t
         set t.turnover_ma5   = nvl2(v_five_average,
                                     round(v_five_average, 2),
                                     null),
             t.turnover_ma10  = nvl2(v_ten_average,
                                     round(v_ten_average, 2),
                                     null),
             t.turnover_ma20  = nvl2(v_twenty_average,
                                     round(v_twenty_average, 2),
                                     null),
             t.turnover_ma60  = nvl2(v_sixty_average,
                                     round(v_sixty_average, 2),
                                     null),
             t.turnover_ma120 = nvl2(v_one_hundred_twenty_average,
                                     round(v_one_hundred_twenty_average, 2),
                                     null),
             t.turnover_ma250 = nvl2(v_two_hundred_fifty_average,
                                     round(v_two_hundred_fifty_average, 2),
                                     null)
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    
    -- 更新所有股票某一天的所有日均线
    /*select avg(d.stock_close) into infiniteAverage from(
                                                                                                                                                                                                                                                                 select * from stock_moving_average t where t.stock_code=stockCode and t.stock_date<=to_date(stockDate,'yyyy-mm-dd') order by t.stock_date desc
                                                                                                                                                                                                                                                          ) d;
                                                                                                                                                                                                                                                          update stock_moving_average t set t.infinite=round(infiniteAverage,2) where t.stock_code=stockCode and t.stock_date=to_date(stockDate,'yyyy-mm-dd');
                                                                                                                                                                                                                                                          */
    end loop;
    commit;
  end WRITE_TURNOVER_MA_BY_DATE;

  /*-------------------------------------------------- 查找某一日价格出现异动的股票 -----------------------------------------------*/
  procedure FIND_ABNORMAL_STOCK(p_date               in varchar2,
                                p_rate               in number,
                                p_stock_result_array out T_STOCK_RESULT_ARRAY) as
    -- 表示code_
    v_code varchar2(10);
    -- T_STOCK_RESULT类型对象。表示出现异动的股票
    type_stock_result T_STOCK_RESULT;
    -- 返回全部stock_code
    cursor cur_all_stock_code is
      select distinct t.code_
        from stock_transaction_data t /*where t.stock_code='sh600094'*/
       order by t.code_ asc;
    -- 某一只股票，某一天的记录
    cursor cur_single_stock_record is
      select *
        from stock_transaction_data t
       where t.code_ = v_code
         and t.date_ = to_date(p_date, 'yyyy-mm-dd');
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  begin
    file_handle          := UTL_FILE.FOPEN('TXTDIR',
                                           'FIND_ABNORMAL_STOCK.txt',
                                           'w');
    p_stock_result_array := T_STOCK_RESULT_ARRAY();
  
    for i in cur_all_stock_code loop
      v_code := i.code_;
      for j in cur_single_stock_record loop
        if cur_single_stock_record%FOUND then
          if j.change_range is not null and
             j.five_day_volatility is not null and
             j.ten_day_volatility is not null and
             j.twenty_day_volatility is not null then
            if abs(j.change_range) >= j.five_day_volatility * p_rate and
               abs(j.change_range) >= j.ten_day_volatility * p_rate and
               abs(j.change_range) >= j.twenty_day_volatility * p_rate then
              dbms_output.put_line(j.code_ || '   ' || j.date_);
            
              type_stock_result := T_STOCK_RESULT(j.code_, j.date_);
              p_stock_result_array.extend;
              p_stock_result_array(p_stock_result_array.count) := type_stock_result;
            
              UTL_FILE.PUT_LINE(file_handle,
                                p_stock_result_array(p_stock_result_array.count)
                                .get_code() || '   ' || p_stock_result_array(p_stock_result_array.count)
                                .get_date());
            end if;
          end if;
        end if;
      end loop;
    end loop;
  end FIND_ABNORMAL_STOCK;

  /*----------------------------------------------- 查找某个交易日MACD最小的股票 -----------------------------------------*/
  procedure FIND_LOWEST_MACD_STOCK(p_stock_date         in varchar2,
                                   p_stock_number       in number,
                                   p_interval_date      in number,
                                   p_stock_result_array out t_stock_result_array) as
    -- 定义数组，用于接收游标的结果集，字段为v_stock_code和v_dif_dea
    type typ_result is record(
      v_stock_code varchar2(20),
      v_dif_dea    number);
    type typ_results is varray(1000) of typ_result;
    results                typ_results;
    CN_BATCH_SIZE_CONSTANT pls_integer := 1000;
  
    -- 表示方法FNC_STOCK_XR返回的结果
    v_fnc_result number := 0;
    -- 表示开始日期
    v_begin_date date;
    -- 表示T_STOCK_RESULT类型
    v_stock_result T_STOCK_RESULT;
    -- 表示需要返回的股票的数量
    v_stock_number number := 0;
  
    -- 获取某个交易日，MACD按照升序排列的股票
    cur_lowest_macd_stock sys_refcursor;
    vc_lowest_macd_stock  varchar2(2000) := 'select t.code_, (t.dif + t.dea) / 2 dif_dea
                                            from stock_transaction_data t
                                           where t.date_ = to_date(:1, ''yyyy-mm-dd'')
                                           order by (t.dif + t.dea) / 2 asc';
  
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type := UTL_FILE.FOPEN('TXTDIR',
                                                     'FIND_LOWEST_MACD_STOCK.txt',
                                                     'w');
  begin
    -- 初始化
    p_stock_result_array := T_STOCK_RESULT_ARRAY();
  
    -- 将当前session缓存cursor的数量设置为200
    execute immediate 'alter session set session_cached_cursors=200';
  
    open cur_lowest_macd_stock for vc_lowest_macd_stock
      using p_stock_date;
    loop
      fetch cur_lowest_macd_stock bulk collect
        into results limit CN_BATCH_SIZE_CONSTANT;
    
      for i in 1 .. results.count loop
        -- 求某只股票从p_stock_date开始向前p_interval_date个交易日的日期
        execute immediate 'select t2.date_
                              from (select t3.date_
                                      from (
                                           select * from (
                                             select *
                                              from stock_transaction_data t
                                             where t.code_ = :1
                                             order by t.date_ desc
                                            ) t1 where rownum <= :2) t3
                                     order by t3.date_ asc) t2
                             where rownum <= 1'
          into v_begin_date
          using results(i).v_stock_code, p_interval_date;
      
        -- 如果v_fnc_result为-1，则表示没有除权
        v_fnc_result := FNC_STOCK_XR(results(i).v_stock_code,
                                     to_char(v_begin_date, 'yyyy-mm-dd'),
                                     p_stock_date);
      
        if v_fnc_result = -1 and v_stock_number < p_stock_number then
          v_stock_result := T_STOCK_RESULT(results(i).v_stock_code || '   ' || results(i)
                                           .v_dif_dea,
                                           to_date(p_stock_date,
                                                   'yyyy-mm-dd'));
          p_stock_result_array.extend;
          p_stock_result_array(p_stock_result_array.count) := v_stock_result;
          v_stock_number := v_stock_number + 1;
        
          UTL_FILE.PUT_LINE(file_handle,
                            p_stock_result_array(p_stock_result_array.count)
                            .get_code() || '   ' || p_stock_result_array(p_stock_result_array.count)
                            .get_date());
        end if;
      end loop;
      exit when results.count < CN_BATCH_SIZE_CONSTANT;
    
    end loop;
  end FIND_LOWEST_MACD_STOCK;

  /*---------------------------------------------- 查找某段时间内涨幅最大的num只股票 ----------------------------------------*/
  procedure FIND_TOP_STOCK(p_begin_date           in varchar2,
                           p_end_date             in varchar2,
                           p_number               in number,
                           p_word_limit_num       number,
                           stock_top_result_array out T_STOCK_TOP_RESULT_ARRAY) as
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type := UTL_FILE.FOPEN('TXTDIR',
                                                     'FIND_TOP_STOCK.txt',
                                                     'w');
  
    -- 查找beginDate至endDate中，按股票的涨幅进行排列，显示STOCK_CODE和涨幅百分比
    cur_top_stock    sys_refcursor;
    vc_cur_top_stock varchar2(1000) := 'select t.code_,
         round((max(t.close_price) - min(t.open_price)) /
               min(t.open_price) * 100,
               2) as rate
         from stock_transaction_data_all t
         where t.date_ between to_date(:1, ''yyyy-mm-dd'') and
               to_date(:2, ''yyyy-mm-dd'')
         group by t.code_
         order by rate desc';
    -- 定义类型typ_top_stock_rec和数组typ_top_stock_arr，用于存储从游标中获取的记录
    type typ_top_stock_rec is record(
      stock_code stock_transaction_data.code_%type,
      rate       number);
    type typ_top_stock_arr is varray(1000) of typ_top_stock_rec;
    top_stock_arr typ_top_stock_arr;
  
    -- 用于限制每次fetch的记录数量
    CN_BATCH_SIZE_CONSTANT pls_integer := 1000;
  
    -- 表示stock_code字段
    v_stock_code varchar2(100);
  
    -- 查询某只股票在某段时间内，最高价出现的那一天。注意：可能这个价格出现的不止一天，因此只取最早的一天
    vc_max_high_date varchar2(1000) := 'select distinct t1.date_
      from stock_transaction_data t1
     where rownum <= 1
       and t1.close_price =
           (select max(t.close_price)
              from stock_transaction_data_all t
             where t.date_ between
                   to_date(:1, ''yyyy-mm-dd'') and
                   to_date(:2, ''yyyy-mm-dd'')
               and t.code_ = :3)
       and t1.date_ between to_date(:4, ''yyyy-mm-dd'') and
           to_date(:5, ''yyyy-mm-dd'')
       and t1.code_ = :6
     order by t1.date_ asc';
    -- 表示某只股票在某个时间内，最高价出现的那一天
    v_stock_high_date date;
  
    -- 查询某只股票在某段时间内，最低价出现的那一天。注意：可能这个价格出现的不止一天，因此只取最晚的一天
    vc_min_low_date varchar2(1000) := 'select distinct t1.date_
        from stock_transaction_data_all t1
       where rownum <= 1
         and t1.open_price =
             (select min(t.open_price)
                from stock_transaction_data_all t
               where t.date_ between
                     to_date(:1, ''yyyy-mm-dd'') and
                     to_date(:2, ''yyyy-mm-dd'')
                 and t.code_ = :3)
         and t1.date_ between to_date(:4, ''yyyy-mm-dd'') and
             to_date(:5, ''yyyy-mm-dd'')
         and t1.code_ = :6
       order by t1.date_ desc';
    -- 表示某只股票在某个时间内，最低价出现的那一天
    v_stock_low_date date;
  
    -- 表示stock_name字段
    v_stock_name varchar2(100);
    -- 表示board_name字段
    v_board_name     varchar2(100); -- T_STOCK_TOP_RESULT类型对象。表示满足条件的股票
    stock_top_result T_STOCK_TOP_RESULT;
    -- 用于表示是否查询到了记录
    num number;
    -- 表示查询到了多少条记录
    record_num number := 1;
  begin
    -- 初始化T_STOCK_TOP_RESULT_ARRAY类型的数组
    stock_top_result_array := T_STOCK_TOP_RESULT_ARRAY();
  
    open cur_top_stock for vc_cur_top_stock
      using p_begin_date, p_end_date;
    loop
      fetch cur_top_stock bulk collect
        into top_stock_arr limit CN_BATCH_SIZE_CONSTANT;
      for i in 1 .. top_stock_arr.count loop
        -- 如果已经到达了p_number个股票的数量，则直接退出存储过程
        if record_num >= p_number then
          exit;
        end if;
      
        v_stock_code := top_stock_arr(i).stock_code;
      
        -- 获取某只股票在某段时间内，最高价出现的那一天
        execute immediate vc_max_high_date
          into v_stock_high_date
          using p_begin_date, p_end_date, v_stock_code, p_begin_date, p_end_date, v_stock_code;
      
        -- 获取某只股票在某段时间内，最低价出现的那一天
        execute immediate vc_min_low_date
          into v_stock_low_date
          using p_begin_date, p_end_date, v_stock_code, p_begin_date, p_end_date, v_stock_code;
      
        -- 要求最高价出现的时间晚于最低价出现的时间。如果最高价出现的时间早于或等于最低价出现的时间，则放弃这只股票
        -- 要求不能有停牌的情况，否则放弃这只股票
        -- 要求不能有过多的一字涨停板的情况，否则放弃这只股票
        if v_stock_high_date > v_stock_low_date and
           fnc_stock_stop(v_stock_code, p_begin_date, p_end_date) = -1 and
           fnc_stock_word_limit(v_stock_code,
                                p_begin_date,
                                p_end_date,
                                p_word_limit_num) = -1 then
        
          -- 有时表STOCK_TRANSACTION_DATA_ALL中有记录，但表STOCK_INFO中没有记录，所以要先判断下numm是否为0
          execute immediate 'select count(t.name_) from STOCK_INFO t where t.code_ = upper(:1)'
            into num
            using top_stock_arr(i).stock_code;
        
          if num != 0 and record_num <= p_number then
            execute immediate 'select t.name_ stock_name, b.name board_name from STOCK_INFO t
                            join board b on b.id=t.board_id
                            where t.code_ = upper(:1)'
              into v_stock_name, v_board_name
              using top_stock_arr(i).stock_code;
          
            stock_top_result := T_STOCK_TOP_RESULT(top_stock_arr(i)
                                                   .stock_code,
                                                   v_stock_name,
                                                   v_board_name,
                                                   top_stock_arr(i).rate);
          
            if stock_top_result is not null then
              stock_top_result_array.extend;
              stock_top_result_array(stock_top_result_array.count) := stock_top_result;
              record_num := record_num + 1;
              UTL_FILE.PUT_LINE(file_handle,
                                stock_top_result_array(stock_top_result_array.count)
                                .get_stock_code() || '   ' || stock_top_result_array(stock_top_result_array.count)
                                .get_stock_name() || '   ' || stock_top_result_array(stock_top_result_array.count)
                                .get_board_name() || '   ' || stock_top_result_array(stock_top_result_array.count)
                                .get_rate());
            end if;
          end if;
        end if;
      end loop;
      exit when top_stock_arr.count < CN_BATCH_SIZE_CONSTANT;
    end loop;
    close cur_top_stock;
  end FIND_TOP_STOCK;

  /*---------------------------------------------- 查找某段时间内跌幅最大的num只股票 ----------------------------------------*/
  procedure FIND_LAST_STOCK(p_begin_date            in varchar2,
                            p_end_date              in varchar2,
                            p_number                in number,
                            stock_last_result_array out T_STOCK_LAST_RESULT_ARRAY) as
    -- 表示stock_code字段
    v_stock_code varchar2(100);
    -- 表示stock_name字段
    v_stock_name varchar2(100);
    -- 表示board_name字段
    v_board_name varchar2(100);
    -- 判断stock_name是否存在
    v_stock_name_exist number;
    -- 判断board_name是否存在
    v_board_name_exist number;
    -- 表示某只股票在某个时间内，最高价出现的那一天
    v_stock_high_date date;
    -- 表示某只股票在某个时间内，最低价出现的那一天
    v_stock_low_date date;
    -- T_STOCK_LAST_RESULT类型对象。表示满足条件的股票
    stock_last_result T_STOCK_LAST_RESULT;
    -- 查找beginDate至endDate中，按股票的跌幅进行排列，显示STOCK_CODE和跌幅百分比
    cursor cur_last_stock is
      select t.code_,
             round((max(t.open_price) - min(t.close_price)) /
                   min(t.open_price) * 100,
                   2) as rate
        from stock_transaction_data t
       where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
       group by t.code_
       order by rate desc;
    -- 查询某只股票在某段时间内，最高价出现的那一天。注意：可能这个价格出现的不止一天，因此只取最晚的一天
    cursor cur_max_high_date is
      select distinct t1.date_
        from stock_transaction_data t1
       where rownum <= 1
         and t1.open_price =
             (select max(t.open_price)
                from stock_transaction_data t
               where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
                     to_date(p_end_date, 'yyyy-mm-dd')
                 and t.code_ = v_stock_code)
         and t1.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
         and t1.code_ = v_stock_code
       order by t1.date_ desc;
    -- 查询某只股票在某段时间内，最低价出现的那一天。注意：可能这个价格出现的不止一天，因此只取最早的一天
    cursor cur_min_low_date is
      select distinct t1.date_
        from stock_transaction_data t1
       where rownum <= 1
         and t1.close_price =
             (select min(t.close_price)
                from stock_transaction_data t
               where t.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
                     to_date(p_end_date, 'yyyy-mm-dd')
                 and t.code_ = v_stock_code)
         and t1.date_ between to_date(p_begin_date, 'yyyy-mm-dd') and
             to_date(p_end_date, 'yyyy-mm-dd')
         and t1.code_ = v_stock_code
       order by t1.date_ asc;
    -- 创建UTL_FILE.file_type对象，用于读写文件
    file_handle UTL_FILE.file_type;
  begin
    file_handle             := UTL_FILE.FOPEN('TXTDIR',
                                              'FIND_LAST_STOCK.txt',
                                              'w');
    stock_last_result_array := T_STOCK_LAST_RESULT_ARRAY();
  
    for i in cur_last_stock loop
      v_stock_code := i.code_;
      -- 获取某只股票在某段时间内，最高价出现的那一天
      for j in cur_max_high_date loop
        v_stock_high_date := j.date_;
      end loop;
      -- 获取某只股票在某段时间内，最低价出现的那一天
      for j in cur_min_low_date loop
        v_stock_low_date := j.date_;
      end loop;
    
      -- 要求最高价出现的时间早于最低价出现的时间。如果最高价出现的时间晚于或等于最低价出现的时间，则放弃这只股票
      -- 当下跌幅度超过了11%，就表示之前有可能是除权
      if v_stock_high_date < v_stock_low_date and
         fnc_stock_xr(v_stock_code, p_begin_date, p_end_date) = -1 then
        /*dbms_output.put_line(i.code_ || '   ' || i.rate);*/
      
        -- 判断stock_name是否存在，如果存在则将其赋给v_stock_name变量
        select count(t.name_)
          into v_stock_name_exist
          from STOCK_INFO t
         where t.code_ = upper(i.code_);
        if v_stock_name_exist <> 0 then
          select t.name_
            into v_stock_name
            from STOCK_INFO t
           where t.code_ = upper(i.code_);
        end if;
      
        -- 判断board_name是否存在，如果存在则将其赋给v_board_name变量
        select count(b.name)
          into v_board_name_exist
          from STOCK_INFO t
          join board b
            on b.id = t.board_id
         where t.code_ = upper(i.code_);
        if v_board_name_exist <> 0 then
          select b.name
            into v_board_name
            from STOCK_INFO t
            join board b
              on b.id = t.board_id
           where t.code_ = upper(i.code_);
        end if;
      
        stock_last_result := T_STOCK_LAST_RESULT(i.code_,
                                                 v_stock_name,
                                                 v_board_name,
                                                 i.rate);
        stock_last_result_array.extend;
        stock_last_result_array(stock_last_result_array.count) := stock_last_result;
        UTL_FILE.PUT_LINE(file_handle,
                          stock_last_result_array(stock_last_result_array.count)
                          .get_stock_code() || '   ' || stock_last_result_array(stock_last_result_array.count)
                          .get_stock_name() || '   ' || stock_last_result_array(stock_last_result_array.count)
                          .get_board_name() || '   -' || stock_last_result_array(stock_last_result_array.count)
                          .get_rate());
      end if;
    end loop;
  end FIND_LAST_STOCK;

  /*---------------------------------------------------- test -------------------------------------------------*/
  procedure TEST(stockDate in varchar2) as
  begin
    ------------------------------------------ standard declare section ---------------------------------------------------
    declare
      -- 表示stock_code
      stockCode varchar2(10);
      -- 某一只股票某一日的记录
      singleStockOneRecord stock_moving_average%rowtype;
      -- define cursor section.返回全部stock_code
      cursor allStockCode is
        select distinct t.stock_code
          from stock_moving_average t
         order by t.stock_code asc;
      -- 某一只股票某一日（不包括这一日）之后所有日期的所有记录
      cursor singleStockRecord is
        select *
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date < to_date(stockDate, 'yyyy-mm-dd')
           and rownum <= 1
         order by t.stock_date desc;
      ------------------------------------------ standard declare section ---------------------------------------------------
    begin
      --------------------------------- standard for loop section --------------------------------------------------------
      for i in allStockCode loop
        stockCode := i.stock_code;
        select *
          into singleStockOneRecord
          from stock_moving_average t
         where t.stock_code = stockCode
           and t.stock_date = to_date(stockDate, 'yyyy-mm-dd');
        for j in singleStockRecord loop
          dbms_output.put_line(j.stock_code || '   11111' ||
                               singleStockOneRecord.today_up_down_percentage);
          dbms_output.put_line(j.stock_date || '   22222' ||
                               j.today_up_down_percentage);
          if j.today_up_down_percentage <> 0 and
             singleStockOneRecord.today_up_down_percentage /
             j.today_up_down_percentage > 2 then
            dbms_output.put_line(j.stock_code);
          end if;
        end loop;
      end loop;
    end;
  end TEST;
  -- end procedure

  PROCEDURE FIND_BOND_OPERATION_STOCK(p_date               IN VARCHAR2,
                                      p_stock_result_array OUT t_stock_result_array) AS
    -- 表示stock_code
    v_code VARCHAR2(50);
    -- 是否是第一条记录
    v_first BOOLEAN := true;
    -- 是否金叉
    v_kd_gold_cross BOOLEAN := false;
    -- ma250是否单调不递减
    v_ma250_not_decrease BOOLEAN := true;
    -- 收盘价是否在(mb+dn)/2之下
    v_close_price_down_mb_dn BOOLEAN := false;
    -- MACD是否金叉
    v_macd_gold_cross BOOLEAN := false;
    -- 表示T_STOCK_RESULT类型
    v_stock_result T_STOCK_RESULT;
    -- 某一只股票的第一条记录
    row_first_stda stock_transaction_data_all%rowtype;
    -- 某一只股票的上一条记录
    row_last_stda stock_transaction_data_all%rowtype;
    -- 查询某一日的全部股票的代码
    CURSOR cur_stock_code IS
      SELECT t.code_
        FROM stock_transaction_data_all t
       WHERE t.date_ = to_date(p_date, 'yyyy-mm-dd');
    -- 查询某一个股票最近2个交易日的记录
    CURSOR cur_two_stda_desc IS
      SELECT *
        FROM (SELECT *
                FROM stock_transaction_data_all t
               WHERE t.code_ = v_code
               ORDER BY t.date_ DESC)
       WHERE rownum <= 2;
    -- 查询某一个股票最近20个交易日的记录
    CURSOR cur_20_stda_desc IS
      SELECT *
        FROM (SELECT *
                FROM stock_transaction_data_all t
               WHERE t.code_ = v_code
               ORDER BY t.date_ DESC)
       WHERE rownum <= 20;
    -- 查询某一个股票最近1个交易日的记录
    CURSOR cur_1_stda_desc IS
      SELECT *
        FROM (SELECT *
                FROM stock_transaction_data_all t
               WHERE t.code_ = v_code
               ORDER BY t.date_ DESC)
       WHERE rownum <= 1;
  BEGIN
    -- 初始化
    p_stock_result_array := T_STOCK_RESULT_ARRAY();
    FOR i IN cur_stock_code LOOP
      v_code                   := i.code_;
      v_first                  := true;
      v_kd_gold_cross          := false;
      v_ma250_not_decrease     := true;
      v_close_price_down_mb_dn := false;
    
      -- 判断是否在当前交易日KD金叉
      FOR i IN cur_two_stda_desc LOOP
        -- 判断是否是第一条记录
        IF v_first = true THEN
          row_first_stda := i;
          v_first        := false;
          CONTINUE;
        END IF;
        -- 如果是第二条记录判断KD是否在当前交易日金叉
        if v_first = false then
          /*IF row_first_stda.k > row_first_stda.d AND i.k <= i.d and*/
          IF row_first_stda.k >= i.k and
             (row_first_stda.k + row_first_stda.d) / 2 <= 50 THEN
            v_kd_gold_cross := true;
          END IF;
        end if;
        if v_kd_gold_cross = false then
          exit;
        end if;
      END LOOP;
    
      -- 判断250日均线是否单调不递减
      v_first := true;
      FOR i IN cur_20_stda_desc LOOP
        -- 判断是否是第一条记录
        IF v_first = true THEN
          row_last_stda := i;
          v_first       := false;
          CONTINUE;
        END IF;
        -- 判断ma250是否单调不递减
        IF i.ma250 <= row_last_stda.ma250 and
           i.ma20 <= row_last_stda.ma20 THEN
          row_last_stda := i;
          CONTINUE;
        ELSE
          v_ma250_not_decrease := false;
          EXIT;
        END IF;
      END LOOP;
    
      -- 判断MACD是否金叉
      /*FOR i IN cur_1_stda_desc LOOP
        IF i.dif >= i.dea THEN
          v_macd_gold_cross := true;
        else
          v_macd_gold_cross := false;
        end if;
      END LOOP;*/
    
      -- 判断收盘价是否在(mb+dn)/2之下
      --    FOR i IN cur_stda_desc
      --    LOOP
      --      IF i.close_price            < (i.mb + i.dn_) / 2 THEN
      --        v_close_price_down_mb_dn := true;
      --      END IF;
      --      v_first := false;
      --      EXIT;
      --    END LOOP;
    
      -- 如果都符合条件，则作为返回值
      IF v_kd_gold_cross = true AND v_ma250_not_decrease = true /*AND
         v_macd_gold_cross = true*/
      /*v_close_price_down_mb_dn = true*/
       THEN
        v_stock_result := T_STOCK_RESULT(v_code,
                                         to_date(p_date, 'yyyy-mm-dd'));
        p_stock_result_array.extend();
        p_stock_result_array(p_stock_result_array.count) := v_stock_result;
        dbms_output.put_line(v_stock_result.code_);
      END IF;
    END LOOP;
  END FIND_BOND_OPERATION_STOCK;

end pkg_stock_transaction_data;
/

