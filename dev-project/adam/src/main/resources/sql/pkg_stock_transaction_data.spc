???create or replace package scott.pkg_stock_transaction_data is
  -- 计算简单移动平均线
  PROCEDURE CALCULATE_FIVE;
  PROCEDURE CALCULATE_TEN;
  PROCEDURE CALCULATE_TWENTY;
  PROCEDURE CALCULATE_SIXTY;
  PROCEDURE CALCULATE_ONEHUNDREDTWENTY;
  PROCEDURE CALCULATE_TWOHUNDREDFIFTY;
  --PROCEDURE CALCULATE_INFINITE;

  -- 插入股票记录
  -- 这个存储过程不再使用了
  procedure WRITE_STOCK_TRANSACTION_DATA(p_date                     in date,
                                         p_code                     in varchar2,
                                         p_open_price               in number,
                                         p_close_price              in number,
                                         p_highest_price            in number,
                                         p_lowest_price             in number,
                                         p_change_amount            in number,
                                         p_change_range             in number,
                                         p_turnover_rate            in number,
                                         p_volume                   in number,
                                         p_turnover                 in number,
                                         p_total_market_value       in number,
                                         p_circulation_market_value in number);

  -- 计算某一天所有股票的简单移动平均线
  PROCEDURE WRITE_MOVING_AVERAGE_BY_DATE(p_date in varchar2);

  -- 计算某一日有哪些股票处于多头排列
  procedure SELECT_BULL_RANK_STOCK(p_date in varchar2, p_num out number);

  TYPE MYCURSOR IS REF CURSOR;
  PROCEDURE TEST(stockDate in varchar2);

  -- 选择股票
  procedure SELECT_REVERSE_STOCK(p_date              in varchar2,
                                 p_percentage        in number,
                                 p_result_stock_code out T_TYPE_ARRAY);
  /*procedure SELECT_STOCK_3(stockDate in varchar2,percentage in number);*/
  /*procedure SELECT_STOCK_WITH_250_SUPPORT(p_date        in varchar2,
  p_near_rate      in number,
  p_down_rate         in number,
  p_stock_result_array out T_STOCK_RESULT_ARRAY);*/
  procedure SELECT_STOCK_WITH_120_SUPPORT(p_date               in varchar2,
                                          p_near_rate          in number,
                                          p_down_rate          in number,
                                          p_stock_result_array out T_STOCK_RESULT_ARRAY);
  procedure SELECT_STOCK_WITH_250_SUPPORT(p_date               in varchar2,
                                          p_near_rate          in number,
                                          p_down_rate          in number,
                                          p_stock_result_array out T_STOCK_RESULT_ARRAY);
  procedure SELECT_MACD_UP_BELOW_ZERO(p_date              in varchar2,
                                      p_rate              in number,
                                      p_stock_macd_result out T_TYPE_ARRAY);
  procedure SELECT_MACD_GOLD_CROSS(p_date              in varchar2,
                                   p_rate              in number,
                                   p_stock_macd_result out T_TYPE_ARRAY);

  -- 计算所有股票当天的收盘价是否大于前一天的收盘价
  -- stock_transaction_data表的up_down字段已经在获取数据时就计算好了，所以现在这个方法用不着了
  procedure WRITE_UP_DOWN;
  procedure WRITE_UP_DOWN_BY_DATE(stockDate in varchar2);

  -- 计算MACD
  procedure WRITE_MACD_INIT;
  procedure WRITE_MACD_EMA;
  procedure WRITE_MACD_DIF;
  procedure WRITE_MACD_DEA;
  procedure WRITE_MACD;
  procedure WRITE_MACD_EMA_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_DIF_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_DEA_BY_DATE(p_date in varchar2);
  procedure WRITE_MACD_BY_DATE(p_date in varchar2);

  -- 计算KD
  procedure WRITE_KD_INIT;
  procedure WRITE_KD_RSV;
  procedure WRITE_KD_K;
  procedure WRITE_KD_D;

  -- 计算每日的kd
  procedure WRITE_KD_BY_DATE_RSV(p_date varchar2);
  procedure WRITE_KD_BY_DATE_K(p_date varchar2);
  procedure WRITE_KD_BY_DATE_D(p_date varchar2);

  -- 计算所有股票的ha和某一日所有股票的ha
  procedure WRITE_HA;
  procedure WRITE_HA_BY_DATE(p_date varchar2);

  -- 计算所有股票的乖离率
  procedure WRITE_BIAS;
  -- 按照日期，计算所有股票在某一日的乖离率
  procedure WRITE_BIAS_BY_DATE(p_date in varchar2);

  -- 计算所有股票的方差
  procedure WRITE_VARIANCE;
  -- 按照日期，计算所有股票在某一日的的方差
  procedure WRITE_VARIANCE_BY_DATE(p_date in varchar2);

  procedure WRITE_CHANGE_RANGE_EX_RIGHT;
  -- WRITE_CHANGE_RANGE_EX_RIGHT_BY_DATE
  procedure WRITE_C_R_E_R_BY_DATE(p_date in varchar2);

  /*--------------------- 计算stock_transaction_data_all表中的布林带 -----------------------*/
  procedure CAL_BOLL;
  /*------- 计算某一日stock_transaction_data_all表中的布林带，必须在计算完均线后运行 ------*/
  procedure CAL_BOLL_BY_DATE(p_date varchar2);

  procedure CALCULATE_VOLATILITY;
  procedure CALCULATE_CORRELATION_RATE(stockCode in varchar2);
  procedure CAL_UP_DOWN_PERCENTAGE_BY_DATE(stockDate in varchar2);
  procedure CAL_VOLATILITY_BY_DATE(stockDate in varchar2);

  -- 计算成交量的简单移动平均线
  PROCEDURE CAL_VOLUME_MA5;
  PROCEDURE CAL_VOLUME_MA10;
  PROCEDURE CAL_VOLUME_MA20;
  PROCEDURE CAL_VOLUME_MA60;
  PROCEDURE CAL_VOLUME_MA120;
  PROCEDURE CAL_VOLUME_MA250;
  -- 计算某一天所有股票的成交量的简单移动平均线
  PROCEDURE WRITE_VOLUME_MA_BY_DATE(p_date in varchar2);

  -- 计算成交额的简单移动平均线
  PROCEDURE CAL_TURNOVER_MA5;
  PROCEDURE CAL_TURNOVER_MA10;
  PROCEDURE CAL_TURNOVER_MA20;
  PROCEDURE CAL_TURNOVER_MA60;
  PROCEDURE CAL_TURNOVER_MA120;
  PROCEDURE CAL_TURNOVER_MA250;
  -- 计算某一天所有股票的成交额的简单移动平均线
  PROCEDURE WRITE_TURNOVER_MA_BY_DATE(p_date in varchar2);

  procedure FIND_ABNORMAL_STOCK(p_date               in varchar2,
                                p_rate               in number,
                                p_stock_result_array out T_STOCK_RESULT_ARRAY);

  -- 查找某个交易日MACD最小的股票
  procedure FIND_LOWEST_MACD_STOCK(p_stock_date         in varchar2,
                                   p_stock_number       in number,
                                   p_interval_date      in number,
                                   p_stock_result_array out t_stock_result_array);

  -- 查找某段时间内涨幅最大的num只股票
  procedure FIND_TOP_STOCK(p_begin_date           in varchar2,
                           p_end_date             in varchar2,
                           p_number               in number,
                           p_word_limit_num       number,
                           stock_top_result_array out T_STOCK_TOP_RESULT_ARRAY);

  -- 查找某段时间内跌幅最大的num只股票
  procedure FIND_LAST_STOCK(p_begin_date            in varchar2,
                            p_end_date              in varchar2,
                            p_number                in number,
                            stock_last_result_array out T_STOCK_LAST_RESULT_ARRAY);

  -- 查找波段操作的股票
  PROCEDURE FIND_BOND_OPERATION_STOCK(p_date               IN VARCHAR2,
                                      p_stock_result_array OUT t_stock_result_array);

end pkg_stock_transaction_data;
/

