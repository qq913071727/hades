???create or replace package scott.PKG_MONTH is

  -- 计算股票月线级别的基础数据
  procedure WRITE_STOCK_MONTH;

  -- 计算某一个月的所有股票的基础数据
  procedure WRITE_STOCK_MONTH_BY_DATE(p_begin_date varchar2,
                                      p_end_date   varchar2);

  -- 计算简单移动平均线
  PROCEDURE WRITE_MA5;
  PROCEDURE WRITE_MA10;
  PROCEDURE WRITE_MA20;
  PROCEDURE WRITE_MA60;
  PROCEDURE WRITE_MA120;
  PROCEDURE WRITE_MA250;

  -- 计算某一个月，所有股票的简单移动平均线
  PROCEDURE WRITE_MA_BY_DATE(p_current_month_begin_date in varchar2,
                             p_current_month_end_date   in varchar2);

  -- 计算KD
  procedure WRITE_MONTH_KD_INIT;
  procedure WRITE_MONTH_KD_RSV;
  procedure WRITE_MONTH_KD_K;
  procedure WRITE_MONTH_KD_D;

  -- 计算某一月的KD
  procedure WRITE_MONTH_KD_BY_DATE_RSV(p_begin_date varchar2,
                                       p_end_date   varchar2);
  procedure WRITE_MONTH_KD_BY_DATE_K(p_begin_date varchar2,
                                     p_end_date   varchar2);
  procedure WRITE_MONTH_KD_BY_DATE_D(p_begin_date varchar2,
                                     p_end_date   varchar2);

  -- 计算MACD
  procedure WRITE_MONTH_MACD_INIT;
  procedure WRITE_MONTH_MACD_EMA;
  procedure WRITE_MONTH_MACD_DIF;
  procedure WRITE_MONTH_MACD_DEA;
  procedure WRITE_MONTH_MACD;

  -- 计算某一月MACD
  procedure WRITE_MONTH_MACD_EMA_BY_DATE(p_stock_month_begin_date in varchar2,
                                         p_stock_month_end_date   in varchar2);
  procedure WRITE_MONTH_MACD_DIF_BY_DATE(p_stock_month_begin_date in varchar2,
                                         p_stock_month_end_date   in varchar2);
  procedure WRITE_MONTH_MACD_DEA_BY_DATE(p_stock_month_begin_date in varchar2,
                                         p_stock_month_end_date   in varchar2);
  procedure WRITE_MONTH_MACD_BY_DATE(p_stock_month_begin_date in varchar2,
                                     p_stock_month_end_date   in varchar2);

end PKG_MONTH;
/

