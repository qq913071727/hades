# coding:utf-8

import matplotlib
import matplotlib.pyplot as plt


class PictureUtil:
    """
    图片工具类
    """

    @staticmethod
    def scatter(data_x_list, data_y_list):
        """
        生成散点图
        :param data_x_list:
        :param data_y_list:
        :return:
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(data_x_list, data_y_list)
        plt.show()

    @staticmethod
    def scatter(data_x_list, data_y_list, s, c):
        """
        生成散点图
        :param c: 颜色
        :param s: 大小
        :param data_x_list:
        :param data_y_list:
        :return:
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(data_x_list, data_y_list, s, c)
        plt.show()
