# coding:utf-8

import json


class JsonUtil:

    @staticmethod
    def object_to_dict(obj):
        """
        将自定义类转换为dict
        """
        d = {}
        d['__class__'] = obj.__class__.__name__
        d['__module__'] = obj.__module__
        d.update(obj.__dict__)
        return d

    @staticmethod
    def dict_to_object(d):
        """
        将dict转换为自定义类
        """
        if '__class__' in d:
            class_name = d.pop('__class__')
            module_name = d.pop('__module__')
            module = __import__(module_name)
            class_ = getattr(module, class_name)
            args = dict((key.encode('ascii'), value) for key, value in d.items())  # get args
            inst = class_(**args)  # create new instance
        else:
            inst = d
        return inst

