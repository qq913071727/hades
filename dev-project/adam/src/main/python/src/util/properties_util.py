# coding:utf-8

class PropertiesUtil:
    """
    properties文件工具类
    """

    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def get_properties(file_name):
        """
        根据指定的properties文件，返回所有的key-value对
        """
        try:
            pro_file = open(file_name, 'r', encoding='utf-8')
            properties = {}
            for line in pro_file:
                if line.find('=') > 0:
                    strs = line.replace('\n', '').split('=')
                    properties[strs[0]] = strs[1]
        except Exception as e:
            raise e
        else:
            pro_file.close()
        return properties

    @staticmethod
    def get_value(file_name, key):
        """
        根据指定的properties文件，返回指定的key的value
        """
        try:
            pro_file = open(file_name, 'r', encoding='utf-8')
            properties = {}
            for line in pro_file:
                if line.find('=') > 0:
                    strs = line.replace('\n', '').split('=')
                    properties[strs[0]] = strs[1]
        except Exception as e:
            raise e
        else:
            pro_file.close()
        return properties[key]
