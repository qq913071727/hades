# coding:utf-8

import os
import tarfile
import os


class FileUtil:

    @staticmethod
    def un_tar(file_name):
        """
        解压tar.gz问及那
        """
        tar = tarfile.open(file_name)
        names = tar.getnames()
        if os.path.isdir(file_name + "_files"):
            pass
        else:
            os.mkdir(file_name + "_files")
        # 因为解压后是很多文件，预先建立同名目录
        for name in names:
            tar.extract(name, file_name + "_files/")
        tar.close()

    @staticmethod
    def read(file_name, mode):
        """
        都指定的文件
        """
        file = open(file_name, mode)
        return file.read()

    @staticmethod
    def write(file_name, mode, content):
        """
        向文件中写入数据
        """
        f = open(file_name, mode)
        f.write(content.encode('utf-8'))


if __name__ == "__main__":
    FileUtil.un_tar('/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz')
