# coding:utf-8

class DataUtil:
    """
    数据工具类
    """

    @staticmethod
    def adjust_data(max_close, min_close, max_index, min_index, current_close):
        """
        归一化。以index为标准，归一化close
        """
        return (max_index - min_index) / (max_close - min_close) * (current_close - min_close)

    @staticmethod
    def max_min(data_list):
        """
        返回列表中的最大值和最小资
        """
        _max = 0
        _min = 0
        for data in data_list:
            if data > _max:
                _max = data
            if data < _min:
                _min = data
        return _max, _min
