# coding:utf-8

import cx_Oracle as cx_Oracle


class OracleManager(object):
    """
    oracle管理器
    """

    # 单态对象
    __oracle_manager = None

    def __new__(cls, *args, **kwargs):
        """
        创建单态对象
        :param args:
        :param kwargs:
        """
        if cls.__oracle_manager is None:
            cls.__oracle_manager = object.__new__(cls)
        return cls.__oracle_manager

    def __init__(self, username, password, url):
        """
        构造函数
        :param username:
        :param password:
        :param url:
        """
        self.username = username
        self.password = password
        self.url = url
        self.conn = None
        self.cursor = None

    def connect(self):
        """
        连接数据库
        :return:
        """
        self.conn = cx_Oracle.connect(self.username, self.password, self.url, encoding='UTF-8')

    def get_cursor(self):
        """
        返回游标
        :return:
        """
        self.cursor = self.conn.cursor()
        return self.cursor

    def batch_insert(self, sql, param_list):
        """
        批量插入数据
        :param sql:
        :param param_list:
        :return:
        """
        self.cursor.executemany(sql, param_list)
        self.conn.commit()

    def cursor_close(self):
        """关闭游标"""
        self.cursor.close()

    def connect_close(self):
        """
        关闭连接
        :return:
        """
        self.conn.close()

    def string_parameter(self, parameter):
        """
        定义string类型的out变量
        """
        return self.cursor.var(cx_Oracle.STRING, parameter)

    def cursor_parameter(self):
        """
        定义cursor类型的out变量
        """
        return self.cursor.var(cx_Oracle.CURSOR)

    def array_parameter(self, array_parameter):
        """
        定义array类型的out变量
        """
        return self.cursor.arrayvar(cx_Oracle.DB_TYPE_OBJECT, array_parameter)