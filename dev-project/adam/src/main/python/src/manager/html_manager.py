#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
from src.manager.log_manager import LogManager

Logger = LogManager.get_logger(__name__)


class HtmlManager:
    """
    解析html文件的管理类
    """

    @staticmethod
    def get(url, headers):
        """
        通过get方法，发送请求
        :param url:
        :param headers:
        :return:
        """
        response = requests.get(url, headers=headers)
        response.encoding = 'utf-8'
        return response.text

    @staticmethod
    def get_soup_from_file(filename, encoding):
        """
        解析html文件，返回soup对象
        :param filename:
        :param encoding:
        :return:
        """
        soup = BeautifulSoup(open(filename, encoding=encoding), features='html.parser')  # features值可为lxml
        return soup

    @staticmethod
    def get_soup_from_url(url):
        """
        解析url，返回soup对象
        :param url:
        :return:
        """
        response = requests.get(url)
        soup = BeautifulSoup(response.text, features='html.parser')
        return soup

    @staticmethod
    def get_element_list(soup, element, attribute_object):
        """
        返回元素列表
        :param soup:
        :param element:
        :param attribute_object:
        :return:
        """
        element_list = soup.find_all(element, attribute_object)
        return element_list
