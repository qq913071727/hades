# coding:utf-8

import pymongo


class MongodbManager(object):
    """
    mongodb管理器
    """

    # 单态对象
    __mongodb_manager = None

    def __new__(cls, *args, **kwargs):
        """
        创建单态对象
        :param args:
        :param kwargs:
        """
        if cls.__mongodb_manager is None:
            cls.__mongodb_manager = object.__new__(cls)
        return cls.__mongodb_manager

    def __init__(self, host, port, database):
        """
        构造函数，连接数据库
        :param host:
        :param port:
        """
        self.host = host
        self.port = port
        self.client = pymongo.MongoClient(host=self.host, port=self.port)
        self.db = self.client[database]

    def get_collection(self, collection):
        """
        获取集合
        :param collection:
        :return:
        """
        return self.db[collection]
