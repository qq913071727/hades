# coding:utf-8

import baostock as bs
import matplotlib.pyplot as plt
from datetime import date

from src.manager.log_manager import LogManager
from src.manager.oracle_manager import OracleManager
from src.config.baostock_bond_index_config import BaostockBondIndexConfig
from src.config.oracle_config import OracleConfig

Logger = LogManager.get_logger(__name__)


class CollectBondIndexHandler:
    """
    通过API接口获取指数K线数据
    """

    def __init__(self) -> None:
        super().__init__()

        self.oracle_manager = OracleManager(OracleConfig.Username, OracleConfig.Password, OracleConfig.Url)
        self.oracle_manager.connect()
        self.cursor = self.oracle_manager.get_cursor()

        self.__code_list = BaostockBondIndexConfig.Code_List
        self.__fields = BaostockBondIndexConfig.Fields
        self.__save_start_date = BaostockBondIndexConfig.Save_Start_Date
        self.__save_end_date = BaostockBondIndexConfig.Save_End_Date
        self.__frequency = BaostockBondIndexConfig.Frequency
        self.__paint_start_date = BaostockBondIndexConfig.Paint_Start_Date
        self.__paint_end_date = BaostockBondIndexConfig.Paint_End_Date

    def save(self):
        """
        通过API接口获取指数K线数据
        """
        # 登录系统
        lg = bs.login()
        # 显示登录返回信息
        Logger.info('登录返回信息[error_code]:' + lg.error_code)
        Logger.info('登录返回信息[error_msg]:' + lg.error_msg)

        # 获取指数(综合指数、规模指数、一级行业指数、二级行业指数、策略指数、成长指数、价值指数、主题指数)K线数据
        # 综合指数，例如：sh.000001 上证指数，sz.399106 深证综指 等；
        # 规模指数，例如：sh.000016 上证50，sh.000300 沪深300，sh.000905 中证500，sz.399001 深证成指等；
        # 一级行业指数，例如：sh.000037 上证医药，sz.399433 国证交运 等；
        # 二级行业指数，例如：sh.000952 300地产，sz.399951 300银行 等；
        # 策略指数，例如：sh.000050 50等权，sh.000982 500等权 等；
        # 成长指数，例如：sz.399376 小盘成长 等；
        # 价值指数，例如：sh.000029 180价值 等；
        # 主题指数，例如：sh.000015 红利指数，sh.000063 上证周期 等；

        # 详细指标参数，参见“历史行情指标参数”章节；“周月线”参数与“日线”参数不同。
        # 周月线指标：date,code,open,high,low,close,volume,amount,adjustflag,turn,pctChg
        for code in self.__code_list:
            rs = bs.query_history_k_data_plus(code, self.__fields, self.__save_start_date,
                                              self.__save_end_date, frequency=self.__frequency)
            Logger.info('API返回结果[error_code]:' + rs.error_code)
            Logger.info('API返回结果[error_msg]:' + rs.error_msg)

            param_list = list()
            while (rs.error_code == '0') & rs.next():
                # 获取一条记录，将记录合并在一起
                row_data = rs.get_row_data()
                print(row_data)
                param_list.append(
                    (date.fromisoformat(row_data[0]), row_data[1][3:], float(row_data[2]), float(row_data[3]),
                     float(row_data[4]), float(row_data[5]),
                     float(row_data[6]), int(row_data[7]), float(row_data[8]), float(row_data[9])))
            self.oracle_manager.batch_insert("insert into bs_bond_index(date_, code_, open_price, highest_price, "
                                             "lowest_price, close_price, pre_close_price, volume, amount, "
                                             "change_percentage) "
                                             "values(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10)", param_list)

        # 登出系统
        bs.logout()

    def paint_all_broken_line_graph(self):
        """
        创建所有指数的折线图
        """
        # 查询000012收盘价，升序排列
        self.cursor.execute(
            "select t.close_price from BS_BOND_INDEX t where t.code_='000012' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        close_price_000012_list = self.cursor.fetchall()
        # 查询日期，升序排列
        self.cursor.execute(
            "select t.date_ from BS_BOND_INDEX t where t.code_='000012' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        date_000012_list = self.cursor.fetchall()
        # 查询000061收盘价，升序排列
        self.cursor.execute(
            "select t.close_price from BS_BOND_INDEX t where t.code_='000061' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        close_price_000061_list = self.cursor.fetchall()
        # 查询日期，升序排列
        self.cursor.execute(
            "select t.date_ from BS_BOND_INDEX t where t.code_='000061' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        date_000061_list = self.cursor.fetchall()
        # 查询000013收盘价，升序排列
        self.cursor.execute(
            "select t.close_price from BS_BOND_INDEX t where t.code_='000013' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        close_price_000013_list = self.cursor.fetchall()
        # 查询日期，升序排列
        self.cursor.execute(
            "select t.date_ from BS_BOND_INDEX t where t.code_='000013' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        date_000013_list = self.cursor.fetchall()
        # 查询000022收盘价，升序排列
        self.cursor.execute(
            "select t.close_price from BS_BOND_INDEX t where t.code_='000022' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        close_price_000022_list = self.cursor.fetchall()
        # 查询日期，升序排列
        self.cursor.execute(
            "select t.date_ from BS_BOND_INDEX t where t.code_='000022' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        date_000022_list = self.cursor.fetchall()

        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号 #有中文出现的情况，需要u'内容'
        plt.title('国债指数')
        plt.plot(date_000012_list, close_price_000012_list, color='green', label='上证国债指数')
        plt.plot(date_000061_list, close_price_000061_list, color='red', label='上证企债30指数')
        plt.plot(date_000013_list, close_price_000013_list, color='skyblue', label='上证企债指数')
        plt.plot(date_000022_list, close_price_000022_list, color='blue', label='上证公司债指数')
        plt.legend()  # 显示图例

        plt.xlabel('日期')
        plt.ylabel('指数')
        plt.show()

    def paint_shanghai_treasury_broken_line_graph(self):
        """
        创建上证国债指数的折线图
        """
        # 查询000012收盘价，升序排列
        self.cursor.execute(
            "select t.close_price from BS_BOND_INDEX t where t.code_='000012' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        close_price_000012_list = self.cursor.fetchall()
        # 查询日期，升序排列
        self.cursor.execute(
            "select t.date_ from BS_BOND_INDEX t where t.code_='000012' "
            "and t.date_ between to_date('" + self.__paint_start_date + "','yyyy-mm-dd') "
                                                                        "and to_date('" + self.__paint_end_date + "','yyyy-mm-dd')"
                                                                                                                  "order by t.date_ asc")
        date_000012_list = self.cursor.fetchall()

        plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
        plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号 #有中文出现的情况，需要u'内容'
        plt.title('国债指数')
        plt.plot(date_000012_list, close_price_000012_list, color='green', label='上证国债指数')
        plt.legend()  # 显示图例

        plt.xlabel('日期')
        plt.ylabel('指数')
        plt.show()
