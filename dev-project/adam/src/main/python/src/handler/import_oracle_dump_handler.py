# coding:utf-8

import os
import subprocess
import time
from src.manager.log_manager import LogManager
from src.config.import_oracle_dump_config import ImportOracledumpConfig

Logger = LogManager.get_logger(__name__)


class ImportOracleDumpHandler:
    """
    循环导入oracle的dump文件
    """

    def __init__(self):
        pass

    def import_oracle_dump(self):
        """
        循环导入oracle的dump文件
        """
        Logger.info("循环导入oracle的dump文件")

        file_list = os.listdir(ImportOracledumpConfig.Path)
        for file in file_list:
            full_path = os.path.join(ImportOracledumpConfig.Path, file)
            result = subprocess.run(['imp', 'scott/tiger', 'file=' + full_path, 'log=dible_db', 'full=y', 'ignore=y'],
                                    capture_output=True, text=True)
            Logger.info('---------------------------------------------------------')
            Logger.info(result.args)
            Logger.info(result.stderr)
            Logger.info(result.stdout)
            time.sleep(1)