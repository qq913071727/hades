# coding:utf-8


import matplotlib.pyplot as plt
from src.manager.oracle_manager import OracleManager
from src.manager.log_manager import LogManager
from src.config.oracle_config import OracleConfig
from src.config.robot2_config import Robot2Config

Logger = LogManager.get_logger(__name__)


class Robot2Handler:

    def __init__(self) -> None:
        super().__init__()

        self.oracle_manager = OracleManager(OracleConfig.Username, OracleConfig.Password, OracleConfig.Url)
        self.oracle_manager.connect()
        self.cursor = self.oracle_manager.get_cursor()

    def paint_macd_gold_cross_date_num_bar_chart(self):
        """
        生成柱状图（macd金叉时，金叉持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（macd金叉时，金叉持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_macd_gold_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        macd_gold_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        macd_gold_cross_date_list = list()
        macd_gold_cross_num_list = list()
        for macd_gold_cross_date_num in macd_gold_cross_date_num_list:
            macd_gold_cross_date_list.append(macd_gold_cross_date_num[0])
            macd_gold_cross_num_list.append(macd_gold_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_gold_cross_date_list, macd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd金叉时，金叉持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_macd_dead_cross_date_num_bar_chart(self):
        """
        生成柱状图（macd死叉时，死叉持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（macd死叉时，死叉持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_macd_dead_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        macd_dead_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        macd_dead_cross_date_list = list()
        macd_dead_cross_num_list = list()
        for macd_dead_cross_date_num in macd_dead_cross_date_num_list:
            macd_dead_cross_date_list.append(macd_dead_cross_date_num[0])
            macd_dead_cross_num_list.append(macd_dead_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_dead_cross_date_list, macd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd死叉时，死叉持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_close_price_ma5_gold_cross_date_num_bar_chart(self):
        """
        生成柱状图（close_price金叉MA5时，死叉持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（close_price金叉MA5时，死叉持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_close_price_ma5_gold_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        close_price_ma5_gold_date_num_list = self.cursor.fetchall()

        # 行列转换
        close_price_ma5_gold_date_list = list()
        close_price_ma5_gold_num_list = list()
        for close_price_ma5_gold_date_num in close_price_ma5_gold_date_num_list:
            close_price_ma5_gold_date_list.append(close_price_ma5_gold_date_num[0])
            close_price_ma5_gold_num_list.append(close_price_ma5_gold_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_date_list, close_price_ma5_gold_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price金叉MA5时，死叉持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_close_price_ma5_dead_cross_date_num_bar_chart(self):
        """
        生成柱状图（close_price死叉MA5时，死叉持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（close_price死叉MA5时，死叉持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_close_price_ma5_dead_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        close_price_ma5_dead_date_num_list = self.cursor.fetchall()

        # 行列转换
        close_price_ma5_dead_date_list = list()
        close_price_ma5_dead_num_list = list()
        for close_price_ma5_dead_date_num in close_price_ma5_dead_date_num_list:
            close_price_ma5_dead_date_list.append(close_price_ma5_dead_date_num[0])
            close_price_ma5_dead_num_list.append(close_price_ma5_dead_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_dead_date_list, close_price_ma5_dead_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price死叉MA5时，死叉持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_hei_kin_ashi_up_down_date_num_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi上升趋势时，上升趋势持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi上升趋势时，上升趋势持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_hei_kin_ashi_up_down t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        hei_kin_ashi_up_down_date_num_list = self.cursor.fetchall()

        # 行列转换
        hei_kin_ashi_up_down_date_list = list()
        hei_kin_ashi_up_down_num_list = list()
        for hei_kin_ashi_up_down_date_num in hei_kin_ashi_up_down_date_num_list:
            hei_kin_ashi_up_down_date_list.append(hei_kin_ashi_up_down_date_num[0])
            hei_kin_ashi_up_down_num_list.append(hei_kin_ashi_up_down_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_up_down_date_list, hei_kin_ashi_up_down_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi上升趋势时，上升趋势持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_hei_kin_ashi_down_up_date_num_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi下跌趋势时，下跌趋势持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi下跌趋势时，下跌趋势持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_hei_kin_ashi_down_up t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        hei_kin_ashi_down_up_date_num_list = self.cursor.fetchall()

        # 行列转换
        hei_kin_ashi_down_up_date_list = list()
        hei_kin_ashi_down_up_num_list = list()
        for hei_kin_ashi_down_up_date_num in hei_kin_ashi_down_up_date_num_list:
            hei_kin_ashi_down_up_date_list.append(hei_kin_ashi_down_up_date_num[0])
            hei_kin_ashi_down_up_num_list.append(hei_kin_ashi_down_up_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_down_up_date_list, hei_kin_ashi_down_up_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi下跌趋势时，下跌趋势持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_kd_gold_cross_date_num_bar_chart(self):
        """
        生成柱状图（kd金叉时，金叉持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（kd金叉时，金叉持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_kd_gold_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        kd_gold_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        kd_gold_cross_date_list = list()
        kd_gold_cross_num_list = list()
        for kd_gold_cross_date_num in kd_gold_cross_date_num_list:
            kd_gold_cross_date_list.append(kd_gold_cross_date_num[0])
            kd_gold_cross_num_list.append(kd_gold_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_gold_cross_date_list, kd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd金叉时，金叉持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_kd_dead_cross_date_num_bar_chart(self):
        """
        生成柱状图（kd死叉时，死叉持续天数与交易次数的关系）
        """
        Logger.info('生成柱状图（kd死叉时，死叉持续天数与交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_kd_dead_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "/*and t1.profit_loss>0*/ and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        kd_dead_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        kd_dead_cross_date_list = list()
        kd_dead_cross_num_list = list()
        for kd_dead_cross_date_num in kd_dead_cross_date_num_list:
            kd_dead_cross_date_list.append(kd_dead_cross_date_num[0])
            kd_dead_cross_num_list.append(kd_dead_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_dead_cross_date_list, kd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd死叉时，死叉持续天数与交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_macd_gold_cross_profit_date_num_bar_chart(self):
        """
        生成柱状图（macd金叉时，金叉持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（macd金叉时，金叉持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_macd_gold_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        macd_gold_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        macd_gold_cross_date_list = list()
        macd_gold_cross_num_list = list()
        for macd_gold_cross_date_num in macd_gold_cross_date_num_list:
            macd_gold_cross_date_list.append(macd_gold_cross_date_num[0])
            macd_gold_cross_num_list.append(macd_gold_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_gold_cross_date_list, macd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd金叉时，金叉持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_macd_dead_cross_profit_date_num_bar_chart(self):
        """
        生成柱状图（macd死叉时，死叉持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（macd死叉时，死叉持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_macd_dead_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        macd_dead_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        macd_dead_cross_date_list = list()
        macd_dead_cross_num_list = list()
        for macd_dead_cross_date_num in macd_dead_cross_date_num_list:
            macd_dead_cross_date_list.append(macd_dead_cross_date_num[0])
            macd_dead_cross_num_list.append(macd_dead_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_dead_cross_date_list, macd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd死叉时，死叉持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_close_price_ma5_gold_cross_profit_date_num_bar_chart(self):
        """
        生成柱状图（close_price金叉MA5时，死叉持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（close_price金叉MA5时，死叉持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_close_price_ma5_gold_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        close_price_ma5_gold_date_num_list = self.cursor.fetchall()

        # 行列转换
        close_price_ma5_gold_date_list = list()
        close_price_ma5_gold_num_list = list()
        for close_price_ma5_gold_date_num in close_price_ma5_gold_date_num_list:
            close_price_ma5_gold_date_list.append(close_price_ma5_gold_date_num[0])
            close_price_ma5_gold_num_list.append(close_price_ma5_gold_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_date_list, close_price_ma5_gold_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price金叉MA5时，死叉持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_close_price_ma5_dead_cross_profit_date_num_bar_chart(self):
        """
        生成柱状图（close_price死叉MA5时，死叉持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（close_price死叉MA5时，死叉持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_close_price_ma5_dead_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        close_price_ma5_dead_date_num_list = self.cursor.fetchall()

        # 行列转换
        close_price_ma5_dead_date_list = list()
        close_price_ma5_dead_num_list = list()
        for close_price_ma5_dead_date_num in close_price_ma5_dead_date_num_list:
            close_price_ma5_dead_date_list.append(close_price_ma5_dead_date_num[0])
            close_price_ma5_dead_num_list.append(close_price_ma5_dead_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_dead_date_list, close_price_ma5_dead_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price死叉MA5时，死叉持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_hei_kin_ashi_up_down_profit_date_num_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi上升趋势时，上升趋势持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi上升趋势时，上升趋势持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_hei_kin_ashi_up_down t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        hei_kin_ashi_up_down_date_num_list = self.cursor.fetchall()

        # 行列转换
        hei_kin_ashi_up_down_date_list = list()
        hei_kin_ashi_up_down_num_list = list()
        for hei_kin_ashi_up_down_date_num in hei_kin_ashi_up_down_date_num_list:
            hei_kin_ashi_up_down_date_list.append(hei_kin_ashi_up_down_date_num[0])
            hei_kin_ashi_up_down_num_list.append(hei_kin_ashi_up_down_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_up_down_date_list, hei_kin_ashi_up_down_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi上升趋势时，上升趋势持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_hei_kin_ashi_down_up_profit_date_num_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi下跌趋势时，下跌趋势持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi下跌趋势时，下跌趋势持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_hei_kin_ashi_down_up t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        hei_kin_ashi_down_up_date_num_list = self.cursor.fetchall()

        # 行列转换
        hei_kin_ashi_down_up_date_list = list()
        hei_kin_ashi_down_up_num_list = list()
        for hei_kin_ashi_down_up_date_num in hei_kin_ashi_down_up_date_num_list:
            hei_kin_ashi_down_up_date_list.append(hei_kin_ashi_down_up_date_num[0])
            hei_kin_ashi_down_up_num_list.append(hei_kin_ashi_down_up_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_down_up_date_list, hei_kin_ashi_down_up_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi下跌趋势时，下跌趋势持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_kd_gold_cross_profit_date_num_bar_chart(self):
        """
        生成柱状图（kd金叉时，金叉持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（kd金叉时，金叉持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_kd_gold_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.buy_date and t1.sell_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        kd_gold_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        kd_gold_cross_date_list = list()
        kd_gold_cross_num_list = list()
        for kd_gold_cross_date_num in kd_gold_cross_date_num_list:
            kd_gold_cross_date_list.append(kd_gold_cross_date_num[0])
            kd_gold_cross_num_list.append(kd_gold_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_gold_cross_date_list, kd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd金叉时，金叉持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_kd_dead_cross_profit_date_num_bar_chart(self):
        """
        生成柱状图（kd死叉时，死叉持续天数与获利交易次数的关系）
        """
        Logger.info('生成柱状图（kd死叉时，死叉持续天数与获利交易次数的关系）')

        # 准备数据
        parameter = {'begin_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                     'end_date': Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                     'transaction_number': Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number}
        self.cursor.execute("select t3.date_num, count(*) "
                            "from (select t1.id, count(t.id_) date_num "
                            "from mdl_kd_dead_cross t1 "
                            "join stock_index t on t.date_ "
                            "between t1.sell_date and t1.buy_date "
                            "and t1.profit_loss>0 and t.code_='000001' "
                            "where t.date_ between to_date(:begin_date, 'yyyy-mm-dd') and to_date(:end_date, 'yyyy-mm-dd') "
                            "group by t1.id) t3 "
                            "group by t3.date_num "
                            "having count(*)>=:transaction_number "
                            "order by t3.date_num asc", parameter)
        kd_dead_cross_date_num_list = self.cursor.fetchall()

        # 行列转换
        kd_dead_cross_date_list = list()
        kd_dead_cross_num_list = list()
        for kd_dead_cross_date_num in kd_dead_cross_date_num_list:
            kd_dead_cross_date_list.append(kd_dead_cross_date_num[0])
            kd_dead_cross_num_list.append(kd_dead_cross_date_num[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_dead_cross_date_list, kd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd死叉时，死叉持续天数与获利交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_macd_gold_cross_transaction_number_bar_chart(self):
        """
        生成柱状图（macd死叉持续天数与之后的金叉获利的交易次数的关系）
        """
        Logger.info('生成柱状图（macd死叉持续天数与之后的金叉获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_macd_g_c_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        macd_dead_cross_date_list = list()
        macd_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            macd_dead_cross_date_list.append(result[0])
            macd_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_dead_cross_date_list, macd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd死叉持续天数与之后的金叉获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_macd_dead_cross_transaction_number_bar_chart(self):
        """
        生成柱状图（macd金叉持续天数与之后的死叉获利的交易次数的关系）
        """
        Logger.info('生成柱状图（macd金叉持续天数与之后的死叉获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_macd_d_c_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        macd_gold_cross_date_list = list()
        macd_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            macd_gold_cross_date_list.append(result[0])
            macd_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_gold_cross_date_list, macd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd金叉持续天数与之后的死叉获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_close_price_ma5_gold_cross_transaction_number_bar_chart(self):
        """
        生成柱状图（close_price死叉ma5持续天数与之后的金叉获利的交易次数的关系）
        """
        Logger.info('生成柱状图（close_price死叉ma5持续天数与之后的金叉获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_c_p_ma5_g_c_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        close_price_ma5_gold_cross_date_list = list()
        close_price_ma5_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            close_price_ma5_gold_cross_date_list.append(result[0])
            close_price_ma5_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_cross_date_list, close_price_ma5_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price死叉ma5持续天数与之后的金叉获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_close_price_ma5_dead_cross_transaction_number_bar_chart(self):
        """
        生成柱状图（close_price金叉ma5持续天数与之后的死叉获利的交易次数的关系）
        """
        Logger.info('生成柱状图（close_price金叉ma5持续天数与之后的死叉获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_c_p_ma5_d_c_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        close_price_ma5_gold_cross_date_list = list()
        close_price_ma5_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            close_price_ma5_gold_cross_date_list.append(result[0])
            close_price_ma5_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_cross_date_list, close_price_ma5_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price金叉ma5持续天数与之后的死叉获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_hei_kin_ashi_up_down_transaction_number_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi下跌趋势持续天数与之后的上涨趋势获利的交易次数的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi下跌趋势持续天数与之后的上涨趋势获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_h_k_a_u_d_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        hei_kin_ashi_up_down_date_list = list()
        hei_kin_ashi_up_down_num_list = list()
        for result in result_cursor.getvalue():
            hei_kin_ashi_up_down_date_list.append(result[0])
            hei_kin_ashi_up_down_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_up_down_date_list, hei_kin_ashi_up_down_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi下跌趋势持续天数与之后的上涨趋势获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_hei_kin_ashi_down_up_transaction_number_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi上涨趋势持续天数与之后的下跌趋势获利的交易次数的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi上涨趋势持续天数与之后的下跌趋势获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_h_k_a_d_u_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        hei_kin_ashi_down_up_date_list = list()
        hei_kin_ashi_down_up_num_list = list()
        for result in result_cursor.getvalue():
            hei_kin_ashi_down_up_date_list.append(result[0])
            hei_kin_ashi_down_up_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_down_up_date_list, hei_kin_ashi_down_up_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi上涨趋势持续天数与之后的下跌趋势获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_kd_gold_cross_transaction_number_bar_chart(self):
        """
        生成柱状图（kd死叉持续天数与之后的金叉获利的交易次数的关系）
        """
        Logger.info('生成柱状图（kd死叉持续天数与之后的金叉获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_kd_g_c_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        kd_gold_cross_date_list = list()
        kd_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            kd_gold_cross_date_list.append(result[0])
            kd_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_gold_cross_date_list, kd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd死叉持续天数与之后的金叉获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_kd_dead_cross_transaction_number_bar_chart(self):
        """
        生成柱状图（kd金叉持续天数与之后的死叉获利的交易次数的关系）
        """
        Logger.info('生成柱状图（kd金叉持续天数与之后的死叉获利的交易次数的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_kd_d_c_interval_t_n",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        kd_dead_cross_date_list = list()
        kd_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            kd_dead_cross_date_list.append(result[0])
            kd_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_dead_cross_date_list, kd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd金叉持续天数与之后的死叉获利的交易次数的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("交易次数")
        # 显示
        plt.show()

    def paint_date_num_before_macd_gold_cross_success_rate_bar_chart(self):
        """
        生成柱状图（macd金叉获利的交易之前的那个macd死叉的持续天数与这次金叉的成功率的关系）
        """
        Logger.info('生成柱状图（macd金叉获利的交易之前的那个macd死叉的持续天数与这次金叉的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_macd_g_c_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        macd_dead_cross_date_list = list()
        macd_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            macd_dead_cross_date_list.append(result[0])
            macd_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_dead_cross_date_list, macd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd金叉获利的交易之前的那个macd死叉的持续天数与这次金叉的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_macd_dead_cross_success_rate_bar_chart(self):
        """
        生成柱状图（macd死叉获利的交易之前的那个macd金叉的持续天数与这次死叉的成功率的关系）
        """
        Logger.info('生成柱状图（macd死叉获利的交易之前的那个macd金叉的持续天数与这次死叉的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_macd_d_c_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        macd_gold_cross_date_list = list()
        macd_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            macd_gold_cross_date_list.append(result[0])
            macd_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_gold_cross_date_list, macd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd死叉获利的交易之前的那个macd金叉的持续天数与这次死叉的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_close_price_ma5_gold_cross_success_rate_bar_chart(self):
        """
        生成柱状图（close_price金叉ma5获利的交易之前的那个close_price死叉ma5的持续天数与这次金叉的成功率的关系）
        """
        Logger.info('生成柱状图（close_price金叉ma5获利的交易之前的那个close_price死叉ma5的持续天数与这次金叉的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_c_p_ma5_g_c_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        close_price_ma5_gold_cross_date_list = list()
        close_price_ma5_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            close_price_ma5_gold_cross_date_list.append(result[0])
            close_price_ma5_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_cross_date_list, close_price_ma5_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price金叉ma5获利的交易之前的那个close_price死叉ma5的持续天数与这次金叉的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_close_price_ma5_dead_cross_success_rate_bar_chart(self):
        """
        生成柱状图（close_price死叉ma5获利的交易之前的那个close_price金叉ma5的持续天数与这次死叉的成功率的关系）
        """
        Logger.info('生成柱状图（close_price死叉ma5获利的交易之前的那个close_price金叉ma5的持续天数与这次死叉的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_c_p_ma5_d_c_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        close_price_ma5_gold_cross_date_list = list()
        close_price_ma5_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            close_price_ma5_gold_cross_date_list.append(result[0])
            close_price_ma5_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_cross_date_list, close_price_ma5_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price死叉ma5获利的交易之前的那个close_price金叉ma5的持续天数与这次死叉的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_hei_kin_ashi_up_down_success_rate_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi上升趋势获利的交易之前的那个hei_kin_ashi下跌趋势的持续天数与这次上升趋势的成功率的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi上升趋势获利的交易之前的那个hei_kin_ashi下跌趋势的持续天数与这次上升趋势的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_h_k_a_u_d_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        hei_kin_ashi_up_down_date_list = list()
        hei_kin_ashi_up_down_num_list = list()
        for result in result_cursor.getvalue():
            hei_kin_ashi_up_down_date_list.append(result[0])
            hei_kin_ashi_up_down_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_up_down_date_list, hei_kin_ashi_up_down_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi上升趋势获利的交易之前的那个hei_kin_ashi下跌趋势的持续天数与这次上升趋势的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_hei_kin_ashi_down_up_success_rate_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi下跌趋势获利的交易之前的那个hei_kin_ashi上升趋势的持续天数与这次下跌趋势的成功率的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi下跌趋势获利的交易之前的那个hei_kin_ashi上升趋势的持续天数与这次下跌趋势的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_h_k_a_d_u_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        hei_kin_ashi_down_up_date_list = list()
        hei_kin_ashi_down_up_num_list = list()
        for result in result_cursor.getvalue():
            hei_kin_ashi_down_up_date_list.append(result[0])
            hei_kin_ashi_down_up_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_down_up_date_list, hei_kin_ashi_down_up_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi下跌趋势获利的交易之前的那个hei_kin_ashi上升趋势的持续天数与这次下跌趋势的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_kd_gold_cross_success_rate_bar_chart(self):
        """
        生成柱状图（kd金叉获利的交易之前的那个kd死叉的持续天数与这次金叉的成功率的关系）
        """
        Logger.info('生成柱状图（kd金叉获利的交易之前的那个kd死叉的持续天数与这次金叉的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_kd_g_c_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        kd_gold_cross_date_list = list()
        kd_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            kd_gold_cross_date_list.append(result[0])
            kd_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_gold_cross_date_list, kd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd金叉获利的交易之前的那个kd死叉的持续天数与这次金叉的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_kd_dead_cross_success_rate_bar_chart(self):
        """
        生成柱状图（kd死叉获利的交易之前的那个kd金叉的持续天数与这次死叉的成功率的关系）
        """
        Logger.info('生成柱状图（kd死叉获利的交易之前的那个kd金叉的持续天数与这次死叉的成功率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_kd_d_c_interval_s_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transact_Number,
                                            result_cursor])
        kd_dead_cross_date_list = list()
        kd_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            kd_dead_cross_date_list.append(result[0])
            kd_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_dead_cross_date_list, kd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd死叉获利的交易之前的那个kd金叉的持续天数与这次死叉的成功率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("成功率")
        # 显示
        plt.show()

    def paint_date_num_before_macd_gold_cross_profit_bar_chart(self):
        """
        生成柱状图（macd金叉之前的死叉持续持续天数与金叉平均收益率的关系）
        """
        Logger.info('生成柱状图（macd金叉之前的死叉持续持续天数与金叉平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_macd_g_c_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        macd_gold_cross_date_list = list()
        macd_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            macd_gold_cross_date_list.append(result[0])
            macd_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_gold_cross_date_list, macd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd金叉之前的死叉持续持续天数与金叉平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_macd_dead_cross_profit_bar_chart(self):
        """
        生成柱状图（macd死叉之前的金叉持续持续天数与死叉平均收益率的关系）
        """
        Logger.info('生成柱状图（macd死叉之前的金叉持续持续天数与死叉平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_macd_d_c_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        macd_dead_cross_date_list = list()
        macd_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            macd_dead_cross_date_list.append(result[0])
            macd_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(macd_dead_cross_date_list, macd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("macd死叉之前的金叉持续持续天数与死叉平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_close_price_ma5_gold_cross_profit_bar_chart(self):
        """
        生成柱状图（close_price金叉ma5之前的死叉持续持续天数与金叉平均收益率的关系）
        """
        Logger.info('生成柱状图（close_price金叉ma5之前的死叉持续持续天数与金叉平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_cp_ma5_gc_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        close_price_ma5_gold_cross_date_list = list()
        close_price_ma5_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            close_price_ma5_gold_cross_date_list.append(result[0])
            close_price_ma5_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_gold_cross_date_list, close_price_ma5_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price金叉ma5之前的死叉持续持续天数与金叉平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_close_price_ma5_dead_cross_profit_bar_chart(self):
        """
        生成柱状图（close_price死叉ma5之前的金叉持续持续天数与死叉平均收益率的关系）
        """
        Logger.info('生成柱状图（close_price死叉ma5之前的金叉持续持续天数与死叉平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_cp_ma5_dc_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        close_price_ma5_dead_cross_date_list = list()
        close_price_ma5_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            close_price_ma5_dead_cross_date_list.append(result[0])
            close_price_ma5_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(close_price_ma5_dead_cross_date_list, close_price_ma5_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("close_price死叉ma5之前的金叉持续持续天数与死叉平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_hei_kin_ashi_up_down_profit_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi上升趋势之前的下跌趋势持续天数与上升趋势平均收益率的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi上升趋势之前的下跌趋势持续天数与上升趋势平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_h_k_a_u_d_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        hei_kin_ashi_up_down_date_list = list()
        hei_kin_ashi_up_down_num_list = list()
        for result in result_cursor.getvalue():
            hei_kin_ashi_up_down_date_list.append(result[0])
            hei_kin_ashi_up_down_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_up_down_date_list, hei_kin_ashi_up_down_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi上升趋势之前的下跌趋势持续天数与上升趋势平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_hei_kin_ashi_down_up_profit_bar_chart(self):
        """
        生成柱状图（hei_kin_ashi下跌趋势之前的上升趋势持续天数与下跌趋势平均收益率的关系）
        """
        Logger.info('生成柱状图（hei_kin_ashi下跌趋势之前的上升趋势持续天数与下跌趋势平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_h_k_a_d_u_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        hei_kin_ashi_down_up_date_list = list()
        hei_kin_ashi_down_up_num_list = list()
        for result in result_cursor.getvalue():
            hei_kin_ashi_down_up_date_list.append(result[0])
            hei_kin_ashi_down_up_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(hei_kin_ashi_down_up_date_list, hei_kin_ashi_down_up_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("hei_kin_ashi下跌趋势之前的上升趋势持续天数与下跌趋势平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_kd_gold_cross_profit_bar_chart(self):
        """
        生成柱状图（kd金叉之前的死叉持续天数与金叉平均收益率的关系）
        """
        Logger.info('生成柱状图（kd金叉之前的死叉持续天数与金叉平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_kd_g_c_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        kd_gold_cross_date_list = list()
        kd_gold_cross_num_list = list()
        for result in result_cursor.getvalue():
            kd_gold_cross_date_list.append(result[0])
            kd_gold_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_gold_cross_date_list, kd_gold_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd金叉之前的死叉持续天数与金叉平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()

    def paint_date_num_before_kd_dead_cross_profit_bar_chart(self):
        """
        生成柱状图（kd死叉之前的金叉持续天数与死叉平均收益率的关系）
        """
        Logger.info('生成柱状图（kd死叉之前的金叉持续天数与死叉平均收益率的关系）')

        result_cursor = self.oracle_manager.cursor_parameter()
        result_list = self.cursor.callproc("pkg_interval.find_kd_d_c_interval_p_r",
                                           [Robot2Config.Gold_Cross_Dead_Cross_Interval_Begin_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_End_Time,
                                            Robot2Config.Gold_Cross_Dead_Cross_Interval_Transaction_Number,
                                            result_cursor])
        kd_dead_cross_date_list = list()
        kd_dead_cross_num_list = list()
        for result in result_cursor.getvalue():
            kd_dead_cross_date_list.append(result[0])
            kd_dead_cross_num_list.append(result[1])

        plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置字体为SimHei显示中文\n",
        plt.rc('font', size=14)  # 设置图中字号大小\n",
        plt.figure(figsize=(6, 4))  # 设置画布\n",

        # 画图，plt.bar()可以画柱状图
        plt.bar(kd_dead_cross_date_list, kd_dead_cross_num_list, width=0.2)  # 绘制柱状图\n",
        # 设置图片名称
        plt.title("kd死叉之前的金叉持续天数与死叉平均收益率的关系")
        # 设置x轴标签名
        plt.xlabel("天数")
        # 设置y轴标签名
        plt.ylabel("平均收益率")
        # 显示
        plt.show()
