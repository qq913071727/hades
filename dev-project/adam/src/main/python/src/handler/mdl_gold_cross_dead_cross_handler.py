# coding:utf-8

from src.manager.log_manager import LogManager
from src.manager.oracle_manager import OracleManager
from src.config.oracle_config import OracleConfig
from src.config.mdl_gold_cross_dead_cross_config import MdlGoldCrossDeadCrossConfig
from src.util.data_util import DataUtil
from pylab import *

Logger = LogManager.get_logger(__name__)


class MdlGoldCrossDeadCrossHandler:

    def __init__(self) -> None:
        super().__init__()

        self.oracle_manager = OracleManager(OracleConfig.Username, OracleConfig.Password, OracleConfig.Url)
        self.oracle_manager.connect()
        self.cursor = self.oracle_manager.get_cursor()

        # 上证指数（000001），取stock_index表的date_和close_price字段，按照date_升序排列
        self.cursor.execute("select t.date_, t.close_price from stock_index t "
                            "where t.date_ between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t.code_='000001' "
                            "order by t.date_ asc" % (
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        self.stock_index_close_price_list = self.cursor.fetchall()

        # 上证指数（000001），取某段时间内的最大收盘价和最小收盘价
        self.cursor.execute("select max(t.close_price), min(t.close_price) from stock_index t "
                            "where t.date_ between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t.code_='000001'" % (
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        self.stock_index_max_min_close_price_list = self.cursor.fetchall()

        # 上证指数的日期和收盘价
        self.date_list = list()
        self.close_price_list = list()
        for stock_index_close_price in self.stock_index_close_price_list:
            self.date_list.append(stock_index_close_price[0])
            self.close_price_list.append(stock_index_close_price[1])
        self.date_list_length = range(len(self.date_list))

        # 上证指数的最大值和最小值
        self.stock_index_max_close_price = self.stock_index_max_min_close_price_list[0][0]
        self.stock_index_min_close_price = self.stock_index_max_min_close_price_list[0][1]

    def paint_macd_gold_cross_analysis_line_chart(self):
        """
        生成折线图（与macd金叉相关的收盘价、成功率、交易次数图、平均dif、平均dea）
        """
        Logger.info('生成折线图（与macd金叉相关的收盘价、成功率、交易次数图、平均dif、平均dea）')

        # 从表mdl_macd_gold_cross中取bug_date、成功率、交易次数、平均收益率，按buy_date升序排列
        self.cursor.execute("select t.buy_date, "
                            "(select count(*) from mdl_macd_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.profit_loss>0 and t2.buy_date=t.buy_date)/"
                            "(select count(*) from mdl_macd_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.buy_date=t.buy_date)*100, "
                            "(select count(*) from mdl_macd_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.buy_date=t.buy_date), "
                            "(select avg(t2.profit_loss) from mdl_macd_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.buy_date=t.buy_date) "
                            "from mdl_macd_gold_cross t "
                            "where t.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
                            "group by t.buy_date "
                            "order by t.buy_date asc" % (
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        mdl_macd_gold_cross_success_rate_list = self.cursor.fetchall()

        # 取某段时间内，dif和dea的平均值、最大值、最小值
        self.cursor.execute(
            "select t.date_, avg(t.dif)*%s, avg(t.dea)*%s, max(t.dif), min(t.dif), max(t.dea), min(t.dea) "
            "from stock_transaction_data_all t "
            "where t.date_ between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
            "group by t.date_ "
            "order by t.date_ asc" % (
                MdlGoldCrossDeadCrossConfig.Macd_Multiple_Weight, MdlGoldCrossDeadCrossConfig.Macd_Multiple_Weight,
                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        stock_transaction_data_average_max_min_dif_dea_list = self.cursor.fetchall()

        # macd金叉的成功率、交易次数、平均收益率
        macd_gold_cross_success_rate_list = list()
        macd_gold_cross_number_list = list()
        macd_gold_cross_average_profit_rate_list = list()
        date_match = False
        for date in self.date_list:
            for mdl_macd_gold_cross_success_rate in mdl_macd_gold_cross_success_rate_list:
                if date == mdl_macd_gold_cross_success_rate[0]:
                    date_match = True
                    macd_gold_cross_success_rate_list.append(mdl_macd_gold_cross_success_rate[1])
                    macd_gold_cross_number_list.append(mdl_macd_gold_cross_success_rate[2])
                    macd_gold_cross_average_profit_rate_list.append(mdl_macd_gold_cross_success_rate[3])
                    break
            if date_match is False:
                macd_gold_cross_success_rate_list.append(0)
                macd_gold_cross_number_list.append(0)
                macd_gold_cross_average_profit_rate_list.append(0)
            date_match = False

        # dif和dea平均值
        average_dif_list = list()
        average_dea_list = list()
        for stock_transaction_data_average_max_min_dif_dea in stock_transaction_data_average_max_min_dif_dea_list:
            average_dif_list.append(stock_transaction_data_average_max_min_dif_dea[1])
            average_dea_list.append(stock_transaction_data_average_max_min_dif_dea[2])

        # 成功率、交易次、平均收益率、dif、dea的最大值和最小值
        max_success_rate, min_success_rate = DataUtil.max_min(macd_gold_cross_success_rate_list)
        max_macd_gold_cross_number, min_macd_gold_cross_number = DataUtil.max_min(macd_gold_cross_number_list)
        max_average_profit_rate, min_average_profit_rate = DataUtil.max_min(macd_gold_cross_average_profit_rate_list)
        max_average_dif = stock_transaction_data_average_max_min_dif_dea_list[0][3]
        min_average_dif = stock_transaction_data_average_max_min_dif_dea_list[0][4]
        max_average_dea = stock_transaction_data_average_max_min_dif_dea_list[0][5]
        min_average_dea = stock_transaction_data_average_max_min_dif_dea_list[0][6]

        # 将收盘价、成功率、交易次、平均收益率、dif、de归一化
        adjust_close_price_list = list()
        adjust_macd_gold_cross_success_rate_list = list()
        adjust_macd_gold_cross_number_list = list()
        adjust_average_profit_rate_list = list()
        adjust_average_dif_list = list()
        adjust_average_dea_list = list()
        for close_price in self.close_price_list:
            adjust_close_price_list.append(
                DataUtil.adjust_data(self.stock_index_max_close_price, self.stock_index_min_close_price,
                                     max_macd_gold_cross_number, min_macd_gold_cross_number,
                                     close_price))
        for macd_gold_cross_success_rate in macd_gold_cross_success_rate_list:
            adjust_macd_gold_cross_success_rate_list.append(
                DataUtil.adjust_data(max_success_rate, min_success_rate, max_macd_gold_cross_number,
                                     min_macd_gold_cross_number,
                                     macd_gold_cross_success_rate))
        for macd_gold_cross_average_profit_rate in macd_gold_cross_average_profit_rate_list:
            adjust_average_profit_rate_list.append(
                DataUtil.adjust_data(max_average_profit_rate, min_average_profit_rate, max_macd_gold_cross_number,
                                     min_macd_gold_cross_number,
                                     macd_gold_cross_average_profit_rate))
        for macd_gold_cross_number in macd_gold_cross_number_list:
            adjust_macd_gold_cross_number_list.append(
                DataUtil.adjust_data(max_macd_gold_cross_number, min_macd_gold_cross_number, max_macd_gold_cross_number,
                                     min_macd_gold_cross_number,
                                     macd_gold_cross_number))
        for average_dif in average_dif_list:
            adjust_average_dif_list.append(
                DataUtil.adjust_data(max_average_dif, min_average_dif, max_macd_gold_cross_number,
                                     min_macd_gold_cross_number,
                                     average_dif))
        for average_dea in average_dea_list:
            adjust_average_dea_list.append(
                DataUtil.adjust_data(max_average_dea, min_average_dea, max_macd_gold_cross_number,
                                     min_macd_gold_cross_number,
                                     average_dea))

        # 画图
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        plt.plot(self.date_list_length, adjust_close_price_list, color='green', label='收盘价')
        plt.plot(self.date_list_length, adjust_macd_gold_cross_success_rate_list, color='red', label='成功率')
        plt.plot(self.date_list_length, macd_gold_cross_number_list, color='skyblue', label='交易次数')
        plt.plot(self.date_list_length, adjust_average_profit_rate_list, color='black', label='平均收益率')
        plt.plot(self.date_list_length, adjust_average_dif_list, color='fuchsia', label='平均dif')
        plt.plot(self.date_list_length, adjust_average_dea_list, color='darkgoldenrod', label='平均dea')
        plt.legend()  # 让图例生效
        plt.xlabel("日期")  # X轴标签
        plt.ylabel("收盘价、成功率、交易次数图、平均dif、平均dea")  # Y轴标签
        plt.title("与macd金叉相关的收盘价、成功率、交易次数图、平均dif、平均dea")  # 标题

        plt.show()

    def paint_macd_dead_cross_analysis_line_chart(self):
        """
        生成折线图（与macd死叉相关的收盘价、成功率、交易次数图、平均dif、平均dea）
        """
        Logger.info('生成折线图（与macd死叉相关的收盘价、成功率、交易次数图、平均dif、平均dea）')

        # 从表mdl_macd_dead_cross中取sell_date、成功率、交易次数、平均收益率，按sell_date升序排列
        self.cursor.execute("select t.sell_date, "
                            "(select count(*) from mdl_macd_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.profit_loss>0 and t2.sell_date=t.sell_date)/"
                            "(select count(*) from mdl_macd_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.sell_date=t.sell_date)*100, "
                            "(select count(*) from mdl_macd_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.sell_date=t.sell_date), "
                            "(select avg(t2.profit_loss) from mdl_macd_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.sell_date=t.sell_date) "
                            "from mdl_macd_dead_cross t "
                            "where t.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
                            "group by t.sell_date "
                            "order by t.sell_date asc" % (
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        mdl_macd_dead_cross_success_rate_list = self.cursor.fetchall()

        # 取某段时间内，dif和dea的平均值、最大值、最小值
        self.cursor.execute(
            "select t.date_, avg(t.dif)*%s, avg(t.dea)*%s, max(t.dif), min(t.dif), max(t.dea), min(t.dea) "
            "from stock_transaction_data_all t "
            "where t.date_ between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
            "group by t.date_ "
            "order by t.date_ asc" % (
                MdlGoldCrossDeadCrossConfig.Macd_Multiple_Weight, MdlGoldCrossDeadCrossConfig.Macd_Multiple_Weight,
                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        stock_transaction_data_average_max_min_dif_dea_list = self.cursor.fetchall()

        # macd死叉的成功率、交易次数、平均收益率
        macd_dead_cross_success_rate_list = list()
        macd_dead_cross_number_list = list()
        macd_dead_cross_average_profit_rate_list = list()
        date_match = False
        for date in self.date_list:
            for mdl_macd_dead_cross_success_rate in mdl_macd_dead_cross_success_rate_list:
                if date == mdl_macd_dead_cross_success_rate[0]:
                    date_match = True
                    macd_dead_cross_success_rate_list.append(mdl_macd_dead_cross_success_rate[1])
                    macd_dead_cross_number_list.append(mdl_macd_dead_cross_success_rate[2])
                    macd_dead_cross_average_profit_rate_list.append(mdl_macd_dead_cross_success_rate[3])
                    break
            if date_match is False:
                macd_dead_cross_success_rate_list.append(0)
                macd_dead_cross_number_list.append(0)
                macd_dead_cross_average_profit_rate_list.append(0)
            date_match = False

        # dif和dea平均值
        average_dif_list = list()
        average_dea_list = list()
        for stock_transaction_data_average_max_min_dif_dea in stock_transaction_data_average_max_min_dif_dea_list:
            average_dif_list.append(stock_transaction_data_average_max_min_dif_dea[1])
            average_dea_list.append(stock_transaction_data_average_max_min_dif_dea[2])

        # 成功率、交易次、平均收益率、dif、dea的最大值和最小值
        max_success_rate, min_success_rate = DataUtil.max_min(macd_dead_cross_success_rate_list)
        max_macd_dead_cross_number, min_macd_dead_cross_number = DataUtil.max_min(macd_dead_cross_number_list)
        max_average_profit_rate, min_average_profit_rate = DataUtil.max_min(macd_dead_cross_average_profit_rate_list)
        max_average_dif = stock_transaction_data_average_max_min_dif_dea_list[0][3]
        min_average_dif = stock_transaction_data_average_max_min_dif_dea_list[0][4]
        max_average_dea = stock_transaction_data_average_max_min_dif_dea_list[0][5]
        min_average_dea = stock_transaction_data_average_max_min_dif_dea_list[0][6]

        # 将收盘价、成功率、交易次、平均收益率、dif、de归一化
        adjust_close_price_list = list()
        adjust_macd_dead_cross_success_rate_list = list()
        adjust_macd_dead_cross_number_list = list()
        adjust_average_profit_rate_list = list()
        adjust_average_dif_list = list()
        adjust_average_dea_list = list()
        for close_price in self.close_price_list:
            adjust_close_price_list.append(
                DataUtil.adjust_data(self.stock_index_max_close_price, self.stock_index_min_close_price,
                                     max_macd_dead_cross_number, min_macd_dead_cross_number,
                                     close_price))
        for macd_dead_cross_success_rate in macd_dead_cross_success_rate_list:
            adjust_macd_dead_cross_success_rate_list.append(
                DataUtil.adjust_data(max_success_rate, min_success_rate, max_macd_dead_cross_number,
                                     min_macd_dead_cross_number,
                                     macd_dead_cross_success_rate))
        for macd_dead_cross_average_profit_rate in macd_dead_cross_average_profit_rate_list:
            adjust_average_profit_rate_list.append(
                DataUtil.adjust_data(max_average_profit_rate, min_average_profit_rate, max_macd_dead_cross_number,
                                     min_macd_dead_cross_number,
                                     macd_dead_cross_average_profit_rate))
        for macd_dead_cross_number in macd_dead_cross_number_list:
            adjust_macd_dead_cross_number_list.append(
                DataUtil.adjust_data(max_macd_dead_cross_number, min_macd_dead_cross_number, max_macd_dead_cross_number,
                                     min_macd_dead_cross_number,
                                     macd_dead_cross_number))
        for average_dif in average_dif_list:
            adjust_average_dif_list.append(
                DataUtil.adjust_data(max_average_dif, min_average_dif, max_macd_dead_cross_number,
                                     min_macd_dead_cross_number,
                                     average_dif))
        for average_dea in average_dea_list:
            adjust_average_dea_list.append(
                DataUtil.adjust_data(max_average_dea, min_average_dea, max_macd_dead_cross_number,
                                     min_macd_dead_cross_number,
                                     average_dea))

        # 画图
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        plt.plot(self.date_list_length, adjust_close_price_list, color='green', label='收盘价')
        plt.plot(self.date_list_length, adjust_macd_dead_cross_success_rate_list, color='red', label='成功率')
        plt.plot(self.date_list_length, macd_dead_cross_number_list, color='skyblue', label='交易次数')
        plt.plot(self.date_list_length, adjust_average_profit_rate_list, color='black', label='平均收益率')
        plt.plot(self.date_list_length, adjust_average_dif_list, color='fuchsia', label='平均dif')
        plt.plot(self.date_list_length, adjust_average_dea_list, color='darkgoldenrod', label='平均dea')
        plt.legend()  # 让图例生效
        plt.xlabel("日期")  # X轴标签
        plt.ylabel("收盘价、成功率、交易次数图、平均dif、平均dea")  # Y轴标签
        plt.title("与macd死叉相关的收盘价、成功率、交易次数图、平均dif、平均dea")  # 标题

        plt.show()

    def paint_close_price_ma5_gold_cross_analysis_line_chart(self):
        """
        生成折线图（与close_price金叉ma5相关的收盘价、成功率、交易次数图、平均dif、平均dea）
        """
        Logger.info('生成折线图（与close_price金叉ma5相关的收盘价、成功率、交易次数图、平均dif、平均dea）')

        # 从表mdl_close_price_ma5_gold_cross中取bug_date、成功率、交易次数、平均收益率，按buy_date升序排列
        self.cursor.execute("select t.buy_date, "
                            "(select count(*) from mdl_close_price_ma5_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.profit_loss>0 and t2.buy_date=t.buy_date)/"
                            "(select count(*) from mdl_close_price_ma5_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.buy_date=t.buy_date)*100, "
                            "(select count(*) from mdl_close_price_ma5_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.buy_date=t.buy_date), "
                            "(select avg(t2.profit_loss) from mdl_close_price_ma5_gold_cross t2 "
                            "where t2.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.buy_date=t.buy_date) "
                            "from mdl_close_price_ma5_gold_cross t "
                            "where t.buy_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
                            "group by t.buy_date "
                            "order by t.buy_date asc" % (
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        mdl_close_price_ma5_gold_cross_success_rate_list = self.cursor.fetchall()

        # 取某段时间内，close_price和ma5的平均值、最大值、最小值
        self.cursor.execute(
            "select t.date_, avg(t.close_price)*%s, avg(t.ma5)*%s, max(t.close_price), min(t.close_price), "
            "max(t.ma5), min(t.ma5) "
            "from stock_transaction_data_all t "
            "where t.date_ between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
            "group by t.date_ "
            "order by t.date_ asc" % (
                MdlGoldCrossDeadCrossConfig.Close_Price_Multiple_Weight,
                MdlGoldCrossDeadCrossConfig.Close_Price_Multiple_Weight,
                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        stock_transaction_data_average_max_min_close_price_ma5_list = self.cursor.fetchall()

        # close_price金叉ma5的成功率、交易次数、平均收益率
        close_price_ma5_gold_cross_success_rate_list = list()
        close_price_ma5_gold_cross_number_list = list()
        close_price_ma5_gold_cross_average_profit_rate_list = list()
        date_match = False
        for date in self.date_list:
            for mdl_close_price_ma5_gold_cross_success_rate in mdl_close_price_ma5_gold_cross_success_rate_list:
                if date == mdl_close_price_ma5_gold_cross_success_rate[0]:
                    date_match = True
                    close_price_ma5_gold_cross_success_rate_list.append(mdl_close_price_ma5_gold_cross_success_rate[1])
                    close_price_ma5_gold_cross_number_list.append(mdl_close_price_ma5_gold_cross_success_rate[2])
                    close_price_ma5_gold_cross_average_profit_rate_list.append(
                        mdl_close_price_ma5_gold_cross_success_rate[3])
                    break
            if date_match is False:
                close_price_ma5_gold_cross_success_rate_list.append(0)
                close_price_ma5_gold_cross_number_list.append(0)
                close_price_ma5_gold_cross_average_profit_rate_list.append(0)
            date_match = False

        # close_price和ma5平均值
        average_close_price_list = list()
        average_ma5_list = list()
        for stock_transaction_data_average_max_min_close_price_ma5 in stock_transaction_data_average_max_min_close_price_ma5_list:
            average_close_price_list.append(stock_transaction_data_average_max_min_close_price_ma5[1])
            average_ma5_list.append(stock_transaction_data_average_max_min_close_price_ma5[2])

        # 成功率、交易次、平均收益率、close_price、ma5的最大值和最小值
        max_success_rate, min_success_rate = DataUtil.max_min(close_price_ma5_gold_cross_success_rate_list)
        max_close_price_ma5_gold_cross_number, min_close_price_ma5_gold_cross_number = DataUtil.max_min(
            close_price_ma5_gold_cross_number_list)
        max_average_profit_rate, min_average_profit_rate = DataUtil.max_min(
            close_price_ma5_gold_cross_average_profit_rate_list)
        max_average_close_price = stock_transaction_data_average_max_min_close_price_ma5_list[0][3]
        min_average_close_price = stock_transaction_data_average_max_min_close_price_ma5_list[0][4]
        max_average_ma5 = stock_transaction_data_average_max_min_close_price_ma5_list[0][5]
        min_average_ma5 = stock_transaction_data_average_max_min_close_price_ma5_list[0][6]

        # 将收盘价、成功率、交易次、平均收益率、close_price、ma5归一化
        adjust_close_price_list = list()
        adjust_close_price_ma5_gold_cross_success_rate_list = list()
        adjust_close_price_ma5_gold_cross_number_list = list()
        adjust_average_profit_rate_list = list()
        adjust_average_close_price_list = list()
        adjust_average_ma5_list = list()
        for close_price in self.close_price_list:
            adjust_close_price_list.append(
                DataUtil.adjust_data(self.stock_index_max_close_price, self.stock_index_min_close_price,
                                     max_close_price_ma5_gold_cross_number, min_close_price_ma5_gold_cross_number,
                                     close_price))
        for close_price_ma5_gold_cross_success_rate in close_price_ma5_gold_cross_success_rate_list:
            adjust_close_price_ma5_gold_cross_success_rate_list.append(
                DataUtil.adjust_data(max_success_rate, min_success_rate, max_close_price_ma5_gold_cross_number,
                                     min_close_price_ma5_gold_cross_number,
                                     close_price_ma5_gold_cross_success_rate))
        for close_price_ma5_gold_cross_average_profit_rate in close_price_ma5_gold_cross_average_profit_rate_list:
            adjust_average_profit_rate_list.append(
                DataUtil.adjust_data(max_average_profit_rate, min_average_profit_rate,
                                     max_close_price_ma5_gold_cross_number,
                                     min_close_price_ma5_gold_cross_number,
                                     close_price_ma5_gold_cross_average_profit_rate))
        for close_price_ma5_gold_cross_number in close_price_ma5_gold_cross_number_list:
            adjust_close_price_ma5_gold_cross_number_list.append(
                DataUtil.adjust_data(max_close_price_ma5_gold_cross_number, min_close_price_ma5_gold_cross_number,
                                     max_close_price_ma5_gold_cross_number,
                                     min_close_price_ma5_gold_cross_number,
                                     close_price_ma5_gold_cross_number))
        for average_close_price in average_close_price_list:
            adjust_average_close_price_list.append(
                DataUtil.adjust_data(max_average_close_price, min_average_close_price,
                                     max_close_price_ma5_gold_cross_number,
                                     min_close_price_ma5_gold_cross_number,
                                     average_close_price))
        for average_ma5 in average_ma5_list:
            adjust_average_ma5_list.append(
                DataUtil.adjust_data(max_average_ma5, min_average_ma5, max_close_price_ma5_gold_cross_number,
                                     min_close_price_ma5_gold_cross_number,
                                     average_ma5))

        # 画图
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        plt.plot(self.date_list_length, adjust_close_price_list, color='green', label='收盘价')
        plt.plot(self.date_list_length, adjust_close_price_ma5_gold_cross_success_rate_list, color='red', label='成功率')
        plt.plot(self.date_list_length, close_price_ma5_gold_cross_number_list, color='skyblue', label='交易次数')
        plt.plot(self.date_list_length, adjust_average_profit_rate_list, color='black', label='平均收益率')
        plt.plot(self.date_list_length, adjust_average_close_price_list, color='fuchsia', label='平均close_price')
        plt.plot(self.date_list_length, adjust_average_ma5_list, color='darkgoldenrod', label='平均ma5')
        plt.legend()  # 让图例生效
        plt.xlabel("日期")  # X轴标签
        plt.ylabel("收盘价、成功率、交易次数图、平均close_price、平均ma5")  # Y轴标签
        plt.title("与close_price金叉ma5相关的收盘价、成功率、交易次数图、平均close_price、平均ma5")  # 标题

        plt.show()

    def paint_close_price_ma5_dead_cross_analysis_line_chart(self):
        """
        生成折线图（与close_price死叉ma5相关的收盘价、成功率、交易次数图、平均dif、平均dea）
        """
        Logger.info('生成折线图（与close_price死叉ma5相关的收盘价、成功率、交易次数图、平均dif、平均dea）')

        # 从表mdl_close_price_ma5_dead_cross中取sell_date、成功率、交易次数、平均收益率，按sell_date升序排列
        self.cursor.execute("select t.sell_date, "
                            "(select count(*) from mdl_close_price_ma5_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.profit_loss>0 and t2.sell_date=t.sell_date)/"
                            "(select count(*) from mdl_close_price_ma5_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.sell_date=t.sell_date)*100, "
                            "(select count(*) from mdl_close_price_ma5_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.sell_date=t.sell_date), "
                            "(select avg(t2.profit_loss) from mdl_close_price_ma5_dead_cross t2 "
                            "where t2.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') and t2.sell_date=t.sell_date) "
                            "from mdl_close_price_ma5_dead_cross t "
                            "where t.sell_date between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
                            "group by t.sell_date "
                            "order by t.sell_date asc" % (
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date,
                                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        mdl_close_price_ma5_dead_cross_success_rate_list = self.cursor.fetchall()

        # 取某段时间内，close_price死叉ma5的平均值、最大值、最小值
        self.cursor.execute(
            "select t.date_, avg(t.close_price)*%s, avg(t.ma5)*%s, max(t.close_price), min(t.close_price), "
            "max(t.ma5), min(t.ma5) "
            "from stock_transaction_data_all t "
            "where t.date_ between to_date('%s', 'yyyy-mm-dd') and to_date('%s', 'yyyy-mm-dd') "
            "group by t.date_ "
            "order by t.date_ asc" % (
                MdlGoldCrossDeadCrossConfig.Close_Price_Multiple_Weight,
                MdlGoldCrossDeadCrossConfig.Close_Price_Multiple_Weight,
                MdlGoldCrossDeadCrossConfig.Begin_Date, MdlGoldCrossDeadCrossConfig.End_Date))
        stock_transaction_data_average_max_min_close_price_ma5_list = self.cursor.fetchall()

        # close_price死叉ma5的成功率、交易次数、平均收益率
        close_price_ma5_dead_cross_success_rate_list = list()
        close_price_ma5_dead_cross_number_list = list()
        close_price_ma5_dead_cross_average_profit_rate_list = list()
        date_match = False
        for date in self.date_list:
            for mdl_close_price_ma5_dead_cross_success_rate in mdl_close_price_ma5_dead_cross_success_rate_list:
                if date == mdl_close_price_ma5_dead_cross_success_rate[0]:
                    date_match = True
                    close_price_ma5_dead_cross_success_rate_list.append(mdl_close_price_ma5_dead_cross_success_rate[1])
                    close_price_ma5_dead_cross_number_list.append(mdl_close_price_ma5_dead_cross_success_rate[2])
                    close_price_ma5_dead_cross_average_profit_rate_list.append(
                        mdl_close_price_ma5_dead_cross_success_rate[3])
                    break
            if date_match is False:
                close_price_ma5_dead_cross_success_rate_list.append(0)
                close_price_ma5_dead_cross_number_list.append(0)
                close_price_ma5_dead_cross_average_profit_rate_list.append(0)
            date_match = False

        # close_price和ma5平均值
        average_close_price_list = list()
        average_ma5_list = list()
        for stock_transaction_data_average_max_min_close_price_ma5 in stock_transaction_data_average_max_min_close_price_ma5_list:
            average_close_price_list.append(stock_transaction_data_average_max_min_close_price_ma5[1])
            average_ma5_list.append(stock_transaction_data_average_max_min_close_price_ma5[2])

        # 成功率、交易次、平均收益率、close_price、ma5的最大值和最小值
        max_success_rate, min_success_rate = DataUtil.max_min(close_price_ma5_dead_cross_success_rate_list)
        max_close_price_ma5_dead_cross_number, min_close_price_ma5_dead_cross_number = DataUtil.max_min(
            close_price_ma5_dead_cross_number_list)
        max_average_profit_rate, min_average_profit_rate = DataUtil.max_min(
            close_price_ma5_dead_cross_average_profit_rate_list)
        max_average_close_price = stock_transaction_data_average_max_min_close_price_ma5_list[0][3]
        min_average_close_price = stock_transaction_data_average_max_min_close_price_ma5_list[0][4]
        max_average_ma5 = stock_transaction_data_average_max_min_close_price_ma5_list[0][5]
        min_average_ma5 = stock_transaction_data_average_max_min_close_price_ma5_list[0][6]

        # 将收盘价、成功率、交易次、平均收益率、close_price、ma5归一化
        adjust_close_price_list = list()
        adjust_close_price_ma5_dead_cross_success_rate_list = list()
        adjust_close_price_ma5_dead_cross_number_list = list()
        adjust_average_profit_rate_list = list()
        adjust_average_close_price_list = list()
        adjust_average_ma5_list = list()
        for close_price in self.close_price_list:
            adjust_close_price_list.append(
                DataUtil.adjust_data(self.stock_index_max_close_price, self.stock_index_min_close_price,
                                     max_close_price_ma5_dead_cross_number, min_close_price_ma5_dead_cross_number,
                                     close_price))
        for close_price_ma5_dead_cross_success_rate in close_price_ma5_dead_cross_success_rate_list:
            adjust_close_price_ma5_dead_cross_success_rate_list.append(
                DataUtil.adjust_data(max_success_rate, min_success_rate, max_close_price_ma5_dead_cross_number,
                                     min_close_price_ma5_dead_cross_number,
                                     close_price_ma5_dead_cross_success_rate))
        for close_price_ma5_dead_cross_average_profit_rate in close_price_ma5_dead_cross_average_profit_rate_list:
            adjust_average_profit_rate_list.append(
                DataUtil.adjust_data(max_average_profit_rate, min_average_profit_rate,
                                     max_close_price_ma5_dead_cross_number,
                                     min_close_price_ma5_dead_cross_number,
                                     close_price_ma5_dead_cross_average_profit_rate))
        for close_price_ma5_dead_cross_number in close_price_ma5_dead_cross_number_list:
            adjust_close_price_ma5_dead_cross_number_list.append(
                DataUtil.adjust_data(max_close_price_ma5_dead_cross_number, min_close_price_ma5_dead_cross_number,
                                     max_close_price_ma5_dead_cross_number,
                                     min_close_price_ma5_dead_cross_number,
                                     close_price_ma5_dead_cross_number))
        for average_close_price in average_close_price_list:
            adjust_average_close_price_list.append(
                DataUtil.adjust_data(max_average_close_price, min_average_close_price,
                                     max_close_price_ma5_dead_cross_number,
                                     min_close_price_ma5_dead_cross_number,
                                     average_close_price))
        for average_ma5 in average_ma5_list:
            adjust_average_ma5_list.append(
                DataUtil.adjust_data(max_average_ma5, min_average_ma5, max_close_price_ma5_dead_cross_number,
                                     min_close_price_ma5_dead_cross_number,
                                     average_ma5))

        # 画图
        mpl.rcParams['font.sans-serif'] = ['SimHei']
        plt.plot(self.date_list_length, adjust_close_price_list, color='green', label='收盘价')
        plt.plot(self.date_list_length, adjust_close_price_ma5_dead_cross_success_rate_list, color='red', label='成功率')
        plt.plot(self.date_list_length, close_price_ma5_dead_cross_number_list, color='skyblue', label='交易次数')
        plt.plot(self.date_list_length, adjust_average_profit_rate_list, color='black', label='平均收益率')
        plt.plot(self.date_list_length, adjust_average_close_price_list, color='fuchsia', label='平均close_price')
        plt.plot(self.date_list_length, adjust_average_ma5_list, color='darkgoldenrod', label='平均ma5')
        plt.legend()  # 让图例生效
        plt.xlabel("日期")  # X轴标签
        plt.ylabel("收盘价、成功率、交易次数图、平均close_price、平均ma5")  # Y轴标签
        plt.title("与close_price死叉ma5相关的收盘价、成功率、交易次数图、平均close_price、平均ma5")  # 标题

        plt.show()
