# coding:utf-8

class BaostockBondIndexConfig:
    """
    代码。上证国债指数：000012；上证企债指数：000013；上证公司债指数：000022；上证分离债指数：000023；上证企债30指数：000061
    """
    Code_List = ['sh.000012', 'sh.000013', 'sh.000022', 'sh.000023', 'sh.000061']

    """
    字段
    """
    Fields = 'date,code,open,high,low,close,preclose,volume,amount,pctChg'

    """
    保存开始时间
    """
    Save_Start_Date = '2024-7-27'

    """
    保存结束时间
    """
    Save_End_Date = '2024-9-4'

    """
    数据类型，默认为d，日k线
    """
    Frequency = 'd'

    """
    创建折线图的开始时间
    """
    Paint_Start_Date = '2023-9-4'

    """
    创建折线图的结束时间
    """
    Paint_End_Date = '2024-9-4'
