# coding:utf-8

class MongodbConfig:
    """
    mongodb数据库的常量类
    """
    # ip
    Host = 'localhost'

    # 端口
    Port = 27017

    # 数据库
    Database = 'black-widow'

    # stock_info集合
    Stock_Info_Collection = 'stock_info'
