# coding:utf-8

class HisRobotConfig:
    """######################################## model表 ############################################"""
    """
    model的id的开始位置
    """
    # Model_Id_Begin = 4012011
    Model_Id_Begin = 1

    """
    model的id的结束位置
    """
    # Model_Id_End = 4092022
    Model_Id_End = 9999999

    """
    model表的文件
    """
    Model_File = 'model.json'

    """################################### his_robot_account表 #####################################"""
    """
    his_robot_account的model_id的开始位置
    """
    His_Robot_Account_Model_Id_Begin = 1

    """
    his_robot_account的model_id的结束位置
    """
    His_Robot_Account_Model_Id_End = 9999999

    """
    his_robot_account表的文件
    """
    His_Robot_Account_File = 'his_robot_account.json'

    """################################### his_robot_account_log表 #####################################"""
    """
    his_robot_account_log的model_id的开始位置
    """
    His_Robot_Account_Log_Model_Id_Begin = 1

    """
    his_robot_account_log的model_id的结束位置
    """
    His_Robot_Account_Log_Model_Id_End = 9999999

    """
    his_robot_account_log表的文件
    """
    His_Robot_Account_Log_File = 'his_robot_account_log.json'

    """################################### his_robot_stock_filter表 #####################################"""
    """
    his_robot_stock_filter的model_id的开始位置
    """
    His_Robot_Stock_Filter_Model_Id_Begin = 1

    """
    his_robot_stock_filter的model_id的结束位置
    """
    His_Robot_Stock_Filter_Model_Id_End = 9999999

    """
    his_robot_stock_filter表的文件
    """
    His_Robot_Stock_Filter_File = 'his_robot_stock_filter.json'

    """################################### his_robot_transaction_record表 #####################################"""
    """
    his_robot_transaction_record的model_id的开始位置
    """
    His_Robot_Transaction_Record_Model_Id_Begin = 1

    """
    his_robot_transaction_record的model_id的结束位置
    """
    His_Robot_Transaction_Record_Model_Id_End = 9999999

    """
    his_robot_transaction_record表的文件
    """
    His_Robot_Transaction_Record_File = 'his_robot_transaction_record.json'
