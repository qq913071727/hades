# coding:utf-8

class OracleConfig:
    """
    oracle数据库的常量类
    """
    # 用户名
    Username = 'scott'

    # 密码
    Password = 'tiger'

    # 连接字符串
    Url = 'localhost:1521/ADAM'
