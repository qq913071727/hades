# coding:utf-8

class MdlGoldCrossDeadCrossConfig:
    """
    开始时间
    """
    Begin_Date = '2022-07-01'

    """
    结束时间
    """
    End_Date = '2022-12-31'

    """
    macd相乘权重
    """
    Macd_Multiple_Weight = 50

    """
    close_price、ma5的相乘权重
    """
    Close_Price_Multiple_Weight = 75
