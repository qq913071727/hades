# coding:utf-8

class Robot2Config:
    """
    开始时间
    """
    Gold_Cross_Dead_Cross_Interval_Begin_Time = '2022-1-1'

    """
    结束时间
    """
    Gold_Cross_Dead_Cross_Interval_End_Time = '2022-12-31'

    """
    生成柱状图（金叉/死叉时，金叉/死叉持续天数与交易次数的关系）使用
    生成柱状图（死叉/金叉持续天数与之后的金叉/死叉获利的交易次数的关系）使用
    生成柱状图（金叉/死叉之前的死叉/金叉持续时间与金叉/死叉平均收益率的关系）
    """
    # 用于所有日期
    # Gold_Cross_Dead_Cross_Interval_Transaction_Number = 100
    # 由于一段时间
    Gold_Cross_Dead_Cross_Interval_Transaction_Number = 1

    """
    生成柱状图（金叉/死叉之前的死叉/金叉持续时间与金叉/死叉成功率的关系）
    """
    # 用于所有日期
    # Gold_Cross_Dead_Cross_Interval_Transact_Number = 50
    # 由于一段时间
    Gold_Cross_Dead_Cross_Interval_Transact_Number = 1
