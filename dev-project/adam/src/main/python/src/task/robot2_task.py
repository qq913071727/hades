# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.robot2_handler import Robot2Handler


class Robot2Task:

    def __init__(self) -> None:
        super().__init__()


if __name__ == '__main__':
    robot2_handler = Robot2Handler()

    # 生成柱状图（金叉/死叉时，金叉/死叉持续天数与交易次数的关系）
    # robot2_handler.paint_macd_gold_cross_date_num_bar_chart()
    # robot2_handler.paint_macd_dead_cross_date_num_bar_chart()
    # robot2_handler.paint_close_price_ma5_gold_cross_date_num_bar_chart()
    # robot2_handler.paint_close_price_ma5_dead_cross_date_num_bar_chart()
    # robot2_handler.paint_hei_kin_ashi_up_down_date_num_bar_chart()
    # robot2_handler.paint_hei_kin_ashi_down_up_date_num_bar_chart()
    # robot2_handler.paint_kd_gold_cross_date_num_bar_chart()
    # robot2_handler.paint_kd_dead_cross_date_num_bar_chart()

    # 生成柱状图（金叉/死叉时，金叉/死叉持续天数与获利交易次数的关系）
    # robot2_handler.paint_macd_gold_cross_profit_date_num_bar_chart()
    # robot2_handler.paint_macd_dead_cross_profit_date_num_bar_chart()
    # robot2_handler.paint_close_price_ma5_gold_cross_profit_date_num_bar_chart()
    # robot2_handler.paint_close_price_ma5_dead_cross_profit_date_num_bar_chart()
    # robot2_handler.paint_hei_kin_ashi_up_down_profit_date_num_bar_chart()
    # robot2_handler.paint_hei_kin_ashi_down_up_profit_date_num_bar_chart()
    # robot2_handler.paint_kd_gold_cross_profit_date_num_bar_chart()
    # robot2_handler.paint_kd_dead_cross_profit_date_num_bar_chart()

    # 生成柱状图（死叉/金叉持续天数与之后的金叉/死叉获利的交易次数的关系）
    # robot2_handler.paint_date_num_before_macd_gold_cross_transaction_number_bar_chart()
    # robot2_handler.paint_date_num_before_macd_dead_cross_transaction_number_bar_chart()
    # robot2_handler.paint_date_num_before_close_price_ma5_gold_cross_transaction_number_bar_chart()
    # robot2_handler.paint_date_num_before_close_price_ma5_dead_cross_transaction_number_bar_chart()
    # robot2_handler.paint_date_num_before_hei_kin_ashi_up_down_transaction_number_bar_chart() ######################### 慢
    # robot2_handler.paint_date_num_before_hei_kin_ashi_down_up_transaction_number_bar_chart()
    # robot2_handler.paint_date_num_before_kd_gold_cross_transaction_number_bar_chart()
    # robot2_handler.paint_date_num_before_kd_dead_cross_transaction_number_bar_chart()

    # 生成柱状图（金叉/死叉之前的死叉/金叉持续天数与金叉/死叉成功率的关系）
    # robot2_handler.paint_date_num_before_macd_gold_cross_success_rate_bar_chart()
    # robot2_handler.paint_date_num_before_macd_dead_cross_success_rate_bar_chart()
    # robot2_handler.paint_date_num_before_close_price_ma5_gold_cross_success_rate_bar_chart()
    # robot2_handler.paint_date_num_before_close_price_ma5_dead_cross_success_rate_bar_chart()
    robot2_handler.paint_date_num_before_hei_kin_ashi_up_down_success_rate_bar_chart() ######################### 慢
    # robot2_handler.paint_date_num_before_hei_kin_ashi_down_up_success_rate_bar_chart()
    # robot2_handler.paint_date_num_before_kd_gold_cross_success_rate_bar_chart()
    # robot2_handler.paint_date_num_before_kd_dead_cross_success_rate_bar_chart()

    # 生成柱状图（金叉/死叉之前的死叉/金叉持续天数与金叉/死叉平均收益率的关系）
    # robot2_handler.paint_date_num_before_macd_gold_cross_profit_bar_chart()
    # robot2_handler.paint_date_num_before_macd_dead_cross_profit_bar_chart()
    # robot2_handler.paint_date_num_before_close_price_ma5_gold_cross_profit_bar_chart()
    # robot2_handler.paint_date_num_before_close_price_ma5_dead_cross_profit_bar_chart()
    robot2_handler.paint_date_num_before_hei_kin_ashi_up_down_profit_bar_chart() ######################### 慢
    # robot2_handler.paint_date_num_before_hei_kin_ashi_down_up_profit_bar_chart()
    # robot2_handler.paint_date_num_before_kd_gold_cross_profit_bar_chart()
    # robot2_handler.paint_date_num_before_kd_dead_cross_profit_bar_chart()
