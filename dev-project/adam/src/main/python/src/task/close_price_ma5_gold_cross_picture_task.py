# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.close_price_ma5_gold_cross_picture_handler import ClosePriceMa5GoldCrossPictureHandler


class ClosePriceMa5GoldCrossPictureTask:

    def __init__(self):
        pass


if __name__ == '__main__':
    create_close_ma5_price_gold_cross_picture_handler = ClosePriceMa5GoldCrossPictureHandler()

    # 生成散点图：x轴为k、d的平均值，y轴为mdl_close_price_ma5_gold_cross表的profit_loss字段
    # create_close_ma5_price_gold_cross_picture_handler.create_close_price_ma5_gold_cross_refer_k_d_scatter_picture()

    # 生成散点图：x轴为dif和dea的平均值，y轴为mdl_close_price_ma5_gold_cross表的profit_loss字段
    # create_close_ma5_price_gold_cross_picture_handler.create_close_price_ma5_gold_cross_refer_dif_dea_scatter_picture()

    # 生成散点图：x轴为k、d的平均值，y轴为dif和dea的平均值，profit_loss为-1、0、1时，圆点的颜色不一样
    create_close_ma5_price_gold_cross_picture_handler.create_close_price_ma5_gold_cross_refer_k_d_and_dif_dea_scatter_picture()