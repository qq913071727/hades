# coding:utf-8
import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.stock_week_handler import StockWeekHandler


class StockWeekTask:

    def __init__(self) -> None:
        super().__init__()


if __name__ == '__main__':
    stock_week_handler = StockWeekHandler()
    stock_week_handler.paint_stock_week_avg_kd_and_macd_gold_cross_profit_rate_scatter()
