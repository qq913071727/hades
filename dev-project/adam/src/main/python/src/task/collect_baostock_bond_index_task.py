# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.collect_bond_index_handler import CollectBondIndexHandler


class CollectBaostockBondIndexTask:

    def __init__(self) -> None:
        super().__init__()


if __name__ == '__main__':
    collect_bond_index_handler = CollectBondIndexHandler()
    # 获取债券指数数据
    collect_bond_index_handler.save()
    # 创建所有指数的折线图
    collect_bond_index_handler.paint_all_broken_line_graph()
    # 创建上证国债指数的折线图
    collect_bond_index_handler.paint_shanghai_treasury_broken_line_graph()
