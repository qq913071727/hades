# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.import_oracle_dump_handler import ImportOracleDumpHandler


class ImportOracleDumpTask:
    """
    循环导入oracle的dump文件
    """

    def __init__(self):
        pass


if __name__ == '__main__':
    import_oracle_dump_handler = ImportOracleDumpHandler()
    # 循环导入oracle的dump文件
    import_oracle_dump_handler.import_oracle_dump()
