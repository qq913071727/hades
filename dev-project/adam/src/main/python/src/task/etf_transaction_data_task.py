# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')

from src.manager.log_manager import LogManager
from src.handler.etf_transaction_data_handler import EtfTransactionDataHandler

Logger = LogManager.get_logger(__name__)


class EtfTransactionDataTask:

    def __init__(self):
        super().__init__()


if __name__ == '__main__':
    etf_transaction_data_handler = EtfTransactionDataHandler()

    # 调用接口，根据date和code，更新last_close_price字段
    etf_transaction_data_handler.update_last_close_price_by_date_and_code()