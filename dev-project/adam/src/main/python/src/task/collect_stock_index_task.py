# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')

from src.manager.log_manager import LogManager
from src.handler.collect_stock_index_handler import CollectStockIndexHandler

Logger = LogManager.get_logger(__name__)


class CollectStockIndexTask:

    def __init__(self):
        super().__init__()


if __name__ == '__main__':
    # 根据date，调用baostock的API获取指数数据，并存储到stock_index表中
    collect_stock_index_task = CollectStockIndexHandler()
    collect_stock_index_task.insert_stock_index()
