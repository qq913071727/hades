# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.his_robot_handler import HisRobotHandler


class HisRobotTask:

    def __init__(self) -> None:
        super().__init__()


if __name__ == '__main__':
    his_robot_handler = HisRobotHandler()

    # # 将model表存储到本地文件中
    # his_robot_handler.store_model_in_file()
    # # 从文件中获取数据，插入到model表中
    # his_robot_handler.insert_into_model()

    # 将his_robot_account表存储到本地文件中
    # his_robot_handler.store_his_robot_account_in_file()
    # 从文件中获取数据，插入到his_robot_account表中
    his_robot_handler.insert_into_his_robot_account()

    # 将his_robot_account_log表存储到本地文件中
    # his_robot_handler.store_his_robot_account_log_in_file()
    # 从文件中获取数据，插入到his_robot_account_log表中
    # his_robot_handler.insert_into_his_robot_account_log()

    # 将his_robot_stock_filter表存储到本地文件中
    # his_robot_handler.store_his_robot_stock_filter_in_file()
    # 从文件中获取数据，插入到his_robot_stock_filter表中
    # his_robot_handler.insert_into_his_robot_stock_filter()

    # 将his_robot_transaction_record表存储到本地文件中
    # his_robot_handler.store_his_robot_transaction_record_in_file()
    # 从文件中获取数据，插入到his_robot_transaction_record表中
    # his_robot_handler.insert_into_his_robot_transaction_record()
