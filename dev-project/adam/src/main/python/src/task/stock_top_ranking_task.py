# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.stock_top_ranking_handler import StockTopRankingHandler
from src.manager.log_manager import LogManager

Logger = LogManager.get_logger(__name__)


class StockTopRankingTask:
    """
    股票流行度排名任务
    """

    def __init__(self):
        pass


if __name__ == '__main__':
    stock_top_ranking_handler = StockTopRankingHandler()

    for i in range(20):
        # 获取股票流行度排名列表，按降序排列
        stock_top_ranking_list = stock_top_ranking_handler.get_stock_top_ranking_list_order_by_desc()
        # 保存StockTopRanking对象列表
        stock_top_ranking_handler.save_stock_top_ranking_list(stock_top_ranking_list)
        Logger.info('第【' + str(i) + '】次')