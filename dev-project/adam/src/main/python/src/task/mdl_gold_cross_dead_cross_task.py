# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')
from src.handler.mdl_gold_cross_dead_cross_handler import MdlGoldCrossDeadCrossHandler


class MdlGoldCrossDeadCrossTask:

    def __init__(self) -> None:
        super().__init__()


if __name__ == '__main__':
    mdl_gold_cross_dead_cross_handler = MdlGoldCrossDeadCrossHandler()
    mdl_gold_cross_dead_cross_handler.paint_macd_gold_cross_analysis_line_chart()
    mdl_gold_cross_dead_cross_handler.paint_macd_dead_cross_analysis_line_chart()
    mdl_gold_cross_dead_cross_handler.paint_close_price_ma5_gold_cross_analysis_line_chart()
    mdl_gold_cross_dead_cross_handler.paint_close_price_ma5_dead_cross_analysis_line_chart()
