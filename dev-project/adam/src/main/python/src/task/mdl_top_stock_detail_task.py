# coding:utf-8

import sys
from src.manager.log_manager import LogManager
from src.handler.mdl_top_stock_detail_handler import MdlTopStockDetailHandler

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')

Logger = LogManager.get_logger(__name__)


class MdlTopStockDetailTask:
    """
    股票top100详细信息任务类
    """

    def __init__(self):
        pass


if __name__ == '__main__':
    mdl_top_stock_detail_handler = MdlTopStockDetailHandler()
    mdl_top_stock_detail_handler.paint_mdl_top_stock_by_code('603396', 60, 55)
