# coding:utf-8

import sys

sys.path.append(
    '/mywork/gitcode-repository/hades/dev-project/adam/src/main/python/dist/adam-python-1.0.tar.gz_files/adam-python-1.0')

from src.manager.log_manager import LogManager
from src.handler.collect_stock_handler import CollectStockHandler

Logger = LogManager.get_logger(__name__)


class CollectStockTask:

    def __init__(self):
        super().__init__()


if __name__ == '__main__':
    pass
    # 根据参数，调用baostock的API获取股票数据，或者存储在csv文件中，或者返回
    # stock_code = sys.argv[1]
    # begin_date = sys.argv[2]
    # end_date = sys.argv[3]
    # collect_stock_handler = CollectStockHandler()
    # return_list = collect_stock_handler.collect(stock_code, begin_date, end_date)
    # Logger.info(return_list)

    # 根据begin_date和end_date，调用baostock的API获取股票数据，并存储到stock_transaction_data表中
    collect_stock_handler = CollectStockHandler()
    collect_stock_handler.insert_stock_transaction_data()
