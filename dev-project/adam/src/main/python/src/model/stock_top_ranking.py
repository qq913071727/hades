# coding:utf-8


class StockTopRanking:
    """
    股票流行度排名
    """

    def __init__(self, code, name, search_result_number):
        # 股票代码
        self.code = code
        # 股票名称
        self.name = name
        # 查询结果数量
        self.search_result_number = search_result_number

    def to_str(self):
        """
        将对象序列化
        :return:
        """
        return '股票代码[code]：' + self.code + '，股票名称[name]：' + self.name + '，查询结果数量[search_result_number]：' + str(
            self.search_result_number)
