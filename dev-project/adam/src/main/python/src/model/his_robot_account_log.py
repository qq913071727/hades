# coding:utf-8

"""
机器人--账户（历史数据）
"""


class HisRobotAccountLog:
    """
    主键
    """
    id_ = None

    """
    日期
    """
    date_ = None

    """
    机器人名字
    """
    robot_name = None

    """
    持股数量
    """
    hold_stock_number = None

    """
    股票资产
    """
    stock_assets = None

    """
    资金资产
    """
    capital_assets = None

    """
    总资产
    """
    total_assets = None

    """
    模型id
    """
    model_id = None