# coding:utf-8

"""
机器人--股票交易记录（历史数据）
"""


class HisRobotTransactionRecord:
    """
    主键
    """
    id_ = None

    """
    机器人名字
    """
    robot_name = None

    """
    股票代码
    """
    stock_code = None

    """
    买入日期
    """
    buy_date = None

    """
    买入价格
    """
    buy_price = None

    """
    买入数量
    """
    buy_amount = None

    """
    卖出日期
    """
    sell_date = None

    """
    卖出价格
    """
    sell_price = None

    """
    卖出数量
    """
    sell_amount = None

    """
    过滤类型/交易策略。1表示MACD金叉；2表示MACD死叉；3表示close_price金叉MA5；4表示close_price死叉MA5；5表示hei_kin_ashi从下跌趋势转为上涨趋势；6表示hei_kin_ashi从上涨趋势转为下跌趋势；7表示KD金叉；8表示KD死叉
    """
    filter_type = None

    """
    交易方向。1表示做多；-1表示做空
    """
    direction = None

    """
    盈亏额
    """
    profit_and_loss = None

    """
    盈亏率
    """
    profit_and_loss_rate = None

    """
    模型id
    """
    model_id = None
