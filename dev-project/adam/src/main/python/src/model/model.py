# coding:utf-8

"""
模型表
"""
class Model:
    """
    主键
    """
    id = None

    """
    模型名称
    """
    name = None

    """
    创建时间
    """
    create_time = None

    """
    测试开始时间
    """
    begin_time = None

    """
    测试结束时间
    """
    end_time = None

    """
    算法描述
    """
    algorithm_description = None

    """
    是否有仓位控制。1表示有，-1表示没有
    """
    s_position_control = None

    """
    平均年收益率
    """
    average_annual_return_rate = None

    """
    是否做多。1表示是，-1表示否
    """
    buying_long = None

    """
    是否做空。1表示是，-1表示否
    """
    short_selling = None

    """
    成功率计算时间。单位：天
    """
    success_rate_calculate_time = None

    """
    成功率阈值。单位：百分比
    """
    success_rate_threshold = None

    """
    1表示使用单一算法，2表示使用所有算法
    """
    p_single_or_all_method = None

    """
    在使用单一算法的条件下，出现了多个金叉或死叉的情况，此时使用哪种方法确定最后使用哪个算法。1表示通过金叉死叉成功率来判断，2表示通过金叉死叉百分比来判断
    """
    p_judge_method = None

    """
    是否控制仓位。0表示不控制，1表示控制（成功率），2表示控制（百分比）
    """
    p_shipping_space_control = None

    """
    当百分比小于这个阈值时才进行买卖交易（单位：%）
    """
    p_percentage_top_threshold = None

    """
    当百分比大于这个阈值时才进行买卖交易（单位：%）
    """
    p_percentage_bottom_threshold = None

    """
    如果为true（数据库列为1），只使用最少的过滤条件。如果为false（数据库列为2），使用所有过滤条件
    """
    p_use_min_filter_condition = None

    """
    1表示成功率算法，2表示百分比算法，3表示收盘价突破或跌破周线级别布林带上轨或下轨
    """
    type_ = None

    """
    是否有强制止损。0表示没有，1表示有
    """
    mandatory_stop_loss = None

    """
    强制止损率。当损失超过这个比率时，立刻平仓
    """
    mandatory_stop_loss_rate = None

    """
    震荡行情：日线级别，某一段时间内，收盘价大于年线的记录数（数量更多）/收盘价小于年线的记录数（数量更少）<=阈值
    """
    volatile_rate = None

    """
    最近几个月内是震荡行情
    """
    volatile_month_number = None

    """
    为1时，表示顺着趋势操作，如果突破布林带上轨的股票数量多于跌破布林带下轨的股票的数量，则寻找跌破下轨后金叉的股票做多
    为1时，表示顺着趋势操作，如果突破布林带上轨的股票数量少于跌破布林带下轨的股票的数量，则寻找突破上轨后死叉的股票做空
    为2时，表示逆着趋势操作，如果突破布林带上轨的股票数量多于跌破布林带下轨的股票的数量，则寻找突破上轨后死叉的股票做空
    为2时，表示逆着趋势操作，如果突破布林带上轨的股票数量少于跌破布林带下轨的股票的数量，则寻找跌破下轨后金叉的股票做多
    """
    follow_or_reverse_trend = None

    """
    每个账号的持股数量
    """
    hold_stock_number_per_account = None

    """
    账号数量
    """
    account_number = None

    """
    是否有ma120单调不递增。-1表示没有，如果不是-1则表示是多少天
    """
    ma120_not_increasing = None

    """
    是否有ma120单调不递减。-1表示没有，如果不是-1则表示是多少天
    """
    ma120_not_decreasing = None

    """
    是否有ma250单调不递增。-1表示没有，如果不是-1则表示是多少天
    """
    ma250_not_increasing = None

    """
    是否有ma250单调不递减。-1表示没有，如果不是-1则表示是多少天
    """
    ma250_not_decreasing = None

    """
    收盘价距离布林带上下轨的百分比
    """
    c_p_percentage_from_up_dn = None

    """
    交易数量(macd)
    """
    macd_transaction_number = None

    """
    交易数量(close_price ma5)
    """
    c_p_ma5_transaction_number = None

    """
    交易数量(hei_kin_ashi)
    """
    h_k_a_transaction_number = None

    """
    交易数量(kd)
    """
    kd_transaction_number = None

    """
    强制止盈率。当损失超过这个比率时，立刻平仓
    """
    mandatory_stop_profit_rate = None

    """
    是否有强制止盈。0表示没有，1表示有
    """
    mandatory_stop_profit = None

    """
    模型表同时适用于success_rate和percentage两种算法，成功率适用的列以s_开头，百分比适用的列以p_开头，两者都适用的不专门设置开头。此外type_列表示使用的是哪种算法
    """
    model = None
