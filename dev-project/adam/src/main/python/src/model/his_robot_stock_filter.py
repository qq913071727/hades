# coding:utf-8

"""
机器人--股票过滤表（历史数据）
"""


class HisRobotStockFilter:
    """
    主键
    """
    id_ = None

    """
    股票代码
    """
    stock_code = None

    """
    模型id
    """
    model_id = None
