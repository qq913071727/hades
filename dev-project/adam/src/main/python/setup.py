# coding=utf-8
from distutils.core import setup

setup(
    name='adam-python',
    version='1.0',
    author='李珅',
    author_email='913071727@qq.com',
    maintainer='李珅',
    maintainer_email='913071727@qq.com',
    url='',
    packages=['', 'src', 'src/config', 'src/constant', 'src/handler', 'src/manager', 'src/model', 'src/util',
              'src/task', 'src/task/board', 'src/task/stock_info']
)
