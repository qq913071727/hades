﻿----------------------- 所有算法的每笔交易成功率、每笔交易收益率、每笔交易收益率 --------------------------
select 'mdl_macd_gold_cross' as 算法类型,
(select count(*) from mdl_macd_gold_cross t) 交易次数,
(select count(*) from mdl_macd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_macd_gold_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.sell_date) max_sell_date from mdl_macd_gold_cross t1
group by t1.stock_code) t2
join mdl_macd_gold_cross t3 on t3.stock_code=t2.stock_code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_macd_dead_cross' as 算法类型,
(select count(*) from mdl_macd_dead_cross t) 交易次数,
(select count(*) from mdl_macd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_macd_dead_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_macd_dead_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.buy_date) max_buy_date from mdl_macd_dead_cross t1
group by t1.stock_code) t2
join mdl_macd_dead_cross t3 on t3.stock_code=t2.stock_code and t3.buy_date=max_buy_date) as 股票平均收益
from dual
union
select 'mdl_close_price_ma5_gold_cross' as 算法类型,
(select count(*) from mdl_close_price_ma5_gold_cross t) 交易次数,
(select count(*) from mdl_close_price_ma5_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_gold_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_close_price_ma5_gold_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.sell_date) max_sell_date from mdl_close_price_ma5_gold_cross t1
group by t1.stock_code) t2
join mdl_close_price_ma5_gold_cross t3 on t3.stock_code=t2.stock_code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_close_price_ma5_dead_cross' as 算法类型,
(select count(*) from mdl_close_price_ma5_dead_cross t) 交易次数,
(select count(*) from mdl_close_price_ma5_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_dead_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_close_price_ma5_dead_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.buy_date) max_buy_date from mdl_close_price_ma5_dead_cross t1
group by t1.stock_code) t2
join mdl_close_price_ma5_dead_cross t3 on t3.stock_code=t2.stock_code and t3.buy_date=max_buy_date) as 股票平均收益
from dual
union
select 'mdl_hei_kin_ashi_up_down' as 算法类型,
(select count(*) from mdl_hei_kin_ashi_up_down t) 交易次数,
(select count(*) from mdl_hei_kin_ashi_up_down t where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_up_down t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_hei_kin_ashi_up_down t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.sell_date) max_sell_date from mdl_hei_kin_ashi_up_down t1
group by t1.stock_code) t2
join mdl_hei_kin_ashi_up_down t3 on t3.stock_code=t2.stock_code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_hei_kin_ashi_down_up' as 算法类型,
(select count(*) from mdl_hei_kin_ashi_down_up t) 交易次数,
(select count(*) from mdl_hei_kin_ashi_down_up t where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_down_up t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_hei_kin_ashi_down_up t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.buy_date) max_buy_date from mdl_hei_kin_ashi_down_up t1
group by t1.stock_code) t2
join mdl_hei_kin_ashi_down_up t3 on t3.stock_code=t2.stock_code and t3.buy_date=max_buy_date) as 股票平均收益
from dual
union
select 'mdl_kd_gold_cross' as 算法类型,
(select count(*) from mdl_kd_gold_cross t) 交易次数,
(select count(*) from mdl_kd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_kd_gold_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_kd_gold_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.sell_date) max_sell_date from mdl_kd_gold_cross t1
group by t1.stock_code) t2
join mdl_kd_gold_cross t3 on t3.stock_code=t2.stock_code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_kd_dead_cross' as 算法类型,
(select count(*) from mdl_kd_dead_cross t) 交易次数,
(select count(*) from mdl_kd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_kd_dead_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_kd_dead_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.stock_code) from
(select t1.stock_code stock_code, max(t1.buy_date) max_buy_date from mdl_kd_dead_cross t1
group by t1.stock_code) t2
join mdl_kd_dead_cross t3 on t3.stock_code=t2.stock_code and t3.buy_date=max_buy_date) as 股票平均收益
from dual;

select
(select avg(t2.accumulative_profit_loss) from mdl_macd_gold_cross t2
join (select t1.stock_code stock_code, max(t1.sell_date) sell_date
from mdl_macd_gold_cross t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.sell_date=t2.sell_date) macd_gold_cross_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_macd_dead_cross t2
join (select t1.stock_code stock_code, max(t1.buy_date) buy_date
from mdl_macd_dead_cross t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.buy_date=t2.buy_date) macd_dead_cross_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_close_price_ma5_gold_cross t2
join (select t1.stock_code stock_code, max(t1.sell_date) sell_date
from mdl_close_price_ma5_gold_cross t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.sell_date=t2.sell_date) c_p_ma5_gold_cross_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_close_price_ma5_dead_cross t2
join (select t1.stock_code stock_code, max(t1.buy_date) buy_date
from mdl_close_price_ma5_dead_cross t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.buy_date=t2.buy_date) c_p_ma5_dead_cross_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_hei_kin_ashi_up_down t2
join (select t1.stock_code stock_code, max(t1.sell_date) sell_date
from mdl_hei_kin_ashi_up_down t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.sell_date=t2.sell_date) h_k_a_up_down_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_hei_kin_ashi_down_up t2
join (select t1.stock_code stock_code, max(t1.buy_date) buy_date
from mdl_hei_kin_ashi_down_up t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.buy_date=t2.buy_date) h_k_a_down_up_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_kd_gold_cross t2
join (select t1.stock_code stock_code, max(t1.sell_date) sell_date
from mdl_kd_gold_cross t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.sell_date=t2.sell_date) kd_gold_cross_accmulative,
(select avg(t2.accumulative_profit_loss) from mdl_kd_dead_cross t2
join (select t1.stock_code stock_code, max(t1.buy_date) buy_date
from mdl_kd_dead_cross t1 group by t1.stock_code) t3
on t3.stock_code=t2.stock_code and t3.buy_date=t2.buy_date) kd_dead_cross_accmulative
from dual;

------------------------------------------ mdl_macd_gold_cross ---------------------------------------------
select
(select count(*) from mdl_macd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t)*100
from dual;

select distinct t.stock_code from mdl_macd_gold_cross t;

select * from mdl_macd_gold_cross t where t.stock_code='002930' order by t.sell_date desc;

select * from mdl_macd_gold_cross t order by t.sell_date desc;
select * from mdl_macd_gold_cross t order by t.buy_date desc;

select t.stock_code, count(t.sell_date), count(t.buy_date) from mdl_macd_gold_cross t
where t.sell_date between to_date('2022-10-10','yyyy-mm-dd') and to_date('2022-10-11','yyyy-mm-dd')
group by t.stock_code
having count(t.sell_date)>1 or count(t.buy_date)>1;

select
(select count(*) from mdl_macd_gold_cross t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea)*100
from dual;

select
(select count(*) from mdl_macd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d
where t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d)*100
from dual;

select
(select count(*) from mdl_macd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea)*100
from dual;

select
(select sum(t.profit_loss) from mdl_macd_gold_cross t)/
(select count(*) from mdl_macd_gold_cross t)
from dual;

select a.close_price_ma_order, count(*)
from mdl_macd_gold_cross macd
join mdl_stock_analysis a on a.code_=macd.stock_code and a.date_=macd.buy_date
where macd.profit_loss>0
group by a.close_price_ma_order
order by count(*) desc;

select * from mdl_macd_gold_cross t where t.sell_date=to_date('2022-03-11','yyyy-mm-dd');

select * from mdl_macd_gold_cross t where t.stock_code='002750' order by t.sell_date asc;
select * from mdl_kd_gold_cross t where t.stock_code='002750' order by t.sell_date asc;
select * from mdl_close_price_ma5_gold_cross t where t.stock_code='002750' order by t.sell_date asc;
select * from mdl_hei_kin_ashi_up_down t where t.stock_code='002750' order by t.sell_date asc;

select t1.stock_code,
(select count(*) from mdl_macd_gold_cross t where t.stock_code=t1.stock_code and t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t where t.stock_code=t1.stock_code)*100 percentage
from mdl_macd_gold_cross t1
group by t1.stock_code
order by percentage desc;

select max(t.buy_date), max(t.sell_date) from mdl_macd_gold_cross t;

/*delete from mdl_macd_gold_cross t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

------------------------------------------ mdl_macd_dead_cross ---------------------------------------------

select count(*) from mdl_macd_dead_cross;

select distinct t.stock_code from mdl_macd_dead_cross t;

select * from mdl_macd_dead_cross t where t.stock_code='600563' order by t.buy_date desc;

select * from mdl_macd_dead_cross t order by t.buy_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_macd_dead_cross t;

select * from MDL_MACD_DEAD_CROSS t order by t.sell_date desc;

select * from mdl_macd_dead_cross t where t.sell_date>=to_date('2022-03-11','yyyy-mm-dd');

select
(select count(*) from mdl_macd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_macd_dead_cross t)*100
from dual;

/*delete from MDL_MACD_DEAD_CROSS t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

-- truncate table MDL_MACD_DEAD_CROSS;

------------------------------------------ mdl_close_price_ma5_gold_cross ---------------------------------------------
select
(select count(*) from mdl_close_price_ma5_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_gold_cross t)*100
from dual;

select distinct t.stock_code from mdl_close_price_ma5_gold_cross t;

select * from mdl_close_price_ma5_gold_cross t where t.stock_code='002009' order by t.sell_date desc;

select * from mdl_close_price_ma5_gold_cross t order by t.sell_date desc;

select count(*) from mdl_close_price_ma5_gold_cross t where t.sell_date>=to_date('2023-1-12','yyyy-mm-dd');

/*delete from mdl_close_price_ma5_gold_cross t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

-- truncate table mdl_close_price_ma5_gold_cross

select
(select count(*) from mdl_close_price_ma5_gold_cross t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_gold_cross t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea)*100
from dual;

select
(select count(*) from mdl_close_price_ma5_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d
where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d)*100
from dual;

select
(select count(*) from mdl_close_price_ma5_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea)*100
from dual;

select
(select sum(t.profit_loss) from mdl_close_price_ma5_gold_cross t)/
(select count(*) from mdl_close_price_ma5_gold_cross t)
from dual;

select a.close_price_ma_order, count(*)
from mdl_close_price_ma5_gold_cross macd
join mdl_stock_analysis a on a.code_=macd.stock_code and a.date_=macd.buy_date
where macd.profit_loss>0
group by a.close_price_ma_order
order by count(*) desc;

select max(t.buy_date), max(t.sell_date) from mdl_close_price_ma5_gold_cross t;

--------------------------------------- mdl_close_price_ma5_dead_cross ------------------------------------------

select count(*) from mdl_close_price_ma5_dead_cross;

select distinct t.stock_code from mdl_close_price_ma5_dead_cross t;

select * from mdl_close_price_ma5_dead_cross t where t.stock_code='002060' order by t.buy_date desc;

select * from mdl_close_price_ma5_dead_cross t order by t.buy_date desc;

select count(*) from mdl_close_price_ma5_dead_cross t
where t.buy_date=to_date('2022-06-23','yyyy-mm-dd') or t.buy_date=to_date('2022-06-24','yyyy-mm-dd');

/*delete from mdl_close_price_ma5_dead_cross t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

select
(select count(*) from mdl_close_price_ma5_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_dead_cross t)*100
from dual;

select max(t.buy_date), max(t.sell_date) from mdl_close_price_ma5_dead_cross t;

-- truncate table mdl_close_price_ma5_dead_cross;

------------------------------------------ mdl_hei_kin_ashi_up_down ---------------------------------------------
select
(select count(*) from mdl_hei_kin_ashi_up_down t where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_up_down t)*100
from dual;

select distinct t.stock_code from mdl_hei_kin_ashi_up_down t;

select * from mdl_hei_kin_ashi_up_down t where t.stock_code='000800' order by t.sell_date desc;

select * from mdl_hei_kin_ashi_up_down t order by t.sell_date desc;

select count(*) from mdl_hei_kin_ashi_up_down t
where t.sell_date=to_date('2022-06-23','yyyy-mm-dd') or t.sell_date=to_date('2022-06-24','yyyy-mm-dd');

/*delete from mdl_hei_kin_ashi_up_down t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

select
(select count(*) from mdl_hei_kin_ashi_up_down t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_up_down t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea)*100
from dual;

select
(select count(*) from mdl_hei_kin_ashi_up_down t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d
where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_up_down t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d)*100
from dual;

select
(select count(*) from mdl_hei_kin_ashi_up_down t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_up_down t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea)*100
from dual;

select
(select sum(t.profit_loss) from mdl_hei_kin_ashi_up_down t)/
(select count(*) from mdl_hei_kin_ashi_up_down t)
from dual;

select a.close_price_ma_order, count(*)
from mdl_hei_kin_ashi_up_down macd
join mdl_stock_analysis a on a.code_=macd.stock_code and a.date_=macd.buy_date
where macd.profit_loss>0
group by a.close_price_ma_order
order by count(*) desc;

select max(t.buy_date), max(t.sell_date) from mdl_hei_kin_ashi_up_down t;

-- truncate table mdl_hei_kin_ashi_up_down

--------------------------------------- mdl_hei_kin_ashi_down_up ------------------------------------------

select count(*) from mdl_hei_kin_ashi_down_up;

select distinct t.stock_code from mdl_hei_kin_ashi_down_up t;

select * from mdl_hei_kin_ashi_down_up t where t.stock_code='688139' order by t.buy_date desc;

select * from mdl_hei_kin_ashi_down_up t order by t.buy_date desc;

select count(*) from mdl_hei_kin_ashi_down_up t
where t.buy_date=to_date('2022-06-23','yyyy-mm-dd') or t.buy_date=to_date('2022-06-24','yyyy-mm-dd');

/*delete from mdl_hei_kin_ashi_down_up t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

select
(select count(*) from mdl_hei_kin_ashi_down_up t where t.profit_loss>0)/
(select count(*) from mdl_hei_kin_ashi_down_up t)*100
from dual;

select max(t.buy_date), max(t.sell_date) from mdl_hei_kin_ashi_down_up t;

-- truncate table mdl_hei_kin_ashi_down_up;

------------------------------------------ mdl_kd_gold_cross ---------------------------------------------

select max(t.buy_date), max(t.sell_date), min(t.buy_date), min(t.sell_date) from mdl_kd_gold_cross t;

select max(t.buy_date), max(t.sell_date) from mdl_kd_gold_cross t;

select count(*) from mdl_kd_gold_cross;

select distinct t.stock_code from mdl_kd_gold_cross t;

select * from mdl_kd_gold_cross t where t.stock_code='002026' order by t.sell_date desc;

select * from mdl_kd_gold_cross t order by t.sell_date desc;

select count(*) from mdl_kd_gold_cross t
where t.sell_date=to_date('2022-06-23','yyyy-mm-dd') or t.sell_date=to_date('2022-06-24','yyyy-mm-dd');

/*delete from mdl_kd_gold_cross t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

-- truncate table mdl_kd_gold_cross;

select
(select count(*) from mdl_kd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_kd_gold_cross t)*100
from dual;

select
(select count(*) from mdl_kd_gold_cross t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_kd_gold_cross t
join stock_transaction_data t1 on t1.code_=t.stock_code and t1.dif>t1.dea)*100
from dual;

select
(select count(*) from mdl_kd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d
where t.profit_loss>0)/
(select count(*) from mdl_kd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.k>t1.d)*100
from dual;

select
(select count(*) from mdl_kd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea
where t.profit_loss>0)/
(select count(*) from mdl_kd_gold_cross t
join stock_week t1 on t1.code_=t.stock_code and t.buy_date between t1.begin_date and t1.end_date and t1.dif>t1.dea)*100
from dual;

select
(select sum(t.profit_loss) from mdl_kd_gold_cross t)/
(select count(*) from mdl_kd_gold_cross t)
from dual;

select a.close_price_ma_order, count(*)
from mdl_kd_gold_cross macd
join mdl_stock_analysis a on a.code_=macd.stock_code and a.date_=macd.buy_date
where macd.profit_loss>0
group by a.close_price_ma_order
order by count(*) desc;

select t.stock_code,count(*) from mdl_kd_gold_cross t
where t.sell_date=to_date('2022-03-11','yyyy-mm-dd') group by t.stock_code;

--------------------------------------- mdl_kd_dead_cross ------------------------------------------

select count(*) from mdl_kd_dead_cross;

select distinct t.stock_code from mdl_kd_dead_cross t;

select * from mdl_kd_dead_cross t where t.stock_code='002026' order by t.buy_date desc;

select * from mdl_kd_dead_cross t order by t.buy_date desc;

/*delete from mdl_kd_dead_cross t
where t.buy_date=to_date('2023-05-29','yyyy-mm-dd') or t.sell_date=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

select count(*) from mdl_kd_dead_cross t where t.buy_date=to_date('2022-06-23','yyyy-mm-dd') or t.buy_date=to_date('2022-06-24','yyyy-mm-dd');

select
(select count(*) from mdl_kd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_kd_dead_cross t)*100
from dual;

select max(t.buy_date), max(t.sell_date) from mdl_kd_dead_cross t;

-- truncate table mdl_kd_dead_cross;

------------------------------------- MDL_VOLUME_TURNOVER_UP_MA250 ----------------------------------------

-- truncate table MDL_VOLUME_TURNOVER_UP_MA250;

select * from MDL_VOLUME_TURNOVER_UP_MA250 t where t.stock_code='600615' order by t.buy_date asc;
select * from stock_transaction_data_all t where t.code_='600615' and t.date_>=to_date('19980114','yyyy-mm-dd')
order by t.date_ asc;

select * from MDL_VOLUME_TURNOVER_UP_MA250 t where t.buy_date=t.sell_date;

------------------------------------------ mdl_week_kd_gold_cross ---------------------------------------------
select
(select count(*) from mdl_week_kd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_week_kd_gold_cross t)*100
from dual;

select
(select sum(t.profit_loss) from mdl_week_kd_gold_cross t)/
(select count(*) from mdl_week_kd_gold_cross t)
from dual;

select max(t.sell_begin_date), min(t.sell_end_date) from mdl_week_kd_gold_cross t;


----------------------------------------- mdl_all_gold_cross -------------------------------------------

select t1.type_,
(select count(*) from mdl_all_gold_cross t where t.type_=t1.type_ and t.profit_loss>0)/
(select count(*) from mdl_all_gold_cross t where t.type_=t1.type_)*100 sort_
from mdl_all_gold_cross t1 group by t1.type_ order by sort_ desc;

select
(select sum(t.profit_loss) from mdl_all_gold_cross t where t.type_='c_5')/
(select count(*) from mdl_all_gold_cross t where t.type_='c_5')
from dual;

select * from mdl_all_gold_cross t where t.type_='5_10' and t.stock_code='300063'
order by t.sell_date desc;

/*delete from MDL_ALL_GOLD_CROSS t where t.type_='c_10';
commit;*/

-- truncate table mdl_all_gold_cross;

---------------------------------------- mdl_week_all_gold_cross --------------------------------------------

select t.type_,
(select count(*) from mdl_week_all_gold_cross t1 where t1.profit_loss>0 and t1.type_=t.type_)/
(select count(*) from mdl_week_all_gold_cross t1 where t1.type_=t.type_)*100 sort_
from MDL_WEEK_ALL_GOLD_CROSS t group by t.type_ order by sort_ desc;

---------------------------------------- STOCK_TOP_RANKING --------------------------------------------

select * from STOCK_TOP_RANKING t order by t.search_result_number desc;

select count(*) from STOCK_TOP_RANKING t;

-- truncate table stock_top_ranking;

---------------------------------------- board2_index --------------------------------------------

select * from board2_index t order by t.transaction_date desc;

select * from board2_index_detail t order by t.transaction_date desc;

/*truncate table board2_index;
truncate table board2_index_detail;*/

------------------------------------- mdl_stock_index_macd_g_c -----------------------------------------

select * from mdl_stock_index_macd_g_c t order by t.sell_date desc;

select * from mdl_stock_index_macd_g_c t where t.stock_index_code='399006' and t.type_=6 
order by t.sell_date desc;

select min(t.date_) from stock_index t where t.code_=399006;

select * from stock_index t 
where t.code_='399006' and t.date_ between to_date('20101101','yyyy-mm-dd') and to_date('20221231','yyyy-mm-dd')
order by t.date_ asc;

select 
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_macd_g_c t where t.stock_index_code='000001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_macd_g_c t where t.stock_index_code='000001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_macd_g_c t where t.stock_index_code='399001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_macd_g_c t where t.stock_index_code='399001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_macd_g_c t where t.stock_index_code='399005' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_macd_g_c t where t.stock_index_code='399005') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_macd_g_c t where t.stock_index_code='399006' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_macd_g_c t where t.stock_index_code='399006') * 12
from dual;

select t.stock_index_code, 
(select * from (
(select t1.accumulative_profit_loss from mdl_stock_index_macd_g_c t1 
where t1.stock_index_code=t.stock_index_code and t1.buy_date between to_date('20110101','yyyy-mm-dd') and to_date('20221231','yyyy-mm-dd')) 
order by t1.buy_date desc) where rownum<=1)
from mdl_stock_index_macd_g_c t 
where t.buy_date between to_date('20110101','yyyy-mm-dd') and to_date('20221231','yyyy-mm-dd') 
group by t.stock_index_code;

/*update mdl_stock_index_macd_g_c t set t.type=4;
commit;*/

-- truncate table mdl_stock_index_macd_g_c;

------------------------------------- mdl_stock_index_c_p_ma5_g_c -----------------------------------------

select * from mdl_stock_index_c_p_ma5_g_c t order by t.sell_date desc;

select * from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399006' and t.type_=6 
order by t.sell_date desc;

select 
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='000001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='000001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399005' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399005') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399006' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_c_p_ma5_g_c t where t.stock_index_code='399006') * 12
from dual;

/*update mdl_stock_index_c_p_ma5_g_c t set t.type=4;
commit;*/

-- truncate table mdl_stock_index_c_p_ma5_g_c;

------------------------------------- mdl_stock_index_h_k_a_up_down -------------------------------------

select * from mdl_stock_index_h_k_a_up_down t order by t.sell_date desc;

select * from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399006' and t.type_=6 
order by t.sell_date desc;

select 
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='000001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='000001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399005' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399005') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399006' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_h_k_a_up_down t where t.stock_index_code='399006') * 12
from dual;

/*update mdl_stock_index_h_k_a_up_down t set t.type=4;
commit;*/

-- truncate table mdl_stock_index_h_k_a_up_down;

------------------------------------- mdl_stock_index_kd_gold_cross -------------------------------------

select * from mdl_stock_index_kd_gold_cross t order by t.sell_date desc;

select * from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399006' and t.type_=6 
order by t.sell_date desc;

select 
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_kd_gold_cross t where t.stock_index_code='000001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399001' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399001') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399005' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399005') * 12,
(select * from (
select t.accumulative_profit_loss - 100 from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399006' order by t.sell_date desc)
where rownum<=1)/
(select count(*) from mdl_stock_index_kd_gold_cross t where t.stock_index_code='399006') * 12
from dual;

/*update mdl_stock_index_kd_gold_cross t set t.type=4;
commit;*/

-- truncate table mdl_stock_index_kd_gold_cross;

--------------------------------------- ETF_INFO ---------------------------------------------

select * from ETF_INFO t;

select * from ETF_INFO t where t.code_ like '%510880%';

select * from etf_info t where t.tendency_type=2;

--------------------------------------- ETF_TRANSACTION_DATA ---------------------------------------------

select count(*) from etf_transaction_data;

select max(t.date_), min(t.date_) from etf_transaction_data t;
      
select * from etf_transaction_data t where t.date_=to_date('2024-3-4','yyyy-mm-dd');

select * from etf_transaction_data t where t.code_ = '159512' /*and t.date_<=to_date('2023-10-18','yyyy-mm-dd') */
order by t.date_ desc;

/*update etf_transaction_data t set t.last_close_price=null;
commit;*/

select t.code_, min(t.date_) from etf_transaction_data t group by t.code_ order by min(t.date_) asc;

/*insert into robot7_stock_filter(stock_code);
select distinct t1.code_ 
from etf_transaction_data t1 
join etf_info ei on substr(ei.code_, 3, 8)=t1.code_ and ei.tendency_type=2 
where t1.date_=to_date('2024-2-23','yyyy-mm-dd')
and t1.correlation250_with_avg_c_p is not null
and t1.code_ not in(select t.stock_code from robot7_stock_transact_record t 
where t.sell_date is null and t.sell_price is null and t.sell_amount is null);*/

select 
(select count(*) from etf_transaction_data t
join etf_info ei on substr(ei.code_, 3, 8)=t.code_ and ei.tendency_type=2 
where t.code_ in(select t1.code_--, ei.name_, avg(t.turnover) 
from ETF_TRANSACTION_DATA t1
join etf_info ei on substr(ei.code_, 3, 8)=t1.code_ and ei.available=1 and ei.tendency_type=2
group by t1.code_, ei.name_ having avg(t1.turnover)>10000)
and t.ma250 is not null and t.correlation250_with_avg_c_p is not null)/
(select count(*) from etf_transaction_data t
join etf_info ei on substr(ei.code_, 3, 8)=t.code_ and ei.tendency_type=2
where t.code_ in(select t1.code_--, ei.name_, avg(t.turnover) 
from ETF_TRANSACTION_DATA t1 
join etf_info ei on substr(ei.code_, 3, 8)=t1.code_ and ei.available=1 and ei.tendency_type=2
group by t1.code_, ei.name_ having avg(t1.turnover)>10000)
and t.ma250 is not null)*100
from dual;

/*update etf_transaction_data t 
set t.ma5=null, t.ma10=null, t.ma20=null, t.ma60=null, t.ma120=null, t.ma250=null,
t.ha_open_price=null, t.ha_close_price=null, t.ha_highest_price=null, t.ha_lowest_price=null,
t.bias5=null, t.bias10=null, t.bias20=null, t.bias60=null, t.bias120=null, t.bias250=null,
t.ema12=null, t.ema26=null, t.dif=null, t.dea=null;
commit;*/

------------------------------------------ mdl_etf_macd_gold_cross ---------------------------------------
select * from mdl_etf_macd_gold_cross t where t.type_=1 order by t.sell_date desc;

select t.type_, count(*) from mdl_etf_macd_gold_cross t group by t.type_;

select max(t.accumulative_profit_loss) from mdl_etf_macd_gold_cross t;

select t.code_, (select ei.name_ from etf_info ei where substr(ei.code_, 3)=t.code_), 
count(*),
(select t222.accumulative_profit_loss from mdl_etf_macd_gold_cross t222 where t222.etf_code=t.code_ and t222.type_=4 and t222.sell_date=
(select t1.sell_date from (select * from mdl_etf_macd_gold_cross memgccc where memgccc.etf_code=t.code_ and memgccc.type_=4 order by memgccc.sell_date desc) t1 where rownum<=1)
),
(select t22.accumulative_profit_loss from mdl_etf_macd_gold_cross t22 where t22.etf_code=t.code_ and t22.type_=4 and t22.sell_date=
(select t1.sell_date from (select * from mdl_etf_macd_gold_cross memgcc where memgcc.etf_code=t.code_ and memgcc.type_=4 order by memgcc.sell_date desc) t1 where rownum<=1)
)/(select count(*) from etf_transaction_data me where me.code_=t.code_)
from etf_transaction_data t
group by t.code_ order by (select t2.accumulative_profit_loss from mdl_etf_macd_gold_cross t2 where t2.etf_code=t.code_ and t2.type_=4 and t2.sell_date=
(select t1.sell_date from (select * from mdl_etf_macd_gold_cross memgc where memgc.etf_code=t.code_ and memgc.type_=4 order by memgc.sell_date desc) t1 where rownum<=1)
)/(select count(*) from etf_transaction_data mee where mee.code_=t.code_) desc;

select * from mdl_etf_macd_gold_cross t where t.buy_date=to_date('2024-6-4','yyyy-mm-dd') 
or t.sell_date=to_date('2024-6-4','yyyy-mm-dd');

select * from mdl_etf_macd_gold_cross t where t.type_=1 and t.etf_code=510880 order by t.buy_date asc;

/*delete from mdl_etf_macd_gold_cross t 
where t.buy_date>=to_date('2024-7-5','yyyy-mm-dd') or t.sell_date>=to_date('2024-7-5','yyyy-mm-dd');
commit;*/

--------------------------------------- mdl_etf_close_price_ma5_g_c -------------------------------------
select * from mdl_etf_close_price_ma5_g_c t where t.type_=1 order by t.sell_date desc;

select t.type_, count(*) from mdl_etf_close_price_ma5_g_c t group by t.type_;

select max(t.accumulative_profit_loss) from mdl_etf_close_price_ma5_g_c t;

/*delete from mdl_etf_close_price_ma5_g_c t 
where t.buy_date>=to_date('2024-6-5','yyyy-mm-dd') or t.sell_date>=to_date('2024-6-5','yyyy-mm-dd');
commit;*/

--------------------------------------- mdl_etf_hei_kin_ashi_down_up -------------------------------------
select * from mdl_etf_hei_kin_ashi_down_up t where t.type_=1 order by t.sell_date desc;

select t.type_, count(*) from mdl_etf_hei_kin_ashi_down_up t group by t.type_;

select max(t.accumulative_profit_loss) from mdl_etf_hei_kin_ashi_down_up t;

/*delete from mdl_etf_hei_kin_ashi_down_up t 
where t.buy_date>=to_date('2024-6-5','yyyy-mm-dd') or t.sell_date>=to_date('2024-6-5','yyyy-mm-dd');
commit;*/

--------------------------------------- mdl_etf_kd_gold_cross -------------------------------------
select * from mdl_etf_kd_gold_cross t where t.type_=1 order by t.sell_date desc;

select t.type_, count(*) from mdl_etf_kd_gold_cross t group by t.type_;

select max(t.accumulative_profit_loss) from mdl_etf_kd_gold_cross t;

/*delete from mdl_etf_kd_gold_cross t 
where t.buy_date>=to_date('2024-12-11','yyyy-mm-dd') or t.sell_date>=to_date('2024-12-11','yyyy-mm-dd');
commit;*/

--------------------------------------- mdl_etf_bull_short_line_up -------------------------------------

select * from mdl_etf_bull_short_line_up t order by t.sell_date asc;

select * from etf_transaction_data t where t.code_=512720 order by t.date_ desc;

select t.etf_code, min(t.buy_date) from mdl_etf_bull_short_line_up t
join etf_info ei on substr(ei.code_, 3, 8)=t.etf_code and ei.tendency_type=2
join etf_transaction_data etd on etd.code_=t.etf_code
group by t.etf_code having avg(etd.turnover)>1000
order by min(t.buy_date) asc;

---------------------------------------- stock_transaction_data --------------------------------------------

select count(*) from stock_transaction_data t where t.date_=to_date('2025-3-20','yyyy-mm-dd');
select count(*) from stock_index t where t.date_=to_date('2025-3-20','yyyy-mm-dd');
select count(*) from stock_transaction_data_all t where t.date_=to_date('2025-3-20','yyyy-mm-dd');
select count(*) from etf_transaction_data t where t.date_=to_date('2025-3-20','yyyy-mm-dd');

select t.code_, count(*) from etf_transaction_data t where t.date_=to_date('2024-10-08','yyyy-mm-dd') group by t.code_ having count(*)>1;

select * from stock_info t where t.code_ like '000%';

select avg(t.ha_week_close_price), avg(t.close_price) from STOCK_WEEK t 
where t.begin_date>=to_date('2023-2-14','yyyy-mm-dd') and t.end_date<=to_date('2025-2-14','yyyy-mm-dd');

select * from STOCK_WEEK t 
where t.begin_date>=to_date('2023-2-13','yyyy-mm-dd') and t.end_date<=to_date('2023-2-17','yyyy-mm-dd') order by t.begin_date asc;

/*update stock_week t set t.ha_week_open_price=null, t.ha_week_close_price=null, t.ha_week_highest_price=null, t.ha_week_lowest_price=null;
commit;*/

/*update stock_info t set t.url_param=upper(t.code_);
update stock_info t set t.code_=substr(t.code_, 4);
commit;*/

/*delete from stock_transaction_data t where t.date_=to_date('2024-12-18','yyyy-mm-dd') and t.code_='600718';
commit;*/

/*delete from stock_transaction_data t where t.date_=to_date('2024-12-18','yyyy-mm-dd');
commit;*/

/*delete from etf_transaction_data t where t.date_=to_date('2024-12-23','yyyy-mm-dd');
commit;*/

select * from stock_transaction_data t where t.date_=to_date('2025-2-17','yyyy-mm-dd');

select * from stock_transaction_data_all t where t.date_=to_date('2014-9-3','yyyy-mm-dd');

select max(t.date_), min(t.date_) from stock_transaction_data t;
/*delete from stock_transaction_data t where t.date_<=to_date('2022-12-31','yyyy-mm-dd');
commit;*/

/*update stock_transaction_data_all t set t.ha_open_price=null, t.ha_close_price=null,
t.ha_highest_price=null, t.ha_lowest_price=null where t.date_=to_date('2022-10-25','yyyy-mm-dd');
commit;*/

select
(select count(*) from stock_transaction_data_all t where t.ha_close_price is not null)/
(select count(*) from stock_transaction_data_all t)*100
from dual;

select * from stock_transaction_data t where t.code_='002124' order by t.date_ desc;
select * from stock_transaction_data_all t where t.code_='000001' order by t.date_ desc;
select * from stock_index t where t.code_='000001' order by t.date_ desc;
select * from etf_transaction_data t where t.code_='510330' order by t.date_ desc;

select /*avg(t1.volume), avg(t1.turnover)*/ * from (
select * from stock_transaction_data t where t.code_='600802' and t.date_<=to_date('2024-12-13','yyyy-mm-dd') 
order by t.date_ desc) t1
where rownum<=250;

/*update stock_transaction_data t set t.change_amount=round(t.change_amount, 2) where t.date_=to_date('2025-2-14','yyyy-mm-dd');
update stock_transaction_data_all t set t.change_amount=round(t.change_amount, 2) where t.date_=to_date('2025-2-14','yyyy-mm-dd');
commit;*/

/*update stock_transaction_data t set t.change_range=round(t.change_range, 2);
update stock_transaction_data_all t set t.change_range=round(t.change_range, 2);
commit;*/


/*update stock_transaction_data t1 set t1.change_range_ex_right=
(select t2.change_range_ex_right from stock_transaction_data_all t2 where t2.code_=t1.code_ and t2.date_=t1.date_);
commit;*/

/*update stock_transaction_data t set t.volume_ma5=null, t.volume_ma10=null,t.volume_ma20=null,t.volume_ma60=null,
t.volume_ma120=null,t.volume_ma250=null where t.date_=to_date('2024-12-17','yyyy-mm-dd');
update stock_transaction_data t set t.turnover_ma5=null, t.turnover_ma10=null,t.turnover_ma20=null,
t.turnover_ma60=null,t.turnover_ma120=null,t.turnover_ma250=null 
where t.date_=to_date('2024-12-17','yyyy-mm-dd');
commit;*/

select count(*) from board_index t where t.board_id=1 order by t.date_ desc;
select count(t.date_) from stock_index t where t.code_='000001';

select * from stock_transaction_data_all t where t.code_='000001' order by t.date_ desc;
select * from board_index t where t.board_id=1 order by t.date_ desc;
select * from mdl_week_kd_gold_cross t order by t.sell_end_date desc;
select * from mdl_macd_gold_cross t order by t.sell_date desc;
select * from mdl_macd_dead_cross t order by t.buy_date desc;
select * from mdl_close_price_ma5_gold_cross t order by t.sell_date desc;
select * from mdl_close_price_ma5_dead_cross t order by t.buy_date desc;
select * from mdl_hei_kin_ashi_up_down t order by t.sell_date desc;
select * from mdl_hei_kin_ashi_down_up t order by t.buy_date desc;
select * from mdl_kd_gold_cross t order by t.sell_date desc;
select * from mdl_kd_dead_cross t order by t.buy_date desc;
select * from mdl_bull_short_line_up t order by t.sell_date desc;
select * from mdl_volume_turnover_up_ma250 t order by t.sell_date desc;
select * from mdl_stock_index_macd_g_c t order by t.sell_date desc;
select * from mdl_stock_index_c_p_ma5_g_c t order by t.sell_date desc;
select * from mdl_stock_index_h_k_a_down_up t order by t.sell_date desc;
select * from mdl_stock_index_kd_gold_cross t order by t.sell_date desc;
select * from mdl_etf_macd_gold_cross t order by t.sell_date desc;
select * from mdl_etf_close_price_ma5_g_c t order by t.sell_date desc;
select * from mdl_etf_hei_kin_ashi_down_up t order by t.sell_date desc;
select * from mdl_etf_kd_gold_cross t order by t.sell_date desc;
select * from mdl_etf_bull_short_line_up t order by t.sell_date desc;
select * from mdl_percentage_ma_gold_cross t order by t.sell_date desc;
select * from mdl_all_gold_cross t order by t.sell_date desc;
select * from mdl_stock_analysis t order by t.date_ desc;
select * from mdl_g_c_d_c_analysis t order by t.date_ desc;
select * from mdl_volume_turnover_up_ma250 t order by t.sell_date desc;
select * from mdl_week_boll_macd_gold_cross t order by t.sell_date desc;
select * from mdl_week_boll_c_p_ma5_g_c t order by t.sell_date desc;
select * from mdl_week_boll_h_k_a_up_down t order by t.sell_date desc;
select * from mdl_week_boll_kd_gold_cross t order by t.sell_date desc;
select * from mdl_week_boll_macd_dead_cross t order by t.sell_date desc;
select * from mdl_week_boll_c_p_ma5_d_c t order by t.sell_date desc;
select * from mdl_week_boll_h_k_a_down_up t order by t.sell_date desc;
select * from mdl_week_boll_kd_dead_cross t order by t.sell_date desc;
select * from mdl_top_stock_detail t order by t.date_ desc;
select * from mdl_stock_week_analysis t where t.code_='000001' order by t.begin_date desc;
select * from mdl_stock_month_analysis t order by t.end_date desc;
select * from stock_index_week t where t.code_='000001' order by t.end_date desc;
select * from stock_week t where t.code_='000001' order by t.end_date desc;
select * from stock_month t where t.code_='000001' order by t.end_date desc;

/*delete from mdl_stock_month_analysis t where t.begin_date>=to_date('2025-01-01','yyyy-mm-dd');
commit;*/

/*delete from stock_month t where t.begin_date>=to_date('2025-01-01','yyyy-mm-dd');
commit;*/

-- truncate table board_index;
-- truncate table MDL_HEI_KIN_ASHI_UP_DOWN;
-- truncate table mdl_hei_kin_ashi_down_up;
-- truncate table mdl_kd_gold_cross;
-- truncate table mdl_kd_dead_cross;
-- truncate table mdl_bull_short_line_up;
-- truncate table mdl_stock_index_macd_g_c;
-- truncate table mdl_stock_index_c_p_ma5_g_c;
-- truncate table mdl_stock_index_h_k_a_down_up;
-- truncate table mdl_stock_index_kd_gold_cross;

/*delete from stock_week t where t.begin_date>=to_date('2024-08-26','yyyy-mm-dd');
commit;*/

-- truncate table stock_transaction_data;
/*insert into stock_transaction_data select * from stock_transaction_data_all t1
where t1.date_>=to_date('2019-01-01','yyyy-mm-dd');*/

select max(t.id_) from mdl_stock_week_analysis t;

select * from stock_transaction_data_all t where t.last_close_price is null;

select t.code_, t.date_ from stock_index t group by t.code_, t.date_ having count(*)>1;

select * from stock_index t where t.date_=to_date('2024-3-4', 'yyyy-mm-dd') for update;

/*update stock_index t set t.ema12=null, t.ema26=null, t.dea=null, t.dif=null 
where t.date_=to_date('2023-8-29', 'yyyy-mm-dd');
commit;*/

select count(1) from stock_index t where t.dea is null;

-- truncate table stock_index;

/*update stock_index t set t.ha_index_open_price=null, t.ha_index_close_price=null,
t.ha_index_highest_price=null, t.ha_index_lowest_price=null;
commit;*/

select * from board_index t where t.date_=to_date('2024-03-04','yyyy-mm-dd');

select max(t.id_) from board_index t;

select * from stock_index_week t where t.begin_date>=to_date('2022-02-26','yyyy-mm-dd');

/*update stock_index_week t set t.ha_index_week_open_price=null, t.ha_index_week_close_price=null,
t.ha_index_week_highest_price=null, t.ha_index_week_lowest_price=null;*/

-- truncate table stock_index_week;

select count(*) from stock_week t where t.begin_date>=to_date('2023-04-10','yyyy-mm-dd');

/*delete from mdl_macd_gold_cross t
where t.buy_date>=to_date('2021-07-29','yyyy-mm-dd') or t.sell_date=to_date('2021-07-29','yyyy-mm-dd');
commit;*/

/*delete from mdl_close_price_ma5_gold_cross t
where t.buy_date>=to_date('2021-07-29','yyyy-mm-dd') or t.sell_date=to_date('2021-07-29','yyyy-mm-dd');
commit;*/

/*delete from mdl_hei_kin_ashi_up_down t
where t.buy_date>=to_date('2021-07-29','yyyy-mm-dd') or t.sell_date=to_date('2021-07-29','yyyy-mm-dd');
commit;*/

/*delete from mdl_stock_analysis t where t.date_=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

/*delete from mdl_top_stock t where t.date_=to_date('2021-07-29','yyyy-mm-dd');
commit;*/

/*delete from mdl_top_stock_detail t where t.date_=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

/*delete from board_index t where t.date_=to_date('2023-11-22','yyyy-mm-dd');
commit;*/

/*delete from stock_index t where t.date_=to_date('2024-12-18','yyyy-mm-dd');
commit;*/

/*delete from stock_transaction_data t where t.date_=to_date('2023-2-17','yyyy-mm-dd');
commit;*/

/*delete from stock_transaction_data_all t where t.date_=to_date('2024-6-5','yyyy-mm-dd');
commit;*/

/*delete from stock_week t where t.begin_date>=to_date('2024-02-26','yyyy-mm-dd');
commit;*/

/*delete from stock_index_week t where t.begin_date>=to_date('2024-02-26','yyyy-mm-dd');
commit;*/

/*delete from stock_month t where t.begin_date=to_date('2023-01-01','yyyy-mm-dd');
commit;*/

/*delete from mdl_stock_month_analysis t where t.begin_date>=to_date('2022-01-01','yyyy-mm-dd');
commit;*/

/*delete from mdl_g_c_d_c_analysis t where t.date_>=to_date('2023-05-29','yyyy-mm-dd');
commit;*/

select * from mdl_top_stock t order by t.date_ desc;

select * from mdl_top_stock_detail t order by t.date_ desc;

select * from mdl_stock_analysis t where t.date_=to_date('2023-1-30','yyyy-mm-dd');

select count(*) from mdl_stock_analysis t where t.date_=to_date('2024-4-17','yyyy-mm-dd');
select count(*) from mdl_stock_analysis t where t.date_=to_date('2024-4-18','yyyy-mm-dd');
select count(*) from mdl_stock_analysis t where t.date_=to_date('2024-4-19','yyyy-mm-dd');

select * from mdl_stock_analysis t where t.code_='000004' order by t.date_ desc;

select count(*) from mdl_bull_short_line_up t;

select max(t.sell_date) from mdl_bull_short_line_up t;

select * from mdl_bull_short_line_up t order by t.sell_date desc;

select * from stock_transaction_data_all t where t.code_='300736' order by t.date_ desc;
-- truncate table mdl_bull_short_line_up;

select * from stock_month t where t.code_='000002' order by t.begin_date desc;

select max(t.id_) from stock_month t;

select * from mdl_stock_month_analysis t where t.code_='000004' order by t.begin_date desc;

select max(t.end_date) from stock_month t;

/*delete from stock_month t where t.begin_date>=to_date('2023-01-01', 'yyyy-mm-dd');
commit;*/

select * from etf_transaction_data t where t.date_ = to_date('20240606', 'yyyy-mm-dd')
and t.correlation250_with_avg_c_p is not null;
       
--------------------------------------- real_stock_transaction_record ---------------------------------------------

select t.stock_code 股票代码, /*si_.name_ 股票名称,*/ t.buy_date 买入日期, t.buy_price 买入价格,
       t.buy_amount 买入数量, t.sell_date 卖出日期, t.sell_price 卖出价格, t.sell_amount 卖出数量,
       t.profit_loss_amount 盈亏数量, t.profit_loss_percentage 盈亏百分比--, sum(t.profit_loss_amount)
from real_stock_transaction_record t
join stock_info si_ on si_.code_=t.stock_code
order by t.buy_date desc;

select * from real_stock_transaction_record t;

select * from real_account for update;

select count(*) from real_transaction_condition;

select * from real_transaction_condition t where t.stock_code='600592';

--------------------------------------- real4_stock_transaction_record ---------------------------------------------

select * from real4_transaction_condition t
where t.stock_code not in(select t1.stock_code from real4_stock_transaction_record t1
where t1.direction=1 and t1.sell_date is null)
union
select * from real4_transaction_condition t
where t.stock_code not in(select t1.stock_code from real4_stock_transaction_record t1
where t1.direction=-1 and t1.buy_date is null);

select * from real4_stock_transaction_record t --where t.buy_date is null or t.sell_date is null
order by t.id_ desc for update;

select * from real4_account for update;

select count(*) from real4_transaction_condition;

select * from real4_transaction_condition t where t.stock_code='600592';

--------------------------------- real7_stock_transaction_record ---------------------------------------

select * from real7_account for update;

select * from real7_stock_transaction_record t where t.sell_date is null for update;

select * from real7_transaction_condition;
select count(*) from real7_transaction_condition;
select * from real7_stock_transaction_record;

select * from real7_transaction_condition t where t.is_limit_up=1;

select * from stock_transaction_data_all t where t.code_='600988' 
and t.date_=to_date('20241029','yyyy-mm-dd');

select * from real7_transaction_condition t where t.stock_code='600988';

select * from real7_transaction_condition t 
where t.close_price_out_of_limit is null and t.variance_type is not null and t.variance is not null 
and t.xr is null and t.suspension is null and t.filter_type is not null and t.accumulative_profit_loss is not null 
and t.direction is not null and t.is_transaction_date = 1 and t.direction = 1 and t.is_limit_up = 2 
order by t.accumulative_profit_loss desc;

select * from real7_transaction_condition t where t.direction=1 order by t.accumulative_profit_loss desc;

SELECT t1.date_, t1.AVERAGE_CLOSE_PRICE, 
round(AVG(t1.AVERAGE_MA5) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA5, 
round(AVG(t1.AVERAGE_MA10) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA10, 
round(AVG(t1.AVERAGE_MA20) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA20, 
round(AVG(t1.AVERAGE_MA60) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA60, 
round(AVG(t1.AVERAGE_MA120) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA120, 
round(AVG(t1.AVERAGE_MA250) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA250 
FROM (select to_char(t.date_, 'yyyy-mm-dd') DATE_, avg(t.close_price) AVERAGE_CLOSE_PRICE, avg(t.ma5) AVERAGE_MA5, avg(t.ma10) AVERAGE_MA10, avg(t.ma20) AVERAGE_MA20, avg(t.ma60) AVERAGE_MA60, avg(t.ma120) AVERAGE_MA120, avg(t.ma250) AVERAGE_MA250 from stock_transaction_data_all t where t.date_ between add_months(to_date('20140903','yyyy-mm-dd'), -24) and to_date('20140903','yyyy-mm-dd') group by t.date_ order by t.date_ desc) t1;

/*update real7_transaction_condition t1 
set t1.prepare_macd_gold_cross=(
select (t.ema12 * (11 / 13 * 2 / 10 - 11 / 13) + t.ema26 * (25 / 27 - 25 / 27 * 2 / 10) + t.dea * 8 / 10) / 
(2 / 13 - 2 / 27 - 2 / 13 * 2 / 10 + 2 / 27 * 2 / 10) 
from stock_transaction_data_all t where t.code_ = '600757' and t.date_ = (
select std.date_ from (select * from stock_transaction_data_all t2 
where t2.code_ = '600757' and t2.date_ <= to_date('20140903', 'yyyy-mm-dd') order by t2.date_ desc) std 
where rownum <= 1)) where t1.stock_code = '600757';*/

-- truncate table real7_transaction_condition;
-- truncate table real7_stock_transaction_record;

------------------------------------------ MODEL ------------------------------------------

select t.*, t.rowid from MODEL t order by t.id desc for update;

select substr(t.id, 0, 3), avg(t.average_annual_return_rate), count(t.average_annual_return_rate) from model t 
group by substr(t.id, 0, 3) order by substr(t.id, 0, 3) desc;

select t.begin_time, avg(t.average_annual_return_rate), count(t.average_annual_return_rate) 
from model t where t.id between 4012011 and 4992022 or t.id in(432, 433)
group by t.begin_time order by avg(t.average_annual_return_rate) desc;

select t.id, t.begin_time, t.average_annual_return_rate from model t where t.id like '432%' order by t.begin_time asc;

select * from model t order by t.average_annual_return_rate asc;

/*update model t
set t.ma120_not_increasing=-1, t.ma120_not_decreasing=-1, t.ma250_not_increasing=-1, t.ma250_not_decreasing=-1;*/

/*delete from model t where t.algorithm_description='周线级别布林带突破上轨或跌破下轨';
commit;*/

-- truncate table model;

------------------------------------------ ROBOT_ACCOUNT ------------------------------------------

select * from ROBOT_ACCOUNT for update;

select count(*) from robot_stock_filter;

select t.*, t.total_assets/7000 from ROBOT_ACCOUNT t;

select * from robot_stock_transaction_record t order by t.id_ desc for update;

select * from robot_stock_transaction_record t where t.filter_type in(1,2,7,8) order by t.id_ asc;

select * from robot_account_log t order by t.id_ desc for update;

select * from robot_stock_filter t where t.stock_code in('600084');

select * from stock_transaction_data_all t
where t.code_='000019' and t.date_<=to_date('2011-3-14','yyyy-mm-dd') order by t.date_ desc;

select
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='黑猫警长') 黑猫警长,
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='一只耳') 一只耳,
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='蓝精灵') 蓝精灵,
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='格格巫') 格格巫
from dual;

select
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='蓝精灵' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='蓝精灵')*100 蓝精灵,
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='格格巫' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='格格巫')*100 格格巫,
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='黑猫警长' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='黑猫警长')*100 黑猫警长,
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='一只耳' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT_STOCK_TRANSACTION_RECORD t where t.robot_name='一只耳')*100 一只耳
from dual;

select max(t.buy_date), max(t.sell_date) from robot_stock_transaction_record t;

/*truncate table ROBOT_STOCK_TRANSACTION_RECORD;
truncate table robot_account_log;*/

/*create table robot_account2 as select * from robot_account;
create table robot_stock_filter2 as select * from robot_stock_filter;
create table transaction_record2 as select * from robot_stock_transaction_record;
create table robot_account_log2 as select * from robot_account_log;*/

------------------------------------------ HIS_ROBOT_* ------------------------------------------

select t.*, t.total_assets/2800 from HIS_ROBOT_ACCOUNT t where t.model_id=7219 for update;

select count(*) from his_robot_transaction_record t where t.model_id=7219 for update;

select * from his_robot_transaction_record t where t.model_id=7308 order by t.profit_and_loss_rate desc;

select avg(t.profit_and_loss_rate) from his_robot_transaction_record t 
join stock_transaction_data_all stda on stda.code_=t.stock_code and stda.date_=t.buy_date
where t.model_id=7221 and stda.close_price=stda.open_price and stda.change_range>=10;

select t.* from his_robot_transaction_record t 
join stock_transaction_data_all stda on stda.code_=t.stock_code and stda.date_=t.buy_date
where t.model_id=7221 and stda.close_price!=stda.open_price /*and stda.change_range>=10*/ order by t.profit_and_loss_rate desc;

select * from his_robot_transaction_record t where t.model_id=7219 order by t.sell_date asc;

select * from HIS_ROBOT_ACCOUNT t where t.model_id=7206 and t.capital_assets<0;

select * from his_robot_transaction_record t where t.model_id=7206 and t.stock_code like '1%' order by t.profit_and_loss_rate asc;

select count(*) from his_robot_stock_filter t where t.model_id=7214;

select substr(t.model_id, 0, 7) from his_robot_transaction_record t 
group by substr(t.model_id, 0, 7) order by substr(t.model_id, 0, 7) desc;

select t.*, t.total_assets/2800 from his_robot_account t where t.model_id=7211 order by t.id_ asc;
select t.filter_type, count(*) from his_robot_transaction_record t where t.model_id=7211 group by t.filter_type;
select count(*), count(*)/count(distinct t.buy_date)/4 from his_robot_transaction_record t where t.model_id=7211;
select t.robot_name, count(*) from his_robot_transaction_record t where t.model_id=7211 group by t.robot_name;
select * from his_robot_account_log t where t.model_id=7211 order by t.date_ desc;
select max(t.total_assets)/2800, min(t.total_assets)/2800 from his_robot_account_log t where t.model_id=7211;
select t.id_, t.robot_name, t.total_registrate_fee_when_buy+t.total_registrate_fee_when_sell, t.total_commission_when_buy+t.total_commission_when_sell
from his_robot_account t where t.model_id=7211;

select * from his_robot_transaction_record t where t.model_id=7001 and t.robot_name='红中' 
and t.sell_date between to_date('20111101','yyyy-mm-dd') and to_date('20120131','yyyy-mm-dd')
order by t.profit_and_loss_rate asc;

select * from stock_transaction_data_all t where t.code_='002646' and t.date_<=to_date('20120113','yyyy-mm-dd') order by t.date_ desc;

select t.date_, avg(t.hold_stock_number) from his_robot_account_log t where t.model_id=6019
group by t.date_ order by avg(t.hold_stock_number) desc;

select * from his_robot_account_log t where t.model_id=7120 and t.capital_assets<0 order by t.date_ asc;

select * from mdl_etf_bull_short_line_up t where t.etf_code = 510880 order by t.buy_date asc;--and t.buy_date = to_date('2011-7-11', 'yyyy-mm-dd');

select distinct t.robot_name from his_robot_transaction_record t where t.model_id=305;

select 
(select count(*) from his_robot_transaction_record t where t.model_id=5042015) total,
(select count(*) from his_robot_transaction_record t1 where t1.model_id=5042015 and t1.filter_type between 1 and 8) robot4_number,
(select count(*) from his_robot_transaction_record t1 where t1.model_id=5042015 and t1.filter_type between 1 and 8)/
(select count(*) from his_robot_transaction_record t1 where t1.model_id=5042015)*100 robot4_percentage,
(select sum(t1.profit_and_loss_rate) from his_robot_transaction_record t1 where t1.model_id=5042015 and t1.filter_type between 1 and 8) robot4_profit_percentage,
(select sum(t1.profit_and_loss) from his_robot_transaction_record t1 where t1.model_id=5042015 and t1.filter_type between 1 and 8) robot4_profit,
(select count(*) from his_robot_transaction_record t2 where t2.model_id=5042015 and t2.filter_type between 9 and 10) robot3_number,
(select count(*) from his_robot_transaction_record t2 where t2.model_id=5042015 and t2.filter_type between 9 and 10)/
(select count(*) from his_robot_transaction_record t2 where t2.model_id=5042015)*100 robot3_percentage,
(select sum(t2.profit_and_loss_rate) from his_robot_transaction_record t2 where t2.model_id=5042015 and t2.filter_type between 9 and 10) robot3_profit_percentage,
(select sum(t2.profit_and_loss) from his_robot_transaction_record t2 where t2.model_id=5042015 and t2.filter_type between 9 and 10) robot3_profit
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='白起' and t.model_id=466) 白起,
(select count(*) from his_robot_transaction_record t where t.robot_name='王翦' and t.model_id=466) 王翦,
(select count(*) from his_robot_transaction_record t where t.robot_name='廉颇' and t.model_id=466) 廉颇,
(select count(*) from his_robot_transaction_record t where t.robot_name='李牧' and t.model_id=466) 李牧
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='黑猫警长' and t.model_id=8) 黑猫警长,
(select count(*) from his_robot_transaction_record t where t.robot_name='一只耳' and t.model_id=8) 一只耳,
(select count(*) from his_robot_transaction_record t where t.robot_name='蓝精灵' and t.model_id=8) 蓝精灵,
(select count(*) from his_robot_transaction_record t where t.robot_name='格格巫' and t.model_id=8) 朱格格巫雀
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='青龙' and t.model_id=206) 青龙,
(select count(*) from his_robot_transaction_record t where t.robot_name='白虎' and t.model_id=206) 白虎,
(select count(*) from his_robot_transaction_record t where t.robot_name='玄武' and t.model_id=206) 玄武,
(select count(*) from his_robot_transaction_record t where t.robot_name='朱雀' and t.model_id=206) 朱雀
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='威震天' and t.model_id=3012011) 威震天,
(select count(*) from his_robot_transaction_record t where t.robot_name='擎天柱' and t.model_id=3012011) 擎天柱,
(select count(*) from his_robot_transaction_record t where t.robot_name='漩涡鸣人' and t.model_id=3012011) 漩涡鸣人,
(select count(*) from his_robot_transaction_record t where t.robot_name='宇智波佐助' and t.model_id=3012011) 宇智波佐助
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='威震天' and t.model_id=43 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='威震天' and t.model_id=43)*100 威震天,
(select count(*) from his_robot_transaction_record t where t.robot_name='擎天柱' and t.model_id=43 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='擎天柱' and t.model_id=43)*100 擎天柱,
(select count(*) from his_robot_transaction_record t where t.robot_name='漩涡鸣人' and t.model_id=43 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='漩涡鸣人' and t.model_id=43)*100 漩涡鸣人,
(select count(*) from his_robot_transaction_record t where t.robot_name='宇智波佐助' and t.model_id=43 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='宇智波佐助' and t.model_id=43)*100 宇智波佐助
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='青龙' and t.model_id=20 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='青龙' and t.model_id=21)*100 青龙,
(select count(*) from his_robot_transaction_record t where t.robot_name='白虎' and t.model_id=21 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='白虎' and t.model_id=21)*100 白虎,
(select count(*) from his_robot_transaction_record t where t.robot_name='玄武' and t.model_id=21 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='玄武' and t.model_id=21)*100 玄武,
(select count(*) from his_robot_transaction_record t where t.robot_name='朱雀' and t.model_id=21 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='朱雀' and t.model_id=21)*100 朱雀
from dual;

select
(select count(*) from his_robot_transaction_record t1 where t1.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t1) * 100
from dual;

select t.filter_type,
  count(t.id_)/(select count(1) from his_robot_transaction_record)*100 percentage,
  ((select count(t2.id_) from his_robot_transaction_record t2
           where t2.profit_and_loss>0 and t2.filter_type=t.filter_type)/
  (select count(1) from his_robot_transaction_record t3 where t3.profit_and_loss>0)*100) percentage_in_profit
from his_robot_transaction_record t
group by t.filter_type order by percentage_in_profit desc;

select t.direction,
  count(t.id_)/(select count(*) from his_robot_transaction_record)*100 percentage,
  ((select count(t2.id_) from his_robot_transaction_record t2
           where t2.profit_and_loss>0 and t2.direction=t.direction)/
  (select count(*) from his_robot_transaction_record t3 where t3.profit_and_loss>0)*100) percentage_in_profit
from his_robot_transaction_record t
group by t.direction order by percentage desc;

select t.filter_type, count(1) from his_robot_transaction_record t
where (t.buy_date between to_date('2015-01-01','yyyy-mm-dd') and to_date('2015-12-31','yyyy-mm-dd'))
or (t.sell_date between to_date('2015-01-01','yyyy-mm-dd') and to_date('2015-12-31','yyyy-mm-dd'))
group by t.filter_type order by count(1) desc;

select to_char(t.buy_date, 'yyyy'), count(1) from his_robot_transaction_record t
where t.model_id in(17,16,15,14,13,12,11,10,5)
group by to_char(t.buy_date, 'yyyy') order by count(1) desc;

select * from his_robot_transaction_record t where to_char(t.buy_date,'yyyy')='2014' and t.stock_code='000963'
order by t.buy_date desc;

select distinct t1.stock_code from real_stock_transaction_record t1
where t1.buy_date>=to_date('2022-06-29','yyyy-mm-dd') or t1.sell_date>=to_date('2022-06-29','yyyy-mm-dd')
INTERSECT
select distinct t.stock_code from his_robot_transaction_record t where t.model_id=20;

select t.model_id, t.date_, count(*) from his_robot_account_log t group by t.model_id, t.date_ having count(*)>4;

select * from his_robot_account_log t where  t.model_id=7013 and t.date_=to_date('2011-4-7','yyyy-mm-dd') for update;

select * from his_robot_transaction_record t where t.model_id=7206;

/*update his_robot_account t set t.model_id=301 where t.model_id>=401;
commit;
update his_robot_account_log t set t.model_id=301 where t.model_id>=401;
commit;
update his_robot_transaction_record t set t.model_id=301 where t.model_id>=401;
commit;*/

/*delete from model t where t.id=7220;
delete from his_robot_transaction_record t where t.model_id=7220;
delete from his_robot_account_log t where t.model_id=7220;
delete from HIS_ROBOT_ACCOUNT t where t.model_id=7220;
commit;*/

/*truncate table his_robot_account;
truncate table his_robot_account_log;
truncate table his_robot_stock_filter;
truncate table his_robot_transaction_record;*/

/*create table temp_robot_account as select * from robot6_account;
create table temp_robot_stock_filter as select * from robot6_stock_filter;
create table temp_transaction_record as select * from robot6_stock_transact_record;
create table temp_robot_account_log as select * from robot6_account_log;*/

------------------------------------------ ROBOT2_ACCOUNT ------------------------------------------

select * from ROBOT2_ACCOUNT for update;

select count(*) from robot2_stock_filter;

select t.*, t.total_assets/7000 from ROBOT2_ACCOUNT t;

select * from ROBOT2_STOCK_TRANSACT_RECORD t order by t.id_ desc;

select * from robot2_account_log t order by t.id_ desc;

select
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='青龙') 青龙,
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='白虎') 白虎,
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='玄武') 玄武,
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='朱雀') 朱雀
from dual;

select
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='青龙' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='青龙')*100 青龙,
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='白虎' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='白虎')*100 白虎,
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='玄武' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='玄武')*100 玄武,
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='朱雀' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT2_STOCK_TRANSACT_RECORD t where t.robot_name='朱雀')*100 朱雀
from dual;

/*truncate table ROBOT2_STOCK_TRANSACT_RECORD;
truncate table robot2_account_log;*/

------------------------------------------ ROBOT3_ACCOUNT ------------------------------------------

select * from ROBOT3_ACCOUNT for update;

select count(*) from robot3_stock_filter;

select t.*, t.total_assets/2800 from ROBOT3_ACCOUNT t;

select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t;

select * from ROBOT3_STOCK_TRANSACT_RECORD t order by t.buy_date desc;

select * from robot3_account_log t order by t.id_ desc;

select * from robot3_stock_filter t where t.stock_code='601106';

select * from ROBOT3_STOCK_TRANSACT_RECORD t order by t.filter_type asc;

select * from stock_transaction_data_all t
where t.code_='600372' and t.date_<=to_date('2021-1-12','yyyy-mm-dd') order by t.date_ desc;

select
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='威震天') 威震天,
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='擎天柱') 擎天柱,
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='漩涡鸣人') 漩涡鸣人,
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='宇智波佐助') 宇智波佐助
from dual;

select
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='威震天' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='威震天')*100 威震天,
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='擎天柱' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='擎天柱')*100 擎天柱,
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='漩涡鸣人' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='漩涡鸣人')*100 漩涡鸣人,
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='宇智波佐助' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT3_STOCK_TRANSACT_RECORD t where t.robot_name='宇智波佐助')*100 宇智波佐助
from dual;

select t.date_, avg(t.hold_stock_number) from robot3_account_log t group by t.date_
order by avg(t.hold_stock_number) desc;

select max(t.buy_date), max(t.sell_date) from ROBOT3_STOCK_TRANSACT_RECORD t;

/*update robot3_account set hold_stock_number=0,stock_assets=0,capital_assets=280000,total_assets=280000;
truncate table robot3_stock_filter;
truncate table ROBOT3_STOCK_TRANSACT_RECORD;
truncate table robot3_account_log;
commit;*/

select to_char(t.date_, 'yyyy-mm-dd') , avg(t.close_price) averageClosePrice, avg(t.ma5) averageMa5, 
avg(t.ma10) averageMa10, avg(t.ma20) averageMa20, avg(t.ma60) averageMa60, avg(t.ma120) averageMa120, 
avg(t.ma250) averageMa250, avg(t.bias5) averageBias5, avg(t.bias10) averageBias10, 
avg(t.bias20) averageBias20, avg(t.bias60) averageBias60, avg(t.bias120) averageBias120, 
avg(t.bias250) averageBias250 
from stock_transaction_data_all t 
where t.date_ = to_date('2011-1-4','yyyy-mm-dd') group by t.date_;

------------------------------------------ ROBOT4_ACCOUNT ------------------------------------------

select * from ROBOT4_ACCOUNT for update;

select count(*) from robot4_stock_filter;

select t.*, t.total_assets/2800 from ROBOT4_ACCOUNT t;

select * from robot4_stock_transact_record t order by t.id_ desc for update;

select * from robot4_stock_transact_record t where t.robot_name='王翦' order by t.id_ desc;

select * from robot4_stock_transact_record t order by t.profit_and_loss_rate asc;
select * from robot4_stock_transact_record t order by t.profit_and_loss_rate desc;

select * from stock_transaction_data_all t
where t.code_='600654' and t.date_>=to_date('2022-5-9','yyyy-mm-dd') order by t.date_ asc;

select * from robot4_account_log t order by t.date_ desc;

select
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='白起') 白起,
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='王翦') 王翦,
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='廉颇') 廉颇,
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='李牧') 李牧
from dual;

select
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='白起' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='白起')*100 白起,
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='王翦' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='王翦')*100 王翦,
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='廉颇' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='廉颇')*100 廉颇,
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='李牧' and t.profit_and_loss_rate>0)/
(select count(*) from ROBOT4_STOCK_TRANSACT_RECORD t where t.robot_name='李牧')*100 李牧
from dual;

select
(select count(*) from robot4_stock_transact_record t where t.filter_type=1 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=1)*100 macd_gold_cross,
(select count(*) from robot4_stock_transact_record t where t.filter_type=2 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=2)*100 macd_dead_cross,
(select count(*) from robot4_stock_transact_record t where t.filter_type=3 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=3)*100 c_p_g_c_ma5,
(select count(*) from robot4_stock_transact_record t where t.filter_type=4 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=4)*100 c_p_d_c_ma5,
(select count(*) from robot4_stock_transact_record t where t.filter_type=5 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=5)*100 h_k_a_up,
(select count(*) from robot4_stock_transact_record t where t.filter_type=6 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=6)*100 h_k_a_down,
(select count(*) from robot4_stock_transact_record t where t.filter_type=7 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=7)*100 kd_gold_cross,
(select count(*) from robot4_stock_transact_record t where t.filter_type=8 and t.profit_and_loss_rate>0)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=8)*100 kd_dead_cross
from dual;

select
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=1)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=1) macd_gold_cross,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=2)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=2) macd_dead_cross,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=3)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=3) c_p_g_c_ma5,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=4)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=4) c_p_d_c_ma5,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=5)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=5) h_k_a_up,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=6)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=6) h_k_a_down,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=7)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=7) kd_gold_cross,
(select sum(t.profit_and_loss_rate) from robot4_stock_transact_record t where t.filter_type=8)/
(select count(*) from robot4_stock_transact_record t where t.filter_type=8) kd_dead_cross
from dual;

select t.date_, avg(t.hold_stock_number) from robot4_account_log t
group by t.date_ order by avg(t.hold_stock_number) desc;

/*truncate table ROBOT4_STOCK_TRANSACT_RECORD;
truncate table robot4_account_log;*/

------------------------------------------ ROBOT5_ACCOUNT ------------------------------------------

select * from ROBOT5_ACCOUNT for update;

select count(*) from robot5_stock_filter t where t.filter_type is null;
select * from robot5_stock_filter t where t.filter_type is not null;

select * from robot5_stock_filter t where t.stock_code='000061';

select t.*, t.total_assets/2800 from ROBOT5_ACCOUNT t;

select count(*) from robot5_stock_transact_record t;

select 
(select sum(t1.profit_and_loss_rate) from robot5_stock_transact_record t1 
where (case when t1.direction=1 then t1.buy_date when t1.direction=-1 then t1.sell_date end) 
between add_months(to_date('20160519', 'yyyy-mm-dd'),-3) and to_date('20160519', 'yyyy-mm-dd') and t1.filter_type between 1 and 8) robot4MainProfitAndLossRate, 
(select sum(t2.profit_and_loss_rate) from robot5_stock_transact_record t2 
where (case when t2.direction=1 then t2.buy_date when t2.direction=-1 then t2.sell_date end) 
between add_months(to_date('20160519', 'yyyy-mm-dd'),-3) and to_date('20160519', 'yyyy-mm-dd') and t2.filter_type between 9 and 10) robot3MainProfitAndLossRate 
from dual;

select 
(select count(*) from robot5_stock_transact_record t) total,
(select count(*) from robot5_stock_transact_record t1 where t1.filter_type between 1 and 8) robot4_number,
(select count(*) from robot5_stock_transact_record t1 where t1.filter_type between 1 and 8)/
(select count(*) from robot5_stock_transact_record t1)*100 robot4_percentage,
(select sum(t1.profit_and_loss_rate) from robot5_stock_transact_record t1 where t1.filter_type between 1 and 8) robot4_profit_percentage,
(select sum(t1.profit_and_loss) from robot5_stock_transact_record t1 where t1.filter_type between 1 and 8) robot4_profit,
(select count(*) from robot5_stock_transact_record t2 where t2.filter_type between 9 and 10) robot3_number,
(select count(*) from robot5_stock_transact_record t2 where t2.filter_type between 9 and 10)/
(select count(*) from robot5_stock_transact_record t2)*100 robot3_percentage,
(select sum(t2.profit_and_loss_rate) from robot5_stock_transact_record t2 where t2.filter_type between 9 and 10) robot3_profit_percentage,
(select sum(t2.profit_and_loss) from robot5_stock_transact_record t2 where t2.filter_type between 9 and 10) robot3_profit
from dual;

select * from robot5_stock_transact_record t 
where t.buy_date is null or t.sell_date is null order by t.id_ asc;

select * from robot5_stock_transact_record t order by t.profit_and_loss_rate asc;

select * from stock_transaction_data_all t where t.code_='600886' order by t.date_ desc;

select * from robot5_account_log t order by t.date_ desc;

select * from stock_transaction_data_all t where t.code_='000001'
and t.date_ between add_months(to_date('20230714', 'yyyy-mm-dd'),-3) and to_date('20230714', 'yyyy-mm-dd')
order by t.date_ asc;
                
select 
(select count(*) from robot5_stock_transact_record t) total,
(select count(*) from robot5_stock_transact_record t1 where t1.filter_type between 1 and 8) robot4_number,
(select count(*) from robot5_stock_transact_record t1 where t1.filter_type between 1 and 8)/
(select count(*) from robot5_stock_transact_record t1)*100 robot4_percentage,
(select count(*) from robot5_stock_transact_record t2 where t2.filter_type between 9 and 10) robot3_number,
(select count(*) from robot5_stock_transact_record t2 where t2.filter_type between 9 and 10)/
(select count(*) from robot5_stock_transact_record t2)*100 robot3_percentage
from dual;

select * from robot5_stock_transact_record t where t.filter_type between 1 and 8;
select * from robot5_stock_transact_record t where t.filter_type between 9 and 10;

/*truncate table ROBOT5_STOCK_TRANSACT_RECORD;
truncate table robot5_account_log;*/

---------------------------------------- ROBOT6_ACCOUNT --------------------------------------------
select * from ROBOT6_ACCOUNT for update;

select * from ROBOT6_STOCK_FILTER t;

select t.*, t.total_assets/2800 from ROBOT6_ACCOUNT t;

select * from robot6_account_log t order by t.date_ desc;

select count(*) from robot6_stock_transact_record t;
select * from robot6_stock_transact_record t order by t.buy_date desc;

select * from robot6_stock_transact_record t order by t.profit_and_loss_rate asc;

select * from ROBOT6_STOCK_FILTER t where t.stock_code='518800';

select * from mdl_etf_close_price_ma5_g_c t where t.etf_code='513500' and t.type_=4 order by t.buy_date desc;

select t.filter_type, avg(t.profit_and_loss_rate) from robot6_stock_transact_record t 
group by t.filter_type order by avg(t.profit_and_loss) desc;

select max(t.total_assets) from robot6_account_log t;

/*truncate table robot6_stock_transact_record;
truncate table robot6_account_log;
truncate table ROBOT6_STOCK_FILTER;*/

select * from mdl_etf_macd_gold_cross t where t.type_=3 and t.etf_code='510050' order by t.buy_date asc;

---------------------------------------- ROBOT7_ACCOUNT --------------------------------------------

select * from robot7_account for update;

select * from robot7_stock_filter;
select count(*) from robot7_stock_filter;

select t.* from robot7_stock_filter t where t.stock_code='600988';

select * from stock_transaction_data_all t where t.code_='600988' 
and t.date_=to_date('2024-10-29','yyyy-mm-dd');

select * from robot7_stock_transact_record t where t.sell_date is null order by t.profit_and_loss_rate desc;
select * from robot7_stock_transact_record t where t.sell_date is null order by t.profit_and_loss_rate asc;

select * from robot7_stock_transact_record t order by t.buy_date asc;

select * from robot7_account;
select * from robot7_account_log t order by t.date_ desc;
select * from robot7_stock_transact_record t where t.robot_name='白板' order by t.buy_date desc;

select t.robot_name, t.stock_code, count(*) from robot7_stock_transact_record t 
where t.sell_date is null group by t.robot_name, t.stock_code having count(*)>1;

select * from etf_transaction_data t where t.change_range>=10;

/*delete from robot7_stock_filter rsf where rsf.stock_code in(
select etd.code_ from etf_transaction_data etd 
where etd.date_=to_date('2023-1-3','yyyy-mm-dd') and etd.change_range>=10 and etd.open_price=etd.close_price);*/

select t.stock_code, sum(t.buy_price*t.buy_amount) from robot7_stock_transact_record t where t.robot_name='白板' group by t.stock_code;
select t.robot_name, sum(t.buy_price*t.buy_amount) from robot7_stock_transact_record t group by t.robot_name;
select * from robot7_stock_transact_record t order by t.profit_and_loss_rate desc;
select * from stock_transaction_data_all t where t.code_='600343' and t.date_>=to_date('2013-12-17','yyyy-mm-dd') order by t.date_ asc;
select max(t.sell_date-t.buy_date) from robot7_stock_transact_record t;

select t.robot_name, sum(t.profit_and_loss) from robot7_stock_transact_record t group by t.robot_name;

select min(t.profit_and_loss_rate) from robot7_stock_transact_record t;

select * from robot7_stock_transact_record t order by t.profit_and_loss_rate desc;
select * from robot7_stock_transact_record t where t.filter_type=9 order by t.profit_and_loss_rate asc;

select * from etf_transaction_data t where t.code_=510880 and t.date_>=to_date('2011-7-4','yyyy-mm-dd') 
order by t.date_ asc;

select * from robot7_stock_transact_record t order by t.buy_date desc;

select t.stock_code, t.sell_date, count(*) from robot7_stock_transact_record t 
group by t.stock_code, t.sell_date having count(*)>1;

select * from robot7_stock_transact_record t where t.sell_date is null;

select sum(t.buy_price*t.buy_amount) from robot7_stock_transact_record t where t.robot_name='白板';

select * from robot7_stock_transact_record t where t.buy_amount=0;

select * from robot7_account_log t where t.capital_assets<0 order by t.date_ asc;

select t.*, t.total_assets/2800 from robot7_account t order by t.id_ asc;
select t.filter_type, count(*) from robot7_stock_transact_record t group by t.filter_type;
select count(*), count(*)/count(distinct t.buy_date)/4 from robot7_stock_transact_record t;
select t.robot_name, count(*) from robot7_stock_transact_record t group by t.robot_name;
select * from robot7_account_log t order by t.date_ desc;
select t.robot_name, max(t.total_assets)/2800, min(t.total_assets)/2800 
from robot7_account_log t group by t.robot_name;
select t.id_, t.robot_name, t.total_registrate_fee_when_buy+t.total_registrate_fee_when_sell, t.total_commission_when_buy+t.total_commission_when_sell
from robot7_account t;

select stda.code_ from stock_transaction_data_all stda 
where stda.date_ = to_date('20200901', 'yyyy-mm-dd') and stda.change_range >= 10 
and stda.code_ not like '300%';

select 
(select count(*) from robot7_stock_transact_record t where t.filter_type=1 and t.profit_and_loss_rate>0)/
(select count(*) from robot7_stock_transact_record t where t.filter_type=1)*100,
(select count(*) from robot7_stock_transact_record t where t.filter_type=3 and t.profit_and_loss_rate>0)/
(select count(*) from robot7_stock_transact_record t where t.filter_type=3)*100,
(select count(*) from robot7_stock_transact_record t where t.filter_type=5 and t.profit_and_loss_rate>0)/
(select count(*) from robot7_stock_transact_record t where t.filter_type=5)*100,
(select count(*) from robot7_stock_transact_record t where t.filter_type=7 and t.profit_and_loss_rate>0)/
(select count(*) from robot7_stock_transact_record t where t.filter_type=7)*100
from dual
union
select 
(select count(*) from robot7_stock_transact_record t where t.filter_type=1),
(select count(*) from robot7_stock_transact_record t where t.filter_type=3),
(select count(*) from robot7_stock_transact_record t where t.filter_type=5),
(select count(*) from robot7_stock_transact_record t where t.filter_type=7)
from dual;

select (
select count(*) from stock_transaction_data_all t 
where t.date_ between to_date('2024-11-25','yyyy-mm-dd') and to_date('2024-11-29','yyyy-mm-dd')
and t.change_range>0)/(
select count(*) from stock_transaction_data_all t 
where t.date_ between to_date('2024-11-25','yyyy-mm-dd') and to_date('2024-11-29','yyyy-mm-dd'))*100
from dual;

select * from (
select distinct t.date_ from stock_transaction_data_all t 
where t.date_<=to_date('2011-1-14', 'yyyy-mm-dd') order by t.date_ desc)
where rownum<=20;

select (
select count(*) from stock_transaction_data_all t 
where t.date_ = to_date('2011-1-14', 'yyyy-mm-dd') 
and t.change_range>0)/(select count(*) from stock_transaction_data_all t 
where t.date_ = to_date('2011-1-14', 'yyyy-mm-dd'))*100 from dual;

select (select count(*) from stock_transaction_data_all t where t.date_ = to_date('2010-12-08T00:00:00+08:00','yyyy-mm-dd') 
and t.change_range>0)/(select count(*) from stock_transaction_data_all t 
where t.date_ = to_date('2010-12-08T00:00:00+08:00', 'yyyy-mm-dd'))*100 from dual
          
/*update robot7_account set hold_stock_number=0,stock_assets=0,capital_assets=280000,total_assets=280000,
total_stamp_duty=null,total_registrate_fee_when_buy=null,total_commission_when_buy=null,
total_registrate_fee_when_sell=null,total_commission_when_sell=null;
truncate table robot7_stock_filter;
truncate table robot7_stock_transact_record;
truncate table robot7_account_log;
commit;*/
    
------------------------------------------------------------------------------------

select
(select count(*) from mdl_kd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_kd_gold_cross t)*100
from dual;

select
(select sum(t.profit_loss) from mdl_kd_gold_cross t)/
(select count(*) from mdl_kd_gold_cross t)
from dual;

select max(t.buy_date), max(t.sell_date) from mdl_close_price_ma5_gold_cross t;

select * from mdl_close_price_ma5_gold_cross t where t.stock_code='000004' order by t.sell_date desc;
select * from stock_transaction_data t where t.code_='000004' order by t.date_ desc;

select * from mdl_hei_kin_ashi_up_down t where t.stock_code='000004' order by t.sell_date desc;
select * from stock_transaction_data t where t.code_='000004' order by t.date_ desc;

-- truncate table mdl_close_price_ma5_gold_cross;

select
(select count(*) from mdl_close_price_ma5_gold_cross t2 where t2.profit_loss>0)/
(select count(*) from mdl_close_price_ma5_gold_cross t1) * 100
from dual;

select
(select sum(t2.profit_loss) from mdl_close_price_ma5_gold_cross t2)/
(select count(*) from mdl_close_price_ma5_gold_cross t1)
from dual;

select
(select sum(t2.profit_loss) from mdl_close_price_ma5_gold_cross t2)/
(select count(*) from mdl_close_price_ma5_gold_cross t1)
from dual;

select
(select sum(t2.profit_loss) from mdl_macd_gold_cross t2)/
(select count(*) from mdl_macd_gold_cross t1)
from dual;

select count(*) from mdl_close_price_ma5_gold_cross;
select count(*) from mdl_macd_gold_cross;

select t.stock_code, max(t.accumulative_profit_loss) from mdl_close_price_ma5_gold_cross t group by t.stock_code
order by max(t.accumulative_profit_loss) asc;
select t.stock_code, max(t.accumulative_profit_loss) from mdl_macd_gold_cross t group by t.stock_code
order by max(t.accumulative_profit_loss) asc;

select t.*, (t.sell_price - t.buy_price)/t.buy_price
from robot_stock_transaction_record t where t.sell_price is not null
order by (t.sell_price - t.buy_price)/t.buy_price desc;

select
(select count(*) from mdl_macd_gold_cross t
join mdl_stock_analysis t1 on (t1.code_=t.stock_code and t1.date_=t.buy_date
and (t1.ma_trend=1))
where t.profit_loss>0)/
(select count(*) from mdl_macd_gold_cross t
join mdl_stock_analysis t1 on (t1.code_=t.stock_code and t1.date_=t.buy_date
and (t1.ma_trend=1)))
from dual;

------------------------------------------ mdl_stock_analysis ------------------------------------------

select count(*) from mdl_stock_analysis t where t.date_=to_date('2022-03-11','yyyy-mm-dd');

/*delete from mdl_stock_analysis t where t.date_=to_date('2022-03-11','yyyy-mm-dd');
commit;*/

------------------------------------------ temp_* ------------------------------------------

select t.* from TEMP_ROBOT_ACCOUNT t;

select t.*, t.total_assets/7000 from TEMP_ROBOT_ACCOUNT t;

select (sum(t.total_assets/7000)-400)/4 from TEMP_ROBOT_ACCOUNT t;

select sum(t.total_assets/7000-100)/4 from TEMP_ROBOT_ACCOUNT t;

select
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='蓝精灵') 蓝精灵,
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='格格巫') 格格巫,
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='黑猫警长') 黑猫警长,
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='一只耳') 一只耳
from dual;

select
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='蓝精灵' and t.profit_and_loss_rate>0)/
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='蓝精灵')*100 蓝精灵,
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='格格巫' and t.profit_and_loss_rate>0)/
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='格格巫')*100 格格巫,
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='黑猫警长' and t.profit_and_loss_rate>0)/
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='黑猫警长')*100 黑猫警长,
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='一只耳' and t.profit_and_loss_rate>0)/
(select count(*) from TEMP_TRANSACTION_RECORD t where t.robot_name='一只耳')*100 一只耳
from dual;

select t.robot_name,
(t.total_assets-550000)/(select count(*) from temp_transaction_record t1 where t1.robot_name='蓝精灵')
from temp_robot_account t where t.robot_name='蓝精灵'
union
select t.robot_name,
(t.total_assets-550000)/(select count(*) from temp_transaction_record t1 where t1.robot_name='格格巫')
from temp_robot_account t where t.robot_name='格格巫'
union
select t.robot_name,
(t.total_assets-550000)/(select count(*) from temp_transaction_record t1 where t1.robot_name='黑猫警长')
from temp_robot_account t where t.robot_name='黑猫警长'
union
select t.robot_name,
(t.total_assets-550000)/(select count(*) from temp_transaction_record t1 where t1.robot_name='一只耳')
from temp_robot_account t where t.robot_name='一只耳';

select count(*) from temp_transaction_record;

select count(*) from temp_robot_account_log;

select count(*) from temp_robot_stock_filter;

/*create table temp_robot_account as select * from robot_account;
create table temp_robot_stock_filter as select * from robot_stock_filter;
create table temp_transaction_record as select * from robot_stock_transaction_record;
create table temp_robot_account_log as select * from robot_account_log;*/

/*truncate table temp_robot_account;
truncate table temp_robot_account_log;
truncate table temp_robot_stock_filter;
truncate table temp_transaction_record;*/

----------------------------------------- real_transaction_condition -------------------------------------------

select count(*) from real_transaction_condition;

select * from real_transaction_condition;

-- truncate table real_transaction_condition;

select count(*) from real_transaction_condition t
where t.is_xr=0 and t.is_less_than_percentage=1 and t.is_less_than_close_price=1
      and t.is_ma120_not_decreasing=1 and t.is_ma250_not_decreasing=1;

select count(*) from real_transaction_condition t
where t.is_xr=0 and t.is_less_than_percentage=1 and t.is_less_than_close_price=1
      and t.is_ma120_not_decreasing=1 and t.is_ma250_not_decreasing=1 and t.macd_gold_cross_close_price is not null;

select count(*) from real_transaction_condition t
where t.is_xr=0 and t.is_less_than_percentage=1 and t.is_less_than_close_price=1
      and t.is_ma120_not_decreasing=1 and t.is_ma250_not_decreasing=1 and t.macd_dead_cross_close_price is not null;

/*create table temp_real_account as select * from real_account;
create table temprealstocktransactionrecord as select * from real_stock_transaction_record;
create table temprealtransactioncondition as select * from real_transaction_condition;*/

----------------------------------------- MDL_TOP_STOCK_DETAIL -------------------------------------------

select distinct(t.date_), count(*)
from MDL_TOP_STOCK_DETAIL t
group by t.date_ order by t.date_ asc;

select count(*) from MDL_TOP_STOCK_DETAIL t;

select min(t.date_), max(t.date_) from MDL_TOP_STOCK_DETAIL t;

select * from MDL_TOP_STOCK_DETAIL t where t.code_='000004' order by t.date_ desc;

select count(*) from MDL_TOP_STOCK_DETAIL t where t.date_=to_date('2022-03-11', 'yyyy-mm-dd');

/*delete from MDL_TOP_STOCK_DETAIL t where t.date_=to_date('2007-04-24', 'yyyy-mm-dd');
commit;*/

-- truncate table mdl_top_stock_detail;

/*update mdl_top_stock_detail t
set t.percentage_21_ma5=null, t.percentage_21_ma10=null, t.percentage_21_ma20=null,
t.percentage_34_ma5=null, t.percentage_34_ma10=null, t.percentage_34_ma20=null,
t.percentage_55_ma5=null, t.percentage_55_ma10=null, t.percentage_55_ma20=null,
t.percentage_89_ma5=null, t.percentage_89_ma10=null, t.percentage_89_ma20=null,
t.percentage_144_ma5=null, t.percentage_144_ma10=null, t.percentage_144_ma20=null,
t.percentage_233_ma5=null, t.percentage_233_ma10=null, t.percentage_233_ma20=null,
t.percentage_377_ma5=null, t.percentage_377_ma10=null, t.percentage_377_ma20=null,
t.percentage_610_ma5=null, t.percentage_610_ma10=null, t.percentage_610_ma20=null;
commit;*/

------------------------------- MDL_PERCENTAGE_MA_GOLD_CROSS ----------------------------------

select * from MDL_PERCENTAGE_MA_GOLD_CROSS;

select count(*) from MDL_PERCENTAGE_MA_GOLD_CROSS;

/*truncate table MDL_PERCENTAGE_MA_GOLD_CROSS;*/

select
(select count(*) from MDL_PERCENTAGE_MA_GOLD_CROSS t where t.profit_loss>0)/
(select count(*) from MDL_PERCENTAGE_MA_GOLD_CROSS t) * 100 percentage
from dual;

select t.type_, (
select
(select count(*) from MDL_PERCENTAGE_MA_GOLD_CROSS t1
join stock_week t3 on t3.code_=t1.stock_code and t1.buy_date between t3.begin_date and t3.end_date and t3.k>t3.d
where t1.type_=t.type_ and t1.profit_loss>0)/
(select count(*) from MDL_PERCENTAGE_MA_GOLD_CROSS t2
join stock_week t4 on t4.code_=t2.stock_code and t2.buy_date between t4.begin_date and t4.end_date and t4.k>t4.d
where t2.type_=t.type_) * 100
from dual) percentage,
(select count(*) from MDL_PERCENTAGE_MA_GOLD_CROSS t2 where t2.type_=t.type_)
from MDL_PERCENTAGE_MA_GOLD_CROSS t
group by t.type_ order by t.type_;

---------------------------------------- MDL_G_C_D_C_ANALYSIS --------------------------------------------

select * from mdl_g_c_d_c_analysis t order by t.date_ desc;

/*insert into mdl_g_c_d_c_analysis(DATE_, MACD_G_C_PERCENT, MACD_D_C_PERCENT, CLOSE_PRICE_G_C_MA5_PERCENT, CLOSE_PRICE_D_C_MA5_PERCENT, HEI_KIN_ASHI_U_D_PERCENT, HEI_KIN_ASHI_D_U_PERCENT, KD_G_C_PERCENT, KD_D_C_PERCENT)
select * from(
select t.date_ DATE_,
(select count(*) from stock_transaction_data_all t1 where t1.dif>t1.dea and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 MACD_G_C_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.dif<t1.dea and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 MACD_D_C_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.close_price>t1.ma5 and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 CLOSE_PRICE_G_C_MA5_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.close_price<t1.ma5 and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 CLOSE_PRICE_D_C_MA5_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.ha_close_price>t1.ha_open_price and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 HEI_KIN_ASHI_U_D_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.ha_close_price<t1.ha_open_price and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 HEI_KIN_ASHI_D_U_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.k>t1.d and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 KD_G_C_PERCENT,
(select count(*) from stock_transaction_data_all t1 where t1.k<t1.d and t1.date_=t.date_)/
(select count(*) from stock_transaction_data_all t1 where t1.date_=t.date_)*100 KD_D_C_PERCENT
from stock_transaction_data_all t
group by t.date_
order by t.date_ asc);*/

-- truncate table mdl_g_c_d_c_analysis;

/*update mdl_g_c_d_c_analysis t
           set t.macd_g_c_success_rate_ma5     = null,
               t.macd_d_c_success_rate_ma5     = null,
               t.c_p_g_c_ma5_success_rate_ma5  = null,
               t.c_p_d_c_ma5_success_rate_ma5  = null,
               t.hei_kin_ashi_u_d_s_r_ma5      = null,
               t.hei_kin_ashi_d_u_s_r_ma5      = null,
               t.kd_g_c_success_rate_ma5       = null,
               t.kd_d_c_success_rate_ma5       = null,
               t.macd_g_c_success_rate_ma10    = null,
               t.macd_d_c_success_rate_ma10    = null,
               t.c_p_g_c_ma5_success_rate_ma10 = null,
               t.c_p_d_c_ma5_success_rate_ma10 = null,
               t.hei_kin_ashi_u_d_s_r_ma10     = null,
               t.hei_kin_ashi_d_u_s_r_ma10     = null,
               t.kd_g_c_success_rate_ma10      = null,
               t.kd_d_c_success_rate_ma10      = null,
               t.macd_g_c_success_rate_ma20    = null,
               t.macd_d_c_success_rate_ma20    = null,
               t.c_p_g_c_ma5_success_rate_ma20 = null,
               t.c_p_d_c_ma5_success_rate_ma20 = null,
               t.hei_kin_ashi_u_d_s_r_ma20     = null,
               t.hei_kin_ashi_d_u_s_r_ma20     = null,
               t.kd_g_c_success_rate_ma20      = null,
               t.kd_d_c_success_rate_ma20      = null;*/

---------------------------------------- mdl_boll_* --------------------------------------------

select count(distinct t.stock_code) from mdl_boll_macd_gold_cross t;

select * from mdl_week_boll_kd_dead_cross t where t.stock_code='000028' order by t.buy_date desc;

select
(select count(*) from mdl_week_boll_macd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_macd_gold_cross t)*100
from dual;
select
(select count(*) from mdl_week_boll_c_p_ma5_g_c t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_c_p_ma5_g_c t)*100
from dual;
select
(select count(*) from mdl_week_boll_h_k_a_up_down t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_h_k_a_up_down t)*100
from dual;
select
(select count(*) from mdl_week_boll_kd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_kd_gold_cross t)*100
from dual;

select
(select count(*) from mdl_week_boll_macd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_macd_dead_cross t)*100
from dual;
select
(select count(*) from mdl_week_boll_c_p_ma5_d_c t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_c_p_ma5_d_c t)*100
from dual;
select
(select count(*) from mdl_week_boll_h_k_a_down_up t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_h_k_a_down_up t)*100
from dual;
select
(select count(*) from mdl_week_boll_kd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_week_boll_kd_dead_cross t)*100
from dual;

select count(*) from mdl_week_boll_macd_gold_cross;
select count(*) from mdl_week_boll_c_p_ma5_gold_cross;
select count(*) from mdl_week_boll_hei_kin_ashi_up_down;
select count(*) from mdl_week_boll_kd_gold_cross;
select count(*) from mdl_week_boll_macd_dead_cross;
select count(*) from md_weekl_boll_c_p_ma5_dead_cross;
select count(*) from mdl_week_boll_hei_kin_ashi_down_up;
select count(*) from mdl_week_boll_kd_dead_cross;

select
(select sum(t.accumulative_profit_loss) from mdl_boll_macd_gold_cross t)/
(select count(*) from mdl_boll_macd_gold_cross t)
from dual;
select
(select sum(t.accumulative_profit_loss) from mdl_boll_c_p_ma5_gold_cross t)/
(select count(*) from mdl_boll_c_p_ma5_gold_cross t)
from dual;
select
(select sum(t.accumulative_profit_loss) from mdl_boll_hei_kin_ashi_up_down t)/
(select count(*) from mdl_boll_hei_kin_ashi_up_down t)
from dual;
select
(select sum(t.accumulative_profit_loss) from mdl_boll_kd_gold_cross t)/
(select count(*) from mdl_boll_kd_gold_cross t)
from dual;

select
(select sum(t.accumulative_profit_loss) from mdl_boll_macd_dead_cross t)/
(select count(*) from mdl_boll_macd_dead_cross t)
from dual;
select
(select sum(t.accumulative_profit_loss) from mdl_boll_c_p_ma5_dead_cross t)/
(select count(*) from mdl_boll_c_p_ma5_dead_cross t)
from dual;
select
(select sum(t.accumulative_profit_loss) from mdl_boll_hei_kin_ashi_down_up t)/
(select count(*) from mdl_boll_hei_kin_ashi_down_up t)
from dual;
select
(select sum(t.accumulative_profit_loss) from mdl_boll_kd_dead_cross t)/
(select count(*) from mdl_boll_kd_dead_cross t)
from dual;

/*truncate table mdl_week_boll_macd_gold_cross;
truncate table mdl_week_boll_c_p_ma5_g_c;
truncate table mdl_week_boll_h_k_a_up_down;
truncate table mdl_week_boll_kd_gold_cross;
truncate table mdl_week_boll_macd_dead_cross;
truncate table mdl_week_boll_c_p_ma5_d_c;
truncate table mdl_week_boll_h_k_a_down_up;
truncate table mdl_week_boll_kd_dead_cross;*/

select t1.close_price_ma_order, count(*)/(select count(*) from mdl_week_boll_macd_gold_cross)*100
from mdl_week_boll_macd_gold_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.buy_date=t1.date_ and t.profit_loss>0
group by t1.close_price_ma_order
order by count(*)/(select count(*) from mdl_week_boll_macd_gold_cross) desc;

select t1.close_price_ma_order, count(*)/(select count(*) from mdl_week_boll_macd_dead_cross)*100
from mdl_week_boll_macd_dead_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.sell_date=t1.date_ and t.profit_loss>0
group by t1.close_price_ma_order
order by count(*)/(select count(*) from mdl_week_boll_macd_dead_cross) desc;

select
(select count(*) from mdl_week_boll_macd_gold_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.buy_date=t1.date_ and t.profit_loss>0
where t1.close_price_ma_order in(6543012, 6543021, 6540312))/
(select count(*) from mdl_week_boll_macd_gold_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.buy_date=t1.date_
where t1.close_price_ma_order in(6543012, 6543021, 6540312))*100
from dual;

select
(select count(*) from mdl_week_boll_macd_dead_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.sell_date=t1.date_ and t.profit_loss>0
where t1.close_price_ma_order in(1203456, 2103456, 2130456))/
(select count(*) from mdl_week_boll_macd_dead_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.sell_date=t1.date_
where t1.close_price_ma_order in(1203456, 2103456, 2130456))*100
from dual;

select t1.close_price_ma_order, count(*)/(select count(*) from mdl_close_price_ma5_gold_cross)*100
from mdl_close_price_ma5_gold_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.buy_date=t1.date_ and t.profit_loss>0
group by t1.close_price_ma_order
order by count(*)/(select count(*) from mdl_close_price_ma5_gold_cross) desc;

select t1.close_price_ma_order, count(*)/(select count(*) from mdl_close_price_ma5_dead_cross)*100
from mdl_close_price_ma5_dead_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.sell_date=t1.date_ and t.profit_loss>0
group by t1.close_price_ma_order
order by count(*)/(select count(*) from mdl_close_price_ma5_dead_cross) desc;

select
(select count(*) from mdl_close_price_ma5_gold_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.buy_date=t1.date_ and t.profit_loss>0
where t1.close_price_ma_order in(6543210, 1023456, 6543120))/
(select count(*) from mdl_close_price_ma5_gold_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.buy_date=t1.date_
where t1.close_price_ma_order in(6543210, 1023456, 6543120))*100
from dual;

select
(select count(*) from mdl_close_price_ma5_dead_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.sell_date=t1.date_ and t.profit_loss>0
where t1.close_price_ma_order in(1203456, 2103456, 2130456))/
(select count(*) from mdl_close_price_ma5_dead_cross t
join mdl_stock_analysis t1 on t.stock_code=t1.code_ and t.sell_date=t1.date_
where t1.close_price_ma_order in(1203456, 2103456, 2130456))*100
from dual;

---------------------------------------- test --------------------------------------------

select
(select count(*) from MDL_MACD_GOLD_CROSS t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_MACD_GOLD_CROSS t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_MACD_DEAD_CROSS t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_MACD_DEAD_CROSS t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_CLOSE_PRICE_MA5_GOLD_CROSS t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_CLOSE_PRICE_MA5_GOLD_CROSS t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_CLOSE_PRICE_MA5_DEAD_CROSS t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_CLOSE_PRICE_MA5_DEAD_CROSS t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_HEI_KIN_ASHI_UP_DOWN t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_HEI_KIN_ASHI_UP_DOWN t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_HEI_KIN_ASHI_DOWN_UP t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_HEI_KIN_ASHI_DOWN_UP t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_KD_GOLD_CROSS t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_KD_GOLD_CROSS t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select
(select count(*) from MDL_KD_DEAD_CROSS t
where t.stock_code='000007' and t.profit_loss>0
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd'))/
(select count(*) from MDL_KD_DEAD_CROSS t
where t.stock_code='000007'
      and t.buy_date between to_date('2022-01-01','yyyy-mm-dd') and to_date('2022-06-23','yyyy-mm-dd')) * 100
from dual;

select * from MDL_STOCK_ANALYSIS t
where t.code_='600615' and t.close_price_ma_order='6543210' order by t.date_ asc;

select * from MDL_STOCK_ANALYSIS t where t.close_price_ma_order='6543210'
order by t.code_, t.date_ asc;

select count(distinct t.stock_code) from mdl_ma_pattern_oversold t;

select distinct t.stock_code from mdl_ma_pattern_oversold t;

select count(*) from mdl_ma_pattern_oversold t
where t.buy_sell_pattern='close_price_gold_cross_dead_cross_ma5_6543210' and t.stock_code='000700' order by t.sell_date desc;

select t.buy_sell_pattern,
(select count(*) from mdl_ma_pattern_oversold t1 where t1.buy_sell_pattern=t.buy_sell_pattern and t1.profit_loss>0)/
(select count(*) from mdl_ma_pattern_oversold t1 where t1.buy_sell_pattern=t.buy_sell_pattern) * 100,
(select sum(t1.profit_loss)/count(*) from mdl_ma_pattern_oversold t1 where t1.buy_sell_pattern=t.buy_sell_pattern)
from mdl_ma_pattern_oversold t
group by t.buy_sell_pattern;

select
(select count(*) from mdl_ma_pattern_oversold t1 where t1.profit_loss>0)/
(select count(*) from mdl_ma_pattern_oversold t1) * 100
from dual;

select * from mdl_ma_pattern_oversold t where t.direction=-1;

select t.buy_sell_pattern, count(*) from mdl_ma_pattern_oversold t group by t.buy_sell_pattern;

-- truncate table mdl_ma_pattern_oversold;

-- delete from mdl_ma_pattern_oversold t1 where t1.buy_sell_pattern='close_price_gold_cross_dead_cross_ma5_6543210';

/*update mdl_ma_pattern_oversold set buy_sell_pattern='close_price_gold_cross_dead_cross_ma5_543210';
update mdl_ma_pattern_oversold set direction=1;*/

select to_char(t.begin_date,'yyyy'), to_char(t.begin_date,'WW'), avg(t.close_price), avg(t.up), avg(t.mb), avg(t.dn_)
from stock_week t
--where t.begin_date>=to_date('2022-09-05','yyyy-mm-dd') and t.end_date<=to_date('2022-09-09','yyyy-mm-dd')
group by to_char(t.begin_date,'yyyy'), to_char(t.begin_date,'WW')
order by to_char(t.begin_date,'yyyy') desc, to_char(t.begin_date,'WW') desc;


select * from stock_week t
where t.begin_date>=to_date('2022-09-05','yyyy-mm-dd') and t.end_date<=to_date('2022-09-09','yyyy-mm-dd')
and t.close_price > t.up;

select * from stock_week t
where t.begin_date>=to_date('2022-09-05','yyyy-mm-dd') and t.end_date<=to_date('2022-09-09','yyyy-mm-dd')
and t.close_price < t.dn_;

select to_char(t.begin_date,'yyyy') as year, to_char(t.begin_date,'WW') as weekNumber,
avg(t.close_price) as averageClosePrice, avg(t.up) as averageUP, avg(t.mb) as averageMB,
avg(t.dn_) as averageDN, count(*)
from stock_week t
where t.begin_date>=to_date('20220905','yyyy-mm-dd') and t.end_date<=to_date('20220909','yyyy-mm-dd')
group by to_char(t.begin_date,'yyyy'), to_char(t.begin_date,'WW')
order by to_char(t.begin_date,'yyyy') desc, to_char(t.begin_date,'WW') desc

select to_date('20220917','yyyy-mm-dd')-to_date('20220914','yyyy-mm-dd') from dual;

select siw.begin_date, siw.end_date, avg(sw.close_price),
(select count(*) from stock_week sw1 where sw1.begin_date>=siw.begin_date and sw1.end_date<=siw.end_date and sw1.close_price>sw1.up)/
(select count(*) from stock_week sw1 where sw1.begin_date>=siw.begin_date and sw1.end_date<=siw.end_date)*100,
(select count(*) from stock_week sw1 where sw1.begin_date>=siw.begin_date and sw1.end_date<=siw.end_date and sw1.close_price<sw1.dn_)/
(select count(*) from stock_week sw1 where sw1.begin_date>=siw.begin_date and sw1.end_date<=siw.end_date)*100
from stock_week sw
join stock_index_week siw on siw.begin_date>=sw.begin_date and siw.end_date<=sw.end_date
where siw.begin_date>=to_date('20220905','yyyy-mm-dd') and siw.end_date<=to_date('20220909','yyyy-mm-dd')
group by siw.begin_date, siw.end_date
order by siw.begin_date desc;

select
(select count(*)
from stock_week sw
where sw.begin_date>=to_date('20220104','yyyy-mm-dd') and sw.end_date<=to_date('20220107','yyyy-mm-dd')
and sw.close_price>sw.up),
(select count(*)
from stock_week sw
where sw.begin_date>=to_date('20220104','yyyy-mm-dd') and sw.end_date<=to_date('20220107','yyyy-mm-dd')
and sw.close_price<sw.dn_)
from dual;

select * from stock_transaction_data_all t
where t.code_='600592' and t.date_<=to_date('2021-01-05','yyyy-mm-dd') order by t.date_ desc;

select * from REAL_TRANSACTION_CONDITION t where t.stock_code='600592';

select * from robot_stock_filter t where t.stock_code='600592';

select
(select count(*) from his_robot_transaction_record t where t.robot_name='威震天' and t.model_id=2) 威震天,
(select count(*) from his_robot_transaction_record t where t.robot_name='擎天柱' and t.model_id=2) 擎天柱,
(select count(*) from his_robot_transaction_record t where t.robot_name='漩涡鸣人' and t.model_id=2) 漩涡鸣人,
(select count(*) from his_robot_transaction_record t where t.robot_name='宇智波佐助' and t.model_id=2) 宇智波佐助
from dual;

select
(select count(*) from his_robot_transaction_record t where t.robot_name='威震天' and t.model_id=2 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='威震天' and t.model_id=2)*100 威震天,
(select count(*) from his_robot_transaction_record t where t.robot_name='擎天柱' and t.model_id=2 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='擎天柱' and t.model_id=2)*100 擎天柱,
(select count(*) from his_robot_transaction_record t where t.robot_name='漩涡鸣人' and t.model_id=2 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='漩涡鸣人' and t.model_id=2)*100 漩涡鸣人,
(select count(*) from his_robot_transaction_record t where t.robot_name='宇智波佐助' and t.model_id=2 and t.profit_and_loss_rate>0)/
(select count(*) from his_robot_transaction_record t where t.robot_name='宇智波佐助' and t.model_id=2)*100 宇智波佐助
from dual;

select * from(
select * from real_transaction_condition t
where t.is_xr=0
and t.is_ma_1_2_0_not_decreasing=1 and t.is_ma_2_5_0_not_decreasing=1
and t.stock_code not in(select t1.stock_code from real_stock_transaction_record t1
where t1.direction=1 and t1.sell_date is null) and t.stock_code='600592'
union
select * from real_transaction_condition t
where t.is_xr=0
and t.is_ma_1_2_0_not_increasing=1 and t.is_ma_2_5_0_not_increasing=1
and t.stock_code not in(select t1.stock_code from real_stock_transaction_record t1
where t1.direction=-1 and t1.buy_date is null) and t.stock_code='600592';

select
  (select count(*)
  from stock_week sw
  where sw.begin_date>=to_date('20210101','yyyy-mm-dd') and sw.end_date<=to_date('20211231','yyyy-mm-dd')
  and (sw.close_price>sw.up or (sw.up-sw.close_price)/sw.close_price<=5/100)) upBollCount,
  (select count(*)
  from stock_week sw
  where sw.begin_date>=to_date('20210101','yyyy-mm-dd') and sw.end_date<=to_date('20211231','yyyy-mm-dd')
  and (sw.close_price<sw.dn_ or (sw.close_price-sw.dn_)/sw.close_price<=5/100)) dnBollCount
from dual;

select * from( 
select distinct siw.begin_date, siw.end_date 
from stock_index_week siw where siw.end_date<to_date('20230109','yyyy-mm-dd') order by siw.begin_date desc) 
where rownum<=2;

select avg(t.k), avg(t.d), t.begin_date from stock_week t 
where t.begin_date>=to_date('20230103','yyyy-mm-dd') and t.end_date<=to_date('20230106','yyyy-mm-dd') 
order by t.begin_date asc;

select t.stock_code stockCode, si_.url_param urlParam, t.transaction_date transactionDate, stda.open_price openPriceFromDB, 
stda.close_price closePriceFromDB, stda.highest_price highestPriceFromDB, stda.lowest_price lowestPriceFromDB, 
(select t2.close_price from (
select t1.close_price from stock_transaction_data_all t1 where t1.code_=t.stock_code and t1.date_<t.transaction_date order by t1.date_ desc ) t2 
where rownum<=1) lastClosePrice, 
(select t3.ma5 from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) previousClosePrice,
(select t3.close_price from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekClosePrice,
(select t3.ma5 from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekMa5,
(select t3.begin_date from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekBeginDate,
(select t3.end_date from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekEndDate,
(select t8.ha_open_price from (
select t7.* from (
select t6.* from (
select t5.* from stock_transaction_data_all t5 where t5.code_=t.stock_code and t5.date_<=t.transaction_date order by t5.date_ desc) t6
where rownum<=2) t7 order by t7.date_) t8
where rownum<=1) lastHaOpenPrice,
(select t8.ha_close_price from (
select t7.* from (
select t6.* from (
select t5.* from stock_transaction_data_all t5 where t5.code_=t.stock_code and t5.date_<=t.transaction_date order by t5.date_ desc) t6
where rownum<=2) t7 order by t7.date_) t8
where rownum<=1) lastHaClosePrice,
(select max(t10.highest_price)
from (select * from stock_transaction_data_all t9 where t9.code_=t.stock_code and t9.date_<t.transaction_date
order by t9.date_ desc) t10
where rownum<=8) highestPriceWith8Day,
(select min(t10.lowest_price) 
from (select * from stock_transaction_data_all t9 where t9.code_=t.stock_code and t9.date_<t.transaction_date
order by t9.date_ desc) t10
where rownum<=8) lowestPriceWith8Day
from real4_transaction_condition t 
join stock_info si_ on si_.code_=t.stock_code
join stock_transaction_data_all stda on stda.code_=t.stock_code and stda.date_=t.transaction_date
where t.stock_code not in(select t1.stock_code from real4_stock_transaction_record t1 
where t1.direction=1 and t1.sell_date is null) 
union 
select t.stock_code stockCode, si_.url_param urlParam, t.transaction_date transactionDate, stda.open_price openPriceFromDB, 
stda.close_price closePriceFromDB, stda.highest_price highestPriceFromDB, stda.lowest_price lowestPriceFromDB, 
(select t2.close_price from (
select t1.close_price from stock_transaction_data_all t1 where t1.code_=t.stock_code and t1.date_<t.transaction_date order by t1.date_ desc ) t2 
where rownum<=1) lastClosePrice, 
(select t3.ma5 from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) previousClosePrice,
(select t3.close_price from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekClosePrice,
(select t3.ma5 from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekMa5,
(select t3.begin_date from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekBeginDate,
(select t3.end_date from stock_week t3 where t3.code_=t.stock_code and t.transaction_date between t3.begin_date and t3.end_date) weekEndDate,
(select t8.ha_open_price from (
select t7.* from (
select t6.* from (
select t5.* from stock_transaction_data_all t5 where t5.code_=t.stock_code and t5.date_<=t.transaction_date order by t5.date_ desc) t6
where rownum<=2) t7 order by t7.date_) t8
where rownum<=1) lastHaOpenPrice,
(select t8.ha_close_price from (
select t7.* from (
select t6.* from (
select t5.* from stock_transaction_data_all t5 where t5.code_=t.stock_code and t5.date_<=t.transaction_date order by t5.date_ desc) t6
where rownum<=2) t7 order by t7.date_) t8
where rownum<=1) lastHaClosePrice,
(select max(t10.highest_price)
from (select * from stock_transaction_data_all t9 where t9.code_=t.stock_code and t9.date_<t.transaction_date
order by t9.date_ desc) t10
where rownum<=8) highestPriceWith8Day,
(select min(t10.lowest_price) 
from (select * from stock_transaction_data_all t9 where t9.code_=t.stock_code and t9.date_<t.transaction_date
order by t9.date_ desc) t10
where rownum<=8) lowestPriceWith8Day
from real4_transaction_condition t 
join stock_info si_ on si_.code_=t.stock_code
join stock_transaction_data_all stda on stda.code_=t.stock_code and stda.date_=t.transaction_date
where t.stock_code not in(select t1.stock_code from real4_stock_transaction_record t1 
where t1.direction=-1 and t1.buy_date is null);

/*delete from stock_transaction_data t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from stock_index t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from etf_transaction_data t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from stock_transaction_data_all t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from board_index t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_macd_gold_cross t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_macd_dead_cross t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_close_price_ma5_gold_cross t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_close_price_ma5_dead_cross t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_hei_kin_ashi_down_up t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_hei_kin_ashi_up_down t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_kd_gold_cross t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_kd_dead_cross t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_bull_short_line_up t where t.buy_date>=to_date('2024-5-29','yyyy-mm-dd') or t.sell_date>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_stock_analysis t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_top_stock_detail t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
delete from mdl_g_c_d_c_analysis t where t.date_>=to_date('2024-5-29','yyyy-mm-dd');
commit;*/

/*update etf_transaction_data t set t.correlation5_with000001=null,t.correlation5_with399001=null,
t.correlation5_with399005=null,t.correlation5_with399006=null,t.correlation5_with_avg_c_p=null;
commit;*/

select * from stock_transaction_data t where t.date_=to_date('2024-6-11','yyyy-mm-dd');
select * from stock_transaction_data_all t where t.date_=to_date('2024-6-11','yyyy-mm-dd');
select * from stock_index t where t.date_=to_date('2024-6-11','yyyy-mm-dd');
select * from etf_transaction_data t where t.date_=to_date('2024-6-11','yyyy-mm-dd');
select * from board_index t where t.date_=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_macd_gold_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_macd_dead_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_close_price_ma5_gold_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_close_price_ma5_dead_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_hei_kin_ashi_down_up t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_hei_kin_ashi_up_down t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_kd_gold_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_kd_dead_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
--select * from mdl_bull_short_line_up t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_etf_macd_gold_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_etf_close_price_ma5_g_c t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_etf_hei_kin_ashi_down_up t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_etf_kd_gold_cross t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
--select * from mdl_etf_bull_short_line_up t where t.buy_date=to_date('2024-6-7','yyyy-mm-dd') or t.sell_date=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_stock_analysis t where t.date_=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_top_stock_detail t where t.date_=to_date('2024-6-7','yyyy-mm-dd');
select * from mdl_g_c_d_c_analysis t where t.date_=to_date('2024-6-7','yyyy-mm-dd');
select * from stock_week t where t.begin_date>=to_date('2024-6-3','yyyy-mm-dd');
select * from stock_index_week t where t.begin_date>=to_date('2024-6-3','yyyy-mm-dd');
select * from mdl_stock_week_analysis t where t.begin_date>=to_date('2024-6-3','yyyy-mm-dd');

/*delete from mdl_bull_short_line_up t where t.buy_date>=to_date('2024-4-16','yyyy-mm-dd') or t.sell_date>=to_date('2024-4-16','yyyy-mm-dd');
delete from mdl_etf_bull_short_line_up t where t.buy_date>=to_date('2024-6-6','yyyy-mm-dd') or t.sell_date>=to_date('2024-6-6','yyyy-mm-dd');*/

select * from (
select * from stock_index_week t where t.code_='000001' and t.end_date<to_date('2024-7-25','yyyy-mm-dd') 
order by t.end_date desc) 
where rownum<=1;

select 
(select avg(t.k), avg(t.d) from stock_week t 
where to_date('2024-7-15','yyyy-mm-dd')>=t.begin_date and to_date('2024-7-19','yyyy-mm-dd')<=t.end_date) lastWeekAverageK,
(select avg(t.k) from stock_week t 
where to_date('2024-7-8','yyyy-mm-dd')>=t.begin_date and to_date('2024-7-11','yyyy-mm-dd')<=t.end_date) lastTwoWeekAverageK
from dual;

select to_char(t.date_, 'yyyy-mm-dd') DATE_, avg(t.close_price) AVERAGE_CLOSE_PRICE, avg(t.ma5) AVERAGE_MA5, 
avg(t.ma10) AVERAGE_MA10, avg(t.ma20) AVERAGE_MA20, avg(t.ma60) AVERAGE_MA60, 
avg(t.ma120) AVERAGE_MA120, avg(t.ma250) AVERAGE_MA250 
from stock_transaction_data_all t 
where t.date_ between to_date('20140729','yyyy-mm-dd') and to_date('20240729','yyyy-mm-dd') 
group by t.date_ 
order by t.date_ asc;
    
SELECT
  t1.date_,
  t1.AVERAGE_CLOSE_PRICE,
  round(AVG(t1.AVERAGE_MA5) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 3) AS AVERAGE_MA5,
  round(AVG(t1.AVERAGE_MA10) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 3) AS AVERAGE_MA10,
  round(AVG(t1.AVERAGE_MA20) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 3) AS AVERAGE_MA20,
  round(AVG(t1.AVERAGE_MA60) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 3) AS AVERAGE_MA60,
  round(AVG(t1.AVERAGE_MA120) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 3) AS AVERAGE_MA120,
  round(AVG(t1.AVERAGE_MA250) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 3) AS AVERAGE_MA250
FROM (
select to_char(t.date_, 'yyyy-mm-dd') DATE_, avg(t.close_price) AVERAGE_CLOSE_PRICE, avg(t.ma5) AVERAGE_MA5, 
avg(t.ma10) AVERAGE_MA10, avg(t.ma20) AVERAGE_MA20, avg(t.ma60) AVERAGE_MA60, 
avg(t.ma120) AVERAGE_MA120, avg(t.ma250) AVERAGE_MA250 
from stock_transaction_data_all t 
where t.date_ between to_date('20140729','yyyy-mm-dd') and to_date('20240729','yyyy-mm-dd') 
group by t.date_ 
order by t.date_ desc
) t1;

delete from robot7_stock_filter where stock_code not in(
select t.code_ from stock_transaction_data_all t where t.date_=to_date('20240729','yyyy-mm-dd') 
and (t.volume > t.volume_ma250 or t.turnover > t.turnover_ma250));

select count(*), sum(num_rows)
from (select t.table_name, t.num_rows
from user_tables t
ORDER BY NUM_ROWS DESC);
