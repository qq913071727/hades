<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>  
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Message Management System</h1>
		<s:a href="index.jsp">home</s:a>
		<s:a href="addMessage.jsp">add message</s:a>
		<s:a href="deleteMessage.jsp">delete message</s:a>
		<s:a href="updateMessage.jsp">update message</s:a>
		<s:a href="searchMessage.jsp">search message</s:a>

		<s:form action="updateMessage.action">
			<s:textfield name="keyWord" label="关键字" size="60" />
			<s:submit name="searchSubmit" value="searchSubmit"  method="executeSearch" />
		</s:form>
		
			<br />
			<s:if test="result==true">updating is successful</s:if>
			<s:elseif test="result==false">updating failed</s:elseif>
			
			<c:if test="${!empty requestScope.messages}">
				<table border="1">
					<c:forEach var="i" begin="0" end="${fn:length(requestScope.messages)-1}">
						<s:form action="updateMessage.action">
						<tr>
							<td>
								<input type="text" name="title" value="${requestScope.messages[i].title}" />
							</td>
							<td>
								<textarea rows="5" cols="45" name="description">${requestScope.messages[i].description}</textarea>
							</td>
							<td>
								<input type="text" name="createTime" value="${requestScope.messages[i].creationTime}" />
							</td>
							<td>
								<s:submit name="updateSubmit" value="updateSubmit" method="executeUpdate" />
								<input type="hidden" name="id" value="${requestScope.messages[i].id}" />
							</td>
						</tr>
						</s:form>
					</c:forEach>
				</table>
			</c:if>
	</center>
</body>
</html>