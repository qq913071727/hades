<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link href="jquery-easyui-1.4/themes/black/easyui.css" rel="stylesheet" />
<script src="jquery-easyui-1.4/jquery.min.js"></script>
<script src="jquery-easyui-1.4/jquery.easyui.min.js"></script>

<title>Insert title here</title>

<script type="text/javascript">
	$(function() {
		$("#mainLayout").layout();
		$("#mainLayout").layout('add', {
			region : 'north',
			title : '北',
			height : 60
		});
		$("#mainLayout").layout('add', {
			region : 'south',
			title : '南',
			height : 100
		});
		$("#mainLayout").layout('add', {
			region : 'east',
			title : '西',
			width : 100
		});
		$("#mainLayout").layout('add', {
			region : 'west',
			title : '东',
			width : 100
		});
		$("#mainLayout").layout('add', {
			region : 'center',
			title : '中'
		});
	})
</script>
</head>
<body>
	<div id="mainLayout" style="width: 500px; height: 500px;"></div>

</body>
</html>
