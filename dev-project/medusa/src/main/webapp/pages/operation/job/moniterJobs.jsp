<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link href="js/themes/default/easyui.css" rel="stylesheet" type="text/css" />
<link href="js/themes/icon.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>

</head>

<body>
	<form id="ff" method="post">
	    <div>
	        <label for="name">name</label>
	        <input class="easyui-validatebox" type="text" name="name" data-options="required:true" />
	    </div>
	    <div>
	        <label for="email">email</label>
	        <input class="easyui-validatebox" type="text" name="email" data-options="validType:'email'" />
	    </div>
	</form>
</body>

</html>