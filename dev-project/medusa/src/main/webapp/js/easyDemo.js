var xmlhttp;

function verify() {
	// 第一步：创建XMLHttpRequest对象
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
		if (xmlhttp.overrideMimeType) {
			xmlhttp.overrideMimeType("text/xml");
		}
	} else if (window.ActiveXObject) {
		var MSXML = [ 'MSXML2.XMLHTTP.6.0', 'MSXML2.XMLHTTP.5.0',
				'MSXML2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP',
				'Microsoft.XMLHTTP' ];
		for ( var i = 0; i < activerxName.length; i++) {
			try {
				xmlhttp = new ActiveXObject(activerxName[i]);
				break;
			} catch (e) {
			}
		}
	}
	
	// 确认XMLHttpRequest对象是否创建成功
	if (!xmlhttp) {
		alert("XMLHttpRequest对象创建失败");
		return;
	}
	
	// 第二步：注册回调方法
	xmlhttp.onreadystatechange = callback;
	// 第三步：设置和服务器端交互的相应参数
	page = encodeURI(encodeURI(page));
	rows = encodeURI(encodeURI(rows));
	alter(page);
	alter(rows);
	xmlhttp.open("POST", "searchMessage.action", "true");
	xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	// 第四步：设置向服务器发送的数据，启动和服务器端的交互
	xmlhttp.send("page=1&rows=5");
}

// 第五步：判断和服务器端的交互是否完成，还要判断服务器端是否正确返回了数据
function callback() {
	if (xmlhttp.readyState == 4) {
		if (xmlhttp.status == 200) {
			var resultNode =document.getElementById("result");
			resultNode.innerHTML = xml.responseText;
			/*var oBook = eval_r('(' + xmlhttp.responseText + ')');
			$.messager.alert('test jsonData', xmlhttp.responseText);*/
		}
	}
}