<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<link rel="stylesheet" type="text/css" href="jquery-easyui-1.4/themes/default/easyui.css">
	    <link rel="stylesheet" type="text/css" href="jquery-easyui-1.4/themes/icon.css">
	    <link rel="stylesheet" type="text/css" href="jquery-easyui-1.4/demo/demo.css">
	    <script type="text/javascript" src="jquery-easyui-1.4/jquery.min.js"></script>
	    <script type="text/javascript" src="jquery-easyui-1.4/jquery.easyui.min.js"></script>
	    <script type="text/javascript" src="js/easyDemo.js"></script>
		
		<title>Insert title here</title>
		</head>
	<body>
		<s:form id="message-form" action="testMessage.action">
			<label>key word: </label>
			<input class="easyui-textbox" type="text" name="keyWord" data-options="width:300"></input>
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">Submit</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">Clear</a>
		</s:form>
	</body>

	<script type="text/javascript">
		$(document).ready(function() {
			
		})
		
		function submitForm(){
			$('#message-form').form('submit');
		}
		function clearForm(){
			$('#message-form').form('clear');
		}
	</script>
</html>