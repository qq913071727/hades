package org.medusa.message.struts.action;

import java.util.Date;

import org.medusa.message.hibernate.ServiceFactory;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;

import com.opensymphony.xwork2.ActionSupport;

public class AddMessageAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private MessageService messageService;

	private String title;
	private String description;
	private boolean result;

	public void validate(){
		System.out.println("**************** validate function in class AddMessageAction is invoked! **********************");	    
		
		if("".equals(title)||null==title){
			try{
				this.clearErrorsAndMessages();
				
				this.addFieldError("title","");
				this.addActionError("澶勭悊鍔ㄤ綔澶辫�?");
				this.addActionMessage("鎻愪氦澶辫触"); 
			}catch(Exception e){
				e.printStackTrace();
			}
		}	
	}

	@Override
	public String execute() throws Exception {
		System.out.println("method execute in class AddMessageAction begin");
		
		Message m = new Message();
		m.setTitle(title);
		m.setDescription(description);
		m.setCreationTime(new Date());

		messageService = ServiceFactory.getMessageService();
		messageService.persistMessage(m);
		result=true;
		return SUCCESS;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}

