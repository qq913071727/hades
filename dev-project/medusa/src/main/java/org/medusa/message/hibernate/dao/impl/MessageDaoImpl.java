package org.medusa.message.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.medusa.message.hibernate.HibernateSessionFactory;
import org.medusa.message.hibernate.dao.MessageDao;
import org.medusa.message.hibernate.model.Message;
import org.noahsark.core.dao.HibernateSessionDao;
import org.noahsark.core.dao.QueryBuilder;
import org.noahsark.core.domainmodel.BaseEntity;
import org.noahsark.core.enumeration.SQLType;


public class MessageDaoImpl extends HibernateSessionDao<Message, Long> implements MessageDao {
	/**
     * 根据id，查找Message对象
     * @param id
     * @return
     */
	public Message findMessage(Long id) {
		log.info("method findMessage in class MessageDaoImpl begin");
		
		Message message=this.find(Message.class, id);
        
        log.info("method findMessage in class MessageDaoImpl end");
        return message;
	}
	
	/**
     * 根据id，查找Message对象
     * @param id
     * @param refresh: true:从数据库查询，false:如果Session中有直接使用，可能 会导致查询出的实体与数据库不一致。
     * @return
     */
    public Message findMessageById(Long id, boolean refresh){
    	log.info("method findMessageById in class MessageDaoImpl begin");
    	
    	BaseEntity<?> baseEntity=this.findById(id, refresh);
    	Message message=(Message)baseEntity;
    	
    	log.info("method findMessageById in class MessageDaoImpl end");
    	return message;
    }
	
    /**
     * 持久化Message对象
     * @param m
     */
	public void persistMessage(Message m) {
		log.info("method persistMessage in class MessageDaoHibernate begin");
		
		this.persist(m);
		
		log.info("method persistMessage in class MessageDaoHibernate end");
	}

	public void delete(Long id) {
		Session session = HibernateSessionFactory.getSession();
        Transaction tx = session.beginTransaction();
		String hql = "DELETE Message WHERE id=:id";
		Query q = session.createQuery(hql);
		q.setParameter("id", id);
		q.executeUpdate();
		tx.commit();
        HibernateSessionFactory.closeSession();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Message> getAllMessage() {
		Session session = HibernateSessionFactory.getSession();
        Transaction tx = session.beginTransaction();
        Query q=session.createQuery("select m from Message m");
        List<Message> l=q.list();
        tx.commit();
        HibernateSessionFactory.closeSession();
		return l;
	}
	
	/**
     * 获取所有message记录的数量
     * @return
     */
    public Long getAllMessageAmount(){
    	log.info("method getAllMessageAmount in class MessageDaoImpl begin");
    	
    	Long result = this.count(new QueryBuilder(sql,SQLType.HQL));
    	
    	log.info("method getAllMessageAmount in class MessageDaoImpl end");
    	return result;

    }
	
	public void updateMessage(Message m){
		Session session = HibernateSessionFactory.getSession();
        Transaction tx = session.beginTransaction();
        Query q=session.createQuery("update Message m set m.description=:description, m.title=:title where m.id=:id");
        q.setParameter("description", m.getDescription());
        q.setParameter("title", m.getTitle());
        q.setParameter("id", m.getId());
        q.executeUpdate();
        tx.commit();
        HibernateSessionFactory.closeSession();
	}

	/**
     * 根据关键字keyWord查询message
     * @param keyWord
     * @return
     */
	@SuppressWarnings("unchecked")
	public List<Message> getMessagesByKeyWord(String keyWord) {
		Session session = HibernateSessionFactory.getSession();
        //Transaction tx = session.beginTransaction();
        Query q=session.createQuery("select m from Message m where m.description like :keyWord");
        q.setParameter("keyWord", "%"+keyWord+"%");
        List<Message> l=q.list();
        session.flush();
        session.clear();
        //tx.commit();
        //HibernateSessionFactory.closeSession();
		return l;
	}
	
	/**
	 * 根据第几页获取，每页几行获取数据
	 * @param page	第几页
	 * @param rows 每页多少行
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Message> getMessageListForPaging(Integer page, Integer rows) {
		
        //当为缺省值的时候进行赋值
//		Integer currentPage = (page == null || page == 0) ? 1 : page;
//		Integer pageSize = (rows == null || rows == 0) ? 10: rows;
		
		Session session = HibernateSessionFactory.getSession();
		Transaction tx = session.beginTransaction();
		Query q=session.createQuery("from Message");
		q.setFirstResult((page - 1) * rows);
		q.setMaxResults(rows);
		List<Message> allMessageList = q.list();
		tx.commit();
		HibernateSessionFactory.closeSession();
		
		return allMessageList;
	}



}
