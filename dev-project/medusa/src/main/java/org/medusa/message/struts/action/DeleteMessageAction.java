package org.medusa.message.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import org.medusa.message.hibernate.ServiceFactory;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;

import com.opensymphony.xwork2.ActionSupport;

public class DeleteMessageAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private MessageService messageService;

	private String keyWord;
	private String searchSubmit;
	private String deleteSubmit;
	private String[] deleteItem;
	private boolean result;

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getSearchSubmit() {
		return searchSubmit;
	}

	public void setSearchSubmit(String searchSubmit) {
		this.searchSubmit = searchSubmit;
	}

	public String getDeleteSubmit() {
		return deleteSubmit;
	}

	public void setDeleteSubmit(String deleteSubmit) {
		this.deleteSubmit = deleteSubmit;
	}

	public String[] getDeleteItem() {
		return deleteItem;
	}

	public void setDeleteItem(String[] deleteItem) {
		this.deleteItem = deleteItem;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String executeSearch() throws Exception {
		messageService = ServiceFactory.getMessageService();
		List<Message> l = messageService.getMessagesByKeyWord(keyWord);
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("messages", l);
		return "success";
	}

	public String executeDelete() throws Exception {
		messageService = ServiceFactory.getMessageService();

		for (int i = 0; deleteItem.length > i; i++) {
			messageService.deleteMessage(Long.parseLong(deleteItem[i]));
		}
		result=true;
		executeSearch();
		return "success";
	}
}
