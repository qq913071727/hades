package org.medusa.message.hibernate.service;

import java.util.List;

import org.medusa.message.hibernate.model.Message;


public interface MessageService {
	/**
     * 根据id，查找Message对象
     * @param id
     * @return
     */
	Message findMessage(Long id);
	
	/**
     * 根据id，查找Message对象
     * @param id
     * @param refresh: true:从数据库查询，false:如果Session中有直接使用，可能 会导致查询出的实体与数据库不一致。
     * @return
     */
	Message findMessageById(Long id,boolean refresh);
	
	/**
     * 持久化Message对象
     * @param m
     */
	void persistMessage(Message m);
	
	void deleteMessage(Long id);
	
	List<Message> getAllMessage();
	
	/**
     * 获取所有message记录的数量
     * @return
     */
    Long getAllMessageAmount();
	
    /**
     * 根据关键字keyWord查询message
     * @param keyWord
     * @return
     */
	List<Message> getMessagesByKeyWord(String keyWord);
	
	/**
	 * 根据第几页获取，每页几行获取数据
	 * @param page	第几页
	 * @param rows 每页多少行
	 * @return
	 */
    List<Message> getMessageListForPaging(Integer page, Integer rows);
	
	void updateMessage(Message m);
}
