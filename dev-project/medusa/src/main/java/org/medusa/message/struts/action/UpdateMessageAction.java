package org.medusa.message.struts.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import org.medusa.message.hibernate.ServiceFactory;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;

import com.opensymphony.xwork2.ActionSupport;

public class UpdateMessageAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private String keyWord;
	private String id;
	private String title;
	private String description;
	private boolean result;

	private MessageService messageService;

	public String executeSearch() throws Exception {
		messageService = ServiceFactory.getMessageService();
		List<Message> l = messageService.getMessagesByKeyWord(keyWord);
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("messages", l);
		return "success";
	}

	public String executeUpdate() throws Exception {
		messageService = ServiceFactory.getMessageService();
		
		Message m =new Message();
		m.setId(Long.valueOf(id));
		m.setTitle(title);
		m.setDescription(description);
		m.setCreationTime(new Date());
		
		messageService.updateMessage(m);
		result = true;
		//executeSearch();
		return "success";
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}
