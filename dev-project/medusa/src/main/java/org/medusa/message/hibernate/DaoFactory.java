package org.medusa.message.hibernate;

import org.medusa.message.hibernate.dao.MessageDao;
import org.medusa.message.hibernate.dao.impl.MessageDaoImpl;

public class DaoFactory {
	private static MessageDao messageDao;

	public static MessageDao getMessageDao() {
		if(null==messageDao){
			messageDao=new MessageDaoImpl();
		}
		return messageDao;
	}

	public static void setMessageDao(MessageDao messageDao) {
		DaoFactory.messageDao = messageDao;
	}
   
}
