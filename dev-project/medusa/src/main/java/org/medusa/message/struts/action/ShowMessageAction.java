package org.medusa.message.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import org.medusa.message.hibernate.ServiceFactory;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;

import com.opensymphony.xwork2.ActionSupport;

public class ShowMessageAction /*extends ActionSupport*/ {

	private static final long serialVersionUID = 1L;
	
	private String showMessage;
	
	private MessageService messageService;
	
	public String execute() throws Exception {
		if("showMessage".equals(showMessage)){
			messageService = ServiceFactory.getMessageService();
			List<Message> l=messageService.getAllMessage();
			HttpServletRequest request = ServletActionContext.getRequest ();
			request.setAttribute("messages", l);
			return "SUCCESS";
		}
		return null;
		
	}

	public String getShowMessage() {
		return showMessage;
	}

	public void setShowMessage(String showMessage) {
		this.showMessage = showMessage;
	}
	
}
