package org.medusa.message.hibernate.service.impl;

import java.util.List;

import org.medusa.message.hibernate.DaoFactory;
import org.medusa.message.hibernate.dao.MessageDao;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;
import org.noahsark.core.service.BaseService;


public class MessageServiceImpl extends BaseService implements MessageService {
	private MessageDao messageDao;
	
	/**
     * 根据id，查找Message对象
     * @param id
     * @return
     */
	public Message findMessage(Long id){
		log.info("method findMessage in class MessageServiceImpl begin");
		
		messageDao=DaoFactory.getMessageDao();
		Message message=messageDao.findMessage(id);
		
		log.info("method findMessage in class MessageServiceImpl end");
		return message;
	}
	
	/**
     * 根据id，查找Message对象
     * @param id
     * @param refresh: true:从数据库查询，false:如果Session中有直接使用，可能 会导致查询出的实体与数据库不一致。
     * @return
     */
	public Message findMessageById(Long id,boolean refresh){
		log.info("method findMessageById in class MessageServiceImpl begin");
		
		messageDao=DaoFactory.getMessageDao();
		Message message=messageDao.findMessageById(id, refresh);
		
		log.info("method findMessageById in class MessageServiceImpl end");
		return message;
	}
	
	/**
     * 持久化Message对象
     * @param m
     */
	public void persistMessage(Message m) {
		messageDao=DaoFactory.getMessageDao();
		messageDao.persistMessage(m);
	}

	public void deleteMessage(Long id) {
		messageDao=DaoFactory.getMessageDao();
		messageDao.delete(id);
	}
	
	public void updateMessage(Message m){
		messageDao=DaoFactory.getMessageDao();	
		messageDao.updateMessage(m);
	}

	public List<Message> getAllMessage() {
		messageDao=DaoFactory.getMessageDao();
		List<Message> l=messageDao.getAllMessage();
		return l;
	}
	
	/**
     * 获取所有message记录的数量
     * @return
     */
	public Long getAllMessageAmount() {
		log.info("method getAllMessageAmount in class MessageServiceImpl begin");
		
		messageDao=DaoFactory.getMessageDao();
		Long result = messageDao.getAllMessageAmount();
		
		log.info("method getAllMessageAmount in class MessageServiceImpl end");
		return result;
	}

	public List<Message> getMessagesByKeyWord(String keyWord) {
		messageDao=DaoFactory.getMessageDao();
		List<Message> l=messageDao.getMessagesByKeyWord(keyWord);
		return l;
	}
	
	/**
	 * 根据第几页获取，每页几行获取数据
	 * @param page	第几页
	 * @param rows 每页多少行
	 * @return
	 */
    public List<Message> getMessageListForPaging(Integer page, Integer rows){
    	messageDao=DaoFactory.getMessageDao();
    	return messageDao.getMessageListForPaging(page, rows);    	
    }

}
