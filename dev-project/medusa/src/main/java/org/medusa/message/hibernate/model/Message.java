package org.medusa.message.hibernate.model;

import java.io.Serializable;
import java.util.Date;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;

import org.noahsark.core.domainmodel.BaseEntity;

//@Entity
//@Table(name = "MSG")
public class Message extends BaseEntity<Long> implements Serializable {
	private static final long serialVersionUID = 1L;
	
//	@Id
//	@Column(name = "ID")
	private Long id;
	
//	@Column(name = "TITLE")
	private String title;
	
//	@Column(name = "DESCRIPTION")
	private String description;
	
//	@Column(name = "CREATIONTIME")
	private Date creationTime;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

}
