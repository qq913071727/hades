package org.medusa.message.struts.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.medusa.message.hibernate.ServiceFactory;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;
import org.noahsark.core.action.BaseCrudAction;
import org.noahsark.core.exception.BusinessException;

public class TestMessageAction extends BaseCrudAction<Message> implements ServletRequestAware {
	private static final long serialVersionUID = 1L;
	
	private MessageService messageService;
	private String keyWord;
		
	public String execute() throws Exception {
		logger.info("11111111111111111111"+keyWord);
		
		messageService = ServiceFactory.getMessageService();
		Message message=new Message();
		//message.setId(new Long(100));
		message.setDescription("test for persistMessage method");
		messageService.persistMessage(message);
		logger.info("22222222222222222222222"+messageService.findMessage(new Long(97)).getDescription());
		//logger.info("22222222222222222222222"+messageService.findMessageById(new Long(keyWord),true).getDescription());
		//logger.info("333333333333333333"+messageService.findMessage(new Long(keyWord)).getDescription());
		
		return "success";
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public void setServletRequest(HttpServletRequest arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initEntity() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void createEntity() throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void updateEntity() throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void removeEntity() throws BusinessException {
		// TODO Auto-generated method stub
		
	}

}
