package org.medusa.message.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class PropertiesUtil {
	
	/**
	 * 获取指定properties文件中的value�?
	 * @param file properties文件的路径和文件�?
	 * @param key properties文件中的key
	 * @return properties文件中的value
	 */
	public static String getValue(String file,String key){
		PropertiesConfiguration config=null;
		try {
			config=new PropertiesConfiguration(file);
			
		} catch (ConfigurationException e1) {
			e1.printStackTrace();
		}
        
		return config.getProperty(key).toString();
	}
	
	/**
	 * 设置指定properties文件中的value�?
	 * @param file properties文件的路径和文件�?
	 * @param key properties文件中的key
	 * @param value properties文件中的value
	 */
	public static void setValue(String file,String key,String value){
		try {
			PropertiesConfiguration config=new PropertiesConfiguration(file);
			config.setProperty(key, value);
			config.save();
		} catch (ConfigurationException e1) {
			e1.printStackTrace();
		}
	}
}
