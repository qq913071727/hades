package org.medusa.message.struts.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import org.medusa.message.hibernate.ServiceFactory;
import org.medusa.message.hibernate.model.Message;
import org.medusa.message.hibernate.service.MessageService;

import org.noahsark.core.action.BaseCrudAction;
import org.noahsark.core.exception.BusinessException;

public class SearchMessageAction extends BaseCrudAction<Message> implements ServletRequestAware {

	private static final long serialVersionUID = 1L;
	private String keyWord;
	private MessageService messageService;
	private HttpServletRequest request;
	private Integer total;
	private List<Object> rows;

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<Object> getRows() {
		return rows;
	}

	public void setRows(List<Object> rows) {
		this.rows = rows;
	}

	public String execute() throws Exception {
		logger.info("method execute in class SearchMessageAction begin");
		
		logger.info("keyWord	"+keyWord);
		
		/*String page = request.getParameter("page");
		String row = request.getParameter("rows");//接受参数page和rows
		System.out.println("page	"+page);
		System.out.println("row	"+row);*/
		
		messageService = ServiceFactory.getMessageService();
		logger.info("*********************************"+messageService.getAllMessageAmount());
		//this.total = messageService.getAllMessageAmount();
		/*this.rows = new ArrayList<Object>();
		List<Message> list = messageService.getMessageListForPaging(Integer.parseInt(page), Integer.parseInt(row));
		
		for(int i=0;i<list.size();i++){
			Message message=list.get(i);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ID", message.getId());
			map.put("title", message.getTitle());
			map.put("description", message.getDescription());
			map.put("createTime", message.getCreationTime());
			this.rows.add(map);
		}*/
		
		//List<Message> keyWordMessageList = messageService.getMessagesByKeyWord(keyWord);
//		List<Message> allMessageList = messageService.getAllMessage();
//		
//		HttpServletResponse response = ServletActionContext.getResponse();
//		HttpServletRequest request = ServletActionContext.getRequest();
		
//		JSONObject jobj = new JSONObject();//new一个JSON
//		jobj.accumulate("total", allMessageList);//total代表一共有多少数据
//		jobj.accumulate("rows", keyWordMessageList);//row是代表显示的页的数据
//
//		response.setCharacterEncoding("utf-8");//指定为utf-8
//		response.getWriter().write(jobj.toString());//转化为JSon格式
		
		
		
		/*for(Message m:keyWordMessageList){
			System.out.println("description:	"+m.getDescription());
		}*/
//		HttpServletRequest request = ServletActionContext.getRequest();
//		request.setAttribute("messages", keyWordMessageList);
		//return null;
		
		logger.info("method execute in class SearchMessageAction end");
		return "success";
	}

	@Override
	protected void createEntity() throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initEntity() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void removeEntity() throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void updateEntity() throws BusinessException {
		// TODO Auto-generated method stub
		
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
}
