package org.medusa.message.hibernate;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.medusa.message.hibernate.model.Message;

public class HibernateTest {
	public static void main(String[] args) throws Exception {
		/*Configuration conf = new Configuration().configure();
		SessionFactory sf = conf.buildSessionFactory();
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Message im = new Message();
		im.setId(2);
		im.setKeyWord("keyWord");
		im.setDescription("djaiofapg");
		im.setCreationTime(new Date());
		System.out.println(im.getId());
		sess.save(im);
		tx.commit();
		sess.close();
		sf.close();*/
		
		Session session = HibernateSessionFactory.getSession();
        Transaction tx = session.beginTransaction();
        Message im = new Message();
        //im.setId(3);
		im.setTitle("keyWord");
		im.setDescription("571290457901257901");
		im.setCreationTime(new Date());
		session.save(im);
		tx.commit();
		HibernateSessionFactory.closeSession();
	}
}
