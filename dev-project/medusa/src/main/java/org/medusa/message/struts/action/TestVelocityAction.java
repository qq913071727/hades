package org.medusa.message.struts.action;

import com.opensymphony.xwork2.ActionSupport;

public class TestVelocityAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private String message;

	public String testVelocity() throws Exception {
		message="*****************************************************************************";
		return "success";
	}
	
	public void setMessage(String message){
		this.message=message;	
	}
	
	public String getMessage(){
		return this.message;	
	}
}
