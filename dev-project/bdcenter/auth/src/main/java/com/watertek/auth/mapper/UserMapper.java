package com.watertek.auth.mapper;

import com.watertek.auth.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper {
    @Select("SELECT * FROM user WHERE id = #{id}")
    User findById(Long id);

    @Select("SELECT * FROM user WHERE name = #{name}")
    User findByName(String name);

    @Select("SELECT * FROM user")
    List<User> findAll();

    @Insert("INSERT INTO user(id, name, password, role) VALUES(#{id}, #{name}, #{password}, #{role})")
    void insert(User user);

    @Update("UPDATE user SET name=#{name}, password=#{password}, role=#{role} WHERE id =#{id}")
    void update(User user);

    @Delete("DELETE FROM user WHERE id =#{id}")
    void delete(Long id);
}
