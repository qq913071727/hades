package com.watertek.auth.service;

import com.watertek.auth.entity.User;

import java.util.List;

public interface UserService {
    User findById(Long id);

    List<User> findAll();

    void insert(User user);

    void update(User user);

    void delete(Long id);
}
