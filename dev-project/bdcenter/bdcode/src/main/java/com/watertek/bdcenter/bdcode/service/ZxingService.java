package com.watertek.bdcenter.bdcode.service;

import com.google.zxing.WriterException;

import java.io.IOException;


public interface ZxingService{
    void createQrCode(String contents) throws IOException, WriterException;

}