package com.watertek.bdcenter.bdcode.service;

import com.watertek.bdcenter.bdcode.entity.AnJian;

import java.util.List;

public interface AnJianService {
    /**
     * 返回全部AnJian对象
     * @return
     */
    List<AnJian> findAll();
}
