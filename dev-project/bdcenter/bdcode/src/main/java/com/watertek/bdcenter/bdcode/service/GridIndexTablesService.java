package com.watertek.bdcenter.bdcode.service;

public interface GridIndexTablesService {

    /**
     * 添加GridIndexTables对象
     * @param rowName
     * @param familyName
     * @param qualifier
     * @param value
     */
    void put(String rowName, String familyName, String qualifier, byte[] value);

    /**
     * 从业务数据库中依次获取每个表格的每条记录的经纬度，加上层级（暂时写死在config.properties文件中），
     * 将其转换为北斗网格码，最后将业务数据的id和北斗网格码存储到HBase中。
     */
    void loadBdCode();
}
