package com.watertek.bdcenter.bdcode.rest;

import com.alibaba.fastjson.JSON;
import com.watertek.bdcenter.bdcode.service.HiveService;
import com.watertek.bdcenter.bdcode.vo.ResultPojo;
import com.watertek.bdcenter.bdcode.vo.SqoopVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class SqoopController extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(SqoopController.class);

    @Autowired
    private HiveService hiveService;

    /**
     * 将源数据库的表结构复制到hive
     *
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/copy/table/structure", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> copyTableStructure(HttpServletRequest request,
                                                         HttpServletResponse response,
                                                         @RequestBody SqoopVo vo) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        hiveService.copyTableStructure(SQOOP_BIN_FILE_PATH, vo.getUrl(), vo.getUsername(), vo.getPassword(),
                vo.getTableName(), vo.getHiveDatabase(), vo.getHiveTableName());

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(ResultPojo.MSG_SUCCESS));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 将数据从关系数据库导入到hive表中
     *
     * @param request
     * @param response
     * @param vo
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/import/table", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> importTable(HttpServletRequest request,
                                                  HttpServletResponse response,
                                                  @RequestBody SqoopVo vo) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        // 将关系型数据的表导入hive中
        hiveService.importHiveTable(SQOOP_BIN_FILE_PATH, vo.getUrl(), vo.getUsername(), vo.getPassword(),
                vo.getTableName(), vo.getHiveDatabase(), vo.getHiveTableName());

        // 从关系型数据库中获取元数据，在本地创建hive orc表
        hiveService.createOrcTable(vo.getHiveTableName());

        // 将hive普通表中的数据插入到hive orc表中
        hiveService.insertOrcTable(vo.getHiveTableName()+HIVE_TEMP_TABLE_SUFFIX, vo.getHiveTableName(), POINT_TO_CODE_LAYER);

        // 存储元数据
        hiveService.storeMetadata(vo.getHiveTableName());

        // 删除hive普通表
        hiveService.dropTable(vo.getHiveTableName()+HIVE_TEMP_TABLE_SUFFIX);

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(ResultPojo.MSG_SUCCESS));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 对hive中指定的表进行手工编码
     *
     * @param request
     * @param response
     * @param vo
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/manual/code", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> manualCode(HttpServletRequest request,
                                                HttpServletResponse response,
                                                @RequestBody SqoopVo vo) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        // 对导入的数据进行编码
        hiveService.manualCode(vo.getHiveTableName(), vo.getHiveTableName(), vo.getId(), vo.getBdCode());

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(ResultPojo.MSG_SUCCESS));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 查询表格中的记录
     * @param request
     * @param response
     * @param tableName
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/query/{tableName}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> query(HttpServletRequest request,
                                           HttpServletResponse response,
                                           @PathVariable(value = "tableName") String tableName) {

        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        List list=hiveService.findAll(tableName);

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(list));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 根据id,查询表格中的记录某一条记录
     * @param request
     * @param response
     * @param tableName
     * @param id
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/query/{tableName}/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> queryById(HttpServletRequest request,
                                            HttpServletResponse response,
                                            @PathVariable(value = "tableName") String tableName,
                                                @PathVariable(value = "id") String id) {

        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        List list=hiveService.findById(tableName, id);

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(list));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 根据北斗网格编码bdCode，查询表格中的某一条记录
     * @param request
     * @param response
     * @param tableName
     * @param bdCode
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/query/{tableName}/bdcode/{bdCode}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> queryByBdCode(HttpServletRequest request,
                                                HttpServletResponse response,
                                                @PathVariable(value = "tableName") String tableName,
                                                @PathVariable(value = "bdCode") String bdCode) {

        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        List list=hiveService.findByBdCode(tableName, bdCode);

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(list));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 对某个表进行分页查询
     * @param request
     * @param response
     * @param tableName
     * @param startNo
     * @param maxCount
     * @return
     */
    @RequestMapping(value = "/geoSOT-API/query/{tableName}/{startNo}/{maxCount}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> queryByPage(HttpServletRequest request,
                                                    HttpServletResponse response,
                                                    @PathVariable(value = "tableName") String tableName,
                                                    @PathVariable(value = "startNo") String startNo,
                                                    @PathVariable(value = "maxCount") String maxCount) {

        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        List list=hiveService.findByPage(tableName, Integer.parseInt(startNo), Integer.parseInt(maxCount));

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(list));
        result.setMessage(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}


