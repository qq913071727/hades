package com.watertek.bdcenter.bdcode.common;

/**
 * 数据库驱动的常量类
 */
public class DriverClassType {

    /**
     * 数据源是hive
     */
    public static final String HIVE="hive";

    /**
     * hive的驱动类
     */
    public static final String HIVE_DRIVER_CLASS="org.apache.hive.jdbc.HiveDriver";

    /**
     * 数据源是mysql
     */
    public static final String MYSQL="mysql";

    /**
     * mysql的驱动类
     */
    public static final String MYSQL_DRIVER_CLASS="com.mysql.jdbc.Driver";
}
