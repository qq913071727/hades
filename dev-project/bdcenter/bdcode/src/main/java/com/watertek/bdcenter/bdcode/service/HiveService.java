package com.watertek.bdcenter.bdcode.service;

import java.util.List;

public interface HiveService {

    /**
     * 将源数据库的表结构复制到hive
     * @param sqoopBinFilePath
     * @param url
     * @param username
     * @param password
     * @param tableName
     * @param hiveDatabase
     * @param hiveTableName
     */
    void copyTableStructure(String sqoopBinFilePath, String url, String username, String password,
                            String tableName, String hiveDatabase, String hiveTableName);

    /**
     * 将关系型数据的表导入hive中
     * @param sqoopBinFilePath
     * @param url
     * @param username
     * @param password
     * @param tableName
     * @param hiveDatabase
     * @param hiveTableName
     */
    void importHiveTable(String sqoopBinFilePath, String url, String username, String password,
                         String tableName, String hiveDatabase, String hiveTableName);

    /**
     * 对指定的hive表进行手工编码
     * @param databaseName
     * @param tableName
     * @param id
     * @param bdCode
     */
    void manualCode(String databaseName, String tableName, String id, String bdCode);

    /**
     * 从关系型数据库中获取元数据，在本地创建hive orc表
     * @param tableName
     */
    void createOrcTable(String tableName);

    /**
     * 存储元数据
     * @param tableName
     */
    void storeMetadata(String tableName);

    /**
     * 将originTable表中的数据插入到destTable表中
     * @param originTable
     * @param destTable
     * @param layer
     */
    void insertOrcTable(String originTable, String destTable, String layer);

    /**
     * 删除表格
     * @param tableName
     */
    void dropTable(String tableName);

    /**
     * 查询表格中的记录
     * @param tableName
     * @return
     */
    List findAll(String tableName);

    /**
     * 根据id,查询表格中的某一条记录
     * @param tableName
     * @param id
     * @return
     */
    List findById(String tableName, String id);

    /**
     * 根据北斗网格编码bdCode，查询表格中的某一条记录
     * @param tableName
     * @param bdCode
     * @return
     */
    List findByBdCode(String tableName, String bdCode);

    /**
     * 对某个表进行分页查询
     * @param tableName
     * @param startNo
     * @param maxCount
     */
    List findByPage(String tableName, int startNo, int maxCount);
}
