package com.watertek.bdcenter.bdcode.dao;

import java.util.List;
import java.util.Map;

public interface HiveDao {

    /**
     * 对指定的hive表进行手工编码
     * @param databaseName
     * @param tableName
     * @param id
     * @param bdCode
     */
    void manualCode(String databaseName, String tableName, String id, String bdCode);

    /**
     * 根据表名获取元数据：列名和列的类型
     * @param tableName
     * @return
     */
    Map<String, String> getMetadataMap(String tableName);

    /**
     * 根据元数据参数map，创建表
     * @param tableName
     * @param map
     */
    void createOrcTable(String tableName, Map<String, String> map);

    /**
     * 存储元数据
     * @param tableName
     */
    void storeMetadata(String tableName, Map<String, String> map);

    /**
     * 将originTable表中的数据插入到destTable表中
     * @param originTable
     * @param destTable
     * @param layer
     */
    void insertOrcTable(String originTable, String destTable, String layer);

    /**
     * 删除表格
     * @param tableName
     */
    void dropTable(String tableName);

    /**
     * 查询表格中的记录
     * @param tableName
     * @return
     */
    List findAll(String tableName);

    /**
     * 根据id,查询表格中的某一条记录
     * @param tableName
     * @param id
     * @return
     */
    List findById(String tableName, String id);

    /**
     * 根据北斗网格编码bdCode，查询表格中的某一条记录
     * @param tableName
     * @param bdCode
     * @return
     */
    List findByBdCode(String tableName, String bdCode);

    /**
     * 对某个表进行分页查询
     * @param tableName
     * @param startNo
     * @param maxCount
     */
    List findByPage(String tableName, int startNo, int maxCount);
}
