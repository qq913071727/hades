package com.watertek.bdcenter.bdcode.service.impl;

import com.watertek.bdcenter.bdcode.entity.AnJian;
import com.watertek.bdcenter.bdcode.mapper.AnJianMapper;
import com.watertek.bdcenter.bdcode.service.AnJianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("anJianService")
public class AnJianServiceImpl extends AbstractService implements AnJianService {
    @Autowired
    private AnJianMapper anJianMapper;

    /**
     * 返回全部AnJian对象
     * @return
     */
    @Override
    public List<AnJian> findAll() {
        return anJianMapper.findAll();
    }
}
