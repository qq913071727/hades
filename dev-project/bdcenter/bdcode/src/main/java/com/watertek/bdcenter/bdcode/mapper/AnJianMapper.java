package com.watertek.bdcenter.bdcode.mapper;

import com.watertek.bdcenter.bdcode.entity.AnJian;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AnJianMapper {
    /**
     * 返回全部AnJian对象
     * @return
     */
    @Select("SELECT * FROM blz_anjian")
    List<AnJian> findAll();
}
