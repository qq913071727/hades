package com.watertek.bdcenter.bdcode.vo;

import java.io.Serializable;

/**
 * sqoop的VO类
 */
public class SqoopVo implements Serializable {
    /**
     * 数据库url，如jdbc:mysql://10.68.1.31:3306/blz_sjbm
     */
    private String url;

    /**
     * 数据库用户名
     */
    private String username;

    /**
     * 数据库密码
     */
    private String password;

    /**
     * 数据库表名
     */
    private String tableName;

    /**
     * hive数据库名
     */
    private String hiveDatabase;

    /**
     * hive数据库表名
     */
    private String hiveTableName;

    /**
     * 经度
     */
    private String longitudeColumn;

    /**
     * 纬度
     */
    private String latitudeColumn;

    /**
     * 每一行的主键
     */
    private String id;

    /**
     * 北斗码
     */
    private String bdCode;

    public String getBdCode() {
        return bdCode;
    }

    public void setBdCode(String bdCode) {
        this.bdCode = bdCode;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitudeColumn() {
        return latitudeColumn;
    }

    public void setLatitudeColumn(String latitudeColumn) {
        this.latitudeColumn = latitudeColumn;
    }

    public String getLongitudeColumn() {

        return longitudeColumn;
    }

    public void setLongitudeColumn(String longitudeColumn) {
        this.longitudeColumn = longitudeColumn;
    }

    public String getHiveTableName() {
        return hiveTableName;
    }

    public void setHiveTableName(String hiveTableName) {
        this.hiveTableName = hiveTableName;
    }

    public String getHiveDatabase() {

        return hiveDatabase;
    }

    public void setHiveDatabase(String hiveDatabase) {
        this.hiveDatabase = hiveDatabase;
    }

    public String getTableName() {

        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
