package com.watertek.bdcenter.bdcode.constant;

/**
 * 表格名称常量类
 */
public class TableName {
    /**
     * grid-index-tables表
     */
    public static final String GRID_INDEX_TABLES_NAME="grid-index-tables";
}
