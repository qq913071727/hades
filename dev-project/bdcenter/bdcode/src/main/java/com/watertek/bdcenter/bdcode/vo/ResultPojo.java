package com.watertek.bdcenter.bdcode.vo;

/**
 * 返回结果封装类
 */
public class ResultPojo implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * CODE_SUCCESS
     */
    public static final String CODE_SUCCESS = "1";
    /**
     * MSG_SUCCESS
     */
    public static final String MSG_SUCCESS = "成功";
    /**
     * CODE_FAILURE
     */
    public static final String CODE_FAILURE = "0";
    /**
     * MSG_FAILURE
     */
    public static final String MSG_FAILURE = "失败";

    /**
     * 请登录
     */
    public static final String CODE_NOT_LOGIN = "2400";
    /**
     * 没有权限操作
     */
    public static final String CODE_NOT_AUTHORIZE = "2403";
    /**
     * 格式校验失败
     */
    public static final String CODE_FORMAT_ERR = "2501";
    /**
     * 逻辑校验失败
     */
    public static final String CODE_LOGIC_ERR = "2502";

    private String code = CODE_SUCCESS;

    private String message = MSG_SUCCESS;

    private Object result = null;

    /**
     * 自定义返回内容
     */
    public ResultPojo(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 自定义返回内容
     */
    public ResultPojo(String code, String message, Object result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public ResultPojo() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResultPojo{" + "code='" + code + '\'' + ", message='" + message + '\'' + ", result=" + result + '}';
    }
}
