package com.watertek.bdcenter.bdcode.util;

import com.watertek.bdcenter.bdcode.common.DriverClassType;

import java.sql.*;

/**
 * 用于获取元数据的工具类
 */
public class MetadataUtil {
    private static final String URL=PropertiesUtil.getValue("config/config.properties", "hive.url");

    private static final String USERNAME=PropertiesUtil.getValue("config/config.properties", "hive.username");

    private static final String PASSWORD=PropertiesUtil.getValue("config/config.properties", "hive.password");

    private static Connection conn;
    private PreparedStatement pstmt;
    private ResultSet resultSet;

    public static Connection getConnnection(String driverClassType, String url, String username, String password) {
        // 如果驱动类或url为空，直接返回null
        if(null==driverClassType){
            return null;
        }
        if(null==url){
            return null;
        }

        try {
            if(driverClassType.equals(DriverClassType.HIVE)){
                Class.forName(DriverClassType.HIVE_DRIVER_CLASS).newInstance();
            }
            if(driverClassType.equals(DriverClassType.MYSQL)){
                Class.forName(DriverClassType.MYSQL_DRIVER_CLASS).newInstance();
            }

            conn = DriverManager.getConnection(url, username, password);        //只是连接hive, 用户名可不传
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static Connection getHiveConnection(String url, String username, String password){
        return getConnnection(DriverClassType.HIVE, url, username, password);
    }

    public static PreparedStatement prepare(Connection conn, String sql) {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ps;
    }

    public static PreparedStatement prepare(String sql){
        PreparedStatement ps = null;
        try {
            ps = getConnnection(DriverClassType.MYSQL_DRIVER_CLASS, URL, USERNAME, PASSWORD).prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ps;
    }

    public static ResultSet executeQuery(String sql){
//        Statement statement=getConnnection().createStatement();
//        try {
//            return statement.executeQuery(sql);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return null;
    }

    /**
     * 根据参数tableName，获取表的元数据
     * @param tableName
     * @return
     */
    public static ResultSet getResultSet(String tableName){
        DatabaseMetaData metaData;
        ResultSet rs;
        try {
            metaData = getConnnection(DriverClassType.MYSQL_DRIVER_CLASS, URL, USERNAME, PASSWORD).getMetaData();
            rs = metaData.getColumns(null, "%", tableName, null);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        DatabaseMetaData metaData;
        ResultSet rs;
        try {
            metaData = getConnnection("com.mysql.jdbc.Driver", "jdbc:hive2://10.68.1.31:10000/bdgcc", "", "").getMetaData();
            rs = metaData.getColumns(null, "%", "blz_anjian", null);
            while (rs.next()) {
                String colName = rs.getString("COLUMN_NAME");
                System.out.println("==============" + colName + "===============");
                System.out.println("Column Name: " + colName);
                System.out.println("Column Type: " + rs.getString("DATA_TYPE"));
                System.out.println("Column Type Name: " + rs.getString("TYPE_NAME"));
                System.out.println("Column Size: " + rs.getString("COLUMN_SIZE"));
                System.out.println("Column Decimal Digits: " + rs.getString("DECIMAL_DIGITS"));
                System.out.println("Column Nullable: " + rs.getString("NULLABLE"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
