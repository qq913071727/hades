package com.watertek.bdcenter.bdcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@ImportResource(locations = {"classpath:/config/hbase-spring.xml"})
public class BdCodeApplication {
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        return new UserDetailsService();
//    }

    // 自动配置Spring框架
    public static void main(String[] args) {
        SpringApplication.run(BdCodeApplication.class, args);
    }
}
