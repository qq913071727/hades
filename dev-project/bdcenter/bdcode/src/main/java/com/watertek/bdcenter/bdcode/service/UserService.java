package com.watertek.bdcenter.bdcode.service;

import com.watertek.bdcenter.bdcode.entity.User;

import java.util.List;

public interface UserService {
    User findById(Long id);

    List<User> findAll();

    void insert(User user);

    void update(User user);

    void delete(Long id);
}
