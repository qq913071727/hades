package com.watertek.bdcenter.bdcode.service.impl;

import com.watertek.bdcenter.bdcode.dao.HiveDao;
import com.watertek.bdcenter.bdcode.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractService {

    @Autowired
    protected HiveDao hiveDao;

    /**
     * 将经纬度转换为北斗网格码时的层级
     */
    public static final String POINT_TO_CODE_LAYER = PropertiesUtil.getValue("config/config.properties", "point.to.code.layer");

    /**
     * hive临时表的后缀
     */
    public static final String HIVE_TEMP_TABLE_SUFFIX = PropertiesUtil.getValue("config/config.properties", "hive.temp.table.suffix");


}
