package com.watertek.bdcenter.bdcode.rest;

import com.watertek.bdcenter.bdcode.common.Picture;
import com.watertek.bdcenter.bdcode.service.AnJianService;
import com.watertek.bdcenter.bdcode.service.GridIndexTablesService;
import com.watertek.bdcenter.bdcode.service.ZxingService;
import com.watertek.bdcenter.bdcode.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RestController;

@RestController
public abstract class AbstractController {
    /**
     * 右偏移量
     */
    public static final int RIGHTWARD_SHIFT=1;

    /**
     * 下偏移量
     */
    public static final int DOWNWARD_SHIFT=-1;

    /**
     * ZxingService服务类
     */
    @Autowired
    public ZxingService zxingService=null;

    /**
     * BCryptPasswordEncoder类
     */
    @Autowired
    public BCryptPasswordEncoder bCryptPasswordEncoder=null;

    /**
     * sqoop命令的路径
     */
    public static final String SQOOP_BIN_FILE_PATH = System.getenv("SQOOP_HOME")+"/bin/sqoop";

    /**
     * 高德开放平台API的URL
     */
    public static final String AMAP_API_URL = PropertiesUtil.getValue("config/config.properties", "amap.api.url");

    /**
     * 高德开放平台API的URL
     */
    public static final String AMAP_API_KEY = PropertiesUtil.getValue("config/config.properties", "amap.api.key");

    /**
     * hive临时表的后缀
     */
    public static final String HIVE_TEMP_TABLE_SUFFIX = PropertiesUtil.getValue("config/config.properties", "hive.temp.table.suffix");

    /**
     * 将经纬度转换为北斗网格码时的层级
     */
    public static final String POINT_TO_CODE_LAYER = PropertiesUtil.getValue("config/config.properties", "point.to.code.layer");

    @Autowired
    protected GridIndexTablesService gridIndexTablesService;

    @Autowired
    protected AnJianService anJianService;

    /**
     * 初始化时，在windows环境，加载GeoSOT.dll库；在linux环境，加载libgeosot库
     */
    static {
        try {
            String os = System.getProperty("os.name");
            if(os.toLowerCase().startsWith("win")){
                System.loadLibrary("GeoSOT");
                Picture.path=PropertiesUtil.getValue("config/config.properties", "windows.picture.path");
            }else{
                System.load("/opt/libgeosot-2.0.so");
                Picture.path=PropertiesUtil.getValue("config/config.properties", "linux.picture.path");
            }
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }
}













