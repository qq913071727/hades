package com.watertek.bdcenter.bdcode.common;

import com.watertek.bdcenter.bdcode.util.PropertiesUtil;

/**
 * 图片路径类
 */
public class Picture {
    /**
     * 图片的路径
     */
    public static String path;

    /**
     * 二维码图片的名称
     */
    public static final String PICTURE_NAME= PropertiesUtil.getValue("config/config.properties", "picture.name");

    /**
     * 二维码图片中小图片的名称
     */
    public static final String SMALL_PICTURE_NAME= PropertiesUtil.getValue("config/config.properties", "small_picture.name");
}
