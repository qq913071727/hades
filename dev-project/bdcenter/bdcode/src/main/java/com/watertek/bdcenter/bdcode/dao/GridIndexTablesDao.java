package com.watertek.bdcenter.bdcode.dao;

import org.apache.hadoop.hbase.client.Mutation;

import java.util.List;

public interface GridIndexTablesDao {

    /**
     * 插入GridIndexTables对象
     * @param rowName
     * @param familyName
     * @param qualifer
     * @param value
     */
    void put(String rowName, String familyName, String qualifer, byte[] value);

    /**
     * 批量加载北斗网格码
     * @param mutationList
     */
    void batchPut(List<Mutation> mutationList);
}
