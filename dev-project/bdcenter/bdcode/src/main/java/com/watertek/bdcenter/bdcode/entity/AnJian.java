package com.watertek.bdcenter.bdcode.entity;

import java.math.BigInteger;
import java.util.Date;

/**
 * 案件类，对应blz_anjian表
 */
public class AnJian {
    private Long id;
    private String anJuanH;
    private String anJianH;
    private String anJianLy;
    private String lianYuanFs;
    private String anJianLx;
    private String daLei;
    private String xiaoLei;
    private String xiLei;
    private String sheQu;
    private BigInteger wangGe;
    private Double lat;
    private Double lon;
    private String jiBie;
    private String poHuaiCd;
    private String yingXiangFw;
    private String miaoShu;
    private String diZhi;
    private String zhaoPian;
    private String shangBaoR;
    private Date shangBaoSj;
    private Long jinPoX;
    private Long chuLi;
    private String lianXiR;
    private String juBaoR;
    private String lianXiFs;
    private String zhuangTai;
    private Long guaHongD;
    private Date fenPaiSj;
    private String zeRenKs;
    private Long zhongYaoX;
    private Date jieZhiSj;
    private Long wanChengSx;

    public Long getWanChengSx() {
        return wanChengSx;
    }

    public void setWanChengSx(Long wanChengSx) {
        this.wanChengSx = wanChengSx;
    }

    public Date getJieZhiSj() {

        return jieZhiSj;
    }

    public void setJieZhiSj(Date jieZhiSj) {
        this.jieZhiSj = jieZhiSj;
    }

    public Long getZhongYaoX() {

        return zhongYaoX;
    }

    public void setZhongYaoX(Long zhongYaoX) {
        this.zhongYaoX = zhongYaoX;
    }

    public String getZeRenKs() {

        return zeRenKs;
    }

    public void setZeRenKs(String zeRenKs) {
        this.zeRenKs = zeRenKs;
    }

    public Date getFenPaiSj() {

        return fenPaiSj;
    }

    public void setFenPaiSj(Date fenPaiSj) {
        this.fenPaiSj = fenPaiSj;
    }

    public Long getGuaHongD() {

        return guaHongD;
    }

    public void setGuaHongD(Long guaHongD) {
        this.guaHongD = guaHongD;
    }

    public String getZhuangTai() {

        return zhuangTai;
    }

    public void setZhuangTai(String zhuangTai) {
        this.zhuangTai = zhuangTai;
    }

    public String getLianXiFs() {

        return lianXiFs;
    }

    public void setLianXiFs(String lianXiFs) {
        this.lianXiFs = lianXiFs;
    }

    public String getJuBaoR() {

        return juBaoR;
    }

    public void setJuBaoR(String juBaoR) {
        this.juBaoR = juBaoR;
    }

    public String getLianXiR() {

        return lianXiR;
    }

    public void setLianXiR(String lianXiR) {
        this.lianXiR = lianXiR;
    }

    public Long getChuLi() {

        return chuLi;
    }

    public void setChuLi(Long chuLi) {
        this.chuLi = chuLi;
    }

    public Long getJinPoX() {

        return jinPoX;
    }

    public void setJinPoX(Long jinPoX) {
        this.jinPoX = jinPoX;
    }

    public Date getShangBaoSj() {

        return shangBaoSj;
    }

    public void setShangBaoSj(Date shangBaoSj) {
        this.shangBaoSj = shangBaoSj;
    }

    public String getShangBaoR() {

        return shangBaoR;
    }

    public void setShangBaoR(String shangBaoR) {
        this.shangBaoR = shangBaoR;
    }

    public String getZhaoPian() {

        return zhaoPian;
    }

    public void setZhaoPian(String zhaoPian) {
        this.zhaoPian = zhaoPian;
    }

    public String getDiZhi() {

        return diZhi;
    }

    public void setDiZhi(String diZhi) {
        this.diZhi = diZhi;
    }

    public String getMiaoShu() {

        return miaoShu;
    }

    public void setMiaoShu(String miaoShu) {
        this.miaoShu = miaoShu;
    }

    public String getYingXiangFw() {

        return yingXiangFw;
    }

    public void setYingXiangFw(String yingXiangFw) {
        this.yingXiangFw = yingXiangFw;
    }

    public String getPoHuaiCd() {

        return poHuaiCd;
    }

    public void setPoHuaiCd(String poHuaiCd) {
        this.poHuaiCd = poHuaiCd;
    }

    public String getJiBie() {

        return jiBie;
    }

    public void setJiBie(String jiBie) {
        this.jiBie = jiBie;
    }

    public Double getLon() {

        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {

        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public BigInteger getWangGe() {

        return wangGe;
    }

    public void setWangGe(BigInteger wangGe) {
        this.wangGe = wangGe;
    }

    public String getSheQu() {

        return sheQu;
    }

    public void setSheQu(String sheQu) {
        this.sheQu = sheQu;
    }

    public String getXiLei() {

        return xiLei;
    }

    public void setXiLei(String xiLei) {
        this.xiLei = xiLei;
    }

    public String getXiaoLei() {

        return xiaoLei;
    }

    public void setXiaoLei(String xiaoLei) {
        this.xiaoLei = xiaoLei;
    }

    public String getDaLei() {

        return daLei;
    }

    public void setDaLei(String daLei) {
        this.daLei = daLei;
    }

    public String getAnJianLx() {

        return anJianLx;
    }

    public void setAnJianLx(String anJianLx) {
        this.anJianLx = anJianLx;
    }

    public String getLianYuanFs() {

        return lianYuanFs;
    }

    public void setLianYuanFs(String lianYuanFs) {
        this.lianYuanFs = lianYuanFs;
    }

    public String getAnJianLy() {

        return anJianLy;
    }

    public void setAnJianLy(String anJianLy) {
        this.anJianLy = anJianLy;
    }

    public String getAnJianH() {

        return anJianH;
    }

    public void setAnJianH(String anJianH) {
        this.anJianH = anJianH;
    }

    public String getAnJuanH() {

        return anJuanH;
    }

    public void setAnJuanH(String anJuanH) {
        this.anJuanH = anJuanH;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
