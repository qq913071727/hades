package com.watertek.bdcenter.bdcode.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 封装了Envelop数组
 */
public class EnvelopResult {
    private List<Envelop> envelops=new ArrayList<Envelop>();

    public List<Envelop> getEnvelops() {
        return envelops;
    }

    public void setEnvelops(List<Envelop> envelops) {
        this.envelops = envelops;
    }
}
