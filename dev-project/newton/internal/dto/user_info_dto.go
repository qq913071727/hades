package dto

// 用户信息
type UserInfoDto struct {
	// id
	Id int
	// 用户名
	Username string
	// token
	Token string
}
