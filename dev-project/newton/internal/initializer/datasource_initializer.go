package initializer

import (
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	iDomain "newton/internal/domain"
	"sync"
)

// 初始化数据源
func InitDataSource() {
	io.Infoln("开始连接数据库")

	var once sync.Once
	var err error
	once.Do(func() {
		//配置MySQL连接参数
		username := "root"   //账号
		password := "123456" //密码
		host := "127.0.0.1"  //数据库地址，可以是Ip或者域名
		port := 3306         //数据库端口
		Dbname := "dracula"  //数据库名
		timeout := "10s"     //连接超时，10秒

		//拼接下dsn参数, dsn格式可以参考上面的语法，这里使用Sprintf动态拼接dsn参数，因为一般数据库连接参数，我们都是保存在配置文件里面，需要从配置文件加载参数，然后拼接dsn。
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local&timeout=%s", username, password, host, port, Dbname, timeout)
		//连接MYSQL, 获得DB类型实例，用于后面的数据库读写操作。
		iDomain.DataSource_.GormDb, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

		//iDomain.DataSource_.GormDb, err = sql.Open("mysql", pDomain.Database_.DriveSourceName)
		//
		sqlDB, _ := iDomain.DataSource_.GormDb.DB()
		sqlDB.SetMaxIdleConns(pDomain.Database_.MaxIdleConnections)
		sqlDB.SetMaxOpenConns(pDomain.Database_.MaxOpenConnections)
	})

	if err != nil {
		io.Fatalf("连接数据库时报错：%s", err.Error())
	} else {
		io.Infoln("数据库连接成功")
	}
}

// 关闭数据库
func CloseDatabase() {
	sqlDB, _ := iDomain.DataSource_.GormDb.DB()
	sqlDB.Close()
}
