package initializer

import (
	"github.com/gin-gonic/gin"
	"newton/internal/controller/impl"
	"newton/internal/domain"
)

// 注册接口
func RegisterApi() {

	/************************************** UserController ***************************************/
	newtonApiV1UserGroup := domain.Router_.Engine_.Group("/newton/api/v1/user")
	// 登录
	newtonApiV1UserGroup.POST("/login", func(c *gin.Context) {
		impl.GetUserController().Login(c)
	})

	/************************************** SystemController ***************************************/
	newtonApiV1SystemGroup := domain.Router_.Engine_.Group("/newton/api/v1/system")
	// 查找system表的全部记录
	newtonApiV1SystemGroup.GET("/findAll", func(c *gin.Context) {
		impl.GetSystemController().FindAll(c)
	})
	// 分页查找（模糊查询）
	newtonApiV1SystemGroup.POST("/page", func(c *gin.Context) {
		impl.GetSystemController().Page(c)
	})
	// 根据系统名称，模糊查找系统
	newtonApiV1SystemGroup.GET("/findByNameLike/:name", func(c *gin.Context) {
		impl.GetSystemController().FindByNameLike(c)
	})
	// 添加系统
	newtonApiV1SystemGroup.POST("/add", func(c *gin.Context) {
		impl.GetSystemController().Add(c)
	})
	// 根据id删除系统
	newtonApiV1SystemGroup.GET("/deleteById/:id", func(c *gin.Context) {
		impl.GetSystemController().DeleteById(c)
	})
	// 根据id修改系统
	newtonApiV1SystemGroup.POST("/updateById", func(c *gin.Context) {
		impl.GetSystemController().UpdateById(c)
	})

	/************************************** EnvironmentController ***************************************/
	newtonApiV1EnvironmentGroup := domain.Router_.Engine_.Group("/newton/api/v1/environment")
	// 查找environment表的全部记录
	newtonApiV1EnvironmentGroup.GET("/findAll", func(c *gin.Context) {
		impl.GetEnvironmentController().FindAll(c)
	})
	// 分页查找（模糊查询）
	newtonApiV1EnvironmentGroup.POST("/page", func(c *gin.Context) {
		impl.GetEnvironmentController().Page(c)
	})
	// 根据环境名称，模糊查找环境
	newtonApiV1EnvironmentGroup.GET("/findByNameLike/:name", func(c *gin.Context) {
		impl.GetEnvironmentController().FindByNameLike(c)
	})
	// 添加环境
	newtonApiV1EnvironmentGroup.POST("/add", func(c *gin.Context) {
		impl.GetEnvironmentController().Add(c)
	})
	// 根据id删除环境
	newtonApiV1EnvironmentGroup.GET("/deleteById/:id", func(c *gin.Context) {
		impl.GetEnvironmentController().DeleteById(c)
	})
	// 根据id修改环境
	newtonApiV1EnvironmentGroup.POST("/updateById", func(c *gin.Context) {
		impl.GetEnvironmentController().UpdateById(c)
	})
}
