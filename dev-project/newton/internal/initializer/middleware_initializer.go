package initializer

import (
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/middleware"
	"newton/internal/domain"
)

// 初始化中间件
func InitMiddleware() {
	io.Infoln("初始化中间件")

	// 初始化中间件
	domain.Router_.Engine_.Use(middleware.Cors())
}
