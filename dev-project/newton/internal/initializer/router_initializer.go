package initializer

import (
	"anubis-framework/pkg/io"
	"github.com/gin-gonic/gin"
	"newton/internal/domain"
)

// 初始化Router
func InitRouter() {
	io.Infoln("初始化Router")

	// 创建路由
	domain.Router_.Engine_ = gin.Default()
}
