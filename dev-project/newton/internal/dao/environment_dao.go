package dao

import (
	"anubis-framework/pkg/domain"
	"newton/internal/model"
	"newton/internal/vo"
)

type EnvironmentDao interface {
	// 查找environment表的全部记录
	FindAll() (*[]model.Environment, error)

	// 分页查找（模糊查询）
	Page(pageVo *vo.PageVo) (*domain.Pager, error)

	// 根据环境名称，模糊查询系统
	FindByNameLike(name string) (*[]model.Environment, error)

	// 根据h环境名称，精确查找系统
	FindByName(name string) (*model.Environment, error)

	// 添加环境
	Add(pageVo *vo.PageVo) (bool, error)

	// 根据id删除环境
	DeleteById(id int) (bool, error)

	// 根据id修改环境
	UpdateById(environment *model.Environment) (bool, error)
}
