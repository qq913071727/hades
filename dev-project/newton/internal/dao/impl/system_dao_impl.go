package impl

import (
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"gorm.io/gorm"
	"newton/internal/domain"
	"newton/internal/model"
	"newton/internal/vo"
)

type SystemDaoImpl struct {
	db *gorm.DB
}

// 返回dao实现类
func GetSystemDaoImpl() *SystemDaoImpl {
	return &SystemDaoImpl{domain.DataSource_.GormDb}
}

// 查找system表的全部记录
func (_systemDaoImpl *SystemDaoImpl) FindAll() (*[]model.System, error) {
	io.Infoln("查找system表的全部记录")

	var resultSystemArray []model.System
	_systemDaoImpl.db.Raw("select * from system t").Scan(&resultSystemArray)
	return &resultSystemArray, nil
}

// 分页查找（模糊查询）
func (_systemDaoImpl *SystemDaoImpl) Page(pageVo *vo.PageVo) (*pDomain.Pager, error) {
	io.Infoln("分页查找system表中的记录")

	var pager pDomain.Pager
	var resultSystemArray []model.System
	var total int64
	_systemDaoImpl.db.Model(&model.System{}).Count(&total)
	_systemDaoImpl.db.Where("name LIKE ?", "%"+pageVo.Name+"%").Limit(pageVo.PageSize).Offset((pageVo.PageNo - 1) * pageVo.PageSize).Find(&resultSystemArray)
	pager.List = &resultSystemArray
	pager.Total = int(total)
	return &pager, nil
}

// 根据系统名称，模糊查询系统
func (_systemDaoImpl *SystemDaoImpl) FindByNameLike(name string) (*[]model.System, error) {
	io.Infoln("根据系统名称，模糊查询系统")

	var resultSystemArray []model.System
	_systemDaoImpl.db.Raw("select * from system t where t.name like ?", "%"+name+"%").Scan(&resultSystemArray)
	return &resultSystemArray, nil
}

// 根据系统名称，精确查找系统
func (_systemDaoImpl *SystemDaoImpl) FindByName(name string) (*model.System, error) {
	io.Infoln("根据系统名称，精确查找系统")

	var resultSystem model.System
	_systemDaoImpl.db.Raw("select * from system t where t.name = ?", name).Scan(&resultSystem)
	return &resultSystem, nil
}

// 添加系统
func (_systemDaoImpl *SystemDaoImpl) Add(pageVo *vo.PageVo) (bool, error) {
	io.Infoln("添加系统")

	var system model.System
	system.Name = pageVo.Name
	_systemDaoImpl.db.Model(&model.System{}).Create(&system)
	return true, nil
}

// 根据id删除系统
func (_systemDaoImpl *SystemDaoImpl) DeleteById(id int) (bool, error) {
	io.Infoln("根据id删除系统")

	_systemDaoImpl.db.Delete(&model.System{}, "id = ?", id)
	return true, nil
}

// 根据id修改系统
func (_systemDaoImpl *SystemDaoImpl) UpdateById(system *model.System) (bool, error) {
	io.Infoln("根据id修改系统")

	_systemDaoImpl.db.Model(&model.System{}).Where("id = ?", system.Id).Update("name", system.Name)
	return true, nil
}
