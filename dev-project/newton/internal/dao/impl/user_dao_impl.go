package impl

import (
	"anubis-framework/pkg/io"
	"gorm.io/gorm"
	"newton/internal/domain"
	"newton/internal/model"
)

type UserDaoImpl struct {
	db *gorm.DB
}

// 返回dao实现类
func GetUserDaoImpl() *UserDaoImpl {
	return &UserDaoImpl{domain.DataSource_.GormDb}
}

// 根据用户名查找user表的记录
func (_userDaoImpl *UserDaoImpl) FindByUsername(username string) (*model.User, error) {
	io.Infoln("根据用户名[%s]查找user表的记录", username)

	var resultUser model.User
	_userDaoImpl.db.Raw("select * from user t where t.username=?",
		username).Scan(&resultUser)
	return &resultUser, nil
}
