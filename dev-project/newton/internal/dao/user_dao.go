package dao

import "newton/internal/model"

type UserDao interface {
	// 根据用户名查找user表的记录
	FindByUsername(username string) (*model.User, error)
}
