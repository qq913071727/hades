package dao

import (
	"anubis-framework/pkg/domain"
	"newton/internal/model"
	"newton/internal/vo"
)

type SystemDao interface {
	// 查找system表的全部记录
	FindAll() (*[]model.System, error)

	// 分页查找（模糊查询）
	Page(pageVo *vo.PageVo) (*domain.Pager, error)

	// 根据系统名称，模糊查询系统
	FindByNameLike(name string) (*[]model.System, error)

	// 根据系统名称，精确查找系统
	FindByName(name string) (*model.System, error)

	// 添加系统
	Add(pageVo *vo.PageVo) (bool, error)

	// 根据id删除系统
	DeleteById(id int) (bool, error)

	// 根据id修改系统
	UpdateById(system *model.System) (bool, error)
}
