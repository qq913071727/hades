package job

import (
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"fmt"
	"newton/internal/domain"
)

// 启动服务器
func RunServer() {
	// 启动服务器
	domain.Router_.Engine_.Run(fmt.Sprintf(":%s", pDomain.Application_.Port))
	io.Infoln("启动服务器在%s端口启动", pDomain.Application_.Port)
}
