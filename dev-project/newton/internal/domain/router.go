package domain

import "github.com/gin-gonic/gin"

type Router struct {
	Engine_ *gin.Engine
}

var Router_ = &Router{}
