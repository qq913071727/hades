package vo

type PageVo struct {
	PageNo   int
	PageSize int
	Name     string
}
