package service

import (
	pDomain "anubis-framework/pkg/domain"
	"newton/internal/model"
)

type UserService interface {

	// 登录
	Login(user *model.User) *pDomain.ServiceResult
}
