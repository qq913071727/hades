package service

import (
	pDomain "anubis-framework/pkg/domain"
	"newton/internal/model"
	"newton/internal/vo"
)

type EnvironmentService interface {

	// 查找所有环境
	FindAll() *pDomain.ServiceResult

	// 分页查找（模糊查询）
	Page(pageVo *vo.PageVo) *pDomain.ServiceResult

	// 根据环境名称，模糊查询环境
	FindByNameLike(name string) *pDomain.ServiceResult

	// 添加系统
	Add(pageVo *vo.PageVo) *pDomain.ServiceResult

	// 根据id删除环境
	DeleteById(id int) *pDomain.ServiceResult

	// 根据id修改环境
	UpdateById(environment *model.Environment) *pDomain.ServiceResult
}
