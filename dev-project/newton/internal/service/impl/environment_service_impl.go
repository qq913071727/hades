package impl

import (
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
	"newton/internal/dao"
	"newton/internal/dao/impl"
	"newton/internal/model"
	"newton/internal/vo"
)

type EnvironmentServiceImpl struct {
	_environmentDaoImpl dao.EnvironmentDao
}

// 获取service的实现类
func GetEnvironmentServiceImpl() *EnvironmentServiceImpl {
	return &EnvironmentServiceImpl{impl.GetEnvironmentDaoImpl()}
}

// 查找所有环境
func (_environmentServiceImpl EnvironmentServiceImpl) FindAll() *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	resultEnvironmentArray, _ := _environmentServiceImpl._environmentDaoImpl.FindAll()

	serviceResult.Code = pConstants.SelectSuccessCode
	serviceResult.Message = pConstants.SelectSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = resultEnvironmentArray
	return serviceResult
}

// 分页查找（模糊查询）
func (_environmentServiceImpl EnvironmentServiceImpl) Page(pageVo *vo.PageVo) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	pager, _ := _environmentServiceImpl._environmentDaoImpl.Page(pageVo)

	serviceResult.Code = pConstants.SelectSuccessCode
	serviceResult.Message = pConstants.SelectSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = pager
	return serviceResult
}

// 根据环境名称，模糊查询环境
func (_environmentServiceImpl EnvironmentServiceImpl) FindByNameLike(name string) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	resultEnvironmentArray, _ := _environmentServiceImpl._environmentDaoImpl.FindByNameLike(name)

	serviceResult.Code = pConstants.SelectSuccessCode
	serviceResult.Message = pConstants.SelectSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = resultEnvironmentArray
	return serviceResult
}

// 添加环境
func (_environmentServiceImpl EnvironmentServiceImpl) Add(pageVo *vo.PageVo) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}

	system, error := _environmentServiceImpl._environmentDaoImpl.FindByName(pageVo.Name)
	if error == nil && util.IsEmptyStruct(*system) {
		result, _ := _environmentServiceImpl._environmentDaoImpl.Add(pageVo)
		serviceResult.Code = pConstants.InsertSuccessCode
		serviceResult.Message = pConstants.InsertSuccessMessage
		serviceResult.Success = true
		serviceResult.Result = result
	} else {
		serviceResult.Code = pConstants.InsertExistsCode
		serviceResult.Message = pConstants.InsertExistsMessage
		serviceResult.Success = false
	}

	return serviceResult
}

// 根据id删除环境
func (_environmentServiceImpl EnvironmentServiceImpl) DeleteById(id int) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}

	result, _ := _environmentServiceImpl._environmentDaoImpl.DeleteById(id)
	serviceResult.Code = pConstants.InsertSuccessCode
	serviceResult.Message = pConstants.InsertSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = result

	return serviceResult
}

// 根据id修改系统
func (_environmentServiceImpl EnvironmentServiceImpl) UpdateById(environment *model.Environment) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}

	result, _ := _environmentServiceImpl._environmentDaoImpl.UpdateById(environment)
	serviceResult.Code = pConstants.InsertSuccessCode
	serviceResult.Message = pConstants.InsertSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = result

	return serviceResult
}
