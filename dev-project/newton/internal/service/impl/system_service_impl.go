package impl

import (
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
	"newton/internal/dao"
	"newton/internal/dao/impl"
	"newton/internal/model"
	"newton/internal/vo"
)

type SystemServiceImpl struct {
	_systemDaoImpl dao.SystemDao
}

// 获取service的实现类
func GetSystemServiceImpl() *SystemServiceImpl {
	return &SystemServiceImpl{impl.GetSystemDaoImpl()}
}

// 查找所有系统
func (_systemServiceImpl SystemServiceImpl) FindAll() *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	resultSystemArray, _ := _systemServiceImpl._systemDaoImpl.FindAll()

	serviceResult.Code = pConstants.SelectSuccessCode
	serviceResult.Message = pConstants.SelectSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = resultSystemArray
	return serviceResult
}

// 分页查找（模糊查询）
func (_systemServiceImpl SystemServiceImpl) Page(pageVo *vo.PageVo) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	pager, _ := _systemServiceImpl._systemDaoImpl.Page(pageVo)

	serviceResult.Code = pConstants.SelectSuccessCode
	serviceResult.Message = pConstants.SelectSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = pager
	return serviceResult
}

// 根据系统名称，模糊查询系统
func (_systemServiceImpl SystemServiceImpl) FindByNameLike(name string) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	resultSystemArray, _ := _systemServiceImpl._systemDaoImpl.FindByNameLike(name)

	serviceResult.Code = pConstants.SelectSuccessCode
	serviceResult.Message = pConstants.SelectSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = resultSystemArray
	return serviceResult
}

// 添加系统
func (_systemServiceImpl SystemServiceImpl) Add(pageVo *vo.PageVo) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}

	system, error := _systemServiceImpl._systemDaoImpl.FindByName(pageVo.Name)
	if error == nil && util.IsEmptyStruct(*system) {
		result, _ := _systemServiceImpl._systemDaoImpl.Add(pageVo)
		serviceResult.Code = pConstants.InsertSuccessCode
		serviceResult.Message = pConstants.InsertSuccessMessage
		serviceResult.Success = true
		serviceResult.Result = result
	} else {
		serviceResult.Code = pConstants.InsertExistsCode
		serviceResult.Message = pConstants.InsertExistsMessage
		serviceResult.Success = false
	}

	return serviceResult
}

// 根据id删除系统
func (_systemServiceImpl SystemServiceImpl) DeleteById(id int) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}

	result, _ := _systemServiceImpl._systemDaoImpl.DeleteById(id)
	serviceResult.Code = pConstants.InsertSuccessCode
	serviceResult.Message = pConstants.InsertSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = result

	return serviceResult
}

// 根据id修改系统
func (_systemServiceImpl SystemServiceImpl) UpdateById(system *model.System) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}

	result, _ := _systemServiceImpl._systemDaoImpl.UpdateById(system)
	serviceResult.Code = pConstants.InsertSuccessCode
	serviceResult.Message = pConstants.InsertSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = result

	return serviceResult
}
