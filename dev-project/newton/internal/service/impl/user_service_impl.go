package impl

import (
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
	"newton/internal/dao"
	"newton/internal/dao/impl"
	"newton/internal/model"
)

type UserServiceImpl struct {
	_userDaoImpl dao.UserDao
}

// 获取service的实现类
func GetUserServiceImpl() *UserServiceImpl {
	return &UserServiceImpl{impl.GetUserDaoImpl()}
}

// 登录
func (_userServiceImpl UserServiceImpl) Login(user *model.User) *pDomain.ServiceResult {
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	resultUser, _ := _userServiceImpl._userDaoImpl.FindByUsername(user.Username)

	// 没有查到记录
	if util.IsEmptyStruct(*resultUser) == true {
		serviceResult.Code = pConstants.SelectFailCode
		serviceResult.Message = pConstants.SelectFailMessage
		serviceResult.Success = false
		return serviceResult
	}

	var md5Password string = util.Md5Password(user.Password, resultUser.Salt)
	if md5Password == resultUser.Password {
		serviceResult.Code = pConstants.SelectSuccessCode
		serviceResult.Message = pConstants.SelectSuccessMessage
		serviceResult.Success = true
		serviceResult.Result = resultUser
		return serviceResult
	} else {
		serviceResult.Code = pConstants.SelectFailCode
		serviceResult.Message = pConstants.SelectFailMessage
		serviceResult.Success = false
		return serviceResult
	}
}
