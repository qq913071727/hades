package service

import (
	pDomain "anubis-framework/pkg/domain"
	"newton/internal/model"
	"newton/internal/vo"
)

type SystemService interface {

	// 查找所有系统
	FindAll() *pDomain.ServiceResult

	// 分页查找（模糊查询）
	Page(pageVo *vo.PageVo) *pDomain.ServiceResult

	// 根据系统名称，模糊查询系统
	FindByNameLike(name string) *pDomain.ServiceResult

	// 添加系统
	Add(pageVo *vo.PageVo) *pDomain.ServiceResult

	// 根据id删除系统
	DeleteById(id int) *pDomain.ServiceResult

	// 根据id修改系统
	UpdateById(system *model.System) *pDomain.ServiceResult
}
