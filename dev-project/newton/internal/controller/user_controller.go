package controller

import "github.com/gin-gonic/gin"

type UserController interface {
	// 登录
	Login(c *gin.Context)
}
