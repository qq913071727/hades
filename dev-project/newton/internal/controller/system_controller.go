package controller

import "github.com/gin-gonic/gin"

type SystemController interface {
	// 查找system表的全部记录
	FindAll(c *gin.Context)

	// 分页查找（模糊查询）
	Page(c *gin.Context)

	// 根据系统名称，模糊查询系统
	FindByNameLike(c *gin.Context)

	// 添加系统
	Add(c *gin.Context)

	// 根据id删除系统
	DeleteById(c *gin.Context)

	// 根据id修改系统
	UpdateById(c *gin.Context)
}
