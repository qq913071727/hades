package controller

import "github.com/gin-gonic/gin"

type EnvironmentController interface {
	// 查找environment表的全部记录
	FindAll(c *gin.Context)

	// 分页查找（模糊查询）
	Page(c *gin.Context)

	// 根据环境名称，模糊查询环境
	FindByNameLike(c *gin.Context)

	// 添加环境
	Add(c *gin.Context)

	// 根据id删除环境
	DeleteById(c *gin.Context)

	// 根据id修改环境
	UpdateById(c *gin.Context)
}
