package impl

import (
	"anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"github.com/gin-gonic/gin"
	"net/http"
	"newton/internal/model"
	"newton/internal/service"
	"newton/internal/service/impl"
	"newton/internal/vo"
	"strconv"
)

type SystemController struct {
	_systemServiceImpl service.SystemService
}

// 获取SystemController
func GetSystemController() *SystemController {
	return &SystemController{impl.GetSystemServiceImpl()}
}

// 查找system表的全部记录
func (_systemController *SystemController) FindAll(c *gin.Context) {
	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	var serviceResult *pDomain.ServiceResult = _systemController._systemServiceImpl.FindAll()
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Success = serviceResult.Success
	apiResult.Result = serviceResult.Result

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 分页查找（模糊查询）
func (_systemController *SystemController) Page(c *gin.Context) {
	var pageVo *vo.PageVo = &vo.PageVo{}
	c.BindJSON(pageVo)

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	if pageVo.PageNo == 0 || pageVo.PageSize == 0 {
		apiResult.Code = constants.ParameterCannotBeNilCode
		apiResult.Message = constants.ParameterCannotBeNilMessage
		apiResult.Success = false
	} else {
		var serviceResult *pDomain.ServiceResult = _systemController._systemServiceImpl.Page(pageVo)
		apiResult.Code = serviceResult.Code
		apiResult.Message = serviceResult.Message
		apiResult.Success = serviceResult.Success
		apiResult.Result = serviceResult.Result
	}

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 根据系统名称，模糊查询系统
func (_systemController *SystemController) FindByNameLike(c *gin.Context) {
	var name string = c.Param("name")

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	if name == "" {
		apiResult.Code = constants.ParameterCannotBeNilCode
		apiResult.Message = constants.ParameterCannotBeNilMessage
		apiResult.Success = false
	} else {
		var serviceResult *pDomain.ServiceResult = _systemController._systemServiceImpl.FindByNameLike(name)
		apiResult.Code = serviceResult.Code
		apiResult.Message = serviceResult.Message
		apiResult.Success = serviceResult.Success
		apiResult.Result = serviceResult.Result
	}

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 添加系统
func (_systemController *SystemController) Add(c *gin.Context) {
	var pageVo *vo.PageVo = &vo.PageVo{}
	c.BindJSON(pageVo)

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	if pageVo.Name == "" {
		apiResult.Code = constants.ParameterCannotBeNilCode
		apiResult.Message = constants.ParameterCannotBeNilMessage
		apiResult.Success = false
	} else {
		var serviceResult *pDomain.ServiceResult = _systemController._systemServiceImpl.Add(pageVo)
		apiResult.Code = serviceResult.Code
		apiResult.Message = serviceResult.Message
		apiResult.Success = serviceResult.Success
		apiResult.Result = serviceResult.Result
	}

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 根据id删除系统
func (_systemController *SystemController) DeleteById(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	var serviceResult *pDomain.ServiceResult = _systemController._systemServiceImpl.DeleteById(id)
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Success = serviceResult.Success
	apiResult.Result = serviceResult.Result

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 根据id修改系统
func (_systemController *SystemController) UpdateById(c *gin.Context) {
	var system *model.System = &model.System{}
	c.BindJSON(system)

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	var serviceResult *pDomain.ServiceResult = _systemController._systemServiceImpl.UpdateById(system)
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Success = serviceResult.Success
	apiResult.Result = serviceResult.Result

	c.JSON(http.StatusOK, *apiResult)
	return
}
