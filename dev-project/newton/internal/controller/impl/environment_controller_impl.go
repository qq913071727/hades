package impl

import (
	"anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"github.com/gin-gonic/gin"
	"net/http"
	"newton/internal/model"
	"newton/internal/service"
	"newton/internal/service/impl"
	"newton/internal/vo"
	"strconv"
)

type EnvironmentController struct {
	_environmentServiceImpl service.EnvironmentService
}

// 获取EnvironmentController
func GetEnvironmentController() *EnvironmentController {
	return &EnvironmentController{impl.GetEnvironmentServiceImpl()}
}

// 查找environment表的全部记录
func (_environmentController *EnvironmentController) FindAll(c *gin.Context) {
	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	var serviceResult *pDomain.ServiceResult = _environmentController._environmentServiceImpl.FindAll()
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Success = serviceResult.Success
	apiResult.Result = serviceResult.Result

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 分页查找（模糊查询）
func (_environmentController *EnvironmentController) Page(c *gin.Context) {
	var pageVo *vo.PageVo = &vo.PageVo{}
	c.BindJSON(pageVo)

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	if pageVo.PageNo == 0 || pageVo.PageSize == 0 {
		apiResult.Code = constants.ParameterCannotBeNilCode
		apiResult.Message = constants.ParameterCannotBeNilMessage
		apiResult.Success = false
	} else {
		var serviceResult *pDomain.ServiceResult = _environmentController._environmentServiceImpl.Page(pageVo)
		apiResult.Code = serviceResult.Code
		apiResult.Message = serviceResult.Message
		apiResult.Success = serviceResult.Success
		apiResult.Result = serviceResult.Result
	}

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 根据环境名称，模糊查询环境
func (_environmentController *EnvironmentController) FindByNameLike(c *gin.Context) {
	var name string = c.Param("name")

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	if name == "" {
		apiResult.Code = constants.ParameterCannotBeNilCode
		apiResult.Message = constants.ParameterCannotBeNilMessage
		apiResult.Success = false
	} else {
		var serviceResult *pDomain.ServiceResult = _environmentController._environmentServiceImpl.FindByNameLike(name)
		apiResult.Code = serviceResult.Code
		apiResult.Message = serviceResult.Message
		apiResult.Success = serviceResult.Success
		apiResult.Result = serviceResult.Result
	}

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 添加环境
func (_environmentController *EnvironmentController) Add(c *gin.Context) {
	var pageVo *vo.PageVo = &vo.PageVo{}
	c.BindJSON(pageVo)

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	if pageVo.Name == "" {
		apiResult.Code = constants.ParameterCannotBeNilCode
		apiResult.Message = constants.ParameterCannotBeNilMessage
		apiResult.Success = false
	} else {
		var serviceResult *pDomain.ServiceResult = _environmentController._environmentServiceImpl.Add(pageVo)
		apiResult.Code = serviceResult.Code
		apiResult.Message = serviceResult.Message
		apiResult.Success = serviceResult.Success
		apiResult.Result = serviceResult.Result
	}

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 根据id删除环境
func (_environmentController *EnvironmentController) DeleteById(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	var serviceResult *pDomain.ServiceResult = _environmentController._environmentServiceImpl.DeleteById(id)
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Success = serviceResult.Success
	apiResult.Result = serviceResult.Result

	c.JSON(http.StatusOK, *apiResult)
	return
}

// 根据id修改环境
func (_environmentController *EnvironmentController) UpdateById(c *gin.Context) {
	var environment *model.Environment = &model.Environment{}
	c.BindJSON(environment)

	var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
	var serviceResult *pDomain.ServiceResult = _environmentController._environmentServiceImpl.UpdateById(environment)
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Success = serviceResult.Success
	apiResult.Result = serviceResult.Result

	c.JSON(http.StatusOK, *apiResult)
	return
}
