package model

// 环境表
type Environment struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

// 返回表名
func (environment *Environment) TableName() string {
	return "ENVIRONMENT"
}
