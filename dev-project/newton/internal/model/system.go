package model

// 系统表
type System struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

// 返回表名
func (system *System) TableName() string {
	return "SYSTEM"
}
