package model

// 用户表
type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Salt     string `json:"salt"`
}

// 返回表名
func (user *User) TableName() string {
	return "USER"
}
