# 创建工程，生成go.mod文件
go mod init GO_WORKPLACE

# 运行文件
go run main.go

# 安装gin
go get github.com/gin-gonic/gin

# 安装
go get gopkg.in/ini.v1