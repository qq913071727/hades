package main

import (
	pInitializer "anubis-framework/pkg/initializer"
	"anubis-framework/pkg/io"
	"flag"
	"newton/internal/constants"
	iInitializer "newton/internal/initializer"
	"newton/internal/job"
)

func init() {
	pInitializer.InitLogConfig("E:/github-repository/log/newton.log")
	pInitializer.InitApplicationConfig()
	pInitializer.InitDatabaseConfig()
	iInitializer.InitDataSource()
	iInitializer.InitRouter()
	iInitializer.InitMiddleware()
	iInitializer.RegisterApi()
}

func main() {
	defer func() {
		if err := recover(); err != nil {
			io.Fatalf("error：", err)
		}
		io.Infoln("程序结束")
	}()

	// 解析命令行参数
	jobName := flag.String("startupType", constants.Server, "任务名称")
	flag.Parse()
	io.Infoln("任务名称：%s", *jobName)

	switch *jobName {
	case constants.Server:
		// 启动服务器
		job.RunServer()
	}

	defer iInitializer.CloseDatabase()
}
