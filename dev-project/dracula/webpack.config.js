const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');

const isProduction = process.env.NODE_ENV === 'production'; // 根据环境变量，判断当前是否为生产模式。

module.exports = {
	entry: './src/main.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: "dracula.js"
	},
	// 根据环境变量决定 mode 的值
	mode: isProduction ? 'production' : 'development',
	module: {
		rules: [
			// 将ES6编译为ES5
			{
				test: /\.js$/,
				loader: "babel-loader",
				exclude: /node_modules/
			},
			// css加载器
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			// url-loader解决图片较多时过多http请求导致页面性能降低的问题，将引入的图片编码，生成dataURl。相当于把图片数据翻译成一串字符,再把这串字符打包到文件中，最终只需要引入这个文件就能访问图片了。
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					outputPath: 'imag/',
					publicPath: '/imag/',
					esModule: false
				}
			},
			{
				test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000
				}
			},
			{ test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader',] },
			// 加载VUE
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
				test: /.less$/,
				use: [
					'style-loader',
					'css-loader',
					'less-loader'
				]
			}
		]
	},
	// 将vue切换为运行时+编译的版本
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': resolve('/src')
		}
	},
	plugins: [
		// 复制index.html
		...(!isProduction ? [new HtmlWebpackPlugin({
			title: "vue demo",
			template: "public/index.html"
		})] : []),
		// 加载VUE
		new VueLoaderPlugin(),
		new webpack.DefinePlugin({
			'process.env.API_URL': JSON.stringify("http://localhost:8080/newton"),
		}),
	],
	devServer: {
		// 配置从目录提供静态文件的选项（默认是'public'文件夹）
		static: path.resolve(__dirname, 'static'),
		open: {
			// 设置打开的页面
			target: ['/'],
			// 设置打开的浏览器的配置
			app: {
				name: 'chrome',
			}
		},
		hot: true,
		port: 8888,
	}
}

/**
 * 解析路径
 */
function resolve(dir) {
	return path.join(__dirname, dir);
}
