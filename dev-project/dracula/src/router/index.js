/**
 * 路由定义
 */
import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../views/layout/index'
import Home from '../views/home/index'
import Login from '../views/login/index'
import ConfigurationSystem from '../views/configuration/system/index'
import ConfigurationEnvironment from '../views/configuration/environment/index'
import ConfigurationService from '../views/configuration/service/index'
import ConfigurationModel from '../views/configuration/model/index'
import ConfigurationApi from '../views/configuration/api/index'
import User from '../views/User'
import All from '../views/All'
import UserPrefix from '../views/UserPrefix'
import Profile from '../views/Profile'
import Posts from '../views/Posts'

Vue.use(Router);

const News = {
	props: ['id'],
	template: '<div>News {{ id }}</div>'
}

// 此Router是自己自定义引入暴露出来的，即是自定义的，以下的Router同样是这样
// 解决两次访问相同路由地址报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
}

export default new Router({
	mode: 'history',
	routes: [
		// 登录页面
		{
			path: "/login",
			name: "login",
			component: Login,
			children: [
				{
					// 不需要表现为路径，他会自动补全
					path: "profile",
					component: Profile
				},
				{
					path: "posts",
					component: Posts
				}
			]
		},
		// 菜单栏
		{
			path: "/",
			name: "layout",
			component: Layout,
			children: [
				// 首页
				{
					path: "/home",
					name: 'home',
					component: Home,
					meta: { title: '首页' }
				},
				// 配置管理
				{
					// 配置管理-->系统
					path: "/configuration/system",
					name: 'configurationSystem',
					component: ConfigurationSystem,
					meta: { title: '系统' }
				},
				{
					// 配置管理-->环境
					path: "/configuration/environment",
					name: 'configurationEnvironment',
					component: ConfigurationEnvironment,
					meta: { title: '环境' }
				},
				{
					// 配置管理-->服务
					path: "/configuration/service",
					name: 'configurationService',
					component: ConfigurationService,
					meta: { title: '服务' }
				},
				{
					// 配置管理-->模块
					path: "/configuration/model",
					name: 'configurationModel',
					component: ConfigurationModel,
					meta: { title: '模块' }
				},
				{
					// 配置管理-->接口
					path: "/configuration/api",
					name: 'configurationApi',
					component: ConfigurationApi,
					meta: { title: '接口' }
				},
			]
		},




		// 下面是无用的
		{
			path: "/user/:username",
			name: "user",
			component: User
		},
		{
			path: "/userPrefix",
			name: "userPrefix",
			component: UserPrefix,
			beforeEnter: (to, from, next) => {
				console.debug(`beforeEnter方法中，从页面${from.path}跳转至${to.path}`);
			}
		},
		{
			path: "/redirectUserPrefix",
			redirect: '/userPrefix'
		},
		// 对于包含命名视图的路由，你必须分别为每个命名视图添加 `props` 选项：
		{
			path: '/news/:id',
			components: { default: News, a: Profile },
			props: { default: true, Profile: false }
		}
		// //  {
		// // 	 path: "*",
		// // 	 name: "all",
		// // 	 component: All
		// // 	},
		// {
		// 	path: "/user-*",
		// 	name: "userPrefix",
		// 	component: UserPrefix
		//  },

	]
});