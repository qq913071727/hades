/**
 * 路由拦截器
 */
import router from './index'
import localStorageKey from '../components/local-storage-key'

router.beforeEach((to, from, next) => {
  const user = JSON.parse(localStorage.getItem(localStorageKey.keySet.userInfo));
  console.debug(`router.beforeEach方法中，从页面${from.path}跳转至${to.path}，header中的用户信息是${JSON.stringify(user)}`);

  if (!user && to.path != '/login') {
    next('/login');
  } else {
    next();
  }
});

router.afterEach((to, from) => {
  console.debug(`router.afterEach方法中，从页面${from.path}跳转至${to.path}`);
});