import Vue from 'vue'
import './assets/css/main.css'
import App from './App'
import MetaInfo from 'vue-meta-info'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Antd from 'ant-design-vue'
import './router/navigation-guard'
import 'ant-design-vue/dist/antd.css'
import * as Icons from '@ant-design/icons-vue'
import KeyValueInput from 'poseidon/KeyValueInput.esm.js'

Vue.use(MetaInfo)
Vue.use(Antd)
Vue.use(ElementUI);
Vue.component('key-value-input', KeyValueInput)

Vue.config.productionTip = false

new Vue({
	router,
	render: h => h(App)
}).$mount("#app");
