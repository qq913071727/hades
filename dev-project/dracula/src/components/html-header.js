/**
 * html中的属性
 */

/**
 * vue单页应用实现优化seo区分tdk
 */
const tdk = {
	title: 'dracula',
	meta: [{
		charset: "utf-8"
	},
	{
		name: "viewport",
		content: "width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
	}]
}


export default {
	tdk
}