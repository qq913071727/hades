/**
 * html中的method属性
 */
const method = {
	post: 'POST',
	get: 'GET',
	update: 'UPDATE',
	delete: 'DELETE',
}


export default {
	method
}