/**
 * localStorage中的key集合
 */
const keySet = {
  /**
   * 用于存储用户信息
   */
  userInfo: 'userInfo'
}

export default {
	keySet
}