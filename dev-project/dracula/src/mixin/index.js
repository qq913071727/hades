export const mixins = {
    data() {
        return {};
    },
    computed: {},
    created() {
        window.addEventListener("keydown", this.handleKeyCode, true);
    },
    beforeDestroy() {
        console.log("beforeDestroy");
        window.removeEventListener("keydown", this.handleKeyCode);
    },
    mounted() { },
    methods: {
        handleKeyCode(e) {
            if (e.keyCode === 13) {
                this.handle("search");
            }
        },
    },
};