import request from "@/utils/request";
import httpMethod from "@/components/http-method.js";

/******************************* 配置-系统 *********************************/
/**
 * 添加系统
 */
export async function add(data) {
  return request({
    url: "/api/v1/system/add",
    method: httpMethod.method.post,
    data
  })
};
/**
 * 根据id删除系统
 * @param {*} id 
 * @returns 
 */
export async function deleteById(id) {
  return request({
    url: `/api/v1/system/deleteById/${id}`,
    method: httpMethod.method.get,
    data: null
  })
};
/**
 * 根据id修改系统
 * @param {*} data 
 * @returns 
 */
 export async function updateById(data) {
  return request({
    url: `/api/v1/system/updateById`,
    method: httpMethod.method.post,
    data
  })
};
/**
 * 查找所有系统
 */
export async function findAll() {
  return request({
    url: `/api/v1/system/findAll`,
    method: httpMethod.method.get,
    data: null,
  })
};
/**
 * 分页查找系统记录
 * @param {*} param 
 * @returns 
 */
export async function page(data) {
  return request({
    url: `/api/v1/system/page`,
    method: httpMethod.method.post,
    data,
  })
};
/**
 * 根据系统名称，模糊查询系统
 * @param {*} name 
 * @returns 
 */
export async function findByNameLike(name) {
  return request({
    url: `/api/v1/system/findByNameLike/${name}`,
    method: httpMethod.method.get,
    data: null,
  })
};

