import request from "@/utils/request";
import httpMethod from "@/components/http-method.js";

/******************************* 配置-环境 *********************************/
/**
 * 添加环境
 */
export async function add(data) {
  return request({
    url: "/api/v1/environment/add",
    method: httpMethod.method.post,
    data
  })
};
/**
 * 根据id删除环境
 * @param {*} id 
 * @returns 
 */
export async function deleteById(id) {
  return request({
    url: `/api/v1/environment/deleteById/${id}`,
    method: httpMethod.method.get,
    data: null
  })
};
/**
 * 根据id修改环境
 * @param {*} data 
 * @returns 
 */
 export async function updateById(data) {
  return request({
    url: `/api/v1/environment/updateById`,
    method: httpMethod.method.post,
    data
  })
};
/**
 * 分页查找环境记录
 * @param {*} param 
 * @returns 
 */
export async function page(data) {
  return request({
    url: `/api/v1/environment/page`,
    method: httpMethod.method.post,
    data,
  })
};


