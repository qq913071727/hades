import request from '@/utils/request';

/**
 * 登录
 */
export function login(username, password) {
  const data = {
    "Username": username,
    "Password": password,
  }
  return request({
    url: '/api/v1/user/login',
    method: 'post',
    data: data
  })
}