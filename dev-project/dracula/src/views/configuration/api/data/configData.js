
/**
 * 下拉列表中的第一项，也是默认被选择的项
 */
export const defaultSelect_ = {id: 0, name: '全部'};

export const systemArray_ = [
	{id: 1, name: '即时通信'},
	{id: 2, name: '远大支持论坛'},
	{id: 3, name: 'b1b产品数据导入'},
	{id: 4, name: '供应商协同'},
	{id: 5, name: '客户协同'},
	{id: 6, name: '监控系统'},
];

export const environmentArray_ = [
	{id: 1, name: '开发环境'},
	{id: 2, name: '测试环境'},
	{id: 3, name: '生产环境'},
];

export const serviceArray_ = [
	{id: 1, name: 'im-end-web'},
	{id: 1, name: 'im-end-data'},
];

export const tableColumns = [
	{
		title: '序号',
		align: "center",
		dataIndex: 'sequence',
		width: '5%',
		scopedSlots: { customRender: 'sequence' },
		customRender: (text, record, index) => index + 1,
	},
	{
		title: '接口名称',
		align: "center",
		dataIndex: 'apiName',
		width: '9%',
		scopedSlots: { customRender: 'apiName' },
	},
	{
		title: '系统名称',
		align: "center",
		dataIndex: 'systemName',
		width: '9%',
		scopedSlots: { customRender: 'systemName' },
	},
	{
		title: '环境名称',
		align: "center",
		dataIndex: 'environmentName',
		width: '7%',
		scopedSlots: { customRender: 'environmentName' },
	},
	{
		title: '服务名称',
		align: "center",
		dataIndex: 'serviceName',
		width: '9%',
		scopedSlots: { customRender: 'serviceName' },
	},
	{
		title: 'url',
		align: "center",
		dataIndex: 'url',
		width: '12%',
		scopedSlots: { customRender: 'url' },
	},
	{
		title: '方法',
		align: "center",
		dataIndex: 'method',
		width: '5%',
		scopedSlots: { customRender: 'method' },
	},
	{
		title: '参数类型',
		align: "center",
		dataIndex: 'parameterType',
		width: '7%',
		scopedSlots: { customRender: 'parameterType' },
	},
	{
		title: '参数',
		align: "center",
		dataIndex: 'parameter',
		width: '9%',
		scopedSlots: { customRender: 'parameter' },
	},
	{
		title: 'header',
		align: "center",
		dataIndex: 'header',
		width: '9%',
		scopedSlots: { customRender: 'header' },
	},
	{
		title: '响应成功的标准',
		align: "center",
		dataIndex: 'responseSuccessStandard',
		width: '6%',
		scopedSlots: { customRender: 'responseSuccessStandard' },
	},
	{
		title: '响应成功的正则表达式',
		align: "center",
		dataIndex: 'responseSuccessRegularExpression',
		width: '8%',
		scopedSlots: { customRender: 'responseSuccessRegularExpression' },
	},
	{
		title: '操作',
		align: "center",
		dataIndex: 'operation',
		width: '5%',
		scopedSlots: { customRender: 'operation' },
	},
];

export const tableData = [
	{
		key: 1,
		apiName: '即时通信登录接口',
		systemName: '即时通信',
		environmentName: '开发环境',
		serviceName: 'in-end-web',
		url: 'http://192.168.1.1:8080/api/v1/user/login',
		method: 'post',
		parameterType: 'form',
		parameter: 'username=admin&password=123456',
		header: '',
		responseSuccessStandard: 'json',
		responseSuccessRegularExpression: 'success: true',
	},
	{
		key: 2,
		apiName: '即时通信登录接口',
		systemName: '即时通信',
		environmentName: '开发环境',
		serviceName: 'in-end-web',
		url: 'http://192.168.1.1:8080/api/v1/user/login',
		method: 'post',
		parameterType: 'form',
		parameter: 'username=admin&password=123456',
		header: '',
		responseSuccessStandard: 'json',
		responseSuccessRegularExpression: 'success: true',
	},
];

