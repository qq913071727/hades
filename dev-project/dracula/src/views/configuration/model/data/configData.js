export const columns = [
	{
		title: '序号',
		align: "center",
		dataIndex: 'sequence',
		width: '10%',
		scopedSlots: { customRender: 'sequence' },
		customRender: (text, record, index) => index + 1,
	},
  {
		title: '名称',
		align: "center",
		dataIndex: 'name',
		width: '70%',
		scopedSlots: { customRender: 'name' },
	},
	{
		title: '操作',
		align: "center",
		dataIndex: 'operation',
		width: '20%',
		scopedSlots: { customRender: 'operation' },
	},
];

export const data = [
	{
		key: 'Erm--systemManagement',
		name: 'Erm--系统管理',
	},
	{
		key: 'Erm--personelManagement',
		name: 'Erm--员工管理',
	},
	{
		key: 'CRM/SRM--home',
		name: 'CRM/SRM--首页',
	},
	{
		key: 'CRM/SRM--basicInformationManagement',
		name: 'CRM/SRM--基本信息管理',
	},
	{
		key: 'CRM/SRM--customerManagement',
		name: 'CRM/SRM--客户管理',
	},
	{
		key: 'CRM/SRM--supplierManagement',
		name: 'CRM/SRM--供应商管理',
	},
	{
		key: 'CRM/SRM--carrierManagement',
		name: 'CRM/SRM--承运商管理',
	},
	{
		key: 'CRM/SRM--warehouseManagement',
		name: 'CRM/SRM--库房管理',
	},
	{
		key: 'inquiryQuotation--home',
		name: '询报价--首页',
	},
	{
		key: 'inquiryQuotation--inquiryManagement',
		name: '询报价--询价管理',
	},
	{
		key: 'inquiryQuotation--quotationManagement',
		name: '询报价--报价管理',
	},
	{
		key: 'inquiryQuotation--supplierCoordinationManagement',
		name: '询报价--供应商协同管理',
	},
	{
		key: 'inquiryQuotation--dataInquiry',
		name: '询报价--数据查询',
	},
];

