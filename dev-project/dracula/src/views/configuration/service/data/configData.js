export const columns = [
	{
		title: '序号',
		align: "center",
		dataIndex: 'sequence',
		width: '10%',
		scopedSlots: { customRender: 'sequence' },
		customRender: (text, record, index) => index + 1,
	},
  {
		title: '名称',
		align: "center",
		dataIndex: 'name',
		width: '70%',
		scopedSlots: { customRender: 'name' },
	},
	{
		title: '操作',
		align: "center",
		dataIndex: 'operation',
		width: '20%',
		scopedSlots: { customRender: 'operation' },
	},
];

export const data = [
	{
		key: 'im-end-vue',
		name: '即时通信前端',
	},
	{
		key: 'im-end-eureka',
		name: 'im-end-eureka',
	},
	{
		key: 'im-end-gateway',
		name: 'im-end-gateway',
	},
	{
		key: 'erp9-data',
		name: 'b1b产品数据导入',
	},
	{
		key: 'im-end-data',
		name: 'im-end-data',
	},
	{
		key: 'im-end-web',
		name: 'im-end-web',
	},
	{
		key: 'cyz-admin-vue',
		name: '远大支持论坛前端（后台管理系统）',
	},
	{
		key: 'cyz-vue',
		name: '远大支持论坛前端（创易站）',
	},
	{
		key: 'cyz-eureka',
		name: 'cyz-eureka',
	},
	{
		key: 'cyz-gateway',
		name: 'cyz-gateway',
	},
	{
		key: 'cyz-web',
		name: 'cyz-web',
	},
	{
		key: 'cyz-admin',
		name: 'cyz-admin',
	},
	{
		key: 'xxl-job-cyz',
		name: 'xxl-job（远大支持论坛）',
	},
];

