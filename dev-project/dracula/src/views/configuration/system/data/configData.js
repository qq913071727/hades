export const columns = [
	{
		title: '序号',
		align: "center",
		dataIndex: 'sequence',
		width: '10%',
		scopedSlots: { customRender: 'sequence' },
		customRender: (text, record, index) => index + 1,
	},
  {
		title: '名称',
		align: "center",
		dataIndex: 'name',
		width: '70%',
		scopedSlots: { customRender: 'name' },
	},
	{
		title: '操作',
		align: "center",
		dataIndex: 'operation',
		width: '20%',
		scopedSlots: { customRender: 'operation' },
	},
];
