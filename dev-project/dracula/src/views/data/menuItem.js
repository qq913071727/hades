/**
 * 首页菜单项中的数据
 */
export const homeMenu = {
  title: '首页',
  name: 'home',
  icon: 'dashboard',
  text: '首页',
  path: '/home',
  index: 'home',
}

/**
 * 配置管理菜单项中的数据
 */
 export const configurationMenu = {
  title: '配置管理',
  name: 'configurationManagement',
  icon: 'dashboard',
  subMenu: [
    { text: '系统', path: '/configuration/system', index: 'configurationManagement_system' },
    { text: '环境', path: '/configuration/environment', index: 'configurationManagement_environment' },
    { text: '服务', path: '/configuration/service', index: 'configurationManagement_service' },
    { text: '模块', path: '/configuration/model', index: 'configurationManagement_model' },
    { text: '接口', path: '/configuration/api', index: 'configurationManagement_api' },
    { text: '服务器', path: '/configuration/server', index: 'configurationManagement_server' },
    { text: '日志', path: '/configuration/log', index: 'configurationManagement_log' },
    { text: '邮箱', path: '/configuration/email', index: 'configurationManagement_email' },
    { text: '短信', path: '/configuration/shortMessage', index: 'configurationManagement_shortMessage' },
  ]
}