package org.noahsark.core.commons.lang;

import java.math.BigDecimal;

public class MainClass {
	@SuppressWarnings("unused")
	public static void main(String[] pArgs) throws Exception {
		TaxReturn return1 = new TaxReturn("012-68-3242", 1998, "O'Brien",
				new BigDecimal(43000.00));
		TaxReturn return2 = new TaxReturn("012-68-3242", 1999, "O'Brien",
				new BigDecimal(45000.00));
		TaxReturn return3 = new TaxReturn("012-68-3242", 1999, "O'Brien",
				new BigDecimal(53222.00));
		//System.out.println("ToStringBuilder: " + return1.toString());
		//System.out.println("HashCodeBuilder: " + return2.hashCode());
		//System.out.println("HashCodeBuilder: " + new TaxReturn("012-68-3242", 1999, "O'Brien",new BigDecimal(53222.00)).hashCode());
		//System.out.println("EqualsBuilder: " + return3.equals(new TaxReturn("012-68-3242", 1999, "O'Brien",new BigDecimal(53222.00))));
		System.out.println("CompareToBuilder: " + return3.compareTo(new TaxReturn("012-68-3242", 1998, "O'Brien",new BigDecimal(80000.00))));
	}
}
