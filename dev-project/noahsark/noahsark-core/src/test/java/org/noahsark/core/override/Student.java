package org.noahsark.core.override;

/**
 * Strudent��д��equals��hashCode���������ֻҪѧ���������ͬһ������ �ȿ���hashCode()������ͬ��ֵ��0�������
 */
public class Student {
	public Student(String no) {
		this.no = no;
	}

	// ��Զ������0
	@Override
	public int hashCode() {
		System.out.println("call hashcode");
		return 0;
	}

	// �����equalsʵ��ѧ����ͬ��ʾ���
	@Override
	public boolean equals(Object obj) {
		System.out.println("call equals");
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (no == null) {
			if (other.no != null)
				return false;
		} else if (!no.equals(other.no))
			return false;
		return true;
	}

	// ѧ��,�������Ψһ
	private String no;
	private String name;

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
