package org.noahsark.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.noahsark.core.dao.PaginationConfiguration.Ordering;

public class QueryBuilderTest {
	private static String sql="from Message m";
	private static QueryBuilder qb=new QueryBuilder(sql);

	public static void main(String[] args) {
		testAddDefaultOrderBy();
//		testAddInCriterion();
//		testAddOrCriterion();
//		testAddCriterion();
//		testAddParam();
//		testAddOrSql();
//		testSetAlias();
//		testAddSql();
		
		System.out.println(qb.calcFinalSql(true));
	}
	
	/**
	 * test failed
	 * reason:
	 * result can not be understood
	 */
	private static void testAddDefaultOrderBy(){
		qb.addCriterion("m.id", "=", "69");
		qb.addDefaultOrderBy("id", Ordering.ASCENDING);
	}
	
	/**
	 * test failed
	 * reason:
	 * result can not be understood
	 */
	private static void testAddInCriterion(){
		List<Integer> list=new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		qb.addInCriterion("m.title", list);
	}
	
	/**
	 * test failed
	 * reason:
	 * result can not be understood
	 */
	private static void testAddOrCriterion(){
		String param=new String("68");
		qb.addOrCriterion("m.id", "=", param);
	}
	
	/**
	 * test failed
	 * reason:
	 * result can not be understood
	 */
	private static void testAddCriterion(){
		String param=new String("68");
		qb.addCriterion("m.id", "=", param);
	}
	
	/**
	 * test failed
	 * reason:
	 * result can not be understood
	 */
	private static void testAddParam(){
		qb.addParam("id", 68);
	}
	
	/**
	 * test is successful
	 * result:
	 * select count(*) from Message m where m.title='abc' or m.id=61
	 */
	private static void testAddOrSql(){
		qb.addOrSql("m.title='abc'").addOrSql("m.id=61");
	}
	
	/**
	 * test failed
	 * reason:
	 * result can not be understood
	 */
	private static void testSetAlias(){
		qb.setAlias("o");
	}

	/**
	 * test is successful
	 * result:
	 * select count(*) from Message m where m.title='abc' and m.id=61
	 */
	private static void testAddSql(){
		qb.addSql("m.title='abc'").addSql("m.id=61");
	}
	

}
