package org.noahsark.core.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.noahsark.core.dao.BaseDao;
import org.noahsark.core.jpa.util.JpaUtil;

public class StockWeekendDaoImpl {
	@SuppressWarnings("unchecked")
	public List<Date> findDates(String beginDate,String endDate){
		StringBuffer sf=new StringBuffer("select distinct * from(select distinct t.weekend_begin_date from " +
				"stock_weekend t union select distinct t.weekend_end_date from stock_weekend t) t " +
				"where t.weekend_begin_date between to_date(:beginDate,'yyyy-mm-dd') " +
				"and to_date(:endDate,'yyyy-mm-dd')");
		EntityManager em=JpaUtil.currentEntityManager();
		Query q=em.createNativeQuery(sf.toString());
		q.setParameter("beginDate", beginDate);
		q.setParameter("endDate", endDate);
		List<Date> l=q.getResultList();
		System.out.println(l);
		return l;
	}
}
