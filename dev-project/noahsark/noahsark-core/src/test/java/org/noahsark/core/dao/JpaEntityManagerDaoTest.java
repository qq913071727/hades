package org.noahsark.core.dao;

import org.noahsark.core.domainmodel.Dept;

public class JpaEntityManagerDaoTest {
	public static void main(String[] args) {
		System.out.println(testFind().getLoc());
	}

	private static Dept testFind(){
		JpaEntityManagerDao<Dept, Long> jemd=new JpaEntityManagerDao<Dept, Long>();
		Dept dept=jemd.find(Dept.class, new Long(10));
		return dept;
	}
}
