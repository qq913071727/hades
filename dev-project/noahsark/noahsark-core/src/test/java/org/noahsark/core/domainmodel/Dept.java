package org.noahsark.core.domainmodel;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.noahsark.core.dao.BaseDao;
import org.noahsark.core.domainmodel.BaseEntity;

//@Entity
//@Table(name="DEPT")
public class Dept extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
//	@Id
//	@Column(name="DEPTNO")
	private BigDecimal deptNo;
//	@Column(name="DNAME")
	private String dName;
//	@Column(name="LOC")
	private String loc;
	
	public BigDecimal getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(BigDecimal deptNo) {
		this.deptNo = deptNo;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}
