package org.noahsark.core.parameterizedtype;

import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;

public class GenericDAO<T> {
	private Class<T> entityClass;

	public GenericDAO() {
		Type type = getClass().getGenericSuperclass();
		Type trueType = ((ParameterizedType)type).getActualTypeArguments()[0];
		this.entityClass = (Class<T>) trueType;
		
		System.out.println(getClass().getGenericSuperclass());
		System.out.println(getClass().getSuperclass());
		System.out.println(getClass());
		System.out.println(trueType);
	}
}
