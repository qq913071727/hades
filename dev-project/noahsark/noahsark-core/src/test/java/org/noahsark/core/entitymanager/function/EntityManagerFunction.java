package org.noahsark.core.entitymanager.function;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.noahsark.core.domainmodel.Dept;

public class EntityManagerFunction {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("mytool");
	private static EntityManager em=emf.createEntityManager();

	public static void main(String args[]){
    	//testFind();
		//testGetReference();
		//testPersist();
		//testUpdate();
		testRemove();
    }
	
	public static void testRemove(){
		em.getTransaction().begin();
		Dept dept=em.find(Dept.class,new BigDecimal(50));
		em.remove(dept);
		em.getTransaction().commit();
		em.close();
	}
	
	public static void testUpdate(){
		em.getTransaction().begin();
		Dept dept=em.find(Dept.class,new BigDecimal(50));
		dept.setdName("55555555");
		em.getTransaction().commit();
		em.close();
	}
	
	public static void testPersist(){
		em.getTransaction().begin();
		Dept dept=new Dept();
		dept.setDeptNo(new BigDecimal(60));
		em.persist(dept);
		em.getTransaction().commit();
		System.out.println(dept.getDeptNo());
		em.close();
	}
	
	public static void testGetReference(){
		Dept dept=em.getReference(Dept.class,new BigDecimal(50));
    	System.out.println(dept.getdName());
    	em.close();
	}
    
    public static void testFind(){
    	Dept dept=em.find(Dept.class,new BigDecimal(10));
    	System.out.println(dept.getdName());
    	em.close();
    }
    
    public static void testFlush(){
    	em.getTransaction().begin();
		Dept dept2=em.find(Dept.class,new BigDecimal(50));
		System.out.println(dept2.getdName());
		dept2.setdName("test");
//		em.flush();
		System.out.println(dept2.getdName());
		
//		Dept dept=new Dept();
//		dept.setDeptNo(new BigDecimal(60));
//		em.persist(dept);
//		em.flush();
		
//		Query query = em.createQuery("select t from Dept t where t.deptNo=60");
//		List<Dept> list = query.getResultList();
//    	System.out.println(list.get(0).getDeptNo());
    	
//		Dept dept2=em.find(Dept.class,new BigDecimal(60));
//    	System.out.println(dept2.getDeptNo());
		
		em.getTransaction().commit();
		em.close();
    }
}
