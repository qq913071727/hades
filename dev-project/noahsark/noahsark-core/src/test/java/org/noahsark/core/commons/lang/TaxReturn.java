package org.noahsark.core.commons.lang;

import java.math.BigDecimal;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class TaxReturn {
	private String ssn;
	private int year;
	private String lastName;
	private BigDecimal taxableIncome;

	public TaxReturn() {
	}

	public TaxReturn(String pSsn, int pYear, String pLastName,
			BigDecimal pTaxableIncome) {
		setSsn(pSsn);
		setYear(pYear);
		setLastName(pLastName);
		setTaxableIncome(pTaxableIncome);
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String pSsn) {
		ssn = pSsn;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int pYear) {
		year = pYear;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String pLastName) {
		lastName = pLastName;
	}

	public BigDecimal getTaxableIncome() {
		return taxableIncome;
	}

	public void setTaxableIncome(BigDecimal pTaxableIncome) {
		taxableIncome = pTaxableIncome;
	}

	public String toString() {
		return new ToStringBuilder(this).append("ssn", ssn)
				.append("year", year).append("lastName", lastName).append("taxableIncome",taxableIncome).toString();
	}

	public int hashCode() {
		return new HashCodeBuilder(3, 7).append(ssn).append(year).append(taxableIncome).toHashCode();
	}

	public boolean equals(Object pObject) {
		boolean equals = false;
		if (pObject instanceof TaxReturn) {
			TaxReturn bean = (TaxReturn) pObject;
			equals = (new EqualsBuilder().append(ssn, bean.ssn).append(year,
					bean.year).append(taxableIncome,bean.taxableIncome)).isEquals();
		}
		return equals;
	}

	public int compareTo(Object pObject) {
		return new CompareToBuilder().append(this.year, ((TaxReturn)pObject).getYear()).toComparison();
		//return CompareToBuilder.reflectionCompare(this, pObject);
	}
}
