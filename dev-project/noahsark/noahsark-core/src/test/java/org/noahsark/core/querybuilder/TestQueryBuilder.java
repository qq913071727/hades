package org.noahsark.core.querybuilder;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.noahsark.core.enumeration.SQLType;
import org.noahsark.core.dao.QueryBuilder;

//import cn.edu.my.seurat.domainmodel.Dept;

public class TestQueryBuilder {
	private static String jpql = "from Dept o";
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("mytool");
	private static EntityManager em=emf.createEntityManager();
	
	public static void main(String[] args){
		em.getTransaction().begin();
//		QueryBuilder qb = new QueryBuilder(jpql,SQLType.JPQL);
//		qb.addCriterion("o.deptNo", "=", "20", true, null);
//		System.out.println(qb.getSqlStringBuffer());
//		Query query=em.createQuery(jpql);
//		List<Dept> list=query.getResultList();
//		System.out.println(list.get(1).getdName());
		em.getTransaction().commit();
		em.close();
	}
}
