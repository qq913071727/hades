package org.noahsark.core.annotation;

import org.noahsark.core.annotation.AnnotationField;
import org.noahsark.core.annotation.AnnotationMethod;
import org.noahsark.core.annotation.AnnotationType;

@AnnotationType(typeValue = "1111111111111111111111111111111111111111111")
public class AnnotationUser {
	/** �ֶ����� */
	@AnnotationField(fieldValue = "22222222222222222222222222222222222222222")
	private String userField;
	
	/**
	 * ʹ���߷���
	 */
	@AnnotationMethod(methodValue = "333333333333333333333333333333333333333333333")
	public String userMethod() {
		return "user default method value";
	}

	/**
	 * ����
	 */
	public void doIt() {
		System.out.println(userField);
	}
	
	public String getUserField() {
		return userField;
	}
	public void setUserField(String userField) {
		this.userField = userField;
	}
}
