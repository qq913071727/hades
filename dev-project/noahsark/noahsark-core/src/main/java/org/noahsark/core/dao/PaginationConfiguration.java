package org.noahsark.core.dao;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * 提供分页、排序、filter信息.
 * 
 * @version Seurat v1.0
 * @author Cui Jie, 2012-6-11
 */
public class PaginationConfiguration implements Serializable {

    private static final long serialVersionUID = -2750287256630146681L;

    /**
     * order enum.
     * 
     * @version Seurat v1.0
     * @author Yin Yanjun, 2012-6-11
     */
    public enum Ordering {
        ASCENDING, DESCENDING
    };

    private int firstRow = -1;
    private int numberOfRows = -1;

    private Map<String, FilterValue> filters;

    private String sortField;
    private Ordering ordering;

    /**
     * 获取table的数据.
     * 
     * @param firstRow firstRow
     * @param numberOfRows numberOfRows
     * @param filters filters
     * @param sortField sortField
     * @param ordering ordering
     */
    public PaginationConfiguration(int firstRow, int numberOfRows,
            Map<String, FilterValue> filters, String sortField, Ordering ordering) {
        this.firstRow = firstRow;
        this.numberOfRows = numberOfRows;
        this.filters = filters;
        this.sortField = sortField;
        this.ordering = ordering;
    }

    /**
     * 获取总行数.
     * 
     * @param filters filters
     */
    public PaginationConfiguration(Map<String, FilterValue> filters) {
        this.filters = filters;
    }

    /**
     * 不基于页面table的分页，直接程序分页.
     * 
     * @param firstRow firstRow
     * @param numberOfRows numberOfRows
     */
    public PaginationConfiguration(int firstRow, int numberOfRows) {
        this.firstRow = firstRow;
        this.numberOfRows = numberOfRows;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public String getSortField() {
        return sortField;
    }

    public Ordering getOrdering() {
        return ordering;
    }

    public Map<String, FilterValue> getFilters() {
        return filters;
    }

    public boolean isSorted() {
        return ordering != null && sortField != null && sortField.trim().length() != 0;
    }

    public boolean isAscendingSorting() {
        return ordering != null && ordering == Ordering.ASCENDING;
    }

    public boolean isFiltered() {
        return filters != null && filters.size() > 0;
    }
}
