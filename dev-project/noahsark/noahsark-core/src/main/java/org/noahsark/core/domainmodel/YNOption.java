package org.noahsark.core.domainmodel;

/**
 * YNOption.
 * 
 */
public enum YNOption {
    Y, N
}
