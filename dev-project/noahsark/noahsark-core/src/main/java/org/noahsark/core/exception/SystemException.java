package org.noahsark.core.exception;

/**
 * System Exception.
 */
public class SystemException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String errorCode;
    private String mgKey;

    /**
     * constructor.
     * 
     * @param errorCode errorCode
     * @param mgKey mgKey
     */
    public SystemException(String errorCode, String mgKey) {
        super();
        this.errorCode = errorCode;
        this.mgKey = mgKey;
    }

    /**
     * constructor.
     * 
     * @param errorCode errorCode
     * @param mgKey mgKey
     * @param cause cause
     */
    public SystemException(String errorCode, String mgKey, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
        this.mgKey = mgKey;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMgKey() {
        return mgKey;
    }

    public void setMgKey(String mgKey) {
        this.mgKey = mgKey;
    }
}
