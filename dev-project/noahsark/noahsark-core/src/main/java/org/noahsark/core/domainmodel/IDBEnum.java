package org.noahsark.core.domainmodel;

/**

 * Implements this when the enumeration is not directly into the database.
 * 
 */
public interface IDBEnum {

    /**
     * value in database.
     * @return String value in database.
     */
    String getValue();

}
