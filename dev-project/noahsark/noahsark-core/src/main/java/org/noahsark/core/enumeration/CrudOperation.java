package org.noahsark.core.enumeration;

public enum CrudOperation {
	CREATE("Add"),RETRIEVE("Select"),UPDATE("Update"),DELETE("Delete");
	
	private String description;
	
	private CrudOperation(String description){
		this.description=description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
