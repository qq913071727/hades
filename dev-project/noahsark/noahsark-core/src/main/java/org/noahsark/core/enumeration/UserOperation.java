package org.noahsark.core.enumeration;

/**
 * user job operation in the web subsystem.
 */
public enum UserOperation {

    CREATE("Create"), ABORT("Abort"), FORCE_RUN("Force Run"), RERUN("Rerun"), SUSPEND("Suspend"), RESUME(
            "Resume"),

    /**
     * to hold job.
     */
    HOLD("Hold"),

    UNHOLD("Unhold"),
    /**
     * to change expected start time when job is Scheduled.
     */
    CHANGE_EST("Expected Start Time (and or Alert Time) has been changed"),

    /**
     * to change priority when job is Queued.
     */
    CHANGE_PRIORITY("Change Priority");

    private String desc;

    /**
     * init UserOperation.
     * 
     * @param desc desc
     */
    private UserOperation(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
