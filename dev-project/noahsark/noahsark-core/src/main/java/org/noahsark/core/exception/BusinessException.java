package org.noahsark.core.exception;

/**
 * 所有业务异常的基类，大部分业务异常直接使用该异常.
 */
public class BusinessException extends Exception {

    private static final long serialVersionUID = 1L;

    private String errorCode;

    /**
     * constructor.
     * 
     * @param message messageKey
     */
    public BusinessException(String message) {
        super(message);
    }

    /**
     * constructor.
     * 
     * @param message messageKey
     * @param cause cause
     */
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor.
     * 
     * @param errorCode errorCode
     * @param message messageKey
     */
    public BusinessException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * constructor.
     * 
     * @param errorCode errorCode
     * @param message messageKey
     * @param cause cause
     */
    public BusinessException(String errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
