package org.noahsark.core.cache;

import java.util.HashMap;
import java.util.Map;

public class MemoryCache<T>
        implements ICache<T> {
    private Map<String, T> map = new HashMap();

    public void add(String key, T obj, int timeout) {
        this.map.put(key, obj);
    }

    public void add(String key, T obj) {
        this.map.put(key, obj);
    }

    public void delByKey(String key) {
        this.map.remove(key);
    }

    public void clearAll() {
        this.map.clear();
    }

    public T getByKey(String key) {
        return (T) this.map.get(key);
    }

    public boolean containKey(String key) {
        return this.map.containsKey(key);
    }
}

