package org.noahsark.core.util;

/**
 * MessageBundleUtil.
 */
public class MessageBundleUtil {

    /**
     * Hidden Validate error.
     */
    //public static FacesMessage hiddenFacesMessage = new FacesMessage("hidden Validate error");

    /**
     * Prvents instantiation for Constant class.
     */
    private MessageBundleUtil() {
    }

    /**
     * Add a status message, looking up the message in the resource bundle using the provided key.
     * If the message is found, it is used, otherwise, the defaultMessageTemplate will be used.
     * 
     * A severity of INFO will be used, and you can specify parameters to be interpolated
     * 
     * @param key String
     * @param params Object
     */
    public static void putGlobalResourceMessages(String key, Object... params) {
        //FacesMessages.instance().addFromResourceBundle(key, params);
    }
    
    /**
     * Add a status message, looking up the message in the resource bundle using the provided key.
     * If the message is found, it is used, otherwise, the defaultMessageTemplate will be used.
     * 
     * A severity of ERROR will be used, and you can specify parameters to be interpolated
     * 
     * @param key String
     * @param params Object
     */
    public static void putGlobalErrorMessages(String key, Object... params) {
        //FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, key, params);
    }

    /**
     * Create a new status message, looking up the message in the resource bundle using the provided
     * key.
     * 
     * The message will be added to the widget specified by the ID. The algorithm used determine
     * which widget the id refers to is determined by the view layer implementation in use.
     * 
     * You can also specify the severity, and parameters to be interpolated
     * 
     * @param elementId String
     * @param key String
     * @param params Object
     */
    public static void putErrorMessages(String elementId, String key, Object... params) {
//        FacesMessages.instance().addToControlFromResourceBundle(elementId,
//                StatusMessage.Severity.ERROR, key, params);
    }

    /**
     * Utility method to create a FacesMessage from a Severity, key,defaultMessageTemplate and
     * params.
     * 
     * This method interpolates the parameters provided
     * 
     * @param keyOrMessage write key in the resource file, or just write message here
     * @param params replace the EL expressions or '{x}' in the message. params's max length is 10.
     * @return FacesMessage FacesMessages String key, String defaultMessageTemplate(not key)
     */
//    public static FacesMessage createFacesMessage(String keyOrMessage, Object... params) {
//        return FacesMessages.createFacesMessage(FacesMessage.SEVERITY_ERROR, keyOrMessage,
//                keyOrMessage, params);
//    }

    /**
     * Create Error Message with the key.
     * 
     * @param keyOrMessage String
     * @param componentId String
     * @param params Object
     * @return ErrorMessage
     */
//    public static ErrorMessage createErrorMessage(String keyOrMessage, String componentId,
//                                                  Object... params) {
//        StatusMessage message = new StatusMessage(null, keyOrMessage, null, keyOrMessage, null);
//        message.interpolate(params);
//        if (Strings.isEmpty(message.getSummary())) {
//            throw new RuntimeException("key not in message.property");
//        }
//        return new ErrorMessage(message.getSummary(), componentId);
//    }

    /**
     * Create Message with the key.
     * 
     * @param keyOrMessage String
     * @param params Object
     * @return String
     */
//    public static String createMessage(String keyOrMessage, Object... params) {
//        StatusMessage message = new StatusMessage(null, keyOrMessage, null, keyOrMessage, null);
//        message.interpolate(params);
//        if (Strings.isEmpty(message.getSummary())) {
//            throw new RuntimeException("key not in message.property");
//        }
//        return message.getSummary();
//    }

    /**
     * Get all faces global messages that have already been added to the context.
     * 
     * @return List FacesMessage
     */
//    public static List<FacesMessage> getCurrentGlobalMessages() {
//        return FacesMessages.instance().getCurrentGlobalMessages();
//    }
}
