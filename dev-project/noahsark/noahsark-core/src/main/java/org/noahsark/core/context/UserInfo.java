package org.noahsark.core.context;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * User Context Information in System.
 * 
 * <pre>
 * Set Default UserInfo for Developer run app in the condition of no login entrance.
 * UserId: 1
 * UserName:test@iata.org
 * Login BSP Code: CN
 * Managed BSP Code List: CN, HK, AU
 * Access Category: GLOBAL
 * </pre>
 * 
 */
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** User Unique Id */
    private Long userId;

    /** User Login Name */
    private String userName;
    
    private String firstName;
    private String lastName;

    private LogoutType logoutType;
    
    private static UserInfo userInfo;

    public UserInfo() {
	}

	/**
     * User Type, includes GLOBAL, REGION, LOCAL and DPC.
     */
    private String accessCategory;

    /** Map for user's extra information storage. Such as BSP Code List managed by user. */
    private Map<String, Object> userInfoMap = new HashMap<String, Object>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccessCategory() {
        return accessCategory;
    }

    public void setAccessCategory(String accessCategory) {
        this.accessCategory = accessCategory;
    }

    public LogoutType getLogoutType() {
        return logoutType;
    }

    public void setLogoutType(LogoutType logoutType) {
        this.logoutType = logoutType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * Acquire UserInfo instance.
     * 
     * @return UserInfo instance
     */
    public static UserInfo instance() {
    	if(null==userInfo){
    		return new UserInfo();
    	}
        return userInfo;
    }

    @SuppressWarnings("unchecked")
	public List<String> getRolePrivileges() {
        return (List<String>) userInfoMap.get("rolePrivileges");
    }

    public void setRolePrivileges(List<String> rolePrivileges) {
        userInfoMap.put("rolePrivileges", rolePrivileges);
    }

    /**
     * 
     * LogoutType.
     * 
     * @version Seurat v1.0
     * @author Yu Tao, 2013-8-14
     */
    public enum LogoutType {
        /**
         * Normal Logout by Logout button.
         */
        NORMAL_LOGOUT,

        /**
         * User Idle for session time out.
         */
        IDLE_TIMEOUT,

        /**
         * User kicked off by self login.
         */
        KICKOFF
    }

}
