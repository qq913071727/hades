package org.noahsark.core.enumeration;

public enum LogicalOperator {
	AND, OR;
}
