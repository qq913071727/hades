package org.noahsark.core.action;

/**
 * 
 * action listener control.
 * 
 */
public class ActionListenerControl {

    private boolean continueFlag = true;

    public boolean isContinueFlag() {
        return continueFlag;
    }

    /**
     * set continue flag to false.
     */
    public void notCoutinue() {
        this.continueFlag = false;
    }

    /**
     * 
     * create instance.
     * 
     * @return ActionListenerControl
     */
    public static ActionListenerControl instance() {
        return new ActionListenerControl();
    }
}
