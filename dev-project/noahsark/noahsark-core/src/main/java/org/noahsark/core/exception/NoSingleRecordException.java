package org.noahsark.core.exception;

/**
 * 
 * This exception is thrown that require a single record but does not contain exactly one.
 * 
 */
public class NoSingleRecordException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * constructor.
     * 
     * @param message message
     */
    public NoSingleRecordException(String message) {
        super("NoSingleRecordException :" + message);
    }

    /**
     * constructor.
     * 
     * @param message message
     * @param cause cause
     */
    public NoSingleRecordException(String message, Throwable cause) {
        super("NoSingleRecordException :" + message, cause);
    }
}
