package org.noahsark.core.cache;

public interface ICache<T> {
    public static final String SYS_BO_LIST_CACHE = "SYS_BO_LIST";

    public abstract void add(String paramString, T paramT, int paramInt);

    public abstract void add(String paramString, T paramT);

    public abstract void delByKey(String paramString);

    public abstract void clearAll();

    public abstract Object getByKey(String paramString);

    public abstract boolean containKey(String paramString);
}
