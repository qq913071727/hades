package org.noahsark.core.dao;

import javax.annotation.Resource;

import org.springframework.orm.jpa.JpaTemplate;

@SuppressWarnings({ "restriction", "deprecation" })
public class JpaTemplateDao {
	@Resource
	private JpaTemplate jpaTemplate;

	public JpaTemplate getJpaTemplate() {
		return jpaTemplate;
	}

	public void setJpaTemplate(JpaTemplate jpaTemplate) {
		this.jpaTemplate = jpaTemplate;
	}
}
