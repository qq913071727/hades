package org.noahsark.core.validator;

/**
 * Error Message for validation.
 */
public class ErrorMessage {

    /**Replaced by keyOrMessage, correspond to the key in message.perporty. */
    @Deprecated
    private String key;
    /**Specific component id , Global message if componentId is null. */
    private String componentId;
    /**Key or message. */
    private String keyOrMessage;
    /**
     * constructor.
     */
    public ErrorMessage() {
    }

    /**
     * Global Error Message.
     *
     * @param keyOrMessageValue - keyOrMessage
     */
    public ErrorMessage(String keyOrMessageValue) {
        this(keyOrMessageValue, null);
        key = keyOrMessageValue;
    }

    /**
     * Error Message for specific component.
     *
     * @param keyOrMessageValue - keyOrMessage
     * @param componentIdValue - componentId
     */
    public ErrorMessage(String keyOrMessageValue, String componentIdValue) {
        this.keyOrMessage = keyOrMessageValue;
        this.componentId = componentIdValue;
        key = keyOrMessageValue;
    }

    @Deprecated
    public String getKey() {
        return key;
    }

    /**
     *  Get Id of the component with exception .
     *
     * @return the componentId
     */
    public String getComponentId() {
        return componentId;
    }

    /**
     *  Set Id of the component with exception .
     * @param value - the componentId to set
     */
    public void setComponentId(String value) {
        this.componentId = value;
    }

    public String getKeyOrMessage() {
        return keyOrMessage;
    }

    public void setKeyOrMessage(String value) {
        this.keyOrMessage = value;
    }

}
