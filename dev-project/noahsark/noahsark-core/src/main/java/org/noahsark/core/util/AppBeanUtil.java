package org.noahsark.core.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class AppBeanUtil implements ApplicationContextAware {
    private static ApplicationContext appContext = null;

    public static <T> T getBean(Class<T> beanClass) {
        return (T) appContext.getBean(beanClass);
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }
}
