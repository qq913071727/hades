package org.noahsark.core.redis;

import org.noahsark.core.util.AppBeanUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.List;

public class RedisTemplate {
    private JedisPool pool;

    public void init()
            throws Exception {
        // 此处可以将redis的相关信息存储在表sys_properties中
//        int redisMaxTotal = this.sysPropertiesManager.getIntByAlias("redis.maxTotal");
//        int redisMinIdle = this.sysPropertiesManager.getIntByAlias("redis.minIdle");
//        int redisPort = this.sysPropertiesManager.getIntByAlias("redis.port");
//        String redisIp = this.sysPropertiesManager.getByAlias("redis.ip");
        int redisMaxTotal = 20;
        int redisMinIdle = 10;
        int redisPort = 6379;
        String redisIp = "'192.168.1.110'";
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(redisMaxTotal);
        jedisPoolConfig.setMinIdle(redisMinIdle);
        jedisPoolConfig.setTestWhileIdle(true);
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setTestOnReturn(true);
        this.pool = new JedisPool(jedisPoolConfig, redisIp, redisPort);
    }

    public static RedisTemplate getRedisTemplate() {
        RedisTemplate template = (RedisTemplate) AppBeanUtil.getBean(RedisTemplate.class);
        return template;
    }

    public void setString(String key, String value) {
        Jedis jedis = getJedis();
        try {
            jedis.set(key, value);
        } catch (Exception ex) {

        } finally {
            close(jedis);
        }
    }

    public void close(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }

//    public void setObject(String key, Object value) {
//        if (BeanUtil.isEmpty(value)) {
//            return;
//        }
//        Jedis jedis = getJedis();
//        try {
//            byte[] bytes = FileUtil.objToBytes(value);
//            jedis.set(key.getBytes("utf-8"), bytes);
//        } catch (Exception ex) {
//
//        } finally {
//            close(jedis);
//        }
//    }

    public String getString(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.get(key);
        } catch (Exception ex) {

            return "";
        } finally {
            close(jedis);
        }
    }

//    public Object getObject(String key) {
//        Jedis jedis = getJedis();
//        try {
//            byte[] bytes = jedis.get(key.getBytes("utf-8"));
//            if (bytes == null) {
//                return null;
//            }
//            return FileUtil.bytesToObject(bytes);
//        } catch (Exception ex) {
//            Object localObject1;
//            logger.error(ExceptionUtil.getExceptionMessage(ex));
//            return null;
//        } finally {
//            close(jedis);
//        }
//    }

    public boolean isExist(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.exists(key.getBytes("utf-8")).booleanValue();
        } catch (Exception ex) {

            return false;
        } finally {
            close(jedis);
        }
    }

    public synchronized Jedis getJedis() {
        Jedis jedis = this.pool.getResource();
        return jedis;
    }

    public void delByKey(String key) {
        Jedis jedis = getJedis();
        try {
            jedis.del(key);
        } catch (Exception ex) {

        } finally {
            close(jedis);
        }
    }

//    public static void main(String[] args)
//            throws Exception {
//        List<String> list = new ArrayList();
//        list.add("����");
//        list.add("chinese");
//
//        byte[] bytes = FileUtil.objToBytes(list);
//
//        List<String> rtList = (List) FileUtil.bytesToObject(bytes);
//        for (String str : rtList) {
//            System.out.println(str);
//        }
//    }

    public void destroy() {
        this.pool.destroy();
    }
}
