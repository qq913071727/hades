package org.noahsark.core.enumeration;

/**
 * 
 * sql type enum.
 * 
 */
public enum SQLType {
	JPQL, HQL, NATIVE_SQL;
}
