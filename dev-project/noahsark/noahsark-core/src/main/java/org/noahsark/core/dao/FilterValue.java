package org.noahsark.core.dao;

/**
 * 
 * filter value vo.
 * 
 */
public class FilterValue {

    public static final String TYPE_ENMU = "enum";
    public static final String TYPE_STRING = "string";
    public static final String TYPE_DATE = "date";
    public static final String TYPE_NUMBER = "number";
    private String type;
    private Object value;
    private String tableColumnName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getTableColumnName() {
        return tableColumnName;
    }

    public void setTableColumnName(String tableColumnName) {
        this.tableColumnName = tableColumnName;
    }

}
