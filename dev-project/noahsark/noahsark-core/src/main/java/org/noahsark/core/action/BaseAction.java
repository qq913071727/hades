package org.noahsark.core.action;

import org.apache.log4j.Logger;
import org.noahsark.core.util.MessageBundleUtil;
import org.noahsark.core.util.ResourceMessage;

/**
 * base action class.
 */
public abstract class BaseAction {
    
    public String globalMessage;

    /**
	 * Logger类的实例，用于记录日志
	 */
	public Logger logger = Logger.getLogger(getClass());

	/**
	 * Add Message.
	 * 
	 * @param key
	 *            Message key
	 * @param params
	 *            message params
	 */
	public void addMessage(String key, Object... params) {
		MessageBundleUtil.putGlobalResourceMessages(key, params);
		globalMessage = ResourceMessage.getMessage(key);
	}

	/**
	 * Add Error Message.
	 * 
	 * @param key
	 *            Message key
	 * @param params
	 *            message params
	 */
	public void addErrorMessage(String key, Object... params) {
		MessageBundleUtil.putGlobalErrorMessages(key, params);
	}

	/**
	 * 
	 * add Error message to facesContext.
	 * 
	 * @param elementId
	 *            组件id
	 * @param key
	 *            Message key
	 * @param params
	 *            message params
	 */
	public void addElementErrorMessage(String elementId, String key,
			Object... params) {
		MessageBundleUtil.putErrorMessages(elementId, key, params);
	}

	/**
	 * 
	 * throw validatorException.
	 * 
	 * @param key
	 *            Message key
	 * @param params
	 *            message params
	 * @throws ValidatorException
	 *             jsf ValidatorException
	 */
//	public void throwValidatorException(String key, Object... params)
//			throws ValidatorException {
//		throw new ValidatorException(MessageBundleUtil.createFacesMessage(key,
//				params));
//	}

//	public String getAgentCode() {
//		return agentCode;
//	}
//
//	public void setAgentCode(String agentCode) {
//		this.agentCode = agentCode;
//	}
//	
	/**
	 * No Row Select.
	 * TRUE: not select; FALSE: is selected.
	 * @param notSelect boolean
	 */
	public void notSelect(boolean notSelect){
	    if(notSelect){
	        this.addErrorMessage("info.not.select");
//	        ActionListenerControl.instance().notCoutinue();

	    }
	}

}
