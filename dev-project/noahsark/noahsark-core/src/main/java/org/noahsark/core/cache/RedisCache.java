package org.noahsark.core.cache;

import org.noahsark.core.redis.RedisTemplate;

import javax.annotation.Resource;

public class RedisCache
        implements ICache {
    @Resource
    RedisTemplate redisTemplate;

//    public void add(String key, Object obj, int timeout) {
//        this.redisTemplate.setObject(key, obj);
//    }

//    public void add(String key, Object obj) {
//        this.redisTemplate.setObject(key, obj);
//    }

    public void delByKey(String key) {
        this.redisTemplate.delByKey(key);
    }

    public void clearAll() {
    }

//    public Object getByKey(String key) {
//        return this.redisTemplate.getObject(key);
//    }

    public boolean containKey(String key) {
        return this.redisTemplate.isExist(key);
    }
}
