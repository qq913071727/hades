package org.noahsark.core.domainmodel;

import java.io.Serializable;

/**
 * As entities primary key using a variety of fields, implement the interface for unified access to 
 * entities primary key.
 * @author user
 *
 * @param <K> Serializable id
 */
public interface IEntity <K extends Serializable>{
	/**
	 * getId
	 * @return
	 */
	K getId();
}
