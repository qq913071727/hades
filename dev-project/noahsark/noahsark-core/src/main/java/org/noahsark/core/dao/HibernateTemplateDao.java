package org.noahsark.core.dao;

import javax.annotation.Resource;

import org.springframework.orm.hibernate3.HibernateTemplate;

@SuppressWarnings("restriction")
public class HibernateTemplateDao{
	
	@Resource
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
}
