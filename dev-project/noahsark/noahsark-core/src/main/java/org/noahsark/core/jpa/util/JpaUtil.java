package org.noahsark.core.jpa.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JpaUtil {
	private final static Log log = LogFactory.getLog(JpaUtil.class);
	
	private static EntityManagerFactory entityManagerFactory;
	
	static {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("MYTOOL");
		} catch (Exception ex) {
			System.err.println("EntityManagerFactory" + ex.getMessage());
		}
	}

	public static EntityManager currentEntityManager() {
		log.info("static method currentEntityManager in class JpaUtil begin");
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		log.info("static method currentEntityManager in class JpaUtil end");
		
		return entityManager;
	}

	public static void closeEntityManager(EntityManager entityManager) {
		log.info("static method closeEntityManager in class JpaUtil begin");
		
		if (null != entityManager) {
			entityManager.close();
		}
		
		log.info("static method closeEntityManager in class JpaUtil end");
	}
}
