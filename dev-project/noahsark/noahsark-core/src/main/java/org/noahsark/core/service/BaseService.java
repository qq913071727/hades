package org.noahsark.core.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * Service base class.
 * 
 */
public abstract class BaseService {

	protected final Log log = LogFactory.getLog(getClass());

}
