package org.noahsark.utilities.common;

public class StringUtils {
	/**
	 * check whether Object instance is blank or not
	 * return true when object instance is null
	 * return true when length of String instance is 0
	 * @param value
	 * @return
	 */
	public static boolean isBlankObject(Object value) {
		if(null==value){
			return true;
		}
		if((value instanceof String)&&((String)value).length()==0){
			return true;
		}
		return false;
    }
	
	/**
	 * check whether String instance is empty or not
	 * return true when String instance is null
	 * return true when length of String instance is 0
	 * @param str
	 * @return
	 */
	public static boolean isEmptyString(String str) {
		if(null==str){
			return true;
		}
		if(0==str.length()){
			return true;
		}
        return false;
    }
	
	/**
     * when the object is null or blank string, returns true.
     * 
     * <pre>
     * StringUtils.isBlank(new Object())=false
     * StringUtils.isBlank(null)=true
     * StringUtils.isBlank("")=true
     * StringUtils.isBlank(" ")=true
     * StringUtils.isBlank("abc")=false
     * </pre>
     * 
     * @param value target instance
     * @return boolean
     */
    public static boolean isBlank(Object value) {
        return ((value == null) || ((value instanceof String) && ((String) value).trim().length() == 0));
    }
}
