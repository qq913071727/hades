package org.noahsark.utilities.common;

/**
 * DoubleUtil.
 */
public class DoubleUtil {
    
    private DoubleUtil(){
    }
    
    /**
     * Parse String to Double. If error, return null.
     * @param str String.
     * @return Double
     */
    public static Double valueOf(String str){
        
        try{
            return Double.valueOf(str.trim());
        }catch(Exception ex){
            return null;
        }
    }
    
    /**
     * Parse String to Double. If error, return 0.0.
     * @param str String.
     * @return Double
     */
    public static Double defaultValueOf(String str){
        
        try{
            return Double.valueOf(str.trim());
        }catch(Exception ex){
            return 0.0;
        }
    }
}
