package org.noahsark.utilities.common;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtilTest {

    public static void main(String[] args){
        Map root=new HashMap<String, Object>();

        // 定义表数组
        List tableList=new ArrayList<List>();
        // 定义表
        List table1=new ArrayList();
        List table2=new ArrayList();
        // 定义行
        List row1=new ArrayList();
        List row2=new ArrayList();
        // 定义行键
        Map rowKey1=new HashMap();
        rowKey1.put("row-key",1);
        Map rowKey2=new HashMap();
        rowKey2.put("row-key",2);
        // 定义列族
        List<Map> columnFamily1=new ArrayList<Map>();
        List<Map> columnFamily2=new ArrayList<Map>();
        // 定义单元
        Map cell1=new HashMap();
        cell1.put("warehouse:bd-code",2817965);
        cell1.put("warehouse:data-category", "facilities");
        cell1.put("warehouse:data-object-id", "111");
        Map cell2=new HashMap();
        cell2.put("dam:bd-code",4354354);
        cell2.put("dam:data-category", "location");
        cell2.put("dam:data-object-id", "222");

        // 添加列族的成员
        columnFamily1.add(cell1);
        columnFamily2.add(cell2);
        // 添加行的成员
        row1.add(rowKey1);
        row1.add(columnFamily1);
        row2.add(rowKey2);
        row2.add(columnFamily2);
        // 添加表的成员
        table1.add(row1);
        table2.add(row2);
        // 添加表数组
        tableList.add(table1);
        tableList.add(table2);
        root.put("grid-index-tables",tableList);

        System.out.println(JSONObject.toJSONString(root));
    }
}
