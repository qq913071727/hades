package org.sraosha.framework.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图像验证码
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VerificationCode", description = "图像验证码")
public class VerificationCode implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "图片(base64格式)", dataType = "String")
    private String imageBase64;

//    @ApiModelProperty(value = "控制验证码是否启用的", dataType = "String")
//    private Boolean captchaEnabled;

    @ApiModelProperty(value = "uuid", dataType = "String")
    private String uuid;

    @ApiModelProperty(value = "计算结果", dataType = "Integer")
    private Integer calculationResult;

}
