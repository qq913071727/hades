package org.sraosha.framework.handler.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(annotations = Service.class)
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 处理NullPointerException异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler({NullPointerException.class})
    public void nullPointerExceptionHandler(NullPointerException e) {
        log.error("NullPointerException异常：", e);
    }

    /**
     * 处理其它异常：统一捕获，写入异常日志
     * 例1：HttpRequestMethodNotSupportedException 路由请求方式异常
     * 例2：NullPointerException 空指针异常
     * 例3：BadSqlGrammarException sql异常
     * ......
     *
     * @param e
     * @return
     */
    @ExceptionHandler({Exception.class})
    public void exceptionHandler(Exception e) {
        log.error("Exception异常：", e);
    }
}
