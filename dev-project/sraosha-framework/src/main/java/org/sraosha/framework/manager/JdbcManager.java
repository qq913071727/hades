package org.sraosha.framework.manager;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.extern.slf4j.Slf4j;
import java.sql.SQLException;

/**
 * @desc   jdbc的管理类
 */
@Slf4j
public class JdbcManager {
    private static DruidDataSource dataSource;

    public JdbcManager(String className, String url, String userName, String password) throws SQLException {
        dataSource = new DruidDataSource();
        dataSource.setDriverClassName(className);
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);

        // 初始化时建立物理连接的个数。初始化发生在显示调用init方法，或者第一次getConnection时
        dataSource.setInitialSize(1);

        // 最小连接池数量
        dataSource.setMinIdle(1);

        // 最大连接池数量
        dataSource.setMaxActive(1);

        // 获取连接时最大等待时间，单位毫秒。配置了maxWait之后，缺省启用公平锁，并发效率会有所下降，如果需要可以通过配置useUnfairLock属性为true使用非公平锁。
        dataSource.setMaxWait(1000 * 20);

        // 有两个含义：1) Destroy线程会检测连接的间隔时间、2) testWhileIdle的判断依据，详细看testWhileIdle属性的说明
        dataSource.setTimeBetweenEvictionRunsMillis(1000 * 60);

        // 配置一个连接在池中最大生存的时间，单位是毫秒
        dataSource.setMaxEvictableIdleTimeMillis(1000 * 60 * 60 * 10);

        // 配置一个连接在池中最小生存的时间，单位是毫秒
        dataSource.setMinEvictableIdleTimeMillis(1000 * 60 * 60 * 9);

        // 这里建议配置为TRUE，防止取到的连接不可用
        dataSource.setTestWhileIdle(true);

        dataSource.setTestOnBorrow(false);
        dataSource.setTestOnReturn(false);
        dataSource.setValidationQuery("select 1");

        dataSource.init();
    }

    /**
     * 获取数据源连接
     * @return 返回数据源连接
     */
    public DruidDataSource getDataSource(){
        return dataSource;
    }

    /**
     * 关闭连接
     */
    public void close(){
        if(dataSource != null && !dataSource.isClosed()){
            dataSource.close();
        }
    }
}