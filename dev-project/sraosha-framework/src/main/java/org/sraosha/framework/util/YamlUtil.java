package org.sraosha.framework.util;

import org.yaml.snakeyaml.Yaml;

import java.util.Map;

/**
 * yaml工具类
 */
public class YamlUtil {
    /**
     * 返回application.yml文件中key为keyArray参数的value值
     * @param env 当前启动环境
     * @param keyArray 配置文件中的参数
     * @return 返回结果
     */
    public static String getPropertyValue(String env, String... keyArray) {
        // 根据启动环境参数env获取im-end-value模块中配置的内容
        Map<String, Object> serverMap = YamlUtil.getServerMap(env);
        if (null != keyArray && keyArray.length > 0){
            for (int i=0; i<keyArray.length - 1; i++){
                serverMap = (Map<String, Object>) serverMap.get(keyArray[i]);
            }
            return (String) serverMap.get(keyArray[keyArray.length - 1]);
        } else {
            return null;
        }
    }

    /**
     * 返回application.yml文件中的value
     * @param env 当前启动环境
     * @return 返回配置文件中的内容
     */
    public static Map<String, Object> getServerMap(String env) {
        Yaml yaml = new Yaml();
        return yaml.load(YamlUtil.class.getResourceAsStream("/" + "application-" + env + ".yml"));
    }
}