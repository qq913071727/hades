package org.sraosha.framework.util;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @desc   aes工具类
 */
public class AesUtils {
//    private static final Logger log = LoggerFactory.getLogger(AesUtils.class);

//    public static void main(String[] args) throws Exception {
//        /*
//         * 此处使用AES-128-ECB加密模式，key需要为16位。
//         */
//        String cKey = "jkl;POIU1234++==";
//        // 需要加密的字串
//        String cSrc = "www.gowhere.so";
//        log.info(cSrc);
//        // 加密
//        String enString = AesUtils.encrypt(cSrc, cKey);
//        log.info("加密后的字串是：" + enString);
//
//        // 解密
//        String deString = AesUtils.decrypt(enString, cKey);
//        log.info("解密后的字串是：" + deString);
//    }

    // 加密
    public static String encrypt(String sSrc, String sKey) throws Exception {
        if (sKey == null) {
//            System.out.print("Key为空null");
            return null;
        }
        // 判断Key是否为16位
        if (sKey.length() != 16) {
//            System.out.print("Key长度不是16位");
            return null;
        }
        byte[] raw = sKey.getBytes("utf-8");
        SecretKeySpec sKeySpec = new SecretKeySpec(raw, "AES");
        //"算法/模式/补码方式"
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, sKeySpec);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));
        // 此处使用BASE64做转码功能，同时能起到2次加密的作用。
        return Base64.encodeBase64String(encrypted);
    }

    // 解密
    public static String decrypt(String sSrc, String sKey) throws Exception {
        try {
            // 判断Key是否正确
            if (sKey == null) {
//                System.out.print("Key为空null");
                return null;
            }

            // 判断Key是否为16位
            if (sKey.length() != 16) {
//                System.out.print("Key长度不是16位");
                return null;
            }

            byte[] raw = sKey.getBytes("utf-8");
            SecretKeySpec sKeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, sKeySpec);

            //先用base64解密
            byte[] encrypted1 = Base64.decodeBase64(sSrc);
            try {
                byte[] original = cipher.doFinal(encrypted1);
                return new String(original,"utf-8");
            } catch (Exception e) {
                e.printStackTrace();
//                log.info(e.toString());
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            log.info(ex.toString());
            return null;
        }
    }
}