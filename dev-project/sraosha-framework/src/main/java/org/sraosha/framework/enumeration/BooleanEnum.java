package org.sraosha.framework.enumeration;

/**
 * Boolean的枚举类
 */
public enum BooleanEnum {
    TRUE(1, true),
    FALSE(0, false);

    private Integer value;
    private Boolean bool;

    BooleanEnum(Integer value, Boolean bool) {
        this.value = value;
        this.bool = bool;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }
}
