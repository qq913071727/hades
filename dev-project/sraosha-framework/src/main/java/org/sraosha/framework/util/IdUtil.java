package org.sraosha.framework.util;

import cn.ipokerface.snowflake.SnowflakeIdGenerator;
import com.baomidou.mybatisplus.extension.api.R;

import java.util.Random;
import java.util.UUID;

/**
 * 生成唯一Id
 */
public class IdUtil {
    private static SnowflakeIdGenerator idWorker;

    static {
        idWorker = new SnowflakeIdGenerator(0L, 0L);
    }

    public static Long snowflakeNextId() {
        return idWorker.nextId();
    }

    /**
     * 创建UUID
     * @return
     */
    public static String createUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

//    public static void main(String[] args) {
//        for (int i = 0; i < 20; i++) {
//            System.out.println(SnowflakeIdUtils.nextId());
//        }
//    }
}


