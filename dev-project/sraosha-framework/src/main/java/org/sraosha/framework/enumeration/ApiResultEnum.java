package org.sraosha.framework.enumeration;

import org.sraosha.framework.dto.ApiResult;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * ApiResult的枚举类，主要涉及接口响应和用户交互相关的状态
 */
public enum ApiResultEnum {

    /**
     * 3001表示缺少参数
     */
    MISSING_SERVLET_REQUEST_PARAMETER(3001, "缺少参数", false),
    /**
     * 3002表示参数重名
     */
    PARAMETER_DUPLICATE(3002, "参数重名", false),
    /**
     * token不合法或已经过期，需要重新登录；或者header中没有token；或者redis中没有token
     */
    TOKEN_ERROR(3003, "token不合法或已经过期，需要重新登录；或者header中没有token；或者redis中没有token", false),
    /**
     * 3004表示手机号已经重复，需要重新输入
     */
    PHONE_DUPLICATE(3004, "手机号已经重复，需要重新输入", false),
    /**
     * 3005表示邮箱已经重复，需要重新输入
     */
    EMAIL_DUPLICATE(3005, "邮箱已经重复，需要重新输入", false),
    /**
     * 3006表示账号已经重复，需要重新输入
     */
    USERNAME_DUPLICATE(3006, "账号已经重复，需要重新输入", false),
    /**
     * 3007表示验证码错误
     */
    WRONG_VERIFICATION_CODE(3007, "验证码错误，需要重新输入", false),
    /**
     * 3008表示手机获取验证码已经超过五次
     */
    PHONE_VERIFICATION_CODE_MORE_THAN_FIVE_TIMES(3008, "手机获取验证码已经超过五次", false),
    /**
     * 请求的header中没有authorization，或者authorization不合法，或者redis中没有authorization
     */
    AUTHORIZATION_ERROR(3010, "请求的header中没有authorization，或者authorization不合法，或者redis中没有authorization", false),
    /**
     * 3011表示header中的clientId参数错误
     */
    WRONG_CLIENT_ID_IN_HEADER(3011, "header中的clientId参数错误", false),
    /**
     * 3014表示请求的header是空
     */
    EMPTY_HEADER(3014, "请求的header是空", false),
    /**
     * 两个密码不相同
     */
    PASSWORD_NOT_SAME(3015, "两个密码不相同", false),
    /**
     * 登录成功
     */
    LOGIN_SUCCESS(3016, "登录成功", true),
    /**
     * 登录失败
     */
    LOGIN_FAIL(3017, "登录失败", false),
    /**
     * 退出登录成功
     */
    LOGOUT_SUCCESS(3018, "退出登录成功", true),
    /**
     * 退出登录失败
     */
    LOGOUT_FAILS(3019, "退出登录失败", false),
    /**
     * 接口已被限流
     */
    INTERFACE_RESTRICTED(3020, "接口已被限流", false),
    /**
     * 服务已被降级
     */
    SERVICE_DEGRADED(3021, "服务已被降级", false),
    /**
     * 热点参数被限流
     */
    HOTSPOT_PARAMETER_RESTRICTED(3022, "热点参数被限流", false),
    /**
     * 触发系统保护规则
     */
    TRIGGER_SYSTEM_PROTECTION_RULE(3023, "触发系统保护规则", false),
    /**
     * 未被授权,请稍后再试
     */
    UNAUTHORIZED(3024, "未被授权,请稍后再试", false),
    /**
     * 缺少所需的请求正文
     */
    REQUIRED_REQUEST_BODY_IS_MISSING(3025, "缺少所需的请求正文", false),
    /**
     * 方法参数验证失败
     */
    METHOD_ARGUMENT_NOT_VALID(3026, "方法参数验证失败", false),

    /**
     * 数据绑定异常
     */
    BINDING(3027, "数据绑定异常", false),
    /**
     * 单个文件超大异常
     */
    FILE_SIZE_LIMIT_EXCEEDED(3028, "单个文件超大异常", false),
    /**
     * 请求数据超大异常
     */
    SIZE_LIMIT_EXCEEDED(3029, "请求数据超大异常", false),
    /**
     * 缺少所需的请求正文
     */
    HTTP_MESSAGE_NOT_READABLE(3030, "缺少所需的请求正文", false),
    /**
     * 未找到请求的资源
     */
    NO_HANDLER_FOUND(3031, "未找到请求的资源", false),
    /**
     * 请求的header中没有client_id，或者client_id错误
     */
    CLIENT_ID_ERROR(3032, "请求的header中没有client_id，或者client_id错误", false);

    private Integer code;

    private String message;

    private Boolean success;

    ApiResultEnum() {
    }

    ApiResultEnum(Integer code, String message, Boolean success) {
        this.code = code;
        this.message = message;
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    /**
     * 缺少参数异常
     * @param message
     * @return
     */
    public static ResponseEntity missingServletRequestParameter(String message) {
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.MISSING_SERVLET_REQUEST_PARAMETER.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    /**
     * 方法参数验证失败
     * @param message
     * @return
     */
    public static ResponseEntity methodArgumentNotValidException(String message){
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.METHOD_ARGUMENT_NOT_VALID.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    /**
     * 数据绑定异常
     * @param message
     * @return
     */
    public static ResponseEntity bindException(String message){
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.BINDING.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    /**
     * 单个文件超大异常
     * @param message
     * @return
     */
    public static ResponseEntity fileSizeLimitExceededException(String message){
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.FILE_SIZE_LIMIT_EXCEEDED.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    /**
     * 请求数据超大异常
     * @param message
     * @return
     */
    public static ResponseEntity sizeLimitExceededException(String message){
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.SIZE_LIMIT_EXCEEDED.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    /**
     * 缺少所需的请求正文
     * @param message
     * @return
     */
    public static ResponseEntity httpMessageNotReadableException(String message){
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.HTTP_MESSAGE_NOT_READABLE.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    /**
     * 未找到请求的资源
     * @param message
     * @return
     */
    public static ResponseEntity noHandlerFoundException(String message){
        return new ResponseEntity<>(ApiResult.error(ApiResultEnum.NO_HANDLER_FOUND.getCode(), message), HttpStatus.BAD_REQUEST);
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
