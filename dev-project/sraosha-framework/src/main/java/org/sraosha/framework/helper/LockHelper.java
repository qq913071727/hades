package org.sraosha.framework.helper;

/**
 * 分布式锁的帮助类
 */
public class LockHelper {

    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static ThreadLocal<String> getThreadLocal() {
        return threadLocal;
    }
}
