package org.sraosha.framework.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.sraosha.framework.enumeration.ServiceResultEnum;

import java.io.Serializable;

/**
 * 调用服务层的返回对象
 * @param <T>
 */
@Data
@ApiModel(value = "ServiceResult<T>", description = "service返回值对象")
public class ServiceResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Long")
    private Integer code;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    public ServiceResult(Integer code, T data, boolean success, String message) {
        this.code = code;
        this.result = data;
        this.success = success;
        this.message = message;
    }

    public ServiceResult(Integer code, boolean success, String message) {
        this.code = code;
        this.success = success;
        this.message = message;
    }

    public ServiceResult() {
    }

    /**
     * @return
     * @description 成功
     * @parms
     */
//    public static <T> ServiceResult<T> ok(Long code, T data, String message) {
//        return new ServiceResult<T>(code, data, true,  message);
//    }

    /**
     * @return
     * @description 成功 不带返回值
     * @parms
     */
//    public static ServiceResult ok(Long code, String message) {
//        return new ServiceResult(code, null, true,  message);
//    }

    /**
     * @return
     * @description 失败
     * @parms
     */
//    public static <T> ServiceResult<T> error(Long code, String message) {
//        return new ServiceResult<T>(code, null, false,  message);
//    }

    /**
     * 创建成功
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> createSuccess() {
        return new ServiceResult<T>(ServiceResultEnum.SELECT_SUCCESS.getCode(), null, true, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 创建失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> createFail() {
        return new ServiceResult<T>(ServiceResultEnum.SELECT_FAIL.getCode(), null, false, ServiceResultEnum.SELECT_FAIL.getMessage());
    }

    /**
     * 查询成功
     * @param data
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> selectSuccess(T data) {
        return new ServiceResult<T>(ServiceResultEnum.SELECT_SUCCESS.getCode(), data, true, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 查询失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> selectFail() {
        return new ServiceResult<T>(ServiceResultEnum.SELECT_FAIL.getCode(), null, false, ServiceResultEnum.SELECT_FAIL.getMessage());
    }

    /**
     * 插入成功
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> insertSuccess() {
        return new ServiceResult<T>(ServiceResultEnum.INSERT_SUCCESS.getCode(), null, true, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 插入失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> insertFail() {
        return new ServiceResult<T>(ServiceResultEnum.INSERT_FAIL.getCode(), null, false, ServiceResultEnum.INSERT_FAIL.getMessage());
    }

    /**
     * 删除成功
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> deleteSuccess() {
        return new ServiceResult<T>(ServiceResultEnum.DELETE_SUCCESS.getCode(), null, true, ServiceResultEnum.DELETE_SUCCESS.getMessage());
    }

    /**
     * 删除失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> deleteFail() {
        return new ServiceResult<T>(ServiceResultEnum.DELETE_FAIL.getCode(), null, false, ServiceResultEnum.DELETE_FAIL.getMessage());
    }

    /**
     * 更新成功
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> updateSuccess() {
        return new ServiceResult<T>(ServiceResultEnum.UPDATE_SUCCESS.getCode(), null, true, ServiceResultEnum.UPDATE_SUCCESS.getMessage());
    }

    /**
     * 更新失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> updateFail() {
        return new ServiceResult<T>(ServiceResultEnum.UPDATE_FAIL.getCode(), null, false, ServiceResultEnum.UPDATE_FAIL.getMessage());
    }

    /**
     * 操作成功
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> operateSuccess() {
        return new ServiceResult<T>(ServiceResultEnum.OPERATE_SUCCESS.getCode(), null, true, ServiceResultEnum.OPERATE_SUCCESS.getMessage());
    }

    /**
     * 操作失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> operateFail() {
        return new ServiceResult<T>(ServiceResultEnum.OPERATE_FAIL.getCode(), null, false, ServiceResultEnum.OPERATE_FAIL.getMessage());
    }

    /**
     * 登录成功
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> loginSuccess() {
        return new ServiceResult<T>(ServiceResultEnum.LOGIN_SUCCESS.getCode(), null, true, ServiceResultEnum.LOGIN_SUCCESS.getMessage());
    }

    /**
     * 登录失败
     * @return
     * @param <T>
     */
    public static <T> ServiceResult<T> loginFail() {
        return new ServiceResult<T>(ServiceResultEnum.LOGIN_FAIL.getCode(), null, false, ServiceResultEnum.LOGIN_FAIL.getMessage());
    }
}