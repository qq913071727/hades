package org.sraosha.framework.enumeration;

/**
 * 消息服务器中的消息的操作类型
 */
public enum QueueMessageOperationTypeEnum {
    SAVE(1, "save"),
    DELETE(2, "delete"),
    UPDATE(3, "update"),
    SELECT(4, "select"),
    TEST(5, "test");

    private Integer code;

    private String description;

    QueueMessageOperationTypeEnum() {
    }

    QueueMessageOperationTypeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
