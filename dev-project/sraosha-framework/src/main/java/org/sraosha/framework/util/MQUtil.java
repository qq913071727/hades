package org.sraosha.framework.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MQUtil extends StaticAutowiredBeanHelper {


    private static RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void init() {
        rabbitTemplate = super.getRabbitTemplate();
    }

    /**
     * @Description: 发送函数
     * @Params:
     * @Return:
     */
    public static void send(String exchange, String routingKey, String jsonStr) {
        Message message = new Message(jsonStr.getBytes(), new MessageProperties());
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

    /**
     * @Description: 发送json数据
     * @Params:
     * @Return:
     */
    public static void send(String exchange, String routingKey, Object content) {
        send(exchange, routingKey, JSONObject.toJSONString(content));
    }

}


