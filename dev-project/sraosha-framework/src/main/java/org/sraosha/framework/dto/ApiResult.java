package org.sraosha.framework.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 调用API的返回对象
 *
 * @param <T>
 */
@Data
@ApiModel(value = "ApiResult<T>", description = "接口返回值对象")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiResult<T> implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Integer")
    private Integer code;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result = null;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    /**
     * 成功
     * @param message
     * @param data
     * @return
     * @param <T>
     */
    public static <T> ApiResult<T> ok(String message, T data) {
        return new ApiResult<T>(HttpStatus.OK.value(), message, data, true);
    }

    /**
     * 失败
     * @param code
     * @param message
     * @return
     * @param <T>
     */
    public static <T> ApiResult<T> error(Integer code, String message) {
        return new ApiResult<T>(code, message, null, false);
    }

    /**
     * 用于在controller类中转换ServiceResult，并返回给客户端
     * @param serviceResult
     */
    public static ResponseEntity convert(ServiceResult serviceResult){
        ApiResult apiResult = new ApiResult();
        apiResult.code = serviceResult.getCode();
        apiResult.success = serviceResult.getSuccess();
        apiResult.message = serviceResult.getMessage();
        apiResult.result = serviceResult.getResult();
        return ResponseEntity.ok(apiResult);
    }

    /**
     * 成功
     * @param message
     * @param data
     * @param httpStatus
     * @return
     */
    public static ResponseEntity ok(String message, Object data, HttpStatus httpStatus) {
        return new ResponseEntity<>(ok(message, data), httpStatus);
    }

    /**
     * 成功
     * @param serviceResult
     * @param httpStatus
     * @return
     */
    public static ResponseEntity ok(ServiceResult serviceResult, HttpStatus httpStatus) {
        return new ResponseEntity<>(ok(serviceResult.getMessage(), serviceResult.getResult()), httpStatus);
    }

    /**
     * 失败
     * @param code
     * @param message
     * @param httpStatus
     * @return
     */
    public static ResponseEntity error(Integer code, String message, HttpStatus httpStatus) {
        return new ResponseEntity<>(error(code, message), httpStatus);
    }

    /**
     * 失败
     * @param serviceResult
     * @param httpStatus
     * @return
     */
    public static ResponseEntity error(ServiceResult serviceResult, HttpStatus httpStatus) {
        return new ResponseEntity<>(error(serviceResult.getCode(), serviceResult.getMessage()), httpStatus);
    }
}