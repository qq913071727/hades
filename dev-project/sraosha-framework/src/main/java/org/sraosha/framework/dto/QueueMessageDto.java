package org.sraosha.framework.dto;

import org.sraosha.framework.enumeration.QueueMessageOperationTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 消息服务器中的消息对象
 */
@Data
@ApiModel(value = "QueueMessageDto<T>", description = "消息服务器中的消息对象")
public class QueueMessageDto<T> implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "操作类型", dataType = "QueueMessageOperationTypeEnum")
    private QueueMessageOperationTypeEnum operationType;

    @ApiModelProperty(value = "类类型", dataType = "Integer")
    private Integer classType;

    @ApiModelProperty(value = "数据", dataType = "T")
    private T data;

    public QueueMessageDto() {
    }

    public QueueMessageDto(QueueMessageOperationTypeEnum operationType, Integer classType, T data) {
        this.operationType = operationType;
        this.classType = classType;
        this.data = data;
    }
}
