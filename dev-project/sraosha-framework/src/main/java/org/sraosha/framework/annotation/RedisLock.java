package org.sraosha.framework.annotation;

import java.lang.annotation.*;

/**
 * 如果waitTime>任务执行的时间，则最终可以等到锁
 * 如果waitTime<任务执行的时间，则最终获得锁失败
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {
    /**
     * redis锁key的前缀
     * @return
     */
    String lockedKey() default "";

    /**
     * key在redis里存在的时间，单位：秒
     * @return
     */
    long expireTime() default 10;

    /**
     * 是否在方法执行完成之后释放锁
     * @return
     */
    boolean release() default true;

    /**
     * 获取锁的最大等待时间，单位：秒，默认不等待，0即为快速失败
     * @return
     */
    long waitTime() default 0;

    /**
     * 是否抛出异常，默认不抛出
     * @return
     */
    boolean throwException() default false;
}
