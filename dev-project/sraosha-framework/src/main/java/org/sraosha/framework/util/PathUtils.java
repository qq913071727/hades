package org.sraosha.framework.util;

public class PathUtils {

    /**
     * 获取文件名后缀
     */
    public static String getFileExtension(String fileName) {
        if (fileName != null && !fileName.isEmpty() && fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    /**
     * 从url中获取文件名(带扩展名)
     */
    public static String getFileNameWithExtensionFromUrl(String url) {
        if (url != null && !url.isEmpty() && url.lastIndexOf("/") != -1) {
            String[] array = url.split("/");
            return array[array.length - 1];
        } else {
            return "";
        }
    }

}
