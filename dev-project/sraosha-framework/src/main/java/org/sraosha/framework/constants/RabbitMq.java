package org.sraosha.framework.constants;

/**
 * rabbitMq相关常量参数
 */
public class RabbitMq {

    /**
     * 队列类型
     */
    public static final String DIRECT_TYPE = "direct";

    /**
     * 操作名字
     */
    public static final String MESSAGE_NAME = "MessageName";

    /**
     * 数据
     */
    public static final String MESSAGE_BODY = "MessageBody";
}