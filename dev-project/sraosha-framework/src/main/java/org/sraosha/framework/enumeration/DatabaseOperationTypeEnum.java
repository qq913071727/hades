package org.sraosha.framework.enumeration;

/**
 * 数据库操作类型
 */
public enum DatabaseOperationTypeEnum {

    /**
     * 增
     */
    INSERT("INSERT"),
    /**
     * 删
     */
    DELETE("DELETE"),
    /**
     * 改
     */
    UPDATE("UPDATE"),
    /**
     * 查
     */
    SELECT("SELECT");

    /**
     * 操作类型
     */
    private String operationType;

    DatabaseOperationTypeEnum() {
    }

    DatabaseOperationTypeEnum(String operationType) {
        this.operationType = operationType;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }
}