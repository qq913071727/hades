package org.sraosha.framework.constants;

import java.util.UUID;

/**
 * redis存储时使用的常量
 */
public class RedisStorage {

    /**
     * client表
     */
    public static final String CLIENT = "CLIENT";

    /**
     * 存储在redis中的token的前缀
     */
    public static final String TOKEN_PREFIX = "TOKEN:";

    /**
     * 存储在redis中的表的前缀
     */
    public static final String TABLE = "TABLE:";

    /**
     * 存储在redis中，user表按照create_date字段降序排列
     */
    public static final String CREATE_DATE_DESC = "CREATE_DATE_DESC";

    /**
     * 存储在redis中，user表按照create_date字段升序排列
     */
    public static final String CREATE_DATE_ASC = "CREATE_DATE_ASC";

    /**
     * 分布式锁的key的前缀
     */
    public static final String LOCK_KEY_PREFIX = "LOCK:";

    /**
     * 存储在redis中的冒号
     */
    public static final String COLON = ":";

    /**
     * 在redis中模糊查询时的*
     */
    public static final String ASTERISK = "*";

    /**
     * 数据库中没有对应的数据，但redis中仍然需要有个占位符
     */
    public static final String EMPTY_VALUE = "empty_value";
}
