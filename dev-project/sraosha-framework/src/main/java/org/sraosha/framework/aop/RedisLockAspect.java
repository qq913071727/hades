package org.sraosha.framework.aop;

import com.alibaba.cloud.commons.lang.StringUtils;
import org.sraosha.framework.annotation.RedisLock;
import org.sraosha.framework.manager.RedisManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * redis分布式锁
 */
@Aspect
public class RedisLockAspect {

    private static final Log log = LogFactory.getLog(RedisLockAspect.class);

    @Resource
    private RedisManager redisManager;

    /**
     * 分布式锁的key
     */
    private static final String KEY_PREFIX_LOCK = "REDIS_LOCK:";

    /**
     * 最小等待单位时间
     */
    private static final long MIN_WAIT_MILL = 300;

    @Around("@annotation(com.common.redis.annotation.RedisLock)")
    public void cacheLockPoint(ProceedingJoinPoint pjp) throws Exception {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method cacheMethod = signature.getMethod();
        if (null == cacheMethod) {
            log.info("未获取到使用方法pjp: {" + pjp + "}");
            return;
        }
        String lockKey = cacheMethod.getAnnotation(RedisLock.class).lockedKey();
        if (StringUtils.isBlank(lockKey)) {
            log.error("method:{" + cacheMethod + "}, 锁名称为空！");
            return;
        }

        //锁占用时间
        long timeOut = cacheMethod.getAnnotation(RedisLock.class).expireTime();
        //等待时间
        long waitTime = cacheMethod.getAnnotation(RedisLock.class).waitTime();
        //是否抛出异常
        boolean throwException = cacheMethod.getAnnotation(RedisLock.class).throwException();
        //是否释放锁
        boolean release = cacheMethod.getAnnotation(RedisLock.class).release();
        //是否有锁
        boolean haveLock = false;

        long startTime = System.currentTimeMillis();
        long endTime = System.currentTimeMillis() + waitTime * 1000;

        try {
            do {
                if (redisManager.setNx(KEY_PREFIX_LOCK + lockKey, lockKey, timeOut, TimeUnit.SECONDS)) {
                    redisManager.expire(KEY_PREFIX_LOCK + lockKey, timeOut, TimeUnit.SECONDS);
                    log.info("method:{" + Thread.currentThread() + "} 获取锁:{" + (KEY_PREFIX_LOCK + lockKey) + "},开始运行！");
                    pjp.proceed();
                    haveLock = true;
                    return;
                }
                log.info("method:{" + Thread.currentThread() + "} 未获取锁:{" + (KEY_PREFIX_LOCK + lockKey) + "},开始等待！");
                Thread.sleep(Math.min(MIN_WAIT_MILL, waitTime * 100));
            } while (System.currentTimeMillis() <= endTime);

            log.info("获得锁失败,放弃等待,之前共等待{" + (System.currentTimeMillis() - startTime) + "}ms,方法将不执行,方法名为{" + cacheMethod + "}");
            if (throwException) {
                throw new Exception("等待锁失败，未正常执行方法！");
            }

        } catch (Throwable e) {
            log.error("method:{" + cacheMethod + "},运行错误！", e);
            if (throwException) {
                throw new Exception("获取锁失败!");
            }
        } finally {
            if (release && haveLock) {
                log.info("method:{" + Thread.currentThread() + "} 执行完成释放锁:{" + (KEY_PREFIX_LOCK + lockKey) + "}");
                redisManager.del(KEY_PREFIX_LOCK + lockKey);
            }
        }
    }
}
