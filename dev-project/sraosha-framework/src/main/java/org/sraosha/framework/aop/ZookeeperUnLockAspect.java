package org.sraosha.framework.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

@Slf4j
@Aspect
public class ZookeeperUnLockAspect extends AbstractAspect {

    @Pointcut("@annotation(org.sraosha.framework.annotation.ZookeeperUnlock)")
    public void doUnlock() {
        log.trace("进入ZookeeperUnLockAspect类的doUnlock方法");
    }

    /******************************************** 解锁 *******************************************/
    /**
     * 进入方法前第一个执行
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("doUnlock()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        log.trace("进入ZookeeperUnLockAspect类的around方法");

        return joinPoint.proceed();
    }

    /**
     * 进入方法前第二个执行
     *
     * @param joinPoint
     */
    @Before("doUnlock()")
    public void doBeforeAdvice(JoinPoint joinPoint) {
        log.trace("进入ZookeeperUnLockAspect类的doBeforeAdvice方法");
    }

    /**
     * 方法结束前第一个执行
     *
     * @param ret
     */
    @AfterReturning(returning = "ret", pointcut = "doUnlock()")
    public void doAfterReturning(Object ret) {
        log.trace("进入ZookeeperUnLockAspect类的doAfterReturning方法");
    }

    /**
     * @param jp
     */
    @AfterThrowing("doUnlock()")
    public void throwException(JoinPoint jp) {
        log.trace("进入ZookeeperUnLockAspect类的throwException方法");
    }


    /**
     * 方法结束前第二个执行
     *
     * @param jp
     */
    @After("doUnlock()")
    public void after(JoinPoint jp) {
        log.trace("进入ZookeeperUnLockAspect类的after方法");
    }
}
