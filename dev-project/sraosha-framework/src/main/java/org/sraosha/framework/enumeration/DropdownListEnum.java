package org.sraosha.framework.enumeration;

/**
 * 下拉列表的枚举类
 */
public enum DropdownListEnum {
    ALL(0, "全选");

    private Integer value;
    private String message;

    DropdownListEnum(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
