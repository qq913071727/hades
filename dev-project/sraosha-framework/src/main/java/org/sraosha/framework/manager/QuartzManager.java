package org.sraosha.framework.manager;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class QuartzManager {

    /**
     * 调度器
     */
    private Scheduler scheduler;

    public QuartzManager(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * 创建定时任务 定时任务创建之后默认启动状态
     * @param jobId
     * @param className
     * @param jobName
     * @param cronExpression
     */
    public void createScheduleJob(String jobId, String className, String jobName, JobDataMap jobDataMap, String cronExpression) {
        try {
            //获取到定时任务的执行类  必须是类的绝对路径名称
            //定时任务类需要是job类的具体实现 QuartzJobBean是job的抽象类。
            Class<? extends Job> jobClass = (Class<? extends Job>) Class.forName(className);
            // 构建定时任务信息 使用id作为任务唯一值，在执行时可以拿到。
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobId).withDescription(jobName).usingJobData(jobDataMap).build();
            // 设置定时任务执行方式
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
            // 构建触发器trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(jobId).withSchedule(scheduleBuilder).build();
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据任务Id暂停定时任务
     *
     * @param jobId     定时任务名称
     * @throws SchedulerException
     */
    public void pauseScheduleJob(String jobId) {
        JobKey jobKey = JobKey.jobKey(jobId);
        try {
            scheduler.pauseJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据任务Id恢复定时任务
     *
     * @param jobId     定时任务id
     * @throws SchedulerException
     */
    public void resumeScheduleJob(String jobId) {
        JobKey jobKey = JobKey.jobKey(jobId);
        try {
            scheduler.resumeJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }


    /**
     * 根据任务id立即运行一次定时任务
     *
     * @param jobId     定时任务id
     * @throws SchedulerException
     */
    public void runOnce(String jobId) {
        JobKey jobKey = JobKey.jobKey(jobId);
        try {
            scheduler.triggerJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新定时任务
     * @param jobId
     * @param cronExpression
     */
    public void updateScheduleJob(String jobId, String cronExpression) {
        try {
            //获取到对应任务的触发器
            TriggerKey triggerKey = TriggerKey.triggerKey(jobId);
            //设置定时任务执行方式
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
            //重新构建任务的触发器trigger
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
            //重置对应的job
            scheduler.rescheduleJob(triggerKey, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据定时任务id从调度器当中删除定时任务
     *
     * @param jobId     定时任务名称
     */
    public void deleteScheduleJob(String jobId) {
        JobKey jobKey = JobKey.jobKey(jobId);
        try {
            scheduler.deleteJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

}
