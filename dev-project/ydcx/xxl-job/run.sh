# B1B产品数据导入（开发环境）部署、启动jar
mvn install -f xxl-job-admin/src/main/resources/b1b-data-dev/deploy-b1b-data-dev.xml -Pb1b-data-dev

# B1B产品数据导入（生产环境）部署、启动jar
mvn install -f xxl-job-admin/src/main/resources/b1b-data-prod/deploy-b1b-data-prod.xml -Pb1b-data-prod

# 供应商协同（测试环境）部署、启动jar
mvn install -f xxl-job-admin/src/main/resources/sc-test/deploy-sc-test.xml -Ptest