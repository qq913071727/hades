export const tableColumn = [
    {
        prop: "name",
        label: "收货人",
    },
    {
        prop: "area",
        label: "所在地区",
    },
    {
        prop: "address",
        label: "详细地址",
    },
    {
        prop: "tel",
        label: "电话/手机号",
    },
    {
        prop: "email",
        label: "邮箱",
    },
];

export const tableCellWidth = [
    {
        prop: "name",
        // width: "200",
    },
    {
        prop: "area",
        // width: "200",
    },
    {
        prop: "address",
        // width: "200",
    },
    {
        prop: "tel",
        // width: "200",
    },
    {
        prop: "email",
        // width: "200",
    },
]

export const tableCellAlign = [
    {
        prop: "name",
        align: "center",
    },
    {
        prop: "area",
        align: "center",
    },
    {
        prop: "address",
        align: "center",
    },
    {
        prop: "tel",
        align: "center",
    },
    {
        prop: "email",
        align: "center",
    },
]

export const tableShowTooltip = [
    {
        prop: "name",
        showTooltip: false,
    },
    {
        prop: "area",
        showTooltip: false,
    },
    {
        prop: "address",
        showTooltip: false,
    },
    {
        prop: "tel",
        showTooltip: false,
    },
    {
        prop: "email",
        showTooltip: false,
    },
]


export const tableData = [
    {
        Key: "李好好",
        ID: "F825480F-C76B-42BE-A63C-60D5DB06D471",
        name: "李好好",
        area: "北京市海淀区",
        address: "北京市海淀区xxx街道xxxxx大厦xx号楼xx层xx号",
        tel: "18800008888",
        email:'lihaohao@360.com'
    },
    {
        Key: "李好好",
        ID: "972B99F5-680E-4FE9-9678-CA0BED6A162B",
        name: "李好好",
        area: "北京市海淀区",
        address: "北京市海淀区xxx街道xxxxx大厦xx号楼xx层xx号",
        tel: "18800008888",
        email:'lihaohao@360.com'
    },
    {
        Key: "李好好",
        ID: "A00A24F2-CC9A-4E09-9018-CE48E44D1DF8",
        name: "李好好",
        area: "北京市海淀区",
        address: "北京市海淀区xxx街道xxxxx大厦xx号楼xx层xx号",
        tel: "18800008888",
        email:'lihaohao@360.com'
    },
    {
        Key: "李好好",
        ID: "B620DECB-B3CF-4DC5-9776-C03718C2E4E6",
        name: "李好好",
        area: "北京市海淀区",
        address: "北京市海淀区xxx街道xxxxx大厦xx号楼xx层xx号",
        tel: "18800008888",
        email:'lihaohao@360.com'
    },
]