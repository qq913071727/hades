export const btnList = [
  {
    type: 1,
    name: "一键采纳",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "accept",
  },
  {
    type: 1,
    name: "生成订单",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "createOrder",
  },
  {
    type: 1,
    name: "再次询价",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "activePrice",
  },
  {
    type: 1,
    name: "议价",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "dicker",
  },
  {
    type: 1,
    name: "不接受",
    bgColor: "rgba(217, 84, 84, 1)",
    color: "#ffffff",
    fuc: "noAccept",
  },


  {
    type: 2,
    name: "生成订单",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "createOrder",
  },
  {
    type: 2,
    name: "再次询价",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "activePrice",
  },
  {
    type: 2,
    name: "议价",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "dicker",
  },


  {
    type: 3,
    name: "再次询价",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "activePrice",
  },

  {
    type: 4,
    name: "删除",
    bgColor: "rgba(217, 84, 84, 1)",
    color: "#ffffff",
    fuc: "delete",
  },
]



export const columnList = [
  { id: 1, label: "状态", prop: "status", isShow: true },
  { id: 2, label: "剩余确认时间", prop: "remainTime", isShow: true },
  { id: 3, label: "型号", prop: "model", isShow: true },
  { id: 4, label: "品牌", prop: "brand", isShow: true },
  { id: 5, label: "数量", prop: "count", isShow: true },
  { id: 6, label: "批次", prop: "batch", isShow: true },
  { id: 7, label: "封装", prop: "encapsulation", isShow: true },
  { id: 8, label: "币种", prop: "currency", isShow: true },
  { id: 9, label: "接受价", prop: "price", isShow: true },
  { id: 10, label: "报价", prop: "quotedPrice", isShow: true },
  { id: 11, label: "交货期", prop: "deliveryDate", isShow: true },
  { id: 12, label: "交货地", prop: "address", isShow: true },
  { id: 13, label: "MPQ", prop: "MPQ", isShow: true },
  { id: 14, label: "包装", prop: "packing", isShow: true },
  { id: 15, label: "报价日期", prop: "quotedDate", isShow: true },
  { id: 16, label: "备注", prop: "remark", isShow: true },
]
