export const btnList = [
  {
    name: "再来一单",
    bgColor: "rgba(62, 151, 149, 1)",
    color: "#ffffff",
    fuc: "again",
  },
  {
    name: "添加",
    bgColor: "rgba(68, 42, 141, 1)",
    color: "#ffffff",
    fuc: "add",
  },
  {
    name: "删除",
    bgColor: "rgba(217, 84, 84, 1)",
    color: "#ffffff",
    fuc: "del",
  },
  {
    name: "去询价",
    bgColor: "rgba(62, 151, 149, 1)",
    color: "#ffffff",
    fuc: "goEnquiry",
  },
  {
    name: "确认",
    bgColor: "rgba(62, 151, 149, 1)",
    color: "#ffffff",
    fuc: "confirm",
  },
  {
    name: "修改截至时间",
    bgColor: "rgba(62, 151, 149, 1)",
    color: "#ffffff",
    fuc: "editTime",
  },
]
