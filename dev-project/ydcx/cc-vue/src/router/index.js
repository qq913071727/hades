import Vue from 'vue'
import Router from 'vue-router'

import demo from '@/views/other/demo' // 调试页

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    redirect: "/login"
  },
  {
    path: "/login",
    name: 'Login',
    component: () =>
      import('@/views/login') // 登录页
  },
  {
    path: "/home",
    name: 'home',
    component: () =>
      import('@/views/home'), // 主页
    children: [

      // =================================================
      {
        path: "index",
        name: 'index',
        component: () =>
          import('@/views/index'), // 首页
        meta: { title: '首页' }
      },
      {
        path: "homeDemo",
        name: 'homeDemo',
        component: () =>
          import('@/views/home/home-demo'), // 主页-表格测试页面
        meta: { title: '订单管理' }
      },
      // ======= 系统管理 =======
      {
        path: "information",
        name: 'information',
        component: () =>
          import('@/views/system/information'), // 基本信息
        meta: { title: '基本信息' }
      },
      {
        path: "accountSecurity",
        name: 'accountSecurity',
        component: () =>
          import('@/views/system/accountSecurity'), // 账号安全
        meta: { title: '账号安全' }
      },
      {
        path: "accountMaMnagement",
        name: 'accountMaMnagement',
        component: () =>
          import('@/views/system/accountMaMnagement'), // 账号管理
        meta: { title: '账号管理' }
      },
      {
        path: "roleManagement",
        name: 'roleManagement',
        component: () =>
          import('@/views/system/roleManagement'), // 角色管理
        meta: { title: '角色管理' }
      },
      {
        path: "meCompanyManagement",
        name: 'meCompanyManagement',
        component: () =>
          import('@/views/system/meCompanyManagement'), // 我的公司管理
        meta: { title: '我的公司管理' }
      },
      {
        path: "companyDetail",
        name: 'companyDetail',
        component: () => import('@/views/system/meCompanyManagement/detail'), // 公司详情
        meta: { title: '公司详情' }
      },
      {
        path: "companyEdit",
        name: 'companyEdit',
        component: () => import('@/views/system/meCompanyManagement/edit'), // 公司修改
        meta: { title: '公司编辑' }
      },
      {
        path: "addressManagement",
        name: 'addressManagement',
        component: () =>
          import('@/views/system/addressManagement'), // 地址管理
        meta: { title: '地址管理' }
      },
      {
        path: "systemAnnouncement",
        name: 'systemAnnouncement',
        component: () =>
          import('@/views/system/systemAnnouncement'), // 系统公告
        meta: { title: '系统公告' }
      },
      {
        path: "systemAnnouncement/:id",
        name: 'systemAnnouncementDetail',
        component: () =>
          import('@/views/system/systemAnnouncement/detail'), // 系统公告详情
        meta: { title: '系统公告详情' }
      },
      {
        path: "messagePrompt",
        name: 'messagePrompt',
        component: () =>
          import('@/views/system/messagePrompt'), // 消息提示
        meta: { title: '消息提醒' }
      },
      {
        path: "messagePrompt/:type/:id",
        name: 'messagePromptDetail',
        component: () =>
          import('@/views/system/messagePrompt/detail'), // 消息提示
        meta: { title: '消息详情' }
      },
      // ======= 询价管理 =======
      {
        path: "publishEnquiry",
        name: 'publishEnquiry',
        component: () =>
          import('@/views/enquiry/publishEnquiry/index'), // 发布询价
        meta: { title: '发布询价' }
      },
      {
        path: "addEnquiry",
        name: 'addEnquiry',
        component: () =>
          import('@/views/enquiry/publishEnquiry/addPrice'), // 新增询价
        meta: { title: '新增询价' }
      },
      {
        path: "detailEnquiry",
        name: 'detailEnquiry',
        component: () =>
          import('@/views/enquiry/publishEnquiry/detail'), // 询价详情
        meta: { title: '询价详情' }
      },
      {
        path: "receiveQuotedPrice",
        name: 'receiveQuotedPrice',
        component: () =>
          import('@/views/enquiry/receiveQuotedPrice/index'), // 我收到的报价
        meta: { title: '我收到的报价' }
      },
      {
        path: "quotedPriceDetail",
        name: 'quotedPriceDetail',
        component: () =>
          import('@/views/enquiry/receiveQuotedPrice/detail'), // 报价详情
        meta: { title: '报价详情' }
      },
      {
        path: "createOrder",
        name: 'createOrder',
        component: () =>
          import('@/views/enquiry/receiveQuotedPrice/createOrder'), // 生成订单
        meta: { title: '生成订单' }
      },
      {
        path: "myDicker",
        name: 'myDicker',
        component: () =>
          import('@/views/enquiry/myDicker/index'), // 我的议价
        meta: { title: '我的议价' }
      },
      {
        path: "validPrice",
        name: 'validPrice',
        component: () =>
          import('@/views/enquiry/validPrice/index'), //  有效报价单
        meta: { title: '有效报价单' }
      },
      // ======= 售后管理aftersales =======
      {
        path: "replacement",
        name: 'replacement',
        component: () =>
          import('@/views/aftersales/replacement'), // 退换补货管理
        meta: { title: '退换补货管理' }
      },
      {
        path: "inventory",
        name: 'inventory',
        component: () =>
          import('@/views/stock/index.vue'), // 库存查询
        meta: { title: '库存查询' }
      },
      // ======= 货物管理 =======
      {
        path: "transitGoods",
        name: 'transitGoods',
        component: () => import('@/views/transit/transitGoods'),
        meta: { title: '在途货物' }
      },
      {
        path: "myReceipt",
        name: 'myReceipt',
        component: () => import('@/views/transit/myReceipt'),
        meta: { title: '我的收货单' }
      },
      {
        path: 'transiListDetail',
        name: 'transiListDetail',
        component: () => import('@/views/transit/transitGoods/transiListDetail'),
        meta: { title: '在途货物详情' }
      },
      {
        path: 'myReceiptDetail',
        name: 'myReceiptDetail',
        component: () => import('@/views/transit/myReceipt/myReceiptDetail'),
        meta: { title: '我的收货单详情' }
      },
    ]
  },
  {
    path: "*",
    name: '404',
    component: () =>
      import('@/views/other/404.vue') // 404页面
  },
  ]
})
