import { addResizeListener, removeResizeListener } from 'element-ui/src/utils/resize-event'
import Vue from 'vue'

const doResize = async(el, binding, vnode, ) => {
    const { componentInstance: $table } = await vnode
    const { value } = binding
    if (!$table.height) {
        throw new Error(`el-$table must set the height. Such as height='100px'`)
    }
    const bottomOffset = (value && value.bottomOffset) || 30
    if (!$table) return
    const height = window.innerHeight - el.getBoundingClientRect().top - bottomOffset
    $table.layout.setHeight(height)
    $table.doLayout()
}
const preventReClick = Vue.directive('preventReClick', {
    // 防止按钮重复点击
    inserted: function (el, binding) {
        el.addEventListener('click', () => {
            if (!el.disabled) {
                el.disabled = true
                setTimeout(() => {
                    el.disabled = false
                }, binding.value || 3000)
            }
        })
    }
});
export default {

    async bind(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            el.resizeListener = async() => {
                    await doResize(el, binding, vnode)
                }
                // el.resizeListener2 = async() => {

            //     await doResize(el, binding, vnode)
            // }
            // addResizeListener(el, el.resizeListener1)
            addResizeListener(window.document.body, el.resizeListener)
        }
    },
    async inserted(el, binding, vnode) {

        if (binding.value.bottomOffset) {
            await doResize(el, binding, vnode)
        }

    },
    async componentUpdated(el, binding, vnode) {

        if (binding.value.bottomOffset) {
            await doResize(el, binding, vnode)
        }
    },
    unbind(el) {
        removeResizeListener(el, el.resizeListener)
    },

}
export { preventReClick }