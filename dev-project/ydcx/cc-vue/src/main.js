import Vue from 'vue';
import router from './router';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import "@/assets/scss/index.scss";
import "@/assets/iconfont/index.css";
import directive from './directive' //directive

import "lib-flexible";

import store from "./store";

import App from './App';
Vue.use(ElementUI);

Vue.use(directive)

Vue.prototype.$store = store;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
