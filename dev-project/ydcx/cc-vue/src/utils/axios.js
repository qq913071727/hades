import axios from "axios";

const Api = axios.create({
    baseURL: '',
    timeout: 1000,
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    }
});


// 请求拦截
Api.interceptors.request.use(function (config) {
    console.log(config)
    return config;
}, function (error) {
    return Promise.reject(error);
});


// 响应拦截
Api.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error);
});


export default Api;