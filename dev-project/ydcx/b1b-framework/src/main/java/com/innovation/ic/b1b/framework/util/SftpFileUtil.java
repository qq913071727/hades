//package com.innovation.ic.b1b.framework.util;
//
//import com.jcraft.jsch.*;
////import org.slf4j.Logger;
////import org.slf4j.LoggerFactory;
//import java.io.*;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.ListIterator;
//
///**
// * @desc   连接sftp服务器上传文件
// * @author linuo
// * @time   2022年6月24日13:18:14
// */
//public class SftpFileUtil {
////    private static final Logger LOGGER = LoggerFactory.getLogger(SftpFileUtil.class);
//
//    /**
//     * 上传文件
//     *
//     * @param directory 图片上传路径
//     * @param bytes     需要转换成文件的byte数组
//     * @param sftp      sftp连接
//     * @param fileName  文件名
//     */
//    public static void upload(String directory, byte[] bytes, ChannelSftp sftp, String fileName) {
//        InputStream io = null;
////        LOGGER.info("上传文件到:[{}]目录下,文件名为:[{}]", directory, fileName);
//        try {
//            String home = sftp.getHome();
//            String pwd = sftp.pwd();
//            if(!pwd.equals(directory)){
//                sftp.cd(directory);
//            }
//            io = new ByteArrayInputStream(bytes);
//            sftp.put(io, fileName);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (null != io) {
//                try {
//                    io.close();
//                    io = null;
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    /**
//     * 复制文件
//     *
//     * @param directory   源路径
//     * @param destination 目标路径
//     * @param fileName    文件名
//     */
//    public static void copy(String directory, String destination, String fileName, ChannelExec channelExec) throws JSchException, IOException {
////        LOGGER.info("复制文件到:[{}]目录下,文件名为:[{}],源[{}]目录", destination, fileName, directory);
////        //正常命令
////        StringBuilder runLog = new StringBuilder("");
////        //错误命令
////        StringBuilder errLog = new StringBuilder("");
////        //记录命令
////        BufferedReader inputStreamReader = null;
////        //记录命令错误
////        BufferedReader errInputStreamReader = null;
//        // Execute a command on the remote machine.
//        try {
////            synchronized (SftpFileUtil.class){
////            LOGGER.info("当前执行图片是:" + fileName);
//            String dos = "mv " + directory + "/" + fileName + " " + destination;
////            LOGGER.info("当前执行命令是:" + dos);
////            LOGGER.info("输出 shell 命令执行日志:" + "exitStatus=" + channelExec.getExitStatus() + ", openChannel.isClosed="
////                    + channelExec.isClosed());
//            channelExec.setCommand(dos);
//            channelExec.connect(); // 执行命令
////            }
//
////            //记录命令执行 log
////            String line = null;
////            while ((line = inputStreamReader.readLine()) != null) {
////                runLog.append(line).append("\n");
////            }
////            //记录命令执行错误 log
////            String errLine = null;
////            while ((errLine = errInputStreamReader.readLine()) != null) {
////                errLog.append(errLine).append("\n");
////            }
////            channelExec.setCommand("");
////            channelExec.disconnect();
////            LOGGER.info("输出 shell 命令执行日志:" + "exitStatus=" + channelExec.getExitStatus() + ", openChannel.isClosed="
////                    + channelExec.isClosed());
////            LOGGER.info("命令执行完成，执行日志如:" + runLog.toString());
////            LOGGER.info("命令执行完成，执行错误日志如:" + errLog.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeChannel(channelExec);
//        }
//// finally {
////            try {
////                if (inputStreamReader != null) {
////                    inputStreamReader.close();
////                }
////                if (errInputStreamReader != null) {
////                    errInputStreamReader.close();
////                }
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
////        }
//    }
//
//    /**
//     * 执行脚本
//     * @param session session
//     * @param directory 脚本路径
//     * @param fileName 脚本名称
//     */
//    public static String execScript(Session session, String directory, String fileName) {
//        ChannelExec channelExec = null;
//        BufferedReader inputStreamReader = null;
//        StringBuilder runLog = new StringBuilder();
//        try {
//            // 1. 组装执行脚本
//            String cmd = "sh " + directory + "/" + fileName;
//
//            // 2. 通过 exec 方式执行 shell 命令
//            channelExec = (ChannelExec) session.openChannel("exec");
//            channelExec.setCommand(cmd);
//            // 3. 执行命令
//            channelExec.connect();
//
//            // 4. 获取标准输入流
//            inputStreamReader = new BufferedReader(new InputStreamReader(channelExec.getInputStream()));
//
//            // 5. 记录命令执行 log
//            String line;
//            while ((line = inputStreamReader.readLine()) != null) {
//                runLog.append(line).append("\n");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (inputStreamReader != null) {
//                    inputStreamReader.close();
//                }
//                closeChannel(channelExec);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return runLog.toString();
//    }
//
//    /**
//     * 关闭链接
//     */
//    public static void closeChannel(ChannelExec channelExec) {
//        if (channelExec != null) {
//            try {
//                channelExec.disconnect();
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public static void closeSession(Session session) {
//        if (session != null) {
//            session.disconnect();
//        }
//    }
//
//    static boolean deleteDirFiles(String newsFile, ChannelSftp sftp) {
//        try {
//            sftp.cd(newsFile);
//            ListIterator a = sftp.ls(newsFile).listIterator();
//            while (a.hasNext()) {
//                ChannelSftp.LsEntry oj = (ChannelSftp.LsEntry) a.next();
//                delete(newsFile, oj.getFilename(), sftp);
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return false;
//    }
//
//    /**
//     * 上传本地文件到sftp指定的服务器，
//     *
//     * @param directory      目标文件夹
//     * @param uploadFile     本地文件夹
//     * @param sftp           sftp地址
//     * @param remoteFileName 重命名的文件名字
//     * @param isRemote       是否需要重命名  是true 就引用remoteFileName 是false就用默认的文件名字
//     */
//    public static void upload(String directory, String uploadFile, ChannelSftp sftp, String remoteFileName, boolean isRemote) {
//        FileInputStream io = null;
//        try {
//            boolean isExist = false;
//            try {
//                SftpATTRS sftpATTRS = sftp.lstat(directory);
//                isExist = true;
//                isExist = sftpATTRS.isDir();
//            } catch (Exception e) {
//                e.printStackTrace();
//                if (e.getMessage().toLowerCase().equals("no such file")) {
//                    isExist = false;
//                }
//            }
//            if (!isExist) {
//                boolean existDir = isExistDir(directory, sftp);
//                if (!existDir) {
//                    String pathArry[] = directory.split("/");
//                    StringBuffer Path = new StringBuffer("/");
//                    for (String path : pathArry) {
//                        if (path.equals("")) {
//                            continue;
//                        }
//                        Path.append(path + "/");
//                        if (!isExistDir(Path + "", sftp)) {
//                            // 建立目录
//                            sftp.mkdir(Path.toString());
//                            // 进入并设置为当前目录
//                        }
//                        sftp.cd(Path.toString());
//                    }
//                }
//            }
//            sftp.cd(directory);
//            File file = new File(uploadFile);
//            io = new FileInputStream(file);
//            if (isRemote) {
//                sftp.put(io, remoteFileName);
//            } else {
//                sftp.put(io, file.getName());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (null != io) {
//                try {
//                    io.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//
//    public static boolean isExistDir(String path, ChannelSftp sftp) {
//        boolean isExist = false;
//        try {
//            SftpATTRS sftpATTRS = sftp.lstat(path);
//            isExist = true;
//            return sftpATTRS.isDir();
//        } catch (Exception e) {
//            e.printStackTrace();
//            if (e.getMessage().toLowerCase().equals("no such file")) {
//                isExist = false;
//            }
//        }
//        return isExist;
//
//    }
//
//    /**
//     * 上传
//     */
//    public static List<String> uploadZip(String directory, String uploadFile, ChannelSftp sftp, List<String> filePath, String remoteName) {
//        try {
//            List<String> list = new ArrayList<>();
//            boolean existDir = isExistDir(directory, sftp);
//            if (!existDir) {
//                sftp.mkdir(directory);
//            }
//            sftp.cd(directory);
//            int i = 1;
//            for (String newPath : filePath) {
//                FileInputStream io = null;
//                try {
//                    File file = new File(uploadFile + newPath);
//                    io = new FileInputStream(file);
//                    sftp.put(io, remoteName + "-" + i + ".jpg");
//                    io.close();
//                    list.add(remoteName + "-" + i + ".jpg");
//                    i++;
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                } finally {
//                    if (null != io) {
//                        try {
//                            io.close();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//            return list;
//        } catch (SftpException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * 下载
//     */
//    public static void download(String directory, String downloadFile, String saveFile, ChannelSftp sftp) {
//        try {
//            sftp.cd(directory);
//            File file = new File(saveFile);
//            sftp.get(downloadFile, new FileOutputStream(file));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 查看
//     */
//    public static List<String> check(String directory, ChannelSftp sftp) {
//        List<String> fileList = new ArrayList<>();
//        try {
//            sftp.cd(directory);
//            ListIterator a = sftp.ls(directory).listIterator();
//            while (a.hasNext()) {
//                ChannelSftp.LsEntry oj = (ChannelSftp.LsEntry) a.next();
//                System.out.println(oj.getFilename());
//                //fileList.add((String) a.next());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return fileList;
//    }
//
//    /**
//     * 删除
//     */
//    public static void delete(String directory, String deleteFile, ChannelSftp sftp) {
//        try {
//            sftp.cd(directory);
//            sftp.rm(deleteFile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}