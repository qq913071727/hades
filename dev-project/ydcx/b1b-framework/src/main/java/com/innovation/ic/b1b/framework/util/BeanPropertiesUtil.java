package com.innovation.ic.b1b.framework.util;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 转换工具类
 * @ClassName: BeanPropertiesUtil
 * @Author: myq
 * @Date: 2022/8/5 下午3:54
 * @Version: 1.0
 */
public class BeanPropertiesUtil {

    /**
     * @return
     * @description obj转换工具
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/5 下午3:59
     */
    public static <T> T convert(Object source, Class<T> targetClazz) {
        T t = null;
        try {
            t = targetClazz.newInstance();
            BeanUtils.copyProperties(source, t);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("转换类异常");
        }
        return t;
    }


    /**
     * @return
     * @description list转换方法
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/5 下午4:06
     */
    public static <T> List<T> convertList(Collection<?> source, Class<T> targetClazz) {
        List<T> tList = null;
        try {
            tList = new ArrayList<>(source.size());
            for (Object obj : source) {
                T t = targetClazz.newInstance();
                BeanUtils.copyProperties(obj, t);
                tList.add(t);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("List<T>转换异常");
        }
        return tList;
    }
}
