package com.innovation.ic.b1b.framework.aop;

import com.innovation.ic.b1b.framework.manager.ZookeeperManager;

import javax.annotation.Resource;

public abstract class AbstractAspect {

    @Resource
    protected ZookeeperManager zookeeperManager;
}
