package com.innovation.ic.b1b.framework.util;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName StaticAutowiredBeanHelper
 * @Description 当静态方法需要注入bean，但无法注入时就可以extends当前bean
 * @Date 2022/9/23
 * @Author myq
 */
public class StaticAutowiredBeanHelper{

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
}
