package com.innovation.ic.b1b.framework.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Date的工具类
 */
public class DateUtils {

    /**
     * 默认格式
     */
    public static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * String类型转换为Date类型，格式为yyyy-MM-dd HH:mm:ss
     *
     * @param str
     * @return
     */
    public static Date stringToDate(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_FORMAT);
        try {
            return simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * String类型转换为Date类型，格式为pattern
     *
     * @param str     字符串类型的时间
     * @param pattern 时间格式
     * @return 返回格式化后的时间
     */
    public static Date stringToDate(String str, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            return simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定时间前的时间
     *
     * @param time 指定时间(单位:秒)
     * @return 返回结果
     */
    public static Date getLeftDateByTime(Integer time) {
        int day = time / 3600 / 24;
        Calendar cal = Calendar.getInstance();
        // 获取前day天的时间
        cal.setTime(new Date(System.currentTimeMillis()));
        cal.add(Calendar.DAY_OF_MONTH, -day);
        return cal.getTime();
    }

    /**
     * 获取指定时间前的时间
     *
     * @param day 指定时间(单位:天)
     * @return 返回结果
     */
    public static Date getLeftDayTime(Integer day) {
        Calendar cal = Calendar.getInstance();
        // 获取前day天的时间
        cal.setTime(new Date(System.currentTimeMillis()));
        cal.add(Calendar.DAY_OF_MONTH, -day);
        return cal.getTime();
    }

    /**
     * 格式化时间
     *
     * @param date    时间
     * @param pattern 格式
     * @return 返回格式化后的时间
     */
    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * 获取当天零点时间
     *
     * @return
     */
    public static Date getCurrentDayBeginTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取明天零点时间
     *
     * @return
     */
    public static Date getNextDayBeginTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取当前时间前一小时的时间
     *
     * @param date
     * @return java.util.Date
     */
    public static Date beforeOneHourToNowDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        /* HOUR_OF_DAY 指示一天中的小时 */
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        return calendar.getTime();
    }

    /**
     * 获取当前时间后一小时的时间
     *
     * @param date
     * @return java.util.Date
     */
    public static Date afterOneHourToNowDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        /* HOUR_OF_DAY 指示一天中的小时 */
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        return calendar.getTime();
    }

    /**
     * 判断俩个时间点 当前时间那个更近
     *
     * @param date       当前时间
     * @param frontsDate 时间A
     * @param frontDate  时间B
     * @return java.util.Date
     */
    public static Boolean contrastDate(Date date, Date frontsDate, Date frontDate) {
        long time = date.getTime();//当前时间戳
        long DateA = frontsDate.getTime();//时间A
        long DateB = frontDate.getTime();//时间B
        long newDateATime = Math.abs(DateA - time);//时间A-当前时间
        long newDateBTime = Math.abs(DateB - time);//时间B-当前时间
        if (newDateATime < newDateBTime) {//A小于B 返回true
            return true;
        } else {//反之 返回false
            return false;
        }
    }

    /**
     * 根据系统时间，向前n小时
     * @param n
     * @return
     */
    public static Date forwardNHour(int n){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -n);
        Date previousHour = calendar.getTime();
        return previousHour;
    }
}