//package com.innovation.ic.b1b.framework.util;
//
//import com.jcraft.jsch.*;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.io.IOUtils;
////import org.slf4j.Logger;
////import org.slf4j.LoggerFactory;
//
//import java.io.*;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//import java.util.Vector;
//
///**
// * sftp连接工具类
// */
//public class SftpUtils {
//
//    private ThreadLocal<ChannelSftp> channelSftpThreadLocal = new ThreadLocal<>();
//
//    private ChannelSftp sftp;
//
//    private Session session;
//    /**
//     * FTP 登录用户名
//     */
//    private String username;
//    /**
//     * FTP 登录密码
//     */
//    private String password;
//    /**
//     * 私钥
//     */
//    private String privateKey;
//    /**
//     * FTP 服务器地址IP地址
//     */
//    private String host;
//    /**
//     * FTP 端口
//     */
//    private int port;
//
//    /**
//     * 构造基于密码认证的sftp对象
//     *
//     * @param username
//     * @param password
//     * @param host
//     * @param port
//     */
//    public SftpUtils(String username, String password, String host, int port) {
//        this.username = username;
//        this.password = password;
//        this.host = host;
//        this.port = port;
//    }
//
//    /**
//     * 构造基于秘钥认证的sftp对象
//     *
//     * @param username
//     * @param host
//     * @param port
//     * @param privateKey
//     */
//    public SftpUtils(String username, String host, int port, String privateKey) {
//        this.username = username;
//        this.host = host;
//        this.port = port;
//        this.privateKey = privateKey;
//    }
//
//    public SftpUtils() {}
//
//    /**
//     * @Description: 获取count数个sftp连接
//     * @Params:
//     * @Return:
//     * @Author: Mr.myq
//     * @Date: 2022/11/19:47
//     */
//    public void tryLoginSetThreadLocal(){
//        channelSftpThreadLocal.set(connect());
//    }
//
//    /**
//     * @Description: 删除元素，防止内存泄漏
//     * @Params:
//     * @Return:
//     * @Author: Mr.myq
//     * @Date: 2022/11/19:54
//     */
//    public void remove(){
//        channelSftpThreadLocal.remove();
//    }
//
//
//    /**
//     * 连接sftp服务器
//     *
//     * @throws Exception
//     */
//    public void login() {
//       sftp = connect();
//    }
//
//    /**
//     * @Description: 获取一个成功连接
//     * @Params:
//     * @Return:
//     * @Author: Mr.myq
//     * @Date: 2022/11/19:52
//     */
//    private ChannelSftp connect(){
//        try {
//            JSch jsch = new JSch();
//            if (privateKey != null) {
//                jsch.addIdentity(privateKey);// 设置私钥
//            }
//            session = jsch.getSession(username, host, port);
//            if (password != null) {
//                session.setPassword(password);
//            }
//            Properties config = new Properties();
//            config.put("StrictHostKeyChecking", "no");
//
//            session.setConfig(config);
//            session.connect();
//            Channel channel = session.openChannel("sftp");
//            channel.connect();
//            return sftp = (ChannelSftp) channel;
//        } catch (JSchException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//
//    /**
//     * 关闭连接 server
//     */
//    public void logout() {
//        if (sftp != null) {
//            if (sftp.isConnected()) {
//                sftp.disconnect();
////                log.info("sftp is closed already");
//            }
//        }
//        if (session != null) {
//            if (session.isConnected()) {
//                session.disconnect();
////                log.info("sshSession is closed already");
//            }
//        }
//    }
//
//    /**
//     * 递归创建目录
//     *
//     * @param originDirectory /opt/image/cyz/test/202205/23
//     */
//    private void mkdirRecursion(String originDirectory) throws SftpException {
//        // 获取从浅到深的目录
//        List<String> dirs = new ArrayList<>();
//        String[] words = originDirectory.trim().split("/");
//        String lastDir = "";
//        for (String word : words) {
//            lastDir += "/" + word;
//            dirs.add(lastDir);
//        }
//
//        // 由浅到深进入目录和创建目录
//        for (String dir : dirs) {
//            try {
//                sftp.cd(dir);
//            } catch (SftpException e) {
//                sftp.mkdir(dir);
//                sftp.cd(dir);
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 将输入流的数据上传到sftp作为文件
//     *
//     * @param directory    上传到该目录
//     * @param sftpFileName sftp端文件名
//     * @param input        输入流
//     * @throws SftpException
//     * @throws Exception
//     */
//    public void upload(String directory, String sftpFileName, InputStream input) throws SftpException {
//        mkdirRecursion(directory);
//        sftp.put(input, sftpFileName);
//    }
//
//    public void personalityUpload(String directory, String sftpFileName, InputStream input) throws SftpException {
//        ChannelSftp channelSftp = null;
//        try {
//            channelSftp = channelSftpThreadLocal.get();
//            personalityMkdirRecursion(channelSftp,directory);
//            channelSftp.put(input, sftpFileName);
//        } catch (SftpException e) {
//            e.printStackTrace();
//            throw new RuntimeException("从ThreadLocal中获取sftp连接错误");
//        } finally {
//            if(channelSftp != null)
//                this.remove();
//        }
//    }
//
//    /**
//     * @Description: 个性的上传方法
//     * @Params:
//     * @Return:
//     * @Author: Mr.myq
//     * @Date: 2022/11/110:26
//     */
//    public void personalityUpload(String directory, String sftpFileName, byte[] byteArr) throws SftpException {
//        // 尝试获取sftp并设置local
//        this.tryLoginSetThreadLocal();
//        personalityUpload(directory, sftpFileName, new ByteArrayInputStream(byteArr));
//    }
//
//
//    private void personalityMkdirRecursion(ChannelSftp channelSftp,String originDirectory) throws SftpException {
//        // 获取从浅到深的目录
//        List<String> dirs = new ArrayList<>();
//        String[] words = originDirectory.trim().split("/");
//        String lastDir = "";
//        for (String word : words) {
//            lastDir += "/" + word;
//            dirs.add(lastDir);
//        }
//        // 由浅到深进入目录和创建目录
//        for (String dir : dirs) {
//            try {
//                channelSftp.cd(dir);
//            } catch (SftpException e) {
//                channelSftp.mkdir(dir);
//                channelSftp.cd(dir);
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    /**
//     * 上传单个文件
//     *
//     * @param directory  上传到sftp目录
//     * @param uploadFile 要上传的文件,包括路径
//     * @throws FileNotFoundException
//     * @throws SftpException
//     * @throws Exception
//     */
//    public void upload(String directory, String uploadFile) throws FileNotFoundException, SftpException {
//        File file = new File(uploadFile);
//        upload(directory, file.getName(), new FileInputStream(file));
//    }
//
//    /**
//     * 将byte[]上传到sftp，作为文件。注意:从String生成byte[]是，要指定字符集。
//     *
//     * @param directory    上传到sftp目录
//     * @param sftpFileName 文件在sftp端的命名
//     * @param byteArr      要上传的字节数组
//     * @throws SftpException
//     * @throws Exception
//     */
//    public void upload(String directory, String sftpFileName, byte[] byteArr) throws SftpException {
//        upload(directory, sftpFileName, new ByteArrayInputStream(byteArr));
//    }
//
//    /**
//     * 将字符串按照指定的字符编码上传到sftp
//     *
//     * @param directory    上传到sftp目录
//     * @param sftpFileName 文件在sftp端的命名
//     * @param dataStr      待上传的数据
//     * @param charsetName  sftp上的文件，按该字符编码保存
//     * @throws UnsupportedEncodingException
//     * @throws SftpException
//     * @throws Exception
//     */
//    public void upload(String directory, String sftpFileName, String dataStr, String charsetName) throws UnsupportedEncodingException, SftpException {
//        upload(directory, sftpFileName, new ByteArrayInputStream(dataStr.getBytes(charsetName)));
//    }
//
//    /**
//     * 下载文件
//     *
//     * @param directory    下载目录
//     * @param downloadFile 下载的文件
//     * @param saveFile     存在本地的路径
//     * @throws SftpException
//     * @throws FileNotFoundException
//     * @throws Exception
//     */
//    public void download(String directory, String downloadFile, String saveFile) throws SftpException, FileNotFoundException {
//        if (directory != null && !"".equals(directory)) {
//            sftp.cd(directory);
//        }
//        File file = new File(saveFile);
//        sftp.get(downloadFile, new FileOutputStream(file));
////        log.info("file:{} is download successful", downloadFile);
//    }
//
//    /**
//     * 下载文件
//     *
//     * @param directory    下载目录
//     * @param downloadFile 下载的文件名
//     * @return 字节数组
//     * @throws SftpException
//     * @throws IOException
//     * @throws Exception
//     */
//    public byte[] download(String directory, String downloadFile) throws SftpException, IOException {
//        if (directory != null && !"".equals(directory)) {
//            sftp.cd(directory);
//        }
//        InputStream is = sftp.get(downloadFile);
//        byte[] fileData = IOUtils.toByteArray(is);
////        log.info("file:{} is download successful", downloadFile);
//        return fileData;
//    }
//
//    /**
//     * 删除文件
//     *
//     * @param directory  要删除文件所在目录
//     * @param deleteFile 要删除的文件
//     * @throws SftpException
//     * @throws Exception
//     */
//    public void delete(String directory, String deleteFile) throws SftpException {
//        sftp.cd(directory);
//        sftp.rm(deleteFile);
//    }
//
//    /**
//     * 列出目录下的文件
//     *
//     * @param directory 要列出的目录
//     * @param directory
//     * @return
//     * @throws SftpException
//     */
//    public Vector<?> listFiles(String directory) throws SftpException {
//        return sftp.ls(directory);
//    }
//
//    public static void main(String[] args) throws SftpException, IOException {
//        SftpUtils sftp = new SftpUtils("root", "Ydec2022", "192.168.80.110", 22);
//        sftp.login();
//
////        byte[] buff = sftp.download("/opt/image/cyz", "ya111.jpg");
////        System.out.println(Arrays.toString(buff));
//
//        File file = new File("D:\\00_测试文件\\ya1.jpg");
//        InputStream is = new FileInputStream(file);
//        sftp.upload("/opt/image/cyz", "ya111.jpg", is);
//
//        sftp.logout();
//    }
//
//}
