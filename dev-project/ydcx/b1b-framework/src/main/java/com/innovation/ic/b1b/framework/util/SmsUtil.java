package com.innovation.ic.b1b.framework.util;

import org.dromara.sms4j.api.SmsBlend;
import org.dromara.sms4j.api.entity.SmsResponse;
import org.dromara.sms4j.core.factory.SmsFactory;
import org.dromara.sms4j.provider.enumerate.SupplierType;

import java.util.LinkedHashMap;
import java.util.Random;

/**
 * 短信工具类
 */
public class SmsUtil {

    private static final SmsBlend aliSms = SmsFactory.createSmsBlend(SupplierType.ALIBABA);

    /**
     * 发送短信
     * @param phone
     * @param templateId
     * @return
     */
    public static SmsResponse send(String phone, String templateId) {
        return aliSms.sendMessage(phone, templateId, new LinkedHashMap<>());
    }

    /**
     * 发送短信
     * @param phone
     * @return
     */
    public static SmsResponse sendByTemplate(String phone, String code) {
        return aliSms.sendMessage(phone, code);
    }

    /**
     * 生成验证码
     * @return
     */
    public static String createVerifyCode(){
        Random random = new Random();
        int verificationCode = random.nextInt(999999) + 100000;
        String verifyCode = String.valueOf(verificationCode);
        return verifyCode;
    }
}
