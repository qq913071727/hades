package com.innovation.ic.b1b.monitor.web.controller;

import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.pojo.HomePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 首页Controller
 */
@Api(value = "首页Controller", tags = "HomeController-首页")
@RestController
@RequestMapping("/api/v1/home")
@Slf4j
public class HomeController extends AbstractController {

    /**
     * 首页数据显示
     */
    @ApiOperation(value = "首页数据显示")
    @PostMapping("findAllHome")
    public ResponseEntity<ApiResult<HomePojo>> findAllHome() {
        HomePojo homePojo = new HomePojo();
        Date date = new Date(System.currentTimeMillis());
        Date behindData = DateUtils.beforeOneHourToNowDate(date);//当前时间前一小时
        Integer apiNumber = apiAvailableJobLogService.findApiClose(date, behindData);//查询接口不可用数据(一小时内)
        homePojo.setApiNumber(apiNumber);
        Integer mysqlNumber = mysqlAvailableJobLogService.findMysqlClose(date, behindData);//查询mysql不可用数据(一小时内)
        homePojo.setMysqlNumber(mysqlNumber);
        Integer serverNumber = serverPerformanceJobLogService.findServerClose(date, behindData);//查询服务性能数据(一小时内/服务器性能/内存超过95%)
        homePojo.setServerNumber(serverNumber);
        Integer serviceActiveNumber = serviceActiveJobLogService.findServerClose(date, behindData);//查询服务不可用数据(一小时内)
        homePojo.setServerActiveNumber(serviceActiveNumber);
        Integer sqlServiceNumber = sqlServerAvailableJobLogService.findSqlServerClose(date, behindData);//查询sql server不可用数据(一小时内)
        homePojo.setSqlServerNumber(sqlServiceNumber);
        Integer elasticsearchNumber = elasticsearchAvailableJobLogService.findElasticsearchClose(date, behindData);//查询elasticsearch不可用数据(一小时内)
        homePojo.setElasticsearchNumber(elasticsearchNumber);
        Integer mongodbNumber = mongodbAvailableJobLogService.findMongodbClose(date, behindData);//查询mongodb不可用数据(一小时内)
        homePojo.setMongodbNumber(mongodbNumber);
        Integer rabbitmqNumber = rabbitmqAvailableJobLogService.findRabbitmqClose(date, behindData);//查询rabbitmq不可用数据(一小时内)
        homePojo.setRabbitmqNumber(rabbitmqNumber);
        Integer redisNumber = redisAvailableJobLogService.findRedisClose(date, behindData);//查询redis不可用数据(一小时内)
        homePojo.setRedisNumber(redisNumber);
        ApiResult<HomePojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        apiResult.setResult(homePojo);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
