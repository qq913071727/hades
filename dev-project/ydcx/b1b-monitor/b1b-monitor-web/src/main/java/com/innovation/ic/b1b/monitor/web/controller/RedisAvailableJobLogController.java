package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJobLog.RedisAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJobLog.RedisAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   redis可用任务日志表controller类
 * @author linuo
 * @time   2023年3月29日09:33:38
 */
@Api(value = "RedisAvailableJobLogController", tags = "中间件监控 - redis - 日志")
@RestController
@RequestMapping("/api/v1/redisAvailableJobLogController")
public class RedisAvailableJobLogController extends AbstractController {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @ApiOperation("查询redis监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<RedisAvailableJobLogListRespData>>> queryList(@RequestBody RedisAvailableJobLogListQueryVo redisAvailableJobLogListQueryVo) {
        if (null == redisAvailableJobLogListQueryVo || null == redisAvailableJobLogListQueryVo.getPageNo() || null == redisAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/redisAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<RedisAvailableJobLogListRespData>> res = redisAvailableJobLogService.queryList(redisAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}