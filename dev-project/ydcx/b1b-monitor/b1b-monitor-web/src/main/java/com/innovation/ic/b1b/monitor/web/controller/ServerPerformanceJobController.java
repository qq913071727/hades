package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   服务器性能任务表controller类
 * @author linuo
 * @time   2023年3月9日15:58:29
 */
@Api(value = "ServerPerformanceJobController", tags = "服务器监控 - 任务")
@RestController
@RequestMapping("/api/v1/serverPerformanceJobController")
@Slf4j
public class ServerPerformanceJobController extends AbstractController {
    @ApiOperation("查询服务器监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ServerPerformanceJobListRespData>>> queryList(@RequestBody ServerPerformanceJobListQueryVo serverPerformanceJobListQueryVo) {
        if (null == serverPerformanceJobListQueryVo || null == serverPerformanceJobListQueryVo.getPageNo() || null == serverPerformanceJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/serverPerformanceJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<ServerPerformanceJobListRespData>> res = serverPerformanceJobService.queryList(serverPerformanceJobListQueryVo, serverPerformanceJobListQueryVo.getIsMain());

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("添加服务器监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody ServerPerformanceJobAddVo serverPerformanceJobAddVo) {
        if (null == serverPerformanceJobAddVo || null == serverPerformanceJobAddVo.getServerId() || Strings.isNullOrEmpty(serverPerformanceJobAddVo.getScheduleExpression())) {
            String message = "添加服务器监控任务时，服务器名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(serverPerformanceJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(serverPerformanceJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(serverPerformanceJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = serverPerformanceJobService.add(serverPerformanceJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取服务器监控任务详情")
    @ApiImplicitParam(name = "id", value = "服务器监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ServerPerformanceJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/serverPerformanceJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<ServerPerformanceJobInfoRespPojo> res = serverPerformanceJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除服务器监控任务")
    @ApiImplicitParam(name = "id", value = "服务器监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/serverPerformanceJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = serverPerformanceJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/serverPerformanceJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = serverPerformanceJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新服务器监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody ServerPerformanceJobUpdateVo serverPerformanceJobUpdateVo) {
        if (null == serverPerformanceJobUpdateVo || null == serverPerformanceJobUpdateVo.getId() || null == serverPerformanceJobUpdateVo.getServerId() || Strings.isNullOrEmpty(serverPerformanceJobUpdateVo.getScheduleExpression())) {
            String message = "添加服务器监控任务时，主键、服务器名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(serverPerformanceJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(serverPerformanceJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(serverPerformanceJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = serverPerformanceJobService.update(serverPerformanceJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}