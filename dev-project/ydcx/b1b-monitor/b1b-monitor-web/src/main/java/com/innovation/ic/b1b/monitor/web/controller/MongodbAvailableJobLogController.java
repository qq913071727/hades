package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJobLog.MongodbAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJobLog.MongodbAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   mongodb可用任务日志表的controller
 * @author linuo
 * @time   2023年3月24日16:33:08
 */
@Api(value = "MongodbAvailableJobLogController", tags = "中间件监控 - mongodb - 日志")
@RestController
@RequestMapping("/api/v1/mongodbAvailableJobLogController")
@Slf4j
public class MongodbAvailableJobLogController extends AbstractController {
    @ApiOperation("查询mongodb监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<MongodbAvailableJobLogListRespData>>> queryList(@RequestBody MongodbAvailableJobLogListQueryVo mongodbAvailableJobLogListQueryVo) {
        if (null == mongodbAvailableJobLogListQueryVo || null == mongodbAvailableJobLogListQueryVo.getPageNo() || null == mongodbAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/mongodbAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<MongodbAvailableJobLogListRespData>> res = mongodbAvailableJobLogService.queryList(mongodbAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}