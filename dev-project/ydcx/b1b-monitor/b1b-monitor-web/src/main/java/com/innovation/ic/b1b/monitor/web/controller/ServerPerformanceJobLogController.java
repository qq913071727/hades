package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLog.ServerPerformanceJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJobLog.ServerPerformanceJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   服务器性能任务日志表controller类
 * @author linuo
 * @time   2023年3月9日15:58:29
 */
@Api(value = "serverPerformanceJobLogController", tags = "服务器监控 - 日志")
@RestController
@RequestMapping("/api/v1/serverPerformanceJobLogController")
@Slf4j
public class ServerPerformanceJobLogController extends AbstractController {
    @ApiOperation("查询服务器性能任务日志")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ServerPerformanceJobLogListRespData>>> queryList(@RequestBody ServerPerformanceJobLogListQueryVo serverPerformanceJobLogListQueryVo) {
        if (null == serverPerformanceJobLogListQueryVo || null == serverPerformanceJobLogListQueryVo.getPageNo() || null == serverPerformanceJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/serverPerformanceJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<ServerPerformanceJobLogListRespData>> res = serverPerformanceJobLogService.queryList(serverPerformanceJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}