package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.LogMonitorJobService;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author myq
 * @since 1.0.0 2023-05-10
 */
@RestController
@RequestMapping("/api/v1/logmonitorjob")
@Api(tags = "")
public class LogMonitorJobController extends AbstractController {

    @Resource
    private LogMonitorJobService LogMonitorJobService;

    @ApiOperation("分页")
    @PostMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "LogMonitorJobVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<PageInfo<LogMonitorJobPojo>>> page(int pageNo, int pageSize,
                                                                       @RequestBody LogMonitorJobVo vo) {

        ServiceResult<PageInfo<LogMonitorJobPojo>> serviceResult = LogMonitorJobService.pageInfo(pageNo, pageSize, vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @ApiOperation("列表")
    @PostMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "LogMonitorJobVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<List<LogMonitorJobPojo>>> list(@RequestBody LogMonitorJobVo vo) {

        ServiceResult<List<LogMonitorJobPojo>> serviceResult = LogMonitorJobService.list(vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }


    @GetMapping("{id}")
    @ApiOperation("明细")
    public ResponseEntity<ApiResult<LogMonitorJobPojo>> get(@PathVariable("id") String id) {
        ServiceResult<LogMonitorJobPojo> serviceResult = LogMonitorJobService.get(id);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("保存")
    public ResponseEntity<ApiResult> save(LogMonitorJobVo vo) {
        LogMonitorJobService.save(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(LogMonitorJobVo vo) {
        LogMonitorJobService.update(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation("删除")
    public ResponseEntity<ApiResult> delete(String[] ids) {
        LogMonitorJobService.delete(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }


}