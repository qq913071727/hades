package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobLogHistoryPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.LogMonitorJobLogHistoryService;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogHistoryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author myq
 * @since 1.0.0 2023-05-10
 */
@RestController
@RequestMapping("/api/v1/logmonitorjobloghistory")
@Api(tags = "")
public class LogMonitorJobLogHistoryController extends AbstractController {

    @Resource
    private LogMonitorJobLogHistoryService LogMonitorJobLogHistoryService;

    @ApiOperation("分页")
    @PostMapping("page")
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"), @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"), @ApiImplicitParam(name = "LogMonitorJobLogHistoryVo", value = "查询参数", dataType = "json", required = false)})
    public ResponseEntity<ApiResult<PageInfo<LogMonitorJobLogHistoryPojo>>> page(int pageNo, int pageSize, @RequestBody LogMonitorJobLogHistoryVo vo) {

        ServiceResult<PageInfo<LogMonitorJobLogHistoryPojo>> serviceResult = LogMonitorJobLogHistoryService.pageInfo(pageNo, pageSize, vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @ApiOperation("列表")
    @PostMapping("list")
    @ApiImplicitParams({@ApiImplicitParam(name = "LogMonitorJobLogHistoryVo", value = "查询参数", dataType = "json", required = false)})
    public ResponseEntity<ApiResult<List<LogMonitorJobLogHistoryPojo>>> list(@RequestBody LogMonitorJobLogHistoryVo vo) {

        ServiceResult<List<LogMonitorJobLogHistoryPojo>> serviceResult = LogMonitorJobLogHistoryService.list(vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }


    @GetMapping("{id}")
    @ApiOperation("明细")
    public ResponseEntity<ApiResult<LogMonitorJobLogHistoryPojo>> get(@PathVariable("id") String id) {
        ServiceResult<LogMonitorJobLogHistoryPojo> serviceResult = LogMonitorJobLogHistoryService.get(id);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("保存")
    public ResponseEntity<ApiResult> save(LogMonitorJobLogHistoryVo vo) {
        LogMonitorJobLogHistoryService.save(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(LogMonitorJobLogHistoryVo vo) {
        LogMonitorJobLogHistoryService.update(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation("删除")
    public ResponseEntity<ApiResult> delete(String[] ids) {
        LogMonitorJobLogHistoryService.delete(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }


}