//package com.b1b.monitor.web.task;
//
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.b1b.monitor.web.config.EmailConfig;
//import com.b1b.monitor.web.component.email.template.SimpleEmailTemplate;
//import com.b1b.monitor.web.pojo.email.FreeInfo;
//import com.b1b.monitor.web.pojo.email.SystemInfo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mail.javamail.MimeMailMessage;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.lang.management.ManagementFactory;
//import java.lang.management.OperatingSystemMXBean;
//
///**
// * @ClassName MonitorFreeTask
// * @Description 监控内存任务类
// * @Date 2022/10/27
// * @Author myq
// */
//@Slf4j
//@Component
//public class MonitorFreeTask extends AbstractTask {
//
//
//
//    /**
//     * @Description: 每隔十秒监控一次内存使用情况
//     * @Params:
//     * @Return:
//     * @Author: Mr.myq
//     * @Date: 2022/10/2715:07
//     */
//    @Scheduled(cron = "* */1 * * * ?")
//    public void scanning() {
//        log.info("》》》》》》》》》》》》》》》》》开始扫描系统资源使用情况》》》》》》》》》》》》》》》》》》》》");
//        //系统资源类
//        SystemInfo systemResourceInfo = SystemResource.getSystemResourceInfo();
//        if (systemResourceInfo.getFreeMemory() <= THRESHOLD_MIN_GB) {
//            // 初始化示例模板 初始化父类
//            // 构建消息
//            SimpleEmailTemplate<MimeMailMessage> simpleEmailTemplate = new SimpleEmailTemplate<>(emailConfig,systemResourceInfo);
//            // 发送请求
//            simpleEmailTemplate.send();
//            log.info("发送邮件结束");
//        }
//
//        log.info("《《《《《《《《《《《《《《《《《 结束扫描系统资源使用情况《《《《《《《《《《《《《《《《《 ");
//    }
//
//
//    /**
//     * @ClassName MonitorFreeTask
//     * @Description 静态资源内部类
//     * @Date 2022/10/27
//     * @Author myq
//     */
//    private static class SystemResource {
//
//        /**
//         * @Description: 获取系统当前资源使用情况
//         * @Params:
//         * @Return:
//         * @Author: Mr.myq
//         * @Date: 2022/10/2715:10
//         */
//        private static SystemInfo getSystemResourceInfo() {
//            final long GB = 1024 * 1024 * 1024;
//            OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
//            String osJson = JSON.toJSONString(operatingSystemMXBean);
//            JSONObject jsonObject = JSON.parseObject(osJson);
//            double processCpuLoad = jsonObject.getDouble("processCpuLoad") * 100;
//            double systemCpuLoad = jsonObject.getDouble("systemCpuLoad") * 100;
//            Long totalPhysicalMemorySize = jsonObject.getLong("totalPhysicalMemorySize");
//            Long freePhysicalMemorySize = jsonObject.getLong("freePhysicalMemorySize");
//            double totalMemory = 1.0 * totalPhysicalMemorySize / GB;
//            double freeMemory = 1.0 * freePhysicalMemorySize / GB;
//            double memoryUseRatio = 1.0 * (totalPhysicalMemorySize - freePhysicalMemorySize) / totalPhysicalMemorySize * 100;
//
//            SystemInfo systemInfo = new FreeInfo();
//            systemInfo.setSystemCpuLoad(systemCpuLoad);
//            systemInfo.setFreeMemory(freeMemory);
//            systemInfo.setMemoryUseRatio(memoryUseRatio);
//            systemInfo.setProcessCpuLoad(processCpuLoad);
//            systemInfo.setTotalMemory(totalMemory);
//            return systemInfo;
//        }
//
//    }
//
//}
