package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   mongodb可用任务表的controller
 * @author linuo
 * @time   2023年3月24日14:38:52
 */
@Api(value = "MongodbAvailableJobController", tags = "中间件监控 - mongodb - 任务")
@RestController
@RequestMapping("/api/v1/mongodbAvailableJobController")
@Slf4j
public class MongodbAvailableJobController extends AbstractController {
    @ApiOperation("添加mongodb监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody MongodbAvailableJobAddVo mongodbAvailableJobAddVo) {
        if (null == mongodbAvailableJobAddVo || null == mongodbAvailableJobAddVo.getMongodbId() || Strings.isNullOrEmpty(mongodbAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加mongodb监控任务时，mongodb名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(mongodbAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(mongodbAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(mongodbAvailableJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mongodbAvailableJobService.add(mongodbAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("查询mongodb监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<MongodbAvailableJobListRespData>>> queryList(@RequestBody MongodbAvailableJobListQueryVo mongodbAvailableJobListQueryVo) {
        if (null == mongodbAvailableJobListQueryVo || null == mongodbAvailableJobListQueryVo.getPageNo() || null == mongodbAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/mongodbAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<MongodbAvailableJobListRespData>> res = mongodbAvailableJobService.queryList(mongodbAvailableJobListQueryVo, mongodbAvailableJobListQueryVo.getIsMain());

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除mongodb监控任务")
    @ApiImplicitParam(name = "id", value = "mongodb监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/mongodbAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mongodbAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取mongodb监控任务详情")
    @ApiImplicitParam(name = "id", value = "mongodb监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<MongodbAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/mongodbAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<MongodbAvailableJobInfoRespPojo> res = mongodbAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新mongodb监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody MongodbAvailableJobUpdateVo mongodbAvailableJobUpdateVo) {
        if (null == mongodbAvailableJobUpdateVo || null == mongodbAvailableJobUpdateVo.getId() || null == mongodbAvailableJobUpdateVo.getMongodbId() || Strings.isNullOrEmpty(mongodbAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新mongodb监控任务时，主键、mongodb名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(mongodbAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(mongodbAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(mongodbAvailableJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mongodbAvailableJobService.update(mongodbAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/mongodbAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mongodbAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}