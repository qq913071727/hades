package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob.ZookeeperAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob.ZookeeperAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   zookeeper可用任务表的controller
 * @author linuo
 * @time   2023年3月24日14:38:52
 */
@Api(value = "ZookeeperAvailableJobController", tags = "中间件监控 - zookeeper - 任务")
@RestController
@RequestMapping("/api/v1/zookeeperAvailableJobController")
@Slf4j
public class ZookeeperAvailableJobController extends AbstractController {
    @ApiOperation("添加zookeeper监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody ZookeeperAvailableJobAddVo zookeeperAvailableJobAddVo) {
        if (null == zookeeperAvailableJobAddVo || null == zookeeperAvailableJobAddVo.getZookeeperId() || Strings.isNullOrEmpty(zookeeperAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加zookeeper监控任务时，zookeeper名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(zookeeperAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(zookeeperAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(zookeeperAvailableJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = zookeeperAvailableJobService.add(zookeeperAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("查询zookeeper监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ZookeeperAvailableJobListRespData>>> queryList(@RequestBody ZookeeperAvailableJobListQueryVo zookeeperAvailableJobListQueryVo) {
        if (null == zookeeperAvailableJobListQueryVo || null == zookeeperAvailableJobListQueryVo.getPageNo() || null == zookeeperAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/zookeeperAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<ZookeeperAvailableJobListRespData>> res = zookeeperAvailableJobService.queryList(zookeeperAvailableJobListQueryVo, zookeeperAvailableJobListQueryVo.getIsMain());
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除zookeeper监控任务")
    @ApiImplicitParam(name = "id", value = "zookeeper监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/zookeeperAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = zookeeperAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取zookeeper监控任务详情")
    @ApiImplicitParam(name = "id", value = "zookeeper监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ZookeeperAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/zookeeperAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<ZookeeperAvailableJobInfoRespPojo> res = zookeeperAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新zookeeper监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody ZookeeperAvailableJobUpdateVo zookeeperAvailableJobUpdateVo) {
        if (null == zookeeperAvailableJobUpdateVo || null == zookeeperAvailableJobUpdateVo.getId() || null == zookeeperAvailableJobUpdateVo.getZookeeperId() || Strings.isNullOrEmpty(zookeeperAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新zookeeper监控任务时，主键、zookeeper名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(zookeeperAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(zookeeperAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(zookeeperAvailableJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = zookeeperAvailableJobService.update(zookeeperAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/zookeeperAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = zookeeperAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}