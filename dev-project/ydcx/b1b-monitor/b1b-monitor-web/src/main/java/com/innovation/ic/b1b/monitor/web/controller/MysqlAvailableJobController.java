package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   mysql可用任务表controller类
 * @author linuo
 * @time   2023年3月15日15:07:53
 */
@Api(value = "MysqlAvailableJobController", tags = "中间件监控 - mysql - 任务")
@RestController
@RequestMapping("/api/v1/mysqlAvailableJobController")
@Slf4j
public class MysqlAvailableJobController extends AbstractController {
    @ApiOperation("查询mysql监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<MysqlAvailableJobListRespData>>> queryList(@RequestBody MysqlAvailableJobListQueryVo mysqlAvailableJobListQueryVo) {
        if (null == mysqlAvailableJobListQueryVo || null == mysqlAvailableJobListQueryVo.getPageNo() || null == mysqlAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/mysqlAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<MysqlAvailableJobListRespData>> res = mysqlAvailableJobService.queryList(mysqlAvailableJobListQueryVo, mysqlAvailableJobListQueryVo.getIsMain());

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("添加mysql监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody MysqlAvailableJobAddVo mysqlAvailableJobAddVo) {
        if (null == mysqlAvailableJobAddVo || null == mysqlAvailableJobAddVo.getMysqlId() || Strings.isNullOrEmpty(mysqlAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加mysql监控任务时，mysql名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(mysqlAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(mysqlAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(mysqlAvailableJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mysqlAvailableJobService.add(mysqlAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除mysql监控任务")
    @ApiImplicitParam(name = "id", value = "mysql监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/mysqlAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mysqlAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取mysql监控任务详情")
    @ApiImplicitParam(name = "id", value = "mysql监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<MysqlAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/mysqlAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<MysqlAvailableJobInfoRespPojo> res = mysqlAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新mysql监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody MysqlAvailableJobUpdateVo mysqlAvailableJobUpdateVo) {
        if (null == mysqlAvailableJobUpdateVo || null == mysqlAvailableJobUpdateVo.getId() || null == mysqlAvailableJobUpdateVo.getMysqlId() || Strings.isNullOrEmpty(mysqlAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新mysql监控任务时，主键、mysql名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(mysqlAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(mysqlAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(mysqlAvailableJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mysqlAvailableJobService.update(mysqlAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/mysqlAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = mysqlAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}