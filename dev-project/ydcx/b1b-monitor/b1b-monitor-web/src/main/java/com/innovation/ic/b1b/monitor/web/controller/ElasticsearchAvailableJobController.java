package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJob.ElasticsearchAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJob.ElasticsearchAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   elasticsearch可用任务表controller类
 * @author linuo
 * @time   2023年3月24日09:53:26
 */
@Api(value = "ElasticsearchAvailableJobController", tags = "中间件监控 - elasticsearch - 任务")
@RestController
@RequestMapping("/api/v1/elasticsearchAvailableJobController")
@Slf4j
public class ElasticsearchAvailableJobController extends AbstractController {
    @ApiOperation("添加elasticsearch监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody ElasticsearchAvailableJobAddVo elasticsearchAvailableJobAddVo) {
        if (null == elasticsearchAvailableJobAddVo || null == elasticsearchAvailableJobAddVo.getElasticsearchId() || Strings.isNullOrEmpty(elasticsearchAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加elasticsearch监控任务时，es名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(elasticsearchAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(elasticsearchAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = elasticsearchAvailableJobService.add(elasticsearchAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("查询elasticsearch监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ElasticsearchAvailableJobListRespData>>> queryList(@RequestBody ElasticsearchAvailableJobListQueryVo elasticsearchAvailableJobListQueryVo) {
        if (null == elasticsearchAvailableJobListQueryVo || null == elasticsearchAvailableJobListQueryVo.getPageNo() || null == elasticsearchAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/elasticsearchAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<ElasticsearchAvailableJobListRespData>> res = elasticsearchAvailableJobService.queryList(elasticsearchAvailableJobListQueryVo, elasticsearchAvailableJobListQueryVo.getIsMain());
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除elasticsearch监控任务")
    @ApiImplicitParam(name = "id", value = "mysql监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/elasticsearchAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = elasticsearchAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取elasticsearch监控任务详情")
    @ApiImplicitParam(name = "id", value = "mysql监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ElasticsearchAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/elasticsearchAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<ElasticsearchAvailableJobInfoRespPojo> res = elasticsearchAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新elasticsearch监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody ElasticsearchAvailableJobUpdateVo elasticsearchAvailableJobUpdateVo) {
        if (null == elasticsearchAvailableJobUpdateVo || null == elasticsearchAvailableJobUpdateVo.getId() || null == elasticsearchAvailableJobUpdateVo.getElasticsearchId() || Strings.isNullOrEmpty(elasticsearchAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新elasticsearch监控任务时，主键、es名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(elasticsearchAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(elasticsearchAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = elasticsearchAvailableJobService.update(elasticsearchAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/elasticsearchAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = elasticsearchAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}