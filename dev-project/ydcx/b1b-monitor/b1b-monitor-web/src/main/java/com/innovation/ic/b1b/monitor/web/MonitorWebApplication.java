package com.innovation.ic.b1b.monitor.web;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName MonitorWebApplication
 * @Description 监控应用启动
 * @Date 2022/10/27
 * @Author myq
 */
@EnableSwagger2
@Slf4j
@MapperScan({"com.innovation.ic.b1b.monitor.base.mapper", "com.innovation.ic.im.end.base.mapper"})
@SpringBootApplication(scanBasePackages = {"com.innovation.ic.b1b.monitor", "com.innovation.ic.im.end"},
        exclude = {org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration.class, org.apache.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration.class})
public class MonitorWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(MonitorWebApplication.class, args);
        log.info("监控系统启动完成");
    }
}
