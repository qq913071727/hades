package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJobLog.CanalAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJobLog.CanalAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   canal可用任务日志表的controller
 * @author linuo
 * @time   2023年5月12日10:11:38
 */
@Api(value = "CanalAvailableJobLogController", tags = "中间件监控 - canal - 日志")
@RestController
@RequestMapping("/api/v1/canalAvailableJobLogController")
@Slf4j
public class CanalAvailableJobLogController extends AbstractController {
    @ApiOperation("查询canal监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<CanalAvailableJobLogListRespData>>> queryList(@RequestBody CanalAvailableJobLogListQueryVo canalAvailableJobLogListQueryVo) {
        if (null == canalAvailableJobLogListQueryVo || null == canalAvailableJobLogListQueryVo.getPageNo() || null == canalAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/canalAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<CanalAvailableJobLogListRespData>> res = canalAvailableJobLogService.queryList(canalAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}