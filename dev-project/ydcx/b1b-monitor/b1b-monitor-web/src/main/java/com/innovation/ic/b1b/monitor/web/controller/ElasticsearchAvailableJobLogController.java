package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJobLog.ElasticsearchAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJobLog.ElasticsearchAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   elasticsearch可用任务日志表controller类
 * @author linuo
 * @time   2023年3月24日13:50:09
 */
@Api(value = "ElasticsearchAvailableJobLogController", tags = "中间件监控 - elasticsearch - 日志")
@RestController
@RequestMapping("/api/v1/elasticsearchAvailableJobLogController")
@Slf4j
public class ElasticsearchAvailableJobLogController extends AbstractController {
    @ApiOperation("查询elasticsearch可用任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ElasticsearchAvailableJobLogListRespData>>> queryList(@RequestBody ElasticsearchAvailableJobLogListQueryVo elasticsearchAvailableJobLogListQueryVo) {
        if (null == elasticsearchAvailableJobLogListQueryVo || null == elasticsearchAvailableJobLogListQueryVo.getPageNo() || null == elasticsearchAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/elasticsearchAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<ElasticsearchAvailableJobLogListRespData>> res = elasticsearchAvailableJobLogService.queryList(elasticsearchAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}