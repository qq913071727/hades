package com.innovation.ic.im.end.web.controller;

import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.web.controller.AbstractController;
import com.innovation.ic.im.end.base.model.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 客户端信息API
 */
@RestController
@RequestMapping("/api/v2/client")
//@DefaultProperties(defaultFallback = "defaultFallback")
public class ClientController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    /**
     * 删除redis中CLIENT:开头的数据，将client表中的数据导入redis
     * @return
     */
//    @HystrixCommand
    @RequestMapping(value = "/importClientIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<Client>>> importClientIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("删除redis中CLIENT:开头的数据，将client表中的数据导入redis");

        ApiResult<List<Client>> apiResult = new ApiResult<List<Client>>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(clientService.findAll().getResult());
        return new ResponseEntity<ApiResult<List<Client>>>(apiResult, HttpStatus.OK);
    }

}