package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   rabbitmq可用任务表controller类
 * @author linuo
 * @time   2023年3月27日11:14:14
 */
@Api(value = "RabbitmqAvailableJobController", tags = "中间件监控 - rabbitmq - 任务")
@RestController
@RequestMapping("/api/v1/rabbitmqAvailableJobController")
public class RabbitmqAvailableJobController extends AbstractController {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @ApiOperation("添加rabbitmq监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody RabbitmqAvailableJobAddVo rabbitmqAvailableJobAddVo) {
        if (null == rabbitmqAvailableJobAddVo || null == rabbitmqAvailableJobAddVo.getRabbitmqId() || Strings.isNullOrEmpty(rabbitmqAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加rabbitmq监控任务时，rabbitmq名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(rabbitmqAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(rabbitmqAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(rabbitmqAvailableJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = rabbitmqAvailableJobService.add(rabbitmqAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("查询rabbitmq监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<RabbitmqAvailableJobListRespData>>> queryList(@RequestBody RabbitmqAvailableJobListQueryVo rabbitmqAvailableJobListQueryVo) {
        if (null == rabbitmqAvailableJobListQueryVo || null == rabbitmqAvailableJobListQueryVo.getPageNo() || null == rabbitmqAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/rabbitmqAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<RabbitmqAvailableJobListRespData>> res = rabbitmqAvailableJobService.queryList(rabbitmqAvailableJobListQueryVo, rabbitmqAvailableJobListQueryVo.getIsMain());

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除rabbitmq监控任务")
    @ApiImplicitParam(name = "id", value = "mysql监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/rabbitmqAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = rabbitmqAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取rabbitmq监控任务详情")
    @ApiImplicitParam(name = "id", value = "rabbitmq监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<RabbitmqAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/rabbitmqAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<RabbitmqAvailableJobInfoRespPojo> res = rabbitmqAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新rabbitmq监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody RabbitmqAvailableJobUpdateVo rabbitmqAvailableJobUpdateVo) {
        if (null == rabbitmqAvailableJobUpdateVo || null == rabbitmqAvailableJobUpdateVo.getId() || null == rabbitmqAvailableJobUpdateVo.getRabbitmqId() || Strings.isNullOrEmpty(rabbitmqAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新rabbitmq监控任务时，主键、rabbitmq名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(rabbitmqAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(rabbitmqAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(rabbitmqAvailableJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = rabbitmqAvailableJobService.update(rabbitmqAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/rabbitmqAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = rabbitmqAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}