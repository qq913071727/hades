package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJobLog.ApiAvailableJobLogDataPojo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJobLog.ApiAvailableJobLogListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zengqinglong
 * @desc 接口可用日志管理
 * @Date 2023/3/30 10:01
 **/
@Api(value = "ApiAvailableJobLogController", tags = "系统监控 - 接口可用 - 日志")
@RestController
@RequestMapping("/api/v1/ApiAvailableJobLogController")
@Slf4j
public class ApiAvailableJobLogController extends AbstractController {
    @ApiOperation("查询接口可用日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ApiAvailableJobLogDataPojo>>> queryList(@RequestBody ApiAvailableJobLogListVo apiAvailableJobLogListVo) {
        if (null == apiAvailableJobLogListVo || null == apiAvailableJobLogListVo.getPageNo() || null == apiAvailableJobLogListVo.getPageSize()) {
            String message = "接口/api/v1/ApiAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }
        ServiceResult<PageInfo<ApiAvailableJobLogDataPojo>> res = apiAvailableJobLogService.queryList(apiAvailableJobLogListVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}
