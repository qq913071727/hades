package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.CellPhone;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.cellPhone.AlarmCellPhoneQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.CellPhoneService;
import com.innovation.ic.b1b.monitor.base.vo.CellPhoneVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * 手机号Controller
 */
@Api(value = "手机号Controller", tags = "CellPhoneController-配置管理-短信")
@RestController
@RequestMapping("/api/v1/cellPhone")
@Slf4j
public class CellPhoneController extends AbstractController {
    @Resource
    private CellPhoneService cellPhoneService;

    /**
     * 手机号
     */
    @ApiOperation(value = "添加或修改接口")
    @PostMapping("addCellPhone")
    public ResponseEntity<ApiResult> addCellPhone(@RequestBody CellPhoneVo cellPhoneVo) {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(cellPhoneVo.getCellPhone())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("手机号不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(cellPhoneVo.getRealName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("真实姓名不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = cellPhoneService.addCellPhone(cellPhoneVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除手机号
     */
    @ApiOperation(value = "删除手机号")
    @PostMapping("deleteCellPhone")
    public ResponseEntity<ApiResult> deleteCellPhone(@RequestBody CellPhoneVo cellPhoneVo) {
        ApiResult apiResult;
        if (cellPhoneVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        cellPhoneService.deleteCellPhone(cellPhoneVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询单个
     */
    @ApiOperation(value = "查询单个")
    @GetMapping("findById")
    public ResponseEntity<ApiResult> findById(CellPhoneVo cellPhoneVo,Boolean isMain) {
        ApiResult apiResult;
        if (cellPhoneVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        CellPhone cellPhone = cellPhoneService.findById(cellPhoneVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(cellPhone);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(CellPhoneVo cellPhoneVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        if (null == cellPhoneVo || null == cellPhoneVo.getPageNo() || null == cellPhoneVo.getPageSize()) {
            String message = "pageNo不能为空";
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (isMain == null) {
            isMain = Boolean.FALSE;
        }
        ServiceResult<PageInfo<CellPhone>> serviceResult = cellPhoneService.findByPage(cellPhoneVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation("查询报警手机号列表")
    @RequestMapping(value = "/queryAlarmCellPhoneList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<List<AlarmCellPhoneQueryRespPojo>>> queryAlarmCellPhoneList(Boolean isMain) {
        ServiceResult<List<AlarmCellPhoneQueryRespPojo>> res = cellPhoneService.queryAlarmCellPhoneList(isMain);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}