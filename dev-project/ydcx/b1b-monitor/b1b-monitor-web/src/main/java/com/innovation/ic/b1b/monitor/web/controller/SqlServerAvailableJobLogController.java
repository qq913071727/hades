package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJobLog.SqlServerAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJobLog.SqlServerAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   sql server可用任务日志表controller类
 * @author linuo
 * @time   2023年3月14日15:39:49
 */
@Api(value = "SqlServerAvailableJobLogController", tags = "中间件监控 - sql server - 日志")
@RestController
@RequestMapping("/api/v1/sqlServerAvailableJobLogController")
@Slf4j
public class SqlServerAvailableJobLogController extends AbstractController {
    @ApiOperation("查询sql server可用任务日志")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<SqlServerAvailableJobLogListRespData>>> queryList(@RequestBody SqlServerAvailableJobLogListQueryVo sqlServerAvailableJobLogListQueryVo) {
        if (null == sqlServerAvailableJobLogListQueryVo || null == sqlServerAvailableJobLogListQueryVo.getPageNo() || null == sqlServerAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/sqlServerAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<SqlServerAvailableJobLogListRespData>> res = sqlServerAvailableJobLogService.queryList(sqlServerAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}