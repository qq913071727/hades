package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   sql server可用任务表controller类
 * @author linuo
 * @time   2023年3月14日15:39:49
 */
@Api(value = "SqlServerAvailableJobController", tags = "中间件监控 - sql server - 任务")
@RestController
@RequestMapping("/api/v1/sqlServerAvailableJobController")
@Slf4j
public class SqlServerAvailableJobController extends AbstractController {
    @ApiOperation("查询sql server监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<SqlServerAvailableJobListRespData>>> queryList(@RequestBody SqlServerAvailableJobListQueryVo sqlServerAvailableJobListQueryVo) {
        if (null == sqlServerAvailableJobListQueryVo || null == sqlServerAvailableJobListQueryVo.getPageNo() || null == sqlServerAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/sqlServerAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<SqlServerAvailableJobListRespData>> res = sqlServerAvailableJobService.queryList(sqlServerAvailableJobListQueryVo, sqlServerAvailableJobListQueryVo.getIsMain());
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("添加sql server监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody SqlServerAvailableJobAddVo sqlServerAvailableJobAddVo) {
        if (null == sqlServerAvailableJobAddVo || null == sqlServerAvailableJobAddVo.getSqlServerId() || Strings.isNullOrEmpty(sqlServerAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加sql server监控任务时，sql server名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(sqlServerAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(sqlServerAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(sqlServerAvailableJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = sqlServerAvailableJobService.add(sqlServerAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除sql server监控任务")
    @ApiImplicitParam(name = "id", value = "sql server监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/sqlServerAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = sqlServerAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取sql server监控任务详情")
    @ApiImplicitParam(name = "id", value = "sql server监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<SqlServerAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/sqlServerAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<SqlServerAvailableJobInfoRespPojo> res = sqlServerAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新sql server监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody SqlServerAvailableJobUpdateVo sqlServerAvailableJobUpdateVo) {
        if (null == sqlServerAvailableJobUpdateVo || null == sqlServerAvailableJobUpdateVo.getId() || null == sqlServerAvailableJobUpdateVo.getSqlServerId() || Strings.isNullOrEmpty(sqlServerAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新sql server监控任务时，主键、sql server名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(sqlServerAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(sqlServerAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(sqlServerAvailableJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = sqlServerAvailableJobService.update(sqlServerAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/sqlServerAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = sqlServerAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}