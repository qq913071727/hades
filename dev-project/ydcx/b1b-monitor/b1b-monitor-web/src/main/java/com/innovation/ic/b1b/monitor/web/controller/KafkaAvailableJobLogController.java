package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJobLog.KafkaAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJobLog.KafkaAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   kafka可用任务日志表的controller
 * @author linuo
 * @time   2023年5月8日14:51:41
 */
@Api(value = "KafkaAvailableJobController", tags = "中间件监控 - kafka - 日志")
@RestController
@RequestMapping("/api/v1/kafkaAvailableJobLogController")
@Slf4j
public class KafkaAvailableJobLogController extends AbstractController {
    @ApiOperation("查询kafka监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<KafkaAvailableJobLogListRespData>>> queryList(@RequestBody KafkaAvailableJobLogListQueryVo kafkaAvailableJobLogListQueryVo) {
        if (null == kafkaAvailableJobLogListQueryVo || null == kafkaAvailableJobLogListQueryVo.getPageNo() || null == kafkaAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/kafkaAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<KafkaAvailableJobLogListRespData>> res = kafkaAvailableJobLogService.queryList(kafkaAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}