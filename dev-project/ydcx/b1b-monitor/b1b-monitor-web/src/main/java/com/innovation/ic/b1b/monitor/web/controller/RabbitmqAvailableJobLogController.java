package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   rabbitmq可用任务日志表controller类
 * @author linuo
 * @time   2023年3月27日15:15:34
 */
@Api(value = "RabbitmqAvailableJobLogController", tags = "中间件监控 - rabbitmq - 日志")
@RestController
@RequestMapping("/api/v1/rabbitmqAvailableJobLogController")
public class RabbitmqAvailableJobLogController extends AbstractController {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @ApiOperation("查询rabbitmq监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<RabbitmqAvailableJobLogListRespData>>> queryList(@RequestBody RabbitmqAvailableJobLogListQueryVo rabbitmqAvailableJobLogListQueryVo) {
        if (null == rabbitmqAvailableJobLogListQueryVo || null == rabbitmqAvailableJobLogListQueryVo.getPageNo() || null == rabbitmqAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/rabbitmqAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<RabbitmqAvailableJobLogListRespData>> res = rabbitmqAvailableJobLogService.queryList(rabbitmqAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}