package com.innovation.ic.b1b.monitor.web.controller;

import com.innovation.ic.b1b.framework.util.IpAddressUtil;
import com.innovation.ic.b1b.monitor.base.model.PageVisitLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.pageVisitLog.PageVisitLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.PageVisitLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 页面访问日志
 */
@Api(value = "PageVisitLogController", tags = "页面访问日志")
@RestController
@RequestMapping("/api/v1/pageVisitLog")
@Slf4j
public class PageVisitLogController extends AbstractController {

    /**
     * 添加PageVisitLog
     */
    @ApiOperation("添加页面访问日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageVisitLogVo", value = "页面访问日志Vo", required = true, dataType = "PageVisitLogVo")
    })
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> save(@RequestBody PageVisitLogVo pageVisitLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == pageVisitLogVo.getSystemId()) {
            message = "调用接口/api/v1/pageVisitLog/save时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == pageVisitLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/pageVisitLog/save时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == pageVisitLogVo.getServiceId()) {
            message = "调用接口/api/v1/pageVisitLog/save时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 插入记录
        PageVisitLog pageVisitLog = new PageVisitLog();
        BeanUtils.copyProperties(pageVisitLogVo, pageVisitLog);
        pageVisitLog.setCreateTime(new Date());
        pageVisitLog.setRemoteAddress(IpAddressUtil.getIpAddress(httpServletRequest));
        pageVisitLogService.savePageVisitLog(pageVisitLog);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查找页面访问日志，支持分页显示
     */
    @ApiOperation("查找页面访问日志，支持分页显示。systemId、environmentId、serviceId、model为0时，表示全选")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageVisitLogVo", value = "页面访问日志Vo", required = true, dataType = "PageVisitLogVo")
    })
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> page(@RequestBody PageVisitLogVo pageVisitLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == pageVisitLogVo.getPageNo() || null == pageVisitLogVo.getPageSize()) {
            message = "调用接口/api/v1/pageVisitLog/page时，pageNo和pageSize不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == pageVisitLogVo.getSystemId()) {
            message = "调用接口/api/v1/pageVisitLog/page时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == pageVisitLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/pageVisitLog/page时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == pageVisitLogVo.getServiceId()) {
            message = "调用接口/api/v1/pageVisitLog/page时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 查询记录
        if (pageVisitLogVo.getSystemId().equals(0)){
            pageVisitLogVo.setSystemId(null);
        }
        if (pageVisitLogVo.getServiceId().equals(0)){
            pageVisitLogVo.setServiceId(null);
        }
        if (pageVisitLogVo.getEnvironmentId().equals(0)){
            pageVisitLogVo.setEnvironmentId(null);
        }
        if (null != pageVisitLogVo.getModule() && pageVisitLogVo.getModule().equals(0)){
            pageVisitLogVo.setModule(null);
        }
        ServiceResult<PageVisitLogPagePojo> serviceResult = pageVisitLogService.page(pageVisitLogVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
