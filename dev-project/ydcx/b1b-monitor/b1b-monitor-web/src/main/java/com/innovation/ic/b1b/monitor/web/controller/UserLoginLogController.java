package com.innovation.ic.b1b.monitor.web.controller;

import com.innovation.ic.b1b.framework.util.IpAddressUtil;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.UserLoginLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog.UserLoginLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.UserLoginLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 用户登录日志
 */
@Api(value = "UserLoginLogController", tags = "用户登录日志")
@RestController
@RequestMapping("/api/v1/userLoginLog")
@Slf4j
public class UserLoginLogController extends AbstractController {

    /**
     * 添加UserLoginLog
     */
    @ApiOperation("添加用户登录日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userLoginLogVo", value = "用户登录日志Vo", required = true, dataType = "UserLoginLogVo")
    })
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> save(@RequestBody UserLoginLogVo userLoginLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == userLoginLogVo.getSystemId()) {
            message = "调用接口/api/v1/userLoginLog/save时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == userLoginLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/userLoginLog/save时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == userLoginLogVo.getServiceId()) {
            message = "调用接口/api/v1/userLoginLog/save时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == userLoginLogVo.getLoginType()) {
            message = "调用接口/api/v1/userLoginLog/save时，服务loginType不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(userLoginLogVo.getUsername())) {
            message = "调用接口/api/v1/userLoginLog/save时，服务username不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 插入记录
        UserLoginLog userLoginLog = new UserLoginLog();
        BeanUtils.copyProperties(userLoginLogVo, userLoginLog);
        userLoginLog.setCreateTime(new Date());
        userLoginLog.setRemoteAddress(IpAddressUtil.getIpAddress(httpServletRequest));
        userLoginLogService.saveUserLoginLog(userLoginLog);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查找用户登录日志，支持分页显示
     */
    @ApiOperation("查找用户登录日志，支持分页显示。systemId、environmentId、serviceId为0时，表示全选")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "UserLoginLogVo", value = "用户登录日志Vo", required = true, dataType = "UserLoginLogVo")
    })
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> page(@RequestBody UserLoginLogVo userLoginLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == userLoginLogVo.getPageNo() || null == userLoginLogVo.getPageSize()) {
            message = "调用接口/api/v1/userLoginLog/page时，pageNo和pageSize不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == userLoginLogVo.getSystemId()) {
            message = "调用接口/api/v1/userLoginLog/page时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == userLoginLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/userLoginLog/page时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == userLoginLogVo.getServiceId()) {
            message = "调用接口/api/v1/userLoginLog/page时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 查询记录
        if (userLoginLogVo.getSystemId().equals(0)){
            userLoginLogVo.setSystemId(null);
        }
        if (userLoginLogVo.getServiceId().equals(0)){
            userLoginLogVo.setServiceId(null);
        }
        if (userLoginLogVo.getEnvironmentId().equals(0)){
            userLoginLogVo.setEnvironmentId(null);
        }
        ServiceResult<UserLoginLogPagePojo> serviceResult = userLoginLogService.page(userLoginLogVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
