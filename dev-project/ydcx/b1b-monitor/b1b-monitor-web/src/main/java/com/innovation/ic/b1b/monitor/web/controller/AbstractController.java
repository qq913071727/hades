package com.innovation.ic.b1b.monitor.web.controller;

import com.innovation.ic.b1b.monitor.base.service.masterSlave.*;
import com.innovation.ic.b1b.monitor.base.service.sharding.FrontEndLogService;
import com.innovation.ic.b1b.monitor.base.service.sharding.InterfaceCallLogService;
import com.innovation.ic.b1b.monitor.base.service.sharding.PageVisitLogService;
import com.innovation.ic.b1b.monitor.base.service.sharding.UserLoginLogService;
import com.innovation.ic.im.end.base.service.impl.ClientService;
import org.quartz.CronExpression;
import javax.annotation.Resource;

/**
 * 抽象controller
 */
public abstract class AbstractController {
    @Resource
    protected ServerPerformanceJobService serverPerformanceJobService;

    @Resource
    protected ServerPerformanceJobLogService serverPerformanceJobLogService;

    @Resource
    protected SqlServerAvailableJobService sqlServerAvailableJobService;

    @Resource
    protected SqlServerAvailableJobLogService sqlServerAvailableJobLogService;

    @Resource
    protected MysqlAvailableJobService mysqlAvailableJobService;

    @Resource
    protected MysqlAvailableJobLogService mysqlAvailableJobLogService;

    @Resource
    protected MySqlService mySqlService;

    @Resource
    protected ServerService serverService;

    @Resource
    protected SqlServerService sqlServerService;

    @Resource
    protected ApiAvailableJobLogService apiAvailableJobLogService;

    @Resource
    protected ApiAvailableJobService apiAvailableJobService;

    @Resource
    protected ServiceActiveJobLogService serviceActiveJobLogService;

    @Resource
    protected PageVisitLogService pageVisitLogService;

    @Resource
    protected UserLoginLogService userLoginLogService;

    @Resource
    protected FrontEndLogService frontEndLogService;

    @Resource
    protected InterfaceCallLogService interfaceCallLogService;

    @Resource
    protected ElasticsearchAvailableJobLogService elasticsearchAvailableJobLogService;

    @Resource
    protected MongodbAvailableJobLogService mongodbAvailableJobLogService;

    @Resource
    protected RabbitmqAvailableJobLogService rabbitmqAvailableJobLogService;

    @Resource
    protected RedisAvailableJobLogService redisAvailableJobLogService;

    @Resource
    protected MongodbAvailableJobService mongodbAvailableJobService;

    @Resource
    protected ElasticsearchService elasticsearchService;

    @Resource
    protected ElasticsearchAvailableJobService elasticsearchAvailableJobService;

    @Resource
    protected MongodbService mongodbService;

    @Resource
    protected ClientService clientService;

    @Resource
    protected RabbitmqService rabbitmqService;

    @Resource
    protected RedisService redisService;
    @Resource
    protected KafkaService kafkaService;
    @Resource
    protected ModelService modelService;
    @Resource
    protected LogService logService;

    @Resource
    protected RabbitmqAvailableJobService rabbitmqAvailableJobService;

    @Resource
    protected RedisAvailableJobService redisAvailableJobService;

    @Resource
    protected ServiceActiveJobService serviceActiveJobService;

    @Resource
    protected ZookeeperAvailableJobService zookeeperAvailableJobService;

    @Resource
    protected ZookeeperAvailableJobLogService zookeeperAvailableJobLogService;

    @Resource
    protected KafkaAvailableJobService kafkaAvailableJobService;

    @Resource
    protected KafkaAvailableJobLogService kafkaAvailableJobLogService;

    @Resource
    protected CanalAvailableJobService canalAvailableJobService;

    @Resource
    protected CanalAvailableJobLogService canalAvailableJobLogService;

    /**
     * 校验cron表达式格式
     * @param cron 表达式
     * @return 返回校验结果
     */
    protected String checkCron(String cron){
        boolean validExpression = CronExpression.isValidExpression(cron);
        if(!validExpression){
            return "请输入正确的调用表达式(例：0 0/30 * * * ?)";
        }
        return null;
    }
}