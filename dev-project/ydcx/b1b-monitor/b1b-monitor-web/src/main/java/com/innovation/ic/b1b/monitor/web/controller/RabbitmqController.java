package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.pojo.RabbitmqPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmq.RabbitmqInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.RabbitmqVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * RabbitmqController
 */
@Api(value = "RabbitmqController", tags = "RabbitmqController-配置管理-Rabbitmq")
@RestController
@RequestMapping("/api/v1/rabbitmq")
@Slf4j
public class RabbitmqController extends AbstractController {
    /**
     * 添加
     */
    @ApiOperation(value = "添加或修改接口")
    @PostMapping("addRabbitmq")
    public ResponseEntity<ApiResult> addRabbitmq(@RequestBody RabbitmqVo rabbitmqVo) {
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(rabbitmqVo.getName())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (rabbitmqVo.getSystemId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("系统名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (rabbitmqVo.getEnvironmentId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("环境名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(rabbitmqVo.getIp())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("ip不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(rabbitmqVo.getPort())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("端口不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(rabbitmqVo.getUserName())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("用户名不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(rabbitmqVo.getPassword())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("密码不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(rabbitmqVo.getVirtualHost())) {
            rabbitmqVo.setVirtualHost("/");
        }
        ServiceResult serviceResult = rabbitmqService.addRabbitmq(rabbitmqVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除接口
     */
    @ApiOperation(value = "删除接口")
    @PostMapping("deleteRabbitmq")
    public ResponseEntity<ApiResult> deleteRabbitmq(@RequestBody RabbitmqVo rabbitmqVo) {
        ApiResult apiResult;
        if (rabbitmqVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        rabbitmqService.deleteRabbitmq(rabbitmqVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(RabbitmqVo rabbitmqVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        if (null == rabbitmqVo || null == rabbitmqVo.getPageNo() || null == rabbitmqVo.getPageSize()) {
            String message = "pageNo不能为空";
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (isMain == null) {
            isMain = Boolean.FALSE;
        }
        ServiceResult<PageInfo<RabbitmqPojo>> serviceResult = rabbitmqService.findByPage(rabbitmqVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation("查询rabbitmq信息列表")
    @RequestMapping(value = "/queryRabbitmqList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<List<RabbitmqInfoRespPojo>>> queryRabbitmqList(Boolean isMain) {
        ServiceResult<List<RabbitmqInfoRespPojo>> res = rabbitmqService.queryRabbitmqList(isMain);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}