package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc   zookeeper可用任务日志表的controller
 * @author linuo
 * @time   2023年5月8日14:51:41
 */
@Api(value = "ZookeeperAvailableJobLogController", tags = "中间件监控 - zookeeper - 日志")
@RestController
@RequestMapping("/api/v1/zookeeperAvailableJobLogController")
@Slf4j
public class ZookeeperAvailableJobLogController extends AbstractController {
    @ApiOperation("查询zookeeper监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ZookeeperAvailableJobLogListRespData>>> queryList(@RequestBody ZookeeperAvailableJobLogListQueryVo zookeeperAvailableJobLogListQueryVo) {
        if (null == zookeeperAvailableJobLogListQueryVo || null == zookeeperAvailableJobLogListQueryVo.getPageNo() || null == zookeeperAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/zookeeperAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<ZookeeperAvailableJobLogListRespData>> res = zookeeperAvailableJobLogService.queryList(zookeeperAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}