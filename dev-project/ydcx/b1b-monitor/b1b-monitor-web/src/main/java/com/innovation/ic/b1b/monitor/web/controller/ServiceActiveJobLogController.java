package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJobLog.ServiceActiveJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJobLog.ServiceActiveJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author swq
 * @desc 服务存活任务日志表controller类
 * @time 2023年3月29日10:08:29
 */
@Api(value = "ServiceActiveJobController", tags = "服务器存活 - 日志")
@RestController
@RequestMapping("/api/v1/serviceActiveJobLogController")
@Slf4j
public class ServiceActiveJobLogController  extends AbstractController{

    @ApiOperation("查询服务存活日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ServiceActiveJobLogListRespData>>> queryList(@RequestBody ServiceActiveJobLogListQueryVo serviceActiveJobLogListQueryVo) {
        if (null == serviceActiveJobLogListQueryVo || null == serviceActiveJobLogListQueryVo.getPageNo() || null == serviceActiveJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/serviceActiveJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<ServiceActiveJobLogListRespData>> data = serviceActiveJobLogService.queryList(serviceActiveJobLogListQueryVo);

        return new ResponseEntity<>(ApiResult.ok(data.getResult(), data.getMessage()), HttpStatus.OK);
    }
}
