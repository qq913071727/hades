package com.innovation.ic.b1b.monitor.web.controller;

import com.innovation.ic.b1b.framework.util.IpAddressUtil;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.InterfaceCallLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.InterfaceCallLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 接口调用日志
 */
@Api(value = "InterfaceCallLogController", tags = "接口调用日志")
@RestController
@RequestMapping("/api/v1/interfaceCallLog")
@Slf4j
public class InterfaceCallLogController extends AbstractController {

    /**
     * 添加InterfaceCallLog
     */
    @ApiOperation("添加接口调用日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "interfaceCallLogVo", value = "接口调用日志Vo", required = true, dataType = "InterfaceCallLogVo")
    })
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> save(@RequestBody InterfaceCallLogVo interfaceCallLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == interfaceCallLogVo.getSystemId()) {
            message = "调用接口/api/v1/interfaceCallLog/save时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == interfaceCallLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/interfaceCallLog/save时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == interfaceCallLogVo.getServiceId()) {
            message = "调用接口/api/v1/interfaceCallLog/save时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(interfaceCallLogVo.getMethod())) {
            message = "调用接口/api/v1/interfaceCallLog/save时，method不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(interfaceCallLogVo.getUrl())) {
            message = "调用接口/api/v1/interfaceCallLog/save时，url不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 插入记录
        InterfaceCallLog interfaceCallLog = new InterfaceCallLog();
        BeanUtils.copyProperties(interfaceCallLogVo, interfaceCallLog);
        interfaceCallLog.setCreateTime(new Date());
        interfaceCallLog.setRemoteAddress(IpAddressUtil.getIpAddress(httpServletRequest));
        interfaceCallLogService.saveInterfaceCallLog(interfaceCallLog);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查找接口调用日志，支持分页显示
     */
    @ApiOperation("查找接口调用日志，支持分页显示。systemId、environmentId、serviceId为0时，表示全选")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "interfaceCallLogVo", value = "接口调用日志Vo", required = true, dataType = "InterfaceCallLogVo")
    })
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> page(@RequestBody InterfaceCallLogVo interfaceCallLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == interfaceCallLogVo.getPageNo() || null == interfaceCallLogVo.getPageSize()) {
            message = "调用接口/api/v1/interfaceCallLog/page时，pageNo和pageSize不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == interfaceCallLogVo.getSystemId()) {
            message = "调用接口/api/v1/interfaceCallLog/page时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == interfaceCallLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/interfaceCallLog/page时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == interfaceCallLogVo.getServiceId()) {
            message = "调用接口/api/v1/interfaceCallLog/page时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 查询记录
        if (interfaceCallLogVo.getSystemId().equals(0)){
            interfaceCallLogVo.setSystemId(null);
        }
        if (interfaceCallLogVo.getServiceId().equals(0)){
            interfaceCallLogVo.setServiceId(null);
        }
        if (interfaceCallLogVo.getEnvironmentId().equals(0)){
            interfaceCallLogVo.setEnvironmentId(null);
        }
        ServiceResult<InterfaceCallLogPagePojo> serviceResult = interfaceCallLogService.page(interfaceCallLogVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
