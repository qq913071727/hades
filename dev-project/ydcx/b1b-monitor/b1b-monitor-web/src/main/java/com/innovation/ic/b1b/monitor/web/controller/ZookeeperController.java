package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.pojo.ZookeeperPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ZookeeperService;
import com.innovation.ic.b1b.monitor.base.vo.ZookeeperVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * LogController
 */
@Api(value = "ZookeeperController", tags = "ZookeeperController-配置管理-zookeeper")
@RestController
@RequestMapping("/api/v1/zookeeper")
@Slf4j
public class ZookeeperController extends AbstractController {

    @Resource
    private ZookeeperService zookeeperService;

    /**
     * 添加
     */
    @ApiOperation(value = "添加或修改接口")
    @PostMapping("addZookeeper")
    public ResponseEntity<ApiResult> addZookeeper(@RequestBody ZookeeperVo zookeeperVo) {
        ApiResult apiResult = new ApiResult<>();
        if (zookeeperVo.getSystemId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("系统名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (zookeeperVo.getEnvironmentId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("环境名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(zookeeperVo.getName())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = zookeeperService.addZookeeper(zookeeperVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除接口
     */
    @ApiOperation(value = "删除接口")
    @PostMapping("deleteLog")
    public ResponseEntity<ApiResult> deleteZookeeper(@RequestBody ZookeeperVo zookeeperVo) {
        ApiResult apiResult;
        if (zookeeperVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        zookeeperService.deleteLog(zookeeperVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(ZookeeperVo zookeeperVo, Boolean mainDb) {
        ApiResult apiResult = new ApiResult<>();
        if (null == zookeeperVo || null == zookeeperVo.getPageNo() || null == zookeeperVo.getPageSize()) {
            String message = "pageNo不能为空";
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (mainDb == null) {
            mainDb = Boolean.FALSE;
        }
        ServiceResult<PageInfo<ZookeeperPojo>> serviceResult = zookeeperService.findByPage(zookeeperVo, mainDb);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


}