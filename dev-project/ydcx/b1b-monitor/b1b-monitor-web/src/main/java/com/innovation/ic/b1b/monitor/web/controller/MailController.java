package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.Email;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.email.AlarmEmailQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EmailService;
import com.innovation.ic.b1b.monitor.base.vo.EmailVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * 邮箱Controller
 */
@Api(value = "邮箱Controller", tags = "MailController-配置管理-邮箱")
@RestController
@RequestMapping("/api/v1/mail")
@Slf4j
public class MailController extends AbstractController {
    @Resource
    private EmailService emailService;

    /**
     * 添加邮箱
     */
    @ApiOperation(value = "添加或修改接口")
    @PostMapping("addEmail")
    public ResponseEntity<ApiResult> addEmail(@RequestBody EmailVo emailVo) {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(emailVo.getEmail())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("邮箱不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(emailVo.getRealName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("真实姓名不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = emailService.addEmail(emailVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除邮箱
     */
    @ApiOperation(value = "删除邮箱")
    @PostMapping("deleteEmail")
    public ResponseEntity<ApiResult> deleteEmail(@RequestBody EmailVo emailVo) {
        ApiResult apiResult;
        if (emailVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        emailService.deleteEmail(emailVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询单个
     */
    @ApiOperation(value = "查询单个")
    @GetMapping("findById")
    public ResponseEntity<ApiResult> findById(EmailVo emailVo,Boolean isMain) {
        ApiResult apiResult;
        if (emailVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        Email email = emailService.findById(emailVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(email);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(EmailVo emailVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        if (null == emailVo || null == emailVo.getPageNo() || null == emailVo.getPageSize()) {
            String message = "pageNo不能为空";
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (isMain == null) {
            isMain = Boolean.FALSE;
        }
        ServiceResult<PageInfo<Email>> serviceResult = emailService.findByPage(emailVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation("查询报警邮箱列表")
    @RequestMapping(value = "/queryAlarmEmailList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<List<AlarmEmailQueryRespPojo>>> queryAlarmEmailList(Boolean isMain) {
        ServiceResult<List<AlarmEmailQueryRespPojo>> res = emailService.queryAlarmEmailList(isMain);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}