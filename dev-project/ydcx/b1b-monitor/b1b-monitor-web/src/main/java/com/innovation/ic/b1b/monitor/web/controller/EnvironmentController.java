package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.EnvironmentPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.EnvironmentVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 环境Controller
 */
@Api(value = "环境Controller", tags = "EnvironmentController-配置管理-环境")
@RestController
@RequestMapping("/api/v1/environment")
@Slf4j
public class EnvironmentController extends AbstractController {


    @Resource
    private EnvironmentService environmentService;


    /**
     * 添加环境
     */
    @ApiOperation(value = "添加环境")
    @PostMapping("addEnvironment")
    public ResponseEntity<ApiResult> addEnvironment(@RequestBody EnvironmentVo environmentVo) {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(environmentVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("环境名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (environmentVo.getSystemId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("系统id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
       ServiceResult serviceResult =  environmentService.addEnvironment(environmentVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除环境
     */
    @ApiOperation(value = "删除环境")
    @PostMapping("deleteEnvironment")
    public ResponseEntity<ApiResult> deleteEnvironment(@RequestBody EnvironmentVo environmentVo) {
        ApiResult apiResult;
        if (environmentVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        environmentService.deleteEnvironment(environmentVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询单个
     */
    @ApiOperation(value = "查询单个")
    @GetMapping("findById")
    public ResponseEntity<ApiResult> findById(EnvironmentVo environmentVo,Boolean isMain) {
        ApiResult apiResult;
        if (environmentVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        EnvironmentPojo environmentPojo = environmentService.findById(environmentVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(environmentPojo);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(EnvironmentVo environmentVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        if (null == environmentVo || null == environmentVo.getPageNo() || null == environmentVo.getPageSize()) {
            String message = "pageNo不能为空";
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (isMain == null) {
            isMain = Boolean.FALSE;
        }
        ServiceResult<PageInfo<EnvironmentPojo>> serviceResult = environmentService.findByPage(environmentVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }



    /**
     * 查询下拉
     */
    @ApiOperation(value = "查询下拉")
    @GetMapping("dropDown")
    public ResponseEntity<ApiResult> dropDown(EnvironmentVo environmentVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        List<EnvironmentPojo> environmentPojos = environmentService.dropDown(environmentVo,isMain);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(environmentPojos);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 添加或者修改下拉
     */
    @ApiOperation(value = "添加或者修改下拉")
    @GetMapping("addDropDown")
    public ResponseEntity<ApiResult> addDropDown(DropDownVo dropDownVo) {
        ApiResult apiResult = new ApiResult<>();
        List<DropDownPojo> environmentPojos = environmentService.addDropDown(dropDownVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(environmentPojos);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


}