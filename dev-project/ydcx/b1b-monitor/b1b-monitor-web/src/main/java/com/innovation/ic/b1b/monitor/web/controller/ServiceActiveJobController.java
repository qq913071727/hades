package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.ServiceActiveJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;

import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob.ServiceActiveJobListRespData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob.ServiceActiveJobListRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob.ServiceActiveJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob.ServiceActiveJobListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author swq
 * @desc 服务存活任务表controller类
 * @time 2023年3月29日10:08:29
 */
@Api(value = "ServiceActiveJobController", tags = "服务器存活 - 任务")
@RestController
@RequestMapping("/api/v1/serviceActiveJobController")
@Slf4j
public class ServiceActiveJobController extends AbstractController {
    @ApiOperation("查询服务存活任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ServiceActiveJobListRespData>>> queryList(@RequestBody ServiceActiveJobListQueryVo serviceActiveJobListQueryVo) {
        if (null == serviceActiveJobListQueryVo || null == serviceActiveJobListQueryVo.getPageNo() || null == serviceActiveJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/serviceActiveJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<ServiceActiveJobListRespData>> data = serviceActiveJobService.queryList(serviceActiveJobListQueryVo,serviceActiveJobListQueryVo.getIsMain());
        return new ResponseEntity<>(ApiResult.ok(data.getResult(), data.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取当前服务详情")
    @ApiImplicitParam(name = "id", value = "服务存活任务id主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<ServiceActiveJob>> info(@PathVariable("id") Integer id) {
        if (id == null) {
            String message = "调用接口【/api/v1/serviceActiveJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 获取当前服务详情
        ServiceResult<ServiceActiveJob> data = serviceActiveJobService.info(id);
        return new ResponseEntity<>(ApiResult.ok(data.getResult(), data.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("添加(修改)服务存活任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody ServiceActiveJobAddVo serviceActiveJobAddVo) {
        if (null == serviceActiveJobAddVo || null == serviceActiveJobAddVo.getSystemId() || null == serviceActiveJobAddVo.getEnvironmentId() ||
                null == serviceActiveJobAddVo.getServiceId() || null == serviceActiveJobAddVo.getEnable() || StringUtils.isEmpty(serviceActiveJobAddVo.getIp()) ||
                StringUtils.isEmpty(serviceActiveJobAddVo.getPort()) || StringUtils.isEmpty(serviceActiveJobAddVo.getUsername()) ||
                StringUtils.isEmpty(serviceActiveJobAddVo.getPassword()) || serviceActiveJobAddVo.getAlarmEmail() == null ||
                serviceActiveJobAddVo.getAlarmCellPhone() == null) {
            String message = "接口/api/v1/serviceActiveJobController/add的参数systemId、environmentId、serviceId " +
                    "、enable 、ip 、port 、username 、password 、alarmEmail 、alarmCellPhone不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }
        // 校验cron表达式格式
        String message = checkCron(serviceActiveJobAddVo.getScheduleExpression());
        if (message != null) {
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<Boolean> data = serviceActiveJobService.add(serviceActiveJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(data.getResult(), data.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除服务存活任务")
    @ApiImplicitParam(name = "id", value = "服务存活任务id主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if (id == null) {
            String message = "接口/api/v1/serviceActiveJobController/delete的参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        //删除对应的服务存活数据
        ServiceResult<Boolean> data = serviceActiveJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(data.getResult(), data.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "服务存活任务id主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if (id == null) {
            String message = "接口/api/v1/serviceActiveJobController/execute的参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> data = serviceActiveJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(data.getResult(), data.getMessage()), HttpStatus.OK);
    }
}
