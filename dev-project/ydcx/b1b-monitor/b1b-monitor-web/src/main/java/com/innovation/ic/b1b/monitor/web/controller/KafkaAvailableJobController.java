package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJob.KafkaAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJob.KafkaAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   kafka可用任务表的controller
 * @author linuo
 * @time   2023年3月24日14:38:52
 */
@Api(value = "KafkaAvailableJobController", tags = "中间件监控 - kafka - 任务")
@RestController
@RequestMapping("/api/v1/kafkaAvailableJobController")
@Slf4j
public class KafkaAvailableJobController extends AbstractController {
    @ApiOperation("添加kafka监控任务")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody KafkaAvailableJobAddVo kafkaAvailableJobAddVo) {
        if (null == kafkaAvailableJobAddVo || null == kafkaAvailableJobAddVo.getKafkaId() || Strings.isNullOrEmpty(kafkaAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加kafka监控任务时，kafka名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(kafkaAvailableJobAddVo.getAlarmEmail()) && Strings.isNullOrEmpty(kafkaAvailableJobAddVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(kafkaAvailableJobAddVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = kafkaAvailableJobService.add(kafkaAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("查询kafka监控任务列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<KafkaAvailableJobListRespData>>> queryList(@RequestBody KafkaAvailableJobListQueryVo kafkaAvailableJobListQueryVo) {
        if (null == kafkaAvailableJobListQueryVo || null == kafkaAvailableJobListQueryVo.getPageNo() || null == kafkaAvailableJobListQueryVo.getPageSize()) {
            String message = "接口/api/v1/kafkaAvailableJobController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 查询数据
        ServiceResult<PageInfo<KafkaAvailableJobListRespData>> res = kafkaAvailableJobService.queryList(kafkaAvailableJobListQueryVo, kafkaAvailableJobListQueryVo.getIsMain());
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("删除kafka监控任务")
    @ApiImplicitParam(name = "id", value = "kafka监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/kafkaAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = kafkaAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取kafka监控任务详情")
    @ApiImplicitParam(name = "id", value = "kafka监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<KafkaAvailableJobInfoRespPojo>> info(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/kafkaAvailableJobController/info】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<KafkaAvailableJobInfoRespPojo> res = kafkaAvailableJobService.info(id, Boolean.FALSE);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新kafka监控任务")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody KafkaAvailableJobUpdateVo kafkaAvailableJobUpdateVo) {
        if (null == kafkaAvailableJobUpdateVo || null == kafkaAvailableJobUpdateVo.getId() || null == kafkaAvailableJobUpdateVo.getKafkaId() || Strings.isNullOrEmpty(kafkaAvailableJobUpdateVo.getScheduleExpression())) {
            String message = "更新kafka监控任务时，主键、kafka名称、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if(Strings.isNullOrEmpty(kafkaAvailableJobUpdateVo.getAlarmEmail()) && Strings.isNullOrEmpty(kafkaAvailableJobUpdateVo.getAlarmCellPhone())){
            String message = "报警邮箱、报警手机号需填写至少一项";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(kafkaAvailableJobUpdateVo.getScheduleExpression());
        if(message != null){
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = kafkaAvailableJobService.update(kafkaAvailableJobUpdateVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "执行任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if(id == null){
            String message = "调用接口【/api/v1/kafkaAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = kafkaAvailableJobService.executeJob(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}