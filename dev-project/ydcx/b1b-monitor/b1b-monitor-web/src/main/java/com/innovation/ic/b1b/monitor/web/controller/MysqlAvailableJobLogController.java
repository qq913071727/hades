package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJobLog.MysqlAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJobLog.MysqlAvailableJobLogListQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   mysql可用任务日志表controller类
 * @author linuo
 * @time   2023年3月16日14:48:57
 */
@Api(value = "MysqlAvailableJobLogController", tags = "中间件监控 - mysql - 日志")
@RestController
@RequestMapping("/api/v1/mysqlAvailableJobLogController")
@Slf4j
public class MysqlAvailableJobLogController extends AbstractController {
    @ApiOperation("查询mysql监控任务日志列表")
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<MysqlAvailableJobLogListRespData>>> queryList(@RequestBody MysqlAvailableJobLogListQueryVo mysqlAvailableJobLogListQueryVo) {
        if (null == mysqlAvailableJobLogListQueryVo || null == mysqlAvailableJobLogListQueryVo.getPageNo() || null == mysqlAvailableJobLogListQueryVo.getPageSize()) {
            String message = "接口/api/v1/mysqlAvailableJobLogController/queryList的参数pageSize、pageNo不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<PageInfo<MysqlAvailableJobLogListRespData>> res = mysqlAvailableJobLogService.queryList(mysqlAvailableJobLogListQueryVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}