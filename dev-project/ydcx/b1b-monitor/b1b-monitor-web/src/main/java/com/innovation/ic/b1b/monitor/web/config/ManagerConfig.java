package com.innovation.ic.b1b.monitor.web.config;

import com.innovation.ic.b1b.framework.manager.*;
import com.innovation.ic.b1b.monitor.base.value.EMailConfig;
import com.innovation.ic.b1b.monitor.base.value.ThreadPoolConfig;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

@Configuration
public class ManagerConfig {

    /****************************************** quartz *********************************************/
    @Autowired
    private Scheduler scheduler;

    @Bean
    public QuartzManager quartzManager() {
        return new QuartzManager(scheduler);
    }

    @Bean
    public void quartz() {
//        QuartzManager quartzManager = quartzManager();
//        QuartzBean quartzBean = new QuartzBean();
//        quartzBean.setJobName("--------------------------------------");
//        quartzBean.setCronExpression("0 */1 * * * ?");
//        quartzBean.setId("1");
//        quartzManager.createScheduleJob("1");

//        quartzManager.pauseScheduleJob(scheduler, "1");

//        quartzManager.runOnce("1");

//        QuartzManager.resumeScheduleJob(scheduler, "1");

//        quartzBean.setJobName("wqqfqe");
//        quartzBean.setCronExpression("0 */1 * * * ?");
//        quartzManager.updateScheduleJob(quartzBean);
//
//
//        QuartzBean quartzBean2 = new QuartzBean();
//        quartzBean2.setJobName("&&&&&&&&&&&&&&&&&&&&&&&&&&");
//        quartzBean2.setCronExpression("0 */1 * * * ?");
//        quartzBean2.setId("2");
//        quartzManager.createScheduleJob(quartzBean2);
    }

    @Autowired
    private ThreadPoolConfig threadPoolConfig;

    @Bean
    public ThreadPoolManager threadPoolManager() {
        return new ThreadPoolManager(threadPoolConfig.getCorePoolSize(),
                threadPoolConfig.getMaximumPoolSize(), threadPoolConfig.getKeepAliveTime(),
                threadPoolConfig.getWorkQueue(), threadPoolConfig.getThreadNamePrefix());
    }

    /****************************************** email *********************************************/
    @Autowired
    private EMailConfig eMailConfig;

    @Bean
    public EMailManager eMailManager() {
        return new EMailManager(eMailConfig.getSendAccount(), eMailConfig.getSendPassword(), eMailConfig.getEMailSMTPHost());
    }

//    /****************************************** redis *********************************************/
//    @Resource
//    private RedisTemplate<String, String> redisTemplate;
//
//    @Bean
//    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
//        RedisSerializer stringSerializer = new StringRedisSerializer();
//        redisTemplate.setKeySerializer(stringSerializer);
//        redisTemplate.setStringSerializer(stringSerializer);
//        redisTemplate.setValueSerializer(stringSerializer);
//        redisTemplate.setHashKeySerializer(stringSerializer);
//        redisTemplate.setHashValueSerializer(stringSerializer);
//        return redisTemplate;
//    }
//
//    @Bean
//    public RedisManager redisManager() {
//        return new RedisManager(redisTemplate);
//    }
//
//    /****************************************** mongodb *********************************************/
//    @Resource
//    private MongoTemplate mongoTemplate;
//
//    @Bean
//    public MongodbManager mongodbManager() {
//        return new MongodbManager(mongoTemplate);
//    }
//
//    /****************************************** rabbitmq *********************************************/
//    @Resource
//    private RabbitMqParamConfig rabbitMqParamConfig;
//
//    @Bean
//    public RabbitMqManager rabbitMqManager() {
//        return new RabbitMqManager(rabbitMqParamConfig.getHost(), rabbitMqParamConfig.getPort(),
//                rabbitMqParamConfig.getUsername(), rabbitMqParamConfig.getPassword(),
//                rabbitMqParamConfig.getVirtualHost());
//    }
//
//    /****************************************** sftp *********************************************/
//    @Resource
//    private SftpParamConfig sftpParamConfig;
//
//    @Bean
//    public SftpChannelManager sftpChannelManager() {
//        return new SftpChannelManager(sftpParamConfig.getUsername(), sftpParamConfig.getPassword(),
//                sftpParamConfig.getHost(), sftpParamConfig.getPort(), sftpParamConfig.getTimeout());
//    }

    /****************************************** zookeeper *********************************************/
//    @Resource
//    private ZookeeperParamConfig zookeeperParamConfig;
//
//    @Bean
//    public ZookeeperManager zookeeperManager() {
//        return new ZookeeperManager(zookeeperParamConfig.getHost(), zookeeperParamConfig.getSessionTimeoutMs(),
//                zookeeperParamConfig.getConnectionTimeoutMs(), zookeeperParamConfig.getBaseSleepTimeMs(),
//                zookeeperParamConfig.getMaxEntries(), this.threadPoolManager().getThreadPoolTaskExecutor().getThreadPoolExecutor());
//    }

    /****************************************** aop *********************************************/
//    @Bean
//    public ZookeeperLockAspect zookeeperLockAspect(){
//        return new ZookeeperLockAspect();
//    }
//
//    @Bean
//    public ZookeeperUnLockAspect zookeeperUnLockAspect(){
//        return new ZookeeperUnLockAspect();
//    }
}
