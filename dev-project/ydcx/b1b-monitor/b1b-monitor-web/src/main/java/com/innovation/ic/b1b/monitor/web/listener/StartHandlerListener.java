package com.innovation.ic.b1b.monitor.web.listener;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.innovation.ic.b1b.monitor.base.handler.*;
import com.innovation.ic.b1b.monitor.base.model.*;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Component
public class StartHandlerListener implements ApplicationListener<ApplicationStartedEvent> {
    @Resource
    private ServiceHelper serviceHelper;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        // 加载ServerPerformanceJobHandler的定时任务
        loadServerPerformanceJobHandler();

        // 加载MysqlAvailableJobHandler的定时任务
        loadMysqlAvailableJobHandler();

        // 加载SqlServerAvailableJobHandler的定时任务
        loadSqlServerAvailableJobHandler();

        // 加载ElasticsearchAvailableJobHandler的定时任务
        loadElasticsearchAvailableJobHandler();

        // 加载ServiceActiveJobHandler的定时任务
        loadServiceActiveJobHandler();

        // 加载RabbitmqAvailableJobHandler的定时任务
        loadRabbitmqAvailableJobHandler();

        // 加载RedisAvailableJobHandler的定时任务
        loadRedisAvailableJobHandler();

        // 加载MongodbAvailableJobHandler的定时任务
        loadMongodbAvailableJobHandler();

        // 加载loadKafkaAvailableJobHandler的定时任务
        loadKafkaAvailableJobHandler();

        // 加载loadCanalAvailableJobHandler的定时任务
        loadCanalAvailableJobHandler();

        // 加载loadZookeeperAvailableJobHandler的定时任务
        loadZookeeperAvailableJobHandler();

        // 加载TransferHistoryDataHandler的定时任务
        loadTransferHistoryDataHandler();

        // 加载ApiActiveJobHandler的定时任务
        loadApiActiveJobHandler();

        // 加载ApiActiveJobHandler的定时任务
        loadLogJobHandler();
    }

    /**
     * @Description: 加载日志定时任务处理器
     * @Author: myq
     * @Date: 2023/5/10 16:17
     */
    private void loadLogJobHandler() {
        log.info("开始加载LogMonitorJobHandler的定时任务");

        List<LogMonitorJob> logMonitorJobs = serviceHelper.getLogMonitorJobMapper().selectList(new QueryWrapper<>());
        for (LogMonitorJob logMonitorJob : logMonitorJobs) {
            String jobId = TableNameConstant.LOG_MONITOR_JOB + "_" + logMonitorJob.getId();

            // 组装任务参数
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put(JobDataMapConstant.ID, logMonitorJob.getId());

            serviceHelper.getQuartzManager().createScheduleJob(jobId, LogMonitorJobHandler.class.getName(), "LogMonitorJobHandler", jobDataMap, logMonitorJob.getScheduleExpression());

            // 任务状态为关闭时需要暂停定时任务
            if(logMonitorJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
            }
        }
        log.info("加载LogMonitorJobHandler的定时任务完成");
    }

    /**
     * 加载MongodbAvailableJobHandler的定时任务
     */
    private void loadMongodbAvailableJobHandler() {
        log.info("开始加载MongodbAvailableJobHandler的定时任务");

        List<MongodbAvailableJob> mongodbAvailableJobs = serviceHelper.getMongodbAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != mongodbAvailableJobs && mongodbAvailableJobs.size() > 0) {
            for (MongodbAvailableJob mongodbAvailableJob : mongodbAvailableJobs) {
                String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + mongodbAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, mongodbAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, MongodbAvailableJobHandler.class.getName(), "MongodbAvailableJobHandler", jobDataMap, mongodbAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(mongodbAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载MongodbAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载loadKafkaAvailableJobHandler的定时任务
     */
    private void loadKafkaAvailableJobHandler() {
        log.info("开始加载loadKafkaAvailableJobHandler的定时任务");

        List<KafkaAvailableJob> kafkaAvailableJobs = serviceHelper.getKafkaAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != kafkaAvailableJobs && kafkaAvailableJobs.size() > 0) {
            for (KafkaAvailableJob kafkaAvailableJob : kafkaAvailableJobs) {
                String jobId = TableNameConstant.KAFKA_AVAILABLE_JOB + "_" + kafkaAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, kafkaAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, KafkaAvailableJobHandler.class.getName(), "KafkaAvailableJobHandler", jobDataMap, kafkaAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(kafkaAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载loadKafkaAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载loadCanalAvailableJobHandler的定时任务
     */
    private void loadCanalAvailableJobHandler() {
        log.info("开始加载loadCanalAvailableJobHandler的定时任务");

        List<CanalAvailableJob> canalAvailableJobs = serviceHelper.getCanalAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != canalAvailableJobs && canalAvailableJobs.size() > 0) {
            for (CanalAvailableJob canalAvailableJob : canalAvailableJobs) {
                String jobId = TableNameConstant.CANAL_AVAILABLE_JOB + "_" + canalAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, canalAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, CanalAvailableJobHandler.class.getName(), "CanalAvailableJobHandler", jobDataMap, canalAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(canalAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载loadCanalAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载loadZookeeperAvailableJobHandler的定时任务
     */
    private void loadZookeeperAvailableJobHandler() {
        log.info("开始加载loadZookeeperAvailableJobHandler的定时任务");

        List<ZookeeperAvailableJob> zookeeperAvailableJobs = serviceHelper.getZookeeperAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != zookeeperAvailableJobs && zookeeperAvailableJobs.size() > 0) {
            for (ZookeeperAvailableJob zookeeperAvailableJob : zookeeperAvailableJobs) {
                String jobId = TableNameConstant.ZOOKEEPER_AVAILABLE_JOB + "_" + zookeeperAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, zookeeperAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, ZookeeperAvailableJobHandler.class.getName(), "ZookeeperAvailableJobHandler", jobDataMap, zookeeperAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(zookeeperAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载loadZookeeperAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载RedisAvailableJobHandler的定时任务
     */
    private void loadRedisAvailableJobHandler() {
        log.info("开始加载RedisAvailableJobHandler的定时任务");

        List<RedisAvailableJob> redisAvailableJobs = serviceHelper.getRedisAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != redisAvailableJobs && redisAvailableJobs.size() > 0) {
            for (RedisAvailableJob redisAvailableJob : redisAvailableJobs) {
                String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + redisAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, redisAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, RedisAvailableJobHandler.class.getName(), "RedisAvailableJobHandler", jobDataMap, redisAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(redisAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载RedisAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载RabbitmqAvailableJobHandler的定时任务
     */
    private void loadRabbitmqAvailableJobHandler() {
        log.info("开始加载RabbitmqAvailableJobHandler的定时任务");

        List<RabbitmqAvailableJob> rabbitmqAvailableJobs = serviceHelper.getRabbitmqAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != rabbitmqAvailableJobs && rabbitmqAvailableJobs.size() > 0) {
            for (RabbitmqAvailableJob rabbitmqAvailableJob : rabbitmqAvailableJobs) {
                String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + rabbitmqAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, rabbitmqAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, RabbitmqAvailableJobHandler.class.getName(), "RabbitmqAvailableJobHandler", jobDataMap, rabbitmqAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(rabbitmqAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载RabbitmqAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载SqlServerAvailableJobHandler的定时任务
     */
    private void loadSqlServerAvailableJobHandler() {
        log.info("开始加载SqlServerAvailableJobHandler的定时任务");

        List<SqlServerAvailableJob> sqlServerAvailableJobs = serviceHelper.getSqlServerAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != sqlServerAvailableJobs && sqlServerAvailableJobs.size() > 0) {
            for (SqlServerAvailableJob sqlServerAvailableJob : sqlServerAvailableJobs) {
                String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + sqlServerAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, sqlServerAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, SqlServerAvailableJobHandler.class.getName(), "SqlServerAvailableJobHandler", jobDataMap, sqlServerAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(sqlServerAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
        log.info("加载SqlServerAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载MysqlAvailableJobHandler的定时任务
     */
    private void loadMysqlAvailableJobHandler() {
        log.info("开始加载MysqlAvailableJobHandler的定时任务");

        List<MysqlAvailableJob> mysqlAvailableJobs = serviceHelper.getMysqlAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != mysqlAvailableJobs && mysqlAvailableJobs.size() > 0) {
            for (MysqlAvailableJob mysqlAvailableJob : mysqlAvailableJobs) {
                String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + mysqlAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, mysqlAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, MysqlAvailableJobHandler.class.getName(), "MysqlAvailableJobHandler", jobDataMap, mysqlAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(mysqlAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }

        log.info("加载MysqlAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载ServerPerformanceJobHandler的定时任务
     */
    private void loadServerPerformanceJobHandler() {
        log.info("开始加载ServerPerformanceJobHandler的定时任务");

        List<ServerPerformanceJob> serverPerformanceJobList = serviceHelper.getServerPerformanceJobMapper().selectList(new QueryWrapper<>());
        if (null != serverPerformanceJobList && serverPerformanceJobList.size() > 0) {
            for (ServerPerformanceJob serverPerformanceJob : serverPerformanceJobList) {
                String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + serverPerformanceJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, serverPerformanceJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, ServerPerformanceJobHandler.class.getName(), "ServerPerformanceJobHandler", jobDataMap, serverPerformanceJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(serverPerformanceJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }

        log.info("加载ServerPerformanceJobHandler的定时任务完成");
    }

    /**
     * 加载ElasticsearchAvailableJobHandler的定时任务
     */
    private void loadElasticsearchAvailableJobHandler() {
        log.info("开始加载ElasticsearchAvailableJobHandler的定时任务");

        List<ElasticsearchAvailableJob> elasticsearchAvailableJobs = serviceHelper.getElasticsearchAvailableJobMapper().selectList(new QueryWrapper<>());
        if (null != elasticsearchAvailableJobs && elasticsearchAvailableJobs.size() > 0) {
            for (ElasticsearchAvailableJob elasticsearchAvailableJob : elasticsearchAvailableJobs) {
                String jobId = TableNameConstant.ELASTICSEARCH_AVAILABLE_JOB + "_" + elasticsearchAvailableJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, elasticsearchAvailableJob.getId());

                serviceHelper.getQuartzManager().createScheduleJob(jobId, ElasticsearchAvailableJobHandler.class.getName(), "ElasticsearchAvailableJobHandler", jobDataMap, elasticsearchAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(elasticsearchAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }

        log.info("加载ElasticsearchAvailableJobHandler的定时任务完成");
    }

    /**
     * 加载ServiceActiveJobHandler的定时任务
     */
    private void loadServiceActiveJobHandler() {
        log.info("开始加载ServiceActiveJobHandler的定时任务");

        List<ServiceActiveJob> serviceActiveJobList = serviceHelper.getServiceActiveJobMapper().selectList(new QueryWrapper<>());
        if (null != serviceActiveJobList && serviceActiveJobList.size() > 0) {
            for (ServiceActiveJob serviceActiveJob : serviceActiveJobList) {
                String jobId = TableNameConstant.SERVICE_ACTIVE_JOB + "_" + serviceActiveJob.getId();

                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, serviceActiveJob.getId());
                serviceHelper.getQuartzManager().createScheduleJob(jobId, ServiceActiveJobHandler.class.getName(), "ServiceActiveJobHandler", jobDataMap, serviceActiveJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(serviceActiveJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
    }

    /**
     * 加载TransferHistoryDataHandler的定时任务
     */
    private void loadTransferHistoryDataHandler() {
        log.info("开始TransferHistoryDataHandler的定时任务");
        //0 0 2 * * ? 2点后执行历史日志
        serviceHelper.getQuartzManager().createScheduleJob("transferHistoryDataHandler_1", TransferHistoryDataHandler.class.getName(), "TransferHistoryDataHandler",  new JobDataMap(),"0 0 2 * * ?");
    }

    /**
     * 加载ApiActiveJobHandler的定时任务
     */
    private void loadApiActiveJobHandler(){
        LambdaQueryWrapper<ApiAvailableJob> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ApiAvailableJob::getEnable, EnableEnum.YES.getCode());
        List<ApiAvailableJob> apiAvailableJobs = serviceHelper.getApiAvailableJobMapper().selectList(wrapper);
        if (null != apiAvailableJobs && apiAvailableJobs.size() > 0) {
            for (ApiAvailableJob apiAvailableJob : apiAvailableJobs) {
                String jobId = TableNameConstant.API_AVAILABLE_JOB + "_" + apiAvailableJob.getId();
                // 组装任务参数
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(JobDataMapConstant.ID, apiAvailableJob.getId());
                serviceHelper.getQuartzManager().createScheduleJob(jobId, ApiAvailableJobHandler.class.getName(), "ApiAvailableJobHandler", jobDataMap, apiAvailableJob.getScheduleExpression());

                // 任务状态为关闭时需要暂停定时任务
                if(apiAvailableJob.getEnable().intValue() == EnableEnum.NO.getCode().intValue()){
                    serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                }
            }
        }
    }
}