package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.pojo.SqlServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServer.SqlServerInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.SqlServerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * SqlServerController
 */
@Api(value = "SqlServerController", tags = "SqlServerController-配置管理-SqlServer")
@RestController
@RequestMapping("/api/v1/sqlServer")
@Slf4j
public class SqlServerController extends AbstractController {
    /**
     * 添加SqlServer
     */
    @ApiOperation(value = "添加或修改接口")
    @PostMapping("addSqlServer")
    public ResponseEntity<ApiResult> addSqlServer(@RequestBody SqlServerVo SqlServerVo) {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(SqlServerVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (SqlServerVo.getSystemId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("系统名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (SqlServerVo.getEnvironmentId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("环境名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(SqlServerVo.getUrl())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("url不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(SqlServerVo.getUserName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("用户名不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(SqlServerVo.getPassword())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("密码不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = sqlServerService.addSqlServer(SqlServerVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除SqlServer
     */
    @ApiOperation(value = "删除SqlServer")
    @PostMapping("deleteSqlServer")
    public ResponseEntity<ApiResult> deleteSqlServer(@RequestBody SqlServerVo sqlServerVo) {
        ApiResult apiResult;
        if (sqlServerVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        sqlServerService.deleteSqlServer(sqlServerVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询单个
     */
    @ApiOperation(value = "查询单个")
    @GetMapping("findById")
    public ResponseEntity<ApiResult> findById(SqlServerVo sqlServerVo,Boolean isMain) {
        ApiResult apiResult;
        if (sqlServerVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        SqlServerPojo email = sqlServerService.findById(sqlServerVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(email);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(SqlServerVo sqlServerVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        if (null == sqlServerVo || null == sqlServerVo.getPageNo() || null == sqlServerVo.getPageSize()) {
            String message = "pageNo不能为空";
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (isMain == null) {
            isMain = Boolean.FALSE;
        }
        ServiceResult<PageInfo<SqlServerPojo>> serviceResult = sqlServerService.findByPage(sqlServerVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation("查询sql server信息列表")
    @RequestMapping(value = "/querySqlServerList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<List<SqlServerInfoRespPojo>>> querySqlServerList(Boolean isMain) {
        ServiceResult<List<SqlServerInfoRespPojo>> res = sqlServerService.querySqlServerList(isMain);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}