package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJob.ApiAvailableJobDataPojo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author zengqinglong
 * @desc 接口调度管理
 * @Date 2023/3/30 9:59
 **/
@Api(value = "ApiAvailableJobController", tags = "系统监控 - 接口可用 - 任务")
@RestController
@RequestMapping("/api/v1/ApiAvailableJobController")
@Slf4j
public class ApiAvailableJobController extends AbstractController {
    /**
     * 添加接口可用监控任务
     *
     * @param apiAvailableJobAddVo
     * @return
     */
    @ApiOperation("添加接口监控任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiAvailableJobAddVo", value = "添加接口监控任务Vo", required = true, dataType = "ApiAvailableJobAddVo")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody ApiAvailableJobAddVo apiAvailableJobAddVo) {
        if (null == apiAvailableJobAddVo || null == apiAvailableJobAddVo.getApiId() || Strings.isNullOrEmpty(apiAvailableJobAddVo.getScheduleExpression())) {
            String message = "添加接口可用监控任务，接口地址、调用表达式均不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        if (null == apiAvailableJobAddVo.getAlarmEmailId() && null == apiAvailableJobAddVo.getAlarmCellPhoneId()) {
            String message = "报警邮箱、报警手机号必填";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(apiAvailableJobAddVo.getScheduleExpression());
        if (message != null) {
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = apiAvailableJobService.add(apiAvailableJobAddVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    /**
     * 查询接口可用监控任务列表
     *
     * @param apiAvailableJobListQueryVo
     * @return
     */
    @ApiOperation("查询接口可用监控任务列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiAvailableJobListQueryVo", value = "查询接口可用监控任务列表Vo", required = true, dataType = "ApiAvailableJobListQueryVo")
    })
    @RequestMapping(value = "/queryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<PageInfo<ApiAvailableJobDataPojo>>> queryList(@RequestBody ApiAvailableJobListQueryVo apiAvailableJobListQueryVo) {
        if (null == apiAvailableJobListQueryVo || null == apiAvailableJobListQueryVo.getPageNo() || null == apiAvailableJobListQueryVo.getPageSize() || null == apiAvailableJobListQueryVo.getIsMain()) {
            String message = "接口/api/v1/ApiAvailableJobController/queryList的参数pageSize、pageNo、isMain不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }
        // 查询数据
        ServiceResult<PageInfo<ApiAvailableJobDataPojo>> pageInfoServiceResult = apiAvailableJobService.queryList(apiAvailableJobListQueryVo,apiAvailableJobListQueryVo.getIsMain());
        return new ResponseEntity<>(ApiResult.ok(pageInfoServiceResult.getResult(), pageInfoServiceResult.getMessage()), HttpStatus.OK);
    }


    @ApiOperation("删除接口监控任务")
    @ApiImplicitParam(name = "id", value = "接口监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id") Integer id) {
        if (id == null) {
            String message = "调用接口【/api/v1/ApiAvailableJobController/delete】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = apiAvailableJobService.delete(id);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("获取接口监控任务详情")

    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "接口监控任务id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "isMain", value = "是否是主数据库", required = true, dataType = "Boolean"),
    })
    @RequestMapping(value = "/info/{id}/{isMain}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiAvailableJobDataPojo>> info(@PathVariable("id") Integer id,@PathVariable("isMain") Boolean isMain) {
        if (id == null || isMain == null) {
            String message = "调用接口【/api/v1/elasticsearchAvailableJobController/info】时，参数id、isMain不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<ApiAvailableJobDataPojo> res = apiAvailableJobService.info(id,isMain);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("更新接口监控任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "availableJobUpdateVo", value = "更新接口监控任务Vo", required = true, dataType = "ApiAvailableJobUpdateVo")
    })
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> update(@RequestBody ApiAvailableJobUpdateVo availableJobUpdateVo) {
        if (null == availableJobUpdateVo || null == availableJobUpdateVo.getId() || null == availableJobUpdateVo.getApiId()) {
            String message = "更新接口监控任务,主键,接口 不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        // 校验cron表达式格式
        String message = checkCron(availableJobUpdateVo.getScheduleExpression());
        if (message != null) {
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = apiAvailableJobService.update(availableJobUpdateVo);

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }

    @ApiOperation("执行任务")
    @ApiImplicitParam(name = "id", value = "接口监控任务id", required = true, dataType = "Integer")
    @RequestMapping(value = "/execute/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> execute(@PathVariable("id") Integer id) {
        if (id == null) {
            String message = "调用接口【/api/v1/elasticsearchAvailableJobController/execute】时，参数id不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> res = apiAvailableJobService.executeJob(id);

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }
}
