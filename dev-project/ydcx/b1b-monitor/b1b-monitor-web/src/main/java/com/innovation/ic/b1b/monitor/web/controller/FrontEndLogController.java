package com.innovation.ic.b1b.monitor.web.controller;

import com.innovation.ic.b1b.framework.util.IpAddressUtil;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.FrontEndLog;
import com.innovation.ic.b1b.monitor.base.model.InterfaceCallLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.frontEndLog.FrontEndLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.FrontEndLogVo;
import com.innovation.ic.b1b.monitor.base.vo.InterfaceCallLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Api(value = "FrontEndLogController", tags = "日志分析 - 前端日志")
@RestController
@RequestMapping("/api/v1/frontEndLog")
@Slf4j
public class FrontEndLogController extends AbstractController {

    /**
     * 添加FrontEndLog
     */
    @ApiOperation("添加前端日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "FrontEndLogVo", value = "前端日志Vo", required = true, dataType = "FrontEndLogVo")
    })
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> save(@RequestBody FrontEndLogVo frontEndLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == frontEndLogVo.getSystemId()) {
            message = "调用接口/api/v1/frontEndLog/save时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == frontEndLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/frontEndLog/save时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == frontEndLogVo.getServiceId()) {
            message = "调用接口/api/v1/frontEndLog/save时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(frontEndLogVo.getContent())) {
            message = "调用接口/api/v1/frontEndLog/save时，content不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 插入记录
        FrontEndLog frontEndLog = new FrontEndLog();
        BeanUtils.copyProperties(frontEndLogVo, frontEndLog);
        frontEndLog.setCreateTime(new Date());
        frontEndLogService.saveFrontEndLog(frontEndLog);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查找前端日志，支持分页显示
     */
    @ApiOperation("查找前端日志，支持分页显示。systemId、environmentId、serviceId、modelId为0时，表示全选")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "frontEndLogVo", value = "前端日志Vo", required = true, dataType = "FrontEndLogVo")
    })
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> page(@RequestBody FrontEndLogVo frontEndLogVo, HttpServletRequest httpServletRequest) {
        ApiResult apiResult;
        String message;

        if (null == frontEndLogVo.getPageNo() || null == frontEndLogVo.getPageSize()) {
            message = "调用接口/api/v1/frontEndLog/page时，pageNo和pageSize不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == frontEndLogVo.getSystemId()) {
            message = "调用接口/api/v1/frontEndLog/page时，系统id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == frontEndLogVo.getEnvironmentId()) {
            message = "调用接口/api/v1/frontEndLog/page时，环境id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("message");
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (null == frontEndLogVo.getServiceId()) {
            message = "调用接口/api/v1/frontEndLog/page时，服务id不能为空";
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            log.warn(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 查询记录
        if (frontEndLogVo.getSystemId().equals(0)){
            frontEndLogVo.setSystemId(null);
        }
        if (frontEndLogVo.getServiceId().equals(0)){
            frontEndLogVo.setServiceId(null);
        }
        if (frontEndLogVo.getEnvironmentId().equals(0)){
            frontEndLogVo.setEnvironmentId(null);
        }
        if (frontEndLogVo.getModelId().equals(0)){
            frontEndLogVo.setModelId(null);
        }
        ServiceResult<FrontEndLogPagePojo> serviceResult = frontEndLogService.page(frontEndLogVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage("成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
