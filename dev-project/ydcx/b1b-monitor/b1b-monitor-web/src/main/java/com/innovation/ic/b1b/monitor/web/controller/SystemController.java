package com.innovation.ic.b1b.monitor.web.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.monitor.base.model.System;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.SystemService;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.SystemVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统Controller
 */
@Api(value = "系统Controller", tags = "SystemController-配置管理-系统")
@RestController
@RequestMapping("/api/v1/system")
@Slf4j
public class SystemController extends AbstractController {


    @Resource
    private SystemService systemService;


    /**
     * 添加系统
     */
    @ApiOperation(value = "添加系统")
    @PostMapping("addSystem")
    public ResponseEntity<ApiResult> addSystem(@RequestBody SystemVo systemVo) {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(systemVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("name不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = systemService.addSystem(systemVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除系统
     */
    @ApiOperation(value = "删除系统")
    @PostMapping("deleteSystem")
    public ResponseEntity<ApiResult> deleteSystem(@RequestBody SystemVo systemVo) {
        ApiResult apiResult;
        if (systemVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        systemService.deleteSystem(systemVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("删除成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询单个
     */
    @ApiOperation(value = "查询单个")
    @GetMapping("findById")
    public ResponseEntity<ApiResult> findById(SystemVo systemVo,Boolean isMain) {
        ApiResult apiResult;
        if (systemVo.getId() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        System system = systemService.findById(systemVo,isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(system);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 查询分页
     */
    @ApiOperation(value = "查询分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult> findByPage(SystemVo systemVo, Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        if (null == systemVo || null == systemVo.getPageNo() || null == systemVo.getPageSize()) {
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("分页不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (isMain == null) {
            isMain = Boolean.FALSE;
        }
        ServiceResult<PageInfo<System>> serviceResult = systemService.findByPage(systemVo, isMain);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 查询下拉
     */
    @ApiOperation(value = "查询下拉")
    @GetMapping("dropDown")
    public ResponseEntity<ApiResult> dropDown(DropDownVo dropDownVo,Boolean isMain) {
        ApiResult apiResult = new ApiResult<>();
        List<DropDownPojo> environmentPojos = systemService.dropDown(dropDownVo,isMain);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("查询成功");
        apiResult.setResult(environmentPojos);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


}