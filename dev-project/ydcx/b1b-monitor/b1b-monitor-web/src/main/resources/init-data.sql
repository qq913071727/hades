/*------------------------------------------------------------------------
--                                  service_active_job表
------------------------------------------------------------------------*/
-- 即时通信
INSERT INTO `service_active_job` VALUES ('1', '1', '1', '1', '192.168.80.109', '22', 'root', 'Ydec2022', 'nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('2', '1', '1', '2', '192.168.80.109', '22', 'root', 'Ydec2022', 'java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/im-end-eureka-dev.hprof -jar /opt/im-end-eureka.jar --spring.profiles.active=dev --spring.application.name=im-end-eureka --server.port=11101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('3', '1', '1', '3', '192.168.80.109', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/im-end-gateway-dev.hprof -jar /opt/im-end-gateway.jar --spring.profiles.active=dev --server.port=17101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('4', '1', '1', '4', '192.168.80.109', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/im-end-data-dev.hprof -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5003 -jar /opt/im-end-data.jar --server.port=14101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('5', '1', '1', '5', '192.168.80.109', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/im-end-web-dev.hprof -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5001 -jar /opt/im-end-web.jar --spring.profiles.active=dev --server.port=13101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('6', '1', '2', '1', '192.168.80.110', '22', 'root', 'Ydec2022', 'nginx: master process /data/nginx-1.22.0/objs/nginx -c /etc/nginx/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('7', '1', '2', '2', '192.168.80.110', '22', 'root', 'Ydec2022', 'java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/project/im-end-test/im-end-eureka-test.hprof -jar /opt/project/im-end-test/im-end-eureka.jar --spring.application.name=im-end-eureka --spring.profiles.active=test --eureka.client.serviceUrl.defaultZone=http://127.0.0.1:11202/eureka/ --server.port=11201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('8', '1', '2', '3', '192.168.80.110', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/project/im-end-test/im-end-gateway-test.hprof -jar /opt/project/im-end-test/im-end-gateway.jar --spring.profiles.active=test --eureka.client.serviceUrl.defaultZone=http://localhost:11201/eureka/,http://localhost:11202/eureka/ --server.port=17201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('9', '1', '2', '4', '192.168.80.110', '22', 'root', 'Ydec2022', 'java -XX:NativeMemoryTracking=detail -jar -Xms1024m -Xmx2048m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/project/im-end-test/im-end-data-test.hprof -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5203 /opt/project/im-end-test/im-end-data.jar --Xms1024M --Xmx1024M --spring.profiles.active=test --eureka.client.serviceUrl.defaultZone=http://localhost:11201/eureka/,http://localhost:11202/eureka/ --server.port=14201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('10', '1', '2', '5', '192.168.80.110', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/project/im-end-test/im-end-web-13201-test.hprof -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5201 -jar /opt/project/im-end-test/im-end-web.jar --spring.profiles.active=test --eureka.client.serviceUrl.defaultZone=http://localhost:11201/eureka/,http://localhost:11202/eureka/ --server.port=13201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('11', '1', '4', '1', '172.16.95.201', '58701', 'root', 'Ydec.iM2022062$', 'nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('12', '1', '4', '2', '172.16.95.201', '58701', 'root', 'Ydec.iM2022062$', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/prod/im-end-eureka-prod.hprof -jar /home/prod/im-end-eureka.jar --spring.application.name=im-end-eureka --eureka.client.serviceUrl.defaultZone=http://127.0.0.1:11302/eureka/ --server.port=11301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('13', '1', '4', '3', '172.16.95.201', '58701', 'root', 'Ydec.iM2022062$', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/prod/im-end-gateway-prod.hprof -jar /home/prod/im-end-gateway.jar --eureka.client.serviceUrl.defaultZone=http://localhost:11301/eureka/,http://localhost:11302/eureka/ --server.port=17301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('14', '1', '4', '4', '172.16.95.201', '58701', 'root', 'Ydec.iM2022062$', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/prod/im-end-data-14301-prod.hprof -Dcom.sun.management.jmxremote.port=5105 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5305 -jar /home/prod/im-end-data.jar --eureka.client.serviceUrl.defaultZone=http://localhost:11301/eureka/,http://localhost:11302/eureka/ --server.port=14301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('15', '1', '4', '5', '172.16.95.201', '58701', 'root', 'Ydec.iM2022062$', 'java -Xms1024M -Xmx2048M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/prod/im-end-web-13301-prod.hprof -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=5101 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5301 -jar /home/prod/im-end-web.jar --eureka.client.serviceUrl.defaultZone=http://localhost:11301/eureka/,http://localhost:11302/eureka/ --server.port=13301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
-- 供应商协同
INSERT INTO `service_active_job` VALUES ('16', '3', '1', '13', '192.168.80.111', '22', 'root', 'Ydec2022', 'nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('17', '3', '1', '14', '192.168.80.111', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/project/sc-gateway-dev.hprof -jar /data/project/sc/sc-gateway/target/sc-gateway.jar --server.port=32101 --spring.application.name=sc-gateway', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('18', '3', '1', '15', '192.168.80.111', '22', 'root', 'Ydec2022', 'java -Xms1024m -Xmx2048m -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/project/sc/sc-web-dev.hprof -Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=192.168.80.111 -Dcom.sun.management.jmxremote.port=3311 -Dcom.sun.management.jmxremote.rmi.port=3311 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -jar /data/project/sc/sc-web/target/sc-web.jar --spring.application.name=sc-web --server.port=33101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('19', '3', '1', '16', '192.168.80.111', '22', 'root', 'Ydec2022', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/project/sc-nacos-config-dev.hprof -jar /data/project/sc/sc-nacos-config/target/sc-nacos-config.jar --server.port=34101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('20', '3', '1', '17', '192.168.80.111', '22', 'root', 'Ydec2022', '/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/bin/java -Djava.ext.dirs=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/jre/lib/ext:/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/project/sc/nacosserver/nacos1/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/data/project/sc/nacosserver/nacos1/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/data/project/sc/nacosserver/nacos1/plugins/health,/data/project/sc/nacosserver/nacos1/plugins/cmdb,/data/project/sc/nacosserver/nacos1/plugins/selector -Dnacos.home=/data/project/sc/nacosserver/nacos1 -jar /data/project/sc/nacosserver/nacos1/target/nacos-server.jar --spring.config.additional-location=file:/data/project/sc/nacosserver/nacos1/conf/ --logging.config=/data/project/sc/nacosserver/nacos1/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('21', '3', '1', '18', '192.168.80.111', '22', 'root', 'Ydec2022', '/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/bin/java -Djava.ext.dirs=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/jre/lib/ext:/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/project/sc/nacosserver/nacos2/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/data/project/sc/nacosserver/nacos2/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/data/project/sc/nacosserver/nacos2/plugins/health,/data/project/sc/nacosserver/nacos2/plugins/cmdb,/data/project/sc/nacosserver/nacos2/plugins/selector -Dnacos.home=/data/project/sc/nacosserver/nacos2 -jar /data/project/sc/nacosserver/nacos2/target/nacos-server.jar --spring.config.additional-location=file:/data/project/sc/nacosserver/nacos2/conf/ --logging.config=/data/project/sc/nacosserver/nacos2/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('22', '3', '1', '19', '192.168.80.111', '22', 'root', 'Ydec2022', '/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/bin/java -Djava.ext.dirs=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/jre/lib/ext:/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.342.b07-1.el7_9.x86_64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/project/sc/nacosserver/nacos3/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/data/project/sc/nacosserver/nacos3/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/data/project/sc/nacosserver/nacos3/plugins/health,/data/project/sc/nacosserver/nacos3/plugins/cmdb,/data/project/sc/nacosserver/nacos3/plugins/selector -Dnacos.home=/data/project/sc/nacosserver/nacos3 -jar /data/project/sc/nacosserver/nacos3/target/nacos-server.jar --spring.config.additional-location=file:/data/project/sc/nacosserver/nacos3/conf/ --logging.config=/data/project/sc/nacosserver/nacos3/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('23', '3', '1', '20', '192.168.80.111', '22', 'root', 'Ydec2022', '/usr/bin/java -jar /data/project/sc/xxl-job/xxl-job-admin_sc-dev.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('24', '3', '2', '13', '192.168.80.61', '22', 'root', 'Aa123qwe', 'nginx: master process /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('25', '3', '2', '14', '192.168.80.61', '22', 'root', 'Aa123qwe', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc-test/sc-web-api/sc-gateway-test.hprof -jar /home/project/sc-test/sc-web-api/sc-gateway.jar --spring.profiles.active=test --server.port=32201 --spring.application.name=sc-gateway', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('26', '3', '2', '15', '192.168.80.61', '22', 'root', 'Aa123qwe', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc-test/sc-web-api/sc-web-test.hprof -jar /home/project/sc-test/sc-web-api/sc-web.jar --spring.profiles.active=test --spring.application.name=sc-web --server.port=33201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('27', '3', '2', '16', '192.168.80.61', '22', 'root', 'Aa123qwe', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc-test/sc-web-api/sc-nacos-config-test.hprof -jar /home/project/sc-test/sc-web-api/sc-nacos-config.jar --spring.profiles.active=test --server.port=34201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('28', '3', '2', '17', '192.168.80.61', '22', 'root', 'Aa123qwe', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc-test/nacos-cluster/nacos1/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/sc-test/nacos-cluster/nacos1/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/sc-test/nacos-cluster/nacos1/plugins/health,/home/project/sc-test/nacos-cluster/nacos1/plugins/cmdb,/home/project/sc-test/nacos-cluster/nacos1/plugins/selector -Dnacos.home=/home/project/sc-test/nacos-cluster/nacos1 -jar /home/project/sc-test/nacos-cluster/nacos1/target/nacos-server.jar --spring.config.additional-location=file:/home/project/sc-test/nacos-cluster/nacos1/conf/ --logging.config=/home/project/sc-test/nacos-cluster/nacos1/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('29', '3', '2', '18', '192.168.80.61', '22', 'root', 'Aa123qwe', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc-test/nacos-cluster/nacos2/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/sc-test/nacos-cluster/nacos2/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/sc-test/nacos-cluster/nacos2/plugins/health,/home/project/sc-test/nacos-cluster/nacos2/plugins/cmdb,/home/project/sc-test/nacos-cluster/nacos2/plugins/selector -Dnacos.home=/home/project/sc-test/nacos-cluster/nacos2 -jar /home/project/sc-test/nacos-cluster/nacos2/target/nacos-server.jar --spring.config.additional-location=file:/home/project/sc-test/nacos-cluster/nacos2/conf/ --logging.config=/home/project/sc-test/nacos-cluster/nacos2/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('30', '3', '2', '19', '192.168.80.61', '22', 'root', 'Aa123qwe', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc-test/nacos-cluster/nacos3/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/sc-test/nacos-cluster/nacos3/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/sc-test/nacos-cluster/nacos3/plugins/health,/home/project/sc-test/nacos-cluster/nacos3/plugins/cmdb,/home/project/sc-test/nacos-cluster/nacos3/plugins/selector -Dnacos.home=/home/project/sc-test/nacos-cluster/nacos3 -jar /home/project/sc-test/nacos-cluster/nacos3/target/nacos-server.jar --spring.config.additional-location=file:/home/project/sc-test/nacos-cluster/nacos3/conf/ --logging.config=/home/project/sc-test/nacos-cluster/nacos3/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('31', '3', '2', '20', '192.168.80.61', '22', 'root', 'Aa123qwe', '/usr/java/jdk1.8.0_301-amd64/bin/java -jar /opt/xxljob/xxl-job-admin_sc-test.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('32', '3', '4', '13', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', 'nginx: master process /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('33', '3', '4', '14', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc/sc-gateway-prod.hprof -jar /home/project/sc/sc-gateway.jar --server.port=32301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('34', '3', '4', '15', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc/sc-web-33301-prod.hprof -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5301 -jar /home/project/sc/sc-web.jar --server.port=33301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('35', '3', '4', '16', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', 'java -Xms1024M -Xmx2048M -XX:PermSize=1024M -XX:MaxPermSize=2048M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc/sc-nacos-config-prod.hprof -jar /home/project/sc/sc-nacos-config.jar --spring.application.name=sc-nacos-config --server.port=34301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('36', '3', '4', '17', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc/nacos-cluster/nacos1/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/sc/nacos-cluster/nacos1/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/sc/nacos-cluster/nacos1/plugins/health,/home/project/sc/nacos-cluster/nacos1/plugins/cmdb,/home/project/sc/nacos-cluster/nacos1/plugins/selector -Dnacos.home=/home/project/sc/nacos-cluster/nacos1 -jar /home/project/sc/nacos-cluster/nacos1/target/nacos-server.jar --spring.config.additional-location=file:/home/project/sc/nacos-cluster/nacos1/conf/ --logging.config=/home/project/sc/nacos-cluster/nacos1/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('37', '3', '4', '18', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc/nacos-cluster/nacos2/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/sc/nacos-cluster/nacos2/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/sc/nacos-cluster/nacos2/plugins/health,/home/project/sc/nacos-cluster/nacos2/plugins/cmdb,/home/project/sc/nacos-cluster/nacos2/plugins/selector -Dnacos.home=/home/project/sc/nacos-cluster/nacos2 -jar /home/project/sc/nacos-cluster/nacos2/target/nacos-server.jar --spring.config.additional-location=file:/home/project/sc/nacos-cluster/nacos2/conf/ --logging.config=/home/project/sc/nacos-cluster/nacos2/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('38', '3', '4', '19', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/sc/nacos-cluster/nacos3/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/sc/nacos-cluster/nacos3/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/sc/nacos-cluster/nacos3/plugins/health,/home/project/sc/nacos-cluster/nacos3/plugins/cmdb,/home/project/sc/nacos-cluster/nacos3/plugins/selector -Dnacos.home=/home/project/sc/nacos-cluster/nacos3 -jar /home/project/sc/nacos-cluster/nacos3/target/nacos-server.jar --spring.config.additional-location=file:/home/project/sc/nacos-cluster/nacos3/conf/ --logging.config=/home/project/sc/nacos-cluster/nacos3/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('39', '3', '4', '20', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', '/usr/bin/java -jar /home/project/sc/xxl-job-admin_sc-prod.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
-- b1b产品数据导入
INSERT INTO `service_active_job` VALUES ('40', '4', '1', '21', '192.168.80.60', '22', 'root', 'Aa123qwe!!$)', '/usr/java/jdk1.8.0_301-amd64/bin/java -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.rmi.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=192.168.80.60 -Xms2048M -Xmx5144M -XX:+UseG1GC -jar /opt/erp9-syn-data/erp9-syn-data.jar --spring.profiles.active=dev --server.port=24500', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('41', '4', '1', '22', '192.168.80.60', '22', 'root', 'Aa123qwe!!$)', '/usr/java/jdk1.8.0_301-amd64/bin/java -jar /opt/xxljob/xxl-job-admin_b1b-data-dev.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('42', '4', '2', '21', null, null, null, null, null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('43', '4', '2', '22', null, null, null, null, null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('44', '4', '4', '21', '172.16.95.77', '58700', 'root', 'Aa62681155Kk', '/usr/java/jdk1.8.0_301-amd64/bin/java -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.rmi.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=172.16.95.77 -Xms2048M -Xmx7144M -XX:+UseG1GC -jar /opt/erp9-syn-data/erp9-syn-data.jar --server.port=11201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('45', '4', '4', '22', '172.16.95.77', '58700', 'root', 'Aa62681155Kk', '/usr/java/jdk1.8.0_301-amd64/bin/java -jar /opt/xxljob/xxl-job-admin_b1b-data-prod.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
-- 客户协同
INSERT INTO `service_active_job` VALUES ('46', '5', '1', '23', '192.168.80.112', '22', 'root', 'gEMm27g1', 'nginx: master process /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('47', '5', '1', '24', '192.168.80.112', '22', 'root', 'gEMm27g1', 'java -jar /home/project/cc/web-api/cc-gateway.jar --Xms1024M --Xmx1024M --XX:PermSize=256M --XX:MaxPermSize=256M --spring.profiles.active=dev --server.port=52101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('48', '5', '1', '25', '192.168.80.112', '22', 'root', 'gEMm27g1', 'java -jar -Xdebug /home/project/cc/web-api/cc-web.jar --Xms1024M --Xmx1024M --XX:PermSize=256M --XX:MaxPermSize=256M --spring.profiles.active=dev --server.port=53101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('49', '5', '1', '26', '192.168.80.112', '22', 'root', 'gEMm27g1', 'java -jar /home/project/cc/web-api/cc-nacos-config.jar --Xms1024M --Xmx1024M --XX:PermSize=256M --XX:MaxPermSize=256M --spring.profiles.active=dev --server.port=54101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('50', '5', '1', '27', '192.168.80.112', '22', 'root', 'gEMm27g1', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/cc/nacos-cluster/nacos1/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/cc/nacos-cluster/nacos1/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/cc/nacos-cluster/nacos1/plugins/health,/home/project/cc/nacos-cluster/nacos1/plugins/cmdb,/home/project/cc/nacos-cluster/nacos1/plugins/selector -Dnacos.home=/home/project/cc/nacos-cluster/nacos1 -jar /home/project/cc/nacos-cluster/nacos1/target/nacos-server.jar --spring.config.additional-location=file:/home/project/cc/nacos-cluster/nacos1/conf/ --logging.config=/home/project/cc/nacos-cluster/nacos1/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('51', '5', '1', '28', '192.168.80.112', '22', 'root', 'gEMm27g1', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/cc/nacos-cluster/nacos2/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/cc/nacos-cluster/nacos2/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/cc/nacos-cluster/nacos2/plugins/health,/home/project/cc/nacos-cluster/nacos2/plugins/cmdb,/home/project/cc/nacos-cluster/nacos2/plugins/selector -Dnacos.home=/home/project/cc/nacos-cluster/nacos2 -jar /home/project/cc/nacos-cluster/nacos2/target/nacos-server.jar --spring.config.additional-location=file:/home/project/cc/nacos-cluster/nacos2/conf/ --logging.config=/home/project/cc/nacos-cluster/nacos2/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('52', '5', '1', '29', '192.168.80.112', '22', 'root', 'gEMm27g1', '/usr/java/jdk1.8.0_301-amd64/bin/java -Djava.ext.dirs=/usr/java/jdk1.8.0_301-amd64/jre/lib/ext:/usr/java/jdk1.8.0_301-amd64/lib/ext -server -Xms1g -Xmx1g -Xmn512m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/project/cc/nacos-cluster/nacos3/logs/java_heapdump.hprof -XX:-UseLargePages -Dnacos.member.list= -Xloggc:/home/project/cc/nacos-cluster/nacos3/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/home/project/cc/nacos-cluster/nacos3/plugins/health,/home/project/cc/nacos-cluster/nacos3/plugins/cmdb,/home/project/cc/nacos-cluster/nacos3/plugins/selector -Dnacos.home=/home/project/cc/nacos-cluster/nacos3 -jar /home/project/cc/nacos-cluster/nacos3/target/nacos-server.jar --spring.config.additional-location=file:/home/project/cc/nacos-cluster/nacos3/conf/ --logging.config=/home/project/cc/nacos-cluster/nacos3/conf/nacos-logback.xml --server.max-http-header-size=524288 nacos.nacos', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('53', '5', '1', '30', '192.168.80.62', '22', 'root', 'Aa123qwe', '/usr/java/jdk1.8.0_301-amd64/bin/java -jar /opt/xxljob/xxl-job-admin_cc-dev.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('54', '5', '2', '23', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('55', '5', '2', '24', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('56', '5', '2', '25', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('57', '5', '2', '26', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('58', '5', '2', '27', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('59', '5', '2', '28', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('60', '5', '2', '29', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('61', '5', '2', '30', '192.168.80.62', '22', 'root', 'Aa123qwe', null, '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('62', '5', '4', '23', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('63', '5', '4', '24', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('64', '5', '4', '25', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('65', '5', '4', '26', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('66', '5', '4', '27', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('67', '5', '4', '28', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('68', '5', '4', '29', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('69', '5', '4', '30', null, null, 'root', null, null, '0 0/30 * * * ?', null, '0', '1', '1', '2023-04-07 10:37:37');
-- b1b数据中台
INSERT INTO `service_active_job` VALUES ('70', '7', '1', '49', '192.168.80.110', '22', 'root', 'Ydec2022', 'java -jar /opt/project/b1b-data-center-dev/web-api/b1b-dc-web.jar --spring.application.name=b1b-dc-web --spring.profiles.active=dev --server.port=64101', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('71', '7', '1', '50', '192.168.80.110', '22', 'root', 'Ydec2022', '/usr/java/jdk1.8.0_301-amd64/bin/java -jar /opt/xxljob/xxl-job-admin_b1b-data-center-dev.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('72', '7', '2', '49', '192.168.80.111', '22', 'root', 'Ydec2022', 'java -jar /data/project/b1b-data-center-test/web-api/b1b-dc-web.jar --spring.application.name=b1b-dc-web --spring.profiles.active=test --server.port=64201', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('73', '7', '2', '50', '192.168.80.111', '22', 'root', 'Ydec2022', '/usr/java/jdk1.8.0_301-amd64/bin/java -jar /opt/xxljob/xxl-job-admin_b1b-data-center-test.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('74', '7', '4', '49', '172.16.95.81', '58700', 'root', 'JdataBase6268!!$)', 'java -jar /data/b1b-data-center/b1b-dc-web.jar --spring.profiles.active=prod --server.port=64301', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');
INSERT INTO `service_active_job` VALUES ('75', '7', '4', '50', '172.16.95.81', '58700', 'root', 'JdataBase6268!!$)', 'java -jar /opt/xxljob/xxl-job-admin_b1b-data-center-prod.jar', '0 0/30 * * * ?', null, '1', '1', '1', '2023-04-07 10:37:37');

/*------------------------------------------------------------------------
--                                  server_performance_job表
------------------------------------------------------------------------*/
INSERT INTO `server_performance_job` VALUES ('1', '1', '0 0/10 * * * ?', null, '1', '1', '1', '2023-03-29 13:31:23', null);
INSERT INTO `server_performance_job` VALUES ('2', '2', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-29 13:51:15', '2023-03-29 13:52:14');
INSERT INTO `server_performance_job` VALUES ('3', '3', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:41:48', null);
INSERT INTO `server_performance_job` VALUES ('4', '4', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:42:13', null);
INSERT INTO `server_performance_job` VALUES ('5', '5', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:42:24', null);
INSERT INTO `server_performance_job` VALUES ('6', '6', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:42:35', null);
INSERT INTO `server_performance_job` VALUES ('7', '7', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:42:45', null);
INSERT INTO `server_performance_job` VALUES ('8', '8', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:42:56', null);
INSERT INTO `server_performance_job` VALUES ('9', '9', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:43:06', null);
INSERT INTO `server_performance_job` VALUES ('10', '10', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:43:16', '2023-03-30 18:06:32');
INSERT INTO `server_performance_job` VALUES ('11', '11', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-30 15:43:29', null);

/*------------------------------------------------------------------------
--                                  sql_server_available_job表
------------------------------------------------------------------------*/
INSERT INTO `sql_server_available_job` VALUES ('1', '1', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:45:51', null);
INSERT INTO `sql_server_available_job` VALUES ('2', '2', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:46:01', null);
INSERT INTO `sql_server_available_job` VALUES ('3', '3', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:46:11', null);
INSERT INTO `sql_server_available_job` VALUES ('4', '4', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:46:26', null);
INSERT INTO `sql_server_available_job` VALUES ('5', '5', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:46:35', null);
INSERT INTO `sql_server_available_job` VALUES ('6', '6', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:46:45', null);

/*------------------------------------------------------------------------
--                                  mysql_available_job表
------------------------------------------------------------------------*/
INSERT INTO `mysql_available_job` VALUES ('1', '1', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:48:23', null);
INSERT INTO `mysql_available_job` VALUES ('2', '2', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:48:30', null);
INSERT INTO `mysql_available_job` VALUES ('3', '3', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:48:39', null);
INSERT INTO `mysql_available_job` VALUES ('4', '4', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:48:48', null);
INSERT INTO `mysql_available_job` VALUES ('5', '5', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:49:01', null);
INSERT INTO `mysql_available_job` VALUES ('6', '6', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:49:12', null);
INSERT INTO `mysql_available_job` VALUES ('7', '7', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:49:21', null);
INSERT INTO `mysql_available_job` VALUES ('8', '8', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:49:34', null);
INSERT INTO `mysql_available_job` VALUES ('9', '9', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:49:43', null);
INSERT INTO `mysql_available_job` VALUES ('10', '10', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:49:56', null);
INSERT INTO `mysql_available_job` VALUES ('11', '11', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:50:06', null);
INSERT INTO `mysql_available_job` VALUES ('12', '12', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:50:16', null);
INSERT INTO `mysql_available_job` VALUES ('13', '13', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:50:31', null);
INSERT INTO `mysql_available_job` VALUES ('14', '14', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:50:42', null);
INSERT INTO `mysql_available_job` VALUES ('15', '15', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:50:56', null);
INSERT INTO `mysql_available_job` VALUES ('16', '16', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:51:13', null);
INSERT INTO `mysql_available_job` VALUES ('17', '17', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:51:23', null);
INSERT INTO `mysql_available_job` VALUES ('18', '18', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:51:32', null);
INSERT INTO `mysql_available_job` VALUES ('19', '19', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:53:19', null);
INSERT INTO `mysql_available_job` VALUES ('20', '20', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:53:28', null);
INSERT INTO `mysql_available_job` VALUES ('21', '21', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:53:36', null);
INSERT INTO `mysql_available_job` VALUES ('22', '22', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:53:47', null);
INSERT INTO `mysql_available_job` VALUES ('23', '23', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:53:57', null);
INSERT INTO `mysql_available_job` VALUES ('24', '24', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:54:10', null);
INSERT INTO `mysql_available_job` VALUES ('25', '25', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:54:26', null);
INSERT INTO `mysql_available_job` VALUES ('26', '26', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:54:37', null);
INSERT INTO `mysql_available_job` VALUES ('27', '27', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:54:48', null);
INSERT INTO `mysql_available_job` VALUES ('28', '28', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:54:57', null);
INSERT INTO `mysql_available_job` VALUES ('29', '29', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:55:07', null);
INSERT INTO `mysql_available_job` VALUES ('30', '30', '0 0/30 * * * ?', null, '1', '1', '1', '2023-03-31 13:55:17', null);

/*------------------------------------------------------------------------
--                                  elasticsearch_available_job表
------------------------------------------------------------------------*/
INSERT INTO `elasticsearch_available_job` VALUES ('1', '1', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 13:58:39', null);
INSERT INTO `elasticsearch_available_job` VALUES ('2', '2', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 13:58:47', null);

/*------------------------------------------------------------------------
--                                  mongodb_available_job表
------------------------------------------------------------------------*/
INSERT INTO `mongodb_available_job` VALUES ('1', '1', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 13:59:59', null);
INSERT INTO `mongodb_available_job` VALUES ('2', '2', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:00:06', null);
INSERT INTO `mongodb_available_job` VALUES ('3', '3', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:00:12', null);
INSERT INTO `mongodb_available_job` VALUES ('4', '4', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:00:20', null);
INSERT INTO `mongodb_available_job` VALUES ('5', '5', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:00:27', null);
INSERT INTO `mongodb_available_job` VALUES ('6', '6', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:00:33', null);

/*------------------------------------------------------------------------
--                                  rabbitmq_available_job表
------------------------------------------------------------------------*/
INSERT INTO `rabbitmq_available_job` VALUES ('1', '1', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:51:22', null);
INSERT INTO `rabbitmq_available_job` VALUES ('2', '2', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:51:29', null);
INSERT INTO `rabbitmq_available_job` VALUES ('3', '3', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:51:35', null);
INSERT INTO `rabbitmq_available_job` VALUES ('4', '4', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:51:44', null);
INSERT INTO `rabbitmq_available_job` VALUES ('5', '5', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:51:54', null);
INSERT INTO `rabbitmq_available_job` VALUES ('6', '6', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:03', null);
INSERT INTO `rabbitmq_available_job` VALUES ('7', '7', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:13', null);
INSERT INTO `rabbitmq_available_job` VALUES ('8', '8', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:20', null);
INSERT INTO `rabbitmq_available_job` VALUES ('9', '9', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:29', null);
INSERT INTO `rabbitmq_available_job` VALUES ('10', '10', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:36', null);
INSERT INTO `rabbitmq_available_job` VALUES ('11', '11', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:45', null);
INSERT INTO `rabbitmq_available_job` VALUES ('12', '12', '0 0/30 * * * ?', '1', '1', '1', '2023-03-31 14:52:53', null);

/*------------------------------------------------------------------------
--                                  redis_available_job表
------------------------------------------------------------------------*/
INSERT INTO `redis_available_job` VALUES ('1', '1', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 11:02:34', null);
INSERT INTO `redis_available_job` VALUES ('2', '2', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:07:56', null);
INSERT INTO `redis_available_job` VALUES ('3', '3', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:08:06', null);
INSERT INTO `redis_available_job` VALUES ('4', '4', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:08:16', null);
INSERT INTO `redis_available_job` VALUES ('5', '5', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:08:48', null);
INSERT INTO `redis_available_job` VALUES ('6', '6', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:09:12', null);
INSERT INTO `redis_available_job` VALUES ('7', '7', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:09:21', null);
INSERT INTO `redis_available_job` VALUES ('8', '8', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:09:31', null);
INSERT INTO `redis_available_job` VALUES ('9', '9', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:09:40', null);
INSERT INTO `redis_available_job` VALUES ('10', '10', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:09:51', null);
INSERT INTO `redis_available_job` VALUES ('11', '11', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:10:02', null);
INSERT INTO `redis_available_job` VALUES ('12', '12', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:10:11', null);
INSERT INTO `redis_available_job` VALUES ('13', '13', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:10:24', null);
INSERT INTO `redis_available_job` VALUES ('14', '14', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:10:35', null);
INSERT INTO `redis_available_job` VALUES ('15', '15', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:10:44', null);
INSERT INTO `redis_available_job` VALUES ('16', '16', '0 0/30 * * * ?', '1', '1', '1', '2023-04-12 14:10:44', null);

/*------------------------------------------------------------------------
--                                  system表
------------------------------------------------------------------------*/
-- 即时通信
INSERT INTO `system` VALUES ('1', '即时通信');
-- 远大支持论坛
INSERT INTO `system` VALUES ('2', '远大支持论坛');
-- 供应商协同
INSERT INTO `system` VALUES ('3', '供应商协同');
-- b1b产品数据导入
INSERT INTO `system` VALUES ('4', 'b1b产品数据导入');
-- 客户协同
INSERT INTO `system` VALUES ('5', '客户协同');
-- erp
INSERT INTO `system` VALUES ('6', 'erp');
-- b1b数据中台
INSERT INTO `system` VALUES ('7', 'b1b数据中台');

/*---------------------------------------------------------------------------
--                                environment表
---------------------------------------------------------------------------*/
INSERT INTO `environment` VALUES ('1', '开发环境');
INSERT INTO `environment` VALUES ('2', '测试环境');
INSERT INTO `environment` VALUES ('3', '验证环境');
INSERT INTO `environment` VALUES ('4', '生产环境');

/*-----------------------------------------------------------------------------
--                                 service表
-----------------------------------------------------------------------------*/
-- 即时通信
INSERT INTO `service` VALUES ('1', '即时通信前端');
INSERT INTO `service` VALUES ('2', 'im-end-eureka');
INSERT INTO `service` VALUES ('3', 'im-end-gateway');
INSERT INTO `service` VALUES ('4', 'im-end-data');
INSERT INTO `service` VALUES ('5', 'im-end-web');
-- 远大支持论坛
INSERT INTO `service` VALUES ('6', '远大支持论坛前端（创易站）');
INSERT INTO `service` VALUES ('7', '远大支持论坛前端（后台管理系统）');
INSERT INTO `service` VALUES ('8', 'cyz-eureka');
INSERT INTO `service` VALUES ('9', 'cyz-gateway');
INSERT INTO `service` VALUES ('10', 'cyz-web');
INSERT INTO `service` VALUES ('11', 'cyz-admin');
INSERT INTO `service` VALUES ('12', 'xxl-job（远大支持论坛）');
-- 供应商协同
INSERT INTO `service` VALUES ('13', '供应商协同前端');
INSERT INTO `service` VALUES ('14', 'sc-gateway');
INSERT INTO `service` VALUES ('15', 'sc-web');
INSERT INTO `service` VALUES ('16', 'sc-nacos-config');
INSERT INTO `service` VALUES ('17', 'nacos1（供应商协同）');
INSERT INTO `service` VALUES ('18', 'nacos2（供应商协同）');
INSERT INTO `service` VALUES ('19', 'nacos3（供应商协同）');
INSERT INTO `service` VALUES ('20', 'xxl-job（供应商协同）');
-- b1b产品数据导入
INSERT INTO `service` VALUES ('21', 'erp9-syn-data');
INSERT INTO `service` VALUES ('22', 'xxl-job（b1b产品数据导入）');
-- 客户协同
INSERT INTO `service` VALUES ('23', '客户协同前端');
INSERT INTO `service` VALUES ('24', 'cc-gateway');
INSERT INTO `service` VALUES ('25', 'cc-web');
INSERT INTO `service` VALUES ('26', 'cc-nacos-config');
INSERT INTO `service` VALUES ('27', 'nacos1（客户协同）');
INSERT INTO `service` VALUES ('28', 'nacos2（客户协同）');
INSERT INTO `service` VALUES ('29', 'nacos3（客户协同）');
INSERT INTO `service` VALUES ('30', 'xxl-job（客户协同）');
-- erp
INSERT INTO `service` VALUES ('31', 'Erm前端');
INSERT INTO `service` VALUES ('32', 'Erm');
INSERT INTO `service` VALUES ('33', 'CRM/SRM前端');
INSERT INTO `service` VALUES ('34', 'CRM/SRM');
INSERT INTO `service` VALUES ('35', '询报价前端');
INSERT INTO `service` VALUES ('36', '询报价');
INSERT INTO `service` VALUES ('37', '销售订单前端');
INSERT INTO `service` VALUES ('38', '销售订单');
INSERT INTO `service` VALUES ('39', '日常管理前端');
INSERT INTO `service` VALUES ('40', '日常管理');
INSERT INTO `service` VALUES ('41', '智能配置前端');
INSERT INTO `service` VALUES ('42', '智能配置');
INSERT INTO `service` VALUES ('43', '标准库前端');
INSERT INTO `service` VALUES ('44', '标准库');
INSERT INTO `service` VALUES ('45', 'Erp登录相关/基座前端');
INSERT INTO `service` VALUES ('46', 'Erp登录相关/基座');
INSERT INTO `service` VALUES ('47', '供应商协同登录相关');
INSERT INTO `service` VALUES ('48', '库存前置审批项目');
-- b1b数据中台
INSERT INTO `service` VALUES ('49', 'b1b-data-center');
INSERT INTO `service` VALUES ('50', 'xxl-job（b1b数据中台）');

/*-----------------------------------------------------------------------------
--                                 model表
-----------------------------------------------------------------------------*/
INSERT INTO `model` VALUES ('1', 'Erm--系统管理');
INSERT INTO `model` VALUES ('2', 'Erm--员工管理');
INSERT INTO `model` VALUES ('3', 'CRM/SRM--首页');
INSERT INTO `model` VALUES ('4', 'CRM/SRM--基本信息管理');
INSERT INTO `model` VALUES ('5', 'CRM/SRM--客户管理');
INSERT INTO `model` VALUES ('6', 'CRM/SRM--供应商管理');
INSERT INTO `model` VALUES ('7', 'CRM/SRM--承运商管理');
INSERT INTO `model` VALUES ('8', 'CRM/SRM--库房管理');
INSERT INTO `model` VALUES ('9', '询报价--首页');
INSERT INTO `model` VALUES ('10', '询报价--询价管理');
INSERT INTO `model` VALUES ('11', '询报价--报价管理');
INSERT INTO `model` VALUES ('12', '询报价--供应商协同管理');
INSERT INTO `model` VALUES ('13', '询报价--数据查询');

/*-----------------------------------------------------------------------------
--                                 rel_system_environment表
-----------------------------------------------------------------------------*/
-- 即时通信
INSERT INTO `rel_system_environment` VALUES ('1', '1', '1');
INSERT INTO `rel_system_environment` VALUES ('2', '1', '2');
INSERT INTO `rel_system_environment` VALUES ('3', '1', '4');
-- 远大支持论坛
INSERT INTO `rel_system_environment` VALUES ('4', '2', '1');
INSERT INTO `rel_system_environment` VALUES ('5', '2', '2');
INSERT INTO `rel_system_environment` VALUES ('6', '2', '4');
-- 供应商协同
INSERT INTO `rel_system_environment` VALUES ('7', '3', '1');
INSERT INTO `rel_system_environment` VALUES ('8', '3', '2');
INSERT INTO `rel_system_environment` VALUES ('9', '3', '4');
-- b1b产品数据导入
INSERT INTO `rel_system_environment` VALUES ('10', '4', '1');
INSERT INTO `rel_system_environment` VALUES ('11', '4', '2');
INSERT INTO `rel_system_environment` VALUES ('12', '4', '4');
-- 客户协同
INSERT INTO `rel_system_environment` VALUES ('13', '5', '1');
INSERT INTO `rel_system_environment` VALUES ('14', '5', '2');
INSERT INTO `rel_system_environment` VALUES ('15', '5', '4');
-- erp
INSERT INTO `rel_system_environment` VALUES ('16', '6', '1');
INSERT INTO `rel_system_environment` VALUES ('17', '6', '2');
INSERT INTO `rel_system_environment` VALUES ('18', '6', '3');
INSERT INTO `rel_system_environment` VALUES ('19', '6', '4');
-- b1b数据中台
INSERT INTO `rel_system_environment` VALUES ('20', '7', '1');
INSERT INTO `rel_system_environment` VALUES ('21', '7', '2');
INSERT INTO `rel_system_environment` VALUES ('22', '7', '4');

/*-----------------------------------------------------------------------------
--                                 rel_system_environment_service表
-----------------------------------------------------------------------------*/
-- 即时通信
INSERT INTO `rel_system_environment_service` VALUES ('1', '1', '1');
INSERT INTO `rel_system_environment_service` VALUES ('2', '1', '2');
INSERT INTO `rel_system_environment_service` VALUES ('3', '1', '3');
INSERT INTO `rel_system_environment_service` VALUES ('4', '1', '4');
INSERT INTO `rel_system_environment_service` VALUES ('5', '1', '5');
INSERT INTO `rel_system_environment_service` VALUES ('6', '2', '1');
INSERT INTO `rel_system_environment_service` VALUES ('7', '2', '2');
INSERT INTO `rel_system_environment_service` VALUES ('8', '2', '3');
INSERT INTO `rel_system_environment_service` VALUES ('9', '2', '4');
INSERT INTO `rel_system_environment_service` VALUES ('10', '2', '5');
INSERT INTO `rel_system_environment_service` VALUES ('11', '3', '1');
INSERT INTO `rel_system_environment_service` VALUES ('12', '3', '2');
INSERT INTO `rel_system_environment_service` VALUES ('13', '3', '3');
INSERT INTO `rel_system_environment_service` VALUES ('14', '3', '4');
INSERT INTO `rel_system_environment_service` VALUES ('15', '3', '5');
-- 远大支持论坛项目
INSERT INTO `rel_system_environment_service` VALUES ('16', '4', '6');
INSERT INTO `rel_system_environment_service` VALUES ('17', '4', '7');
INSERT INTO `rel_system_environment_service` VALUES ('18', '4', '8');
INSERT INTO `rel_system_environment_service` VALUES ('19', '4', '9');
INSERT INTO `rel_system_environment_service` VALUES ('20', '4', '10');
INSERT INTO `rel_system_environment_service` VALUES ('21', '4', '11');
INSERT INTO `rel_system_environment_service` VALUES ('22', '4', '12');
INSERT INTO `rel_system_environment_service` VALUES ('23', '5', '6');
INSERT INTO `rel_system_environment_service` VALUES ('24', '5', '7');
INSERT INTO `rel_system_environment_service` VALUES ('25', '5', '8');
INSERT INTO `rel_system_environment_service` VALUES ('26', '5', '9');
INSERT INTO `rel_system_environment_service` VALUES ('27', '5', '10');
INSERT INTO `rel_system_environment_service` VALUES ('28', '5', '11');
INSERT INTO `rel_system_environment_service` VALUES ('29', '5', '12');
INSERT INTO `rel_system_environment_service` VALUES ('30', '6', '6');
INSERT INTO `rel_system_environment_service` VALUES ('31', '6', '7');
INSERT INTO `rel_system_environment_service` VALUES ('32', '6', '8');
INSERT INTO `rel_system_environment_service` VALUES ('33', '6', '9');
INSERT INTO `rel_system_environment_service` VALUES ('34', '6', '10');
INSERT INTO `rel_system_environment_service` VALUES ('35', '6', '11');
INSERT INTO `rel_system_environment_service` VALUES ('36', '6', '12');
-- 供应商协同
INSERT INTO `rel_system_environment_service` VALUES ('37', '7', '13');
INSERT INTO `rel_system_environment_service` VALUES ('38', '7', '14');
INSERT INTO `rel_system_environment_service` VALUES ('39', '7', '15');
INSERT INTO `rel_system_environment_service` VALUES ('40', '7', '16');
INSERT INTO `rel_system_environment_service` VALUES ('41', '7', '17');
INSERT INTO `rel_system_environment_service` VALUES ('42', '7', '18');
INSERT INTO `rel_system_environment_service` VALUES ('43', '7', '19');
INSERT INTO `rel_system_environment_service` VALUES ('44', '7', '20');
INSERT INTO `rel_system_environment_service` VALUES ('45', '8', '13');
INSERT INTO `rel_system_environment_service` VALUES ('46', '8', '14');
INSERT INTO `rel_system_environment_service` VALUES ('47', '8', '15');
INSERT INTO `rel_system_environment_service` VALUES ('48', '8', '16');
INSERT INTO `rel_system_environment_service` VALUES ('49', '8', '17');
INSERT INTO `rel_system_environment_service` VALUES ('50', '8', '18');
INSERT INTO `rel_system_environment_service` VALUES ('51', '8', '19');
INSERT INTO `rel_system_environment_service` VALUES ('52', '8', '20');
INSERT INTO `rel_system_environment_service` VALUES ('53', '9', '13');
INSERT INTO `rel_system_environment_service` VALUES ('54', '9', '14');
INSERT INTO `rel_system_environment_service` VALUES ('55', '9', '15');
INSERT INTO `rel_system_environment_service` VALUES ('56', '9', '16');
INSERT INTO `rel_system_environment_service` VALUES ('57', '9', '17');
INSERT INTO `rel_system_environment_service` VALUES ('58', '9', '18');
INSERT INTO `rel_system_environment_service` VALUES ('59', '9', '19');
INSERT INTO `rel_system_environment_service` VALUES ('60', '9', '20');
-- b1b产品数据导入
INSERT INTO `rel_system_environment_service` VALUES ('61', '10', '21');
INSERT INTO `rel_system_environment_service` VALUES ('62', '10', '22');
INSERT INTO `rel_system_environment_service` VALUES ('63', '11', '21');
INSERT INTO `rel_system_environment_service` VALUES ('64', '11', '22');
INSERT INTO `rel_system_environment_service` VALUES ('65', '12', '21');
INSERT INTO `rel_system_environment_service` VALUES ('66', '12', '22');
-- 客户协同
INSERT INTO `rel_system_environment_service` VALUES ('67', '13', '23');
INSERT INTO `rel_system_environment_service` VALUES ('68', '13', '24');
INSERT INTO `rel_system_environment_service` VALUES ('69', '13', '25');
INSERT INTO `rel_system_environment_service` VALUES ('70', '13', '26');
INSERT INTO `rel_system_environment_service` VALUES ('71', '13', '27');
INSERT INTO `rel_system_environment_service` VALUES ('72', '13', '28');
INSERT INTO `rel_system_environment_service` VALUES ('73', '13', '29');
INSERT INTO `rel_system_environment_service` VALUES ('74', '13', '30');
INSERT INTO `rel_system_environment_service` VALUES ('75', '14', '23');
INSERT INTO `rel_system_environment_service` VALUES ('76', '14', '24');
INSERT INTO `rel_system_environment_service` VALUES ('77', '14', '25');
INSERT INTO `rel_system_environment_service` VALUES ('78', '14', '26');
INSERT INTO `rel_system_environment_service` VALUES ('79', '14', '27');
INSERT INTO `rel_system_environment_service` VALUES ('80', '14', '28');
INSERT INTO `rel_system_environment_service` VALUES ('81', '14', '29');
INSERT INTO `rel_system_environment_service` VALUES ('82', '14', '30');
INSERT INTO `rel_system_environment_service` VALUES ('83', '15', '23');
INSERT INTO `rel_system_environment_service` VALUES ('84', '15', '24');
INSERT INTO `rel_system_environment_service` VALUES ('85', '15', '25');
INSERT INTO `rel_system_environment_service` VALUES ('86', '15', '26');
INSERT INTO `rel_system_environment_service` VALUES ('87', '15', '27');
INSERT INTO `rel_system_environment_service` VALUES ('88', '15', '28');
INSERT INTO `rel_system_environment_service` VALUES ('89', '15', '29');
INSERT INTO `rel_system_environment_service` VALUES ('90', '15', '30');
-- erp
INSERT INTO `rel_system_environment_service` VALUES ('91', '16', '31');
INSERT INTO `rel_system_environment_service` VALUES ('92', '16', '32');
INSERT INTO `rel_system_environment_service` VALUES ('93', '16', '33');
INSERT INTO `rel_system_environment_service` VALUES ('94', '16', '34');
INSERT INTO `rel_system_environment_service` VALUES ('95', '16', '35');
INSERT INTO `rel_system_environment_service` VALUES ('96', '16', '36');
INSERT INTO `rel_system_environment_service` VALUES ('97', '16', '37');
INSERT INTO `rel_system_environment_service` VALUES ('98', '16', '38');
INSERT INTO `rel_system_environment_service` VALUES ('99', '16', '39');
INSERT INTO `rel_system_environment_service` VALUES ('100', '16', '40');
INSERT INTO `rel_system_environment_service` VALUES ('101', '16', '41');
INSERT INTO `rel_system_environment_service` VALUES ('102', '16', '42');
INSERT INTO `rel_system_environment_service` VALUES ('103', '16', '43');
INSERT INTO `rel_system_environment_service` VALUES ('104', '16', '44');
INSERT INTO `rel_system_environment_service` VALUES ('105', '16', '45');
INSERT INTO `rel_system_environment_service` VALUES ('106', '16', '46');
INSERT INTO `rel_system_environment_service` VALUES ('107', '16', '47');
INSERT INTO `rel_system_environment_service` VALUES ('108', '16', '48');
INSERT INTO `rel_system_environment_service` VALUES ('109', '17', '31');
INSERT INTO `rel_system_environment_service` VALUES ('110', '17', '32');
INSERT INTO `rel_system_environment_service` VALUES ('111', '17', '33');
INSERT INTO `rel_system_environment_service` VALUES ('112', '17', '34');
INSERT INTO `rel_system_environment_service` VALUES ('113', '17', '35');
INSERT INTO `rel_system_environment_service` VALUES ('114', '17', '36');
INSERT INTO `rel_system_environment_service` VALUES ('115', '17', '37');
INSERT INTO `rel_system_environment_service` VALUES ('116', '17', '38');
INSERT INTO `rel_system_environment_service` VALUES ('117', '17', '39');
INSERT INTO `rel_system_environment_service` VALUES ('118', '17', '40');
INSERT INTO `rel_system_environment_service` VALUES ('119', '17', '41');
INSERT INTO `rel_system_environment_service` VALUES ('120', '17', '42');
INSERT INTO `rel_system_environment_service` VALUES ('121', '17', '43');
INSERT INTO `rel_system_environment_service` VALUES ('122', '17', '44');
INSERT INTO `rel_system_environment_service` VALUES ('123', '17', '45');
INSERT INTO `rel_system_environment_service` VALUES ('124', '17', '46');
INSERT INTO `rel_system_environment_service` VALUES ('125', '17', '47');
INSERT INTO `rel_system_environment_service` VALUES ('126', '17', '48');
INSERT INTO `rel_system_environment_service` VALUES ('127', '18', '31');
INSERT INTO `rel_system_environment_service` VALUES ('128', '18', '32');
INSERT INTO `rel_system_environment_service` VALUES ('129', '18', '33');
INSERT INTO `rel_system_environment_service` VALUES ('130', '18', '34');
INSERT INTO `rel_system_environment_service` VALUES ('131', '18', '35');
INSERT INTO `rel_system_environment_service` VALUES ('132', '18', '36');
INSERT INTO `rel_system_environment_service` VALUES ('133', '18', '37');
INSERT INTO `rel_system_environment_service` VALUES ('134', '18', '38');
INSERT INTO `rel_system_environment_service` VALUES ('135', '18', '39');
INSERT INTO `rel_system_environment_service` VALUES ('136', '18', '40');
INSERT INTO `rel_system_environment_service` VALUES ('137', '18', '41');
INSERT INTO `rel_system_environment_service` VALUES ('138', '18', '42');
INSERT INTO `rel_system_environment_service` VALUES ('139', '18', '43');
INSERT INTO `rel_system_environment_service` VALUES ('140', '18', '44');
INSERT INTO `rel_system_environment_service` VALUES ('141', '18', '45');
INSERT INTO `rel_system_environment_service` VALUES ('142', '18', '46');
INSERT INTO `rel_system_environment_service` VALUES ('143', '18', '47');
INSERT INTO `rel_system_environment_service` VALUES ('144', '18', '48');
INSERT INTO `rel_system_environment_service` VALUES ('145', '19', '31');
INSERT INTO `rel_system_environment_service` VALUES ('146', '19', '32');
INSERT INTO `rel_system_environment_service` VALUES ('147', '19', '33');
INSERT INTO `rel_system_environment_service` VALUES ('148', '19', '34');
INSERT INTO `rel_system_environment_service` VALUES ('149', '19', '35');
INSERT INTO `rel_system_environment_service` VALUES ('150', '19', '36');
INSERT INTO `rel_system_environment_service` VALUES ('151', '19', '37');
INSERT INTO `rel_system_environment_service` VALUES ('152', '19', '38');
INSERT INTO `rel_system_environment_service` VALUES ('153', '19', '39');
INSERT INTO `rel_system_environment_service` VALUES ('154', '19', '40');
INSERT INTO `rel_system_environment_service` VALUES ('155', '19', '41');
INSERT INTO `rel_system_environment_service` VALUES ('156', '19', '42');
INSERT INTO `rel_system_environment_service` VALUES ('157', '19', '43');
INSERT INTO `rel_system_environment_service` VALUES ('158', '19', '44');
INSERT INTO `rel_system_environment_service` VALUES ('159', '19', '45');
INSERT INTO `rel_system_environment_service` VALUES ('160', '19', '46');
INSERT INTO `rel_system_environment_service` VALUES ('161', '19', '47');
INSERT INTO `rel_system_environment_service` VALUES ('162', '19', '48');
-- b1b数据中台
INSERT INTO `rel_system_environment_service` VALUES ('163', '20', '49');
INSERT INTO `rel_system_environment_service` VALUES ('164', '20', '50');
INSERT INTO `rel_system_environment_service` VALUES ('165', '21', '49');
INSERT INTO `rel_system_environment_service` VALUES ('166', '21', '50');
INSERT INTO `rel_system_environment_service` VALUES ('167', '22', '49');
INSERT INTO `rel_system_environment_service` VALUES ('168', '22', '50');

/*-----------------------------------------------------------------------------
--                                 server表
-----------------------------------------------------------------------------*/
INSERT INTO `server` VALUES ('1', '192.168.80.109', '1', '192.168.80.109', '22', 'root', 'Ydec2022', '即时通信的开发环境');
INSERT INTO `server` VALUES ('2', '192.168.80.110', '1', '192.168.80.110', '22', 'root', 'Ydec2022', '即时通信的测试环境、远大支持论坛的开发环境');
INSERT INTO `server` VALUES ('3', '192.168.80.111', '1', '192.168.80.111', '22', 'root', 'Ydec2022', '供应商协同开发环境');
INSERT INTO `server` VALUES ('4', '192.168.80.112', '1', '192.168.80.112', '22', 'root', 'gEMm27g1', '客户协同开发环境');
INSERT INTO `server` VALUES ('5', '172.16.95.82', '1', '172.16.95.82', '58700', 'root', 'JdataBase6268!!$)', '供应商协同生产环境');
INSERT INTO `server` VALUES ('6', '172.16.95.201', '1', '172.16.95.201', '58701', 'root', 'Ydec.iM2022062$', '即时通信生产环境');
INSERT INTO `server` VALUES ('7', '192.168.80.60', '1', '192.168.80.60', '22', 'root', 'Aa123qwe', 'b1b产品数据导入开发环境、监控系统开发环境');
INSERT INTO `server` VALUES ('8', '192.168.80.61', '1', '192.168.80.61', '22', 'root', 'Aa123qwe', '供应商协同测试环境');
INSERT INTO `server` VALUES ('9', '192.168.80.62', '1', '192.168.80.62', '22', 'root', 'Aa123qwe', '客户协同测试环境');
INSERT INTO `server` VALUES ('10', '172.16.95.81', '1', '172.16.95.81', '58700', 'root', 'JdataBase6268!!$)', '远大支持论坛生产环境');
INSERT INTO `server` VALUES ('11', '172.16.95.77', '1', '172.16.95.77', '58700', 'root', 'Aa62681155Kk', 'b1b产品数据导入生产环境（erp9-sync-data）');

/*-----------------------------------------------------------------------------
--                                 email表
-----------------------------------------------------------------------------*/
INSERT INTO `email` VALUES ('1', 'lishen@b1b.com', '李珅');
INSERT INTO `email` VALUES ('2', '1307701607@qq.com', '宋文淇');

/*-----------------------------------------------------------------------------
--                                 cell_phone表
-----------------------------------------------------------------------------*/
INSERT INTO `cell_phone` VALUES ('1', '15811111111', '李珅');

/*-----------------------------------------------------------------------------
--                                 sql_server表
-----------------------------------------------------------------------------*/
INSERT INTO `sql_server` VALUES ('1', 'erp开发环境和测试环境sql server数据库', '6', '2', 'jdbc:sqlserver://172.16.98.49:6523;database=PveStandard', 'u_v0erp90', 'kzE50bVYl9P73vJXd36p');
INSERT INTO `sql_server` VALUES ('2', 'erp验证环境sql server数据库', '6', '3', 'jdbc:sqlserver://172.16.95.60:5312;database=PveCrm', 'u60_linuo', 'LN8CWgj58OGB');
INSERT INTO `sql_server` VALUES ('3', 'erp生产环境sql server数据库', '6', '4', 'jdbc:sqlserver://172.16.95.139:5312;database=PveStandard', 'uro_im', 'X8Qt61jmzARB92swMe7q4PDQq1026E');
INSERT INTO `sql_server` VALUES ('4', 'b1b产品数据导入开发环境sql server数据库', '4', '1', 'jdbc:sqlserver://101.200.55.149:6522;Databasename=PvDataB1B', 'uo149_b1bls', 'm40z3HP17an67pg6pTfc07y86sOCk8');
INSERT INTO `sql_server` VALUES ('5', 'b1b产品数据导入测试环境sql server数据库', '4', '2', 'jdbc:sqlserver://101.200.55.149:6522;Databasename=PvDataB1B', 'uo149_b1bls', 'm40z3HP17an67pg6pTfc07y86sOCk8');
INSERT INTO `sql_server` VALUES ('6', 'b1b产品数据导入生产环境sql server数据库', '4', '4', 'jdbc:sqlserver://101.200.55.149:6522;Databasename=PvDataB1B', 'uo149_b1bls', 'm40z3HP17an67pg6pTfc07y86sOCk8');

/*-----------------------------------------------------------------------------
--                                 mysql表
-----------------------------------------------------------------------------*/
-- 即时通信
INSERT INTO `mysql` VALUES ('1', '即时通信-开发环境-mysql数据库(im-erp9)', '1', '1', 'jdbc:mysql://192.168.80.109:3306/im-erp9', 'root', 'PgeWKwHMQ44zQSUi');
INSERT INTO `mysql` VALUES ('2', '即时通信-测试环境-mysql数据库(im-erp9-test)', '1', '2', 'jdbc:mysql://192.168.80.110:3306/im-erp9-test', 'root', 'K4j7mLbKNGL83LLt');
INSERT INTO `mysql` VALUES ('3', '即时通信-生产环境-mysql数据库(im-erp9-prod)', '1', '4', 'jdbc:mysql://172.16.95.201:3306/im-erp9-prod', 'root', 'PZIH201hZmN9t6oo');
-- 远大支持论坛
INSERT INTO `mysql` VALUES ('4', '远大支持论坛-开发环境-mysql数据库(cyz)', '2', '1', 'jdbc:mysql://192.168.80.109:3306/cyz', 'root', 'PgeWKwHMQ44zQSUi');
INSERT INTO `mysql` VALUES ('5', '远大支持论坛-开发环境-mysql数据库(xxl_job)', '2', '1', 'jdbc:mysql://192.168.80.109:3306/xxl_job_cyz-dev', 'root', 'PgeWKwHMQ44zQSUi');
INSERT INTO `mysql` VALUES ('6', '远大支持论坛-测试环境-mysql数据库(cyz-test)', '2', '2', null, null, null);
INSERT INTO `mysql` VALUES ('7', '远大支持论坛-测试环境-mysql数据库(xxl_job)', '2', '2', null, null, null);
INSERT INTO `mysql` VALUES ('8', '远大支持论坛-生产环境-mysql数据库(cyz-prod)', '2', '4', 'jdbc:mysql://172.16.95.81:3306/cyz-prod', 'root', 'GDipoqWNaY92x4Sn');
INSERT INTO `mysql` VALUES ('9', '远大支持论坛-生产环境-mysql数据库(xxl_job)', '2', '4', 'jdbc:mysql://172.16.95.81:3306/xxl_job_cyz-prod', 'root', 'GDipoqWNaY92x4Sn');
-- 供应商协同
INSERT INTO `mysql` VALUES ('10', '供应商协同-开发环境-mysql数据库(sc)', '3', '1', 'jdbc:mysql://192.168.80.111:3306/sc', 'root', '0HZkkjlpHezPj5b8');
INSERT INTO `mysql` VALUES ('11', '供应商协同-开发环境-mysql数据库(sc-erp)', '3', '1', 'jdbc:mysql://172.16.95.82:3306/sc-erp', 'root', 'aoIBPoqCB2ckBrtM');
INSERT INTO `mysql` VALUES ('12', '供应商协同-开发环境-mysql数据库(xxl_job)', '3', '1', 'jdbc:mysql://192.168.80.111:3306/xxl_job_sc-dev', 'root', '0HZkkjlpHezPj5b8');
INSERT INTO `mysql` VALUES ('13', '供应商协同-测试环境-mysql数据库(sc-test)', '3', '2', 'jdbc:mysql://192.168.80.61:3306/sc-test', 'root', '0dNQrXGe8lz2RaVy');
INSERT INTO `mysql` VALUES ('14', '供应商协同-测试环境-mysql数据库(sc-erp)', '3', '2', 'jdbc:mysql://172.16.95.82:3306/sc-erp', 'root', 'aoIBPoqCB2ckBrtM');
INSERT INTO `mysql` VALUES ('15', '供应商协同-测试环境-mysql数据库(xxl_job)', '3', '2', 'jdbc:mysql://192.168.80.61:3306/xxl_job_sc-test', 'root', '0dNQrXGe8lz2RaVy');
INSERT INTO `mysql` VALUES ('16', '供应商协同-生产环境-mysql数据库(sc-prod)', '3', '4', 'jdbc:mysql://172.16.95.82:3306/sc-prod', 'root', 'aoIBPoqCB2ckBrtM');
INSERT INTO `mysql` VALUES ('17', '供应商协同-生产环境-mysql数据库(sc-erp)', '3', '4', 'jdbc:mysql://172.16.95.82:3306/sc-erp', 'root', 'aoIBPoqCB2ckBrtM');
INSERT INTO `mysql` VALUES ('18', '供应商协同-生产环境-mysql数据库(xxl_job)', '3', '4', 'jdbc:mysql://172.16.95.82:3306/xxl_job_sc-prod', 'root', 'aoIBPoqCB2ckBrtM');
-- b1b产品数据导入
INSERT INTO `mysql` VALUES ('19', 'b1b产品数据导入-开发环境-mysql数据库(b1b_goods)', '4', '1', 'jdbc:mysql://192.168.80.211:3306/b1b_goods', 'b1b_goods', '1qaz2wsx12');
INSERT INTO `mysql` VALUES ('20', 'b1b产品数据导入-开发环境-mysql数据库(xxl_job)', '4', '1', 'jdbc:mysql://192.168.80.112:3306/xxl_job_b1b-data-dev', 'root', 'ZhtlPaFWrbdN7wvH');
INSERT INTO `mysql` VALUES ('21', 'b1b产品数据导入-测试环境-mysql数据库(b1b_goods)', '4', '2', null, null, null);
INSERT INTO `mysql` VALUES ('22', 'b1b产品数据导入-测试环境-mysql数据库(xxl_job)', '4', '2', null, null, null);
INSERT INTO `mysql` VALUES ('23', 'b1b产品数据导入-生产环境-mysql数据库(b1b_goods)', '4', '4', 'jdbc:mysql://192.168.80.211:3306/b1b_goods', 'b1b_goods', '1qaz2wsx12');
INSERT INTO `mysql` VALUES ('24', 'b1b产品数据导入-生产环境-mysql数据库(xxl_job)', '4', '4', 'jdbc:mysql://172.16.95.76:4417/xxl_job_b1b-data-prod', 'root', 'YuandaEC2022');
-- 客户协同
INSERT INTO `mysql` VALUES ('25', '客户协同-开发环境-mysql数据库(cc)', '4', '1', 'jdbc:mysql://192.168.80.112:3306/cc', 'root', 'ZhtlPaFWrbdN7wvH');
INSERT INTO `mysql` VALUES ('26', '客户协同-开发环境-mysql数据库(xxl_job)', '4', '1', 'jdbc:mysql://192.168.80.60:3306/xxl_job_cc-dev', 'root', '1PG4PSxhfjKEriGQ');
INSERT INTO `mysql` VALUES ('27', '客户协同-测试环境-mysql数据库(cc-test)', '4', '2', null, null, null);
INSERT INTO `mysql` VALUES ('28', '客户协同-测试环境-mysql数据库(xxl_job)', '4', '2', null, null, null);
INSERT INTO `mysql` VALUES ('29', '客户协同-生产环境-mysql数据库(cc-prod)', '4', '4', null, null, null);
INSERT INTO `mysql` VALUES ('30', '客户协同-生产环境-mysql数据库(xxl_job)', '4', '4', null, null, null);

/*-----------------------------------------------------------------------------
--                                 elasticsearch表
-----------------------------------------------------------------------------*/
-- b1b产品数据导入
INSERT INTO `elasticsearch` VALUES ('1', 'b1b产品数据导入开发环境elasticsearch', '4', '1', '192.168.80.211', '38199', 'cms_goods');
INSERT INTO `elasticsearch` VALUES ('2', 'b1b产品数据导入生产环境elasticsearch', '4', '4', '172.16.95.75', '19200', 'cms_goods_01');

/*-----------------------------------------------------------------------------
--                                 mongodb表
-----------------------------------------------------------------------------*/
-- 供应商协同
INSERT INTO `mongodb` VALUES ('1', '供应商协同开发环境mongodb', '3', '1', 'mongodb://root:mlAs3p0NKRSrMpXq@192.168.80.111:27017/sc');
INSERT INTO `mongodb` VALUES ('2', '供应商协同测试环境mongodb', '3', '2', 'mongodb://root:1QW9YmLwPUX4IYts@192.168.80.61:27017/sc-test');
INSERT INTO `mongodb` VALUES ('3', '供应商协同生产环境mongodb', '3', '4', 'mongodb://root:12Njffnq6JuDD1AH@172.16.95.82:27017/sc-prod');
-- 客户协同
INSERT INTO `mongodb` VALUES ('4', '客户协同开发环境mongodb', '5', '1', 'mongodb://root:MzMPevsV3z51kJKS@192.168.80.112:27017/cc');
INSERT INTO `mongodb` VALUES ('5', '客户协同测试环境mongodb', '5', '2', 'mongodb://root:1QW9YmLwPUX4IYts@192.168.80.61:27017/cc-test');
INSERT INTO `mongodb` VALUES ('6', '客户协同生产环境mongodb', '5', '4', 'mongodb://root:12Njffnq6JuDD1AH@172.16.95.82:27017/cc-prod');

/*------------------------------------------------------------------------
--                                  rabbitmq表
------------------------------------------------------------------------*/
INSERT INTO `rabbitmq` VALUES ('1', '即时通信开发环境rabbitmq', '1', '1', '192.168.80.109', '5672', 'imend', 'KJ0P2K87aZtiTKzp', '/');
INSERT INTO `rabbitmq` VALUES ('2', '即时通信测试环境rabbitmq', '1', '2', '192.168.80.110', '5672', 'imend', 'ZiyBlQIrQg4vm7Dm', '/');
INSERT INTO `rabbitmq` VALUES ('3', '即时通信生产环境rabbitmq', '1', '4', '172.16.95.201', '5672', 'imend', 'mGhEOMfmxRHCqjIv', '/');
INSERT INTO `rabbitmq` VALUES ('4', '远大支持论坛开发环境rabbitmq', '2', '1', '192.168.80.111', '5672', 'sc', '123456', '/');
INSERT INTO `rabbitmq` VALUES ('5', '远大支持论坛测试环境rabbitmq', '2', '2', null, null, null, null, null);
INSERT INTO `rabbitmq` VALUES ('6', '远大支持论坛生产环境rabbitmq', '2', '4', '172.16.95.81', '5672', 'cyz', 'kS7yd0r3fNnHipzr', '/');
INSERT INTO `rabbitmq` VALUES ('7', '供应商协同开发环境rabbitmq', '3', '1', '172.16.98.49', '5672', 'u_b1b', 't15amsVpZ74H06l8S3jAfRO6QnnVwb', 'vh_b1b');
INSERT INTO `rabbitmq` VALUES ('8', '供应商协同测试环境rabbitmq', '3', '2', '172.16.95.60', '5672', 'u_b1b', 't15amsVpZ74H06l8S3jAfRO6QnnVwb', 'vh_b1b');
INSERT INTO `rabbitmq` VALUES ('9', '供应商协同生产环境rabbitmq', '3', '4', '172.16.95.58', '5672', 'u_erp9', 'uX1M69CmhTJ7BI4Ort79', 'vh_erp9');
INSERT INTO `rabbitmq` VALUES ('10', '客户协同开发环境rabbitmq', '5', '1', '192.168.80.112', '5672', 'cc', 'EBUlOVAKoHmRF1Yq', '/');
INSERT INTO `rabbitmq` VALUES ('11', '客户协同测试环境rabbitmq', '5', '2', '192.168.80.62', '5672', 'cc', '60YPCgzvNW43zPGi', '/');
INSERT INTO `rabbitmq` VALUES ('12', '客户协同生产环境rabbitmq', '5', '4', null, null, null, null, null);

/*------------------------------------------------------------------------
--                                  redis表
------------------------------------------------------------------------*/
INSERT INTO `redis` VALUES ('1', '即时通信-开发环境-redis集群', '1', '1', '0', '192.168.80.109:7000,192.168.80.109:7001,192.168.80.109:7002,192.168.80.109:7003,192.168.80.109:7004,192.168.80.109:7005', 'OBxhpshrNpl27Etv');
INSERT INTO `redis` VALUES ('2', '即时通信-测试环境-redis集群', '1', '2', '0', '192.168.80.110:7010,192.168.80.110:7011,192.168.80.110:7012,192.168.80.110:7013,192.168.80.110:7014,192.168.80.110:7015', '3JoNcYPZkBxcIrNu');
INSERT INTO `redis` VALUES ('3', '即时通信-生产环境-redis集群', '1', '3', '0', '172.16.95.201:7020,172.16.95.201:7021,172.16.95.201:7022,172.16.95.201:7023,172.16.95.201:7024,172.16.95.201:7025', 'oAi8cJmzENBtVcq4');
INSERT INTO `redis` VALUES ('4', '远大支持论坛-开发环境-redis集群', '2', '1', '0', '192.168.80.110:7040,192.168.80.110:7041,192.168.80.110:7042,192.168.80.110:7043,192.168.80.110:7044,192.168.80.110:7045', 'YThypJbHhWd5yN9p');
INSERT INTO `redis` VALUES ('5', '远大支持论坛-测试环境-redis集群', '2', '2', null, null, null);
INSERT INTO `redis` VALUES ('6', '远大支持论坛-生产环境-redis集群', '2', '3', '0', '172.16.95.81:7000,172.16.95.81:7001,172.16.95.81:7002,172.16.95.81:7003,172.16.95.81:7004,172.16.95.81:7005', 'YThypJbHhWd5yN9p');
INSERT INTO `redis` VALUES ('7', '供应商协同-开发环境-redis集群', '3', '1', '0', '192.168.80.111:7030,192.168.80.111:7031,192.168.80.111:7032,192.168.80.111:7033,192.168.80.111:7034,192.168.80.111:7035', '0o6g69oEqCeJLyQB');
INSERT INTO `redis` VALUES ('8', '供应商协同-测试环境-redis集群', '3', '2', '0', '192.168.80.61:7080,192.168.80.61:7081,192.168.80.61:7082,192.168.80.61:7083,192.168.80.61:7084,192.168.80.61:7085', 'wjwiTA2Gfrh1V81p');
INSERT INTO `redis` VALUES ('9', '供应商协同-生产环境-redis集群', '3', '3', '0', '172.16.95.82:7060,172.16.95.82:7061,172.16.95.82:7062,172.16.95.82:7063,172.16.95.82:7064,172.16.95.82:7065', 'tf7ryNDptbYm7qSP');
INSERT INTO `redis` VALUES ('10', '客户协同-开发环境-redis集群', '5', '1', '0', '192.168.80.112:7070,192.168.80.112:7071,192.168.80.112:7072,192.168.80.112:7073,192.168.80.112:7074,192.168.80.112:7075', 'hH2PhH3wMwtWRs5o');
INSERT INTO `redis` VALUES ('11', '客户协同-测试环境-redis集群', '5', '2', null, null, null);
INSERT INTO `redis` VALUES ('12', '客户协同-生产环境-redis集群', '5', '3', null, null, null);
INSERT INTO `redis` VALUES ('13', 'erp-测试环境-redis', '6', '2', '0', '172.30.10.49:7369', null);
INSERT INTO `redis` VALUES ('14', 'erp-验证环境-redis', '6', '3', '0', '172.16.95.60:7369', null);
INSERT INTO `redis` VALUES ('15', 'erp-生产环境-redis', '6', '4', '0', '172.16.95.57:6379', null);
INSERT INTO `redis` VALUES ('16', 'b1b数据中台-开发环境-redis集群', '7', '1', '0', '192.168.80.60:7100,192.168.80.60:7101,192.168.80.60:7102,192.168.80.60:7103,192.168.80.60:7104,192.168.80.60:7105', 'DKV17z6mBqkWiwHh');

/*------------------------------------------------------------------------
--                                  api表
------------------------------------------------------------------------*/
-- 供应商协同（开发环境）
INSERT INTO `api` VALUES ('1', 'https://erp9.b1b.com/scsapi/Inquiry/GetMenuMarkCount?userId=USR06524', '3', '1', '13', 'https://erp9.b1b.com/scsapi/Inquiry/GetMenuMarkCount?userId=USR06524?userId=USR06524', '2', '2', '', '', '1', '.*\"status\":200,\"success\":true,\"data\":(.+).*');
INSERT INTO `api` VALUES ('2', 'https://erp9.b1b.com/scsapi/Report/GetPopularInquiryProducts', '3', '1', '13', 'https://erp9.b1b.com/scsapi/Report/GetPopularInquiryProducts', '1', '2', '', '', '1', '.*\"status\":200,\"success\":true,\"data\":(.+).*');
INSERT INTO `api` VALUES ('3', 'https://erp9.b1b.com/scsapi/Inquiry/GetSuppliers?userid=USR06524', '3', '1', '13', 'https://erp9.b1b.com/scsapi/Inquiry/GetSuppliers?userid=USR06524', '1', '2', '', '', '1', '.*\"status\":200,\"success\":true,\"data\":(.+).*');
INSERT INTO `api` VALUES ('4', 'https://out.bj.51db.com:36102/810_api/login/JSRInfoByIDStype?id=USR06524&stype=WaitInfo', '3', '1', '13', 'https://out.bj.51db.com:36102/810_api/login/JSRInfoByIDStype?id=USR06524&stype=WaitInfo', '1', '2', '', '', '1', '.*\"status\":\"200\",\"message\":\"成功\",\"isSuccess\":true,:(.+).*');
INSERT INTO `api` VALUES ('5', 'https://erp9.b1b.com/scsapi/Report/GetRealInquiryOrders?userId=USR06524', '3', '1', '13', 'https://erp9.b1b.com/scsapi/Report/GetRealInquiryOrders?userId=USR06524', '1', '2', '', '', '1', '.*\"status\":200,\"success\":true,\"data\":(.+).*');
INSERT INTO `api` VALUES ('6', 'https://out.bj.51db.com:36102/810_api/DJProcess/WaitInfo?key=2e4e16f7176a4e75ae683986119a304d&gys=%E6%B5%8B%E8%AF%95%E4%B8%93%E7%94%A8%E4%BE%9B%E5%BA%94%E5%95%86%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%9B%9B%E5%B7%9D%E7%99%BE%E5%88%A9%E5%A4%A9%E6%81%92%E8%8D%AF%E4%B8%9A%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E6%B7%B1%E5%9C%B3%E5%B8%82%E6%A3%AE%E4%BA%BF%E6%AC%A7%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%8C%97%E4%BA%AC%E8%AF%9A%E5%87%8C%E4%BC%9F%E4%B8%9A%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E4%B8%8A%E6%B5%B7%E8%8B%B1%E6%96%B9%E8%BD%AF%E4%BB%B6%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%90%88%E8%82%A5%E5%8D%8E%E7%A7%91%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AE%BE%E5%A4%87%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8', '3', '1', '13', 'https://out.bj.51db.com:36102/810_api/DJProcess/WaitInfo?key=2e4e16f7176a4e75ae683986119a304d&gys=%E6%B5%8B%E8%AF%95%E4%B8%93%E7%94%A8%E4%BE%9B%E5%BA%94%E5%95%86%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%9B%9B%E5%B7%9D%E7%99%BE%E5%88%A9%E5%A4%A9%E6%81%92%E8%8D%AF%E4%B8%9A%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E6%B7%B1%E5%9C%B3%E5%B8%82%E6%A3%AE%E4%BA%BF%E6%AC%A7%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%8C%97%E4%BA%AC%E8%AF%9A%E5%87%8C%E4%BC%9F%E4%B8%9A%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E4%B8%8A%E6%B5%B7%E8%8B%B1%E6%96%B9%E8%BD%AF%E4%BB%B6%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%90%88%E8%82%A5%E5%8D%8E%E7%A7%91%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AE%BE%E5%A4%87%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8', '1', '2', '', '', '1', '.*\"status\":\"200\",\"message\":\"成功\",\"isSuccess\":true,.*');
INSERT INTO `api` VALUES ('7', 'https://out.bj.51db.com:36102/810_api/DJProcess/WaitInfoPay?key=2e4e16f7176a4e75ae683986119a304d&gys=%E6%B5%8B%E8%AF%95%E4%B8%93%E7%94%A8%E4%BE%9B%E5%BA%94%E5%95%86%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%9B%9B%E5%B7%9D%E7%99%BE%E5%88%A9%E5%A4%A9%E6%81%92%E8%8D%AF%E4%B8%9A%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E6%B7%B1%E5%9C%B3%E5%B8%82%E6%A3%AE%E4%BA%BF%E6%AC%A7%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%8C%97%E4%BA%AC%E8%AF%9A%E5%87%8C%E4%BC%9F%E4%B8%9A%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E4%B8%8A%E6%B5%B7%E8%8B%B1%E6%96%B9%E8%BD%AF%E4%BB%B6%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%90%88%E8%82%A5%E5%8D%8E%E7%A7%91%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AE%BE%E5%A4%87%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8', '3', '1', '13', 'https://out.bj.51db.com:36102/810_api/DJProcess/WaitInfoPay?key=2e4e16f7176a4e75ae683986119a304d&gys=%E6%B5%8B%E8%AF%95%E4%B8%93%E7%94%A8%E4%BE%9B%E5%BA%94%E5%95%86%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%9B%9B%E5%B7%9D%E7%99%BE%E5%88%A9%E5%A4%A9%E6%81%92%E8%8D%AF%E4%B8%9A%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E6%B7%B1%E5%9C%B3%E5%B8%82%E6%A3%AE%E4%BA%BF%E6%AC%A7%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%8C%97%E4%BA%AC%E8%AF%9A%E5%87%8C%E4%BC%9F%E4%B8%9A%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E4%B8%8A%E6%B5%B7%E8%8B%B1%E6%96%B9%E8%BD%AF%E4%BB%B6%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%7C%E5%90%88%E8%82%A5%E5%8D%8E%E7%A7%91%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AE%BE%E5%A4%87%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8', '1', '2', '', '', '1', '.*\"status\":\"200\",\"message\":\"成功\",\"isSuccess\":true,.*');
INSERT INTO `api` VALUES ('8', 'https://out.bj.51db.com:36102/services/core.ashx', '3', '1', '13', 'https://out.bj.51db.com:36102/services/core.ashx', '2', '3', '{data: {ID: 80890},\nrequest_item: \"更新已开发票B1B状态\",\nrequest_service: \"B1B供应商协同开票处理\",\ntoken: \"731f9608-1653-4f31-9de7-002be83683aa\"}', '', '1', '.*\"success\":true,\"status_code\":200,\"msg\":.*');
INSERT INTO `api` VALUES ('9', 'https://erp9.b1b.com/scsapi/Report/GetHomePageData?userid=USR06524', '3', '1', '13', 'https://erp9.b1b.com/scsapi/Report/GetHomePageData?userid=USR06524', '1', '2', '', '', '1', '.*\"status\":200,\"success\":true,\"data\".*');

/*------------------------------------------------------------------------
--                                  api_available_job表
------------------------------------------------------------------------*/
-- 供应商协同（开发环境）
INSERT INTO `api_available_job` VALUES ('1', '1', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 14:06:19');
INSERT INTO `api_available_job` VALUES ('2', '2', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 14:40:34');
INSERT INTO `api_available_job` VALUES ('3', '3', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 15:23:09');
INSERT INTO `api_available_job` VALUES ('4', '4', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 15:24:57');
INSERT INTO `api_available_job` VALUES ('5', '5', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 15:43:14');
INSERT INTO `api_available_job` VALUES ('6', '6', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 16:48:27');
INSERT INTO `api_available_job` VALUES ('7', '7', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 16:51:44');
INSERT INTO `api_available_job` VALUES ('8', '8', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 16:54:43');
INSERT INTO `api_available_job` VALUES ('9', '9', '0 0/30 * * * ?', '1', '1', '1', '2023-05-19 16:57:36');
