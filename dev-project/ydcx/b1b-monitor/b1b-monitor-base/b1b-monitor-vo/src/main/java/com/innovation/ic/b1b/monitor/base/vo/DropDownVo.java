package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 下拉框Vo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DropDownVo", description = "下拉框Vo")
public class DropDownVo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "类型： 1 系统  2 环境  3 服务 ", dataType = "Integer")
    private Integer type;

}
