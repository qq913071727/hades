package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
    import lombok.Getter;
import lombok.Setter;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@ApiModel(value = "LogMonitorJobLogVo", description = " 请求类")
@Setter
@Getter
public class LogMonitorJobLogVo {

                                                        

    
                                                            @ApiModelProperty(value = "主键")
            private Integer id;
                    
                                                            @ApiModelProperty(value = "日志监控任务的id")
            private Integer logMonitorJobId;
                    
                                                            @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活")
            private Integer active;
                    
                                                            @ApiModelProperty(value = "启动时间")
            private String startTime;
                    
                                                            @ApiModelProperty(value = "问题描述")
            private String description;
                    }