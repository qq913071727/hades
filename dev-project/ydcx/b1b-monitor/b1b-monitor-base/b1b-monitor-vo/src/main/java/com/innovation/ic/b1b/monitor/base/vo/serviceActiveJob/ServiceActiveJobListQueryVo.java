package com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   服务存活任务表查询列表的Vo类
 * @author swq
 * @time   2023年3月29日10:16:29
 */
@Data
@ApiModel(value = "ServiceActiveJobListQueryVo", description = "服务存活任务表查询列表的Vo类")
public class ServiceActiveJobListQueryVo {

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "创建时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "是否启用。1表示启用，0表示未启用", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "是否是主数据库", dataType = "Boolean")
    private Boolean isMain;
}
