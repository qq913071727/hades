package com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   服务存活任务表添加(修改)数据的Vo类
 * @author swq
 * @time   2023年3月29日10:16:29
 */
@Data
@ApiModel(value = "ServiceActiveJobAddVo", description = "服务存活任务表添加(修改)数据的Vo类")
public class ServiceActiveJobAddVo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "是否启用", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    private String jobHandler;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "IP", dataType = "String")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "String")
    private String port;

    @ApiModelProperty(value = "用户", dataType = "String")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "正则表达式", dataType = "String")
    private String processRegularExpression;

    @ApiModelProperty(value = "报警邮箱", dataType = "Integer")
    private Integer alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "Integer")
    private Integer alarmCellPhone;
}
