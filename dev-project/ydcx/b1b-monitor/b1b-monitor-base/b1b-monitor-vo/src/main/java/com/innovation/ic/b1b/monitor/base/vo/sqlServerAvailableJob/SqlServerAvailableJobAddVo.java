package com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   sql server可用任务添加的Vo类
 * @author linuo
 * @time   2023年3月14日15:41:10
 */
@Data
@ApiModel(value = "SqlServerAvailableJobAddVo", description = "sql server可用任务添加的Vo类")
public class SqlServerAvailableJobAddVo {
    @ApiModelProperty(value = "sqlServerId", dataType = "Integer")
    private Integer sqlServerId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    private String jobHandler;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;
}