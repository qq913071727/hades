package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SqlServerVo", description = "SqlServerVo")
public class SqlServerVo  extends PageVo{

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "系统名字", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名字", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "环境ids,前端不用理会", dataType = "List")
    private List<Integer> environmentIds;

}
