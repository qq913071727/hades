package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "EmailVo", description = "邮件")
public class EmailVo extends PageVo{

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;


    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

}
