package com.innovation.ic.b1b.monitor.base.vo.apiAvailableJobLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zengqinglong
 * @desc 接口可用任务添加的Vo类
 * @Date 2023/3/30 10:39
 **/
@Data
@ApiModel(value = "ApiAvailableJobLogListVo", description = "接口可用任务列表的Vo类")
public class ApiAvailableJobLogListVo {
    @ApiModelProperty(value = "系统 id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境 id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务 id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "接口id", dataType = "Integer")
    private Integer apiId;

    @ApiModelProperty(value = "表示接口是否可以。1表示可用，0表示不可用", dataType = "Integer")
    private Integer available;

    @ApiModelProperty(value = "启动时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "启动时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}
