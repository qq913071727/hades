package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ModelVo", description = "ModelVo")
public class ModelVo  extends  PageVo{


    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "删除或者更新传", dataType = "Integer")
    private Integer relId;


    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "环境ids,前端不用理会", dataType = "List")
    private List<Integer> environmentIds;

    @ApiModelProperty(value = "serviceId", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "modelId", dataType = "Integer")
    private Integer modelId;

    @ApiModelProperty(value = "模块名字", dataType = "Integer")
    private String name;

}
