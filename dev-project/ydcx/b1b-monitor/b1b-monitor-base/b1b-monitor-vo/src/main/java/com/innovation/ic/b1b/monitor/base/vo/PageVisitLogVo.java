package com.innovation.ic.b1b.monitor.base.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PageVisitLogVo", description = "页面访问日志Vo")
public class PageVisitLogVo {

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "所属模块。1表示基座、2表示erm、3表示crm/srm、4表示询报价、5表示销售订单、6表示采购管理、7表示日常管理、8表示审批管理、9表示智能配置、10表示标准库、11表示远大论坛、12表示统计报表", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "页面url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "开始时间", dataType = "Date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date beginDate;

    @ApiModelProperty(value = "结束时间", dataType = "Date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date endDate;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页几行", dataType = "Integer")
    private Integer pageSize;
}
