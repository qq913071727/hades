package com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   更新rabbitmq可用任务的Vo类
 * @author linuo
 * @time   2023年3月27日14:47:07
 */
@Data
@ApiModel(value = "RabbitmqAvailableJobUpdateVo", description = "更新rabbitmq可用任务的Vo类")
public class RabbitmqAvailableJobUpdateVo {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "rabbitmq id", dataType = "Integer")
    private Integer rabbitmqId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;
}