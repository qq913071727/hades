package com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   添加Elasticsearch可用任务的Vo类
 * @author linuo
 * @time   2023年3月24日09:59:15
 */
@Data
@ApiModel(value = "ElasticsearchAvailableJobAddVo", description = "添加Elasticsearch可用任务的Vo类")
public class ElasticsearchAvailableJobAddVo {
    @ApiModelProperty(value = "elasticsearch id", dataType = "Integer")
    private Integer elasticsearchId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;
}