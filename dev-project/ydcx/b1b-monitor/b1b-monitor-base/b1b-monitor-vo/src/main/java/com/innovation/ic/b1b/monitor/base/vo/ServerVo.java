package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServerVo", description = "服务器Vo")
public class ServerVo extends PageVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "ip", dataType = "String")
    private String ip;

    @ApiModelProperty(value = "操作系统:1表示linux，2表示windows", dataType = "String")
    private Integer systemOperation;

    @ApiModelProperty(value = "ssh协议的端口", dataType = "String")
    private Integer sshPort;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;
}