package com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   添加rabbitmq监控任务的Vo类
 * @author linuo
 * @time   2023年3月27日11:20:04
 */
@Data
@ApiModel(value = "RabbitmqAvailableJobAddVo", description = "添加rabbitmq监控任务的Vo类")
public class RabbitmqAvailableJobAddVo {
    @ApiModelProperty(value = "rabbitmq id", dataType = "Integer")
    private Integer rabbitmqId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;
}