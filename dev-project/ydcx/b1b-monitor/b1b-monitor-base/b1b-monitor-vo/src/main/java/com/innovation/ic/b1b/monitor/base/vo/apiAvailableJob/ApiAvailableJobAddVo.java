package com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zengqinglong
 * @desc 接口可用任务添加的Vo类
 * @Date 2023/3/30 10:39
 **/
@Data
@ApiModel(value = "ApiAvailableJobAddVo", description = "接口可用任务添加的Vo类")
public class ApiAvailableJobAddVo {
    @ApiModelProperty(value = "接口id", dataType = "Integer")
    private Integer apiId;

    @ApiModelProperty(value = "调度表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用 1:启用 0:未启用", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "Integer")
    private Integer alarmEmailId;

    @ApiModelProperty(value = "报警手机号", dataType = "Integer")
    private Integer alarmCellPhoneId;
}
