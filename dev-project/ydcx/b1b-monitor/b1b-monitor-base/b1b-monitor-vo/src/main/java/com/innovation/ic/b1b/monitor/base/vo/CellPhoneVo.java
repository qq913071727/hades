package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CellPhoneVo", description = "手机号")
public class CellPhoneVo extends PageVo{

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String cellPhone;


    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

}
