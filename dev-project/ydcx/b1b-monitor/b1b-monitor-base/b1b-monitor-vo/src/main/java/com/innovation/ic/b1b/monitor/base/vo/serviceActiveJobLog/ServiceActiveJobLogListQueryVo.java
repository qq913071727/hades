package com.innovation.ic.b1b.monitor.base.vo.serviceActiveJobLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc 服务存活日志表查询列表的Vo类
 * @author swq
 * @time 2023年3月30日16:16:29
 */
@Data
@ApiModel(value = "ServiceActiveJobLogListQueryVo", description = "服务存活日志表查询列表的Vo类")
public class ServiceActiveJobLogListQueryVo {

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活", dataType = "Integer")
    private Integer active;

    @ApiModelProperty(value = "创建时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "创建时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}
