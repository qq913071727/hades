package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Rabbitmq", description = "Rabbitmq")
public class RabbitmqVo extends PageVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "环境ids,前端不用理会", dataType = "List")
    private List<Integer> environmentIds;


    @ApiModelProperty(value = "ip", dataType = "String")
    private String ip;


    @ApiModelProperty(value = "port", dataType = "String")
    private String port;


    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;


    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;


    @ApiModelProperty(value = "虚拟主机", dataType = "String")
    private String virtualHost;


}