package com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zengqinglong
 * @desc 接口可用任务添加的Vo类
 * @Date 2023/3/30 10:39
 **/
@Data
@ApiModel(value = "ApiAvailableJobListQueryVo", description = "接口可用任务列表的Vo类")
public class ApiAvailableJobListQueryVo {
    @ApiModelProperty(value = "系统 id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "服务 id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "创建时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "创建时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "是否是主数据库", dataType = "Boolean")
    private Boolean isMain;
}
