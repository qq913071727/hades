package com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJobLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   查询服务器性能任务日志列表接口的Vo类
 * @author linuo
 * @time   2023年3月14日11:36:01
 */
@Data
@ApiModel(value = "ServerPerformanceJobLogListQueryVo", description = "查询服务器性能任务日志列表接口的Vo类")
public class ServerPerformanceJobLogListQueryVo {
    @ApiModelProperty(value = "服务器名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "服务器id", dataType = "Integer")
    private Integer serverId;

    @ApiModelProperty(value = "启动时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "启动时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}