package com.innovation.ic.b1b.monitor.base.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RedisVo", description = "RedisVo")
public class RedisVo extends PageVo{
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "环境ids,前端不用理会", dataType = "List")
    private List<Integer> environmentIds;

    @ApiModelProperty(value = "数据库", dataType = "Integer")
    private Integer redisDatabase;


    @ApiModelProperty(value = "节点（ip和端口）。多个节点有逗号分割", dataType = "String")
    private String nodes;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;
}