package com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   添加mysql可用任务的Vo类
 * @author linuo
 * @time   2023年3月16日11:24:55
 */
@Data
@ApiModel(value = "MysqlAvailableJobAddVo", description = "添加mysql可用任务的Vo类")
public class MysqlAvailableJobAddVo {
    @ApiModelProperty(value = "mysql id", dataType = "Integer")
    private Integer mysqlId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    private String jobHandler;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;
}