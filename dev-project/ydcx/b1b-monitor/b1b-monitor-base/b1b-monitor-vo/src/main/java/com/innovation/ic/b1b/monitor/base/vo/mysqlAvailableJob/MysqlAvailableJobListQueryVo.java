package com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   查询mysql监控任务列表的Vo类
 * @author linuo
 * @time   2023年3月15日15:09:20
 */
@Data
@ApiModel(value = "MysqlAvailableJobListQueryVo", description = "查询mysql监控任务列表的Vo类")
public class MysqlAvailableJobListQueryVo {
    @ApiModelProperty(value = "mysql id", dataType = "Integer")
    private Integer mysqlId;

    @ApiModelProperty(value = "mysql名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "创建时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "是否查询主库(true是、false否)", dataType = "Boolean")
    private Boolean isMain;
}