package com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   更新redis可用任务的Vo类
 * @author linuo
 * @time   2023年3月28日15:46:01
 */
@Data
@ApiModel(value = "RedisAvailableJobUpdateVo", description = "更新redis可用任务的Vo类")
public class RedisAvailableJobUpdateVo {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "redis id", dataType = "Integer")
    private Integer redisId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;
}