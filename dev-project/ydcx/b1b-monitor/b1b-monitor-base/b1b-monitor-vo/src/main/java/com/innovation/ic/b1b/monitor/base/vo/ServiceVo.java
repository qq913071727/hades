package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServiceVo", description = "服务Vo")
public class ServiceVo extends PageVo {


    @ApiModelProperty(value = "主键id", dataType = "String")
    private Integer id;


    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;


    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;


    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "环境ids,前端不用理会", dataType = "List")
    private List<Integer> environmentIds;

    @ApiModelProperty(value = "reId", dataType = "Integer")
    private Integer reId;


}
