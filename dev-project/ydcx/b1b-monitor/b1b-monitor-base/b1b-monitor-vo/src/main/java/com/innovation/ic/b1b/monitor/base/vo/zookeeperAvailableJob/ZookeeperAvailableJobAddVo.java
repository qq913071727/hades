package com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   添加zookeeper监控任务的Vo类
 * @author linuo
 * @time   2023年5月11日10:45:58
 */
@Data
@ApiModel(value = "ZookeeperAvailableJobAddVo", description = "添加zookeeper监控任务的Vo类")
public class ZookeeperAvailableJobAddVo {
    @ApiModelProperty(value = "zookeeper id", dataType = "Integer")
    private Integer zookeeperId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;
}