package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   分页请求参数的Vo类
 * @author linuo
 * @time   2023年3月9日15:56:07
 */
@Data
@ApiModel(value = "PageVo", description = "分页请求参数的Vo类")
public class PageVo {
    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}