package com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJobLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   查询zookeeper监控任务日志列表接口的Vo类
 * @author linuo
 * @time   2023年5月11日16:32:01
 */
@Data
@ApiModel(value = "zookeeperAvailableJobLogListQueryVo", description = "查询zookeeper监控任务日志列表接口的Vo类")
public class ZookeeperAvailableJobLogListQueryVo {
    @ApiModelProperty(value = "zookeeper名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "zookeeperId", dataType = "Integer")
    private Integer zookeeperId;

    @ApiModelProperty(value = "启动时间开始", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "启动时间结束", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活", dataType = "Integer")
    private Integer active;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}