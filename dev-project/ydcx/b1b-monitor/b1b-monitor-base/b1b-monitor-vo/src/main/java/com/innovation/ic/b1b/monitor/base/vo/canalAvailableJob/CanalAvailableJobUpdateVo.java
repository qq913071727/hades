package com.innovation.ic.b1b.monitor.base.vo.canalAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   更新canal监控任务的Vo类
 * @author linuo
 * @time   2023年5月12日10:38:32
 */
@Data
@ApiModel(value = "CanalAvailableJobUpdateVo", description = "更新canal监控任务的Vo类")
public class CanalAvailableJobUpdateVo {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "canal id", dataType = "Integer")
    private Integer canalId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;
}