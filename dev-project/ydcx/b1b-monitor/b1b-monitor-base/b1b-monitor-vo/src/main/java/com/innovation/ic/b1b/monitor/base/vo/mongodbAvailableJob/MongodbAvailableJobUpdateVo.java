package com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   更新mongodb监控任务的Vo类
 * @author linuo
 * @time   2023年3月24日16:23:22
 */
@Data
@ApiModel(value = "MongodbAvailableJobUpdateVo", description = "更新mongodb监控任务的Vo类")
public class MongodbAvailableJobUpdateVo {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "mongodb id", dataType = "Integer")
    private Integer mongodbId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;
}