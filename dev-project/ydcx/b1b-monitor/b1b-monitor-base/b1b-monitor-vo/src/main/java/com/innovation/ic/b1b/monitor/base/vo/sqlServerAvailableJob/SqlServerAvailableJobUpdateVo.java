package com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc   sql server可用任务更新的Vo类
 * @author linuo
 * @time   2023年3月15日11:15:14
 */
@Data
@ApiModel(value = "SqlServerAvailableJobUpdateVo", description = "sql server可用任务更新的Vo类")
public class SqlServerAvailableJobUpdateVo {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "sqlServerId", dataType = "Integer")
    private Integer sqlServerId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    private String jobHandler;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;
}