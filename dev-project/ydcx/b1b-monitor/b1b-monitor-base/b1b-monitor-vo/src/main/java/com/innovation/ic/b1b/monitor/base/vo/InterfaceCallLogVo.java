package com.innovation.ic.b1b.monitor.base.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InterfaceCallLogVo", description = "接口调用日志Vo")
public class InterfaceCallLogVo {

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "方法", dataType = "String")
    private String method;

    @ApiModelProperty(value = "页面url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "开始日期", dataType = "Date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date beginDate;

    @ApiModelProperty(value = "结束日期", dataType = "Date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date endDate;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页几行", dataType = "Integer")
    private Integer pageSize;
}
