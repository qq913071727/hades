package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @desc   Zookeeper表实体类
 * @author linuo
 * @time   2023年5月11日10:09:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Zookeeper", description = "Zookeeper")
public class ZookeeperVo extends PageVo{

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "环境ids,前端不用理会", dataType = "List")
    private List<Integer> environmentIds;

    @ApiModelProperty(value = "IP", dataType = "String")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "String")
    private String port;
}