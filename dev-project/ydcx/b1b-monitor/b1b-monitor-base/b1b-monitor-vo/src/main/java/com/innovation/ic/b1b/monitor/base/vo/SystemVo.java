package com.innovation.ic.b1b.monitor.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SystemVo", description = "系统Vo")
public class SystemVo extends  PageVo{

    @ApiModelProperty(value = "主键id", dataType = "String")
    private Integer id;


    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;


}
