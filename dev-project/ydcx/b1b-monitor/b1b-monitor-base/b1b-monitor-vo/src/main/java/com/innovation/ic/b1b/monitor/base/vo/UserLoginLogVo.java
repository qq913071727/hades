package com.innovation.ic.b1b.monitor.base.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginLogVo", description = "用户登录日志Vo")
public class UserLoginLogVo {

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String username;

    @ApiModelProperty(value = "登录类型。1表示账号密码登录，2表示手机免密登录，3表示微信登录，4表示QQ登录，5表示支付宝登录", dataType = "Integer")
    private Integer loginType;

    @ApiModelProperty(value = "开始日期", dataType = "Date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date beginDate;

    @ApiModelProperty(value = "结束日期", dataType = "Date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date endDate;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页几行", dataType = "Integer")
    private Integer pageSize;
}
