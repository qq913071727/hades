package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FrontEndLog", description = "前端日志")
@TableName("front_end_log")
public class FrontEndLog {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    @TableField(value = "service_id")
    private Integer serviceId;

    @ApiModelProperty(value = "模块id", dataType = "Integer")
    @TableField(value = "model_id")
    private Integer modelId;

    @ApiModelProperty(value = "内容", dataType = "String")
    @TableField(value = "content")
    private String content;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}
