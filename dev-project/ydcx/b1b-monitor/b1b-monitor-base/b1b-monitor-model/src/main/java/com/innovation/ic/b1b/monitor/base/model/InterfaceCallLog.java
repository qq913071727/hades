package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InterfaceCallLog", description = "接口调用日志")
@TableName("interface_call_log")
public class InterfaceCallLog {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    @TableField(value = "service_id")
    private Integer serviceId;

    @ApiModelProperty(value = "接口url", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "方法", dataType = "String")
    @TableField(value = "method")
    private String method;

    @ApiModelProperty(value = "客户端ip", dataType = "String")
    @TableField(value = "remote_address")
    private String remoteAddress;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}
