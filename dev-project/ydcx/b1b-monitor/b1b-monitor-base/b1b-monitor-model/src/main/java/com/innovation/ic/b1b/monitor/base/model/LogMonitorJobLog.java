package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@TableName("log_monitor_job_log")
@ApiModel(value = "log_monitor_job_log", description = "")
@Setter
@Getter
public class LogMonitorJobLog {



                        @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
        
                            @ApiModelProperty(value = "日志监控任务的id")
    private Integer logMonitorJobId;
        
                            @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活")
    private Integer active;
        
                            @ApiModelProperty(value = "启动时间")
    private String startTime;
        
                            @ApiModelProperty(value = "问题描述")
    private String description;
        }