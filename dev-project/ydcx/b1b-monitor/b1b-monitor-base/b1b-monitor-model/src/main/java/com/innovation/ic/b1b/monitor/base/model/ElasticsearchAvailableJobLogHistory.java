package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ElasticsearchAvailableJobLogHistory", description = "elasticsearch可用任务历史日志表")
@TableName("elasticsearch_available_job_log_history")
public class ElasticsearchAvailableJobLogHistory {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "elasticsearch可用任务id", dataType = "Integer")
    @TableField(value = "elasticsearch_available_job_id")
    private Integer elasticsearchAvailableJobId;

    @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活", dataType = "Integer")
    @TableField(value = "active")
    private Integer active;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @TableField(value = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    @TableField(value = "description")
    private String description;
}
