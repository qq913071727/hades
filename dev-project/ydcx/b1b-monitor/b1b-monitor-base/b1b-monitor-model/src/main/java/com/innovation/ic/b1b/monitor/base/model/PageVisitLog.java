package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PageVisitLog", description = "页面访问日志")
@TableName("page_visit_log")
public class PageVisitLog {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    @TableField(value = "service_id")
    private Integer serviceId;

    @ApiModelProperty(value = "所属模块。1表示基座、2表示erm、3表示crm/srm、4表示询报价、5表示销售订单、6表示采购管理、7表示日常管理、8表示审批管理、9表示智能配置、10表示标准库、11表示远大论坛、12表示统计报表", dataType = "Integer")
    @TableField(value = "module")
    private Integer module;

    @ApiModelProperty(value = "页面url", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "客户端ip", dataType = "String")
    @TableField(value = "remote_address")
    private String remoteAddress;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}
