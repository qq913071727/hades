package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   服务器性能任务日志详情表
 * @author linuo
 * @time   2023年3月14日11:11:03
 */
@ApiModel(value = "ServerPerformanceJobLogDetail", description = "服务器性能任务日志详情表")
@TableName("server_performance_job_log_detail")
@Data
public class ServerPerformanceJobLogDetail implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "服务器性能任务日志id", dataType = "Integer")
    @TableField(value = "server_performance_job_log_id")
    private Integer serverPerformanceJobLogId;

    @ApiModelProperty(value = "文件系统", dataType = "String")
    @TableField(value = "file_system")
    private String fileSystem;

    @ApiModelProperty(value = "已用磁盘空间百分比", dataType = "double")
    @TableField(value = "used_disk_space_percentage")
    private double usedDiskSpacePercentage;

    @ApiModelProperty(value = "挂载点", dataType = "String")
    @TableField(value = "mount_point")
    private String mountPoint;
}