package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   zookeeper可用任务表实体类
 * @author linuo
 * @time   2023年5月11日10:14:15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ZookeeperAvailableJob", description = "zookeeper可用任务表")
@TableName("zookeeper_available_job")
public class ZookeeperAvailableJob {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "zookeeper id", dataType = "Integer")
    @TableField(value = "zookeeper_id")
    private Integer zookeeperId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    @TableField(value = "schedule_expression")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    @TableField(value = "enable")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    @TableField(value = "alarm_email")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    @TableField(value = "alarm_cell_phone")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "modify_time")
    private Date modifyTime;
}