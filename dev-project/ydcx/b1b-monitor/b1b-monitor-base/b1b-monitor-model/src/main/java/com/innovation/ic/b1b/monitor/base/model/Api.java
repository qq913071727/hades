package com.innovation.ic.b1b.monitor.base.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Api", description = "Api")
@TableName("api")
public class Api {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    @TableField(value = "service_id")
    private Integer serviceId;


    @ApiModelProperty(value = "url", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "方法。1表示GET，2表示POST", dataType = "Integer")
    @TableField(value = "method")
    private Integer method;

    @ApiModelProperty(value = "参数类型。1表示form中的参数，2表示url查询字符串，3表示body中的参数", dataType = "Integer")
    @TableField(value = "parameter_type")
    private Integer parameterType;

    @ApiModelProperty(value = "参数", dataType = "String")
    @TableField(value = "parameter")
    private String parameter;

    @ApiModelProperty(value = "header中的参数", dataType = "String")
    @TableField(value = "header")
    private String header;

    @ApiModelProperty(value = "接口调用成功的标准。1表示正则表达式，2表示文件下载成功", dataType = "Integer")
    @TableField(value = "response_success_standard")
    private Integer responseSuccessStandard;


    @ApiModelProperty(value = "判断接口调用成功的正则表达式", dataType = "String")
    @TableField(value = "response_success_regular_expression")
    private String responseSuccessRegularExpression;




}
