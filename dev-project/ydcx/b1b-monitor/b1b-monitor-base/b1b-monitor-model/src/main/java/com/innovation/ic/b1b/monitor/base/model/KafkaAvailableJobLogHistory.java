package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   kafka可用任务历史日志表实体类
 * @author linuo
 * @time   2023年5月8日11:27:40
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "KafkaAvailableJobLogHistory", description = "kafka可用任务历史日志表")
@TableName("kafka_available_job_log_history")
public class KafkaAvailableJobLogHistory {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "kafka可用任务id", dataType = "Integer")
    @TableField(value = "kafka_available_job_id")
    private Integer kafkaAvailableJobId;

    @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活", dataType = "Integer")
    @TableField(value = "active")
    private Integer active;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @TableField(value = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    @TableField(value = "description")
    private String description;
}