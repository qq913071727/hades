package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@TableName("log_monitor_job")
@ApiModel(value = "log_monitor_job", description = "")
@Setter
@Getter
public class LogMonitorJob {



                        @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Integer id;
        
                            @ApiModelProperty(value = "log id")
    private Integer logId;
        
                            @ApiModelProperty(value = "调用表达式")
    private String scheduleExpression;
        
                            @ApiModelProperty(value = "是否启用(0否、1是)")
    private Integer enable;
        
                            @ApiModelProperty(value = "报警邮箱")
    private String alarmEmail;
        
                            @ApiModelProperty(value = "报警手机号")
    private String alarmCellPhone;
        
                            @ApiModelProperty(value = "创建时间")
    private String createTime;
        
                            @ApiModelProperty(value = "更新时间")
    private String modifyTime;
        }