package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   Kafka表实体类
 * @author linuo
 * @time   2023年5月8日11:16:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Kafka", description = "Kafka")
@TableName("kafka")
public class Kafka {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "IP", dataType = "String")
    @TableField(value = "ip")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "String")
    @TableField(value = "port")
    private String port;
}