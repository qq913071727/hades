package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RelSystemEnvironmentServiceModel", description = "服务和模块关联表")
@TableName("rel_system_environment_service_model")
public class RelSystemEnvironmentServiceModel {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "服务环境系统关联id", dataType = "Integer")
    @TableField(value = "rel_system_environment_service_id")
    private Integer relSystemEnvironmentServiceId;


    @ApiModelProperty(value = "模块id", dataType = "Integer")
    @TableField(value = "model_id")
    private Integer modelId;

}
