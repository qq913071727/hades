package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   sql server可用任务表
 * @author linuo
 * @time   2023年3月14日13:44:06
 */
@ApiModel(value = "SqlServerAvailableJob", description = "sql server可用任务表")
@TableName("sql_server_available_job")
@Data
public class SqlServerAvailableJob implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "sql server id", dataType = "Integer")
    @TableField(value = "sql_server_id")
    private Integer sqlServerId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    @TableField(value = "schedule_expression")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    @TableField(value = "job_handler")
    private String jobHandler;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    @TableField(value = "enable")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    @TableField(value = "alarm_email")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    @TableField(value = "alarm_cell_phone")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "modify_time")
    private Date modifyTime;
}