package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   mysql实体类
 * @author linuo
 * @time   2023年3月16日13:17:50
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Mysql", description = "Mysql")
@TableName("mysql")
public class Mysql {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "url", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;
}