package com.innovation.ic.b1b.monitor.base.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiAvailableJobLog", description = "接口可用任务日志表")
@TableName("api_available_job_log")
public class ApiAvailableJobLog {


    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "api可用任务id", dataType = "Integer")
    @TableField(value = "api_available_job_id")
    private Integer apiAvailableJobId;

    @ApiModelProperty(value = "表示接口是否可以。1表示可用，0表示不可用", dataType = "Integer")
    @TableField(value = "available")
    private Integer available;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    @TableField(value = "description")
    private String description;
}
