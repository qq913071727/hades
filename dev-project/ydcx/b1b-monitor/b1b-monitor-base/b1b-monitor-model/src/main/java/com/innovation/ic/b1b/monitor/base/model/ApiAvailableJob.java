package com.innovation.ic.b1b.monitor.base.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiAvailableJob", description = "接口可用任务表")
@TableName("api_available_job")
public class ApiAvailableJob {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "接口id", dataType = "Integer")
    @TableField(value = "api_id")
    private Integer apiId;

    @ApiModelProperty(value = "调度表达式", dataType = "String")
    @TableField(value = "schedule_expression")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用 1:启用 0:未启用", dataType = "Integer")
    @TableField(value = "enable")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "Integer")
    @TableField(value = "alarm_email_id")
    private Integer alarmEmailId;

    @ApiModelProperty(value = "报警手机号", dataType = "Integer")
    @TableField(value = "alarm_cell_phone_id")
    private Integer alarmCellPhoneId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_time")
    private Date createTime;

}
