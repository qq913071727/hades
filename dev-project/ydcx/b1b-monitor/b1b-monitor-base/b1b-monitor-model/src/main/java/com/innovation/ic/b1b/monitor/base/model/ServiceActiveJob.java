package com.innovation.ic.b1b.monitor.base.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "serviceActiveJob", description = "服务存活任务表")
@TableName("service_active_job")
public class ServiceActiveJob {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    @TableField(value = "service_id")
    private Integer serviceId;

    @ApiModelProperty(value = "IP", dataType = "String")
    @TableField(value = "ip")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "String")
    @TableField(value = "port")
    private String port;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "判断服务的进程是否存在的正则表达式", dataType = "String")
    @TableField(value = "process_regular_expression")
    private String processRegularExpression;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    @TableField(value = "schedule_expression")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    @TableField(value = "job_handler")
    private String jobHandler;

    @ApiModelProperty(value = "是否启用。1表示启用，0表示未启用", dataType = "Integer")
    @TableField(value = "enable")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "Integer")
    @TableField(value = "alarm_email_id")
    private Integer alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "Integer")
    @TableField(value = "alarm_cell_phone_id")
    private Integer alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_time")
    private Date createTime;
}
