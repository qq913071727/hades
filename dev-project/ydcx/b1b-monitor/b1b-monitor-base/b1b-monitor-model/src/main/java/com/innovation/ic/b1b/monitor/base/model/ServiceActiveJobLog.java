package com.innovation.ic.b1b.monitor.base.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServiceActiveJobLog", description = "服务存活任务日志表")
@TableName("service_active_job_log")
public class ServiceActiveJobLog {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "服务存活任务id", dataType = "Integer")
    @TableField(value = "service_active_job_id")
    private Integer serviceActiveJobId;

    @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活", dataType = "Integer")
    @TableField(value = "active")
    private Integer active;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    @TableField(value = "description")
    private String description;
}
