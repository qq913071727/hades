package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   服务器性能任务表实体类
 * @author linuo
 * @time   2023年3月9日15:23:30
 */
@ApiModel(value = "ServerPerformanceJob", description = "服务器性能任务表")
@TableName("server_performance_job")
@Data
public class ServerPerformanceJob implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "服务器id", dataType = "Integer")
    @TableField(value = "server_id")
    private Integer serverId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    @TableField(value = "schedule_expression")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    @TableField(value = "job_handler")
    private String jobHandler;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    @TableField(value = "enable")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    @TableField(value = "alarm_email")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    @TableField(value = "alarm_cell_phone")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "modify_time")
    private Date modifyTime;
}