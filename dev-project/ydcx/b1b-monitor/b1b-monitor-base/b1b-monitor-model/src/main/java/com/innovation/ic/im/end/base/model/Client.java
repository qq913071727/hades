package com.innovation.ic.im.end.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 客户端信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Client", description = "客户端信息")
@TableName("client")
public class Client {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "客户端名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "类型(1.即时通讯前端;2.erp9前端;3. 芯聊客户端)", dataType = "String")
    @TableField(value = "type_")
    private String type;
}