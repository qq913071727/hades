package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Server", description = "服务器表")
@TableName("server")
public class Server {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "ip", dataType = "String")
    @TableField(value = "ip")
    private String ip;

    @ApiModelProperty(value = "ssh协议的端口", dataType = "String")
    @TableField(value = "ssh_port")
    private Integer sshPort;

    @ApiModelProperty(value = "操作系统。1表示linux，2表示windows", dataType = "Integer")
    @TableField(value = "system_operation")
    private Integer systemOperation;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "description")
    private String description;
}