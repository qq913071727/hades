package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Elasticsearch", description = "Elasticsearch")
@TableName("elasticsearch")
public class Elasticsearch {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "ip", dataType = "String")
    @TableField(value = "ip")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "String")
    @TableField(value = "port")
    private String port;

    @ApiModelProperty(value = "index", dataType = "String")
    @TableField(value = "es_index")
    private String esIndex;
}