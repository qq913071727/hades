package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Log", description = "日志")
@TableName("log")
public class Log {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "日志名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    @TableField(value = "system_id")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    @TableField(value = "environment_id")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    @TableField(value = "service_id")
    private Integer serviceId;

    @ApiModelProperty(value = "ip", dataType = "String")
    @TableField(value = "ip")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "Integer")
    @TableField(value = "port")
    private Integer port;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "日志文件路径的正则表达式", dataType = "String")
    @TableField(value = "file_regular_expression")
    private String fileRegularExpression;

    @ApiModelProperty(value = "关键字列表，用英文逗号分割", dataType = "String")
    @TableField(value = "key_word_list")
    private String keyWordList;
}
