package com.innovation.ic.b1b.monitor.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   服务器性能任务日志表
 * @author linuo
 * @time   2023年3月14日11:11:03
 */
@ApiModel(value = "ServerPerformanceJobLog", description = "服务器性能任务日志表")
@TableName("server_performance_job_log")
@Data
public class ServerPerformanceJobLog implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "服务器性能任务id", dataType = "Integer")
    @TableField(value = "server_performance_job_id")
    private Integer serverPerformanceJobId;

    @ApiModelProperty(value = "已用内存百分比", dataType = "Double")
    @TableField(value = "used_ram_percentage")
    private Double usedRamPercentage;

    @ApiModelProperty(value = "已用磁盘空间最大百分比", dataType = "Double")
    @TableField(value = "used_disk_space_max_percentage")
    private Double usedDiskSpaceMaxPercentage;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @TableField(value = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "String")
    @TableField(value = "description")
    private String description;
}