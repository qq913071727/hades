package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   mysql可用任务表的常量类
 * @author linuo
 * @time   2023年3月22日09:30:45
 */
public class MysqlAvailableJobConstant {
    /** mysql id */
    public static final String MYSQL_ID = "mysql_id";

    /** 主键id */
    public static final String ID = "id";

    /** mysql驱动名称 */
    public static final String MYSQL_CLASS_NAME = "com.mysql.jdbc.Driver";

    /** 查询sql */
    public static final String QUERY_SQL = "select * from user";

    /** Host */
    public static final String HOST = "Host";

    /** 用户 */
    public static final String USER = "User";
}
