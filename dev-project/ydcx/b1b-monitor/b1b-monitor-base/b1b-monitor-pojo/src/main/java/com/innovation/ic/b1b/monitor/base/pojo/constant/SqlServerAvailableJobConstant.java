package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   sql server可用任务表的常量类
 * @author linuo
 * @time   2023年3月22日09:50:55
 */
public class SqlServerAvailableJobConstant {
    /** sql server id */
    public static final String SQL_SERVER_ID = "sql_server_id";

    /** 主键id */
    public static final String ID = "id";

    /** 查询sql */
    public static final String QUERY_SQL = "SELECT * FROM PrimaryKeys";

    /** sql server驱动名称 */
    public static final String SQL_SERVER_CLASS_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    /** 名称 */
    public static final String NAME = "Name";

    /** 值 */
    public static final String VALUE = "Value";
}
