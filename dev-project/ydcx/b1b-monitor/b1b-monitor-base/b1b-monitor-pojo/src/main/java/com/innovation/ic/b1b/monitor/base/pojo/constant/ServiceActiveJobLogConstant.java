package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   服务存活日志常量类
 * @author swq
 * @time   2023年4月10日16:45:14
 */
public class ServiceActiveJobLogConstant {

    /** ssh 命令 */
    public static final String SSH_ORDER = "ps -ef | grep ";

    /** ssh 命令 */
    public static final String SSH_ORDER2 = "| grep -v grep";

    /** ssh 命令 */
    public static final String SSH_ORDER3 = "'";

    /** 判断条件 后缀 */
    public static final String CONDITION = ".jar";

    /** 状态 正常*/
    public static final Integer STATUS_ONE = 1;

    /** 状态 停用*/
    public static final Integer STATUS_ZERO = 0;

    /** 执行 未启动 信息*/
    public static final String EXECUTE_MSG = "当前任务是未启动状态,无法执行";
}
