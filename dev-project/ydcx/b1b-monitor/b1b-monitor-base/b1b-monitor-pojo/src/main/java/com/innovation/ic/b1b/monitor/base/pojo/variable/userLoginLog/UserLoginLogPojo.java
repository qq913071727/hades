package com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登录日志分页显示的pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginLogPojo", description = "分页显示Vo")
public class UserLoginLogPojo implements Serializable {

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

//    @ApiModelProperty(value = "日期", dataType = "String")
////    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
//    private String date;

    @ApiModelProperty(value = "日期", dataType = "String")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private String theDate;

    @ApiModelProperty(value = "登录次数", dataType = "Integer")
    private Integer loginCount;

    @ApiModelProperty(value = "登录账号数量", dataType = "Integer")
    private Integer loginUsernameCount;
}
