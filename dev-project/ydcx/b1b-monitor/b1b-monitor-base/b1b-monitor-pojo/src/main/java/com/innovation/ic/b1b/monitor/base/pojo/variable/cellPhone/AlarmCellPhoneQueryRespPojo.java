package com.innovation.ic.b1b.monitor.base.pojo.variable.cellPhone;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   报警手机号查询接口返回的pojo类
 * @author linuo
 * @time   2023年3月21日10:40:30
 */
@Data
public class AlarmCellPhoneQueryRespPojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String cellPhone;
}