package com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearch;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   es查询接口返回的pojo类
 * @author linuo
 * @time   2023年3月24日16:03:20
 */
@Data
public class ElasticsearchQueryRespPojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "es名称", dataType = "String")
    private String name;
}