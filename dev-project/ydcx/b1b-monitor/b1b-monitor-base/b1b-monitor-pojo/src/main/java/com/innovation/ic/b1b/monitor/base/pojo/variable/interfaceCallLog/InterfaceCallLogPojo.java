package com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口访问日志分页显示的pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InterfaceCallLogPojo", description = "分页显示Vo")
public class InterfaceCallLogPojo implements Serializable {

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

//    @ApiModelProperty(value = "日期", dataType = "Date")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
//    private Date date;

    @ApiModelProperty(value = "接口", dataType = "String")
    private String url;

    @ApiModelProperty(value = "调用次数", dataType = "Integer")
    private Integer count;
}
