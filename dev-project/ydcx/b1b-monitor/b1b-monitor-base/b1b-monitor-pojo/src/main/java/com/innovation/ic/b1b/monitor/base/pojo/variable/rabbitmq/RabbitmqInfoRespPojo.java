package com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmq;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   查询rabbitmq信息接口返回的pojo类
 * @author linuo
 * @time   2023年3月28日16:16:49
 */
@Data
public class RabbitmqInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "rabbitmqId", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "rabbitmq名称", dataType = "String")
    private String name;
}