package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   服务器空间Pojo
 * @author linuo
 * @time   2023年3月22日13:17:55
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServerSpacePojo", description = "服务器空间Pojo")
public class ServerSpacePojo {
    @ApiModelProperty(value = "文件系统", dataType = "String")
    private String fileSystem;

    @ApiModelProperty(value = "容量", dataType = "String")
    private String capacity;

    @ApiModelProperty(value = "已用", dataType = "String")
    private String used;

    @ApiModelProperty(value = "可用", dataType = "String")
    private String available;

    @ApiModelProperty(value = "已用百分比", dataType = "String")
    private String percentageSpent;

    @ApiModelProperty(value = "挂载点", dataType = "String")
    private String mountPoint;
}