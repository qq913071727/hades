package com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLog;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLogDetail.ServerPerformanceJobLogDetailInfoData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @desc   服务器性能任务日志表查询接口返回的数据
 * @author linuo
 * @time   2023年3月13日13:30:09
 */
@Data
public class ServerPerformanceJobLogListRespData implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "服务器名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "已用内存百分比", dataType = "String")
    private String usedRamPercentage;

    @ApiModelProperty(value = "已用磁盘空间最大百分比", dataType = "Double")
    private String usedDiskSpaceMaxPercentage;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "Date")
    private String description;

    @ApiModelProperty(value = "服务器性能任务日志详情信息", dataType = "Date")
    private List<ServerPerformanceJobLogDetailInfoData> detail;
}