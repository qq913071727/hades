package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LogPojo", description = "LogPojo")
public  class LogPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;



    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;


    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

    @ApiModelProperty(value = "ip", dataType = "String")
    private String ip;


    @ApiModelProperty(value = "端口", dataType = "Integer")
    private Integer port;


    @ApiModelProperty(value = "账号", dataType = "String")
    private String username;


    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;


    @ApiModelProperty(value = "日志文件路径的正则表达式", dataType = "String")
    private String fileRegularExpression;


    @ApiModelProperty(value = "关键字列表，用英文逗号分割", dataType = "String")
    private String keyWordList;


}