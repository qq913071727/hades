package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   zookeeper可用任务表的常量类
 * @author linuo
 * @time   2023年5月11日13:20:27
 */
public class ZookeeperAvailableJobConstant {
    /** zookeeper id */
    public static final String ZOOKEEPER_ID = "zookeeper_id";

    /** 主键id */
    public static final String ID = "id";
}
