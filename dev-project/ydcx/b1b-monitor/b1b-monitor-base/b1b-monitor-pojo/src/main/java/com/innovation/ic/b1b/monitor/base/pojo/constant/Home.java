package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @author swq
 * @desc 首页功能相关固定查询条件
 * @time 2023年3月20日16:52:26
 */
public class Home {

    /**
     * 接口状态/mysql状态/服务器性能状态/服务存活任务状态/sql server状态
     * 默认0表示没有存活
     */
    public static final Integer STATUS_TYPE = 0;

    /**
     * 已用内存百分比
     */
    public static final Double USED_RAM_PERCENTAGE = 95.0;

    /**
     * 已用磁盘空间百分比
     */
    public static final Double USED_DISK_SPACE_MAX_PERCENTAGE = 95.0;
}
