package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   elasticsearch可用任务表的常量类
 * @author linuo
 * @time   2023年3月24日10:13:36
 */
public class ElasticsearchAvailableJobConstant {
    /** elasticsearch id */
    public static final String ELASTICSEARCH_ID = "elasticsearch_id";

    /** 主键id */
    public static final String ID = "id";
}
