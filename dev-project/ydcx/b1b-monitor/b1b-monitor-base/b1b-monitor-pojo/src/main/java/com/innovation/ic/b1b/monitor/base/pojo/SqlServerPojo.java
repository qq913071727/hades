package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SqlServerPojo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;


    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;


    @ApiModelProperty(value = "系统id", dataType = "Integer")
        private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

}
