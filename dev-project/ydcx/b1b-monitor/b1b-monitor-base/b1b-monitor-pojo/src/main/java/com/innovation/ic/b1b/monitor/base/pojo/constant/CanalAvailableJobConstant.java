package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   canal可用任务表的常量类
 * @author linuo
 * @time   2023年5月12日10:15:19
 */
public class CanalAvailableJobConstant {
    /** canal id */
    public static final String CANAL_ID = "canal_id";

    /** 主键id */
    public static final String ID = "id";
}
