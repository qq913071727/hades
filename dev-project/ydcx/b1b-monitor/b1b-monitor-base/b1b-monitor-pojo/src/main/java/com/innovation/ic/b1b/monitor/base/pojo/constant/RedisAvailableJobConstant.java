package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   redis可用任务表的常量类
 * @author linuo
 * @time   2023年3月28日14:56:42
 */
public class RedisAvailableJobConstant {
    /** redis id */
    public static final String REDIS_ID = "redis_id";

    /** 主键id */
    public static final String ID = "id";
}
