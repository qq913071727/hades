package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServicePojo", description = "服务表Pojo")
public class ServicePojo {

    @ApiModelProperty(value = "主键id", dataType = "String")
    private Integer id;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "reId", dataType = "Integer")
    private Integer reId;


}
