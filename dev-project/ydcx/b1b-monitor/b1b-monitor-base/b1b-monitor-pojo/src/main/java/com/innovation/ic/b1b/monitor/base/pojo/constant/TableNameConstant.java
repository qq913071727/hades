package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   表名常量类
 * @author linuo
 * @time   2023年3月17日09:38:06
 */
public class TableNameConstant {
    /** 服务器性能任务表 */
    public static final String SERVER_PERFORMANCE_JOB = "ServerPerformanceJob";

    /** mysql可用任务表 */
    public static final String MYSQL_AVAILABLE_JOB = "MysqlAvailableJob";

    /** rabbitmq可用任务表 */
    public static final String RABBITMQ_AVAILABLE_JOB = "RabbitmqAvailableJob";

    /** sql server可用任务表 */
    public static final String SQL_SERVER_AVAILABLE_JOB = "SqlServerAvailableJob";

    /** elasticsearch可用任务表 */
    public static final String ELASTICSEARCH_AVAILABLE_JOB = "ElasticsearchAvailableJob";

    /** mongodb可用任务表 */
    public static final String MONGODB_AVAILABLE_JOB = "MongodbAvailableJob";

    /**
     *  logMonitor任务表
     */
    public static final String LOG_MONITOR_JOB = "logMonitorJob";

    /** kafka可用任务表 */
    public static final String KAFKA_AVAILABLE_JOB = "KafkaAvailableJob";

    /** canal可用任务表 */
    public static final String CANAL_AVAILABLE_JOB = "CanalAvailableJob";

    /** zookeeper可用任务表 */
    public static final String ZOOKEEPER_AVAILABLE_JOB = "ZookeeperAvailableJob";

    /** redis可用任务表 */
    public static final String REDIS_AVAILABLE_JOB = "RedisAvailableJob";

    /** 接口 任务表 */
    public static final String API_AVAILABLE_JOB = "ApiAvailableJob";

    /** 服务存活任务表 */
    public static final String SERVICE_ACTIVE_JOB = "ServiceActiveJob";
}
