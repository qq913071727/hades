package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   服务器性能任务表的常量类
 * @author linuo
 * @time   2023年3月22日09:45:14
 */
public class ServerPerformanceJobConstant {
    /** 服务器id */
    public static final String SERVER_ID = "server_id";

    /** 主键id */
    public static final String ID = "id";

    /** 内存类型 */
    public static final String MEMORY_TYPE = "Mem:";

    /** 虚拟内存类型 */
    public static final String SWAP_MEMORY_TYPE = "Swap:";
}
