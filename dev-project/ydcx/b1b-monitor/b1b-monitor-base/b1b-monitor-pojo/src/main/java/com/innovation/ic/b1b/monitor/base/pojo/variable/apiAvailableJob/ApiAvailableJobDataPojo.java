package com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJob;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 接口任务数据
 * @Date 2023/3/30 13:26
 **/
@Data
public class ApiAvailableJobDataPojo {

    @ApiModelProperty(value = "接口任务id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "系统 id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "环境 id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

    @ApiModelProperty(value = "服务 id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "接口名称", dataType = "String")
    private String apiName;

    @ApiModelProperty(value = "接口id", dataType = "Integer")
    private Integer apiId;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用 1:启用 0:未启用", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警邮箱id", dataType = "Integer")
    private Integer alarmEmailId;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "报警手机号id", dataType = "Integer")
    private Integer alarmCellPhoneId;

    @ApiModelProperty(value = "正则表达式", dataType = "String")
    private String responseSuccessRegularExpression;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date createTime;
}
