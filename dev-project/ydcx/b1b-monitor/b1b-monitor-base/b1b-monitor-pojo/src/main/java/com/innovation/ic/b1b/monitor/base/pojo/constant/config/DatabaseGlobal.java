package com.innovation.ic.b1b.monitor.base.pojo.constant.config;

public class DatabaseGlobal {

    public static final String B1B_MONITOR0 = "b1b-monitor0";

    public static final String B1B_MONITOR1 = "b1b-monitor1";

    public static final String B1B_MONITOR2 = "b1b-monitor2";

    /**
     * 监控系统的b1b-monitor数据库（分库分表）
     */
    public static final String B1B_MONITOR_SHARDING = "b1b-monitor-sharding";

    /**
     * 即时通信的数据库
     */
    public static final String IM = "im";

    /**
     * 监控系统的b1b-monitor数据库（读写分离）
     */
    public static final String B1B_MONITOR_MASTER_SLAVE = "b1b-monitor-master-slave";
}
