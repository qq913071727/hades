package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   任务数据集合的常量类
 * @author linuo
 * @time   2023年3月20日15:07:44
 */
public class JobDataMapConstant {
    /** 接收账号 */
    public static final String RECEIVE_ACCOUNT = "receiveAccount";

    /** 真实姓名 */
    public static final String REAL_NAME = "realName";

    /** 任务id */
    public static final String JOB_ID = "jobId";

    /** id */
    public static final String ID = "id";
}
