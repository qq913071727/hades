package com.innovation.ic.b1b.monitor.base.pojo.enums;

/**
 * @author zengqinglong
 * @desc api 参数类型
 * @Date 2023/4/11 14:37
 **/
public enum ApiParameterTypeEnum {
    FROM(1, "from中的参数"),
    NULL(2, "无"),
    JSON(3, "body中的参数");

    private Integer code;
    private String desc;

    ApiParameterTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
