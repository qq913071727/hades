package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServicePojo", description = "服务表Pojo")
public class ApiPojo {

    @ApiModelProperty(value = "主键id", dataType = "String")
    private Integer id;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;


    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;


    @ApiModelProperty(value = "服务id", dataType = "Integer")
    private Integer serviceId;


    @ApiModelProperty(value = "url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "方法。1表示GET，2表示POST", dataType = "Integer")
    private Integer method;

    @ApiModelProperty(value = "参数类型。1表示form中的参数，2表示url查询字符串，3表示body中的参数", dataType = "Integer")
    private Integer parameterType;

    @ApiModelProperty(value = "参数", dataType = "String")
    private String parameter;

    @ApiModelProperty(value = "header中的参数", dataType = "String")
    private String header;

    @ApiModelProperty(value = "接口调用成功的标准。1表示正则表达式，2表示文件下载成功", dataType = "Integer")
    private Integer responseSuccessStandard;


    @ApiModelProperty(value = "判断接口调用成功的正则表达式", dataType = "String")
    private String responseSuccessRegularExpression;


}
