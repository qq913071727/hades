package com.innovation.ic.b1b.monitor.base.pojo.enums;

/**
 * @author zengqinglong
 * @desc api 请求
 * @Date 2023/4/11 14:36
 **/
public enum ApiMethodEnum {
    GET(1,"GET"),
    POST(2,"POST");
    private Integer code;
    private String desc;

    ApiMethodEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
