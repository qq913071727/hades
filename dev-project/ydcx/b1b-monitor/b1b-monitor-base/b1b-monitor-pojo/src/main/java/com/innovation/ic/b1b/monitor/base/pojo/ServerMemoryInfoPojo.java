package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   内存信息Pojo
 * @author linuo
 * @time   2023年3月23日10:12:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ServerMemoryInfoPojo", description = "内存信息Pojo")
public class ServerMemoryInfoPojo {
    @ApiModelProperty(value = "内存类型(Mem物理内存、Swap虚拟内存)", dataType = "String")
    private String type;

    @ApiModelProperty(value = "总容量", dataType = "BigDecimal")
    private BigDecimal total;

    @ApiModelProperty(value = "已用容量", dataType = "BigDecimal")
    private BigDecimal used;

    @ApiModelProperty(value = "空闲内存", dataType = "BigDecimal")
    private BigDecimal free;

    @ApiModelProperty(value = "共享内存", dataType = "BigDecimal")
    private BigDecimal shared;

    @ApiModelProperty(value = "缓冲区缓存", dataType = "BigDecimal")
    private BigDecimal buffOrCache;

    @ApiModelProperty(value = "可用内存", dataType = "BigDecimal")
    private BigDecimal available;
}