package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "EnvironmentVo", description = "环境Vo")
public class EnvironmentPojo {

    @ApiModelProperty(value = "主键id", dataType = "String")
    private Integer id;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String name;


    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "系统名称id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "关联id，修改和删除的时候传", dataType = "String")
    private Integer associationId;
}
