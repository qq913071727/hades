package com.innovation.ic.b1b.monitor.base.pojo.variable.email;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   报警邮箱查询接口返回的pojo类
 * @author linuo
 * @time   2023年3月21日10:36:55
 */
@Data
public class AlarmEmailQueryRespPojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;
}