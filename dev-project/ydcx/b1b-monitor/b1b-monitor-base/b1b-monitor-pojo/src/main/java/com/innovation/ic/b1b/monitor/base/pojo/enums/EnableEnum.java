package com.innovation.ic.b1b.monitor.base.pojo.enums;

/**
 * @desc   启用状态枚举
 * @author linuo
 * @time   2023年3月13日15:50:41
 */
public enum EnableEnum {
    YES(1, "是"),
    NO(0, "否");

    private Integer code;
    private String desc;

    EnableEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static EnableEnum of(Integer code) {
        for (EnableEnum c : EnableEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (EnableEnum c : EnableEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}