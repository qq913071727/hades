package com.innovation.ic.b1b.monitor.base.pojo.variable.frontEndLog;

import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 前端日志分页显示的pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FrontEndLogPagePojo", description = "分页显示Vo")
public class FrontEndLogPagePojo implements Serializable {

    @ApiModelProperty(value = "分页显示的数据", dataType = "List")
    private List<FrontEndLogPojo> frontEndLogPojoList;

    @ApiModelProperty(value = "记录总数", dataType = "Integer")
    private Integer count;
}
