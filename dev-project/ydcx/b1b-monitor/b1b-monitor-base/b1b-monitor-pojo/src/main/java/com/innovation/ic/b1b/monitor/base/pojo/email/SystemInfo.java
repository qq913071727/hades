package com.innovation.ic.b1b.monitor.base.pojo.email;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @ClassName SystemInfo
 * @Description 系统信息类 (资源信息类，超类)
 * @Date 2022/10/27
 * @Author myq
 */
@Getter
@Setter
public abstract class SystemInfo {

    /**
     * cpu 占用率
     */
    private Double systemCpuLoad;
    /**
     * 内存 占用率
     */
    private Double memoryUseRatio;
    /**
     * 内存 总大小
     */
    private Double totalMemory;
    /**
     * 内存 空闲大小
     */
    private Double freeMemory;
    /**
     * 该进程对CPU的占用率
     */
    private Double processCpuLoad;

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String hostAddress = null;
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        result.append("IP:").append(hostAddress).append("，").append("系统CPU占用率: ")
                .append(twoDecimal(systemCpuLoad))
                .append("%，内存占用率：")
                .append(twoDecimal(memoryUseRatio))
                .append("%，系统总内存：")
                .append(twoDecimal(totalMemory))
                .append("GB，系统剩余内存：")
                .append(twoDecimal(freeMemory))
                .append("GB，该进程占用CPU：")
                .append(twoDecimal(processCpuLoad))
                .append("%");
        return result.toString();
    }

    /**
     * @Description: 向上取整保留俩位小数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2715:17
     */
    private static double twoDecimal(double doubleValue) {
        BigDecimal bigDecimal = new BigDecimal(doubleValue).setScale(2, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }

}
