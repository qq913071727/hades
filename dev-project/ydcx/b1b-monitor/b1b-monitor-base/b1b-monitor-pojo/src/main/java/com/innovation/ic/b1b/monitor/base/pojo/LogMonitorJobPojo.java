package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@ApiModel(value= "LogMonitorJobPojo" ,description = " 响应类")
@Setter
@Getter
public class LogMonitorJobPojo{


                                                        
    
                                                            @ApiModelProperty(value = "主键")
            private Integer id;
                    
                                                            @ApiModelProperty(value = "log id")
            private Integer logId;
                    
                                                            @ApiModelProperty(value = "调用表达式")
            private String scheduleExpression;
                    
                                                            @ApiModelProperty(value = "是否启用(0否、1是)")
            private Integer enable;
                    
                                                            @ApiModelProperty(value = "报警邮箱")
            private String alarmEmail;
                    
                                                            @ApiModelProperty(value = "报警手机号")
            private String alarmCellPhone;
                    
                                                            @ApiModelProperty(value = "创建时间")
            private String createTime;
                    
                                                            @ApiModelProperty(value = "更新时间")
            private String modifyTime;
                    }