package com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author swq
 * @desc 服务存活任务表查询接口返回的数据
 * @time 2023年3月29日13:30:09
 */
@Data
public class ServiceActiveJobListRespData {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

    @ApiModelProperty(value = "IP", dataType = "String")
    private String ip;

    @ApiModelProperty(value = "端口", dataType = "String")
    private String port;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "判断服务的进程是否存在的正则表达式", dataType = "String")
    private String processRegularExpression;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    private String jobHandler;

    @ApiModelProperty(value = "是否启用。1表示启用，0表示未启用", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
