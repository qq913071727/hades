package com.innovation.ic.b1b.monitor.base.pojo.variable.server;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   服务器查询接口返回的pojo类
 * @author linuo
 * @time   2023年3月21日10:50:41
 */
@Data
public class ServerQueryRespPojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "服务器名称", dataType = "String")
    private String name;
}