package com.innovation.ic.b1b.monitor.base.pojo.enums;

/**
 * @author zengqinglong
 * @desc 接口可用
 * @Date 2023/4/11 15:11
 **/
public enum ApiAvailableEnum {
    AVAILABLE(1, "可用"),
    NO_AVAILABLE(0, "不可用");
    private Integer code;
    private String desc;

    ApiAvailableEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
