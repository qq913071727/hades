package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   Mongodb可用任务表的常量类
 * @author linuo
 * @time   2023年3月24日14:52:28
 */
public class MongodbAvailableJobConstant {
    /** mongodb id */
    public static final String MONGODB_ID = "mongodb_id";

    /** 主键id */
    public static final String ID = "id";
}
