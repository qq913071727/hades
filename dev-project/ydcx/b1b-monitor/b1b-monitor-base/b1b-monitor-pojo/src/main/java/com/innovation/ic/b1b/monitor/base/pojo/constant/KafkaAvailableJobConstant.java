package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   Kafka可用任务表的常量类
 * @author linuo
 * @time   2023年5月8日13:25:05
 */
public class KafkaAvailableJobConstant {
    /** kafka id */
    public static final String KAFKA_ID = "kafka_id";

    /** 主键id */
    public static final String ID = "id";
}
