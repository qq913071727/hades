package com.innovation.ic.b1b.monitor.base.pojo.variable.pageVisitLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 页面访问日志分页显示的pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PageVisitLogPojo", description = "分页显示Vo")
public class PageVisitLogPojo implements Serializable {

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

    @ApiModelProperty(value = "url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "页面访问次数", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "客户端ip", dataType = "String")
    private String remoteAddress;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createTime;
}
