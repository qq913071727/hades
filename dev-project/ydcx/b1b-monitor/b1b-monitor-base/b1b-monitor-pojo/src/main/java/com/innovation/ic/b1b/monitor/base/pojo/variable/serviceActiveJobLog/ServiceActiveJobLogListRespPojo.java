package com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJobLog;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @desc   服务存活日志表查询接口返回的数据
 * @author swq
 * @time   2023年3月29日13:30:09
 */
@Data
public class ServiceActiveJobLogListRespPojo {
    @ApiModelProperty(value = "数据数量", dataType = "Integer")
    private Long count;

    @ApiModelProperty(value = "返回数据", dataType = "String")
    private List<ServiceActiveJobLogListRespData> data;
}
