package com.innovation.ic.b1b.monitor.base.pojo.variable.mysql;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   查询mysql信息接口返回的pojo类
 * @author linuo
 * @time   2023年3月20日10:06:12
 */
@Data
public class MysqlInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "mysqlId", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "mysql名称", dataType = "String")
    private String name;
}