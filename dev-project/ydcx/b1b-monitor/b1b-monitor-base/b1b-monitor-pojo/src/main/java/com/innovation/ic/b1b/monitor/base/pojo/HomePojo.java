package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "HomePojo", description = "首页数据封装pojo类")
public class HomePojo {

    @ApiModelProperty(value = "一小时内不可用接口数据数量", dataType = "Integer")
    private Integer apiNumber;

    @ApiModelProperty(value = "一小时内不可用mysql数据数量", dataType = "Integer")
    private Integer mysqlNumber;

    @ApiModelProperty(value = "一小时内不可用服务性能数据数量", dataType = "Integer")
    private Integer serverNumber;

    @ApiModelProperty(value = "一小时内不可用服务存活任务日志数据数量", dataType = "Integer")
    private Integer serverActiveNumber;

    @ApiModelProperty(value = "一小时内不可用sql server任务日志数量", dataType = "Integer")
    private Integer sqlServerNumber;

    @ApiModelProperty(value = "一小时内不可用 elasticsearch任务日志数量", dataType = "Integer")
    private Integer elasticsearchNumber;

    @ApiModelProperty(value = "一小时内不可用 mongodb任务日志数量", dataType = "Integer")
    private Integer mongodbNumber;

    @ApiModelProperty(value = "一小时内不可用 rabbitmq任务日志数量", dataType = "Integer")
    private Integer rabbitmqNumber;

    @ApiModelProperty(value = "一小时内不可用 redis任务日志数量", dataType = "Integer")
    private Integer redisNumber;

}
