package com.innovation.ic.b1b.monitor.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RabbitmqPojo", description = "RabbitmqPojo")
public class RabbitmqPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "系统id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境id", dataType = "Integer")
    private Integer environmentId;


    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "ip", dataType = "String")
    private String ip;


    @ApiModelProperty(value = "port", dataType = "String")
    private String port;


    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;


    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;


    @ApiModelProperty(value = "虚拟主机", dataType = "String")
    private String virtualHost;


}