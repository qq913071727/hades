package com.innovation.ic.b1b.monitor.base.pojo.variable.mongodb;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   查询mongodb信息接口返回的pojo类
 * @author linuo
 * @time   2023年3月24日16:51:26
 */
@Data
public class MongodbInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "mongodbId", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "mysql名称", dataType = "String")
    private String name;
}