package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   常量类
 * @author linuo
 * @time   2023年4月4日14:05:00
 */
public class Constant {
    /** 顿号魔法值 */
    public static final String PAUSE = "、";

    /** 是否启用字段值 */
    public static final String ENABLE_FIELD = "enable";
}
