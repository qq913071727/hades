package com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 用户登录日志分页显示的pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginLogPagePojo", description = "分页显示Vo")
public class UserLoginLogPagePojo implements Serializable {

    @ApiModelProperty(value = "分页显示的数据", dataType = "List")
    private List<UserLoginLogPojo> userLoginLogPojoList;

    @ApiModelProperty(value = "记录总数", dataType = "Integer")
    private Integer count;
}
