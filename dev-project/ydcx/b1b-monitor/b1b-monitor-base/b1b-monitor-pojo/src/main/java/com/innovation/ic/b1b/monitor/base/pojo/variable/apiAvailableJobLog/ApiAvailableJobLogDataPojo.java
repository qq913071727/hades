package com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJobLog;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 接口任务日志数据
 * @Date 2023/3/30 13:26
 **/
@Data
public class ApiAvailableJobLogDataPojo {

    @ApiModelProperty(value = "接口任务日志id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "系统名称", dataType = "String")
    private String systemName;

    @ApiModelProperty(value = "系统 id", dataType = "Integer")
    private Integer systemId;

    @ApiModelProperty(value = "环境名称", dataType = "String")
    private String environmentName;

    @ApiModelProperty(value = "环境 id", dataType = "Integer")
    private Integer environmentId;

    @ApiModelProperty(value = "服务名称", dataType = "String")
    private String serviceName;

    @ApiModelProperty(value = "服务 id", dataType = "Integer")
    private Integer serviceId;

    @ApiModelProperty(value = "接口名称", dataType = "String")
    private String apiName;

    @ApiModelProperty(value = "接口id", dataType = "Integer")
    private Integer apiId;

    @ApiModelProperty(value = "表示接口是否可以。1表示可用，0表示不可用", dataType = "Integer")
    private Integer available;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "Date")
    private String description;
}
