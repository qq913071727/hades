package com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   服务器性能任务表详情接口返回的pojo类
 * @author linuo
 * @time   2023年3月15日11:02:05
 */
@Data
public class ServerPerformanceJobInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "服务器Id", dataType = "Integer")
    private Integer serverId;

    @ApiModelProperty(value = "服务器名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "任务处理器", dataType = "String")
    private String jobHandler;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date createTime;
}