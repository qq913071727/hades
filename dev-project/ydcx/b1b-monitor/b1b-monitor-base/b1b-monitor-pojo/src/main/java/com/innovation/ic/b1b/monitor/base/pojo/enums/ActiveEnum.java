package com.innovation.ic.b1b.monitor.base.pojo.enums;

/**
 * @desc   存活状态枚举
 * @author linuo
 * @time   2023年3月23日14:58:32
 */
public enum ActiveEnum {
    YES(1, "存活"),
    NO(0, "未存活");

    private Integer code;
    private String desc;

    ActiveEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ActiveEnum of(Integer code) {
        for (ActiveEnum c : ActiveEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (ActiveEnum c : ActiveEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}