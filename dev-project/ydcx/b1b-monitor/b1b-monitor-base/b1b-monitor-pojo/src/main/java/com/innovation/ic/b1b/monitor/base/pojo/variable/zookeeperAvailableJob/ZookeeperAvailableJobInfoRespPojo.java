package com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   zookeeper可用任务表详情接口返回的pojo类
 * @author linuo
 * @time   2023年5月11日14:41:32
 */
@Data
public class ZookeeperAvailableJobInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "zookeeperId", dataType = "Integer")
    private Integer zookeeperId;

    @ApiModelProperty(value = "zookeeper名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "调用表达式", dataType = "String")
    private String scheduleExpression;

    @ApiModelProperty(value = "是否启用(0否、1是)", dataType = "Integer")
    private Integer enable;

    @ApiModelProperty(value = "报警邮箱", dataType = "String")
    private String alarmEmail;

    @ApiModelProperty(value = "报警手机号", dataType = "String")
    private String alarmCellPhone;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date createTime;
}