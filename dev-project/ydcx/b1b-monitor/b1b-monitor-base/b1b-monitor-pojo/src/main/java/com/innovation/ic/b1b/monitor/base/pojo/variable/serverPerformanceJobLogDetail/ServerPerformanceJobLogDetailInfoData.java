package com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLogDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   服务器性能任务日志详情表返回的数据
 * @author linuo
 * @time   2023年3月23日11:31:06
 */
@Data
public class ServerPerformanceJobLogDetailInfoData implements Serializable {
    @ApiModelProperty(value = "文件系统", dataType = "String")
    private String fileSystem;

    @ApiModelProperty(value = "已用内存百分比", dataType = "String")
    private String usedDiskSpacePercentage;

    @ApiModelProperty(value = "挂载点", dataType = "String")
    private String mountPoint;
}