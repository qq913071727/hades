package com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJobLog;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   查询mysql监控任务日志列表接口返回的数据
 * @author linuo
 * @time   2023年3月16日15:01:45
 */
@Data
public class MysqlAvailableJobLogListRespData implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "mysql名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "是否存活。1表示存活，0表示没有存活", dataType = "Integer")
    private Integer active;

    @ApiModelProperty(value = "启动时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date startTime;

    @ApiModelProperty(value = "问题描述", dataType = "Date")
    private String description;
}