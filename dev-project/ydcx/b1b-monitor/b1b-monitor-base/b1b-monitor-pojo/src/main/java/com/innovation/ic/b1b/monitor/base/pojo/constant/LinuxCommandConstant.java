package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   linux命令常量类
 * @author linuo
 * @time   2023年3月22日16:24:24
 */
public class LinuxCommandConstant {
    /** 查看服务器磁盘情况 */
    public static final String CHECK_SERVER_DISK_STATUS = "df -lh";

    /** 查看内存占用情况 */
    public static final String VIEW_MEMORY_USAGE = "free";
}
