package com.innovation.ic.b1b.monitor.base.pojo.constant;

/**
 * @desc   rabbitmq可用任务表的常量类
 * @author linuo
 * @time   2023年3月27日13:19:09
 */
public class RabbitmqAvailableJobConstant {
    /** rabbitmq id */
    public static final String RABBITMQ_ID = "rabbitmq_id";

    /** 主键id */
    public static final String ID = "id";
}
