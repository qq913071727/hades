package com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   查询sql server信息接口返回的pojo类
 * @author linuo
 * @time   2023年3月20日13:14:10
 */
@Data
public class SqlServerInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "sql server Id", dataType = "Long")
    private Long sqlServerId;

    @ApiModelProperty(value = "sql server名称", dataType = "String")
    private String sqlServerName;
}