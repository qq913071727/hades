package com.innovation.ic.b1b.monitor.base.pojo.enums;

/**
 * @desc   不需要分库分表的数据库表配置枚举类
 * @author linuo
 * @time   2023年3月29日11:26:48
 */
public enum NoNeedSubtableEnum {
    API("api", "接口表"),
    API_AVAILABLE_JOB("api_available_job", "接口可用任务表"),
    API_AVAILABLE_JOB_LOG("api_available_job_log", "接口可用任务日志表"),
    API_AVAILABLE_JOB_LOG_HISTORY("api_available_job_log_history", "接口可用任务日志表（历史数据）"),
    CELL_PHONE("cell_phone", "手机号表"),
    ELASTICSEARCH("elasticsearch", "elasticsearch表"),
    ELASTICSEARCH_AVAILABLE_JOB("elasticsearch_available_job", "elasticsearch可用任务表"),
    ELASTICSEARCH_AVAILABLE_JOB_LOG("elasticsearch_available_job_log", "elasticsearch可用任务日志表"),
    ELASTICSEARCH_AVAILABLE_JOB_LOG_HISTORY("elasticsearch_available_job_log_history", "elasticsearch可用任务日志表（历史数据）"),
    EMAIL("email", "邮箱表"),
    ENVIRONMENT("environment", "环境表"),
    MONGODB("mongodb", "mongodb表"),
    MONGODB_AVAILABLE_JOB("mongodb_available_job", "mongodb可用任务表"),
    MONGODB_AVAILABLE_JOB_LOG("mongodb_available_job_log", "elasticsearch可用任务日志表"),
    mongodb_available_job_log_history("mongodb_available_job_log_history", "mongodb可用任务日志表（历史数据）"),
    MYSQL("mysql", "mysql表"),
    MYSQL_AVAILABLE_JOB("mysql_available_job", "mysql可用任务表"),
    MYSQL_AVAILABLE_JOB_LOG("mysql_available_job_log", "mysql可用任务日志表"),
    MYSQL_AVAILABLE_JOB_LOG_HISTORY("mysql_available_job_log_history", "mysql可用任务日志表（历史数据）"),
    RABBITMQ("rabbitmq", "rabbitmq表"),
    RABBITMQ_AVAILABLE_JOB("rabbitmq_available_job", "rabbitmq可用任务表"),
    RABBITMQ_AVAILABLE_JOB_LOG("rabbitmq_available_job_log", "rabbitmq可用任务日志表"),
    RABBITMQ_AVAILABLE_JOB_LOG_HISTORY("rabbitmq_available_job_log_history", "rabbitmq可用任务日志表（历史数据）"),
    REDIS("redis", "redis表"),
    REDIS_AVAILABLE_JOB("redis_available_job", "redis可用任务表"),
    REDIS_AVAILABLE_JOB_LOG("redis_available_job_log", "redis可用任务日志表"),
    REDIS_AVAILABLE_JOB_LOG_HISTORY("redis_available_job_log_history", "redis可用任务日志表（历史数据）"),
    REL_ENVIRONMENT_SERVICE("rel_environment_service", "环境和服务的关系表"),
    REL_SYSTEM_ENVIRONMENT("rel_system_environment", "系统和环境的关系表"),
    SERVER("server", "server表"),
    SERVER_PERFORMANCE_JOB("server_performance_job", "服务器性能任务表"),
    SERVER_PERFORMANCE_JOB_LOG("server_performance_job_log", "服务器性能任务日志表"),
    SERVER_PERFORMANCE_JOB_LOG_DETAIL("server_performance_job_log_detail", "服务器性能任务日志详情表"),
    SERVER_PERFORMANCE_JOB_LOG_HISTORY("server_performance_job_log_history", "服务器性能任务日志表"),
    SERVICE("service", "service表"),
    SERVICE_ACTIVE_JOB("service_active_job", "服务存活任务表"),
    SERVICE_ACTIVE_JOB_LOG("service_active_job_log", "服务存活任务日志表"),
    SERVICE_ACTIVE_JOB_LOG_HISTORY("service_active_job_log_history", "服务存活任务日志表（历史数据）"),
    SQL_SERVER("sql_server", "sql server表"),
    SQL_SERVER_AVAILABLE_JOB("sql_server_available_job", "SqlServer可用任务表"),
    SQL_SERVER_AVAILABLE_JOB_LOG("sql_server_available_job_log", "SqlServer可用任务日志表"),
    SQL_SERVER_AVAILABLE_JOB_LOG_HISTORY("sql_server_available_job_log_history", "sql server可用任务日志表（历史数据）"),
    SYSTEM("system", "系统表"),
    ;

    private String tableName;
    private String desc;

    NoNeedSubtableEnum(String tableName, String desc) {
        this.tableName = tableName;
        this.desc = desc;
    }

    public static NoNeedSubtableEnum of(Integer getTableName) {
        for (NoNeedSubtableEnum c : NoNeedSubtableEnum.values()) {
            if (c.getTableName().equals(getTableName)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer getTableName) {
        for (NoNeedSubtableEnum c : NoNeedSubtableEnum.values()) {
            if (c.getTableName().equals(getTableName)) {
                return c.desc;
            }
        }
        return null;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}