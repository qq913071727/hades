package com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJob;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 接口任务
 * @Date 2023/3/30 13:26
 **/
@Data
public class ApiAvailableJobListPojo implements Serializable {
    @ApiModelProperty(value = "数据总数", dataType = "Long")
    private Long count;

    @ApiModelProperty(value = "返回数据", dataType = "String")
    private List<ApiAvailableJobDataPojo> data;
}