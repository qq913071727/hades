package com.innovation.ic.b1b.monitor.base.pojo.variable.redis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   查询redis信息接口返回的pojo类
 * @author linuo
 * @time   2023年3月28日16:26:26
 */
@Data
public class RedisInfoRespPojo implements Serializable {
    @ApiModelProperty(value = "redisId", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "rabbitmq名称", dataType = "String")
    private String name;
}