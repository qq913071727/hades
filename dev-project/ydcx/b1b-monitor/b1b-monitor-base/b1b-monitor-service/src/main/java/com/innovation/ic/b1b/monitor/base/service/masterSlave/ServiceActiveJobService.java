package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.ServiceActiveJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob.ServiceActiveJobListRespData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob.ServiceActiveJobListRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob.ServiceActiveJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob.ServiceActiveJobListQueryVo;

public interface ServiceActiveJobService {
    /**
     * 查询服务存活任务表数据
     * @param serviceActiveJobListQueryVo 查询服务存活任务表查询列表的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ServiceActiveJobListRespData>>queryList(ServiceActiveJobListQueryVo serviceActiveJobListQueryVo,Boolean isMain);

    /**
     * 获取当前服务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    ServiceResult<ServiceActiveJob> info(Integer id);

    /**
     * 添加服务器监控任务
     * @param serviceActiveJobAddVo 服务存活任务表添加数据的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(ServiceActiveJobAddVo serviceActiveJobAddVo);

    /**
     * 删除当前服务存活数据
     * @param id 主键id
     * @return 返回删除结果
     *
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 执行任务
     * @param id 执行任务id
     * @return 返回处理结果
     */
    ServiceResult<Boolean> executeJob(Integer id);
}
