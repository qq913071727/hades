package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJobLog.MysqlAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJobLog.MysqlAvailableJobLogListQueryVo;
import java.util.Date;

/**
 * @desc   mysql可用任务日志表Service
 * @author linuo
 * @time   2023年3月15日15:05:18
 */
public interface MysqlAvailableJobLogService {
    /**
     * 查询mysql监控任务日志列表
     * @param mysqlAvailableJobLogListQueryVo 查询mysql监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<MysqlAvailableJobLogListRespData>> queryList(MysqlAvailableJobLogListQueryVo mysqlAvailableJobLogListQueryVo);

    /**
     * 查询mysql 1小时内不可用数据量
     * @param date 当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findMysqlClose(Date date, Date behindData);

    void deleteByAvailableJobId(Integer id);
}
