package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobUpdateVo;
import java.util.List;

/**
 * @desc   rabbitmq可用任务表service
 * @author linuo
 * @time   2023年3月27日10:43:36
 */
public interface RabbitmqAvailableJobService {
    /**
     * 添加rabbitmq监控任务
     * @param rabbitmqAvailableJobAddVo 添加rabbitmq监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(RabbitmqAvailableJobAddVo rabbitmqAvailableJobAddVo);

    /**
     * 查询rabbitmq监控任务列表
     * @param rabbitmqAvailableJobListQueryVo 查询rabbitmq监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<RabbitmqAvailableJobListRespData>> queryList(RabbitmqAvailableJobListQueryVo rabbitmqAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除rabbitmq监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取rabbitmq监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<RabbitmqAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新rabbitmq监控任务
     * @param rabbitmqAvailableJobUpdateVo 更新rabbitmq可用任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(RabbitmqAvailableJobUpdateVo rabbitmqAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);

    List<RabbitmqAvailableJob> findByRabbitmqId(Integer rabbitmqId);
}