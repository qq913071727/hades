package com.innovation.ic.b1b.monitor.base.service.sharding;

import com.innovation.ic.b1b.monitor.base.model.FrontEndLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.frontEndLog.FrontEndLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.FrontEndLogVo;
import com.innovation.ic.b1b.monitor.base.vo.InterfaceCallLogVo;

public interface FrontEndLogService {

    /**
     * 添加FrontEndLog类型对象
     * @param frontEndLog
     */
    ServiceResult<Integer> saveFrontEndLog(FrontEndLog frontEndLog);

    /**
     * 分页显示
     * @param frontEndLogVo
     * @return
     */
    ServiceResult<FrontEndLogPagePojo> page(FrontEndLogVo frontEndLogVo);
}
