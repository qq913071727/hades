package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJobLog.ServiceActiveJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJobLog.ServiceActiveJobLogListQueryVo;

import java.util.Date;

/**
 * @author swq
 * @desc 服务存活任务日志表Service
 * @time 2023年3月20日10:05:18
 */
public interface ServiceActiveJobLogService {

    /**
     * 查询服务存活任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findServerClose(Date date, Date behindData);

    /**
     * 查询服务存活日志表数据
     * @param serviceActiveJobLogListQueryVo 查询服务存活日志表查询列表的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ServiceActiveJobLogListRespData>> queryList(ServiceActiveJobLogListQueryVo serviceActiveJobLogListQueryVo);
}
