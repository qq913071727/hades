package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.MysqlAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MysqlAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class MysqlAvailableJobLogHistoryServiceImpl extends ServiceImpl<MysqlAvailableJobLogHistoryMapper, MysqlAvailableJobLogHistory> implements MysqlAvailableJobLogHistoryService {
    @Override
    public void deleteByAvailableJobId(Integer id) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("mysql_available_job_id",id);
        this.baseMapper.deleteByMap(paramMap);
    }
}
