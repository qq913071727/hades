package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJobLog.SqlServerAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJobLog.SqlServerAvailableJobLogListQueryVo;
import java.util.Date;

/**
 * @desc   sql server可用任务日志表Service
 * @author linuo
 * @time   2023年3月15日13:44:33
 */
public interface SqlServerAvailableJobLogService {
    /**
     * 查询sql server可用任务日志
     * @param sqlServerAvailableJobLogListQueryVo 查询sql server可用任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<SqlServerAvailableJobLogListRespData>> queryList(SqlServerAvailableJobLogListQueryVo sqlServerAvailableJobLogListQueryVo);

    /**
     * sql server可用任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findSqlServerClose(Date date, Date behindData);

    void deleteBySqlServerAvailableJobId(Integer sqlServerAvailableJobId);
}
