package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.ElasticsearchAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.model.ElasticsearchAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ElasticsearchAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class ElasticsearchAvailableJobLogHistoryServiceImpl extends ServiceImpl<ElasticsearchAvailableJobLogHistoryMapper, ElasticsearchAvailableJobLogHistory> implements ElasticsearchAvailableJobLogHistoryService {
    @Override
    public void deleteByElasticsearchAvailableJobId(Integer elasticsearchAvailableJobId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("elasticsearch_available_job_id",elasticsearchAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
