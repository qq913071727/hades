package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.MongodbPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodb.MongodbInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.MongodbVo;

import java.util.List;

public interface MongodbService {
    ServiceResult addMongodb(MongodbVo mongodbVo);

    void deleteMongodb(MongodbVo mongodbVo);

    ServiceResult<PageInfo<MongodbPojo>> findByPage(MongodbVo mongodbVo,Boolean mainDb);

    /**
     * 查询mongodb信息列表
     * @return 返回查询列表
     */
    ServiceResult<List<MongodbInfoRespPojo>> queryMongodbInfoList(Boolean mainDb);

    void deleteByEnvironmentId(Integer id);
}