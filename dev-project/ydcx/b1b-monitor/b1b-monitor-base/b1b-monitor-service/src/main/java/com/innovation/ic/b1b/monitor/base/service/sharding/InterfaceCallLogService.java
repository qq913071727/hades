package com.innovation.ic.b1b.monitor.base.service.sharding;

import com.innovation.ic.b1b.monitor.base.model.InterfaceCallLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog.UserLoginLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.InterfaceCallLogVo;
import com.innovation.ic.b1b.monitor.base.vo.UserLoginLogVo;

public interface InterfaceCallLogService {

    /**
     * 添加InterfaceCallLog类型对象
     * @param interfaceCallLog
     */
    ServiceResult<Integer> saveInterfaceCallLog(InterfaceCallLog interfaceCallLog);

    /**
     * 分页显示
     * @param interfaceCallLogVo
     * @return
     */
    ServiceResult<InterfaceCallLogPagePojo> page(InterfaceCallLogVo interfaceCallLogVo);
}
