package com.innovation.ic.b1b.monitor.base.service.sharding.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.PageVisitLogMapper;
import com.innovation.ic.b1b.monitor.base.model.PageVisitLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.pageVisitLog.PageVisitLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.pageVisitLog.PageVisitLogPojo;
import com.innovation.ic.b1b.monitor.base.service.sharding.PageVisitLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.PageVisitLogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class PageVisitLogServiceImpl extends ServiceImpl<PageVisitLogMapper, PageVisitLog> implements PageVisitLogService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加PageVisitLog类型对象
     *
     * @param pageVisitLog
     */
    @Override
    public ServiceResult<Integer> savePageVisitLog(PageVisitLog pageVisitLog) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();

        int result = baseMapper.insert(pageVisitLog);

        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 分页显示
     * @param pageVisitLogVo
     * @return
     */
    @Override
    public ServiceResult<PageVisitLogPagePojo> page(PageVisitLogVo pageVisitLogVo) {
        Integer start = pageVisitLogVo.getPageSize() * (pageVisitLogVo.getPageNo() - 1);

        // 数据
        List<PageVisitLogPojo> pageVisitLogPojoList = serviceHelper.getPageVisitLogMapper().page(pageVisitLogVo.getSystemId(),
                pageVisitLogVo.getEnvironmentId(), pageVisitLogVo.getServiceId(), pageVisitLogVo.getUrl(), pageVisitLogVo.getBeginDate(),
                pageVisitLogVo.getEndDate(), start, pageVisitLogVo.getPageSize());

        // 记录总数
        List<Integer> integerList = serviceHelper.getPageVisitLogMapper().pageCount(pageVisitLogVo.getSystemId(),
                pageVisitLogVo.getEnvironmentId(), pageVisitLogVo.getServiceId(), pageVisitLogVo.getUrl(),
                pageVisitLogVo.getBeginDate(), pageVisitLogVo.getEndDate());
        Integer count = integerList.size();

        PageVisitLogPagePojo pageVisitLogPagePojo = new PageVisitLogPagePojo();
        pageVisitLogPagePojo.setPageVisitLogPojoList(pageVisitLogPojoList);
        pageVisitLogPagePojo.setCount(count);

        return ServiceResult.ok(pageVisitLogPagePojo, ServiceResult.SELECT_SUCCESS);
    }
}
