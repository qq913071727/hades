package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.MysqlAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJobLog.MysqlAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MysqlAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJobLog.MysqlAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   mysql可用任务表Service实现类
 * @author linuo
 * @time   2023年3月15日15:06:41
 */
@Slf4j
@Service
public class MysqlAvailableJobLogServiceImpl extends ServiceImpl<MysqlAvailableJobLogMapper, MysqlAvailableJobLog> implements MysqlAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询mysql监控任务日志列表
     * @param mysqlAvailableJobLogListQueryVo 查询mysql监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<MysqlAvailableJobLogListRespData>> queryList(MysqlAvailableJobLogListQueryVo mysqlAvailableJobLogListQueryVo) {
        PageHelper.startPage(mysqlAvailableJobLogListQueryVo.getPageNo(), mysqlAvailableJobLogListQueryVo.getPageSize());

        // 查询mysql可用任务日志表数据
        List<MysqlAvailableJobLogListRespData> mysqlAvailableJobLogListRespData = serviceHelper.getMysqlAvailableJobLogMapper().queryList(mysqlAvailableJobLogListQueryVo);
        PageInfo<MysqlAvailableJobLogListRespData> pageInfo = new PageInfo<>(mysqlAvailableJobLogListRespData);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 查询mysql 1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findMysqlClose(Date date, Date behindData) {
        Map<Integer, MysqlAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<MysqlAvailableJobLog> mysqlAvailableJobLogQueryWrapper = new QueryWrapper<>();
        mysqlAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        mysqlAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        mysqlAvailableJobLogQueryWrapper.eq("active", Home.STATUS_TYPE);
        List<MysqlAvailableJobLog> mysqlAvailableJobLogs = baseMapper.selectList(mysqlAvailableJobLogQueryWrapper);
        for (MysqlAvailableJobLog mysqlAvailableJobLog : mysqlAvailableJobLogs) {
            if (map.containsKey(mysqlAvailableJobLog.getMysqlAvailableJobId())) {//mysql可用任务id是有重复的
                MysqlAvailableJobLog mapData = map.get(mysqlAvailableJobLog.getMysqlAvailableJobId());//map中数据
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), mysqlAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(mysqlAvailableJobLog.getMysqlAvailableJobId(), mysqlAvailableJobLog);
                    }
                } else {//list 近
                    if (mysqlAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(mysqlAvailableJobLog.getMysqlAvailableJobId(), mysqlAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//服务器性能任务id不重复
                if (mysqlAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(mysqlAvailableJobLog.getMysqlAvailableJobId(), mysqlAvailableJobLog);
                }else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    @Override
    public void deleteByAvailableJobId(Integer id) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("mysql_available_job_id",id);
        this.baseMapper.deleteByMap(paramMap);
    }
}