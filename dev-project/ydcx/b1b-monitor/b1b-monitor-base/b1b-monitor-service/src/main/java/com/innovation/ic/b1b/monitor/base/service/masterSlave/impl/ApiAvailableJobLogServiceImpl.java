package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.ApiAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.ApiAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJobLog.ApiAvailableJobLogDataPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ApiAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJobLog.ApiAvailableJobLogListVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author swq
 * @desc 接口可用任务日志表Service实现类
 * @time 2023年3月20日10:06:41
 */
@Service
@Slf4j
public class ApiAvailableJobLogServiceImpl extends ServiceImpl<ApiAvailableJobLogMapper, ApiAvailableJobLog> implements ApiAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询接口1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findApiClose(Date date, Date behindData) {
        Map<Integer, ApiAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<ApiAvailableJobLog> apiAvailableJobLogQueryWrapper = new QueryWrapper<>();
        apiAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        apiAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        List<ApiAvailableJobLog> apiAvailableJobLogs = baseMapper.selectList(apiAvailableJobLogQueryWrapper);
        for (ApiAvailableJobLog apiAvailableJobLog : apiAvailableJobLogs) {
            if (map.containsKey(apiAvailableJobLog.getApiAvailableJobId())) {//api可用任务id是有重复的
                ApiAvailableJobLog mapData = map.get(apiAvailableJobLog.getApiAvailableJobId());//map中数据
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), apiAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getAvailable() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(apiAvailableJobLog.getApiAvailableJobId(), apiAvailableJobLog);
                    }
                } else {//list 近
                    if (apiAvailableJobLog.getAvailable() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(apiAvailableJobLog.getApiAvailableJobId(), apiAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//服务器性能任务id不重复
                if (apiAvailableJobLog.getAvailable() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(apiAvailableJobLog.getApiAvailableJobId(), apiAvailableJobLog);
                } else {//反之直接跳过
                    continue;
                }

            }
        }
        Integer number = map.size();
        return number;

    }

    /**
     * 根据条件查询日志列表数据
     *
     * @param apiAvailableJobLogListVo
     * @return
     */
    @Override
    public ServiceResult<PageInfo<ApiAvailableJobLogDataPojo>> queryList(ApiAvailableJobLogListVo apiAvailableJobLogListVo) {
        PageHelper.startPage(apiAvailableJobLogListVo.getPageNo(), apiAvailableJobLogListVo.getPageSize());
        // 查询接口可用任务日志列表数据
        List<ApiAvailableJobLogDataPojo> apiAvailableJobLogDataPojos = serviceHelper.getApiAvailableJobLogMapper().queryList(apiAvailableJobLogListVo);

        PageInfo<ApiAvailableJobLogDataPojo> pageInfo = new PageInfo<>(apiAvailableJobLogDataPojos);
        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }


}
