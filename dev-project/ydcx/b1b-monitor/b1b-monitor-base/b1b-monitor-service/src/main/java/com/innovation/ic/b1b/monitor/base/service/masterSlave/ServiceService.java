package com.innovation.ic.b1b.monitor.base.service.masterSlave;


import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.ServicePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.ServiceVo;

import java.util.List;

public interface ServiceService{


    ServiceResult addService(ServiceVo serviceVo);

    void deleteService(ServiceVo serviceVo);


    ServiceResult<PageInfo<ServicePojo>> findByPage(ServiceVo serviceVo,Boolean mainDb);

    ServicePojo findByService(ServiceVo serviceVo);

    void deleteByEnvironmentId(Integer environmentId);


    List<DropDownPojo> dropDown(DropDownVo dropDownVo,Boolean mainDb);
}
