package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.ServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.server.ServerQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.ServerVo;

import java.util.List;

/**
 * @desc   服务器表Service
 * @author linuo
 * @time   2023年3月21日10:47:34
 */
public interface ServerService {
    /**
     * 查询服务器列表
     * @return 返回查询结果
     */
    ServiceResult<List<ServerQueryRespPojo>> queryServerInfoList();

    ServiceResult addServer(ServerVo serviceVo);

    void deleteServer(ServerVo serverVo);

    ServiceResult<PageInfo<ServerPojo>> findByPage(ServerVo serverVo,Boolean mainDb);
}
