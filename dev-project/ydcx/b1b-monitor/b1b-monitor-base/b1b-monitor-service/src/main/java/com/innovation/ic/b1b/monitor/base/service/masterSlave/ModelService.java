package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.ModelPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.ModelVo;

public interface ModelService {
    ServiceResult addModel(ModelVo modelVo);

    void deleteModel(ModelVo modelVo);

    ServiceResult<PageInfo<ModelPojo>> findByPage(ModelVo modelVo, Boolean mainDb);
}
