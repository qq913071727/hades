package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.ElasticsearchPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearch.ElasticsearchQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.ElasticsearchVo;
import java.util.List;

public interface ElasticsearchService {
    ServiceResult addElasticsearch(ElasticsearchVo elasticsearchVo);

    void deleteElasticsearch(ElasticsearchVo elasticsearchVo);

    ServiceResult<PageInfo<ElasticsearchPojo>> findByPage(ElasticsearchVo elasticsearchVo,Boolean mainDb);

    /**
     * 查询es列表
     * @return 返回查询结果
     */
    ServiceResult<List<ElasticsearchQueryRespPojo>> queryServerInfoList(Boolean mainDb);

    void deleteByEnvironmentId(Integer id);
}