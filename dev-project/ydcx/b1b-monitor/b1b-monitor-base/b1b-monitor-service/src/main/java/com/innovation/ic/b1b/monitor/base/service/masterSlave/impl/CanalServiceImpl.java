package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.CanalMapper;
import com.innovation.ic.b1b.monitor.base.model.Canal;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.CanalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @desc   Canal表Service实现类
 * @author linuo
 * @time   2023年5月8日11:19:34
 */
@Slf4j
@Service
public class CanalServiceImpl extends ServiceImpl<CanalMapper, Canal> implements CanalService {
   
}