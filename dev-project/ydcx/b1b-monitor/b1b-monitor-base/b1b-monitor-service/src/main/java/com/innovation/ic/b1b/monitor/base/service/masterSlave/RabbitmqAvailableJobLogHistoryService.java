package com.innovation.ic.b1b.monitor.base.service.masterSlave;

public interface RabbitmqAvailableJobLogHistoryService {
    void deleteByRabbitmqAvailableJobId(Integer rabbitmqAvailableJobId);
}
