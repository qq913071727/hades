package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.handler.SqlServerAvailableJobHandler;
import com.innovation.ic.b1b.monitor.base.mapper.SqlServerAvailableJobMapper;
import com.innovation.ic.b1b.monitor.base.model.SqlServer;
import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.SqlServerAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.SqlServerAvailableJobService;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobUpdateVo;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @desc   sql server可用任务表Service实现类
 * @author linuo
 * @time   2023年3月14日15:37:09
 */
@Service
public class SqlServerAvailableJobServiceImpl extends ServiceImpl<SqlServerAvailableJobMapper, SqlServerAvailableJob> implements SqlServerAvailableJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加sql server监控任务
     * @param sqlServerAvailableJobAddVo sql server可用任务添加的Vo类
     * @return 返回添加结果
     */
    @Override
    public ServiceResult<Boolean> add(SqlServerAvailableJobAddVo sqlServerAvailableJobAddVo) {
        boolean result = Boolean.FALSE;
        String message;

        Boolean aBoolean = judgeIfHaveSameData(null, sqlServerAvailableJobAddVo.getSqlServerId());
        if(aBoolean){
            message = "当前sql server监控任务已存在，请勿重复添加";
        }else{
            // 校验sqlServerId是否存在
            SqlServer sqlServer = serviceHelper.getSqlServerMapper().selectById(sqlServerAvailableJobAddVo.getSqlServerId());
            if(sqlServer != null){
                SqlServerAvailableJob sqlServerAvailableJob = new SqlServerAvailableJob();
                BeanUtils.copyProperties(sqlServerAvailableJobAddVo, sqlServerAvailableJob);
                if(sqlServerAvailableJob.getEnable() == null){
                    sqlServerAvailableJob.setEnable(EnableEnum.NO.getCode());
                }
                sqlServerAvailableJob.setCreateTime(new Date(System.currentTimeMillis()));
                int insert = serviceHelper.getSqlServerAvailableJobMapper().insert(sqlServerAvailableJob);
                if(insert > 0){
                    message = ServiceResult.INSERT_SUCCESS;
                    result = Boolean.TRUE;

                    String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + sqlServerAvailableJob.getId();

                    // 组装任务参数
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(JobDataMapConstant.ID, sqlServerAvailableJob.getId());

                    serviceHelper.getQuartzManager().createScheduleJob(jobId, SqlServerAvailableJobHandler.class.getName(), "SqlServerAvailableJobHandler", jobDataMap, sqlServerAvailableJob.getScheduleExpression());
                }else{
                    message = ServiceResult.INSERT_FAIL;
                }
            }else{
                message = "sqlServer信息不存在，请添加SqlServer配置后重新添加sqlServer监控任务";
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 查询sql server监控任务列表
     * @param sqlServerAvailableJobListQueryVo 查询sql server监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<SqlServerAvailableJobListRespData>> queryList(SqlServerAvailableJobListQueryVo sqlServerAvailableJobListQueryVo, Boolean isMain) {
        PageHelper.startPage(sqlServerAvailableJobListQueryVo.getPageNo(), sqlServerAvailableJobListQueryVo.getPageSize());

        // 查询sql server监控任务列表数据
        List<SqlServerAvailableJobListRespData> data = serviceHelper.getSqlServerAvailableJobMapper().queryList(sqlServerAvailableJobListQueryVo);

        PageInfo<SqlServerAvailableJobListRespData> pageInfo = new PageInfo<>(data);
        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 删除sql server监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> delete(Integer id) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        int i = serviceHelper.getSqlServerAvailableJobMapper().deleteById(id);
        if(i > 0){
            logger.info("已删除id为[{}]sql server监控任务", id);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;

            try {
                // 删除任务
                String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + id;
                serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            }catch (Exception e){
                logger.info("删除sql server监控后台任务失败,原因:", e);
            }
        }else{
            logger.info("删除id为[{}]sql server监控任务失败，数据不存在", id);
        }

        return ServiceResult.ok(result, message);
    }

    /**
     * 获取sql server监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回sql server监控任务详情
     */
    @Override
    public ServiceResult<SqlServerAvailableJobInfoRespPojo> info(Integer id, Boolean isMain) {
        SqlServerAvailableJobInfoRespPojo result = serviceHelper.getSqlServerAvailableJobMapper().info(id);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 更新sql server监控任务
     * @param sqlServerAvailableJobUpdateVo sql server可用任务更新的Vo类
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> update(SqlServerAvailableJobUpdateVo sqlServerAvailableJobUpdateVo) {
        Boolean aBoolean = judgeIfHaveSameData(sqlServerAvailableJobUpdateVo.getId(), sqlServerAvailableJobUpdateVo.getSqlServerId());
        if(aBoolean){
            ServiceResult<Boolean> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setMessage("当前sql server监控任务已存在，请勿重复添加");
            return serviceResult;
        }else{
            // 校验sqlServerId是否存在
            SqlServer sqlServer = serviceHelper.getSqlServerMapper().selectById(sqlServerAvailableJobUpdateVo.getSqlServerId());
            if(sqlServer != null){
                // 查询历史数据
                SqlServerAvailableJob historyData = serviceHelper.getSqlServerAvailableJobMapper().selectById(sqlServerAvailableJobUpdateVo.getId());

                int i = serviceHelper.getSqlServerAvailableJobMapper().updateInfo(sqlServerAvailableJobUpdateVo);
                if(i > 0){
                    logger.info("更新id为[{}]sql server监控任务成功", sqlServerAvailableJobUpdateVo.getId());

                    Integer enable = sqlServerAvailableJobUpdateVo.getEnable();
                    // 原来是启用现在关闭时需要暂停后台任务
                    if(historyData.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && enable.intValue() == EnableEnum.NO.getCode().intValue()){
                        String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + sqlServerAvailableJobUpdateVo.getId();
                        logger.info("暂停后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                    }else if(historyData.getEnable().intValue() == EnableEnum.NO.getCode().intValue() && enable.intValue() == EnableEnum.YES.getCode().intValue()) {
                        // 原来是关闭现在启用任务时需要恢复后台任务
                        String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + sqlServerAvailableJobUpdateVo.getId();
                        logger.info("恢复后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().resumeScheduleJob(jobId);
                    }else{
                        String scheduleExpression = sqlServerAvailableJobUpdateVo.getScheduleExpression();
                        if(sqlServerAvailableJobUpdateVo.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && !historyData.getScheduleExpression().equals(scheduleExpression)){
                            String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + sqlServerAvailableJobUpdateVo.getId();
                            serviceHelper.getQuartzManager().updateScheduleJob(jobId, scheduleExpression);
                        }
                    }

                    return ServiceResult.ok(Boolean.TRUE, ServiceResult.UPDATE_SUCCESS);
                }else{
                    logger.info("更新id为[{}]sql server监控任务失败", sqlServerAvailableJobUpdateVo.getId());
                    return ServiceResult.ok(Boolean.FALSE, ServiceResult.UPDATE_FAIL);
                }
            }else{
                logger.info("sqlServerId=[{}]的信息不存在", sqlServerAvailableJobUpdateVo.getSqlServerId());
                return ServiceResult.error(ServiceResult.UPDATE_FAIL);
            }
        }
    }

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    @Override
    public ServiceResult<Boolean> executeJob(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        try {
            String jobId = TableNameConstant.SQL_SERVER_AVAILABLE_JOB + "_" + id;
            serviceHelper.getQuartzManager().runOnce(jobId);
        }catch (Exception e){
            serviceResult.setMessage(ServiceResult.OPERATE_FAIL);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 判断是否有相同数据
     * @param id 主键id
     * @param sqlServerId sqlServerId
     * @return 返回判断结果
     */
    private Boolean judgeIfHaveSameData(Integer id, Integer sqlServerId){
        Boolean result = Boolean.FALSE;
        QueryWrapper<SqlServerAvailableJob> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SqlServerAvailableJobConstant.SQL_SERVER_ID, sqlServerId);
        if(id != null){
            queryWrapper.ne(SqlServerAvailableJobConstant.ID, id);
        }
        Integer selectCount = serviceHelper.getSqlServerAvailableJobMapper().selectCount(queryWrapper);
        if(selectCount > 0){
            result = Boolean.TRUE;
        }

        return result;
    }
}