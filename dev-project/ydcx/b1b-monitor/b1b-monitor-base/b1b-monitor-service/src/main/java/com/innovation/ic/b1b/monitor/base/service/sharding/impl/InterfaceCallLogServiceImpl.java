package com.innovation.ic.b1b.monitor.base.service.sharding.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.InterfaceCallLogMapper;
import com.innovation.ic.b1b.monitor.base.model.InterfaceCallLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPojo;
import com.innovation.ic.b1b.monitor.base.service.sharding.InterfaceCallLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.InterfaceCallLogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class InterfaceCallLogServiceImpl extends ServiceImpl<InterfaceCallLogMapper, InterfaceCallLog> implements InterfaceCallLogService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加InterfaceCallLog类型对象
     * @param interfaceCallLog
     */
    @Override
    public ServiceResult<Integer> saveInterfaceCallLog(InterfaceCallLog interfaceCallLog) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();

        int result = baseMapper.insert(interfaceCallLog);

        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 分页显示
     * @param interfaceCallLogVo
     * @return
     */
    @Override
    public ServiceResult<InterfaceCallLogPagePojo>  page(InterfaceCallLogVo interfaceCallLogVo) {
        Integer start = interfaceCallLogVo.getPageSize() * (interfaceCallLogVo.getPageNo() - 1);

        // 数据
        List<InterfaceCallLogPojo> interfaceCallLogPojoList = serviceHelper.getInterfaceCallLogMapper().page(interfaceCallLogVo.getSystemId(),
                interfaceCallLogVo.getEnvironmentId(), interfaceCallLogVo.getServiceId(), interfaceCallLogVo.getUrl(), interfaceCallLogVo.getBeginDate(),
                interfaceCallLogVo.getEndDate(), start, interfaceCallLogVo.getPageSize());

        // 记录总数
        List<Integer> integerList = serviceHelper.getInterfaceCallLogMapper().pageCount(interfaceCallLogVo.getSystemId(),
                interfaceCallLogVo.getEnvironmentId(), interfaceCallLogVo.getServiceId(), interfaceCallLogVo.getUrl(),
                interfaceCallLogVo.getBeginDate(), interfaceCallLogVo.getEndDate());
        Integer count = integerList.size();

        InterfaceCallLogPagePojo interfaceCallLogPagePojo = new InterfaceCallLogPagePojo();
        interfaceCallLogPagePojo.setInterfaceCallLogPojoList(interfaceCallLogPojoList);
        interfaceCallLogPagePojo.setCount(count);

        return ServiceResult.ok(interfaceCallLogPagePojo, ServiceResult.SELECT_SUCCESS);
    }
}
