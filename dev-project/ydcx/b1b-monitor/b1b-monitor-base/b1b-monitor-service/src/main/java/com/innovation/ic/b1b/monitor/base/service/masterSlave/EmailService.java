package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.Email;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.email.AlarmEmailQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.EmailVo;
import java.util.List;

public interface EmailService {
    ServiceResult  addEmail(EmailVo emailVo);

    void deleteEmail(EmailVo emailVo);

    Email findById(EmailVo emailVo,Boolean mainDb);

    ServiceResult<PageInfo<Email>> findByPage(EmailVo emailVo,Boolean mainDb);

    /**
     * 查询报警邮箱列表
     * @return 返回查询结果
     */
    ServiceResult<List<AlarmEmailQueryRespPojo>> queryAlarmEmailList(Boolean mainDb);
}