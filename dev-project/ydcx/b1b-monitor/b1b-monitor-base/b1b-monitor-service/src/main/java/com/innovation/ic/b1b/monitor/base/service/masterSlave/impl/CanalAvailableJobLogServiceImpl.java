package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.CanalAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.CanalAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJobLog.CanalAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.CanalAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJobLog.CanalAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Canal可用任务日志表Service实现类
 * @author linuo
 * @time   2023年5月12日10:03:48
 */
@Slf4j
@Service
public class CanalAvailableJobLogServiceImpl extends ServiceImpl<CanalAvailableJobLogMapper, CanalAvailableJobLog> implements CanalAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询canal监控任务日志列表
     * @param canalAvailableJobLogListQueryVo 查询canal监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<CanalAvailableJobLogListRespData>> queryList(CanalAvailableJobLogListQueryVo canalAvailableJobLogListQueryVo) {
        PageHelper.startPage(canalAvailableJobLogListQueryVo.getPageNo(), canalAvailableJobLogListQueryVo.getPageSize());

        // 查询elasticsearch可用任务日志列表数据
        List<CanalAvailableJobLogListRespData> list = serviceHelper.getCanalAvailableJobLogMapper().queryList(canalAvailableJobLogListQueryVo);
        PageInfo<CanalAvailableJobLogListRespData> pageInfo = new PageInfo<>(list);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }
}