package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.KafkaAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.KafkaAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJobLog.KafkaAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.KafkaAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJobLog.KafkaAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Kafka可用任务日志表Service实现类
 * @author linuo
 * @time   2023年5月8日11:24:32
 */
@Slf4j
@Service
public class KafkaAvailableJobLogServiceImpl extends ServiceImpl<KafkaAvailableJobLogMapper, KafkaAvailableJobLog> implements KafkaAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询kafka监控任务日志列表
     * @param kafkaAvailableJobLogListQueryVo 查询kafka监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<KafkaAvailableJobLogListRespData>> queryList(KafkaAvailableJobLogListQueryVo kafkaAvailableJobLogListQueryVo) {
        PageHelper.startPage(kafkaAvailableJobLogListQueryVo.getPageNo(), kafkaAvailableJobLogListQueryVo.getPageSize());

        // 查询kafka可用任务日志表数据
        List<KafkaAvailableJobLogListRespData> data = serviceHelper.getKafkaAvailableJobLogMapper().queryList(kafkaAvailableJobLogListQueryVo);
        PageInfo<KafkaAvailableJobLogListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }
}
