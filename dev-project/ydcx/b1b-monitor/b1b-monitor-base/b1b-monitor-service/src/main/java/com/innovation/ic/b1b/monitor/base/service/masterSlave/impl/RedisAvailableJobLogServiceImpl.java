package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.RedisAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJobLog.RedisAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RedisAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJobLog.RedisAvailableJobLogListQueryVo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RedisAvailableJobLogServiceImpl extends ServiceImpl<RedisAvailableJobLogMapper, RedisAvailableJobLog> implements RedisAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * redis 可用任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findRedisClose(Date date, Date behindData) {
        Map<Integer, RedisAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<RedisAvailableJobLog> redisAvailableJobLogQueryWrapper = new QueryWrapper<>();
        redisAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        redisAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        List<RedisAvailableJobLog> redisAvailableJobLogs = baseMapper.selectList(redisAvailableJobLogQueryWrapper);//当前一小时内所有的数据
        for (RedisAvailableJobLog redisAvailableJobLog : redisAvailableJobLogs) {
            if (map.containsKey(redisAvailableJobLog.getRedisAvailableJobId())) {//elasticsearch可用任务id是有重复的
                RedisAvailableJobLog mapData = map.get(redisAvailableJobLog.getRedisAvailableJobId());
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), redisAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(redisAvailableJobLog.getRedisAvailableJobId(), redisAvailableJobLog);
                    }
                } else {//list 近
                    if (redisAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(redisAvailableJobLog.getRedisAvailableJobId(), redisAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//不重复
                if (redisAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(redisAvailableJobLog.getRedisAvailableJobId(), redisAvailableJobLog);
                } else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    /**
     * 查询redis监控任务日志列表
     * @param redisAvailableJobLogListQueryVo 查询redis监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<RedisAvailableJobLogListRespData>> queryList(RedisAvailableJobLogListQueryVo redisAvailableJobLogListQueryVo) {
        PageHelper.startPage(redisAvailableJobLogListQueryVo.getPageNo(), redisAvailableJobLogListQueryVo.getPageSize());

        // 查询redis可用任务日志表数据
        List<RedisAvailableJobLogListRespData> data = serviceHelper.getRedisAvailableJobLogMapper().queryList(redisAvailableJobLogListQueryVo);
        PageInfo<RedisAvailableJobLogListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    @Override
    public void deleteByRedisAvailableJobId(Integer redisAvailableJobId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("redis_available_job_id", redisAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}