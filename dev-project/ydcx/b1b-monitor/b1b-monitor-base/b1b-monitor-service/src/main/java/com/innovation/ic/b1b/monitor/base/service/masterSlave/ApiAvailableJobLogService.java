package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJobLog.ApiAvailableJobLogDataPojo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJobLog.ApiAvailableJobLogListVo;

import java.util.Date;

/**
 * @author swq
 * @desc 接口可用任务日志表Service
 * @time 2023年3月20日10:05:18
 */
public interface ApiAvailableJobLogService {
    /**
     * 查询接口1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findApiClose(Date date, Date behindData);

    /**
     * 根据条件查询日志列表数据
     *
     * @param apiAvailableJobLogListVo
     * @return
     */
    ServiceResult<PageInfo<ApiAvailableJobLogDataPojo>> queryList(ApiAvailableJobLogListVo apiAvailableJobLogListVo);
}
