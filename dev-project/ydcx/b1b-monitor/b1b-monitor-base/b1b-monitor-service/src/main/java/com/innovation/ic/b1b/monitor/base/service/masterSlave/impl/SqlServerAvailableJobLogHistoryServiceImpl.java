package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.SqlServerAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.SqlServerAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class SqlServerAvailableJobLogHistoryServiceImpl extends ServiceImpl<SqlServerAvailableJobLogHistoryMapper, SqlServerAvailableJobLogHistory> implements SqlServerAvailableJobLogHistoryService {
    @Override
    public void deleteBySqlServerAvailableJobId(Integer sqlServerAvailableJobId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("sql_server_available_job_id",sqlServerAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
