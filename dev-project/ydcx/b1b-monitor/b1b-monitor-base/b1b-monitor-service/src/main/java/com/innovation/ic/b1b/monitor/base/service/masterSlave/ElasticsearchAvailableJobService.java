package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.ElasticsearchAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJob.ElasticsearchAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJob.ElasticsearchAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobUpdateVo;
import java.util.List;

/**
 * @desc   elasticsearch可用任务表Service
 * @author linuo
 * @time   2023年3月24日09:43:42
 */
public interface ElasticsearchAvailableJobService {

    /**
     * 添加elasticsearch监控任务
     * @param elasticsearchAvailableJobAddVo 添加Elasticsearch可用任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(ElasticsearchAvailableJobAddVo elasticsearchAvailableJobAddVo);

    /**
     * 查询elasticsearch监控任务列表
     * @param elasticsearchAvailableJobListQueryVo 查询Elasticsearch监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ElasticsearchAvailableJobListRespData>> queryList(ElasticsearchAvailableJobListQueryVo elasticsearchAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除elasticsearch监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取elasticsearch监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<ElasticsearchAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新elasticsearch监控任务
     * @param elasticsearchAvailableJobUpdateVo 更新Elasticsearch可用任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(ElasticsearchAvailableJobUpdateVo elasticsearchAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);

    List<ElasticsearchAvailableJob> findByElasticsearchId(Integer elasticsearchId);
}
