package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.handler.RedisAvailableJobHandler;
import com.innovation.ic.b1b.monitor.base.mapper.RedisAvailableJobMapper;
import com.innovation.ic.b1b.monitor.base.model.Redis;
import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.RedisAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJob.RedisAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJob.RedisAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RedisAvailableJobService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobUpdateVo;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RedisAvailableJobServiceImpl extends ServiceImpl<RedisAvailableJobMapper, RedisAvailableJob> implements RedisAvailableJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加redis监控任务
     * @param redisAvailableJobAddVo 添加redis监控任务的Vo类
     * @return 返回添加结果
     */
    @Override
    public ServiceResult<Boolean> add(RedisAvailableJobAddVo redisAvailableJobAddVo) {
        boolean result = Boolean.FALSE;
        String message;

        Boolean aBoolean = judgeIfHaveSameData(null, redisAvailableJobAddVo.getRedisId());
        if(aBoolean){
            message = "当前redis监控任务已存在，请勿重复添加";
            logger.info(message);
        }else{
            // 校验redis是否存在
            Redis redis = serviceHelper.getRedisMapper().selectById(redisAvailableJobAddVo.getRedisId());
            if(redis != null){
                RedisAvailableJob redisAvailableJob = new RedisAvailableJob();
                BeanUtils.copyProperties(redisAvailableJobAddVo, redisAvailableJob);
                if(redisAvailableJob.getEnable() == null){
                    redisAvailableJob.setEnable(EnableEnum.NO.getCode());
                }
                redisAvailableJob.setCreateTime(new Date(System.currentTimeMillis()));
                int insert = serviceHelper.getRedisAvailableJobMapper().insert(redisAvailableJob);
                if(insert > 0){
                    message = ServiceResult.INSERT_SUCCESS;
                    result = Boolean.TRUE;

                    String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + redisAvailableJob.getId();

                    // 组装任务参数
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(JobDataMapConstant.ID, redisAvailableJob.getId());

                    serviceHelper.getQuartzManager().createScheduleJob(jobId, RedisAvailableJobHandler.class.getName(), "RedisAvailableJobHandler", jobDataMap, redisAvailableJob.getScheduleExpression());
                }else{
                    message = ServiceResult.INSERT_FAIL;
                }
            }else{
                message = "redis信息不存在，请配置redis信息后重新添加redis监控任务";
                logger.info(message);
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 查询redis监控任务列表
     * @param redisAvailableJobListQueryVo 查询redis监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<RedisAvailableJobListRespData>> queryList(RedisAvailableJobListQueryVo redisAvailableJobListQueryVo, Boolean isMain) {
        PageHelper.startPage(redisAvailableJobListQueryVo.getPageNo(), redisAvailableJobListQueryVo.getPageSize());

        // 查询redis可用任务列表数据
        List<RedisAvailableJobListRespData> data = serviceHelper.getRedisAvailableJobMapper().queryList(redisAvailableJobListQueryVo);
        PageInfo<RedisAvailableJobListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 删除redis监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> delete(Integer id) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        int i = serviceHelper.getRedisAvailableJobMapper().deleteById(id);
        if(i > 0){
            logger.info("已删除id为[{}]redis监控任务", id);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;

            try {
                // 删除任务
                String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + id;
                serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            }catch (Exception e){
                logger.info("删除redis监控后台任务失败,原因:", e);
            }
        }else{
            logger.info("删除id为[{}]redis监控任务失败，数据不存在", id);
        }

        return ServiceResult.ok(result, message);
    }

    /**
     * 获取redis监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<RedisAvailableJobInfoRespPojo> info(Integer id, Boolean isMain) {
        RedisAvailableJobInfoRespPojo result = serviceHelper.getRedisAvailableJobMapper().info(id);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 更新redis监控任务
     * @param redisAvailableJobUpdateVo 更新redis可用任务的Vo类
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> update(RedisAvailableJobUpdateVo redisAvailableJobUpdateVo) {
        Boolean aBoolean = judgeIfHaveSameData(redisAvailableJobUpdateVo.getId(), redisAvailableJobUpdateVo.getRedisId());
        if(aBoolean){
            ServiceResult<Boolean> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setMessage("当前redis监控任务已存在，请勿重复添加");
            return serviceResult;
        }else{
            // 校验redis是否存在
            Redis redis = serviceHelper.getRedisMapper().selectById(redisAvailableJobUpdateVo.getRedisId());
            if(redis != null){
                RedisAvailableJob historyData = serviceHelper.getRedisAvailableJobMapper().selectById(redisAvailableJobUpdateVo.getId());

                int i = serviceHelper.getRedisAvailableJobMapper().updateInfo(redisAvailableJobUpdateVo);
                if(i > 0){
                    logger.info("更新id为[{}]redis监控任务成功", redisAvailableJobUpdateVo.getId());

                    Integer enable = redisAvailableJobUpdateVo.getEnable();
                    // 原来是启用现在关闭时需要暂停后台任务
                    if(historyData.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && enable.intValue() == EnableEnum.NO.getCode().intValue()){
                        String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + redisAvailableJobUpdateVo.getId();
                        logger.info("暂停后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                    }else if(historyData.getEnable().intValue() == EnableEnum.NO.getCode().intValue() && enable.intValue() == EnableEnum.YES.getCode().intValue()) {
                        // 原来是关闭现在启用任务时需要恢复后台任务
                        String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + redisAvailableJobUpdateVo.getId();
                        logger.info("恢复后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().resumeScheduleJob(jobId);
                    }else{
                        String scheduleExpression = redisAvailableJobUpdateVo.getScheduleExpression();
                        if(redisAvailableJobUpdateVo.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && !historyData.getScheduleExpression().equals(scheduleExpression)){
                            String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + redisAvailableJobUpdateVo.getId();
                            serviceHelper.getQuartzManager().updateScheduleJob(jobId, scheduleExpression);
                        }
                    }

                    return ServiceResult.ok(Boolean.TRUE, ServiceResult.UPDATE_SUCCESS);
                }else{
                    logger.info("更新id为[{}]redis监控任务失败", redisAvailableJobUpdateVo.getId());
                    return ServiceResult.ok(Boolean.FALSE, ServiceResult.UPDATE_FAIL);
                }
            }else{
                logger.info("redisId=[{}]的信息不存在", redisAvailableJobUpdateVo.getRedisId());
                return ServiceResult.error(ServiceResult.UPDATE_FAIL);
            }
        }
    }

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    @Override
    public ServiceResult<Boolean> executeJob(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        try {
            String jobId = TableNameConstant.REDIS_AVAILABLE_JOB + "_" + id;
            serviceHelper.getQuartzManager().runOnce(jobId);
        }catch (Exception e){
            serviceResult.setMessage(ServiceResult.OPERATE_FAIL);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public List<RedisAvailableJob> findByRedisId(Integer redisId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("redis_id", redisId);
        return this.baseMapper.selectByMap(paramMap);
    }

    /**
     * 判断是否有相同数据
     * @param id 主键id
     * @param redisId redisId
     * @return 返回判断结果
     */
    private Boolean judgeIfHaveSameData(Integer id, Integer redisId){
        Boolean result = Boolean.FALSE;

        QueryWrapper<RedisAvailableJob> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(RedisAvailableJobConstant.REDIS_ID, redisId);
        if(id != null){
            queryWrapper.ne(RedisAvailableJobConstant.ID, id);
        }
        Integer selectCount = serviceHelper.getRedisAvailableJobMapper().selectCount(queryWrapper);
        if(selectCount > 0){
            result = Boolean.TRUE;
        }

        return result;
    }
}
