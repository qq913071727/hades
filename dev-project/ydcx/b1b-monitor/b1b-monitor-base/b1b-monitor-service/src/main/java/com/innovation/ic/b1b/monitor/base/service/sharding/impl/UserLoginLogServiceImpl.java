package com.innovation.ic.b1b.monitor.base.service.sharding.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.UserLoginLogMapper;
import com.innovation.ic.b1b.monitor.base.model.UserLoginLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog.UserLoginLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog.UserLoginLogPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.service.sharding.UserLoginLogService;
import com.innovation.ic.b1b.monitor.base.vo.UserLoginLogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements UserLoginLogService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加UserLoginLog类型对象
     * @param userLoginLog
     */
    @Override
    public ServiceResult<Integer> saveUserLoginLog(UserLoginLog userLoginLog) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();

        int result = baseMapper.insert(userLoginLog);

        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 分页显示
     * @param userLoginLogVo
     * @return
     */
    @Override
    public ServiceResult<UserLoginLogPagePojo> page(UserLoginLogVo userLoginLogVo) {
        Integer start = userLoginLogVo.getPageSize() * (userLoginLogVo.getPageNo() - 1);

        // 数据：这个地方原本是一个sql，但是由于使用了jdbc-sharding，其中count(distinct)无法使用，因此把原本一个sql拆为两个sql
        List<UserLoginLogPojo> userLoginLogPojoList = serviceHelper.getUserLoginLogMapper().page_phrase1(userLoginLogVo.getSystemId(),
                userLoginLogVo.getEnvironmentId(), userLoginLogVo.getServiceId(), userLoginLogVo.getBeginDate(),
                userLoginLogVo.getEndDate(), start, userLoginLogVo.getPageSize());
//        List<UserLoginLogPojo> loginUsernameCountList = serviceHelper.getUserLoginLogMapper().page_phrase2(userLoginLogVo.getSystemId(),
//                userLoginLogVo.getEnvironmentId(), userLoginLogVo.getServiceId(), userLoginLogVo.getBeginDate(),
//                userLoginLogVo.getEndDate(), start, userLoginLogVo.getPageSize());
//        if (userLoginLogPojoList.size() > 0 && loginUsernameCountList.size() > 0){
//            for (int i=0; i<userLoginLogPojoList.size(); i++){
//                userLoginLogPojoList.get(i).setLoginUsernameCount(loginUsernameCountList.get(i).getLoginUsernameCount());
//            }
//        }

        // 记录总数
        List<Integer> integerList = serviceHelper.getUserLoginLogMapper().pageCount(userLoginLogVo.getSystemId(),
                userLoginLogVo.getEnvironmentId(), userLoginLogVo.getServiceId(),
                userLoginLogVo.getBeginDate(), userLoginLogVo.getEndDate());
        Integer count = integerList.size();

        UserLoginLogPagePojo userLoginLogPagePojo = new UserLoginLogPagePojo();
        userLoginLogPagePojo.setUserLoginLogPojoList(userLoginLogPojoList);
        userLoginLogPagePojo.setCount(count);

        return ServiceResult.ok(userLoginLogPagePojo, ServiceResult.SELECT_SUCCESS);
    }
}
