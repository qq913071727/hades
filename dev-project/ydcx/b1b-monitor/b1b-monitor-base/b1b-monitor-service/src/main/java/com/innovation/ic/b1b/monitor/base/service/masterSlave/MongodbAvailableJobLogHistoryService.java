package com.innovation.ic.b1b.monitor.base.service.masterSlave;

public interface MongodbAvailableJobLogHistoryService {

    void deleteByMongodbAvailableJobId(Integer mongodbAvailableJobId);
}