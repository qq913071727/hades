package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.RelSystemEnvironmentMapper;
import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironment;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RelSystemEnvironmentService;
import com.innovation.ic.b1b.monitor.base.vo.EnvironmentVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class RelSystemEnvironmentImpl extends ServiceImpl<RelSystemEnvironmentMapper, RelSystemEnvironment> implements RelSystemEnvironmentService {
    @Override
    public boolean save(EnvironmentVo environmentVo) {
        QueryWrapper<RelSystemEnvironment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("system_id", environmentVo.getSystemId());
        queryWrapper.eq("environment_id", environmentVo.getEnvId());
        //查重
        if (environmentVo.getId() == null) {
            List<RelSystemEnvironment> relSystemEnvironmentList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(relSystemEnvironmentList)) {
                return true;
            }
            RelSystemEnvironment relSystemEnvironment = new RelSystemEnvironment();
            relSystemEnvironment.setSystemId(environmentVo.getSystemId());
            relSystemEnvironment.setEnvironmentId(environmentVo.getEnvId());
            this.baseMapper.insert(relSystemEnvironment);
        } else {
            queryWrapper.ne("id", environmentVo.getAssociationId());
            List<RelSystemEnvironment> relSystemEnvironmentList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(relSystemEnvironmentList)) {
                return true;
            }
            RelSystemEnvironment relSystemEnvironment = new RelSystemEnvironment();
            relSystemEnvironment.setSystemId(environmentVo.getSystemId());
            relSystemEnvironment.setEnvironmentId(environmentVo.getEnvId());
            relSystemEnvironment.setId(environmentVo.getAssociationId());
            this.baseMapper.updateById(relSystemEnvironment);
        }
        return false;
    }

    @Override
    public void deleteByEnvironmentId(Integer id) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("environment_id", id);
        this.baseMapper.deleteByMap(paramMap);
    }

    @Override
    public List<Integer> findByEnvId(Integer id) {
        return this.baseMapper.findByEnvId(id);
    }

    @Override
    public List<Integer> findByServiceId(Integer id) {
        return this.baseMapper.findByServiceId(id);
    }

    @Override
    public List<Integer> findBySystemId(Integer systemId) {
        return this.baseMapper.findBySystemId(systemId);
    }

    @Override
    public RelSystemEnvironment findBysystemIdAndEnvironmentId(Integer systemId, Integer environmentId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("system_id", systemId);
        paramMap.put("environment_id", environmentId);
        List<RelSystemEnvironment> relSystemEnvironmentList = this.baseMapper.selectByMap(paramMap);
        if(CollectionUtils.isEmpty(relSystemEnvironmentList)){
            return null;
        }
        return relSystemEnvironmentList.get(0);
    }


}
