package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobVo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobPojo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
public interface LogMonitorJobService {

    ServiceResult<PageInfo<LogMonitorJobPojo>> pageInfo(int page, int rows, LogMonitorJobVo vo);

    ServiceResult<List<LogMonitorJobPojo>> list(LogMonitorJobVo vo);

    ServiceResult<LogMonitorJobPojo> get(String id);

    void save(LogMonitorJobVo vo);

    void update(LogMonitorJobVo vo);

    void delete(String[] ids);
}