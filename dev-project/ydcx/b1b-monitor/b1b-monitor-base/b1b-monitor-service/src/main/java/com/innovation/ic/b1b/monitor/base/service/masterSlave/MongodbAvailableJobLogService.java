package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJobLog.MongodbAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJobLog.MongodbAvailableJobLogListQueryVo;
import java.util.Date;

public interface MongodbAvailableJobLogService {
    /**
     * mongodb可用任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findMongodbClose(Date date, Date behindData);

    /**
     * 查询mysql监控任务日志列表
     * @param mongodbAvailableJobLogListQueryVo 查询mongodb监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<MongodbAvailableJobLogListRespData>> queryList(MongodbAvailableJobLogListQueryVo mongodbAvailableJobLogListQueryVo);

    void deleteByMongodbAvailableJobId(Integer mongodbAvailableJobId);
}