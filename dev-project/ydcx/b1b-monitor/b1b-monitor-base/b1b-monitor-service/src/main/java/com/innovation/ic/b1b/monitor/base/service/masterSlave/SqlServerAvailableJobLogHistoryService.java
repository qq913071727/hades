package com.innovation.ic.b1b.monitor.base.service.masterSlave;

public interface SqlServerAvailableJobLogHistoryService {

    void deleteBySqlServerAvailableJobId(Integer sqlServerAvailableJobId);
}
