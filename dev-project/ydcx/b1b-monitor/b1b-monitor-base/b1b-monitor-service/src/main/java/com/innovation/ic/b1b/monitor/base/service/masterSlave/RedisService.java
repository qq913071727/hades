package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.RedisPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redis.RedisInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.RedisVo;
import java.util.List;

public interface RedisService {


    ServiceResult addRedis(RedisVo redisVo);

    void deleteRedis(RedisVo redisVo);

    ServiceResult<PageInfo<RedisPojo>> findByPage(RedisVo redisVo,Boolean mainDb);

    /**
     * 查询redis信息列表
     * @return 返回查询结果
     */
    ServiceResult<List<RedisInfoRespPojo>> queryRedisList(Boolean mainDb);

    void deleteByEnvironmentId(Integer id);
}