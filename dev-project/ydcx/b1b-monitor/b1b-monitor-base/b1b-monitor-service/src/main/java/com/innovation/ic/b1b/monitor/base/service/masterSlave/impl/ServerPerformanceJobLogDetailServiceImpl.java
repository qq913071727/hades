package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.ServerPerformanceJobLogDetailMapper;
import com.innovation.ic.b1b.monitor.base.model.ServerPerformanceJobLogDetail;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServerPerformanceJobLogDetailService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @desc   服务器性能任务日志详情表Service实现类
 * @author linuo
 * @time   2023年3月22日17:40:07
 */
@Slf4j
@Service
public class ServerPerformanceJobLogDetailServiceImpl extends ServiceImpl<ServerPerformanceJobLogDetailMapper, ServerPerformanceJobLogDetail> implements ServerPerformanceJobLogDetailService {
    @Resource
    private ServiceHelper serviceHelper;

}