package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.LogPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.LogVo;

public interface LogService {

    ServiceResult addLog(LogVo logVo);


    void deleteLog(LogVo logVo);

    ServiceResult<PageInfo<LogPojo>> findByPage(LogVo logVo, Boolean mainDb);
}
