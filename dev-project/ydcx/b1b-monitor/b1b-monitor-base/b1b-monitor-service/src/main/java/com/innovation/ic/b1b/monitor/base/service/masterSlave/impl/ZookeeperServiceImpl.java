package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.ZookeeperMapper;
import com.innovation.ic.b1b.monitor.base.model.Zookeeper;
import com.innovation.ic.b1b.monitor.base.pojo.ZookeeperPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ZookeeperService;
import com.innovation.ic.b1b.monitor.base.vo.ZookeeperVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author linuo
 * @desc Zookeeper表Service实现类
 * @time 2023年5月11日10:11:20
 */
@Slf4j
@Service
public class ZookeeperServiceImpl extends ServiceImpl<ZookeeperMapper, Zookeeper> implements ZookeeperService {

    @Resource
    private EnvironmentService environmentService;

    @Override
    public ServiceResult addZookeeper(ZookeeperVo zookeeperVo) {
        ServiceResult serviceResult = new ServiceResult();
        QueryWrapper<Zookeeper> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", zookeeperVo.getName());
        queryWrapper.eq("system_id", zookeeperVo.getSystemId());
        queryWrapper.eq("environment_id", zookeeperVo.getEnvironmentId());
        Integer id = zookeeperVo.getId();
        Zookeeper zookeeper = new Zookeeper();
        BeanUtil.copyProperties(zookeeperVo, zookeeper);
        if (id == null) {
            List<Zookeeper> zklList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(zklList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Zookeeper> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", zookeeperVo.getName());
            List<Zookeeper> zklCheckList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(zklCheckList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }
            this.baseMapper.insert(zookeeper);
        } else {
            queryWrapper.ne("id", id);
            List<Zookeeper> zklList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(zklList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Zookeeper> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", zookeeperVo.getName());
            queryWrapperName.ne("id", id);
            List<Zookeeper> zklCheckList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(zklCheckList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }

            this.baseMapper.updateById(zookeeper);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteLog(ZookeeperVo zookeeperVo) {
        this.baseMapper.deleteById(zookeeperVo.getId());


    }

    @Override
    public ServiceResult<PageInfo<ZookeeperPojo>> findByPage(ZookeeperVo zookeeperVo, Boolean mainDb) {
        ServiceResult<PageInfo<ZookeeperPojo>> serviceResult = new ServiceResult<>();
        Integer environmentId = zookeeperVo.getEnvironmentId();
        if (environmentId != null) {
            List<Integer> environmentIds = environmentService.findByName(environmentId);
            if (!CollectionUtils.isEmpty(environmentIds)) {
                zookeeperVo.setEnvironmentIds(environmentIds);
            }
        }
        PageHelper.startPage(zookeeperVo.getPageNo(), zookeeperVo.getPageSize());
        List<ZookeeperPojo> zookeeperPojoList = this.baseMapper.findByPage(zookeeperVo);
        PageInfo<ZookeeperPojo> pageInfo = new PageInfo<>(zookeeperPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }
}