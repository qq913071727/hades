package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.MongodbAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJobLog.MongodbAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MongodbAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJobLog.MongodbAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class MongodbAvailableJobLogServiceImpl extends ServiceImpl<MongodbAvailableJobLogMapper, MongodbAvailableJobLog> implements MongodbAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * mongodb可用任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findMongodbClose(Date date, Date behindData) {
        Map<Integer, MongodbAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<MongodbAvailableJobLog> mongodbAvailableJobLogQueryWrapper = new QueryWrapper<>();
        mongodbAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        mongodbAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        List<MongodbAvailableJobLog> mongodbAvailableJobLogs = baseMapper.selectList(mongodbAvailableJobLogQueryWrapper);//当前一小时内所有的数据
        for (MongodbAvailableJobLog mongodbAvailableJobLog : mongodbAvailableJobLogs) {
            if (map.containsKey(mongodbAvailableJobLog.getMongodbAvailableJobId())) {//elasticsearch可用任务id是有重复的
                MongodbAvailableJobLog mapData = map.get(mongodbAvailableJobLog.getMongodbAvailableJobId());
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), mongodbAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(mongodbAvailableJobLog.getMongodbAvailableJobId(), mongodbAvailableJobLog);
                    }
                } else {//list 近
                    if (mongodbAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(mongodbAvailableJobLog.getMongodbAvailableJobId(), mongodbAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//不重复
                if (mongodbAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(mongodbAvailableJobLog.getMongodbAvailableJobId(), mongodbAvailableJobLog);
                } else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    /**
     * 查询mysql监控任务日志列表
     * @param mongodbAvailableJobLogListQueryVo 查询mongodb监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<MongodbAvailableJobLogListRespData>> queryList(MongodbAvailableJobLogListQueryVo mongodbAvailableJobLogListQueryVo) {
        PageHelper.startPage(mongodbAvailableJobLogListQueryVo.getPageNo(), mongodbAvailableJobLogListQueryVo.getPageSize());

        // 查询mongodb可用任务日志表数据
        List<MongodbAvailableJobLogListRespData> data = serviceHelper.getMongodbAvailableJobLogMapper().queryList(mongodbAvailableJobLogListQueryVo);
        PageInfo<MongodbAvailableJobLogListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    @Override
    public void deleteByMongodbAvailableJobId(Integer mongodbAvailableJobId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mongodb_available_job_id", mongodbAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
