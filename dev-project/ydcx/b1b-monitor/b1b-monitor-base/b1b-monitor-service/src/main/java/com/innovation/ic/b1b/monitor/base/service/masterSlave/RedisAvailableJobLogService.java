package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJobLog.RedisAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJobLog.RedisAvailableJobLogListQueryVo;
import java.util.Date;

public interface RedisAvailableJobLogService {
    /**
     * redis 可用任务日志表1小时内不可用数据量
     * @param date 当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findRedisClose(Date date, Date behindData);

    /**
     * 查询redis监控任务日志列表
     * @param redisAvailableJobLogListQueryVo 查询redis监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<RedisAvailableJobLogListRespData>> queryList(RedisAvailableJobLogListQueryVo redisAvailableJobLogListQueryVo);

    void deleteByRedisAvailableJobId(Integer redisAvailableJobId);
}