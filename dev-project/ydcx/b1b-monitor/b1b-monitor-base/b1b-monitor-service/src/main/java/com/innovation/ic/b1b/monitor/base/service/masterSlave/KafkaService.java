package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.KafkaPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.KafkaVo;

/**
 * @desc   Kafka表Service
 * @author linuo
 * @time   2023年5月8日11:17:51
 */
public interface KafkaService {

    ServiceResult addKafka(KafkaVo kafkaVo);

    void deleteKafka(KafkaVo kafkaVo);

    ServiceResult<PageInfo<KafkaPojo>> findByPage(KafkaVo kafkaVo, Boolean mainDb);
}