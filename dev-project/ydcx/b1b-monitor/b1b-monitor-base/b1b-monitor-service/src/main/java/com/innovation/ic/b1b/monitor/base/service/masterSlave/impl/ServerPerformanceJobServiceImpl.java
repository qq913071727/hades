package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.handler.ServerPerformanceJobHandler;
import com.innovation.ic.b1b.monitor.base.mapper.ServerPerformanceJobMapper;
import com.innovation.ic.b1b.monitor.base.model.Server;
import com.innovation.ic.b1b.monitor.base.model.ServerPerformanceJob;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.ServerPerformanceJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServerPerformanceJobService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobUpdateVo;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @desc   服务器性能任务表Service实现类
 * @author linuo
 * @time   2023年3月9日17:02:48
 */
@Slf4j
@Service
public class ServerPerformanceJobServiceImpl extends ServiceImpl<ServerPerformanceJobMapper, ServerPerformanceJob> implements ServerPerformanceJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询服务器性能任务列表数据
     * @param serverPerformanceJobListQueryVo 服务器监控任务查询列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<ServerPerformanceJobListRespData>> queryList(ServerPerformanceJobListQueryVo serverPerformanceJobListQueryVo, Boolean isMain) {
        PageHelper.startPage(serverPerformanceJobListQueryVo.getPageNo(), serverPerformanceJobListQueryVo.getPageSize());

        // 查询服务器性能任务列表数据
        List<ServerPerformanceJobListRespData> data = serviceHelper.getServerPerformanceJobMapper().queryList(serverPerformanceJobListQueryVo);

        PageInfo<ServerPerformanceJobListRespData> pageInfo = new PageInfo<>(data);
        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 添加服务器监控任务
     * @param serverPerformanceJobAddVo 添加服务器监控任务的Vo类
     * @return 返回添加结果
     */
    @Override
    public ServiceResult<Boolean> add(ServerPerformanceJobAddVo serverPerformanceJobAddVo) {
        boolean result = Boolean.FALSE;
        String message;

        Boolean aBoolean = judgeIfHaveSameData(null, serverPerformanceJobAddVo.getServerId());
        if(aBoolean){
            message = "当前服务器监控任务已存在，请勿重复添加";
        }else{
            // 校验服务器id是否存在
            Server server = serviceHelper.getServerMapper().selectById(serverPerformanceJobAddVo.getServerId());
            if (server != null) {
                ServerPerformanceJob serverPerformanceJob = new ServerPerformanceJob();
                BeanUtils.copyProperties(serverPerformanceJobAddVo, serverPerformanceJob);
                if (serverPerformanceJob.getEnable() == null) {
                    serverPerformanceJob.setEnable(EnableEnum.NO.getCode());
                }
                serverPerformanceJob.setCreateTime(new Date(System.currentTimeMillis()));
                int insert = serviceHelper.getServerPerformanceJobMapper().insert(serverPerformanceJob);
                if (insert > 0) {
                    message = ServiceResult.INSERT_SUCCESS;
                    result = Boolean.TRUE;

                    String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + serverPerformanceJob.getId();

                    // 组装任务参数
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(JobDataMapConstant.ID, serverPerformanceJob.getId());

                    serviceHelper.getQuartzManager().createScheduleJob(jobId, ServerPerformanceJobHandler.class.getName(), "ServerPerformanceJobHandler", jobDataMap, serverPerformanceJob.getScheduleExpression());
                } else {
                    message = ServiceResult.INSERT_FAIL;
                }
            } else {
                message = "服务器信息不存在，请配置服务器信息后重新添加服务器监控任务";
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 执行任务
     * @param id 执行任务id
     * @return 返回处理结果
     */
    @Override
    public ServiceResult<Boolean> executeJob(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        try {
            String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + id;
            serviceHelper.getQuartzManager().runOnce(jobId);
        }catch (Exception e){
            serviceResult.setMessage(ServiceResult.OPERATE_FAIL);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查看服务器监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<ServerPerformanceJobInfoRespPojo> info(Integer id, Boolean isMain) {
        ServerPerformanceJobInfoRespPojo result = serviceHelper.getServerPerformanceJobMapper().info(id);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 删除服务器监控任务
     *
     * @param id 主键id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> delete(Integer id) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        int i = serviceHelper.getServerPerformanceJobMapper().deleteById(id);
        if (i > 0) {
            logger.info("已删除id为[{}]服务器监控任务", id);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;

            try {
                // 删除任务
                String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + id;
                serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            }catch (Exception e){
                log.info("删除服务器监控后台任务失败,原因:", e);
            }
        } else {
            logger.info("删除id为[{}]服务器监控任务失败，数据不存在", id);
        }

        return ServiceResult.ok(result, message);
    }

    /**
     * 更新服务器监控任务
     * @param serverPerformanceJobUpdateVo 更新服务器监控任务的Vo类
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> update(ServerPerformanceJobUpdateVo serverPerformanceJobUpdateVo) {
        Boolean aBoolean = judgeIfHaveSameData(serverPerformanceJobUpdateVo.getId(), serverPerformanceJobUpdateVo.getServerId());
        if(aBoolean){
            ServiceResult<Boolean> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setMessage("当前服务器监控任务已存在，请勿重复添加");
            return serviceResult;
        }else{
            // 校验服务器id是否存在
            Server server = serviceHelper.getServerMapper().selectById(serverPerformanceJobUpdateVo.getServerId());
            if (server != null) {
                // 查询历史数据
                ServerPerformanceJob historyData = serviceHelper.getServerPerformanceJobMapper().selectById(serverPerformanceJobUpdateVo.getId());

                int i = serviceHelper.getServerPerformanceJobMapper().updateInfo(serverPerformanceJobUpdateVo);
                if (i > 0) {
                    logger.info("更新id为[{}]服务器监控任务成功", serverPerformanceJobUpdateVo.getId());

                    Integer enable = serverPerformanceJobUpdateVo.getEnable();
                    // 原来是启用现在关闭时需要暂停后台任务
                    if(historyData.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && enable.intValue() == EnableEnum.NO.getCode().intValue()){
                        String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + serverPerformanceJobUpdateVo.getId();
                        log.info("暂停后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                    }else if(historyData.getEnable().intValue() == EnableEnum.NO.getCode().intValue() && enable.intValue() == EnableEnum.YES.getCode().intValue()) {
                        // 原来是关闭现在启用任务时需要恢复后台任务
                        String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + serverPerformanceJobUpdateVo.getId();
                        log.info("恢复后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().resumeScheduleJob(jobId);
                    }else{
                        String scheduleExpression = serverPerformanceJobUpdateVo.getScheduleExpression();
                        if(serverPerformanceJobUpdateVo.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && !historyData.getScheduleExpression().equals(scheduleExpression)){
                            String jobId = TableNameConstant.SERVER_PERFORMANCE_JOB + "_" + serverPerformanceJobUpdateVo.getId();
                            serviceHelper.getQuartzManager().updateScheduleJob(jobId, scheduleExpression);
                        }
                    }

                    return ServiceResult.ok(Boolean.TRUE, ServiceResult.UPDATE_SUCCESS);
                } else {
                    logger.info("更新id为[{}]服务器监控任务失败", serverPerformanceJobUpdateVo.getId());
                    return ServiceResult.ok(Boolean.FALSE, ServiceResult.UPDATE_FAIL);
                }
            } else {
                log.info("服务器id=[{}]的信息不存在", serverPerformanceJobUpdateVo.getServerId());
                return ServiceResult.error(ServiceResult.UPDATE_FAIL);
            }
        }
    }

    /**
     * 判断是否有相同数据
     * @param id 主键id
     * @param serverId 服务器id
     * @return 返回判断结果
     */
    private Boolean judgeIfHaveSameData(Integer id, Integer serverId){
        Boolean result = Boolean.FALSE;

        QueryWrapper<ServerPerformanceJob> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(ServerPerformanceJobConstant.SERVER_ID, serverId);
        if(id != null){
            queryWrapper.ne(ServerPerformanceJobConstant.ID, id);
        }
        Integer selectCount = serviceHelper.getServerPerformanceJobMapper().selectCount(queryWrapper);
        if(selectCount > 0){
            result = Boolean.TRUE;
        }

        return result;
    }
}