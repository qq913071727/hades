package com.innovation.ic.b1b.monitor.base.service.masterSlave;


import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.ApiPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.ApiVo;

public interface ApiService{


    ServiceResult addApi(ApiVo apiVo);

    void deleteApi(ApiVo apiVo);

    ApiPojo findById(ApiVo apiVo,Boolean mainDb);

    ServiceResult<PageInfo<ApiPojo>> findByPage(ApiVo apiVo,Boolean mainDb);

    void deleteServiceId(Integer serviceId);
}
