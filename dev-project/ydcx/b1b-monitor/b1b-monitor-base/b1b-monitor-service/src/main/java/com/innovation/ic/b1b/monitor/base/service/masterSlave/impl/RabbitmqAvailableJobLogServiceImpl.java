package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.RabbitmqAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RabbitmqAvailableJobLogServiceImpl extends ServiceImpl<RabbitmqAvailableJobLogMapper, RabbitmqAvailableJobLog> implements RabbitmqAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * rabbitma 可用任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findRabbitmqClose(Date date, Date behindData) {
        Map<Integer, RabbitmqAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<RabbitmqAvailableJobLog> rabbitmqAvailableJobLogQueryWrapper = new QueryWrapper<>();
        rabbitmqAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        rabbitmqAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        List<RabbitmqAvailableJobLog> rabbitmqAvailableJobLogs = baseMapper.selectList(rabbitmqAvailableJobLogQueryWrapper);//当前一小时内所有的数据
        for (RabbitmqAvailableJobLog rabbitmqAvailableJobLog : rabbitmqAvailableJobLogs) {
            if (map.containsKey(rabbitmqAvailableJobLog.getRabbitmqAvailableJobId())) {//elasticsearch可用任务id是有重复的
                RabbitmqAvailableJobLog mapData = map.get(rabbitmqAvailableJobLog.getRabbitmqAvailableJobId());
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), rabbitmqAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(rabbitmqAvailableJobLog.getRabbitmqAvailableJobId(), rabbitmqAvailableJobLog);
                    }
                } else {//list 近
                    if (rabbitmqAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(rabbitmqAvailableJobLog.getRabbitmqAvailableJobId(), rabbitmqAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//不重复
                if (rabbitmqAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(rabbitmqAvailableJobLog.getRabbitmqAvailableJobId(), rabbitmqAvailableJobLog);
                } else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    /**
     * 查询rabbitmq监控任务日志列表
     * @param rabbitmqAvailableJobLogListQueryVo 查询rabbitmq监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<RabbitmqAvailableJobLogListRespData>> queryList(RabbitmqAvailableJobLogListQueryVo rabbitmqAvailableJobLogListQueryVo) {
        PageHelper.startPage(rabbitmqAvailableJobLogListQueryVo.getPageNo(), rabbitmqAvailableJobLogListQueryVo.getPageSize());

        // 查询rabbitmq可用任务日志表数据
        List<RabbitmqAvailableJobLogListRespData> data = serviceHelper.getRabbitmqAvailableJobLogMapper().queryList(rabbitmqAvailableJobLogListQueryVo);
        PageInfo<RabbitmqAvailableJobLogListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    @Override
    public void deleteByRabbitmqAvailableJobId(Integer rabbitmqAvailableJobId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("rabbitmq_available_job_id", rabbitmqAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
