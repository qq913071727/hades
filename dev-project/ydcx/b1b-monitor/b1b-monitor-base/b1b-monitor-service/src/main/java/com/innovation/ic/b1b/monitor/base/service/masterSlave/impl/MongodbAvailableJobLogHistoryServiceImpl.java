package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.MongodbAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MongodbAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class MongodbAvailableJobLogHistoryServiceImpl extends ServiceImpl<MongodbAvailableJobLogHistoryMapper, MongodbAvailableJobLogHistory> implements MongodbAvailableJobLogHistoryService {
    @Override
    public void deleteByMongodbAvailableJobId(Integer mongodbAvailableJobId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("mongodb_available_job_id", mongodbAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
