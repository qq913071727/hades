package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListQueryVo;

/**
 * @desc   Zookeeper可用任务日志表Service
 * @author linuo
 * @time   2023年5月11日15:13:25
 */
public interface ZookeeperAvailableJobLogService {
    /**
     * 查询zookeeper监控任务日志列表
     * @param zookeeperAvailableJobLogListQueryVo 查询zookeeper监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ZookeeperAvailableJobLogListRespData>> queryList(ZookeeperAvailableJobLogListQueryVo zookeeperAvailableJobLogListQueryVo);
}