package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJob.CanalAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJob.CanalAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJob.CanalAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJob.CanalAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJob.CanalAvailableJobUpdateVo;

/**
 * @desc   canal可用任务表Service
 * @author linuo
 * @time   2023年5月12日09:47:27
 */
public interface CanalAvailableJobService {
    /**
     * 添加canal监控任务
     * @param canalAvailableJobAddVo 添加canal监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(CanalAvailableJobAddVo canalAvailableJobAddVo);

    /**
     * 查询canal监控任务列表
     * @param canalAvailableJobListQueryVo 查询canal监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<CanalAvailableJobListRespData>> queryList(CanalAvailableJobListQueryVo canalAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除canal监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取canal监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<CanalAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新canal监控任务
     * @param canalAvailableJobUpdateVo 更新canal监控任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(CanalAvailableJobUpdateVo canalAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);
}