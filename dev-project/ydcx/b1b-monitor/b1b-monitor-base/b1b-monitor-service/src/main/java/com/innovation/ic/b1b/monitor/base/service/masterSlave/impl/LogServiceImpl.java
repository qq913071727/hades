package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.LogMapper;
import com.innovation.ic.b1b.monitor.base.model.Log;
import com.innovation.ic.b1b.monitor.base.pojo.LogPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.LogService;
import com.innovation.ic.b1b.monitor.base.vo.LogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {

    @Resource
    private EnvironmentService environmentService;

    @Override
    public ServiceResult addLog(LogVo logVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = logVo.getId();
        Log log = new Log();
        BeanUtil.copyProperties(logVo, log);
        QueryWrapper<Log> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("system_id", logVo.getServiceId());
        queryWrapper.eq("environment_id", logVo.getEnvironmentId());
        queryWrapper.eq("service_id", logVo.getServiceId());
        if (id == null) {
            List<Log> logList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(logList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Log> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", logVo.getName());
            List<Log> KafkaNameList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(KafkaNameList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }

            this.baseMapper.insert(log);
        } else {
            queryWrapper.ne("id", id);
            List<Log> logList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(logList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Log> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", logVo.getName());
            queryWrapperName.ne("id", id);
            List<Log> KafkaNameList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(KafkaNameList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }
            this.baseMapper.updateById(log);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;


    }

    @Override
    public void deleteLog(LogVo logVo) {
        this.baseMapper.deleteById(logVo.getId());
    }

    @Override
    public ServiceResult<PageInfo<LogPojo>> findByPage(LogVo logVo, Boolean mainDb) {
        ServiceResult<PageInfo<LogPojo>> serviceResult = new ServiceResult<>();
        Integer environmentId = logVo.getEnvironmentId();
        if (environmentId != null) {
            List<Integer> environmentIds = environmentService.findByName(environmentId);
            if (!CollectionUtils.isEmpty(environmentIds)) {
                logVo.setEnvironmentIds(environmentIds);
            }
        }
        PageHelper.startPage(logVo.getPageNo(), logVo.getPageSize());
        List<LogPojo> logPojoList = this.baseMapper.findByPage(logVo);
        PageInfo<LogPojo> pageInfo = new PageInfo<>(logPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

}
