package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.RabbitmqAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class RabbitmqAvailableJobLogHistoryServiceImpl extends ServiceImpl<RabbitmqAvailableJobLogHistoryMapper, RabbitmqAvailableJobLogHistory> implements RabbitmqAvailableJobLogHistoryService {
    @Override
    public void deleteByRabbitmqAvailableJobId(Integer rabbitmqAvailableJobId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("rabbitmq_available_job_id", rabbitmqAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
