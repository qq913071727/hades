package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.ServiceMapper;
import com.innovation.ic.b1b.monitor.base.model.RelEnvironmentService;
import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironment;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.ServicePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ApiService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RelEnvironmentServiceService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RelSystemEnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceService;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.ServiceVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ServiceServiceImpl extends ServiceImpl<ServiceMapper, com.innovation.ic.b1b.monitor.base.model.Service> implements ServiceService {

    @Resource
    private RelEnvironmentServiceService relEnvironmentServiceService;

    @Resource
    private RelSystemEnvironmentService relSystemEnvironmentService;

    @Resource
    private ApiService apiService;


    @Resource
    private EnvironmentService environmentService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServiceResult addService(ServiceVo serviceVo) {
        ServiceResult serviceResult = new ServiceResult();
        RelSystemEnvironment relSystemEnvironment = relSystemEnvironmentService.findBysystemIdAndEnvironmentId(serviceVo.getSystemId(),serviceVo.getEnvironmentId());
        if(relSystemEnvironment == null){
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("选择的系统或者环境不存在");
            return serviceResult;
        }
        com.innovation.ic.b1b.monitor.base.model.Service service = new com.innovation.ic.b1b.monitor.base.model.Service();
        service.setName(serviceVo.getName());
        service.setId(serviceVo.getId());
        Integer id = serviceVo.getId();
        Map<String, Object> paramMap = new HashMap<>();
        //验重
        paramMap.put("systemId", serviceVo.getSystemId());
        paramMap.put("environmentId", serviceVo.getEnvironmentId());
        paramMap.put("name", serviceVo.getName());
        if (id == null) {
            Integer count = this.baseMapper.checkRepeat(paramMap);
            if (count > 0) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字重复");
                return serviceResult;
            }
            this.baseMapper.insert(service);
            RelEnvironmentService relEnvironmentService = new RelEnvironmentService();
            relEnvironmentService.setRelSystemEnvironmentId(relSystemEnvironment.getId());
            relEnvironmentService.setServiceId(service.getId());
            relEnvironmentServiceService.add(relEnvironmentService);
        } else {
            //验重
            paramMap.put("id", serviceVo.getReId());
            Integer count = this.baseMapper.checkRepeat(paramMap);
            if (count > 0) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字重复");
                return serviceResult;
            }
            if (serviceVo.getReId() == null) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("reId不能为空");
                return serviceResult;
            }
            this.baseMapper.updateById(service);
            RelEnvironmentService relEnvironmentService = new RelEnvironmentService();
            relEnvironmentService.setId(serviceVo.getReId());
            relEnvironmentService.setRelSystemEnvironmentId(relSystemEnvironment.getId());
            relEnvironmentService.setServiceId(service.getId());
            relEnvironmentServiceService.updateRelEnvironmentService(relEnvironmentService);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteService(ServiceVo serviceVo) {
        Integer id = serviceVo.getId();
        this.baseMapper.deleteById(id);
        relEnvironmentServiceService.deleteByServiceId(id);
        //删除服务的同时删除接口
        apiService.deleteServiceId(id);
    }


    @Override
    public ServiceResult<PageInfo<ServicePojo>> findByPage(ServiceVo serviceVo,Boolean mainDb) {
        ServiceResult<PageInfo<ServicePojo>> serviceResult = new ServiceResult<>();
        //当环境id不为空时
        Integer environmentId = serviceVo.getEnvironmentId();
        if (environmentId != null) {
            List<Integer> environmentIds = environmentService.findByName(environmentId);
            if (!CollectionUtils.isEmpty(environmentIds)) {
                serviceVo.setEnvironmentIds(environmentIds);
            }
        }
        PageHelper.startPage(serviceVo.getPageNo(), serviceVo.getPageSize());
        List<ServicePojo> environmentPojoList = this.baseMapper.findByPage(serviceVo);
        PageInfo<ServicePojo> pageInfo = new PageInfo<>(environmentPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServicePojo findByService(ServiceVo serviceVo) {
        return this.baseMapper.findByService(serviceVo.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteByEnvironmentId(Integer environmentId) {
        if (environmentId == null) {
            return;
        }

        List<Integer> serviceIdList = relEnvironmentServiceService.findByEnvironmentId(environmentId);
        if (CollectionUtils.isEmpty(serviceIdList)) {
            return;
        }
        for (Integer serviceId : serviceIdList) {
            ServiceVo serviceVo = new ServiceVo();
            serviceVo.setId(serviceId);
            this.deleteService(serviceVo);
        }
    }

    @Override
    public List<DropDownPojo> dropDown(DropDownVo dropDownVo,Boolean mainDb) {
        Integer type = dropDownVo.getType();
        List<com.innovation.ic.b1b.monitor.base.model.Service> list = new ArrayList<>();
        List<DropDownPojo> resultList = new ArrayList<>();
        List<Integer> systemIds = null;
        QueryWrapper<com.innovation.ic.b1b.monitor.base.model.Service> queryWrapper = new QueryWrapper<>();
        if (type == null) {
            //查询所有
            list = this.baseMapper.selectList(queryWrapper);
        }
        if (type != null && type == 1) {
            //找到系统下所有环境
            List<Integer> envIds = relSystemEnvironmentService.findBySystemId(dropDownVo.getId());
            if(CollectionUtils.isEmpty(envIds)){
                return resultList;
            }
            list = this.baseMapper.findByEnvIds(envIds);
        }
        if (type != null && type == 2) {
            //查询当前环境下所有的接口
            List<Integer> envIds = environmentService.findByName(dropDownVo.getId());
            if(CollectionUtils.isEmpty(envIds)){
                return resultList;
            }
            list = this.baseMapper.findByEnvIds(envIds);
        }
        for (com.innovation.ic.b1b.monitor.base.model.Service service : list) {
            DropDownPojo dropDownPojo = new DropDownPojo();
            BeanUtil.copyProperties(service, dropDownPojo);
            resultList.add(dropDownPojo);
        }
        return resultList;
    }


}
