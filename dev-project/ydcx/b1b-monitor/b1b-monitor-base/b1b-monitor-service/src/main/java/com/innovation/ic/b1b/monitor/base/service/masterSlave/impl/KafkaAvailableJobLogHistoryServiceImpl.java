package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.KafkaAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.mapper.KafkaMapper;
import com.innovation.ic.b1b.monitor.base.model.KafkaAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.KafkaAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @desc   kafka可用任务历史日志表Service实现类
 * @author linuo
 * @time   2023年5月8日11:29:18
 */
@Slf4j
@Service
public class KafkaAvailableJobLogHistoryServiceImpl extends ServiceImpl<KafkaAvailableJobLogHistoryMapper, KafkaAvailableJobLogHistory> implements KafkaAvailableJobLogHistoryService {
    @Resource
    private KafkaAvailableJobLogHistoryMapper kafkaAvailableJobLogHistoryMapper;

}
