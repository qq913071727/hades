package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.RelSystemEnvironmentServiceModelMapper;
import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironmentServiceModel;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RelSystemEnvironmentServiceModelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RelSystemEnvironmentServiceModelServiceImpl extends ServiceImpl<RelSystemEnvironmentServiceModelMapper, RelSystemEnvironmentServiceModel> implements RelSystemEnvironmentServiceModelService {
    @Override
    public void insert(RelSystemEnvironmentServiceModel relSystemEnvironmentServiceModel) {
        this.baseMapper.insert(relSystemEnvironmentServiceModel);
    }

    @Override
    public void update(RelSystemEnvironmentServiceModel relSystemEnvironmentServiceModel) {
        this.baseMapper.updateById(relSystemEnvironmentServiceModel);
    }

    @Override
    public void deleteById(Integer relId) {
        this.baseMapper.deleteById(relId);
    }
}
