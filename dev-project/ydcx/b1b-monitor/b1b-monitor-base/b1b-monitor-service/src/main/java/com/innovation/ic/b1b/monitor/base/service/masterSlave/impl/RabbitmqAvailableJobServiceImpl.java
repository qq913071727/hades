package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.handler.RabbitmqAvailableJobHandler;
import com.innovation.ic.b1b.monitor.base.mapper.RabbitmqAvailableJobMapper;
import com.innovation.ic.b1b.monitor.base.model.Rabbitmq;
import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.MysqlAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.RabbitmqAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqAvailableJobService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobUpdateVo;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   rabbitmq可用任务表service实现类
 * @author linuo
 * @time   2023年3月27日10:43:36
 */
@Service
public class RabbitmqAvailableJobServiceImpl extends ServiceImpl<RabbitmqAvailableJobMapper, RabbitmqAvailableJob> implements RabbitmqAvailableJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加rabbitmq监控任务
     * @param rabbitmqAvailableJobAddVo 添加rabbitmq监控任务的Vo类
     * @return 返回添加结果
     */
    @Override
    public ServiceResult<Boolean> add(RabbitmqAvailableJobAddVo rabbitmqAvailableJobAddVo) {
        boolean result = Boolean.FALSE;
        String message;

        Boolean aBoolean = judgeIfHaveSameData(null, rabbitmqAvailableJobAddVo.getRabbitmqId());
        if(aBoolean){
            message = "当前rabbitmq监控任务已存在，请勿重复添加";
            logger.info(message);
        }else{
            // 校验rabbitmq是否存在
            Rabbitmq rabbitmq = serviceHelper.getRabbitmqMapper().selectById(rabbitmqAvailableJobAddVo.getRabbitmqId());
            if(rabbitmq != null){
                RabbitmqAvailableJob rabbitmqAvailableJob = new RabbitmqAvailableJob();
                BeanUtils.copyProperties(rabbitmqAvailableJobAddVo, rabbitmqAvailableJob);
                if(rabbitmqAvailableJob.getEnable() == null){
                    rabbitmqAvailableJob.setEnable(EnableEnum.NO.getCode());
                }
                rabbitmqAvailableJob.setCreateTime(new Date(System.currentTimeMillis()));
                int insert = serviceHelper.getRabbitmqAvailableJobMapper().insert(rabbitmqAvailableJob);
                if(insert > 0){
                    message = ServiceResult.INSERT_SUCCESS;
                    result = Boolean.TRUE;

                    String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + rabbitmqAvailableJob.getId();

                    // 组装任务参数
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(JobDataMapConstant.ID, rabbitmqAvailableJob.getId());

                    serviceHelper.getQuartzManager().createScheduleJob(jobId, RabbitmqAvailableJobHandler.class.getName(), "RabbitmqAvailableJobHandler", jobDataMap, rabbitmqAvailableJob.getScheduleExpression());
                }else{
                    message = ServiceResult.INSERT_FAIL;
                }
            }else{
                message = "rabbitmq信息不存在，请配置rabbitmq信息后重新添加rabbitmq监控任务";
                logger.info(message);
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 查询rabbitmq监控任务列表
     * @param rabbitmqAvailableJobListQueryVo 查询rabbitmq监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<RabbitmqAvailableJobListRespData>> queryList(RabbitmqAvailableJobListQueryVo rabbitmqAvailableJobListQueryVo, Boolean isMain) {
        PageHelper.startPage(rabbitmqAvailableJobListQueryVo.getPageNo(), rabbitmqAvailableJobListQueryVo.getPageSize());

        // 查询rabbitmq可用任务列表数据
        List<RabbitmqAvailableJobListRespData> data = serviceHelper.getRabbitmqAvailableJobMapper().queryList(rabbitmqAvailableJobListQueryVo);
        PageInfo<RabbitmqAvailableJobListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 删除rabbitmq监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> delete(Integer id) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        int i = serviceHelper.getRabbitmqAvailableJobMapper().deleteById(id);
        if(i > 0){
            logger.info("已删除id为[{}]rabbitmq监控任务", id);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;

            try {
                // 删除任务
                String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + id;
                serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            }catch (Exception e){
                logger.info("删除rabbitmq监控后台任务失败,原因:", e);
            }
        }else{
            logger.info("删除id为[{}]rabbitmq监控任务失败，数据不存在", id);
        }

        return ServiceResult.ok(result, message);
    }

    /**
     * 获取rabbitmq监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<RabbitmqAvailableJobInfoRespPojo> info(Integer id, Boolean isMain) {
        RabbitmqAvailableJobInfoRespPojo result = serviceHelper.getRabbitmqAvailableJobMapper().info(id);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 更新rabbitmq监控任务
     * @param rabbitmqAvailableJobUpdateVo 更新rabbitmq可用任务的Vo类
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> update(RabbitmqAvailableJobUpdateVo rabbitmqAvailableJobUpdateVo) {
        Boolean aBoolean = judgeIfHaveSameData(rabbitmqAvailableJobUpdateVo.getId(), rabbitmqAvailableJobUpdateVo.getRabbitmqId());
        if(aBoolean){
            ServiceResult<Boolean> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setMessage("当前mysql监控任务已存在，请勿重复添加");
            return serviceResult;
        }else{
            // 校验rabbitmq是否存在
            Rabbitmq rabbitmq = serviceHelper.getRabbitmqMapper().selectById(rabbitmqAvailableJobUpdateVo.getRabbitmqId());
            if(rabbitmq != null){
                RabbitmqAvailableJob historyData = serviceHelper.getRabbitmqAvailableJobMapper().selectById(rabbitmqAvailableJobUpdateVo.getId());

                int i = serviceHelper.getRabbitmqAvailableJobMapper().updateInfo(rabbitmqAvailableJobUpdateVo);
                if(i > 0){
                    logger.info("更新id为[{}]rabbitmq监控任务成功", rabbitmqAvailableJobUpdateVo.getId());

                    Integer enable = rabbitmqAvailableJobUpdateVo.getEnable();
                    // 原来是启用现在关闭时需要暂停后台任务
                    if(historyData.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && enable.intValue() == EnableEnum.NO.getCode().intValue()){
                        String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + rabbitmqAvailableJobUpdateVo.getId();
                        logger.info("暂停后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                    }else if(historyData.getEnable().intValue() == EnableEnum.NO.getCode().intValue() && enable.intValue() == EnableEnum.YES.getCode().intValue()) {
                        // 原来是关闭现在启用任务时需要恢复后台任务
                        String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + rabbitmqAvailableJobUpdateVo.getId();
                        logger.info("恢复后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().resumeScheduleJob(jobId);
                    }else{
                        String scheduleExpression = rabbitmqAvailableJobUpdateVo.getScheduleExpression();
                        if(rabbitmqAvailableJobUpdateVo.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && !historyData.getScheduleExpression().equals(scheduleExpression)){
                            String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + rabbitmqAvailableJobUpdateVo.getId();
                            serviceHelper.getQuartzManager().updateScheduleJob(jobId, scheduleExpression);
                        }
                    }

                    return ServiceResult.ok(Boolean.TRUE, ServiceResult.UPDATE_SUCCESS);
                }else{
                    logger.info("更新id为[{}]rabbitmq监控任务失败", rabbitmqAvailableJobUpdateVo.getId());
                    return ServiceResult.ok(Boolean.FALSE, ServiceResult.UPDATE_FAIL);
                }
            }else{
                logger.info("rabbitmqId=[{}]的信息不存在", rabbitmqAvailableJobUpdateVo.getRabbitmqId());
                return ServiceResult.error(ServiceResult.UPDATE_FAIL);
            }
        }
    }

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    @Override
    public ServiceResult<Boolean> executeJob(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        try {
            String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + id;
            serviceHelper.getQuartzManager().runOnce(jobId);
        }catch (Exception e){
            serviceResult.setMessage(ServiceResult.OPERATE_FAIL);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public List<RabbitmqAvailableJob> findByRabbitmqId(Integer rabbitmqId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("rabbitmq_id",rabbitmqId);
        return this.baseMapper.selectByMap(paramMap);
    }

    /**
     * 判断是否有相同数据
     * @param id 主键id
     * @param rabbitmqId rabbitmqId
     * @return 返回判断结果
     */
    private Boolean judgeIfHaveSameData(Integer id, Integer rabbitmqId){
        Boolean result = Boolean.FALSE;

        QueryWrapper<RabbitmqAvailableJob> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(RabbitmqAvailableJobConstant.RABBITMQ_ID, rabbitmqId);
        if(id != null){
            queryWrapper.ne(MysqlAvailableJobConstant.ID, id);
        }
        Integer selectCount = serviceHelper.getRabbitmqAvailableJobMapper().selectCount(queryWrapper);
        if(selectCount > 0){
            result = Boolean.TRUE;
        }

        return result;
    }
}