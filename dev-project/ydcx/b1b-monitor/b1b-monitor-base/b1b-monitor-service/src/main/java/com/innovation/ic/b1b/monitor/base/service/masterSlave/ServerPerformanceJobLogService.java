package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLog.ServerPerformanceJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJobLog.ServerPerformanceJobLogListQueryVo;
import java.util.Date;

/**
 * @desc   服务器性能任务日志表Service
 * @author linuo
 * @time   2023年3月14日11:16:24
 */
public interface ServerPerformanceJobLogService {
    /**
     * 查询服务器性能任务日志表数据
     * @param serverPerformanceJobLogListQueryVo 查询服务器性能任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ServerPerformanceJobLogListRespData>> queryList(ServerPerformanceJobLogListQueryVo serverPerformanceJobLogListQueryVo);

    /**
     * 查询服务1小时内不可用数据量(性能/内存超过95%)
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findServerClose(Date date, Date behindData);
}
