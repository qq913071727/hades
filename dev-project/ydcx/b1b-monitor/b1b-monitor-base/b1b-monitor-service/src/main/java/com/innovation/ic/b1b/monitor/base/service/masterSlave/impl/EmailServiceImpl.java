package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.EmailMapper;
import com.innovation.ic.b1b.monitor.base.model.Email;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.email.AlarmEmailQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EmailService;
import com.innovation.ic.b1b.monitor.base.vo.EmailVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import java.util.List;

@Service
@Slf4j
public class EmailServiceImpl extends ServiceImpl<EmailMapper, Email> implements EmailService {
    @Override
    public ServiceResult addEmail(EmailVo emailVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = emailVo.getId();
        Email email = new Email();
        BeanUtil.copyProperties(emailVo, email);
        QueryWrapper<Email> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email",emailVo.getEmail());
        if (id == null) {
            //验重,环境和系统id唯一
            List<Email> emailList = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(emailList)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("邮箱重复");
                return serviceResult;
            }
            this.baseMapper.insert(email);
        } else {
            queryWrapper.ne("id",id);
            List<Email> emailList = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(emailList)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("邮箱重复");
                return serviceResult;
            }
            this.baseMapper.updateById(email);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteEmail(EmailVo emailVo) {
        this.baseMapper.deleteById(emailVo.getId());
    }

    @Override
    public Email findById(EmailVo emailVo,Boolean mainDb) {
        return this.baseMapper.selectById(emailVo.getId());
    }

    @Override
    public ServiceResult<PageInfo<Email>> findByPage(EmailVo emailVo,Boolean mainDb) {
        ServiceResult<PageInfo<Email>> serviceResult = new ServiceResult<>();

        PageHelper.startPage(emailVo.getPageNo(),emailVo.getPageSize());

        //Page<Email> page = new Page<>(emailVo.getPageNo(), emailVo.getPageSize());
        QueryWrapper<Email> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(emailVo.getEmail())) {
            queryWrapper.like("email", emailVo.getEmail());
        }
        if (!StringUtils.isEmpty(emailVo.getRealName())) {
            queryWrapper.like("real_name", emailVo.getRealName());
        }
        List<Email> emails = this.baseMapper.selectList(queryWrapper);
        PageInfo<Email> pageInfo = new PageInfo<>(emails);
        serviceResult.setResult(pageInfo);
        serviceResult.setMessage("查询成功");
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查询报警邮箱列表
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<AlarmEmailQueryRespPojo>> queryAlarmEmailList(Boolean mainDb) {
        List<AlarmEmailQueryRespPojo> result = this.baseMapper.queryAlarmEmailList();
        ServiceResult<List<AlarmEmailQueryRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }
}