package com.innovation.ic.b1b.monitor.base.service.sharding.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.FrontEndLogMapper;
import com.innovation.ic.b1b.monitor.base.model.FrontEndLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.frontEndLog.FrontEndLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.frontEndLog.FrontEndLogPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPagePojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPojo;
import com.innovation.ic.b1b.monitor.base.service.sharding.FrontEndLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.FrontEndLogVo;
import com.innovation.ic.b1b.monitor.base.vo.InterfaceCallLogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class FrontEndLogServiceImpl extends ServiceImpl<FrontEndLogMapper, FrontEndLog> implements FrontEndLogService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加FrontEndLog类型对象
     *
     * @param frontEndLog
     */
    @Override
    public ServiceResult<Integer> saveFrontEndLog(FrontEndLog frontEndLog) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();

        int result = baseMapper.insert(frontEndLog);

        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 分页显示
     * @param frontEndLogVo
     * @return
     */
    @Override
    public ServiceResult<FrontEndLogPagePojo> page(FrontEndLogVo frontEndLogVo) {
        Integer start = frontEndLogVo.getPageSize() * (frontEndLogVo.getPageNo() - 1);

        // 数据
        List<FrontEndLogPojo> frontEndLogPagePojoList = serviceHelper.getFrontEndLogMapper().page(frontEndLogVo.getSystemId(),
                frontEndLogVo.getEnvironmentId(), frontEndLogVo.getServiceId(), frontEndLogVo.getModelId(), frontEndLogVo.getBeginDate(),
                frontEndLogVo.getEndDate(), start, frontEndLogVo.getPageSize());

        // 记录总数
        List<Integer> integerList = serviceHelper.getFrontEndLogMapper().pageCount(frontEndLogVo.getSystemId(),
                frontEndLogVo.getEnvironmentId(), frontEndLogVo.getServiceId(), frontEndLogVo.getModelId(),
                frontEndLogVo.getBeginDate(), frontEndLogVo.getEndDate());
        Integer count = integerList.size();

        FrontEndLogPagePojo frontEndLogPagePojo = new FrontEndLogPagePojo();
        frontEndLogPagePojo.setFrontEndLogPojoList(frontEndLogPagePojoList);
        frontEndLogPagePojo.setCount(count);

        return ServiceResult.ok(frontEndLogPagePojo, ServiceResult.SELECT_SUCCESS);
    }
}
