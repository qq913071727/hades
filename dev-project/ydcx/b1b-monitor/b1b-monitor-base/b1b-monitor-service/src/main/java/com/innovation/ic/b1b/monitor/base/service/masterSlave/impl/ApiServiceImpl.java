package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.ApiMapper;
import com.innovation.ic.b1b.monitor.base.model.Api;
import com.innovation.ic.b1b.monitor.base.pojo.ApiPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ApiService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.vo.ApiVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ApiServiceImpl  extends ServiceImpl<ApiMapper, Api> implements ApiService {

    @Resource
    private EnvironmentService environmentService;

    @Override
    public ServiceResult addApi(ApiVo apiVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = apiVo.getId();
        Api api = new Api();
        BeanUtil.copyProperties(apiVo, api);
        QueryWrapper<Api> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",api.getName());
        queryWrapper.eq("system_id",api.getSystemId());
        queryWrapper.eq("environment_id",api.getEnvironmentId());
        queryWrapper.eq("service_id",api.getServiceId());
        if(id == null){
            //验重,环境和系统id唯一
            List<Api> environments = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(environments)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字重复");
                return serviceResult;
            }
            this.baseMapper.insert(api);
        }else{
            queryWrapper.ne("id",id);
            //验重,环境和系统id唯一
            List<Api> environments = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(environments)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字重复");
                return serviceResult;
            }
            this.baseMapper.updateById(api);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteApi(ApiVo apiVo) {
        this.baseMapper.deleteById(apiVo.getId());
    }

    @Override
    public ApiPojo findById(ApiVo apiVo,Boolean mainDb) {
        return this.baseMapper.findById(apiVo);
    }

    @Override
    public ServiceResult<PageInfo<ApiPojo>> findByPage(ApiVo apiVo,Boolean mainDb) {
        ServiceResult<PageInfo<ApiPojo>> serviceResult = new ServiceResult<>();
        //当环境id不为空时
        Integer environmentId = apiVo.getEnvironmentId();
        if(environmentId != null){
            List<Integer> environmentIds = environmentService.findByName(environmentId);
            if(!CollectionUtils.isEmpty(environmentIds)){
                apiVo.setEnvironmentIds(environmentIds);
            }
        }
        PageHelper.startPage(apiVo.getPageNo(),apiVo.getPageSize());
        List<ApiPojo> environmentPojoList = this.baseMapper.findByPage(apiVo);
        PageInfo<ApiPojo> pageInfo = new PageInfo<>(environmentPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public void deleteServiceId(Integer serviceId) {
        Map<String,Object> map = new HashMap<>();
        map.put("service_id",serviceId);
        this.baseMapper.deleteByMap(map);
    }
}
