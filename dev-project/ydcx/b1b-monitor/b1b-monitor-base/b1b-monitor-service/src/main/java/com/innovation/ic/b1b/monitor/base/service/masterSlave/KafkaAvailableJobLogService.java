package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJobLog.KafkaAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJobLog.KafkaAvailableJobLogListQueryVo;

/**
 * @desc   Kafka可用任务日志表Service
 * @author linuo
 * @time   2023年5月8日11:23:54
 */
public interface KafkaAvailableJobLogService {
    /**
     * 查询kafka监控任务日志列表
     * @param kafkaAvailableJobLogListQueryVo 查询kafka监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<KafkaAvailableJobLogListRespData>> queryList(KafkaAvailableJobLogListQueryVo kafkaAvailableJobLogListQueryVo);
}