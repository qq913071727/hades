package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.SqlServerAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJobLog.SqlServerAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.SqlServerAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJobLog.SqlServerAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   sql server可用任务日志表Service实现类
 * @author linuo
 * @time   2023年3月15日13:45:10
 */
@Slf4j
@Service
public class SqlServerAvailableJobLogServiceImpl extends ServiceImpl<SqlServerAvailableJobLogMapper, SqlServerAvailableJobLog> implements SqlServerAvailableJobLogService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询sql server可用任务日志
     * @param sqlServerAvailableJobLogListQueryVo 查询sql server可用任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<SqlServerAvailableJobLogListRespData>> queryList(SqlServerAvailableJobLogListQueryVo sqlServerAvailableJobLogListQueryVo) {
        PageHelper.startPage(sqlServerAvailableJobLogListQueryVo.getPageNo(), sqlServerAvailableJobLogListQueryVo.getPageSize());

        // 查询sql server可用任务日志表数据
        List<SqlServerAvailableJobLogListRespData> sqlServerAvailableJobLogs = serviceHelper.getSqlServerAvailableJobLogMapper().queryList(sqlServerAvailableJobLogListQueryVo);
        PageInfo<SqlServerAvailableJobLogListRespData> pageInfo = new PageInfo<>(sqlServerAvailableJobLogs);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * sql server可用任务日志表1小时内不可用数据量
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findSqlServerClose(Date date, Date behindData) {
        Map<Integer, SqlServerAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<SqlServerAvailableJobLog> sqlServerAvailableJobLogQueryWrapper = new QueryWrapper<>();
        sqlServerAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        sqlServerAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        sqlServerAvailableJobLogQueryWrapper.eq("active", Home.STATUS_TYPE);
        List<SqlServerAvailableJobLog> sqlServerAvailableJobLogs = baseMapper.selectList(sqlServerAvailableJobLogQueryWrapper);
        for (SqlServerAvailableJobLog sqlServerAvailableJobLog : sqlServerAvailableJobLogs) {
            if (map.containsKey(sqlServerAvailableJobLog.getSqlServerAvailableJobId())) {//sql server可用任务id是有重复的
                SqlServerAvailableJobLog mapData = map.get(sqlServerAvailableJobLog.getSqlServerAvailableJobId());//map中数据
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), sqlServerAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(sqlServerAvailableJobLog.getSqlServerAvailableJobId(), sqlServerAvailableJobLog);
                    }
                } else {//list 近
                    if (sqlServerAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(sqlServerAvailableJobLog.getSqlServerAvailableJobId(), sqlServerAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//服务器性能任务id不重复
                if (sqlServerAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(sqlServerAvailableJobLog.getSqlServerAvailableJobId(), sqlServerAvailableJobLog);
                } else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    @Override
    public void deleteBySqlServerAvailableJobId(Integer sqlServerAvailableJobId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("sql_server_available_job_id",sqlServerAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}