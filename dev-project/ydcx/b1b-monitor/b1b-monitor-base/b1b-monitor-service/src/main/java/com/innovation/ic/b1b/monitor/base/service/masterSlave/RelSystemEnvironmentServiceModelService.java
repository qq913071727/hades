package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironmentServiceModel;

public interface RelSystemEnvironmentServiceModelService {
    void insert(RelSystemEnvironmentServiceModel relSystemEnvironmentServiceModel);

    void update(RelSystemEnvironmentServiceModel relSystemEnvironmentServiceModel);

    void deleteById(Integer relId);
}
