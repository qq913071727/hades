package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob.ZookeeperAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob.ZookeeperAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobUpdateVo;

/**
 * @desc   ZookeeperAvailableJob表Service
 * @author linuo
 * @time   2023年5月11日10:15:53
 */
public interface ZookeeperAvailableJobService {
    /**
     * 添加zookeeper监控任务
     * @param zookeeperAvailableJobAddVo 添加zookeeper监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(ZookeeperAvailableJobAddVo zookeeperAvailableJobAddVo);

    /**
     * 查询zookeeper监控任务列表
     * @param zookeeperAvailableJobListQueryVo 查询zookeeper监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ZookeeperAvailableJobListRespData>> queryList(ZookeeperAvailableJobListQueryVo zookeeperAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除zookeeper监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取zookeeper监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<ZookeeperAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新zookeeper监控任务
     * @param zookeeperAvailableJobUpdateVo 更新zookeeper监控任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(ZookeeperAvailableJobUpdateVo zookeeperAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);
}