package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobLogPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogVo;

import java.util.List;

/**
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
public interface LogMonitorJobLogService {

    ServiceResult<PageInfo<LogMonitorJobLogPojo>> pageInfo(int page, int rows, LogMonitorJobLogVo vo);

    ServiceResult<List<LogMonitorJobLogPojo>> list(LogMonitorJobLogVo vo);

    ServiceResult<LogMonitorJobLogPojo> get(String id);

    void save(LogMonitorJobLogVo vo);

    void update(LogMonitorJobLogVo vo);

    void delete(String[] ids);
}