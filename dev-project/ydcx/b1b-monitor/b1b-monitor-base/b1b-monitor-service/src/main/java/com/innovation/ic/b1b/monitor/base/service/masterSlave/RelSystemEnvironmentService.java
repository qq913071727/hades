package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironment;
import com.innovation.ic.b1b.monitor.base.vo.EnvironmentVo;

import java.util.List;

public interface RelSystemEnvironmentService {
    boolean save(EnvironmentVo environmentVo);

    void deleteByEnvironmentId(Integer id);

    List<Integer> findByEnvId(Integer id);

    List<Integer> findByServiceId(Integer id);

    List<Integer> findBySystemId(Integer systemId);

    RelSystemEnvironment findBysystemIdAndEnvironmentId(Integer systemId, Integer environmentId);
}
