package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListQueryVo;
import java.util.Date;

public interface RabbitmqAvailableJobLogService {
    /**
     * rabbitma 可用任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    Integer findRabbitmqClose(Date date, Date behindData);

    /**
     * 查询rabbitmq监控任务日志列表
     * @param rabbitmqAvailableJobLogListQueryVo 查询rabbitmq监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<RabbitmqAvailableJobLogListRespData>> queryList(RabbitmqAvailableJobLogListQueryVo rabbitmqAvailableJobLogListQueryVo);

    void deleteByRabbitmqAvailableJobId(Integer rabbitmqAvailableJobId);
}