package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobUpdateVo;
import java.util.List;

/**
 * @desc   mongodb可用任务表service
 * @author linuo
 * @time   2023年3月24日14:30:33
 */
public interface MongodbAvailableJobService {
    /**
     * 添加mongodb监控任务
     * @param mongodbAvailableJobAddVo 添加mongodb监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(MongodbAvailableJobAddVo mongodbAvailableJobAddVo);

    /**
     * 查询mongodb监控任务列表
     * @param mongodbAvailableJobListQueryVo 查询mongodb监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询列表
     */
    ServiceResult<PageInfo<MongodbAvailableJobListRespData>> queryList(MongodbAvailableJobListQueryVo mongodbAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除mongodb监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取mongodb监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<MongodbAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新mongodb监控任务
     * @param mongodbAvailableJobUpdateVo 更新mongodb监控任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(MongodbAvailableJobUpdateVo mongodbAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);

    List<MongodbAvailableJob> findByMongodbId(Integer mongodbId);
}
