package com.innovation.ic.b1b.monitor.base.service.sharding;

import com.innovation.ic.b1b.monitor.base.model.UserLoginLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog.UserLoginLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.UserLoginLogVo;

public interface UserLoginLogService {

    /**
     * 添加UserLoginLog类型对象
     * @param userLoginLog
     */
    ServiceResult<Integer> saveUserLoginLog(UserLoginLog userLoginLog);

    /**
     * 分页显示
     * @param userLoginLogVo
     * @return
     */
    ServiceResult<UserLoginLogPagePojo> page(UserLoginLogVo userLoginLogVo);
}
