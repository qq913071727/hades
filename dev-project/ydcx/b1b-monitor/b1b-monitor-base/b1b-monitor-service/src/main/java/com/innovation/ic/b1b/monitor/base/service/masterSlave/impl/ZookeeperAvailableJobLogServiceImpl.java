package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.ZookeeperAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.ZookeeperAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ZookeeperAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Zookeeper可用任务日志表Service实现类
 * @author linuo
 * @time   2023年5月8日11:24:32
 */
@Slf4j
@Service
public class ZookeeperAvailableJobLogServiceImpl extends ServiceImpl<ZookeeperAvailableJobLogMapper, ZookeeperAvailableJobLog> implements ZookeeperAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询zookeeper监控任务日志列表
     * @param zookeeperAvailableJobLogListQueryVo 查询zookeeper监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<ZookeeperAvailableJobLogListRespData>> queryList(ZookeeperAvailableJobLogListQueryVo zookeeperAvailableJobLogListQueryVo) {
        PageHelper.startPage(zookeeperAvailableJobLogListQueryVo.getPageNo(), zookeeperAvailableJobLogListQueryVo.getPageSize());

        // 查询zookeeper可用任务日志表数据
        List<ZookeeperAvailableJobLogListRespData> data = serviceHelper.getZookeeperAvailableJobLogMapper().queryList(zookeeperAvailableJobLogListQueryVo);
        PageInfo<ZookeeperAvailableJobLogListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }
}