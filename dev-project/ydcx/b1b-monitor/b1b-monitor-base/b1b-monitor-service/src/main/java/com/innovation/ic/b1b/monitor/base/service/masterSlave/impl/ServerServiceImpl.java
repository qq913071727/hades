package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.ServerMapper;
import com.innovation.ic.b1b.monitor.base.model.Server;
import com.innovation.ic.b1b.monitor.base.pojo.ServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.server.ServerQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServerService;
import com.innovation.ic.b1b.monitor.base.vo.ServerVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @desc   服务器表Service实现类
 * @author linuo
 * @time   2023年3月21日10:48:39
 */
@Service
@Slf4j
public class ServerServiceImpl extends ServiceImpl<ServerMapper, Server> implements ServerService {
    /**
     * 查询服务器列表
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<ServerQueryRespPojo>> queryServerInfoList() {
        List<ServerQueryRespPojo> result = this.baseMapper.queryServerInfoList();
        ServiceResult<List<ServerQueryRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServiceResult addServer(ServerVo serviceVo) {
        ServiceResult serviceResult = new ServiceResult();
        QueryWrapper<Server> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ip",serviceVo.getIp());
        Server server = new Server();
        Integer id = serviceVo.getId();
        BeanUtil.copyProperties(serviceVo,server);
        if(id == null){
            //验重,环境和系统id唯一
            List<Server> serverList = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(serverList)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("ip重复");
                return serviceResult;
            }
            this.baseMapper.insert(server);
        }else{
            queryWrapper.ne("id",id);
            //验重,环境和系统id唯一
            List<Server> serverList = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(serverList)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("ip重复");
                return serviceResult;
            }
            this.baseMapper.updateById(server);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteServer(ServerVo serverVo) {
        this.baseMapper.deleteById(serverVo.getId());
    }

    @Override
    public ServiceResult<PageInfo<ServerPojo>> findByPage(ServerVo serverVo,Boolean mainDb) {
        ServiceResult<PageInfo<ServerPojo>> serviceResult = new ServiceResult<>();
        PageHelper.startPage(serverVo.getPageNo(),serverVo.getPageSize());
        List<ServerPojo> serverPojos = this.baseMapper.findByPage(serverVo);
        PageInfo<ServerPojo> pageInfo = new PageInfo<>(serverPojos);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(pageInfo);
        return serviceResult;
    }
}