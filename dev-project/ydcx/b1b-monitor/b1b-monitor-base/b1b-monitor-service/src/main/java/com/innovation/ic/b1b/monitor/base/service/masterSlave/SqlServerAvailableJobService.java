package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobUpdateVo;

/**
 * @desc   sql server可用任务表Service
 * @author linuo
 * @time   2023年3月14日15:36:34
 */
public interface SqlServerAvailableJobService {
    /**
     * 添加sql server监控任务
     * @param sqlServerAvailableJobAddVo sql server可用任务添加的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(SqlServerAvailableJobAddVo sqlServerAvailableJobAddVo);

    /**
     * 查询sql server监控任务列表
     * @param sqlServerAvailableJobListQueryVo 查询sql server监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<SqlServerAvailableJobListRespData>> queryList(SqlServerAvailableJobListQueryVo sqlServerAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除sql server监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取sql server监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回sql server监控任务详情
     */
    ServiceResult<SqlServerAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新sql server监控任务
     * @param sqlServerAvailableJobUpdateVo sql server可用任务更新的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(SqlServerAvailableJobUpdateVo sqlServerAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);
}
