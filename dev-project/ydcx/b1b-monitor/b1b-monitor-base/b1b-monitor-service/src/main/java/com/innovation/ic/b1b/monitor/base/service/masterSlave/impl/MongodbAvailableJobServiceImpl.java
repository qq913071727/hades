package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.handler.MongodbAvailableJobHandler;
import com.innovation.ic.b1b.monitor.base.mapper.MongodbAvailableJobMapper;
import com.innovation.ic.b1b.monitor.base.model.Mongodb;
import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.MongodbAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MongodbAvailableJobService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobUpdateVo;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MongodbAvailableJobServiceImpl extends ServiceImpl<MongodbAvailableJobMapper, MongodbAvailableJob> implements MongodbAvailableJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 添加mongodb监控任务
     * @param mongodbAvailableJobAddVo 添加mongodb监控任务的Vo类
     * @return 返回添加结果
     */
    @Override
    public ServiceResult<Boolean> add(MongodbAvailableJobAddVo mongodbAvailableJobAddVo) {
        boolean result = Boolean.FALSE;
        String message;

        Boolean aBoolean = judgeIfHaveSameData(null, mongodbAvailableJobAddVo.getMongodbId());
        if(aBoolean){
            message = "当前mongodb监控任务已存在，请勿重复添加";
        }else{
            // 校验mongodbId是否存在
            Mongodb mongodb = serviceHelper.getMongodbMapper().selectById(mongodbAvailableJobAddVo.getMongodbId());
            if(mongodb != null){
                MongodbAvailableJob mongodbAvailableJob = new MongodbAvailableJob();
                BeanUtils.copyProperties(mongodbAvailableJobAddVo, mongodbAvailableJob);
                if(mongodbAvailableJob.getEnable() == null){
                    mongodbAvailableJob.setEnable(EnableEnum.NO.getCode());
                }
                mongodbAvailableJob.setCreateTime(new Date(System.currentTimeMillis()));
                int insert = serviceHelper.getMongodbAvailableJobMapper().insert(mongodbAvailableJob);
                if(insert > 0){
                    message = ServiceResult.INSERT_SUCCESS;
                    result = Boolean.TRUE;

                    String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + mongodbAvailableJob.getId();

                    // 组装任务参数
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(JobDataMapConstant.ID, mongodbAvailableJob.getId());

                    serviceHelper.getQuartzManager().createScheduleJob(jobId, MongodbAvailableJobHandler.class.getName(), "MongodbAvailableJobHandler", jobDataMap, mongodbAvailableJob.getScheduleExpression());
                }else{
                    message = ServiceResult.INSERT_FAIL;
                }
            }else{
                message = "mongodb信息不存在，请配置mongodb信息后重新添加mongodb监控任务";
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 查询mongodb监控任务列表
     * @param mongodbAvailableJobListQueryVo 查询mongodb监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询列表
     */
    @Override
    public ServiceResult<PageInfo<MongodbAvailableJobListRespData>> queryList(MongodbAvailableJobListQueryVo mongodbAvailableJobListQueryVo, Boolean isMain) {
        PageHelper.startPage(mongodbAvailableJobListQueryVo.getPageNo(), mongodbAvailableJobListQueryVo.getPageSize());

        // 查询mongodb可用任务列表数据
        List<MongodbAvailableJobListRespData> data = serviceHelper.getMongodbAvailableJobMapper().queryList(mongodbAvailableJobListQueryVo);
        PageInfo<MongodbAvailableJobListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 删除mongodb监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> delete(Integer id) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        int i = serviceHelper.getMongodbAvailableJobMapper().deleteById(id);
        if(i > 0){
            logger.info("已删除id为[{}]mongodb监控任务", id);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;

            try {
                // 删除任务
                String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + id;
                serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            }catch (Exception e){
                logger.info("删除mongodb监控后台任务失败,原因:", e);
            }
        }else{
            logger.info("删除id为[{}]mongodb监控任务失败，数据不存在", id);
        }

        return ServiceResult.ok(result, message);
    }

    /**
     * 获取mongodb监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<MongodbAvailableJobInfoRespPojo> info(Integer id, Boolean isMain) {
        MongodbAvailableJobInfoRespPojo result = serviceHelper.getMongodbAvailableJobMapper().info(id);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 更新mongodb监控任务
     * @param mongodbAvailableJobUpdateVo 更新mongodb监控任务的Vo类
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> update(MongodbAvailableJobUpdateVo mongodbAvailableJobUpdateVo) {
        Boolean aBoolean = judgeIfHaveSameData(mongodbAvailableJobUpdateVo.getId(), mongodbAvailableJobUpdateVo.getMongodbId());
        if(aBoolean){
            ServiceResult<Boolean> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setMessage("当前mongodb监控任务已存在，请勿重复添加");
            return serviceResult;
        }else{
            // 校验mongodbId是否存在
            Mongodb mongodb = serviceHelper.getMongodbMapper().selectById(mongodbAvailableJobUpdateVo.getMongodbId());
            if(mongodb != null){
                MongodbAvailableJob historyData = serviceHelper.getMongodbAvailableJobMapper().selectById(mongodbAvailableJobUpdateVo.getId());

                int i = serviceHelper.getMongodbAvailableJobMapper().updateInfo(mongodbAvailableJobUpdateVo);
                if(i > 0){
                    logger.info("更新id为[{}]mongodb监控任务成功", mongodbAvailableJobUpdateVo.getId());

                    Integer enable = mongodbAvailableJobUpdateVo.getEnable();
                    // 原来是启用现在关闭时需要暂停后台任务
                    if(historyData.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && enable.intValue() == EnableEnum.NO.getCode().intValue()){
                        String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + mongodbAvailableJobUpdateVo.getId();
                        logger.info("暂停后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                    }else if(historyData.getEnable().intValue() == EnableEnum.NO.getCode().intValue() && enable.intValue() == EnableEnum.YES.getCode().intValue()) {
                        // 原来是关闭现在启用任务时需要恢复后台任务
                        String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + mongodbAvailableJobUpdateVo.getId();
                        logger.info("恢复后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().resumeScheduleJob(jobId);
                    }else{
                        String scheduleExpression = mongodbAvailableJobUpdateVo.getScheduleExpression();
                        if(mongodbAvailableJobUpdateVo.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && !historyData.getScheduleExpression().equals(scheduleExpression)){
                            String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + mongodbAvailableJobUpdateVo.getId();
                            serviceHelper.getQuartzManager().updateScheduleJob(jobId, scheduleExpression);
                        }
                    }

                    return ServiceResult.ok(Boolean.TRUE, ServiceResult.UPDATE_SUCCESS);
                }else{
                    logger.info("更新id为[{}]mongodb监控任务失败", mongodbAvailableJobUpdateVo.getId());
                    return ServiceResult.ok(Boolean.FALSE, ServiceResult.UPDATE_FAIL);
                }
            }else{
                logger.info("mongodbId=[{}]的信息不存在", mongodbAvailableJobUpdateVo.getMongodbId());
                return ServiceResult.error(ServiceResult.UPDATE_FAIL);
            }
        }
    }

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    @Override
    public ServiceResult<Boolean> executeJob(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        try {
            String jobId = TableNameConstant.MONGODB_AVAILABLE_JOB + "_" + id;
            serviceHelper.getQuartzManager().runOnce(jobId);
        }catch (Exception e){
            serviceResult.setMessage(ServiceResult.OPERATE_FAIL);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public List<MongodbAvailableJob> findByMongodbId(Integer mongodbId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("mongodb_id",mongodbId);
        return this.baseMapper.selectByMap(paramMap);
    }

    /**
     * 判断是否有相同数据
     * @param id 主键id
     * @param mongodbId mongodbId
     * @return 返回判断结果
     */
    private Boolean judgeIfHaveSameData(Integer id, Integer mongodbId){
        Boolean result = Boolean.FALSE;

        QueryWrapper<MongodbAvailableJob> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(MongodbAvailableJobConstant.MONGODB_ID, mongodbId);
        if(id != null){
            queryWrapper.ne(MongodbAvailableJobConstant.ID, id);
        }
        Integer selectCount = serviceHelper.getMongodbAvailableJobMapper().selectCount(queryWrapper);
        if(selectCount > 0){
            result = Boolean.TRUE;
        }

        return result;
    }
}