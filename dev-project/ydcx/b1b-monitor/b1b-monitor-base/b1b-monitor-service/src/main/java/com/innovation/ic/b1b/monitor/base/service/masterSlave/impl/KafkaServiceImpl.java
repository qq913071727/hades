package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.KafkaMapper;
import com.innovation.ic.b1b.monitor.base.model.Kafka;
import com.innovation.ic.b1b.monitor.base.pojo.KafkaPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.KafkaService;
import com.innovation.ic.b1b.monitor.base.vo.KafkaVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Kafka表Service实现类
 * @author linuo
 * @time   2023年5月8日11:19:34
 */
@Slf4j
@Service
public class KafkaServiceImpl extends ServiceImpl<KafkaMapper, Kafka> implements KafkaService {
    @Resource
    private KafkaMapper kafkaMapper;

    @Resource
    private EnvironmentService environmentService;

    @Override
    public ServiceResult addKafka(KafkaVo kafkaVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = kafkaVo.getId();
        Kafka kafka = new Kafka();
        BeanUtil.copyProperties(kafkaVo, kafka);
        QueryWrapper<Kafka> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ip", kafkaVo.getIp());
        queryWrapper.eq("port", kafkaVo.getPort());
        if (id == null) {
            List<Kafka> KafkaList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(KafkaList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Kafka> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", kafkaVo.getName());
            List<Kafka> KafkaNameList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(KafkaNameList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }

            this.baseMapper.insert(kafka);
        } else {
            queryWrapper.ne("id", id);
            List<Kafka> KafkaList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(KafkaList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Kafka> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", kafkaVo.getName());
            queryWrapperName.ne("id", id);
            List<Kafka> KafkaNameList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(KafkaNameList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }
            this.baseMapper.updateById(kafka);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteKafka(KafkaVo kafkaVo) {
        this.baseMapper.deleteById(kafkaVo.getId());
    }

    @Override
    public ServiceResult<PageInfo<KafkaPojo>> findByPage(KafkaVo kafkaVo, Boolean mainDb) {
        ServiceResult<PageInfo<KafkaPojo>> serviceResult = new ServiceResult<>();
        Integer environmentId = kafkaVo.getEnvironmentId();
        if (environmentId != null) {
            List<Integer> environmentIds = environmentService.findByName(environmentId);
            if (!CollectionUtils.isEmpty(environmentIds)) {
                kafkaVo.setEnvironmentIds(environmentIds);
            }
        }
        PageHelper.startPage(kafkaVo.getPageNo(), kafkaVo.getPageSize());
        List<KafkaPojo> redisPojoList = this.baseMapper.findByPage(kafkaVo);
        PageInfo<KafkaPojo> pageInfo = new PageInfo<>(redisPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;


    }
}
