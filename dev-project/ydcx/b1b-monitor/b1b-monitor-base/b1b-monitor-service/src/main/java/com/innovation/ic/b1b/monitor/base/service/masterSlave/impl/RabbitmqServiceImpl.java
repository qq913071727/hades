package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.RabbitmqMapper;
import com.innovation.ic.b1b.monitor.base.model.Rabbitmq;
import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.RabbitmqPojo;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmq.RabbitmqInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqAvailableJobLogHistoryService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqAvailableJobService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RabbitmqService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.RabbitmqVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class RabbitmqServiceImpl extends ServiceImpl<RabbitmqMapper, Rabbitmq> implements RabbitmqService {
    @Resource
    private EnvironmentService environmentService;

    @Resource
    private RabbitmqAvailableJobService rabbitmqAvailableJobService;

    @Resource
    private RabbitmqAvailableJobLogService rabbitmqAvailableJobLogService;

    @Resource
    private RabbitmqAvailableJobLogHistoryService rabbitmqAvailableJobLogHistoryService;

    @Resource
    private ServiceHelper serviceHelper;


    @Override
    public ServiceResult addRabbitmq(RabbitmqVo rabbitmqVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = rabbitmqVo.getId();
        Rabbitmq rabbitmq = new Rabbitmq();
        BeanUtil.copyProperties(rabbitmqVo, rabbitmq);
        QueryWrapper<Rabbitmq> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ip", rabbitmqVo.getIp());
        queryWrapper.eq("port", rabbitmqVo.getPort());
        if (id == null) {
            List<Rabbitmq> rabbitmqList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(rabbitmqList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Rabbitmq> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", rabbitmqVo.getName());
            List<Rabbitmq> rabbitmqNameList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(rabbitmqNameList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }

            this.baseMapper.insert(rabbitmq);
        } else {
            queryWrapper.ne("id", id);
            List<Rabbitmq> rabbitmqList = this.baseMapper.selectList(queryWrapper);
            if (!CollectionUtils.isEmpty(rabbitmqList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("重复,请重新添加");
                return serviceResult;
            }
            QueryWrapper<Rabbitmq> queryWrapperName = new QueryWrapper<>();
            queryWrapperName.eq("name", rabbitmqVo.getName());
            queryWrapperName.ne("id", id);
            List<Rabbitmq> rabbitmqNameList = this.baseMapper.selectList(queryWrapperName);
            if (!CollectionUtils.isEmpty(rabbitmqNameList)) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字不能重复");
                return serviceResult;
            }
            this.baseMapper.updateById(rabbitmq);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteRabbitmq(RabbitmqVo rabbitmqVo) {
        Integer id = rabbitmqVo.getId();
        this.baseMapper.deleteById(id);
        this.deleteByRabbitmqId(id);
    }

    @Override
    public ServiceResult<PageInfo<RabbitmqPojo>> findByPage(RabbitmqVo rabbitmqVo,Boolean mainDb) {
        ServiceResult<PageInfo<RabbitmqPojo>> serviceResult = new ServiceResult<>();
        Integer environmentId = rabbitmqVo.getEnvironmentId();
        if (environmentId != null) {
            List<Integer> environmentIds = environmentService.findByName(environmentId);
            if (!CollectionUtils.isEmpty(environmentIds)) {
                rabbitmqVo.setEnvironmentIds(environmentIds);
            }
        }
        PageHelper.startPage(rabbitmqVo.getPageNo(), rabbitmqVo.getPageSize());
        List<RabbitmqPojo> rabbitmqPojoList = this.baseMapper.findByPage(rabbitmqVo);
        PageInfo<RabbitmqPojo> pageInfo = new PageInfo<>(rabbitmqPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查询rabbitmq信息列表
     *
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<RabbitmqInfoRespPojo>> queryRabbitmqList(Boolean mainDb) {
        List<RabbitmqInfoRespPojo> result = this.baseMapper.queryRabbitmqList();
        ServiceResult<List<RabbitmqInfoRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public void deleteByEnvironmentId(Integer environmentId) {
        QueryWrapper<Rabbitmq> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("environment_id", environmentId);
        List<Rabbitmq> rabbitmqList = this.baseMapper.selectList(queryWrapper);
        for (Rabbitmq rabbitmq : rabbitmqList) {
            this.deleteByRabbitmqId(rabbitmq.getId());
        }
        Map<String, Object> param = new HashMap<>();
        param.put("environment_id", environmentId);
        this.baseMapper.deleteByMap(param);
    }


    /**
     * 级联删除关联日志及任务
     * @param rabbitmqId
     */
    private void deleteByRabbitmqId(Integer rabbitmqId){
        List<RabbitmqAvailableJob> rabbitmqAvailableJobList = rabbitmqAvailableJobService.findByRabbitmqId(rabbitmqId);
        for (RabbitmqAvailableJob rabbitmqAvailableJob : rabbitmqAvailableJobList) {
            Integer rabbitmqAvailableJobId = rabbitmqAvailableJob.getId();
            String jobId = TableNameConstant.RABBITMQ_AVAILABLE_JOB + "_" + rabbitmqAvailableJobId;
            serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            rabbitmqAvailableJobService.delete(rabbitmqAvailableJobId);
            //同时删除日志和历史日志
            rabbitmqAvailableJobLogService.deleteByRabbitmqAvailableJobId(rabbitmqAvailableJobId);
            rabbitmqAvailableJobLogHistoryService.deleteByRabbitmqAvailableJobId(rabbitmqAvailableJobId);
        }
    }


}
