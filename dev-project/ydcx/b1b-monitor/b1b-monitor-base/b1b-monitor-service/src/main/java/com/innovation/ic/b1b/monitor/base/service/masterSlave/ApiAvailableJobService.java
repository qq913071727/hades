package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJob.ApiAvailableJobDataPojo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobUpdateVo;

/**
 * @author zengqinglong
 * @desc 接口可用任务管理
 * @Date 2023/3/30 10:39
 **/
public interface ApiAvailableJobService {
    /**
     * 新增任务
     *
     * @param apiAvailableJobAddVo
     * @return
     */
    ServiceResult<Boolean> add(ApiAvailableJobAddVo apiAvailableJobAddVo);

    /**
     * 根据条件获取 接口任务列表
     *
     * @param apiAvailableJobListQueryVo
     * @return
     */
    ServiceResult<PageInfo<ApiAvailableJobDataPojo>> queryList(ApiAvailableJobListQueryVo apiAvailableJobListQueryVo,Boolean isMain);

    /**
     * 根据id 删除接口监控任务
     *
     * @param id
     * @return
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 根据id 查询接口监控任务详情
     *
     * @param id
     * @return
     */
    ServiceResult<ApiAvailableJobDataPojo> info(Integer id,Boolean isMian);

    /**
     * 根据id 更新接口监控任务
     *
     * @param availableJobUpdateVo
     * @return
     */
    ServiceResult<Boolean> update(ApiAvailableJobUpdateVo availableJobUpdateVo);

    /**
     * 根据id 执行接口监控任务
     *
     * @param id
     * @return
     */
    ServiceResult<Boolean> executeJob(Integer id);
}
