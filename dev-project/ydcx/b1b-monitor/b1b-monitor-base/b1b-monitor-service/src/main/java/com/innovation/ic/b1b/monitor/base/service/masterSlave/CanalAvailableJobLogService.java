package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJobLog.CanalAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJobLog.CanalAvailableJobLogListQueryVo;

/**
 * @desc   canal可用任务日志表Service
 * @author linuo
 * @time   2023年5月12日09:47:59
 */
public interface CanalAvailableJobLogService {
    /**
     * 查询canal监控任务日志列表
     * @param canalAvailableJobLogListQueryVo 查询canal监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<CanalAvailableJobLogListRespData>> queryList(CanalAvailableJobLogListQueryVo canalAvailableJobLogListQueryVo);
}