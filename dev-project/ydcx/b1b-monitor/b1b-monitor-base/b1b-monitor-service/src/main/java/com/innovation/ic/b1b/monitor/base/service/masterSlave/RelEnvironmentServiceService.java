package com.innovation.ic.b1b.monitor.base.service.masterSlave;


import com.innovation.ic.b1b.monitor.base.model.RelEnvironmentService;
import com.innovation.ic.b1b.monitor.base.vo.ModelVo;

import java.util.List;

public interface RelEnvironmentServiceService{

    void add(RelEnvironmentService relEnvironmentService);

    List<Integer> findByEnvironmentId(Integer id);

    void updateRelEnvironmentService(RelEnvironmentService relEnvironmentService);


    void deleteById(Integer reId);

    void deleteByServiceId(Integer serviceId);

    Integer findByServiceIdAndSystemId(ModelVo modelVo);
}
