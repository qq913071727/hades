package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.CellPhoneMapper;
import com.innovation.ic.b1b.monitor.base.model.CellPhone;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.cellPhone.AlarmCellPhoneQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.CellPhoneService;
import com.innovation.ic.b1b.monitor.base.vo.CellPhoneVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import java.util.List;

@Service
@Slf4j
public class CellPhoneServiceImpl extends ServiceImpl<CellPhoneMapper, CellPhone> implements CellPhoneService {
    @Override
    public ServiceResult addCellPhone(CellPhoneVo cellPhoneVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = cellPhoneVo.getId();
        CellPhone cellPhone = new CellPhone();
        BeanUtil.copyProperties(cellPhoneVo, cellPhone);
        QueryWrapper<CellPhone> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cell_phone",cellPhoneVo.getCellPhone());
        if (id == null) {
            List<CellPhone> cellPhones = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(cellPhones)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("手机号重复");
                return serviceResult;
            }
            this.baseMapper.insert(cellPhone);
        } else {
            queryWrapper.ne("id",id);
            List<CellPhone> cellPhones = this.baseMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(cellPhones)){
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("手机号重复");
                return serviceResult;
            }
            this.baseMapper.updateById(cellPhone);
        }

        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    public void deleteCellPhone(CellPhoneVo cellPhoneVo) {
        this.baseMapper.deleteById(cellPhoneVo.getId());
    }

    @Override
    public CellPhone findById(CellPhoneVo cellPhoneVo,Boolean mainDb) {
        return this.baseMapper.selectById(cellPhoneVo.getId());
    }

    @Override
    public ServiceResult<PageInfo<CellPhone>> findByPage(CellPhoneVo cellPhoneVo,Boolean mainDb) {
        ServiceResult<PageInfo<CellPhone>> serviceResult = new ServiceResult<>();
        PageHelper.startPage(cellPhoneVo.getPageNo(),cellPhoneVo.getPageSize());
        QueryWrapper<CellPhone> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(cellPhoneVo.getCellPhone())) {
            queryWrapper.like("cell_phone", cellPhoneVo.getCellPhone());
        }
        if (!StringUtils.isEmpty(cellPhoneVo.getRealName())) {
            queryWrapper.like("real_name", cellPhoneVo.getRealName());
        }
        List<CellPhone> emails = this.baseMapper.selectList(queryWrapper);
        PageInfo<CellPhone> pageInfo = new PageInfo<>(emails);
        serviceResult.setResult(pageInfo);
        serviceResult.setMessage("查询成功");
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查询报警手机号列表
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<AlarmCellPhoneQueryRespPojo>> queryAlarmCellPhoneList(Boolean mainDb) {
        List<AlarmCellPhoneQueryRespPojo> result = this.baseMapper.queryAlarmCellPhoneList();
        ServiceResult<List<AlarmCellPhoneQueryRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }
}