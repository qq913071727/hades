package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.RelEnvironmentServiceMapper;
import com.innovation.ic.b1b.monitor.base.model.RelEnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RelEnvironmentServiceService;
import com.innovation.ic.b1b.monitor.base.vo.ModelVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RelEnvironmentServiceServiceImpl  extends ServiceImpl<RelEnvironmentServiceMapper, RelEnvironmentService> implements RelEnvironmentServiceService {


    @Override
    public void add(RelEnvironmentService relEnvironmentService) {
        this.baseMapper.insert(relEnvironmentService);
    }

    @Override
    public List<Integer> findByEnvironmentId(Integer environmentId) {
        return this.baseMapper.findByEnvironmentId(environmentId);
    }

    @Override
    public void updateRelEnvironmentService(RelEnvironmentService relEnvironmentService) {
/*        UpdateWrapper<RelEnvironmentService> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("service_id", relEnvironmentService.getServiceId());
        updateWrapper.eq("environment_id", relEnvironmentService.getEnvironmentId());
        updateWrapper.set("environment_id", relEnvironmentService.getEnvironmentId());
        this.baseMapper.update(null, updateWrapper);*/
        this.baseMapper.updateById(relEnvironmentService);
    }



    @Override
    public void deleteById(Integer reId) {
        this.baseMapper.deleteById(reId);
    }

    @Override
    public void deleteByServiceId(Integer serviceId) {
        Map<String,Object> param = new HashMap<>();
        param.put("service_id",serviceId);
        this.baseMapper.deleteByMap(param);
    }

    @Override
    public Integer findByServiceIdAndSystemId(ModelVo modelVo) {
        return this.baseMapper.findByServiceIdAndSystemId(modelVo);
    }
}
