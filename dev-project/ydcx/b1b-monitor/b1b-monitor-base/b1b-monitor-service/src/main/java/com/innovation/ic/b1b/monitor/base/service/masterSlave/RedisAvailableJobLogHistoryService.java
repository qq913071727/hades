package com.innovation.ic.b1b.monitor.base.service.masterSlave;

public interface RedisAvailableJobLogHistoryService {

    void deleteByRedisAvailableJobId(Integer redisAvailableJobId);
}