package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogHistoryVo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobLogHistoryPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
public interface LogMonitorJobLogHistoryService {

    ServiceResult<PageInfo<LogMonitorJobLogHistoryPojo>> pageInfo( int page, int rows,LogMonitorJobLogHistoryVo vo);

    ServiceResult<List<LogMonitorJobLogHistoryPojo>> list(LogMonitorJobLogHistoryVo vo);

    ServiceResult<LogMonitorJobLogHistoryPojo> get(String id);

    void save(LogMonitorJobLogHistoryVo vo);

    void update(LogMonitorJobLogHistoryVo vo);

    void delete(String[] ids);
}