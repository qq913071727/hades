package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.ModelMapper;
import com.innovation.ic.b1b.monitor.base.model.Model;
import com.innovation.ic.b1b.monitor.base.pojo.ModelPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ModelService;
import com.innovation.ic.b1b.monitor.base.vo.ModelVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ModelServiceImpl extends ServiceImpl<ModelMapper, Model> implements ModelService {
    @Override
    public ServiceResult addModel(ModelVo modelVo) {
        return null;
    }

    @Override
    public void deleteModel(ModelVo modelVo) {

    }

    @Override
    public ServiceResult<PageInfo<ModelPojo>> findByPage(ModelVo modelVo, Boolean mainDb) {
        return null;
    }
}
