package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.LogMonitorJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.LogMonitorJobLogHistoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogHistoryVo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobLogHistoryPojo;
import com.innovation.ic.b1b.monitor.base.model.LogMonitorJobLogHistory;
import org.springframework.beans.BeanUtils;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@Service
public class LogMonitorJobLogHistoryServiceImpl extends ServiceImpl<LogMonitorJobLogHistoryMapper, LogMonitorJobLogHistory> implements LogMonitorJobLogHistoryService {

    @Resource
    private LogMonitorJobLogHistoryMapper baseMapper;

    @Override
    public ServiceResult<PageInfo<LogMonitorJobLogHistoryPojo>> pageInfo(int page, int rows, LogMonitorJobLogHistoryVo dto) {
        PageHelper.startPage(page, rows);
        List<LogMonitorJobLogHistory> list = baseMapper.pageInfo(dto);
        List<LogMonitorJobLogHistoryPojo> targetList = BeanPropertiesUtil.convertList(list,LogMonitorJobLogHistoryPojo.class);
        PageInfo<LogMonitorJobLogHistoryPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<LogMonitorJobLogHistoryPojo>> list(LogMonitorJobLogHistoryVo dto) {
        List<LogMonitorJobLogHistory> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, LogMonitorJobLogHistoryPojo.class), "ok");
    }

    private Wrapper<LogMonitorJobLogHistory> getWrapper(LogMonitorJobLogHistoryVo dto) {
        LambdaQueryWrapper<LogMonitorJobLogHistory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(LogMonitorJobLogHistory::getId, dto.getId());
        return lambdaQueryWrapper;
    }


    @Override
    public ServiceResult<LogMonitorJobLogHistoryPojo> get(String id) {
        LogMonitorJobLogHistory entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, LogMonitorJobLogHistoryPojo.class), "ok");
    }

    @Override
    public void save(LogMonitorJobLogHistoryVo dto) {
        LogMonitorJobLogHistory entity = BeanPropertiesUtil.convert(dto, LogMonitorJobLogHistory. class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(LogMonitorJobLogHistoryVo dto) {
        LogMonitorJobLogHistory entity = BeanPropertiesUtil.convert(dto, LogMonitorJobLogHistory. class);

        baseMapper.updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}