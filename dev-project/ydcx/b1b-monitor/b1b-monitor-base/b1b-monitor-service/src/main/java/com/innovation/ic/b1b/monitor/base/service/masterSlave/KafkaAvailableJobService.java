package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJob.KafkaAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJob.KafkaAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobUpdateVo;

/**
 * @desc   Kafka可用任务表Service
 * @author linuo
 * @time   2023年5月8日11:17:51
 */
public interface KafkaAvailableJobService {
    /**
     * 添加kafka监控任务
     * @param kafkaAvailableJobAddVo 添加kafka监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(KafkaAvailableJobAddVo kafkaAvailableJobAddVo);

    /**
     * 查询kafka监控任务列表
     * @param kafkaAvailableJobListQueryVo 查询kafka监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<KafkaAvailableJobListRespData>> queryList(KafkaAvailableJobListQueryVo kafkaAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除kafka监控任务
     * @param id kafka监控任务id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取kafka监控任务详情
     * @param id kafka监控任务id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<KafkaAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新kafka监控任务
     * @param kafkaAvailableJobUpdateVo 更新kafka监控任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(KafkaAvailableJobUpdateVo kafkaAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id kafka监控任务id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);
}