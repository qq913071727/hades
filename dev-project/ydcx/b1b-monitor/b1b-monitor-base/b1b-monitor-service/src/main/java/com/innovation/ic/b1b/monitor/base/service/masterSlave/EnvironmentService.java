package com.innovation.ic.b1b.monitor.base.service.masterSlave;


import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.EnvironmentPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.EnvironmentVo;

import java.util.List;

public interface EnvironmentService {


    ServiceResult addEnvironment(EnvironmentVo environmentVo);

    void deleteEnvironment(EnvironmentVo environmentVo);

    EnvironmentPojo findById(EnvironmentVo environmentVo,Boolean mainDb);

    ServiceResult<PageInfo<EnvironmentPojo>> findByPage(EnvironmentVo environmentVo,Boolean mainDb);

    void deleteBySystemId(Integer systemId);

    List<EnvironmentPojo> dropDown(EnvironmentVo environmentVo,Boolean mainDb);

    /**
     * 根据环境id查询所有的名字相同ids
     * @param environmentId
     * @return
     */
    List<Integer> findByName(Integer environmentId);

    List<DropDownPojo> addDropDown(DropDownVo dropDownVo);
}
