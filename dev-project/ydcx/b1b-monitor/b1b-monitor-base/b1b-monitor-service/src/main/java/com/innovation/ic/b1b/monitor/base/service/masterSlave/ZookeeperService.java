package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.ZookeeperPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.ZookeeperVo;

/**
 * @desc   Zookeeper表Service
 * @author linuo
 * @time   2023年5月11日10:10:21
 */
public interface ZookeeperService {

    ServiceResult addZookeeper(ZookeeperVo zookeeperVo);

    void deleteLog(ZookeeperVo zookeeperVo);

    ServiceResult<PageInfo<ZookeeperPojo>> findByPage(ZookeeperVo zookeeperVo, Boolean mainDb);
}