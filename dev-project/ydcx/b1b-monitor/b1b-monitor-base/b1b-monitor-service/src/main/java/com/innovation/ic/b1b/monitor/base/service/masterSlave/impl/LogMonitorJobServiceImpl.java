package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.b1b.monitor.base.mapper.LogMonitorJobMapper;
import com.innovation.ic.b1b.monitor.base.model.LogMonitorJob;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.LogMonitorJobService;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@Service
public class LogMonitorJobServiceImpl extends ServiceImpl<LogMonitorJobMapper, LogMonitorJob> implements LogMonitorJobService {

    @Resource
    private LogMonitorJobMapper baseMapper;

    @Override
    public ServiceResult<PageInfo<LogMonitorJobPojo>> pageInfo(int page, int rows, LogMonitorJobVo dto) {
        PageHelper.startPage(page, rows);
        List<LogMonitorJob> list = baseMapper.pageInfo(dto);
        List<LogMonitorJobPojo> targetList = BeanPropertiesUtil.convertList(list, LogMonitorJobPojo.class);
        PageInfo<LogMonitorJobPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<LogMonitorJobPojo>> list(LogMonitorJobVo dto) {
        List<LogMonitorJob> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, LogMonitorJobPojo.class), "ok");
    }

    private Wrapper<LogMonitorJob> getWrapper(LogMonitorJobVo dto) {
        LambdaQueryWrapper<LogMonitorJob> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(LogMonitorJob::getId, dto.getId());
        return lambdaQueryWrapper;
    }


    @Override
    public ServiceResult<LogMonitorJobPojo> get(String id) {
        LogMonitorJob entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, LogMonitorJobPojo.class), "ok");
    }

    @Override
    public void save(LogMonitorJobVo dto) {
        LogMonitorJob entity = BeanPropertiesUtil.convert(dto, LogMonitorJob.class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(LogMonitorJobVo dto) {
        LogMonitorJob entity = BeanPropertiesUtil.convert(dto, LogMonitorJob.class);

        baseMapper.updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}