package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.MySqlPojo;
import com.innovation.ic.b1b.monitor.base.pojo.SqlServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysql.MysqlInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.MySqlVo;
import java.util.List;

public interface MySqlService {
    ServiceResult addMySql(MySqlVo mySqlVo);

    void deleteMySql(MySqlVo mySqlVo);

    MySqlPojo findById(MySqlVo mySqlVo,Boolean mainDb);

    ServiceResult<PageInfo<SqlServerPojo>> findByPage(MySqlVo mySqlVo,Boolean mainDb);

    void deleteBySystemId(Integer systemId);

    void deleteByEnvironmentId(Integer environmentId);

    /**
     * 查询mysql信息列表
     * @return 返回查询结果
     */
    ServiceResult<List<MysqlInfoRespPojo>> queryMysqlList(Boolean mainDb);
}