package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.handler.MysqlAvailableJobHandler;
import com.innovation.ic.b1b.monitor.base.mapper.MysqlAvailableJobMapper;
import com.innovation.ic.b1b.monitor.base.model.Mysql;
import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.MysqlAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.TableNameConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.EnableEnum;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MysqlAvailableJobService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobUpdateVo;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   mysql可用任务表Service实现类
 * @author linuo
 * @time   2023年3月15日15:06:41
 */
@Service
public class MysqlAvailableJobServiceImpl extends ServiceImpl<MysqlAvailableJobMapper, MysqlAvailableJob> implements MysqlAvailableJobService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询mysql监控任务列表
     * @param mysqlAvailableJobListQueryVo 查询mysql监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<MysqlAvailableJobListRespData>> queryList(MysqlAvailableJobListQueryVo mysqlAvailableJobListQueryVo, Boolean isMain) {
        PageHelper.startPage(mysqlAvailableJobListQueryVo.getPageNo(), mysqlAvailableJobListQueryVo.getPageSize());

        // 查询mysql可用任务列表数据
        List<MysqlAvailableJobListRespData> data = serviceHelper.getMysqlAvailableJobMapper().queryList(mysqlAvailableJobListQueryVo);
        PageInfo<MysqlAvailableJobListRespData> pageInfo = new PageInfo<>(data);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 添加mysql监控任务
     * @param mysqlAvailableJobAddVo 添加mysql可用任务的Vo类
     * @return 返回添加结果
     */
    @Override
    public ServiceResult<Boolean> add(MysqlAvailableJobAddVo mysqlAvailableJobAddVo) {
        boolean result = Boolean.FALSE;
        String message;

        Boolean aBoolean = judgeIfHaveSameData(null, mysqlAvailableJobAddVo.getMysqlId());
        if(aBoolean){
            message = "当前mysql监控任务已存在，请勿重复添加";
        }else{
            // 校验mysqlId是否存在
            Mysql mysql = serviceHelper.getMysqlMapper().selectById(mysqlAvailableJobAddVo.getMysqlId());
            if(mysql != null){
                MysqlAvailableJob mysqlAvailableJob = new MysqlAvailableJob();
                BeanUtils.copyProperties(mysqlAvailableJobAddVo, mysqlAvailableJob);
                if(mysqlAvailableJob.getEnable() == null){
                    mysqlAvailableJob.setEnable(EnableEnum.NO.getCode());
                }
                mysqlAvailableJob.setCreateTime(new Date(System.currentTimeMillis()));
                int insert = serviceHelper.getMysqlAvailableJobMapper().insert(mysqlAvailableJob);
                if(insert > 0){
                    message = ServiceResult.INSERT_SUCCESS;
                    result = Boolean.TRUE;

                    String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + mysqlAvailableJob.getId();

                    // 组装任务参数
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.put(JobDataMapConstant.ID, mysqlAvailableJob.getId());

                    serviceHelper.getQuartzManager().createScheduleJob(jobId, MysqlAvailableJobHandler.class.getName(), "MysqlAvailableJobHandler", jobDataMap, mysqlAvailableJob.getScheduleExpression());
                }else{
                    message = ServiceResult.INSERT_FAIL;
                }
            }else{
                message = "mysql信息不存在，请配置mysql信息后重新添加mysql监控任务";
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 删除mysql监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> delete(Integer id) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        int i = serviceHelper.getMysqlAvailableJobMapper().deleteById(id);
        if(i > 0){
            logger.info("已删除id为[{}]mysql监控任务", id);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;

            try {
                // 删除任务
                String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + id;
                serviceHelper.getQuartzManager().deleteScheduleJob(jobId);
            }catch (Exception e){
                logger.info("删除mysql监控后台任务失败,原因:", e);
            }
        }else{
            logger.info("删除id为[{}]mysql监控任务失败，数据不存在", id);
        }

        return ServiceResult.ok(result, message);
    }

    /**
     * 获取mysql监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<MysqlAvailableJobInfoRespPojo> info(Integer id, Boolean isMain) {
        MysqlAvailableJobInfoRespPojo result = serviceHelper.getMysqlAvailableJobMapper().info(id);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 更新mysql监控任务
     * @param mysqlAvailableJobUpdateVo 更新mysql可用任务的Vo类
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> update(MysqlAvailableJobUpdateVo mysqlAvailableJobUpdateVo) {
        Boolean aBoolean = judgeIfHaveSameData(mysqlAvailableJobUpdateVo.getId(), mysqlAvailableJobUpdateVo.getMysqlId());
        if(aBoolean){
            ServiceResult<Boolean> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setMessage("当前mysql监控任务已存在，请勿重复添加");
            return serviceResult;
        }else{
            // 校验mysql是否存在
            Mysql mysql = serviceHelper.getMysqlMapper().selectById(mysqlAvailableJobUpdateVo.getMysqlId());
            if(mysql != null){
                // 查询历史数据
                MysqlAvailableJob historyData = serviceHelper.getMysqlAvailableJobMapper().selectById(mysqlAvailableJobUpdateVo.getId());

                int i = serviceHelper.getMysqlAvailableJobMapper().updateInfo(mysqlAvailableJobUpdateVo);
                if(i > 0){
                    logger.info("更新id为[{}]mysql监控任务成功", mysqlAvailableJobUpdateVo.getId());

                    Integer enable = mysqlAvailableJobUpdateVo.getEnable();
                    // 原来是启用现在关闭时需要暂停后台任务
                    if(historyData.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && enable.intValue() == EnableEnum.NO.getCode().intValue()){
                        String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + mysqlAvailableJobUpdateVo.getId();
                        logger.info("暂停后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().pauseScheduleJob(jobId);
                    }else if(historyData.getEnable().intValue() == EnableEnum.NO.getCode().intValue() && enable.intValue() == EnableEnum.YES.getCode().intValue()) {
                        // 原来是关闭现在启用任务时需要恢复后台任务
                        String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + mysqlAvailableJobUpdateVo.getId();
                        logger.info("恢复后台任务,jobId:[{}]", jobId);
                        serviceHelper.getQuartzManager().resumeScheduleJob(jobId);
                    }else{
                        String scheduleExpression = mysqlAvailableJobUpdateVo.getScheduleExpression();
                        if(mysqlAvailableJobUpdateVo.getEnable().intValue() == EnableEnum.YES.getCode().intValue() && !historyData.getScheduleExpression().equals(scheduleExpression)){
                            String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + mysqlAvailableJobUpdateVo.getId();
                            serviceHelper.getQuartzManager().updateScheduleJob(jobId, scheduleExpression);
                        }
                    }

                    return ServiceResult.ok(Boolean.TRUE, ServiceResult.UPDATE_SUCCESS);
                }else{
                    logger.info("更新id为[{}]mysql监控任务失败", mysqlAvailableJobUpdateVo.getId());
                    return ServiceResult.ok(Boolean.FALSE, ServiceResult.UPDATE_FAIL);
                }
            }else{
                logger.info("mysqlId=[{}]的信息不存在", mysqlAvailableJobUpdateVo.getMysqlId());
                return ServiceResult.error(ServiceResult.UPDATE_FAIL);
            }
        }
    }

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    @Override
    public ServiceResult<Boolean> executeJob(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        try {
            String jobId = TableNameConstant.MYSQL_AVAILABLE_JOB + "_" + id;
            serviceHelper.getQuartzManager().runOnce(jobId);
        }catch (Exception e){
            serviceResult.setMessage(ServiceResult.OPERATE_FAIL);
            serviceResult.setResult(Boolean.FALSE);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public List<MysqlAvailableJob> findByMysqlId(Integer id) {
        Map<String,Object> param = new HashMap<>();
        param.put("mysql_id",id);
        return this.baseMapper.selectByMap(param);
    }

    /**
     * 判断是否有相同数据
     * @param id 主键id
     * @param mysqlId mysqlId
     * @return 返回判断结果
     */
    private Boolean judgeIfHaveSameData(Integer id, Integer mysqlId){
        Boolean result = Boolean.FALSE;

        QueryWrapper<MysqlAvailableJob> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(MysqlAvailableJobConstant.MYSQL_ID, mysqlId);
        if(id != null){
            queryWrapper.ne(MysqlAvailableJobConstant.ID, id);
        }
        Integer selectCount = serviceHelper.getMysqlAvailableJobMapper().selectCount(queryWrapper);
        if(selectCount > 0){
            result = Boolean.TRUE;
        }

        return result;
    }
}