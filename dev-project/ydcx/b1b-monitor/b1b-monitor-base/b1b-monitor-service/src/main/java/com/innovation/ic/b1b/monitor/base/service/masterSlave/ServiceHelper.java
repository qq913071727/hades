package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.innovation.ic.b1b.framework.manager.QuartzManager;
import com.innovation.ic.b1b.monitor.base.mapper.*;
import lombok.Getter;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * @desc   ServiceHelper
 * @author linuo
 * @time   2023年3月13日15:44:10
 */
@Getter
@Component
public class ServiceHelper {

    /********************************************* manager ********************************************/
    @Resource
    private QuartzManager quartzManager;

    /********************************************* mapper ********************************************/
    @Resource
    private ServerPerformanceJobMapper serverPerformanceJobMapper;

    @Resource
    private ServerPerformanceJobLogMapper serverPerformanceJobLogMapper;

    @Resource
    private ServerPerformanceJobLogDetailMapper serverPerformanceJobLogDetailMapper;

    @Resource
    private SqlServerAvailableJobMapper sqlServerAvailableJobMapper;

    @Resource
    private ServerMapper serverMapper;

    @Resource
    private SqlServerMapper sqlServerMapper;

    @Resource
    private SqlServerAvailableJobLogMapper sqlServerAvailableJobLogMapper;

    @Resource
    private MysqlAvailableJobMapper mysqlAvailableJobMapper;

    @Resource
    private MysqlAvailableJobLogMapper mysqlAvailableJobLogMapper;

    @Resource
    private MySqlMapper mysqlMapper;

    @Resource
    private PageVisitLogMapper pageVisitLogMapper;

    @Resource
    private FrontEndLogMapper frontEndLogMapper;

    @Resource
    private UserLoginLogMapper userLoginLogMapper;

    @Resource
    private InterfaceCallLogMapper interfaceCallLogMapper;

    @Resource
    private ElasticsearchAvailableJobMapper elasticsearchAvailableJobMapper;

    @Resource
    private ElasticsearchAvailableJobLogMapper elasticsearchAvailableJobLogMapper;

    @Resource
    private ElasticsearchMapper elasticsearchMapper;

    @Resource
    private MongodbMapper mongodbMapper;

    @Resource
    private MongodbAvailableJobMapper mongodbAvailableJobMapper;

    @Resource
    private LogMonitorJobMapper logMonitorJobMapper;

    @Resource
    private MongodbAvailableJobLogMapper mongodbAvailableJobLogMapper;

    @Resource
    private RabbitmqAvailableJobMapper rabbitmqAvailableJobMapper;

    @Resource
    private RabbitmqAvailableJobLogMapper rabbitmqAvailableJobLogMapper;

    @Resource
    private RabbitmqMapper rabbitmqMapper;

    @Resource
    private RedisMapper redisMapper;

    @Resource
    private RedisAvailableJobMapper redisAvailableJobMapper;

    @Resource
    private RedisAvailableJobLogMapper redisAvailableJobLogMapper;

    @Resource
    private ServiceActiveJobMapper serviceActiveJobMapper;

    @Resource
    private ServiceActiveJobLogMapper serviceActiveJobLogMapper;

    @Resource
    private ApiAvailableJobMapper apiAvailableJobMapper;

    @Resource
    private ApiAvailableJobLogMapper apiAvailableJobLogMapper;

    @Resource
    private ApiMapper apiMapper;

    @Resource
    private ZookeeperAvailableJobMapper zookeeperAvailableJobMapper;

    @Resource
    private ZookeeperAvailableJobLogMapper zookeeperAvailableJobLogMapper;

    @Resource
    private KafkaMapper kafkaMapper;

    @Resource
    private KafkaAvailableJobMapper kafkaAvailableJobMapper;

    @Resource
    private KafkaAvailableJobLogMapper kafkaAvailableJobLogMapper;

    @Resource
    private CanalMapper canalMapper;

    @Resource
    private CanalAvailableJobMapper canalAvailableJobMapper;

    @Resource
    private CanalAvailableJobLogMapper canalAvailableJobLogMapper;

    @Resource
    private ZookeeperMapper zookeeperMapper;
}