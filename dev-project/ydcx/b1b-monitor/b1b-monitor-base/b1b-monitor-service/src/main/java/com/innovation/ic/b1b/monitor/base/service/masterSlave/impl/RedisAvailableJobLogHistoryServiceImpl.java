package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.monitor.base.mapper.RedisAvailableJobLogHistoryMapper;
import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJobLogHistory;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RedisAvailableJobLogHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class RedisAvailableJobLogHistoryServiceImpl extends ServiceImpl<RedisAvailableJobLogHistoryMapper, RedisAvailableJobLogHistory> implements RedisAvailableJobLogHistoryService {
    @Override
    public void deleteByRedisAvailableJobId(Integer redisAvailableJobId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("redis_available_job_id", redisAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}
