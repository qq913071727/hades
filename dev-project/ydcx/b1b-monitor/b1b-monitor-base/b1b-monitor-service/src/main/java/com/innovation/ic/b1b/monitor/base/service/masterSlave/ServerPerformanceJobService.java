package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobUpdateVo;

/**
 * @desc   服务器性能任务表Service
 * @author linuo
 * @time   2023年3月9日16:17:15
 */
public interface ServerPerformanceJobService {
    /**
     * 查询服务器性能任务列表数据
     * @param serverPerformanceJobListQueryVo 服务器监控任务查询列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<ServerPerformanceJobListRespData>> queryList(ServerPerformanceJobListQueryVo serverPerformanceJobListQueryVo, Boolean isMain);

    /**
     * 添加服务器监控任务
     * @param serverPerformanceJobAddVo 添加服务器监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(ServerPerformanceJobAddVo serverPerformanceJobAddVo);

    /**
     * 查看服务器监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<ServerPerformanceJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 删除服务器监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 更新服务器监控任务
     * @param serverPerformanceJobUpdateVo 更新服务器监控任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(ServerPerformanceJobUpdateVo serverPerformanceJobUpdateVo);

    /**
     * 执行任务
     * @param id 执行任务id
     * @return 返回处理结果
     */
    ServiceResult<Boolean> executeJob(Integer id);
}
