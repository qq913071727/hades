package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.ServerPerformanceJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.ServerPerformanceJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLog.ServerPerformanceJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLogDetail.ServerPerformanceJobLogDetailInfoData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServerPerformanceJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJobLog.ServerPerformanceJobLogListQueryVo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author linuo
 * @desc 服务器性能任务日志表Service实现类
 * @time 2023年3月14日11:17:38
 */
@Service
public class ServerPerformanceJobLogServiceImpl extends ServiceImpl<ServerPerformanceJobLogMapper, ServerPerformanceJobLog> implements ServerPerformanceJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询服务器性能任务日志表数据
     *
     * @param serverPerformanceJobLogListQueryVo 查询服务器性能任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<ServerPerformanceJobLogListRespData>> queryList(ServerPerformanceJobLogListQueryVo serverPerformanceJobLogListQueryVo) {
        PageHelper.startPage(serverPerformanceJobLogListQueryVo.getPageNo(), serverPerformanceJobLogListQueryVo.getPageSize());

        // 查询服务器性能任务日志表数据
        List<ServerPerformanceJobLogListRespData> serverPerformanceJobs = serviceHelper.getServerPerformanceJobLogMapper().queryList(serverPerformanceJobLogListQueryVo);
        if (serverPerformanceJobs != null && serverPerformanceJobs.size() > 0) {
            for (ServerPerformanceJobLogListRespData data : serverPerformanceJobs) {
                if (!Strings.isNullOrEmpty(data.getUsedRamPercentage())) {
                    // 格式化为百分比
                    String s = handlePercentageData(data.getUsedRamPercentage());
                    data.setUsedRamPercentage(s);
                }
                if (!Strings.isNullOrEmpty(data.getUsedDiskSpaceMaxPercentage())) {
                    // 格式化为百分比
                    String s = handlePercentageData(data.getUsedDiskSpaceMaxPercentage());
                    data.setUsedDiskSpaceMaxPercentage(s);
                }

                // 查询服务器性能任务日志详情
                List<ServerPerformanceJobLogDetailInfoData> detail = serviceHelper.getServerPerformanceJobLogDetailMapper().getDataByLogId(data.getId());
                if (detail != null && detail.size() > 0) {
                    for (ServerPerformanceJobLogDetailInfoData serverPerformanceJobLogDetailInfoData : detail) {
                        String usedDiskSpacePercentage = serverPerformanceJobLogDetailInfoData.getUsedDiskSpacePercentage();
                        // 格式化为百分比
                        String s = handlePercentageData(usedDiskSpacePercentage);
                        serverPerformanceJobLogDetailInfoData.setUsedDiskSpacePercentage(s);
                    }
                }
                data.setDetail(detail);
            }
        }

        PageInfo<ServerPerformanceJobLogListRespData> pageInfo = new PageInfo<>(serverPerformanceJobs);
        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 格式化为百分比
     *
     * @param percentage 百分比
     * @return 返回处理结果
     */
    private String handlePercentageData(String percentage) {
        BigDecimal bigDecimal = new BigDecimal(percentage).setScale(0, BigDecimal.ROUND_HALF_UP);
        return bigDecimal.toString() + "%";
    }

    /**
     * 查询服务1小时内不可用数据量(性能/内存超过95%)
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findServerClose(Date date, Date behindData) {
        Map<Integer, ServerPerformanceJobLog> map = new HashMap<>();//用于存放结果数据
        Map<Integer, ServerPerformanceJobLog> serverMap = new HashMap<>();//用于筛选数据
        QueryWrapper<ServerPerformanceJobLog> serverPerformanceJobLogQueryWrapper = new QueryWrapper<>();
        serverPerformanceJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        serverPerformanceJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
//        serverPerformanceJobLogQueryWrapper.ge("used_ram_percentage", Home.USED_RAM_PERCENTAGE).or().ge("used_disk_space_max_percentage", Home.USED_DISK_SPACE_MAX_PERCENTAGE);
        List<ServerPerformanceJobLog> serverPerformanceJobLogs = serviceHelper.getServerPerformanceJobLogMapper().selectList(serverPerformanceJobLogQueryWrapper);
        for (ServerPerformanceJobLog serverPerformanceJobLog : serverPerformanceJobLogs) {
            if (map.containsKey(serverPerformanceJobLog.getServerPerformanceJobId())) {//服务器性能任务id是有重复的
                ServerPerformanceJobLog mapData = map.get(serverPerformanceJobLog.getServerPerformanceJobId());//map中数据
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), serverPerformanceJobLog.getStartTime())) {//true map近
                    continue;
                } else {//list 近
                    map.put(serverPerformanceJobLog.getServerPerformanceJobId(), serverPerformanceJobLog);
                }
            } else {//服务器性能任务id不重复
                map.put(serverPerformanceJobLog.getServerPerformanceJobId(), serverPerformanceJobLog);
            }
        }
        for (Integer key : map.keySet()) {
            ServerPerformanceJobLog serverPerformanceJobLog = map.get(key);
            if (serverPerformanceJobLog.getUsedRamPercentage() != null && serverPerformanceJobLog.getUsedDiskSpaceMaxPercentage() != null) {
                if (serverPerformanceJobLog.getUsedRamPercentage() > Home.USED_RAM_PERCENTAGE || serverPerformanceJobLog.getUsedDiskSpaceMaxPercentage() > Home.USED_DISK_SPACE_MAX_PERCENTAGE) {//筛选不过95的数据 全不要
                    serverMap.put(serverPerformanceJobLog.getId(), serverPerformanceJobLog);//筛选数据
                }
            }
        }
        Integer number = serverMap.size();
        return number;
    }
}