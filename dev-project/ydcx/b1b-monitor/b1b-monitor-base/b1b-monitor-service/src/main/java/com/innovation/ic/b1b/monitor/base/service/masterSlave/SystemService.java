package com.innovation.ic.b1b.monitor.base.service.masterSlave;


import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.System;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.SystemVo;

import java.util.List;

public interface SystemService{


    ServiceResult addSystem(SystemVo systemVo);

    void deleteSystem(SystemVo systemVo);

    System findById(SystemVo systemVo,Boolean mainDb);

    ServiceResult<PageInfo<System>> findByPage(SystemVo systemVo,Boolean mainDb);

    List<DropDownPojo> dropDown(DropDownVo dropDownVo,Boolean mainDb);

}
