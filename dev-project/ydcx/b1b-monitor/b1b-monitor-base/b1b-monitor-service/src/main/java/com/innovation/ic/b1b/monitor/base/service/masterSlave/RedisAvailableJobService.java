package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJob.RedisAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJob.RedisAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobUpdateVo;
import java.util.List;

public interface RedisAvailableJobService {
    /**
     * 添加redis监控任务
     * @param redisAvailableJobAddVo 添加redis监控任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(RedisAvailableJobAddVo redisAvailableJobAddVo);

    /**
     * 查询redis监控任务列表
     * @param redisAvailableJobListQueryVo 查询redis监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<RedisAvailableJobListRespData>> queryList(RedisAvailableJobListQueryVo redisAvailableJobListQueryVo, Boolean isMain);

    /**
     * 删除redis监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取redis监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<RedisAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新redis监控任务
     * @param redisAvailableJobUpdateVo 更新redis可用任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(RedisAvailableJobUpdateVo redisAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);

    List<RedisAvailableJob> findByRedisId(Integer redisId);
}