package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.SystemMapper;
import com.innovation.ic.b1b.monitor.base.model.System;
import com.innovation.ic.b1b.monitor.base.pojo.DropDownPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.EnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.MySqlService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.RelSystemEnvironmentService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.SqlServerService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.SystemService;
import com.innovation.ic.b1b.monitor.base.vo.DropDownVo;
import com.innovation.ic.b1b.monitor.base.vo.SystemVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SystemServiceImpl extends ServiceImpl<SystemMapper, System> implements SystemService {


    @Resource
    private EnvironmentService environmentService;


    @Resource
    private RelSystemEnvironmentService relSystemEnvironmentService;

    @Resource
    private MySqlService mySqlService;

    @Resource
    private SqlServerService sqlServerService;


    @Override
    public ServiceResult addSystem(SystemVo systemVo) {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = systemVo.getId();
        System system = new System();
        system.setName(systemVo.getName());
        system.setId(systemVo.getId());
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", systemVo.getName());
        if (id == null) {
            Integer count = this.baseMapper.checkRepeat(paramMap);
            if (count > 0) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字重复");
                return serviceResult;
            }
            this.baseMapper.addSystem(system);
        } else {
            //修改的时候也得校验重复
            paramMap.put("id", systemVo.getId());
            int count = this.baseMapper.checkRepeat(paramMap);
            if (count > 0) {
                serviceResult.setSuccess(Boolean.FALSE);
                serviceResult.setMessage("名字重复");
                return serviceResult;
            }
            this.baseMapper.updateSystem(system);
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("成功");
        return serviceResult;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSystem(SystemVo systemVo) {
        Integer id = systemVo.getId();
        this.baseMapper.deleteSystem(id);
        environmentService.deleteBySystemId(id);
    }

    @Override
    public System findById(SystemVo systemVo,Boolean mainDb) {
        return this.baseMapper.findById(systemVo.getId());
    }

    @Override
    public ServiceResult<PageInfo<System>> findByPage(SystemVo systemVo,Boolean mainDb) {
        ServiceResult<PageInfo<System>> serviceResult = new ServiceResult<>();

        PageHelper.startPage(systemVo.getPageNo(), systemVo.getPageSize());
        List<System> environmentPojoList = this.baseMapper.findByPage(systemVo);
        PageInfo<System> pageInfo = new PageInfo<>(environmentPojoList);
        serviceResult.setResult(pageInfo);
        serviceResult.setMessage("查询成功");
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public List<DropDownPojo> dropDown(DropDownVo dropDownVo,Boolean mainDb) {
        Integer type = dropDownVo.getType();
        List<System> list = new ArrayList<>();
        List<DropDownPojo> resultList = new ArrayList<>();
        List<Integer> systemIds = null;
        if (type == null) {
            //查询所有
            list = this.baseMapper.findByPage(new SystemVo());
        }
        //根据环境查询所有系统
        if (type != null && type == 2) {
            //查询所有
            Integer id = dropDownVo.getId();
            systemIds = relSystemEnvironmentService.findByEnvId(id);
            if (CollectionUtils.isEmpty(systemIds)) {
                return resultList;
            }
            list = this.baseMapper.findByIds(systemIds);
        }

        if (type != null && type == 3) {
            //根据服务查询所有系统id
            Integer id = dropDownVo.getId();
            systemIds = relSystemEnvironmentService.findByServiceId(id);
            if (CollectionUtils.isEmpty(systemIds)) {
                return resultList;
            }
            list = this.baseMapper.findByIds(systemIds);
        }
        for (System system : list) {
            DropDownPojo dropDownPojo = new DropDownPojo();
            BeanUtil.copyProperties(system, dropDownPojo);
            resultList.add(dropDownPojo);
        }
        return resultList;
    }

}
