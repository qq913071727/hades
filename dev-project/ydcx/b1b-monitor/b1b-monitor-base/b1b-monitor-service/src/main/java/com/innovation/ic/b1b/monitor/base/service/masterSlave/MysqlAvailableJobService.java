package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobAddVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobUpdateVo;
import java.util.List;

/**
 * @desc   mysql可用任务表Service
 * @author linuo
 * @time   2023年3月15日15:04:32
 */
public interface MysqlAvailableJobService {
    /**
     * 查询mysql监控任务列表
     * @param mysqlAvailableJobListQueryVo 查询mysql监控任务列表的Vo类
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<PageInfo<MysqlAvailableJobListRespData>> queryList(MysqlAvailableJobListQueryVo mysqlAvailableJobListQueryVo, Boolean isMain);

    /**
     * 添加mysql监控任务
     * @param mysqlAvailableJobAddVo 添加mysql可用任务的Vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> add(MysqlAvailableJobAddVo mysqlAvailableJobAddVo);

    /**
     * 删除mysql监控任务
     * @param id 主键id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Integer id);

    /**
     * 获取mysql监控任务详情
     * @param id 主键id
     * @param isMain 是否查询主库
     * @return 返回查询结果
     */
    ServiceResult<MysqlAvailableJobInfoRespPojo> info(Integer id, Boolean isMain);

    /**
     * 更新mysql监控任务
     * @param mysqlAvailableJobUpdateVo 更新mysql可用任务的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> update(MysqlAvailableJobUpdateVo mysqlAvailableJobUpdateVo);

    /**
     * 执行任务
     * @param id 主键id
     * @return 返回执行结果
     */
    ServiceResult<Boolean> executeJob(Integer id);

    List<MysqlAvailableJob> findByMysqlId(Integer id);
}
