package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.mapper.LogMonitorJobLogMapper;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.LogMonitorJobLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogVo;
import com.innovation.ic.b1b.monitor.base.pojo.LogMonitorJobLogPojo;
import com.innovation.ic.b1b.monitor.base.model.LogMonitorJobLog;
import org.springframework.beans.BeanUtils;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@Service
public class LogMonitorJobLogServiceImpl extends ServiceImpl<LogMonitorJobLogMapper, LogMonitorJobLog> implements LogMonitorJobLogService {

    @Resource
    private LogMonitorJobLogMapper baseMapper;

    @Override
    public ServiceResult<PageInfo<LogMonitorJobLogPojo>> pageInfo(int page, int rows, LogMonitorJobLogVo dto) {
        PageHelper.startPage(page, rows);
        List<LogMonitorJobLog> list = baseMapper.pageInfo(dto);
        List<LogMonitorJobLogPojo> targetList = BeanPropertiesUtil.convertList(list,LogMonitorJobLogPojo.class);
        PageInfo<LogMonitorJobLogPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<LogMonitorJobLogPojo>> list(LogMonitorJobLogVo dto) {
        List<LogMonitorJobLog> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, LogMonitorJobLogPojo.class), "ok");
    }

    private Wrapper<LogMonitorJobLog> getWrapper(LogMonitorJobLogVo dto) {
        LambdaQueryWrapper<LogMonitorJobLog> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(LogMonitorJobLog::getId, dto.getId());
        return lambdaQueryWrapper;
    }


    @Override
    public ServiceResult<LogMonitorJobLogPojo> get(String id) {
        LogMonitorJobLog entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, LogMonitorJobLogPojo.class), "ok");
    }

    @Override
    public void save(LogMonitorJobLogVo dto) {
        LogMonitorJobLog entity = BeanPropertiesUtil.convert(dto, LogMonitorJobLog. class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(LogMonitorJobLogVo dto) {
        LogMonitorJobLog entity = BeanPropertiesUtil.convert(dto, LogMonitorJobLog. class);

        baseMapper.updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}