package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.model.CellPhone;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.cellPhone.AlarmCellPhoneQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.CellPhoneVo;
import java.util.List;

public interface CellPhoneService {
    ServiceResult addCellPhone(CellPhoneVo cellPhoneVo);

    void deleteCellPhone(CellPhoneVo cellPhoneVo);

    CellPhone findById(CellPhoneVo cellPhoneVo,Boolean mainDb);

    ServiceResult<PageInfo<CellPhone>> findByPage(CellPhoneVo cellPhoneVo,Boolean mainDb);

    /**
     * 查询报警手机号列表
     * @return 返回查询结果
     */
    ServiceResult<List<AlarmCellPhoneQueryRespPojo>> queryAlarmCellPhoneList(Boolean mainDb);
}