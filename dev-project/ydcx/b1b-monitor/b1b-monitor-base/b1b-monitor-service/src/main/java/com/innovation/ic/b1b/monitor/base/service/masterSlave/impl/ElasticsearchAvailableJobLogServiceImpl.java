package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.ElasticsearchAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.ElasticsearchAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJobLog.ElasticsearchAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ElasticsearchAvailableJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJobLog.ElasticsearchAvailableJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ElasticsearchAvailableJobLogServiceImpl extends ServiceImpl<ElasticsearchAvailableJobLogMapper, ElasticsearchAvailableJobLog> implements ElasticsearchAvailableJobLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * elasticsearch可用任务日志表1小时内不可用数据量
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findElasticsearchClose(Date date, Date behindData) {
        Map<Integer, ElasticsearchAvailableJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<ElasticsearchAvailableJobLog> elasticsearchAvailableJobLogQueryWrapper = new QueryWrapper<>();
        elasticsearchAvailableJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        elasticsearchAvailableJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        List<ElasticsearchAvailableJobLog> elasticsearchAvailableJobLogs = baseMapper.selectList(elasticsearchAvailableJobLogQueryWrapper);//当前一小时内所有的数据
        for (ElasticsearchAvailableJobLog elasticsearchAvailableJobLog : elasticsearchAvailableJobLogs) {
            if (map.containsKey(elasticsearchAvailableJobLog.getElasticsearchAvailableJobId())) {//elasticsearch可用任务id是有重复的
                ElasticsearchAvailableJobLog mapData = map.get(elasticsearchAvailableJobLog.getElasticsearchAvailableJobId());
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), elasticsearchAvailableJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(elasticsearchAvailableJobLog.getElasticsearchAvailableJobId(), elasticsearchAvailableJobLog);
                    }
                } else {//list 近
                    if (elasticsearchAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(elasticsearchAvailableJobLog.getElasticsearchAvailableJobId(), elasticsearchAvailableJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//不重复
                if (elasticsearchAvailableJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(elasticsearchAvailableJobLog.getElasticsearchAvailableJobId(), elasticsearchAvailableJobLog);
                } else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    /**
     * 查询elasticsearch可用任务日志列表
     * @param elasticsearchAvailableJobLogListQueryVo 查询elasticsearch可用任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<PageInfo<ElasticsearchAvailableJobLogListRespData>> queryList(ElasticsearchAvailableJobLogListQueryVo elasticsearchAvailableJobLogListQueryVo) {
        PageHelper.startPage(elasticsearchAvailableJobLogListQueryVo.getPageNo(), elasticsearchAvailableJobLogListQueryVo.getPageSize());

        // 查询elasticsearch可用任务日志列表数据
        List<ElasticsearchAvailableJobLogListRespData> elasticsearchAvailableJobLogListRespData = serviceHelper.getElasticsearchAvailableJobLogMapper().queryList(elasticsearchAvailableJobLogListQueryVo);
        PageInfo<ElasticsearchAvailableJobLogListRespData> pageInfo = new PageInfo<>(elasticsearchAvailableJobLogListRespData);

        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }

    @Override
    public void deleteByElasticsearchAvailableJobId(Integer elasticsearchAvailableJobId) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("elasticsearch_available_job_id",elasticsearchAvailableJobId);
        this.baseMapper.deleteByMap(paramMap);
    }
}