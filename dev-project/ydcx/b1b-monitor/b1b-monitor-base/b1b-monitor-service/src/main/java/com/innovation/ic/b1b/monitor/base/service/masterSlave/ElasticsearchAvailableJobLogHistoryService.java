package com.innovation.ic.b1b.monitor.base.service.masterSlave;


public interface ElasticsearchAvailableJobLogHistoryService {

    void deleteByElasticsearchAvailableJobId(Integer elasticsearchAvailableJobId);
}
