package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.RabbitmqPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmq.RabbitmqInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.RabbitmqVo;
import java.util.List;

public interface RabbitmqService {

    ServiceResult addRabbitmq(RabbitmqVo rabbitmqVo);

    void deleteRabbitmq(RabbitmqVo rabbitmqVo);

    ServiceResult<PageInfo<RabbitmqPojo>> findByPage(RabbitmqVo rabbitmqVo,Boolean mainDb);

    /**
     * 查询rabbitmq信息列表
     * @return 返回查询结果
     */
    ServiceResult<List<RabbitmqInfoRespPojo>> queryRabbitmqList(Boolean mainDb);

    void deleteByEnvironmentId(Integer id);
}