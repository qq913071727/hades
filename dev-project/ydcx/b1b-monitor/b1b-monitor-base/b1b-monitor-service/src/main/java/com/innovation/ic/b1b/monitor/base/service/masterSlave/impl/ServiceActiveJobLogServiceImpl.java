package com.innovation.ic.b1b.monitor.base.service.masterSlave.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.ServiceActiveJobLogMapper;
import com.innovation.ic.b1b.monitor.base.model.ServiceActiveJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.Home;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJobLog.ServiceActiveJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceActiveJobLogService;
import com.innovation.ic.b1b.monitor.base.service.masterSlave.ServiceHelper;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJobLog.ServiceActiveJobLogListQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author swq
 * @desc 服务存活任务日志表Service实现类
 * @time 2023年3月20日10:06:41
 */
@Service
@Slf4j
public class ServiceActiveJobLogServiceImpl extends ServiceImpl<ServiceActiveJobLogMapper, ServiceActiveJobLog> implements ServiceActiveJobLogService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询服务存活任务日志表1小时内不可用数据量
     *
     * @param date       当前时间
     * @param behindData 一小时前时间
     * @return 返回查询结果
     */
    @Override
    public Integer findServerClose(Date date, Date behindData) {
        Map<Integer, ServiceActiveJobLog> map = new HashMap<>();//用于筛选数据
        QueryWrapper<ServiceActiveJobLog> serviceActiveJobLogQueryWrapper = new QueryWrapper<>();
        serviceActiveJobLogQueryWrapper.ge("start_time", behindData);//大于等于当前时间
        serviceActiveJobLogQueryWrapper.le("start_time", date);//小于等于当前时间
        serviceActiveJobLogQueryWrapper.eq("active", Home.STATUS_TYPE);
        List<ServiceActiveJobLog> serviceActiveJobLogs = baseMapper.selectList(serviceActiveJobLogQueryWrapper);
        for (ServiceActiveJobLog serviceActiveJobLog : serviceActiveJobLogs) {
            if (map.containsKey(serviceActiveJobLog.getServiceActiveJobId())) {//服务存活任务id是有重复的
                ServiceActiveJobLog mapData = map.get(serviceActiveJobLog.getServiceActiveJobId());//map中数据
                if (DateUtils.contrastDate(behindData, mapData.getStartTime(), serviceActiveJobLog.getStartTime())) {//true map近
                    if (mapData.getActive() == Home.STATUS_TYPE) {//因当前map中状态为0 可以直接跳过
                        continue;
                    } else {//因当前map中状态不为0 需要覆盖
                        map.put(serviceActiveJobLog.getServiceActiveJobId(), serviceActiveJobLog);
                    }
                } else {//list 近
                    if (serviceActiveJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                        //判断当前list状态为0 需要把list中数据存入
                        map.put(serviceActiveJobLog.getServiceActiveJobId(), serviceActiveJobLog);
                    } else {// list不为0直接跳过
                        continue;
                    }
                }
            } else {//服务器性能任务id不重复
                if (serviceActiveJobLog.getActive() == Home.STATUS_TYPE) {//list中状态为0 进行覆盖
                    map.put(serviceActiveJobLog.getServiceActiveJobId(), serviceActiveJobLog);
                } else {//反之直接跳过
                    continue;
                }
            }
        }
        Integer number = map.size();
        return number;
    }

    /**
     * 查询服务存活日志表数据
     *
     * @param serviceActiveJobLogListQueryVo 查询服务存活日志表查询列表的Vo类
     * @return 返回查询结果
     */
    public ServiceResult<PageInfo<ServiceActiveJobLogListRespData>> queryList(ServiceActiveJobLogListQueryVo serviceActiveJobLogListQueryVo) {
        PageHelper.startPage(serviceActiveJobLogListQueryVo.getPageNo(),serviceActiveJobLogListQueryVo.getPageSize());
        // 查询服务存活任务表数据
        List<ServiceActiveJobLogListRespData> data = serviceHelper.getServiceActiveJobLogMapper().queryList(serviceActiveJobLogListQueryVo);
        PageInfo<ServiceActiveJobLogListRespData> pageInfo = new PageInfo<>(data);
        return ServiceResult.ok(pageInfo, ServiceResult.SELECT_SUCCESS);
    }
}
