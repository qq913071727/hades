package com.innovation.ic.b1b.monitor.base.service.masterSlave;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.monitor.base.pojo.SqlServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServer.SqlServerInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.SqlServerVo;
import java.util.List;

public interface SqlServerService {

    ServiceResult addSqlServer(SqlServerVo sqlServerVo);

    void deleteSqlServer(SqlServerVo sqlServerVo);

    SqlServerPojo findById(SqlServerVo sqlServerVo,Boolean mainDb);

    ServiceResult<PageInfo<SqlServerPojo>> findByPage(SqlServerVo sqlServerVo,Boolean mainDb);

    void deleteBySystemId(Integer systemId);

    void deleteByEnvironmentId(Integer environmentId);

    /**
     * 查询sql server信息列表
     * @return 返回查询结果
     */
    ServiceResult<List<SqlServerInfoRespPojo>> querySqlServerList(Boolean mainDb);
}