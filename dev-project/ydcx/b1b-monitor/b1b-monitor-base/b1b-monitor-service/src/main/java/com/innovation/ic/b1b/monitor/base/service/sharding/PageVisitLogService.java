package com.innovation.ic.b1b.monitor.base.service.sharding;

import com.innovation.ic.b1b.monitor.base.model.PageVisitLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.monitor.base.pojo.variable.pageVisitLog.PageVisitLogPagePojo;
import com.innovation.ic.b1b.monitor.base.vo.PageVisitLogVo;

public interface PageVisitLogService {

    /**
     * 添加PageVisitLog类型对象
     * @param pageVisitLog
     */
    ServiceResult<Integer> savePageVisitLog(PageVisitLog pageVisitLog);

    /**
     * 分页显示
     * @param pageVisitLogVo
     * @return
     */
    ServiceResult<PageVisitLogPagePojo> page(PageVisitLogVo pageVisitLogVo);
}
