package com.innovation.ic.b1b.monitor.base.handler;

import com.alibaba.druid.pool.DruidDataSource;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.JdbcManager;
import com.innovation.ic.b1b.monitor.base.mapper.SqlServerAvailableJobLogMapper;
import com.innovation.ic.b1b.monitor.base.mapper.SqlServerAvailableJobMapper;
import com.innovation.ic.b1b.monitor.base.mapper.SqlServerMapper;
import com.innovation.ic.b1b.monitor.base.model.SqlServer;
import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJob;
import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.constant.JobDataMapConstant;
import com.innovation.ic.b1b.monitor.base.pojo.constant.SqlServerAvailableJobConstant;
import com.innovation.ic.b1b.monitor.base.pojo.enums.ActiveEnum;
import com.innovation.ic.b1b.monitor.base.value.TimeSetConfig;
import lombok.SneakyThrows;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import javax.annotation.Resource;
import java.sql.*;
import java.util.Random;

/**
 * @desc   监控sql server可用任务处理类
 * @author linuo
 * @time   2023年3月23日15:19:11
 */
public class SqlServerAvailableJobHandler extends QuartzJobBean {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private HandlerHelper handlerHelper;

    @Resource
    private SqlServerMapper sqlServerMapper;

    @Resource
    private TimeSetConfig timeSetConfig;

    @Resource
    private SqlServerAvailableJobMapper sqlServerAvailableJobMapper;

    @Resource
    private SqlServerAvailableJobLogMapper sqlServerAvailableJobLogMapper;

    @SneakyThrows
    @Override
    protected void executeInternal(org.quartz.JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("开始执行sql server可用任务");

        // 获取1-5间的随机数
        int min = 1;
        int max = 5;
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        int sleepTime = s * 1000;
        log.info("休眠[{}]秒", s);
        Thread.sleep(sleepTime);

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        int id = jobDataMap.getInt(JobDataMapConstant.ID);
        SqlServerAvailableJob sqlServerAvailableJob = sqlServerAvailableJobMapper.selectById(id);
        if(sqlServerAvailableJob != null){
            SqlServer sqlServer = sqlServerMapper.selectById(sqlServerAvailableJob.getSqlServerId());
            if(sqlServer != null){
                String sqlServerName = sqlServer.getName();

                // 存活状态
                Integer active = ActiveEnum.NO.getCode();

                // sql server可用任务日志表实体
                SqlServerAvailableJobLog sqlServerAvailableJobLog = new SqlServerAvailableJobLog();
                sqlServerAvailableJobLog.setSqlServerAvailableJobId(id);
                sqlServerAvailableJobLog.setStartTime(new Date(System.currentTimeMillis()));

                if(Strings.isNullOrEmpty(sqlServer.getUrl()) || Strings.isNullOrEmpty(sqlServer.getUserName()) || Strings.isNullOrEmpty(sqlServer.getPassword())){
                    String message = sqlServerName + "配置信息不完整,无法检测是否存活";
                    log.info(message);
                    sqlServerAvailableJobLog.setDescription(message);
                }else{
                    // 判断SqlServer是否可用
                    String message = judgeSqlServerIfActive(sqlServer);
                    if(message == null){
                        active = ActiveEnum.YES.getCode();
                    }else{
                        sqlServerAvailableJobLog.setDescription(message);
                    }
                }

                sqlServerAvailableJobLog.setActive(active);
                int insert = sqlServerAvailableJobLogMapper.insert(sqlServerAvailableJobLog);
                if(insert > 0){
                    log.info("sql server可用任务日志插入成功");
                    if(active.intValue() == ActiveEnum.NO.getCode().intValue()){
                        // 发送邮件
                        String message = sqlServerName + "出现异常，原因：" + sqlServerAvailableJobLog.getDescription() + "，请查看SqlServer状态并及时进行异常处理。";
                        handlerHelper.sendEmail(sqlServerAvailableJob.getAlarmEmail(), message, null);
                    }
                }
            }else{
                log.info("未在sql server表中查询到id=[{}]的配置信息,无法执行监控sql server可用任务", sqlServerAvailableJob.getSqlServerId());
            }
        }else{
            log.info("监控sqlServer可用任务id=[{}]的数据不存在,无法执行任务", id);
        }
    }

    /**
     * 判断SqlServer是否可用
     * @param sqlServer sqlServer配置
     * @return 返回结果
     */
    private String judgeSqlServerIfActive(SqlServer sqlServer) throws InterruptedException {
        String message = null;
        try {
            log.info("第一次执行监控SqlServer是否可用任务");
            JdbcManager jdbcManager = new JdbcManager(SqlServerAvailableJobConstant.SQL_SERVER_CLASS_NAME, sqlServer.getUrl(), sqlServer.getUserName(), sqlServer.getPassword());
            DruidDataSource dataSource = jdbcManager.getDataSource();
            if(dataSource != null){
                log.info("SqlServer连接成功");
            }
            jdbcManager.close();
        }catch (Exception e){
            log.info("第一次连接:[{}]数据库失败,原因:[{}],待{}秒后重试", sqlServer.getUrl(), e.toString(), timeSetConfig.getRetryWaitTime());
            Thread.sleep(timeSetConfig.getRetryWaitTime() * 1000);

            try {
                log.info("第二次执行监控SqlServer是否可用任务");
                JdbcManager jdbcManager = new JdbcManager(SqlServerAvailableJobConstant.SQL_SERVER_CLASS_NAME, sqlServer.getUrl(), sqlServer.getUserName(), sqlServer.getPassword());
                DruidDataSource dataSource = jdbcManager.getDataSource();
                if(dataSource != null){
                    log.info("SqlServer连接成功");
                }
                jdbcManager.close();
            }catch (Exception e1) {
                log.info("第二次连接:[{}]数据库失败,原因:[{}],待{}秒后重试", sqlServer.getUrl(), e1.toString(), timeSetConfig.getRetryWaitTime());
                Thread.sleep(timeSetConfig.getRetryWaitTime() * 1000);

                try {
                    log.info("第三次执行监控SqlServer是否可用任务");
                    JdbcManager jdbcManager = new JdbcManager(SqlServerAvailableJobConstant.SQL_SERVER_CLASS_NAME, sqlServer.getUrl(), sqlServer.getUserName(), sqlServer.getPassword());
                    DruidDataSource dataSource = jdbcManager.getDataSource();
                    if(dataSource != null){
                        log.info("SqlServer连接成功");
                    }
                    jdbcManager.close();
                }catch (Exception e2) {
                    log.info("第三次连接:[{}]数据库失败,原因:[{}],SqlServer连接失败", sqlServer.getUrl(), e2.toString());
                    message = e2.toString();
                }
            }
        }

        if(!Strings.isNullOrEmpty(message)){
            if(message.length() > 100){
                message = message.substring(0, 100);
            }
        }

        return message;
    }
}