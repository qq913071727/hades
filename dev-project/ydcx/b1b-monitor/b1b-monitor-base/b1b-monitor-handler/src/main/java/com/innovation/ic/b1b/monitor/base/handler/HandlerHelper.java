package com.innovation.ic.b1b.monitor.base.handler;

import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.EMailManager;
import com.innovation.ic.b1b.monitor.base.mapper.EmailMapper;
import com.innovation.ic.b1b.monitor.base.model.Email;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

@Data
@Component
public class HandlerHelper {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private EMailManager eMailManager;

    @Resource
    private EmailMapper emailMapper;

    /** 默认主题 */
    private String subject = "报警";

    /**
     * 发送邮件
     * @param alarmEmail 报警邮箱地址
     * @param subject 主题
     */
    protected void sendEmail(String alarmEmail, String message, String subject) {
        if(Strings.isNullOrEmpty(alarmEmail)){
            log.info("查询email表的id为空,无法发送邮件");
            return;
        }

        if(Strings.isNullOrEmpty(message)){
            log.info("邮件发送内容为空,无法发送邮件");
            return;
        }

        Email email = emailMapper.selectById(alarmEmail);
        if(email != null){
            log.info("发送给[{}]的邮箱[{}]的内容为[{}]", email.getRealName(), email.getEmail(), message);

            if(!Strings.isNullOrEmpty(subject)){
                this.subject = subject;
            }

            eMailManager.send(email.getEmail(), email.getRealName(), this.subject, message);
        }else{
            log.info("未查询到id为[{}]的邮件配置内容,无法发送邮件", alarmEmail);
        }
    }
}