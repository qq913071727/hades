package com.innovation.ic.b1b.monitor.base.handler;

import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.monitor.base.mapper.*;
import com.innovation.ic.b1b.monitor.base.model.LogMonitorJobLog;
import lombok.SneakyThrows;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zengqinglong
 * @desc 定时把对应日志 放入历史日志中
 * @Date 2023/3/31 15:48
 **/
public class TransferHistoryDataHandler extends QuartzJobBean {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private static final String PATTERN = "yyyy-MM-dd";
    private static String outTime = DateUtils.formatDate(DateUtils.getLeftDayTime(7), PATTERN);
    @Resource
    private ApiAvailableJobLogMapper apiAvailableJobLogMapper;
    @Resource
    private ElasticsearchAvailableJobLogMapper elasticsearchAvailableJobLogMapper;
    @Resource
    private MongodbAvailableJobLogMapper mongodbAvailableJobLogMapper;
    @Resource
    private MysqlAvailableJobLogMapper mysqlAvailableJobLogMapper;
    @Resource
    private RabbitmqAvailableJobLogMapper rabbitmqAvailableJobLogMapper;
    @Resource
    private RedisAvailableJobLogMapper redisAvailableJobLogMapper;
    @Resource
    private ServerPerformanceJobLogMapper serverPerformanceJobLogMapper;
    @Resource
    private ServerPerformanceJobLogDetailMapper serverPerformanceJobLogDetailMapper;
    @Resource
    private ServiceActiveJobLogMapper serviceActiveJobLogMapper;
    @Resource
    private SqlServerAvailableJobLogMapper sqlServerAvailableJobLogMapper;
    @Resource
    private KafkaAvailableJobLogMapper kafkaAvailableJobLogMapper;
    @Resource
    private LogMonitorJobLogMapper logMonitorJobLogMapper;

    /**
     * 转移系统监控历史日志
     *
     * @param jobExecutionContext
     * @throws JobExecutionException
     */
    @SneakyThrows
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            log.info("转移系统监控历史日志 开始");
            //转移 api_available_job_log 表数据到 api_available_job_log_history 表中
            transferApiAvailableJobLogToApiAvailableJobLogHistory();

            // 转移 elasticsearch_available_job_log 表数据到 elasticsearch_available_job_log_history 表中
            transferElasticsearchAvailableJobLogToElasticsearchAvailableJobLogHistory();

            //转移 mongodb_available_job_log 表数据到 mongodb_available_job_log_history 表中
            transferMongodbAvailableJobLogToMongodbAvailableJobLogHistory();

            //转移 mysql_available_job_log 表数据到 mysql_available_job_log_history 表中
            transferMysqlAvailableJobLogToMysqlAvailableJobLogHistory();

            //转移 rabbitmq_available_job_log 表数据到 rabbitmq_available_job_log_history 表中
            transferRabbitmqAvailableJobLogToRabbitmqAvailableJobLogHistory();

            //转移 redis_available_job_log 表数据到 redis_available_job_log_history 表中
            transferRedisAvailableJobLogToRedisAvailableJobLogHistory();

            //转移 server_performance_job_log_detail 表数据到 server_performance_job_log_detail_history 表中
            // 这个一定要比transferServerPerformanceJobLogToServerPerformanceJobLogHistory 先执行
            transferServerPerformanceJobLogDetailToServerPerformanceJobLogDetailHistory();

            // 转移 server_performance_job_log 表数据到 server_performance_job_log_history 表中
            transferServerPerformanceJobLogToServerPerformanceJobLogHistory();

            //转移 service_active_job_log 表数据到 service_active_job_log_history 表中
            transferServiceActiveJobLogToServiceActiveJobLogHistory();

            //转移 sql_server_available_job_log 表数据到 sql_server_available_job_log_history 表中
            transferSqlServerAvailableJobLogToSqlServerAvailableJobLogHistory();

            //转移 kafka_available_job_log 表数据到 kafka_available_job_log_history 表中
            transferKafkaAvailableJobLogToKafkaAvailableJobLogHistory();

            //转移 log_monitor_job_log 表数据到 log_monitor_job_log_history 表中
            transferLogMonitorJobLogToLogMonitorJobLogHistory();
            log.info("转移系统监控历史日志 开始");
        } catch (Exception e) {
            log.info("转移系统监控历史日志 失败,e:{}", e);
        }
    }



    /**
     * 转移 api_available_job_log 表数据到 api_available_job_log_history 表中
     */
    public void transferApiAvailableJobLogToApiAvailableJobLogHistory() {
        log.info("-----转移 api_available_job_log 表数据到 api_available_job_log_history 表中-----");
        int count = apiAvailableJobLogMapper.transferApiAvailableJobLogToApiAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 api_available_job_log 表数据到 api_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 api_available_job_log 表历史数据开始-----");
            int delCount = apiAvailableJobLogMapper.deleteApiAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 api_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 api_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 api_available_job_log 表数据到 api_available_job_log_history 表中结束-----");
    }

    /**
     * 转移 elasticsearch_available_job_log 表数据到 elasticsearch_available_job_log_history 表中
     */
    public void transferElasticsearchAvailableJobLogToElasticsearchAvailableJobLogHistory() {
        log.info("-----转移 elasticsearch_available_job_log 表数据到 elasticsearch_available_job_log_history 表中开始-----");
        int count = elasticsearchAvailableJobLogMapper.transferElasticsearchAvailableJobLogToElasticsearchAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 elasticsearch_available_job_log 表数据到 elasticsearch_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 elasticsearch_available_job_log 表历史数据开始-----");
            int delCount = elasticsearchAvailableJobLogMapper.deleteElasticsearchAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 elasticsearch_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 elasticsearch_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 elasticsearch_available_job_log 表数据到 elasticsearch_available_job_log_history 表中结束-----");
    }

    /**
     * 转移 mongodb_available_job_log 表数据到 mongodb_available_job_log_history 表中
     */
    public void transferMongodbAvailableJobLogToMongodbAvailableJobLogHistory() {
        log.info("-----转移 mongodb_available_job_log 表数据到 mongodb_available_job_log_history 表中开始-----");
        int count = mongodbAvailableJobLogMapper.transferMongodbAvailableJobLogToMongodbAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 mongodb_available_job_log 表数据到 mongodb_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 mongodb_available_job_log 表历史数据开始-----");
            int delCount = mongodbAvailableJobLogMapper.deleteMongodbAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 mongodb_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 mongodb_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 mongodb_available_job_log 表数据到 mongodb_available_job_log_history 表中结束-----");
    }

    /**
     * 转移 mysql_available_job_log 表数据到 mysql_available_job_log_history 表中
     */
    public void transferMysqlAvailableJobLogToMysqlAvailableJobLogHistory() {
        log.info("-----转移 mysql_available_job_log 表数据到 mysql_available_job_log_history 表中开始-----");
        int count = mysqlAvailableJobLogMapper.transferMysqlAvailableJobLogToMysqlAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 mysql_available_job_log 表数据到 mysql_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 mysql_available_job_log 表历史数据开始-----");
            int delCount = mysqlAvailableJobLogMapper.deleteMysqlAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 mysql_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 mysql_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 mysql_available_job_log 表数据到 mysql_available_job_log_history 表中结束-----");
    }

    /**
     * 转移 rabbitmq_available_job_log 表数据到 rabbitmq_available_job_log_history 表中
     */
    public void transferRabbitmqAvailableJobLogToRabbitmqAvailableJobLogHistory() {
        log.info("-----rabbitmq_available_job_log 表数据到 rabbitmq_available_job_log_history 表中开始-----");
        int count = rabbitmqAvailableJobLogMapper.transferRabbitmqAvailableJobLogToRabbitmqAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 rabbitmq_available_job_log 表数据到 rabbitmq_available_job_log_history成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 rabbitmq_available_job_log 表历史数据开始-----");
            int delCount = rabbitmqAvailableJobLogMapper.deleteRabbitmqAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 rabbitmq_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 rabbitmq_available_job_log 表历史数据结束-----");
        }
        log.info("-----rabbitmq_available_job_log 表数据到 rabbitmq_available_job_log_history表中结束-----");
    }

    /**
     * 转移 redis_available_job_log 表数据到 redis_available_job_log_history 表中
     */
    public void transferRedisAvailableJobLogToRedisAvailableJobLogHistory() {
        log.info("-----转移 redis_available_job_log 表数据到 redis_available_job_log_history 表中开始-----");
        int count = redisAvailableJobLogMapper.transferRedisAvailableJobLogToRedisAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 redis_available_job_log 表数据到 redis_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 redis_available_job_log 表历史数据开始-----");
            int delCount = redisAvailableJobLogMapper.deleteRedisAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 redis_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 redis_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 redis_available_job_log 表数据到 redis_available_job_log_history 表中结束-----");
    }

    /**
     * 转移 server_performance_job_log 表数据到 server_performance_job_log_history 表中
     */
    public void transferServerPerformanceJobLogToServerPerformanceJobLogHistory() {
        log.info("-----转移 server_performance_job_log 表数据到 server_performance_job_log_history 表中开始-----");
        int count = serverPerformanceJobLogMapper.transferServerPerformanceJobLogToServerPerformanceJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 server_performance_job_log 表数据到 server_performance_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 server_performance_job_log 表历史数据开始-----");
            int delCount = serverPerformanceJobLogMapper.deleteServerPerformanceJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 server_performance_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 server_performance_job_log 表历史数据结束-----");
        }
        log.info("-----转移 server_performance_job_log 表数据到 server_performance_job_log_history 表中结束-----");
    }

    /**
     * 转移 server_performance_job_log_detail 表数据到 server_performance_job_log_detail_history 表中
     */
    public void transferServerPerformanceJobLogDetailToServerPerformanceJobLogDetailHistory() {
        log.info("-----转移 server_performance_job_log_detail 表数据到 server_performance_job_log_detail_history 表中开始-----");
        int count = serverPerformanceJobLogDetailMapper.transferServerPerformanceJobLogDetailToServerPerformanceJobLogDetailHistory(outTime);
        if (count > 0) {
            log.info("转移 server_performance_job_log_detail 表数据到 server_performance_job_log_detail_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 server_performance_job_log_detail 表历史数据开始-----");
            int delCount = serverPerformanceJobLogDetailMapper.deleteServerPerformanceJobLogDetailData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 server_performance_job_log_detail 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 server_performance_job_log_detail 表历史数据结束-----");
        }
        log.info("-----转移 server_performance_job_log_detail 表数据到 server_performance_job_log_detail_history 表中结束-----");
    }

    /**
     * 转移 service_active_job_log 表数据到 service_active_job_log_history 表中
     */
    public void transferServiceActiveJobLogToServiceActiveJobLogHistory() {
        log.info("-----转移 service_active_job_log 表数据到 service_active_job_log_history 表中开始-----");
        int count = serviceActiveJobLogMapper.transferServiceActiveJobLogToServiceActiveJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 service_active_job_log 表数据到 service_active_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 service_active_job_log 表历史数据开始-----");
            int delCount = serviceActiveJobLogMapper.deleteServiceActiveJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 service_active_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 service_active_job_log 表历史数据结束-----");
        }
        log.info("-----转移 service_active_job_log 表数据到 service_active_job_log_history 表中结束-----");
    }

    /**
     * 转移 sql_server_available_job_log 表数据到 sql_server_available_job_log_history 表中
     */
    public void transferSqlServerAvailableJobLogToSqlServerAvailableJobLogHistory() {
        log.info("-----转移 sql_server_available_job_log 表数据到 sql_server_available_job_log_history 表中开始-----");
        int count = sqlServerAvailableJobLogMapper.transferSqlServerAvailableJobLogToSqlServerAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 sql_server_available_job_log 表数据到 sql_server_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 sql_server_available_job_log 表历史数据开始-----");
            int delCount = sqlServerAvailableJobLogMapper.deleteSqlServerAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 sql_server_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 sql_server_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 sql_server_available_job_log 表数据到 sql_server_available_job_log_history 表中结束-----");
    }
    /**
     *  转移 kafka_available_job_log 表数据到 kafka_available_job_log_history 表中
     */
    public void transferKafkaAvailableJobLogToKafkaAvailableJobLogHistory() {
        log.info("-----转移 kafka_available_job_log 表数据到 kafka_available_job_log_history 表中开始-----");
        int count = kafkaAvailableJobLogMapper.transferKafkaAvailableJobLogToKafkaAvailableJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 kafka_available_job_log 表数据到 kafka_available_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 kafka_available_job_log 表历史数据开始-----");
            int delCount = kafkaAvailableJobLogMapper.deleteKafkaAvailableJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 kafka_available_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 kafka_available_job_log 表历史数据结束-----");
        }
        log.info("-----转移 kafka_available_job_log 表数据到 kafka_available_job_log_history 表中结束-----");
    }


    /**
     *  转移 log_monitor_job_log 表数据到 log_monitor_job_log_history 表中
     */
    public void transferLogMonitorJobLogToLogMonitorJobLogHistory() {
        log.info("-----转移 log_monitor_job_log 表数据到 log_monitor_job_log_history 表中开始-----");
        int count = logMonitorJobLogMapper.transferLogMonitorJobLogToLogMonitorJobLogHistory(outTime);
        if (count > 0) {
            log.info("转移 log_monitor_job_log 表数据到 log_monitor_job_log_history 表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的 log_monitor_job_log 表历史数据开始-----");
            int delCount = logMonitorJobLogMapper.deleteLogMonitorJobLogData(outTime);
            if (delCount > 0) {
                log.info("删除转移成功的 log_monitor_job_log 表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的 log_monitor_job_log 表历史数据结束-----");
        }
        log.info("-----转移 log_monitor_job_log 表数据到 log_monitor_job_log_history 表中结束-----");
    }

}
