package com.innovation.ic.b1b.monitor.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   各时间配置
 * @author linuo
 * @time   2023年5月6日13:19:07
 */
@Data
@Component
@ConfigurationProperties(prefix = "timeset")
public class TimeSetConfig {
    /** 定时任务重试等待时间(单位秒) */
    private Integer retryWaitTime;
}