package com.innovation.ic.b1b.monitor.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * jdbc-sharding数据源的配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.datasource.sharding.b1b-monitor2")
public class ShardingB1bMonitor2DataSourceConfig {

    private String driverClassName;

    private String username;

    private String password;

    private String url;

    private Integer initialSize;

    private Integer minIdle;

    private Integer maxActive;

    private String validationQuery;

    private Boolean testOnBorrow;

    private Boolean testOnReturn;

    private Boolean testWhileIdle;

    private Integer timeBetweenEvictionRunsMillis;
}
