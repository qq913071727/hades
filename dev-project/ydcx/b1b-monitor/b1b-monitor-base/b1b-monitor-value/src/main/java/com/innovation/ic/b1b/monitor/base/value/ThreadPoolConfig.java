package com.innovation.ic.b1b.monitor.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程池配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "threadpool")
public class ThreadPoolConfig {
    /** 核心线程数 */
    private Integer corePoolSize;

    /** 最大线程数 */
    private Integer maximumPoolSize;

    /** 线程空闲时间 */
    private Integer keepAliveTime;

    /** 队列，当线程数目超过核心线程数时用于保存任务的队列 */
    private Integer workQueue;

    /** 线程名称前缀 */
    private String threadNamePrefix;
}