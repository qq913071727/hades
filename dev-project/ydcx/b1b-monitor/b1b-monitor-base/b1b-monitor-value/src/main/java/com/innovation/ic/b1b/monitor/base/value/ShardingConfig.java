package com.innovation.ic.b1b.monitor.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * sharding分库分表配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "sharding")
public class ShardingConfig {

    /**
     * 分库的数量
     */
    private Integer databaseNumber;

    /**
     * 分表的数量
     */
    private Integer tableNumber;

    /**
     * 是否打印sql
     */
    private Boolean sqlShow;
}
