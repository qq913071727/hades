package com.innovation.ic.b1b.monitor.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 邮件的配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "email")
public class EMailConfig {

    private String sendAccount;

    private String sendPassword;

    private String eMailSMTPHost;
}
