package com.innovation.ic.b1b.monitor.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   服务器监控配置参数
 * @author linuo
 * @time   2023年3月22日14:23:55
 */
@Data
@Component
@ConfigurationProperties(prefix = "server.performance")
public class ServerPerformanceConfig {
    /** 磁盘占用预警百分比 */
    private Integer diskWarningPercentage;

    /** 内存占用预警百分比 */
    private Integer memoryWarningPercentage;
}
