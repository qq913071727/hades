package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Kafka;
import com.innovation.ic.b1b.monitor.base.pojo.KafkaPojo;
import com.innovation.ic.b1b.monitor.base.vo.KafkaVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @desc   Kafka表Mapper类
 * @author linuo
 * @time   2023年5月8日11:19:56
 */
@Repository
public interface KafkaMapper extends EasyBaseMapper<Kafka> {

    List<KafkaPojo> findByPage(KafkaVo kafkaVo);
}