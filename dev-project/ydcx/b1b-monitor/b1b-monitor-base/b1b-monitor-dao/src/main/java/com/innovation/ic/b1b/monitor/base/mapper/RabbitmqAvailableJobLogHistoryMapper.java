package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJobLogHistory;

public interface RabbitmqAvailableJobLogHistoryMapper extends EasyBaseMapper<RabbitmqAvailableJobLogHistory> {

}
