package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Email;
import com.innovation.ic.b1b.monitor.base.pojo.variable.email.AlarmEmailQueryRespPojo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EmailMapper extends EasyBaseMapper<Email> {
    /**
     * 查询报警邮箱列表
     * @return 返回查询结果
     */
    List<AlarmEmailQueryRespPojo> queryAlarmEmailList();
}