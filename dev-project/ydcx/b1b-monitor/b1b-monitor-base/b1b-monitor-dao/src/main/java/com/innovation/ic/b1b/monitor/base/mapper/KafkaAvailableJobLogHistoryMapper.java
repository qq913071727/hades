package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.KafkaAvailableJobLogHistory;
import org.springframework.stereotype.Repository;

/**
 * @desc   kafka可用任务历史日志表Mapper类
 * @author linuo
 * @time   2023年5月8日11:30:00
 */
@Repository
public interface KafkaAvailableJobLogHistoryMapper extends EasyBaseMapper<KafkaAvailableJobLogHistory> {

}