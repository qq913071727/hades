package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.CanalAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJobLog.CanalAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJobLog.CanalAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   Canal可用任务日志表Mapper类
 * @author linuo
 * @time   2023年5月12日09:56:41
 */
@Repository
public interface CanalAvailableJobLogMapper extends EasyBaseMapper<CanalAvailableJobLog> {
    /**
     * 查询canal监控任务日志列表
     * @param canalAvailableJobLogListQueryVo 查询canal监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<CanalAvailableJobLogListRespData> queryList(@Param("param") CanalAvailableJobLogListQueryVo canalAvailableJobLogListQueryVo);
}