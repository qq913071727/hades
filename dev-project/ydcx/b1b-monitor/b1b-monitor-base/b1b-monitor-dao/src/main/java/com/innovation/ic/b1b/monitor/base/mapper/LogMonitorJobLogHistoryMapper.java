package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.LogMonitorJobLogHistory;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogHistoryVo;
import org.apache.ibatis.annotations.Mapper;
import java.util.Map;
import java.util.List;

/**
 * 
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@Mapper
public interface LogMonitorJobLogHistoryMapper extends EasyBaseMapper<LogMonitorJobLogHistory> {

    List<LogMonitorJobLogHistory> pageInfo(LogMonitorJobLogHistoryVo vo);
}
