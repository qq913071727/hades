package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ElasticsearchAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJobLog.ElasticsearchAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJobLog.ElasticsearchAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface ElasticsearchAvailableJobLogMapper extends EasyBaseMapper<ElasticsearchAvailableJobLog>{
    /**
     * 查询elasticsearch可用任务日志列表
     * @param elasticsearchAvailableJobLogListQueryVo 查询elasticsearch可用任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<ElasticsearchAvailableJobLogListRespData> queryList(@Param("param") ElasticsearchAvailableJobLogListQueryVo elasticsearchAvailableJobLogListQueryVo);

    /**
     * 转移 elasticsearch_available_job_log 表数据到 elasticsearch_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferElasticsearchAvailableJobLogToElasticsearchAvailableJobLogHistory(String outTime);

    /**
     * 删除转移成功的elasticsearch_available_job_log表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteElasticsearchAvailableJobLogData(String outTime);
}