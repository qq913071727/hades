package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Service;
import com.innovation.ic.b1b.monitor.base.pojo.ServicePojo;
import com.innovation.ic.b1b.monitor.base.vo.ServiceVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ServiceMapper extends EasyBaseMapper<Service> {

    ServicePojo findByService(@Param("id") Integer id);

    List<ServicePojo> findByPage(ServiceVo serviceVo);

    Integer checkRepeat(Map<String, Object> paramMap);

    List<Service> findByEnvIds(@Param("envIds")List<Integer> envIds);
}