package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.FrontEndLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.frontEndLog.FrontEndLogPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FrontEndLogMapper extends EasyBaseMapper<FrontEndLog> {

    List<FrontEndLogPojo> page(@Param("systemId") Integer systemId, @Param("environmentId") Integer environmentId,
                               @Param("serviceId") Integer serviceId, @Param("modelId") Integer modelId,
                               @Param("beginDate") Date beginDate, @Param("endDate") Date endDate,
                               @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    List pageCount(@Param("systemId") Integer systemId, @Param("environmentId") Integer environmentId,
                   @Param("serviceId") Integer serviceId, @Param("modelId") Integer modelId,
                   @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
