package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.CanalAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJob.CanalAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.canalAvailableJob.CanalAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJob.CanalAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.canalAvailableJob.CanalAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   Canal可用任务表Mapper类
 * @author linuo
 * @time   2023年5月12日09:55:27
 */
@Repository
public interface CanalAvailableJobMapper extends EasyBaseMapper<CanalAvailableJob> {
    /**
     * 查询canal可用任务列表数据
     * @param canalAvailableJobListQueryVo 查询canal监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<CanalAvailableJobListRespData> queryList(@Param("param") CanalAvailableJobListQueryVo canalAvailableJobListQueryVo);

    /**
     * 获取canal监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    CanalAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新canal监控任务
     * @param canalAvailableJobUpdateVo 更新canal监控任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") CanalAvailableJobUpdateVo canalAvailableJobUpdateVo);
}