package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ZookeeperAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob.ZookeeperAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJob.ZookeeperAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJob.ZookeeperAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   ZookeeperAvailableJob表Mapper类
 * @author linuo
 * @time   2023年5月11日10:21:48
 */
@Repository
public interface ZookeeperAvailableJobMapper extends EasyBaseMapper<ZookeeperAvailableJob> {
    /**
     * 查询zookeeper监控任务列表
     * @param zookeeperAvailableJobListQueryVo 查询zookeeper监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<ZookeeperAvailableJobListRespData> queryList(@Param("param") ZookeeperAvailableJobListQueryVo zookeeperAvailableJobListQueryVo);

    /**
     * 获取zookeeper监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    ZookeeperAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新zookeeper监控任务
     * @param zookeeperAvailableJobUpdateVo 更新zookeeper监控任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") ZookeeperAvailableJobUpdateVo zookeeperAvailableJobUpdateVo);
}