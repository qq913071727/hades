package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.KafkaAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJobLog.KafkaAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJobLog.KafkaAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   Kafka可用任务日志表Mapper类
 * @author linuo
 * @time   2023年5月8日11:25:42
 */
@Repository
public interface KafkaAvailableJobLogMapper extends EasyBaseMapper<KafkaAvailableJobLog> {
    /**
     * 转移 kafka_available_job_log 表数据到 kafka_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferKafkaAvailableJobLogToKafkaAvailableJobLogHistory(String outTime);

    /**
     * 删除转移成功的 kafka_available_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteKafkaAvailableJobLogData(String outTime);

    /**
     * 查询kafka监控任务日志列表
     * @param kafkaAvailableJobLogListQueryVo 查询kafka监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<KafkaAvailableJobLogListRespData> queryList(@Param("param") KafkaAvailableJobLogListQueryVo kafkaAvailableJobLogListQueryVo);
}