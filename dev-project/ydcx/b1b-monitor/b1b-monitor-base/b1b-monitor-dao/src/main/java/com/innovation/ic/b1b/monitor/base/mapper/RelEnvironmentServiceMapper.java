package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RelEnvironmentService;
import com.innovation.ic.b1b.monitor.base.vo.ModelVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RelEnvironmentServiceMapper extends EasyBaseMapper<RelEnvironmentService> {


    List<Integer> findByServiceId(@Param("serviceId") Integer id);

    List<Integer> findByEnvironmentId(@Param("environmentId")Integer environmentId);

    Integer findByServiceIdAndSystemId(ModelVo modelVo);
}
