package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Mysql;
import com.innovation.ic.b1b.monitor.base.pojo.MySqlPojo;
import com.innovation.ic.b1b.monitor.base.pojo.SqlServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysql.MysqlInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.MySqlVo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MySqlMapper extends EasyBaseMapper<Mysql> {
    MySqlPojo findById(Integer id);

    List<SqlServerPojo> findByPage(MySqlVo mySqlVo);

    /**
     * 查询mysql信息列表
     * @return 返回查询结果
     */
    List<MysqlInfoRespPojo> queryMysqlList();
}