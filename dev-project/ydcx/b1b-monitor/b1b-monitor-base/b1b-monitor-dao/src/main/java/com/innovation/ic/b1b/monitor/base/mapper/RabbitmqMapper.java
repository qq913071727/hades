package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Rabbitmq;
import com.innovation.ic.b1b.monitor.base.pojo.RabbitmqPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmq.RabbitmqInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.RabbitmqVo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RabbitmqMapper extends EasyBaseMapper<Rabbitmq> {
    List<RabbitmqPojo> findByPage(RabbitmqVo rabbitmqVo);

    /**
     * 查询rabbitmq信息列表
     * @return 返回查询结果
     */
    List<RabbitmqInfoRespPojo> queryRabbitmqList();
}