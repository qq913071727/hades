package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJob.RedisAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJob.RedisAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJob.RedisAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface RedisAvailableJobMapper extends EasyBaseMapper<RedisAvailableJob> {
    /**
     * 查询redis监控任务列表
     * @param redisAvailableJobListQueryVo 查询redis监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<RedisAvailableJobListRespData> queryList(@Param("param") RedisAvailableJobListQueryVo redisAvailableJobListQueryVo);

    /**
     * 获取redis监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    RedisAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新redis监控任务
     * @param redisAvailableJobUpdateVo 更新redis可用任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") RedisAvailableJobUpdateVo redisAvailableJobUpdateVo);
}