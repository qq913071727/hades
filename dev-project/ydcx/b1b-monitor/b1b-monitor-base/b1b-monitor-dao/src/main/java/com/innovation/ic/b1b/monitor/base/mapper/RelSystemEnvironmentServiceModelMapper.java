package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironmentServiceModel;
import org.springframework.stereotype.Repository;


@Repository
public interface RelSystemEnvironmentServiceModelMapper extends EasyBaseMapper<RelSystemEnvironmentServiceModel> {


}
