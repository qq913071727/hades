package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ApiAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJobLog.ApiAvailableJobLogDataPojo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJobLog.ApiAvailableJobLogListVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApiAvailableJobLogMapper extends EasyBaseMapper<ApiAvailableJobLog> {
    /**
     * 根据条件 查询接口日志数据
     *
     * @param param
     * @return
     */
    List<ApiAvailableJobLogDataPojo> queryList(@Param("param") ApiAvailableJobLogListVo param);

    /**
     * 转移 api_available_job_log 表数据到 api_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferApiAvailableJobLogToApiAvailableJobLogHistory(@Param("outTime")String outTime);

    /**
     * 删除转移后的数据
     *
     * @param outTime
     * @return
     */
    int deleteApiAvailableJobLogData(@Param("outTime")String outTime);
}
