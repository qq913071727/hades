package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJobLog.MysqlAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJobLog.MysqlAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MysqlAvailableJobLogMapper extends EasyBaseMapper<MysqlAvailableJobLog> {
    /**
     * 查询mysql可用任务日志表数据
     * @param mysqlAvailableJobLogListQueryVo 查询mysql监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<MysqlAvailableJobLogListRespData> queryList(@Param("param") MysqlAvailableJobLogListQueryVo mysqlAvailableJobLogListQueryVo);

    /**
     * 转移 mysql_available_job_log 表数据到 mysql_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferMysqlAvailableJobLogToMysqlAvailableJobLogHistory(String outTime);

    /**
     * 删除转移成功的 mysql_available_job_log 表历史数据开始
     *
     * @param outTime
     * @return
     */
    int deleteMysqlAvailableJobLogData(String outTime);
}