package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Model;
import com.innovation.ic.b1b.monitor.base.pojo.ModelPojo;
import com.innovation.ic.b1b.monitor.base.vo.ModelVo;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ModelMapper extends EasyBaseMapper<Model> {



    List<ModelPojo> findByPage(ModelVo modelVo);
}
