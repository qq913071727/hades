package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Environment;
import com.innovation.ic.b1b.monitor.base.pojo.EnvironmentPojo;
import com.innovation.ic.b1b.monitor.base.vo.EnvironmentVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EnvironmentMapper extends EasyBaseMapper<Environment> {


    List<com.innovation.ic.b1b.monitor.base.pojo.EnvironmentPojo> findByPage(EnvironmentVo environmentVo);

    EnvironmentPojo findById(@Param("id") Integer id);

    List<EnvironmentPojo> dropDown(EnvironmentVo environmentVo);

    void saveReturnId(Environment environment);
}
