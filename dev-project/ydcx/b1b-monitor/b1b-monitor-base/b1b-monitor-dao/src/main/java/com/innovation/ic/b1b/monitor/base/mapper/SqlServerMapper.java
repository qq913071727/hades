package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.SqlServer;
import com.innovation.ic.b1b.monitor.base.pojo.SqlServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServer.SqlServerInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.SqlServerVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SqlServerMapper extends EasyBaseMapper<SqlServer> {

    SqlServerPojo findById(@Param("id") Integer id);

    List<SqlServerPojo> findByPage(SqlServerVo sqlServerVo);

    /**
     * 查询sql server信息列表
     * @return 返回查询结果
     */
    List<SqlServerInfoRespPojo> querySqlServerList();
}