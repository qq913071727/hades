package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJobLog.SqlServerAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJobLog.SqlServerAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SqlServerAvailableJobLogMapper extends EasyBaseMapper<SqlServerAvailableJobLog> {
    /**
     * 查询sql server可用任务日志表数据
     * @param sqlServerAvailableJobLogListQueryVo 查询sql server可用任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<SqlServerAvailableJobLogListRespData> queryList(@Param("param") SqlServerAvailableJobLogListQueryVo sqlServerAvailableJobLogListQueryVo);

    /**
     * 转移 sql_server_available_job_log 表数据到 sql_server_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferSqlServerAvailableJobLogToSqlServerAvailableJobLogHistory(String outTime);

    /**
     * 删除转移成功的 sql_server_available_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteSqlServerAvailableJobLogData(String outTime);
}