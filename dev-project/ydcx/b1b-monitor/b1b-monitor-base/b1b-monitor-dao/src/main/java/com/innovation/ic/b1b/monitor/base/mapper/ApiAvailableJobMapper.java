package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ApiAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.apiAvailableJob.ApiAvailableJobDataPojo;
import com.innovation.ic.b1b.monitor.base.vo.apiAvailableJob.ApiAvailableJobListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApiAvailableJobMapper extends EasyBaseMapper<ApiAvailableJob> {

    /**
     * 根据id 查询接口监控任务详情
     *
     * @param id
     * @return
     */
    ApiAvailableJobDataPojo info(@Param("id") Integer id);

    /**
     * 根据条件获取 接口任务列表
     *
     * @param param
     * @return
     */
    List<ApiAvailableJobDataPojo> queryList( @Param("param") ApiAvailableJobListQueryVo param);
}
