package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.sqlServerAvailableJob.SqlServerAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.sqlServerAvailableJob.SqlServerAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SqlServerAvailableJobMapper extends EasyBaseMapper<SqlServerAvailableJob> {
    /**
     * 查询sql server监控任务列表
     * @param sqlServerAvailableJobListQueryVo 查询sql server监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<SqlServerAvailableJobListRespData> queryList(@Param("param") SqlServerAvailableJobListQueryVo sqlServerAvailableJobListQueryVo);

    /**
     * 获取sql server监控任务详情
     * @param id 主键id
     * @return 返回sql server监控任务详情
     */
    SqlServerAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新sql server监控任务
     * @param sqlServerAvailableJobUpdateVo sql server可用任务更新的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") SqlServerAvailableJobUpdateVo sqlServerAvailableJobUpdateVo);
}