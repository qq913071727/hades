package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJobLogHistory;

public interface MongodbAvailableJobLogHistoryMapper extends EasyBaseMapper<MongodbAvailableJobLogHistory> {

}
