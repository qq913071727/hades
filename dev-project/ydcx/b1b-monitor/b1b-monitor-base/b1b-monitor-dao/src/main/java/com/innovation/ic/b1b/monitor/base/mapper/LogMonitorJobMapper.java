package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.LogMonitorJob;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@Mapper
public interface LogMonitorJobMapper extends EasyBaseMapper<LogMonitorJob> {

    List<LogMonitorJob> pageInfo(LogMonitorJobVo vo);
}
