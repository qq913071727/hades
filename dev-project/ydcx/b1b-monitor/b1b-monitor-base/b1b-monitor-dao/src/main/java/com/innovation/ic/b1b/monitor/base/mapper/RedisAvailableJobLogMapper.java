package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redisAvailableJobLog.RedisAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.redisAvailableJobLog.RedisAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface RedisAvailableJobLogMapper extends EasyBaseMapper<RedisAvailableJobLog> {
    /**
     * 查询redis可用任务日志表数据
     * @param redisAvailableJobLogListQueryVo 查询redis监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<RedisAvailableJobLogListRespData> queryList(@Param("param") RedisAvailableJobLogListQueryVo redisAvailableJobLogListQueryVo);

    /**
     * 转移 redis_available_job_log 表数据到 redis_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferRedisAvailableJobLogToRedisAvailableJobLogHistory(String outTime);

    /**
     * 删除转移成功的 redis_available_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteRedisAvailableJobLogData(String outTime);
}
