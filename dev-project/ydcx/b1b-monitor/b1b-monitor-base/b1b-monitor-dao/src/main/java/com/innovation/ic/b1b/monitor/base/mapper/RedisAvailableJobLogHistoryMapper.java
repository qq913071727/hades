package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RedisAvailableJobLogHistory;

public interface RedisAvailableJobLogHistoryMapper extends EasyBaseMapper<RedisAvailableJobLogHistory> {

}
