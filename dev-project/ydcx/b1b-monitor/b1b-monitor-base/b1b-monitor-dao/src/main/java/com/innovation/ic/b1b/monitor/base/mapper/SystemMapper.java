package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.vo.SystemVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.innovation.ic.b1b.monitor.base.model.System;

import java.util.List;
import java.util.Map;


@Repository
public interface SystemMapper extends EasyBaseMapper<System> {


    void addSystem(System system);

    void updateSystem(System system);

    void deleteSystem(@Param("id") Integer id);

    System findById(@Param("id")Integer id);

    List<System> findByPage(SystemVo systemVo);

    Integer checkRepeat(Map<String, Object> paramMap);

    List<System> findByIds(@Param("ids") List<Integer> systemIds);
}
