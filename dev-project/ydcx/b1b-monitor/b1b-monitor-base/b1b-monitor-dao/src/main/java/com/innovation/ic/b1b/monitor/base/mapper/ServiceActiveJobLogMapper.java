package com.innovation.ic.b1b.monitor.base.mapper;


import com.innovation.ic.b1b.monitor.base.model.ServiceActiveJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJobLog.ServiceActiveJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJobLog.ServiceActiveJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceActiveJobLogMapper extends EasyBaseMapper<ServiceActiveJobLog> {
    /**
     * 查询服务存活任务表数据
     * @param serviceActiveJobLogListQueryVo 服务存活任务表查询列表的Vo类
     * @return 返回查询结果
     */
    List<ServiceActiveJobLogListRespData> queryList(@Param("param") ServiceActiveJobLogListQueryVo serviceActiveJobLogListQueryVo);

    /**
     * 转移 service_active_job_log 表数据到 service_active_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferServiceActiveJobLogToServiceActiveJobLogHistory(@Param("outTime")String outTime);

    /**
     * 删除转移成功的 service_active_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteServiceActiveJobLogData(@Param("outTime")String outTime);
}
