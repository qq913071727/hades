package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Zookeeper;
import com.innovation.ic.b1b.monitor.base.pojo.ZookeeperPojo;
import com.innovation.ic.b1b.monitor.base.vo.ZookeeperVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @desc   Zookeeper表Mapper类
 * @author linuo
 * @time   2023年5月11日10:12:13
 */
@Repository
public interface ZookeeperMapper extends EasyBaseMapper<Zookeeper> {

    List<ZookeeperPojo> findByPage(ZookeeperVo zookeeperVo);
}