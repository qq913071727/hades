package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ServerPerformanceJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLog.ServerPerformanceJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJobLog.ServerPerformanceJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ServerPerformanceJobLogMapper extends EasyBaseMapper<ServerPerformanceJobLog> {
    /**
     * 查询服务器性能任务日志表数据
     * @param serverPerformanceJobLogListQueryVo 查询服务器性能任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<ServerPerformanceJobLogListRespData> queryList(@Param("param") ServerPerformanceJobLogListQueryVo serverPerformanceJobLogListQueryVo);

    /**
     * 转移 server_performance_job_log 表数据到 server_performance_job_log_history 表中
     * @param outTime
     * @return
     */
    int transferServerPerformanceJobLogToServerPerformanceJobLogHistory(String outTime);

    /**
     * 删除转移成功的 server_performance_job_log 表历史数据
     * @param outTime
     * @return
     */
    int deleteServerPerformanceJobLogData(String outTime);
}