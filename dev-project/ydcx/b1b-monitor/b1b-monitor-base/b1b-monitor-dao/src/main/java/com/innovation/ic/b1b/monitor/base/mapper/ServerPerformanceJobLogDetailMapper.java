package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ServerPerformanceJobLogDetail;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJobLogDetail.ServerPerformanceJobLogDetailInfoData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ServerPerformanceJobLogDetailMapper extends EasyBaseMapper<ServerPerformanceJobLogDetail> {
    /**
     * 查询服务器性能任务日志详情
     * @param id 服务器性能任务日志id
     * @return 返回查询结果
     */
    List<ServerPerformanceJobLogDetailInfoData> getDataByLogId(@Param("id") Integer id);

    /**
     * 转移 server_performance_job_log_detail 表数据到 server_performance_job_log_detail_history 表中
     *
     * @param outTime
     * @return
     */
    int transferServerPerformanceJobLogDetailToServerPerformanceJobLogDetailHistory(@Param("outTime")String outTime);

    /**
     * 删除转移成功的 server_performance_job_log_detail 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteServerPerformanceJobLogDetailData(@Param("outTime")String outTime);
}