package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ServiceActiveJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob.ServiceActiveJobData;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serviceActiveJob.ServiceActiveJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serviceActiveJob.ServiceActiveJobListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceActiveJobMapper extends EasyBaseMapper<ServiceActiveJob> {

    /**
     * 查询服务存活任务表数据
     * @param serviceActiveJobListQueryVo 服务存活任务表查询列表的Vo类
     * @return 返回查询结果
     */
    List<ServiceActiveJobListRespData> queryList(@Param("param") ServiceActiveJobListQueryVo serviceActiveJobListQueryVo);

    /**
     * 获取当前服务详情
     *
     * @param id 主键id
     * @return 返回查询结果
     */
    ServiceActiveJobListRespData info(@Param("id") Integer id);

    /**
     * 定时任务查看添加服务存活日志数据
     *
     * @param id 主键id
     * @return 返回查询结果
     */
    ServiceActiveJobData findId(@Param("id") Integer id);
}
