package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.LogMonitorJobLog;
import com.innovation.ic.b1b.monitor.base.vo.LogMonitorJobLogVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-10
 */
@Mapper
public interface LogMonitorJobLogMapper extends EasyBaseMapper<LogMonitorJobLog> {

    List<LogMonitorJobLog> pageInfo(LogMonitorJobLogVo vo);

    /**
     * 转移 log_monitor_job_log 表数据到 log_monitor_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferLogMonitorJobLogToLogMonitorJobLogHistory(String outTime);

    /**
     * 删除转移成功的 log_monitor_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteLogMonitorJobLogData(String outTime);
}
