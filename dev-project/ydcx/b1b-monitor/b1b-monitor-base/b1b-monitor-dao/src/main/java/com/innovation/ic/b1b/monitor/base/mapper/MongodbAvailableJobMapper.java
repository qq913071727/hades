package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJob.MongodbAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJob.MongodbAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface MongodbAvailableJobMapper extends EasyBaseMapper<MongodbAvailableJob> {
    /**
     * 查询mongodb可用任务列表数据
     * @param mongodbAvailableJobListQueryVo 查询mongodb监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<MongodbAvailableJobListRespData> queryList(@Param("param") MongodbAvailableJobListQueryVo mongodbAvailableJobListQueryVo);

    /**
     * 获取mongodb监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    MongodbAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新mongodb监控任务
     * @param mongodbAvailableJobUpdateVo 更新mongodb监控任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") MongodbAvailableJobUpdateVo mongodbAvailableJobUpdateVo);
}