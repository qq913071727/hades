package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Redis;
import com.innovation.ic.b1b.monitor.base.pojo.RedisPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.redis.RedisInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.RedisVo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RedisMapper extends EasyBaseMapper<Redis> {
    List<RedisPojo> findByPage(RedisVo redisVo);

    /**
     * 查询redis信息列表
     * @return 返回查询结果
     */
    List<RedisInfoRespPojo> queryRedisList();
}