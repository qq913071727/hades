package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.KafkaAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJob.KafkaAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.kafkaAvailableJob.KafkaAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.kafkaAvailableJob.KafkaAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   Kafka可用任务表Mapper类
 * @author linuo
 * @time   2023年5月8日11:39:24
 */
@Repository
public interface KafkaAvailableJobMapper extends EasyBaseMapper<KafkaAvailableJob> {
    /**
     * 查询kafka监控任务列表
     * @param kafkaAvailableJobListQueryVo 查询kafka监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<KafkaAvailableJobListRespData> queryList(@Param("param") KafkaAvailableJobListQueryVo kafkaAvailableJobListQueryVo);

    /**
     * 获取kafka监控任务详情
     * @param id kafka监控任务id
     * @return 返回查询结果
     */
    KafkaAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新kafka监控任务
     * @param kafkaAvailableJobUpdateVo 更新kafka监控任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") KafkaAvailableJobUpdateVo kafkaAvailableJobUpdateVo);
}