package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RelSystemEnvironment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RelSystemEnvironmentMapper extends EasyBaseMapper<RelSystemEnvironment>{


    List<Integer> findByEnvId(@Param("id") Integer id);

    List<Integer> findByServiceId(@Param("id")Integer id);

    List<Integer> findBySystemId(@Param("systemId") Integer systemId);

}
