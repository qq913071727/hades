package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Mongodb;
import com.innovation.ic.b1b.monitor.base.pojo.MongodbPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodb.MongodbInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.MongodbVo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MongodbMapper extends EasyBaseMapper<Mongodb> {
    List<MongodbPojo> findByPage(MongodbVo mongodbVo);

    /**
     * 查询mongodb信息列表
     * @return 返回查询列表
     */
    List<MongodbInfoRespPojo> queryMongodbInfoList();
}