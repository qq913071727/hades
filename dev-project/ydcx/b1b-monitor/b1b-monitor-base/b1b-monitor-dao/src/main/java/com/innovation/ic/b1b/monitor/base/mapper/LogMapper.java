package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Log;
import com.innovation.ic.b1b.monitor.base.pojo.LogPojo;
import com.innovation.ic.b1b.monitor.base.vo.LogVo;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LogMapper extends EasyBaseMapper<Log> {


    List<LogPojo> findByPage(LogVo logVo);
}
