package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.InterfaceCallLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.interfaceCallLog.InterfaceCallLogPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.userLoginLog.UserLoginLogPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface InterfaceCallLogMapper extends EasyBaseMapper<InterfaceCallLog> {

    List<InterfaceCallLogPojo> page(@Param("systemId") Integer systemId, @Param("environmentId") Integer environmentId,
                                    @Param("serviceId") Integer serviceId, @Param("url") String url,
                                    @Param("beginDate") Date beginDate, @Param("endDate") Date endDate,
                                    @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    List pageCount(@Param("systemId") Integer systemId, @Param("environmentId") Integer environmentId,
                   @Param("serviceId") Integer serviceId, @Param("url") String url,
                   @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
