package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Api;
import com.innovation.ic.b1b.monitor.base.pojo.ApiPojo;
import com.innovation.ic.b1b.monitor.base.vo.ApiVo;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ApiMapper extends EasyBaseMapper<Api> {


    ApiPojo findById(ApiVo apiVo);

    List<ApiPojo> findByPage(ApiVo apiVo);
}
