package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ZookeeperAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.zookeeperAvailableJobLog.ZookeeperAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   Zookeeper任务日志表Mapper类
 * @author linuo
 * @time   2023年5月11日15:14:48
 */
@Repository
public interface ZookeeperAvailableJobLogMapper extends EasyBaseMapper<ZookeeperAvailableJobLog> {
    /**
     * 查询zookeeper监控任务日志列表
     * @param zookeeperAvailableJobLogListQueryVo 查询zookeeper监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<ZookeeperAvailableJobLogListRespData> queryList(@Param("param") ZookeeperAvailableJobLogListQueryVo zookeeperAvailableJobLogListQueryVo);
}