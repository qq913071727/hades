package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJob.RabbitmqAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJob.RabbitmqAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface RabbitmqAvailableJobMapper extends EasyBaseMapper<RabbitmqAvailableJob> {
    /**
     * 查询rabbitmq可用任务列表数据
     * @param rabbitmqAvailableJobListQueryVo 查询rabbitmq监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<RabbitmqAvailableJobListRespData> queryList(@Param("param") RabbitmqAvailableJobListQueryVo rabbitmqAvailableJobListQueryVo);

    /**
     * 获取rabbitmq监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    RabbitmqAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新rabbitmq监控任务
     * @param rabbitmqAvailableJobUpdateVo 更新rabbitmq可用任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") RabbitmqAvailableJobUpdateVo rabbitmqAvailableJobUpdateVo);
}