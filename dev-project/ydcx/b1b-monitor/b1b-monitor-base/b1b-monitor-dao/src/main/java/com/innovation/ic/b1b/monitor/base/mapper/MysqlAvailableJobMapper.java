package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mysqlAvailableJob.MysqlAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.mysqlAvailableJob.MysqlAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MysqlAvailableJobMapper extends EasyBaseMapper<MysqlAvailableJob> {
    /**
     * 查询mysql可用任务列表数据
     * @param mysqlAvailableJobListQueryVo 查询mysql监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<MysqlAvailableJobListRespData> queryList(@Param("param") MysqlAvailableJobListQueryVo mysqlAvailableJobListQueryVo);

    /**
     * 获取mysql监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    MysqlAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新mysql监控任务
     * @param mysqlAvailableJobUpdateVo 更新mysql可用任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") MysqlAvailableJobUpdateVo mysqlAvailableJobUpdateVo);
}