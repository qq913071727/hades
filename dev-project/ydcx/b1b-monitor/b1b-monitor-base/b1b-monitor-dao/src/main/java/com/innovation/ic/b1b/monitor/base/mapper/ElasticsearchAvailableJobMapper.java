package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ElasticsearchAvailableJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJob.ElasticsearchAvailableJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearchAvailableJob.ElasticsearchAvailableJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.elasticsearchAvailableJob.ElasticsearchAvailableJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ElasticsearchAvailableJobMapper extends EasyBaseMapper<ElasticsearchAvailableJob> {
    /**
     * 查询elasticsearch监控任务列表
     * @param elasticsearchAvailableJobListQueryVo 查询Elasticsearch监控任务列表的Vo类
     * @return 返回查询结果
     */
    List<ElasticsearchAvailableJobListRespData> queryList(@Param("param") ElasticsearchAvailableJobListQueryVo elasticsearchAvailableJobListQueryVo);

    /**
     * 获取elasticsearch监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    ElasticsearchAvailableJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新elasticsearch监控任务
     * @param elasticsearchAvailableJobUpdateVo 更新Elasticsearch可用任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") ElasticsearchAvailableJobUpdateVo elasticsearchAvailableJobUpdateVo);
}