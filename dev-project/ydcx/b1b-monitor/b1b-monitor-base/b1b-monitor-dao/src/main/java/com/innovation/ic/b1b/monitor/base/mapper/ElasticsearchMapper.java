package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Elasticsearch;
import com.innovation.ic.b1b.monitor.base.pojo.ElasticsearchPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.elasticsearch.ElasticsearchQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.ElasticsearchVo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ElasticsearchMapper extends EasyBaseMapper<Elasticsearch> {

    List<ElasticsearchPojo> findByPage(ElasticsearchVo elasticsearchVo);

    /**
     * 查询es列表
     * @return 返回查询结果
     */
    List<ElasticsearchQueryRespPojo> queryServerInfoList();
}