package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.SqlServerAvailableJobLogHistory;
import org.springframework.stereotype.Repository;

@Repository
public interface SqlServerAvailableJobLogHistoryMapper extends EasyBaseMapper<SqlServerAvailableJobLogHistory> {

}