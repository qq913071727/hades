package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ServerPerformanceJob;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobInfoRespPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.serverPerformanceJob.ServerPerformanceJobListRespData;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobListQueryVo;
import com.innovation.ic.b1b.monitor.base.vo.serverPerformanceJob.ServerPerformanceJobUpdateVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ServerPerformanceJobMapper extends EasyBaseMapper<ServerPerformanceJob> {
    /**
     * 查询服务器性能任务列表数据
     * @param serverPerformanceJobListQueryVo 服务器监控任务查询列表的Vo类
     * @return 返回查询结果
     */
    List<ServerPerformanceJobListRespData> queryList(@Param("param") ServerPerformanceJobListQueryVo serverPerformanceJobListQueryVo);

    /**
     * 查看服务器监控任务详情
     * @param id 主键id
     * @return 返回查询结果
     */
    ServerPerformanceJobInfoRespPojo info(@Param("id") Integer id);

    /**
     * 更新服务器监控任务
     * @param serverPerformanceJobUpdateVo 更新服务器监控任务的Vo类
     * @return 返回更新结果
     */
    int updateInfo(@Param("param") ServerPerformanceJobUpdateVo serverPerformanceJobUpdateVo);

    /**
     * 查询报警邮箱列表
     * @return 返回查询结果
     */
    List<String> queryAlarmEmailList();

    /**
     * 查询报警手机号列表
     * @return 返回查询结果
     */
    List<String> queryAlarmCellPhoneList();
}