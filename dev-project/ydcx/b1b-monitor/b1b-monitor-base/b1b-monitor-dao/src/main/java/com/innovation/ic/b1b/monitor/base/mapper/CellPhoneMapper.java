package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.CellPhone;
import com.innovation.ic.b1b.monitor.base.pojo.variable.cellPhone.AlarmCellPhoneQueryRespPojo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CellPhoneMapper extends EasyBaseMapper<CellPhone> {
    /**
     * 查询报警手机号列表
     * @return 返回查询结果
     */
    List<AlarmCellPhoneQueryRespPojo> queryAlarmCellPhoneList();
}