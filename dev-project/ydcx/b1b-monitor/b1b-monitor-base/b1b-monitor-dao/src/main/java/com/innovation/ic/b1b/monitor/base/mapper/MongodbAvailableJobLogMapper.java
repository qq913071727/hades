package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.MongodbAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.mongodbAvailableJobLog.MongodbAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.mongodbAvailableJobLog.MongodbAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface MongodbAvailableJobLogMapper extends EasyBaseMapper<MongodbAvailableJobLog> {
    /**
     * 查询mongodb可用任务日志表数据
     * @param mongodbAvailableJobLogListQueryVo 查询mongodb监控任务日志列表接口的Vo类
     * @return 返回查询接口
     */
    List<MongodbAvailableJobLogListRespData> queryList(@Param("param") MongodbAvailableJobLogListQueryVo mongodbAvailableJobLogListQueryVo);

    /**
     * 转移 mongodb_available_job_log 表数据到 mongodb_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferMongodbAvailableJobLogToMongodbAvailableJobLogHistory(String outTime);

    /**
     *删除转移成功的 mongodb_available_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteMongodbAvailableJobLogData(String outTime);
}
