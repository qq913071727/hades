package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.RabbitmqAvailableJobLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListRespData;
import com.innovation.ic.b1b.monitor.base.vo.rabbitmqAvailableJobLog.RabbitmqAvailableJobLogListQueryVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface RabbitmqAvailableJobLogMapper extends EasyBaseMapper<RabbitmqAvailableJobLog> {
    /**
     * 查询rabbitmq可用任务日志表数据
     * @param rabbitmqAvailableJobLogListQueryVo 查询rabbitmq监控任务日志列表接口的Vo类
     * @return 返回查询结果
     */
    List<RabbitmqAvailableJobLogListRespData> queryList(@Param("param") RabbitmqAvailableJobLogListQueryVo rabbitmqAvailableJobLogListQueryVo);

    /**
     * rabbitmq_available_job_log 表数据到 rabbitmq_available_job_log_history 表中
     *
     * @param outTime
     * @return
     */
    int transferRabbitmqAvailableJobLogToRabbitmqAvailableJobLogHistory(String outTime);

    /**
     *
     * 删除转移成功的 rabbitmq_available_job_log 表历史数据
     *
     * @param outTime
     * @return
     */
    int deleteRabbitmqAvailableJobLogData(String outTime);
}