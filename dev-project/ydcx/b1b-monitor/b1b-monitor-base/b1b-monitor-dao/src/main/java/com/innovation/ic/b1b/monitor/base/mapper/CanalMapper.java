package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Canal;
import org.springframework.stereotype.Repository;

/**
 * @desc   canal表Mapper类
 * @author linuo
 * @time   2023年5月12日09:54:56
 */
@Repository
public interface CanalMapper extends EasyBaseMapper<Canal> {

}