package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.PageVisitLog;
import com.innovation.ic.b1b.monitor.base.pojo.variable.pageVisitLog.PageVisitLogPojo;
import com.innovation.ic.b1b.monitor.base.vo.PageVisitLogVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PageVisitLogMapper extends EasyBaseMapper<PageVisitLog> {

    List<PageVisitLogPojo> page(@Param("systemId") Integer systemId, @Param("environmentId") Integer environmentId,
                                @Param("serviceId") Integer serviceId, @Param("url") String url, @Param("beginDate") Date beginDate,
                                @Param("endDate") Date endDate, @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    List pageCount(@Param("systemId") Integer systemId, @Param("environmentId") Integer environmentId,
                   @Param("serviceId") Integer serviceId, @Param("url") String url,
                   @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
