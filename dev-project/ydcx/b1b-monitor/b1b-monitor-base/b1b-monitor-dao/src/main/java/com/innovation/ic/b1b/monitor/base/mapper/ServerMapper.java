package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.Server;
import com.innovation.ic.b1b.monitor.base.pojo.ServerPojo;
import com.innovation.ic.b1b.monitor.base.pojo.variable.server.ServerQueryRespPojo;
import com.innovation.ic.b1b.monitor.base.vo.ServerVo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ServerMapper extends EasyBaseMapper<Server> {
    /**
     * 查询服务器信息列表
     * @return 返回查询结果
     */
    List<ServerQueryRespPojo> queryServerInfoList();

    List<ServerPojo> findByPage(ServerVo serverVo);
}