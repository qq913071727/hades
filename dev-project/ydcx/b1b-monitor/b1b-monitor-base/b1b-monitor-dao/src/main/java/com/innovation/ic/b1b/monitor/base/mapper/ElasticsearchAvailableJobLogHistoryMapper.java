package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.ElasticsearchAvailableJobLogHistory;

public interface ElasticsearchAvailableJobLogHistoryMapper extends EasyBaseMapper<ElasticsearchAvailableJobLogHistory>{

}
