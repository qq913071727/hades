package com.innovation.ic.im.end.base.mapper;

import com.innovation.ic.b1b.monitor.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.Client;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientMapper extends EasyBaseMapper<Client> {
    /**
     * 根据id获取客户端类型
     * @param id 主键
     * @return 返回客户端类型
     */
    Integer getTypeById(@Param("id") String id);

    /**
     * 获取登录类型
     * @return 返回登录类型
     */
    List<Integer> getLoginTypeList();
}