package com.innovation.ic.b1b.monitor.base.mapper;

import com.innovation.ic.b1b.monitor.base.model.MysqlAvailableJobLogHistory;
import org.springframework.stereotype.Repository;

@Repository
public interface MysqlAvailableJobLogHistoryMapper extends EasyBaseMapper<MysqlAvailableJobLogHistory> {

}