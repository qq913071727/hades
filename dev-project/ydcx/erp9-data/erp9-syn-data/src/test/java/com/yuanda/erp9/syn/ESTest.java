package com.yuanda.erp9.syn;

import com.yuanda.erp9.syn.config.RestClientConfig;
import org.elasticsearch.client.RestHighLevelClient;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest(classes = SynApplication.class)
@RunWith(value = SpringRunner.class)
public class ESTest {

    @Resource
    private RestClientConfig restClientConfig;


    @Test
    public void test() {
        AtomicInteger i = new AtomicInteger(1);
        List<RestClientConfig> list = new ArrayList<>();
        Thread thread = new Thread(() -> {
            while (i.getAndIncrement() <= 20000) {
                RestHighLevelClient restHighLevelClient = restClientConfig.restHighLevelClient();
                try {
                    System.out.println("第" + i+ "次"+restHighLevelClient.toString());
                    Thread.sleep(50);

                    restHighLevelClient.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
