package com.yuanda.erp9.syn.mapper.erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统主体更新日志记录表
 * @author myq
 * @since 1.0.0 2022-11-30
 */
@Mapper
public interface CmsTopicUpdateLogMapper extends BaseMapper<CmsTopicUpdateLogEntity> {
    /**
     * 通过系统主题修改是否有更新字段
     * @param systemTopic 系统主题
     * @return 返回修改结果
     */
    int editUpdatedBySystemTopic(@Param("systemTopic") String systemTopic);
}
