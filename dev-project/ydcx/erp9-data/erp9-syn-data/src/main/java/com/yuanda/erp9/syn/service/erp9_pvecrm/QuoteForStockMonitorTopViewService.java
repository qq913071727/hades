package com.yuanda.erp9.syn.service.erp9_pvecrm;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

/**
 * @author zengqinglong
 * @desc 价格预警管理
 * @Date 2023/4/6 14:18
 **/
public interface QuoteForStockMonitorTopViewService {
    /**
     * 根据 型号和有效期日期获取数据
     * @param partNumber
     * @param startDate
     * @param endDate
     */
    BigDecimal findByParNumberAndDate(String partNumber, String startDate, String endDate , BigDecimal rate, SimpleDateFormat simpleFormat);
}
