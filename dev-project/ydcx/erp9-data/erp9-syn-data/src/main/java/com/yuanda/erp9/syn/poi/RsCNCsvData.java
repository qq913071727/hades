package com.yuanda.erp9.syn.poi;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @ClassName RsCNCsvData
 * @Description rs供应商对应的excel(中国数据)
 * @Date 2022/12/9
 * @Author swq
 */
@Data
@Slf4j
public class RsCNCsvData implements Serializable {

    private String distributorPartNumber;//sign标识

    private String manufacturerPartNumber;//型号

    private String manufacturer;//制造商

    private String description;//描述

    private String rohs;//hisp

    private String productUrl;//不需要

    private String quantityAvailable;//不需要

    private String priceQuantity;//最小起订量

    private String imageUrl;//img

    private String productType;//分类

    private String baseUom;//不需要

    private String salesUom;//不需要

    private String packSize;//mpq

    private String hkdPrice1;

    private String hkdPrice2;

    private String hkdPrice3;

    private String hkdPrice4;

    private String hkdPrice5;

    private String attributeName1;

    private String attributeName2;

    private String attributeName3;

    private String attributeName4;

    private String attributeName5;

    private String attributeName6;

    private String attributeName7;

    private String attributeName8;

    private String attributeName9;

    private String attributeName10;

    private String attributeName11;

    private String attributeName12;

    private String attributeName13;

    private String attributeName14;

    private String attributeName15;

    private String attributeValue1;

    private String attributeValue2;

    private String attributeValue3;

    private String attributeValue4;

    private String attributeValue5;

    private String attributeValue6;

    private String attributeValue7;

    private String attributeValue8;

    private String attributeValue9;

    private String attributeValue10;

    private String attributeValue11;

    private String attributeValue12;

    private String attributeValue13;

    private String attributeValue14;

    private String attributeValue15;

    private String stockQtyGb;//库存

    private String stockQtyCN;//库存

    private String shippingWeight;//不需要

    public static RsCNCsvData toRsCNCsvDataFormat(String[] data) {
        try {
            RsCNCsvData rsCNCsvData = new RsCNCsvData();
            rsCNCsvData.setDistributorPartNumber(data[0]);
            rsCNCsvData.setManufacturerPartNumber(data[1]);
            rsCNCsvData.setManufacturer(data[2]);
            rsCNCsvData.setDescription(data[3]);
            rsCNCsvData.setRohs(data[4]);
            rsCNCsvData.setProductUrl(data[5]);
            rsCNCsvData.setQuantityAvailable(data[6]);
            rsCNCsvData.setPriceQuantity(data[7]);
            rsCNCsvData.setImageUrl(data[8]);
            rsCNCsvData.setProductType(data[9]);
            rsCNCsvData.setBaseUom(data[10]);
            rsCNCsvData.setSalesUom(data[11]);
            rsCNCsvData.setPackSize(data[12]);
            rsCNCsvData.setHkdPrice1(data[13]);
            rsCNCsvData.setHkdPrice2(data[14]);
            rsCNCsvData.setHkdPrice3(data[15]);
            rsCNCsvData.setHkdPrice4(data[16]);
            rsCNCsvData.setHkdPrice5(data[17]);
            rsCNCsvData.setAttributeName1(data[18]);
            rsCNCsvData.setAttributeName2(data[19]);
            rsCNCsvData.setAttributeName3(data[20]);
            rsCNCsvData.setAttributeName4(data[21]);
            rsCNCsvData.setAttributeName5(data[22]);
            rsCNCsvData.setAttributeName6(data[23]);
            rsCNCsvData.setAttributeName7(data[24]);
            rsCNCsvData.setAttributeName8(data[25]);
            rsCNCsvData.setAttributeName9(data[26]);
            rsCNCsvData.setAttributeName10(data[27]);
            rsCNCsvData.setAttributeName11(data[28]);
            rsCNCsvData.setAttributeName12(data[29]);
            rsCNCsvData.setAttributeName13(data[30]);
            rsCNCsvData.setAttributeName14(data[31]);
            rsCNCsvData.setAttributeName15(data[32]);
            rsCNCsvData.setAttributeValue1(data[33]);
            rsCNCsvData.setAttributeValue2(data[34]);
            rsCNCsvData.setAttributeValue3(data[35]);
            rsCNCsvData.setAttributeValue4(data[36]);
            rsCNCsvData.setAttributeValue5(data[37]);
            rsCNCsvData.setAttributeValue6(data[38]);
            rsCNCsvData.setAttributeValue7(data[39]);
            rsCNCsvData.setAttributeValue8(data[40]);
            rsCNCsvData.setAttributeValue9(data[41]);
            rsCNCsvData.setAttributeValue10(data[42]);
            rsCNCsvData.setAttributeValue11(data[43]);
            rsCNCsvData.setAttributeValue12(data[44]);
            rsCNCsvData.setAttributeValue13(data[45]);
            rsCNCsvData.setAttributeValue14(data[46]);
            rsCNCsvData.setAttributeValue15(data[47]);
            rsCNCsvData.setStockQtyGb(data[48]);
            rsCNCsvData.setStockQtyCN(data[49]);
            rsCNCsvData.setShippingWeight(data[50]);
            return rsCNCsvData;
        }catch (Exception e){
            log.info("RS中国数据解析异常,放弃当前这条数据");
            return null;
        }

    }
}
