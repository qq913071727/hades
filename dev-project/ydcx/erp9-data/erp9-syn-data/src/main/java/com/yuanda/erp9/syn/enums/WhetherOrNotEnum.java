package com.yuanda.erp9.syn.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   是否枚举
 * @author linuo
 * @time   2022年11月28日16:09:09
 */
public enum WhetherOrNotEnum {
    YES(1,"是"),
    NO(0,"否");

    private Integer code;
    private String desc;

    WhetherOrNotEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static WhetherOrNotEnum of(Integer code) {
        for (WhetherOrNotEnum c : WhetherOrNotEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (WhetherOrNotEnum c : WhetherOrNotEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (WhetherOrNotEnum e : WhetherOrNotEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}