package com.yuanda.erp9.syn.service.erp9;


import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Subject
 * @Description 定义呗观察者
 * @Date 2022/11/15
 * @Author myq
 */
public interface Subject {

    // 存储订阅者
    List<Observer> list = new ArrayList<>();

    // 注册订阅者
    abstract void registerObserver(Observer obs);

    // 移除订阅者
    abstract void removeObserver(Observer obs);

    //通知所有的观察者更新状态
    abstract void notifyAllObservers();

}
