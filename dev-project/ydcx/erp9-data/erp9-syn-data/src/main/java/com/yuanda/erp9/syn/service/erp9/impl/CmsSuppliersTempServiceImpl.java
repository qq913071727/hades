package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.entity.CmsSuppliersTempEntity;
import com.yuanda.erp9.syn.mapper.erp9.CmsSuppliersTempMapper;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @desc   供应商临时表Service实现类
 * @author linuo
 * @time   2023年1月4日11:35:48
 */
@Service
@Slf4j
public class CmsSuppliersTempServiceImpl extends CsvSaveImpl implements CmsSuppliersTempService {
    @Resource
    private CmsSuppliersTempMapper cmsSuppliersTempMapper;

    /**
     * 获取供应商id
     * @param supplierName 供应商名称
     * @param supplier erp的供应商id
     * @return 返回查询结果
     */
    @Override
    public Integer getSupplierIdByUs(String supplierName, String supplier) {
        Integer supplierId = suppliersCache.get(supplierName);
        if(supplierId == null){
            // 插入供应商数据
            CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
            cmsSuppliersTempEntity.setSupplierUs(supplier);
            cmsSuppliersTempEntity.setName(supplierName);
            cmsSuppliersTempEntity.setTag(0);
            int insert = cmsSuppliersTempMapper.insert(cmsSuppliersTempEntity);
            if(insert > 0){
                supplierId = cmsSuppliersTempEntity.getSupplierid();
            }
        }
        return supplierId;
    }
}