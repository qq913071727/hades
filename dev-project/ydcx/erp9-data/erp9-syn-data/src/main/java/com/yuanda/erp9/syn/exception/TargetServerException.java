package com.yuanda.erp9.syn.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName TargetServerException
 * @Description 目标服务器错误（连接拒绝、连接超时、连接重置等异常）
 * @Date 2023/1/4
 * @Author myq
 */
@Getter
@Setter
public class TargetServerException extends RuntimeException {

    private Integer supplier;
    private String errMsg;
    private String fileName;

    /**
     * @Description: 默认构造器
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/410:52
     */
    public TargetServerException(String message){
        super(message);
        this.errMsg = message;
    }


    /**
     * @Description: 带供应商ID的构造器
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/410:52
     */
    public TargetServerException(String message,Integer supplier,String fileName){
        super(message);
        this.errMsg = message;
        this.supplier = supplier;
        this.fileName = fileName;
    }

}
