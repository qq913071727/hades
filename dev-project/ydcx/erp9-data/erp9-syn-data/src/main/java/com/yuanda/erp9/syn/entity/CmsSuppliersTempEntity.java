package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 供应商临时表
 *
 * @author myq
 * @since 1.0.0 2022-11-24
 */
@TableName("cms_suppliers_temp")
@Setter
@Getter
public class CmsSuppliersTempEntity implements Serializable {


    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "供应商id")
    private Integer supplierid;

    @ApiModelProperty(value = "")
    private String supplierUs;

    @ApiModelProperty(value = "供应商名称")
    private String name;

    @ApiModelProperty(value = "0-已有 1-新建")
    private Integer tag;
}