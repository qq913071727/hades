package com.yuanda.erp9.syn.mapper.dyj_middledata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.dyj_middledata.DyjInstorageByPriceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zengqinglong
 * @desc 价格预警读表
 * @Date 2023/4/7 14:19
 **/
@Mapper
public interface DyjInstorageByPriceMapper extends BaseMapper<DyjInstorageByPriceEntity> {
}