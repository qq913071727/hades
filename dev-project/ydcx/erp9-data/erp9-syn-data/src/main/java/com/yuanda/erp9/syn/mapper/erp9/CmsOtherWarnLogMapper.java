package com.yuanda.erp9.syn.mapper.erp9;

import com.yuanda.erp9.syn.core.RootMapper;
import com.yuanda.erp9.syn.entity.CmsOtherWarnLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 其他警告或异常日志
 *
 * @author myq
 * @since 1.0.0 2023-01-04
 */
@Mapper
public interface CmsOtherWarnLogMapper extends RootMapper<CmsOtherWarnLogEntity> {

}
