package com.yuanda.erp9.syn.helper;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.yuanda.erp9.syn.util.MathUtil;

import java.util.*;

/**
 * @ClassName EasyExcelHelper
 * @Description EasyExcelUtil的帮助类
 * @Date 2022/12/1
 * @Author myq
 */
public class CoreStaffExcelHelper {


    public static List<String> sheets = Arrays.asList("consignment", "excess", "franchised", "extra");

    /**
     * 每个sheet的阶梯价格费率表
     */
    private static Map<String, double[]> ladderPrice = new HashMap<>();

    static {
        double[] consignment = {0.97, 0.97, 0.97, 0.97, 0.97, 0.95, 0.95, 0.95};
        double[] excess = {0.9, 0.9, 0.9, 0.9, 0.9, 0.88, 0.88, 0.88};
        double[] franchised = {1, 1, 1, 1, 1, 0.98, 0.98, 0.98};
        double[] extra = {1.06, 1.06, 1.06, 1.06, 1.06, 1.06, 1.06, 1.06};
        ladderPrice.put(sheets.get(0), consignment);
        ladderPrice.put(sheets.get(1), excess);
        ladderPrice.put(sheets.get(2), franchised);
        ladderPrice.put(sheets.get(3), extra);
    }

    // 阶梯价格范围
    private static int[] ladderPriceRange = new int[]{1, 10, 50, 100, 500, 1000, 2000, 4000};

    /**
     * @Description: 获取每个sheet对应的费率
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/114:28
     */
    private static double getLadderRate(String key, int index) {
        if (!sheets.contains(key))
            throw new IllegalArgumentException("非法的参数：" + key);
        if (index < 0 || index >= 8)
            throw new IndexOutOfBoundsException("索引越界：" + index);

        return ladderPrice.get(key)[index];
    }

    /**
     * @Description: 获取费率价格
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/114:40
     */
    public static List<Map<String, String>> getLadderPrice(String key, String... ladderPriceStr) {
        // 阶梯价格
        List<Map<String, String>> priceList = new ArrayList<>(6);
        int last = ladderPriceStr.length - 1;
        int index = 0;
        for (int x = 0; x < ladderPriceStr.length; x++) {
            Map<String, String> priceMap = new LinkedHashMap<>();
            priceMap.put("min", String.valueOf(ladderPriceRange[x]));
            // 0无限大 防止数组越界
            if (x == last) {
                priceMap.put("max", "0");
            } else {
                priceMap.put("max", String.valueOf(ladderPriceRange[x + 1]));
            }
            try {
                // 过滤掉非法数据
                String s = ladderPriceStr[x];
                if (StringUtils.isEmpty(s)) {
                    return priceList;
                }
                // 阶梯价格
                priceMap.put("price", MathUtil.multiply(s.replaceAll(",",""),  getLadderRate(key, x)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 1RMB 2USD
            priceMap.put("currency", "2");
            priceMap.put("position", index++ + "");
            priceMap.put("discount_rate", "0.0000");
            priceMap.put("discount_price", "0.0000");
            priceList.add(priceMap);
        }
        return priceList;
    }


}
