package com.yuanda.erp9.syn.service.erp9;

/**
 * @desc   Waldom数据导入
 * @author linuo
 * @time   2022年11月30日16:26:42
 */
public interface WaldomDataImportService {
    /**
     * 组装CmsGoodsPojo数据
     * @param topicUpdateLogId 系统主体更新日志记录表id
     * @param filePath 文件路径
     */
    String importCmsGoodsPojo(Long topicUpdateLogId, String filePath);
}