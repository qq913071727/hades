package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsSuppliersTempEntity;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.CachePrefixEnum;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.PeiGenesisAnalyeService;
import com.yuanda.erp9.syn.util.CacheUtil;
import com.yuanda.erp9.syn.util.FileUtils;
import com.yuanda.erp9.syn.value.PeiInventoryProperty;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class importPeiInventoryDataTask {
    private static final Logger log = LoggerFactory.getLogger(importPeiInventoryDataTask.class);

    private static CacheUtil.Cache suppliersCache = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());

    @Resource
    private PeiGenesisAnalyeService peiGenesisAnalyeService;

    @Resource
    private PeiInventoryProperty peiInventoryProperty;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    private final String suppliers = "PEI-Genesis";
    private final String crmNum = "US000008259";
    private final String fileName = "PeiInventory";

    /**
     * 导入PEI-Genesis数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    //@Scheduled(cron = "0 49 13 * * ?")
    @XxlJob("importPeiInventory")
    @EnableSchedule
    public void importPeiInventory() {
        //供应商id
        CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
        cmsSuppliersTempEntity.setName(suppliers);
        cmsSuppliersTempEntity.setSupplierUs(crmNum);
        Integer suppliersId = suppliersCache.insertAndGet(cmsSuppliersTempEntity);
        log.info(fileName + "文件下载开始");
        // 1. 通过下载地址获取文件
        String filePath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.PEI_INVENTORY_DOWN_FILE_URL, peiInventoryProperty.getDownloadPath(), suppliersId);
        try {
            if (!Strings.isNullOrEmpty(filePath)) {
                log.info(fileName + "文件下载完成,文件保存路径:[{}]", filePath);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(suppliersId);
                entity.setSystemTopicType(SubjectSupplierEnum.PEI_INVENTORY.getType());
                entity.setSystemTopic(SubjectSupplierEnum.PEI_INVENTORY.getSubject());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                // 更新系统主体更新日志记录表数据
                log.info(fileName + "解析文件数据进行入库操作");
                Map<String,String> paramMap = new HashMap<>();
                paramMap.put("topicUpdateLogId", topicUpdateLogId.toString());
                paramMap.put("filePath", filePath);
                peiGenesisAnalyeService.analysisPeiGenesis(paramMap);
            }
        } catch (Exception e) {
            log.error("解析PeiInventory异常", e);
        }
    }


}