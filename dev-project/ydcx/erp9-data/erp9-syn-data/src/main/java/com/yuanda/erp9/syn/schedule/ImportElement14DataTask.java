package com.yuanda.erp9.syn.schedule;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.config.FileParamConfig;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.Element14TxtImportService;
import com.yuanda.erp9.syn.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc Element14
 * @Date 2022/12/14 16:45
 **/
@Configuration
@EnableScheduling
public class ImportElement14DataTask {
    private static final Logger log = LoggerFactory.getLogger(ImportElement14DataTask.class);

    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Resource
    private Element14TxtImportService element14TxtImportService;

    @Resource
    private CmsSuppliersTempService cmsSuppliersTempService;

    // 供应商名称
    private static final String SUPPLIER = "Element14";

    // 供应商id
    private static final String SUPPLIER_ID = "US000000035";

    /**
     * 导入Element14_CN数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importElement14CNDataJob")
    @EnableSchedule
    public void importElement14CNData() {
        try {
            log.warn("导入Element14_CN数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.warn("1. 通过ELEMENT14_CN下载地址获取文件");
            // 1. 通过下载地址获取文件
            //ELEMENT14_CN 下载文件url
            String fileCNPath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.ELEMENT14_CN_DOWN_FILE_URL, fileParamConfig.getDownloadPath(), supplierId);
            if (StringUtils.isNotEmpty(fileCNPath)) {
                log.warn("Element14_CN数据文件下载完成,文件保存路径:[fileCNPath:{}", fileCNPath);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(supplierId);
                entity.setSystemTopicType(SubjectSupplierEnum.ELEMENT14.getType());
                entity.setSystemTopic(SubjectSupplierEnum.ELEMENT14.getSubject());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                // 导入Element14文件数据
                log.warn("2. 解析文件数据进行入库操作");
                element14TxtImportService.analysisElement14(topicUpdateLogId, fileCNPath);
            } else {
                log.warn("Element14_CN数据文件下载失败,文件路径为空,任务结束:[fileCNPath:{}]", fileCNPath);
            }
        } catch (Exception e) {
            log.warn("下载ELEMENT14_CN文件错误:e", e);
            e.printStackTrace();
        }
    }

    /**
     * 导入Element14_HK数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importElement14HKDataJob")
    @EnableSchedule
    public void importElement14HKData() {
        try {
            log.warn("导入Element14_HK数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.warn("1. 通过下载地址ELEMENT14_HK获取文件");
            //ELEMENT14_HK 下载文件url
            String fileHKPath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.ELEMENT14_HK_DOWN_FILE_URL, fileParamConfig.getDownloadPath(), supplierId);
            if (StringUtils.isNotEmpty(fileHKPath)) {
                log.warn("Element14_HK数据文件下载完成,文件保存路径:[fileHKPath:{}]", fileHKPath);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(supplierId);
                entity.setSystemTopicType(SubjectSupplierEnum.ELEMENT14.getType());
                entity.setSystemTopic(SubjectSupplierEnum.ELEMENT14.getSubject());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                // 导入Element14文件数据
                log.warn("2. 解析Element14_HK文件数据进行入库操作");
                element14TxtImportService.analysisElement14(topicUpdateLogId, fileHKPath);
            } else {
                log.warn("Element14_HK数据文件下载失败,文件路径为空,任务结束:[fileHKPath:{}]", fileHKPath);
            }
        } catch (Exception e) {
            log.warn("下载Element14_HK数据错误:e", e);
            e.printStackTrace();
        }
    }
}
