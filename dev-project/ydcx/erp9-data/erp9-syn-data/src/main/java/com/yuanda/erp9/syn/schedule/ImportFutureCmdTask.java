package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.FutureExcelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Date;

/**
 * @ClassName EventListener
 * @Description 事件监听
 * @Date 2022/11/14
 * @Author myq
 */
@Slf4j
@Component
public class ImportFutureCmdTask {
    @Autowired
    private FutureExcelService futureExcelService;

    @Autowired
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    /**
     * @Description: 命令事件监听
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1414:12
     */
//    @Scheduled(cron = "0 0 4 * * ?")
    @XxlJob("ImportFutureCmdTask_commandEventListener")
    @EnableSchedule
    public void commandEventListener() {
        log.info("*****************future-excel-staring*****************");
        try {
            CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
            entity.setCreateAt(new Date());
            entity.setSupplier(4);
            entity.setSystemTopicType(SubjectSupplierEnum.FUTURE.getType());
            entity.setSystemTopic(SubjectSupplierEnum.FUTURE.getSubject());
            Long pk = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);

            // 下载excel并解析文件
            futureExcelService.parse(pk);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("future更新数据异常： {}" , e);
        }
        log.info("*****************future-excel-completed*****************");
    }
}