package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.service.erp9.InitTopicUpdateLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时初始化日志
 */
@Slf4j
@Component
public class InitTopicUpdateLogTask {
    @Autowired
    private InitTopicUpdateLogService initTopicUpdateLogService;

    /**
     * 初始化日志表
     */
    //@Scheduled(cron = "0 0 0 * * ?")
    @XxlJob("InitTopicUpdateLogTableTask")
    public void InitTopicUpdateLogTableTask() {
        initTopicUpdateLogService.initTopicUpdateLogTable();
    }
}