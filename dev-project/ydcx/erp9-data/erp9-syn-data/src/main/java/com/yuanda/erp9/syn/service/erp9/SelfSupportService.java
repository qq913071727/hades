package com.yuanda.erp9.syn.service.erp9;

/**
 * @ClassName SelfSupportService
 * @Description 自营接口
 * @Date 2023/1/6
 * @Author myq
 */
public interface SelfSupportService {

    /**
     * @Description: 自营解析接口
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/615:01
     */
    void parse(Long pk);
}
