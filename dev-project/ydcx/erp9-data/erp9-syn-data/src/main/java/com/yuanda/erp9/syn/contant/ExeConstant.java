package com.yuanda.erp9.syn.contant;

/**
 * @ClassName ExeConstant
 * @Description exe 常量类， 下面默认的路径对应resources下面的目录文件
 * @Date 2022/11/14
 * @Author myq
 */
public interface ExeConstant {


    /**
     * @Description: exe 默认的路径
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1410:31
     */
    String EXE_DEFAULT_PATH = "/exe/futureelectronics.exe";

    /**
     * @Description: 临时 默认的路径
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1410:31
     */
    String SUNLOCAL_XLSX_PATH = "\\xlsx\\SUNLORD.xls";

    /**
     * @Description: 临时 默认的路径
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1410:31
     */
    String SUNLOCAL_CSV_PATH = "\\xlsx\\publishlt20150501.csv";



}
