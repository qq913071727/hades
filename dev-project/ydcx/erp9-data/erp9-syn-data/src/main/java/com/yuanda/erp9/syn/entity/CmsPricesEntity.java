package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 价格阶梯
 *
 * @author myq
 * @since 1.0.0 2022-11-15
 */
@TableName("cms_prices")
@Setter
@Getter
public class CmsPricesEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer priceid;

    @ApiModelProperty(value = "价格阶梯表id")
    private Integer ladderid;

    @ApiModelProperty(value = "商品id")
    private String goodid;

    @ApiModelProperty(value = "最小值")
    private Long min;

    @ApiModelProperty(value = "最大值")
    private Long max;

    @ApiModelProperty(value = "进价")
    private BigDecimal price;

    @ApiModelProperty(value = "货币1人民币2美元")
    private Integer currency;

    @ApiModelProperty(value = "折扣率（ps）")
    private BigDecimal discountRate;

    @ApiModelProperty(value = "折扣价（ps）")
    private BigDecimal discountPrice;

    @ApiModelProperty(value = "位置")
    private Integer position;

    @ApiModelProperty(value = "发布状态")
    private Integer publish;

    @ApiModelProperty(value = "删除时间")
    private Date deletedAt;

    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    private Date updatedAt;
}