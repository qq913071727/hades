package com.yuanda.erp9.syn.poi;

import lombok.Data;
import org.elasticsearch.common.Strings;

/**
 * @desc   ICGoo数据解析
 * @author linuo
 * @time   2022年12月2日13:31:54
 */
@Data
public class ICGooCsvData {
    /** 行数 */
    private Integer row;

    /** 型号 */
    private String model;

    /** 品牌 */
    private String brand;

    /** 封装 */
    private String packaging;

    /** 批次 */
    private String batch;

    /** 最小起订量 */
    private Long moq;

    /** 库存 */
    private Integer inventory;

    /** 渠道代码 */
    private String channelCode;

    /** 币种 */
    private String currency;

    /** 报价阶梯1 */
    private Long bjQtyBreak1;

    /** 报价阶梯价格1 */
    private String bjPriceBreak1;

    /** 报价阶梯2 */
    private Long bjQtyBreak2;

    /** 报价阶梯价格2 */
    private String bjPriceBreak2;

    /** 报价阶梯3 */
    private Long bjQtyBreak3;

    /** 报价阶梯价格3 */
    private String bjPriceBreak3;

    /** 报价阶梯4 */
    private Long bjQtyBreak4;

    /** 报价阶梯价格4 */
    private String bjPriceBreak4;

    /** 报价阶梯5 */
    private Long bjQtyBreak5;

    /** 报价阶梯价格5 */
    private String bjPriceBreak5;

    /** 报价阶梯6 */
    private Long bjQtyBreak6;

    /** 报价阶梯价格6 */
    private String bjPriceBreak6;

    public static ICGooCsvData toICGooCsvDataFormat(String[] data, Integer i){
        ICGooCsvData icGooCsvData = new ICGooCsvData();
        icGooCsvData.setRow(i + 1);
        icGooCsvData.setModel(data[0]);
        icGooCsvData.setBrand(data[1]);
        icGooCsvData.setPackaging(data[2]);
        icGooCsvData.setBatch(data[3]);
        icGooCsvData.setMoq(!Strings.isNullOrEmpty(data[4]) ? Long.valueOf(data[4]) : null);
        icGooCsvData.setInventory(!Strings.isNullOrEmpty(data[5]) ? Integer.valueOf(data[5]) : null);
        icGooCsvData.setChannelCode(data[6]);
        icGooCsvData.setCurrency(data[7]);
        icGooCsvData.setBjQtyBreak1(!Strings.isNullOrEmpty(data[18]) ? Double.valueOf(data[18]).longValue() : null);
        icGooCsvData.setBjPriceBreak1(data[19]);
        icGooCsvData.setBjQtyBreak2(!Strings.isNullOrEmpty(data[20]) ? Double.valueOf(data[20]).longValue() : null);
        icGooCsvData.setBjPriceBreak2(data[21]);
        icGooCsvData.setBjQtyBreak3(!Strings.isNullOrEmpty(data[22]) ? Double.valueOf(data[22]).longValue() : null);
        icGooCsvData.setBjPriceBreak3(data[23]);
        icGooCsvData.setBjQtyBreak4(!Strings.isNullOrEmpty(data[24]) ? Double.valueOf(data[24]).longValue() : null);
        icGooCsvData.setBjPriceBreak4(data[25]);
        icGooCsvData.setBjQtyBreak5(!Strings.isNullOrEmpty(data[26]) ? Double.valueOf(data[26]).longValue() : null);
        icGooCsvData.setBjPriceBreak5(data[27]);
        icGooCsvData.setBjQtyBreak6(!Strings.isNullOrEmpty(data[28]) ? Double.valueOf(data[28]).longValue() : null);
        icGooCsvData.setBjPriceBreak6(data[29]);
        return icGooCsvData;
    }
}