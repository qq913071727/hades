package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品表-副表
 *
 * @author myq
 * @since 1.0.0 2022-11-15
 */
@TableName("cms_goods_information")
@Setter
@Getter
public class CmsGoodsInformationEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "商品id")
    private String goodid;

    @ApiModelProperty(value = "商品活动")
    private Integer activity;

    @ApiModelProperty(value = "商品主图")
    private String atlas;

    @ApiModelProperty(value = "商品详情")
    private String content;

    @ApiModelProperty(value = "ecnn是否禁用0是1否")
    private Integer ecnn;

    @ApiModelProperty(value = "ecnn号")
    private String ecnnnumber;

    @ApiModelProperty(value = "是否有关税0是1否")
    private Integer tariffs;

    @ApiModelProperty(value = "关税百分比")
    private Integer tariffsrate;

    @ApiModelProperty(value = "是否有商检费0是1否")
    private Integer inspectionCharges;

    @ApiModelProperty(value = "商检费")
    private Double inspectionChargesFee;

    @ApiModelProperty(value = "参数")
    private Object params;

    @ApiModelProperty(value = "删除时间")
    private Date deletedAt;

    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    private Date updatedAt;
}