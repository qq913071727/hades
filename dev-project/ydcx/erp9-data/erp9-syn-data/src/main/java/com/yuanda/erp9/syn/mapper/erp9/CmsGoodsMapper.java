package com.yuanda.erp9.syn.mapper.erp9;

import com.yuanda.erp9.syn.core.RootMapper;
import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * 商品表
 *
 * @author myq
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface CmsGoodsMapper extends RootMapper<CmsGoodsEntity> {
    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void setConcurrentInsert();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void setUniqueChecks();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void setForeignKeyChecks();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void setAutocommit();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void setBulkInsertBufferSize();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void resetConcurrentInsert();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void resetUniqueChecks();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void resetForeignKeyChecks();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void resetAutocommit();

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:25
     */
    void resetBulkInsertBufferSize();

    /**
     * @Description: 清空表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1616:02
     */
    void truncateTable(String truncateSqlStr);

    /**
     * @Description: 批量插入
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1716:38
     */
    @Override
    int insertBatch(List<CmsGoodsEntity> list);

    /**
     * 保存商品信息
     */
    int insertNew(@Param("cmsGoodsEntity") CmsGoodsEntity cmsGoodsEntity);

    /**
     * 根据商品id更新阶梯价格
     *
     * @param goodId 商品id
     * @param prices 阶梯价格配置
     * @return 返回更新结果
     */
    int updatePricesById(@Param("goodId") Integer goodId, @Param("prices") String prices);

    /**
     * @param supplierId
     * @Description: 根据供应商ID查看数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/79:30
     */
    List<CmsGoodsEntity> querySupplierId(@Param("supplierId") Integer supplierId);

    /**
     * @param supplierId
     * @Description: 根据供应商ID查看数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/79:30
     */
    List<CmsGoodsEntity> queryRsSupplierId(@Param("supplierId") Integer supplierId);

    /**
     * @Description: 根据供应商ID和hisp查询数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/711:40
     */
    List<CmsGoodsEntity> querySupplierIdAndHisp(@Param("supplierId") Integer supplierId,@Param("source") Integer source, @Param("hisp") String hispSet);

    /**
     * @Description: 根据供应商ID和hisp查询数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/711:40
     */
    List<CmsGoodsEntity> querySupplierIdAndHispSet(@Param("supplierId") Integer supplierId, @Param("hisps") Set<String> hispSet);

    /**
     * @Description: 根据供应商ID和hisp查询数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/711:40
     */
    List<CmsGoodsEntity> queryRsSupplierIdAndHispSet(@Param("supplierId") Integer supplierId, @Param("source") Integer source, @Param("hisps") Set<String> hispSet);

    /**
     * 更新对应的品牌的汇率
     *
     * @param rate
     * @param tariffsRate
     * @param inspectionChargesFee
     * @param manufacturer
     * @param model
     * @return
     */
    Integer updateRateByManufacturerAndModel(@Param("rate") String rate,
                                             @Param("tariffsRate") Integer tariffsRate,
                                             @Param("inspectionChargesFee") BigDecimal inspectionChargesFee,
                                             @Param("manufacturer") String manufacturer,
                                             @Param("model") String model);

    Integer updateAllRate(@Param("rateSuffix") String rateSuffix,
                          @Param("customsExchangeRate") String customsExchangeRate);

    List<CmsGoodsEntity> findByLimit(@Param("number") Integer number, @Param("pageSize") Integer pageSize);
}