package com.yuanda.erp9.syn.StockPriceMonitor.Request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author zengqinglong
 * @desc erp推送询价请求
 * @Date 2023/4/6 10:57
 **/
@Data
public class ErpPushInquiryPriceRequest implements Serializable {
    //客户必填
    private String clientID;
    //交货地必填
    private String district;
    //币种必填
    private String currency;
    //默认3 3是区分库存价格预警数据
    private Integer source;
    //默认4
    private Integer invoiceType;
    //默认1
    private Integer invoiceMethod;
    private List<Inquirys> inquirys;
    private String summary;

    public static class Inquirys{
        private String bomID;
        private boolean isBomImport;
        private String inputID;
        //型号必填
        private String partNumber;
        //品牌必填
        private String brand;
        //数量必填
        private Integer quantity;
        private String packageCase;
        private String batch;
        private String packing;
        private Integer mpq;
        private Integer unitPrice;
        //销售id 必填
        private String salerID;
        //交货日期必填
        private String deliveryDate;
        //购货性质必填 1:即时采购 2：被动备货 3：主动备货 4：战略备货 5：样品赠品 6：小批量采购 7：大批量采购 8：原厂备货 9：特殊备货
        private Integer nature;
        //是否代理必填
        private boolean isAgent;
        private String channelRequire;
        private List<String> supplierIds;
        private String summary;
        private String bomFileName;
        private String bomFileUrl;
        private String bomLineNo;
        //询价类型必填
        private Integer inquiryOrderType;

        public String getBomID() {
            return bomID;
        }

        public boolean isBomImport() {
            return isBomImport;
        }

        public String getInputID() {
            return inputID;
        }

        public String getPartNumber() {
            return partNumber;
        }

        public String getBrand() {
            return brand;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public String getPackageCase() {
            return packageCase;
        }

        public String getBatch() {
            return batch;
        }

        public String getPacking() {
            return packing;
        }

        public Integer getMpq() {
            return mpq;
        }

        public Integer getUnitPrice() {
            return unitPrice;
        }

        public String getSalerID() {
            return salerID;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public Integer getNature() {
            return nature;
        }

        public boolean getIsAgent() {
            return isAgent;
        }

        public String getChannelRequire() {
            return channelRequire;
        }

        public List<String> getSupplierIds() {
            return supplierIds;
        }

        public String getSummary() {
            return summary;
        }

        public String getBomFileName() {
            return bomFileName;
        }

        public String getBomFileUrl() {
            return bomFileUrl;
        }

        public String getBomLineNo() {
            return bomLineNo;
        }

        public Integer getInquiryOrderType() {
            return inquiryOrderType;
        }

        public void setBomID(String bomID) {
            this.bomID = bomID;
        }

        public void setBomImport(boolean bomImport) {
            isBomImport = bomImport;
        }

        public void setInputID(String inputID) {
            this.inputID = inputID;
        }

        public void setPartNumber(String partNumber) {
            this.partNumber = partNumber;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public void setPackageCase(String packageCase) {
            this.packageCase = packageCase;
        }

        public void setBatch(String batch) {
            this.batch = batch;
        }

        public void setPacking(String packing) {
            this.packing = packing;
        }

        public void setMpq(Integer mpq) {
            this.mpq = mpq;
        }

        public void setUnitPrice(Integer unitPrice) {
            this.unitPrice = unitPrice;
        }

        public void setSalerID(String salerID) {
            this.salerID = salerID;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public void setNature(Integer nature) {
            this.nature = nature;
        }

        public void setIsAgent(boolean isAgent) {
            isAgent = isAgent;
        }

        public void setChannelRequire(String channelRequire) {
            this.channelRequire = channelRequire;
        }

        public void setSupplierIds(List<String> supplierIds) {
            this.supplierIds = supplierIds;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public void setBomFileName(String bomFileName) {
            this.bomFileName = bomFileName;
        }

        public void setBomFileUrl(String bomFileUrl) {
            this.bomFileUrl = bomFileUrl;
        }

        public void setBomLineNo(String bomLineNo) {
            this.bomLineNo = bomLineNo;
        }

        public void setInquiryOrderType(Integer inquiryOrderType) {
            this.inquiryOrderType = inquiryOrderType;
        }
    }
}
