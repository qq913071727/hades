package com.yuanda.erp9.syn.schedule;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.config.FileParamConfig;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.AvnetExcelService;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc avne数据导入
 * @Date 2022/12/14 15:06
 **/
@Configuration
@EnableScheduling
public class ImportAvnetDataTask {
    private static final Logger log = LoggerFactory.getLogger(ImportAvnetDataTask.class);

    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Resource
    private AvnetExcelService avnetExcelServicel;

    @Resource
    private CmsSuppliersTempService cmsSuppliersTempService;
    // 供应商名称
    private static final String SUPPLIER = "Avnet";
    // 供应商id
    private static final String SUPPLIER_ID = "US000000016";

    /**
     * 导入AvnetAsia数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importAvnetAsiaDataJob")
    @EnableSchedule
    public void importAvnetAsiaData() {
        try {
            log.warn("导入Avnet_Asia数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);

            CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
            entity.setCreateAt(new Date());
            entity.setSupplier(supplierId);
            entity.setSystemTopicType(SubjectSupplierEnum.AVENT.getType());
            entity.setSystemTopic(SubjectSupplierEnum.AVENT.getSubject());
            Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);

            log.warn("1. 通过Avnet_Asia下载地址获取文件");
            // 1. 通过下载地址获取文件
            //allic_asia
            String filePathAllicAsia = FileUtils.avnetDownAndReadFile(DownloadUrlConstants.AVNET_LOGIN_USER_NAME,
                    DownloadUrlConstants.AVNET_LOGIN_PASSWORD,
                    DownloadUrlConstants.AVNET_LOGIN_URL,
                    DownloadUrlConstants.AVNET_DOWN_FILE_URL,
                    fileParamConfig.getDownloadPath(),
                    DownloadUrlConstants.AVNET_ASIA_NAME, supplierId);
            if (StringUtils.isNotEmpty(filePathAllicAsia)) {
                log.warn("Avnet 数据allic_asia文件下载完成,文件保存路径:{}", filePathAllicAsia);
                // 导入Avnet文件数据
                log.warn("2. 解析Avnet 数据allic_asia文件进行入库操作");
                avnetExcelServicel.analysisAvnet(topicUpdateLogId, filePathAllicAsia);
            } else {
                log.warn("Avnet 数据allic_asia文件路径为空,不执行解析,任务结束");
            }
        } catch (Exception e) {
            log.warn("下载导入数据错误:e", e);
            e.printStackTrace();
        }
    }

    /**
     * 判断文件是否为有效文件(修改日期为当天的)
     *
     * @param lastModified
     * @return
     */
    public boolean JudgeWhetherTheFileIsValid(Long lastModified) {
        // 当前时间
        Date now = new Date();
        // 默认的年月日的格式. yyyy-MM-dd
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        //获取今天的日期
        String nowDay = sf.format(now);
        //对比的时间
        String day = sf.format(new Date(lastModified));

        return day.equals(nowDay);
    }

    /**
     * 导入AvnetEmea数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importAvnetEmeaDataJob")
    @EnableSchedule
    public void importAvnetEmeaData() {
        try {
            log.info("导入Avnet_Emea数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
            entity.setCreateAt(new Date());
            entity.setSupplier(supplierId);
            entity.setSystemTopicType(SubjectSupplierEnum.AVENT.getType());
            entity.setSystemTopic(SubjectSupplierEnum.AVENT.getSubject());
            Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
            log.warn("1. 通过Avnet_Emea下载地址获取文件");
            //allic_emea
            String filePathAllicEmea = FileUtils.avnetDownAndReadFile(DownloadUrlConstants.AVNET_LOGIN_USER_NAME,
                    DownloadUrlConstants.AVNET_LOGIN_PASSWORD,
                    DownloadUrlConstants.AVNET_LOGIN_URL,
                    DownloadUrlConstants.AVNET_DOWN_FILE_URL,
                    fileParamConfig.getDownloadPath(),
                    DownloadUrlConstants.AVNET_EMEA_NAME, supplierId);
            if (StringUtils.isNotEmpty(filePathAllicEmea)) {
                log.warn("Avnet 数据allic_emea文件下载完成,文件保存路径:{}", filePathAllicEmea);
                // 导入Avnet文件数据
                log.warn("2. 解析Avnet 数据allic_emea文件进行入库操作");
                avnetExcelServicel.analysisAvnet(topicUpdateLogId, filePathAllicEmea);
            } else {
                log.warn("Avnet 数据allic_emea文件路径为空,不执行解析,任务结束");
            }
        } catch (Exception e) {
            log.warn("下载Avnet_Emea数据错误:e", e);
            e.printStackTrace();
        }
    }

    /**
     * 导入Avnet数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importAvnetDataJob")
    @EnableSchedule
    public void importAvnetData() {
        try {
            log.warn("导入Avnet数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);

            CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
            entity.setCreateAt(new Date());
            entity.setSupplier(supplierId);
            entity.setSystemTopicType(SubjectSupplierEnum.AVENT.getType());
            entity.setSystemTopic(SubjectSupplierEnum.AVENT.getSubject());
            Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
            log.warn("1. 通过Avnet下载地址获取文件");
            //allic
            String filePathAllic = FileUtils.avnetDownAndReadFile(DownloadUrlConstants.AVNET_LOGIN_USER_NAME,
                    DownloadUrlConstants.AVNET_LOGIN_PASSWORD,
                    DownloadUrlConstants.AVNET_LOGIN_URL,
                    DownloadUrlConstants.AVNET_DOWN_FILE_URL,
                    fileParamConfig.getDownloadPath(),
                    DownloadUrlConstants.AVNET_NAME, supplierId);
            if (StringUtils.isNotEmpty(filePathAllic)) {
                log.warn("Avnet 数据allic文件下载完成,文件保存路径:{}", filePathAllic);
                // 导入Avnet文件数据
                log.warn("2. 解析Avnet 数据allic文件进行入库操作");
                avnetExcelServicel.analysisAvnet(topicUpdateLogId, filePathAllic);
            } else {
                log.warn("Avnet 数据allic文件路径为空,不执行解析,任务结束");
            }
        } catch (Exception e) {
            log.warn("下载Avnet导入数据错误:e", e);
            e.printStackTrace();
        }
    }


    /**
     * 导入Avnet数据
     * <p>
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importAvnetDataNoDownJob")
    @EnableSchedule
    public void importAvnetDataNoDownJob() {
        try {
            log.warn("1. 解析Avnet 不下载,重新解析当天文件,(需要核实是否是当天已经下好的文件(3个g))");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
            entity.setCreateAt(new Date());
            entity.setSupplier(supplierId);
            entity.setSystemTopicType(SubjectSupplierEnum.AVENT.getType());
            entity.setSystemTopic(SubjectSupplierEnum.AVENT.getSubject());
            Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
            avnetExcelServicel.analysisAvnet(topicUpdateLogId, fileParamConfig.getDownloadPath() + DownloadUrlConstants.AVNET_NAME);
        } catch (Exception e) {
            log.warn("下载Avnet导入数据错误:e", e);
            e.printStackTrace();
        }
    }
}
