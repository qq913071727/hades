package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.entity.CmsOtherWarnLogEntity;
import com.yuanda.erp9.syn.mapper.erp9.CmsOtherWarnLogMapper;
import com.yuanda.erp9.syn.service.erp9.CmsOtherWarnLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName CmsOtherWarnLogServiceImpl
 * @Description
 * @Date 2023/1/4
 * @Author myq
 */
@Service
@Slf4j
public class CmsOtherWarnLogServiceImpl implements CmsOtherWarnLogService {


    @Resource
    private CmsOtherWarnLogMapper cmsOtherWarnLogMapper;

    /**
     * @Description: 新增其他异常
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/410:46
     */
    @Override
    public void addOtherWarnLogEntity(CmsOtherWarnLogEntity entity) {
        cmsOtherWarnLogMapper.insert(entity);
    }
}
