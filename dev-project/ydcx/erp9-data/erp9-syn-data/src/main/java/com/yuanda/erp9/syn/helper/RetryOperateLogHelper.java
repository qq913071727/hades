package com.yuanda.erp9.syn.helper;


import com.alibaba.fastjson.JSON;
import com.yuanda.erp9.syn.execule.RetryOperateLog;
import com.yuanda.erp9.syn.execule.thread.RetryOperateLogThread;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName RetryOperateLogUtil
 * @Description
 * @Date 2022/12/22
 * @Author myq
 */
@Component
public class RetryOperateLogHelper {

    private static RetryOperateLogThread retryOperateLogThread;

    @PostConstruct
    public void instance() {
        retryOperateLogThread = RetryOperateLogThread.getInstance();
    }


    /**
     * @ClassName RetryOperateLogUtil
     * @Description @ex 异常信息 @list 元数据
     * @Date 2022/12/22
     * @Author myq
     */
    public void addAll(List<CmsGoodsPojo> list, String ex) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        Integer supplierId = list.get(0).getCmsGoodsEntity().getSupplier();
        List<RetryOperateLog> logs = new ArrayList<>();
        list.forEach(e -> {
            RetryOperateLog log = RetryOperateLog.builder().exceptionMsg(ex).sourceDataJsonString(JSON.toJSONString(e)).supplierId(supplierId).build();
            logs.add(log);
        });
        retryOperateLogThread.addAll(logs);
    }

    /**
     * @Description: 构建补偿对象
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2215:14
     */
    public void add(Object pojo, Integer supplierId, String ex) {
        retryOperateLogThread.add(RetryOperateLog.builder().exceptionMsg(ex).sourceDataJsonString(JSON.toJSONString(pojo)).supplierId(supplierId).build());
    }


    /**
     * @Description: 构建补偿对象
     * @Params: @isJson true需要 false不需要
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2215:14
     */
    public void add(Object pojo, Integer supplierId, String ex, boolean isJson) {
        if (isJson)
            add(pojo,supplierId,ex);
        else
            retryOperateLogThread.add(RetryOperateLog.builder().exceptionMsg(ex).sourceDataJsonString(pojo.toString()).supplierId(supplierId).build());
    }


}
