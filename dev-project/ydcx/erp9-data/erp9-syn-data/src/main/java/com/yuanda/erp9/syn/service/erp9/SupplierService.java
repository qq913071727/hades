package com.yuanda.erp9.syn.service.erp9;


import com.yuanda.erp9.syn.entity.CmsSuppliersTempEntity;

import java.util.List;

/**
 * @ClassName SupplierService
 * @Description
 * @Date 2022/12/16
 * @Author myq
 */
public interface SupplierService {


    List<CmsSuppliersTempEntity> selectList();

    /**
     * 插入 供应商
     *
     * @param userId
     * @param userName
     * @return
     */
    Integer insertSupplier(String userId, String userName);

    /**
     * @Description: 新增
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2315:13
     */
    void insert(CmsSuppliersTempEntity suppliersTempEntity);
}
