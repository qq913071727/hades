package com.yuanda.erp9.syn.StockPriceMonitor.Response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zengqinglong
 * @desc 登陆响应参数
 * @Date 2023/4/6 10:52
 **/
@Data
public class ErpLoginResponse implements Serializable {
    private boolean success;
    private String token;
    private String refresh_token;
    private Integer xpires_in;
    private String token_type;
}
