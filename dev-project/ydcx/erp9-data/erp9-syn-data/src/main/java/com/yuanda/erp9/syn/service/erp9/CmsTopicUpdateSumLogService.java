package com.yuanda.erp9.syn.service.erp9;

/**
 * @author linuo
 * @desc 系统主体更新数量日志记录表service
 * @time 2023年4月25日11:44:30
 */
public interface CmsTopicUpdateSumLogService {
    /**
     * 插入系统主体更新数量日志记录表数据
     *
     * @param sum              数据数量
     * @param topicUpdateLogId 主题更新日志表主键id
     * @return 返回插入结果
     */
    Integer insertBySum(Long sum, Long topicUpdateLogId);

    /**
     * 插入或更新系统主体更新数量日志记录表数据
     *
     * @param sum
     * @param topicUpdateLogId
     * @return
     */
    Integer insertOrUpdateByTopicUpdateLogId(Long sum, Long topicUpdateLogId);
}
