package com.yuanda.erp9.syn.config;

import com.yuanda.erp9.syn.contant.DatabaseGlobal;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
@Slf4j
public class DataSourceAspect {
    @Pointcut("execution( * com.yuanda.erp9.syn.service.erp9.impl..*.*(..))")
    private void erp9Aspect() {
    }

    @Pointcut("execution(* com.yuanda.erp9.syn.service.rate.impl..*.*(..))")
    private void rateAspect() {
    }

    @Pointcut("execution(* com.yuanda.erp9.syn.service.erp9_pvecrm.impl..*.*(..))")
    private void erp9PveCrmAspect() {
    }
    @Pointcut("execution(* com.yuanda.erp9.syn.service.erp9_pvestandard.impl..*.*(..))")
    private void erp9PveStandardAspect() {
    }
    @Pointcut("execution(* com.yuanda.erp9.syn.service.dyj_middledata.impl..*.*(..))")
    private void dyjMiddleDataAspect() {
    }

    @Before("erp9Aspect()")
    public void cyz() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.erp9);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.erp9);
    }

    @Before("rateAspect()")
    public void pveStandard() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.rate);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.rate);
    }

    @Before("erp9PveCrmAspect()")
    public void erp9PveCrm() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.erp9_pveCrm);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.erp9_pveCrm);
    }

    @Before("erp9PveStandardAspect()")
    public void erp9PveStandard() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.erp9_pvestandard);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.erp9_pvestandard);
    }

    @Before("dyjMiddleDataAspect()")
    public void dyjMiddleData() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.dyj_middleData);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.dyj_middleData);
    }
}