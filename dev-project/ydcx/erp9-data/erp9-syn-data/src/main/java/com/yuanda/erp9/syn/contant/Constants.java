package com.yuanda.erp9.syn.contant;

/**
 * @desc   常量值
 * @author linuo
 * @time   2023年4月25日13:28:01
 */
public class Constants {
    /** 供应商 */
    public static final String SUPPLIER = "supplier";

    /** 主键id */
    public static final String ID = "id";
}