package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.CachePrefixEnum;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateLogMapper;
import com.yuanda.erp9.syn.service.erp9.InitTopicUpdateLogService;
import com.yuanda.erp9.syn.util.CacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Slf4j
@Service
public class InitTopicUpdateLogServiceImpl implements InitTopicUpdateLogService {


    public static CacheUtil.Cache suppliersCache = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());

    @Autowired
    private CmsTopicUpdateLogMapper cmsTopicUpdateLogMapper;


    /**
     * @Description: 初始化供应商更新日志
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/317:24
     */
    @Override
    public void initTopicUpdateLogTable() {
        for (SubjectSupplierEnum subjectSupplierEnum : SubjectSupplierEnum.values()) {
            CmsTopicUpdateLogEntity cmsTopicUpdateLogEntity = new CmsTopicUpdateLogEntity();
            cmsTopicUpdateLogEntity.setSystemTopic(subjectSupplierEnum.getSubject());
            String supplier = subjectSupplierEnum.getSupplier();
           /* CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
            cmsSuppliersTempEntity.setName(supplier);
            cmsSuppliersTempEntity.setSupplierUs("");*/
            Integer suppliersId = suppliersCache.get(supplier);
            if(suppliersId == null){
                log.warn("initTopicUpdateLogTable 供应商未找到,错误数据 subject {} ,supplier {}  ",subjectSupplierEnum.getSubject(),supplier);
                return;
            }
            cmsTopicUpdateLogEntity.setSystemTopicType(subjectSupplierEnum.getType());
            cmsTopicUpdateLogEntity.setSupplier(suppliersId);
            cmsTopicUpdateLogEntity.setUpdated(0);
            cmsTopicUpdateLogEntity.setCreateAt(new Date());
            cmsTopicUpdateLogMapper.insert(cmsTopicUpdateLogEntity);
        }

    }

    /**
     * @Description: 新增 (自己开启事务)
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1616:47
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int insertEntity(CmsTopicUpdateLogEntity entity) {
        return cmsTopicUpdateLogMapper.insert(entity);
    }
}
