package com.yuanda.erp9.syn.service.dyj_middledata;

import com.yuanda.erp9.syn.entity.dyj_middledata.DyjInstorageByYJPriceEntity;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 大赢家库存价格预警写表管理
 * @Date 2023/4/7 14:51
 **/
public interface DyjInstorageByYJPriceService {
    /**
     * 批量插入
     *
     * @param insertDate
     */
    void batchInsert(List<DyjInstorageByYJPriceEntity> insertDate);
}
