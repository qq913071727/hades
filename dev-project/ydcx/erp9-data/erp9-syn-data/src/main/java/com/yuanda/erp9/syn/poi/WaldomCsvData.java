package com.yuanda.erp9.syn.poi;

import lombok.Data;

/**
 * @desc   Waldom数据解析
 * @author linuo
 * @time   2022年12月1日10:53:59
 */
@Data
public class WaldomCsvData {
    /** 行数 */
    private Integer row;

    /**  */
    private String prc;

    /** 型号 */
    private String partNumber;

    /** 制造商名称 */
    private String manufacturerName;

    /** EMEA库存 */
    private String inStockEMEA;

    /** USA库存 */
    private String inStockUSA;

    /** APAC库存 */
    private String inStockAPAC;

    /** CN库存 */
    private String inStockCN;

    /** 零件描述 */
    private String partDescription;

    private String coo;

    private String rohs;

    private String uom;

    private String hts;

    /** 币种 */
    private String currency;

    /** 包装类型 */
    private String packageType;

    /**  */
    private String leadTime;

    /**  */
    private String moq;

    /**  */
    private String spq;

    /** 阶梯价格起订量 */
    private String priceBreakQty1;

    /** 阶梯价格 */
    private String priceBreak1;

    /** 阶梯价格起订量 */
    private String priceBreakQty2;

    /** 阶梯价格 */
    private String priceBreak2;

    /** 阶梯价格起订量 */
    private String priceBreakQty3;

    /** 阶梯价格 */
    private String priceBreak3;

    /** 阶梯价格起订量 */
    private String priceBreakQty4;

    /** 阶梯价格 */
    private String priceBreak4;

    /** 阶梯价格起订量 */
    private String priceBreakQty5;

    /** 阶梯价格 */
    private String priceBreak5;

    /** 阶梯价格起订量 */
    private String priceBreakQty6;

    /** 阶梯价格 */
    private String priceBreak6;

    /** 阶梯价格起订量 */
    private String priceBreakQty7;

    /** 阶梯价格 */
    private String priceBreak7;

    /** 阶梯价格起订量 */
    private String priceBreakQty8;

    /** 阶梯价格 */
    private String priceBreak8;

    /** 阶梯价格起订量 */
    private String priceBreakQty9;

    /** 阶梯价格 */
    private String priceBreak9;

    /**  */
    private String dateCode;

    public static WaldomCsvData toWaldomCsvDataFormat(String[] data, Integer i){
        WaldomCsvData waldomCsvData = new WaldomCsvData();
        waldomCsvData.setRow(i + 1);
        waldomCsvData.setPrc(data[0]);
        waldomCsvData.setPartNumber(data[1]);
        waldomCsvData.setManufacturerName(data[2]);
        waldomCsvData.setInStockEMEA(data[3]);
        waldomCsvData.setInStockUSA(data[4]);
        waldomCsvData.setInStockAPAC(data[5]);
        waldomCsvData.setInStockCN(data[6]);
        waldomCsvData.setPartDescription(data[7]);
        waldomCsvData.setCoo(data[8]);
        waldomCsvData.setRohs(data[9]);
        waldomCsvData.setUom(data[10]);
        waldomCsvData.setHts(data[11]);
        waldomCsvData.setCurrency(data[12]);
        waldomCsvData.setPackageType(data[13]);
        waldomCsvData.setLeadTime(data[14]);
        waldomCsvData.setMoq(data[15]);
        waldomCsvData.setSpq(data[16]);
        waldomCsvData.setPriceBreakQty1(data[17]);
        waldomCsvData.setPriceBreak1(data[18]);
        waldomCsvData.setPriceBreakQty2(data[19]);
        waldomCsvData.setPriceBreak2(data[20]);
        waldomCsvData.setPriceBreakQty3(data[21]);
        waldomCsvData.setPriceBreak3(data[22]);
        waldomCsvData.setPriceBreakQty4(data[23]);
        waldomCsvData.setPriceBreak4(data[24]);
        waldomCsvData.setPriceBreakQty5(data[25]);
        waldomCsvData.setPriceBreak5(data[26]);
        waldomCsvData.setPriceBreakQty6(data[27]);
        waldomCsvData.setPriceBreak6(data[28]);
        waldomCsvData.setPriceBreakQty7(data[29]);
        waldomCsvData.setPriceBreak7(data[30]);
        waldomCsvData.setPriceBreakQty8(data[31]);
        waldomCsvData.setPriceBreak8(data[32]);
        waldomCsvData.setPriceBreakQty9(data[33]);
        waldomCsvData.setPriceBreak9(data[34]);
        waldomCsvData.setDateCode(data[35]);
        return waldomCsvData;
    }
}