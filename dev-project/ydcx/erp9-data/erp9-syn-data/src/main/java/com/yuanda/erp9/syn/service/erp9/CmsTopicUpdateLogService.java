package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;

/**
 * @desc   系统主体更新日志记录表Service
 * @author linuo
 * @time   2022年12月13日13:34:42
 */
public interface CmsTopicUpdateLogService {
    /**
     * 通过系统主题修改是否有更新字段
     * @param systemTopic 系统主题
     * @return 返回修改结果
     */
    int editUpdatedBySystemTopic(String systemTopic);

    /**
     * 插入系统主体更新日志记录表数据
     * @param subjectSupplierEnum 系统主题枚举
     * @return 返回插入结果
     */
    int insertBySystemTopic(SubjectSupplierEnum subjectSupplierEnum);

    /**
     * @Description: 已初始化就更新否则新增（0无数据，1有数据）
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/317:27
     */
    Long updateOrInsertOfNon(CmsTopicUpdateLogEntity cmsTopicUpdateLogEntity);
}