package com.yuanda.erp9.syn.contant;

public interface DatabaseGlobal {
    /**
     * cyz系统
     */
    String erp9 = "erp9";

    /**
     * 标准型号库(sqlserver数据库)
     */
    String rate = "rate";

    /**
     * ERP 询价(sqlserver数据库)
     */
    String erp9_pveCrm = "erp9-pve-crm";

    /**
     * ERP 标准品牌(sqlserver数据库)
     */
    String erp9_pvestandard = "erp9_pvestandard";

    /**
     * 大赢家 (sqlserver数据库)
     */
    String dyj_middleData = "dyj-middle-data";
}