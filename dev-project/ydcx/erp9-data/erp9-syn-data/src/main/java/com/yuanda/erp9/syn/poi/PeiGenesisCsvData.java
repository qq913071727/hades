package com.yuanda.erp9.syn.poi;


import lombok.Data;

@Data
public class PeiGenesisCsvData {

    private String manufacturer;


    private String manufacturerPartNumber;

    private String peiPartNumber;

    private String description;

    
    
    private String productCategory6;

    private String availableStock;


    private String location;


    private String unitOfMeasure;


    private String minimum;


    private String leadTime;


    private String commodityCode;


    private String sedFlag;


    private String rohs;

    private String exportControlType;


    private String exportControlClass;


    private String peigenesisComUrl;


}
