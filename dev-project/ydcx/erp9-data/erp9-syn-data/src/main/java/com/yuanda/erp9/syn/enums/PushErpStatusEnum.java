package com.yuanda.erp9.syn.enums;

/**
 * @author zengqinglong
 * @desc 推送erp枚举
 * @Date 2023/4/7 17:02
 **/
public enum PushErpStatusEnum {
    TO_BE_PUSH(1, "待推送"),
    PUSHED(2, "已推送");

    private Integer status;
    private String desc;

    PushErpStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
