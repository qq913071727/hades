package com.yuanda.erp9.syn.enums;

/**
 * @ClassName ThreadPoolTypeEnum
 * @Description 线程池类型
 * @Date 2022/11/29
 * @Author myq
 */
public enum ThreadPoolTypeEnum {


    /**
     *  专用的
     */
    SPECIAL_PURPOSE,
    /**
     * 通用的
     */
    CURRENCY
    ;


}
