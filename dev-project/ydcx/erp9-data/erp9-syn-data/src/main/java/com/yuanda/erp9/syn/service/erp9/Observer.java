package com.yuanda.erp9.syn.service.erp9;


import com.yuanda.erp9.syn.service.erp9.impl.AbstractSubject;

/**
 * @ClassName Observer
 * @Description  定义观察者
 * @Date 2022/11/15
 * @Author myq
 */
public interface Observer {

    void update(AbstractSubject subject);
}
