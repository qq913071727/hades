package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsOtherWarnLogEntity;

/**
 * @ClassName CmsOutherWarnLogService
 * @Description
 * @Date 2023/1/4
 * @Author myq
 */
public interface CmsOtherWarnLogService {


    void addOtherWarnLogEntity(CmsOtherWarnLogEntity entity);
}
