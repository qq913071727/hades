package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;

/**
 * @desc   品牌表service类
 * @author linuo
 * @time   2023年1月4日13:53:45
 */
public interface CmsBrandsTempService {
    /**
     * 插入数据
     * @param cmsBrandsTempEntity 品牌实体类
     */
    void insert(CmsBrandsTempEntity cmsBrandsTempEntity);
}