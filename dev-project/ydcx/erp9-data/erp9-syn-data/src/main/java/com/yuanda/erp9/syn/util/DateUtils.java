package com.yuanda.erp9.syn.util;

import com.yuanda.erp9.syn.contant.DateConstants;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

public class DateUtils {
    /**
     * 通用时间格式
     */
    public static String simpleDateHms = "yyyy-MM-dd HH:mm:ss";


    public static SimpleDateFormat simpleFormat = new SimpleDateFormat(simpleDateHms);

    public static synchronized Date stringToDate(String date, SimpleDateFormat format) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        try {
            return format.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String dateToString(Date date, String simple) {
        String result = "";
        if (date == null || StringUtils.isEmpty(simple)) {
            return result;
        }
        return new SimpleDateFormat(simple).format(date);
    }

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2617:08
     */
    public static String now() {
        return new SimpleDateFormat(simpleDateHms).format(new Date());
    }


    /**
     * 是否处于当天时间
     *
     * @param nowTime
     * @return
     */
    public static boolean isSameDay(long nowTime) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long startTime = calendar.getTime().getTime();

        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        long endTime = calendar.getTime().getTime();
        if (nowTime >= startTime && nowTime <= endTime) {
            return true;
        }
        return false;
    }


    /**
     * 获取当前年月日
     *
     * @return 返回当前年月日信息
     */
    public static Map<String, Integer> getYearMonthDay() {
        Map<String, Integer> result = new HashMap<>();
        Calendar now = Calendar.getInstance();
        result.put(DateConstants.YEAR, now.get(Calendar.YEAR));
        result.put(DateConstants.MONTH, now.get(Calendar.MONTH) + 1);
        result.put(DateConstants.DAY, now.get(Calendar.DAY_OF_MONTH));
        return result;
    }


    /**
     * 更新时间
     * @param date
     * @return
     */
    public static synchronized String dateToZone(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
      /*  ZoneId zoneId = ZoneId.ofOffset("GMT", ZoneOffset.ofHours(8));
        TimeZone timeZone = TimeZone.getTimeZone(zoneId);
        df.setTimeZone(timeZone);*/
        return df.format(date);
    }

}
