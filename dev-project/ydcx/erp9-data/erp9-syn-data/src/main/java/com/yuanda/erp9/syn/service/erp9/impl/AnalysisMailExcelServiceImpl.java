package com.yuanda.erp9.syn.service.erp9.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yuanda.erp9.syn.contant.RateCalculateConstants;
import com.yuanda.erp9.syn.entity.*;
import com.yuanda.erp9.syn.enums.CachePrefixEnum;
import com.yuanda.erp9.syn.enums.SourceEnum;
import com.yuanda.erp9.syn.execule.thread.Master;
import com.yuanda.erp9.syn.helper.RetryOperateLogHelper;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateSumLogMapper;
import com.yuanda.erp9.syn.poi.AventAsiaHotExcelData;
import com.yuanda.erp9.syn.poi.AventEbvHotExcelData;
import com.yuanda.erp9.syn.poi.SilicaExcelData;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.pojo.CmsPricesPojo;
import com.yuanda.erp9.syn.service.erp9.AnalysisMailExcelService;
import com.yuanda.erp9.syn.util.CacheUtil;
import com.yuanda.erp9.syn.util.DateUtils;
import com.yuanda.erp9.syn.util.EasyExcelUtils;
import com.yuanda.erp9.syn.util.PriceUtil;
import com.yuanda.erp9.syn.util.RateCalculateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

@Service
@Slf4j
public class AnalysisMailExcelServiceImpl implements AnalysisMailExcelService {


    @Resource
    private RetryOperateLogHelper retryOperateLogHelper;

    @Resource
    private CmsTopicUpdateSumLogMapper cmsTopicUpdateSumLogMapper;


    private CacheUtil.Cache brandCache = CacheUtil.init(CachePrefixEnum.BRAND.getKey());


    private CacheUtil.Cache suppliersCache = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());


    private final Long zero = Long.valueOf("0");

    @Override
    public void analysisAventAsiaHot(HashMap<String, String> param) {
        String filePath = param.get("filePath");
        if (StringUtils.isEmpty(filePath)) {
            return;
        }
        AtomicInteger row = new AtomicInteger();

        String hisp = "avhasia";
        String crmNum = "US000006971";
        //供应商名字
        String suppliers = "Avnet-Hot";
        //供应商id
        CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
        cmsSuppliersTempEntity.setName(suppliers);
        cmsSuppliersTempEntity.setSupplierUs(crmNum);
        Integer suppliersId = suppliersCache.insertAndGet(cmsSuppliersTempEntity);
        Master<CmsGoodsPojo> objectMaster = new Master<>(suppliersId, hisp, SourceEnum.EMAIL.getCode());
        try {
            EasyExcelUtils.readExcel(filePath, AventAsiaHotExcelData.class, aventAsiaHotExcelData -> {
                row.getAndIncrement();
                try {
                    objectMaster.add(this.bindAventAsiaHotCmsGoodsPojo(aventAsiaHotExcelData, hisp, row, crmNum, suppliersId, suppliers));
                } catch (Exception e) {
                    log.warn("analysisAventAsiaHot 发生错误 行数 {} ,错误数据 {}  ", row.get() + 1, JSONObject.toJSONString(aventAsiaHotExcelData));
                    retryOperateLogHelper.add(aventAsiaHotExcelData, suppliersId, e.toString());
                }
            });
        } finally {
            objectMaster.isBegin = false;
            //记录总数
            insertCmsTopicUpdateSumLogEntity(Long.valueOf(param.get("topicUpdateLogId")).intValue(),row.get());

        }
    }


    /**
     * 绑定AventAsiaHot对象
     *
     * @param aventAsiaHotExcelData
     * @param hisp
     * @param row
     * @param crmNum
     * @return
     */
    public CmsGoodsPojo bindAventAsiaHotCmsGoodsPojo(AventAsiaHotExcelData aventAsiaHotExcelData, String hisp, AtomicInteger row, String crmNum, Integer suppliersId, String suppliers) {
        CmsGoodsPojo cmsGoodsPojo = new CmsGoodsPojo();
        String sign = hisp + "_" + aventAsiaHotExcelData.getMfrPn() + "_" + row;

        //处理完以后的批次号
        String productDateCode = aventAsiaHotExcelData.getProductDateCode();
        String batchNumber = EasyExcelUtils.processingBatchNumber(productDateCode);
        //型号
        String mfrPn = aventAsiaHotExcelData.getMfrPn();
        //品牌
        String mfrName = aventAsiaHotExcelData.getMfrName();

        //品牌id
        CmsBrandsTempEntity cmsBrandsTempEntity = new CmsBrandsTempEntity();
        cmsBrandsTempEntity.setName(mfrName);
        Integer brandId = brandCache.insertAndGet(cmsBrandsTempEntity);


        String incodes = EasyExcelUtils.getIncodes(sign, crmNum, suppliersId);


        CmsGoodsEntity cmsGoodsEntity = new CmsGoodsEntity();
        cmsGoodsEntity.setGoodid(incodes);
        cmsGoodsEntity.setAdminid(0);
        cmsGoodsEntity.setTenantsid(0);
        cmsGoodsEntity.setIncodes(incodes);

        cmsGoodsEntity.setClassifyid(0);
        cmsGoodsEntity.setImg("");
        cmsGoodsEntity.setType(0);

        cmsGoodsEntity.setStatus(1);
        cmsGoodsEntity.setStatusMsg("");


        cmsGoodsEntity.setDataType(1);
        cmsGoodsEntity.setModel(mfrPn);
        cmsGoodsEntity.setBrand(brandId);
        cmsGoodsEntity.setSupplier(suppliersId);
        cmsGoodsEntity.setIsSampleApply(0);
        cmsGoodsEntity.setBatch(batchNumber);

        cmsGoodsEntity.setOriginalPrice(BigDecimal.ZERO);
        cmsGoodsEntity.setPurchasePrice(BigDecimal.ZERO);
        String indicativeUnitPriceUsd = aventAsiaHotExcelData.getIndicativeUnitPriceUsd();
/*        if (com.innovation.ic.b1b.framework.util.StringUtils.validateParameter(indicativeUnitPriceUsd) && EasyExcelUtils.isPrice(indicativeUnitPriceUsd)) {
            cmsGoodsEntity.setUnitPrice(PriceUtil.handlePrice(indicativeUnitPriceUsd).doubleValue());
        }*/
        cmsGoodsEntity.setRepertory(Integer.valueOf(aventAsiaHotExcelData.getAvailableQty()));
        cmsGoodsEntity.setRepertoryArea("亚洲");
        cmsGoodsEntity.setPublish(1);
        cmsGoodsEntity.setDescribe("");
        cmsGoodsEntity.setFiles("");
        cmsGoodsEntity.setPayType(0);

        cmsGoodsEntity.setMinimumOrder(Long.valueOf(aventAsiaHotExcelData.getMoq()));
        cmsGoodsEntity.setIncremental(Long.valueOf(aventAsiaHotExcelData.getMpq()));
        cmsGoodsEntity.setMpq(Long.valueOf(aventAsiaHotExcelData.getMpq()));

        cmsGoodsEntity.setDeliveryArea("0,1");

        cmsGoodsEntity.setDomesticDate("15-25工作日");
        cmsGoodsEntity.setHongkongDate("13-18工作日");

        cmsGoodsEntity.setHisp(hisp);

        cmsGoodsEntity.setSales(0);
        cmsGoodsEntity.setClick(0);
        cmsGoodsEntity.setVisitor(0);

        cmsGoodsEntity.setCreatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setUpdatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setActivityId(0);
        cmsGoodsEntity.setAtlas("");


        //费率
        JSONObject data = RateCalculateUtil.getRate(cmsGoodsEntity.getBrandName(), cmsGoodsEntity.getModel());

        // 费率信息
        JSONObject rateJsonObject = (JSONObject) data.get(RateCalculateConstants.RATE);

        cmsGoodsEntity.setRates(rateJsonObject);

        //关税
        BigDecimal tariffRate = rateJsonObject.getBigDecimal(RateCalculateConstants.TARIFFRATE_FIELD);
        //商检税
        BigDecimal cIQprice = rateJsonObject.getBigDecimal(RateCalculateConstants.CIQPRICE_FIELD);

        cmsGoodsEntity.setCcc(0);
        cmsGoodsEntity.setEmbargo(0);

        //3c和禁运
        Object ccc = data.get(RateCalculateConstants.CCC_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setCcc(Integer.valueOf(ccc.toString()));
        }

        Object embargo = data.get(RateCalculateConstants.EMBARGO_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setEmbargo(Integer.valueOf(embargo.toString()));
        }
        Object eccn = data.get(RateCalculateConstants.ECCN_FIELD);
        if (eccn != null) {
            cmsGoodsEntity.setEccnNo(eccn.toString());
        }

        //税费计算和存储
        if (tariffRate != null) {
            cmsGoodsEntity.setTariffsRate(tariffRate.doubleValue());
        }
        if (cIQprice != null) {
            cmsGoodsEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }

        cmsGoodsEntity.setSource(SourceEnum.EMAIL.getCode());
        cmsGoodsEntity.setSplName(suppliers);
        cmsGoodsEntity.setBrandName(mfrName);


        List<Map<String, String>> paramsListMap = new ArrayList<>(1);
        Map<String, String> paramsMap = JSON.parseObject(JSON.toJSONString(aventAsiaHotExcelData), new TypeReference<Map<String, String>>() {
        });
        paramsListMap.add(paramsMap);

        //cmsGoodsEntity.setParams(paramsListMap);
        List<CmsPricesEntity> cmsPricesEntities = new ArrayList<>();
        if (com.innovation.ic.b1b.framework.util.StringUtils.validateParameter(indicativeUnitPriceUsd) && EasyExcelUtils.isPrice(indicativeUnitPriceUsd)) {
            CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
            cmsPricesEntity.setGoodid(cmsGoodsEntity.getGoodid());
            cmsPricesEntity.setLadderid(0);
            cmsPricesEntity.setMin(Long.valueOf(aventAsiaHotExcelData.getMoq()));
            cmsPricesEntity.setMax(zero);
            cmsPricesEntity.setPrice(PriceUtil.handlePrice(indicativeUnitPriceUsd));
            cmsPricesEntity.setCurrency(2);
            cmsPricesEntity.setPosition(1);
            cmsPricesEntity.setPublish(1);
            cmsGoodsEntity.setUnitPrice(new Double(0));
            cmsPricesEntities.add(cmsPricesEntity);
        }
/*        if (cmsPricesEntity != null) {
            List<Map<String, String>> pricesListMap = new ArrayList<>(1);
            Map<String, String> pricesMap = JSON.parseObject(JSON.toJSONString(cmsPricesEntity), new TypeReference<Map<String, String>>() {
            });
            pricesListMap.add(pricesMap);
            //cmsGoodsEntity.setPrices(pricesListMap);
        }*/


        List<CmsPricesPojo> prices = new ArrayList<>();
        for (CmsPricesEntity cmsPricesEntityBean : cmsPricesEntities) {
            CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
            BeanUtils.copyProperties(cmsPricesEntityBean, cmsPricesPojo);
            prices.add(cmsPricesPojo);
        }

        // 更新阶梯价格配置到商品表
        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
        cmsGoodsEntity.setPrices(jsonArray.toJSONString());


        cmsGoodsPojo.setCmsPricesEntityList(cmsPricesEntities);


        /*if (cmsGoodsEntity != null) {
            cmsPricesEntities.add(cmsPricesEntity);
        }*/

        cmsGoodsPojo.setCmsPricesEntityList(cmsPricesEntities);

        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setGoodid(cmsGoodsEntity.getGoodid());
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setEcnn(1);


        //修改,税费计算和存储
        if (cIQprice == null) {
            cmsGoodsInformationEntity.setInspectionCharges(0);
            cmsGoodsInformationEntity.setInspectionChargesFee(BigDecimal.ZERO.doubleValue());
        } else {
            cmsGoodsInformationEntity.setInspectionCharges(1);
            cmsGoodsInformationEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }

        if (tariffRate == null) {
            cmsGoodsInformationEntity.setTariffs(1);
        } else {
            cmsGoodsInformationEntity.setTariffs(0);
        }
        //cmsGoodsInformationEntity.setParams(paramsListMap);
        cmsGoodsPojo.setCmsGoodsEntity(cmsGoodsEntity);
        cmsGoodsPojo.setCmsGoodsInformationEntity(cmsGoodsInformationEntity);
        return cmsGoodsPojo;
    }


    @Override
    public void analysisAventEbvHot(HashMap<String, String> param) {
        AtomicInteger row = new AtomicInteger();
        String filePath = param.get("filePath");
        String hisp = "avhebv";
        String crmNum = "US000006971";
        //供应商名字
        String suppliers = "Avnet-Hot";
        //供应商id
        CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
        cmsSuppliersTempEntity.setName(suppliers);
        cmsSuppliersTempEntity.setSupplierUs(crmNum);
        Integer suppliersId = suppliersCache.insertAndGet(cmsSuppliersTempEntity);

        Master<CmsGoodsPojo> objectMaster = new Master<>(suppliersId, hisp, SourceEnum.EMAIL.getCode());
        try {
            EasyExcelUtils.readExcel(filePath, AventEbvHotExcelData.class, new Consumer<AventEbvHotExcelData>() {
                @Override
                public void accept(AventEbvHotExcelData aventEbvHotExcelData) {
                    row.getAndIncrement();
                    try {
                        //供应商id
                        objectMaster.add(bindAventEbvHotCmsGoodsPojo(hisp, aventEbvHotExcelData, row, crmNum, suppliers, suppliersId));
                    } catch (Exception e) {
                        log.warn("analysisAventEbvHot 发生错误 行数需要加1 {} ,错误数据 {}  ", row.get(), JSONObject.toJSONString(aventEbvHotExcelData));
                        retryOperateLogHelper.add(aventEbvHotExcelData, suppliersId, e.toString());
                    }
                }
            });
        } finally {
            objectMaster.isBegin = false;
            insertCmsTopicUpdateSumLogEntity(Long.valueOf(param.get("topicUpdateLogId")).intValue(),row.get());
        }
    }


    private CmsGoodsPojo bindAventEbvHotCmsGoodsPojo(String hisp, AventEbvHotExcelData aventEbvHotExcelData, AtomicInteger row, String crmNum, String suppliers, Integer suppliersId) {
        CmsGoodsPojo cmsGoodsPojo = new CmsGoodsPojo();
        String sign = hisp + "_" + aventEbvHotExcelData.getArticle() + "_" + row;
        //型号
        String article = aventEbvHotExcelData.getArticle();
        //品牌
        String manuf = aventEbvHotExcelData.getManuf();

        //品牌id
        CmsBrandsTempEntity cmsBrandsTempEntity = new CmsBrandsTempEntity();
        cmsBrandsTempEntity.setName(manuf);
        Integer brandId = brandCache.insertAndGet(cmsBrandsTempEntity);


        String incodes = EasyExcelUtils.getIncodes(sign, crmNum, suppliersId);

        String moq = aventEbvHotExcelData.getMoq();
        Long moqInteger = Long.valueOf(moq);

        String mpq = aventEbvHotExcelData.getMpq();
        Long mpqInteger = Long.valueOf(mpq);


        Long finaleMoq = moqInteger;

        String price = aventEbvHotExcelData.getPrice();
        Double priceDouble = Double.valueOf(price);
        //计算moq
        if (priceDouble > 0) {
            //金额大于200美金 最小起订量： 需要满足最小起订金额200美金，同时也满足MPQ的倍数
            if (moqInteger * priceDouble < 200) {
                int p = (int) Math.ceil(200 / (moqInteger * priceDouble));
                finaleMoq = p * moqInteger;
            }

            //起订量必须是mpq的倍数
            if (finaleMoq <= mpqInteger) {
                finaleMoq = mpqInteger;
            }
            //假如moq=4,mpq=3,要满足moq是mpq的倍数，则moq应该为6，是mqp的最小倍数
            else {
                finaleMoq = mpqInteger * ((int) Math.ceil((double) finaleMoq / mpqInteger));
            }
        }


        CmsGoodsEntity cmsGoodsEntity = new CmsGoodsEntity();
        cmsGoodsEntity.setGoodid(incodes);
        cmsGoodsEntity.setAdminid(0);
        cmsGoodsEntity.setTenantsid(0);
        cmsGoodsEntity.setIncodes(incodes);

        cmsGoodsEntity.setClassifyid(0);
        cmsGoodsEntity.setImg("");
        cmsGoodsEntity.setType(0);

        cmsGoodsEntity.setStatus(1);
        cmsGoodsEntity.setStatusMsg("");


        cmsGoodsEntity.setDataType(1);

        cmsGoodsEntity.setModel(article);

        cmsGoodsEntity.setBrand(brandId);
        cmsGoodsEntity.setSupplier(suppliersId);

        cmsGoodsEntity.setIsSampleApply(0);
        cmsGoodsEntity.setBatch("");


        cmsGoodsEntity.setOriginalPrice(BigDecimal.ZERO);
        cmsGoodsEntity.setPurchasePrice(BigDecimal.ZERO);
        //cmsGoodsEntity.setUnitPrice(PriceUtil.handlePrice(aventEbvHotExcelData.getPrice()).doubleValue());


        cmsGoodsEntity.setRepertory(Integer.valueOf(aventEbvHotExcelData.getStock()));
        cmsGoodsEntity.setRepertoryArea("欧洲");
        cmsGoodsEntity.setPublish(1);
        cmsGoodsEntity.setDescribe(aventEbvHotExcelData.getRoHs());
        cmsGoodsEntity.setFiles("");
        cmsGoodsEntity.setPayType(0);


        cmsGoodsEntity.setMinimumOrder(finaleMoq);
        cmsGoodsEntity.setIncremental(Long.valueOf(aventEbvHotExcelData.getMpq()));
        cmsGoodsEntity.setMpq(Long.valueOf(aventEbvHotExcelData.getMpq()));


        cmsGoodsEntity.setDeliveryArea("0,1");
        cmsGoodsEntity.setDomesticDate("10-12工作日");
        cmsGoodsEntity.setHongkongDate("8-10工作日");
        cmsGoodsEntity.setHisp(hisp);

        cmsGoodsEntity.setSales(0);
        cmsGoodsEntity.setClick(0);
        cmsGoodsEntity.setVisitor(0);

        cmsGoodsEntity.setCreatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setUpdatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setActivityId(0);
        cmsGoodsEntity.setAtlas("");


        //费率
        JSONObject data = RateCalculateUtil.getRate(cmsGoodsEntity.getBrandName(), cmsGoodsEntity.getModel());

        // 费率信息
        JSONObject rateJsonObject = (JSONObject) data.get(RateCalculateConstants.RATE);

        cmsGoodsEntity.setRates(rateJsonObject);

        //关税
        BigDecimal tariffRate = rateJsonObject.getBigDecimal(RateCalculateConstants.TARIFFRATE_FIELD);
        //商检税
        BigDecimal cIQprice = rateJsonObject.getBigDecimal(RateCalculateConstants.CIQPRICE_FIELD);

        cmsGoodsEntity.setCcc(0);
        cmsGoodsEntity.setEmbargo(0);

        //3c和禁运
        Object ccc = data.get(RateCalculateConstants.CCC_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setCcc(Integer.valueOf(ccc.toString()));
        }

        Object embargo = data.get(RateCalculateConstants.EMBARGO_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setEmbargo(Integer.valueOf(embargo.toString()));
        }
        Object eccn = data.get(RateCalculateConstants.ECCN_FIELD);
        if (eccn != null) {
            cmsGoodsEntity.setEccnNo(eccn.toString());
        }


        //税费计算和存储
        if (tariffRate != null) {
            cmsGoodsEntity.setTariffsRate(tariffRate.doubleValue());
        }
        if (cIQprice != null) {
            cmsGoodsEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }


        cmsGoodsEntity.setSource(SourceEnum.EMAIL.getCode());
        cmsGoodsEntity.setSplName(suppliers);
        cmsGoodsEntity.setBrandName(manuf);


        List<Map<String, String>> paramsListMap = new ArrayList<>(1);
        Map<String, String> paramsMap = JSON.parseObject(JSON.toJSONString(aventEbvHotExcelData), new TypeReference<Map<String, String>>() {
        });
        paramsListMap.add(paramsMap);


        //cmsGoodsEntity.setParams(paramsListMap);

        CmsPricesEntity cmsPricesEntity = null;

        if (EasyExcelUtils.isPrice(aventEbvHotExcelData.getPrice()) && Double.valueOf(aventEbvHotExcelData.getPrice()) > Double.valueOf("0.00000000000000000000")) {
            cmsPricesEntity = new CmsPricesEntity();
            cmsPricesEntity.setGoodid(cmsGoodsEntity.getGoodid());
            cmsPricesEntity.setLadderid(0);
            cmsPricesEntity.setMin(finaleMoq);
            cmsPricesEntity.setMax(zero);
            cmsPricesEntity.setPrice(PriceUtil.handlePrice(aventEbvHotExcelData.getPrice()));
            cmsPricesEntity.setCurrency(2);
            cmsPricesEntity.setPosition(0);
            cmsPricesEntity.setPublish(1);

        }


        List<CmsPricesEntity> cmsPricesEntities = new ArrayList<>();
/*        List<Map<String, String>> pricesListMap = new ArrayList<>(1);
        Map<String, String> pricesMap = JSON.parseObject(JSON.toJSONString(cmsPricesEntity), new TypeReference<Map<String, String>>() {
        });
        pricesListMap.add(pricesMap);*/
        if (cmsPricesEntity != null) {
            cmsPricesEntities.add(cmsPricesEntity);
        }


        List<CmsPricesPojo> prices = new ArrayList<>();
        for (CmsPricesEntity cmsPricesEntityBean : cmsPricesEntities) {
            CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
            BeanUtils.copyProperties(cmsPricesEntityBean, cmsPricesPojo);
            prices.add(cmsPricesPojo);
        }

        // 更新阶梯价格配置到商品表
        if (prices != null && prices.size() > 0) {
            JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
            cmsGoodsEntity.setPrices(jsonArray.toJSONString());
        }

        if (cmsPricesEntity != null) {
            cmsPricesEntities.add(cmsPricesEntity);
        }

        cmsGoodsPojo.setCmsPricesEntityList(cmsPricesEntities);

        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setGoodid(cmsGoodsEntity.getGoodid());
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setEcnn(1);


        //修改,税费计算和存储
        if (cIQprice == null) {
            cmsGoodsInformationEntity.setInspectionCharges(0);
            cmsGoodsInformationEntity.setInspectionChargesFee(BigDecimal.ZERO.doubleValue());
        } else {
            cmsGoodsInformationEntity.setInspectionCharges(1);
            cmsGoodsInformationEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }


        if (tariffRate == null) {
            cmsGoodsInformationEntity.setTariffs(1);
        } else {
            cmsGoodsInformationEntity.setTariffs(0);
        }

        //cmsGoodsInformationEntity.setParams(paramsListMap);
        cmsGoodsPojo.setCmsGoodsEntity(cmsGoodsEntity);
        cmsGoodsPojo.setCmsGoodsInformationEntity(cmsGoodsInformationEntity);
        return cmsGoodsPojo;
    }


    @Override
    public void analysisSilica(HashMap<String, String> param) {
        AtomicInteger row = new AtomicInteger();
        String filePath = param.get("filePath");
        String hisp = "avhsilica";
        String crmNum = "US000006971";
        String suppliers = "Avnet-Hot";
        //供应商id
        CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
        cmsSuppliersTempEntity.setName(suppliers);
        cmsSuppliersTempEntity.setSupplierUs(crmNum);
        Integer suppliersId = suppliersCache.insertAndGet(cmsSuppliersTempEntity);

        Master<CmsGoodsPojo> objectMaster = new Master<>(suppliersId, hisp, SourceEnum.EMAIL.getCode());
        try {
            EasyExcelUtils.readExcel(filePath, SilicaExcelData.class, new Consumer<SilicaExcelData>() {
                @Override
                public void accept(SilicaExcelData silicaExcelData) {
                    //开始逐行解析入库
                    row.getAndIncrement();
                    String sign = hisp + "_" + silicaExcelData.getDescription() + "_" + row;
                    try {
                        objectMaster.add(bindSilicaCmsGoodsPojo(silicaExcelData, crmNum, sign, hisp, suppliers));
                    } catch (Exception e) {
                        log.warn("analysisSilica 发生错误 行数需要加1 {} ,错误数据 {}  ", row.get(), JSONObject.toJSONString(silicaExcelData));
                        retryOperateLogHelper.add(silicaExcelData, suppliersId, e.toString());
                    }
                }
            });
        } finally {
            objectMaster.isBegin = false;
            insertCmsTopicUpdateSumLogEntity(Long.valueOf(param.get("topicUpdateLogId")).intValue(),row.get());
        }
    }


    private CmsGoodsPojo bindSilicaCmsGoodsPojo(SilicaExcelData silicaExcelData, String crmNum, String sign, String hisp, String suppliers) {
        CmsGoodsPojo cmsGoodsPojo = new CmsGoodsPojo();
        //型号
        String description = silicaExcelData.getDescription();
        //品牌
        String name = silicaExcelData.getName();

        //品牌id
        CmsBrandsTempEntity cmsBrandsTempEntity = new CmsBrandsTempEntity();
        cmsBrandsTempEntity.setName(name);

        Integer brandId = brandCache.insertAndGet(cmsBrandsTempEntity);
        //供应商id
        CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
        cmsSuppliersTempEntity.setName(suppliers);
        cmsSuppliersTempEntity.setSupplierUs(crmNum);
        Integer suppliersId = suppliersCache.insertAndGet(cmsSuppliersTempEntity);

        String incodes = EasyExcelUtils.getIncodes(sign, crmNum, suppliersId);

        CmsGoodsEntity cmsGoodsEntity = new CmsGoodsEntity();
        cmsGoodsEntity.setGoodid(incodes);
        cmsGoodsEntity.setAdminid(0);
        cmsGoodsEntity.setTenantsid(0);
        cmsGoodsEntity.setIncodes(incodes);

        cmsGoodsEntity.setClassifyid(0);
        cmsGoodsEntity.setImg("");
        cmsGoodsEntity.setType(0);

        cmsGoodsEntity.setStatus(1);
        cmsGoodsEntity.setStatusMsg("");


        cmsGoodsEntity.setDataType(1);
        cmsGoodsEntity.setModel(description);
        cmsGoodsEntity.setBrand(brandId);
        cmsGoodsEntity.setSupplier(suppliersId);
        cmsGoodsEntity.setIsSampleApply(0);
        cmsGoodsEntity.setBatch("");


        cmsGoodsEntity.setOriginalPrice(BigDecimal.ZERO);
        cmsGoodsEntity.setPurchasePrice(BigDecimal.ZERO);
        //cmsGoodsEntity.setUnitPrice(PriceUtil.handlePrice(silicaExcelData.getResaleUSDpce()).doubleValue());


        cmsGoodsEntity.setRepertory(Integer.valueOf(silicaExcelData.getQas()));
        cmsGoodsEntity.setRepertoryArea("比利时");
        cmsGoodsEntity.setPublish(1);
        cmsGoodsEntity.setDescribe(silicaExcelData.getTechnicalText());
        cmsGoodsEntity.setFiles("");
        cmsGoodsEntity.setPayType(0);


        String pack = silicaExcelData.getPack();
        Long packQty = Long.valueOf(pack);
        String resaleUSDpce = silicaExcelData.getResaleUSDpce();

        Long finaleMoq = null;
        if (!StringUtils.isEmpty(pack)) {
            if (!StringUtils.isEmpty(resaleUSDpce)) {
                double price = Double.valueOf(resaleUSDpce);
                Long g = Math.max((int) (150 / price) + 1, packQty);
                finaleMoq = (g / (double) packQty > g / packQty ? g / packQty + 1 : g / packQty) * packQty;
            } else {
                finaleMoq = packQty;
            }
        }
        Long mpq = null;
        if (packQty > 0) {
            mpq = finaleMoq;
        }

        cmsGoodsEntity.setMinimumOrder(finaleMoq);
        cmsGoodsEntity.setIncremental(mpq);
        cmsGoodsEntity.setMpq(mpq);

        cmsGoodsEntity.setDeliveryArea("0,1");
        cmsGoodsEntity.setDomesticDate("15-20工作日");
        cmsGoodsEntity.setHongkongDate("13-18工作日");
        cmsGoodsEntity.setHisp(hisp);

        cmsGoodsEntity.setCcc(1);


        cmsGoodsEntity.setSales(0);
        cmsGoodsEntity.setClick(0);
        cmsGoodsEntity.setVisitor(0);

        cmsGoodsEntity.setCreatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setUpdatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));

        cmsGoodsEntity.setActivityId(0);
        cmsGoodsEntity.setAtlas("");

        //税费计算和存储
              /*  cmsGoodsEntity.setTariffsRate(1);
                cmsGoodsEntity.setInspectionChargesFee(BigDecimal.ZERO);*/


        //费率
        JSONObject data = RateCalculateUtil.getRate(cmsGoodsEntity.getBrandName(), cmsGoodsEntity.getModel());

        // 费率信息
        JSONObject rateJsonObject = (JSONObject) data.get(RateCalculateConstants.RATE);

        cmsGoodsEntity.setRates(rateJsonObject);

        //关税
        BigDecimal tariffRate = rateJsonObject.getBigDecimal(RateCalculateConstants.TARIFFRATE_FIELD);
        //商检税
        BigDecimal cIQprice = rateJsonObject.getBigDecimal(RateCalculateConstants.CIQPRICE_FIELD);


        cmsGoodsEntity.setCcc(0);
        cmsGoodsEntity.setEmbargo(0);
        //3c和禁运
        Object ccc = data.get(RateCalculateConstants.CCC_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setCcc(Integer.valueOf(ccc.toString()));
        }

        Object embargo = data.get(RateCalculateConstants.EMBARGO_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setEmbargo(Integer.valueOf(embargo.toString()));
        }
        Object eccn = data.get(RateCalculateConstants.ECCN_FIELD);
        if (eccn != null) {
            cmsGoodsEntity.setEccnNo(eccn.toString());
        }


        //税费计算和存储
        if (tariffRate != null) {
            cmsGoodsEntity.setTariffsRate(tariffRate.doubleValue());
        }
        if (cIQprice != null) {
            cmsGoodsEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }


        cmsGoodsEntity.setSource(SourceEnum.EMAIL.getCode());
        cmsGoodsEntity.setSplName(suppliers);
        cmsGoodsEntity.setBrandName(name);


        List<Map<String, String>> paramsListMap = new ArrayList<>(1);
        Map<String, String> paramsMap = JSON.parseObject(JSON.toJSONString(silicaExcelData), new TypeReference<Map<String, String>>() {
        });
        paramsListMap.add(paramsMap);

        //cmsGoodsEntity.setParams(paramsListMap);


        CmsPricesEntity cmsPricesEntity = null;

        if (EasyExcelUtils.isPrice(silicaExcelData.getResaleUSDpce())) {
            cmsPricesEntity = new CmsPricesEntity();
            cmsPricesEntity.setGoodid(cmsGoodsEntity.getGoodid());
            cmsPricesEntity.setLadderid(0);
            cmsPricesEntity.setMin(packQty);
            cmsPricesEntity.setMax(zero);
            cmsPricesEntity.setPrice(PriceUtil.handlePrice(silicaExcelData.getResaleUSDpce()));
            cmsPricesEntity.setCurrency(2);
            cmsPricesEntity.setPosition(1);
            cmsPricesEntity.setPublish(1);

        }


//                List<Map<String, String>> pricesListMap = new ArrayList<>(1);
//                Map<String, String> pricesMap = JSON.parseObject(JSON.toJSONString(cmsPricesEntity), new TypeReference<Map<String, String>>() {
//                });
//                pricesListMap.add(pricesMap);


        List<CmsPricesEntity> cmsPricesEntities = new ArrayList<>();
      /*  if (cmsPricesEntity != null) {
            List<Map<String, String>> pricesListMap = new ArrayList<>(1);
            Map<String, String> pricesMap = JSON.parseObject(JSON.toJSONString(cmsPricesEntity), new TypeReference<Map<String, String>>() {
            });
            pricesListMap.add(pricesMap);
            cmsGoodsEntity.setPrices(pricesListMap);
            cmsPricesEntities.add(cmsPricesEntity);
        }*/


        if (cmsPricesEntity != null) {
            cmsPricesEntities.add(cmsPricesEntity);
        }


        List<CmsPricesPojo> prices = new ArrayList<>();
        for (CmsPricesEntity cmsPricesEntityBean : cmsPricesEntities) {
            CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
            BeanUtils.copyProperties(cmsPricesEntityBean, cmsPricesPojo);
            prices.add(cmsPricesPojo);
        }

        // 更新阶梯价格配置到商品表
        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
        cmsGoodsEntity.setPrices(jsonArray.toJSONString());


        cmsGoodsPojo.setCmsPricesEntityList(cmsPricesEntities);
        cmsPricesEntity.setGoodid(cmsGoodsEntity.getGoodid());

        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setGoodid(cmsGoodsEntity.getGoodid());
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setEcnn(1);

        //修改,税费计算和存储
        if (cIQprice == null) {
            cmsGoodsInformationEntity.setInspectionCharges(0);
            cmsGoodsInformationEntity.setInspectionChargesFee(BigDecimal.ZERO.doubleValue());
        } else {
            cmsGoodsInformationEntity.setInspectionCharges(1);
            cmsGoodsInformationEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }

        if (tariffRate == null) {
            cmsGoodsInformationEntity.setTariffs(1);
        } else {
            cmsGoodsInformationEntity.setTariffs(0);
        }
        //cmsGoodsInformationEntity.setParams(paramsListMap);

        cmsGoodsPojo.setCmsGoodsEntity(cmsGoodsEntity);
        cmsGoodsPojo.setCmsGoodsInformationEntity(cmsGoodsInformationEntity);
        return cmsGoodsPojo;
    }

    /**
     * 插入系统主体更新数量日志记录表数据
     *
     * @param topicUpdateLogId 主题更新日志表主键id
     * @param dataSize         数据数量
     */
    protected void insertCmsTopicUpdateSumLogEntity(Integer topicUpdateLogId, Integer dataSize) {
        CmsTopicUpdateSumLogEntity cmsTopicUpdateSumLogEntity = new CmsTopicUpdateSumLogEntity();
        cmsTopicUpdateSumLogEntity.setTopicUpdateLogId(topicUpdateLogId);
        cmsTopicUpdateSumLogEntity.setUpdateSum(dataSize);
        cmsTopicUpdateSumLogEntity.setCreateTime(new Date(System.currentTimeMillis()));
        cmsTopicUpdateSumLogMapper.insert(cmsTopicUpdateSumLogEntity);
    }

}
