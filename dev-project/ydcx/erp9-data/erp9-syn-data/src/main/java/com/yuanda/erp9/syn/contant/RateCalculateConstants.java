package com.yuanda.erp9.syn.contant;

/**
 * @desc   费率计算常量值
 * @author linuo
 * @time   2022年12月16日14:44:55
 */
public class RateCalculateConstants {
    /** 零 */
    public static final String ZERO = "0";

    public static final String VATRATE_FIELD = "VATRate";

    public static final String TARIFFRATE_FIELD = "TariffRate";

    public static final String EXCISETAXRATE_FIELD = "ExciseTaxRate";

    public static final String CIQPRICE_FIELD = "CIQprice";

    public static final String ADDEDTARIFFRATE_FIELD = "AddedTariffRate";

    public static final String CUSTOMSEXCHANGERATE_FIELD = "CustomsExchangeRate";

    /** 是否3c认证(1是、0否) */
    public static final String CCC_FIELD = "Ccc";

    /** 是否禁运(1是、0否) */
    public static final String EMBARGO_FIELD = "Embargo";

    /** Eccn */
    public static final String ECCN_FIELD = "Eccn";

    /** 费率 */
    public static final String RATE = "rate";
}