package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.vo.EsCmsGoodsVo;

import java.util.List;

/**
 * @ClassName ESService
 * @Description
 * @Date 2022/12/20
 * @Author myq
 */
public interface ESService {

    /**
     * @Description: 根据主键删除数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2011:03
     */
    void deleteAllByIds(List<String> ids);


    /**
     * @Description: 添加es数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2011:09
     */
    void addESData(List<CmsGoodsPojo> taskList);

    /**
     * 更新es数据
     *
     * @param list
     */
    void updateESData(List<CmsGoodsEntity> list);


    /**
     * @Description: 根绝查看条件删除
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/1415:25
     */
    List<String> deleteByQueryBuilders(Integer supplierId, String hisp, Integer source);

    /**
     * @Description: 根绝查看条件删除
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/1415:25
     */
    void deleteBySupplierIdAndHispAndSource(Integer supplierId, String hisp, Integer source);

    /**
     * 根据条件删除 型号
     *
     * @param supplier
     * @param hisp
     * @param source
     * @param repertoryArea
     */
    void deleteBySupplierIdAndHispAndSourceAndRepertoryArea(Integer supplier, String hisp, Integer source, String repertoryArea);

    /**
     * 根据条件查询商品详情
     *
     * @param supplier
     * @param model
     * @param source
     */
    List<EsCmsGoodsVo> findGoodsBySupplierIdAndModelAndSource(Integer supplier, String model, Integer source);
}
