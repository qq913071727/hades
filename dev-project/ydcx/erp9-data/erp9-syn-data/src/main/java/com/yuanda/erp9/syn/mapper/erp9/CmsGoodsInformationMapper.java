package com.yuanda.erp9.syn.mapper.erp9;

import com.yuanda.erp9.syn.core.RootMapper;
import com.yuanda.erp9.syn.entity.CmsGoodsInformationEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品表-副表
 *
 * @author myq 
 * @since 1.0.0 2022-11-15
 */
@Mapper
public interface CmsGoodsInformationMapper extends RootMapper<CmsGoodsInformationEntity> {


}
