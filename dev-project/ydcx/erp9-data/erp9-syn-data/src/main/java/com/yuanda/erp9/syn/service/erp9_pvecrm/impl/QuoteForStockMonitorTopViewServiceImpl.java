package com.yuanda.erp9.syn.service.erp9_pvecrm.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yuanda.erp9.syn.entity.erp9_precrm.QuoteForStockMonitorTopViewEntity;
import com.yuanda.erp9.syn.enums.ErpCurrencyEnum;
import com.yuanda.erp9.syn.mapper.erp9_precrm.QuoteForStockMonitorTopViewMapper;
import com.yuanda.erp9.syn.service.erp9_pvecrm.QuoteForStockMonitorTopViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zengqinglong
 * @desc 价格预警管理
 * @Date 2023/4/6 14:19
 **/
@Service
@Slf4j
public class QuoteForStockMonitorTopViewServiceImpl implements QuoteForStockMonitorTopViewService {
    @Resource
    private QuoteForStockMonitorTopViewMapper quoteForStockMonitorTopViewMapper;

    /**
     * 获取有效日期里最小单价
     *
     * @param partNumber
     * @param startDate
     * @param endDate
     * @return
     */
    @Override
    public BigDecimal findByParNumberAndDate(String partNumber, String startDate, String endDate, BigDecimal rate, SimpleDateFormat simpleFormat) {
        LambdaQueryWrapper<QuoteForStockMonitorTopViewEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(QuoteForStockMonitorTopViewEntity::getPartNumber, partNumber);
        //获取有效的数据
        queryWrapper.ge(QuoteForStockMonitorTopViewEntity::getCreateDate, startDate + " 00:00:00");
        queryWrapper.le(QuoteForStockMonitorTopViewEntity::getCreateDate, endDate + " 23:59:59");
        queryWrapper.orderByDesc(QuoteForStockMonitorTopViewEntity::getCreateDate);
        List<QuoteForStockMonitorTopViewEntity> quoteForStockMonitorTopViewEntities = quoteForStockMonitorTopViewMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(quoteForStockMonitorTopViewEntities)) {
            return null;
        }

        //排序
        quoteForStockMonitorTopViewEntities = quoteForStockMonitorTopViewEntities.stream()
                .map(QuoteForStockMonitorTopViewEntity -> {
                    //判断是否是美元 如果是美元就需要转换价格
                    if (QuoteForStockMonitorTopViewEntity.getCurrency().equals(ErpCurrencyEnum.DOLLAR.getCode()) ||
                            QuoteForStockMonitorTopViewEntity.getCurrency().equals(ErpCurrencyEnum.USD.getCode())) {
                        BigDecimal price = BigDecimal.valueOf(QuoteForStockMonitorTopViewEntity.getPrice()).multiply(rate);
                        price = price.add(price.multiply(BigDecimal.valueOf(0.05)));
                        QuoteForStockMonitorTopViewEntity.setPrice(price.floatValue());
                    }
                    try {
                        QuoteForStockMonitorTopViewEntity.setCreateDate(simpleFormat.parse(simpleFormat.format(QuoteForStockMonitorTopViewEntity.getCreateDate())));
                    } catch (ParseException e) {
                        log.info("转换时间失败", e);
                    }
                    return QuoteForStockMonitorTopViewEntity;
                }).sorted(Comparator.comparing(QuoteForStockMonitorTopViewEntity::getCreateDate, Comparator.reverseOrder())
                        .thenComparing(QuoteForStockMonitorTopViewEntity::getPrice)).collect(Collectors.toList());

//        //判断是否是人民币 如果是人民币 无需转换价格
//        if (quoteForStockMonitorTopViewEntities.get(0).getCurrency().equals(ErpCurrencyEnum.RMB.getCode()) ||
//                quoteForStockMonitorTopViewEntities.get(0).getCurrency().equals(ErpCurrencyEnum.CNY.getCode())) {
//            return BigDecimal.valueOf(quoteForStockMonitorTopViewEntities.get(0).getPrice());
//        }
//        //美元需要转换成人民币 同时需要在价格上  5  %的点数
//        BigDecimal price = BigDecimal.valueOf(quoteForStockMonitorTopViewEntities.get(0).getPrice()).multiply(rate);
//        price = price.add(price.multiply(BigDecimal.valueOf(0.05)));
        return BigDecimal.valueOf(quoteForStockMonitorTopViewEntities.get(0).getPrice());
    }



}
