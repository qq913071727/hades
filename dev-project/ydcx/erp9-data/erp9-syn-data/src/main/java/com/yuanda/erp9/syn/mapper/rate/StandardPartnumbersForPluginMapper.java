package com.yuanda.erp9.syn.mapper.rate;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.StandardPartnumbersForPluginEntity;
import com.yuanda.erp9.syn.pojo.StandardPartnumbersForPluginDataPojo;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface StandardPartnumbersForPluginMapper extends BaseMapper<StandardPartnumbersForPluginEntity> {
    /**
     * 查询所有数据
     * @return 返回查询结果
     */
    List<StandardPartnumbersForPluginDataPojo> selectList();
}