package com.yuanda.erp9.syn.execule.thread;


/**
 * @ClassName AbstractWorker
 * @Description
 * @Date 2022/11/28
 * @Author myq
 */
public abstract class AbstractWorker<T> implements Worker {


    /**
     * 子类需要初始化此方法，并由父类的线程调用run函数
     */
    abstract void task();

    /**
     * @Description: 通贩抽象类扩展调用子类的函数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2815:13
     */
    @Override
    public void run() {
        // 调用子类方法
        this.task();
    }
}
