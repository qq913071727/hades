package com.yuanda.erp9.syn.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName JsonReaderUtil
 * @Description
 * @Date 2022/12/2
 * @Author myq
 */
public class JsonReaderUtil {


    /**
     * @Description: 返回json对象
     * @Params: filePath json/FutureConfig.json
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/217:57
     */
    public static JSONObject readJsonFile(String filePath) {
        StringBuilder stringBuilder = new StringBuilder();
        InputStreamReader inputStreamReader = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = new ClassPathResource(filePath).getInputStream();
            inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                    inputStreamReader.close();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return JSONObject.parseObject(stringBuilder.toString());
    }
}
