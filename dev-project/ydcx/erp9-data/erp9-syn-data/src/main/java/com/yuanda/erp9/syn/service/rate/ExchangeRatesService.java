package com.yuanda.erp9.syn.service.rate;

import java.math.BigDecimal;

/**
 * @desc   ExchangeRates表的service
 * @author linuo
 * @time   2022年12月16日14:51:02
 */
public interface ExchangeRatesService {

    /**
     * 获取有效数据
     * @return 返回查询结果
     */
    BigDecimal getEffectiveData();
}