package com.yuanda.erp9.syn.service.erp9_pvestandard.impl;

import com.yuanda.erp9.syn.mapper.erp9_pvestandard.PetNamesMapper;
import com.yuanda.erp9.syn.service.erp9_pvestandard.PetNamesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌对照管理管理
 * @Date 2023/4/12 14:46
 **/
@Service
public class PetNamesServiceImpl implements PetNamesService {
    @Resource
    private PetNamesMapper petNamesMapper;

    /**
     *  根据品牌 转换 标准库名称
     *
     * @param brand
     * @return
     */
    @Override
    public List<String> conversionStandardNameByBrand(String brand) {
        return petNamesMapper.conversionStandardNameByBrand(brand);
    }
}
