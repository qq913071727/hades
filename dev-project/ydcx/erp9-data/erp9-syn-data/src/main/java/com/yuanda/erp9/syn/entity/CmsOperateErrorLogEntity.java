package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 操作异常日志
 *
 * @author myq
 * @since 1.0.0 2022-12-22
 */
@TableName("cms_operate_error_log")
@Setter
@Getter
public class CmsOperateErrorLogEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "供应商主键")
    private Integer supplierId;

    @ApiModelProperty(value = "失败的新增json串")
    private String insertFailJson;

    @ApiModelProperty(value = "异常信息")
    private String exceptionMsg;

    @ApiModelProperty(value = "")
    private String createAt;
}