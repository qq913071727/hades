package com.yuanda.erp9.syn.condition;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.stereotype.Component;

/**
 * @Author myq
 * @Date 2023/4/13 13:37
 * @Version 1.0
 * @Description 属性条件
 */
@Component
@Slf4j
public class PriceConfigPropertyCondition implements Condition {

    @Value("${standard.price.enable}")
    private boolean priceFlag;

    /***
     * @param context
     * @param metadata
     * @return boolean
     * @Description:
     * @Author: myq
     * @Date: 2023/4/1313:39
     */
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        log.warn("#################价格预警任务已关闭####################");
        return priceFlag;
    }
}
