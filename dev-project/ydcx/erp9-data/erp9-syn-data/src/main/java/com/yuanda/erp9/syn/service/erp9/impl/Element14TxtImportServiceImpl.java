package com.yuanda.erp9.syn.service.erp9.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.yuanda.erp9.syn.contant.RateCalculateConstants;
import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsInformationEntity;
import com.yuanda.erp9.syn.entity.CmsPricesEntity;
import com.yuanda.erp9.syn.enums.*;
import com.yuanda.erp9.syn.execule.thread.Master;
import com.yuanda.erp9.syn.helper.RetryOperateLogHelper;
import com.yuanda.erp9.syn.poi.Element14TxtData;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.pojo.CmsPricesPojo;
import com.yuanda.erp9.syn.service.erp9.BrandService;
import com.yuanda.erp9.syn.service.erp9.Element14TxtImportService;
import com.yuanda.erp9.syn.service.erp9.SupplierService;
import com.yuanda.erp9.syn.util.*;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zengqinglong
 * @desc Element14
 * @Date 2022/12/5 20:21
 **/
@Service
@Slf4j
public class Element14TxtImportServiceImpl extends CsvSaveImpl implements Element14TxtImportService {
    @Autowired
    private RetryOperateLogHelper retryOperateLogHelper;
    @Resource
    private BrandService brandService;
    @Resource
    private SupplierService supplierService;

    // 供应商名称
    private static final String SUPPLIER = "Element14";

    // 供应商id
    private static final String SUPPLIER_ID = "US000000035";
    protected static CacheUtil.ObjectCache exchangeRates = CacheUtil.objectInit(CachePrefixEnum.EXCHANGE_RATES.getKey());
    private static CacheUtil.Cache brand = CacheUtil.init(CachePrefixEnum.BRAND.getKey());
    private static CacheUtil.Cache suppliers = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());
    private static ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();

    @Override
    public void analysisElement14(Long topicUpdateLogId, String filePath) {
        Master<CmsGoodsPojo> objectMaster = null;
        // 读取csv数据
        CsvReader csvReader = null;
        try {
            File file = new File(filePath);
            //判断文件是否存在
            if (file.isFile() && file.exists()) {
                log.info("Element14数据导入开始");
                Boolean isHK = false;
                //判断文件是否为hk
                if (file.getName().contains("eCat_LIANCHUANG_ELECTRONICS_hk_data.txt")) {
                    isHK = true;
                }
                // 处理供应商信息，缓存中没有当前供应商时添加信息到数据库和缓存中
                Integer supplierId = suppliers.get(SUPPLIER);
                if (supplierId == null) {
                    // 插入供应商数据
                    supplierId = supplierService.insertSupplier(SUPPLIER_ID, SUPPLIER);
                }
                String hisp = isHK ? "1" : "0";


                objectMaster = new Master<>(supplierId, hisp, SourceEnum.DATA_PACKAGE.getCode());


                //txt转换成csv
                String readerFilePath = txtToCsv(filePath);

                csvReader = new CsvReader(readerFilePath, ',');
                // 跳过表头(相当于读取完了表头)
                csvReader.readHeaders();

                long count = 0;
                // 读取每行的内容
                while (csvReader.readRecord()) {
                    count++;
                    // 获取当前行的所有数据(数组形式)
                    //String[] values = csvReader.getValues();
                    Element14TxtData element14TxtData;
                    if (isHK) {
                        element14TxtData = Element14TxtData.toElement14CsvHkDataFormat(csvReader);
                    } else {
                        element14TxtData = Element14TxtData.toElement14CsvCnDataFormat(csvReader);
                    }
                    if (element14TxtData == null) {
                        continue;
                    }

                    element14TxtData.setIsHk(isHK);
                    element14TxtData.setSupplierId(supplierId);
                    CmsGoodsPojo cmsGoodsPojo = insertDataToDatabase(element14TxtData);
                    if (cmsGoodsPojo != null) {
                        objectMaster.add(cmsGoodsPojo);
                    }
                }

                if (topicUpdateLogId != null) {
                    // 插入系统主体更新数量日志记录表数据
                    cmsTopicUpdateSumLogService.insertOrUpdateByTopicUpdateLogId(count, topicUpdateLogId);
                }

                log.info("Element14数据导入开始");
            }
        } catch (Exception e) {
            log.info("Element14数据导入 错误,e");
            e.printStackTrace();
        } finally {
            // 退出标志
            if (objectMaster != null) {
                objectMaster.isBegin = false;
            }
            map.clear();
            // 读取完成之后,关闭流
            if (csvReader != null) {
                csvReader.close();
            }
        }
    }

    //将txt文件存储为csv文件
    public String txtToCsv(String filePath) {
        String writeFile = filePath.substring(0, filePath.length() - 4) + ".csv";
        File file = new File(filePath);
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
        try {
            CsvWriter csvWriter = new CsvWriter(writeFile, ',', Charset.forName("UTF-8"));
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String string = "";
            Integer min = 1;
            while ((string = bufferedReader.readLine()) != null) {
                //将首列转成小写方便取值
                if (min == 1) {
                    String[] s = string.split("\\|");
                    for (int i = 0; i < s.length; i++) {
                        s[i] = s[i].toLowerCase();
                    }
                    csvWriter.writeRecord(s);
                } else {
                    String[] s = string.split("\\|");
                    csvWriter.writeRecord(s);
                }
                min++;
            }
            csvWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writeFile;
    }

    /**
     * 解析 入库
     *
     * @param element14TxtData
     */
    private CmsGoodsPojo insertDataToDatabase(Element14TxtData element14TxtData) {
        try {
            // 包装类
            CmsGoodsPojo cmsGoodsPojo = new CmsGoodsPojo();
            //设置cn常量
            String hisp = element14TxtData.getIsHk() ? "1" : "0";
            //汇率
            BigDecimal rate = new BigDecimal(1.13);
            String depot = "上海";
            String _sign = "cn";
            String delivery = "15-20工作日";
            String hkDelivery = "12-15工作日";
            String hkDelivery2 = "10-13工作日";
            Integer currency = CurrencyEnum.RMB.getCode();
            //库存
            int count = Integer.valueOf(StringUtils.isEmpty(element14TxtData.getWarehouseCN()) ? "0" : element14TxtData.getWarehouseCN()) + Integer.valueOf(element14TxtData.getWarehouseUK()) + Integer.valueOf(element14TxtData.getWarehouseSG()) + Integer.valueOf(element14TxtData.getWarehouseUS());

            //设置hk常量
            if (element14TxtData.getIsHk()) {
                rate = new BigDecimal(0.134);
                depot = "海外";
                _sign = "hk";
                currency = CurrencyEnum.DOLLAR.getCode();
                count = Integer.valueOf(element14TxtData.getWarehouseUK()) + Integer.valueOf(element14TxtData.getWarehouseSG()) + Integer.valueOf(element14TxtData.getWarehouseUS());
            }
            //hk 排除品牌
            String manufacturer = element14TxtData.getManName();
            if (StringUtils.isEmpty(manufacturer)) {
                return null;
            }
            if (currency.equals(CurrencyEnum.DOLLAR.getCode()) && (manufacturer.equals("Analog Devices Inc") || manufacturer.equals("MAXIM") || manufacturer.equals("MAXIM INTEGRATED / ANALOG DEVICES") || manufacturer.equals("ANALOG DEVICES"))) {
                return null;
            }

            //商品类
            CmsGoodsEntity cmsGoodsEntity = new CmsGoodsEntity();
            cmsGoodsEntity.setAdminid(0);
            cmsGoodsEntity.setTenantsid(0);

            cmsGoodsEntity.setClassifyid(0);
            //img
            cmsGoodsEntity.setImg("");
            cmsGoodsEntity.setType(0);
            cmsGoodsEntity.setStatus(1);
            cmsGoodsEntity.setStatusMsg("");
            cmsGoodsEntity.setDataType(1);
            //产品型号
            cmsGoodsEntity.setModel(element14TxtData.getManSku());

            // 处理品牌信息，缓存中没有当前品牌时添加信息到数据库和缓存中
            Integer i = brandCache.get(manufacturer);
            if (i == null) {
                CmsBrandsTempEntity entity = new CmsBrandsTempEntity();
                entity.setName(manufacturer);
                entity.setTag(BrandTagEnum.NEW.getCode());
                CacheUtil.Cache brands = CacheUtil.init(CachePrefixEnum.BRAND.getKey());
                Integer brandId = brands.insertAndGet(entity);
                cmsGoodsEntity.setBrand(brandId);
            } else {
                cmsGoodsEntity.setBrand(i);
            }

            cmsGoodsEntity.setSupplier(element14TxtData.getSupplierId());
            cmsGoodsEntity.setIsSampleApply(0);
            //avnet无批次
            cmsGoodsEntity.setBatch("");
            cmsGoodsEntity.setOriginalPrice(new BigDecimal("0.00"));
            cmsGoodsEntity.setPurchasePrice(new BigDecimal("0.00"));
            cmsGoodsEntity.setUnitPrice(0.00);
            //库存
            cmsGoodsEntity.setRepertory(count);
            // 库存所在地
            cmsGoodsEntity.setRepertoryArea(depot);
            cmsGoodsEntity.setPublish(1);
            //描述
            cmsGoodsEntity.setDescribe("");
            // files
            cmsGoodsEntity.setFiles("");
            cmsGoodsEntity.setPayType(0);
            //最新起订
            Long minmumOrder = Long.valueOf(StringUtils.isEmpty(element14TxtData.getSoq()) ? "1" : element14TxtData.getSoq());
            cmsGoodsEntity.setMinimumOrder(minmumOrder == 0 ? 1L : minmumOrder);
            //增量
            cmsGoodsEntity.setIncremental(minmumOrder == 0 ? 1L : minmumOrder);
            // mpq
            cmsGoodsEntity.setMpq(minmumOrder == 0 ? 1L : minmumOrder);
            // 交货地区
            cmsGoodsEntity.setDeliveryArea(element14TxtData.getIsHk() ? DeliveryAreaEnum.OTHER.getCode() : DeliveryAreaEnum.DOMESTIC.getCode());
            //货期问题，如果原始数据有规则，而且有天数差距的按原始规则走
            //针对香港交货日期和国内交货日期：币种是人民币的香港交货日期是空值,是其他币种的有两种情况：1.如果数据本身就有香港交货日期和国内交货日期，就使用原始的香港交货日期和国内交货日期
            //国内交货日期
            cmsGoodsEntity.setDomesticDate(element14TxtData.getIsHk() ? hkDelivery : delivery);
            //香港交货时间
            cmsGoodsEntity.setHongkongDate(element14TxtData.getIsHk() ? hkDelivery2 : "");
            //hisp
            cmsGoodsEntity.setHisp(hisp);
            cmsGoodsEntity.setSales(0);
            cmsGoodsEntity.setClick(0);
            cmsGoodsEntity.setVisitor(0);
            cmsGoodsEntity.setCreatedAt(DateUtils.now());
            cmsGoodsEntity.setUpdatedAt(DateUtils.now());
            cmsGoodsEntity.setActivityId(0);
            cmsGoodsEntity.setAtlas("");
            // 商品详情
            cmsGoodsEntity.setContent("");
            // 参数
            Map<String, String> paramMap = new LinkedHashMap<>();
            paramMap.put("产品确认", "不需要");
            paramMap.put("UOM", StringUtils.isEmpty(element14TxtData.getUom()) ? "" : element14TxtData.getUom().replace(" ", ""));
            paramMap.put("COO", StringUtils.isEmpty(element14TxtData.getCountryOfOrigin()) ? "" : element14TxtData.getCountryOfOrigin().replace(" ", ""));
            paramMap.put("RoHs Status", StringUtils.isEmpty(element14TxtData.getRoHsStatus()) ? "" : element14TxtData.getRoHsStatus().replace(" ", ""));
            paramMap.put("重量（克）", StringUtils.isEmpty(element14TxtData.getWeight()) ? "" : element14TxtData.getWeight().replace(" ", ""));
            paramMap.put("Hisp", hisp);
            cmsGoodsEntity.setParams(JSON.toJSONString(paramMap));
            cmsGoodsEntity.setSource(SourceEnum.DATA_PACKAGE.getCode());
            //供应商名称
            cmsGoodsEntity.setSplName(SUPPLIER);
            //品牌名称
            cmsGoodsEntity.setBrandName(manufacturer);
            // 费率
            JSONObject data = RateCalculateUtil.getRate(cmsGoodsEntity.getBrandName(), cmsGoodsEntity.getModel());

            // 是否需要3c认证
            String ccc = (String) data.get(RateCalculateConstants.CCC_FIELD);
            cmsGoodsEntity.setCcc(Integer.valueOf(ccc));

            // 是否禁运
            String embargo = (String) data.get(RateCalculateConstants.EMBARGO_FIELD);
            cmsGoodsEntity.setEmbargo(Integer.valueOf(embargo));

            //设置eccn
            String eccn = (String) data.get(RateCalculateConstants.ECCN_FIELD);
            cmsGoodsEntity.setEccnNo(eccn);

            // 费率信息
            JSONObject jsonObject = (JSONObject) data.get(RateCalculateConstants.RATE);
            cmsGoodsEntity.setRates(jsonObject);
            if (jsonObject != null && !jsonObject.isEmpty()) {
                // 关税率
                String tariffsRate = jsonObject.getString(RateCalculateConstants.TARIFFRATE_FIELD);
                cmsGoodsEntity.setTariffsRate(Double.valueOf(tariffsRate));
                // 商检费
                String ciqPrice = jsonObject.getString(RateCalculateConstants.CIQPRICE_FIELD);
                cmsGoodsEntity.setInspectionChargesFee(Double.valueOf(ciqPrice));
            }
            //cmsGoodsEntity.setTariffsRate(0);
            //cmsGoodsEntity.setInspectionChargesFee(BigDecimal.ZERO);
            //特殊算法
            String sign = _sign + "_" + element14TxtData.getElement14OrderCode();
            if (map.containsKey(sign)) {
                return null;
            } else {
                map.put(sign, 0);
            }
            String incodes = EasyExcelUtils.getIncodes(sign, SUPPLIER_ID, cmsGoodsEntity.getSupplier());
            cmsGoodsEntity.setIncodes(incodes);
            cmsGoodsEntity.setGoodid(incodes);
            // 阶梯价格信息
            cmsGoodsPojo.setCmsPricesEntityList(cmsPrices(element14TxtData, cmsGoodsEntity, rate, currency));
            //商品信息
            cmsGoodsPojo.setCmsGoodsEntity(cmsGoodsEntity);
            // 商品副表信息
            cmsGoodsPojo.setCmsGoodsInformationEntity(cmsGoodsInformation(cmsGoodsEntity));

            return cmsGoodsPojo;
        } catch (Exception e) {
            log.warn("解析异常,放弃当前这条数据,element14TxtData:{}", JSON.toJSON(element14TxtData));
            retryOperateLogHelper.add(element14TxtData, element14TxtData.getSupplierId(), e.getMessage());
            return null;
        }
    }

    /**
     * 保存阶梯价格信息
     *
     * @param element14TxtData element14数据解析
     * @param cmsGoodsEntity   商品信息
     */
    private List<CmsPricesEntity> cmsPrices(Element14TxtData element14TxtData, CmsGoodsEntity
            cmsGoodsEntity, BigDecimal rate, Integer currency) throws Exception {
        try {
            List<CmsPricesEntity> result = new ArrayList<>();
            Long max = 0L;
            // 一级阶梯价格
            Long b1 = Long.valueOf(element14TxtData.getB1());
            if (b1 > 0 && Double.valueOf(element14TxtData.getP1()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b2 = Long.valueOf(element14TxtData.getB2());
                if (b2 == 0) {
                    max = 0L;
                } else {
                    max = b2 - 1;
                }
                result.add(cmsPriceData(b1, max, element14TxtData.getP1(), currency, 1, rate, cmsGoodsEntity.getGoodid()));
                //获取第一个阶梯价格
                if (b1 == 0) {
                    b1 = 1L;
                }
                //设置最小起订量
                if (cmsGoodsEntity.getMinimumOrder() < b1) {
                    cmsGoodsEntity.setMinimumOrder(b1);
                    //增量
                    cmsGoodsEntity.setIncremental(b1);
                    // mpq
                    cmsGoodsEntity.setMpq(b1);
                }
                if (currency.equals(CurrencyEnum.DOLLAR.getCode())) {

                }
                /*Double RMBPrice;
                //判断是否是美元,如果是美元需要转换成人民币
                if (currency.equals(CurrencyEnum.DOLLAR.getCode())) {
                    Object o = exchangeRates.get(CachePrefixEnum.EXCHANGE_RATES.getKey());
                    if (o != null) {
                        RMBPrice = (BigDecimal.valueOf(Double.valueOf(Double.valueOf(element14TxtData.getP1()))).multiply((BigDecimal) o)).doubleValue();
                    } else {
                        RMBPrice = Double.valueOf(element14TxtData.getP1());
                    }
                } else {
                    RMBPrice = Double.valueOf(element14TxtData.getP1());
                }
                //设置单价
                //cmsGoodsEntity.setUnitPrice(Double.parseDouble(MathUtil.divide(RMBPrice, b1)));*/
            }

            // 二级阶梯价格
            Long b2 = Long.valueOf(element14TxtData.getB2());
            if (b2 > 0 && Double.valueOf(element14TxtData.getP2()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b3 = Long.valueOf(element14TxtData.getB3());
                if (b3 == 0) {
                    max = 0L;
                } else {
                    max = b3 - 1;
                }
                result.add(cmsPriceData(b2, max, element14TxtData.getP2(), currency, 2, rate, cmsGoodsEntity.getGoodid()));
            }

            // 三级阶梯价格
            Long b3 = Long.valueOf(element14TxtData.getB3());
            if (b3 > 0 && Double.valueOf(element14TxtData.getP3()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b4 = Long.valueOf(element14TxtData.getB4());
                if (b4 == 0) {
                    max = 0L;
                } else {
                    max = b4 - 1;
                }
                result.add(cmsPriceData(b3, max, element14TxtData.getP3(), currency, 3, rate, cmsGoodsEntity.getGoodid()));
            }

            // 四级阶梯价格
            Long b4 = Long.valueOf(element14TxtData.getB4());
            if (b4 > 0 && Double.valueOf(element14TxtData.getP4()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b5 = Long.valueOf(element14TxtData.getB5());
                if (b5 == 0) {
                    max = 0L;
                } else {
                    max = b5 - 1;
                }
                result.add(cmsPriceData(b4, max, element14TxtData.getP4(), currency, 4, rate, cmsGoodsEntity.getGoodid()));
            }

            // 五级阶梯价格
            Long b5 = Long.valueOf(element14TxtData.getB5());
            if (b5 > 0 && Double.valueOf(element14TxtData.getP5()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b6 = Long.valueOf(element14TxtData.getB6());
                if (b6 == 0) {
                    max = 0L;
                } else {
                    max = b6 - 1;
                }
                result.add(cmsPriceData(b5, max, element14TxtData.getP5(), currency, 5, rate, cmsGoodsEntity.getGoodid()));
            }
            // 六级阶梯价格
            Long b6 = Long.valueOf(element14TxtData.getB6());
            if (b6 > 0 && Double.valueOf(element14TxtData.getP6()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b7 = Long.valueOf(element14TxtData.getB7());
                if (b7 == 0) {
                    max = 0L;
                } else {
                    max = b7 - 1;
                }
                result.add(cmsPriceData(b6, max, element14TxtData.getP6(), currency, 6, rate, cmsGoodsEntity.getGoodid()));
            }

            // 七级阶梯价格
            Long b7 = Long.valueOf(element14TxtData.getB7());
            if (b7 > 0 && Double.valueOf(element14TxtData.getP7()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b8 = Long.valueOf(element14TxtData.getB8());
                if (b8 == 0) {
                    max = 0L;
                } else {
                    max = b8 - 1;
                }
                result.add(cmsPriceData(b7, max, element14TxtData.getP7(), currency, 7, rate, cmsGoodsEntity.getGoodid()));
            }
            // 八级阶梯价格
            Long b8 = Long.valueOf(element14TxtData.getB8());
            if (b8 > 0 && Double.valueOf(element14TxtData.getP8()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b9 = Long.valueOf(element14TxtData.getB9());
                if (b9 == 0) {
                    max = 0L;
                } else {
                    max = b9 - 1;
                }
                result.add(cmsPriceData(b8, max, element14TxtData.getP8(), currency, 8, rate, cmsGoodsEntity.getGoodid()));
            }

            // 九级阶梯价格
            Long b9 = Long.valueOf(element14TxtData.getB9());
            if (b9 > 0 && Double.valueOf(element14TxtData.getP9()) > 0) {
                //判断当前是不是最后一个阶梯价格
                Long b10 = Long.valueOf(element14TxtData.getB10());
                if (b10 == 0) {
                    max = 0L;
                } else {
                    max = b10 - 1;
                }
                result.add(cmsPriceData(b9, max, element14TxtData.getP9(), currency, 9, rate, cmsGoodsEntity.getGoodid()));
            }
            // 十级阶梯价格
            Long b10 = Long.valueOf(element14TxtData.getB10());
            if (b10 > 0 && Double.valueOf(element14TxtData.getP10()) > 0) {
                result.add(cmsPriceData(b10, 0L, element14TxtData.getP10(), currency, 10, rate, cmsGoodsEntity.getGoodid()));
            }


            List<CmsPricesPojo> prices = new ArrayList<>();
            for (CmsPricesEntity cmsPricesEntity : result) {
                CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
                BeanUtils.copyProperties(cmsPricesEntity, cmsPricesPojo);
                prices.add(cmsPricesPojo);
            }

            // 更新阶梯价格配置到商品表
            JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
            cmsGoodsEntity.setPrices(jsonArray.toJSONString());

            return result;
        } catch (Exception e) {

            throw new Exception("解析价格异常");
        }
    }

    /**
     * 阶梯价格数据
     *
     * @param min      最小值
     * @param max      最大值
     * @param price    进价
     * @param currency 货币1人民币2美元
     * @param position 位置
     * @param rate     汇率
     */
    private CmsPricesEntity cmsPriceData(Long min, Long max, String price, Integer currency, Integer
            position, BigDecimal rate, String goodid) {
        CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
        cmsPricesEntity.setGoodid(goodid);
        cmsPricesEntity.setLadderid(0);
        cmsPricesEntity.setMin(min);
        cmsPricesEntity.setMax(max == null ? 0L : max);
        if (!Strings.isEmpty(price)) {
            cmsPricesEntity.setPrice(PriceUtil.handlePrice(BigDecimal.valueOf(Double.valueOf(price)).multiply(rate).toString()));
        } else {
            cmsPricesEntity.setPrice(PriceUtil.handlePrice("0"));
        }
        cmsPricesEntity.setCurrency(currency);
        cmsPricesEntity.setPosition(position);
        cmsPricesEntity.setPublish(1);
        cmsPricesEntity.setCreatedAt(new Date(System.currentTimeMillis()));
        return cmsPricesEntity;
    }

    /**
     * 保存商品表-副表信息
     *
     * @param cmsGoodsEntity 商品表
     */
    private CmsGoodsInformationEntity cmsGoodsInformation(CmsGoodsEntity cmsGoodsEntity) {
        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setGoodid(cmsGoodsEntity.getGoodid());
        cmsGoodsInformationEntity.setEcnn(1);
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setContent("");
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setEcnnnumber(cmsGoodsEntity.getEccnNo());
        cmsGoodsInformationEntity.setTariffs(0);
        cmsGoodsInformationEntity.setTariffsrate(0);
        if (cmsGoodsEntity.getInspectionChargesFee() == null) {
            cmsGoodsInformationEntity.setInspectionCharges(1);
            cmsGoodsInformationEntity.setInspectionChargesFee(0.0);
        } else {
            cmsGoodsInformationEntity.setInspectionCharges(cmsGoodsEntity.getInspectionChargesFee() > 0 ? 1 : 0);
            cmsGoodsInformationEntity.setInspectionChargesFee(cmsGoodsEntity.getInspectionChargesFee());
        }
        cmsGoodsInformationEntity.setParams(cmsGoodsEntity.getParams().toString());
        cmsGoodsInformationEntity.setCreatedAt(new Date());
        return cmsGoodsInformationEntity;
    }
}
