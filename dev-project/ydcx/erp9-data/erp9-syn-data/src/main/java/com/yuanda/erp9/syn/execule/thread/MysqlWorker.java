package com.yuanda.erp9.syn.execule.thread;

import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.service.erp9.GoodsService;
import com.yuanda.erp9.syn.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName Worker
 * @Description 子任务工作者
 * @Date 2022/11/15
 * @Author myq
 */
@Slf4j
public class MysqlWorker<T> extends AbstractWorker<T> {

    // 数据集合
    private List<T> task;
    // 子任务执行情况结果集
    private ConcurrentHashMap<String, Object> resultMap;


    public MysqlWorker(List<T> task, ConcurrentHashMap<String, Object> resultMap) {
        // 初始化子任务
        this.task = task;
        this.resultMap = resultMap;
    }

    /**
     * @Description: 由父类线程池调用
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2815:24
     */
    @Override
    void task() {
        String workerThreadName = Thread.currentThread().getName();
        try {
            log.info("**************开始同步mysql数据**************");
            GoodsService goodsService = SpringContextUtil.getBean("goodsServiceImpl");
            goodsService.insertBatch((List<CmsGoodsPojo>) task);
            log.info("**************结束同步mysql数据**************");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(workerThreadName + ":{} ", e);
            resultMap.put("errMsg", "worker run error : {" + e + "}");
            resultMap.put("errThreadName", workerThreadName);
            resultMap.put("errCode", 500);
            throw new RuntimeException("Thread-Name" + workerThreadName + "worker run error :{}");
        } finally {
            task.clear();
        }
    }


}
