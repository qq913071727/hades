package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统主体更新日志记录表
 *
 * @author myq
 * @since 1.0.0 2022-11-30
 */
@TableName("cms_topic_update_log")
@Setter
@Getter
public class CmsTopicUpdateLogEntity implements Serializable {


    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "系统主题")
    private String systemTopic;

    @ApiModelProperty(value = "供应商id")
    private Integer supplier;

    @ApiModelProperty(value = "系统主体类型 1：邮件 2：exe下载")
    private Integer systemTopicType;

    @ApiModelProperty(value = "是否有更新 1：是 0：否")
    private Integer updated;

    @ApiModelProperty(value = "创建时间")
    private Date createAt;

    @ApiModelProperty(value = "更新时间")
    private String updateAt;
}