package com.yuanda.erp9.syn.entity.erp9_precrm;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 价格预警视图
 * @Date 2023/4/6 14:05
 **/
@ApiModel(value = "QuoteForStockMonitorTopView", description = "价格预警视图")
@TableName("QuoteForStockMonitorTopView")
@Data
public class QuoteForStockMonitorTopViewEntity {
    @ApiModelProperty(value = "型号")
    @TableField(value = "PartNumber")
    private String partNumber;

    @ApiModelProperty(value = "品牌")
    @TableField(value = "Brand")
    private String brand;

    @ApiModelProperty(value = "币种")
    @TableField(value = "Currency")
    private String currency;

    @ApiModelProperty(value = "价格")
    @TableField(value = "Price")
    private Float price;

    @ApiModelProperty(value = "有效时间")
    @TableField(value = "CreateDate")
    private Date createDate;
}
