package com.yuanda.erp9.syn.vo;

import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 汇率入参实体类
 * @Date 2022/12/15 17:18
 **/
@Data
public class RateVo  implements Serializable {
    private String operate;

    private String  type;

    private Integer district;

    private Integer from;

    private Integer to;

    private BigDecimal value;

    private String  startDate;

    private String modifyDate;

}
