package com.yuanda.erp9.syn.pojo;

import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsInformationEntity;
import com.yuanda.erp9.syn.entity.CmsPricesEntity;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.List;

/**
 * @ClassName CmsGoodsPojo
 * @Description 商品包装类
 * @Date 2022/11/18
 * @Author myq
 */
@Setter
@Getter
public class CmsGoodsPojo implements Serializable {
    /**
     * new CmsGoodsPojo 每次都会需要初始化创建新的对象，否则会出现空指针异常。
     */
    private CmsGoodsEntity cmsGoodsEntity;

    private CmsGoodsInformationEntity cmsGoodsInformationEntity;

    // 阶梯价格
    private List<CmsPricesEntity> cmsPricesEntityList;
}