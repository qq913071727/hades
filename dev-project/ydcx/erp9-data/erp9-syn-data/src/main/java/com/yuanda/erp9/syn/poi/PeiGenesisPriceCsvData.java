package com.yuanda.erp9.syn.poi;


import lombok.Data;

@Data
public class PeiGenesisPriceCsvData {

    private String manufacturer;

    private String manufacturerPartNumber;

    private String description;

    private String quantity1;

    private String cost1;

    private String quantity2;

    private String cost2;

    private String quantity3;

    private String cost3;

    private String quantity4;

    private String cost4;

    private String quantity5;

    private String cost5;

    private String currency;

    private String unitOfMeasure;

    private String minimumQuantity;

    private String leadTime;

    private String peiPCat6;

    private String peiProductcode;

    private String countryOfOrigin;

    private String commodityCode;

    private String sedFlag;

    private String roHS;

    private String exportControlType;

    private String exportControlClassification;



}
