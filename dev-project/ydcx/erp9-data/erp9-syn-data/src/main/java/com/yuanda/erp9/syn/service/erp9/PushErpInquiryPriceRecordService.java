package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.PushErpInquiryPriceRecordEntity;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 推送询价记录管理
 * @Date 2023/4/6 14:23
 **/
public interface PushErpInquiryPriceRecordService {
    /**
     * 根据型号查询有效推送
     *
     * @param pushErpInquiryPriceRecordEntity
     * @return
     */
    List<PushErpInquiryPriceRecordEntity> findByPartNumber(PushErpInquiryPriceRecordEntity pushErpInquiryPriceRecordEntity);

    /**
     * 批量插入推送记录
     *
     * @param insertPushErp
     */
    void batchInsert(List<PushErpInquiryPriceRecordEntity> insertPushErp);

    /**
     * 批量更新推送记录
     *
     * @param updatePushErp
     */
    void batchUpdate(List<PushErpInquiryPriceRecordEntity> updatePushErp);

    /**
     *根据状态查询出没有推送的记录 状态。1:待推送，2:已推送
     *
     * @return
     */
    List<PushErpInquiryPriceRecordEntity> findByStatus();

    /**
     * 根据id更新推送状态
     *
     * @param ids
     */
    void updateStatusAndInquiryNumberByIds(List<Integer> ids,String inquiryNumber);
}
