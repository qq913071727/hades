package com.yuanda.erp9.syn.schedule;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.config.FileParamConfig;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.Chip1StopCsvImportService;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc Chip1Stop 数据导入
 * @Date 2022/12/14 16:30
 **/
@Configuration
@EnableScheduling
public class ImportChip1StopDataTask {
    private static final Logger log = LoggerFactory.getLogger(ImportChip1StopDataTask.class);

    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Resource
    private Chip1StopCsvImportService chip1StopCsvImportService;

    @Resource
    private CmsSuppliersTempService cmsSuppliersTempService;

    // 供应商名称
    private static final String SUPPLIER = "Chip1Stop";

    // 供应商id
    private static final String SUPPLIER_ID = "US000000051";

    /**
     * 导入Chip1Stop_ARROWTECH数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importChip1StopARROWTECHDataJob")
    @EnableSchedule
    public void importChip1StopARROWTECHData() {
        try {
            log.warn("导入Chip1Stop_ARROWTECH数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.warn("1. 通过Chip1Stop_ARROWTECH下载地址获取文件");
            // 1. 通过下载地址获取文件
            //CHIP1STOP_ARROWTECH 下载文件url
            String fileArrowtechPath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.CHIP1STOP_ARROWTECH_DOWN_FILE_URL, fileParamConfig.getDownloadPath(), supplierId);
            if (StringUtils.isNotEmpty(fileArrowtechPath)) {
                log.warn("Chip1Stop_ARROWTECH数据文件下载完成,文件保存路径:[fileArrowtechPath:{}]", fileArrowtechPath);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(supplierId);
                entity.setSystemTopicType(SubjectSupplierEnum.CHIP_1_STOP.getType());
                entity.setSystemTopic(SubjectSupplierEnum.CHIP_1_STOP.getSubject());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                // 导入Chip1Stop文件数据
                log.warn("2. 解析Chip1Stop_ARROWTECH文件数据进行入库操作");
                chip1StopCsvImportService.analysisChip1Stop(topicUpdateLogId,fileArrowtechPath);
            } else {
                log.warn("Chip1Stop_ARROWTECH数据文件下载失败,文件路径为空,任务结束:[Chip1Stop_ARROWTECH:{}]", fileArrowtechPath);
            }
        } catch (Exception e) {
            log.warn("下载Chip1Stop_ARROWTECH导入错误:e", e);
            e.printStackTrace();
        }
    }

    /**
     * 导入Chip1Stop_ti数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importChip1StopTiDataJob")
    @EnableSchedule
    public void importChip1StopTiData() {
        try {
            log.warn("导入Chip1Stop_TIi数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.warn("1. 通过Chip1Stop_TI下载地址获取文件");
            //CHIP1STOP_TI 下载文件url
            String fileTiPath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.CHIP1STOP_TI_DOWN_FILE_URL, fileParamConfig.getDownloadPath(), supplierId);
            if (StringUtils.isNotEmpty(fileTiPath)) {
                log.warn("Chip1Stop_TI数据文件下载完成,文件保存路径:[fileTiPath:{}]", fileTiPath);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(supplierId);
                entity.setSystemTopicType(SubjectSupplierEnum.CHIP_1_STOP.getType());
                entity.setSystemTopic(SubjectSupplierEnum.CHIP_1_STOP.getSubject());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                // 导入Chip1Stop文件数据
                log.warn("2. 解析Chip1Stop_TI文件数据进行入库操作");
                chip1StopCsvImportService.analysisChip1Stop(topicUpdateLogId,fileTiPath);
            } else {
                log.warn("Chip1Stop_TI数据文件下载失败,文件路径为空,任务结束:[fileTiPath:{}]", fileTiPath);
            }
        } catch (Exception e) {
            log.warn("下载Chip1Stop_TI导入错误:e", e);
            e.printStackTrace();
        }
    }

    /**
     * 导入Chip1Stop_VER2数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importChip1StopVER2DataJob")
    @EnableSchedule
    public void importChip1StopVER2Data() {
        try {
            log.warn("导入Chip1Stop数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.warn("1. 通过Chip1Stop_VER2下载地址获取文件");
            // CHIP1STOP_VER2 下载文件url
            String fileVar2Path = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.CHIP1STOP_VER2_DOWN_FILE_URL, fileParamConfig.getDownloadPath(), supplierId);
            if (StringUtils.isNotEmpty(fileVar2Path)) {
                log.warn("Chip1Stop_VER2数据文件下载完成,文件保存路径:[fileVar2Path:{}]", fileVar2Path);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(supplierId);
                entity.setSystemTopicType(SubjectSupplierEnum.CHIP_1_STOP.getType());
                entity.setSystemTopic(SubjectSupplierEnum.CHIP_1_STOP.getSubject());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                // 导入Chip1Stop文件数据
                log.warn("2.解析Chip1Stop_VER2文件数据进行入库操作");
                chip1StopCsvImportService.analysisChip1Stop(topicUpdateLogId,fileVar2Path);
            } else {
                log.warn("Chip1Stop_VER2数据文件下载失败,文件路径为空,任务结束:[fileVar2Path:{}]", fileVar2Path);
            }
        } catch (Exception e) {
            log.warn("下载Chip1Stop_VER2导入错误:e", e);
            e.printStackTrace();
        }
    }
}
