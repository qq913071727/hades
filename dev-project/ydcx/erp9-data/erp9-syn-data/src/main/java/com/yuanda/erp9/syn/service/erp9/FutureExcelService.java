package com.yuanda.erp9.syn.service.erp9;

/**
 * @ClassName FutureExcelService
 * @Description
 * @Date 2022/12/6
 * @Author myq
 */
public interface FutureExcelService {

    public void parse(Long pk);
}
