package com.yuanda.erp9.syn.poi;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @ClassName CoreStaffStockExcelData
 * @Description excel对应的类
 * @Date 2022/11/24
 * @Author myq
 */
@Getter
@ToString
@Setter
public class CoreStaffStockExcelData implements Serializable {

    @ExcelProperty(index = 0)
    private String id;

    @ExcelProperty(index = 1)
    private String category;

    @ExcelProperty(index = 2)
    private String pn;

    @ExcelProperty(index = 3)
    private String mfg;

    @ExcelProperty(index = 4)
    private String qty;

    @ExcelProperty(index = 5)
    private String moq;

    @ExcelProperty(index = 6)
    private String dc;

    @ExcelProperty(index = 7)
    private String stockType;

    @ExcelProperty(index = 8)
    private String price1;

    @ExcelProperty(index = 9)
    private String price2;

    @ExcelProperty(index = 10)
    private String price3;

    @ExcelProperty(index = 11)
    private String price4;

    @ExcelProperty(index = 12)
    private String price5;

    @ExcelProperty(index = 13)
    private String price6;

    @ExcelProperty(index = 14)
    private String price7;

    @ExcelProperty(index = 15)
    private String price8;

    @ExcelProperty(index = 16)
    private String coo;


}
