//package com.yuanda.erp9.syn.util;
//
//import com.yuanda.erp9.syn.value.ImapMailProperty;
//import lombok.extern.slf4j.Slf4j;
//
//import javax.mail.Address;
//import javax.mail.BodyPart;
//import javax.mail.Flags;
//import javax.mail.Folder;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.Part;
//import javax.mail.Session;
//import javax.mail.Store;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//import javax.mail.internet.MimeUtility;
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.UnsupportedEncodingException;
//import java.security.Security;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Properties;
//
//
///**
// * 使用Imap协议接收邮件
// */
//@Slf4j
//public class ImapReceiveMailUtils {
//
//
//
//
//    //ssl加密
//    static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
//    // 定义连接imap服务器的属性信息
//    static String protocol = "imap";
//
//
//
//    /**
//     * 保存到指定路径
//     *
//     * @param savePath 解析完以后是否需要删除,不删除会重复解析
//     * @param isDelete
//     */
//    public static void saveMailFile(String savePath, boolean isDelete) {
//        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
//        //有些参数可能不需要
//        Properties props = new Properties();
//        props.setProperty("mail.imap.socketFactory.class", SSL_FACTORY);
//        props.setProperty("mail.imap.socketFactory.fallback", "false");
//        props.setProperty("mail.transport.protocol", protocol);
//        props.setProperty("mail.imap.port", ImapMailProperty.getMailProperty().getImapMailPort() + "");
//        props.setProperty("mail.imap.socketFactory.port", ImapMailProperty.getMailProperty().getImapMailPort() + "");
//        Folder folder = null;
//        Store store = null;
//        try {
//            //创建会话
//            Session session = Session.getInstance(props);
//            //存储对象
//            store = session.getStore("imap");
//            //连接
//            store.connect(ImapMailProperty.getMailProperty().getImapMailServer(), ImapMailProperty.getMailProperty().getImapMailPort(), ImapMailProperty.getMailProperty().getImapMailUserName(), ImapMailProperty.getMailProperty().getImapMailToken());
//            // 获得收件箱
//            folder = store.getFolder("INBOX");
//            // 以读写模式打开收件箱
//            folder.open(Folder.READ_WRITE);
//            Message[] messages = folder.getMessages();
//            parseMessage(savePath, isDelete, messages);
//        } catch (Exception e) {
//            log.error("解析mail异常 邮箱 {} ,登陆账号 {} ", ImapMailProperty.getMailProperty().getImapMailServer(), ImapMailProperty.getMailProperty().getImapMailUserName(), e);
//        } finally {
//            if (folder != null) {
//                try {
//                    folder.close(true);
//                } catch (MessagingException e) {
//                    log.error("folder关闭流异常", e);
//                }
//            }
//            if (store != null) {
//                try {
//                    store.close();
//                } catch (MessagingException e) {
//                    log.error("store关闭流异常", e);
//                }
//            }
//        }
//    }
//
//
//    /**
//     * 解析邮件
//     *
//     * @param messages 要解析的邮件列表
//     */
//    private static void parseMessage(String savePath, boolean isDelete, Message... messages) throws MessagingException, IOException {
//        if (messages == null || messages.length < 1) {
//            throw new MessagingException("未找到要解析的邮件!");
//        }
//        // 解析所有邮件
//        for (int i = 0, count = messages.length; i < count; i++) {
//            MimeMessage msg = (MimeMessage) messages[i];
//            log.info("------------------解析第 {} 封邮件-------------------- ", msg.getMessageNumber());
//            log.info("主题: " + getSubject(msg));
//            log.info("发件人: " + getFrom(msg));
//            log.info("发送时间：" + getSentDate(msg, null));
//            log.info("邮件大小：" + msg.getSize() * 1024 + "kb");
//            boolean isContainerAttachment = isContainAttachment(msg);
//            log.info("是否包含附件：" + isContainerAttachment);
//            if (isContainerAttachment) {
//                //保存附件路径
//                saveAttachment(msg, "C:\\Users\\B1B\\Desktop\\数据同步\\邮件\\", getSubject(msg)); //保存附件路径
//
//            }
//            StringBuffer content = new StringBuffer(30);
//            getMailTextContent(msg, content);
//            System.out.println("邮件正文：" + (content.length() > 100 ? content.substring(0, 100) + "..." : content));
//            log.info("------------------解析第 {} 封邮件结束-------------------- ", msg.getMessageNumber());
//            System.out.println();
////            if (isDelete) {
////                msg.setFlag(Flags.Flag.DELETED, true);
////                log.info("------------------解析第 {} 封邮件删除-------------------- ", msg.getMessageNumber());
////            }
//        }
//    }
//
//
//    /**
//     * 获得邮件主题
//     *
//     * @param msg 邮件内容
//     * @return 解码后的邮件主题
//     */
//    private static String getSubject(MimeMessage msg) throws UnsupportedEncodingException, MessagingException {
//        return MimeUtility.decodeText(msg.getSubject());
//    }
//
//    /**
//     * 获得邮件发件人
//     *
//     * @param msg 邮件内容
//     * @return 姓名 <Email地址>
//     * @throws MessagingException
//     * @throws UnsupportedEncodingException
//     */
//    private static String getFrom(MimeMessage msg) throws MessagingException, UnsupportedEncodingException {
//        String from = "";
//        Address[] froms = msg.getFrom();
//        if (froms.length < 1) {
//            throw new MessagingException("没有发件人!");
//        }
//        InternetAddress address = (InternetAddress) froms[0];
//        String person = address.getPersonal();
//        if (person != null) {
//            person = MimeUtility.decodeText(person) + " ";
//        } else {
//            person = "";
//        }
//        from = person + "<" + address.getAddress() + ">";
//
//        return from;
//    }
//
//
//    /**
//     * 获得邮件发送时间
//     *
//     * @param msg 邮件内容
//     * @return yyyy年mm月dd日 星期X HH:mm
//     * @throws MessagingException
//     */
//    private static String getSentDate(MimeMessage msg, String pattern) throws MessagingException {
//        Date receivedDate = msg.getSentDate();
//        if (receivedDate == null) {
//            return "";
//        }
//        if (pattern == null || "".equals(pattern)) {
//            pattern = "yyyy年MM月dd日 E HH:mm ";
//        }
//        return new SimpleDateFormat(pattern).format(receivedDate);
//    }
//
//    /**
//     * 判断邮件中是否包含附件
//     * <p>
//     * //* @param msg 邮件内容
//     *
//     * @return 邮件中存在附件返回true，不存在返回false
//     * @throws MessagingException
//     * @throws IOException
//     */
//    private static boolean isContainAttachment(Part part) throws MessagingException, IOException {
//        boolean flag = false;
//        if (part.isMimeType("multipart/*")) {
//            MimeMultipart multipart = (MimeMultipart) part.getContent();
//            int partCount = multipart.getCount();
//            for (int i = 0; i < partCount; i++) {
//                BodyPart bodyPart = multipart.getBodyPart(i);
//                String disp = bodyPart.getDisposition();
//                if (disp != null && (disp.equalsIgnoreCase(Part.ATTACHMENT) || disp.equalsIgnoreCase(Part.INLINE))) {
//                    flag = true;
//                } else if (bodyPart.isMimeType("multipart/*")) {
//                    flag = isContainAttachment(bodyPart);
//                } else {
//                    String contentType = bodyPart.getContentType();
//                    if (contentType.indexOf("application") != -1) {
//                        flag = true;
//                    }
//
//                    if (contentType.indexOf("name") != -1) {
//                        flag = true;
//                    }
//                }
//                if (flag) {
//                    break;
//                }
//            }
//        } else if (part.isMimeType("message/rfc822")) {
//            flag = isContainAttachment((Part) part.getContent());
//        }
//        return flag;
//    }
//
//
//    /**
//     * 获得邮件文本内容
//     *
//     * @param part    邮件体
//     * @param content 存储邮件文本内容的字符串
//     * @throws MessagingException
//     * @throws IOException
//     */
//    private static void getMailTextContent(Part part, StringBuffer content) throws MessagingException, IOException {
//        //如果是文本类型的附件，通过getContent方法可以取到文本内容，但这不是我们需要的结果，所以在这里要做判断
//        boolean isContainTextAttach = part.getContentType().indexOf("name") > 0;
//        if (part.isMimeType("text/*") && !isContainTextAttach) {
//            content.append(part.getContent().toString());
//        } else if (part.isMimeType("message/rfc822")) {
//            getMailTextContent((Part) part.getContent(), content);
//        } else if (part.isMimeType("multipart/*")) {
//            Multipart multipart = (Multipart) part.getContent();
//            int partCount = multipart.getCount();
//            for (int i = 0; i < partCount; i++) {
//                BodyPart bodyPart = multipart.getBodyPart(i);
//                getMailTextContent(bodyPart, content);
//            }
//        }
//    }
//
//    /**
//     * 保存附件
//     *
//     * @param part    邮件中多个组合体中的其中一个组合体
//     * @param destDir 附件保存目录
//     * @throws UnsupportedEncodingException
//     * @throws MessagingException
//     * @throws FileNotFoundException
//     * @throws IOException
//     */
//    private static void saveAttachment(Part part, String destDir,String subject) throws UnsupportedEncodingException, MessagingException,
//            FileNotFoundException, IOException {
//        if (part.isMimeType("multipart/*")) {
//            Multipart multipart = (Multipart) part.getContent();    //复杂体邮件
//            //复杂体邮件包含多个邮件体
//            int partCount = multipart.getCount();
//            for (int i = 0; i < partCount; i++) {
//                //获得复杂体邮件中其中一个邮件体
//                BodyPart bodyPart = multipart.getBodyPart(i);
//                //某一个邮件体也有可能是由多个邮件体组成的复杂体
//                String disp = bodyPart.getDisposition();
//                if (disp != null && (disp.equalsIgnoreCase(Part.ATTACHMENT) || disp.equalsIgnoreCase(Part.INLINE))) {
//                    InputStream is = bodyPart.getInputStream();
//                    saveFile(is, destDir, decodeText(bodyPart.getFileName()),subject);
//                } else if (bodyPart.isMimeType("multipart/*")) {
//                    saveAttachment(bodyPart, destDir,subject);
//                } else {
//                    String contentType = bodyPart.getContentType();
//                    if (contentType.indexOf("name") != -1 || contentType.indexOf("application") != -1) {
//                        saveFile(bodyPart.getInputStream(), destDir, decodeText(bodyPart.getFileName()),subject);
//                    }
//                }
//            }
//        } else if (part.isMimeType("message/rfc822")) {
//            saveAttachment((Part) part.getContent(), destDir,subject);
//        }
//    }
//
//    /**
//     * 读取输入流中的数据保存至指定目录
//     *
//     * @param is       输入流
//     * @param fileName 文件名
//     * @param destDir  文件存储目录
//     * @throws FileNotFoundException
//     * @throws IOException
//     */
///*    private static void saveFile1(InputStream is, String destDir, String fileName) throws IOException {
//        BufferedInputStream bis = null;
//        BufferedOutputStream bos = null;
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(new File(destDir + fileName));
//            bis = new BufferedInputStream(is);
//            bos = new BufferedOutputStream(fos);
//            int len = -1;
//            while ((len = bis.read()) != -1) {
//                bos.write(len);
//                bos.flush();
//            }
//        } catch (Exception e) {
//            log.error("保存邮件附件发生异常 : ", e);
//            throw e;
//        } finally {
//            if (bos != null) {
//                bos.close();
//            }
//            if (bis != null) {
//                bis.close();
//            }
//            if (fos != null) {
//                fos.close();
//            }
//            if (is != null) {
//                is.close();
//            }
//
//        }
//    }*/
//
//    /**
//     * 读取输入流中的数据保存至指定目录
//     *
//     * @param is       输入流
//     * @param fileName 文件名
//     * @param destDir  文件存储目录
//     * @throws FileNotFoundException
//     * @throws IOException
//     */
//    private static void saveFile(InputStream is, String destDir, String fileName,String subject) throws IOException {
//        String filePath = destDir + fileName;
//        try (
//                FileOutputStream fos = new FileOutputStream(new File(filePath));
//                BufferedInputStream bis = new BufferedInputStream(is);
//                BufferedOutputStream bos = new BufferedOutputStream(fos)) {
//            int len = -1;
//            while ((len = bis.read()) != -1) {
//                bos.write(len);
//                bos.flush();
//            }
//        } catch (Exception e) {
//            log.error("保存邮件附件发生异常 : ", e);
//            throw e;
//        }
//        //保存文件成功,开始解析
//
//    }
//
//    /**
//     * 文本解码
//     *
//     * @param encodeText 解码MimeUtility.encodeText(String text)方法编码后的文本
//     * @return 解码后的文本
//     * @throws UnsupportedEncodingException
//     */
//    private static String decodeText(String encodeText) throws UnsupportedEncodingException {
//        if (encodeText == null || "".equals(encodeText)) {
//            return "";
//        } else {
//            return MimeUtility.decodeText(encodeText);
//        }
//    }
//
//
//}
//
//
//
