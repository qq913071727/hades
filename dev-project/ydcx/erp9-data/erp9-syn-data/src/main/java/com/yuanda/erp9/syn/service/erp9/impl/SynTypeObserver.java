package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.service.erp9.Observer;

/**
 * @ClassName SynTypeObserver
 * @Description 同步状态-=观察者
 * @Date 2022/11/15
 * @Author myq
 */
public class SynTypeObserver implements Observer {


    /**
     * @Description: 观察者模式
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1511:14
     */
    @Override
    public void update(AbstractSubject subject) {
        if (AbstractSubject.Syn.CONTAIN == subject.getSyn()) {
            // todo 解析并同步数据
        }
    }


}
