package com.yuanda.erp9.syn.service.erp9;

/**
 * @author zengqinglong
 * @desc Element14 数据解析
 * @Date 2022/12/5 20:20
 **/
public interface Element14TxtImportService {
    /**
     * 分析Element14数据包
     *
     * @param topicUpdateLogId
     * @param filePath
     */
    void analysisElement14(Long topicUpdateLogId, String filePath);
}
