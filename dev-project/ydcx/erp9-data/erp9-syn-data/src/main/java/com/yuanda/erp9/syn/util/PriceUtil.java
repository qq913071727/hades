package com.yuanda.erp9.syn.util;

import org.elasticsearch.common.Strings;
import java.math.BigDecimal;

/**
 * @desc   价格处理工具类
 * @author linuo
 * @time   2023年3月1日13:58:48
 */
public class PriceUtil {
    /**
     * 处理阶梯价格中的单价，小数位小于4位的用0补齐，超过4位的保留原始数据
     * @param price 价格
     * @return 返回处理后的价格
     */
    public static BigDecimal handlePrice(String price){
        if(Strings.isNullOrEmpty(price)){
            return BigDecimal.ZERO.setScale(4, BigDecimal.ROUND_UP);
        }

        String[] split = price.split("\\.");
        if(split.length == 2){
            String s = split[1];
            if(s.length() > 4){
                return BigDecimal.valueOf(Double.parseDouble(price));
            }
        }

        return BigDecimal.valueOf(Double.parseDouble(price)).setScale(4, BigDecimal.ROUND_UP);
    }
}