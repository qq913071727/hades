package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

@TableName("StandardPartnumbersForPlugin")
@Data
public class StandardPartnumbersForPluginEntity implements Serializable {
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    @TableField(value = "ID")
    private String id;

    @ApiModelProperty(value = "型号")
    @TableField(value = "PartNumber")
    private String partNumber;

    @ApiModelProperty(value = "品牌")
    @TableField(value = "Manufacturer")
    private String manufacturer;

    @ApiModelProperty(value = "VATRate")
    @TableField(value = "VATRate")
    private Float vatRate;

    @ApiModelProperty(value = "TariffRate")
    @TableField(value = "TariffRate")
    private Float tariffRate;

    @ApiModelProperty(value = "ExciseTaxRate")
    @TableField(value = "ExciseTaxRate")
    private Float exciseTaxRate;

    @ApiModelProperty(value = "CIQprice")
    @TableField(value = "CIQprice")
    private Float ciqPrice;

    @ApiModelProperty(value = "AddedTariffRate")
    @TableField(value = "AddedTariffRate")
    private Float addedTariffRate;
}	