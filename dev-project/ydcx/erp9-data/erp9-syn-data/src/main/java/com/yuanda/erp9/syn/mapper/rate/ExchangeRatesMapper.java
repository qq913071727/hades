package com.yuanda.erp9.syn.mapper.rate;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.ExchangeRatesEntity;
import org.apache.ibatis.annotations.Mapper;
import java.math.BigDecimal;

@Mapper
public interface ExchangeRatesMapper extends BaseMapper<ExchangeRatesEntity> {
    /**
     * 获取有效数据
     * @return 返回查询结果
     */
    BigDecimal getEffectiveData();
}