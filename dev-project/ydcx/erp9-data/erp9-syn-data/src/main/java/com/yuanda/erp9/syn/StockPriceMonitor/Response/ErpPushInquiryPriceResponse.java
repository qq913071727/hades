package com.yuanda.erp9.syn.StockPriceMonitor.Response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zengqinglong
 * @desc 推送库存价格询价记录
 * @Date 2023/4/10 14:10
 **/
@Data
public class ErpPushInquiryPriceResponse implements Serializable {
    private Integer status;
    private boolean success;
    private String msg;
    private String data;
}
