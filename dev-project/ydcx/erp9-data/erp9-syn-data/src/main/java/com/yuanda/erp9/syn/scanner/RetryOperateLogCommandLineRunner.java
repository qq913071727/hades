package com.yuanda.erp9.syn.scanner;

import com.yuanda.erp9.syn.execule.thread.RetryOperateLogThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @ClassName RetryOperateLogCommandLineRunner
 * @Description
 * @Date 2022/12/22
 * @Author myq
 */
@Component
public class RetryOperateLogCommandLineRunner implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
        RetryOperateLogThread.getInstance().start();
    }
}
