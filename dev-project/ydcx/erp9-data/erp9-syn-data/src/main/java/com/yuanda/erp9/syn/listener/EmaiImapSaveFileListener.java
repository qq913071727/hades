package com.yuanda.erp9.syn.listener;

import com.yuanda.erp9.syn.event.EmaiImapSaveFileEvent;
import com.yuanda.erp9.syn.service.erp9.AnalysisMailExcelService;
import com.yuanda.erp9.syn.service.erp9.AventAbacusExcelService;
import com.yuanda.erp9.syn.service.erp9.Chip1AdimaxService;
import com.yuanda.erp9.syn.service.erp9.CoreStaffStockExcelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;

@Component
@Slf4j
public class EmaiImapSaveFileListener {

    @Autowired
    private AnalysisMailExcelService analysisMailExcelService;

    @Autowired
    private CoreStaffStockExcelService coreStaffStockExcelService;

    @Autowired
    private AventAbacusExcelService aventAbacusExcelService;

    @Autowired
    private Chip1AdimaxService chip1AdimaxService;


    @EventListener
    @Async("parseMessageThreadPool")
    public void ListenerOne(EmaiImapSaveFileEvent emaiImapSaveFileEvent) {
        Object source = emaiImapSaveFileEvent.getSource();
        if (source == null) {
            return;
        }
        HashMap<String, String> param = (HashMap<String, String>) source;
        String filePath = param.get("filePath");
        String subject = param.get("subject");
        Long pk = Long.parseLong(param.get("topicUpdateLogId"));
        log.info("EmaiImapSaveFileListener  filePath  开始解析附件  [ {}  ]  ,subject [  {} ]  ", filePath, subject);
        //根据不同的类型,走不通的service
        switch (subject) {
            case "AventAsiaHot":
                analysisMailExcelService.analysisAventAsiaHot(param);
                break;
            case "AventEbvHot":
                analysisMailExcelService.analysisAventEbvHot(param);
                break;
            case "AventSilicaHot":
                analysisMailExcelService.analysisSilica(param);
                break;
            case "AventAbacusHot":
                aventAbacusExcelService.parse(filePath,pk);
                break;
            case "Chip1ADI":
                chip1AdimaxService.parse(filePath,pk);
                break;
            default:
                log.warn("类型错误找不到具体方法,走特殊方法处理");
                break;
        }
        if (subject.contains("Corestaff Stock List") || subject.contains("CorestaffStockList")) {
            coreStaffStockExcelService.parse(filePath,pk);
        }
        log.info("EmaiImapSaveFileListener  filePath  解析附件完成   [ {}  ]  ,subject [  {}  ] ", filePath, subject);
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }

}
