package com.yuanda.erp9.syn.util;

import java.math.BigDecimal;

/**
 * @ClassName MathUtil
 * @Description 科学计算工具类
 * @Date 2022/12/20
 * @Author myq
 */
public class MathUtil {
    /**
     * @Description: 返回 n/y 保留4位小数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2018:03
     */
    public static String divide(Object n, Object y) {
        return new BigDecimal(n.toString()).divide(new BigDecimal(y.toString()), 4, BigDecimal.ROUND_UP).toString();
    }

    /**
     * @Description: 返回 n/y 保留4位小数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2018:03
     */
    public static String divide(Object n, Object y, int scale) {
        return new BigDecimal(n.toString()).divide(new BigDecimal(y.toString()), scale, BigDecimal.ROUND_UP).toString();
    }

    /**
     * @Description: 返回 n/y 保留4位小数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2018:03
     */
    public static String multiply(Object n, Object y) {
        return new BigDecimal(n.toString()).multiply(new BigDecimal(y.toString())).setScale(4, BigDecimal.ROUND_UP).toString();
    }

    /**
     * @Description: 返回 n*y
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2018:03
     */
    public static String multiply(Object n, Object y, int scale) {
        return new BigDecimal(n.toString()).multiply(new BigDecimal(y.toString())).setScale(scale, BigDecimal.ROUND_UP).toString();
    }
}