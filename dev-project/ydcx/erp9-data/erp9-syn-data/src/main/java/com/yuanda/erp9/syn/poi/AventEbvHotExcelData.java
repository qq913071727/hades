package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

/**
 *  AventEbvHot数据解析
 * @author liangjun
 */
@Data
public class AventEbvHotExcelData {

    @ExcelProperty(index = 0)
    private String article;

    @ExcelProperty(index = 1)
    private String manuf;

    @ExcelProperty(index = 2)
    private String stock;

    @ExcelProperty(index = 3)
    @NumberFormat(value = "0.00000000000000000000")
    private String price;

    @ExcelProperty(index = 4)
    private String mpq;

    @ExcelProperty(index = 5)
    private String lt;

    @ExcelProperty(index = 6)
    private String articleMan;

    @ExcelProperty(index = 7)
    private String moq;

    @ExcelProperty(index = 8)
    private String packType;

    @ExcelProperty(index = 9)
    private String exportCode;

    @ExcelProperty(index = 10)
    private String roHs;


}
