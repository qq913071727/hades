package com.yuanda.erp9.syn.mapper.erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.CmsSuppliersTempEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 供应商临时表
 * @author myq
 * @since 1.0.0 2022-11-24
 */
@Mapper
public interface CmsSuppliersTempMapper extends BaseMapper<CmsSuppliersTempEntity> {

    /**
     * 根据名称获取id
     * @param name 供应商名称
     * @return 返回查询结果
     */
    Integer selectIdByName(@Param("name") String name);
}
