package com.yuanda.erp9.syn.util;

import com.yuanda.erp9.syn.entity.CmsPricesEntity;
import java.math.BigDecimal;
import java.util.List;

/**
 * @desc   最小起订量处理工具类
 * @author linuo
 * @time   2023年3月1日13:58:48
 */
public class MoqUtil {
    /**
     * 根据最小起订量判断属于哪个阶梯获取对应阶梯的价格，再根据最小起订价格计算最小起订量
     * @param minimumOrder 原始数据中的最小起订量
     * @param incremental 增量
     * @param price 最小起订价格
     * @param list 阶梯价格配置
     * @return 返回计算结果
     */
    public static Long getMoq(Long minimumOrder, Long incremental, BigDecimal price, List<CmsPricesEntity> list) {
        Long result = null;

        // 根据原始数据中的最小起订量获取的符合阶梯价格的金额
        BigDecimal firstPrice = getPriceByMoqAndLadderPrice(minimumOrder, list);

        // 根据原始数据中的最小起订量算出来的金额
        BigDecimal multiply = firstPrice.multiply(BigDecimal.valueOf(minimumOrder));

        // 判断根据原始数据中的最小起订量算出来的金额是否小于最小起订价格
        if(multiply.compareTo(price) < 0){
            for (int i = 0; i < list.size(); i++) {
                CmsPricesEntity cmsPricesEntity = list.get(i);

                // 阶梯价格配置中小于0或等于0的过滤掉
                if(cmsPricesEntity.getPrice().compareTo(BigDecimal.ZERO) <= 0){
                    continue;
                }

                // 起订量 = 增量 * (最小起订价格 / 阶梯价格配置中的金额 / 增量)
                BigDecimal divide = price.divide(cmsPricesEntity.getPrice(), 0, BigDecimal.ROUND_UP);

                /*
                BigDecimal divide = price.divide(cmsPricesEntity.getPrice(), 20, BigDecimal.ROUND_UP);
                BigDecimal divide1 = divide.divide(BigDecimal.valueOf(incremental), 0, BigDecimal.ROUND_UP);
                long multiply1 = BigDecimal.valueOf(incremental).multiply(divide1).longValue();
                */

                long multiply1 = divide.longValue();

                // 起订金额 = 起订量 * 起订量符合的阶梯价格的金额
                BigDecimal priceByMoqAndLadderPrice = getPriceByMoqAndLadderPrice(multiply1, list);
                BigDecimal multiply2 = priceByMoqAndLadderPrice.multiply(BigDecimal.valueOf(multiply1));

                if(multiply2.compareTo(price) >= 0){
                    result = multiply1;
                    break;
                }
            }
        }else{
            result = minimumOrder;
        }
        return result;
    }

    /**
     * 根据最小起订量获取阶梯价格
     * @param moq 最小起订量
     * @param list 阶梯价格配置
     * @return 返回符合判断的阶梯价格
     */
    private static BigDecimal getPriceByMoqAndLadderPrice(Long moq, List<CmsPricesEntity> list){
        BigDecimal price = BigDecimal.ZERO;
        if(list != null && list.size() > 0){
            for(CmsPricesEntity cmsPricesEntity : list){
                Long min = cmsPricesEntity.getMin();
                Long max = cmsPricesEntity.getMax();
                if(max == 0 && moq >= min){
                    price = cmsPricesEntity.getPrice();
                    break;
                }else{
                    if(moq >= min && moq < max){
                        price = cmsPricesEntity.getPrice();
                        break;
                    }
                }
            }
        }
        return price;
    }
}