package com.yuanda.erp9.syn.service.erp9;

/**
 * @ClassName FutureExcelService
 * @Description future供应商接口
 * @Date 2022/12/1
 * @Author myq
 */
public interface AventAbacusExcelService {

    /**
     * @Description: 解析excel
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2415:55
     */
    void parse(String filePath, Long pk);
}
