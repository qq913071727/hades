package com.yuanda.erp9.syn.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "thread.pool.parsemessage")
public class ParseMessageThreadProperty {

    private int corePoolSize;

    private int maxPoolSize;

    private int keepAliveTime;

    private int queueCapacity;

    private int awaitTerminationSeconds;

    private String threadNamePrefix;

    /** 队列，当线程数目超过核心线程数时用于保存任务的队列 */
    private Integer workQueue;
}