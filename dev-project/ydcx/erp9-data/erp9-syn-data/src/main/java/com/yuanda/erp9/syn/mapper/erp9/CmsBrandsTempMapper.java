package com.yuanda.erp9.syn.mapper.erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 *
 * @author myq 
 * @since 1.0.0 2022-11-24
 */
@Mapper
public interface CmsBrandsTempMapper extends BaseMapper<CmsBrandsTempEntity> {


    List<CmsBrandsTempEntity> select();
}
