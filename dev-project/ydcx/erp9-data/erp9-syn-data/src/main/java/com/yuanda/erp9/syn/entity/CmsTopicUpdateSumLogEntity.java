package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @desc   系统主体更新数量日志记录表实体类
 * @author linuo
 * @time   2023年4月25日11:42:04
 */
@Data
@TableName("cms_topic_update_sum_log")
public class CmsTopicUpdateSumLogEntity implements Serializable {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "主题更新日志表主键", dataType = "Integer")
    @TableField(value = "topic_update_log_id")
    private Integer topicUpdateLogId;

    @ApiModelProperty(value = "更新数量", dataType = "Integer")
    @TableField(value = "update_sum")
    private Integer updateSum;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}