package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * @author myq
 * @since 1.0.0 2022-11-24
 */
@TableName("cms_brands_temp")
@Setter
@Getter
public class CmsBrandsTempEntity implements Serializable {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "品牌id")
    private Integer brandid;

    @ApiModelProperty(value = "品牌名称")
    private String name;

    @ApiModelProperty(value = "0-已有 1-新建")
    private Integer tag;
}