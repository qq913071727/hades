package com.yuanda.erp9.syn.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc   型号费率数据
 * @author linuo
 * @time   2023年1月6日10:50:26
 */
@Data
public class StandardPartnumbersForPluginDataPojo {
    @ApiModelProperty(value = "型号")
    private String partNumber;

    @ApiModelProperty(value = "品牌")
    private String manufacturer;

    @ApiModelProperty(value = "VATRate")
    private Float vatRate;

    @ApiModelProperty(value = "TariffRate")
    private Float tariffRate;

    @ApiModelProperty(value = "ExciseTaxRate")
    private Float exciseTaxRate;

    @ApiModelProperty(value = "CIQprice")
    private Float ciqPrice;

    @ApiModelProperty(value = "AddedTariffRate")
    private Float addedTariffRate;

    @ApiModelProperty(value = "是否3c认证(1是、0否)")
    private Integer ccc;

    @ApiModelProperty(value = "是否禁运(1是、0否)")
    private Integer embargo;

    @ApiModelProperty(value = "Eccn")
    private String eccn;
}