package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsOperateErrorLogEntity;

import java.util.List;

/**
 * @ClassName CmsOperateErrorLogServer
 * @Description
 * @Date 2022/12/22
 * @Author myq
 */
public interface CmsOperateErrorLogService {

    /**
     * @Description: 添加错误日志
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2214:48
     */
    void insertBatch(List<CmsOperateErrorLogEntity> logEntities);
}
