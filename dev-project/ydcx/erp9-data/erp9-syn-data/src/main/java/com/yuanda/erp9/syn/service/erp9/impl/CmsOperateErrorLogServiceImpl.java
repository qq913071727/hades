package com.yuanda.erp9.syn.service.erp9.impl;


import com.yuanda.erp9.syn.entity.CmsOperateErrorLogEntity;
import com.yuanda.erp9.syn.mapper.erp9.CmsOperateErrorLogMapper;
import com.yuanda.erp9.syn.service.erp9.CmsOperateErrorLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName CmsOperateErrorLogServiceImpl
 * @Description
 * @Date 2022/12/22
 * @Author myq
 */
@Service
@Slf4j
public class CmsOperateErrorLogServiceImpl implements CmsOperateErrorLogService {

    @Autowired
    private CmsOperateErrorLogMapper cmsOperateErrorLogMapper;


    @Override
    public void insertBatch(List<CmsOperateErrorLogEntity> logEntities) {
        if(!CollectionUtils.isEmpty(logEntities)){
            cmsOperateErrorLogMapper.insertBatch(logEntities);
            logEntities.clear();
        }
    }
}
