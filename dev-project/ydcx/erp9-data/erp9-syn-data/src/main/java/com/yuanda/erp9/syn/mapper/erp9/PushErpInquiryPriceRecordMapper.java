package com.yuanda.erp9.syn.mapper.erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.PushErpInquiryPriceRecordEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 推送erp询价记录
 * @Date 2023/4/4 16:45
 **/
@Mapper
public interface PushErpInquiryPriceRecordMapper extends BaseMapper<PushErpInquiryPriceRecordEntity> {
    /**
     * 批量插入推送记录
     *
     * @param list
     */
    void batchInsert(@Param("list") List<PushErpInquiryPriceRecordEntity> list);

    /**
     * 批量更新推送记录
     *
     * @param ids
     * @param inquiryNumber
     */
    void updateStatusAndInquiryNumberByIds(@Param("ids")List<Integer> ids,@Param("inquiryNumber") String inquiryNumber);
}
