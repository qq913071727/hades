package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.service.erp9.Observer;

/**
 * @ClassName SynStatus
 * @Description 同步状态
 * @Date 2022/11/14
 * @Author myq
 */
public class SynTypeSubject extends AbstractSubject{

    /**
     * 通知所有观察者
     *
     * @param synStatus
     */
    public void setSynStatus(Syn synStatus) {
        // 初始化父类状态
        super.setSyn(synStatus);
        if (synStatus != Syn.NONE) {
            // 开始通知
           this.notifyAllObservers();
        }
    }


    @Override
    public void registerObserver(Observer obs) {
        list.add(obs);
    }

    @Override
    public void removeObserver(Observer obs) {
        list.remove(obs);
    }

    @Override
    public void notifyAllObservers() {
        for (Observer observerList : list) {
            observerList.update(this);

        }
    }


}
