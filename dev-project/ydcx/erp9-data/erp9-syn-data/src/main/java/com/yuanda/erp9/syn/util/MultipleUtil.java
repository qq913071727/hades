package com.yuanda.erp9.syn.util;

/**
 * @Author myq
 * @Date 2023/3/22 14:27
 * @Version 1.0
 * @Description
 */
public class MultipleUtil {


    /***
     * @param minQty 原始数据中最小起订量 （excel中）
     * @param moq  根据最小起订金额计算出的最小启动量 (MoqUtil返回的)
     * @return Long  返回最终的最小起订量（倍数要求）
     * @Description:
     * @Author: myq
     * @Date: 2023/3/2214:37
     */
    public static Long getMinimumByMultiple(Long minQty, Long moq) {
        if(moq == null)
            return minQty;
        long res = 0;
        do {
            if (moq % minQty == 0) {
                res = moq;
                break;
            }
            moq++;
        } while (true);
        return res;
    }

}
