package com.yuanda.erp9.syn.service.erp9;

/**
 * @author zengqinglong
 * @desc 解析数据包excel文件
 * @Date 2022/11/26 10:25
 **/
public interface AvnetExcelService {
    /**
     * 分析avnet数据包
     *
     * @param topicUpdateLogId
     * @param filePath
     */
    void analysisAvnet(Long topicUpdateLogId, String filePath);
}
