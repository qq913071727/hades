package com.yuanda.erp9.syn.enums;

/**
 * @author zengqinglong
 * @desc erp 币种
 * @Date 2023/4/6 16:51
 **/
public enum  ErpCurrencyEnum {
    RMB("1","人民币"),
    CNY("CNY","人民币"),
    DOLLAR("2","美元"),
    USD("USD","美元");

    private String code;
    private String desc;

    ErpCurrencyEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }


}
