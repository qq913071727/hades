package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

/**
 * AventAsiaHot数据解析
 * @author liangjun
 */
@Data
public class AventAsiaHotExcelData {

    @ExcelProperty(index = 0)
    private String mfrName;

    @ExcelProperty(index = 1)
    private String mfrPn;

    @ExcelProperty(index = 2)
    private String avnetPn;

    @ExcelProperty(index = 3)
    private String grBatchNo;

    @ExcelProperty(index = 4)
    private String availableQty;

    @ExcelProperty(index = 5)
    private String stockLocation;

    @ExcelProperty(index = 6)
    private String moq;

    @ExcelProperty(index = 7)
    private String mpq;

    @ExcelProperty(index = 8)
    private String productDateCode;

    @ExcelProperty(index = 9)
    private String factoryLeadTimeDays;

    @ExcelProperty(index = 10)
    @NumberFormat(value = "0.00000000000000000000")
    private String indicativeUnitPriceUsd;

    @ExcelProperty(index = 11)
    private String promotionalPrice;

}
