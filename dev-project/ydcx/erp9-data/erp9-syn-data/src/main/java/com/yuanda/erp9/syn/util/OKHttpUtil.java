package com.yuanda.erp9.syn.util;


import okhttp3.*;
import org.apache.commons.collections.MapUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName OKHttpUtil
 * @Description
 * @Date 2022/12/15
 * @Author myq
 */
public class OKHttpUtil {


    public static OkHttpClient client = null;

    static {
        client = new OkHttpClient.Builder()
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .connectTimeout(50L, TimeUnit.SECONDS)
                .readTimeout(60L, TimeUnit.SECONDS)
                .writeTimeout(120L, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }

    private OKHttpUtil() {
    }


    /**
     * @Description: get
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/611:11
     */
    public static String doGet(String host, String path, Map<String, String> headers, Map<String, String> querys) throws Exception {
        StringBuffer url = new StringBuffer(host + (path == null ? "" : path));
        if (querys != null) {
            url.append("?");
            Iterator iterator = querys.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> e = (Map.Entry) iterator.next();
                url.append((String) e.getKey()).append("=").append((String) e.getValue() + "&");
            }
            url = new StringBuffer(url.substring(0, url.length() - 1));
        }
        Request.Builder requestBuilder = new Request.Builder();
        if (headers != null && headers.size() > 0) {
            Iterator iterator = headers.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                requestBuilder.addHeader(key, (String) headers.get(key));
            }
        }
        Request request = (requestBuilder).url(url.toString()).build();
        Response response = client.newCall(request).execute();
        String responseStr = response.body() == null ? "" : response.body().string();
        return responseStr;
    }


    /**
     * @Description: post
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/611:11
     */
    public static String doPost(String host, String path, Map<String, String> headers, Map<String, String> queryParam, Map<String, Object> formParam, String jsonParam) throws Exception {
        StringBuffer url = new StringBuffer(host + (path == null ? "" : path));
        if (queryParam != null) {
            url.append("?");
            Iterator iterator = queryParam.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> e = (Map.Entry) iterator.next();
                url.append((String) e.getKey()).append("=").append((String) e.getValue() + "&");
            }
            url = new StringBuffer(url.substring(0, url.length() - 1));
        }
        Request.Builder requestBuilder = new Request.Builder();
        if (headers != null && headers.size() > 0) {
            Iterator iterator = headers.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                requestBuilder.addHeader(key, (String) headers.get(key));
            }
        }
        requestBuilder.url(url.toString());
        /**
         * from表单参数
         */
        Request request = null;
        if (MapUtils.isNotEmpty(formParam)) {
            FormBody.Builder body = new FormBody.Builder();
            formParam.forEach((k, v) -> {
                body.add(k, String.valueOf(v));
            });
            FormBody formBody = body.build();
            request = requestBuilder.post(formBody).build();
        }

        if (null != jsonParam) {
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, jsonParam);
            request = requestBuilder.post(body).build();
        }

        Response response = client.newCall(request).execute();
        String responseStr = response.body() == null ? "" : response.body().string();
        return responseStr;
    }

}
