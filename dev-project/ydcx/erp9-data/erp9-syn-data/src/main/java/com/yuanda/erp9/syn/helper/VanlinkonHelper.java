package com.yuanda.erp9.syn.helper;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yuanda.erp9.syn.exception.TargetServerException;
import com.yuanda.erp9.syn.util.MathUtil;
import com.yuanda.erp9.syn.util.OKHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @ClassName VanlinkonHelper
 * @Description
 * @Date 2022/12/15
 * @Author myq
 */
@Slf4j
@Component
public class VanlinkonHelper {


    /**
     * @Description: 获取数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1515:38
     */
    public JSONObject getData(int page) {
        String s = null;
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("page", String.valueOf(page));
        queryMap.put("auth_id", "ydcx");
        queryMap.put("secret", "155808dda9a3721cb943df35e1f7fc6a");
        Map<String, String> header = new LinkedHashMap<>();
        header.put("UserAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");
        header.put("Referer", "Https://www.vanlinkon.com");
        try {
            System.setProperty("https.protocols", "TLSv1.2");
            s = OKHttpUtil.doGet("Https://api.vanlinkon.com/", "api/inventories2", header, queryMap);
        } catch (Exception e) {
            throw new TargetServerException("okHttp发送get请求失败：" + e);
        }
        return JSONObject.parseObject(s);
    }


    /**
     * @Description: 根据ID获取对应的库房信息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1516:12
     */
    public String getStore(String storeid) {
        switch (storeid) {
            case "001":
                return "上海";
            case "002":
                return "深圳";
            case "003":
                return "连可连代销I";
            case "004 ":
                return "连可连代销II";
            case "005":
                return "连可连代销III";
            case "006":
                return "连可连代销IV";
            case "007":
                return "连可连代销V ";
            default:
                return "";
        }
    }


    /**
     * @Description: 阶梯价格
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1516:55
     */
    public List<Map<String, String>> getLadderPrice(Integer isLadder, BigDecimal price, Integer isVipDiscount, String storeId, JSONArray ladderInfo) {
        int size = ladderInfo.size();
        int last = size - 1;
        int index = 1;
        List<Map<String, String>> ladderPriceList = new ArrayList<>(size);
        double pricediscount = 1.00;
        try {
            //如果is_vip_discount=0或者storeid=008或者storeid=042，倍数则为1.05倍
            if (Integer.valueOf(0).equals(isVipDiscount) || "008".equals(storeId) || "042".equals(storeId)) {
                pricediscount = 1.05;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //is_ladder==1处理价格阶梯
        if (Integer.valueOf(1).equals(isLadder)) {
            //取值这个字段进行循环
            for (int x = 0; x < size; x++) {
                Map<String, String> priceMap = new HashMap<>();
                JSONObject info = JSONObject.parseObject(ladderInfo.getString(x));
                String n = info.getString("min_number");
                String p = info.getString("min_price");
                //min_number和min_price不为空的情况下进行处理
                if (!"".equals(n) && !"".equals(p)) {
                    //count取值min_number
                    priceMap.put("min", n);
                    //price取值 min_price,乘以倍数
                    priceMap.put("price", MathUtil.multiply(pricediscount, Double.parseDouble(p)));
                    //币种人民币
                    priceMap.put("currency", "1");
                    if (x == last) {
                        priceMap.put("max", "0");
                    } else {
                        priceMap.put("max", JSONObject.parseObject(ladderInfo.getString(x + 1)).getString("min_number"));
                    }
                    priceMap.put("position", "" + index++);
                    priceMap.put("discount_rate", "0.0000");
                    priceMap.put("discount_price", "0.0000");
                    ladderPriceList.add(priceMap);
                }

            }
        }
        //is_ladder != 1处理价格阶梯
        else {
            Map<String, String> priceMap = new HashMap<>();
            //count取值min_number
            priceMap.put("min", JSONObject.parseObject(ladderInfo.getString(0)).getString("min_number"));
            //price取值 min_price,乘以倍数
            priceMap.put("price", MathUtil.multiply(pricediscount, price.doubleValue()));
            //币种人民币
            priceMap.put("currency", "1");
            priceMap.put("max", "0");
            priceMap.put("position", "1");
            priceMap.put("discount_rate", "0.0000");
            priceMap.put("discount_price", "0.0000");
            ladderPriceList.add(priceMap);
        }
        return ladderPriceList;

    }


}