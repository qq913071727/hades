package com.yuanda.erp9.syn.service.dyj_middledata.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yuanda.erp9.syn.entity.dyj_middledata.DyjInstorageByPriceEntity;
import com.yuanda.erp9.syn.mapper.dyj_middledata.DyjInstorageByPriceMapper;
import com.yuanda.erp9.syn.service.dyj_middledata.DyjInstorageByPriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 大赢家库存价格预警读表管理
 * @Date 2023/4/7 14:50
 **/
@Service
public class DyjInstorageByPriceServiceImpl implements DyjInstorageByPriceService {
    @Resource
    private DyjInstorageByPriceMapper dyjInstorageByPriceMapper;

    @Override
    public List<DyjInstorageByPriceEntity> findAll() {
        LambdaQueryWrapper<DyjInstorageByPriceEntity> wrapper = new LambdaQueryWrapper<>();
        return dyjInstorageByPriceMapper.selectList(wrapper);
    }
}
