package com.yuanda.erp9.syn.service.erp9;


/**
 * @ClassName VanlinkonService
 * @Description
 * @Date 2022/12/15
 * @Author myq
 */
public interface VanlinkonService {

    public void parse(Long pk);

}
