package com.yuanda.erp9.syn.contant;

/**
 * 数据库操作类型
 */
public class DatabaseOperationType {

    /**
     * 增
     */
    public static final String INSERT = "insertBatch";

    /**
     * 删
     */
    public static final String DELETE = "delete";

    /**
     * 改
     */
    public static final String UPDATE = "update";

}
