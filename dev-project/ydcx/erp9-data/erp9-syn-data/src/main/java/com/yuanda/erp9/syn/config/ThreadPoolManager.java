package com.yuanda.erp9.syn.config;

import com.yuanda.erp9.syn.value.ParseMessageThreadProperty;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池管理器
 */
@Component
public class ThreadPoolManager {
    @Resource
    private ParseMessageThreadProperty parseMessageThreadProperty;

    private static ParseMessageThreadProperty config;

    @PostConstruct
    public void init() {
        config = parseMessageThreadProperty;
    }

    private static ThreadPoolExecutor threadPoolExecutor;

    public static ThreadPoolExecutor getInstance() {
        if (null == threadPoolExecutor) {
            threadPoolExecutor = new ThreadPoolExecutor(
                    config.getCorePoolSize(),
                    config.getMaxPoolSize(),
                    config.getKeepAliveTime(), TimeUnit.SECONDS, new LinkedBlockingDeque(config.getWorkQueue()),
                    new CustomizableThreadFactory("erp9-syn-data-pool-"),
                    new ThreadPoolExecutor.AbortPolicy()
            );
            return threadPoolExecutor;
        } else {
            return threadPoolExecutor;
        }
    }
}