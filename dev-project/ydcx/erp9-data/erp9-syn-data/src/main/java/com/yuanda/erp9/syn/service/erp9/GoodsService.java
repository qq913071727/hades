package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.vo.PluginVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * @ClassName GoodsService
 * @Description
 * @Date 2022/11/15
 * @Author myq
 */
public interface GoodsService {
    /**
     * @param pojos
     * @Description: 批量新增
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1613:23
     */
    void insertBatch(List<CmsGoodsPojo> pojos);

    /**
     * @Description: 批量新增之前set
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:30
     */
    void insertBatchBeforeSet();

    /**
     * @Description: 批量新增之后reset
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1713:31
     */
    void insertBatchAfterReset();

    /**
     * @Description: 清空表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1616:02
     */
    void truncateTable(String tableName);

    /**
     * @Description: 返回brandID
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2414:17
     */
    void getPKByBrandName(CmsBrandsTempEntity entity);

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2414:17
     */
    Integer getPKBySupplierName(String supplierName);

    /**
     * @Description: 删除上次更新的数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1915:18
     */
    void deleteAll(List<String> goodDeletePk);

    /**
     * @Description: 获取待删除的主键集合
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2010:53
     */
    List<String> queryDeleteIds(Integer supplierId, String hisp, Integer source);

    /**
     * @Description: 获取待删除的主键集合
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2010:53
     */
    List<String> queryDeleteIds(Integer supplierId, Set<String> hispSet);

    /**
     * @Description: 获取待删除的主键集合
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2010:53
     */
    List<String> queryRsDeleteIds(Integer supplierId, Set<String> hispSet, Integer source);

    /**
     * plugin表更改 影响goods表
     *
     * @param pluginVo
     */
    Integer updateRateByManufacturerAndModel(PluginVo pluginVo, String rate, Integer tariffsRate, BigDecimal inspectionChargesFee);

    Integer updateAllRate(String value);

    /**
     * 根据Manufacturer和Model 查询已更改的数据
     *
     * @param pluginVo
     * @return
     */
    List<CmsGoodsEntity> findByManufacturerAndModel(PluginVo pluginVo);


    /**
     * 查询
     *
     * @return
     */
    Integer findAllCount();

    /**
     * 分页查询
     *
     * @param key
     * @param integer
     * @return
     */
    List<CmsGoodsEntity> findByLimit(Integer key, Integer integer);
}