package com.yuanda.erp9.syn.helper;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName FutureExcelHelper
 * @Description
 * @Date 2022/12/2
 * @Author myq
 */
@Component
@Slf4j
public class FutureExcelHelper extends JsonConfigHelper {


    private final String fileName = "json/FutureConfig.json";


    /**
     * key = uniqueId list = 阶梯价格
     * 同一个uniqueId的进价数据
     */
    private static ConcurrentHashMap<String, List<String>> prefixUniquePrices = new ConcurrentHashMap<>();


    /**
     * @Description: 获取depot
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/218:07
     */
    public String getDepot(String key) {
        JSONObject jsonObject = JSONObject.parseObject(super.getJSONString(fileName, key));
        return jsonObject.getString("Depot");
    }

    /**
     * @Description: 费率
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/218:08
     */
    public Double getRate(String key) {
        JSONObject jsonObject = JSONObject.parseObject(super.getJSONString(fileName, key));
        return jsonObject.getDouble("Rate");
    }

    /**
     * @Description: 货期
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/218:08
     */
    public String getDelivery(String key) {
        JSONObject jsonObject = JSONObject.parseObject(super.getJSONString(fileName, key));
        return jsonObject.getString("Delivery");
    }


    /**
     * @Description: 获取阶梯价格
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/114:40
     */
    public List<Map<String, String>> getLadderPrice(List<String> qtyList, List<String> priceList, String userType, String uniqueId) {
        // 阶梯价格
        List<Map<String, String>> resPriceList = new ArrayList<>(6);
        int last = 6 - 1;
        int index = 0;
        try {
            // A7038192 C5155406
            for (int x = 0; x < 6; x++) {
                String qty = qtyList.get(x);
                // 阶梯价格
                String price = priceList.get(x);
                // 当前阶梯价格为空
                if (StringUtils.isEmpty(price) || "CALL".equals(price)) {
                    return resPriceList;
                }
                Map<String, String> priceMap = new LinkedHashMap<>();
                // 第一个阶梯数量
                priceMap.put("min", qty);
                // 0无限大 防止数组越界
                if (x == last) {
                    priceMap.put("max", "0");
                } else {
                    String nextPrice = priceList.get(x + 1);
                    if (StringUtils.isEmpty(nextPrice) || "CALL".equals(nextPrice)) {
                        priceMap.put("max", "0");
                    } else {
                        priceMap.put("max", String.valueOf(Integer.parseInt(qtyList.get(x + 1)) - 1));
                    }
                }
                priceMap.put("price", price);
                // 1RMB 2USD
                priceMap.put("currency", "2");
                priceMap.put("position", index++ + "");
                if ("NORTEC".equals(userType)) {
                    prefixUniquePrices.put(uniqueId, priceList);
                    priceMap.put("discount_rate", "0.0000");
                    priceMap.put("discount_price", price);
                } else {
                    // 同一个uniqueId的进价数据
                    List<String> prePriceList = prefixUniquePrices.get(uniqueId);
                    if (CollectionUtils.isEmpty(prePriceList)) {
                        priceMap.put("discount_rate", "0.0000");
                        priceMap.put("discount_price", "0.0000");
                    } else {
                        // 原价
                        BigDecimal originalPrice = new BigDecimal(price);
                        String inPrice = prePriceList.get(x);
                        if (StringUtils.isEmpty(inPrice) || "CALL".equals(inPrice)) {
                            return resPriceList;
                        }
                        // 进价
                        BigDecimal purchasePrice = new BigDecimal(inPrice);
                        BigDecimal subtract = originalPrice.subtract(purchasePrice).setScale(8, BigDecimal.ROUND_UP);
                        BigDecimal bigDecimal = subtract.divide(originalPrice, 4, BigDecimal.ROUND_UP);
                        priceMap.put("discount_rate", StringUtils.isEmpty(bigDecimal.toString()) ? "0.00" : bigDecimal.toString());
                        priceMap.put("discount_price", purchasePrice.toString());
                    }

                }
                resPriceList.add(priceMap);
            }
        } catch (NumberFormatException e) {
            log.warn("Future解析数据异常：{}" + e.getMessage());
        } finally {
            if (!userType.equals("NORTEC")) {
                prefixUniquePrices.remove(uniqueId);
            }
        }
        return resPriceList;
    }


}
