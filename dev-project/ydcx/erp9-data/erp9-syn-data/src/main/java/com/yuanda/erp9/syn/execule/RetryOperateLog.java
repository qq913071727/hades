package com.yuanda.erp9.syn.execule;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName RetryOperateLog
 * @Description 补偿日志类
 * @Date 2022/12/22
 * @Author myq
 */
@Builder
@Getter
@Setter
public class RetryOperateLog {

    /**
     * 原数据json串
     */
    private String sourceDataJsonString;

    /**
     * 异常日志
     */
    private String exceptionMsg;

    /**
     * 供应商ID
     */
    private Integer supplierId;
}
