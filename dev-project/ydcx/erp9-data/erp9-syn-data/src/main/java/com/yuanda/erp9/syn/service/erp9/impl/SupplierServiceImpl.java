package com.yuanda.erp9.syn.service.erp9.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yuanda.erp9.syn.contant.TableConstant;
import com.yuanda.erp9.syn.entity.CmsSuppliersTempEntity;
import com.yuanda.erp9.syn.mapper.erp9.CmsSuppliersTempMapper;
import com.yuanda.erp9.syn.service.erp9.SupplierService;
import com.yuanda.erp9.syn.util.CacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName BrandServiceImpl
 * @Description
 * @Date 2022/12/16
 * @Author myq
 */
@Slf4j
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private CmsSuppliersTempMapper cmsSuppliersTempMapper;

    @Override
    public List<CmsSuppliersTempEntity> selectList() {
        return cmsSuppliersTempMapper.selectList(new QueryWrapper<CmsSuppliersTempEntity>().isNotNull("name").isNotNull("supplier_us"));
    }

    @Override
    public Integer insertSupplier(String userId, String userName) {
        // 插入供应商数据
        CmsSuppliersTempEntity cmsSuppliersTempEntity = new CmsSuppliersTempEntity();
        cmsSuppliersTempEntity.setSupplierUs(userId);
        cmsSuppliersTempEntity.setName(userName);
        cmsSuppliersTempEntity.setTag(1);
        cmsSuppliersTempMapper.insert(cmsSuppliersTempEntity);
        // 缓存
        CacheUtil.Cache cache = CacheUtil.init(TableConstant.CMS_SUPPLIERS_TEMP);
        cache.put(cmsSuppliersTempEntity.getName(), cmsSuppliersTempEntity.getSupplierid());
        return cmsSuppliersTempEntity.getSupplierid();
    }

    /**
     * @Description: 新增
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2315:13
     */
    @Override
    public void insert(CmsSuppliersTempEntity suppliersTempEntity) {
        cmsSuppliersTempMapper.insert(suppliersTempEntity);
    }
}
