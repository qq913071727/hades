package com.yuanda.erp9.syn.service.rate.impl;

import com.yuanda.erp9.syn.mapper.rate.ExchangeRatesMapper;
import com.yuanda.erp9.syn.service.rate.ExchangeRatesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @desc   ExchangeRates表的service的实现类
 * @author linuo
 * @time   2022年12月16日14:51:26
 */
@Service
@Slf4j
public class ExchangeRatesServiceImpl implements ExchangeRatesService {
    @Resource
    private ExchangeRatesMapper exchangeRatesMapper;

    /**
     * 获取有效数据
     * @return 返回查询结果
     */
    @Override
    public BigDecimal getEffectiveData() {
        BigDecimal result = exchangeRatesMapper.getEffectiveData();
        if(result != null){
            return result;
        }
        return BigDecimal.ZERO;
    }
}