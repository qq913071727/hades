package com.yuanda.erp9.syn.StockPriceMonitor.Response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zengqinglong
 * @desc 汇率响应
 * @Date 2023/4/6 11:24
 **/
@Data
public class RatioResponse implements Serializable {
    private Integer status;
    private boolean success;
    private RatioVo data;

    @Data
    public static class RatioVo {
        private BigDecimal rate;
    }
}
