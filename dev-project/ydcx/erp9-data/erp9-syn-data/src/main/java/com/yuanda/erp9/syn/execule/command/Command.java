package com.yuanda.erp9.syn.execule.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.XmlWebApplicationContext;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * @ClassName Command
 * @Description 命令执行类
 * @Date 2022/11/14
 * @Author myq
 */
@Slf4j
public class Command {


    /**
     * @Description: 执行 。exe 等可执行程序
     * @Params: command 。exe执行路径
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1410:25
     */
    public static String executeCmd() throws Exception {
        final String exePath = "/opt/erp9-data/";
        Runtime runtime = Runtime.getRuntime();
        Resource resource = new XmlWebApplicationContext().getResource("classpath:exe/futureelectronics.exe");
        String uri = resource.getURI().toString();
        String path = uri.substring(uri.indexOf(":") + 2);
        log.info("******************开始下载future文件路径：" + path + "********************");

        String waitParsingPath = path.substring(0, path.indexOf("erp9-data") + 9) + "/future_files/INV_SHARE.xlsx";
        log.info("******************待解析的future文件路径：" + waitParsingPath + "********************");
        // windows上执行exe
//        Process process = runtime.exec("cmd /c " +  path);
        // linux上通过wine执行exe
        Process process = runtime.exec("wine " + path);
        return print(process) ? waitParsingPath : "";

    }


    /**
     * @Description: ProcessBuilder 执行程序
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1416:31
     */
    public static void startProgram(String programPath, String programName) throws IOException {
        String path = Command.class.getClassLoader().getResource("exe/futureelectronics.exe").getPath();
        int endIndex = path.lastIndexOf("/") + 1;
        String relativePath = path.substring(0, endIndex);
        try {
            ProcessBuilder pBuilder = new ProcessBuilder();
            pBuilder.directory(new File(relativePath));
            pBuilder.command(programName);
            pBuilder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @Description: desktop 执行程序
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1416:31
     */
    public static void startProgram(String programPath) throws IOException {
        log.info("启动应用程序：" + programPath);
        try {
            Desktop.getDesktop().open(new File(programPath));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("应用程序：" + programPath + "不存在！");
        }
    }

    /**
     * @Description: 打印命令行结果
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1415:01
     */
    private static boolean print(Process cmd) throws Exception {
        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(new InputStreamReader(cmd.getInputStream(), "GBK"));
        StringBuilder stringBuilder = new StringBuilder();
        String o = null;
        while ((o = bufferedReader.readLine()) != null) {
            stringBuilder.append(o);
        }
        cmd.waitFor();
        log.info("************future下载完成**************");
        if (cmd.exitValue() == 1) {
            log.error("结果集 :" + stringBuilder.toString());
            return false;
        } else {
            log.info("结果集 :" + stringBuilder.toString());
            return true;
        }
    }


}
