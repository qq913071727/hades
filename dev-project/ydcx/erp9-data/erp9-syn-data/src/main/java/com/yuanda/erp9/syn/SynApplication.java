package com.yuanda.erp9.syn;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @ClassName SynApplication
 * @Description 同步数据应用
 * @Date 2022/11/11
 * @Author myq
 */
@Slf4j
@SpringBootApplication(scanBasePackages = "com.yuanda.erp9.syn")
@EnableScheduling
@EnableAsync
@MapperScan({"com.yuanda.erp9.syn.mapper.erp9", "com.yuanda.erp9.syn.mapper.rate",
        "com.yuanda.erp9.syn.mapper.erp9_pvecrm","com.yuanda.erp9.syn.mapper.erp9_pvestandard",
        "com.yuanda.erp9.syn.mapper.dyj_middledata"})
public class SynApplication {

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(SynApplication.class, args);
        log.info("********************同步数据应用启动完成*************************");
    }
}