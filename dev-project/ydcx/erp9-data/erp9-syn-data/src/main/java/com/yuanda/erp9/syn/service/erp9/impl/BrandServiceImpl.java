package com.yuanda.erp9.syn.service.erp9.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yuanda.erp9.syn.contant.TableConstant;
import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.enums.BrandTagEnum;
import com.yuanda.erp9.syn.mapper.erp9.CmsBrandsTempMapper;
import com.yuanda.erp9.syn.service.erp9.BrandService;
import com.yuanda.erp9.syn.util.CacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName BrandServiceImpl
 * @Description
 * @Date 2022/12/16
 * @Author myq
 */
@Slf4j
@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private CmsBrandsTempMapper cmsBrandsTempMapper;

    @Override
    public List<CmsBrandsTempEntity> selectList() {
        return cmsBrandsTempMapper.selectList(new QueryWrapper<CmsBrandsTempEntity>().isNotNull("name"));
    }

    @Override
    public Integer insertBrand(String manufacturer) {
        CmsBrandsTempEntity entity = new CmsBrandsTempEntity();
        entity.setName(manufacturer);
        entity.setTag(BrandTagEnum.NEW.getCode());

        // 添加数据
        cmsBrandsTempMapper.insert(entity);

        // 缓存
        CacheUtil.Cache cache = CacheUtil.init(TableConstant.CMS_BRANDS_TEMP);
        cache.put(entity.getName(), entity.getBrandid());

        return entity.getBrandid();
    }


    /**
     * @Description: 新增
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2315:12
     */
    @Override
    public void insert(CmsBrandsTempEntity brandsTempEntity) {
        cmsBrandsTempMapper.insert(brandsTempEntity);
    }
}
