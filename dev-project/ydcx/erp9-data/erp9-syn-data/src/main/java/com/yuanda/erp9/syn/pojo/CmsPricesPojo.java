package com.yuanda.erp9.syn.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @desc   价格阶梯
 * @author linuo
 * @time   2022年11月30日14:58:17
 */
@Data
public class CmsPricesPojo {
    @ApiModelProperty(value = "最大值")
    private Long max;

    @ApiModelProperty(value = "最小值")
    private Long min;

    @ApiModelProperty(value = "进价")
    private BigDecimal price;

    @ApiModelProperty(value = "货币1人民币2美元")
    private Integer currency;

    @ApiModelProperty(value = "位置")
    private Integer position;

    @ApiModelProperty(value = "折扣率（ps）")
    private BigDecimal discountRate;

    @ApiModelProperty(value = "折扣价（ps）")
    private BigDecimal discountPrice;
}