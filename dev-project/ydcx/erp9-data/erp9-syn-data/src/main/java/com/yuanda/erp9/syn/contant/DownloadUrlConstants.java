package com.yuanda.erp9.syn.contant;

/**
 * @author linuo
 * @desc 下载url的常量值
 * @time 2022年12月7日15:44:19
 */
public class DownloadUrlConstants {
    /**
     * Aipco下载文件url
     */
    public static final String AIPCO_DOWN_FILE_URL = "http://dataup.b1b.com/aipco/aipco_all.csv";

    /**
     * Aipco下载文件名
     */
    public static final String AIPCO_DOWN_FILE_NAME = "aipco_all.csv";

    /**
     * Waldom下载文件url
     */
    public static final String WALDOM_DOWN_FILE_URL = "http://www.waldomapac.com/dailyFeed/ANDA_Inventory_Feed.csv";

    /**
     * Waldom下载文件名
     */
    public static final String WALDOM_DOWN_FILE_NAME = "ANDA_Inventory_Feed.csv";

    /**
     * ICGoo下载文件url
     */
    public static final String ICGOO_DOWN_FILE_URL = "http://dataup.b1b.com/goo/wangsq.csv";

    /**
     * ICGoo下载文件名
     */
    public static final String ICGOO_DOWN_FILE_NAME = "wangsq.csv";

    /**
     * CHIP1STOP_ARROWTECH 下载文件url
     */
    public static final String CHIP1STOP_ARROWTECH_DOWN_FILE_URL = "http://dataup.b1b.com/chip1/Chip1StopStockList_Arrowtech.zip";

    /**
     * CHIP1STOP_TI 下载文件url
     */
    public static final String CHIP1STOP_TI_DOWN_FILE_URL = "http://dataup.b1b.com/chip1/Chip1StopStockList_TI_Arrowtech.zip";

    /**
     * CHIP1STOP_VER2 下载文件url
     */
    public static final String CHIP1STOP_VER2_DOWN_FILE_URL = "http://dataup.b1b.com/chip1/Chip1StopStockList_ver2_Arrowtech.zip";

    /**
     * ELEMENT14_CN 下载文件url
     */
    public static final String ELEMENT14_CN_DOWN_FILE_URL = "http://pfd.premierfarnell.com/eCat_LIANCHUANG_ELECTRONICS_cn_data.zip";

    /**
     * ELEMENT14_HK 下载文件url
     */
    public static final String ELEMENT14_HK_DOWN_FILE_URL = "http://pfd.premierfarnell.com/eCat_LIANCHUANG_ELECTRONICS_hk_data.zip";

    /**
     * RS CN 下载文件url
     */
    public static final String RS_CN_DOWN_FILE_URL = "http://dataup.b1b.com/rs/rs_components_cn_b1b.csv.zip";

    /**
     * RS HK 下载文件url
     */
    public static final String RS_HK_DOWN_FILE_URL = "http://dataup.b1b.com/rs/rs_components_HK-cn_b1b.csv.zip";

    /**
     * AVNET 登陆地址
     */
    public static final String AVNET_LOGIN_URL = "https://b2b.avnet.com:8143/WebInterface/function/";

    public static final String AVNET_LOGIN_USER_NAME = "allic";

    public static final String AVNET_LOGIN_PASSWORD = "AvNet!23";
    /**
     * AVNET 下载地址
     */
    public static final String AVNET_DOWN_FILE_URL = "https://b2b.avnet.com:8143/DIGEXP_External_Data_Feed/AllicFTP/";
    /**
     * AVNET_ASIA_NAME
     */
    public static final String AVNET_ASIA_NAME = "allic_asia.csv";
    /**
     * AVNET_NAME
     */
    public static final String AVNET_NAME = "allic.csv";
    /**
     * AVNET_EMEA_NAME
     */
    public static final String AVNET_EMEA_NAME = "allic_emea.csv";


    /**
     * PEI_INVENTORY 下载文件url
     */
    public static final String PEI_INVENTORY_DOWN_FILE_URL = "http://dataup.b1b.com/peigenesis/pei_inventory.zip";


    /**
     * PEI_INVENTORY保存文件路径
     */
    public static final String PEI_INVENTORY_NAME = "pei_inventory.zip";

}