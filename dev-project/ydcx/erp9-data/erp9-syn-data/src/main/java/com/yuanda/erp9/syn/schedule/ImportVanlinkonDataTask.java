package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.VanlinkonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
@Slf4j
public class ImportVanlinkonDataTask {
    @Autowired
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Autowired
    private VanlinkonService vanlinkonService;

    /**
     * @Description: 导入数据
     * @Params: 周二到周日的14.10 和 17.10 执行各一次，每周一的10.40执行一次
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1517:44
     */
//    @Schedules({
//            @Scheduled(cron = "0 40 10 ? * MON"),
//            @Scheduled(cron = "0 10 14,17 ? * TUE-FRI")
//    })
    @XxlJob("ImportVanlinkonDataTask_importVanlinkonDataTask")
    @EnableSchedule
    public void importVanlinkonDataTask() {
        log.info("*****************vanlinkon-http-data-staring*****************");
        CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
        entity.setCreateAt(new Date());
        entity.setSupplier(44);
        entity.setSystemTopicType(SubjectSupplierEnum.VANLINKON.getType());
        entity.setSystemTopic(SubjectSupplierEnum.VANLINKON.getSubject());
        Long pk = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);

        vanlinkonService.parse(pk);
        log.info("*****************vanlinkon-http-data-end*****************");
    }
}
