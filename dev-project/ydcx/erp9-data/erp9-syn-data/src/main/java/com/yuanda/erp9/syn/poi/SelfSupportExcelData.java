package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @ClassName SelfSupportExcelData
 * @Description 自营数据
 * @Date 2023/1/10
 * @Author myq
 */
@Data
public class SelfSupportExcelData {

    @ExcelProperty(index = 1)
    private String type;

    @ExcelProperty(index = 2)
    private String specificationsModel;

    @ExcelProperty(index = 3)
    private String specificationsParam;

}
