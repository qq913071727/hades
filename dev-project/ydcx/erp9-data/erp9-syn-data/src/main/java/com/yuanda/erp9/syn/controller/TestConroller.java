package com.yuanda.erp9.syn.controller;

import com.yuanda.erp9.syn.schedule.ImportFutureCmdTask;
import com.yuanda.erp9.syn.schedule.ImportVanlinkonDataTask;
import com.yuanda.erp9.syn.service.erp9.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.io.IOException;

@Slf4j
@RestController
public class TestConroller {
    @Resource
    private ImapReceiveMailService imapReceiveMailService;

    @Resource
    private AipcoDataImportService aipcoDataImportService;

    @Resource
    private WaldomDataImportService waldomDataImportService;

    @Resource
    private AvnetExcelService avnetExcelService;

    @Resource
    private ICGooDataImportService icGooDataImportService;

    @Resource
    private RsDataImportService rsDataImportService;

    @Resource
    private Chip1StopCsvImportService chip1StopCsvImportService;

    @Resource
    private FutureExcelService futureExcelService;

    @Resource
    private CoreStaffStockExcelService coreStaffStockExcelService;

    @Resource
    private AventAbacusExcelService aventAbacusExcelService;

    @Resource
    private VanlinkonService vanlinkonService;

    @Resource
    private ImportFutureCmdTask importFutureCmdTask;

    @Resource
    private SelfSupportService selfSupportService;

    @Resource
    private Chip1AdimaxService chip1AdimaxService;

    @Resource
    private ImportVanlinkonDataTask importVanlinkonDataTask;

    @GetMapping("test")
    public String test() {
//        coreStaffStockExcelService.parse("C:\\Users\\myq\\Desktop\\Corestaff Stock List.xlsx", pk);
//        aventAbacusExcelService.parse("C:\\Users\\myq\\Desktop\\abacus.xlsx");
        importVanlinkonDataTask.importVanlinkonDataTask();
//        chip1AdimaxService.parse("C:\\Users\\myq\\Desktop\\Chip1Stop_ADIONINFIN_RMB_20230308.xls");
//        importFutureCmdTask.commandEventListener();
//        selfSupportService.parse();
//        futureExcelService.parse();

        return "ok";
    }

    @GetMapping("email")
    public String email() {
        imapReceiveMailService.downloadImapMail();
        return "ok";
    }

    /**
     * 导入Aipco文件数据
     *
     * @return 返回导入结果
     */
    @GetMapping("importAipcoFileData")
    public String importAipcoFileData() throws InterruptedException {
        return aipcoDataImportService.importCmsGoodsPojo(1L,"D:\\aipco_all.csv");
    }

    /**
     * 导入Waldom文件数据
     *
     * @return 返回导入结果
     */
    @GetMapping("importWaldomFileData")
    public String importWaldomFileData() throws InterruptedException {
        return waldomDataImportService.importCmsGoodsPojo(1L,"D:\\ANDA_Inventory_Feed.csv");
    }

    /**
     * 导入ICGoo文件数据
     *
     * @return 返回导入结果
     */
    @GetMapping("importIcGooFileData")
    public String importIcGooFileData() throws InterruptedException {
        return icGooDataImportService.importCmsGoodsPojo(1L,"D:\\wangsq.csv");
    }
    /**
     * 导入rs香港文件数据
     *
     * @return 返回导入结果
     * @throws InterruptedException InterruptedException
     */
    @GetMapping("importRsHkFileData")
    public String importRsHkFileData() throws InterruptedException, IOException {
        rsDataImportService.csvDataInsert("D:\\rs_components_cn_b1b.csv", "D:\\rs_components_HK-cn_b1b.csv");
        return "ok";
    }

}