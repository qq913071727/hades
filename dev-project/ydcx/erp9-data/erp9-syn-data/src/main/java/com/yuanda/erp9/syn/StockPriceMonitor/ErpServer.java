package com.yuanda.erp9.syn.StockPriceMonitor;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.yuanda.erp9.syn.StockPriceMonitor.Request.ErpLoginRequest;
import com.yuanda.erp9.syn.StockPriceMonitor.Request.ErpPushInquiryPriceRequest;
import com.yuanda.erp9.syn.StockPriceMonitor.Response.ErpLoginResponse;
import com.yuanda.erp9.syn.StockPriceMonitor.Response.ErpPushInquiryPriceResponse;
import com.yuanda.erp9.syn.StockPriceMonitor.Response.RatioResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc erp 服务
 * @Date 2023/4/6 11:18
 **/
@Slf4j
@Component
public class ErpServer {
    /**
     * 测试 和 生产只能选一个不能同时（大赢家就提供一个库）
     * 汇率地址 erp9.b1b.com测试地址  erp.b1b.cn生产地址
     */
    private static String RatioSelectorUrl = "https://erp.b1b.cn/bcsapi/api/ElementUI/ExchangeRatioSelector";

    /**
     * 登陆地址 rp9.b1b.com测试地址  erp.b1b.cn生产地址
     */
    private static String LoginUrl = "https://erp.b1b.cn/platapi/plat/login";

    /**
     * 测试 和 生产只能选一个不能同时（大赢家就提供一个库）
     * 推送询价地址 rp9.b1b.com测试地址  erp.b1b.cn生产地址
     */
    private static String pushInquiryPriceUrl = "https://erp.b1b.cn/rfqapi/api/Bom/Enter";

    /**
     * 测试 和 生产只能选一个不能同时（大赢家就提供一个库）
     * 登陆用户名 warning
     */
    private static String UserName = "warning";
    /**
     * 测试 和 生产只能选一个不能同时（大赢家就提供一个库）
     * 登陆密码 warning
     */
    private static String Passwrod = "123456";


    /**
     * 获取人民币转换美元的汇率
     *
     * @return
     */
    public RatioResponse getRatio() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "100");
        map.put("form", "USD");
        map.put("to", "CNY");
        Map<String, String> headers = new HashMap<>();
        headers.put("connection", "Keep-Alive");
        headers.put("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        headers.put("accept", "*/*");
        long l = System.currentTimeMillis();
        log.info("调用 [ERP服务 获取汇率] : 开始, url:{}, header:{}, param:{}", RatioSelectorUrl, headers, map);
        String result = "";
        try {
            result = HttpUtils.doGet(RatioSelectorUrl, map, headers);
            log.info("调用 [ERP服务 获取汇率] : 结束 耗时:{}ms  result:{} ", System.currentTimeMillis() - l, result);
        } catch (Exception e) {
            log.error("调用 [ERP服务 获取汇率] 失败 耗时:{}ms   ", System.currentTimeMillis() - l, e);
            RatioResponse commonRsp = new RatioResponse();
            commonRsp.setStatus(-1);
            commonRsp.setSuccess(false);
            return commonRsp;
        }
        return JSON.parseObject(result, RatioResponse.class);
    }

    /**
     * 登陆
     *
     * @return
     */
    public ErpLoginResponse ErpLogin() {
        ErpLoginRequest erpLoginRequest = new ErpLoginRequest();
        erpLoginRequest.setUserName(UserName);
        erpLoginRequest.setPasswrod(Passwrod);
        long l = System.currentTimeMillis();
        log.info("调用 [ERP服务 登陆] : 开始, url:{}, param:{}", RatioSelectorUrl, JSON.toJSONString(erpLoginRequest));
        String result = "";
        try {
            result = HttpUtils.doPostWithJson(LoginUrl, erpLoginRequest, null);
            log.info("调用 [ERP服务 登陆] : 结束 耗时:{}ms  result:{} ", System.currentTimeMillis() - l, result);
        } catch (Exception e) {
            log.error("调用 [ERP服务 登陆] 失败 耗时:{}ms   ", System.currentTimeMillis() - l, e);
            ErpLoginResponse commonRsp = new ErpLoginResponse();
            commonRsp.setSuccess(false);
            return commonRsp;
        }

        return JSON.parseObject(result, ErpLoginResponse.class);
    }

    /**
     * 推送询价
     *
     * @param erpPushInquiryPriceRequest
     * @param token
     */
    public ErpPushInquiryPriceResponse pushInquiryPriceRequest(ErpPushInquiryPriceRequest erpPushInquiryPriceRequest, String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("connection", "Keep-Alive");
        headers.put("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        headers.put("accept", "*/*");
        headers.put("Content-Type", "application/json;UTF-8");
        headers.put("Authorization", "Bearer " + token);
        long l = System.currentTimeMillis();
        log.info("调用 [ERP服务 推送询价] : 开始, url:{},headers{}, param:{}", pushInquiryPriceUrl, headers, JSON.toJSONString(erpPushInquiryPriceRequest));
        String result = "";
        try {
            result = HttpUtils.doPostWithJson(pushInquiryPriceUrl, erpPushInquiryPriceRequest, headers);
            log.info("调用 [ERP服务 推送询价] : 结束 耗时:{}ms  result:{} ", System.currentTimeMillis() - l, result);
        } catch (Exception e) {
            log.error("调用 [ERP服务 推送询价] 失败 耗时:{}ms   ", System.currentTimeMillis() - l, e);
        }

        return JSON.parseObject(result, ErpPushInquiryPriceResponse.class);
    }
}
