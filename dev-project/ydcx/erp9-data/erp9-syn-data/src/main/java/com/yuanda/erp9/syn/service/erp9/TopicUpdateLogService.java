package com.yuanda.erp9.syn.service.erp9;


/**
 * @ClassName TopicUpdateLogService
 * @Description
 * @Date 2022/12/16
 * @Author myq
 */
public interface TopicUpdateLogService {


    void insert(Integer type,String subject,Integer supplierId);
}
