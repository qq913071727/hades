package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 其他警告或异常日志
 *
 * @author myq
 * @since 1.0.0 2023-01-04
 */
@TableName("cms_other_warn_log")
@Setter
@Getter
public class CmsOtherWarnLogEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "供应商主键")
    private Integer supplier;

    @ApiModelProperty(value = "文件名称/主题名称/zip名称")
    private String fileName;

    @ApiModelProperty(value = "警告日志：邮件解析失败、zip无法解压、无法连接到目标服务器等错误日志")
    private String warnLog;

    @ApiModelProperty(value = "创建时间")
    private String createTime;
}