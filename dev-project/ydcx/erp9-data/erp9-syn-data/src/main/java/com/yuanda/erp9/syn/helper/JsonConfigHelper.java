package com.yuanda.erp9.syn.helper;

import com.alibaba.fastjson.JSONObject;
import com.yuanda.erp9.syn.util.JsonReaderUtil;

/**
 * @ClassName JsonConfigHelper
 * @Description 帮助子类初始化json配置文件
 * @Date 2022/12/6
 * @Author myq
 */
public class JsonConfigHelper {


    /**
     * @Description: 获取完成jsonObject
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/218:03
     */
    protected String getJSONString(String fileName, String key) {
        return JsonReaderUtil.readJsonFile(fileName).getString(key);
    }
}
