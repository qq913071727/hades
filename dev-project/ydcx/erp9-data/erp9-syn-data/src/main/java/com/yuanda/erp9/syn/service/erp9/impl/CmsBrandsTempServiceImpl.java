package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.mapper.erp9.CmsBrandsTempMapper;
import com.yuanda.erp9.syn.service.erp9.CmsBrandsTempService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @desc   品牌表service实现类
 * @author linuo
 * @time   2023年1月4日13:55:48
 */
@Slf4j
@Service
public class CmsBrandsTempServiceImpl implements CmsBrandsTempService {
    @Resource
    private CmsBrandsTempMapper cmsBrandsTempMapper;

    /**
     * 插入数据
     * @param cmsBrandsTempEntity 品牌实体类
     */
    @Override
    public void insert(CmsBrandsTempEntity cmsBrandsTempEntity) {
        cmsBrandsTempMapper.insert(cmsBrandsTempEntity);
    }
}