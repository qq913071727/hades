package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.SelfSupportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Date;

/**
 * @ClassName ImportSelfSupportDataTask
 * @Description 自营定时任务
 * @Date 2023/1/10
 * @Author myq
 */
@Component
@Slf4j
public class ImportSelfSupportDataTask {
    @Autowired
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Autowired
    private SelfSupportService selfSupportService;

    /**
     * @Description: 导入数据
     * @Return: 每天7.40执行一次，每小时执行一个 一共执行12次
     * @Author: Mr.myq
     * @Date: 2022/12/1517:44
     */
//    @Scheduled(cron = "0 40 7,8,9,10,11,12,13,14,15,16,17,18 * * ?")
    @XxlJob("ImportSelfSupportDataTask_importSelfSupportDataTask")
    @EnableSchedule
    public void importSelfSupportDataTask() {
        log.info("*****************SelfSupport-http-data-staring*****************");
        CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
        entity.setCreateAt(new Date());
        entity.setSupplier(29);
        entity.setSystemTopicType(SubjectSupplierEnum.SELFSUPPORT.getType());
        entity.setSystemTopic(SubjectSupplierEnum.SELFSUPPORT.getSubject());
        Long pk = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);

        selfSupportService.parse(pk);
        log.info("*****************SelfSupport-http-data-end*****************");
    }
}