package com.yuanda.erp9.syn.execule.thread;

import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.service.erp9.ESService;
import com.yuanda.erp9.syn.service.erp9.impl.ESServiceImpl;
import com.yuanda.erp9.syn.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * @ClassName Worker
 * @Description 子任务工作者
 * @Date 2022/11/15
 * @Author myq
 */
@Slf4j
public class ElasticSearchWorker<T> extends AbstractWorker<T> {

    // 数据集合
    private List<T> task;
    // 子任务执行情况结果集
    private ConcurrentHashMap<String, Object> resultMap;

    public ElasticSearchWorker(List<T> task, ConcurrentHashMap<String, Object> resultMap) {
        this.task = task;
        this.resultMap = resultMap;
    }


    /**
     * @Description: 将数据分批执行添加到数据库中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1517:09
     */
    @Override
    void task() {
        String workerThreadName = Thread.currentThread().getName();
        try {
            log.info("**************开始同步es数据**************");
            ESService esService = SpringContextUtil.getBean("ESServiceImpl", ESServiceImpl.class);
            esService.addESData((List<CmsGoodsPojo>) task);
            log.info("***************结束同步es数据*************");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("线程名称：{},同步es数据异常： {}", workerThreadName, e.toString());

            resultMap.put("errMsg", "worker run error : {" + e + "}");
            resultMap.put("errThreadName", workerThreadName);
            resultMap.put("type", "elasticsearch");
            throw new RuntimeException("Thread-Name: " + workerThreadName + "worker run error :{}");
        } finally {
            task.clear();
        }
    }

}
