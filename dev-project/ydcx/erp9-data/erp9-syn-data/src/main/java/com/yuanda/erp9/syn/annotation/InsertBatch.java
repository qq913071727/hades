package com.yuanda.erp9.syn.annotation;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @ClassName InsertBatch
 * @Description  方法上增加此注解可以帮助root做一些前期准备工作和后期重置工作
 * @Date 2022/11/16
 * @Author myq
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InsertBatch {

    /**
     * 需要请空的表名称
     */
    String truncateTable() default "";


    /**
     * 使用tableName的value值
     */
//    @AliasFor(
//            annotation = TableName.class, value = "value"
//    )
    Class<?> tableName() default Object.class;


    /**
     * 反射调用方法
     */
    String method()default "";

}
