package com.yuanda.erp9.syn.mapper.erp9;

import com.yuanda.erp9.syn.core.RootMapper;
import com.yuanda.erp9.syn.entity.CmsOperateErrorLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 操作异常日志
 *
 * @author myq
 * @since 1.0.0 2022-12-22
 */
@Mapper
public interface CmsOperateErrorLogMapper extends RootMapper<CmsOperateErrorLogEntity> {

}
