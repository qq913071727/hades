package com.yuanda.erp9.syn.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc   批次号参数pojo类
 * @author linuo
 * @time   2022年12月1日16:13:48
 */
@Data
public class BatchParamPojo {
    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "数量")
    private Integer count;
}