package com.yuanda.erp9.syn.service.erp9;

/**
 * @desc   供应商临时表Service
 * @author linuo
 * @time   2023年1月4日11:35:06
 */
public interface CmsSuppliersTempService {
    /**
     * 获取供应商id
     * @param supplierName 供应商名称
     * @param supplier erp的供应商id
     * @return 返回查询结果
     */
    Integer getSupplierIdByUs(String supplierName, String supplier);
}