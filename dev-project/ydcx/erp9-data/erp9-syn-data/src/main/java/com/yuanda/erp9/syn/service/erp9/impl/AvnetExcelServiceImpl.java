package com.yuanda.erp9.syn.service.erp9.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.csvreader.CsvReader;
import com.yuanda.erp9.syn.contant.RateCalculateConstants;
import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsInformationEntity;
import com.yuanda.erp9.syn.entity.CmsPricesEntity;
import com.yuanda.erp9.syn.enums.*;
import com.yuanda.erp9.syn.execule.thread.Master;
import com.yuanda.erp9.syn.helper.RetryOperateLogHelper;
import com.yuanda.erp9.syn.poi.AvnetCsvData;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.pojo.CmsPricesPojo;
import com.yuanda.erp9.syn.service.erp9.AvnetExcelService;
import com.yuanda.erp9.syn.service.erp9.SupplierService;
import com.yuanda.erp9.syn.util.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zengqinglong
 * @desc Avnet
 * @Date 2022/11/29 10:37
 **/
@Service
@Slf4j
public class AvnetExcelServiceImpl extends CsvSaveImpl implements AvnetExcelService {
    @Resource
    private SupplierService supplierService;
    @Autowired
    private RetryOperateLogHelper retryOperateLogHelper;

    protected static CacheUtil.ObjectCache exchangeRates = CacheUtil.objectInit(CachePrefixEnum.EXCHANGE_RATES.getKey());
    private static CacheUtil.Cache brand = CacheUtil.init(CachePrefixEnum.BRAND.getKey());
    private static CacheUtil.Cache suppliers = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());
    private static ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();

    @Override
    public void analysisAvnet(Long topicUpdateLogId, String filePath) {
        if (StringUtils.isEmpty(filePath)) {
            return;
        }
        //获取不同Avnet配置文件
        AvnetContantPojo avnetContantPojo;
        if (filePath.endsWith("allic_asia.csv")) {
            avnetContantPojo = getAvnetAsia();
        } else if (filePath.endsWith("allic_emea.csv")) {
            avnetContantPojo = getAvnetEmea();
        } else if (filePath.endsWith("allic.csv")) {
            avnetContantPojo = getAvnet();
        } else {
            return;
        }


        // 处理供应商信息，缓存中没有当前供应商时添加信息到数据库和缓存中
        Integer supplierId = suppliers.get(avnetContantPojo.getUserName());
        if (supplierId == null) {
            // 插入供应商数据
            int insert = supplierService.insertSupplier(avnetContantPojo.getUserId(), avnetContantPojo.getUserName());
            supplierId = insert;
        }

        Master<CmsGoodsPojo> objectMaster = new Master<>(supplierId, null, SourceEnum.DATA_PACKAGE.getCode());
        // 读取csv数据
        CsvReader csvReader = null;
        try {
            log.info("Avnet数据导入开始");
            csvReader = new CsvReader(filePath, ',');
            // 跳过表头(相当于读取完了表头)
            csvReader.readHeaders();
            long count = 0;
            // 读取每行的内容
            while (csvReader.readRecord()) {
                count++;
                // 获取当前行的所有数据(数组形式)
                String[] values = csvReader.getValues();
                AvnetCsvData avnetCsvData = AvnetCsvData.toAvnetCsvDataFormat(values);
                if (avnetCsvData == null) {
                    continue;
                }
                avnetCsvData.setSupplierId(supplierId);
                CmsGoodsPojo cmsGoodsPojo = insertDataToDatabase(avnetCsvData, avnetContantPojo);
                if (cmsGoodsPojo != null) {
                    objectMaster.add(cmsGoodsPojo);
                }
            }

            if (topicUpdateLogId != null) {
                // 插入系统主体更新数量日志记录表数据
                cmsTopicUpdateSumLogService.insertOrUpdateByTopicUpdateLogId(count, topicUpdateLogId);
            }
            log.info("Avnet数据导入结束");
        } catch (Exception e) {
            log.warn("解析avnet文件异常:e:", e);
        } finally {
            // 退出标志
            objectMaster.isBegin = false;
            map.clear();
            // 读取完成之后,关闭流
            if (csvReader != null) {
                csvReader.close();
            }
        }
    }

    private CmsGoodsPojo insertDataToDatabase(AvnetCsvData avnetCsvData, AvnetContantPojo avnetContantPojo) {
        try {
            // 包装类
            CmsGoodsPojo cmsGoodsPojo = new CmsGoodsPojo();
            //AvNet     Xilinx    品牌产品滤掉，删除  mf_name= Xilinx
            //Avnet :  删除 Xilinx     20191202 邱花 （xilinx 继续删除；TI 可以上线了）
            if (null == avnetCsvData.getManufacturer() || StringUtils.isEmpty(avnetCsvData.getManufacturer()) || avnetCsvData.getManufacturer().equals("xilinx")) {
                return null;
            }
            //排除库存为空的数据 排除小于1的数据
            if (null == avnetCsvData.getStockQuantity() || Integer.valueOf(avnetCsvData.getStockQuantity()) < 1) {
                return null;
            }

            String manufacturer = avnetCsvData.getManufacturer();

            //商品类
            CmsGoodsEntity cmsGoodsEntity = new CmsGoodsEntity();
            cmsGoodsEntity.setAdminid(0);
            cmsGoodsEntity.setTenantsid(0);

            cmsGoodsEntity.setClassifyid(0);
            //img
            if (StringUtils.isEmpty(avnetCsvData.getProductImageURL())) {
                cmsGoodsEntity.setImg("");
            } else {
                cmsGoodsEntity.setImg(avnetCsvData.getProductImageURL().length() > 20 ? avnetCsvData.getProductImageURL() : "");

            }
            cmsGoodsEntity.setType(0);
            cmsGoodsEntity.setStatus(1);
            cmsGoodsEntity.setStatusMsg("");
            cmsGoodsEntity.setDataType(1);
            //产品型号
            cmsGoodsEntity.setModel(avnetCsvData.getManufacturerPart());

            // 处理品牌信息，缓存中没有当前品牌时添加信息到数据库和缓存中
            Integer i = brandCache.get(manufacturer);
            if (i == null) {
                CmsBrandsTempEntity entity = new CmsBrandsTempEntity();
                entity.setName(manufacturer);
                entity.setTag(BrandTagEnum.NEW.getCode());
                CacheUtil.Cache brands = CacheUtil.init(CachePrefixEnum.BRAND.getKey());
                Integer brandId = brands.insertAndGet(entity);
                cmsGoodsEntity.setBrand(brandId);
            } else {
                cmsGoodsEntity.setBrand(i);
            }

            cmsGoodsEntity.setSupplier(avnetCsvData.getSupplierId());
            cmsGoodsEntity.setIsSampleApply(0);
            //avnet无批次
            cmsGoodsEntity.setBatch("");
            cmsGoodsEntity.setOriginalPrice(new BigDecimal("0.00"));
            cmsGoodsEntity.setPurchasePrice(new BigDecimal("0.00"));
            cmsGoodsEntity.setUnitPrice(0.00);
            //库存
            Integer integer = Integer.valueOf(StringUtils.isEmpty(avnetCsvData.getStockQuantity()) ? "0" : avnetCsvData.getStockQuantity());
            //库存为零期货需要更改为空
            if (integer == 0) {
                avnetContantPojo.setDelivery("");
                avnetContantPojo.setHkDelivery("");
            }
            cmsGoodsEntity.setRepertory(integer);
            // 库存所在地
            cmsGoodsEntity.setRepertoryArea(avnetContantPojo.getDepot());
            cmsGoodsEntity.setPublish(1);
            //描述
            cmsGoodsEntity.setDescribe(avnetCsvData.getDescription() == null ? "" : avnetCsvData.getDescription().substring(0, avnetCsvData.getDescription().length() > 100 ? 100 : avnetCsvData.getDescription().length()));
            // files
            if (StringUtils.isEmpty(avnetCsvData.getDatasheetURL())) {
                cmsGoodsEntity.setFiles("");
            } else {
                cmsGoodsEntity.setFiles(avnetCsvData.getDatasheetURL().length() > 19 ? avnetCsvData.getDatasheetURL() : "");
            }
            cmsGoodsEntity.setPayType(0);
            //最新起订
            Long minimumOrder = Long.valueOf(StringUtils.isEmpty(avnetCsvData.getMoq()) ? "1" : avnetCsvData.getMoq());
            cmsGoodsEntity.setMinimumOrder(minimumOrder == 0 ? 1L : minimumOrder);
            // mpq
            Long mpq = Long.valueOf(StringUtils.isEmpty(avnetCsvData.getMulti()) ? cmsGoodsEntity.getMinimumOrder().toString() : avnetCsvData.getMulti());
            cmsGoodsEntity.setMpq(mpq == 0 ? 1L : mpq);
            //增量
            cmsGoodsEntity.setIncremental(mpq == 0 ? 1 : mpq);
            // 交货地区
            cmsGoodsEntity.setDeliveryArea(DeliveryAreaEnum.OTHER.getCode());
            //国内交货日期
            cmsGoodsEntity.setDomesticDate(avnetContantPojo.getDelivery());
            //香港交货时间
            cmsGoodsEntity.setHongkongDate(avnetContantPojo.getHkDelivery());
            //用地区来区分不同文件夹 目前只执行 avnet_asia
            //cmsGoodsEntity.setHisp(avnetContantPojo.getDepot());
            cmsGoodsEntity.setSales(0);
            cmsGoodsEntity.setClick(0);
            cmsGoodsEntity.setVisitor(0);
            cmsGoodsEntity.setCreatedAt(DateUtils.now());
            cmsGoodsEntity.setUpdatedAt(DateUtils.now());
            cmsGoodsEntity.setActivityId(0);
            cmsGoodsEntity.setAtlas("");
            // 商品详情
            cmsGoodsEntity.setContent("");
            // eccno
            cmsGoodsEntity.setEccnNo(avnetCsvData.getEccn());
            //判断价格阶梯是否为空，为空不计算最小起订量
//            if (!StringUtils.isEmpty(avnetCsvData.getStepPrice())) {
//                String[] stepPrice = avnetCsvData.getStepPrice().split("\\|");
//                //获取最低档价格
//                String s = stepPrice[0];
//                String[] split1 = s.split(":");
//                if (split1.length == 2) {
//                    BigDecimal qtyBreak = new BigDecimal(split1[0]);
//                    BigDecimal priceBreak = new BigDecimal(split1[1]);
//                    //计算最小起订量时最低阶梯价格
//                    BigDecimal count = BigDecimal.valueOf(cmsGoodsEntity.getMinimumOrder()).multiply(priceBreak);
//                    // 判断最小起订量时最低阶梯价格是否低于MinMoney，低于MinMoney时要修改最小起订量使金额大于等于MinMoney
//                    if (count.compareTo(avnetContantPojo.getMinMoney()) < 0) {
//                        //排除错误金额
//                        if (priceBreak.compareTo(BigDecimal.ZERO) != 0) {
//                            // 向上取整
//                            long moq = avnetContantPojo.getMinMoney().divide(priceBreak, 0, BigDecimal.ROUND_UP).longValue();
//                            cmsGoodsEntity.setMinimumOrder(Math.toIntExact(moq));
//                        }
//                    }
//                }
//            }
            cmsGoodsEntity.setSource(SourceEnum.DATA_PACKAGE.getCode());
            //供应商名称
            cmsGoodsEntity.setSplName(avnetContantPojo.getUserName());
            //品牌名称
            cmsGoodsEntity.setBrandName(manufacturer);

            // 费率
            JSONObject data = RateCalculateUtil.getRate(cmsGoodsEntity.getBrandName(), cmsGoodsEntity.getModel());
            // 是否需要3c认证
            String ccc = (String) data.get(RateCalculateConstants.CCC_FIELD);
            cmsGoodsEntity.setCcc(Integer.valueOf(ccc));

            // 是否禁运
            String embargo = (String) data.get(RateCalculateConstants.EMBARGO_FIELD);
            cmsGoodsEntity.setEmbargo(Integer.valueOf(embargo));

            //设置eccn
            if (com.innovation.ic.b1b.framework.util.StringUtils.isEmpty(cmsGoodsEntity.getEccnNo())) {
                String eccn = (String) data.get(RateCalculateConstants.ECCN_FIELD);
                cmsGoodsEntity.setEccnNo(eccn);
            }

            // 费率信息
            JSONObject jsonObject = (JSONObject) data.get(RateCalculateConstants.RATE);
            cmsGoodsEntity.setRates(jsonObject);
            if (jsonObject != null && !jsonObject.isEmpty()) {
                // 关税率
                String tariffsRate = jsonObject.getString(RateCalculateConstants.TARIFFRATE_FIELD);
                cmsGoodsEntity.setTariffsRate(Double.valueOf(tariffsRate));

                // 商检费
                String ciqPrice = jsonObject.getString(RateCalculateConstants.CIQPRICE_FIELD);
                cmsGoodsEntity.setInspectionChargesFee(Double.valueOf(ciqPrice));
            }
            // 参数
            Map<String, String> paramMap = new LinkedHashMap<>();
            paramMap.put("RoHS_Non_Compliant", avnetCsvData.getPrRoHSNonCompliantice());
            paramMap.put("NCNR", avnetCsvData.getNcnr());
            paramMap.put("Obsolete_Part", avnetCsvData.getObsoletePart());
            paramMap.put("ECCN", avnetCsvData.getEccn());
            paramMap.put("Schedule_B", avnetCsvData.getScheduleB());
            paramMap.put("Unspsc", avnetCsvData.getUnspsc());
            paramMap.put("Unspsc_Version", avnetCsvData.getUnspscVersion());
            //禁运
            String embargos = "3A001;3A002;5A001;5A002";
            String[] split = embargos.split(";");
            paramMap.put("禁运", null);
            if (avnetCsvData.getEccn() != null) {
                for (String s : split) {
                    if (avnetCsvData.getEccn().startsWith(s)) {
                        paramMap.put("禁运", avnetCsvData.getEccn());
                        // 是否禁运 优先级最高
                        cmsGoodsEntity.setEmbargo(1);
                        break;
                    }
                }
            }
            cmsGoodsEntity.setParams(JSON.toJSONString(paramMap));
            //cmsGoodsEntity.setTariffsRate(0);
            //cmsGoodsEntity.setInspectionChargesFee(BigDecimal.ZERO);
            //特殊算法
            String sign = getSign(paramMap, avnetContantPojo, avnetCsvData);
            //sign重复 需要在后面拼接自增
            String signEnd = "";
            if (map.containsKey(sign)) {
                return null;
            } else {
                map.put(sign, 0);
            }
            //  max.incrementAndGet();
            String incodes = EasyExcelUtils.getIncodes(sign + signEnd, avnetContantPojo.getUserId(), cmsGoodsEntity.getSupplier());
            cmsGoodsEntity.setIncodes(incodes);
            cmsGoodsEntity.setGoodid(incodes);
            // 阶梯价格信息
            List<CmsPricesEntity> list = cmsPrices(avnetCsvData, cmsGoodsEntity);
            cmsGoodsPojo.setCmsPricesEntityList(list);

            //设置最小起订量
            cmsGoodsEntity.setMinimumOrder(MoqUtil.getMoq(cmsGoodsEntity.getMinimumOrder(), cmsGoodsEntity.getIncremental(), avnetContantPojo.getMinMoney(), list));

            //商品信息
            cmsGoodsPojo.setCmsGoodsEntity(cmsGoodsEntity);
            // 商品副表信息
            cmsGoodsPojo.setCmsGoodsInformationEntity(cmsGoodsInformation(cmsGoodsEntity));
            paramMap.clear();
            return cmsGoodsPojo;
        } catch (Exception e) {
            log.warn("解析异常,放弃当前这条数据,avnetCsvData:{}", JSON.toJSON(avnetCsvData));
            retryOperateLogHelper.add(avnetCsvData, avnetCsvData.getSupplierId(), e.getMessage());
            return null;
        }
    }

    /**
     * 获取sign
     *
     * @param paramMap
     * @return
     */
    private String getSign(Map<String, String> paramMap, AvnetContantPojo avnetContantPojo, AvnetCsvData avnetCsvData) {
        paramMap.put("供应商", avnetContantPojo.getUserName());
        paramMap.put("制造商", avnetCsvData.getManufacturer());
        paramMap.put("批次", "");
        paramMap.put("封装", "");
        paramMap.put("包装", avnetCsvData.getPackage1());
        paramMap.put("库房信息", avnetContantPojo.getDepot());
        paramMap.put("货期", avnetContantPojo.getDelivery());

        StringBuilder builder = new StringBuilder();
        //型号
        builder.append(avnetCsvData.getManufacturerPart());
        if (null != avnetCsvData.getMaAlternatePartnuf()) {
            builder.append(avnetCsvData.getMaAlternatePartnuf());
        }
        for (String key : paramMap.keySet()) {
            builder.append(key).append("\t").append(paramMap.get(key)).append("\n");
        }
        //供应商id
        builder.append(avnetContantPojo.getUserId());
        String sign = encrypt32(builder.toString());
        return sign;
    }

    /**
     * 保存阶梯价格
     *
     * @param avnetCsvData
     * @param cmsGoodsEntity
     */
    private List<CmsPricesEntity> cmsPrices(AvnetCsvData avnetCsvData, CmsGoodsEntity cmsGoodsEntity) {
        if (StringUtils.isEmpty(avnetCsvData.getStepPrice())) {
            return null;
        }
        String[] stepPrice = avnetCsvData.getStepPrice().split("\\|");
        List<CmsPricesEntity> list = new ArrayList<>(stepPrice.length);
        if (stepPrice.length == 1) {
            String[] split = stepPrice[0].split(":");
            //排除错误数据
            if (split.length == 1) {
                return null;
            }
            //判断如果只有一个阶梯价格且价格为零不做处理
            if (BigDecimal.valueOf(Double.valueOf(split[1])).compareTo(BigDecimal.ZERO) == 0) {
                return null;
            }
            //判断当前就一个阶梯价格,数量从1到最大都是一个价格
            CmsPricesEntity cmsPricesEntity = cmsPriceData(Long.valueOf(split[0]), 0L, split[1], CurrencyEnum.DOLLAR.getCode(), 1, cmsGoodsEntity.getGoodid());
            list.add(cmsPricesEntity);
            //更新cmsGoodsEntity prices字段
            CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
            BeanUtils.copyProperties(cmsPricesEntity, cmsPricesPojo);
            List<CmsPricesPojo> prices = new ArrayList<>();
            prices.add(cmsPricesPojo);
            JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
            cmsGoodsEntity.setPrices(jsonArray.toJSONString());
            /*Long aLong = Long.valueOf(split[0]);
            if (aLong == 0) {
                aLong = 1L;
            }
            //美元需要转成人民币
            Double RMBPrice;
            Object o = exchangeRates.get(CachePrefixEnum.EXCHANGE_RATES.getKey());
            if (o != null) {
                RMBPrice = (BigDecimal.valueOf(Double.valueOf(split[1])).multiply((BigDecimal) o)).doubleValue();
            } else {
                RMBPrice = Double.valueOf(Double.valueOf(split[1]));
            }
            cmsGoodsEntity.setUnitPrice(Double.parseDouble(MathUtil.divide(RMBPrice, aLong)));*/
            return list;
        }

        for (int i = 0; i < stepPrice.length; i++) {
            String[] split = stepPrice[i].split(":");
            //排除错误数据
            if (split.length == 1) {
                continue;
            }
            //排除价格为零的阶梯
            if (BigDecimal.valueOf(Double.valueOf(split[1])).compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            /* //获取第一个阶梯价格
            if (i == 0) {
                Long aLong = Long.valueOf(split[0]);
                if (aLong == 0) {
                    aLong = 1L;
                }
               //美元需要转成人民币
                Double RMBPrice;
                Object o = exchangeRates.get(CachePrefixEnum.EXCHANGE_RATES.getKey());
                if (o != null) {
                    RMBPrice = (BigDecimal.valueOf(Double.valueOf(split[1])).multiply((BigDecimal) o)).doubleValue();
                } else {
                    RMBPrice = Double.valueOf(split[1]);
                }
                cmsGoodsEntity.setUnitPrice(Double.parseDouble(MathUtil.divide(RMBPrice, aLong)));
            }*/
            //最后阶梯价格是当前到最大
            if (i == stepPrice.length - 1) {
                CmsPricesEntity cmsPricesEntity = cmsPriceData(Long.valueOf(split[0]), 0L, split[1], CurrencyEnum.DOLLAR.getCode(), i + 1, cmsGoodsEntity.getGoodid());
                list.add(cmsPricesEntity);
            } else {
                String[] split1 = stepPrice[i + 1].split(":");
                //计算当前阶梯价格
                CmsPricesEntity cmsPricesEntity = cmsPriceData(Long.valueOf(split[0]), Long.valueOf(split1[0]) - 1, split[1], CurrencyEnum.DOLLAR.getCode(), i + 1, cmsGoodsEntity.getGoodid());
                list.add(cmsPricesEntity);
            }
        }
        List<CmsPricesPojo> prices = new ArrayList<>();
        for (CmsPricesEntity cmsPricesEntity : list) {
            CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
            BeanUtils.copyProperties(cmsPricesEntity, cmsPricesPojo);
            prices.add(cmsPricesPojo);
        }

        // 更新阶梯价格配置到商品表
        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
        cmsGoodsEntity.setPrices(jsonArray.toJSONString());
        return list;
    }

    /**
     * 保存阶梯价格数据
     *
     * @param min      最小值
     * @param max      最大值
     * @param price    进价
     * @param currency 货币1人民币2美元
     * @param position 位置
     */
    private CmsPricesEntity cmsPriceData(Long min, Long max, String price, Integer currency, Integer position, String goodid) {
        CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
        cmsPricesEntity.setGoodid(goodid);
        cmsPricesEntity.setLadderid(0);
        cmsPricesEntity.setMin(min);
        cmsPricesEntity.setMax(max == null ? 0L : max);
        if (!Strings.isEmpty(price)) {
            cmsPricesEntity.setPrice(PriceUtil.handlePrice(price));
        } else {
            cmsPricesEntity.setPrice(PriceUtil.handlePrice("0"));
        }
        cmsPricesEntity.setCurrency(currency);
        cmsPricesEntity.setPosition(position);
        cmsPricesEntity.setPublish(1);
        cmsPricesEntity.setCreatedAt(new Date(System.currentTimeMillis()));
        return cmsPricesEntity;
    }

    /**
     * 保存商品表-副表信息
     *
     * @param cmsGoodsEntity 商品表
     */
    private CmsGoodsInformationEntity cmsGoodsInformation(CmsGoodsEntity cmsGoodsEntity) {
        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setEcnn(1);
        cmsGoodsInformationEntity.setGoodid(cmsGoodsEntity.getGoodid());
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setContent("");
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setEcnnnumber(cmsGoodsEntity.getEccnNo());
        cmsGoodsInformationEntity.setTariffs(0);
        cmsGoodsInformationEntity.setTariffsrate(0);
        if (cmsGoodsEntity.getInspectionChargesFee() == null) {
            cmsGoodsInformationEntity.setInspectionCharges(1);
            cmsGoodsInformationEntity.setInspectionChargesFee(0.0);
        } else {
            cmsGoodsInformationEntity.setInspectionCharges(cmsGoodsEntity.getInspectionChargesFee() > 0 ? 1 : 0);
            cmsGoodsInformationEntity.setInspectionChargesFee(cmsGoodsEntity.getInspectionChargesFee());
        }
        cmsGoodsInformationEntity.setParams(cmsGoodsEntity.getParams().toString());
        cmsGoodsInformationEntity.setCreatedAt(new Date());
        return cmsGoodsInformationEntity;
    }

    /**
     * 16位MD5加密算法
     *
     * @param encryptStr
     * @return
     */
    private static String encrypt32(String encryptStr) {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] md5Bytes = md5.digest(encryptStr.getBytes());
            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16) {
                    hexValue.append("0");
                }
                hexValue.append(Integer.toHexString(val));
            }
            encryptStr = hexValue.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return encryptStr.toUpperCase().substring(8, 24);
    }

    /**
     * avnet.csv
     *
     * @return
     */
    public AvnetContantPojo getAvnet() {
        AvnetContantPojo avnetContantPojo = new AvnetContantPojo();
        avnetContantPojo.setUserId("US000000016");
        avnetContantPojo.setUserName("Avnet");
        avnetContantPojo.setMinMoney(BigDecimal.valueOf(30));
        avnetContantPojo.setDepot("美国");
        avnetContantPojo.setDelivery("15-25个工作日");
        avnetContantPojo.setHkDelivery("10-13个工作日");
        avnetContantPojo.setType("Original");
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        list.add(new BigDecimal(1));
        list.add(new BigDecimal(1));
        list.add(new BigDecimal(1));
        list.add(new BigDecimal(1));
        avnetContantPojo.setPrices(list);
        return avnetContantPojo;
    }

    /**
     * avnet_asia.csv
     *
     * @return
     */
    public AvnetContantPojo getAvnetAsia() {
        AvnetContantPojo avnetContantPojo = new AvnetContantPojo();
        avnetContantPojo.setUserId("US000000016");
        avnetContantPojo.setUserName("Avnet");
        avnetContantPojo.setMinMoney(BigDecimal.valueOf(50));
        avnetContantPojo.setDepot("亚洲");
        avnetContantPojo.setDelivery("12-15个工作日");
        avnetContantPojo.setHkDelivery("10-13个工作日");
        avnetContantPojo.setType("Original");
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        list.add(new BigDecimal(1));
        avnetContantPojo.setPrices(list);
        return avnetContantPojo;
    }

    /**
     * avnet_emea.csv
     *
     * @return
     */
    public AvnetContantPojo getAvnetEmea() {
        AvnetContantPojo avnetContantPojo = new AvnetContantPojo();
        avnetContantPojo.setUserId("US000000016");
        avnetContantPojo.setUserName("Avnet");
        avnetContantPojo.setMinMoney(BigDecimal.valueOf(100));
        avnetContantPojo.setDepot("欧洲");
        avnetContantPojo.setDelivery("12-15个工作日");

        avnetContantPojo.setHkDelivery("10-13个工作日");
        avnetContantPojo.setType("Original");
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        list.add(new BigDecimal(1));
        avnetContantPojo.setPrices(list);
        return avnetContantPojo;
    }

    /**
     * avnet常量 不同文件值不同,avnet.csv avnet_asia.csv avnet_emea.csv
     */
    @Data
    public static class AvnetContantPojo {
        private String userId;

        private String userName;

        private BigDecimal minMoney;

        private String depot;

        private String delivery;
        //香港货期算法有变动，是按国内的货期减2天 2023-05-18 赵耀要求更改
        private String hkDelivery;

        private String type;

        private List<BigDecimal> prices;
    }
}
