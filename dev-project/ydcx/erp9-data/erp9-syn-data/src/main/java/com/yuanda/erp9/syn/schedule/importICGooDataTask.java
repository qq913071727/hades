package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.config.FileParamConfig;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.ICGooDataImportService;
import com.yuanda.erp9.syn.util.FileUtils;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;

@Component
public class importICGooDataTask {
    private static final Logger log = LoggerFactory.getLogger(importICGooDataTask.class);

    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private ICGooDataImportService icGooDataImportService;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Resource
    private CmsSuppliersTempService cmsSuppliersTempService;

    /** 供应商名称 */
    private static final String SUPPLIER = "Cycle-chips Components Inc.";

    /** 供应商id */
    private static final String SUPPLIER_ID = "US000008311";

    /**
     * 导入ICGoo数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importICGooDataJob")
    @EnableSchedule
    public void importICGooData() {
        try {
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.info("导入ICGoo数据定时任务开始");
            log.info("1. 通过下载地址获取文件");
            // 1. 通过下载地址获取文件
            String filePath = FileUtils.downLoadFromUrl(DownloadUrlConstants.ICGOO_DOWN_FILE_URL, DownloadUrlConstants.ICGOO_DOWN_FILE_NAME, fileParamConfig.getDownloadPath());
            if(!Strings.isNullOrEmpty(filePath)){
                log.info("ICGoo文件下载完成,文件保存路径:[{}]", filePath);

                // 更新系统主体更新日志记录表数据
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setSupplier(supplierId);
                entity.setCreateAt(new Date(System.currentTimeMillis()));
                entity.setSystemTopicType(SubjectSupplierEnum.WANGSQ.getType());
                entity.setSystemTopic(SubjectSupplierEnum.WANGSQ.getSupplier());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                log.info("插入系统主体更新日志记录表数据完成");

                // 导入ICGoo文件数据
                log.info("2. 解析文件数据进行入库操作");
                icGooDataImportService.importCmsGoodsPojo(topicUpdateLogId, filePath);
            }
        } catch (Exception e) {
            log.info("处理ICGoo供应商文件出现问题,原因:", e);
            //throw new TargetServerException(e.getMessage(), supplierId, SubjectSupplierEnum.WANGSQ.getSubject());
        }
    }
}