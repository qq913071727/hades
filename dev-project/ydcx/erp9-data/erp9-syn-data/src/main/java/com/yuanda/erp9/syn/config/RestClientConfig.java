package com.yuanda.erp9.syn.config;

import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @ClassName RestClientConfig
 * @Description
 * @Date 2022/11/25
 * @Author myq
 */
@Configuration
@ConfigurationProperties(prefix = "spring.elasticsearch.rest")
@Getter
@Setter
public class RestClientConfig {

    private String host;
    private int port;
    private String index;
    private long connectionTimeout;
    private long readTimeout;

    /**
     * 创建多例对象
     */
    @Bean
    @Scope("prototype")
    public RestHighLevelClient restHighLevelClient() {
        //核心,配置超时时间,添加configCallback,之后创建连接时会将超时设置到socket上
        RestClientBuilder.RequestConfigCallback requestConfigCallback =
                requestConfigBuilder -> requestConfigBuilder
                        .setSocketTimeout(60000)
                        .setConnectionRequestTimeout(20000);
        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port)).setRequestConfigCallback(requestConfigCallback);

        builder.setMaxRetryTimeoutMillis(60 * 1000);
        return new RestHighLevelClient(builder);
    }

}
