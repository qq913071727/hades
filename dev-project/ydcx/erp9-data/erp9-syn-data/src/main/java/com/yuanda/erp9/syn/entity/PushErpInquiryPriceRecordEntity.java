package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 推送erp询价价格记录
 * @Date 2023/4/4 16:45
 **/
@TableName("push_erp_inquiry_price_record")
@Setter
@Getter
public class PushErpInquiryPriceRecordEntity implements Serializable {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "状态 1:待推送，2:已推送")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String  note;

    @ApiModelProperty(value = "询价单号")
    private String  inquiryNumber;

    @ApiModelProperty(value = "创建时间")
    private Date  createTime;

    @ApiModelProperty(value = "更新时间")
    private Date  updateTime;
}
