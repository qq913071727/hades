package com.yuanda.erp9.syn.mapper.erp9_pvestandard;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.erp9_pvestandard.PetNamesEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PetNamesMapper extends BaseMapper<PetNamesEntity> {
    /**
     *  根据品牌 转换 标准库名称
     *
     * @param brand
     * @return
     */
    List<String> conversionStandardNameByBrand(@Param("brand") String brand);
}