package com.yuanda.erp9.syn.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   供应商起订金额枚举
 * @author linuo
 * @time   2022年11月29日13:46:11
 */
public enum SuppliersMinimumAmountEnum {
    B3("b3",100, "USD"),
    B9("b9",100, "USD"),
    B28("b28",100, "USD"),
    B27("b27",100, "USD"),
    B37("b37",100, "USD"),

    B47("b47",100, "USD"),

    B2("b2",100, "USD"),
    B11("b11",100, "USD"),
    B13("b13",100, "USD"),

    A16("a16",50, "RMB"),
    B17("b17",50, "RMB"),
    A21("a21",50, "RMB"),
    A23("a23",50, "RMB"),
    A24("a24",50, "RMB"),
    A25("a25",50, "RMB"),
    A26("a26",50, "RMB"),
    A27("a27",50, "RMB"),
    A29("a29",50, "RMB"),
    A30("a30",50, "RMB"),
    A31("a31",50, "RMB"),
    A34("a34",50, "RMB"),
    A35("a35",50, "RMB"),
    A36("a36",50, "RMB"),
    A37("a37",50, "RMB"),
    A2("a2",50, "RMB"),
    A5("a5",50, "RMB"),
    A7("a7",50, "RMB"),
    A8("a8",50, "RMB"),
    A9("a9",50, "RMB"),
    A10("a10",50, "RMB"),
    A12("a12",50, "RMB"),
    A11("a11",50, "RMB"),
    A15("a15",50, "RMB"),
    A38("a38",50, "RMB"),
    A39("a39",50, "RMB"),
    A40("a40",50, "RMB"),
    A41("a41",50, "RMB"),
    A42("a42",50, "RMB"),
    A43("a43",50, "RMB"),
    A44("a44",50, "RMB"),
    A45("a45",50, "RMB"),
    A47("a47",50, "RMB"),
    A48("a48",50, "RMB"),
    A49("a49",50, "RMB"),

    A19("a19",0, "RMB"),
    A1("a1",0, "RMB"),
    A4("a4",0, "RMB"),
    A3("a3",0, "RMB"),
    A6("a6",0, "RMB"),
    A13("a13",0, "RMB"),
    A14("a14",0, "RMB"),
    A17("a17",0, "RMB"),
    A18("a18",0, "RMB"),
    A20("a20",0, "RMB"),
    B18("b18",0, "RMB"),
    A22("a22",0, "RMB"),
    A32("a32",0, "RMB"),
    A33("a33",0, "RMB"),
    B29("b29",0, "RMB"),
    A46("a46",0, "RMB"),
    B20("b20",0, "RMB"),
    C1("c1",0, "RMB"),
    A52("a52",0, "RMB"),
    A51("a51",0, "RMB"),
    A50("a50",0, "RMB"),
    A53("a53",0, "RMB"),
    A54("a54",0, "RMB"),
    A55("a55",0, "RMB"),
    A56("a56",0, "RMB"),
    A57("a57",0, "RMB");

    /** 供应商标识ID */
    private String id;

    /** 起订金额 */
    private Integer money;

    /** 币种 */
    private String currency;

    SuppliersMinimumAmountEnum(String id, Integer money, String currency) {
        this.id = id;
        this.money = money;
        this.currency = currency;
    }

    public static SuppliersMinimumAmountEnum of(String id) {
        for (SuppliersMinimumAmountEnum c : SuppliersMinimumAmountEnum.values()) {
            if (c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (SuppliersMinimumAmountEnum e : SuppliersMinimumAmountEnum.values()) {
            Map param = new HashMap();
            param.put("id", e.id);
            param.put("money", e.money);
            param.put("currency", e.currency);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}