package com.yuanda.erp9.syn.service.erp9;

/**
 * @ClassName CoreStaffStockExcelService
 * @Description
 * @Date 2022/11/24
 * @Author myq
 */
public interface CoreStaffStockExcelService {


    /**
     * @Description: 解析excel
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2415:55
     */
    void parse(String filePath, Long pk);
}
