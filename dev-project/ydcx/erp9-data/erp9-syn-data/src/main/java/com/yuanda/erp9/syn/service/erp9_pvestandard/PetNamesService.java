package com.yuanda.erp9.syn.service.erp9_pvestandard;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌管理
 * @Date 2023/4/12 14:45
 **/
public interface PetNamesService {
    /**
     * 根据品牌 转换 标准库名称
     * @param brand
     * @return
     */
    List<String> conversionStandardNameByBrand(String brand);
}
