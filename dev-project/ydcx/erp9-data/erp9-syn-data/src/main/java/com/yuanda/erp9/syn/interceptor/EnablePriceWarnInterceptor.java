package com.yuanda.erp9.syn.interceptor;

import com.yuanda.erp9.syn.annotation.EnablePriceWarn;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Author myq
 * @Date 2023/4/13 14:20
 * @Version 1.0
 * @Description
 */
@Aspect
@Component
@Slf4j
public class EnablePriceWarnInterceptor {

    @Value("${standard.price.enable}")
    private boolean priceFlag;

    @Pointcut("@annotation(com.yuanda.erp9.syn.annotation.EnablePriceWarn)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        // 获取方法签名
        MethodSignature ms = (MethodSignature) point.getSignature();
        Method method = ms.getMethod();
        // 获取注解
        EnablePriceWarn enablePriceWarn = (EnablePriceWarn) method.getAnnotation(EnablePriceWarn.class);
        if(enablePriceWarn != null){
            if(priceFlag){
                log.warn("#################价格预警任务已关闭####################");
                return null;
            }
        }
        return point.proceed();
    }
}
