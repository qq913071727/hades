package com.yuanda.erp9.syn.poi;

import lombok.Data;
import org.elasticsearch.common.Strings;

/**
 * @desc   Aipco数据解析
 * @author linuo
 * @time   2022年11月28日11:02:27
 */
@Data
public class AipcoCsvData {
    /** 属性 */
    private Integer row;

    /** 制造商 */
    private String manufacturer;

    /** 型号 */
    private String mpn;

    /** 地区 */
    private String region;

    /** 库存 */
    private Integer inventory;

    /** 最小起订量 */
    private Long moq;

    /** 阶梯价格起订量 */
    private Long qtyBreak1;

    /** 阶梯价格 */
    private String priceBreak1;

    /** 阶梯价格起订量 */
    private Long qtyBreak2;

    /** 阶梯价格 */
    private String priceBreak2;

    /** 阶梯价格起订量 */
    private Long qtyBreak3;

    /** 阶梯价格 */
    private String priceBreak3;

    /** 阶梯价格起订量 */
    private Long qtyBreak4;

    /** 阶梯价格 */
    private String priceBreak4;

    /** 阶梯价格起订量 */
    private Long qtyBreak5;

    /** 阶梯价格 */
    private String priceBreak5;

    /** 阶梯价格起订量 */
    private Long qtyBreak6;

    /** 阶梯价格 */
    private String priceBreak6;

    /** 属性 */
    private String rohs;

    public static AipcoCsvData toAipcoCsvDataFormat(String[] data, Integer row){
        AipcoCsvData aipcoCsvData = new AipcoCsvData();
        aipcoCsvData.setRow(row + 1);
        aipcoCsvData.setManufacturer(data[1]);
        aipcoCsvData.setMpn(data[2]);
        aipcoCsvData.setRegion(data[3]);
        aipcoCsvData.setInventory((!Strings.isNullOrEmpty(data[4])) ? Integer.valueOf(data[4]) : null);
        aipcoCsvData.setMoq((!Strings.isNullOrEmpty(data[5])) ? Long.valueOf(data[5]) : null);
        aipcoCsvData.setQtyBreak1((!Strings.isNullOrEmpty(data[8])) ? Long.valueOf(data[8]) : null);
        aipcoCsvData.setPriceBreak1(data[9]);
        aipcoCsvData.setQtyBreak2((!Strings.isNullOrEmpty(data[10])) ? Long.valueOf(data[10]) : null);
        aipcoCsvData.setPriceBreak2(data[11]);
        aipcoCsvData.setQtyBreak3((!Strings.isNullOrEmpty(data[12])) ? Long.valueOf(data[12]) : null);
        aipcoCsvData.setPriceBreak3(data[13]);
        aipcoCsvData.setQtyBreak4((!Strings.isNullOrEmpty(data[14])) ? Long.valueOf(data[14]) : null);
        aipcoCsvData.setPriceBreak4(data[15]);
        aipcoCsvData.setQtyBreak5((!Strings.isNullOrEmpty(data[16])) ? Long.valueOf(data[16]) : null);
        aipcoCsvData.setPriceBreak5(data[17]);
        aipcoCsvData.setQtyBreak6((!Strings.isNullOrEmpty(data[18])) ? Long.valueOf(data[18]) : null);
        aipcoCsvData.setPriceBreak6(data[19]);
        aipcoCsvData.setRohs(data[20]);
        return aipcoCsvData;
    }
}