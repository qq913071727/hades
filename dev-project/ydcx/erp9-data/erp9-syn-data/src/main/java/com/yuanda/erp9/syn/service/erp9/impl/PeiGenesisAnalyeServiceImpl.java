package com.yuanda.erp9.syn.service.erp9.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.yuanda.erp9.syn.contant.RateCalculateConstants;
import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsInformationEntity;
import com.yuanda.erp9.syn.entity.CmsPricesEntity;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateSumLogEntity;
import com.yuanda.erp9.syn.enums.CachePrefixEnum;
import com.yuanda.erp9.syn.enums.SourceEnum;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.execule.thread.Master;
import com.yuanda.erp9.syn.helper.RetryOperateLogHelper;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateLogMapper;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateSumLogMapper;
import com.yuanda.erp9.syn.poi.PeiGenesisCsvData;
import com.yuanda.erp9.syn.poi.PeiGenesisPriceCsvData;
import com.yuanda.erp9.syn.pojo.CmsGoodsPojo;
import com.yuanda.erp9.syn.pojo.CmsPricesPojo;
import com.yuanda.erp9.syn.service.erp9.PeiGenesisAnalyeService;
import com.yuanda.erp9.syn.util.CacheUtil;
import com.yuanda.erp9.syn.util.CsvReadUtils;
import com.yuanda.erp9.syn.util.DateUtils;
import com.yuanda.erp9.syn.util.EasyExcelUtils;
import com.yuanda.erp9.syn.util.PriceUtil;
import com.yuanda.erp9.syn.util.RateCalculateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
public class PeiGenesisAnalyeServiceImpl implements PeiGenesisAnalyeService {

    protected static CacheUtil.ObjectCache exchangeRates = CacheUtil.objectInit(CachePrefixEnum.EXCHANGE_RATES.getKey());

    @Resource
    private RetryOperateLogHelper retryOperateLogHelper;

    @Resource
    private CmsTopicUpdateSumLogMapper cmsTopicUpdateSumLogMapper;

    private static List<String> filePathSort = new ArrayList<>();


    private final Long zero = Long.valueOf("0");

    static {
        //固定顺序文件读取,不然业务会出错
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_CN_RMB.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_CN_RMB_standard.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_CN_USD.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_CN_USD_standard.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_PG_USD.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_UK_USD.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_UK_USD_standard.csv");
        filePathSort.add("/opt/erp9-syn-data/download/peiinventoryprice/pei_prices_US_USD_standard.csv");
    }





    private String rmb = "RMB";
    private String usd = "USD";
    private String us = "US";

    private static CacheUtil.Cache brandCache = CacheUtil.init(CachePrefixEnum.BRAND.getKey());


    private static CacheUtil.Cache suppliersCache = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());


    @Resource
    private CmsTopicUpdateLogMapper cmsTopicUpdateLogMapper;

    @Override
    public void analysisPeiGenesis(Map<String,String> paramMap) {
        String filePath = paramMap.get("filePath");
        List<String[]> paramsList = CsvReadUtils.parseCSV(filePath);
        if (CollectionUtils.isEmpty(paramsList)) {
            return;
        }
        String suppliers = "PEI-Genesis";

        //供应商id
        Integer suppliersId = suppliersCache.get(suppliers);

        Master<CmsGoodsPojo> objectMaster = new Master<>(suppliersId, null, 1);
        try {
            CmsTopicUpdateLogEntity cmsTopicUpdateLogEntity = new CmsTopicUpdateLogEntity();
            cmsTopicUpdateLogEntity.setSupplier(suppliersId);
            cmsTopicUpdateLogEntity.setUpdated(1);
            cmsTopicUpdateLogEntity.setSystemTopicType(3);
            cmsTopicUpdateLogEntity.setSystemTopic(SubjectSupplierEnum.PEI_INVENTORY.getSubject());
            cmsTopicUpdateLogEntity.setCreateAt(new Date());
            cmsTopicUpdateLogMapper.insert(cmsTopicUpdateLogEntity);
            //转换阶梯价格
            Map<String, Map<String, PeiGenesisPriceCsvData>> priceStringMap = convertPricePath();
            int index = 0;
            for (String[] dataValue : paramsList) {
                try {
                    PeiGenesisCsvData peiGenesisCsvData = this.convertPeiGenesisCsvData(dataValue);
                    //封装pojo对象
                    CmsGoodsPojo cmsGoodsPojo = this.convertCmsGoodsPojo(peiGenesisCsvData, ++index, priceStringMap, suppliersId);
                    objectMaster.add(cmsGoodsPojo);
                } catch (Exception e) {
                    log.warn("analysisPeiGenesis 发生错误 {} ,错误数据 {}  ", index + 1, JSONObject.toJSONString(dataValue));
                    retryOperateLogHelper.add(JSONObject.toJSONString(dataValue), suppliersId, e.toString());
                }
            }
        } catch (Exception e) {
            log.error("解析文件PeiGenesis 错误 ", e);
        }finally {
            objectMaster.isBegin = false;
            //记录总数
            insertCmsTopicUpdateSumLogEntity(Long.valueOf(paramMap.get("topicUpdateLogId")).intValue(),paramsList.size());
        }
    }



    private Map<String, Map<String, PeiGenesisPriceCsvData>> convertPricePath() {
        Map<String, Map<String, PeiGenesisPriceCsvData>> result = new HashMap<>();
        log.info("倍捷开始转换阶梯价格");
        for (String filePath : filePathSort) {
            log.info("倍捷阶梯价格文件 {} ", filePath);
            //根据文件名,解析文件map
            String name = filePath.split("/")[5];
            String prefix = name.split("_")[2];
            Map<String, PeiGenesisPriceCsvData> peiGenesisPriceCsvDataMap = result.get(prefix);
            if (peiGenesisPriceCsvDataMap == null) {
                peiGenesisPriceCsvDataMap = new HashMap<>();
            }
            List<String[]> priceCsv = CsvReadUtils.parseCSV(filePath);
            for (String[] priceBean : priceCsv) {
                PeiGenesisPriceCsvData peiGenesisPriceCsvData = this.bindPeiGenesisPriceCsvData(priceBean);
                String currency = peiGenesisPriceCsvData.getCurrency();
                if (!rmb.equals(currency) && !usd.equals(currency)) {
                    continue;
                }
                String key = peiGenesisPriceCsvData.getPeiProductcode();
                peiGenesisPriceCsvDataMap.put(key, peiGenesisPriceCsvData);
            }
            result.put(prefix, peiGenesisPriceCsvDataMap);
        }
        return result;
    }

    private PeiGenesisPriceCsvData  bindPeiGenesisPriceCsvData(String[] priceStrings) {
        PeiGenesisPriceCsvData peiGenesisPriceCsvData = new PeiGenesisPriceCsvData();
        peiGenesisPriceCsvData.setManufacturer(priceStrings[0]);
        peiGenesisPriceCsvData.setManufacturerPartNumber(priceStrings[1]);
        peiGenesisPriceCsvData.setDescription(priceStrings[2]);
        peiGenesisPriceCsvData.setQuantity1(priceStrings[3]);
        peiGenesisPriceCsvData.setCost1(priceStrings[4]);
        peiGenesisPriceCsvData.setQuantity2(priceStrings[5]);
        peiGenesisPriceCsvData.setCost2(priceStrings[6]);
        peiGenesisPriceCsvData.setQuantity3(priceStrings[7]);
        peiGenesisPriceCsvData.setCost3(priceStrings[8]);
        peiGenesisPriceCsvData.setQuantity4(priceStrings[9]);
        peiGenesisPriceCsvData.setCost4(priceStrings[10]);
        peiGenesisPriceCsvData.setQuantity5(priceStrings[11]);
        peiGenesisPriceCsvData.setCost5(priceStrings[12]);
        peiGenesisPriceCsvData.setCurrency(priceStrings[13]);
        peiGenesisPriceCsvData.setUnitOfMeasure(priceStrings[14]);
        peiGenesisPriceCsvData.setMinimumQuantity(priceStrings[15]);
        peiGenesisPriceCsvData.setLeadTime(priceStrings[16]);
        peiGenesisPriceCsvData.setPeiPCat6(priceStrings[17]);
        peiGenesisPriceCsvData.setPeiProductcode(priceStrings[18]);
        peiGenesisPriceCsvData.setCountryOfOrigin(priceStrings[19]);
        peiGenesisPriceCsvData.setCommodityCode(priceStrings[20]);
        peiGenesisPriceCsvData.setSedFlag(priceStrings[21]);
        peiGenesisPriceCsvData.setRoHS(priceStrings[22]);
        peiGenesisPriceCsvData.setExportControlType(priceStrings[23]);
        peiGenesisPriceCsvData.setExportControlClassification(priceStrings[24]);
        return peiGenesisPriceCsvData;
    }


    private CmsGoodsPojo convertCmsGoodsPojo(PeiGenesisCsvData peiGenesisCsvData, Integer count, Map<String, Map<String, PeiGenesisPriceCsvData>> priceStringMap, Integer suppliersId) {
        CmsGoodsPojo cmsGoodsPojo = new CmsGoodsPojo();
        String hisp = "";
        String crmNum = "US000008259";
        String sign = peiGenesisCsvData.getManufacturerPartNumber() + "_" + count;


        //批次号
        String batchNumber = "";
        //型号
        String manufacturerPartNumber = peiGenesisCsvData.getManufacturerPartNumber();
        //品牌
        String manufacturer = peiGenesisCsvData.getManufacturer();
        //供应商名字
        String suppliers = "PEI-Genesis";

        //品牌id
        CmsBrandsTempEntity cmsBrandsTempEntity = new CmsBrandsTempEntity();
        cmsBrandsTempEntity.setName(manufacturer);
        Integer brandId = brandCache.insertAndGet(cmsBrandsTempEntity);


        String incodes = EasyExcelUtils.getIncodes(sign, crmNum, suppliersId);


        CmsGoodsEntity cmsGoodsEntity = new CmsGoodsEntity();
        cmsGoodsEntity.setGoodid(incodes);
        cmsGoodsEntity.setAdminid(0);
        cmsGoodsEntity.setTenantsid(0);
        cmsGoodsEntity.setIncodes(incodes);

        cmsGoodsEntity.setClassifyid(0);
        cmsGoodsEntity.setImg("");
        cmsGoodsEntity.setType(0);

        cmsGoodsEntity.setStatus(1);
        cmsGoodsEntity.setStatusMsg("");


        cmsGoodsEntity.setDataType(1);
        cmsGoodsEntity.setModel(manufacturerPartNumber);
        cmsGoodsEntity.setBrand(brandId);
        cmsGoodsEntity.setSupplier(suppliersId);
        cmsGoodsEntity.setIsSampleApply(0);
        cmsGoodsEntity.setBatch(batchNumber);

        cmsGoodsEntity.setOriginalPrice(BigDecimal.ZERO);
        cmsGoodsEntity.setPurchasePrice(BigDecimal.ZERO);

        //处理阶梯价格
        PeiGenesisPriceCsvData peiGenesisPriceCsvData = null;
        Map<String, PeiGenesisPriceCsvData> locaPeiGenesisPriceCsvDataMap = priceStringMap.get(peiGenesisCsvData.getLocation());
        if (!CollectionUtils.isEmpty(locaPeiGenesisPriceCsvDataMap)) {
            String key = peiGenesisCsvData.getPeiPartNumber();
            peiGenesisPriceCsvData = locaPeiGenesisPriceCsvDataMap.get(key);
            if (peiGenesisPriceCsvData != null) {
                //美元转换人民币
                //原始单价
                BigDecimal unitPrice = new BigDecimal(peiGenesisPriceCsvData.getCost1());
                if (us.equals(peiGenesisCsvData.getLocation())) {
                    Object o = exchangeRates.get(CachePrefixEnum.EXCHANGE_RATES.getKey());
                    if (o != null) {
                        BigDecimal b2 = new BigDecimal(o.toString());
                        //乘法
                        unitPrice = unitPrice.multiply(b2).setScale(4, BigDecimal.ROUND_HALF_UP);
                    }
                }
                BigDecimal quantity1 = new BigDecimal(peiGenesisPriceCsvData.getQuantity1());
                double finalUnitPrice = unitPrice.divide(quantity1, 4, BigDecimal.ROUND_HALF_UP).doubleValue();
                cmsGoodsEntity.setUnitPrice(finalUnitPrice);
            } else {
                cmsGoodsEntity.setUnitPrice(new Double(0));
            }
        }

        Double unitPrice = cmsGoodsEntity.getUnitPrice();
        //如果没有价格为空
        if (unitPrice == null) {
            cmsGoodsEntity.setUnitPrice(Double.valueOf(0));
        }

        String availableStock = peiGenesisCsvData.getAvailableStock();
        if (StringUtils.validateParameter(availableStock)) {
            cmsGoodsEntity.setRepertory(new BigDecimal(availableStock).intValue());
        } else {
            cmsGoodsEntity.setRepertory(0);
        }

        //转换仓库地
        String repertoryArea = "";
        switch (peiGenesisCsvData.getLocation()) {
            case "CN":
                repertoryArea = "珠海";
                break;
            case "UK":
                repertoryArea = "海外";
                break;
            case "US":
                repertoryArea = "海外";
                break;
        }
        cmsGoodsEntity.setRepertoryArea(repertoryArea);

        cmsGoodsEntity.setPublish(1);
        cmsGoodsEntity.setDescribe(peiGenesisCsvData.getDescription());
        cmsGoodsEntity.setFiles("");
        cmsGoodsEntity.setPayType(0);


        String minimum = peiGenesisCsvData.getMinimum();

        cmsGoodsEntity.setMinimumOrder(Long.valueOf(minimum));
        cmsGoodsEntity.setIncremental(Long.valueOf(minimum));
        cmsGoodsEntity.setMpq(Long.valueOf(minimum));

        cmsGoodsEntity.setDomesticDate("10-15工作日");
        cmsGoodsEntity.setHongkongDate("8-13工作日");

        cmsGoodsEntity.setHisp(hisp);

        cmsGoodsEntity.setCcc(1);
        //默认0
        cmsGoodsEntity.setDeliveryArea("0");

        cmsGoodsEntity.setSales(0);
        cmsGoodsEntity.setClick(0);
        cmsGoodsEntity.setVisitor(0);
        //cmsGoodsEntity.setIsToday(1);

        cmsGoodsEntity.setCreatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setUpdatedAt(DateUtils.dateToString(new Date(), DateUtils.simpleDateHms));
        cmsGoodsEntity.setActivityId(0);
        cmsGoodsEntity.setAtlas("");

        //费率
        JSONObject data = RateCalculateUtil.getRate(cmsGoodsEntity.getBrandName(), cmsGoodsEntity.getModel());

        // 费率信息
        JSONObject rateJsonObject = (JSONObject) data.get(RateCalculateConstants.RATE);
        cmsGoodsEntity.setRates(rateJsonObject);

        //关税
        BigDecimal tariffRate = rateJsonObject.getBigDecimal(RateCalculateConstants.TARIFFRATE_FIELD);
        //商检税
        BigDecimal cIQprice = rateJsonObject.getBigDecimal(RateCalculateConstants.CIQPRICE_FIELD);

        //税费计算和存储
        if (tariffRate != null) {
            cmsGoodsEntity.setTariffsRate(tariffRate.doubleValue());
        }
        if (cIQprice != null) {
            cmsGoodsEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }


        Object ccc = data.get(RateCalculateConstants.CCC_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setCcc(Integer.valueOf(ccc.toString()));
        }

        Object embargo = data.get(RateCalculateConstants.EMBARGO_FIELD);
        if (ccc != null) {
            cmsGoodsEntity.setEmbargo(Integer.valueOf(embargo.toString()));
        }
        Object eccn = data.get(RateCalculateConstants.ECCN_FIELD);
        if (eccn != null) {
            cmsGoodsEntity.setEccnNo(eccn.toString());
        }


        cmsGoodsEntity.setSource(SourceEnum.DATA_PACKAGE.getCode());
        cmsGoodsEntity.setSplName(suppliers);
        cmsGoodsEntity.setBrandName(manufacturer);

        //阶梯价格list
        List<CmsPricesEntity> cmsPricesEntityList = new ArrayList<>();
        //阶梯价格Map
        //List<Map<String, String>> pricesMap = new ArrayList<>();
        //计算阶梯价格
        if (peiGenesisPriceCsvData != null) {
            BigDecimal cost1BigDecimal = new BigDecimal(peiGenesisPriceCsvData.getCost1()).stripTrailingZeros();
            BigDecimal quantity1Decimal = new BigDecimal(peiGenesisPriceCsvData.getQuantity1()).stripTrailingZeros();
            if (!(cost1BigDecimal.compareTo(BigDecimal.ZERO) == 0)) {
                CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
                cmsPricesEntity.setLadderid(0);
                cmsPricesEntity.setGoodid(incodes);
                cmsPricesEntity.setMin(quantity1Decimal.longValue());
                Long quantity2 = new BigDecimal(peiGenesisPriceCsvData.getQuantity2()).longValue();

                if (quantity2 - 1 < 0) {
                    cmsPricesEntity.setMax(zero);
                } else {
                    cmsPricesEntity.setMax(quantity2 - 1);
                }
                cmsPricesEntity.setPrice(cost1BigDecimal);
                String location = peiGenesisPriceCsvData.getCurrency();
                if (rmb.equals(location)) {
                    cmsPricesEntity.setCurrency(1);
                    cmsGoodsEntity.setDeliveryArea("0");
                } else {
                    cmsPricesEntity.setCurrency(2);
                    cmsGoodsEntity.setDeliveryArea("0,1");
                }
                cmsPricesEntity.setPosition(1);
                cmsPricesEntity.setPublish(1);
                cmsPricesEntityList.add(cmsPricesEntity);
            }


            BigDecimal cost2BigDecimal = new BigDecimal(peiGenesisPriceCsvData.getCost2()).stripTrailingZeros();
            BigDecimal quantity2Decimal = new BigDecimal(peiGenesisPriceCsvData.getQuantity2()).stripTrailingZeros();


            if (!(cost2BigDecimal.compareTo(BigDecimal.ZERO) == 0)) {
                CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
                cmsPricesEntity.setLadderid(0);
                cmsPricesEntity.setGoodid(incodes);
                cmsPricesEntity.setMin(quantity2Decimal.longValue());

                Long quantity3 = new BigDecimal(peiGenesisPriceCsvData.getQuantity3()).longValue();

                if (quantity3 - 1 < 0) {
                    cmsPricesEntity.setMax(zero);
                } else {
                    cmsPricesEntity.setMax(quantity3 - 1);
                }
                cmsPricesEntity.setPrice(cost2BigDecimal);
                String location = peiGenesisPriceCsvData.getCurrency();
                if (rmb.equals(location)) {
                    cmsPricesEntity.setCurrency(1);
                } else {
                    cmsPricesEntity.setCurrency(2);
                }
                cmsPricesEntity.setPosition(2);
                cmsPricesEntity.setPublish(1);
                cmsPricesEntityList.add(cmsPricesEntity);
            }


            BigDecimal cost3BigDecimal = new BigDecimal(peiGenesisPriceCsvData.getCost3()).stripTrailingZeros();
            BigDecimal quantity3Decimal = new BigDecimal(peiGenesisPriceCsvData.getQuantity3()).stripTrailingZeros();

            if (!(cost3BigDecimal.compareTo(BigDecimal.ZERO) == 0)) {
                CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
                cmsPricesEntity.setLadderid(0);
                cmsPricesEntity.setGoodid(incodes);
                cmsPricesEntity.setMin(quantity3Decimal.longValue());

                Long quantity4 = new BigDecimal(peiGenesisPriceCsvData.getQuantity4()).stripTrailingZeros().longValue();
                if (quantity4 - 1 < 0) {
                    cmsPricesEntity.setMax(zero);
                } else {
                    cmsPricesEntity.setMax(quantity4 - 1);
                }
                cmsPricesEntity.setPrice(cost3BigDecimal);
                String location = peiGenesisPriceCsvData.getCurrency();
                if (rmb.equals(location)) {
                    cmsPricesEntity.setCurrency(1);
                } else {
                    cmsPricesEntity.setCurrency(2);
                }
                cmsPricesEntity.setPosition(3);
                cmsPricesEntity.setPublish(1);
                cmsPricesEntityList.add(cmsPricesEntity);
            }


            BigDecimal cost4BigDecimal = new BigDecimal(peiGenesisPriceCsvData.getCost4()).stripTrailingZeros();
            BigDecimal quantity4Decimal = new BigDecimal(peiGenesisPriceCsvData.getQuantity4()).stripTrailingZeros();

            if (!(cost4BigDecimal.compareTo(BigDecimal.ZERO) == 0)) {
                CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
                cmsPricesEntity.setLadderid(0);
                cmsPricesEntity.setGoodid(incodes);
                cmsPricesEntity.setMin(quantity4Decimal.longValue());

                Long quantity5 = new BigDecimal(peiGenesisPriceCsvData.getQuantity5()).stripTrailingZeros().longValue();
                if (quantity5 - 1 < 0) {
                    cmsPricesEntity.setMax(zero);
                } else {
                    cmsPricesEntity.setMax(quantity5 - 1);
                }
                cmsPricesEntity.setPrice(cost4BigDecimal);
                String location = peiGenesisPriceCsvData.getCurrency();
                if (rmb.equals(location)) {
                    cmsPricesEntity.setCurrency(1);
                } else {
                    cmsPricesEntity.setCurrency(2);
                }
                cmsPricesEntity.setPosition(4);
                cmsPricesEntity.setPublish(1);
                cmsPricesEntityList.add(cmsPricesEntity);
            }


            BigDecimal cost5BigDecimal = new BigDecimal(peiGenesisPriceCsvData.getCost5()).stripTrailingZeros();
            BigDecimal quantity5Decimal = new BigDecimal(peiGenesisPriceCsvData.getQuantity5()).stripTrailingZeros();

            if (!(cost5BigDecimal.compareTo(BigDecimal.ZERO) == 0)) {
                CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
                cmsPricesEntity.setLadderid(0);
                cmsPricesEntity.setGoodid(incodes);
                cmsPricesEntity.setMin(quantity5Decimal.longValue());
                cmsPricesEntity.setMax(zero);
                cmsPricesEntity.setPrice(cost5BigDecimal);
                String location = peiGenesisPriceCsvData.getCurrency();
                if (rmb.equals(location)) {
                    cmsPricesEntity.setCurrency(1);
                } else {
                    cmsPricesEntity.setCurrency(2);
                }
                cmsPricesEntity.setPosition(5);
                cmsPricesEntity.setPublish(1);
                cmsPricesEntityList.add(cmsPricesEntity);
            }

        }

/*        List<Map<String, String>> paramsListMap = new ArrayList<>(1);
        Map<String, String> paramsMap = JSON.parseObject(JSON.toJSONString(cmsGoodsEntity), new TypeReference<Map<String, String>>() {
        });
        paramsListMap.add(paramsMap);*/


        //阶梯价格集合
        //cmsGoodsEntity.setPrices(pricesMap);


        //本身对象
        //cmsGoodsEntity.setParams(paramsListMap);


        List<CmsPricesPojo> prices = new ArrayList<>();
        for (CmsPricesEntity cmsPricesEntityBean : cmsPricesEntityList) {
            CmsPricesPojo cmsPricesPojo = new CmsPricesPojo();
            BeanUtils.copyProperties(cmsPricesEntityBean, cmsPricesPojo);
            cmsPricesPojo.setPrice(PriceUtil.handlePrice(cmsPricesEntityBean.getPrice().toPlainString()));
            prices.add(cmsPricesPojo);
        }

        // 更新阶梯价格配置到商品表
        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(prices, SerializerFeature.WriteMapNullValue));
        cmsGoodsEntity.setPrices(jsonArray.toJSONString());


        cmsGoodsPojo.setCmsPricesEntityList(cmsPricesEntityList);


        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setGoodid(cmsGoodsEntity.getGoodid());
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setEcnn(1);


        //修改,税费计算和存储
        if (cIQprice == null) {
            cmsGoodsInformationEntity.setInspectionCharges(0);
            cmsGoodsInformationEntity.setInspectionChargesFee(BigDecimal.ZERO.doubleValue());
        } else {
            cmsGoodsInformationEntity.setInspectionCharges(1);
            cmsGoodsInformationEntity.setInspectionChargesFee(cIQprice.doubleValue());
        }

        if (tariffRate == null) {
            cmsGoodsInformationEntity.setTariffs(1);
        } else {
            cmsGoodsInformationEntity.setTariffs(0);
        }

        cmsGoodsEntity.setUnitPrice(null);
        //cmsGoodsInformationEntity.setParams(paramsListMap);
        cmsGoodsPojo.setCmsGoodsEntity(cmsGoodsEntity);
        cmsGoodsPojo.setCmsGoodsInformationEntity(cmsGoodsInformationEntity);

        return cmsGoodsPojo;
    }


    /**
     * 转换为对象
     *
     * @param dataValue
     * @return
     */
    private PeiGenesisCsvData convertPeiGenesisCsvData(String[] dataValue) {
        PeiGenesisCsvData peiGenesisCsvData = new PeiGenesisCsvData();
        peiGenesisCsvData.setManufacturer(isNull(dataValue[0]));
        peiGenesisCsvData.setManufacturerPartNumber(isNull(dataValue[1]));
        peiGenesisCsvData.setPeiPartNumber(isNull(dataValue[2]));
        peiGenesisCsvData.setDescription(isNull(dataValue[3]));
        peiGenesisCsvData.setProductCategory6(isNull(dataValue[4]));
        peiGenesisCsvData.setAvailableStock(isNull(dataValue[5]));
        peiGenesisCsvData.setLocation(isNull(dataValue[6]));
        peiGenesisCsvData.setUnitOfMeasure(isNull(dataValue[7]));
        peiGenesisCsvData.setMinimum(isNull(dataValue[8]));
        peiGenesisCsvData.setLeadTime(isNull(dataValue[9]));
        peiGenesisCsvData.setCommodityCode(isNull(dataValue[10]));
        peiGenesisCsvData.setSedFlag(isNull(dataValue[11]));
        peiGenesisCsvData.setRohs(isNull(dataValue[12]));
        peiGenesisCsvData.setExportControlType(isNull(dataValue[13]));
        peiGenesisCsvData.setExportControlClass(isNull(dataValue[14]));
        peiGenesisCsvData.setPeigenesisComUrl(isNull(dataValue[15]));
        return peiGenesisCsvData;
    }


    /**
     * 处理空值
     *
     * @param value
     * @return
     */
    private static String isNull(String value) {
        if (StringUtils.validateParameter(value)) {
            return value;
        }
        return "";
    }

    /**
     * 插入系统主体更新数量日志记录表数据
     *
     * @param topicUpdateLogId 主题更新日志表主键id
     * @param dataSize         数据数量
     */
    protected void insertCmsTopicUpdateSumLogEntity(Integer topicUpdateLogId, Integer dataSize) {
        CmsTopicUpdateSumLogEntity cmsTopicUpdateSumLogEntity = new CmsTopicUpdateSumLogEntity();
        cmsTopicUpdateSumLogEntity.setTopicUpdateLogId(topicUpdateLogId);
        cmsTopicUpdateSumLogEntity.setUpdateSum(dataSize);
        cmsTopicUpdateSumLogEntity.setCreateTime(new Date(System.currentTimeMillis()));
        cmsTopicUpdateSumLogMapper.insert(cmsTopicUpdateSumLogEntity);
    }


}
