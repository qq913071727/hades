package com.yuanda.erp9.syn.util;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName SubListUtil
 * @Description 截取list工具类
 * @Date 2022/12/22
 * @Author myq
 */
public class SubListUtil {


    /**
     * @Description: 按照指定的数量拆分集合
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/2216:34
     */
    public static <T> List<List<T>> splitList(List<T> list, int len) {
        if(CollectionUtils.isEmpty(list) || len < 1){
            return Collections.emptyList();
        }

        List<List<T>> result = new ArrayList<>();
        int size = list.size();
        int count = (size + len - 1) / len;

        for (int i = 0; i < count; i++) {
            List<T> subList = list.subList(i * len, (Math.min((i + 1) * len, size)));
            result.add(subList);
        }
        return result;
    }

}
