package com.yuanda.erp9.syn.contant;

import java.math.BigDecimal;

/**
 * @ClassName RsSteppedPriceConstant
 * @Description Rs 供应商常量类
 * @Date 2022/12/8
 * @Author swq
 */
public interface RsSteppedPriceConstant {

    /**
     * 港币转化美元比例(阶梯价转换)
     */
    double RATE = 7.9099;

    /**
     * 中国数据比例（阶梯价转换）
     */
    double VATR = 0.13;

    /**
     * 香港商品id前缀
     */
    String HK = "HK";

    /**
     * 中国商品id前缀
     */
    String CN = "CN";

    /**
     * 香港商品阶梯价数据
     */
    String HK1 = "HK1";
    String HK2 = "HK2";
    String HK3 = "HK3";
    String HK4 = "HK4";
    String HK5 = "HK5";

    /**
     * 中国商品阶梯价数据
     */
    String CN1 = "CN1";
    String CN2 = "CN2";
    String CN3 = "CN3";
    String CN4 = "CN4";
    String CN5 = "CN5";

}
