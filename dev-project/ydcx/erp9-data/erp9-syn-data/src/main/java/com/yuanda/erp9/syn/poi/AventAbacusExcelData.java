package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

/**
 * @ClassName AventAbacusExcelData
 * @Description AventAbacusexcel对应的类
 * @Date 2022/12/5
 * @Author myq
 */
@Data
public class AventAbacusExcelData {
    @ExcelProperty(index = 0)
    private String article;

    @ExcelProperty(index = 1)
    private String manuf;

    @ExcelProperty(index = 2)
    private String stock;

    @ExcelProperty(index = 3)
    @NumberFormat(value = "0.00000")
    private String price;

    @ExcelProperty(index = 4)
    private String currency;

    @ExcelProperty(index = 5)
    private String mpq;

    @ExcelProperty(index = 6)
    private String LT;

    @ExcelProperty(index = 7)
    private String article_man;

    @ExcelProperty(index = 8)
    private String obs;

    @ExcelProperty(index = 9)
    private String abc;

    @ExcelProperty(index = 10)
    private String moq;

    @ExcelProperty(index = 11)
    private String priceclass;

    @ExcelProperty(index = 12)
    private String packtype;

    @ExcelProperty(index = 13)
    private String RoHS;

    @ExcelProperty(index = 14)
    private String techDescr;

    @ExcelProperty(index = 15)
    private String LTB;

    @ExcelProperty(index = 16)
    private String PLANT;

    @ExcelProperty(index = 17)
    private String SAP_MATERIAL;

    @ExcelProperty(index = 18)
    private String SAP_ARTICLE;

}
