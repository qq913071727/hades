package com.yuanda.erp9.syn.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   文件配置类
 * @author linuo
 * @time   2022年12月7日15:51:55
 */
@Data
@Component
@ConfigurationProperties(prefix = "file")
public class FileParamConfig {
    /** 文件下载地址 */
    private String downloadPath;
}