package com.yuanda.erp9.syn.mapper.dyj_middledata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.dyj_middledata.DyjInstorageByYJPriceEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 大赢家价格预警写表
 * @Date 2023/4/7 14:19
 **/
@Mapper
public interface DyjInstorageByYJPriceMapper extends BaseMapper<DyjInstorageByYJPriceEntity> {
    /**
     * 批量插入大赢家价格
     *
     * @param list
     */
    void batchInsert(@Param("list") List<DyjInstorageByYJPriceEntity> list);
}
