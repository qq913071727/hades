package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @ClassName Chip1AdimaxExcelData
 * @Description
 * @Date 2023/2/8
 * @Author myq
 */
@Data
public class Chip1AdimaxExcelData {

    @ExcelProperty(index = 0)
    private String mfr;

    @ExcelProperty(index = 1)
    private String c1SPart;

    @ExcelProperty(index = 2)
    private String mfrPart;

    @ExcelProperty(index = 3)
    private String moq;

    @ExcelProperty(index = 4)
    private String availQty;

    @ExcelProperty(index = 5)
    private String daysToShip;

    @ExcelProperty(index = 6)
    private String break1;

    @ExcelProperty(index = 7)
    private String break2;

    @ExcelProperty(index = 8)
    private String break3;

    @ExcelProperty(index = 9)
    private String break4;

    @ExcelProperty(index = 10)
    private String break5;

    @ExcelProperty(index = 11)
    private String break6;

    @ExcelProperty(index = 12)
    private String break7;

    @ExcelProperty(index = 13)
    private String price1;

    @ExcelProperty(index = 14)
    private String price2;

    @ExcelProperty(index = 15)
    private String price3;

    @ExcelProperty(index = 16)
    private String price4;

    @ExcelProperty(index = 17)
    private String price5;

    @ExcelProperty(index = 18)
    private String price6;

    @ExcelProperty(index = 19)
    private String price7;


}
