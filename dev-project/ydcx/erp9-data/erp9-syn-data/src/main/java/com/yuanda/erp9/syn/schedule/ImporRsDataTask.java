package com.yuanda.erp9.syn.schedule;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.config.FileParamConfig;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.service.erp9.CmsOtherWarnLogService;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.service.erp9.RsDataImportService;
import com.yuanda.erp9.syn.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;

/**
 * @author swq
 * @desc Rs
 * @Date 2022/12/16 16:53
 **/

@Component
//@Configuration
//@EnableScheduling
public class ImporRsDataTask {
    private static final Logger log = LoggerFactory.getLogger(ImporRsDataTask.class);
    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Resource
    private RsDataImportService rsDataImportService;

    @Resource
    private CmsSuppliersTempService cmsSuppliersTempService;

    @Resource
    private CmsOtherWarnLogService cmsOtherWarnLogService;

    // 供应商名称
    private static final String SUPPLIER = "RS Components";

    // 供应商id
    private static final String SUPPLIER_ID = "US000000044";

    /**
     * 导入Rs数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importRsDataJob")
//    @Scheduled(cron="0 0 23 * * ?")
//    @Scheduled(cron = "0 0/1 * * * ?")
    @EnableSchedule
    public void imporRsData() {
        try {
            log.info("导入RS数据定时任务开始");
            Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
            log.info("1. 通过下载地址获取文件");
            // 1. 通过下载地址获取文件
            //RS_CN 下载文件url
            String fileCNPath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.RS_CN_DOWN_FILE_URL, fileParamConfig.getDownloadPath(),supplierId);
            //RS_HK 下载文件url
            String fileHKPath = FileUtils.DownAndReadFileAndUnZipFiles(DownloadUrlConstants.RS_HK_DOWN_FILE_URL, fileParamConfig.getDownloadPath(),supplierId);
            if (StringUtils.isNotEmpty(fileCNPath) && StringUtils.isNotEmpty(fileHKPath)) {
                log.info("RS数据文件下载完成,文件保存路径:[fileCNPath:{},fileHKPath:{}]", fileCNPath, fileHKPath);
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setCreateAt(new Date());
                entity.setSupplier(supplierId);
                entity.setSystemTopicType(SubjectSupplierEnum.RS_Components.getType());
                entity.setSystemTopic(SubjectSupplierEnum.RS_Components.getSubject());
                cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                log.info("插入系统主体更新日志记录表数据完成");
                // 导入RS文件数据
                log.info("2. 解析中国文件(和香港文件)数据进行入库操作");
                rsDataImportService.csvDataInsert(fileCNPath,fileHKPath);
            } else {
                log.warn("RS数据文件下载失败,文件保存路径:[fileCNPath:{},fileHKPath:{}]", fileCNPath, fileHKPath);
            }
        } catch (Exception e) {
            log.warn("下载导入数据错误:",e);
            e.printStackTrace();
        }

    }
}
