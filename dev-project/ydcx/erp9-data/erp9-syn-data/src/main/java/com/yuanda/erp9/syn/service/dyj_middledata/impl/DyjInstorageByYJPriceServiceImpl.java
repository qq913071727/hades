package com.yuanda.erp9.syn.service.dyj_middledata.impl;

import com.yuanda.erp9.syn.entity.dyj_middledata.DyjInstorageByYJPriceEntity;
import com.yuanda.erp9.syn.mapper.dyj_middledata.DyjInstorageByYJPriceMapper;
import com.yuanda.erp9.syn.service.dyj_middledata.DyjInstorageByYJPriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 大赢家库存价格预警写表管理
 * @Date 2023/4/7 14:51
 **/
@Service
public class DyjInstorageByYJPriceServiceImpl implements DyjInstorageByYJPriceService {
    @Resource
    private DyjInstorageByYJPriceMapper dyjInstorageByYJPriceMapper;
    @Override
    public void batchInsert(List<DyjInstorageByYJPriceEntity> insertDate) {
        dyjInstorageByYJPriceMapper.batchInsert(insertDate);
    }
}
