package com.yuanda.erp9.syn.annotation;

import java.lang.annotation.*;

/**
 * @Author myq
 * @Date 2023/4/13 14:18
 * @Version 1.0
 * @Description
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableSchedule {
}
