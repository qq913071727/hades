package com.yuanda.erp9.syn.poi;

import com.csvreader.CsvReader;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @author zengqinglong
 * @desc Element14
 * @Date 2022/12/6 14:02
 **/
@Data
@Slf4j
public class Element14TxtData implements Serializable {
    private String categoryName;
    private String element14OrderCode;
    private String manSku;
    private String manName;
    private String uom;
    private String soq;
    private String weight;
    private String countryOfOrigin;
    private String b1;
    private String p1;
    private String b2;
    private String p2;
    private String b3;
    private String p3;
    private String b4;
    private String p4;
    private String b5;
    private String p5;
    private String b6;
    private String p6;
    private String b7;
    private String p7;
    private String b8;
    private String p8;
    private String b9;
    private String P9;
    private String b10;
    private String p10;
    private String roHsStatus;
    private String warehouseCN;
    private String warehouseUK;
    private String warehouseUS;
    private String warehouseSG;

    private Boolean isHk;
    private Integer supplierId;
    /**
     * element14 cn
     *
     * @param data
     * @return
     */
    public static Element14TxtData toElement14TxtCnDataFormat(String[] data) {
        try {
            Element14TxtData element14TxtData = new Element14TxtData();
            element14TxtData.setCategoryName(data[0]);
            element14TxtData.setElement14OrderCode(data[1]);
            element14TxtData.setManSku(data[2]);
            element14TxtData.setManName(data[3]);
            element14TxtData.setUom(data[4]);
            element14TxtData.setSoq(data[5]);
            element14TxtData.setWeight(data[6]);
            element14TxtData.setCountryOfOrigin(data[7]);
            element14TxtData.setB1(data[12]);
            element14TxtData.setP1(data[13]);
            element14TxtData.setB2(data[14]);
            element14TxtData.setP2(data[15]);
            element14TxtData.setB3(data[16]);
            element14TxtData.setP3(data[17]);
            element14TxtData.setB4(data[18]);
            element14TxtData.setP4(data[19]);
            element14TxtData.setB5(data[20]);
            element14TxtData.setP5(data[21]);
            element14TxtData.setB6(data[22]);
            element14TxtData.setP6(data[23]);
            element14TxtData.setB7(data[24]);
            element14TxtData.setP7(data[25]);
            element14TxtData.setB8(data[26]);
            element14TxtData.setP8(data[27]);
            element14TxtData.setB9(data[28]);
            element14TxtData.setP9(data[29]);
            element14TxtData.setB10(data[30]);
            element14TxtData.setP10(data[31]);
            element14TxtData.setRoHsStatus(data[32]);
            element14TxtData.setWarehouseCN(data[8]);
            element14TxtData.setWarehouseUK(data[9]);
            element14TxtData.setWarehouseUS(data[11]);
            element14TxtData.setWarehouseSG(data[10]);
            return element14TxtData;
        } catch (Exception e) {
            log.info("Element14解析异常,放弃当前这条数据");
            return null;
        }
    }
    /**
     * element14 cn
     *
     * @param csvReader
     * @return
     */
    public static Element14TxtData toElement14CsvCnDataFormat(CsvReader csvReader) {
        try {
            Element14TxtData element14TxtData = new Element14TxtData();
            element14TxtData.setCategoryName(csvReader.get("category name 1"));
            element14TxtData.setElement14OrderCode(csvReader.get("element14 order code"));
            element14TxtData.setManSku(csvReader.get("man sku"));
            element14TxtData.setManName(csvReader.get("man name"));
            element14TxtData.setUom(csvReader.get("uom"));
            element14TxtData.setSoq(csvReader.get("soq"));
            element14TxtData.setWeight(csvReader.get("weight"));
            element14TxtData.setCountryOfOrigin(csvReader.get("country of origin"));
            element14TxtData.setB1(csvReader.get("b1"));
            element14TxtData.setP1(csvReader.get("p1"));
            element14TxtData.setB2(csvReader.get("b2"));
            element14TxtData.setP2(csvReader.get("p2"));
            element14TxtData.setB3(csvReader.get("b3"));
            element14TxtData.setP3(csvReader.get("p3"));
            element14TxtData.setB4(csvReader.get("b4"));
            element14TxtData.setP4(csvReader.get("p4"));
            element14TxtData.setB5(csvReader.get("b5"));
            element14TxtData.setP5(csvReader.get("p5"));
            element14TxtData.setB6(csvReader.get("b6"));
            element14TxtData.setP6(csvReader.get("p6"));
            element14TxtData.setB7(csvReader.get("b7"));
            element14TxtData.setP7(csvReader.get("p7"));
            element14TxtData.setB8(csvReader.get("b8"));
            element14TxtData.setP8(csvReader.get("p8"));
            element14TxtData.setB9(csvReader.get("b9"));
            element14TxtData.setP9(csvReader.get("p9"));
            element14TxtData.setB10(csvReader.get("b10"));
            element14TxtData.setP10(csvReader.get("p10"));
            element14TxtData.setRoHsStatus(csvReader.get("rohs status"));
            element14TxtData.setWarehouseCN(csvReader.get("warehouse (cn)"));
            element14TxtData.setWarehouseUK(csvReader.get("warehouse (uk)"));
            element14TxtData.setWarehouseUS(csvReader.get("warehouse (us)"));
            element14TxtData.setWarehouseSG(csvReader.get("warehouse (sg)"));
            return element14TxtData;
        } catch (Exception e) {
            log.info("Element14解析异常,放弃当前这条数据");
            return null;
        }
    }

    /**
     * element14 hk
     *
     * @param data
     * @return
     */
    public static Element14TxtData toElement14TxtHkDataFormat(String[] data) {
        try {
            Element14TxtData element14TxtData = new Element14TxtData();
            element14TxtData.setCategoryName(data[0]);
            element14TxtData.setElement14OrderCode(data[1]);
            element14TxtData.setManSku(data[2]);
            element14TxtData.setManName(data[3]);
            element14TxtData.setUom(data[4]);
            element14TxtData.setSoq(data[5]);
            element14TxtData.setWeight(data[6]);
            element14TxtData.setCountryOfOrigin(data[7]);
            element14TxtData.setB1(data[10]);
            element14TxtData.setP1(data[11]);
            element14TxtData.setB2(data[12]);
            element14TxtData.setP2(data[13]);
            element14TxtData.setB3(data[14]);
            element14TxtData.setP3(data[15]);
            element14TxtData.setB4(data[16]);
            element14TxtData.setP4(data[17]);
            element14TxtData.setB5(data[18]);
            element14TxtData.setP5(data[19]);
            element14TxtData.setB6(data[20]);
            element14TxtData.setP6(data[21]);
            element14TxtData.setB7(data[22]);
            element14TxtData.setP7(data[23]);
            element14TxtData.setB8(data[24]);
            element14TxtData.setP8(data[25]);
            element14TxtData.setB9(data[26]);
            element14TxtData.setP9(data[27]);
            element14TxtData.setB10(data[28]);
            element14TxtData.setP10(data[29]);
            element14TxtData.setRoHsStatus(data[30]);
            element14TxtData.setWarehouseUK(data[8]);
            element14TxtData.setWarehouseUS(data[31]);
            element14TxtData.setWarehouseSG(data[9]);
            return element14TxtData;
        } catch (Exception e) {
            log.info("Element14解析异常,放弃当前这条数据");
            return null;
        }
    }

    /**
     * element14 hk
     *
     * @param csvReader
     * @return
     */
    public static Element14TxtData toElement14CsvHkDataFormat(CsvReader csvReader) {
        try {
            Element14TxtData element14TxtData = new Element14TxtData();
            element14TxtData.setCategoryName(csvReader.get("category name 1"));
            element14TxtData.setElement14OrderCode(csvReader.get("element14 order code"));
            element14TxtData.setManSku(csvReader.get("man sku"));
            element14TxtData.setManName(csvReader.get("man name"));
            element14TxtData.setUom(csvReader.get("uom"));
            element14TxtData.setSoq(csvReader.get("soq"));
            element14TxtData.setWeight(csvReader.get("weight"));
            element14TxtData.setCountryOfOrigin(csvReader.get("country of origin"));
            element14TxtData.setB1(csvReader.get("b1"));
            element14TxtData.setP1(csvReader.get("p1"));
            element14TxtData.setB2(csvReader.get("b1"));
            element14TxtData.setP2(csvReader.get("p2"));
            element14TxtData.setB3(csvReader.get("b3"));
            element14TxtData.setP3(csvReader.get("p3"));
            element14TxtData.setB4(csvReader.get("b4"));
            element14TxtData.setP4(csvReader.get("p4"));
            element14TxtData.setB5(csvReader.get("b5"));
            element14TxtData.setP5(csvReader.get("p5"));
            element14TxtData.setB6(csvReader.get("b6"));
            element14TxtData.setP6(csvReader.get("p6"));
            element14TxtData.setB7(csvReader.get("b7"));
            element14TxtData.setP7(csvReader.get("p7"));
            element14TxtData.setB8(csvReader.get("b8"));
            element14TxtData.setP8(csvReader.get("p8"));
            element14TxtData.setB9(csvReader.get("b9"));
            element14TxtData.setP9(csvReader.get("p9"));
            element14TxtData.setB10(csvReader.get("b10"));
            element14TxtData.setP10(csvReader.get("p10"));
            element14TxtData.setRoHsStatus(csvReader.get("rohs status"));
            element14TxtData.setWarehouseUK(csvReader.get("warehouse (uk)"));
            element14TxtData.setWarehouseUS(csvReader.get("warehouse (us)"));
            element14TxtData.setWarehouseSG(csvReader.get("warehouse (sg)"));
            return element14TxtData;
        } catch (Exception e) {
            log.info("Element14解析异常,放弃当前这条数据");
            return null;
        }
    }
}
