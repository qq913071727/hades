package com.yuanda.erp9.syn.service.erp9;

import java.io.IOException;

/**
 * @desc   Rs数据导入
 * @author swq
 * @time   2022年12月1日18:57:32
 */
public interface RsDataImportService {

    /**
     * csv数据插入
     * @param fileCNPath 中国文件路径
     * @param fileHKPath 香港文件路径
     */
    void csvDataInsert(String fileCNPath,String fileHKPath) throws InterruptedException, IOException;
}
