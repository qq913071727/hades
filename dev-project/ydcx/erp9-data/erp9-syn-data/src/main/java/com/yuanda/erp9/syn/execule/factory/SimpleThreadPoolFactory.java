package com.yuanda.erp9.syn.execule.factory;

import com.yuanda.erp9.syn.enums.ThreadPoolTypeEnum;
import com.yuanda.erp9.syn.execule.thread.CurrencyThreadPool;
import com.yuanda.erp9.syn.execule.thread.GlobalThreadPool;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @ClassName SimpleThreadPoolFactory
 * @Description 简单的工厂类
 * @Date 2022/11/29
 * @Author myq
 */
public class SimpleThreadPoolFactory {

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/2916:47
     */
    public static ThreadPoolTaskExecutor choose(ThreadPoolTypeEnum threadPoolTypeEnum) {
        // 专用的线程池
        if (ThreadPoolTypeEnum.SPECIAL_PURPOSE.equals(threadPoolTypeEnum)) {
            return GlobalThreadPool.getInstance();
            // 其他的、通用的类型
        } else if (ThreadPoolTypeEnum.CURRENCY.equals(threadPoolTypeEnum)) {
            return CurrencyThreadPool.getInstance();
        } else {
            throw new IllegalArgumentException("非法的参数类型");
        }
    }
}
