package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zengqinglong
 * @desc Avnet表
 * @Date 2022/11/28 9:34
 **/
@Data
@Slf4j
public class AvnetCsvData {
    @ExcelProperty(index = 0)
    private String manufacturerPart;

    @ExcelProperty(index = 1)
    private String maAlternatePartnuf;

    @ExcelProperty(index = 2)
    private String description;

    @ExcelProperty(index = 3)
    private String prRoHSNonCompliantice;

    @ExcelProperty(index = 4)
    private String ncnr;

    @ExcelProperty(index = 5)
    private String obsoletePart;

    @ExcelProperty(index = 6)
    private String eccn;

    @ExcelProperty(index = 7)
    private String scheduleB;

    @ExcelProperty(index = 8)
    private String htsn;

    @ExcelProperty(index = 9)
    private String unspsc;

    @ExcelProperty(index = 10)
    private String unspscVersion;

    @ExcelProperty(index = 11)
    private String stockLocation;

    @ExcelProperty(index = 12)
    private String stockQuantity;

    @ExcelProperty(index = 13)
    private String moq;

    @ExcelProperty(index = 14)
    private String multi;

    @ExcelProperty(index = 15)
    private String manufacturer;

    @ExcelProperty(index = 16)
    private String package1;

    @ExcelProperty(index = 17)
    private String stepPrice;

    @ExcelProperty(index = 18)
    private String priceCurrency;

    @ExcelProperty(index = 19)
    private String datasheetURL;

    @ExcelProperty(index = 20)
    private String productImageURL;

    private Integer supplierId;

    public static AvnetCsvData toAvnetCsvDataFormat(String[] data) {
        try {
            AvnetCsvData avnetExcelData = new AvnetCsvData();
            avnetExcelData.setManufacturerPart(data[0]);
            avnetExcelData.setMaAlternatePartnuf(data[1]);
            avnetExcelData.setDescription(data[2]);
            avnetExcelData.setPrRoHSNonCompliantice(data[3]);
            avnetExcelData.setNcnr(data[4]);
            avnetExcelData.setObsoletePart(data[5]);
            avnetExcelData.setEccn(data[6]);
            avnetExcelData.setScheduleB(data[7]);
            avnetExcelData.setHtsn(data[8]);
            avnetExcelData.setUnspsc(data[9]);
            avnetExcelData.setUnspscVersion(data[10]);
            avnetExcelData.setStockLocation(data[11]);
            avnetExcelData.setStockQuantity(data[12]);
            avnetExcelData.setMoq(data[13]);
            avnetExcelData.setMulti(data[14]);
            avnetExcelData.setManufacturer(data[15]);
            avnetExcelData.setPackage1(data[16]);
            avnetExcelData.setStepPrice(data[17]);
            avnetExcelData.setPriceCurrency(data[18]);
            avnetExcelData.setDatasheetURL(data[19]);
            avnetExcelData.setProductImageURL(data[20]);
            return avnetExcelData;
        } catch (Exception e) {
            log.info("Avnet解析异常,放弃当前这条数据");
            return null;
        }
    }
}
