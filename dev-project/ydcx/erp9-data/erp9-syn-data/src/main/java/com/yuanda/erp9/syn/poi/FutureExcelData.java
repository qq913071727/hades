package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * @ClassName FutureExcelData
 * @Description future供应商对应的excel
 * @Date 2022/11/18
 * @Author myq
 */
@Data
public class FutureExcelData {

    @ExcelProperty(index = 0)
    private String region;

    @ExcelProperty(index = 1)
    private String mpn;

    @ExcelProperty(index = 2)
    private String mfr;

    @ExcelProperty(index = 3)
    private String mfrName;

    @ExcelProperty(index = 4)
    private String description;

    @ExcelProperty(index = 5)
    private String ats;

    @ExcelProperty(index = 6)
    private String notes;

    @ExcelProperty(index = 7)
    private String dateCode;

    @ExcelProperty(index = 8)
    private String packageType2;

    @ExcelProperty(index = 9)
    private String minqty;

    @ExcelProperty(index = 10)
    private String mpq;

    @ExcelProperty(index = 11)
    private String eccn;

    @ExcelProperty(index = 12)
    private String htsCode;

    @ExcelProperty(index = 13)
    private String htscodeFull;

    @ExcelProperty(index = 14)
    private String coo;

    @ExcelProperty(index = 15)
    private String priceBreak1;

    @ExcelProperty(index = 16)
    private String priceQty1;

    @ExcelProperty(index = 17)
    private String priceBreak2;

    @ExcelProperty(index = 18)
    private String priceQty2;

    @ExcelProperty(index = 19)
    private String priceBreak3;

    @ExcelProperty(index = 20)
    private String priceQty3;

    @ExcelProperty(index = 21)
    private String priceBreak4;

    @ExcelProperty(index = 22)
    private String priceQty4;

    @ExcelProperty(index = 23)
    private String priceBreak5;

    @ExcelProperty(index = 24)
    private String priceQty5;

    @ExcelProperty(index = 25)
    private String priceBreak6;

    @ExcelProperty(index = 26)
    private String priceQty6;

    /**
     * 修改坐标
     */
    @ExcelProperty(index = 28)
    private String standardLeadtime;

    @ExcelProperty(index = 27)
    private String minqtyNoAts;

    @ExcelProperty(index = 29)
    private String uniqueID;

    @ExcelProperty(index = 30)
    private String rohs;

    // 根据规则生成
    private String userType;

}
