package com.yuanda.erp9.syn.execule;

import com.yuanda.erp9.syn.entity.CmsGoodsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Engine
 * @Description 调度中心
 * @Date 2022/11/15
 * @Author myq
 */
public class Engine {

    private volatile boolean task = false;

    List<CmsGoodsEntity> cmsGoodsEntities = new ArrayList<>();

    /**
     * @Description: 分配任务
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1515:28
     */
    private void distribution(){

        task = true;

    }

}
