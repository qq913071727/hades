package com.yuanda.erp9.syn.entity.dyj_middledata;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 大赢家价格预警写表
 * @Date 2023/4/7 14:19
 **/
@ApiModel(value = "DyjInstorageByPriceEntity", description = "大赢家价格预警写表")
@TableName("DYJ_InstorageByYJPrice")
@Data
public class DyjInstorageByYJPriceEntity  implements Serializable {
    @TableField(value = "编号")
    private Integer number;

    @TableField(value = "型号")
    private String model;

    @TableField(value = "厂家")
    private String manufactor;

    @TableField(value = "价格")
    private BigDecimal price;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "创建时间")
    private Date creatDate;
}
