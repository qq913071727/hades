package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @ClassName RsHkCsvData
 * @Description rs供应商对应的excel(香港数据)
 * @Date 2022/12/9
 * @Author swq
 */
@Data
@Slf4j
public class RsHkCsvData implements Serializable {

    @ExcelProperty(index = 0)
    private String distributorPartNumber;//sign标识

    @ExcelProperty(index = 1)
    private String manufacturerPartNumber;//型号

    @ExcelProperty(index = 2)//
    private String manufacturer;//制造商

    @ExcelProperty(index = 3)
    private String description;//描述

    @ExcelProperty(index = 4)
    private String rohs;//hisp

    @ExcelProperty(index = 5)
    private String productUrl;//不需要

    @ExcelProperty(index = 6)
    private String quantityAvailable;//不需要

    @ExcelProperty(index = 7)
    private String priceQuantity;//最小起订量

    @ExcelProperty(index = 8)
    private String imageUrl;//img

    @ExcelProperty(index = 9)
    private String productType;//分类

    @ExcelProperty(index = 10)
    private String baseUom;//不需要

    @ExcelProperty(index = 11)
    private String salesUom;//不需要

    @ExcelProperty(index = 12)
    private String packSize;//mpq

    @ExcelProperty(index = 13)
    private String hkdPrice1;

    @ExcelProperty(index = 14)
    private String hkdPrice2;

    @ExcelProperty(index = 15)
    private String hkdPrice3;

    @ExcelProperty(index = 16)
    private String hkdPrice4;

    @ExcelProperty(index = 17)
    private String hkdPrice5;

    @ExcelProperty(index = 18)
    private String attributeName1;

    @ExcelProperty(index = 19)
    private String attributeName2;

    @ExcelProperty(index = 20)
    private String attributeName3;

    @ExcelProperty(index = 21)
    private String attributeName4;

    @ExcelProperty(index = 22)
    private String attributeName5;

    @ExcelProperty(index = 23)
    private String attributeName6;

    @ExcelProperty(index = 24)
    private String attributeName7;

    @ExcelProperty(index = 25)
    private String attributeName8;

    @ExcelProperty(index = 26)
    private String attributeName9;

    @ExcelProperty(index = 27)
    private String attributeName10;

    @ExcelProperty(index = 28)
    private String attributeName11;

    @ExcelProperty(index = 29)
    private String attributeName12;

    @ExcelProperty(index = 30)
    private String attributeName13;

    @ExcelProperty(index = 31)
    private String attributeName14;

    @ExcelProperty(index = 32)
    private String attributeName15;

    @ExcelProperty(index = 33)
    private String attributeValue1;

    @ExcelProperty(index = 34)
    private String attributeValue2;

    @ExcelProperty(index = 35)
    private String attributeValue3;

    @ExcelProperty(index = 36)
    private String attributeValue4;

    @ExcelProperty(index = 37)
    private String attributeValue5;

    @ExcelProperty(index = 38)
    private String attributeValue6;

    @ExcelProperty(index = 39)
    private String attributeValue7;

    @ExcelProperty(index = 40)
    private String attributeValue8;

    @ExcelProperty(index = 41)
    private String attributeValue9;

    @ExcelProperty(index = 42)
    private String attributeValue10;

    @ExcelProperty(index = 43)
    private String attributeValue11;

    @ExcelProperty(index = 44)
    private String attributeValue12;

    @ExcelProperty(index = 45)
    private String attributeValue13;

    @ExcelProperty(index = 46)
    private String attributeValue14;

    @ExcelProperty(index = 47)
    private String attributeValue15;

    @ExcelProperty(index = 48)
    private String stockQtyGb;//库存

    @ExcelProperty(index = 49)
    private String shippingWeight;//不需要

    public static RsHkCsvData toRsHkCsvDataFormat(String[] data) {
        try {
            RsHkCsvData rsHkCsvData = new RsHkCsvData();
            rsHkCsvData.setDistributorPartNumber(data[0]);
            rsHkCsvData.setManufacturerPartNumber(data[1]);
            rsHkCsvData.setManufacturer(data[2]);
            rsHkCsvData.setDescription(data[3]);
            rsHkCsvData.setRohs(data[4]);
            rsHkCsvData.setProductUrl(data[5]);
            rsHkCsvData.setQuantityAvailable(data[6]);
            rsHkCsvData.setPriceQuantity(data[7]);
            rsHkCsvData.setImageUrl(data[8]);
            rsHkCsvData.setProductType(data[9]);
            rsHkCsvData.setBaseUom(data[10]);
            rsHkCsvData.setSalesUom(data[11]);
            rsHkCsvData.setPackSize(data[12]);
            rsHkCsvData.setHkdPrice1(data[13]);
            rsHkCsvData.setHkdPrice2(data[14]);
            rsHkCsvData.setHkdPrice3(data[15]);
            rsHkCsvData.setHkdPrice4(data[16]);
            rsHkCsvData.setHkdPrice5(data[17]);
            rsHkCsvData.setAttributeName1(data[18]);
            rsHkCsvData.setAttributeName2(data[19]);
            rsHkCsvData.setAttributeName3(data[20]);
            rsHkCsvData.setAttributeName4(data[21]);
            rsHkCsvData.setAttributeName5(data[22]);
            rsHkCsvData.setAttributeName6(data[23]);
            rsHkCsvData.setAttributeName7(data[24]);
            rsHkCsvData.setAttributeName8(data[25]);
            rsHkCsvData.setAttributeName9(data[26]);
            rsHkCsvData.setAttributeName10(data[27]);
            rsHkCsvData.setAttributeName11(data[28]);
            rsHkCsvData.setAttributeName12(data[29]);
            rsHkCsvData.setAttributeName13(data[30]);
            rsHkCsvData.setAttributeName14(data[31]);
            rsHkCsvData.setAttributeName15(data[32]);
            rsHkCsvData.setAttributeValue1(data[33]);
            rsHkCsvData.setAttributeValue2(data[34]);
            rsHkCsvData.setAttributeValue3(data[35]);
            rsHkCsvData.setAttributeValue4(data[36]);
            rsHkCsvData.setAttributeValue5(data[37]);
            rsHkCsvData.setAttributeValue6(data[38]);
            rsHkCsvData.setAttributeValue7(data[39]);
            rsHkCsvData.setAttributeValue8(data[40]);
            rsHkCsvData.setAttributeValue9(data[41]);
            rsHkCsvData.setAttributeValue10(data[42]);
            rsHkCsvData.setAttributeValue11(data[43]);
            rsHkCsvData.setAttributeValue12(data[44]);
            rsHkCsvData.setAttributeValue13(data[45]);
            rsHkCsvData.setAttributeValue14(data[46]);
            rsHkCsvData.setAttributeValue15(data[47]);
            rsHkCsvData.setStockQtyGb(data[48]);
            rsHkCsvData.setShippingWeight(data[49]);
            return rsHkCsvData;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
