package com.yuanda.erp9.syn.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态路由数据源
 */
@Order(1)
@Slf4j
public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        String datasource = DynamicDataSourceContextHolder.getDataSourceKey();
        super.setDefaultTargetDataSource(datasource);
        log.debug("使用数据源 {}", datasource);
        return datasource;
    }
}