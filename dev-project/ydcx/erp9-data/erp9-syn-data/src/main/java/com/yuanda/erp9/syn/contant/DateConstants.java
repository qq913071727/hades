package com.yuanda.erp9.syn.contant;

/**
 * @desc   日期常量值
 * @author linuo
 * @time   2022年12月9日16:59:10
 */
public class DateConstants {
    /** 年 */
    public static final String YEAR = "year";

    /** 月 */
    public static final String MONTH = "month";

    /** 日 */
    public static final String DAY = "day";
}