package com.yuanda.erp9.syn.service.erp9;

/**
 * @Description:
 * @Author: Mr.myq
 * @Date: 2023/2/89:45
 */
public interface Chip1AdimaxService {


    /**
     * @param excelPath
     * @param pk
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/89:45
     */
    void parse(String excelPath, Long pk);
}
