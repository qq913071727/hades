package com.yuanda.erp9.syn.service.erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.yuanda.erp9.syn.entity.PushErpInquiryPriceRecordEntity;
import com.yuanda.erp9.syn.enums.PushErpStatusEnum;
import com.yuanda.erp9.syn.mapper.erp9.PushErpInquiryPriceRecordMapper;
import com.yuanda.erp9.syn.service.erp9.PushErpInquiryPriceRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 推送询价记录管理
 * @Date 2023/4/6 14:24
 **/
@Service
public class PushErpInquiryPriceRecordServiceImpl implements PushErpInquiryPriceRecordService {
    @Resource
    private PushErpInquiryPriceRecordMapper pushErpInquiryPriceRecordMapper;

    @Override
    public List<PushErpInquiryPriceRecordEntity> findByPartNumber(PushErpInquiryPriceRecordEntity pushErpInquiryPriceRecordEntity) {
        LambdaQueryWrapper<PushErpInquiryPriceRecordEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PushErpInquiryPriceRecordEntity::getModel, pushErpInquiryPriceRecordEntity.getModel());
        if (StringUtils.isNotEmpty(pushErpInquiryPriceRecordEntity.getBrand())) {
            wrapper.eq(PushErpInquiryPriceRecordEntity::getBrand, pushErpInquiryPriceRecordEntity.getBrand());
        }
        return pushErpInquiryPriceRecordMapper.selectList(wrapper);
    }


    @Override
    public void batchInsert(List<PushErpInquiryPriceRecordEntity> insertPushErp) {
        pushErpInquiryPriceRecordMapper.batchInsert(insertPushErp);
    }


    @Override
    public void batchUpdate(List<PushErpInquiryPriceRecordEntity> updatePushErp) {
        for (PushErpInquiryPriceRecordEntity  erpInquiryPriceRecordEntity : updatePushErp) {
            pushErpInquiryPriceRecordMapper.updateById(erpInquiryPriceRecordEntity);
        }
    }

    @Override
    public List<PushErpInquiryPriceRecordEntity> findByStatus() {
        LambdaQueryWrapper<PushErpInquiryPriceRecordEntity> wrapper = new LambdaQueryWrapper<>();
        //查询未推送的
        wrapper.eq(PushErpInquiryPriceRecordEntity::getStatus, PushErpStatusEnum.TO_BE_PUSH.getStatus());
        return pushErpInquiryPriceRecordMapper.selectList(wrapper);
    }

    @Override
    public void updateStatusAndInquiryNumberByIds(List<Integer> ids,String inquiryNumber) {
        pushErpInquiryPriceRecordMapper.updateStatusAndInquiryNumberByIds(ids,inquiryNumber);
    }
}
