package com.yuanda.erp9.syn.service.erp9.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateLogMapper;
import com.yuanda.erp9.syn.service.erp9.TopicUpdateLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @ClassName TopicUpdatelogServiceImpl
 * @Description
 * @Date 2022/12/16
 * @Author myq
 */
@Service
@Slf4j
public class TopicUpdatelogServiceImpl implements TopicUpdateLogService {


    @Autowired
    private CmsTopicUpdateLogMapper cmsTopicUpdateLogMapper;


    /**
     * @Description: 新增主题更新日志
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/1913:58
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void insert(Integer type, String subject, Integer supplierId) {
        log.info("*****************主题："+subject+"有更新*********************");
        CmsTopicUpdateLogEntity cmsTopicUpdateLogEntity = new CmsTopicUpdateLogEntity();
        cmsTopicUpdateLogEntity.setSystemTopicType(type);
        cmsTopicUpdateLogEntity.setUpdated(1);
        cmsTopicUpdateLogEntity.setSystemTopic(subject);
        cmsTopicUpdateLogEntity.setCreateAt(new Date());
        cmsTopicUpdateLogEntity.setSupplier(supplierId);
        cmsTopicUpdateLogMapper.insert(cmsTopicUpdateLogEntity);
    }
}
