package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商品表
 * @author myq
 * @since 1.0.0 2022-11-15
 */
@TableName("cms_goods")
@Setter
@Getter
public class CmsGoodsEntity implements Serializable {
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value = "商品id")
    private String goodid;

    @ApiModelProperty(value = "原厂后台id，关联administrators表主键")
    private Integer adminid;

    @ApiModelProperty(value = "原厂入驻表主键ID")
    private Integer tenantsid;

    @ApiModelProperty(value = "产品唯一标识")
    private String incodes;

    @ApiModelProperty(value = "分类id")
    private Integer classifyid;

    @ApiModelProperty(value = "产品图片")
    private String img;

    @ApiModelProperty(value = "产品类型：0现货1期货")
    private Integer type;

    @ApiModelProperty(value = "审核状态0待审核1审核通过2审核拒绝")
    private Integer status;

    @ApiModelProperty(value = "审核理由")
    private String statusMsg;

    @ApiModelProperty(value = "数据源0入驻原厂1供应商2自营")
    private Integer dataType;

    @ApiModelProperty(value = "产品型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private Integer brand;

    @ApiModelProperty(value = "供应商")
    private Integer supplier;

    @ApiModelProperty(value = "样品申请 0 不可以 1 可以")
    private Integer isSampleApply;

    @ApiModelProperty(value = "批次号")
    private String batch;

    @ApiModelProperty(value = "原价  (USD原价)")
    private BigDecimal originalPrice;

    @ApiModelProperty(value = "进价（RMB原价）")
    private BigDecimal purchasePrice;

    @ApiModelProperty(value = "单价（价格排序时使用）货币类型：人民币")
    private Double unitPrice;

    @ApiModelProperty(value = "库存")
    private Integer repertory;

    @ApiModelProperty(value = "库存所在地")
    private String repertoryArea;

    @ApiModelProperty(value = "发布状态")
    private Integer publish;

    @ApiModelProperty(value = "描述")
    private String describe;

    @ApiModelProperty(value = "数据手册")
    private String files;

    @ApiModelProperty(value = "购买类型0单个买1整包买")
    private Integer payType;

    @ApiModelProperty(value = "最小起订")
    private Long minimumOrder;

    @ApiModelProperty(value = "增量")
    private Long incremental;

    @ApiModelProperty(value = "mpq")
    private Long mpq;

    @ApiModelProperty(value = "交货地区：0国内1香港")
    private String deliveryArea;

    @ApiModelProperty(value = "国内交货日期")
    private String domesticDate;

    @ApiModelProperty(value = "香港交货日期")
    private String hongkongDate;

    @ApiModelProperty(value = "")
    private String hisp;

    @ApiModelProperty(value = "是否需要3c认证0是1否")
    private Integer ccc;

    @ApiModelProperty(value = "销量")
    private Integer sales;

    @ApiModelProperty(value = "点击量")
    private Integer click;

    @ApiModelProperty(value = "访客记录")
    private Integer visitor;

    @ApiModelProperty(value = "")
    private String deletedAt;

    @ApiModelProperty(value = "")
    private String createdAt;

    @ApiModelProperty(value = "")
    private String updatedAt;

    @ApiModelProperty(value = "商品活动")
    private Integer activityId;

    @ApiModelProperty(value = "商品主图")
    private String atlas;

    @ApiModelProperty(value = "商品详情")
    private String content;

    @ApiModelProperty(value = "ecnn号")
    private String eccnNo;

    @ApiModelProperty(value = "关税率")
    private Double tariffsRate;

    @ApiModelProperty(value = "商检费")
    private Double inspectionChargesFee;

    @ApiModelProperty(value = "参数")
    private Object params;

    @ApiModelProperty(value = "价格阶梯")
    private Object prices;

    @ApiModelProperty(value = "数据来源 1：数据包 2：邮件 3：实时")
    private Integer source;

    @ApiModelProperty(value = "供应商名称")
    private String splName;

    @ApiModelProperty(value = "品牌名称")
    private String brandName;

    @ApiModelProperty(value = "费率")
    private Object rates;

    @ApiModelProperty(value = "封装")
    private String package_;

    @ApiModelProperty(value = "包装")
    private String packing;

    @ApiModelProperty(value = "是否禁运(1是、0否)")
    private Integer embargo;
}