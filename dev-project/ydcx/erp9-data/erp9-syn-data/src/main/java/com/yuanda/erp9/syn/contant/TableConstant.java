package com.yuanda.erp9.syn.contant;

/**
 * @ClassName TableConstant
 * @Description
 * @Date 2022/11/24
 * @Author myq
 */
public interface TableConstant {


    /**
     *  品牌
     */
    String CMS_BRANDS_TEMP = "cms_brands_temp";


    /**
     *  生产厂商
     */
    String CMS_SUPPLIERS_TEMP = "cms_suppliers_temp";

}
