package com.yuanda.erp9.syn.service.erp9;

import java.util.HashMap;

public interface AnalysisMailExcelService {


    void analysisAventAsiaHot(HashMap<String, String> param);


    void analysisAventEbvHot(HashMap<String, String> param);


    void analysisSilica(HashMap<String, String> param);

}
