package com.yuanda.erp9.syn.service.erp9;

/**
 * @desc   ICGoo数据导入
 * @author linuo
 * @time   2022年11月30日16:37:33
 */
public interface ICGooDataImportService {
    /**
     * 导入ICGoo文件数据
     * @param topicUpdateLogId 系统主体更新日志记录表id
     * @param filePath 文件路径
     */
    String importCmsGoodsPojo(Long topicUpdateLogId, String filePath);
}