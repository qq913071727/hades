package com.yuanda.erp9.syn.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   币种枚举
 * @author linuo
 * @time   2022年11月29日13:46:11
 */
public enum CurrencyEnum {
    RMB(1,"人民币"),
    DOLLAR(2,"美元");

    private Integer code;
    private String desc;

    CurrencyEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CurrencyEnum of(Integer code) {
        for (CurrencyEnum c : CurrencyEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (CurrencyEnum c : CurrencyEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (CurrencyEnum e : CurrencyEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}