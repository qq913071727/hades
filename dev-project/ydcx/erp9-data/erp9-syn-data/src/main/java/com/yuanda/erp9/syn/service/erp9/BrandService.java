package com.yuanda.erp9.syn.service.erp9;


import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;

import java.util.List;

/**
 * @ClassName BrandService
 * @Description
 * @Date 2022/12/16
 * @Author myq
 */
public interface BrandService {


   List<CmsBrandsTempEntity> selectList();

   /**
    * 插入品牌
    *
    * @param manufacturer
    */
   Integer insertBrand(String manufacturer);


   /**
    * @Description: 插入
    * @Params:
    * @Return:
    * @Author: Mr.myq
    * @Date: 2022/12/2315:12
    */
   void insert(CmsBrandsTempEntity brandsTempEntity);
}
