package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.contant.RsSteppedPriceConstant;
import com.yuanda.erp9.syn.contant.TableConstant;
import com.yuanda.erp9.syn.entity.CmsBrandsTempEntity;
import com.yuanda.erp9.syn.entity.CmsGoodsInformationEntity;
import com.yuanda.erp9.syn.entity.CmsPricesEntity;
import com.yuanda.erp9.syn.enums.BrandTagEnum;
import com.yuanda.erp9.syn.enums.CachePrefixEnum;
import com.yuanda.erp9.syn.mapper.erp9.CmsBrandsTempMapper;
import com.yuanda.erp9.syn.mapper.erp9.CmsSuppliersTempMapper;
import com.yuanda.erp9.syn.service.erp9.CmsBrandsTempService;
import com.yuanda.erp9.syn.util.CacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.Strings;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author swq
 * @desc rs数据保存的实现类
 * @time 2022年12月7日10:03:07
 */
@Slf4j
public class RsSaveImpl {

    @Resource
    protected CmsSuppliersTempMapper cmsSuppliersTempMapper;

    @Resource
    protected CmsBrandsTempService cmsBrandsTempService;


    protected static CacheUtil.Cache brandCache = CacheUtil.init(CachePrefixEnum.BRAND.getKey());
    protected static CacheUtil.Cache suppliersCache = CacheUtil.init(CachePrefixEnum.SUPPLIERS.getKey());


    /**
     * 插入品牌数据
     *
     * @param name 品牌名称
     */
    protected Integer insertBrand(String name) {
        CmsBrandsTempEntity entity = new CmsBrandsTempEntity();
        entity.setName(name);
        entity.setTag(BrandTagEnum.NEW.getCode());

        // 添加数据
        cmsBrandsTempService.insert(entity);

        // 缓存
        CacheUtil.Cache cache = CacheUtil.init(TableConstant.CMS_BRANDS_TEMP);
        cache.put(entity.getName(), entity.getBrandid());

        return entity.getBrandid();
    }

    /**
     * 组装阶梯价格数据
     *
     * @param min      最小值
     * @param max      最大值
     * @param price    进价
     * @param currency 货币1人民币2美元
     * @param position 位置
     *                 *@param isCN 标识
     */
    protected CmsPricesEntity createCmsPriceData(Integer min, Integer max, String price, Integer currency, Integer position, Boolean isCN, String goodid) {
        CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
        cmsPricesEntity.setLadderid(0);
//        cmsPricesEntity.setMin(min);
//        cmsPricesEntity.setMax(max == null ? 0 : max);
//        if (!Strings.isEmpty(price)) {
//            price = handlePriceBreak(price, isCN);
//            cmsPricesEntity.setPrice(BigDecimal.valueOf(Double.parseDouble(price)));
//        } else {
//            cmsPricesEntity.setPrice(BigDecimal.ZERO);
//        }
//        cmsPricesEntity.setGoodid(goodid);
//        cmsPricesEntity.setCurrency(currency);
//        cmsPricesEntity.setDiscountRate(BigDecimal.valueOf(0.00));
//        cmsPricesEntity.setPosition(position);
//        cmsPricesEntity.setPublish(1);
//        cmsPricesEntity.setCreatedAt(new Date(System.currentTimeMillis()));
        return cmsPricesEntity;
    }

    /**
     * 组装阶梯价格数据 中国
     *
     * @param min      最小值
     * @param max      最大值
     * @param price    进价
     * @param currency 货币1人民币2美元
     * @param position 位置
     */
    protected CmsPricesEntity createCNCmsPriceData(Integer min, Integer max, String price, Integer currency, Integer position,String goodid) {
        CmsPricesEntity cmsPricesEntity = new CmsPricesEntity();
        cmsPricesEntity.setLadderid(0);
//        cmsPricesEntity.setMin(min);
//        cmsPricesEntity.setMax(max == null ? 0 : max);
//        if (!Strings.isEmpty(price)) {
//            price = handleCNPriceBreak(price);
//            cmsPricesEntity.setPrice(BigDecimal.valueOf(Double.parseDouble(price)));
//        } else {
//            cmsPricesEntity.setPrice(BigDecimal.ZERO);
//        }
//        cmsPricesEntity.setGoodid(goodid);
//        cmsPricesEntity.setCurrency(currency);
//        cmsPricesEntity.setDiscountRate(BigDecimal.valueOf(0.00));
//        cmsPricesEntity.setPosition(position);
//        cmsPricesEntity.setPublish(1);
//        cmsPricesEntity.setCreatedAt(new Date(System.currentTimeMillis()));
        return cmsPricesEntity;
    }

    /**
     * 组装商品表-副表信息
     */
    protected CmsGoodsInformationEntity createCmsGoodsInformation(String goodid) {
        CmsGoodsInformationEntity cmsGoodsInformationEntity = new CmsGoodsInformationEntity();
        cmsGoodsInformationEntity.setGoodid(goodid);
        cmsGoodsInformationEntity.setActivity(0);
        cmsGoodsInformationEntity.setAtlas("");
        cmsGoodsInformationEntity.setEcnn(0);
        cmsGoodsInformationEntity.setTariffs(1);
        cmsGoodsInformationEntity.setInspectionCharges(1);
//        cmsGoodsInformationEntity.setInspectionChargesFee(BigDecimal.ZERO);
        cmsGoodsInformationEntity.setCreatedAt(new Date(System.currentTimeMillis()));
        return cmsGoodsInformationEntity;
    }

    /**
     * 处理阶梯价格，当前港币除以美元
     *
     * @param priceBreak 阶梯价格
     * @return 返回处理完的阶梯价格
     */
    protected String handlePriceBreak(String priceBreak, Boolean isCN) {
        double data = Double.parseDouble(priceBreak);//把阶梯价转为double
        if (isCN == true) {//通过标识确认数据来源
            double rate = 1 / RsSteppedPriceConstant.RATE;//汇率
            double newRate = data * rate;
            String nwePriceBreak = String.valueOf(newRate);
            return nwePriceBreak;
        }
        double rate = data / RsSteppedPriceConstant.RATE;//进行港币换算为美元
        String nwePriceBreak = String.valueOf(rate);
        return nwePriceBreak;
    }

    /**
     * 处理阶梯价格，人民币
     *
     * @param priceBreak 阶梯价格
     * @return 返回处理完的阶梯价格
     */
    protected String handleCNPriceBreak(String priceBreak) {
        double data = Double.parseDouble(priceBreak);//把阶梯价转为double
        double rate = 1 + RsSteppedPriceConstant.VATR;
        double newRate = data * rate;
        String nwePriceBreak = String.valueOf(newRate);
        return nwePriceBreak;
    }
}
