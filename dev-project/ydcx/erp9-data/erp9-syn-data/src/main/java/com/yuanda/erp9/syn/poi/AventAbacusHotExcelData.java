package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * AventAbacusHot数据解析
 *
 * @author liangjun
 */
@Data
public class AventAbacusHotExcelData {

    @ExcelProperty(index = 0)
    private String article;

    @ExcelProperty(index = 1)
    private String manuf;

    @ExcelProperty(index = 2)
    private String stock;

    @ExcelProperty(index = 3)
    private String price;

    @ExcelProperty(index = 4)
    private String currency;

    @ExcelProperty(index = 5)
    private String mpq;

    @ExcelProperty(index = 6)
    private String lt;

    @ExcelProperty(index = 7)
    private String articleMan;

    @ExcelProperty(index = 8)
    private String obs;

    @ExcelProperty(index = 9)
    private String abc;

    @ExcelProperty(index = 10)
    private String moq;

    @ExcelProperty(index = 11)
    private String priceClass;

    @ExcelProperty(index = 12)
    private String packType;

    @ExcelProperty(index = 13)
    private String roHs;

    @ExcelProperty(index = 14)
    private String techDescr;

    @ExcelProperty(index = 15)
    private String ltb;

    @ExcelProperty(index = 16)
    private String plant;

    @ExcelProperty(index = 17)
    private String sapMaterial;

    @ExcelProperty(index = 18)
    private String sapArticle;

}
