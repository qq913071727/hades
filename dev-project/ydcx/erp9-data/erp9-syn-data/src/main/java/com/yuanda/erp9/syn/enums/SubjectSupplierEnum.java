package com.yuanda.erp9.syn.enums;


/**
 * 供应商对应主题
 */
public enum SubjectSupplierEnum {
    AVENT_ASIA_HOT("AventAsiaHot", "Avnet-Hot", 1),
    AVENT_EBV_HOT("AventEbvHot", "Avnet-Hot", 1),
    AVENT_SILICA_HOT("AventSilicaHot", "Avnet-Hot", 1),
    FUTURE("Future Electronics", "Future Electronics", 2),
    CORESTAFF("Corestaff Stock List", "Corestaff", 1),
    CHIP1_ADIMAX("Chip1", "Kyber Electronics Company Ltd.", 1),
    AVENTABACUSHOT("AventAbacusHot", "Avnet-Hot", 1),
    VANLINKON("Vanlinkon", "Vanlinkon", 3),
    SELFSUPPORT("Ic360.cn", "Ic360.cn", 3),
    PEI_INVENTORY("pei_inventory", "PEI-Genesis", 3),
    /**
     * Aipco文件下载
     */
    AIPCO_ALL("aipco_all", "Aipco", 1),
    /**
     * Waldom文件下载
     */
    ANDA_INVENTORY_FEED("ANDA_Inventory_Feed", "北京英赛尔科技有限公司", 1),
    /**
     * ICGoo文件下载
     */
    WANGSQ("wangsq", "Cycle-chips Components Inc.", 1),
    AVENT("Avnet", "Avnet", 3),
    CHIP_1_STOP("Chip1Stop", "Chip1Stop", 3),
    ELEMENT14("Element14", "Element14", 3),
    RS_Components("RS_Components", "RS_Components", 3),
    ;

    /**
     * 主题
     */
    private String subject;

    /**
     * 供应商名字
     */
    private String supplier;

    /**
     * 系统主体类型 1：邮件 2：exe下载 3：URL下载
     */
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    SubjectSupplierEnum(String subject, String supplier, Integer type) {
        this.subject = subject;
        this.supplier = supplier;
        this.type = type;
    }

    /**
     * 主题
     *
     * @param subject
     * @return
     */
    public static String getSupplier(String subject) {
        for (SubjectSupplierEnum c : SubjectSupplierEnum.values()) {
            if (c.getSubject().equals(subject)) {
                return c.getSupplier();
            }
        }
        return null;
    }


    /**
     * 主题
     *
     * @param subject
     * @return
     */
    public static String getLikeSupplier(String subject) {
        for (SubjectSupplierEnum c : SubjectSupplierEnum.values()) {
            if (subject.contains(c.getSubject()) || c.getSubject().replace(" ", "").equals(subject)) {
                return c.getSupplier();
            }
        }
        return null;
    }

}