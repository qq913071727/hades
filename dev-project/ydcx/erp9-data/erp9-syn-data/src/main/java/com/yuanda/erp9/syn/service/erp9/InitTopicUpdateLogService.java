package com.yuanda.erp9.syn.service.erp9;

import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;

public interface InitTopicUpdateLogService {

    void initTopicUpdateLogTable();

    int insertEntity(CmsTopicUpdateLogEntity entity);

}
