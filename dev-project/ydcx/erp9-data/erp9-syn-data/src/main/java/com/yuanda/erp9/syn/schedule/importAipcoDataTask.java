package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.annotation.EnableSchedule;
import com.yuanda.erp9.syn.config.FileParamConfig;
import com.yuanda.erp9.syn.contant.DownloadUrlConstants;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.exception.TargetServerException;
import com.yuanda.erp9.syn.service.erp9.AipcoDataImportService;
import com.yuanda.erp9.syn.service.erp9.CmsSuppliersTempService;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import com.yuanda.erp9.syn.util.FileUtils;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;

@Component
public class importAipcoDataTask {
    private static final Logger log = LoggerFactory.getLogger(importAipcoDataTask.class);

    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private AipcoDataImportService aipcoDataImportService;

    @Resource
    private CmsTopicUpdateLogService cmsTopicUpdateLogService;

    @Resource
    private CmsSuppliersTempService cmsSuppliersTempService;

    /** 供应商名称 */
    private static final String SUPPLIER = "Aipco";

    /** 供应商id */
    private static final String SUPPLIER_ID = "US000000073";

    /**
     * 导入Aipco数据
     * 1. 通过下载地址获取文件
     * 2. 解析文件数据进行入库操作
     */
    @XxlJob("importAipcoDataJob")
    @EnableSchedule
    public void importAipcoData() {
        Integer supplierId = cmsSuppliersTempService.getSupplierIdByUs(SUPPLIER, SUPPLIER_ID);
        try {
            log.info("导入Aipco数据定时任务开始");
            log.info("1. 通过下载地址获取文件");
            // 1. 通过下载地址获取文件
            String filePath = FileUtils.downLoadFromUrl(DownloadUrlConstants.AIPCO_DOWN_FILE_URL, DownloadUrlConstants.AIPCO_DOWN_FILE_NAME, fileParamConfig.getDownloadPath());
            if(!Strings.isNullOrEmpty(filePath)){
                log.info("Aipco文件下载完成,文件保存路径:[{}]", filePath);

                // 更新系统主体更新日志记录表数据
                CmsTopicUpdateLogEntity entity = new CmsTopicUpdateLogEntity();
                entity.setSupplier(supplierId);
                entity.setCreateAt(new Date(System.currentTimeMillis()));
                entity.setSystemTopicType(SubjectSupplierEnum.AIPCO_ALL.getType());
                entity.setSystemTopic(SubjectSupplierEnum.AIPCO_ALL.getSupplier());
                Long topicUpdateLogId = cmsTopicUpdateLogService.updateOrInsertOfNon(entity);
                log.info("插入系统主体更新日志记录表数据完成");

                // 导入Aipco文件数据
                log.info("2. 解析文件数据进行入库操作");
                aipcoDataImportService.importCmsGoodsPojo(topicUpdateLogId, filePath);
            }
        } catch (Exception e) {
            log.info("处理Aipco供应商文件出现问题,原因:", e);
            throw new TargetServerException(e.getMessage(), supplierId, SubjectSupplierEnum.AIPCO_ALL.getSubject());
        }
    }
}