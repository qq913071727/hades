package com.yuanda.erp9.syn.service.erp9;

/**
 * @author zengqinglong
 * @desc Chip1Stop供应商数据导入
 * @Date 2022/12/2 14:58
 **/
public interface Chip1StopCsvImportService {
    /**
     * 分析Chip1Stop数据包
     *
     * @param topicUpdateLogId
     * @param filePath
     */
    void analysisChip1Stop(Long topicUpdateLogId, String filePath);
}
