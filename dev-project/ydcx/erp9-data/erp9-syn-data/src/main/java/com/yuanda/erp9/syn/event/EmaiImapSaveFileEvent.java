package com.yuanda.erp9.syn.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;

/**
 * @author B1B
 */
@Slf4j
public class EmaiImapSaveFileEvent extends ApplicationEvent {


    public EmaiImapSaveFileEvent(Object source) {
        super(source);
    }

}
