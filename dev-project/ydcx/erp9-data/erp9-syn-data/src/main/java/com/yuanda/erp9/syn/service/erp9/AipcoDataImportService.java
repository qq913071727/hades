package com.yuanda.erp9.syn.service.erp9;

/**
 * @desc   Aipco数据导入
 * @author linuo
 * @time   2022年11月28日10:57:32
 */
public interface AipcoDataImportService {
    /**
     * 数据插入
     * @param topicUpdateLogId 系统主体更新日志记录表id
     * @param filePath 文件路径
     */
    String importCmsGoodsPojo(Long topicUpdateLogId, String filePath);
}