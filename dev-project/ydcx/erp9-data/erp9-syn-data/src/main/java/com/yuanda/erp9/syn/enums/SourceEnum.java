package com.yuanda.erp9.syn.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   数据来源枚举
 * @author linuo
 * @time   2022年12月1日14:55:06
 */
public enum SourceEnum {
    DATA_PACKAGE(1,"数据包"),
    EMAIL(2,"邮件"),
    NEW(3,"实时");

    private Integer code;
    private String desc;

    SourceEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static SourceEnum of(Integer code) {
        for (SourceEnum c : SourceEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (SourceEnum c : SourceEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (SourceEnum e : SourceEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}