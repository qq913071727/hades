package com.yuanda.erp9.syn.service.erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateSumLogEntity;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateSumLogMapper;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateSumLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author linuo
 * @desc 系统主体更新数量日志记录表service实现类
 * @time 2023年4月25日11:45:29
 */
@Slf4j
@Service
public class CmsTopicUpdateSumLogServiceImpl implements CmsTopicUpdateSumLogService {
    @Autowired
    private CmsTopicUpdateSumLogMapper cmsTopicUpdateSumLogMapper;

    /**
     * 插入系统主体更新数量日志记录表数据
     *
     * @param sum              数据数量
     * @param topicUpdateLogId 主题更新日志表主键id
     * @return 返回插入结果
     */
    @Override
    public Integer insertBySum(Long sum, Long topicUpdateLogId) {
        CmsTopicUpdateSumLogEntity logEntity = new CmsTopicUpdateSumLogEntity();
        logEntity.setTopicUpdateLogId(topicUpdateLogId.intValue());
        logEntity.setUpdateSum(sum.intValue());
        logEntity.setCreateTime(new Date());
        return cmsTopicUpdateSumLogMapper.insert(logEntity);
    }

    /**
     * 插入或更新系统主体更新数量日志记录表数据
     *
     * @param sum
     * @param topicUpdateLogId
     * @return
     */
    @Override
    public Integer insertOrUpdateByTopicUpdateLogId(Long sum, Long topicUpdateLogId) {
        LambdaQueryWrapper<CmsTopicUpdateSumLogEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CmsTopicUpdateSumLogEntity::getTopicUpdateLogId, topicUpdateLogId);
        wrapper.orderByDesc(CmsTopicUpdateSumLogEntity::getId);
        wrapper.last("limit 1");
        CmsTopicUpdateSumLogEntity res = cmsTopicUpdateSumLogMapper.selectOne(wrapper);
        if (res != null) {
            res.setUpdateSum(res.getUpdateSum() + sum.intValue());
            int i = cmsTopicUpdateSumLogMapper.updateById(res);
            if (i > 0) {
                log.info("系统主体更新数量日志记录表数据完成");
            }
            return i;
        }

        CmsTopicUpdateSumLogEntity cmsTopicUpdateSumLogEntity = new CmsTopicUpdateSumLogEntity();
        cmsTopicUpdateSumLogEntity.setTopicUpdateLogId(topicUpdateLogId.intValue());
        cmsTopicUpdateSumLogEntity.setUpdateSum(sum.intValue());
        cmsTopicUpdateSumLogEntity.setCreateTime(new Date(System.currentTimeMillis()));
        int insert = cmsTopicUpdateSumLogMapper.insert(cmsTopicUpdateSumLogEntity);
        if (insert > 0) {
            log.info("系统主体更新数量日志记录表数据插入完成");
        }
        return insert;
    }
}
