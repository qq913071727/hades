package com.yuanda.erp9.syn.service.erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateLogEntity;
import com.yuanda.erp9.syn.enums.CmsTopicUpdatedStatusEnum;
import com.yuanda.erp9.syn.enums.SubjectSupplierEnum;
import com.yuanda.erp9.syn.mapper.erp9.CmsSuppliersTempMapper;
import com.yuanda.erp9.syn.mapper.erp9.CmsTopicUpdateLogMapper;
import com.yuanda.erp9.syn.service.erp9.CmsTopicUpdateLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @desc   系统主体更新日志记录表Service实现类
 * @author linuo
 * @time   2022年12月13日13:35:44
 */
@Service
@Slf4j
public class CmsTopicUpdateLogServiceImpl extends CsvSaveImpl implements CmsTopicUpdateLogService {
    @Resource
    private CmsTopicUpdateLogMapper cmsTopicUpdateLogMapper;

    @Resource
    private CmsSuppliersTempMapper cmsSuppliersTempMapper;

    /**
     * 通过系统主题修改是否有更新字段
     * @param systemTopic 系统主题
     * @return 返回修改结果
     */
    @Override
    public int editUpdatedBySystemTopic(String systemTopic) {
        return cmsTopicUpdateLogMapper.editUpdatedBySystemTopic(systemTopic);
    }

    /**
     * 插入系统主体更新日志记录表数据
     * @param subjectSupplierEnum 系统主题枚举
     * @return 返回插入结果
     */
    @Override
    public int insertBySystemTopic(SubjectSupplierEnum subjectSupplierEnum) {
        // 根据名称获取id
        Integer id = cmsSuppliersTempMapper.selectIdByName(subjectSupplierEnum.getSupplier());
        if(id != null){
            CmsTopicUpdateLogEntity cmsTopicUpdateLogEntity = new CmsTopicUpdateLogEntity();
            cmsTopicUpdateLogEntity.setSystemTopic(subjectSupplierEnum.getSupplier());
            cmsTopicUpdateLogEntity.setSupplier(id);
            cmsTopicUpdateLogEntity.setSystemTopicType(subjectSupplierEnum.getType());
            cmsTopicUpdateLogEntity.setUpdated(1);
            cmsTopicUpdateLogEntity.setCreateAt(new Date(System.currentTimeMillis()));
            return cmsTopicUpdateLogMapper.insert(cmsTopicUpdateLogEntity);
        }
        return 0;
    }

    /**
     * @Description: 已初始化就更新否则新增（0无数据，1有数据）
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/317:28
     */
    @Override
    public Long updateOrInsertOfNon(CmsTopicUpdateLogEntity paramEntity) {
        long pk = 0;
        QueryWrapper<CmsTopicUpdateLogEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("supplier", paramEntity.getSupplier());
        wrapper.orderByDesc("id");
        wrapper.last("limit 1");
        CmsTopicUpdateLogEntity res = cmsTopicUpdateLogMapper.selectOne(wrapper);
        if(null != res){
            if(res.getUpdated().equals(CmsTopicUpdatedStatusEnum.NO.getCode())){
                // 更新
                paramEntity.setUpdateAt(LocalDateTime.now().toString());
                LambdaQueryWrapper<CmsTopicUpdateLogEntity> update = new LambdaQueryWrapper<>();
                update.eq(CmsTopicUpdateLogEntity::getId,res.getId());
                cmsTopicUpdateLogMapper.update(paramEntity,update);
                pk = res.getId();
            }else {
                // 新增
                paramEntity.setUpdated(CmsTopicUpdatedStatusEnum.YES.getCode());
                cmsTopicUpdateLogMapper.insert(paramEntity);
                pk = paramEntity.getId();
            }
        }
        return pk;
    }
}