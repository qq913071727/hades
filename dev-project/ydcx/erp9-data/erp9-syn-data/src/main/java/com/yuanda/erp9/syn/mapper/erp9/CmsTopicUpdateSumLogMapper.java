package com.yuanda.erp9.syn.mapper.erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.CmsTopicUpdateSumLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @desc   系统主体更新数量日志记录表mapper
 * @author linuo
 * @time   2023年4月25日11:46:24
 */
@Mapper
public interface CmsTopicUpdateSumLogMapper extends BaseMapper<CmsTopicUpdateSumLogEntity> {

}
