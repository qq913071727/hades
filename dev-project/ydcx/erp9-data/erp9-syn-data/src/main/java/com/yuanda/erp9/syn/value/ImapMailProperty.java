package com.yuanda.erp9.syn.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author B1B
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "imap.emai")
public class ImapMailProperty {

    private String server;

    private int port;

    private String userName;

    private String token;

    private String savePath;


}
