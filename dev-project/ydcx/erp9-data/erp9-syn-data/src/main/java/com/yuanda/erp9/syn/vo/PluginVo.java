package com.yuanda.erp9.syn.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zengqinglong
 * @desc plugin表入参
 * @Date 2022/12/16 11:13
 **/
public class PluginVo implements Serializable {
    private String operate;

    private String id;

    private String partNumber;

    private String manufacturer;

    private BigDecimal vatRate;

    private BigDecimal tariffRate;

    private BigDecimal exciseTaxRate;

    private BigDecimal cIQprice;

    private BigDecimal addedTariffRate;

    public String getOperate() {
        return operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BigDecimal getVatRate() {
        return vatRate;
    }

    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    public BigDecimal getTariffRate() {
        return tariffRate;
    }

    public void setTariffRate(BigDecimal tariffRate) {
        this.tariffRate = tariffRate;
    }

    public BigDecimal getExciseTaxRate() {
        return exciseTaxRate;
    }

    public void setExciseTaxRate(BigDecimal exciseTaxRate) {
        this.exciseTaxRate = exciseTaxRate;
    }

    public BigDecimal getcIQprice() {
        return cIQprice;
    }

    public void setcIQprice(BigDecimal cIQprice) {
        this.cIQprice = cIQprice;
    }

    public BigDecimal getAddedTariffRate() {
        return addedTariffRate;
    }

    public void setAddedTariffRate(BigDecimal addedTariffRate) {
        this.addedTariffRate = addedTariffRate;
    }
}
