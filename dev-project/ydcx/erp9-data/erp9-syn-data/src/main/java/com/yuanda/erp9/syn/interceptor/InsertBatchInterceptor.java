package com.yuanda.erp9.syn.interceptor;

import com.yuanda.erp9.syn.annotation.InsertBatch;
import com.yuanda.erp9.syn.service.erp9.GoodsService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @ClassName InsertBatchInterceptor
 * @Description
 * @Date 2022/11/16
 * @Author myq
 */
@Component
@Aspect
public class InsertBatchInterceptor {

    @Autowired
    private GoodsService goodsService;

    /**
     * @Description: 切点
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1611:31
     */
    @Pointcut("@annotation(com.yuanda.erp9.syn.annotation.InsertBatch)")
    public void pointcut() {
    }


    /**
     * @Description: 环绕通知
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/1611:31
     */
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        // 获取方法签名
        MethodSignature ms = (MethodSignature) point.getSignature();
        Method method = ms.getMethod();
        // 获取注解
        InsertBatch insertBatch = (InsertBatch) method.getAnnotation(InsertBatch.class);
        // 执行方法之前
        if (insertBatch != null) {
            goodsService.insertBatchBeforeSet();
        }
        //执行方法
        Object result = point.proceed();
        // 执行方法之后
        if (insertBatch != null) {
            goodsService.insertBatchAfterReset();
        }

        return result;
    }
}