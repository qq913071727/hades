package com.yuanda.erp9.syn.service.erp9.impl;

import com.yuanda.erp9.syn.service.erp9.Subject;

/**
 * @ClassName AbstractSubject
 * @Description
 * @Date 2022/11/15
 * @Author myq
 */
public abstract class AbstractSubject implements Subject {

    private volatile Syn syn;

    public Syn getSyn() {
        return syn;
    }

    public void setSyn(Syn syn) {
        this.syn = syn;
    }

    /**
     * 内部枚举类，用来保存同步状态
     */
    public enum Syn {

        /**
         * 不需要同步
         */
        NONE,

        /**
         * 需要同步
         */
        CONTAIN,

        /**
         * 同步错误
         */
        ERROR,
        ;

    }
}
