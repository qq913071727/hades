package com.yuanda.erp9.syn.service.erp9;

import java.util.Map;

/**
 * 解析excel
 */
public interface PeiGenesisAnalyeService {
    /**
     * 解析PeiGenesis数据包
     * @param paramMap
     */
    void analysisPeiGenesis(Map<String,String> paramMap);


}
