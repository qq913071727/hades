package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zengqinglong
 * @desc chip1stop
 * @Date 2022/12/2 15:42
 **/
@Data
@Slf4j
public class Chip1StopCsvData {
    @ExcelProperty(index = 0)
    private String manufacturer;

    @ExcelProperty(index = 1)
    private String mpn;

    @ExcelProperty(index = 2)
    private String sku;

    @ExcelProperty(index = 3)
    private String quantity;

    @ExcelProperty(index = 4)
    private String priceBreak1;

    @ExcelProperty(index = 5)
    private String priceUsd1;

    @ExcelProperty(index = 6)
    private String priceBreak2;

    @ExcelProperty(index = 7)
    private String priceUsd2;

    @ExcelProperty(index = 8)
    private String priceBreak3;

    @ExcelProperty(index = 9)
    private String priceUsd3;

    @ExcelProperty(index = 10)
    private String priceBreak4;

    @ExcelProperty(index = 11)
    private String priceUsd4;

    @ExcelProperty(index = 12)
    private String priceBreak5;

    @ExcelProperty(index = 13)
    private String priceUsd5;

    @ExcelProperty(index = 14)
    private String priceBreak6;

    @ExcelProperty(index = 15)
    private String priceUsd6;

    @ExcelProperty(index = 16)
    private String rohs;

    @ExcelProperty(index = 17)
    private String pb;

    @ExcelProperty(index = 18)
    private String packaging;

    @ExcelProperty(index = 19)
    private String category;

    @ExcelProperty(index = 20)
    private String description;

    @ExcelProperty(index = 21)
    private String moq;

    @ExcelProperty(index = 22)
    private String spq;

    @ExcelProperty(index = 23)
    private String leadTime;

    private String signIndex;

    private String hisp;

    private Integer supplier;
    public static Chip1StopCsvData toChip1StopCsvDataFormat(String[] data) {
        try {
            Chip1StopCsvData chip1StopCsvData = new Chip1StopCsvData();
            chip1StopCsvData.setManufacturer(data[0]);
            chip1StopCsvData.setMpn(data[1]);
            chip1StopCsvData.setSku(data[2]);
            chip1StopCsvData.setQuantity(data[3]);
            chip1StopCsvData.setPriceBreak1(data[4]);
            chip1StopCsvData.setPriceUsd1(data[5]);
            chip1StopCsvData.setPriceBreak2(data[6]);
            chip1StopCsvData.setPriceUsd2(data[7]);
            chip1StopCsvData.setPriceBreak3(data[8]);
            chip1StopCsvData.setPriceUsd3(data[9]);
            chip1StopCsvData.setPriceBreak4(data[10]);
            chip1StopCsvData.setPriceUsd4(data[11]);
            chip1StopCsvData.setPriceBreak5(data[12]);
            chip1StopCsvData.setPriceUsd5(data[13]);
            chip1StopCsvData.setPriceBreak6(data[14]);
            chip1StopCsvData.setPriceUsd6(data[15]);
            chip1StopCsvData.setRohs(data[16]);
            chip1StopCsvData.setPb(data[17]);
            chip1StopCsvData.setPackaging(data[18]);
            chip1StopCsvData.setCategory(data[19]);
            chip1StopCsvData.setDescription(data[20]);
            chip1StopCsvData.setMoq(data[21]);
            chip1StopCsvData.setSpq(data[22]);
            chip1StopCsvData.setLeadTime(data[23]);
            return chip1StopCsvData;
        } catch (Exception e) {
            log.info("Chip1Stop解析异常,放弃当前这条数据");
            return null;
        }
    }
}
