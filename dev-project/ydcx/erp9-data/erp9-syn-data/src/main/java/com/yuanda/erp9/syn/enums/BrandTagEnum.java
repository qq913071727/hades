package com.yuanda.erp9.syn.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   品牌标记枚举
 * @author linuo
 * @time   2022年11月28日14:00:03
 */
public enum BrandTagEnum {
    EXIST(0,"已有"),
    NEW(1,"新建");

    private Integer code;
    private String desc;

    BrandTagEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static BrandTagEnum of(Integer code) {
        for (BrandTagEnum c : BrandTagEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (BrandTagEnum c : BrandTagEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (BrandTagEnum e : BrandTagEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}