package com.yuanda.erp9.syn.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author B1B
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "peiinventory.price")
public class PeiInventoryProperty {

    private String downloadPath;
    private String file;
}
