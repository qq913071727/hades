package com.yuanda.erp9.syn.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.yuanda.erp9.syn.service.erp9.ImapReceiveMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时下载解析邮箱文件
 */
@Slf4j
@Component
public class ImportDownloadMailTask {
    @Autowired
    private ImapReceiveMailService imapReceiveMailService;

    /**
     * 定时下载邮件
     */
    //@Scheduled(cron = "*/5 * * * * ?")
    @XxlJob("downloadImapMailTask")
    public void downloadImapMailTask() {
        imapReceiveMailService.downloadImapMail();
    }
}