package com.yuanda.erp9.syn.service.rate;

import com.yuanda.erp9.syn.entity.StandardPartnumbersForPluginEntity;
import com.yuanda.erp9.syn.pojo.StandardPartnumbersForPluginDataPojo;
import java.util.List;

/**
 * @desc   型号汇率service
 * @author linuo
 * @time   2022年12月15日16:40:03
 */
public interface StandardPartnumbersForPluginService {
    /**
     * 根据品牌、型号获取数据
     * @param brand 品牌
     * @param partNumber 型号
     * @return 返回查询结果
     */
    StandardPartnumbersForPluginEntity getDataByBrandPartNumber(String brand, String partNumber);

    /**
     * 查询所有数据
     * @return 返回查询结果
     */
    List<StandardPartnumbersForPluginDataPojo> selectList();
}