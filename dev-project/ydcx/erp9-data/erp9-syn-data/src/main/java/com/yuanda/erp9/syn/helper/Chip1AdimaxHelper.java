package com.yuanda.erp9.syn.helper;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.yuanda.erp9.syn.util.MathUtil;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName Chip1AdimaxHelper
 * @Description
 * @Date 2023/2/8
 * @Author myq
 */
@Component
public class Chip1AdimaxHelper {


    /**
     * @Description: 计算阶梯价格
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/810:43
     */
    public <T> List<Map<String, String>> getLadderPrice(List<String> countList, List<String> priceList) {
        final double rate = 1.025;
        int index = 0;
        int len = countList.stream().filter(Objects::nonNull).collect(Collectors.toList()).size();
        List<Map<String, String>> ladderPriceList = new ArrayList<>();
        for (int x = 0; x < len; x++) {
            Map<String, String> priceMap = new HashMap<>();
            String count = countList.get(x);
            String multiply = MathUtil.multiply(priceList.get(x), rate);
            priceMap.put("discount_rate", "0.0000");
            priceMap.put("discount_price", "0.0000");
            priceMap.put("min", count);
            if (x == len - 1) {
                priceMap.put("max", "0");
            } else {
                priceMap.put("max", (StringUtils.isNotEmpty(countList.get(x + 1)))? countList.get(x + 1) : "0" );
            }
            priceMap.put("price", multiply);
            priceMap.put("currency", "1");
            priceMap.put("position", index++ + "");
            ladderPriceList.add(priceMap);
        }
        return ladderPriceList;
    }
}
