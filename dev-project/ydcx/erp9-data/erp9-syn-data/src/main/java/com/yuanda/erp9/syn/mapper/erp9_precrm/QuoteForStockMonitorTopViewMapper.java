package com.yuanda.erp9.syn.mapper.erp9_precrm;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuanda.erp9.syn.entity.erp9_precrm.QuoteForStockMonitorTopViewEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuoteForStockMonitorTopViewMapper extends BaseMapper<QuoteForStockMonitorTopViewEntity> {
}