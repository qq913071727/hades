package com.yuanda.erp9.syn.poi;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

/**
 * Silica数据解析
 *
 * @author liangjun
 */
@Data
public class SilicaExcelData {

    @ExcelProperty(index = 0)
    private String material;

    @ExcelProperty(index = 1)
    private String description;

    @ExcelProperty(index = 2)
    private String leadtime;

    @ExcelProperty(index = 3)
    private String qas;

    @ExcelProperty(index = 4)
    private String pack;

    @ExcelProperty(index = 5)
    @NumberFormat(value = "0.00000000000000000000")
    private String resaleUSDpce;

    @ExcelProperty(index = 6)
    private String name;

    @ExcelProperty(index = 7)
    private String technicalText;


    @ExcelProperty(index = 8)
    private String green;


}
