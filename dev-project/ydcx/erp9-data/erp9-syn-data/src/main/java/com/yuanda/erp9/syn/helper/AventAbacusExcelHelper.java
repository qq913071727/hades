package com.yuanda.erp9.syn.helper;


import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName AventAbacusExcelHelper
 * @Description
 * @Date 2022/12/6
 * @Author myq
 */
@Component
public class AventAbacusExcelHelper extends JsonConfigHelper {

    private final String fileName = "json/AventAbacusConfig.json";

    /**
     * @Description: 货期
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/218:08
     */
    public String getDelivery(String key) {
        String jsonString = super.getJSONString(fileName, key);
        return jsonString;
    }


    /**
     * @Description: 获取阶梯价格
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/12/114:40
     */
    public List<Map<String, String>> getLadderPrice(int packQty, BigDecimal price, String priceType) {
        // 阶梯价格
        List<Map<String, String>> resPriceList = new ArrayList<>(6);
        Map<String, String> priceMap = new LinkedHashMap<>();
        // 第一个阶梯数量
        priceMap.put("min", String.valueOf(packQty));
        // 0无限大
        priceMap.put("max", "0");
        // 阶梯价格
        priceMap.put("price", String.valueOf(price));
        // 1RMB 2USD
        priceMap.put("currency", "USD".equals(priceType) ? "2" : "1");
        priceMap.put("position", "1");
        priceMap.put("discount_rate", "0.0000");
        priceMap.put("discount_price", "0.0000");
        resPriceList.add(priceMap);

        return resPriceList;
    }
}
