package com.yuanda.erp9.syn.util;

import com.alibaba.fastjson.JSONObject;
import com.yuanda.erp9.syn.contant.DatabaseOperationType;
import com.yuanda.erp9.syn.contant.RateCalculateConstants;
import com.yuanda.erp9.syn.enums.CachePrefixEnum;
import com.yuanda.erp9.syn.vo.PluginVo;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author linuo
 * @desc   费率计算工具类
 * @time   2022年12月15日16:29:57
 */
@Component
public class RateCalculateUtil {
    private static final Logger log = LoggerFactory.getLogger(RateCalculateUtil.class);
    protected static CacheUtil.ObjectCache exchangeRates = CacheUtil.objectInit(CachePrefixEnum.EXCHANGE_RATES.getKey());
    protected static CacheUtil.ObjectCache standardPartnumbersForPlugin = CacheUtil.objectInit(CachePrefixEnum.STANDARD_PARTNUMBER_SFORPLUGIN.getKey());
    private static BigDecimal value = null;

    /**
     * 根据品牌、型号获取费率
     * @param brand 品牌
     * @param partNumber 型号
     * @return 返回费率
     */
    public static JSONObject getRate(String brand, String partNumber) {
        String VATRate = RateCalculateConstants.ZERO;
        String TariffRate = RateCalculateConstants.ZERO;
        String ExciseTaxRate = RateCalculateConstants.ZERO;
        String CIQprice = RateCalculateConstants.ZERO;
        String AddedTariffRate = RateCalculateConstants.ZERO;
        String CustomsExchangeRate;
        String Ccc = RateCalculateConstants.ZERO;
        String Embargo = RateCalculateConstants.ZERO;
        String Eccn = "";

        if (!Strings.isNullOrEmpty(brand) && !Strings.isNullOrEmpty(partNumber)) {
            Object o1 = standardPartnumbersForPlugin.get(partNumber.toLowerCase().trim());
            if (o1 != null) {
                Map<String, String> rateMap = (Map<String, String>) o1;
                VATRate = rateMap.get(RateCalculateConstants.VATRATE_FIELD);
                TariffRate = rateMap.get(RateCalculateConstants.TARIFFRATE_FIELD);
                ExciseTaxRate = rateMap.get(RateCalculateConstants.EXCISETAXRATE_FIELD);
                CIQprice = rateMap.get(RateCalculateConstants.CIQPRICE_FIELD);
                AddedTariffRate = rateMap.get(RateCalculateConstants.ADDEDTARIFFRATE_FIELD);
                Ccc = rateMap.get(RateCalculateConstants.CCC_FIELD);
                Embargo = rateMap.get(RateCalculateConstants.EMBARGO_FIELD);
                Eccn = rateMap.get(RateCalculateConstants.ECCN_FIELD);
            }
        }

        // 处理汇率
        if (value == null) {
            Object o = exchangeRates.get(CachePrefixEnum.EXCHANGE_RATES.getKey());
            if (o != null) {
                value = (BigDecimal) o;
            }
        }
        CustomsExchangeRate = value.setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();

        // 组装费率json数据
        JSONObject rateJson = new JSONObject();
        rateJson.put(RateCalculateConstants.VATRATE_FIELD, VATRate);
        rateJson.put(RateCalculateConstants.TARIFFRATE_FIELD, TariffRate);
        rateJson.put(RateCalculateConstants.EXCISETAXRATE_FIELD, ExciseTaxRate);
        rateJson.put(RateCalculateConstants.CIQPRICE_FIELD, CIQprice);
        rateJson.put(RateCalculateConstants.ADDEDTARIFFRATE_FIELD, AddedTariffRate);
        rateJson.put(RateCalculateConstants.CUSTOMSEXCHANGERATE_FIELD, CustomsExchangeRate);

        // 组装返回信息
        JSONObject result = new JSONObject();
        result.put(RateCalculateConstants.RATE, rateJson);
        result.put(RateCalculateConstants.CCC_FIELD, Ccc);
        result.put(RateCalculateConstants.EMBARGO_FIELD, Embargo);
        result.put(RateCalculateConstants.ECCN_FIELD, Eccn);
        return result;
    }

    /**
     * 获取汇率
     * @param pluginVo plugin表入参
     * @return 返回处理结果
     */
    public static String getUpdateOrInset(PluginVo pluginVo) {
        String VATRate = pluginVo.getVatRate().setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();
        String TariffRate = pluginVo.getTariffRate().setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();
        String ExciseTaxRate = pluginVo.getExciseTaxRate().setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();
        String CIQprice = pluginVo.getcIQprice().setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();
        String AddedTariffRate = pluginVo.getAddedTariffRate().setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();
        JSONObject json = new JSONObject();
        if (value == null) {
            Object o = exchangeRates.get(CachePrefixEnum.EXCHANGE_RATES.getKey());
            if (o != null) {
                value = (BigDecimal) o;
            }
        }
        String CustomsExchangeRate = value.setScale(7, BigDecimal.ROUND_HALF_UP).toPlainString();
        json.put(RateCalculateConstants.VATRATE_FIELD, VATRate);
        json.put(RateCalculateConstants.TARIFFRATE_FIELD, TariffRate);
        json.put(RateCalculateConstants.EXCISETAXRATE_FIELD, ExciseTaxRate);
        json.put(RateCalculateConstants.CIQPRICE_FIELD, CIQprice);
        json.put(RateCalculateConstants.ADDEDTARIFFRATE_FIELD, AddedTariffRate);
        json.put(RateCalculateConstants.CUSTOMSEXCHANGERATE_FIELD, CustomsExchangeRate);
        Map<String, String> rateMap = new HashMap<>();
        rateMap.put(RateCalculateConstants.VATRATE_FIELD, VATRate);
        rateMap.put(RateCalculateConstants.TARIFFRATE_FIELD, TariffRate);
        rateMap.put(RateCalculateConstants.EXCISETAXRATE_FIELD, ExciseTaxRate);
        rateMap.put(RateCalculateConstants.CIQPRICE_FIELD, CIQprice);
        rateMap.put(RateCalculateConstants.ADDEDTARIFFRATE_FIELD, AddedTariffRate);
        //更新缓存
        if (pluginVo.getOperate().equals(DatabaseOperationType.INSERT) || pluginVo.getOperate().equals(DatabaseOperationType.UPDATE)) {
            standardPartnumbersForPlugin.put(pluginVo.getPartNumber().toLowerCase().trim(), rateMap);
        } else {
            standardPartnumbersForPlugin.del(pluginVo.getPartNumber().toLowerCase().trim());
        }
        return json.toJSONString();
    }

    /**
     * 删除汇率都为零
     * @return
     */
    public static String getDel() {
        JSONObject json = new JSONObject();
        json.put(RateCalculateConstants.VATRATE_FIELD, RateCalculateConstants.ZERO);
        json.put(RateCalculateConstants.TARIFFRATE_FIELD, RateCalculateConstants.ZERO);
        json.put(RateCalculateConstants.EXCISETAXRATE_FIELD, RateCalculateConstants.ZERO);
        json.put(RateCalculateConstants.CIQPRICE_FIELD, RateCalculateConstants.ZERO);
        json.put(RateCalculateConstants.ADDEDTARIFFRATE_FIELD, RateCalculateConstants.ZERO);
        json.put(RateCalculateConstants.CUSTOMSEXCHANGERATE_FIELD, RateCalculateConstants.ZERO);
        return json.toJSONString();
    }

    /**
     * 更新缓存
     *
     * @param effectiveData
     */
    public static void updateRate(BigDecimal effectiveData) {
        // 缓存
        exchangeRates.put(CachePrefixEnum.EXCHANGE_RATES.getKey(), effectiveData);
    }
}