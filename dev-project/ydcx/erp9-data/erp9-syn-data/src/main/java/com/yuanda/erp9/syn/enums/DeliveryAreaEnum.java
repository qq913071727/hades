package com.yuanda.erp9.syn.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   交货地区枚举
 * @author linuo
 * @time   2022年11月28日14:00:03
 */
public enum DeliveryAreaEnum {
    DOMESTIC("0","国内"),
    OTHER("0,1","国内,香港");

    private String code;
    private String desc;

    DeliveryAreaEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static DeliveryAreaEnum of(String code) {
        for (DeliveryAreaEnum c : DeliveryAreaEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(String code) {
        for (DeliveryAreaEnum c : DeliveryAreaEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (DeliveryAreaEnum e : DeliveryAreaEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}