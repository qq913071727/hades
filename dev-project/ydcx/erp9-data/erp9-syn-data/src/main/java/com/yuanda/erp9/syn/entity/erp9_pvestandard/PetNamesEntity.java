package com.yuanda.erp9.syn.entity.erp9_pvestandard;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 品牌别名表
 * @Date 2023/4/12 14:29
 **/
@ApiModel(value = "PetNames", description = "品牌别名表")
@TableName("PetNames")
@Data
public class PetNamesEntity {
    @ApiModelProperty(value = "主键")
    @TableField(value = "ID")
    private String ID;

    @ApiModelProperty(value = "别名类型")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "名称")
    @TableField(value = "OriginName")
    private String originName;

    @ApiModelProperty(value = "别名")
    @TableField(value = "Naming")
    private String naming;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "CreatorID")
    private String creatorId;

    @TableField(value = "Classify")
    private String classify;

}
