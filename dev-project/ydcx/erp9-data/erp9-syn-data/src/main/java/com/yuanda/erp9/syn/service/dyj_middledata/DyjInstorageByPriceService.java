package com.yuanda.erp9.syn.service.dyj_middledata;

import com.yuanda.erp9.syn.entity.dyj_middledata.DyjInstorageByPriceEntity;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 大赢家库存价格预警读表管理
 * @Date 2023/4/7 14:49
 **/
public interface DyjInstorageByPriceService {
    /**
     * 获取大赢家读表所有内容
     *
     * @return
     */
    List<DyjInstorageByPriceEntity> findAll();

}
