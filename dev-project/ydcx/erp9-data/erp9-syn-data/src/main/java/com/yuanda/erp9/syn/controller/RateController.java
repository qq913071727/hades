package com.yuanda.erp9.syn.controller;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.yuanda.erp9.syn.config.ThreadPoolManager;
import com.yuanda.erp9.syn.helper.AsyncRateHelper;
import com.yuanda.erp9.syn.pojo.variable.ApiResult;
import com.yuanda.erp9.syn.vo.PluginVo;
import com.yuanda.erp9.syn.vo.RateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author zengqinglong
 * @desc 汇率 管理
 * @Date 2022/12/15 15:54
 **/
@Slf4j
@RestController
@Api(value = "汇率API", tags = "RateController")
@RequestMapping("/api/rate")
public class RateController {

    @Resource
    private AsyncRateHelper asyncRateHelper;

    /**
     * 汇率表操作
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "汇率管理：rate表操作")
    @RequestMapping(value = "/rateTableOperate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> rateTableOperate(@RequestBody RateVo rateVo, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(rateVo.getOperate())) {
            String message = "调用接口【/api/rate/rateTableOperate时，参数Operate不能为空";
            log.info(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        //异步处理rate
        asyncRateHelper.handleRate(rateVo);

        log.info("汇率管理：rate表操作接口调用成功,rateVo:{}", JSON.toJSON(rateVo));
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("接口调用成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * plugin管理
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "plugin管理：plugin表操作")
    @RequestMapping(value = "/pluginTableOperate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> pluginTableOperate(@RequestBody PluginVo pluginVo, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(pluginVo.getOperate())) {
            String message = "调用接口【/api/rate/pluginTableOperate时，参数Operate不能为空";
            log.info(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //异步处理plugin
        asyncRateHelper.handlePlugin(pluginVo);

        log.info("plugin管理：plugin表操作,pluginVo:{}", JSON.toJSON(pluginVo));
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("接口调用成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
