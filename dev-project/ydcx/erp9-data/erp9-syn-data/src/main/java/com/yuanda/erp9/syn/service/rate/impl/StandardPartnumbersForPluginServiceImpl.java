package com.yuanda.erp9.syn.service.rate.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yuanda.erp9.syn.entity.StandardPartnumbersForPluginEntity;
import com.yuanda.erp9.syn.mapper.rate.StandardPartnumbersForPluginMapper;
import com.yuanda.erp9.syn.pojo.StandardPartnumbersForPluginDataPojo;
import com.yuanda.erp9.syn.service.rate.StandardPartnumbersForPluginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class StandardPartnumbersForPluginServiceImpl implements StandardPartnumbersForPluginService {
    @Resource
    private StandardPartnumbersForPluginMapper standardPartnumbersForPluginMapper;

    /**
     * 根据品牌、型号获取数据
     * @param brand 品牌
     * @param partNumber 型号
     * @return 返回查询结果
     */
    @Override
    public StandardPartnumbersForPluginEntity getDataByBrandPartNumber(String brand, String partNumber) {
        QueryWrapper<StandardPartnumbersForPluginEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("Manufacturer", brand);
        queryWrapper.eq("PartNumber", partNumber);
        return standardPartnumbersForPluginMapper.selectOne(queryWrapper);
    }

    /**
     * 查询所有数据
     * @return 返回查询结果
     */
    @Override
    public List<StandardPartnumbersForPluginDataPojo> selectList() {
        return standardPartnumbersForPluginMapper.selectList();
    }
}