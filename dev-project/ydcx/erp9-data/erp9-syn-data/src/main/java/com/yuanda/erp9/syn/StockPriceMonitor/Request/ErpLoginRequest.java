package com.yuanda.erp9.syn.StockPriceMonitor.Request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zengqinglong
 * @desc 登陆请求
 * @Date 2023/4/6 10:51
 **/
@Data
public class ErpLoginRequest implements Serializable {
    private String UserName;
    private String Passwrod;
}
