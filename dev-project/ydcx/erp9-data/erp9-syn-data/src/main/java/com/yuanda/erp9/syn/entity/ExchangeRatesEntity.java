package com.yuanda.erp9.syn.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@TableName("ExchangeRates")
@Data
public class ExchangeRatesEntity implements Serializable {
    @ApiModelProperty(value = "海关 浮动")
    @TableField(value = "Type")
    private String type;

    @TableField(value = "District")
    private Integer district;

    @TableField(value = "From")
    private Integer from;

    @TableField(value = "To")
    private Integer to;

    @TableField(value = "Value")
    private Float value;

    @ApiModelProperty(value = "预设汇率的开始时间")
    @TableField(value = "StartDate")
    private Date startDate;

    @ApiModelProperty(value = "最后更新时间")
    @TableField(value = "ModifyDate")
    private Date modifyDate;
}