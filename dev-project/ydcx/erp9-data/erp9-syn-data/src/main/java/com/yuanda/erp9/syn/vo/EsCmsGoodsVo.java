package com.yuanda.erp9.syn.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zengqinglong
 * @desc es 返回实体
 * @Date 2023/4/7 13:16
 **/
@Data
public class EsCmsGoodsVo implements Serializable {
    private String goodid;
    private Integer adminid;
    private Integer tenantsid;
    private String incodes;
    private Integer classifyid;
    private String img;
    private String type;
    private Integer type_id;
    private Integer status;
    private String status_msg;
    private String data_type;
    private Integer data_type_id;
    private String model;
    private String brand;
    private Integer brand_id;
    private String supplier;
    private Integer supplier_id;
    private Integer is_sample_apply;
    private String batch;
    private Integer original_price;
    private Integer purchase_price;
    private double unit_price;
    private Integer repertory;
    private String repertory_area;
    private Integer publish;
    private String describe;
    private String files;
    private Integer pay_type;
    private Integer minimum_order;
    private Integer incremental;
    private Integer mpq;
    private String delivery_area;
    private String domestic_date;
    private String hongkong_date;
    private String hisp;
    private Integer ccc;
    private Integer sales;
    private Integer click;
    private Integer visitor;
    private String deleted_at;
    private String created_at;
    private String updated_at;
    private Integer activity_id;
    private String atlas;
    private String content;
    private String eccn_no;
    private double tariffs_rate;
    private double inspection_charges_fee;
    private String params;
    private String prices;
    private Integer source;
    private String rates;
    private String package_;
    private String packing;
}
