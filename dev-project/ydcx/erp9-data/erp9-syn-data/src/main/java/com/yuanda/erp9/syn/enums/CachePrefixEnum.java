package com.yuanda.erp9.syn.enums;

public enum CachePrefixEnum {
    BRAND("brand"),
    SUPPLIERS("suppliers"),
    EXCHANGE_RATES("ExchangeRates"),
    STANDARD_PARTNUMBER_SFORPLUGIN("StandardPartnumbersForPlugin"),
    RS_HK_DATA("rsHKData"),
    ;

    private String key;

    CachePrefixEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}