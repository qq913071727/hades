package com.yuanda.erp9.syn.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName ServerPrepareException
 * @Description 准备阶段异常（下载文件、读取excel、处理邮件等异常）
 * @Date 2023/2/7
 * @Author myq
 */
@Getter
@Setter
public class ServerPrepareException extends RuntimeException {

    private Integer supplier;
    private String errMsg;
    private String fileName;

    /**
     * @Description: 默认构造器
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/410:52
     */
    public ServerPrepareException(String message) {
        super(message);
        this.errMsg = message;
    }


    /**
     * @Description: 带供应商ID的构造器
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/410:52
     */
    public ServerPrepareException(String message, Integer supplier, String fileName) {
        super(message);
        this.errMsg = message;
        this.supplier = supplier;
        this.fileName = fileName;
    }
}
