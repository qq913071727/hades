import { router } from './router/router'
// //顶部进度条  不使用进度环
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({ showSpinner: false })

import { getToken } from '@/utils/token'
const whiteList = ['/']
router.beforeEach(async(to, from, next) => {
    NProgress.start()
    if(getToken()){
        if(to.name === 'login'){
            next({path:'/index'})
            NProgress.done()
        }else{
            next()
            NProgress.done()
        }
    }else{
        console.log(whiteList.indexOf(to.path));
        if (whiteList.indexOf(to.path) !== -1) {
            //如果要跳转的路由在白名单里，则跳转过去
            next()
        } else {
        //否则跳转到登录页面
            next({path:'/'})
            NProgress.done()
        }
    }
    NProgress.done()
})

