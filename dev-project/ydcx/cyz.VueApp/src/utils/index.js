//  untils/index.js中添加px2remLoader

const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap: options.sourceMap
    }
  }

  // 增加代码，px转rem配置（需要将px2remloader添加进loaders数组中）
  const px2remLoader = {
    loader: 'px2rem-loader',
    options: {
      remUnit: 192,  //根据视觉稿，rem为px的十分之一，1920px  192 rem
      // remPrecision: 8//换算的rem保留几位小数点
    }
  }

//  px2remLoader 放进loaders数组中

function generateLoaders (loader, loaderOptions) {
   const loaders = options.usePostCSS ? [cssLoader, postcssLoader, px2remLoader] : [cssLoader, px2remLoader]

   if (loader) {
     loaders.push({
       loader: loader + '-loader',
       options: Object.assign({}, loaderOptions, {
         sourceMap: options.sourceMap
       })
     })
   }
   //
 }
