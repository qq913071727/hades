import Cookies from 'js-cookie'

const TokenKey = process.env.VUE_APP_CookieName

// token保存
export function saveToken(val) {
    return Cookies.set(TokenKey,val)
}

// 获取token
export function getToken(token, time = 86400,) {
    return Cookies.get(TokenKey, token, {
        expires: new Date(new Date().getTime() + (time * 1000)),
    })
}

// 删除token
export function removeToken() {
    return Cookies.remove(TokenKey)
}

