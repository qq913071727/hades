import axios from 'axios'
import { Message } from 'element-ui';
import { getToken,removeToken } from './token.js'
import router from '@/router/router.js'

var instance = axios.create({
    //设置基地址
    baseURL: process.env.VUE_APP_URL , //process.env.BASE_API, //api的base_url
    withCredentials: true, //跨域照样协带cookie
});
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // console.log(config,'config');
    // 在发送请求之前做些什么
      // 首先得有token我们才加
    if (getToken()) {
        config.headers.token = 'Bearer '+ getToken()
    }
    config.headers.authorization = '6a77998089b8467d83d2094a8011b02d'
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (res) {
    // 对响应数据做点什么
    if (typeof res.data == 'undefined' && !res.data) {
        return res;
    }
    // if (response.data.code == 200) {
    //     // 因为返回数据里面axios帮我们额外的包了一层data但实际我们基本不用，所以我们把它干掉
    //     return response.data;
    // } else if(response.data.code == 206){
    //     // Token出错处理
    //     Message.error(response.data.message)
    //     // 跳转登录页
    //     router.push("/")
    //     // 清理掉Token
    //     removeToken();
    //     return Promise.reject("error");
    // }
    // else {
    //     // 提示用户错误
    //     // 出错了我们还有必要返回数据出去吗？
    //     //抛出一个错误，不要让后面代码执行
    //     //只要return了一个Promise.reject("error")后面的接口的.then就不会执行了
    //     Message.error(response.data.message)
    //     return Promise.reject("error");
    // }
    const code = res.status
    var msg = res.data.message
    if (code !== 200) {
        Message({
            message: msg || '请求错误,请重试',
            type: 'error',
        })
        return Promise.reject(new Error(msg))
    } else {
        return res.data
    }
}, function (error) {
    // 对响应错误做点什么
    console.log(error, 'error');
    let code = (error.response && error.response.status) || ""
    console.log(code);
    console.error(error.message)
    return Promise.reject(error);
});

export default instance