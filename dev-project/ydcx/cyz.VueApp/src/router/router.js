
/*
/*路由基本使用
1：安装vue-router   npm i vue-router
2:导入vue-router    import VueRouter from 'vue-router'
3:注册              Vue.use(VueRouter)
4:实例化VueRouter   const router=new VueRouter({routes:[//写相应的路由配制]})
   输出 该路由实例化对象   export default router
5：注入到vue实例   
   导入路由实例化对象
   import router from "路径"
   new Vue({router})  
6:来一个路由出口   router-view
*/
import Vue from 'vue'
import Router from "vue-router" // 导入路由
Vue.use(Router)
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject);
    return originalPush.call(this, location).catch((err) => err);
};

export const pageRoutes = [
    {
        path: "/",
        name:"login",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/login/login'),
        meta:{
            name:"/",
            keepAlive: false
        }
    },
    {  
        path: "/index",
        name:"index",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/index'),
        meta:{
            keepAlive: true,
        }
    },

    {  
        path: "/device/:id",
        name:"currency",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/model/currency'),
        meta:{
                name:'currency',
                keepAlive: true
            },
    },
    {
        path: "/home/service",
        name:"service",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/plan/index'),
        meta:{
            name:'service',
            keepAlive: true
        },
    },
    {
        path: "/home/guide",
        name:"guide",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/guide/index'),
        meta:{
            name:'guide',
            keepAlive: true
        }
    },
    {
        path: "/home/video",
        name:"video",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/video/index'),
        meta:{
            name:'video',
            keepAlive: true
        }
    },
    {
        path: "/home/SMT",
        name:"smt",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/produce/smt'),
        meta:{
            name:'SMT',
            keepAlive: true
        },
    },
    {
        path: "/home/PCB",
        name:"pcb",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/produce/pcb'),
        meta:{
            name:'PCB',
            keepAlive: true
        }
    },
    {
        path:"/home/SMTdetail",
        name:"detail",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/produce/detail'),
        meta:{
            name:'SMT',
            keepAlive: true
        },
    },
    {
        path:"/home/PCBdetail",
        name:"PCBdetail",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/produce/pcbdetail'),
        meta:{
            name:'PCB',
            keepAlive: true
        },
    },
    {
        path:"/home/planDetail",
        name:"planDetail",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/plan/detail'),
        meta:{
            name:'service',
            keepAlive: true
        }
    },
    {
        path:"/home/videoDetail",
        name:"videoDetail",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/video/detail'),
        meta:{
            name:'video',
            keepAlive: true
        }
    },
    {
        path:"/home/preview",
        name:"preview",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/guide/preview'),
        meta:{
            name:'guide',
            keepAlive: true
        }
    }, 
    {
        path:"/home/guideDetail",
        name:"guideDetail",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/guide/detail'),
        meta:{
            name:'guide',
            keepAlive: true
        }
    },
    {
        path:"/mine",
        name:"mine",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/mine.vue'),
        meta:{
            keepAlive: true
        }
    },
    {
        path:'/search',
        name:'search',
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/model/search'),
        meta:{
            name:'device',
            keepAlive: true
        },
    },
    
    {  
        path: "/test",
        name:"test",
        component: () =>import ( /* webpackChunkName: "group-main" */ '@/view/home/model/index'),
        meta:{
            keepAlive: true
        }
    },

];
const createRouter = () =>
     new Router({
         // 如果对于所有路由导航，简单地让页面滚动到顶部。返回 savedPosition，在按下 后退/前进 按钮时
        //  scrollBehavior(to, from, savedPosition) {
        //     console.log(to,from,savedPosition);
        //      if (savedPosition) {
        //          return savedPosition;
        //      } else {
        //         console.log(222);
        //          return { x: 0, y: 0 };
        //      }
        //  },
         routes: pageRoutes,
     });
 export { createRouter };
 export const router = createRouter();
//  router.beforeEach((to, from, next) => {
//     console.log(document.body.scrollTop,'滚动');
//     document.body.scrollTop = 0;
//     next()
//  });

// 路由导航守卫
// import  NProgress from 'nprogress'
// import 'nprogress/nprogress.css'
// import {removeToken} from "@/utils/token.js"
// import store from '@/store/index.js'
// import { Message } from 'element-ui';

// router.beforeEach((to,from,next) =>{
//     // 进度条开启
//     NProgress.start()
//     if (to.meta.rules.includes(store.state.role)) {
//         next()
//     } else {
//         Message.warning("您无权访问该页面！")
//          //清除token
//          removeToken();
//          next("/")
//     }
    
// })
//防止重复点击
// const originalPush = Router.prototype.push
// Router.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// };

// 进入后守卫
// router.afterEach((to,from) =>{
//     NProgress.done()
//     window.console.log(from)
//     document.title = to.meta.title
// })
// 输出 出去
// export default router











