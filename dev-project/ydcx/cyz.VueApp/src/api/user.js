import instance from '@/utils/request.js'
// 获取当前用户信息
function getUserinfro(params) {
    return instance({
        url: "/api/v1/user/info",
        method: "get",
        params
    })
}
// 获取投稿标签
function getUserOption(params) {
    return instance({
        url: "/api/v1/avd/contributeStatus",
        method: "get",
        params
    })
}
// 我的投稿
function myArticle(data) {
    return instance({
        url: "/api/v1/avd/list",
        method: "post",
        data
    })
}
// 收藏点赞
function myCollectArt(data) {
    return instance({
        url: "/api/v1/userViewLog/list",
        method: "post",
        data
    })
}
// 状态修改
function setUserStatus(data) {
    return instance({
        url: "/user/status",
        method: "post",
        data
    })
}
// 编辑数据
function editUserData(data) {
    return instance({
        url: "/user/edit",
        method: "post",
        data
    })
}
// 工单列表
function repairOrderList(params,paramsSize) {
    return instance({
        url: "/api/v1/repairOrder/queryMyRepairOrder/" + params + '/' + paramsSize,
        method: "get",
        
    })
}
// 获取工单内容
function repairOrderInfo(params) {
    return instance({
        url: "/api/v1/repairOrder/info/" + params ,
        method: "get",
        
    })
}
// 上传文件成功删除
function deleteByFilePath(data) {
    return instance({
        url: "/api/v1/repairOrderFile/deleteByFilePath" ,
        method: "post",
        data
    })
}
// 删除工单
function deleteRepairOrder(params) {
    return instance({
        url: "/api/v1/repairOrderFile/delete/" + params ,
        method: "get",
        
    })
}
// 下载工单
function downloadRepairOrder(params) {
    return instance({
        url: "/api/v1/repairOrderFile/download/" + params ,
        method: "get",
        responseType: 'arraybuffer',
    })
}
// 添加工单
function addRepairOrder(data) {
    return instance({
        url: "/api/v1/repairOrder/add",
        method: "post",
        data
    })
}
// 工单回复
function replyRepairOrder(data) {
    return instance({
        url: "/api/v1/repairOrder/reply",
        method: "post",
        data
    })
}
// 查询工单分类信息 
function queryRepairOrderType(params) {
    return instance({
        url: "/api/v1/repairOrderType/queryRepairOrderTypeList",
        method: "get",
        params
    })
}

export {getUserinfro, getUserOption,myArticle, myCollectArt, setUserStatus, editUserData,repairOrderList,repairOrderInfo,addRepairOrder,replyRepairOrder,queryRepairOrderType,deleteRepairOrder,downloadRepairOrder,deleteByFilePath}