import instance from '@/utils/request.js'
// 修改后器件选型分类
function getCategories(params) {
    return instance({
        url: "/api/v1/deviceSelection/findDeviceSelectionMenu",
        method: "get",
        params 
    })
}
// 快速筛选 
function getFastSelectProperties(data) {
    return instance({
        url: "/api/v1/deviceSelectionParameter/getFastSelectProperties",
        method: "post",
        data
    })
}
// 参数筛选 
function getParamSelectProperties(data) {
    return instance({
        url: "/api/v1/deviceSelectionParameter/getParamSelectProperties",
        method: "post",
        data
    })
}

// 器件选项
// function getCategoriesOption(data) {
//     return instance({
//         url: "/api/v1/productSearch/properties",
//         method: "post",
//         data
//     })
// }
// 根据分类获取表头
function gettableHeader(data) {
    return instance({
        url: "/api/v1/productSearch/getCategoriesProperty?categoryId="+ data,
        method: "post",
    })
}

// 产品列表
function getList(data) {
    return instance({
        url: "/api/v1/productSearch/list",
        method: "post",
        data
    })
}
// 对比接口
function getContrast(data) {
    return instance({
        url: "/api/v1/productSearch/getListByUids",
        method: "post",
        data
    })
}
// 模糊查询
 function getSelectProperties(data) {
    return instance({
        url: "/api/v1/deviceSelectionParameter/queryFuzzySelectProperties",
        method: "post",
        data,
    }) 
}
export { getCategories,getList,getContrast,gettableHeader,getFastSelectProperties,getParamSelectProperties,getSelectProperties}