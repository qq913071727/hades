import instance from '@/utils/request.js'
function toLogin(data) {
    return instance({
        url: "/api/v1/user/login",
        method: "post",
        data
    })
}
export { toLogin }