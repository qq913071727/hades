import instance from '@/utils/request.js'
/*
    首页
 */
// 轮播图 背景图
export function getBanner(params) {
    return instance({
        url: "/api/v1/banner/getAvailableBannerList",
        method: "get",
        // 注意点get请求是用params这个参数
        params      //params: params   
    })
}
// 修改后轮播图
export function getCarousel(params) {
    return instance({
        url: "/api/v1/deviceSelection/findDeviceSelection",
        method: "get",
        // 注意点get请求是用params这个参数
        params      //params: params   
    })
}

// 信息反馈选项
export function getInFeedbackOption(params) {
    return instance({
        url: "/api/v1/feedback/types",
        method: "get",
        // 注意点get请求是用params这个参数
        params      //params: params   
    })
}

// 信息反馈提交

export function getFeedbackSave(data) {
    return instance({
        url: "/api/v1/feedback/save",
        method: "post",
        data,
    }) 
}
// 搜索页
export function queryProducts(data) {
    return instance({
        url: "/api/v1/productSearch/queryProductsByBrandPartNumber",
        method: "post",
        data,
    }) 
}
// 型号模糊查询
export function querypartNumber(data) {
    return instance({
        url: "/api/v1/product/queryPartNumbersByContext",
        method: "post",
        data,
    }) 
}

