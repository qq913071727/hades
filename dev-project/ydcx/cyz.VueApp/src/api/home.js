import instance from '@/utils/request.js'
/**
    SMT
 */
// 城市
function getcity(params) {
    return instance({
        url: "/api/v1/region/getCities",
        method: "get",
        // 注意点get请求是用params这个参数
        params      //params: params   
    })
}
// SMT列表
function getSMTlist(data) {
    return instance({
        url: "/api/v1/mfrsmt/list",
        method: "post",
        data     
    })
}
// SMT列表选项
function getSMTlistoption(params) {
    return instance({
        url: "/api/v1/mfrsmt/listOptions",
        method: "get",
        // 注意点get请求是用params这个参数
        params      //params: params   
    })
}
//SMT详情
function getSMTdetail(MfrId) {
    return instance({
        url:"/api/v1/mfrsmt/detail/" + MfrId,
        method:"get",
        // params  
    })
}
// 在线询价

/**
    PCB
 */
// pcb列表选项
function getPCBlistoption(params) {
    return instance({
        url: "/api/v1/mfrpcb/listOptions",
        method: "get",
        // 注意点get请求是用params这个参数
        params      //params: params   
    })
}
// PCB列表
function getPCBlist(data) {
    return instance({
        url: "/api/v1/mfrpcb/list",
        method: "post",
        data
    })
}

// PCB详情
function getPCBdetail(MfrId) {
    return instance({
        url:"/api/v1/mfrpcb/detail/" + MfrId,
        method:"get",
        // params  
    })
}
// 在线询价
function getPCBinquiry(data) {
    return instance({
        url: "/api/v1/inquiry/miniSave",
        method: "post",
        data
    })
}

/**
    方案咨询
 */
// 方案选项
function getPlanoption(params) {
    return instance({
        url: "/api/v1/sln/listOptions",
        method: "get",
        params      //params: params   
    })
}

// 方案咨询列表
function getPlanlist(data) {
    return instance({
        url: "/api/v1/sln/list",
        method: "post",
        data
    })
}
// 方案咨询详情
function getPlandetail(SlnId) {
    return instance({
        url:"/api/v1/sln/detail/" + SlnId,
        method:"get",
        // params  
    })
}

/**
    视频
 */
// 视频选项
function getVideoCategories(params) {
    return instance({
        url: "/api/v1/video/getCategories",
        method: "get",
        params      //params: params   
    })
}
function getVideoBrand(params) {
    return instance({
        url: "/api/v1/video/getBrands",
        method: "get",
        params      //params: params   
    })
}
function getVideoType(params) {
    return instance({
        url: "/api/v1/video/getTypes",
        method: "get",
        params      //params: params   
    })
}

// 视频列表
function getVideolist(data) {
    return instance({
        url: "/api/v1/video/getList",
        method: "get",
        data
    })
}

// 视频详情
function getVideodetail(id) {
    return instance({
        url:"/api/v1/video/getDetail/" + id,
        method:"get",
        // params  
    })
}



export { getcity,getSMTlist,getSMTlistoption,getSMTdetail,getPCBlist,getPCBdetail,getPCBlistoption,getPCBinquiry,getPlanoption,getPlanlist,getPlandetail,getVideoCategories,getVideoBrand,getVideoType,getVideolist,getVideodetail}
