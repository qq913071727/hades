import instance from '@/utils/request.js'

// 图片上传
function getImgList(data,x) {
    return instance({
        url: "/api/v1/file/uploadImage?category=" + x,
        method: "post",
        data     
    })
}
// 图片删除
function deleteImgList(data) {
    return instance({
        url: "/api/v1/file/deleteImgg?relativePath=" +data,
        method: "delete",
        headers: {
            'Content-Type': 'multipart/form-data'
        },
    })
}
// 视频上传
function getVideoList(data,x) {
    return instance({
        url: "/api/v1/file/uploadVideo?category=" + x,
        method: "post",
        data     
    })
}
// 视频删除
function deleteVideoList(data) {
    return instance({
        url: "/api/v1/file/deleteVideo",
        method: "delete",
        data     
    })
}
// 上传附件
function uploadProtocol(data) {
    return instance({
        url: "/api/v1/file/uploadProtocol",
        method: "post",
        data,
    }) 
}

// 新增文章选项
function getActicleoption(params) {
    return instance({
        url: "/api/v1/avd/editOptions",
        method: "get",
        params      //params: params   
    })
}
// 新增文章提交按钮
function getActicleSave(data) {
    return instance({
        url: "/api/v1/avd/save",
        method: "post",
        data,
    }) 
}

// 避坑指南列表
function getGuideList(data) {
    return instance({
        url: "/api/v1/avd/list",
        method: "post",
        data
    }) 
}
// 避坑指南详情页
function getActicleDetail(avdId) {
    return instance({
        url: "/api/v1/avd/detail/" + avdId,
        method: "get", 
    })
}
// 用户增加阅读 点赞 收藏
function getGuideAdd(data) {
    return instance({
        url: "api/v1/avd/userViewLogAdd",
        method: "post",
        data
    }) 
}
// 用户减少点赞 收藏
function getGuideReduce(data) {
    return instance({
        url: "/api/v1/avd/userViewLogReduce",
        method: "post",
        data
    }) 
}

// 避坑指南新增文章审核通过
function getGuidesuccess(data) {
    return instance({
        url: "/api/v1/avd/approveOk",
        method: "post",
        data
    }) 
}

export{getImgList,getActicleoption,getActicleSave,getGuideList,getGuideAdd,getGuideReduce,getActicleDetail,getGuidesuccess,getVideoList,deleteImgList,deleteVideoList,uploadProtocol}