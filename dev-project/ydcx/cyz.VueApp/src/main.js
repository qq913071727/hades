import Vue from 'vue'
import App from './App.vue'
// 导入element插件
import ElementUI from 'element-ui';
// 导入element的css
import 'element-ui/lib/theme-chalk/index.css';

import './permission';

// 视频
import VideoPlayer from 'vue-video-player'
require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')
Vue.use(VideoPlayer)
// 这个是为了兼容 m3u8
const hls = require('videojs-contrib-hls')
Vue.use(hls)

// 轮播图
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'
Vue.use(VueAwesomeSwiper)

// 导入路由实例对象
import {router} from './router/router.js'
// 导入vuex
import store from '@/store/index.js'

// 注册elementui
Vue.use(ElementUI);
Vue.config.productionTip = false
// 5：注入到vue实例  
new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
