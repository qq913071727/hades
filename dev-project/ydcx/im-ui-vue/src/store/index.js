import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    current_contact: {},
    user: {},
  },
  mutations: {
    setCurrentContact(state, contact) {
      state.current_contact = contact
    },
    setUser(state, user) {
      state.user = user
    }
  },
  actions: {
  },
  modules: {
  }
})