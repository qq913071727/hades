import Vue from 'vue'
import Router from 'vue-router'
import login from '@/view/login'
import Container from '@/components/Container'
Vue.use(Router)
export default new Router({
  routes: [
    {  
      path: "/",
      name:"login",
      component: login,
     
    },
    {
      path: '/:id/:name/:type',
      name: 'Container',
      component: Container,
      props: true,
    },
  ]
})