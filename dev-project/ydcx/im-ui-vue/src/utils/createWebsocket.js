export default class createWebSocket {
    ws = null
    onmessageCb = null
    constructor(params, onmessageCb) {
        this.ws = new WebSocket(
        `ws://im.51db.com:38201/im-end-web/ws/v1/group/${params}/`,
        "Authorization\'4f52f26643d647b9b6a4e94c1147f9c2"
        );

        this.onmessageCb = onmessageCb

        this.ws.onopen = this.onopen

        this.ws.onmessage = this.onmessage

        this.ws.onclose = this.onclose

        this.ws.onerror = this.onerror
    }

    onopen() {
        console.log('打开 websocket 连接');
    }

    onmessage(event) {
        this.onmessageCb(JSON.stringify(event.data));
    }

    onclose() {
        console.log('关闭 websocket 连接');
    }

    onerror() {
        console.log('websocket 连接出错')
    }
}