import Cookies from 'js-cookie'

const TokenKey = 'ydcx_Yahv.Erp'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function theRequest() {
  let u = window.location.href
  let theRequest = new Object()
  if (u.indexOf('?') !== -1) {
    let s = u.split('?')
    let strs = s[1].split('&')
    for (let i = 0; i < strs.length; i++) {
      theRequest[ strs[i].split('=')[0]  ]= decodeURI(strs[i].split('=')[1] )
    }
  }
  return theRequest
}