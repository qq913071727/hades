﻿/*回调windows事件：name 回调标识; data 数据（字符串或json字符串）*/
function FireEvent(name, data) {

    var event = new MessageEvent(name, { 'view': window, 'bubbles': false, 'cancelable': false, 'data':data ==null ?null : JSON.stringify(data) });
    document.dispatchEvent(event);
} 

export function Logon(data) {

    FireEvent("Logon", data);

    if (!!window['_6CC549F31D4E87D549992B39024C022E']) {
        var ___1 = window['_6CC549F31D4E87D549992B39024C022E'];
        window['_6CC549F31D4E87D549992B39024C022E'] = null;
        return ___1;
    }
}
export function GetLoginType() {

    FireEvent("GetLoginType", null);

    if (!!window['_694D89041EBDBA615E8DF5E4B6336154']) {
        var ___1 = window['_694D89041EBDBA615E8DF5E4B6336154'];
        window['_694D89041EBDBA615E8DF5E4B6336154'] = null;
        return ___1;
    }
}
export function GetMessage(data) {

    FireEvent("GetMessage", data);

    if (!!window['_8B13966EB1F1591C8FDDDA80DE11B2F5']) {
        var ___1 = window['_8B13966EB1F1591C8FDDDA80DE11B2F5'];
        window['_8B13966EB1F1591C8FDDDA80DE11B2F5'] = null;
        return ___1;
    }
}
export function GetBackUrl(data) {

    FireEvent("GetBackUrl", data);

    if (!!window['_4FC76B301AE14A22444F3DD1D2E3A682']) {
        var ___1 = window['_4FC76B301AE14A22444F3DD1D2E3A682'];
        window['_4FC76B301AE14A22444F3DD1D2E3A682'] = null;
        return ___1;
    }
}
