// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import LemonIMUI from 'lemon-imui-max';
import 'lemon-imui-max/dist/index.css';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './styles/reset.css';
Vue.config.productionTip = false
Vue.use(LemonIMUI)
Vue.use(ElementUI);

// 为了方便在组件中直接使用, 可以将API对象挂载到vue原型对象上
import * as API from '@/api'

// 适配
import 'lib-flexible'

import store from './store'
Vue.prototype.$API = API

let timer
Vue.prototype.$Debounce = function(fn,delay){
    var delay = delay || 300;
    return () => {
        var args = arguments;
        var that = this;
        clearTimeout(timer)
        timer = setTimeout(function(){
            fn.apply(that,args)
        },delay)
    }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  store,
  template: '<App/>'
})
