import request from '@/utils/request';


export default {
    //添加话术
    add(params){
        return request.post('/im-end-web/api/v1/dialogue/add',params)
    },
    //删除话术
    delete(id) {
        return request(`/im-end-web/api/v1/dialogue/delete/${id}`)
    },
    //修改话术
    update(params){
        return request.post('/im-end-web/api/v1/dialogue/update',params)
    },
    //话术置顶
    topping(id){
        return request(`/im-end-web/api/v1/dialogue/topping/${id}`)
    },
    //返回话术
    find(params){
        return request.post('/im-end-web/api/v1/dialogue/findByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc',params)
    },
   
}