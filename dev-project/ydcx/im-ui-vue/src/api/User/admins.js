import request from '@/utils/request'

export default {
    //获取所有联系人
    getAdmins() {
        return request('/im-end-web/api/v1/account/findAllOrderByRealNameAsc')
    },
    //组织机构
    getOrganization(organizationId,accountId,username) {
        return request(`/im-end-web/api/v1/treeNode/get/${organizationId}/${accountId}/${username}`)
    },
    //群组
    getGroup(params) {
        return request.post('/im-end-web/api/v1/group/add', params)
    },
    //账号
    login(adminsId) {
        return request(`/im-end-web/api/v1/account/detailByAdminsId/${adminsId}`)
    },
    //根据account获取账号详细信息
    getUserInfo(account){
        return request(`/im-end-web/api/v1/account/detailByAccount/${account}`)
    },
    //获取聊天个人记录
    getMessage(params) {
        return request(`/im-end-web/api/v1/message/pageByType/${params}`)
    },
    //获取默认群组聊天记录
    GroupMessage(params) {
        return request(`/im-end-web/api/v1/groupMessage/pageByType/${params}`)
    },
    //获取自定义群聊天记录
    userGroupMessage(params) {
        return request(`/im-end-web/api/v1/userGroupMessage/pageByType/${params}`)
    },
    //获取近期的联系人
    account(account) {
        return request(`/im-end-web/api/v1/account/current/${account}`)
    },
    //模糊查询
    searchByRealName(data) {
        return request.post('/im-end-web/api/v1/account/searchByRealName', data)
    },
    //获取有消息的群组和用户自定义群组
    groupAccount(account) {
        return request(`/im-end-web/api/v1/group/current/${account}`)
    },
    //下载文件、图片
    fileDownload(data) {
        return request.post('/im-end-web/api/v1/file/download',data,{responseType: 'blob'})
    },
    //创建自定义群聊
    userGroup(data){
        return request.post('/im-end-web/api/v1/userGroup/create',data)
    },
    //个人查看上下文
    getContext(fromUserAccount,toUserAccount,id,initRow,direction,pageSize,pageNo){
        return request.post(`/im-end-web/api/v1/message/context/${fromUserAccount}/${toUserAccount}/${id}/${initRow}/${direction}/${pageSize}/${pageNo}`)
    },
    //默认群聊查看上下文
    getGroupContext(id,fromUserAccount,initRow,direction,pageSize,pageNo){
        return request.post(`/im-end-web/api/v1/groupMessage/context/${id}/${fromUserAccount}/${initRow}/${direction}/${pageSize}/${pageNo}`)
    },
    //自定义群聊查看上下文
    getUserGroupContext(id,fromUserAccount,initRow,direction,pageSize,pageNo){
        return request.post(`/im-end-web/api/v1/userGroupMessage/context/${id}/${fromUserAccount}/${initRow}/${direction}/${pageSize}/${pageNo}`)
    },
    //获取自定义群聊成员详细信息
    refUserGroupAccount(userGroupId,accountId,username){
        return request(`/im-end-web/api/v1/refUserGroupAccount/detail/${userGroupId}/${accountId}/${username}`)
    },
    //查找用户聊天记录
    userSearchMessage(data){
        return request.post('/im-end-web/api/v1/message/searchAndPageByType',data)
    },
    //查询默认群聊天记录
    groupSearchMessage(data){
        return request.post('/im-end-web/api/v1/groupMessage/searchAndPageByType',data)
    },
    //查询自定义群聊天记录
    userGroupSearchMessage(data){
        return request.post('/im-end-web/api/v1/userGroupMessage/searchAndPageByType',data)
    },
     //根据群组Id获取默认群组中的人数
     groupNumber(groupId){
        return request(`/im-end-web/api/v1/refGroupAccount/count/${groupId}`)
    },
    //获取自定义群聊在线人数
    userGroupNumber(userGroupId){
        return request(`/im-end-web/api/v1/refUserGroupAccount/count/${userGroupId}`)
    },
     //置顶联系人
     topping(params) {
        return request(`/im-end-web/api/v1/chatPair/topping/${params}`)
    },
    //默认群置顶
    groupToppingAccount(params){
        return request(`/im-end-web/api/v1/refGroupAccount/topping/${params}`)
    },
    //自定义群聊置顶
    userGroupToppingAccount(params){
        return request(`/im-end-web/api/v1/refUserGroupAccount/topping/${params}`)
    },
    //一对一免打扰
    personNoReminder(params){
        return request(`/im-end-web/api/v1/chatPair/noReminder/${params}`)
    },
    //默认群组消息免打扰
    groupNoReminder(params){
        return request(`/im-end-web/api/v1/refGroupAccount/noReminder/${params}`)
    },
    //自定义群消息免打扰
    userGroupNoReminder(params){
        return request(`/im-end-web/api/v1/refUserGroupAccount/noReminder/${params}`)
    },
    //退出自定义群聊
    groupSignOut(params){
        return request(`/im-end-web/api/v1/refUserGroupAccount/exit/${params}`)
    },
    //恢复个人离线消息
    offlineMessage(fromUserAccount,toUserAccount){
        return request(`/im-end-web/api/v1/message/restore/${fromUserAccount}/${toUserAccount}`)
    },
    //恢复默认组离线消息
    groupOfflineMessage(groupId,toUserAccount){
        return request(`/im-end-web/api/v1/groupMessageReceiver/restore/${groupId}/${toUserAccount}`)
    },
    //恢复自定义组离线消息
    userGroupOfflineMessage(userGroupId,toUserAccount){
        return request(`/im-end-web/api/v1/userGroupMessageReceiver/restore/${userGroupId}/${toUserAccount}`)
    },
    //清空一对一聊天记录
    emptyChat(fromUserAccount,toUserAccount){
        return request(`/im-end-web/api/v1/chatPair/invisible/${fromUserAccount}/${toUserAccount}`)
    },
    //清空默认群聊天记录
    emptyGroupChat(groupId,accountId,username){
        return request(`/im-end-web/api/v1/refGroupAccount/invisible/${groupId}/${accountId}/${username}`)
    },
    //清空自定义群聊天记录
    emptyUserGroupChat(userGroupId,accountId,username){
        return request(`/im-end-web/api/v1/refUserGroupAccount/invisible/${userGroupId}/${accountId}/${username}`)
    },
    //添加自定义群组成员
    addUserGroupAccount(params){
        return request.post(`/im-end-web/api/v1/refUserGroupAccount/add/`,params)
    },
    //合并近期联系人
    allPerson(account){
        return request(`/im-end-web/api/v1/userGroup/current/${account}`)
    },
    //退出群聊
    quitUserGroup(userGroupId,accountId,username){
        return request(`/im-end-web/api/v1/refUserGroupAccount/exit/${userGroupId}/${accountId}/${username}`)
    },
    //将有消息的默认群组和自定义群组按照置顶顺序,按照聊天时间排序
    sort(account){
        return request(`/im-end-web/api/v1/group/current/${account}`)
    },
    //个人离线
    prosonalOffline(fromUserAccount,toUserAccount){
        return request(`/im-end-web/api/v1/chatPair/updateLastContactTime/${fromUserAccount}/${toUserAccount}`)
    },
    //默认群
    groupLastContactTime(groupId,accountId,username){
        return request(`/im-end-web/api/v1/refGroupAccount/updateLastContactTime/${groupId}/${accountId}/${username}`)
    },
    //自定义群
    userGroupLastContactTime(userGroupId,accountId,username){
        return request(`/im-end-web/api/v1/refUserGroupAccount/updateLastContactTime/${userGroupId}/${accountId}/${username}`)
    },
    //一对一撤回消息
    recallMessage(id,dateTime){
        return request(`/im-end-web/api/v1/message/retraction/${id}/${dateTime}`)
    },
    //默认群撤回消息
    groupRecallMessage(data){
        return request.post('/im-end-web/api/v1/groupMessageReceiver/retraction',data)
    },
    //自定义群撤回消息
    userGroupRecallMessage(data){
        return request.post('/im-end-web/api/v1/userGroupMessageReceiver/retraction',data)
    },
    //一对一删除聊天
    removeChat(fromUserAccount,toUserAccount){
        return request(`/im-end-web/api/v1/chatPair/deleteChat/${fromUserAccount}/${toUserAccount}`)
    },
    //默认群组删除聊天
    removeGroupChat(groupId,accountId,username){
        return request(`/im-end-web/api/v1/refGroupAccount/deleteGroupChat/${groupId}/${accountId}/${username}`)
    },
    //自定义群组删除聊天
    removeUserGroupChat(userGroupId,accountId,username){
        return request(`/im-end-web/api/v1/refUserGroupAccount/deleteUserGroupChat/${userGroupId}/${accountId}/${username}`)
    },
    //修改自定义群名
    editUserGroupName(data){
        return request.post('/im-end-web/api/v1/userGroup/update',data)
    },
    //更新已读状态
    updatechatPair(fromUserAccount,toUserAccount,status){
        return request(`/im-end-web/api/v1/chatPair/updateChatPageStatus/${fromUserAccount}/${toUserAccount}/${status}`)
    },

}