const CompressionPlugin = require('compression-webpack-plugin');
const productionGzipExtensions = /\.(js|css|json|txt|html|ico|svg)(\?.*)?$/i;
const timeStamp = new Date().getTime();
module.exports = {
  publicPath: './',
  productionSourceMap: false,
  filenameHashing: false,
  configureWebpack: {    //重点
    output: { // 输出重构 打包编译后的js文件名称,添加时间戳.
      filename: `js/js[name].${timeStamp}.js`,
      chunkFilename: `js/chunk.[id].${timeStamp}.js`,
    }
  },
  css: { //重点.
    extract: { // 打包后css文件名称添加时间戳
      filename: `css/[name].${timeStamp}.css`,
      chunkFilename: `css/chunk.[id].${timeStamp}.css`,
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set('@', resolve('src'));
    config.when(process.env.NODE_ENV !== 'development',
      config => {
        // 提供带 Content-Encoding 编码的压缩版的资源
        config.plugin('compressionPlugin')
          .use(new CompressionPlugin({
            filename: '[path].gz[query]', // 目标文件名
            algorithm: 'gzip',  // 压缩算法
            test: productionGzipExtensions, // 满足正则表达式的文件会被压缩
            threshold: 10240, // 只处理比这个值大的资源。按字节计算 10240=10KB
            minRatio: 0.8, // 只有压缩率比这个值小的资源才会被处理
            deleteOriginalAssets: true // 是否删除原资源
          }));
      }
    )
  }
}


