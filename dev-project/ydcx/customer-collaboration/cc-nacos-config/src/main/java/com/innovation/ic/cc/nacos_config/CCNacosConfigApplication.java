package com.innovation.ic.cc.nacos_config;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CCNacosConfigApplication {

    private static final Logger log = LoggerFactory.getLogger(CCNacosConfigApplication.class);

    public static void main(String[] args) {
        LogbackHelper.setLogbackPort(args);
        SpringApplication.run(CCNacosConfigApplication.class, args);
        log.info("cc-nacos-config服务启动成功");
    }
}
