package com.innovation.ic.cc.gateway.schedule;

import com.innovation.ic.cc.base.handler.cc.ContextHandler;

import javax.annotation.Resource;

/**
 * 定时任务的抽象类
 */
public abstract class AbstractSchedule {

    @Resource
    protected ContextHandler contextHandler;
}