package com.innovation.ic.cc.gateway.config;

import com.innovation.ic.b1b.framework.manager.MongodbManager;
import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.SftpChannelManager;
import com.innovation.ic.cc.base.value.RabbitMqParamConfig;
import com.innovation.ic.cc.base.value.SftpParamConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

@Configuration
public class ManagerConfig {

    /****************************************** redis *********************************************/
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setStringSerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }

    @Bean
    public RedisManager redisManager() {
        return new RedisManager(redisTemplate);
    }

    /****************************************** mongodb *********************************************/
    @Resource
    private MongoTemplate mongoTemplate;

    @Bean
    public MongodbManager mongodbManager() {
        return new MongodbManager(mongoTemplate);
    }

    /****************************************** rabbitmq *********************************************/
    @Resource
    private RabbitMqParamConfig rabbitMqParamConfig;

    @Bean
    public RabbitMqManager rabbitMqManager() {
        return new RabbitMqManager(rabbitMqParamConfig.getHost(), rabbitMqParamConfig.getPort(),
                rabbitMqParamConfig.getUsername(), rabbitMqParamConfig.getPassword(),
                rabbitMqParamConfig.getVirtualHost());
    }

    /****************************************** sftp *********************************************/
    @Resource
    private SftpParamConfig sftpParamConfig;

    @Bean
    public SftpChannelManager sftpChannelManager() {
        return new SftpChannelManager(sftpParamConfig.getUsername(), sftpParamConfig.getPassword(),
                sftpParamConfig.getHost(), sftpParamConfig.getPort(), sftpParamConfig.getTimeout());
    }
}
