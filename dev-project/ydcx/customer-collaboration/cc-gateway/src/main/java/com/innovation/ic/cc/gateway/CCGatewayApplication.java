package com.innovation.ic.cc.gateway;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com.innovation.ic.cc")
@MapperScan("com.innovation.ic.cc.base.mapper")
@EnableDiscoveryClient
public class CCGatewayApplication {

    private static final Logger log = LoggerFactory.getLogger(CCGatewayApplication.class);


    public static void main(String[] args) {
        LogbackHelper.setLogbackPort(args);
        SpringApplication.run(CCGatewayApplication.class, args);
        log.info("cc-gateway服务启动成功");
    }


}