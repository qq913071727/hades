package com.innovation.ic.cc.gateway.config;

import com.innovation.ic.cc.base.pojo.constant.config.DatabaseGlobal;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
public class DataSourceAspect {

    private static final Logger log = LoggerFactory.getLogger(DataSourceAspect.class);

    @Pointcut("execution(* com.innovation.ic.cc.base.service.cc.impl..*.*(..))")
    private void ccAspect() {
    }

    @Before("ccAspect()")
    public void cc() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.CC);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.CC);
    }

}
