package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 用户动作消息关系表
 *
 * @author myq
 * @since 1.0.0 2023-01-06
 */
@TableName("action_message_user_link")
@Setter
@Getter
public class ActionMessageUserLink implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "消息主键")
    private Integer actionMessageId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "是否已读: 0否 1是")
    private Integer isRead;
}