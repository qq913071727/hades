package com.innovation.ic.cc.base.model.cc.erp9_pvecrm;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * erp公司信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "EnterprisesTopView", description = "ERP9公司信息")
@TableName("EnterprisesTopView")
public class EnterprisesTopView {


    @ApiModelProperty(value = "供应商id", dataType = "String")
    @TableField(value = "ID")
    private String id;


    @ApiModelProperty(value = "供应商id", dataType = "String")
    @TableField(value = "Name")
    private String name;


    @ApiModelProperty(value = "状态", dataType = "String")
    @TableField(value = "Status")
    private Integer status;



    @ApiModelProperty(value = "国别", dataType = "String")
    @TableField(value = "District")
    private String district;


    @ApiModelProperty(value = "供应商类型", dataType = "String")
    @TableField(value = "SupplierType")
    private String supplierType;

    @ApiModelProperty(value = "企业性质", dataType = "String")
    @TableField(value = "Nature")
    private String nature;

    @ApiModelProperty(value = "发票类型", dataType = "String")
    @TableField(value = "InvoiceType")
    private String invoiceType;

    @ApiModelProperty(value = "文件集合", dataType = "String")
    @TableField(value = "Files")
    private String files;


}