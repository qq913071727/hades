package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 询价表。一条记录表示一次询价
 * </p>
 *
 * @author mybatis-generator
 * @since 2023-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("inquiry_price")
@ApiModel(value = "InquiryPrice", description = "询价表。一条记录表示一次询价")
public class InquiryPrice {
    /**
     * 询价主键
     */
    @ApiModelProperty(value = "询价主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 询价单号
     */
    @ApiModelProperty(value = "询价单号", dataType = "String")
    @TableField(value = "odd_numbers")
    private String oddNumbers;

    /**
     * 询价方式 1:逐条询价 2:bom询价
     */
    @ApiModelProperty(value = "询价方式 1:逐条询价 2:bom询价", dataType = "Integer")
    @TableField(value = "inquiry_method")
    private Integer inquiryMethod;

    /**
     * 我的公司id，对应my_company表的id
     */
    @ApiModelProperty(value = "我的公司id，对应my_company表的id", dataType = "Integer")
    @TableField(value = "my_company_id")
    private Integer myCompanyId;

    /**
     * 我的公司名称，对应my_company表的name
     */
    @ApiModelProperty(value = "我的公司名称，对应my_company表的name", dataType = "String")
    @TableField(value = "my_company_name")
    private String myCompanyName;

    /**
     * 客户id，询价必要条件
     */
    @ApiModelProperty(value = "客户id，询价必要条件", dataType = "String")
    @TableField(value = "site_id")
    private String siteId;

    /**
     * 国别地区，询价必要条件
     */
    @ApiModelProperty(value = "国别地区，询价必要条件", dataType = "String")
    @TableField(value = "country_region_id")
    private String countryRegionId;

    /**
     * 销售id，询价必要条件
     */
    @ApiModelProperty(value = "销售id，询价必要条件", dataType = "String")
    @TableField(value = "saler_id")
    private String salerId;

    /**
     * 币种 1:人民币 2:美元
     */
    @ApiModelProperty(value = "币种 1:人民币 2:美元", dataType = "Integer")
    @TableField(value = "currency")
    private Integer currency;

    /**
     * 发票id
     */
    @ApiModelProperty(value = "发票id", dataType = "String")
    @TableField(value = "invoice_type_id")
    private String invoiceTypeId;

    /**
     * 发票名称
     */
    @ApiModelProperty(value = "发票名称", dataType = "String")
    @TableField(value = "invoice_name")
    private String invoiceName;

    /**
     * 截止时间
     */
    @ApiModelProperty(value = "截止时间", dataType = "Date")
    @TableField(value = "deadline")
    private Date deadline;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    @ApiModelProperty(value = "询价类型。1:实单询价，2:普通询价", dataType = "Integer")
    @TableField(value = "inquiry_type")
    private Integer inquiryType;

    /**
     * 交货地。1:大陆，2:香港
     */
    @ApiModelProperty(value = "交货地。1:大陆，2:香港", dataType = "Integer")
    @TableField(value = "delivery_place")
    private Integer deliveryPlace;

    /**
     * 状态。
     */
    @ApiModelProperty(value = "状态。", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "note")
    private String note;

    /**
     * 询价人id，对应user表的id
     */
    @ApiModelProperty(value = "询价人id，对应user表的id", dataType = "String")
    @TableField(value = "user_id")
    private String userId;

    /**
     * 询价人真实姓名，对应user表的real_name
     */
    @ApiModelProperty(value = "询价人真实姓名，对应user表的real_name", dataType = "String")
    @TableField(value = "real_name")
    private String realName;

    /**
     * erp询价id
     */
    @ApiModelProperty(value = "erp询价id", dataType = "String")
    @TableField(value = "erp_inquiry_id")
    private String erpInquiryId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 最近修改时间
     */
    @ApiModelProperty(value = "最近修改时间", dataType = "Date")
    @TableField(value = "update_time")
    private Date updateTime;
}
