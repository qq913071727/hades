package com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 销售详情
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StaffsTopView", description = "销售详情")
@TableName("StaffsTopView")
public class StaffsTopView {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "销售id", dataType = "String")
    @TableField(value = "AdminID")
    private String adminId;

    @ApiModelProperty(value = "销售人姓名", dataType = "String")
    @TableField(value = "Name")
    private String name;

    @ApiModelProperty(value = "销售人手机号", dataType = "String")
    @TableField(value = "Mobile")
    private String mobile;

    @ApiModelProperty(value = "销售人邮箱", dataType = "String")
    @TableField(value = "Email")
    private String email;

    @ApiModelProperty(value = "状态", dataType = "String")
    @TableField(value = "Status")
    private String status;
}
