package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyCurrency", description = "我的公司币种")
@TableName("my_company_currency")
public class MyCompanyCurrency {


    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名字", dataType = "String")
    private String name;


    @ApiModelProperty(value = "中文名", dataType = "String")
    private String enName;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "国际符号", dataType = "String")
    private String symbol;

    @ApiModelProperty(value = "国际符号缩写", dataType = "String")
    private String shortSymbol;

    @ApiModelProperty(value = "数字代码", dataType = "Integer")
    private Integer cumericCode;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    private Integer orderIndex;

    @ApiModelProperty(value = "外部唯一id", dataType = "String")
    private String externalId;

}
