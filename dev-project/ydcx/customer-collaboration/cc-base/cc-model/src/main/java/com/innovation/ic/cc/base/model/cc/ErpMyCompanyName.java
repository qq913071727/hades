package com.innovation.ic.cc.base.model.cc;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 保存erp我的公司
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("erp_my_company_name")
public class ErpMyCompanyName {


    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "name", dataType = "String")
    private String name;


    @ApiModelProperty(value = "创建日期", dataType = "Date")
    private Date createDate;


    @ApiModelProperty(value = "公司状态:  500禁用  400黑名单 200正常", dataType = "Integer")
    private Integer enterpriseStatus;

    @ApiModelProperty(value = "外部主键id", dataType = "String")
    private String externalId;


    @ApiModelProperty(value = "国别", dataType = "String")
    private String district;

    @ApiModelProperty(value = "客户类型", dataType = "String")
    private String type;

    @ApiModelProperty(value = "主要产品", dataType = "String")
    private String products;


    @ApiModelProperty(value = "文件", dataType = "String")
    private String files;


}
