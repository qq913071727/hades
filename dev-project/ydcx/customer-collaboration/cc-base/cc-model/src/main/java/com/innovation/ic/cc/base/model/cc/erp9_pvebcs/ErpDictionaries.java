package com.innovation.ic.cc.base.model.cc.erp9_pvebcs;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 字典表
 *
 * @author Administrator
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class ErpDictionaries {


    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "联合id", dataType = "String")
    private String externalAllyId;

    @ApiModelProperty(value = "字典表code", dataType = "String")
    private String dCode;

    @ApiModelProperty(value = "字典表描述", dataType = "String")
    private String dName;

    @ApiModelProperty(value = "erp中的value", dataType = "String")
    private String dValue;

}
