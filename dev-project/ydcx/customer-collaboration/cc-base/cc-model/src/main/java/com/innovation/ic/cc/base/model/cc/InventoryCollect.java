package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   库存收藏信息表实体类
 * @author linuo
 * @time   2023年5月15日16:40:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryCollect", description = "库存收藏信息表")
@TableName("inventory_collect")
public class InventoryCollect {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "part_number")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "brand")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    @TableField(value = "count")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    @TableField(value = "batch")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    @TableField(value = "package")
    private String packages;

    @ApiModelProperty(value = "价格", dataType = "BigDecimal")
    @TableField(value = "price")
    private BigDecimal price;

    @ApiModelProperty(value = "库存所在地(中国大陆、中国香港)", dataType = "String")
    @TableField(value = "inventory_home")
    private String inventoryHome;
}