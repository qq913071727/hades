package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 菜单
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompany", description = "我的公司")
@TableName("my_company")
public class MyCompany {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "国别地区", dataType = "String")
    private String countryRegionId;

    @ApiModelProperty(value = "公司类型", dataType = "String")
    private String companyTypeId;


    @ApiModelProperty(value = "主要产品", dataType = "String")
    private String mainProducts;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;


    @ApiModelProperty(value = "注册币种 : 1 人民币  2美金", dataType = "String")
    private Integer currency;


    @ApiModelProperty(value = "根据枚举", dataType = "String")
    private Integer invoiceType;

    @ApiModelProperty(value = "发票名字", dataType = "String")
    private String invoiceTypeName;


    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String createId;


    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "erp传送SiteID", dataType = "String")
    private String siteId;


}
