package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * 收货单订单和收货单运单关联表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@TableName("receipt_order_shipping")
@ApiModel(value = "receipt_order_shipping", description = "收货单订单和收货单运单关联表")
@Setter
@Getter
public class ReceiptOrderShipping {



                        @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Long id;
        
                            @ApiModelProperty(value = "收货单订单id")
    private Long receiptOrderId;
        
                            @ApiModelProperty(value = "收货单运单id")
    private Long receiptShippingId;
        }