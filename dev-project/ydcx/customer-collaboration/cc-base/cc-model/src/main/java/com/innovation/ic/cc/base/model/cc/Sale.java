package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 销售
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Sale", description = "销售")
@Document(collection = "sale")
public class Sale {

    @ApiModelProperty(value = "主键。user表中的企业id", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "企业id", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;

    @ApiModelProperty(value = "销售id", dataType = "String")
    @TableField(value = "sale_account_id")
    private String saleAccountId;

    @ApiModelProperty(value = "销售姓名", dataType = "String")
    @TableField(value = "user_name")
    private String userName;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @TableField(value = "phone")
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    @TableField(value = "email")
    private String email;
}
