package com.innovation.ic.cc.base.model.cc.erp9_pvebcs;

import lombok.Data;

/**
 * 查询erp币种
 */
@Data
public class CompanyCurrency {

    private Integer id;
    private String name;
    private String enName;
    private String symbol;
    private String shortSymbol;
    private Integer cnumericCode;
    private Integer orderIndex;
    private String externalId;

}
