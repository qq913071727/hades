package com.innovation.ic.cc.base.model.cc;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 询价记录表。一次询价可以有多条询价记录
 * </p>
 *
 * @author mybatis-generator
 * @since 2023-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("inquiry_price_record")
@ApiModel(value = "InquiryPriceRecord", description = "询价记录表。一次询价可以有多条询价记录")
public class InquiryPriceRecord  {

    /**
     * 询价记录主键
     */
    @ApiModelProperty(value = "询价记录主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 询价id
     */
    @ApiModelProperty(value = "询价id", dataType = "Integer")
    @TableField(value = "inquiry_price_id")
    private Integer inquiryPriceId;

    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "model")
    private String model;

    /**
     * 品牌
     */
    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "brand")
    private String brand;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", dataType = "Integer")
    @TableField(value = "amount")
    private Integer amount;

    /**
     * 批次
     */
    @ApiModelProperty(value = "批次", dataType = "String")
    @TableField(value = "batch")
    private String batch;

    /**
     * 封装
     */
    @ApiModelProperty(value = "封装", dataType = "String")
    @TableField(value = "encapsulation")
    private String encapsulation;

    /**
     * 目标价
     */
    @ApiModelProperty(value = "目标价", dataType = "BigDecimal")
    @TableField(value = "accept_price")
    private BigDecimal acceptPrice;

    /**
     * 交货期
     */
    @ApiModelProperty(value = "交货期", dataType = "Date")
    @TableField(value = "delivery_date")
    private Date deliveryDate;

    /**
     * 截止时间
     */
    @ApiModelProperty(value = "截止时间", dataType = "Date")
    @TableField(value = "deadline")
    private Date deadline;

    /**
     * mpq
     */
    @ApiModelProperty(value = "mpq", dataType = "Integer")
    @TableField(value = "mpq")
    private Integer mpq;

    /**
     * 包装
     */
    @ApiModelProperty(value = "包装", dataType = "String")
    @TableField(value = "packing")
    private String packing;

    /**
     * 状态。1:待询价，2:待报价，3:已报价，4:议价中，5:议价完成，6:已生成订单,7:议价失败, 8:超时未报价,9：无法报价
     */
    @ApiModelProperty(value = "状态。1:待询价，2:待报价，3:已报价，4:议价中，5:议价完成，6:已生成订单,7:议价失败, 8:超时未报价,9：无法报价", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "note")
    private String note;

    /**
     * bom单匹配选择后不可议价 议价标志: 1:可以议价 0:不可议价
     */
    @ApiModelProperty(value = "bom单匹配选择后不可议价 议价标志: 1:可以议价 0:不可议价", dataType = "Boolean")
    @TableField(value = "discussion_price_flag")
    private Boolean discussionPriceFlag;

    /**
     * erp 针对型号的询价id
     */
    @ApiModelProperty(value = "erp 针对型号的询价id", dataType = "String")
    @TableField(value = "erp_sub_inquiry_id")
    private String erpSubInquiryId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 最近修改时间
     */
    @ApiModelProperty(value = "最近修改时间", dataType = "Date")
    @TableField(value = "update_time")
    private Date updateTime;
}
