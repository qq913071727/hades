package com.innovation.ic.cc.base.model.cc;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 账号详细信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "User", description = "账号详细信息")
@TableName("user")
public class User {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

//    @ApiModelProperty(value = "Sql Server库users表中的ID", dataType = "String")
//    @TableField(value = "user_id")
//    private String userId;

    @ApiModelProperty(value = "站点 1 供应商协同系统、2 芯达通系统", dataType = "Integer")
    @TableField(value = "web_site")
    private Integer webSite;

    @ApiModelProperty(value = "主账号的id，如果为空，则表示这是主账号", dataType = "String")
    @TableField(value = "father_id")
    private String fatherId;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;

    @ApiModelProperty(value = "企业名称）", dataType = "String")
    @TableField(value = "enterprise_name")
    private String enterpriseName;

    @ApiModelProperty(value = "类型（1 卖家[供应商]） Seller Buyer", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    @TableField(value = "user_name")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "real_name")
    private String realName;

    @ApiModelProperty(value = "名称拼音", dataType = "String")
    @TableField(value = "pin_yin")
    private String pinYin;

    @ApiModelProperty(value = "职位：1表示经理，2表示员工", dataType = "Integer")
    @TableField(value = "position")
    private Integer position;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @TableField(value = "phone")
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    @TableField(value = "email")
    private String email;

//    @ApiModelProperty(value = "创建人ID", dataType = "String")
//    @TableField(value = "creator_id")
//    private String creatorId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "create_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createDate;

//    @ApiModelProperty(value = "修改人ID", dataType = "String")
//    @TableField(value = "modifier_id")
//    private String modifierId;

    @ApiModelProperty(value = "修改日期", dataType = "Date")
    @TableField(value = "modify_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date modifyDate;

    @ApiModelProperty(value = "状态（正常200 500停用）", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "评分", dataType = "Float")
    @TableField(value = "score")
    private Float score;
}
