package com.innovation.ic.cc.base.model.cc.erp9_pvecrm;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * erp公司注册信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CompanySCTopView", description = "ERP9公司注册信息")
@TableName("CompanyCCTopView")
public class CompanySCTopView {



    @ApiModelProperty(value = "供应商id", dataType = "String")
    @TableField(value = "ID")
    private String id;

    @ApiModelProperty(value = "供应商id", dataType = "String")
    @TableField(value = "Name")
    private String name;


    @ApiModelProperty(value = "国别", dataType = "String")
    @TableField(value = "District")
    private String district;


    @ApiModelProperty(value = "类型", dataType = "String")
    @TableField(value = "Type")
    private String type;


    @ApiModelProperty(value = "产品", dataType = "String")
    @TableField(value = "Products")
    private String products;

    @ApiModelProperty(value = "附件", dataType = "String")
    @TableField(value = "Files")
    private String files;






}
