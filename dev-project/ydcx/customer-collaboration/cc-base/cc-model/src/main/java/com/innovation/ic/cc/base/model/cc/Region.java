package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 地区查询
 */
@Data
@ApiModel(value = "region", description = "地区")
@TableName("region")
public class Region {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名字", dataType = "String")
    private String name;

    @ApiModelProperty(value = "区域国标编码", dataType = "String")
    private String statisCode;


    @ApiModelProperty(value = "区域编码", dataType = "String")
    private String code;


    @ApiModelProperty(value = "区域全称", dataType = "String")
    private String fullName;


    @ApiModelProperty(value = "类型,1 国家、2省份、3市、4县、5街道、6村 ", dataType = "String")
    private Integer regionType;

    @ApiModelProperty(value = "上级对象编号", dataType = "String")
    private Integer parentId;


}
