package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 收货地址
 */
@Data
@ApiModel(value = "ShippingAddress", description = "收货地址")
@TableName("shipping_address")
public class ShippingAddress{


    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @ApiModelProperty(value = "收货人", dataType = "String")
    private String consignee;

    @ApiModelProperty(value = "省份关联id", dataType = "Integer")
    private Integer province;

    @ApiModelProperty(value = "市区id", dataType = "Integer")
    private Integer city;

    @ApiModelProperty(value = "区id", dataType = "Integer")
    private Integer distinguish;

    @ApiModelProperty(value = "街道id", dataType = "Integer")
    private Integer street;

    @ApiModelProperty(value = "详细地址", dataType = "String")
    private String detailedAddress;

    @ApiModelProperty(value = "电话", dataType = "String")
    private String telephone;


    @ApiModelProperty(value = "1 为true,是否默认地址", dataType = "Integer")
    private Integer defaultAddress;


    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "创建人id", dataType = "Integer")
    private String createId;

    @ApiModelProperty(value = "省份名字", dataType = "Integer")
    private String provinceName;

    @ApiModelProperty(value = "市区名字", dataType = "Integer")
    private String cityName;


    @ApiModelProperty(value = "区名字", dataType = "Integer")
    private String distinguishName;

    @ApiModelProperty(value = "街道名字", dataType = "Integer")
    private String streetName;


}
