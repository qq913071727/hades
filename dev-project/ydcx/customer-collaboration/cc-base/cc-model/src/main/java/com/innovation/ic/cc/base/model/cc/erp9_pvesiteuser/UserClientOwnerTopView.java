package com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 账号关联销售id表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserClientOwnerTopView", description = "账号关联销售id表")
@TableName("UserClientOwnerTopView")
public class UserClientOwnerTopView {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "UserID", type = IdType.INPUT)
    private String userId;

    @ApiModelProperty(value = "销售id", dataType = "String")
    @TableField(value = "OwnerID")
    private String ownerId;

    @ApiModelProperty(value = "企业Id", dataType = "String")
    @TableField(value = "ClientID")
    private String clientId;

    @ApiModelProperty(value = "企业名称", dataType = "String")
    @TableField(value = "ClientName")
    private String clientName;
}
