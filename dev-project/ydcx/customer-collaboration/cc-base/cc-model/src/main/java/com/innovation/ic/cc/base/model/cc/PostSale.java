package com.innovation.ic.cc.base.model.cc;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 售后管理
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PostSale", description = "售后管理")
@TableName("post_sale")
public class PostSale {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "订单项", dataType = "String")
    @TableField(value = "order_no")
    private String orderNo;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "model")
    private String model;

    @ApiModelProperty(value = "类型。1.退货 2.换货 3.补货", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "售后数量", dataType = "Integer")
    @TableField(value = "amount")
    private Integer amount;

    @ApiModelProperty(value = "单价", dataType = "BigDecimal")
    @TableField(value = "unit_price")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "售后金额", dataType = "BigDecimal")
    @TableField(value = "total_price")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "币种。1表示人民币 2美元", dataType = "Integer")
    @TableField(value = "currency")
    private Integer currency;

    @ApiModelProperty(value = "申请日期", dataType = "Date")
    @TableField(value = "application_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date applicationDate;

    @ApiModelProperty(value = "运单号", dataType = "String")
    @TableField(value = "waybill_number")
    private String waybillNumber;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "note")
    private String note;
}
