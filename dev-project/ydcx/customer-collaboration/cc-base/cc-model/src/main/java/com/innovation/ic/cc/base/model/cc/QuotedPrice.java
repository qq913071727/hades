package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 报价表。与询价对应
 * </p>
 *
 * @author zengqinglong
 * @since 2023-02-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "quoted_price", description = "报价表。与询价对应")
@TableName("quoted_price")
public class QuotedPrice implements Serializable{
    /**
     * 报价主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 询价id
     */
    private Integer inquiryPriceId;

    /**
     * 询价单号
     */
    private String inquiryPriceNumber;

    /**
     * 我的公司id，对应my_company表的id
     */
    private Integer myCompanyId;

    /**
     * 我的公司名称，对应my_company表的name
     */
    private String myCompanyName;

    /**
     * 币种。1:人民币，2:美元
     */
    private Integer currency;

    /**
     * 截止时间
     */
    private Date deadline;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    private Integer inquiryType;

    /**
     * 交货地。1:大陆，2;香港
     */
    private Integer deliveryPlace;

    /**
     * 发票类型。1:增值税普通发票（纸质）-13%，2:增值税专用发票（纸质）-13.01%，3:普通发票（纸质）-0%
     */
    private Integer invoiceType;

    /**
     * 备注
     */
    private String note;

    /**
     * 型号列表，用空格分割
     */
    private String modelList;

    /**
     * 询价日期
     */
    private Date datetime;

    /**
     * BOM单号
     */
    private String bomNumber;

    /**
     * 询价人id，对应user表的id
     */
    private String userId;

    /**
     * 询价人真实姓名，对应user表的real_name
     */
    private String realName;

    /**
     * 状态。1:待采纳，2:部分采纳，3:超时未采纳，4:无效报价,5:已采纳
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最近修改时间
     */
    private Date updateTime;

}
