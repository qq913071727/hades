package com.innovation.ic.cc.base.model.cc;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * 系统公告
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-20
 */
@TableName("sys_announcement")
@ApiModel(value = "sys_announcement", description = "系统公告")
@Setter
@Getter
public class SysAnnouncement {


    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "主题")
    private String subject;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "启用： 0否 1是")
    private Integer enableFlag;

    @ApiModelProperty(value = "")
    private String createTime;

    @ApiModelProperty(value = "用户ID")
    private Integer createUserId;

    @ApiModelProperty(value = "用户昵称")
    private String createUserName;
}