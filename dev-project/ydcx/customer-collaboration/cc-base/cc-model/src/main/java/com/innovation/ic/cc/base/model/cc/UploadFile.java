package com.innovation.ic.cc.base.model.cc;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 上传文件
 */

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("upload_file")
public class UploadFile {


    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "文件名", dataType = "String")
    private String fileName;


    @ApiModelProperty(value = "文件后缀", dataType = "String")
    private String suffix;


    @ApiModelProperty(value = "系统生产唯一文件名", dataType = "String")
    private String onlyFileName;


    @ApiModelProperty(value = "默认图片 0", dataType = "int")
    private int type;

    @ApiModelProperty(value = "所属模块", dataType = "String")
    private String model;




    @ApiModelProperty(value = "关联Id,可用于关联查询", dataType = "String")
    private String relationId;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;


    @ApiModelProperty(value = "完整服务器路径", dataType = "String")
    private String fullPath;

    @ApiModelProperty(value = "状态", dataType = "String")
    private String status;


    @ApiModelProperty(value = "创建日期", dataType = "Date")
    private Date createDate;





}
