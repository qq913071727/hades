package com.innovation.ic.cc.base.model.cc;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 议价记录表。一次报价记录对应一个议价记录
 * </p>
 *
 * @author mybatis-generator
 * @since 2023-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("discussion_price_record")
@ApiModel(value = "DiscussionPriceRecord", description = "议价记录表。一次报价记录对应一个议价记录")
public class DiscussionPriceRecord {
    /**
     * 议价记录主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "议价记录主键", dataType = "Integer")
    private Integer id;

    /**
     * 报价id
     */
    @ApiModelProperty(value = "报价id", dataType = "Integer")
    @TableField(value = "quoted_price_id")
    private Integer quotedPriceId;

    /**
     * 询价id
     */
    @ApiModelProperty(value = "询价id", dataType = "Integer")
    @TableField(value = "inquiry_price_id")
    private Integer inquiryPriceId;

    /**
     * 询价记录表id
     */
    @ApiModelProperty(value = "询价记录表id", dataType = "Integer")
    @TableField(value = "inquiry_price_record_id")
    private Integer inquiryPriceRecordId;

    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "model")
    private String model;

    /**
     * 品牌
     */
    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "brand")
    private String brand;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", dataType = "Integer")
    @TableField(value = "amount")
    private Integer amount;

    /**
     * 批次
     */
    @ApiModelProperty(value = "批次", dataType = "String")
    @TableField(value = "batch")
    private String batch;

    /**
     * 封装
     */
    @ApiModelProperty(value = "封装", dataType = "String")
    @TableField(value = "encapsulation")
    private String encapsulation;

    /**
     * 目标价
     */
    @ApiModelProperty(value = "目标价", dataType = "BigDecimal")
    @TableField(value = "accept_price")
    private BigDecimal acceptPrice;

    /**
     * 交货期
     */
    @ApiModelProperty(value = "交货期", dataType = "Date")
    @TableField(value = "delivery_date")
    private Date deliveryDate;

    /**
     * 截止时间
     */
    @ApiModelProperty(value = "截止时间", dataType = "Date")
    @TableField(value = "deadline")
    private Date deadline;

    /**
     * mpq
     */
    @ApiModelProperty(value = "mpq", dataType = "Integer")
    @TableField(value = "mpq")
    private Integer mpq;

    /**
     * 包装
     */
    @ApiModelProperty(value = "包装", dataType = "String")
    @TableField(value = "packing")
    private String packing;

    /**
     * 状态。0:取消议价，1:议价完成，2:议价中 3:议价失败
     */
    @ApiModelProperty(value = "状态。0:取消议价，1:议价完成，2:议价中 3:议价失败", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "note")
    private String note;

    /**
     * 币种 1:人民币 2:美元
     */
    @ApiModelProperty(value = "币种 1:人民币 2:美元", dataType = "Integer")
    @TableField(value = "currency")
    private Integer currency;

    /**
     * 交货地。1:大陆，2:香港
     */
    @ApiModelProperty(value = "交货地。1:大陆，2:香港", dataType = "Integer")
    @TableField(value = "delivery_place")
    private Integer deliveryPlace;

    /**
     * erp 针对型号的议价id
     */
    @ApiModelProperty(value = "erp 针对型号的议价id", dataType = "String")
    @TableField(value = "deadline")
    private String erpSubDiscussionId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 最近修改时间
     */
    @ApiModelProperty(value = "最近修改时间", dataType = "Date")
    @TableField(value = "update_time")
    private Date updateTime;
}
