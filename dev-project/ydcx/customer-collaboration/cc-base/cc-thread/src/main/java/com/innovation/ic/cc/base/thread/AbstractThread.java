package com.innovation.ic.cc.base.thread;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cc.base.pojo.constant.handler.RabbitMqConstants;

/**
 * @desc   抽象Thread
 * @author linuo
 * @time   2022年10月18日14:03:57
 */
public abstract class AbstractThread {
    /**
     * 获取消息体中的数据内容
     * @param bodyString 消息体
     * @return 返回消息体中的数据内容
     */
    protected String getMessageBody(String bodyString) {
        String messageBody = null;
        JSONObject json = (JSONObject) JSONObject.parse(bodyString);
        if (json != null && !json.isEmpty()) {
            messageBody = json.getString(RabbitMqConstants.MESSAGE_BODY);
        }
        return messageBody;
    }
}