package com.innovation.ic.cc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.Sale;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UsersTopView;
import com.innovation.ic.cc.base.pojo.UserMainPojo;
import com.innovation.ic.cc.base.pojo.constant.UserConstants;
import com.innovation.ic.cc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.cc.base.pojo.constant.model.DefaultUserConstants;
import com.innovation.ic.cc.base.service.cc.SaleService;
import com.innovation.ic.cc.base.service.cc.UserService;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author swq
 * @desc 监听主账号新增/修改队列
 * @time 2023年5月17日10:12:24
 */
public class ListenModelAddUsersQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private UserService userService;
    private SaleService saleService;

    public ListenModelAddUsersQueueThread(Channel channel, UserService userService, SaleService saleService) {
        this.channel = channel;
        this.userService = userService;
        this.saleService = saleService;
    }

    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.CC_ERP_EXCHANGE;
        //String queue = RabbitMqConstants.ERP_TO_SC_QUEUE_NAME;
        String routingKey = RabbitMqConstants.ERP_CC_ADD_USERS;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyString = new String(body);
                logger.info("接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            UserMainPojo userMainPojo = JSONObject.parseObject(messageBody, UserMainPojo.class);
                            if (userMainPojo != null) {
                                //处理账号信息
                                userService.handleModelAddUsersMqMsg(userMainPojo);
                                if (userMainPojo.getOperationType() == UserConstants.TYPE_ONE) {//为新增才会执行
                                    //处理销售信息
                                    saleService.handleModelAddSaleMqMsg(userMainPojo);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}