package com.innovation.ic.cc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.cc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.cc.base.service.cc.ErpMyCompanyNameService;
import com.innovation.ic.cc.base.vo.ErpMyCompanyNameVo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc   监听erp我的公司添加事件队列
 *
 */
public class ListenAddErpCompanyQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private ErpMyCompanyNameService erpMyCompanyNameService;

    public ListenAddErpCompanyQueueThread(Channel channel, ErpMyCompanyNameService erpMyCompanyNameService) {
        this.channel = channel;
        this.erpMyCompanyNameService = erpMyCompanyNameService;
    }

    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.CC_ERP_EXCHANGE;
        //String queue = RabbitMqConstants.ERP_TO_SC_QUEUE_NAME;
        String routingKey = RabbitMqConstants.ERP_CC_ADD_COMPANY;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                String bodyString = new String(body);
                logger.info("添加我的公司 , 接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            ErpMyCompanyNameVo erpMyCompanyNameVo = JSONObject.parseObject(messageBody, ErpMyCompanyNameVo.class);
                            erpMyCompanyNameService.saveOrUpdateErpCompany(erpMyCompanyNameVo);
                        }
                    }
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}