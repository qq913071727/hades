package com.innovation.ic.cc.base.thread;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc   执行脚本导入redis数据
 * @author zengqinglong
 * @time   2023-04-25
 */
public class ImportRedisDataThread extends AbstractThread implements Runnable{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String[] scripts;

    public ImportRedisDataThread(String[] scripts) {
        this.scripts = scripts;
    }

    @SneakyThrows
    public void run() {
        // 休眠3分钟，防止出现服务未注册到nacos导致调用接口失败的问题
        Thread.sleep(1000 * 60 * 3);
        if(scripts != null){
            logger.info("-----开始执行命令-----");
            for(String str : scripts){
                logger.info("执行脚本 -> {}", str);
                Thread.sleep(1000 * 2);
                try {
                    Process exec = Runtime.getRuntime().exec(str);
                    exec.waitFor();
                }catch (Exception e){
                    logger.error("执行脚本[{}]出现问题,原因:", str, e);
                }
            }
            logger.info("-----执行命令结束-----");
        }
    }
}