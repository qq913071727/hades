package com.innovation.ic.cc.base.vo.quotedPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 报价列表入参
 * @Date 2023/2/24 13:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QuotedPricePageListVo", description = "报价列表入参")
public class QuotedPricePageListVo {
    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    /**
     * 询价单号
     */
    @ApiModelProperty(value = "询价单号", dataType = "String")
    private String inquiryPriceNumber;

    /**
     * 询价类型。1:实单询价，2:普通询价 ,3:BOM询价
     */
    @ApiModelProperty(value = "询价类型。1:实单询价，2:普通询价 ,3:BOM询价", dataType = "Integer")
    private Integer inquiryType;


    @ApiModelProperty(value = "开始时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @ApiModelProperty(value = "结束时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    @NotNull(message = "当前页,不能为空")
    private Integer pageNo;

    @ApiModelProperty(value = "显示几条", dataType = "Integer")
    @NotNull(message = "显示几条,不能为空")
    private Integer pageSize;
}

