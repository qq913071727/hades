package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
        import java.math.BigDecimal;
    import lombok.Getter;
import lombok.Setter;

/**
 * 收货单运单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@ApiModel(value = "ReceiptShippingVo", description = "收货单运单 请求类")
@Setter
@Getter
public class ReceiptShippingVo {

                                                        

    
                                                            @ApiModelProperty(value = "")
            private Long id;
                    
                                                            @ApiModelProperty(value = "运单号")
            private String shippingCode;
                    
                                                            @ApiModelProperty(value = "运单状态： 10待收货 20部分收货 30已收货 ")
            private Integer shippingState;
                    
                                                            @ApiModelProperty(value = "运单型号")
            private String shippingModel;
                    
                                                            @ApiModelProperty(value = "运单品牌")
            private String shippingBrand;
                    
                                                            @ApiModelProperty(value = "运单数量")
            private Integer shippingCount;
                    
                                                            @ApiModelProperty(value = "运单批次号")
            private Integer shippingBatch;
                    
                                                            @ApiModelProperty(value = "运单封装")
            private String shippingPrivate;
                    
                                                            @ApiModelProperty(value = "运单单价")
            private BigDecimal shippingUnitPrice;
                    
                                                            @ApiModelProperty(value = "运单总金额")
            private BigDecimal shippingTotalPrice;
                    
                                                            @ApiModelProperty(value = "订单项")
            private String shippingOrderTerm;
                    
                                                            @ApiModelProperty(value = "运单备注")
            private String shippingDesc;
                    }