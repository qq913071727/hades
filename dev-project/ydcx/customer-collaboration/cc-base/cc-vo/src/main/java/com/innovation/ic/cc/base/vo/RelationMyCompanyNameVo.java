package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 关联关系添加
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RelationMyCompanyNameVo {
    @ApiModelProperty(value = "公司ID", dataType = "Integer")
    private String ID;

    @ApiModelProperty(value = "关联公司ID", dataType = "String")
    private String SubID;


    @ApiModelProperty(value = "收款账号ID", dataType = "String")
    private String BookAccountID;

    @ApiModelProperty(value = "收款公司", dataType = "String")
    private String Title;



    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;


    @ApiModelProperty(value = "审核状态", dataType = "String")
    private Integer Status;

    @ApiModelProperty(value = "附件", dataType = "String")
    private String Files;

    @ApiModelProperty(value = "我的公司主键id", dataType = "String")
    private String myCompanyId;


    @ApiModelProperty(value = "SwiftCode", dataType = "String")
    private String SwiftCode;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String Currency;



    @ApiModelProperty(value = "备注", dataType = "String")
    private String Summary;





}