package com.innovation.ic.cc.base.vo.InquiryPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @author zengqinglong
 * @desc 询价品牌型号列表入参
 * @Date 2023/2/27 11:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceRecordListVo", description = "询价品牌型号列表入参")
public class InquiryPriceRecordListVo {
    @ApiModelProperty(value = "询价id", dataType = "String")
    private String inquiryPriceId;
    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    /**
     * 状态。
     */
    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    @ApiModelProperty(value = "询价类型 1:实单询价，2:普通询价", dataType = "Integer")
    private Integer inquiryType;
}
