package com.innovation.ic.cc.base.vo.quotedPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 报价 一键采纳/不接受入参
 * @Date 2023/3/1 13:55
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QuotedPriceAdoptionOrNotAcceptedVo", description = "报价 一键采纳/不接受入参")
public class QuotedPriceAdoptionOrNotAcceptedVo {
    @ApiModelProperty(value = "报价id", dataType = "String")
    @NotBlank(message = "报价id 不能为空")
    private String quotedPriceId;

    @ApiModelProperty(value = "报价品牌型号id", dataType = "List")
    @NotEmpty(message = "报价id 不能为空")
    private List<String> ids;

    @ApiModelProperty(value = "一键采纳 :3:有效报价单，不接受 :4:无效报价单", dataType = "Integer")
    @NotNull(message ="按钮类型 不能为空")
    private Integer status;
}
