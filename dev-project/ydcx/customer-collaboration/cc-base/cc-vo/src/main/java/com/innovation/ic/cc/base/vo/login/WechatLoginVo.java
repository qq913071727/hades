package com.innovation.ic.cc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   微信登录的Vo类
 * @author linuo
 * @time   2022年8月17日11:33:08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "WechatLoginVo", description = "微信登录的Vo类")
public class WechatLoginVo {
    @ApiModelProperty(value = "临时授权码", dataType = "String")
    private String authCode;
}