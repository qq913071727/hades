package com.innovation.ic.cc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   绑定第三方账号的Vo类
 * @author linuo
 * @time   2022年8月17日16:46:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BandThirdAccountVo", description = "绑定第三方账号的Vo类")
public class BindThirdAccountVo {
    @ApiModelProperty(value = "绑定账号类型(0 微信、1 QQ、3 支付宝)", dataType = "String")
    private String type;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String passWord;

    @ApiModelProperty(value = "用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的", dataType = "String")
    private String unionID;

    @ApiModelProperty(value = "授权用户唯一标识", dataType = "String")
    private String openID;
}