package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "CompanyCurrencyVo", description = "我的公司币种Vo")
public class CompanyCurrencyVo {

    private String ID;
    private String Name;
    private String EnName;
    private String Symbol;
    private String ShortSymbol;
    private Integer NumericCode;
    private Integer OrderIndex;


}
