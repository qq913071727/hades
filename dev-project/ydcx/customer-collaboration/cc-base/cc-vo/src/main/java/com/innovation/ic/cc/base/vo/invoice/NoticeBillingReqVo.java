package com.innovation.ic.cc.base.vo.invoice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.List;

/**
 * @desc   通知开票接口的Vo类
 * @author linuo
 * @time   2023年5月17日14:34:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "NoticeBillingReqVo", description = "通知开票接口的Vo类")
public class NoticeBillingReqVo implements Serializable {
    @ApiModelProperty(value = "订单项集合", dataType = "List<String>")
    private List<String> orderItemList;
}