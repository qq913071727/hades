package com.innovation.ic.cc.base.vo.InquiryPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 询价型号记录
 * @Date 2023/2/15 13:30
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceRecordVo", description = "询价型号记录")
public class InquiryPriceRecordVo {
    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    /**
     * 品牌
     */
    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer amount;

    /**
     * 批次
     */
    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    /**
     * 封装
     */
    @ApiModelProperty(value = "封装", dataType = "String")
    private String encapsulation;

    /**
     * 目标价
     */
    @ApiModelProperty(value = "目标价", dataType = "BigDecimal")
    private BigDecimal acceptPrice;

    /**
     * 交货期
     */
    @ApiModelProperty(value = "交货期", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deliveryDate;

    /**
     * mpq
     */
    @ApiModelProperty(value = "mpq", dataType = "Integer")
    private Integer mpq;

    /**
     * 包装
     */
    @ApiModelProperty(value = "包装", dataType = "String")
    private String packing;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", dataType = "String")
    private String note;
}
