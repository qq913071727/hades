package com.innovation.ic.cc.base.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 售后管理的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PostSaleVo", description = "售后管理的Vo类")
public class PostSaleVo {

    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    @ApiModelProperty(value = "订单项", dataType = "String")
    private String orderNo;

    @ApiModelProperty(value = "运单号", dataType = "String")
    private String waybillNumber;

    @ApiModelProperty(value = "类型。1.退货 2.换货 3.补货", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "开始时间(与结束时间并存)", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startData;

    @ApiModelProperty(value = "结束时间(与开始时间并存)", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ceaseData;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "显示几条", dataType = "Integer")
    private Integer pageSize;
}
