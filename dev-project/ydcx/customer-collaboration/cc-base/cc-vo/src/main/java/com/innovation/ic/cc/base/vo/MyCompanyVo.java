package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 我的公司
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyVo", description = "我的公司")
public class MyCompanyVo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "国别地区id", dataType = "String")
    private String countryRegionId;


    @ApiModelProperty(value = "公司类型id", dataType = "String")
    private String companyTypeId;


    @ApiModelProperty(value = "主要产品", dataType = "String")
    private String mainProducts;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String currency;



    @ApiModelProperty(value = "发票类型", dataType = "String")
    private String invoiceType;


    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String createId;


    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;


    @ApiModelProperty(value = "创建人名字", dataType = "String")
    private String creatorUserName;


    @ApiModelProperty(value = "上传图片的ids,包含多个", dataType = "List<String>")
    private List<String> pictureIds;


}
