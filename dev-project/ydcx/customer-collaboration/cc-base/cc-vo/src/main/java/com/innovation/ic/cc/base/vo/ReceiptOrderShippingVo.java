package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 收货单订单和收货单运单关联表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@ApiModel(value = "ReceiptOrderShippingVo", description = "收货单订单和收货单运单关联表 请求类")
@Setter
@Getter
public class ReceiptOrderShippingVo {


    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "收货单订单id")
    private Long receiptOrderId;

    @ApiModelProperty(value = "收货单运单id")
    private Long receiptShippingId;
}