package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DepartmentVo", description = "部门的Vo类")
public class DepartmentVo {

    @ApiModelProperty(value = "部门id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "部门名称", dataType = "String")
    private String departmentName;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

}
