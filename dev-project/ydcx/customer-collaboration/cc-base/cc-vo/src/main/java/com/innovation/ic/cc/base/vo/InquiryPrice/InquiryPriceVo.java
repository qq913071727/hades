package com.innovation.ic.cc.base.vo.InquiryPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价
 * @Date 2023/2/15 13:22
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceVo", description = "询价单")
public class InquiryPriceVo {
    /**
     * 我的公司id，对应my_company表的id
     */
    @ApiModelProperty(value = "我的公司id", dataType = "Integer")
    private Integer myCompanyId;
    /**
     * 询价有效期
     */
    @ApiModelProperty(value = "询价有效期", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;
    /**
     * 询价类型。1:表示实单询价，2:表示普通询价
     */
    @ApiModelProperty(value = "询价类型。1:实单询价，2:普通询价", dataType = "Integer")
    private Integer inquiryType;
    /**
     * 询价型号列表
     */
    @ApiModelProperty(value = "型号列表", dataType = "List")
    private List<InquiryPriceRecordVo> inquiryPriceRecordVoList = new ArrayList<InquiryPriceRecordVo>();
}
