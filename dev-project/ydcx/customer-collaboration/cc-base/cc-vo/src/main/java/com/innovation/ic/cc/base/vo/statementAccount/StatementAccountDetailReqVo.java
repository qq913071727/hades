package com.innovation.ic.cc.base.vo.statementAccount;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * @desc   获取对账单详情接口的Vo类
 * @author linuo
 * @time   2023年5月17日13:47:33
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StatementAccountDetailReqVo", description = "获取对账单详情接口的Vo类")
public class StatementAccountDetailReqVo implements Serializable {
    @ApiModelProperty(value = "对账单号", dataType = "String")
    private String statementNumber;
}