package com.innovation.ic.cc.base.vo.discussionPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 议价 入参
 * @Date 2023/3/1 16:23
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DiscussionPriceCreatedVo", description = "议价 入参")
public class DiscussionPriceCreatedVo {
    /**
     * 询价记录id
     */
    @ApiModelProperty(value = "询价记录id", dataType = "Integer")
    private Integer inquiryPriceId;

    /**
     * 目标价格
     */
    @ApiModelProperty(value = "目标价格", dataType = "BigDecimal")
    private BigDecimal acceptPrice;

    /**
     * 议价数量
     */
    @ApiModelProperty(value = "议价数量", dataType = "Integer")
    private Integer amount;

    /**
     * 交货期
     */
    @ApiModelProperty(value = "交货期", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deliveryDate;


    /**
     * 议价有效期
     */
    @ApiModelProperty(value = "议价有效期", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", dataType = "Date")
    private String note;
}


