package com.innovation.ic.cc.base.vo.inventoryCollect;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @desc   收藏、取消收藏库存数据接口的Vo类
 * @author linuo
 * @time   2023年5月16日16:52:32
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CollectInventoryAddReqVo", description = "收藏、取消收藏库存数据接口的Vo类")
public class InventoryCollectCancleReqVo implements Serializable {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "价格", dataType = "BigDecimal")
    private BigDecimal price;

    @ApiModelProperty(value = "库存所在地(中国大陆、中国香港)", dataType = "String")
    private String inventoryHome;
}