package com.innovation.ic.cc.base.vo.quotedPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @author zengqinglong
 * @desc 询价品牌型号列表入参
 * @Date 2023/2/27 11:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceRecordListVo", description = "询价品牌型号列表入参")
public class QuotedPriceRecordListVo {
    @ApiModelProperty(value = "报价id", dataType = "String")
    @NotBlank(message = "报价id,不能为空")
    private String quotedPriceId;
    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    /**
     * 状态。1:待处理，2:报价中，3:超时未报价，4:无法报价，5:已报价，6:已生成报价单,7:已生成报价单, 8:待确定
     */
    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;
}
