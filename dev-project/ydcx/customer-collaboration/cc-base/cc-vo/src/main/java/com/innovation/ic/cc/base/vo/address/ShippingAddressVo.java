package com.innovation.ic.cc.base.vo.address;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 收货地址Vo
 */
@Data
@ApiModel(value = "ShippingAddressVo", description = "收货地址Vo")
public class ShippingAddressVo {

    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "收货人", dataType = "String")
    private String consignee;

    @ApiModelProperty(value = "省份关联id", dataType = "Integer")
    private Integer province;

    @ApiModelProperty(value = "市区id", dataType = "Integer")
    private Integer city;

    @ApiModelProperty(value = "区id", dataType = "Integer")
    private Integer distinguish;

    @ApiModelProperty(value = "街道id", dataType = "Integer")
    private Integer street;

    @ApiModelProperty(value = "是否默认 1 为默认", dataType = "Integer")
    private Integer defaultAddress;

    @ApiModelProperty(value = "详细地址", dataType = "String")
    private String detailedAddress;


    @ApiModelProperty(value = "电话", dataType = "String")
    private String telephone;


    @ApiModelProperty(value = "创建人id", dataType = "Integer")
    private String createId;


}
