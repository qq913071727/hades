package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * erp字典表Vo
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErpDictionariesVo {

    @ApiModelProperty(value = "枚举ID", dataType = "String")
    private String ID;

    @ApiModelProperty(value = "枚举名称", dataType = "String")
    private String Name;

    @ApiModelProperty(value = "枚举项值ID", dataType = "String")
    private String ArgumentID;

    @ApiModelProperty(value = "枚举项文本", dataType = "String")
    private String Text;

    @ApiModelProperty(value = "枚举项值", dataType = "String")
    private String Value;

}