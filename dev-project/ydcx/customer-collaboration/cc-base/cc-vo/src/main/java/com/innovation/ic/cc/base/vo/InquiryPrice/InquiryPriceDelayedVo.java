package com.innovation.ic.cc.base.vo.InquiryPrice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 询价延时
 * @Date 2023/5/10 15:14
 **/
@Data
public class InquiryPriceDelayedVo {
    /**
     * 询价id
     */
    @ApiModelProperty(value = "询价id", dataType = "Integer")
    private Integer id;

    /**
     * 询价有效期
     */
    @ApiModelProperty(value = "询价有效期", dataType = "Date")
    private Date  deadline;
}
