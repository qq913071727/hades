package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class FileVo implements Serializable {
    private static final long serialVersionUID = -4502262131052618219L;

    /***
     * 文件名字
     */
    private String FileName;

    /**
     * 完整url
     */
    private String Url;
}