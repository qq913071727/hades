package com.innovation.ic.cc.base.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 修改密码的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UpdatePasswordVo", description = "账号的vo类")
public class UpdatePasswordVo {

    @ApiModelProperty(value = "密码", dataType = "String")
    private String oldPassword;

    @ApiModelProperty(value = "新密码", dataType = "String")
    private String newPassword;
}