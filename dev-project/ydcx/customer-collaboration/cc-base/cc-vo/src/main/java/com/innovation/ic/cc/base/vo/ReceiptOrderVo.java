package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单收货单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@ApiModel(value = "ReceiptOrderVo", description = "订单收货单 请求类")
@Setter
@Getter
public class ReceiptOrderVo {

    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "订单号")
    private String orderCode;

    @ApiModelProperty(value = "收货状态: 10待收货 20部分收货 30已收货")
    private Integer orderState;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderPrice;

    @ApiModelProperty(value = "币种：1USD 2RMD")
    private Integer orderCurrency;

    @ApiModelProperty(value = "订单交货方式")
    private Integer orderDelivery;

    @ApiModelProperty(value = "交货地")
    private String orderDeliveryAddress;

    @ApiModelProperty(value = "发票类型：1增值税专用发票")
    private Integer orderInvoiceType;

    @ApiModelProperty(value = "采购人ID")
    private String orderProcureUserId;

    @ApiModelProperty(value = "采购人名称")
    private String orderProcureUserName;

    @ApiModelProperty(value = "下单时间")
    private String orderTime;

    @ApiModelProperty(value = "")
    private String createTime;

    @ApiModelProperty(value = "")
    private String updateTime;

    @ApiModelProperty(value = "下单开始时间")
    private String beginDate;

    @ApiModelProperty(value = "下单结束时间")
    private String endDate;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "运单号")
    private String shippingCode;




}