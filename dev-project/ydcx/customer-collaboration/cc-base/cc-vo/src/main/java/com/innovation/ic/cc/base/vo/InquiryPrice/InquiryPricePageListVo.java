package com.innovation.ic.cc.base.vo.InquiryPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价列表入参
 * @Date 2023/2/24 13:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPricePageListVo", description = "询价列表入参")
public class InquiryPricePageListVo {
    /**
     * 我的公司id
     */
    @ApiModelProperty(value = "我的公司id", dataType = "Integer")
    private Integer myCompanyId;

    /**
     * 状态 0:全部 1:待询价 2:待报价 3:已报价 4:议价中 5:已生成订单 6:超时未报价 7:无效询价
     */
    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;

    /**
     * 型号
     */
    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    /**
     * 询价单号
     */
    @ApiModelProperty(value = "询价单号", dataType = "String")
    private String inquiryPriceNumber;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    @ApiModelProperty(value = "询价类型。1:实单询价，2:普通询价", dataType = "Integer")
    private Integer inquiryType;

    @ApiModelProperty(value = "开始时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @ApiModelProperty(value = "结束时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "显示几条", dataType = "Integer")
    private Integer pageSize;

    /**
     * 主账号 能看当前所以下的单 ， 不是主账号需要分 级别 经理能看到 手底下员工的信息 员工级别只能看到自己的信息
     */
    private List<String> userIds;

    /**
     * 不同 状态 包含其他状态
     */
    private List<Integer> statusList;
}

