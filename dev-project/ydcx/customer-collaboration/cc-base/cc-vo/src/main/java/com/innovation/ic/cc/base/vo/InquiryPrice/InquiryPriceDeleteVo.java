package com.innovation.ic.cc.base.vo.InquiryPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价品牌型号删除入参
 * @Date 2023/2/27 16:55
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceDeleteVo", description = "询价品牌型号删除入参")
public class InquiryPriceDeleteVo {
    @ApiModelProperty(value = "询价id", dataType = "String")
    private String inquiryPriceId;

    @ApiModelProperty(value = "询价品牌型号id", dataType = "List")
    private List<String> ids;
}
