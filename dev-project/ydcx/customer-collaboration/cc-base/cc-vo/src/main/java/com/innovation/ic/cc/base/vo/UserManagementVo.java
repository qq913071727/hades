package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 人员管理的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserManagementVo", description = "人员管理的vo类")
public class UserManagementVo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "部门id", dataType = "Integer")
    private Integer departmentId;

    @ApiModelProperty(value = "被管理用户的id", dataType = "List")
    private List<String> managedUserId;

}
