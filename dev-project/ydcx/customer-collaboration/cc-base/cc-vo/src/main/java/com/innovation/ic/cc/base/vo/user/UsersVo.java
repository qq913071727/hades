package com.innovation.ic.cc.base.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.util.List;

/**
 * 账号的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UsersVo", description = "账号的vo类")
public class UsersVo {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "主账号的id", dataType = "String")
    private String fatherId;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "新密码", dataType = "String")
    private String newPassword;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "职位：1表示经理，2表示员工", dataType = "Integer")
    private Integer position;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "显示几条", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "账号状态", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "认证token", dataType = "String")
    private String token;

    @ApiModelProperty(value = "日期时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date time;

    @ApiModelProperty(value = "短信验证码", dataType = "String")
    private String code;

    @ApiModelProperty(value = "电子邮箱", dataType = "String")
    private String email;

    @ApiModelProperty(value = "权限id", dataType = "String")
    private String roleId;

    @ApiModelProperty(value = "角色相关信息", dataType = "List")
    private List<String> roleListId;
}