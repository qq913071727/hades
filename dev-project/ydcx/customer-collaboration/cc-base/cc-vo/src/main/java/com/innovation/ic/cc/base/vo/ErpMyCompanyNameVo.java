package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 保存我的公司,同步erp
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErpMyCompanyNameVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private String ID;

    @ApiModelProperty(value = "name", dataType = "String")
    private String Name;

    @ApiModelProperty(value = "国别", dataType = "String")
    private String District;

    @ApiModelProperty(value = "客户类型", dataType = "String")
    private String Type;

    @ApiModelProperty(value = "客户类型", dataType = "String")
    private String Products;

    @ApiModelProperty(value = "客户类型", dataType = "String")
    private String Files;


    @ApiModelProperty(value = "状态", dataType = "String")
    private String Status;


}