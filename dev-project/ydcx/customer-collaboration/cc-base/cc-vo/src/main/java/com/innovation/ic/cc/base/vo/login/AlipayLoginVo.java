package com.innovation.ic.cc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   支付宝扫码登录的Vo类
 * @author linuo
 * @time   2022年8月16日09:08:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AlipayLoginVo", description = "支付宝扫码登录的Vo类")
public class AlipayLoginVo {

    @ApiModelProperty(value = "临时授权码", dataType = "String")
    private String authCode;
}