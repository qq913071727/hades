package com.innovation.ic.cc.base.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 忘记密码的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ForgetPasswordVo", description = "忘记密码的vo类")
public class ForgetPasswordVo {

    @ApiModelProperty(value = "账号", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String passWord;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "验证码", dataType = "String")
    private String code;

    @ApiModelProperty(value = "站点 1 供应商协同系统、2 客户协同系统、3 芯达通系统", dataType = "Integer")
    private Integer webSite;

    @ApiModelProperty(value = " 1 Seller 卖家(供应商)，2 Buyer 买家(客户)", dataType = "Integer")
    private Integer type;

}
