package com.innovation.ic.cc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   通过账号密码登录接口的vo类
 * @author linuo
 * @time   2023年5月18日16:07:40
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LoginByUserNameVo", description = "通过账号密码登录接口的vo类")
public class LoginByUserNameVo {
    @ApiModelProperty(value = "登入账户", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "登录类型", dataType = "Integer")
    private Integer webSite;
}