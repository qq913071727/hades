package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 系统公告
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-20
 */
@ApiModel(value = "SysAnnouncementVo", description = "系统公告 请求类")
@Setter
@Getter
public class SysAnnouncementVo {

    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "主题")
    private String subject;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "启用： 0否 1是")
    private Integer enableFlag;

    @ApiModelProperty(value = "")
    private String createTime;

    @ApiModelProperty(value = "用户ID")
    private Integer createUserId;

    @ApiModelProperty(value = "用户昵称")
    private String createUserName;
}