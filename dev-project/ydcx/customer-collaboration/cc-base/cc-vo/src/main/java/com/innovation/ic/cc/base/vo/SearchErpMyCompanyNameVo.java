package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class SearchErpMyCompanyNameVo implements Serializable {
    private static final long serialVersionUID = -403395427578851727L;

    @ApiModelProperty(value = "公司名称", dataType = "string")
    private String name;

    @ApiModelProperty(value = "当前页数", dataType = "Integer")
    private Integer pageNum;

    @ApiModelProperty(value = "每页显示多少", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "前端不用理会,条数", dataType = "Integer")
    private Integer pageParam;

    @ApiModelProperty(value = "前端不用理会,状态", dataType = "String")
    private Integer status;
}