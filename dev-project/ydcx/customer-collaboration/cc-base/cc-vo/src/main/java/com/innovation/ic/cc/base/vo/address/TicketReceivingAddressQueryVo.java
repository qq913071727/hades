package com.innovation.ic.cc.base.vo.address;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 收票地址Vo
 */
@Data
@ApiModel(value = "TicketReceivingAddressVo", description = "收票地址查询Vo")
public class TicketReceivingAddressQueryVo {


    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;


    @ApiModelProperty(value = "每页显示多少", dataType = "Integer")
    private Integer pageSize;


    @ApiModelProperty(value = "当前页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String createId;

}
