package com.innovation.ic.cc.base.vo.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单树
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MenuTree", description = "菜单树")
public class MenuTreeVo {

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "菜单节点列表", dataType = "List")
    private List<MenuVo> menuNodeList = new ArrayList<MenuVo>();
}
