package com.innovation.ic.cc.base.vo.address;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 国家地区查询Vo
 */
@Data
@ApiModel(value = "RegionQueryVo", description = "国家地区查询Vo")
public class RegionQueryVo {

    @ApiModelProperty(value = "每页显示多少", dataType = "Integer",required = true)
    private Integer pageSize;


    @ApiModelProperty(value = "当前页数", dataType = "Integer",required = true)
    private Integer pageNo;

    @ApiModelProperty(value = "查询当前id下的区域,为空则默认第一层", dataType = "String")
    private Integer id;


}
