package com.innovation.ic.cc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   QQ登录的Vo类
 * @author linuo
 * @time   2022年8月18日13:28:56
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QQLoginVo", description = "QQ登录的Vo类")
public class QQLoginVo {
    @ApiModelProperty(value = "临时授权码", dataType = "String")
    private String authCode;
}