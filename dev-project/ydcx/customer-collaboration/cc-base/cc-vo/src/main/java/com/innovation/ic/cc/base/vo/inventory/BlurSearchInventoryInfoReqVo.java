package com.innovation.ic.cc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * @desc   模糊查询库存信息的请求Vo类
 * @author linuo
 * @time   2023年5月15日10:41:44
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BlurSearchInventoryInfoReqVo", description = "模糊查询库存信息的请求Vo类")
public class BlurSearchInventoryInfoReqVo implements Serializable {
    @ApiModelProperty(value = "型号", dataType = "string")
    private String partNumber;
}