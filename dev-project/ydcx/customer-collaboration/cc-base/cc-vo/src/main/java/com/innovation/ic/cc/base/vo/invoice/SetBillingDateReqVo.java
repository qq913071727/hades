package com.innovation.ic.cc.base.vo.invoice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.List;

/**
 * @desc   设置开票日接口的Vo类
 * @author linuo
 * @time   2023年5月17日14:39:11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SetBillingDateReqVo", description = "设置开票日接口的Vo类")
public class SetBillingDateReqVo implements Serializable {
    @ApiModelProperty(value = "订单项集合", dataType = "List<String>")
    private List<String> orderItemList;
}