package com.innovation.ic.cc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典表
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DictionariesTypeVo {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String ID;

    @ApiModelProperty(value = "字典描述", dataType = "String")
    private String Name;
}