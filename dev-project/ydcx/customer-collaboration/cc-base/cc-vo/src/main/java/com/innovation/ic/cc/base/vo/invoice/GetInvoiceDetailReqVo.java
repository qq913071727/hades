package com.innovation.ic.cc.base.vo.invoice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * @desc   获取订单详情接口的Vo类
 * @author linuo
 * @time   2023年5月17日14:43:10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "GetInvoiceDetailReqVo", description = "获取订单详情接口的Vo类")
public class GetInvoiceDetailReqVo implements Serializable {
    @ApiModelProperty(value = "发票号", dataType = "String")
    private String invoiceNumber;
}