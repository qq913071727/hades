package com.innovation.ic.cc.base.vo.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户和角色的关系
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MapUserRoleVo", description = "用户和角色的关系")
public class MapUserRoleVo {

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "角色id", dataType = "String")
    private String roleId;
}
