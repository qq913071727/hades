package com.innovation.ic.cc.base.handler.cc;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayUserInfoShareRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.cc.base.pojo.constant.LoginConstants;
import com.innovation.ic.cc.base.pojo.constant.third_party_api.AlipayConstants;
import com.innovation.ic.cc.base.pojo.enums.LoginTypeEnum;
import org.apache.http.HttpStatus;

/**
 * @desc   支付宝相关工具类
 * @author linuo
 * @time   2022年8月15日14:37:18
 */
public class AlipayHandler {
    /**
     * 获取支付宝登录授权地址
     * @param getAuthUrl appId
     * @param redirectUrl 回调地址
     * @return 返回获取登录授权地址url
     */
    public static String getAuthUrl(String getAuthUrl, String redirectUrl){
        String url = null;

        String param = LoginConstants.TYPE + LoginConstants.EQUAL_SIGN + LoginTypeEnum.ALIPAY.getCode().toString() + "&" +
                AlipayConstants.REDIRECT_URL_FIELD + LoginConstants.EQUAL_SIGN + redirectUrl;
        String result = HttpUtils.sendGet(getAuthUrl, param, null);
        if(!Strings.isNullOrEmpty(result)){
            JSONObject json = (JSONObject) JSONObject.parse(result);
            Integer status = json.getInteger("status");
            if(status == HttpStatus.SC_OK){
                url = json.getString(LoginConstants.DATA);
            }
        }
        return url;
    }

    /**
     * 通过临时授权码获取userId
     * @param authCode 临时授权码
     * @param appId appId
     * @param appPrivateKey 商户私钥
     * @param alipayPublicKey 支付宝公钥
     * @return 返回access_token 和 userId
     */
    public static String getUserIdByAuthCode(String authCode, String appId, String appPrivateKey, String alipayPublicKey){
        String userId = null;

        // 交换令牌
        String accessToken = null;
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConstants.URL, appId, appPrivateKey, LoginConstants.JSON, LoginConstants.CHARSET_GBK, alipayPublicKey, LoginConstants.RSA2);
        AlipaySystemOauthTokenRequest alipaySystemOauthTokenRequest = new AlipaySystemOauthTokenRequest();
        alipaySystemOauthTokenRequest.setCode(authCode);
        alipaySystemOauthTokenRequest.setGrantType(LoginConstants.AUTHORIZATION_CODE);
        try {
            AlipaySystemOauthTokenResponse oauthTokenResponse = alipayClient.execute(alipaySystemOauthTokenRequest);
            accessToken = oauthTokenResponse.getAccessToken();
        } catch (AlipayApiException e) {
            //处理异常
            e.printStackTrace();
        }

        if(!Strings.isNullOrEmpty(accessToken)){
            userId = getUserIdByAccessToken(accessToken, alipayClient);
        }

        return userId;
    }

    /**
     * 获取用户信息
     * @param accessToken 交换令牌
     * @param alipayClient 支付宝客户端连接
     * @return 返回用户信息
     */
    public static String getUserIdByAccessToken(String accessToken, AlipayClient alipayClient){
        String userId = null;
        AlipayUserInfoShareRequest alipayUserInfoShareRequest = new AlipayUserInfoShareRequest();
        try {
            AlipayUserInfoShareResponse alipayUserInfoShareResponse = alipayClient.execute(alipayUserInfoShareRequest, accessToken);
            String body = alipayUserInfoShareResponse.getBody();
            JSONObject json = (JSONObject) JSONObject.parse(body);
            if(json != null){
                userId = json.getString(LoginConstants.USER_ID);
            }
        } catch (AlipayApiException e) {
            //处理异常
            e.printStackTrace();
        }
        return userId;
    }
}