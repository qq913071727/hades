package com.innovation.ic.cc.base.handler.cc;

import com.innovation.ic.cc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.cc.base.pojo.global.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ContextHandler extends CCAbstractHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 向Context中添加clientId列表
     */
    public void putClientSet() {
        logger.info("向Context中添加clientId列表");

        Set<String> clientSet = redisManager.sMembers(RedisStorage.CLIENT);
        Context.put(RedisStorage.CLIENT, clientSet);
    }
}