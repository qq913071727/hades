package com.innovation.ic.cc.base.handler.cc;

import com.innovation.ic.cc.base.model.cc.DiscussionPriceRecord;
import com.innovation.ic.cc.base.model.cc.InquiryPriceRecord;
import com.innovation.ic.cc.base.model.cc.QuotedPriceRecord;
import com.innovation.ic.cc.base.pojo.enums.CurrencyEnum;
import com.innovation.ic.cc.base.pojo.enums.DeliveryPlaceTypeEnum;
import com.innovation.ic.cc.base.pojo.enums.InquiryPriceRecordStatusEnum;
import com.innovation.ic.cc.base.pojo.enums.InquiryTypeEnum;
import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceRecordPojo;
import com.innovation.ic.cc.base.pojo.variable.discussionPrice.DiscussionPriceRecordPojo;
import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceRecordPojo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author zengqinglong
 * @desc 询价处理器
 * @Date 2023/2/15 14:48
 **/
@Component
@Slf4j
public class InquiryPriceHandler extends CCAbstractHandler {
    public static String InquiryPriceIdKey = "INQUIRY_PRICE_ID_KEY";
    /**
     * 时间格式
     */
    public static String simpleDateHms = "yyMMdd";

    public static SimpleDateFormat simpleFormat = new SimpleDateFormat(simpleDateHms);

    /**
     * 获取当天时间日期
     *
     * @return
     */
    public String dateToString() {
        return simpleFormat.format(new Date());
    }

    /**
     * 生成询价单号
     */
    public String generateInquiryPriceNumber() {
        String vid = "";
        while (true) {
            vid = dateToString() + getVid();
            //过期时间24小时 86400秒
            Long number = redisManager.sAdd(InquiryPriceIdKey, 86400, vid);
            if (number > 0) {
                break;
            }
        }
        return vid;
    }


    public static String[] chars = new String[]{"a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};

    public static String getVid() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < 4; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();
    }


    /**
     * 询价记录转换参数
     *
     * @param inquiryPriceRecordPojo
     * @return
     */
    public InquiryPriceRecordPojo InquiryPriceRecordPojo(InquiryPriceRecordPojo inquiryPriceRecordPojo, Integer inquiryType, String invoiceName) {
        //inquiryPriceRecordPojo.setStatusDesc(InquiryPriceRecordStatusEnum.parse(inquiryPriceRecordPojo.getStatus()).getDesc());
        inquiryPriceRecordPojo.setInquiryTypeDesc(InquiryTypeEnum.parse(inquiryType).getDesc());
        inquiryPriceRecordPojo.setInquiryType(inquiryType);
        inquiryPriceRecordPojo.setInvoiceName(invoiceName);
        return inquiryPriceRecordPojo;
    }

    /**
     * 询价记录实体类转换参数
     *
     * @param inquiryPriceRecord
     * @return
     */
    public InquiryPriceRecordPojo inquiryPriceRecordToInquiryPriceRecordPojo(InquiryPriceRecord inquiryPriceRecord, Integer inquiryType, String invoiceName) {
        InquiryPriceRecordPojo inquiryPriceRecordPojo = new InquiryPriceRecordPojo();
        BeanUtils.copyProperties(inquiryPriceRecord,inquiryPriceRecordPojo);
        inquiryPriceRecordPojo.setInquiryPriceRecordId(inquiryPriceRecord.getId());
        inquiryPriceRecordPojo.setStatusDesc(InquiryPriceRecordStatusEnum.parse(inquiryPriceRecord.getStatus()).getDesc());
        inquiryPriceRecordPojo.setInquiryTypeDesc(InquiryTypeEnum.parse(inquiryType).getDesc());
        inquiryPriceRecordPojo.setInquiryType(inquiryType);
        inquiryPriceRecordPojo.setInvoiceName(invoiceName);
        return inquiryPriceRecordPojo;
    }

    /**
     * 报价记录实体类转换参数
     *
     * @param quotedPriceRecord
     * @return
     */
    public QuotedPriceRecordPojo quotedPriceRecordToQuotedPriceRecordPojo(QuotedPriceRecord quotedPriceRecord) {
        QuotedPriceRecordPojo quotedPriceRecordPojo = new QuotedPriceRecordPojo();
        BeanUtils.copyProperties(quotedPriceRecord, quotedPriceRecordPojo);
        quotedPriceRecordPojo.setCurrencyDesc(CurrencyEnum.parse(quotedPriceRecord.getCurrency()).getDesc());
        quotedPriceRecordPojo.setDeliveryPlaceDesc(DeliveryPlaceTypeEnum.parse(quotedPriceRecord.getDeliveryPlace()).getDesc());
        return quotedPriceRecordPojo;
    }

    /**
     * 议价记录视图类转换参数
     *
     * @param discussionPriceRecord
     * @return
     */
    public DiscussionPriceRecordPojo discussionPriceRecordToDiscussionPriceRecordPojo(DiscussionPriceRecord discussionPriceRecord) {
        DiscussionPriceRecordPojo discussionPriceRecordPojo = new DiscussionPriceRecordPojo();
        BeanUtils.copyProperties(discussionPriceRecord, discussionPriceRecordPojo);
        discussionPriceRecordPojo.setCurrencyDesc(CurrencyEnum.parse(discussionPriceRecord.getCurrency()).getDesc());
        discussionPriceRecordPojo.setDeliveryPlaceDesc((DeliveryPlaceTypeEnum.parse(discussionPriceRecord.getDeliveryPlace()).getDesc()));
        return discussionPriceRecordPojo;
    }
}
