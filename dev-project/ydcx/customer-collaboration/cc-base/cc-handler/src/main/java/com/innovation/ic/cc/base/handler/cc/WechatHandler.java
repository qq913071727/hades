package com.innovation.ic.cc.base.handler.cc;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.cc.base.pojo.constant.LoginConstants;
import com.innovation.ic.cc.base.pojo.constant.third_party_api.WechatConstants;
import com.innovation.ic.cc.base.pojo.enums.LoginTypeEnum;
import org.apache.http.HttpStatus;

/**
 * @desc   微信相关工具类
 * @author linuo
 * @time   2022年8月17日09:25:58
 */
public class WechatHandler {
    /**
     * 获取微信登录授权地址
     * @param getAuthUrl 获取登录授权地址url
     * @param redirectUrl 回调地址
     * @return 返回
     */
    public static String getAuthUrl(String getAuthUrl, String redirectUrl){
        String url = null;
        String param = LoginConstants.TYPE + LoginConstants.EQUAL_SIGN + LoginTypeEnum.WECHAT.getCode().toString() + "&" +
                WechatConstants.REDIRECT_URL_FIELD + LoginConstants.EQUAL_SIGN + redirectUrl;
        String result = HttpUtils.sendGet(getAuthUrl, param, null);
        if(!Strings.isNullOrEmpty(result)){
            JSONObject json = (JSONObject) JSONObject.parse(result);
            Integer status = json.getInteger(LoginConstants.STATUS_FIELD);
            if(status == HttpStatus.SC_OK){
                url = json.getString(LoginConstants.DATA);
            }
        }
        return url;
    }

    /**
     * 通过临时授权码获取用户个人信息
     * @param appId 应用唯一标识，在微信开放平台提交应用审核通过后获得
     * @param secret 应用密钥AppSecret，在微信开放平台提交应用审核通过后获得
     * @param authCode 临时授权码
     * @return 返回用户个人信息
     */
    public static JSONObject getUserInfoByAuthCode(String appId, String secret, String authCode) {
        JSONObject result = null;
        // 获取access_token(接口调用凭证)及openId(授权用户唯一标识)
        JSONObject json = getAccessTokenAndOpenId(appId, secret, authCode);
        if(json != null && !Strings.isNullOrEmpty(json.getString(WechatConstants.ACCESS_TOKEN)) && !Strings.isNullOrEmpty(json.getString(WechatConstants.OPEN_ID_FIELD))){
            String accessToken = json.getString(WechatConstants.ACCESS_TOKEN);
            String openId = json.getString(WechatConstants.OPEN_ID_FIELD);
            // 获取用户个人信息
            String unionId = WechatHandler.getUserInfo(accessToken, openId);
            if(!Strings.isNullOrEmpty(unionId)){
                unionId = unionId.replaceAll(" ", "");
                openId = openId.replaceAll(" ", "");
                result = new JSONObject();
                result.put(WechatConstants.UNION_ID_FIELD, unionId);
                result.put(WechatConstants.OPEN_ID_FIELD, openId);
            }
        }
        return result;
    }

    /**
     * 获取access_token(接口调用凭证)及openId(授权用户唯一标识)
     * @param appId 应用唯一标识，在微信开放平台提交应用审核通过后获得
     * @param secret 应用密钥AppSecret，在微信开放平台提交应用审核通过后获得
     * @param authCode 临时授权码
     * @return 返回接口调用凭证
     */
    private static JSONObject getAccessTokenAndOpenId(String appId, String secret, String authCode) {
        JSONObject jsonObject = null;
        String param = "appid=" + appId + "&secret=" + secret + "&code=" + authCode + "&grant_type=authorization_code";
        String result = HttpUtils.sendGet(WechatConstants.GET_ACCESS_TOKEN_URL, param, null);
        if(!Strings.isNullOrEmpty(result)){
            jsonObject = new JSONObject();
            JSONObject json = (JSONObject) JSONObject.parse(result);
            if(!Strings.isNullOrEmpty(json.getString(WechatConstants.ACCESS_TOKEN_FIELD))){
                jsonObject.put(WechatConstants.ACCESS_TOKEN, json.getString(WechatConstants.ACCESS_TOKEN_FIELD));
            }
            if(!Strings.isNullOrEmpty(json.getString(WechatConstants.OPEN_ID_FIELD))){
                jsonObject.put(WechatConstants.OPEN_ID_FIELD, json.getString(WechatConstants.OPEN_ID_FIELD));
            }
        }
        return jsonObject;
    }

    /**
     * 获取用户个人信息
     * @param accessToken 接口调用凭证
     * @param openId 授权用户唯一标识
     * @return 返回unionid。用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。
     */
    private static String getUserInfo(String accessToken, String openId) {
        String param = WechatConstants.ACCESS_TOKEN_FIELD + "=" + accessToken + "&" + WechatConstants.OPEN_ID_FIELD + "=" + openId;
        String result = HttpUtils.sendGet(WechatConstants.GET_USER_INFO_URL, param, null);
        if(!Strings.isNullOrEmpty(result)){
            JSONObject json = (JSONObject) JSONObject.parse(result);
            if(!Strings.isNullOrEmpty(json.getString(WechatConstants.UNION_ID_FIELD))){
                return json.getString(WechatConstants.UNION_ID_FIELD);
            }
        }
        return null;
    }
}