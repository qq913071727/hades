package com.innovation.ic.cc.base.handler.cc;

import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.MenuTree;
import com.innovation.ic.cc.base.model.cc.Role;
import com.innovation.ic.cc.base.pojo.constant.model.MenuSelected;
import com.innovation.ic.cc.base.pojo.constant.model.MenuType;
import com.innovation.ic.cc.base.pojo.constant.model.RoleAvailableStatus;
import com.innovation.ic.cc.base.pojo.enums.MenuFirstEnum;
import com.innovation.ic.cc.base.pojo.enums.MenuSecondEnum;
import com.innovation.ic.cc.base.pojo.enums.MenuThirdEnum;
import com.innovation.ic.cc.base.pojo.variable.menu.Menu;
import com.innovation.ic.cc.base.vo.menu.MenuTreeAndUserRoleVo;
import com.innovation.ic.cc.base.vo.menu.MenuTreeVo;
import com.innovation.ic.cc.base.vo.menu.RoleVo;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MenuHandler extends CCAbstractHandler {
    /**
     * 生成所有菜单
     */
    public MenuTree createFullMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();
        // 首页 1
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("1");
            firstMenu.setName("首页");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
        }
        //订单管理 2
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("2");
            firstMenu.setName("订单管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 我的订单 2-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-1");
                secondMenu.setName("我的订单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 新增订单 2-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-1-1");
                    thirdMenu.setName("新增订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 废弃 2-1-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-1-2");
                    thirdMenu.setName("废弃");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        //询价管理 3
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("3");
            firstMenu.setName("询价管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 发布询价 3-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-1");
                secondMenu.setName("发布询价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 新增询价 3-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-1-1");
                    thirdMenu.setName("新增询价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 我收到的报价 3-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-2");
                secondMenu.setName("我收到的报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 一键采纳 3-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-2-1");
                    thirdMenu.setName("一键采纳");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 生成订单 3-2-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-2-2");
                    thirdMenu.setName("生成订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 我的议价 3-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-3");
                secondMenu.setName("我的议价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 确认议价 3-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-1");
                    thirdMenu.setName("确认议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 激活询价 3-3-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-2");
                    thirdMenu.setName("激活询价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 不接受 3-3-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-3");
                    thirdMenu.setName("不接受");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 废弃 3-3-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-4");
                    thirdMenu.setName("废弃");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 生成订单 3-3-5
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-5");
                    thirdMenu.setName("生成订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 有效报价单 3-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-4");
                secondMenu.setName("有效报价单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 生成订单 3-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-4-1");
                    thirdMenu.setName("生成订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 激活询价 3-4-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-4-2");
                    thirdMenu.setName("激活询价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 废弃 3-4-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-4-3");
                    thirdMenu.setName("废弃");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        //库存信息查询 4
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("4");
            firstMenu.setName("库存信息查询");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 库存查询 4-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("4-1");
                secondMenu.setName("库存查询");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 去询价 4-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-1");
                    thirdMenu.setName("去询价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 议价 4-1-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-2");
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 生成订单 4-1-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-3");
                    thirdMenu.setName("生成订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 导入BOM 4-1-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-4");
                    thirdMenu.setName("导入BOM");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 特价库存查询 4-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("4-2");
                secondMenu.setName("特价库存查询");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 议价 4-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-2-1");
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 生成订单 4-2-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-2-2");
                    thirdMenu.setName("生成订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 导入BOM 4-2-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-2-3");
                    thirdMenu.setName("导入BOM");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 期货信息查询 4-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("4-3");
                secondMenu.setName("期货信息查询");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 去询价 4-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-3-1");
                    thirdMenu.setName("去询价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 议价 4-3-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-3-2");
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 生成订单 4-3-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-3-3");
                    thirdMenu.setName("生成订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 导入BOM 4-3-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-3-4");
                    thirdMenu.setName("导入BOM");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        //收货管理 5
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("5");
            firstMenu.setName("收货管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 在途货物 5-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-1");
                secondMenu.setName("在途货物");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 取消订单 5-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-1-1");
                    thirdMenu.setName("取消订单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 我的收货单 5-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-2");
                secondMenu.setName("我的收货单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 确认收货单 5-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-2-1");
                    thirdMenu.setName("确认收货单");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        //售后管理 6
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("6");
            firstMenu.setName("售后管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 退换补货管理 6-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-1");
                secondMenu.setName("退换补货管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 上传物流单号 6-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-1-1");
                    thirdMenu.setName("上传物流单号");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 取消申请 6-1-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-1-2");
                    thirdMenu.setName("取消申请");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        //财务管理 7
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("7");
            firstMenu.setName("财务管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 对账单 7-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("7-1");
                secondMenu.setName("对账单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 付款管理 7-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("7-2");
                secondMenu.setName("付款管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 开票申请 7-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("7-3");
                secondMenu.setName("开票申请");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 通知开票 7-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("7-3-1");
                    thirdMenu.setName("通知开票");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 我的发票 7-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("7-4");
                secondMenu.setName("我的发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 合同管理 7-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("7-5");
                secondMenu.setName("合同管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 创建合同 7-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("7-5-1");
                    thirdMenu.setName("创建合同");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 废弃 7-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("7-5-2");
                    thirdMenu.setName("废弃");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        //增值服务 8
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("8");
            firstMenu.setName("增值服务");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 型号替代 8-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-1");
                secondMenu.setName("型号替代");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 样品申请 8-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-2");
                secondMenu.setName("样品申请");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 技术支持 8-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-3");
                secondMenu.setName("技术支持");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 方案查询 8-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-4");
                secondMenu.setName("方案查询");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 风险查询 8-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-5");
                secondMenu.setName("风险查询");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 仓储报关 8-6
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-6");
                secondMenu.setName("仓储报关");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 检测服务 8-7
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-7");
                secondMenu.setName("检测服务");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // PCBA 8-8
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("8-8");
                secondMenu.setName("PCBA");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
        }
        //系统管理 9
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("9");
            firstMenu.setName("系统管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
            // 基本信息 9-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-1");
                secondMenu.setName("型号替代");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 修改密码 9-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-1-1");
                    thirdMenu.setName("修改密码");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 修改绑定 9-1-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-1-2");
                    thirdMenu.setName("修改绑定");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 退出登录 9-1-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-1-3");
                    thirdMenu.setName("退出登录");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 账号安全 9-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-2");
                secondMenu.setName("账号安全");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 账号管理 9-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-3");
                secondMenu.setName("账号管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 新增 9-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 修改 9-3-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-2");
                    thirdMenu.setName("修改");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 重置密码 9-3-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-3");
                    thirdMenu.setName("重置密码");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 设置权限 9-3-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-4");
                    thirdMenu.setName("设置权限");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 人员管理 9-3-5
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-5");
                    thirdMenu.setName("人员管理");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 启用 9-3-6
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-6");
                    thirdMenu.setName("启用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
                // 停用 9-3-7
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-3-7");
                    thirdMenu.setName("停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 角色管理 9-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-4");
                secondMenu.setName("角色管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 新增角色 9-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-4-1");
                    thirdMenu.setName("新增角色");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 我的公司管理 9-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-5");
                secondMenu.setName("我的公司管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 新增公司 9-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-5-1");
                    thirdMenu.setName("新增公司");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 地址管理 9-6
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-6");
                secondMenu.setName("地址管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
                // 新增收货地址 9-6-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("9-6-1");
                    thirdMenu.setName("新增收货地址");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
            // 系统公告 9-7
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-7");
                secondMenu.setName("系统公告");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
            // 消息提醒 9-8
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("9-8");
                secondMenu.setName("消息提醒");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }
        }
        return menuTree;
    }

    /**
     * 生成主账号的菜单。主账号：有所有菜单和按钮权限
     */
    public MenuTree createMainAccountMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();
        //获取第一级菜单
        for (MenuFirstEnum menuFirstEnum : MenuFirstEnum.values()) {
            Menu firstMenu = new Menu();
            firstMenu.setId(menuFirstEnum.getId());
            firstMenu.setName(menuFirstEnum.getName());
            firstMenu.setType(menuFirstEnum.getMenuType());
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(menuFirstEnum.getPath());
            firstMenu.setIcon(menuFirstEnum.getIcon());
            menuTree.getFirstMenuList().add(firstMenu);
            //获取二级菜单
            for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.getSecondMenuList(firstMenu.getId())) {
                Menu secondMenu = new Menu();
                secondMenu.setId(menuSecondEnum.getId());
                secondMenu.setName(menuSecondEnum.getName());
                secondMenu.setType(menuSecondEnum.getMenuType());
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(menuSecondEnum.getPath());
                secondMenu.setIcon(menuSecondEnum.getIcon());
                firstMenu.getChildMenuList().add(secondMenu);
                //获取三级菜单
                for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.getThirdMenuList(secondMenu.getId())) {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId(menuThirdEnum.getId());
                    thirdMenu.setName(menuThirdEnum.getName());
                    thirdMenu.setType(menuThirdEnum.getMenuType());
                    thirdMenu.setSelected(MenuSelected.YES);
                    thirdMenu.setPath(menuThirdEnum.getPath());
                    thirdMenu.setIcon(menuThirdEnum.getIcon());
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }
        return menuTree;
    }

    /**
     * 生成子账号（经理）的菜单。子账号(经理)：没有系统管理-角色管理，只有系统管理-账号管理-查看、人员管理按钮
     */
    public MenuTree createSubAccountManagerMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();
        //获取第一级菜单
        for (MenuFirstEnum menuFirstEnum : MenuFirstEnum.values()) {
            Menu firstMenu = new Menu();
            firstMenu.setId(menuFirstEnum.getId());
            firstMenu.setName(menuFirstEnum.getName());
            firstMenu.setType(menuFirstEnum.getMenuType());
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(menuFirstEnum.getPath());
            firstMenu.setIcon(menuFirstEnum.getIcon());
            menuTree.getFirstMenuList().add(firstMenu);
            //获取二级菜单
            for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.getSecondMenuList(firstMenu.getId())) {
                //经理排除 角色管理菜单 我的公司管理
                if (!menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ROLE_MANAGEMENT.getId())
                        && !menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_MY_COMPANY_MANAGEMENT.getId())) {
                    Menu secondMenu = new Menu();
                    secondMenu.setId(menuSecondEnum.getId());
                    secondMenu.setName(menuSecondEnum.getName());
                    secondMenu.setType(menuSecondEnum.getMenuType());
                    secondMenu.setSelected(MenuSelected.NO);
                    secondMenu.setPath(menuSecondEnum.getPath());
                    secondMenu.setIcon(menuSecondEnum.getIcon());
                    firstMenu.getChildMenuList().add(secondMenu);
                    //经理只能看列表不能操作
                    if (!menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ACCOUNT_MANAGEMENT.getId())) {
                        //获取三级菜单
                        for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.getThirdMenuList(secondMenu.getId())) {
                            Menu thirdMenu = new Menu();
                            thirdMenu.setId(menuThirdEnum.getId());
                            thirdMenu.setName(menuThirdEnum.getName());
                            thirdMenu.setType(menuThirdEnum.getMenuType());
                            thirdMenu.setSelected(MenuSelected.NO);
                            thirdMenu.setPath(menuThirdEnum.getPath());
                            thirdMenu.setIcon(menuThirdEnum.getIcon());
                            secondMenu.getChildMenuList().add(thirdMenu);
                        }
                    }
                }
            }
        }
        return menuTree;
    }

    /**
     * 子账号(员工)：没有系统管理-账号管理、角色管理
     */
    public MenuTree createSubAccountPersonnelMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();
        //获取第一级菜单
        for (MenuFirstEnum menuFirstEnum : MenuFirstEnum.values()) {
            Menu firstMenu = new Menu();
            firstMenu.setId(menuFirstEnum.getId());
            firstMenu.setName(menuFirstEnum.getName());
            firstMenu.setType(menuFirstEnum.getMenuType());
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(menuFirstEnum.getPath());
            firstMenu.setIcon(menuFirstEnum.getIcon());
            menuTree.getFirstMenuList().add(firstMenu);
            //获取二级菜单
            for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.getSecondMenuList(firstMenu.getId())) {
                //排除角色管理菜单 和 账户管理 我的公司管理
                if (!menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ROLE_MANAGEMENT.getId()) &&
                        !menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ACCOUNT_MANAGEMENT.getId()) &&
                        !menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_MY_COMPANY_MANAGEMENT.getId())) {
                    Menu secondMenu = new Menu();
                    secondMenu.setId(menuSecondEnum.getId());
                    secondMenu.setName(menuSecondEnum.getName());
                    secondMenu.setType(menuSecondEnum.getMenuType());
                    secondMenu.setSelected(MenuSelected.NO);
                    secondMenu.setPath(menuSecondEnum.getPath());
                    secondMenu.setIcon(menuSecondEnum.getIcon());
                    firstMenu.getChildMenuList().add(secondMenu);

                    //获取三级菜单
                    for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.getThirdMenuList(secondMenu.getId())) {
                        Menu thirdMenu = new Menu();
                        thirdMenu.setId(menuThirdEnum.getId());
                        thirdMenu.setName(menuThirdEnum.getName());
                        thirdMenu.setType(menuThirdEnum.getMenuType());
                        thirdMenu.setSelected(MenuSelected.NO);
                        thirdMenu.setPath(menuThirdEnum.getPath());
                        thirdMenu.setIcon(menuThirdEnum.getIcon());
                        secondMenu.getChildMenuList().add(thirdMenu);
                    }

                }
            }
        }
        return menuTree;
    }

    /**
     * 将RoleVo转换为Role类型
     *
     * @param roleVo
     * @param userId
     * @return
     */
    public Role roleVoToRole(RoleVo roleVo, String userId) {
        Role role = new Role();
        if (null != roleVo && !StringUtils.isEmpty(roleVo.getName())) {
            role.setName(roleVo.getName());
        }
        role.setAvailable(RoleAvailableStatus.YES);
        role.setCreateTime(new Date());
        if (!StringUtils.isEmpty(userId)) {
            role.setCreatorId(userId);
        }
        if (null != roleVo && null != roleVo.getMenuVoList()) {
            role.setMenuList(BeanPropertiesUtil.convertList(roleVo.getMenuVoList(), Menu.class));
        }

        return role;
    }

    /**
     * 将MenuTreeVo类型的对象转换为MenuTree类型的对象
     *
     * @param menuTreeVo
     * @param loginUserId
     * @return
     */
    public MenuTree menuTreeVoToMenuTree(MenuTreeVo menuTreeVo, String loginUserId) {
        MenuTree menuTree = new MenuTree();
        if (null != menuTreeVo && StringUtils.validateParameter(menuTreeVo.getUserId())) {
            menuTree.setUserId(menuTreeVo.getUserId());
        }
        menuTree.setCreatorId(loginUserId);
        menuTree.setCreateTime(new Date());
        if (null != menuTreeVo && null != menuTreeVo.getMenuNodeList()) {
            menuTree.setFirstMenuList(BeanPropertiesUtil.convertList(menuTreeVo.getMenuNodeList(), Menu.class));
        }

        return menuTree;
    }

    /**
     * 将MenuTreeAndUserRoleVo类型的对象转换为MenuTree类型的对象
     *
     * @param menuTreeAndUserRoleVo
     * @param loginUserId
     * @return
     */
    public MenuTree menuTreeAndUserRoleVoToMenuTree(MenuTreeAndUserRoleVo menuTreeAndUserRoleVo, String loginUserId) {
        MenuTree menuTree = new MenuTree();
        if (null != menuTreeAndUserRoleVo && StringUtils.validateParameter(menuTreeAndUserRoleVo.getUserId())) {
            menuTree.setUserId(menuTreeAndUserRoleVo.getUserId());
        }
        menuTree.setCreatorId(loginUserId);
        menuTree.setCreateTime(new Date());
        if (null != menuTreeAndUserRoleVo && null != menuTreeAndUserRoleVo.getMenuNodeList()) {
            menuTree.setFirstMenuList(BeanPropertiesUtil.convertList(menuTreeAndUserRoleVo.getMenuNodeList(), Menu.class));
        }
        return menuTree;
    }
}
