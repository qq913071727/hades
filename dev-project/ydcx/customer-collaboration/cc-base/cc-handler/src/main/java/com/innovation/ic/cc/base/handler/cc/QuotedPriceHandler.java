//package com.innovation.ic.cc.base.handler.cc;
//
//import com.innovation.ic.cc.base.model.cc.QuotedPrice;
//import com.innovation.ic.cc.base.model.cc.QuotedPriceRecord;
//import com.innovation.ic.cc.base.pojo.enums.*;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPricePojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceRecordPojo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeanUtils;
//import org.springframework.stereotype.Component;
//
///**
// * @author zengqinglong
// * @desc 报价处理器
// * @Date 2023/2/27 14:28
// **/
//@Component
//@Slf4j
//public class QuotedPriceHandler {
//    /**
//     * 报价实体类转换参数
//     *
//     * @param quotedPrice
//     * @return
//     */
//    public QuotedPricePojo quotedPriceToQuotedPricePojo(QuotedPrice quotedPrice) {
//        QuotedPricePojo quotedPricePojo = new QuotedPricePojo();
//        BeanUtils.copyProperties(quotedPricePojo, quotedPrice);
//        quotedPricePojo.setId(quotedPrice.getId().toString());
//        //设置币种
//        quotedPricePojo.setCurrencyDesc(CurrencyEnum.parse(quotedPrice.getCurrency()).getDesc());
//        //设置类型
//        quotedPricePojo.setInquiryTypeDesc(InquiryTypeEnum.parse(quotedPrice.getInquiryType()).getDesc());
//        //设置交货地
//        quotedPricePojo.setDeliveryPlaceDesc(DeliveryPlaceTypeEnum.parse(quotedPrice.getDeliveryPlace()).getDesc());
//        //发票类型
//        quotedPricePojo.setInvoiceTypeDesc(InvoiceTypeEnum.parse(quotedPrice.getInvoiceType()).getDesc());
//        //状态
//        quotedPricePojo.setStatusDesc(InquiryPriceStatusEnum.parse(quotedPrice.getStatus()).getDesc());
//        //截止时间戳
//        quotedPricePojo.setDeadlineTimestamp(String.valueOf(quotedPrice.getDeadline().getTime()));
//        //询价时间戳
//        quotedPricePojo.setDateTimestamp(String.valueOf(quotedPrice.getDatetime().getTime()));
//        return quotedPricePojo;
//    }
//
//    /**
//     * 报价记录实体类转换参数
//     *
//     * @param quotedPriceRecord
//     * @return
//     */
//    public QuotedPriceRecordPojo quotedPriceRecordToQuotedPriceRecordPojo(QuotedPriceRecord quotedPriceRecord) {
//        QuotedPriceRecordPojo quotedPriceRecordPojo = new QuotedPriceRecordPojo();
//        BeanUtils.copyProperties(quotedPriceRecordPojo, quotedPriceRecord);
//        quotedPriceRecordPojo.setId(quotedPriceRecord.getId().toString());
//        quotedPriceRecordPojo.setInquiryPriceRecordId(quotedPriceRecord.getInquiryPriceRecordId().toString());
//        //状态
//        quotedPriceRecordPojo.setStatusDesc(InquiryPriceRecordStatusEnum.parse(quotedPriceRecord.getStatus()).getDesc());
//        //截止时间戳
//        quotedPriceRecordPojo.setDeliveryDateTimestamp(String.valueOf(quotedPriceRecord.getDeliveryDate().getTime()));
//        //剩余时间戳
//        if (quotedPriceRecord.getRemainingTime() != null) {
//            quotedPriceRecordPojo.setRemainingTimestamp(String.valueOf(quotedPriceRecord.getRemainingTime().getTime()));
//        }
//        return quotedPriceRecordPojo;
//    }
//}
