package com.innovation.ic.cc.base.handler.cc;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.framework.util.HanziToPinyinUtils;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.MenuTree;
import com.innovation.ic.cc.base.model.cc.PostSale;
import com.innovation.ic.cc.base.model.cc.Role;
import com.innovation.ic.cc.base.model.cc.User;
import com.innovation.ic.cc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.cc.base.pojo.constant.model.RoleAvailableStatus;
import com.innovation.ic.cc.base.pojo.variable.menu.Menu;
import com.innovation.ic.cc.base.vo.menu.MenuTreeVo;
import com.innovation.ic.cc.base.vo.menu.RoleVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * model包中类的处理器
 */
@Component
public class ModelHandler extends CCAbstractHandler {

    private static final Logger logger = LoggerFactory.getLogger(ModelHandler.class);

    /**
     * 将ParentUsersTopView类型对象转换为xml字符串。用于存储到redis
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     * 4.没有password字段。
     *
     * @param user
     * @return
     */
    public String toXmlStorageString(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == user) {
            return null;
        } else {
            if (null != user.getId()) {
                stringBuilder.append("<id>").append(user.getId()).append("</id>");
            }
//            if (null != user.getUserId()) {
//                stringBuilder.append("<userId>").append(user.getUserId()).append("</userId>");
//            }
            if (null != user.getFatherId()) {
                stringBuilder.append("<webSite>").append(user.getFatherId()).append("</webSite>");
            }
            if (null != user.getWebSite()) {
                stringBuilder.append("<fatherId>").append(user.getWebSite()).append("</fatherId>");
            }
            if (null != user.getEnterpriseId()) {
                stringBuilder.append("<enterpriseId>").append(user.getEnterpriseId()).append("</enterpriseId>");
            }
            if (null != user.getEnterpriseName()) {
                stringBuilder.append("<enterpriseName>").append(user.getEnterpriseName()).append("</enterpriseName>");
            }
            if (null != user.getType()) {
                stringBuilder.append("<type>").append(user.getType()).append("</type>");
            }
            if (!StringUtils.isEmpty(user.getUserName())) {
                stringBuilder.append("<userName>").append(user.getUserName()).append("</userName>");
            }
            if (!StringUtils.isEmpty(user.getRealName())) {
                stringBuilder.append("<realName>").append(user.getRealName()).append("</realName>");
            }
            if (null != user.getPosition()) {
                stringBuilder.append("<position>").append(user.getPosition()).append("</position>");
            }
            if (!StringUtils.isEmpty(user.getPhone())) {
                stringBuilder.append("<phone>").append(user.getPhone()).append("</phone>");
            }
            if (!StringUtils.isEmpty(user.getEmail())) {
                stringBuilder.append("<email>").append(user.getEmail()).append("</email>");
            }
            if (null != user.getCreateDate()) {

                stringBuilder.append("<createDate>").append(DateUtils.formatDate(user.getCreateDate(), DateUtils.DEFAULT_FORMAT)).append("</createDate>");
            }
            if (null != user.getModifyDate()) {
                stringBuilder.append("<modifyDate>").append(DateUtils.formatDate(user.getModifyDate(), DateUtils.DEFAULT_FORMAT)).append("</modifyDate>");
            }
            if (null != user.getStatus()) {
                stringBuilder.append("<status>").append(user.getStatus()).append("</status>");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * 将ParentUsersTopView类型对象转换为xml字符串。用于模糊查询
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     * 4.每个元素之间要有*。
     *
     * @param user
     * @return
     */
    public String toXmlFilterString(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == user) {
            return null;
        } else {
            if (!StringUtils.isEmpty(user.getUserName())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<userName>")
                        .append(RedisStorage.ASTERISK).append(user.getUserName())
                        .append(RedisStorage.ASTERISK).append("</userName>");
            }
            if (!StringUtils.isEmpty(user.getRealName())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<realName>")
                        .append(RedisStorage.ASTERISK).append(user.getRealName())
                        .append(RedisStorage.ASTERISK).append("</realName>");
            }
            if (null != user.getPosition()) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<position>")
                        .append(RedisStorage.ASTERISK).append(user.getPosition())
                        .append(RedisStorage.ASTERISK).append("</position>");
            }
            if (!StringUtils.isEmpty(user.getPhone())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<phone>")
                        .append(RedisStorage.ASTERISK).append(user.getPhone())
                        .append(RedisStorage.ASTERISK).append("</phone>");
            }
            if (null != user.getStatus()) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<status>")
                        .append(user.getStatus()).append("</status>");
            }
            stringBuilder.append(RedisStorage.ASTERISK);
            return stringBuilder.toString();
        }
    }

    /**
     * 将JSONObject对象转换为User对象
     *
     * @param jsonObject
     * @return
     */
    public User jsonObjectToUser(JSONObject jsonObject) {
        if (null != jsonObject) {
            User user = new User();
            if (null != jsonObject.getString("id")) {
                user.setId(jsonObject.getString("id"));
            }
//            if (null != jsonObject.getString("user_id")){
//                user.setUserId(jsonObject.getString("user_id"));
//            }
            if (null != jsonObject.getInteger("web_site")) {
                user.setWebSite(jsonObject.getInteger("web_site"));
            }
            if (null != jsonObject.getString("father_id")) {
                user.setFatherId(jsonObject.getString("father_id"));
            }
            if (null != jsonObject.getString("enterprise_id")) {
                user.setEnterpriseId(jsonObject.getString("enterprise_id"));
            }
            if (null != jsonObject.getString("enterprise_name")) {
                user.setEnterpriseName(jsonObject.getString("enterprise_name"));
            }
            if (null != jsonObject.getInteger("type")) {
                user.setType(jsonObject.getInteger("type"));
            }
            if (null != jsonObject.getString("user_name")) {
                user.setUserName(jsonObject.getString("user_name"));
            }
            if (null != jsonObject.getString("password")) {
                user.setPassword(jsonObject.getString("password"));
            }
            if (null != jsonObject.getString("real_name")) {
                user.setRealName(jsonObject.getString("real_name"));
                user.setPinYin(HanziToPinyinUtils.getPinyin(jsonObject.getString("real_name")));
            }
            if (null != jsonObject.getString("position")) {
                user.setPosition(jsonObject.getInteger("position"));
            }
            if (null != jsonObject.getString("phone")) {
                user.setPhone(jsonObject.getString("phone"));
            }
            if (null != jsonObject.getString("email")) {
                user.setEmail(jsonObject.getString("email"));
            }
            if (null != jsonObject.getDate("create_date")) {
                user.setCreateDate(jsonObject.getDate("create_date"));
            }
            if (null != jsonObject.getDate("modify_date")) {
                user.setModifyDate(jsonObject.getDate("modify_date"));
            }
            if (null != jsonObject.getInteger("status")) {
                user.setStatus(jsonObject.getInteger("status"));
            }
            return user;
        } else {
            return null;
        }
    }

    /**
     * 将RoleVo转换为Role类型
     *
     * @param roleVo
     * @param userId
     * @return
     */
    public Role roleVoToRole(RoleVo roleVo, String userId) {
        Role role = new Role();
        if (null != roleVo && !StringUtils.isEmpty(roleVo.getName())) {
            role.setName(roleVo.getName());
        }
        role.setAvailable(RoleAvailableStatus.YES);
        role.setCreateTime(new Date());
        if (!StringUtils.isEmpty(userId)) {
            role.setCreatorId(userId);
        }
        if (null != roleVo && null != roleVo.getMenuVoList()) {
            role.setMenuList(BeanPropertiesUtil.convertList(roleVo.getMenuVoList(), Menu.class));
        }

        return role;
    }

    /**
     * 将MenuTreeVo类型的对象转换为MenuTree类型的对象
     *
     * @param menuTreeVo
     * @param loginUserId
     * @return
     */
    public MenuTree menuTreeVoToMenuTree(MenuTreeVo menuTreeVo, String loginUserId) {
        MenuTree menuTree = new MenuTree();
        if (null != menuTreeVo && StringUtils.validateParameter(menuTreeVo.getUserId())) {
            menuTree.setUserId(menuTreeVo.getUserId());
        }
        menuTree.setCreatorId(loginUserId);
        menuTree.setCreateTime(new Date());
        if (null != menuTreeVo && null != menuTreeVo.getMenuNodeList()) {
            menuTree.setFirstMenuList(BeanPropertiesUtil.convertList(menuTreeVo.getMenuNodeList(), Menu.class));
        }

        return menuTree;
    }


    /**
     * 将PostSale对象转换为xml字符串。用于存储到redis
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     *
     * @param postSale
     * @return
     */
    public String brandToXmlStorageString(PostSale postSale) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == postSale) {
            return null;
        } else {
            if (StringUtils.isNotEmpty(postSale.getId())) {
                stringBuilder.append("<id>").append(postSale.getId()).append("</id>");
            }
            if (StringUtils.isNotEmpty(postSale.getOrderNo())) {
                stringBuilder.append("<orderNo>").append(postSale.getOrderNo()).append("</orderNo>");
            }
            if (StringUtils.isNotEmpty(postSale.getModel())) {
                stringBuilder.append("<model>").append(postSale.getModel()).append("</model>");
            }
            if (postSale.getType() != null) {
                stringBuilder.append("<type>").append(postSale.getType()).append("</type>");
            }
            if (postSale.getAmount() != null) {
                stringBuilder.append("<amount>").append(postSale.getAmount()).append("</amount>");
            }
            if (!postSale.getUnitPrice().equals(0)) {
                stringBuilder.append("<unitPrice>").append(postSale.getUnitPrice()).append("</unitPrice>");
            }
            if (!postSale.getTotalPrice().equals(0)) {
                stringBuilder.append("<totalPrice>").append(postSale.getTotalPrice()).append("</totalPrice>");
            }
            if (postSale.getCurrency() != null) {
                stringBuilder.append("<currency>").append(postSale.getCurrency()).append("</currency>");
            }
            if (postSale.getApplicationDate() != null) {
                String s = DateUtils.formatDate(postSale.getApplicationDate(), DateUtils.DEFAULT_FORMAT);
                stringBuilder.append("<applicationDate>").append(s).append("</applicationDate>");
            }
            if (StringUtils.isNotEmpty(postSale.getWaybillNumber())) {
                stringBuilder.append("<waybillNumber>").append(postSale.getWaybillNumber()).append("</waybillNumber>");
            }
            if (StringUtils.isNotEmpty(postSale.getNote())) {
                stringBuilder.append("<note>").append(postSale.getNote()).append("</note>");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 将postSale对象转换为xml字符串。用于模糊查询
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     * 4.每个元素之间要有*。
     *
     * @param postSale
     * @return
     */
    public String brandToXmlFilterString(PostSale postSale, Boolean allKey) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == postSale) {
            return null;
        } else {
            if (StringUtils.isNotEmpty(postSale.getOrderNo())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<orderNo>");
                //判断是否需全匹配查询
                if (allKey) {
                    stringBuilder.append(RedisStorage.ASTERISK);
                }
                stringBuilder.append(postSale.getOrderNo()).append(RedisStorage.ASTERISK).append("</orderNo>");
            }
            if (StringUtils.isNotEmpty(postSale.getModel())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<model>");
                //判断是否需全匹配查询
                if (allKey) {
                    stringBuilder.append(RedisStorage.ASTERISK);
                }
                stringBuilder.append(postSale.getModel()).append(RedisStorage.ASTERISK).append("</model>");
            }
            if (postSale.getType() != null) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<type>");
                stringBuilder.append(postSale.getType()).append("</type>");
            }
//            if (postSale.getApplicationDate() != null) {
//                stringBuilder.append(postSale.getApplicationDate()).append("</applicationDate>");
//            }
            if (StringUtils.isNotEmpty(postSale.getWaybillNumber())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<waybillNumber>");
                //判断是否需全匹配查询
                if (allKey) {
                    stringBuilder.append(RedisStorage.ASTERISK);
                }
                stringBuilder.append(postSale.getWaybillNumber()).append(RedisStorage.ASTERISK).append("</waybillNumber>");
            }
            stringBuilder.append(RedisStorage.ASTERISK);
            return stringBuilder.toString();
        }
    }
}
