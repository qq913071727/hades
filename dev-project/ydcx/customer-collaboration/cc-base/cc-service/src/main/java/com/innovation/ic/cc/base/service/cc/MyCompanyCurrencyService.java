package com.innovation.ic.cc.base.service.cc;


import com.innovation.ic.cc.base.model.cc.MyCompanyCurrency;
import com.innovation.ic.cc.base.vo.CompanyCurrencyVo;

import java.util.List;

public interface MyCompanyCurrencyService {

    void saveBatch(List<MyCompanyCurrency> bindMyCompanyCurrency);

    void truncate();

    void listenDeleteCompanyCurrency(CompanyCurrencyVo companyCurrencyVo);

    void listenUpdateCompanyCurrency(CompanyCurrencyVo companyCurrencyVo);

    void listenAddCompanyCurrency(CompanyCurrencyVo companyCurrencyVo);

    List<MyCompanyCurrency> findAll();

    MyCompanyCurrency findByExternalId(String currency);


    MyCompanyCurrency findById(String id);

    MyCompanyCurrency findByEnName(String currency);
}
