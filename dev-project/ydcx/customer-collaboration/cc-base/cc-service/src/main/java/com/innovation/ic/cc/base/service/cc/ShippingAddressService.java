package com.innovation.ic.cc.base.service.cc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.cc.base.model.cc.ShippingAddress;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.address.ShippingAddressQueryVo;
import com.innovation.ic.cc.base.vo.address.ShippingAddressVo;

import java.util.List;

public interface ShippingAddressService {
    ServiceResult addShippingAddress(ShippingAddressVo shippingAddressVo);

    ServiceResult deleteShippingAddress(List<String> ids, AuthenticationUser authenticationUser);

    void updateShippingAddress(ShippingAddressVo shippingAddressVo);

    ServiceResult<IPage<ShippingAddress>> getShippingAddressList(ShippingAddressQueryVo shippingAddressQueryVo);

    void setUpDefault(ShippingAddressQueryVo shippingAddressQueryVo);
}
