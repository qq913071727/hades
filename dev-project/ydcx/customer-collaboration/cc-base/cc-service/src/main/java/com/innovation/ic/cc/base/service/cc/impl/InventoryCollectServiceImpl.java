package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.innovation.ic.cc.base.mapper.cc.InventoryCollectMapper;
import com.innovation.ic.cc.base.model.cc.InventoryCollect;
import com.innovation.ic.cc.base.pojo.constant.InventoryCollectConstants;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.InventoryCollectService;
import com.innovation.ic.cc.base.vo.inventoryCollect.InventoryCollectCancleReqVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @desc   库存收藏表service实现类
 * @author linuo
 * @time   2023年5月15日16:46:20
 */
@Service
public class InventoryCollectServiceImpl extends ServiceImpl<InventoryCollectMapper, InventoryCollect> implements InventoryCollectService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private InventoryCollectMapper inventoryCollectMapper;

    /**
     * 库存收藏数量查询
     * @return 返回结果
     */
    @Override
    public ServiceResult<Integer> queryInventoryCollectCount() {
        QueryWrapper<InventoryCollect> queryWrapper = new QueryWrapper<>();
        Integer result = inventoryCollectMapper.selectCount(queryWrapper);
        return ServiceResult.ok(result, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 收藏库存数据
     * @param inventoryCollectCancleReqVo 收藏、取消收藏库存数据接口的Vo类
     * @return 返回结果
     */
    @Override
    public ServiceResult<Boolean> add(InventoryCollectCancleReqVo inventoryCollectCancleReqVo) {
        Boolean result = Boolean.FALSE;
        String message = ServiceResult.OPERATE_FAIL;

        InventoryCollect inventoryCollect = new InventoryCollect();
        BeanUtils.copyProperties(inventoryCollectCancleReqVo, inventoryCollect);
        int insert = inventoryCollectMapper.insert(inventoryCollect);
        if(insert > 0){
            logger.info("收藏库存数据成功");
            result = Boolean.TRUE;
            message = ServiceResult.OPERATE_SUCCESS;
        }
        return ServiceResult.ok(result, message);
    }

    /**
     * 取消收藏
     * @param inventoryCollectCancleReqVo 收藏、取消收藏库存数据接口的Vo类
     * @return 返回结果
     */
    @Override
    public ServiceResult<Boolean> cancleCollect(InventoryCollectCancleReqVo inventoryCollectCancleReqVo) {
        Boolean result = Boolean.FALSE;
        String message = ServiceResult.OPERATE_FAIL;

        QueryWrapper<InventoryCollect> queryWrapper = new QueryWrapper<>();

        boolean ifQuery = Boolean.FALSE;

        if(!Strings.isNullOrEmpty(inventoryCollectCancleReqVo.getPartNumber())){
            queryWrapper.eq(InventoryCollectConstants.PART_NUMBER_FIELD, inventoryCollectCancleReqVo.getPartNumber());
            ifQuery = Boolean.TRUE;
        }
        if(!Strings.isNullOrEmpty(inventoryCollectCancleReqVo.getBrand())){
            queryWrapper.eq(InventoryCollectConstants.BRAND_FIELD, inventoryCollectCancleReqVo.getBrand());
            ifQuery = Boolean.TRUE;
        }
        if(inventoryCollectCancleReqVo.getCount() != null){
            queryWrapper.eq(InventoryCollectConstants.COUNT_FIELD, inventoryCollectCancleReqVo.getCount());
            ifQuery = Boolean.TRUE;
        }
        if(!Strings.isNullOrEmpty(inventoryCollectCancleReqVo.getBatch())){
            queryWrapper.eq(InventoryCollectConstants.BATCH_FIELD, inventoryCollectCancleReqVo.getBatch());
            ifQuery = Boolean.TRUE;
        }
        if(!Strings.isNullOrEmpty(inventoryCollectCancleReqVo.getPackages())){
            queryWrapper.eq(InventoryCollectConstants.PACKAGE_FIELD, inventoryCollectCancleReqVo.getPackages());
            ifQuery = Boolean.TRUE;
        }
        if(inventoryCollectCancleReqVo.getPrice() != null){
            queryWrapper.eq(InventoryCollectConstants.PRICE_FIELD, inventoryCollectCancleReqVo.getPrice());
            ifQuery = Boolean.TRUE;
        }
        if(!Strings.isNullOrEmpty(inventoryCollectCancleReqVo.getInventoryHome())){
            queryWrapper.eq(InventoryCollectConstants.INVENTORY_HOME_FIELD, inventoryCollectCancleReqVo.getInventoryHome());
            ifQuery = Boolean.TRUE;
        }

        if(ifQuery){
            int insert = inventoryCollectMapper.delete(queryWrapper);
            if(insert > 0){
                logger.info("取消收藏库存数据成功");
                result = Boolean.TRUE;
                message = ServiceResult.OPERATE_SUCCESS;
            }
        }

        return ServiceResult.ok(result, message);
    }
}