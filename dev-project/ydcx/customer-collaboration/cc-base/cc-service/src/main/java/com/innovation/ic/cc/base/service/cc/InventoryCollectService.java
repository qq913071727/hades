package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.inventoryCollect.InventoryCollectCancleReqVo;

/**
 * @desc   库存收藏表service
 * @author linuo
 * @time   2023年5月15日16:43:49
 */
public interface InventoryCollectService {
    /**
     * 库存收藏数量查询
     * @return 返回结果
     */
    ServiceResult<Integer> queryInventoryCollectCount();

    /**
     * 收藏库存数据
     * @param inventoryCollectCancleReqVo 收藏、取消收藏库存数据接口的Vo类
     * @return 返回结果
     */
    ServiceResult<Boolean> add(InventoryCollectCancleReqVo inventoryCollectCancleReqVo);

    /**
     * 取消收藏
     * @param inventoryCollectCancleReqVo 收藏、取消收藏库存数据接口的Vo类
     * @return 返回结果
     */
    ServiceResult<Boolean> cancleCollect(InventoryCollectCancleReqVo inventoryCollectCancleReqVo);
}