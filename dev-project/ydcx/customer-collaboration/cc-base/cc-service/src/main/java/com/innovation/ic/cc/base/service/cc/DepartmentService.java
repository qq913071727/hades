package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.model.cc.Department;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.DepartmentVo;
import java.util.List;

/**
 *  Department的接口类
 */
public interface DepartmentService {

    //添加部门数据
    ServiceResult<Boolean> addDepartment(DepartmentVo departmentVo);

    //查询所有部门(当前登录用户下)
    ServiceResult<List<Department>> findUserIdAllDepartment(String userId);

    //删除当前部门信息
    ServiceResult<Boolean> dellDepartment(Integer id);

    //将department表的数据导入到redis
    void initDepartmentData();
}
