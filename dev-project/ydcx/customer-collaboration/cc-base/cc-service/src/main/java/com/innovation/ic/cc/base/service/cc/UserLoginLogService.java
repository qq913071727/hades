package com.innovation.ic.cc.base.service.cc;

public interface UserLoginLogService {

    /**
     * 将账号信息存入登录日志表中
     *
     * @param username 账号
     * @param ipAddress 用户ip
     * @param loginType 用户登录方式
     * @return
     */
    void addUserLog(String username, String ipAddress, Integer loginType);
}