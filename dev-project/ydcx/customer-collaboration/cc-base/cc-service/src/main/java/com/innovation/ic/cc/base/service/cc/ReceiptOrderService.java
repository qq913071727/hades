package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.vo.ReceiptOrderVo;
import com.innovation.ic.cc.base.pojo.ReceiptOrderPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 订单收货单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
public interface ReceiptOrderService {

    ServiceResult<PageInfo<ReceiptOrderPojo>> pageInfo( int page, int rows,ReceiptOrderVo vo);

    ServiceResult<List<ReceiptOrderPojo>> list(ReceiptOrderVo vo);

    ServiceResult<ReceiptOrderPojo> get(String id);

    void save(ReceiptOrderVo vo);

    void update(ReceiptOrderVo vo);

    void delete(String[] ids);

    ServiceResult<PageInfo<ReceiptOrderPojo>> pages(int pageNo, int pageSize, ReceiptOrderVo vo);

    void confirm(List<Long> ids);
}