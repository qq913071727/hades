package com.innovation.ic.cc.base.service.erp9_pvebcs;

import com.innovation.ic.cc.base.model.cc.erp9_pvebcs.ErpDictionaries;
import com.innovation.ic.cc.base.pojo.enums.ImportEnumerationEnum;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;

import java.util.List;

public interface ErpDictionariesService {


    ServiceResult<List<ErpDictionaries>> findErpDictionaries(ImportEnumerationEnum... importEnumerationEnums);

}
