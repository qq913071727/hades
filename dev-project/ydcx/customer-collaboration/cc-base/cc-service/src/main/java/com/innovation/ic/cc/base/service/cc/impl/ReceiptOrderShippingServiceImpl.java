package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.mapper.cc.ReceiptOrderShippingMapper;
import com.innovation.ic.cc.base.service.cc.ReceiptOrderShippingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.innovation.ic.cc.base.vo.ReceiptOrderShippingVo;
import com.innovation.ic.cc.base.pojo.ReceiptOrderShippingPojo;
import com.innovation.ic.cc.base.model.cc.ReceiptOrderShipping;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import org.springframework.beans.BeanUtils;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 收货单订单和收货单运单关联表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@Service
public class ReceiptOrderShippingServiceImpl extends ServiceImpl<ReceiptOrderShippingMapper, ReceiptOrderShipping> implements ReceiptOrderShippingService {

    @Resource
    private ReceiptOrderShippingMapper baseMapper;

    @Override
    public ServiceResult<PageInfo<ReceiptOrderShippingPojo>> pageInfo(int page, int rows, ReceiptOrderShippingVo dto) {
        PageHelper.startPage(page, rows);
        List<ReceiptOrderShipping> list = baseMapper.pageInfo(dto);
        List<ReceiptOrderShippingPojo> targetList = BeanPropertiesUtil.convertList(list,ReceiptOrderShippingPojo.class);
        PageInfo<ReceiptOrderShippingPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<ReceiptOrderShippingPojo>> list(ReceiptOrderShippingVo dto) {
        List<ReceiptOrderShipping> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, ReceiptOrderShippingPojo.class), "ok");
    }

    private Wrapper<ReceiptOrderShipping> getWrapper(ReceiptOrderShippingVo dto) {
        LambdaQueryWrapper<ReceiptOrderShipping> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ReceiptOrderShipping::getId, dto.getId());
        return lambdaQueryWrapper;
    }


    @Override
    public ServiceResult<ReceiptOrderShippingPojo> get(String id) {
        ReceiptOrderShipping entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, ReceiptOrderShippingPojo.class), "ok");
    }

    @Override
    public void save(ReceiptOrderShippingVo dto) {
        ReceiptOrderShipping entity = BeanPropertiesUtil.convert(dto, ReceiptOrderShipping. class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(ReceiptOrderShippingVo dto) {
        ReceiptOrderShipping entity = BeanPropertiesUtil.convert(dto, ReceiptOrderShipping. class);

        baseMapper.updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}