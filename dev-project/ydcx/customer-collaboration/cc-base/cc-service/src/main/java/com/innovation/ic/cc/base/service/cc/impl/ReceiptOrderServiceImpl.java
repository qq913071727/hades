package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.mapper.cc.ReceiptOrderMapper;
import com.innovation.ic.cc.base.mapper.cc.ReceiptOrderShippingMapper;
import com.innovation.ic.cc.base.mapper.cc.ReceiptShippingMapper;
import com.innovation.ic.cc.base.model.cc.ReceiptOrderShipping;
import com.innovation.ic.cc.base.model.cc.ReceiptShipping;
import com.innovation.ic.cc.base.pojo.ReceiptOrderShippingPojo;
import com.innovation.ic.cc.base.pojo.ReceiptShippingPojo;
import com.innovation.ic.cc.base.pojo.enums.ReceiptStateEnum;
import com.innovation.ic.cc.base.service.cc.ReceiptOrderService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.innovation.ic.cc.base.vo.ReceiptOrderVo;
import com.innovation.ic.cc.base.pojo.ReceiptOrderPojo;
import com.innovation.ic.cc.base.model.cc.ReceiptOrder;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import org.springframework.beans.BeanUtils;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 订单收货单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@Service
public class ReceiptOrderServiceImpl extends ServiceImpl<ReceiptOrderMapper, ReceiptOrder> implements ReceiptOrderService {

    @Resource
    private ReceiptOrderMapper baseMapper;
    @Resource
    private ReceiptOrderShippingMapper receiptOrderShippingMapper;
    @Resource
    private ReceiptShippingMapper receiptShippingMapper;

    @Override
    public ServiceResult<PageInfo<ReceiptOrderPojo>> pageInfo(int page, int rows, ReceiptOrderVo dto) {
        PageHelper.startPage(page, rows);
        // 查看收货单订单数据
        LambdaQueryWrapper<ReceiptOrder> receiptOrderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderLambdaQueryWrapper.eq(StringUtils.isNotEmpty(dto.getOrderCode()), ReceiptOrder::getOrderCode, dto.getOrderCode());
        receiptOrderLambdaQueryWrapper.between(StringUtils.isNotEmpty(dto.getBeginDate()),ReceiptOrder::getOrderTime,dto.getBeginDate(),dto.getEndDate());
        receiptOrderLambdaQueryWrapper.orderByAsc(ReceiptOrder::getOrderState);
        receiptOrderLambdaQueryWrapper.orderByDesc(ReceiptOrder::getOrderTime);
        List<ReceiptOrder> receiptOrders = baseMapper.selectList(receiptOrderLambdaQueryWrapper);
        List<ReceiptOrderPojo> receiptOrderPojos = BeanPropertiesUtil.convertList(receiptOrders, ReceiptOrderPojo.class);
        // 查询订单运单中间表数据
        LambdaQueryWrapper<ReceiptOrderShipping> receiptOrderShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderShippingLambdaQueryWrapper.in(ReceiptOrderShipping::getReceiptOrderId, receiptOrderPojos.stream().map(ReceiptOrderPojo::getId).collect(Collectors.toSet()));
        List<ReceiptOrderShipping> receiptOrderShippings = receiptOrderShippingMapper.selectList(receiptOrderShippingLambdaQueryWrapper);
        List<ReceiptOrderShippingPojo> receiptOrderShippingPojos = BeanPropertiesUtil.convertList(receiptOrderShippings, ReceiptOrderShippingPojo.class);
        // 查询运单表数据
        List<Long> orderShippingIds = receiptOrderShippings.stream().map(ReceiptOrderShipping::getReceiptShippingId).collect(Collectors.toList());
        LambdaQueryWrapper<ReceiptShipping> receiptShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptShippingLambdaQueryWrapper.eq(StringUtils.isNotEmpty(dto.getModel()),ReceiptShipping::getShippingModel,dto.getModel());
        receiptShippingLambdaQueryWrapper.eq(StringUtils.isNotEmpty(dto.getShippingCode()),ReceiptShipping::getShippingCode,dto.getShippingCode());
        receiptShippingLambdaQueryWrapper.in(!CollectionUtils.isEmpty(orderShippingIds),ReceiptShipping::getId,orderShippingIds);
        List<ReceiptShipping> receiptShippings = receiptShippingMapper.selectList(receiptShippingLambdaQueryWrapper);

        for(ReceiptOrderPojo pojo: receiptOrderPojos){
            List<ReceiptOrderShippingPojo> receiptOrderShippingPojoList = new ArrayList<>();
            for(ReceiptOrderShippingPojo  receiptOrderShippingPojo:  receiptOrderShippingPojos){
                if(pojo.getId().equals(receiptOrderShippingPojo.getReceiptOrderId())) {
                    for (ReceiptShipping shipping : receiptShippings) {
                        if (receiptOrderShippingPojo.getReceiptShippingId().equals(shipping.getId())) {
                            receiptOrderShippingPojo.setReceiptShippingPojo(BeanPropertiesUtil.convert(shipping, ReceiptShippingPojo.class));
                        }
                    }
                    receiptOrderShippingPojoList.add(receiptOrderShippingPojo);
                }
            }
            pojo.setReceiptOrderShippingPojos(receiptOrderShippingPojoList);
        }
        return ServiceResult.ok(new PageInfo<>(receiptOrderPojos), "ok");
    }


    @Override
    public ServiceResult<PageInfo<ReceiptOrderPojo>> pages(int pageNo, int pageSize, ReceiptOrderVo dto) {
        PageHelper.startPage(pageNo, pageSize);
        // 查询运单订单表数据
        List<ReceiptOrderPojo> pages = baseMapper.pages(dto);
        Set<Long> collect = pages.stream().map(ReceiptOrderPojo::getId).collect(Collectors.toSet());
        // 查询订单运单中间表数据
        LambdaQueryWrapper<ReceiptOrderShipping> receiptOrderShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderShippingLambdaQueryWrapper.in(ReceiptOrderShipping::getReceiptOrderId, collect);
        List<ReceiptOrderShipping> receiptOrderShippings = receiptOrderShippingMapper.selectList(receiptOrderShippingLambdaQueryWrapper);
        List<ReceiptOrderShippingPojo> receiptOrderShippingPojos = BeanPropertiesUtil.convertList(receiptOrderShippings, ReceiptOrderShippingPojo.class);
        // 查询运单表数据
        List<Long> orderShippingIds = receiptOrderShippings.stream().map(ReceiptOrderShipping::getReceiptShippingId).collect(Collectors.toList());
        LambdaQueryWrapper<ReceiptShipping> receiptShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptShippingLambdaQueryWrapper.in(!CollectionUtils.isEmpty(orderShippingIds),ReceiptShipping::getId,orderShippingIds);
        List<ReceiptShipping> receiptShippings = receiptShippingMapper.selectList(receiptShippingLambdaQueryWrapper);

        for(ReceiptOrderPojo pojo: pages){
            List<ReceiptOrderShippingPojo> receiptOrderShippingPojoList = new ArrayList<>();
            for(ReceiptOrderShippingPojo  receiptOrderShippingPojo:  receiptOrderShippingPojos){
                if(pojo.getId().equals(receiptOrderShippingPojo.getReceiptOrderId())) {
                    for (ReceiptShipping shipping : receiptShippings) {
                        if (receiptOrderShippingPojo.getReceiptShippingId().equals(shipping.getId())) {
                            receiptOrderShippingPojo.setReceiptShippingPojo(BeanPropertiesUtil.convert(shipping, ReceiptShippingPojo.class));
                        }
                    }
                    receiptOrderShippingPojoList.add(receiptOrderShippingPojo);
                }
            }
            pojo.setReceiptOrderShippingPojos(receiptOrderShippingPojoList);
        }
        return ServiceResult.ok(new PageInfo<>(pages), "ok");
    }

    @Override
    public ServiceResult<List<ReceiptOrderPojo>> list(ReceiptOrderVo dto) {
        List<ReceiptOrder> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, ReceiptOrderPojo.class), "ok");
    }

    private Wrapper<ReceiptOrder> getWrapper(ReceiptOrderVo dto) {
        LambdaQueryWrapper<ReceiptOrder> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.ge(ReceiptOrder::getOrderState, dto.getOrderState());
        return lambdaQueryWrapper;
    }

    @Override
    public ServiceResult<ReceiptOrderPojo> get(String id) {
        ReceiptOrder entity = baseMapper.selectById(id);
        ReceiptOrderPojo orderPojo = BeanPropertiesUtil.convert(entity, ReceiptOrderPojo.class);
        // 查询订单运单中间表数据
        LambdaQueryWrapper<ReceiptOrderShipping> receiptOrderShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderShippingLambdaQueryWrapper.eq(ReceiptOrderShipping::getReceiptOrderId, orderPojo);
        List<ReceiptOrderShipping> receiptOrderShippings = receiptOrderShippingMapper.selectList(receiptOrderShippingLambdaQueryWrapper);
        List<ReceiptOrderShippingPojo> receiptOrderShippingPojos = BeanPropertiesUtil.convertList(receiptOrderShippings, ReceiptOrderShippingPojo.class);
        // 查询运单表数据
        List<Long> orderShippingIds = receiptOrderShippings.stream().map(ReceiptOrderShipping::getReceiptShippingId).collect(Collectors.toList());
        LambdaQueryWrapper<ReceiptShipping> receiptShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptShippingLambdaQueryWrapper.in(!CollectionUtils.isEmpty(orderShippingIds),ReceiptShipping::getId,orderShippingIds);
        List<ReceiptShipping> receiptShippings = receiptShippingMapper.selectList(receiptShippingLambdaQueryWrapper);

        List<ReceiptOrderShippingPojo> receiptOrderShippingPojoList = new ArrayList<>();
        for(ReceiptOrderShippingPojo  receiptOrderShippingPojo:  receiptOrderShippingPojos){
            if(orderPojo.getId().equals(receiptOrderShippingPojo.getReceiptOrderId())) {
                for (ReceiptShipping shipping : receiptShippings) {
                    if (receiptOrderShippingPojo.getReceiptShippingId().equals(shipping.getId())) {
                        receiptOrderShippingPojo.setReceiptShippingPojo(BeanPropertiesUtil.convert(shipping, ReceiptShippingPojo.class));
                    }
                }
                receiptOrderShippingPojoList.add(receiptOrderShippingPojo);
            }
        }
        orderPojo.setReceiptOrderShippingPojos(receiptOrderShippingPojoList);
        return ServiceResult.ok(orderPojo, "ok");
    }

    @Override
    public void save(ReceiptOrderVo dto) {
        ReceiptOrder entity = BeanPropertiesUtil.convert(dto, ReceiptOrder.class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(ReceiptOrderVo dto) {
        ReceiptOrder entity = BeanPropertiesUtil.convert(dto, ReceiptOrder.class);

        baseMapper.updateById(entity);
    }


    /**
     * @param ids
     * @Description: 确认收货
     * @Author: myq
     * @Date: 2023/5/18 11:25
     */
    @Override
    @Transactional
    public void confirm(List<Long> ids) {
        Assert.notEmpty(ids,"参数不能为空");

        ReceiptOrder receiptOrder = new ReceiptOrder();
        receiptOrder.setOrderState(ReceiptStateEnum.RECEIPTED.getCode());
        LambdaQueryWrapper<ReceiptOrder> receiptOrderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderLambdaQueryWrapper.in(ReceiptOrder::getId,ids);
        baseMapper.update(receiptOrder,receiptOrderLambdaQueryWrapper);

        LambdaQueryWrapper<ReceiptOrderShipping> receiptOrderShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderShippingLambdaQueryWrapper.in(ReceiptOrderShipping::getReceiptOrderId, ids);
        List<ReceiptOrderShipping> receiptOrderShippings = receiptOrderShippingMapper.selectList(receiptOrderShippingLambdaQueryWrapper);

        List<Long> orderShippingIds = receiptOrderShippings.stream().map(ReceiptOrderShipping::getReceiptShippingId).collect(Collectors.toList());
        LambdaQueryWrapper<ReceiptShipping> receiptShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptShippingLambdaQueryWrapper.in(!CollectionUtils.isEmpty(orderShippingIds),ReceiptShipping::getId,orderShippingIds);
        ReceiptShipping receiptShipping = new ReceiptShipping();
        receiptShipping.setShippingState(ReceiptStateEnum.RECEIPTED.getCode());
        receiptShippingMapper.update(receiptShipping, receiptShippingLambdaQueryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}