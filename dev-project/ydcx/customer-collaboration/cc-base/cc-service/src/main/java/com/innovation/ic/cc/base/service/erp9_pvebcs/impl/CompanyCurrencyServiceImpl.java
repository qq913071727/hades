package com.innovation.ic.cc.base.service.erp9_pvebcs.impl;

import com.innovation.ic.cc.base.model.cc.erp9_pvebcs.CompanyCurrency;
import com.innovation.ic.cc.base.service.erp9_pvebcs.CompanyCurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import  com.innovation.ic.cc.base.mapper.erp9_pvebcs.CompanyCurrencyMapper;
import java.util.List;


/**
 * erp币种同步
 */
@Slf4j
@Service
public class CompanyCurrencyServiceImpl implements CompanyCurrencyService {

    @Autowired
    private CompanyCurrencyMapper companyCurrencyMapper;

    @Override
    public List<CompanyCurrency> findCompanyCurrency() {
        return companyCurrencyMapper.findCompanyCurrency();
    }
}
