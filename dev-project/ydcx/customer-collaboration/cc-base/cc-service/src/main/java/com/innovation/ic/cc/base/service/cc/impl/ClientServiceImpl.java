package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.cc.ClientMapper;
import com.innovation.ic.cc.base.model.cc.Client;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ClientService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Client的具体实现类
 * @author linuo
 * @time   2022年8月11日15:25:55
 */
@Service
@Transactional
public class ClientServiceImpl extends ServiceImpl<ClientMapper, Client> implements ClientService {
    @Resource
    private ClientMapper clientMapper;

    /**
     * 查找client表中所有数据
     * @return 返回client表中所有数据
     */
    @Override
    public ServiceResult<List<Client>> findAll() {
        ServiceResult<List<Client>> serviceResult = new ServiceResult<>();
        List<Client> clientList = clientMapper.selectList(null);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(clientList);
        return serviceResult;
    }
}