package com.innovation.ic.cc.base.service.cc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.cc.base.model.cc.TicketReceivingAddress;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.address.TicketReceivingAddressQueryVo;
import com.innovation.ic.cc.base.vo.address.TicketReceivingAddressVo;

import java.util.List;

public interface TicketReceivingAddressService {

    ServiceResult addTicketReceivingAddress(TicketReceivingAddressVo ticketReceivingAddressVo);

    ServiceResult deleteTicketReceivingAddress(List<String> ids, AuthenticationUser authenticationUser);

    void updateticketReceivingAddress(TicketReceivingAddressVo ticketReceivingAddressVo);

    ServiceResult<IPage<TicketReceivingAddress>> getTicketReceivingAddressList(TicketReceivingAddressQueryVo ticketReceivingAddressQueryVo);

    void setUpDefault(TicketReceivingAddressQueryVo ticketReceivingAddressQueryVo);
}
