//package com.innovation.ic.cc.base.service.cc.impl;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.cc.base.handler.cc.QuotedPriceHandler;
//import com.innovation.ic.cc.base.mapper.cc.QuotedPriceMapper;
//import com.innovation.ic.cc.base.mapper.cc.QuotedPriceRecordMapper;
//import com.innovation.ic.cc.base.model.cc.QuotedPrice;
//import com.innovation.ic.cc.base.model.cc.QuotedPriceRecord;
//import com.innovation.ic.cc.base.pojo.enums.QuotedPriceRecordStatusEnum;
//import com.innovation.ic.cc.base.pojo.enums.QuotedPriceStatusEnum;
//import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceAndQuotedPriceRecordPojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPricePojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceRecordPojo;
//import com.innovation.ic.cc.base.service.cc.QuotedPriceService;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPriceAdoptionOrNotAcceptedVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPricePageListVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPriceRecordListVo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
///**
// * @author zengqinglong
// * @desc 报价管理
// * @Date 2023/2/14 13:59
// **/
//@Slf4j
//@Service
//public class QuotedPriceServiceImpl extends ServiceImpl<QuotedPriceMapper, QuotedPrice> implements QuotedPriceService {
//    @Resource
//    private QuotedPriceHandler quotedPriceHandler;
//
//    @Resource
//    private QuotedPriceRecordMapper quotedPriceRecordMapper;
//
//    /**
//     * @param quotedPricePageListVo
//     * @param authenticationUser
//     * @return
//     */
//    @Override
//    public PageInfo<QuotedPricePojo> pageList(QuotedPricePageListVo quotedPricePageListVo, AuthenticationUser authenticationUser) {
//        PageHelper.startPage(quotedPricePageListVo.getPageNo(), quotedPricePageListVo.getPageSize());
//        LambdaQueryWrapper<QuotedPrice> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        //todo 获取公司id
//        Set<String> myCompanyIdS = getMyCompanyId(authenticationUser.getEpId());
//
//        brandLambdaQueryWrapper.in(QuotedPrice::getMyCompanyId, myCompanyIdS);
//        if (StringUtils.isNotEmpty(quotedPricePageListVo.getModel())) {
//            brandLambdaQueryWrapper.like(QuotedPrice::getModelList, quotedPricePageListVo.getModel());
//        }
//        if (StringUtils.isNotEmpty(quotedPricePageListVo.getInquiryPriceNumber())) {
//            brandLambdaQueryWrapper.like(QuotedPrice::getInquiryPriceNumber, quotedPricePageListVo.getInquiryPriceNumber());
//        }
//        if (null != quotedPricePageListVo.getInquiryType()) {
//            brandLambdaQueryWrapper.eq(QuotedPrice::getInquiryType, quotedPricePageListVo.getInquiryType());
//        }
//
//        if (null != quotedPricePageListVo.getStartTime()) {
//            brandLambdaQueryWrapper.ge(QuotedPrice::getCreateTime, quotedPricePageListVo.getStartTime());
//        }
//
//        if (null != quotedPricePageListVo.getEndTime()) {
//            brandLambdaQueryWrapper.le(QuotedPrice::getCreateTime, quotedPricePageListVo.getEndTime());
//        }
//        //判断是否是主账号,不是主账号只能看到自己的询价
//        if (authenticationUser.getIsMain() == 0) {
//            brandLambdaQueryWrapper.eq(QuotedPrice::getUserId, authenticationUser.getId());
//        }
//        brandLambdaQueryWrapper.orderByDesc(QuotedPrice::getCreateTime);
//        List<QuotedPrice> list = baseMapper.selectList(brandLambdaQueryWrapper);
//
//        //包装参数
//        List<QuotedPricePojo> listPojo = list.stream().map(quotedPrice -> {
//            QuotedPricePojo quotedPricePojo = quotedPriceHandler.quotedPriceToQuotedPricePojo(quotedPrice);
//            return quotedPricePojo;
//        }).collect(Collectors.toList());
//        return new PageInfo<>(listPojo);
//    }
//
//    /**
//     * 询价管理: 我收到的报价: 一键采纳
//     *
//     * @param ids
//     */
//    @Override
//    public void adopt(List<String> ids) {
//        for (String id : ids) {
//            //todo 设置延迟队列判断 并且 设置 采纳后对应的截止时间
//            quotedPriceRecordMapper.updateStatusByQuotedPriceIdAndStatus(id, QuotedPriceRecordStatusEnum.TO_BE_ADOPTED.getStatus(), QuotedPriceRecordStatusEnum.VALID_QUOTATION.getStatus());
//            //判断是否需要更改报价单状态
//            checkQuotedPriceStatusAndUpdateStatus(id);
//        }
//    }
//
//    /**
//     * 报价详情
//     *
//     * @param id
//     * @return
//     */
//    @Override
//    public QuotedPriceAndQuotedPriceRecordPojo details(String id) {
//        QuotedPriceAndQuotedPriceRecordPojo quotedPriceAndQuotedPriceRecordPojo = new QuotedPriceAndQuotedPriceRecordPojo();
//        QuotedPrice quotedPrice = baseMapper.selectById(id);
//        if (quotedPrice != null) {
//            quotedPriceAndQuotedPriceRecordPojo.setQuotedPricePojo(quotedPriceHandler.quotedPriceToQuotedPricePojo(quotedPrice));
//            LambdaQueryWrapper<QuotedPriceRecord> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
//            brandLambdaQueryWrapper.eq(QuotedPriceRecord::getQuotedPriceId, quotedPrice.getId());
//            List<QuotedPriceRecordPojo> list = quotedPriceRecordMapper.selectList(brandLambdaQueryWrapper).stream().map(quotedPriceRecord -> {
//                return quotedPriceHandler.quotedPriceRecordToQuotedPriceRecordPojo(quotedPriceRecord);
//            }).collect(Collectors.toList());
//            quotedPriceAndQuotedPriceRecordPojo.setQuotedPriceRecordPojoList(list);
//        }
//        return quotedPriceAndQuotedPriceRecordPojo;
//    }
//
//    /**
//     * 过滤品牌型号
//     *
//     * @param quotedPriceRecordListVo
//     * @return
//     */
//    @Override
//    public List<QuotedPriceRecordPojo> filterBrandModelList(QuotedPriceRecordListVo quotedPriceRecordListVo) {
//        LambdaQueryWrapper<QuotedPriceRecord> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        brandLambdaQueryWrapper.eq(QuotedPriceRecord::getQuotedPriceId, quotedPriceRecordListVo.getQuotedPriceId());
//        if (StringUtils.isNotEmpty(quotedPriceRecordListVo.getModel())) {
//            brandLambdaQueryWrapper.eq(QuotedPriceRecord::getModel, quotedPriceRecordListVo.getModel());
//        }
//        if (null != quotedPriceRecordListVo.getStatus()) {
//            brandLambdaQueryWrapper.eq(QuotedPriceRecord::getStatus, quotedPriceRecordListVo.getStatus());
//        }
//        List<QuotedPriceRecord> quotedPriceRecordList = quotedPriceRecordMapper.selectList(brandLambdaQueryWrapper);
//        return quotedPriceRecordList.stream().map(quotedPriceRecord -> {
//            return quotedPriceHandler.quotedPriceRecordToQuotedPriceRecordPojo(quotedPriceRecord);
//        }).collect(Collectors.toList());
//    }
//
//    /**
//     * 报价详情 : 一键采纳/不接受
//     *
//     * @param quotedPriceAdoptionOrNotAcceptedVo
//     */
//    @Override
//    public void adoptionOrNotAccepted(QuotedPriceAdoptionOrNotAcceptedVo quotedPriceAdoptionOrNotAcceptedVo) {
//        //todo 设置延迟队列判断 并且 设置 采纳后对应的截止时间
//        quotedPriceRecordMapper.updateStatusByQuotedPriceIdAndStatusAndIds(
//                quotedPriceAdoptionOrNotAcceptedVo.getQuotedPriceId(),
//                QuotedPriceStatusEnum.TO_BE_ADOPTED.getStatus(),
//                quotedPriceAdoptionOrNotAcceptedVo.getStatus(),
//                quotedPriceAdoptionOrNotAcceptedVo.getIds());
//
//        //判断是否需要更改报价单状态
//        checkQuotedPriceStatusAndUpdateStatus(quotedPriceAdoptionOrNotAcceptedVo.getQuotedPriceId());
//    }
//
//    /**
//     * 2
//     * 获取对应的公司
//     *
//     * @return
//     */
//    private Set<String> getMyCompanyId(String epId) {
//        return null;
//    }
//
//    /**
//     * 检查报价单是否需要更改状态
//     *
//     * @param id 报价单id
//     */
//    private void checkQuotedPriceStatusAndUpdateStatus(String id) {
//        QuotedPrice quotedPrice = baseMapper.selectById(id);
//        //排除为空的
//        if (quotedPrice == null) {
//            return;
//        }
//        //查询所有品牌型号总数
//        LambdaQueryWrapper<QuotedPriceRecord> countWrapper = new LambdaQueryWrapper<>();
//        countWrapper.eq(QuotedPriceRecord::getQuotedPriceId, id);
//        Integer count = quotedPriceRecordMapper.selectCount(countWrapper);
//        QuotedPrice updateStatusQuotedPrice = new QuotedPrice();
//        updateStatusQuotedPrice.setId(quotedPrice.getId());
//        //待采纳
//        if (isToBeAdopted(id, count) && quotedPrice.getStatus() != QuotedPriceStatusEnum.TO_BE_ADOPTED.getStatus()) {
//            updateStatusQuotedPrice.setStatus(QuotedPriceStatusEnum.TO_BE_ADOPTED.getStatus());
//            baseMapper.updateById(updateStatusQuotedPrice);
//            return;
//        }
//
//        //是否为无效报价
//        if (isInvalidQuotation(id, count) && quotedPrice.getStatus() != QuotedPriceStatusEnum.INVALID_QUOTATION.getStatus()) {
//            updateStatusQuotedPrice.setStatus(QuotedPriceStatusEnum.INVALID_QUOTATION.getStatus());
//            baseMapper.updateById(updateStatusQuotedPrice);
//            return;
//        }
//
//        //是否为超时未采纳
//        if (isTimeoutNotAdopted(id, count) && quotedPrice.getStatus() != QuotedPriceStatusEnum.TIMEOUT_NOT_ADOPTED.getStatus()) {
//            updateStatusQuotedPrice.setStatus(QuotedPriceStatusEnum.TIMEOUT_NOT_ADOPTED.getStatus());
//            baseMapper.updateById(updateStatusQuotedPrice);
//            return;
//        }
//
//        //是否为已采纳
//        if (isAdopted(id, count) && quotedPrice.getStatus() != QuotedPriceStatusEnum.ADOPTED.getStatus()) {
//            updateStatusQuotedPrice.setStatus(QuotedPriceStatusEnum.ADOPTED.getStatus());
//            baseMapper.updateById(updateStatusQuotedPrice);
//            return;
//        }
//
//        //是否为部分采纳
//        if (isPartialAdoption(id, count) && quotedPrice.getStatus() != QuotedPriceStatusEnum.PARTIAL_ADOPTION.getStatus()) {
//            updateStatusQuotedPrice.setStatus(QuotedPriceStatusEnum.PARTIAL_ADOPTION.getStatus());
//            baseMapper.updateById(updateStatusQuotedPrice);
//            return;
//        }
//    }
//
//    /**
//     * 是否为待采纳
//     *
//     * @param id
//     * @param count
//     * @return
//     */
//    private Boolean isToBeAdopted(String id, Integer count) {
//        LambdaQueryWrapper<QuotedPriceRecord> invalidQuotationWrapper = new LambdaQueryWrapper<>();
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getQuotedPriceId, id);
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getStatus, QuotedPriceRecordStatusEnum.TO_BE_ADOPTED.getStatus());
//        Integer invalidQuotationCount = quotedPriceRecordMapper.selectCount(invalidQuotationWrapper);
//        if (invalidQuotationCount.equals(count)) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * 是否为无效报价
//     *
//     * @param id
//     * @param count
//     * @return
//     */
//    private Boolean isInvalidQuotation(String id, Integer count) {
//        LambdaQueryWrapper<QuotedPriceRecord> invalidQuotationWrapper = new LambdaQueryWrapper<>();
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getQuotedPriceId, id);
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getStatus, QuotedPriceRecordStatusEnum.INVALID_QUOTATION.getStatus());
//        Integer invalidQuotationCount = quotedPriceRecordMapper.selectCount(invalidQuotationWrapper);
//        if (invalidQuotationCount.equals(count)) {
//            return true;
//        }
//        return false;
//    }
//
//
//    /**
//     * 是否为超时未采纳
//     *
//     * @param id
//     * @param count
//     * @return
//     */
//    private Boolean isTimeoutNotAdopted(String id, Integer count) {
//        LambdaQueryWrapper<QuotedPriceRecord> invalidQuotationWrapper = new LambdaQueryWrapper<>();
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getQuotedPriceId, id);
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getStatus, QuotedPriceRecordStatusEnum.TIMEOUT_NOT_ADOPTED.getStatus());
//        Integer invalidQuotationCount = quotedPriceRecordMapper.selectCount(invalidQuotationWrapper);
//        if (invalidQuotationCount.equals(count)) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * 是否为已采纳
//     *
//     * @param id
//     * @param count
//     * @return
//     */
//    private Boolean isAdopted(String id, Integer count) {
//        LambdaQueryWrapper<QuotedPriceRecord> invalidQuotationWrapper = new LambdaQueryWrapper<>();
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getQuotedPriceId, id);
//        invalidQuotationWrapper.in(QuotedPriceRecord::getStatus, QuotedPriceRecordStatusEnum.VALID_QUOTATION.getStatus(),
//                QuotedPriceRecordStatusEnum.INVALID_QUOTATION.getStatus(),
//                QuotedPriceRecordStatusEnum.ORDER_GENERATED.getStatus());
//        Integer invalidQuotationCount = quotedPriceRecordMapper.selectCount(invalidQuotationWrapper);
//        if (invalidQuotationCount.equals(count)) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * 是否为部分采纳
//     *
//     * @param id
//     * @return
//     */
//    private Boolean isPartialAdoption(String id, Integer count) {
//        LambdaQueryWrapper<QuotedPriceRecord> invalidQuotationWrapper = new LambdaQueryWrapper<>();
//        invalidQuotationWrapper.eq(QuotedPriceRecord::getQuotedPriceId, id);
//        invalidQuotationWrapper.in(QuotedPriceRecord::getStatus, QuotedPriceRecordStatusEnum.VALID_QUOTATION.getStatus(),
//                QuotedPriceRecordStatusEnum.ORDER_GENERATED.getStatus());
//        Integer invalidQuotationCount = quotedPriceRecordMapper.selectCount(invalidQuotationWrapper);
//        if (invalidQuotationCount > 0 && invalidQuotationCount < count) {
//            return true;
//        }
//        return false;
//    }
//
//}
