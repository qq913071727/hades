package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.model.cc.User;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UsersTopView;
import com.innovation.ic.cc.base.pojo.UserMainPojo;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.PageNationPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.UserPagePojo;
import com.innovation.ic.cc.base.vo.login.LoginByUserNameVo;
import com.innovation.ic.cc.base.vo.user.UsersVo;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface UserService {

    /**
     * 查找所有User对象
     *
     * @return
     */
    ServiceResult<List<User>> findAll();

    /**
     * 忘记密码
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> forgetPassword(UsersVo usersVo);

    /**
     * 通过主键获取用户详情
     *
     * @param id
     * @return
     */
    ServiceResult<AuthenticationUser> queryUsers(String id);

    /**
     * 修改密码
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> updatePassword(UsersVo usersVo, AuthenticationUser authenticationUser);

    /**
     * 修改手机号
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Map> updatePhone(UsersVo usersVo) throws IOException;

    /**
     * 根据id查询用户
     *
     * @param userId
     * @return
     */
    ServiceResult<User> findByUserId(String userId);

    /**
     * 当前主账号下分页展示子账号信息
     *
     * @param usersVo
     * @return
     */
    ServiceResult<PageNationPojo<UserPagePojo>> pageUserNew(UsersVo usersVo);

    /**
     * 判断当前数据是否重复
     *
     * @param userData
     * @param key
     * @return
     */
    ServiceResult<Boolean> findData(String userData, String key);

    /**
     * 新增账号
     *
     * @param usersVo
     * @return
     */
    ServiceResult<String> saveUser(UsersVo usersVo, String id) throws IOException, ParseException;

    /**
     * 修改账号信息
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> updateUser(UsersVo usersVo) throws IOException;

    /**
     * 重置密码
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> resetPassword(UsersVo usersVo);

    /**
     * 账号状态更改
     *
     * @param usersVo
     * @return
     */
    void updateStatus(UsersVo usersVo);

    /**
     * 初始化校验相关所有数据
     *
     * @return
     */
    void initCheckData();

    /**
     * 判断当前密码数据是否重复
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> findPassWord(UsersVo usersVo) throws IOException;

    /**
     * 判断手机号是否不正确
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> findAccountPhone(UsersVo usersVo);

    /**
     * 初始化用户相关所有数据
     *
     * @return
     */
    void initUserData();

    /**
     * 先清空 MySQL中users主账号
     *
     * @param map
     * @return
     */
    void batchDelete(Map<String, Object> map);

    /**
     * 把sql server中数据同步到MySQL中user表中 新
     *
     * @param userList
     */
    void insertAllNew(List<ParentUsersTopView> userList);

    /**
     * 把sql server中数据同步到MySQL中user表中
     *
     * @param userList
     * @return
     */
    void insertAll(List<ParentUsersTopView> userList);

    /**
     * 监听主账号新增/修改
     * @param userMainPojo sqlservice中相关数据
     */
    void handleModelAddUsersMqMsg(UserMainPojo userMainPojo);

    /**
     * 监听密码被重置
     * @param usersTopView usersTopView
     */
    void handleModelUpdatePassWordMqMsg(UsersTopView usersTopView);

    /**
     * 监听主账号状态
     * @param userMainPojo sqlservice中相关数据
     */
    void handleModelUpdateUsersMqMsg(UserMainPojo userMainPojo);

    /**
     * 通过账号密码登录
     * @param loginByUserNameVo 通过账号密码登录接口的vo类
     * @return 返回登录结果
     */
    ServiceResult<AuthenticationUser>  loginByUsername(LoginByUserNameVo loginByUserNameVo) throws IOException;
}