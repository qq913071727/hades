package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.vo.ReceiptOrderShippingVo;
import com.innovation.ic.cc.base.pojo.ReceiptOrderShippingPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 收货单订单和收货单运单关联表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
public interface ReceiptOrderShippingService {

    ServiceResult<PageInfo<ReceiptOrderShippingPojo>> pageInfo( int page, int rows,ReceiptOrderShippingVo vo);

    ServiceResult<List<ReceiptOrderShippingPojo>> list(ReceiptOrderShippingVo vo);

    ServiceResult<ReceiptOrderShippingPojo> get(String id);

    void save(ReceiptOrderShippingVo vo);

    void update(ReceiptOrderShippingVo vo);

    void delete(String[] ids);
}