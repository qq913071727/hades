package com.innovation.ic.cc.base.service.cc;


import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.UserManagementPojo;
import com.innovation.ic.cc.base.vo.UserManagementVo;

import java.util.List;

/**
 *  UserManagement的接口类
 */
public interface UserManagementService {

    //进行批量添加人员数据
    ServiceResult<Boolean> addUserManagement(UserManagementVo userManagementVo);

    //查询当前部门下用户的人员管理数据
    ServiceResult<List<UserManagementPojo>> findDepartmentUser(UserManagementVo userManagementVo);

    //删除人员管理数据
    ServiceResult<Boolean> dellUserManagement(UserManagementVo userManagementVo);

    //将user_management表的数据导入到redis
    void initUserManagementData();
}
