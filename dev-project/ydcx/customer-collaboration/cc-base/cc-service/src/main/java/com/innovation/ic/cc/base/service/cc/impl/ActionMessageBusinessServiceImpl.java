package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.cc.base.mapper.cc.ActionMessageBusinessMapper;
import com.innovation.ic.cc.base.model.cc.ActionMessageBusiness;
import com.innovation.ic.cc.base.pojo.ActionMessageBusinessPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ActionMessageBusinessService;
import com.innovation.ic.cc.base.vo.ActionMessageBusinessVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 动作消息业务表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-22
 */
@Service
public class ActionMessageBusinessServiceImpl extends ServiceImpl<ActionMessageBusinessMapper, ActionMessageBusiness> implements ActionMessageBusinessService {

    @Resource
    private ActionMessageBusinessMapper baseMapper;

    @Override
    public ServiceResult<PageInfo<ActionMessageBusinessPojo>> pageInfo(int page, int rows, ActionMessageBusinessVo dto) {
        PageHelper.startPage(page, rows);
        List<ActionMessageBusiness> list = baseMapper.pageInfo(dto);
        List<ActionMessageBusinessPojo> targetList = BeanPropertiesUtil.convertList(list, ActionMessageBusinessPojo.class);
        PageInfo<ActionMessageBusinessPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<ActionMessageBusinessPojo>> list(ActionMessageBusinessVo dto) {
        List<ActionMessageBusiness> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, ActionMessageBusinessPojo.class), "ok");
    }

    private Wrapper<ActionMessageBusiness> getWrapper(ActionMessageBusinessVo dto) {
        LambdaQueryWrapper<ActionMessageBusiness> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ActionMessageBusiness::getActionMessageId, dto.getActionMessageId());
        return lambdaQueryWrapper;
    }


    @Override
    public ServiceResult<ActionMessageBusinessPojo> get(String id) {
        ActionMessageBusiness entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, ActionMessageBusinessPojo.class), "ok");
    }

    @Override
    public void save(ActionMessageBusinessVo dto) {
        ActionMessageBusiness entity = BeanPropertiesUtil.convert(dto, ActionMessageBusiness.class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(ActionMessageBusinessVo dto) {
        ActionMessageBusiness entity = BeanPropertiesUtil.convert(dto, ActionMessageBusiness.class);

        baseMapper.updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}