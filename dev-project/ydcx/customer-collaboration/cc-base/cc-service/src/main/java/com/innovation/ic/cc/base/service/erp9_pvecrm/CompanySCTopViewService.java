package com.innovation.ic.cc.base.service.erp9_pvecrm;


import com.innovation.ic.cc.base.model.cc.erp9_pvecrm.CompanySCTopView;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;

import java.util.List;

/**
 * 注册公司信息
 */
public interface CompanySCTopViewService {


    ServiceResult<List<CompanySCTopView>> findByIds(List<String> ids);


}