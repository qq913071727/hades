package com.innovation.ic.cc.base.service.cc.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.TicketReceivingAddressMapper;
import com.innovation.ic.cc.base.model.cc.Region;
import com.innovation.ic.cc.base.model.cc.TicketReceivingAddress;
import com.innovation.ic.cc.base.pojo.enums.ShippingAddressEnum;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.RegionService;
import com.innovation.ic.cc.base.service.cc.TicketReceivingAddressService;
import com.innovation.ic.cc.base.vo.address.TicketReceivingAddressQueryVo;
import com.innovation.ic.cc.base.vo.address.TicketReceivingAddressVo;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TicketReceivingAddressServiceImpl extends ServiceImpl<TicketReceivingAddressMapper, TicketReceivingAddress> implements TicketReceivingAddressService {


    @Resource
    private RegionService regionService;

    @Override
    public ServiceResult addTicketReceivingAddress(TicketReceivingAddressVo ticketReceivingAddressVo) {
        ServiceResult serviceResult = new ServiceResult();
        if (ticketReceivingAddressVo.getDefaultAddress().equals(ShippingAddressEnum.DEFAULT.getCode()) && StringUtils.validateParameter(ticketReceivingAddressVo.getCreateId())) {
            //更新当前创建人所有创建地址默认为否
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("create_id", ticketReceivingAddressVo.getCreateId());
            paramMap.put("default_address", ShippingAddressEnum.DEFAULT.getCode());
            List<TicketReceivingAddress> ticketReceivingAddressList = this.baseMapper.selectByMap(paramMap);
            if (!CollectionUtils.isEmpty(ticketReceivingAddressList)) {
                for (TicketReceivingAddress ticketReceivingAddress : ticketReceivingAddressList) {
                    UpdateWrapper<TicketReceivingAddress> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id", ticketReceivingAddress.getId());
                    updateWrapper.set("default_address", ShippingAddressEnum.NO_DEFAULT.getCode());
                    this.baseMapper.update(null, updateWrapper);
                }
            }
        }
        TicketReceivingAddress ticketReceivingAddress = new TicketReceivingAddress();
        BeanUtil.copyProperties(ticketReceivingAddressVo, ticketReceivingAddress);
        ticketReceivingAddress.setCreateTime(new Date());
        List<Integer> paramList = new ArrayList<>();
        paramList.add(ticketReceivingAddressVo.getProvince());
        paramList.add(ticketReceivingAddressVo.getCity());
        paramList.add(ticketReceivingAddressVo.getDistinguish());
        paramList.add(ticketReceivingAddressVo.getStreet());
        List<Region> regionList = regionService.findByIds(paramList);
        if (!CollectionUtils.isEmpty(regionList)) {
            for (Region region : regionList) {
                if (region.getId().equals(ticketReceivingAddressVo.getProvince())) {
                    ticketReceivingAddress.setProvinceName(region.getName());
                }
                if (region.getId().equals(ticketReceivingAddressVo.getCity())) {
                    ticketReceivingAddress.setCityName(region.getName());
                }
                if (region.getId().equals(ticketReceivingAddressVo.getDistinguish())) {
                    ticketReceivingAddress.setDistinguishName(region.getName());
                }
                if (region.getId().equals(ticketReceivingAddressVo.getStreet())) {
                    ticketReceivingAddress.setStreetName(region.getName());
                }
            }
        }
        this.baseMapper.insert(ticketReceivingAddress);
        //查询地址总数
        QueryWrapper<TicketReceivingAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("create_id", ticketReceivingAddressVo.getCreateId());
        Integer count = this.baseMapper.selectCount(queryWrapper);
        serviceResult.setMessage("添加成功");
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(count);
        return serviceResult;
    }

    @Override
    public ServiceResult deleteTicketReceivingAddress(List<String> ids, AuthenticationUser authenticationUser) {
        ServiceResult serviceResult = new ServiceResult();
        List<TicketReceivingAddress> ticketReceivingAddressList = this.baseMapper.selectBatchIds(ids);
        if (CollectionUtils.isEmpty(ticketReceivingAddressList)) {
            return serviceResult;
        }
        for (TicketReceivingAddress ticketReceivingAddress : ticketReceivingAddressList) {
            if (authenticationUser.getId().equals(ticketReceivingAddress.getCreateId())) {
                this.baseMapper.deleteById(ticketReceivingAddress.getId());
            }
        }
        //查询地址总数
        QueryWrapper<TicketReceivingAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("create_id", authenticationUser.getId());
        Integer count = this.baseMapper.selectCount(queryWrapper);
        serviceResult.setResult(count);
        return serviceResult;
    }

    @Override
    public void updateticketReceivingAddress(TicketReceivingAddressVo ticketReceivingAddressVo) {
        if (ticketReceivingAddressVo.getDefaultAddress().equals(ShippingAddressEnum.DEFAULT.getCode()) && StringUtils.validateParameter(ticketReceivingAddressVo.getCreateId())) {
            //更新当前创建人所有创建地址默认为否
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("create_id", ticketReceivingAddressVo.getCreateId());
            paramMap.put("default_address", ShippingAddressEnum.DEFAULT.getCode());
            List<TicketReceivingAddress> ticketReceivingAddressList = this.baseMapper.selectByMap(paramMap);
            if (!CollectionUtils.isEmpty(ticketReceivingAddressList)) {
                for (TicketReceivingAddress ticketReceivingAddress : ticketReceivingAddressList) {
                    UpdateWrapper<TicketReceivingAddress> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id", ticketReceivingAddress.getId());
                    updateWrapper.set("default_address", ShippingAddressEnum.NO_DEFAULT.getCode());
                    this.baseMapper.update(null, updateWrapper);
                }
            }
        }
        TicketReceivingAddress ticketReceivingAddress = new TicketReceivingAddress();
        BeanUtil.copyProperties(ticketReceivingAddressVo, ticketReceivingAddress);
        ticketReceivingAddress.setCreateTime(new Date());
        List<Integer> paramList = new ArrayList<>();
        paramList.add(ticketReceivingAddressVo.getProvince());
        paramList.add(ticketReceivingAddressVo.getCity());
        paramList.add(ticketReceivingAddressVo.getDistinguish());
        paramList.add(ticketReceivingAddressVo.getStreet());
        List<Region> regionList = regionService.findByIds(paramList);
        if (!CollectionUtils.isEmpty(regionList)) {
            for (Region region : regionList) {
                if (region.getId().equals(ticketReceivingAddressVo.getProvince())) {
                    ticketReceivingAddress.setProvinceName(region.getName());
                }
                if (region.getId().equals(ticketReceivingAddressVo.getCity())) {
                    ticketReceivingAddress.setCityName(region.getName());
                }
                if (region.getId().equals(ticketReceivingAddressVo.getDistinguish())) {
                    ticketReceivingAddress.setDistinguishName(region.getName());
                }
                if (region.getId().equals(ticketReceivingAddressVo.getStreet())) {
                    ticketReceivingAddress.setStreetName(region.getName());
                }
            }
        }
        this.baseMapper.updateById(ticketReceivingAddress);
    }

    @Override
    public ServiceResult<IPage<TicketReceivingAddress>> getTicketReceivingAddressList(TicketReceivingAddressQueryVo ticketReceivingAddressQueryVo) {
        ServiceResult<IPage<TicketReceivingAddress>> serviceResult = new ServiceResult<>();
        LambdaQueryWrapper<TicketReceivingAddress> ticketReceivingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ticketReceivingLambdaQueryWrapper.eq(TicketReceivingAddress::getCreateId, ticketReceivingAddressQueryVo.getCreateId());
        ticketReceivingLambdaQueryWrapper.orderByDesc(TicketReceivingAddress::getCreateTime);
        Page<TicketReceivingAddress> shippingAddressPage = baseMapper.selectPage(new Page<>(ticketReceivingAddressQueryVo.getPageNo(), ticketReceivingAddressQueryVo.getPageSize()), ticketReceivingLambdaQueryWrapper);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(shippingAddressPage);
        return serviceResult;
    }

    @Override
    public void setUpDefault(TicketReceivingAddressQueryVo ticketReceivingAddressQueryVo) {
        //更新当前创建人所有创建地址默认为否
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("create_id", ticketReceivingAddressQueryVo.getCreateId());
        paramMap.put("default_address", ShippingAddressEnum.DEFAULT.getCode());
        List<TicketReceivingAddress> ticketReceivingAddressList = this.baseMapper.selectByMap(paramMap);
        if (!CollectionUtils.isEmpty(ticketReceivingAddressList)) {
            for (TicketReceivingAddress ticketReceivingAddress : ticketReceivingAddressList) {
                UpdateWrapper<TicketReceivingAddress> updateWrapper = new UpdateWrapper<>();
                updateWrapper.eq("id", ticketReceivingAddress.getId());
                updateWrapper.set("default_address", ShippingAddressEnum.NO_DEFAULT.getCode());
                this.baseMapper.update(null, updateWrapper);
            }
        }
        UpdateWrapper<TicketReceivingAddress> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", ticketReceivingAddressQueryVo.getId());
        updateWrapper.set("default_address", ShippingAddressEnum.DEFAULT.getCode());
        this.baseMapper.update(null, updateWrapper);
    }
}
