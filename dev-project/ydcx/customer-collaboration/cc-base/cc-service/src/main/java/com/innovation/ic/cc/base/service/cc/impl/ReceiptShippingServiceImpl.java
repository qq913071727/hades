package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.mapper.cc.ReceiptOrderMapper;
import com.innovation.ic.cc.base.mapper.cc.ReceiptOrderShippingMapper;
import com.innovation.ic.cc.base.mapper.cc.ReceiptShippingMapper;
import com.innovation.ic.cc.base.model.cc.ReceiptOrder;
import com.innovation.ic.cc.base.model.cc.ReceiptOrderShipping;
import com.innovation.ic.cc.base.pojo.enums.ReceiptStateEnum;
import com.innovation.ic.cc.base.service.cc.ReceiptShippingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.innovation.ic.cc.base.vo.ReceiptShippingVo;
import com.innovation.ic.cc.base.pojo.ReceiptShippingPojo;
import com.innovation.ic.cc.base.model.cc.ReceiptShipping;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import org.springframework.beans.BeanUtils;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 收货单运单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@Service
public class ReceiptShippingServiceImpl extends ServiceImpl<ReceiptShippingMapper, ReceiptShipping> implements ReceiptShippingService {

    @Resource
    private ReceiptShippingMapper baseMapper;

    @Resource
    private ReceiptOrderMapper receiptOrderMapper;

    @Resource
    private ReceiptOrderShippingMapper receiptOrderShippingMapper;

    @Override
    public ServiceResult<PageInfo<ReceiptShippingPojo>> pageInfo(int page, int rows, ReceiptShippingVo dto) {
        PageHelper.startPage(page, rows);
        List<ReceiptShipping> list = baseMapper.pageInfo(dto);
        List<ReceiptShippingPojo> targetList = BeanPropertiesUtil.convertList(list,ReceiptShippingPojo.class);
        PageInfo<ReceiptShippingPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<ReceiptShippingPojo>> list(ReceiptShippingVo dto) {
        List<ReceiptShipping> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, ReceiptShippingPojo.class), "ok");
    }

    private Wrapper<ReceiptShipping> getWrapper(ReceiptShippingVo dto) {
        LambdaQueryWrapper<ReceiptShipping> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ReceiptShipping::getId, dto.getId());
        return lambdaQueryWrapper;
    }


    @Override
    public ServiceResult<ReceiptShippingPojo> get(String id) {
        ReceiptShipping entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, ReceiptShippingPojo.class), "ok");
    }

    @Override
    public void save(ReceiptShippingVo dto) {
        ReceiptShipping entity = BeanPropertiesUtil.convert(dto, ReceiptShipping. class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(ReceiptShippingVo dto) {
        ReceiptShipping entity = BeanPropertiesUtil.convert(dto, ReceiptShipping. class);

        baseMapper.updateById(entity);
    }

    @Override
    @Transactional
    public void confirm(List<Long> ids) {
        Assert.notEmpty(ids,"参数不能为空");
        // 更新收货单运单状态 已收货
        LambdaQueryWrapper<ReceiptShipping> receiptShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptShippingLambdaQueryWrapper.in(!CollectionUtils.isEmpty(ids),ReceiptShipping::getId,ids);
        ReceiptShipping receiptShipping = new ReceiptShipping();
        receiptShipping.setShippingState(ReceiptStateEnum.RECEIPTED.getCode());
        baseMapper.update(receiptShipping, receiptShippingLambdaQueryWrapper);
        // 中间表数据
        LambdaQueryWrapper<ReceiptOrderShipping> receiptOrderShippingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderShippingLambdaQueryWrapper.in(ReceiptOrderShipping::getReceiptShippingId, ids);
        List<ReceiptOrderShipping> receiptOrderShippings = receiptOrderShippingMapper.selectList(receiptOrderShippingLambdaQueryWrapper);

        // 更新收货单订单表状态 已收货/部分收货
        ReceiptOrder receiptOrder = new ReceiptOrder();
        LambdaQueryWrapper<ReceiptOrderShipping> groupReceiptOrderIdWrapper = new LambdaQueryWrapper<>();
        groupReceiptOrderIdWrapper.eq(ReceiptOrderShipping::getReceiptOrderId,receiptOrderShippings.get(0));
        Integer groupCountByOrderId = receiptOrderShippingMapper.selectCount(groupReceiptOrderIdWrapper);
        if(groupCountByOrderId == ids.size()){
            receiptOrder.setOrderState(ReceiptStateEnum.RECEIPTED.getCode());
        }else {
            receiptOrder.setOrderState(ReceiptStateEnum.PART_RECEIPT.getCode());
        }
        LambdaQueryWrapper<ReceiptOrder> receiptOrderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        receiptOrderLambdaQueryWrapper.in(ReceiptOrder::getId,receiptOrderShippings.stream().map(ReceiptOrderShipping::getReceiptShippingId).collect(Collectors.toSet()));
        receiptOrderMapper.update(receiptOrder,receiptOrderLambdaQueryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

}