package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.inventory.InventoryBlurSearchRespPojo;
import com.innovation.ic.cc.base.vo.inventory.BlurSearchInventoryInfoReqVo;
import java.util.List;

/**
 * @desc   库存相关service
 * @author linuo
 * @time   2023年5月12日16:01:41
 */
public interface InventoryService {
    /**
     * 获取调用大赢家接口时的key值
     * @return 返回结果
     */
    ServiceResult<String> getDyjInterfaceKey();

    /**
     * 模糊查询库存信息
     * @param blurSearchInventoryInfoReqVo 模糊查询库存信息的请求Vo类
     * @param key 调用大赢家接口时的key值
     * @return 返回查询结果
     */
    ServiceResult<List<InventoryBlurSearchRespPojo>> blurSearchInventoryInfo(BlurSearchInventoryInfoReqVo blurSearchInventoryInfoReqVo, String key);
}