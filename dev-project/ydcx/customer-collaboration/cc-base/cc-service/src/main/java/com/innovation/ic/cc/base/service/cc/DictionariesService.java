package com.innovation.ic.cc.base.service.cc;


import com.innovation.ic.cc.base.model.cc.Dictionaries;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.DictionariesErpVo;
import com.innovation.ic.cc.base.vo.DictionariesVo;
import com.innovation.ic.cc.base.vo.ErpDictionariesVo;

import java.util.List;
import java.util.Map;

public interface DictionariesService {

    void saveBatch(List<DictionariesVo> dictionaries);

    ServiceResult<List<Dictionaries>> searchDistrict(String dTypeId);

    void saveDictionaries(DictionariesErpVo dictionariesErpVo);

    void deleteById(String id);


    ServiceResult<List<Dictionaries>> findByIds(List<String> ids);

    void listenAddDistrict(ErpDictionariesVo erpDictionariesVo);


    ServiceResult<Dictionaries> findById(String id);

    List<Dictionaries> findAll();

    ServiceResult<Map<String,List<Dictionaries>>> findByDcode(List<String> paramList);

    List<Dictionaries> findByDcodes(List<String> paramList);

    ServiceResult<Map<String, List<Dictionaries>>> findByMyCompanyDcode(List<String> paramList);
}
