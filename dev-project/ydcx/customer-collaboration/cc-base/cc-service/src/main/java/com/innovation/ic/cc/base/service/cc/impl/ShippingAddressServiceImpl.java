package com.innovation.ic.cc.base.service.cc.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.ShippingAddressMapper;
import com.innovation.ic.cc.base.model.cc.Region;
import com.innovation.ic.cc.base.model.cc.ShippingAddress;
import com.innovation.ic.cc.base.pojo.enums.ShippingAddressEnum;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.RegionService;
import com.innovation.ic.cc.base.service.cc.ShippingAddressService;
import com.innovation.ic.cc.base.vo.address.ShippingAddressQueryVo;
import com.innovation.ic.cc.base.vo.address.ShippingAddressVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ShippingAddressServiceImpl extends ServiceImpl<ShippingAddressMapper, ShippingAddress> implements ShippingAddressService {

    @Resource
    private RegionService regionService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServiceResult addShippingAddress(ShippingAddressVo shippingAddressVo) {
        ServiceResult serviceResult = new ServiceResult();
        if (shippingAddressVo.getDefaultAddress().equals(ShippingAddressEnum.DEFAULT.getCode()) && StringUtils.validateParameter(shippingAddressVo.getCreateId())) {
            //更新当前创建人所有创建地址默认为否
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("create_id", shippingAddressVo.getCreateId());
            paramMap.put("default_address", ShippingAddressEnum.DEFAULT.getCode());
            List<ShippingAddress> shippingAddressesList = this.baseMapper.selectByMap(paramMap);
            if (!CollectionUtils.isEmpty(shippingAddressesList)) {
                for (ShippingAddress shippingAddress : shippingAddressesList) {
                    UpdateWrapper<ShippingAddress> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id", shippingAddress.getId());
                    updateWrapper.set("default_address", ShippingAddressEnum.NO_DEFAULT.getCode());
                    this.baseMapper.update(null, updateWrapper);
                }
            }
        }
        ShippingAddress shippingAddress = new ShippingAddress();
        BeanUtil.copyProperties(shippingAddressVo, shippingAddress);
        shippingAddress.setCreateTime(new Date());
        List<Integer> paramList = new ArrayList<>();
        paramList.add(shippingAddressVo.getProvince());
        paramList.add(shippingAddressVo.getCity());
        paramList.add(shippingAddressVo.getDistinguish());
        paramList.add(shippingAddressVo.getStreet());
        List<Region> regionList = regionService.findByIds(paramList);
        if (!CollectionUtils.isEmpty(regionList)) {
            for (Region region : regionList) {
                if (region.getId().equals(shippingAddressVo.getProvince())) {
                    shippingAddress.setProvinceName(region.getName());
                }
                if (region.getId().equals(shippingAddressVo.getCity())) {
                    shippingAddress.setCityName(region.getName());
                }
                if (region.getId().equals(shippingAddressVo.getDistinguish())) {
                    shippingAddress.setDistinguishName(region.getName());
                }
                if (region.getId().equals(shippingAddressVo.getStreet())) {
                    shippingAddress.setStreetName(region.getName());
                }
            }
        }
        this.baseMapper.insert(shippingAddress);
        //查询地址总数
        QueryWrapper<ShippingAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("create_id",shippingAddressVo.getCreateId());
        Integer count = this.baseMapper.selectCount(queryWrapper);
        serviceResult.setMessage("添加成功");
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(count);
        return serviceResult;
    }

    @Override
    public ServiceResult deleteShippingAddress(List<String> ids, AuthenticationUser authenticationUser) {
        ServiceResult serviceResult = new ServiceResult();
        List<ShippingAddress> shippingAddressesList = this.baseMapper.selectBatchIds(ids);
        if (CollectionUtils.isEmpty(shippingAddressesList)) {
            return serviceResult;
        }
        for (ShippingAddress shippingAddress : shippingAddressesList) {
            if (authenticationUser.getId().equals(shippingAddress.getCreateId())) {
                this.baseMapper.deleteById(shippingAddress.getId());
            }
        }
        //查询地址总数
        QueryWrapper<ShippingAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("create_id",authenticationUser.getId());
        Integer count = this.baseMapper.selectCount(queryWrapper);
        serviceResult.setResult(count);
        return serviceResult;
    }

    @Override
    public void updateShippingAddress(ShippingAddressVo shippingAddressVo) {
        if (shippingAddressVo.getDefaultAddress().equals(ShippingAddressEnum.DEFAULT.getCode()) && StringUtils.validateParameter(shippingAddressVo.getCreateId())) {
            //更新当前创建人所有创建地址默认为否
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("create_id", shippingAddressVo.getCreateId());
            paramMap.put("default_address", ShippingAddressEnum.DEFAULT.getCode());
            List<ShippingAddress> shippingAddressesList = this.baseMapper.selectByMap(paramMap);
            if (!CollectionUtils.isEmpty(shippingAddressesList)) {
                for (ShippingAddress shippingAddress : shippingAddressesList) {
                    UpdateWrapper<ShippingAddress> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id", shippingAddress.getId());
                    updateWrapper.set("default_address", ShippingAddressEnum.NO_DEFAULT.getCode());
                    this.baseMapper.update(null, updateWrapper);
                }
            }
        }
        ShippingAddress shippingAddress = new ShippingAddress();
        BeanUtil.copyProperties(shippingAddressVo, shippingAddress);
        List<Integer> paramList = new ArrayList<>();
        paramList.add(shippingAddressVo.getProvince());
        paramList.add(shippingAddressVo.getCity());
        paramList.add(shippingAddressVo.getDistinguish());
        paramList.add(shippingAddressVo.getStreet());
        List<Region> regionList = regionService.findByIds(paramList);
        if (!CollectionUtils.isEmpty(regionList)) {
            for (Region region : regionList) {
                if (region.getId().equals(shippingAddressVo.getProvince())) {
                    shippingAddress.setProvinceName(region.getName());
                }
                if (region.getId().equals(shippingAddressVo.getCity())) {
                    shippingAddress.setCityName(region.getName());
                }
                if (region.getId().equals(shippingAddressVo.getDistinguish())) {
                    shippingAddress.setDistinguishName(region.getName());
                }
                if (region.getId().equals(shippingAddressVo.getStreet())) {
                    shippingAddress.setStreetName(region.getName());
                }
            }
        }
        this.updateById(shippingAddress);
    }

    @Override
    public ServiceResult<IPage<ShippingAddress>> getShippingAddressList(ShippingAddressQueryVo shippingAddressQueryVo) {
        ServiceResult<IPage<ShippingAddress>> serviceResult = new ServiceResult<>();
        LambdaQueryWrapper<ShippingAddress> shippingAddressLambdaQueryWrapper = new LambdaQueryWrapper<>();
        shippingAddressLambdaQueryWrapper.eq(ShippingAddress::getCreateId, shippingAddressQueryVo.getCreateId());
        shippingAddressLambdaQueryWrapper.orderByDesc(ShippingAddress::getCreateTime);
        Page<ShippingAddress> shippingAddressPage = baseMapper.selectPage(new Page<>(shippingAddressQueryVo.getPageNo(), shippingAddressQueryVo.getPageSize()), shippingAddressLambdaQueryWrapper);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(shippingAddressPage);
        return serviceResult;
    }

    @Override
    public void setUpDefault(ShippingAddressQueryVo shippingAddressQueryVo) {
        //更新当前创建人所有创建地址默认为否
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("create_id", shippingAddressQueryVo.getCreateId());
        paramMap.put("default_address", ShippingAddressEnum.DEFAULT.getCode());
        List<ShippingAddress> shippingAddressesList = this.baseMapper.selectByMap(paramMap);
        if (!CollectionUtils.isEmpty(shippingAddressesList)) {
            for (ShippingAddress shippingAddress : shippingAddressesList) {
                UpdateWrapper<ShippingAddress> updateWrapper = new UpdateWrapper<>();
                updateWrapper.eq("id", shippingAddress.getId());
                updateWrapper.set("default_address", ShippingAddressEnum.NO_DEFAULT.getCode());
                this.baseMapper.update(null, updateWrapper);
            }
        }

        UpdateWrapper<ShippingAddress> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", shippingAddressQueryVo.getId());
        updateWrapper.set("default_address", ShippingAddressEnum.DEFAULT.getCode());
        this.baseMapper.update(null, updateWrapper);
    }
}
