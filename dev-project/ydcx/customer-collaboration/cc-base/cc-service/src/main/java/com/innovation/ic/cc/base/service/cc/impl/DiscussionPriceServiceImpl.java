//package com.innovation.ic.cc.base.service.cc.impl;
//
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.innovation.ic.b1b.framework.manager.RedisManager;
//import com.innovation.ic.cc.base.mapper.cc.DiscussionPriceRecordMapper;
//import com.innovation.ic.cc.base.mapper.cc.QuotedPriceRecordMapper;
//import com.innovation.ic.cc.base.model.cc.DiscussionPriceRecord;
//import com.innovation.ic.cc.base.model.cc.QuotedPriceRecord;
//import com.innovation.ic.cc.base.service.cc.DiscussionPriceService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//
///**
// * @author zengqinglong
// * @desc 议价管理
// * @Date 2023/2/14 17:03
// **/
//@Slf4j
//@Service
//public class DiscussionPriceServiceImpl extends ServiceImpl<DiscussionPriceRecordMapper, DiscussionPriceRecord> implements DiscussionPriceService {
//    @Resource
//    private QuotedPriceRecordMapper quotedPriceRecordMapper;
//    @Resource
//    private RedisManager redisManager;
//
//    /**
//     * 创建议价
//     *
//     * @param quotedPriceRecordId
//     */
//    @Override
//    public void createdDiscussionPrice(String quotedPriceRecordId) {
//        QuotedPriceRecord quotedPriceRecord = quotedPriceRecordMapper.selectById(quotedPriceRecordId);
//        //判断是否为空
//        if (quotedPriceRecord == null){
//            log.error("创建议价 失败,当前quotedPriceRecordId:{},不存在",quotedPriceRecordId);
//           return;
//        }
//        //todo 暂停开发
//        //创建议价单
//        DiscussionPriceRecord discussionPriceRecord = new DiscussionPriceRecord();
//
//
//    }
//}
