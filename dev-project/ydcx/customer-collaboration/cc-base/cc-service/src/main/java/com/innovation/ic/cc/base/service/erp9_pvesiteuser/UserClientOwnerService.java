package com.innovation.ic.cc.base.service.erp9_pvesiteuser;

import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UserClientOwnerTopView;

import java.util.List;

public interface UserClientOwnerService {

    /**
     * 获取用户关联销售id集合
     * @return
     */
    List<UserClientOwnerTopView> selectList(List<String> userList);
}
