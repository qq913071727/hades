package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.cc.base.mapper.cc.SysAnnouncementMapper;
import com.innovation.ic.cc.base.model.cc.SysAnnouncement;
import com.innovation.ic.cc.base.pojo.SysAnnouncementPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.SysAnnouncementService;
import com.innovation.ic.cc.base.vo.SysAnnouncementVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 系统公告
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-20
 */
@Service
public class SysAnnouncementServiceImpl extends ServiceImpl<SysAnnouncementMapper, SysAnnouncement> implements SysAnnouncementService {

    @Resource
    private SysAnnouncementMapper baseMapper;

    @Override
    public ServiceResult<PageInfo<SysAnnouncementPojo>> pageInfo(int page, int rows, SysAnnouncementVo dto) {
        PageHelper.startPage(page, rows);
        List<SysAnnouncement> list = baseMapper.pageInfo(dto);
        List<SysAnnouncementPojo> targetList = BeanPropertiesUtil.convertList(list, SysAnnouncementPojo.class);
        PageInfo<SysAnnouncementPojo> pageInfo = new PageInfo<>(targetList);
        BeanUtils.copyProperties(new PageInfo<>(list), pageInfo);
        return ServiceResult.ok(pageInfo, "ok");
    }

    @Override
    public ServiceResult<List<SysAnnouncementPojo>> list(SysAnnouncementVo dto) {
        List<SysAnnouncement> entityList = baseMapper.selectList(getWrapper(dto));

        return ServiceResult.ok(BeanPropertiesUtil.convertList(entityList, SysAnnouncementPojo.class), "ok");
    }

    private Wrapper<SysAnnouncement> getWrapper(SysAnnouncementVo dto) {
        LambdaQueryWrapper<SysAnnouncement> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SysAnnouncement::getId,dto.getId());
        return lambdaQueryWrapper;
    }

    @Override
    public ServiceResult<SysAnnouncementPojo> get(String id) throws Exception {
        SysAnnouncement entity = baseMapper.selectById(id);

        return ServiceResult.ok(BeanPropertiesUtil.convert(entity, SysAnnouncementPojo.class), "ok");
    }

    @Override
    public void save(SysAnnouncementVo dto) throws Exception {
        SysAnnouncement entity = BeanPropertiesUtil.convert(dto, SysAnnouncement.class);

        baseMapper.insert(entity);
    }

    @Override
    public void update(SysAnnouncementVo dto) throws Exception {
        SysAnnouncement entity = BeanPropertiesUtil.convert(dto, SysAnnouncement.class);

        updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String[] ids) {
        baseMapper.deleteBatchIds(Arrays.asList(ids));
    }

    /**
     * @Description: 公告信息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/2017:08
     */
    @Override
    public ServiceResult<SysAnnouncementPojo> getAnnouncementInfo() throws Exception {
        LambdaQueryWrapper<SysAnnouncement> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysAnnouncement::getEnableFlag,1);
        SysAnnouncement sysAnnouncement = baseMapper.selectOne(wrapper);

        return ServiceResult.ok(BeanPropertiesUtil.convert(sysAnnouncement,SysAnnouncementPojo.class),"ok");
    }
}