package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.DictionariesTypeMapper;
import com.innovation.ic.cc.base.model.cc.DictionariesType;
import com.innovation.ic.cc.base.service.cc.DictionariesTypeService;
import com.innovation.ic.cc.base.vo.DictionariesTypeVo;
import com.innovation.ic.cc.base.vo.ErpDictionariesVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


@Service
@Slf4j
public class DictionariesTypeServiceImpl extends ServiceImpl<DictionariesTypeMapper, DictionariesType> implements DictionariesTypeService {


    @Resource
    private DictionariesTypeMapper dictionariesTypeMapper;

    @Override
    public void saveDictionariesType(DictionariesTypeVo dictionariesTypeVo) {
        if (StringUtils.validateParameter(dictionariesTypeVo.getID())) {
            return;
        }
        if (StringUtils.validateParameter(dictionariesTypeVo.getName())) {
            return;
        }
        DictionariesType dictionariesType = new DictionariesType();
        dictionariesType.setName(dictionariesTypeVo.getName());
        dictionariesType.setId(dictionariesTypeVo.getID());
        dictionariesType.setCreateDate(new Date());
        dictionariesTypeMapper.insert(dictionariesType);
    }

    @Override
    public void listenAddDistrictType(ErpDictionariesVo erpDictionariesVo) {
        String id = erpDictionariesVo.getID();
        String name = erpDictionariesVo.getName();
        if (!StringUtils.validateParameter(id) || !StringUtils.validateParameter(name)) {
            return;
        }
        DictionariesType containDictionariesType = this.baseMapper.selectById(id);
        if(containDictionariesType == null){
            DictionariesType dictionariesType = new DictionariesType();
            dictionariesType.setId(id);
            dictionariesType.setName(name);
            dictionariesType.setCreateDate(new Date());
            save(dictionariesType);
        }else{
            containDictionariesType.setName(name);
            this.baseMapper.updateById(containDictionariesType);
        }
    }

    @Override
    public void deleteBatchIds(HashSet<String> dictionariesTypeIdS) {
        this.baseMapper.deleteBatchIds(dictionariesTypeIdS);
    }

    @Override
    public void insertBatchSomeColumn(List<DictionariesType> dtList) {
        this.baseMapper.insertBatchSomeColumn(dtList);
    }

    @Override
    public DictionariesType selectById(String id) {
        return this.baseMapper.selectById(id);
    }
}
