package com.innovation.ic.cc.base.service.cc.impl;


import com.innovation.ic.cc.base.model.cc.MapUserRole;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.MapUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * MapUserRoleService的具体实现类
 */
@Service
@Transactional
public class MapUserRoleServiceImpl  implements MapUserRoleService {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 保存MapUserRole类型对象列表（先删除旧的关系，再添加新的关系）
     *
     * @param mapUserRoleList
     * @return
     */
    @Override
    public ServiceResult<Boolean> saveOrUpdateMapUserRoleList(List<MapUserRole> mapUserRoleList) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();

        // 删除
        Query query = Query.query(Criteria.where("userId").is(mapUserRoleList.get(0).getUserId()));
        mongoTemplate.remove(query, MapUserRole.class);

        // 添加
        mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, MapUserRole.class).insert(mapUserRoleList).execute();

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据userId，查找MapUserRole对象列表
     *
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<List<MapUserRole>> findByUserId(String userId) {
        ServiceResult<List<MapUserRole>> serviceResult = new ServiceResult<List<MapUserRole>>();

        Query query = new Query(Criteria.where("userId").is(userId));
        List<MapUserRole> mapUserRoleList = mongoTemplate.find(query, MapUserRole.class);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(mapUserRoleList);
        return serviceResult;
    }
}
