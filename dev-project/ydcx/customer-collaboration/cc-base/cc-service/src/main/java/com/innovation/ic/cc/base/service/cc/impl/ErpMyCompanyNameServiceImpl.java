package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.ErpMyCompanyNameMapper;
import com.innovation.ic.cc.base.model.cc.ErpMyCompanyName;
import com.innovation.ic.cc.base.pojo.enums.ErpMyCompanyEnum;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ErpMyCompanyNameService;
import com.innovation.ic.cc.base.service.cc.MyCompanyService;
import com.innovation.ic.cc.base.vo.ErpMyCompanyNameVo;
import com.innovation.ic.cc.base.vo.SearchErpMyCompanyNameVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j

public class ErpMyCompanyNameServiceImpl extends ServiceImpl<ErpMyCompanyNameMapper, ErpMyCompanyName> implements ErpMyCompanyNameService {

    @Resource
    private MyCompanyService myCompanyService;



    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBatch(List<ErpMyCompanyName> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        this.getBaseMapper().insertBatchSomeColumn(list);
    }

    @Override
    public ServiceResult<List<ErpMyCompanyName>> findByLikeCompanyName(SearchErpMyCompanyNameVo searchErpMyCompanyNameVo) {
        ServiceResult<List<ErpMyCompanyName>> serviceResult = new ServiceResult<>();
        Integer pageSize = searchErpMyCompanyNameVo.getPageSize();
        Integer pageNum = searchErpMyCompanyNameVo.getPageNum() - 1;
        searchErpMyCompanyNameVo.setPageParam(pageNum * pageSize);
        searchErpMyCompanyNameVo.setPageNum(pageSize);
        //根据要求,只会查正常状态的公司
        searchErpMyCompanyNameVo.setStatus(ErpMyCompanyEnum.NORMAL.getCode());
        List<ErpMyCompanyName> erpMyCompanyNameList = this.baseMapper.findByLikeCompanyName(searchErpMyCompanyNameVo);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erpMyCompanyNameList);
        return serviceResult;
    }

    @Override
    public void saveOrUpdateErpCompany(ErpMyCompanyNameVo erpMyCompanyNameVo) {
        if (!StringUtils.validateParameter(erpMyCompanyNameVo.getName())) {
            return;
        }
        //验重,如果名字一样,就不进行添加
        Map<String, Object> param = new HashMap<>();
        param.put("name", erpMyCompanyNameVo.getName());
        List<ErpMyCompanyName> erpMyCompanyNames = this.getBaseMapper().selectByMap(param);
        if (!CollectionUtils.isEmpty(erpMyCompanyNames)) {
            //更新
            ErpMyCompanyName updateErpMyCompanyName = erpMyCompanyNames.get(0);
            updateErpMyCompanyName.setName(erpMyCompanyNameVo.getName());
            updateErpMyCompanyName.setExternalId(erpMyCompanyNameVo.getID());
            updateErpMyCompanyName.setDistrict(erpMyCompanyNameVo.getDistrict());
            updateErpMyCompanyName.setType(erpMyCompanyNameVo.getType());
            updateErpMyCompanyName.setProducts(erpMyCompanyNameVo.getProducts());
            updateErpMyCompanyName.setFiles(erpMyCompanyNameVo.getFiles());
            updateErpMyCompanyName.setEnterpriseStatus(ErpMyCompanyEnum.NORMAL.getCode());
            this.baseMapper.updateById(updateErpMyCompanyName);
        } else {
            ErpMyCompanyName erpMyCompanyName = new ErpMyCompanyName();
            erpMyCompanyName.setName(erpMyCompanyNameVo.getName());
            erpMyCompanyName.setCreateDate(new Date());
            //默认通过
            erpMyCompanyName.setEnterpriseStatus(ErpMyCompanyEnum.NORMAL.getCode());
            erpMyCompanyName.setExternalId(erpMyCompanyNameVo.getID());
            erpMyCompanyName.setDistrict(erpMyCompanyNameVo.getDistrict());
            erpMyCompanyName.setType(erpMyCompanyNameVo.getType());
            erpMyCompanyName.setProducts(erpMyCompanyNameVo.getProducts());
            erpMyCompanyName.setFiles(erpMyCompanyNameVo.getFiles());
            this.baseMapper.insert(erpMyCompanyName);
        }
    }

    @Override
    public void updateErpCompany(ErpMyCompanyNameVo erpMyCompanyNameVo) {
        if (!StringUtils.validateParameter(erpMyCompanyNameVo.getID())) {
            return;
        }
        if (erpMyCompanyNameVo.getStatus() == null) {
            return;
        }
        UpdateWrapper<ErpMyCompanyName> wrapper = Wrappers.update();
        wrapper.lambda()
                .set(ErpMyCompanyName::getEnterpriseStatus, erpMyCompanyNameVo.getStatus())
                .eq(ErpMyCompanyName::getExternalId, erpMyCompanyNameVo.getID());
        update(null, wrapper);
    }

    @Override
    public Integer findByCompanyName(String name) {
        return this.baseMapper.findByCompanyName(name);
    }

    @Override
    public ErpMyCompanyName findById(String id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public ErpMyCompanyName findByExternalId(String externalId) {
        Map<String, Object> param = new HashMap<>();
        param.put("external_id", externalId);
        List<ErpMyCompanyName> erpMyCompanyNames = this.baseMapper.selectByMap(param);
        if (!CollectionUtils.isEmpty(erpMyCompanyNames)) {
            return erpMyCompanyNames.get(0);
        }
        return null;
    }

    @Override
    public ErpMyCompanyName findByNameMap(Map<String, Object> paramMap) {
        List<ErpMyCompanyName> erpMyCompanyNames = this.baseMapper.selectByMap(paramMap);
        if (!CollectionUtils.isEmpty(erpMyCompanyNames)) {
            return erpMyCompanyNames.get(0);
        }
        return null;
    }

    @Override
    public void trunCateTable() {
        this.baseMapper.trunCateTable();
    }


}
