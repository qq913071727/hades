package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.model.cc.UploadFile;
import com.innovation.ic.cc.base.pojo.enums.UploadFileEnum;
import com.innovation.ic.cc.base.pojo.enums.UploadFileStatusEnum;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.FileVo;
import com.innovation.ic.cc.base.vo.RelationMyCompanyNameVo;
import com.jcraft.jsch.SftpException;

import java.util.List;

public interface UploadFileService {

    void updateRelationId(List<String> relationPictureIds, UploadFileEnum uploadFileEnum, Integer relationId);

    ServiceResult fileUpload(byte[] bytes, String originalFilename, String suffix, String creatorID) throws SftpException;


    ServiceResult<List<UploadFile>> findByRelationId(UploadFileEnum uploadFileEnum, String relationId);


    ServiceResult<List<UploadFile>> findByRelationId(UploadFileEnum uploadFileEnum, String relationId, UploadFileStatusEnum uploadFileStatusEnum);

    void saveRelationMyCompanyBatch(List<FileVo> list, RelationMyCompanyNameVo relationMyCompanyNameVo);

    void saveUploadFile(List<UploadFile> files);


    ServiceResult<List<UploadFile>> findByIds(List<String> ids);

    void updateRelationIdStatus(UploadFileEnum uploadFileEnum, String relationId, UploadFileStatusEnum uploadFileStatusEnum);

    ServiceResult pictureUploadToModel(byte[] bytes, String originalFilename, String suffix, String id, String model, String relationId);
}
