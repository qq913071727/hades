package com.innovation.ic.cc.base.service.cc.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.cc.base.mapper.cc.MyCompanyMapper;
import com.innovation.ic.cc.base.model.cc.*;
import com.innovation.ic.cc.base.pojo.constant.DyjInterfaceConstants;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.inventory.InventoryBlurSearchRespPojo;
import com.innovation.ic.cc.base.service.cc.*;
import com.innovation.ic.cc.base.value.DyjInterfaceAddressConfig;
import com.innovation.ic.cc.base.vo.inventory.BlurSearchInventoryInfoReqVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class InventoryServiceImpl extends ServiceImpl<MyCompanyMapper, MyCompany> implements InventoryService {
    @Resource
    private DyjInterfaceAddressConfig dyjInterfaceAddressConfig;

    /**
     * 获取调用大赢家接口时的key值
     * @return 返回结果
     */
    @Override
    public ServiceResult<String> getDyjInterfaceKey() {
        String getJsrInfoByIdAddress = dyjInterfaceAddressConfig.getGetJsrInfoByIdAddress();
        Map<String, String> headers = new HashMap<>();
        headers.put(DyjInterfaceConstants.HOST, DyjInterfaceConstants.HOST);
        headers.put(DyjInterfaceConstants.USER_AGENT, DyjInterfaceConstants.USER_AGENT);
        String key = HttpUtils.doGet(getJsrInfoByIdAddress, null, headers);
        log.info("获取调用大赢家接口时的key值为:[{}]", key);

        return ServiceResult.ok(key, ServiceResult.SELECT_SUCCESS);
    }

    /**
     * 模糊查询库存信息
     * @param blurSearchInventoryInfoReqVo 模糊查询库存信息的请求Vo类
     * @param key 调用大赢家接口时的key值
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<InventoryBlurSearchRespPojo>> blurSearchInventoryInfo(BlurSearchInventoryInfoReqVo blurSearchInventoryInfoReqVo, String key) {
        List<InventoryBlurSearchRespPojo> list = new ArrayList<>();

        String blurQueryPartNumberAddress = dyjInterfaceAddressConfig.getBlurQueryPartNumberAddress();
        JSONObject json = new JSONObject();
        json.put(DyjInterfaceConstants.KEY, key.substring(1, key.length() - 1));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(DyjInterfaceConstants.OBJ_PART_NO, blurSearchInventoryInfoReqVo.getPartNumber());

        json.put(DyjInterfaceConstants.DATA, jsonObject.toString());
        String result = HttpUtils.sendPost(blurQueryPartNumberAddress, json.toString());
        if(!Strings.isNullOrEmpty(result)){
            result = result.replaceAll("\\\\", "");
            result = result.substring(1, result.length() - 1);
            JSONObject parse = JSONObject.parseObject(result);
            if(parse != null && !parse.isEmpty()){
                JSONArray data = parse.getJSONArray(DyjInterfaceConstants.DATA);
                if(data != null && data.size() > 0){
                    for (Object object : data){
                        JSONObject inventoryInfo = (JSONObject) object;
                        InventoryBlurSearchRespPojo inventoryBlurSearchRespPojo = new InventoryBlurSearchRespPojo();
                        inventoryBlurSearchRespPojo.setPartNumber(inventoryInfo.getString("型号"));
                        inventoryBlurSearchRespPojo.setBrand(inventoryInfo.getString("厂家"));
                        inventoryBlurSearchRespPojo.setCount(inventoryInfo.getLongValue("可占货数量"));
                        inventoryBlurSearchRespPojo.setBatch(inventoryInfo.getString("批号"));
                        inventoryBlurSearchRespPojo.setPackages(inventoryInfo.getString("封装"));
                        inventoryBlurSearchRespPojo.setPrice(null);
                        inventoryBlurSearchRespPojo.setInventoryHome(null);
                        list.add(inventoryBlurSearchRespPojo);
                    }
                }
            }
        }

        return ServiceResult.ok(list, ServiceResult.SELECT_SUCCESS);
    }
}