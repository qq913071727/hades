package com.innovation.ic.cc.base.service.erp9_pvesiteuser;

import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.StaffsTopView;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UserClientOwnerTopView;

import java.util.List;

public interface StaffsService {
    //通过销售id获取销售相关数据
    List<StaffsTopView> selectList(List<UserClientOwnerTopView> ownerData);
}
