package com.innovation.ic.cc.base.service.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.model.cc.MyCompany;
import com.innovation.ic.cc.base.model.cc.User;
import com.innovation.ic.cc.base.pojo.MyCompanyPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.ErpMyCompanyNameVo;
import com.innovation.ic.cc.base.vo.MyCompanyVo;
import com.innovation.ic.cc.base.vo.RelationMyCompanyNameVo;
import com.innovation.ic.cc.base.vo.SearchCompanyVo;

import java.io.IOException;

public interface MyCompanyService {


    ServiceResult registerCompany(MyCompanyVo myCompanyVo) throws IOException;


    ServiceResult checkRegistrationCompany(MyCompanyVo myCompanyVo);

    ServiceResult<PageInfo<MyCompanyPojo>> findByMyCompany(SearchCompanyVo searchCompanyVo);


    void updateMyCompanyExternalId(ErpMyCompanyNameVo erpMyCompanyNameVo);

    MyCompany findByName(String Name);



    ServiceResult findByEpId(String epId);

    ServiceResult<MyCompanyPojo> getDetail(Integer id);

    ServiceResult deactivateMyCompany(Integer relationEnterpriseId) throws IOException;

    void addUserRelation(User userdata);

    void listenDeleteErpCompany(RelationMyCompanyNameVo relationMyCompanyNameVo);

    void listenAuditErpCompany(RelationMyCompanyNameVo relationMyCompanyNameVo);

    void relationMyCompanyNameVo(RelationMyCompanyNameVo relationMyCompanyNameVo);
}
