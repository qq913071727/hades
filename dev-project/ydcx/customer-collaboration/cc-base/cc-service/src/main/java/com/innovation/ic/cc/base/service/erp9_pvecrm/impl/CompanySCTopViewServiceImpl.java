package com.innovation.ic.cc.base.service.erp9_pvecrm.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.erp9_pvecrm.CompanySCTopViewMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvecrm.CompanySCTopView;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.erp9_pvecrm.CompanySCTopViewService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanySCTopViewServiceImpl  extends ServiceImpl<CompanySCTopViewMapper, CompanySCTopView> implements CompanySCTopViewService {


    @Override
    public ServiceResult<List<CompanySCTopView>> findByIds(List<String> ids) {
        ServiceResult<List<CompanySCTopView>> serviceResult = new ServiceResult<>();
        List<CompanySCTopView> companySCTopViews = this.baseMapper.selectBatchIds(ids);
        serviceResult.setResult(companySCTopViews);
        return serviceResult;
    }
}
