package com.innovation.ic.cc.base.service.erp9_pvecrm;

import com.innovation.ic.cc.base.model.cc.erp9_pvecrm.EnterprisesTopView;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;

import java.util.List;

/**
 * @author linuo
 * @desc SpecialsSCTopView的服务类接口
 * @time 2022年8月29日14:41:18
 */
public interface EnterprisesTopViewService {


    ServiceResult<Integer> countEnterprises();


    ServiceResult< List<EnterprisesTopView>> findByEnterprisesTopView(Integer offset, Integer rows);


}