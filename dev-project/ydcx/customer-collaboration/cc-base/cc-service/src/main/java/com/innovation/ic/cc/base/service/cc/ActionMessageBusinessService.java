package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.vo.ActionMessageBusinessVo;
import com.innovation.ic.cc.base.pojo.ActionMessageBusinessPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 动作消息业务表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-22
 */
public interface ActionMessageBusinessService {

    ServiceResult<PageInfo<ActionMessageBusinessPojo>> pageInfo( int page, int rows,ActionMessageBusinessVo vo);

    ServiceResult<List<ActionMessageBusinessPojo>> list(ActionMessageBusinessVo vo);

    ServiceResult<ActionMessageBusinessPojo> get(String id);

    void save(ActionMessageBusinessVo vo);

    void update(ActionMessageBusinessVo vo);

    void delete(String[] ids);
}