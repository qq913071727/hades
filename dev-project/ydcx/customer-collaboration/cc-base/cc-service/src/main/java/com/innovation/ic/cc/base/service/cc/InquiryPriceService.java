package com.innovation.ic.cc.base.service.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.model.cc.InquiryPrice;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceDetailsPojo;
import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceListPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPriceDelayedVo;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPricePageListVo;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPriceVo;
import com.innovation.ic.cc.base.vo.discussionPrice.DiscussionPriceCreatedVo;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价管理
 * @Date 2023/2/14 13:59
 **/
public interface InquiryPriceService {

    /**
     * 发布询价
     *
     * @param inquiryPriceVo
     * @param authenticationUser
     * @return
     */
    InquiryPrice releaseInquiryPrice(InquiryPriceVo inquiryPriceVo, AuthenticationUser authenticationUser);

    /**
     * 查询对应
     *
     * @param inquiryPricePageListVo
     * @return
     */
    PageInfo<InquiryPriceListPojo> pageList(InquiryPricePageListVo inquiryPricePageListVo, AuthenticationUser authenticationUser);

    /**
     * 询价详情
     *
     * @param id
     * @return
     */
    ServiceResult<InquiryPriceDetailsPojo> details(String id);

    /**
     * 返回所有询价单号
     *
     * @return
     */
    List<String> findAllSameDayNumber();

    /**
     * 发送询价
     *
     * @param ids   询价id
     * @param token
     */
    ServiceResult launchInquiry(List<String> ids, String token);

    /**
     * 取消议价
     *
     * @param id    议价记录id
     * @param token
     * @return ServiceResult
     */
    ServiceResult cancelDiscussionPrice(Integer id, String token);

    /**
     * 延时询价
     *
     * @param inquiryPriceDelayedVo
     * @param token
     * @return ServiceResult
     */
    ServiceResult delayedInquiryPrice(InquiryPriceDelayedVo inquiryPriceDelayedVo, String token);

    /**
     * 议价
     *
     * @param discussionPriceCreatedVo
     * @param token
     * @return
     */
    ServiceResult discussionPrice(DiscussionPriceCreatedVo discussionPriceCreatedVo, String token);

    /**
     * 处理延迟消息 修改状态
     *
     * @param msg
     */
    void handleMsg(String msg);
}
