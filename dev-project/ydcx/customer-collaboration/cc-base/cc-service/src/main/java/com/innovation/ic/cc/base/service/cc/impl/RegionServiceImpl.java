package com.innovation.ic.cc.base.service.cc.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.RegionMapper;
import com.innovation.ic.cc.base.mapper.cc.ShippingAddressMapper;
import com.innovation.ic.cc.base.model.cc.Region;
import com.innovation.ic.cc.base.model.cc.ShippingAddress;
import com.innovation.ic.cc.base.pojo.enums.ShippingAddressEnum;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.RegionService;
import com.innovation.ic.cc.base.service.cc.ShippingAddressService;
import com.innovation.ic.cc.base.vo.address.RegionQueryVo;
import com.innovation.ic.cc.base.vo.address.ShippingAddressVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {


    @Override
    public List<Region> findByIds(List<Integer> ids) {
        if(CollectionUtils.isEmpty(ids)){
            return null;
        }
        return this.baseMapper.selectBatchIds(ids);
    }

    @Override
    public ServiceResult<IPage<Region>> getRegionList(RegionQueryVo regionQueryVo) {
        ServiceResult<IPage<Region>> serviceResult = new ServiceResult<>();
        //PageHelper.startPage(regionQueryVo.getPageNo(), regionQueryVo.getPageSize());
        LambdaQueryWrapper<Region> regionLambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(null == regionQueryVo.getId()){
            regionLambdaQueryWrapper.eq(Region::getRegionType,2);
        }else{
            regionLambdaQueryWrapper.eq(Region::getParentId,regionQueryVo.getId());
        }
        IPage<Region> regionIPage = this.baseMapper.selectPage(new Page<>(regionQueryVo.getPageNo(), regionQueryVo.getPageSize()), regionLambdaQueryWrapper);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(regionIPage);
        return serviceResult;
    }
}
