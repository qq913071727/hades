package com.innovation.ic.cc.base.service.erp9_pvesiteuser.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.erp9_pvesiteuser.ParentUsersMapper;
import com.innovation.ic.cc.base.mapper.erp9_pvesiteuser.UsersMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UsersTopView;
import com.innovation.ic.cc.base.service.erp9_pvesiteuser.UsersService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class UsersServiceImpl extends ServiceImpl<UsersMapper, UsersTopView> implements UsersService {
    @Resource
    private UsersMapper usersMapper;
    @Resource
    private ParentUsersMapper parentUsersMapper;
    /**
     * 获取sql server中users数据（只存在主账号数据）
     * @return
     */
   public List<ParentUsersTopView> selectList(){
       QueryWrapper<ParentUsersTopView> parentUsersTopViewQueryWrapper = new QueryWrapper<>();
       parentUsersTopViewQueryWrapper.eq("WebSite",2);
       parentUsersTopViewQueryWrapper.eq("Type",2);
       List<ParentUsersTopView> userList = parentUsersMapper.selectList(parentUsersTopViewQueryWrapper);
       return userList;
   }

}