package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.SaleMapper;
import com.innovation.ic.cc.base.model.cc.Sale;
import com.innovation.ic.cc.base.pojo.UserMainPojo;
import com.innovation.ic.cc.base.service.cc.SaleService;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SaleServiceImpl extends ServiceImpl<SaleMapper, Sale> implements SaleService {

    //批量删除 销售数据
    public void batchDelete(List<String> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    //对本地mysql 销售相关数据进行批量添加
    public void insertAllNew(Map<String, Sale> saleMap) {
        List<Sale> sales = new ArrayList<>();
        for (Sale sale : saleMap.values()) {
            sales.add(sale);
        }
        //对本地mysql 销售相关数据进行批量添加
        baseMapper.insertBatchSomeColumn(sales);
    }

    //监听销售数据添加
    public void handleModelAddSaleMqMsg(UserMainPojo userMainPojo) {
        Sale sale = new Sale();
        sale.setId(userMainPojo.getId());//用户id
        sale.setEnterpriseId(userMainPojo.getEnterpriseId());//企业id
        sale.setSaleAccountId(userMainPojo.getSaleAccountId());//销售id
        sale.setUserName(userMainPojo.getSaleName());//销售姓名
        if (!StringUtils.isEmpty(userMainPojo.getSaleMobile())) {//手机号不为空
            sale.setPhone(userMainPojo.getSaleMobile());
        }
        if (!StringUtils.isEmpty(userMainPojo.getSaleEmail())) {//邮箱不为空
            sale.setEmail(userMainPojo.getSaleEmail());
        }
        baseMapper.insert(sale);
    }

    //修改企业中对应的销售人
    public void updateSaleId(UserMainPojo userMainPojo) {
        Sale sale = new Sale();
        sale.setSaleAccountId(userMainPojo.getSaleAccountId());
        sale.setUserName(userMainPojo.getUserName());
        if (!StringUtils.isEmpty(userMainPojo.getEmail())) {//不清楚erp那边销售邮箱是否为空
            sale.setEmail(userMainPojo.getEmail());
        }
        if (!StringUtils.isEmpty(userMainPojo.getPhone())) {//不清楚erp那边销售手机是否为空
            sale.setPhone(userMainPojo.getPhone());
        }
        UpdateWrapper<Sale> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", userMainPojo.getEnterpriseId());
        baseMapper.update(sale, updateWrapper);
    }

    //修改销售信息
    public void updateSaleData(UserMainPojo userMainPojo) {
        Sale sale = new Sale();
        sale.setUserName(userMainPojo.getUserName());
        if (!StringUtils.isEmpty(userMainPojo.getEmail())) {//不清楚erp那边销售邮箱是否为空
            sale.setEmail(userMainPojo.getEmail());
        }
        if (!StringUtils.isEmpty(userMainPojo.getPhone())) {//不清楚erp那边销售手机是否为空
            sale.setPhone(userMainPojo.getPhone());
        }
        UpdateWrapper<Sale> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("sale_account_id", userMainPojo.getSaleAccountId());
        baseMapper.update(sale, updateWrapper);
    }
}
