package com.innovation.ic.cc.base.service.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.pojo.SysAnnouncementPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.SysAnnouncementVo;
import java.util.List;

/**
 * 系统公告
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-20
 */
public interface SysAnnouncementService {

    ServiceResult<PageInfo<SysAnnouncementPojo>> pageInfo(int page, int rows, SysAnnouncementVo vo);

    ServiceResult<List<SysAnnouncementPojo>> list(SysAnnouncementVo vo);

    ServiceResult<SysAnnouncementPojo> get(String id) throws Exception;

    void save(SysAnnouncementVo vo) throws Exception;

    void update(SysAnnouncementVo vo) throws Exception;

    void delete(String[] ids);

    ServiceResult<SysAnnouncementPojo> getAnnouncementInfo() throws Exception;
}