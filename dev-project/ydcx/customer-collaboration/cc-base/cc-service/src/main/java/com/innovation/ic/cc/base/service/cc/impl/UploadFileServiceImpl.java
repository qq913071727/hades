package com.innovation.ic.cc.base.service.cc.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.manager.SftpChannelManager;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.mapper.cc.UploadFileMapper;
import com.innovation.ic.cc.base.model.cc.UploadFile;
import com.innovation.ic.cc.base.pojo.enums.UploadFileEnum;
import com.innovation.ic.cc.base.pojo.enums.UploadFileStatusEnum;
import com.innovation.ic.cc.base.pojo.enums.UploadFileTypeEnum;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.UploadFileService;
import com.innovation.ic.cc.base.value.FileParamConfig;
import com.innovation.ic.cc.base.vo.FileVo;
import com.innovation.ic.cc.base.vo.RelationMyCompanyNameVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author B1B
 */
@Service
@Slf4j
public class UploadFileServiceImpl extends ServiceImpl<UploadFileMapper, UploadFile> implements UploadFileService {

    @Resource
    private FileParamConfig fileParamConfig;




    @Resource
    private SftpChannelManager sftpChannelManager;

    @Override
    public void updateRelationId(List<String> relationPictureIds, UploadFileEnum uploadFileEnum, Integer relationId) {
        if (CollectionUtils.isEmpty(relationPictureIds) || uploadFileEnum == null) {
            return;
        }
        this.baseMapper.updateRelationId(relationPictureIds, relationId.toString(),uploadFileEnum.getCode());
    }

    /**
     * 上传文件
     *
     * @param bytes
     * @param fileName
     * @param suffix
     * @return
     */
    @Override
    public ServiceResult fileUpload(byte[] bytes, String fileName, String suffix, String creatorID){
        ServiceResult serviceResult = new ServiceResult();
        //生成唯一文件名
        String onlyFileName = IdUtil.simpleUUID() + "." + suffix;
        //保存数据库
        UploadFile uploadFile = new UploadFile();
        uploadFile.setFileName(fileName);
        uploadFile.setSuffix(suffix);
        uploadFile.setOnlyFileName(onlyFileName);
        uploadFile.setType(UploadFileTypeEnum.IMAGE.getCode());
        uploadFile.setCreatorId(creatorID);
        uploadFile.setCreateDate(new Date());
        uploadFile.setStatus(UploadFileStatusEnum.DEFAULT.getCode());
        uploadFile.setFullPath(fileParamConfig.getImageExhibitionUrl() + onlyFileName);
        //上传文件
        sftpChannelManager.upload(fileParamConfig.getFileUrl(), bytes, onlyFileName);
        this.baseMapper.insert(uploadFile);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("上传成功");
        serviceResult.setResult(uploadFile.getId());
        return serviceResult;
    }

    @Override
    public ServiceResult<List<UploadFile>> findByRelationId(UploadFileEnum uploadFileEnum, String relationId) {
        ServiceResult<List<UploadFile>> serviceResult = new ServiceResult<>();
        if (!StringUtils.validateParameter(relationId) || uploadFileEnum == null) {
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("relation_id", relationId);
        param.put("model", uploadFileEnum.getCode());
        List<UploadFile> uploadFiles = this.baseMapper.selectByMap(param);
        serviceResult.setResult(uploadFiles);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServiceResult<List<UploadFile>> findByRelationId(UploadFileEnum uploadFileEnum, String relationId, UploadFileStatusEnum uploadFileStatusEnum) {
        ServiceResult<List<UploadFile>> serviceResult = new ServiceResult<>();
        if (!StringUtils.validateParameter(relationId) || uploadFileEnum == null) {
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("relation_id", relationId);
        param.put("model", uploadFileEnum.getCode());
        param.put("status", uploadFileStatusEnum.getCode());
        List<UploadFile> uploadFiles = this.baseMapper.selectByMap(param);
        serviceResult.setResult(uploadFiles);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public void saveRelationMyCompanyBatch(List<FileVo> list, RelationMyCompanyNameVo relationMyCompanyNameVo) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        List<UploadFile> paramList = new ArrayList<>();
        for (FileVo fileVo : list) {
            String fileName = fileVo.getFileName();
            UploadFile uploadFile = new UploadFile();
            uploadFile.setFileName(fileName);
            uploadFile.setSuffix(fileName.substring(fileName.lastIndexOf(".") + 1));
            uploadFile.setCreatorId(relationMyCompanyNameVo.getCreator());
            uploadFile.setCreateDate(new Date());
            uploadFile.setFullPath(fileVo.getUrl());
            uploadFile.setRelationId(relationMyCompanyNameVo.getMyCompanyId());
            uploadFile.setModel(UploadFileEnum.MY_COMPANY.getCode());
            paramList.add(uploadFile);
        }
        this.baseMapper.insertBatchSomeColumn(paramList);
    }

    @Override
    public void saveUploadFile(List<UploadFile> files) {
        if (CollectionUtils.isEmpty(files)) {
            return;
        }
        saveBatch(files);
    }

    @Override
    public ServiceResult<List<UploadFile>> findByIds(List<String> ids) {
        ServiceResult<List<UploadFile>> serviceResult = new ServiceResult<>();
        if (CollectionUtils.isEmpty(ids)) {
            return serviceResult;
        }
        List<UploadFile> uploadFiles = this.baseMapper.selectBatchIds(ids);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(uploadFiles);
        return serviceResult;
    }

    @Override
    public void updateRelationIdStatus(UploadFileEnum uploadFileEnum, String relationId, UploadFileStatusEnum uploadFileStatusEnum) {
        if (uploadFileEnum == null) {
            return;
        }
        if (StringUtils.isEmpty(relationId)) {
            return;
        }
        if (uploadFileStatusEnum == null) {
            return;
        }
        UpdateWrapper<UploadFile> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("relation_id", relationId);
        updateWrapper.eq("model", uploadFileEnum.getCode());
        updateWrapper.set("status", uploadFileStatusEnum.getCode());
        update(updateWrapper);
    }

    @Override
    public ServiceResult pictureUploadToModel(byte[] bytes, String fileName, String suffix, String creatorID, String model, String relationId) {
        ServiceResult serviceResult = new ServiceResult();
        //生成唯一文件名
        String onlyFileName = IdUtil.simpleUUID() + "." + suffix;
        //保存数据库
        UploadFile uploadFile = new UploadFile();
        uploadFile.setFileName(fileName);
        uploadFile.setSuffix(suffix);
        uploadFile.setOnlyFileName(onlyFileName);
        uploadFile.setType(UploadFileTypeEnum.IMAGE.getCode());
        uploadFile.setCreatorId(creatorID);
        uploadFile.setCreateDate(new Date());
        uploadFile.setStatus(UploadFileStatusEnum.DEFAULT.getCode());
        uploadFile.setRelationId(relationId);
        uploadFile.setModel(model);
        uploadFile.setFullPath(fileParamConfig.getFileUrl() + File.separator + onlyFileName);
        //上传文件
        //serviceHelper.getSftpChannelManager().upload(fileParamConfig.getFileUrl(), bytes, onlyFileName);
        this.baseMapper.insert(uploadFile);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("上传成功");
        serviceResult.setResult(uploadFile.getId());
        return serviceResult;
    }


}
