package com.innovation.ic.cc.base.service.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.model.cc.ActionMessage;
import com.innovation.ic.cc.base.pojo.ActionMessgaePojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;

import java.util.List;

public interface ActionMessageService {


    /**
     * @Description: 分页
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/816:44
     */
    ServiceResult<PageInfo<ActionMessgaePojo>> page(int pageNo, int pageSize, String id);

    /**
     * @Description: 处理队列消息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/816:56
     */
    ServiceResult handleActionMessage(ActionMessage actionMessage, List<String> userIds);


    /**
     * @param userId
     * @Description: 全部已读
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/616:26
     */
    void isReadAll(String userId);

    /**
     * @Description: 返回当前用户的未读消息总数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/1417:11
     */
    ServiceResult<Integer> unReadTotal(String userId);
}
