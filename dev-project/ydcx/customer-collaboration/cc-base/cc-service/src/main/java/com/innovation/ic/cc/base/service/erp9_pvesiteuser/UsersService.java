package com.innovation.ic.cc.base.service.erp9_pvesiteuser;

import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.ParentUsersTopView;

import java.util.List;

public interface UsersService {
    /**
     * 获取sql server中users数据
     * @return
     */
    List<ParentUsersTopView> selectList();
}
