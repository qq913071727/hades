package com.innovation.ic.cc.base.service.cc;


import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;


/**
 * @desc   SmsInfo的服务类接口
 * @author swq
 * @time   2023年2月7日9:22:37
 */
public interface SmsInfoService {

    /**
     * 发送短信验证码
     * @param type 短信验证码类型,1: 登录
     * @param mobile 手机号
     * @return 返回发送结果
     */
    ServiceResult<Boolean> send(String type, String mobile);

    /**
     * 通过ERP发送短信验证码
     * @param mobile 手机号
     * @return 返回发送结果
     */
    ServiceResult<JSONObject> sendSmsCodeByErp(String mobile);
}