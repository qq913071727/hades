package com.innovation.ic.cc.base.service.erp9_pvesiteuser.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.erp9_pvesiteuser.StaffsMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.StaffsTopView;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UserClientOwnerTopView;
import com.innovation.ic.cc.base.service.erp9_pvesiteuser.StaffsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
@Service
@Transactional
public class StaffsServiceImpl extends ServiceImpl<StaffsMapper, StaffsTopView> implements StaffsService {

    //通过销售id获取销售相关数据
    public List<StaffsTopView> selectList(List<UserClientOwnerTopView> ownerData) {
        List<String> ownerIds = new ArrayList<>();
        for (UserClientOwnerTopView ownerDatum : ownerData) {
            ownerIds.add(ownerDatum.getOwnerId());
        }
        QueryWrapper<StaffsTopView> staffsTopViewQueryWrapper = new QueryWrapper<>();
        staffsTopViewQueryWrapper.in("AdminID", ownerIds);
        List<StaffsTopView> list = baseMapper.selectList(staffsTopViewQueryWrapper);
        return list;
    }
}
