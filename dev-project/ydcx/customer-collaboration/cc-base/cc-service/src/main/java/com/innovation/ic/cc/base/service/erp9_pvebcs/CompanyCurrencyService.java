package com.innovation.ic.cc.base.service.erp9_pvebcs;

import com.innovation.ic.cc.base.model.cc.erp9_pvebcs.CompanyCurrency;

import java.util.List;

public interface CompanyCurrencyService {
    List<CompanyCurrency> findCompanyCurrency();

}
