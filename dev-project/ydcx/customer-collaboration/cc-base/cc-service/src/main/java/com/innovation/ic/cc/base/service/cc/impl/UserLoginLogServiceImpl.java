package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.cc.UserLoginLogMapper;
import com.innovation.ic.cc.base.model.cc.UserLoginLog;
import com.innovation.ic.cc.base.service.cc.UserLoginLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Date;

@Service
@Transactional
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements UserLoginLogService {
    @Resource
    private UserLoginLogMapper userLoginLogMapper;

    /**
     * 将账号信息存入登录日志表中
     * @param username 账号
     * @param ipAddress 用户ip
     * @param loginType 用户登录方式
     * @return 返回结果
     */
    @Override
    public void addUserLog(String username, String ipAddress, Integer loginType){
        UserLoginLog userLoginLog = new UserLoginLog();
        // 账号
        userLoginLog.setUsername(username);
        // 客户端ip
        userLoginLog.setRemoteAddress(ipAddress);
        // 创建时间
        userLoginLog.setCreateTime(new Date());
        // 登录方式
        userLoginLog.setLoginType(loginType);
        userLoginLogMapper.insert(userLoginLog);
    }
}