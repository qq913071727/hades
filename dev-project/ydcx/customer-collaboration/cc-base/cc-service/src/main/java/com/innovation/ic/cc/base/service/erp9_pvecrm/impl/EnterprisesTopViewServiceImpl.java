package com.innovation.ic.cc.base.service.erp9_pvecrm.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.erp9_pvecrm.EnterprisesTopViewMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvecrm.EnterprisesTopView;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.erp9_pvecrm.EnterprisesTopViewService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 获取erp公司信息
 */
@Service
@Transactional
public class EnterprisesTopViewServiceImpl extends ServiceImpl<EnterprisesTopViewMapper, EnterprisesTopView> implements EnterprisesTopViewService {

    @Override
    public ServiceResult<Integer> countEnterprises() {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        Integer result = this.getBaseMapper().countEnterprises();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }

    @Override
    public ServiceResult<List<EnterprisesTopView>> findByEnterprisesTopView(Integer offset, Integer rows) {
        ServiceResult< List<EnterprisesTopView>> serviceResult = new ServiceResult<>();
        if(offset == null || rows ==null){
            return serviceResult;
        }
        List<EnterprisesTopView> result = this.getBaseMapper().findByEnterprisesTopView(offset, rows);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }
}