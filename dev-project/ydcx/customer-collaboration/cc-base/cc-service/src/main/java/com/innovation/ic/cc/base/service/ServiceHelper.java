package com.innovation.ic.cc.base.service;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.manager.MongodbManager;
import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.SftpChannelManager;
import com.innovation.ic.cc.base.handler.cc.MenuHandler;
import com.innovation.ic.cc.base.mapper.cc.DepartmentMapper;
import com.innovation.ic.cc.base.mapper.cc.UserManagementMapper;
import com.innovation.ic.cc.base.mapper.cc.UserMapper;
import com.innovation.ic.cc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.cc.base.value.ErpInterfaceAddressConfig;
import com.innovation.ic.cc.base.value.FileParamConfig;
import com.innovation.ic.cc.base.value.LoginConfig;
import com.innovation.ic.cc.base.value.UserConfig;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Getter
@Component
public class ServiceHelper {

    @Resource
    private LoginConfig loginConfig;

    @Resource
    private UserManagementMapper userManagementMapper;

    @Resource
    private RedisManager redisManager;

    @Resource
    private RabbitMqManager rabbitMqManager;

    @Resource
    private SftpChannelManager sftpChannelManager;

    @Autowired
    private MongodbManager mongodbManager;

    @Resource
    private UserConfig userConfig;

    @Resource
    private UserMapper userMapper;

    @Resource
    private DepartmentMapper departmentMapper;

    @Resource
    private ErpInterfaceAddressConfig erpInterfaceAddressConfig;

    @Resource
    private MenuHandler menuHandler;

    @Resource
    private FileParamConfig fileParamConfig;

    /**
     * 包装发送到rabbitmq的消息内容
     *
     * @param messageName 操作名字
     * @param messageBody 数据
     * @return 返回处理后的mq消息内容
     */
    public JSONObject packageSendRabbitMqMsgData(JSONObject messageBody, String messageName) {
        JSONObject result = new JSONObject();
        result.put(RabbitMqConstants.MESSAGE_NAME, messageName);
        result.put(RabbitMqConstants.MESSAGE_BODY, messageBody);
        return result;
    }
}
