package com.innovation.ic.cc.base.service.cc;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.login.BindThirdAccountVo;
import com.innovation.ic.cc.base.vo.login.SmsLoginVo;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @desc   登录的服务类接口
 * @author linuo
 * @time   2022年8月8日15:26:40
 */
public interface LoginService {
    /**
     * 通过短信验证码登录
     * @param smsLoginVo 短信验证码登录参数
     * @return 返回登录结果
     */
    ServiceResult<JSONObject> loginBySms(SmsLoginVo smsLoginVo) throws Exception;

    /**
     * 获取支付宝登录授权地址
     * @param redirectUrl 回调地址
     * @return 返回支付宝登录地址
     */
    ServiceResult<String> getAlipayAuthUrl(String redirectUrl) throws UnsupportedEncodingException;

    /**
     * 支付宝登录
     * @param authCode 临时授权码
     * @return 返回登录结果及登录信息
     */
    ServiceResult<JSONObject> loginByAlipay(String authCode);

    /**
     * 获取微信登录授权地址
     * @return 返回微信登录授权地址
     */
    ServiceResult<String> getWechatAuthUrl(String redirectUrl) throws UnsupportedEncodingException;

    /**
     * 获取QQ登录授权地址
     * @param redirectUrl 回调地址
     * @return 返回QQ登录授权地址
     */
    ServiceResult<String> getQQAuthUrl(String redirectUrl) throws UnsupportedEncodingException;

    /**
     * 微信登录
     * @param authCode 临时授权码
     * @return 返回登录结果及登录信息
     */
    ServiceResult<JSONObject> loginByWechat(String authCode);

    /**
     * QQ登录
     * @param authCode 临时授权码
     * @return 返回登录结果及登录信息
     */
    ServiceResult<JSONObject> loginByQQ(String authCode);

    /**
     * 绑定第三方账户
     * @param bindThirdAccountVo 绑定第三方账号的Vo类
     * @return 返回绑定结果
     */
    ServiceResult<JSONObject> bindThirdAccount(BindThirdAccountVo bindThirdAccountVo) throws IOException;

    /**
     * 解绑第三方账户
     * @param type 解绑账号类型(0 微信、1 QQ、3 支付宝)
     * @param userId 用户id
     * @return 返回解绑结果
     */
    ServiceResult<JSONObject> unBindThirdAccount(String type, String userId);
}