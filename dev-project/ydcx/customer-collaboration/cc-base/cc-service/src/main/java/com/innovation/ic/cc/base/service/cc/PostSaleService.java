package com.innovation.ic.cc.base.service.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.model.cc.PostSale;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.PostSaleVo;

public interface PostSaleService {

    //将post_sale表的数据导入到redis
    void initPostSaleData();

    /**
     * 分页展示售后管理数据
     *
     * @param postSaleVo
     * @return
     */
    ServiceResult<PageInfo<PostSale>> pagePostSale(PostSaleVo postSaleVo);
}
