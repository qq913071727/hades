package com.innovation.ic.cc.base.service.cc;


import com.innovation.ic.cc.base.model.cc.MenuTree;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;

/**
 * MenuTree的服务类接口
 */
public interface MenuTreeService {
    /**
     * 先删除旧的，再添加新的MenuTree对象
     * @param menuTree
     * @return
     */
    ServiceResult<MenuTree> saveOrUpdateMenuTree(MenuTree menuTree);

    /**
     * 根据userId，查找MenuTree对象
     * @param userId
     * @return
     */
    ServiceResult<MenuTree> findByUserId(String userId);
}
