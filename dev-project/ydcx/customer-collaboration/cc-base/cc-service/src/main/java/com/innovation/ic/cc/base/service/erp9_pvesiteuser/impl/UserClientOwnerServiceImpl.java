package com.innovation.ic.cc.base.service.erp9_pvesiteuser.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.erp9_pvesiteuser.UserClientOwnerMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UserClientOwnerTopView;
import com.innovation.ic.cc.base.service.erp9_pvesiteuser.UserClientOwnerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class UserClientOwnerServiceImpl extends ServiceImpl<UserClientOwnerMapper, UserClientOwnerTopView> implements UserClientOwnerService {
    /**
     * 获取用户关联销售id集合
     * @return
     */
    public List<UserClientOwnerTopView> selectList(List<String> ids){
        List<UserClientOwnerTopView> data = baseMapper.selectBatchIds(ids);
        return data;
    }
}
