package com.innovation.ic.cc.base.service.cc.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cc.base.mapper.cc.MyCompanyCurrencyMapper;
import com.innovation.ic.cc.base.model.cc.MyCompanyCurrency;
import com.innovation.ic.cc.base.service.cc.MyCompanyCurrencyService;
import com.innovation.ic.cc.base.vo.CompanyCurrencyVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Slf4j
public class MyCompanyCurrencyServiceImpl extends ServiceImpl<MyCompanyCurrencyMapper, MyCompanyCurrency> implements MyCompanyCurrencyService {

    @Override
    public void saveBatch(List<MyCompanyCurrency> bindMyCompanyCurrencys) {
        if (CollectionUtils.isEmpty(bindMyCompanyCurrencys)) {
            return;
        }
        this.baseMapper.insertBatchSomeColumn(bindMyCompanyCurrencys);
    }

    @Override
    public void truncate() {
        this.baseMapper.truncate();
    }

    @Override
    public void listenDeleteCompanyCurrency(CompanyCurrencyVo companyCurrencyVo) {

    }

    @Override
    public void listenUpdateCompanyCurrency(CompanyCurrencyVo companyCurrencyVo) {

    }

    @Override
    public void listenAddCompanyCurrency(CompanyCurrencyVo companyCurrencyVo) {

    }

    @Override
    public List<MyCompanyCurrency> findAll() {
        return null;
    }

    @Override
    public MyCompanyCurrency findByExternalId(String currency) {
        return null;
    }

    @Override
    public MyCompanyCurrency findById(String id) {
        return null;
    }

    @Override
    public MyCompanyCurrency findByEnName(String currency) {
        return null;
    }
}
