package com.innovation.ic.cc.base.service.cc;


import com.innovation.ic.cc.base.model.cc.DictionariesType;
import com.innovation.ic.cc.base.vo.DictionariesTypeVo;
import com.innovation.ic.cc.base.vo.ErpDictionariesVo;

import java.util.HashSet;
import java.util.List;

public interface DictionariesTypeService {

    void saveDictionariesType(DictionariesTypeVo dictionariesTypeVo);


    void listenAddDistrictType(ErpDictionariesVo erpDictionariesVo);

    void deleteBatchIds(HashSet<String> dictionariesTypeIdS);

    void insertBatchSomeColumn(List<DictionariesType> dtList);

    DictionariesType selectById(String id);
}
