package com.innovation.ic.cc.base.service.cc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.cc.base.model.cc.Region;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.address.RegionQueryVo;

import java.util.List;


public interface RegionService {


    List<Region> findByIds(List<Integer> ids);

    ServiceResult<IPage<Region>> getRegionList(RegionQueryVo regionQueryVo);
}
