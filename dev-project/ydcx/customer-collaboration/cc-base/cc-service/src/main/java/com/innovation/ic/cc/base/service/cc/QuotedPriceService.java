//package com.innovation.ic.cc.base.service.cc;
//
//import com.github.pagehelper.PageInfo;
//import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
//import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceRecordPojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceAndQuotedPriceRecordPojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPricePojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceRecordPojo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPriceAdoptionOrNotAcceptedVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPricePageListVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPriceRecordListVo;
//
//import java.util.List;
//
///**
// * @author zengqinglong
// * @desc 报价管理
// * @Date 2023/2/14 13:59
// **/
//public interface QuotedPriceService {
//    /**
//     * 分页查询列表
//     *
//     * @param quotedPricePageListVo
//     * @param authenticationUser
//     * @return
//     */
//    PageInfo<QuotedPricePojo> pageList(QuotedPricePageListVo quotedPricePageListVo, AuthenticationUser authenticationUser);
//
//    /**
//     * 询价管理: 我收到的报价: 一键采纳
//     *
//     * @param ids
//     */
//    void adopt(List<String> ids);
//
//    /**
//     * 报价详情
//     *
//     * @param id
//     * @return
//     */
//    QuotedPriceAndQuotedPriceRecordPojo details(String id);
//
//    /**
//     * 报价详情 : 过滤品牌型号
//     *
//     * @param quotedPriceRecordListVo
//     * @return
//     */
//    List<QuotedPriceRecordPojo> filterBrandModelList(QuotedPriceRecordListVo quotedPriceRecordListVo);
//
//    /**
//     * 报价详情 : 一键采纳/不接受
//     *
//     * @param quotedPriceAdoptionOrNotAcceptedVo
//     */
//    void adoptionOrNotAccepted(QuotedPriceAdoptionOrNotAcceptedVo quotedPriceAdoptionOrNotAcceptedVo);
//}
