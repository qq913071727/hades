package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.vo.ReceiptShippingVo;
import com.innovation.ic.cc.base.pojo.ReceiptShippingPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 收货单运单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
public interface ReceiptShippingService {

    ServiceResult<PageInfo<ReceiptShippingPojo>> pageInfo( int page, int rows,ReceiptShippingVo vo);

    ServiceResult<List<ReceiptShippingPojo>> list(ReceiptShippingVo vo);

    ServiceResult<ReceiptShippingPojo> get(String id);

    void save(ReceiptShippingVo vo);

    void update(ReceiptShippingVo vo);

    void delete(String[] ids);

    void confirm(List<Long> ids);
}