package com.innovation.ic.cc.base.service.cc;

import com.innovation.ic.cc.base.model.cc.Sale;
import com.innovation.ic.cc.base.pojo.UserMainPojo;

import java.util.List;
import java.util.Map;

public interface SaleService {
    //监听销售数据添加
    void handleModelAddSaleMqMsg(UserMainPojo userMainPojo);

    //批量删除
    void batchDelete(List<String> ids);

    //对本地mysql 销售相关数据进行批量添加
    void insertAllNew(Map<String, Sale> saleMap);

    //修改企业中对应的销售人
    void updateSaleId(UserMainPojo userMainPojo);

    //修改销售信息
    void updateSaleData(UserMainPojo userMainPojo);
}
