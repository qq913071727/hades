package com.innovation.ic.cc.base.service.cc.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.mapper.cc.ActionMessageMapper;
import com.innovation.ic.cc.base.mapper.cc.ActionMessageUserLinkMapper;
import com.innovation.ic.cc.base.model.cc.ActionMessage;
import com.innovation.ic.cc.base.model.cc.ActionMessageUserLink;
import com.innovation.ic.cc.base.pojo.ActionMessgaePojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ActionMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ActionMessageServiceImpl
 * @Description 动作消息实现类
 * @Date 2022/10/8
 * @Author myq
 */
@Slf4j
@Service
public class ActionMessageServiceImpl extends ServiceImpl<ActionMessageMapper, ActionMessage> implements ActionMessageService {

    @Resource
    private ActionMessageUserLinkMapper actionMessageUserLinkMapper;

    /**
     * @Description: 分页列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/615:54
     */
    @Override
    public ServiceResult<PageInfo<ActionMessgaePojo>> page(int pageNo, int pageSize, String id) {
        PageHelper.startPage(pageNo, pageSize);

        List<ActionMessgaePojo> actionMessageList = this.baseMapper.pageInfo(id);

        return ServiceResult.ok(new PageInfo<>(actionMessageList), "动作消息分页数据OK");
    }

    /**
     * @Description: 新增动作消息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/616:04
     */
    @Override
    public ServiceResult handleActionMessage(ActionMessage actionMessage, List<String> userIds) {
        this.baseMapper.insert(actionMessage);

        if (CollectionUtils.isEmpty(userIds)){
            throw new RuntimeException("用户列表为空");
        }

        List<ActionMessageUserLink> list = new ArrayList<>();
        userIds.forEach(e -> {
            ActionMessageUserLink link = new ActionMessageUserLink();
            link.setActionMessageId(actionMessage.getId().intValue());
            link.setUserId(e);
            link.setIsRead(0);
            list.add(link);
        });
        //批量新增数据
        actionMessageUserLinkMapper.insertBatchSomeColumn(list);
        return ServiceResult.ok("新增消息ok");
    }

    /**
     * @param userId
     * @Description: 全部已读
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/616:27
     */
    @Override
    public void isReadAll(String userId) {
        LambdaQueryWrapper<ActionMessageUserLink> linkLambdaQueryWrapper = new LambdaQueryWrapper<>();
        linkLambdaQueryWrapper.eq(ActionMessageUserLink::getUserId, userId);

        ActionMessageUserLink link = new ActionMessageUserLink();
        link.setIsRead(1);
        actionMessageUserLinkMapper.update(link, linkLambdaQueryWrapper);
    }

    /**
     * @Description: 返回未读消息的总数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/1417:11
     */
    @Override
    public ServiceResult<Integer> unReadTotal(String userId) {
        LambdaQueryWrapper<ActionMessageUserLink> linkLambdaQueryWrapper = new LambdaQueryWrapper<>();
        linkLambdaQueryWrapper.eq(ActionMessageUserLink::getUserId,userId);
        linkLambdaQueryWrapper.eq(ActionMessageUserLink::getIsRead,0);

        List<ActionMessageUserLink> actionMessageUserLinks = actionMessageUserLinkMapper.selectList(linkLambdaQueryWrapper);
        return ServiceResult.ok(actionMessageUserLinks.size(),"ok");
    }
}