package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.MyCompanyCurrency;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MyCompanyCurrencyMapper extends EasyBaseMapper<MyCompanyCurrency> {


    void truncate();

    MyCompanyCurrency findByExternalId(@Param("externalId") String externalId);
}
