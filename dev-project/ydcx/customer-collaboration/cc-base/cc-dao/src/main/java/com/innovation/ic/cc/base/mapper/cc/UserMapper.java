package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.User;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.ParentUsersTopView;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UserMapper extends EasyBaseMapper<User> {
    /**
     * 通过id修改日期时间和状态
     *
     * @param time   日期时间
     * @param status 账号状态
     * @param ids    账号ids
     */
    void updateStatus(@Param("time") String time, @Param("status") Integer status, @Param("ids") String[] ids);

    /**
     * 把list中sql server中的数据 同步的到mysql user表中
     *
     * @param userList
     * @return
     */
    Integer insertUserList(@Param("userList") List<ParentUsersTopView> userList);

    /**
     * 根据手机号查询账号
     * @param phoneNumber 手机号
     * @return 返回查询结果
     */
    List<String> selectAccountByPhoneNumber(@Param("phoneNumber") String phoneNumber);
}