package com.innovation.ic.cc.base.mapper.cc;


import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.QuotedPrice;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 报价表。与询价对应 Mapper 接口
 * </p>
 *
 * @author zengqinglong
 * @since 2023-02-23
 */
@Mapper
public interface QuotedPriceMapper extends EasyBaseMapper<QuotedPrice> {

}
