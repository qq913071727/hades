package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.Region;
import com.innovation.ic.cc.base.model.cc.ShippingAddress;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionMapper extends EasyBaseMapper<Region> {
    
}
