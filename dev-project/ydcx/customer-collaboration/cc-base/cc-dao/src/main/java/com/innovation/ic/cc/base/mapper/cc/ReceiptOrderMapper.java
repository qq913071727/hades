package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.model.cc.ReceiptOrder;
import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.pojo.ReceiptOrderPojo;
import com.innovation.ic.cc.base.vo.ReceiptOrderVo;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 订单收货单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@Mapper
public interface ReceiptOrderMapper extends EasyBaseMapper<ReceiptOrder> {

    /**
     * @param vo  * @return List<ReceiptOrderPojo>
     * @Description: 多表关联查询
     * @Author: myq
     * @Date: 2023/5/18 10:45
     */
    List<ReceiptOrderPojo> pageInfo(ReceiptOrderVo vo);


    /***
     * @param vo  * @return List<ReceiptOrderPojo>
     * @Description: 多表in查询
     * @Author: myq
     * @Date: 2023/5/18 10:45
     */
    List<ReceiptOrderPojo> pages(ReceiptOrderVo vo);


}
