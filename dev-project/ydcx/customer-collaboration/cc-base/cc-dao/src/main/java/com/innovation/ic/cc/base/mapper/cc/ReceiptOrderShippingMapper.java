package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.model.cc.ReceiptOrderShipping;
import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.vo.ReceiptOrderShippingVo;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 收货单订单和收货单运单关联表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@Mapper
public interface ReceiptOrderShippingMapper extends EasyBaseMapper<ReceiptOrderShipping> {

    List<ReceiptOrderShipping> pageInfo(ReceiptOrderShippingVo vo);


}
