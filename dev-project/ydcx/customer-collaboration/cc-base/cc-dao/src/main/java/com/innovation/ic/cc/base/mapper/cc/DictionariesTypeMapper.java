package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.DictionariesType;
import org.springframework.stereotype.Repository;

/***
 * 字典类型
 */
@Repository
public interface DictionariesTypeMapper extends EasyBaseMapper<DictionariesType> {

}