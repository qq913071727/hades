package com.innovation.ic.cc.base.mapper.erp9_pvebcs;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvebcs.CompanyCurrency;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CompanyCurrencyMapper extends EasyBaseMapper<CompanyCurrency> {

    List<CompanyCurrency> findCompanyCurrency();
}
