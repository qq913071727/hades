package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.UploadFile;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 上传文件
 */
@Repository
public interface UploadFileMapper extends EasyBaseMapper<UploadFile> {

    void updateRelationId(@Param("ids") List<String> ids, @Param("relationId") String relationId, @Param("model") String model);
}