package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.ErpMyCompanyName;
import com.innovation.ic.cc.base.vo.SearchErpMyCompanyNameVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 我的公司,已经注册过的
 */
@Repository
public interface ErpMyCompanyNameMapper extends EasyBaseMapper<ErpMyCompanyName> {

    List<ErpMyCompanyName> findByLikeCompanyName(SearchErpMyCompanyNameVo searchErpMyCompanyName);

    Integer findByCompanyName(@Param("name") String name);

    void trunCateTable();
}