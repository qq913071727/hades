package com.innovation.ic.cc.base.mapper.erp9_pvecrm;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvecrm.CompanySCTopView;
import org.springframework.stereotype.Repository;

/**
 * @desc   SpecialsSCTopView表的mapper类
 * @author linuo
 * @time   2022年8月29日14:43:29
 */
@Repository
public interface CompanySCTopViewMapper extends EasyBaseMapper<CompanySCTopView> {

}