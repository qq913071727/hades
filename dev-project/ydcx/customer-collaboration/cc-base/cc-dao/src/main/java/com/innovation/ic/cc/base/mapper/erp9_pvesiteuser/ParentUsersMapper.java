package com.innovation.ic.cc.base.mapper.erp9_pvesiteuser;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.ParentUsersTopView;

public interface ParentUsersMapper extends EasyBaseMapper<ParentUsersTopView> {
}
