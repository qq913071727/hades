package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.UserLoginLog;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginLogMapper extends EasyBaseMapper<UserLoginLog> {
}
