package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.SysAnnouncement;
import com.innovation.ic.cc.base.vo.SysAnnouncementVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 系统公告
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-20
 */
@Mapper
public interface SysAnnouncementMapper extends EasyBaseMapper<SysAnnouncement> {

    List<SysAnnouncement> pageInfo(SysAnnouncementVo vo);
}
