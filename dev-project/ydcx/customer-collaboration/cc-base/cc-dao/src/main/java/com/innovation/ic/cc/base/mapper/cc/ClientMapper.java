package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.Client;
import org.springframework.stereotype.Repository;

/**
 * @desc   Client表的mapper类
 * @author linuo
 * @time   2022年8月11日15:27:35
 */
@Repository
public interface ClientMapper extends EasyBaseMapper<Client> {

}