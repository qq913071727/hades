package com.innovation.ic.cc.base.mapper.erp9_pvebcs;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvebcs.ErpDictionaries;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author erp字典
 */
@Repository
public interface ErpDictionariesMapper extends EasyBaseMapper<ErpDictionaries> {


    List<ErpDictionaries>  findErpDictionaries(@Param("dCode") String dCode);
}
