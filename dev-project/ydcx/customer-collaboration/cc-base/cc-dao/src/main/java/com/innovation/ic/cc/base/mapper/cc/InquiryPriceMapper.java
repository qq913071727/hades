package com.innovation.ic.cc.base.mapper.cc;


import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.InquiryPrice;
import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceAndInquiryPriceRecordBO;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPricePageListVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 询价表。一条记录表示一次询价 Mapper 接口
 * </p>
 *
 * @author zengqinglong
 * @since 2023-04-26
 */
@Repository
public interface InquiryPriceMapper extends EasyBaseMapper<InquiryPrice> {
    /**
     * 分页获取数据
     *
     * @param params
     * @return
     */
    List<InquiryPriceAndInquiryPriceRecordBO> pageList(@Param("params") InquiryPricePageListVo params);

    /**
     * 返回所有询价单号
     *
     * @param startDate
     * @param endDate
     * @return
     */
    List<String> findAllSameDayNumber(@Param("startDate") String startDate, @Param("endDate") String endDate);

}
