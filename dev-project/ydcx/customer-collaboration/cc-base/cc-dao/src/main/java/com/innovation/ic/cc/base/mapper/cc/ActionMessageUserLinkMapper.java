package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.ActionMessageUserLink;
import org.apache.ibatis.annotations.Mapper;


/**
 * 用户动作消息关系表
 *
 * @author myq
 * @since 1.0.0 2023-01-06
 */
@Mapper
public interface ActionMessageUserLinkMapper extends EasyBaseMapper<ActionMessageUserLink> {

}
