package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.Dictionaries;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 字典
 */
@Repository
public interface DictionariesMapper extends EasyBaseMapper<Dictionaries> {

    List<Dictionaries> findByDcode(@Param("codes") List<String> paramList);
}