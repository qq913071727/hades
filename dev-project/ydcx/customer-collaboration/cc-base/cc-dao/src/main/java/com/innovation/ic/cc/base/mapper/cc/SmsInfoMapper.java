package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.SmsInfo;
import org.springframework.stereotype.Repository;



/**
 * @desc   SmsInfo的mapper
 * @author swq
 * @time   2023年2月7日9:24:01
 */
@Repository
public interface SmsInfoMapper extends EasyBaseMapper<SmsInfo> {

}