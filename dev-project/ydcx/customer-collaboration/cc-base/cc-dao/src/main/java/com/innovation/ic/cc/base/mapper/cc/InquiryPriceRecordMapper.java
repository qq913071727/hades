package com.innovation.ic.cc.base.mapper.cc;


import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.InquiryPriceRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 询价记录表。一次询价可以有多条询价记录 Mapper 接口
 * </p>
 *
 * @author zengqinglong
 * @since 2023-04-26
 */
@Repository
public interface InquiryPriceRecordMapper extends EasyBaseMapper<InquiryPriceRecord> {
    /**
     * 批量插入
     *
     * @param list
     */
    void insertBatch(@Param("list") List<InquiryPriceRecord> list);

    /**
     * 根据inquiryPriceId 批量更改状态
     *
     * @param inquiryPriceId
     * @param status
     * @param updateStats
     */
    int updateStatusByInquiryPriceId(@Param("inquiryPriceId") Integer inquiryPriceId,
                                      @Param("status")int status ,
                                      @Param("updateStats")int updateStats);
}
