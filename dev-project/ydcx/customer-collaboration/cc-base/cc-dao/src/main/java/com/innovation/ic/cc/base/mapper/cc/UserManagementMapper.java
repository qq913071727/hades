package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.UserManagement;
import org.springframework.stereotype.Repository;

@Repository
public interface UserManagementMapper extends EasyBaseMapper<UserManagement> {
}
