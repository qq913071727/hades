package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.ActionMessage;
import com.innovation.ic.cc.base.pojo.ActionMessgaePojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionMessageMapper extends EasyBaseMapper<ActionMessage> {


    List<ActionMessgaePojo> pageInfo(String toUserId);
}
