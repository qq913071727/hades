package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.model.cc.ReceiptShipping;
import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.vo.ReceiptShippingVo;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 收货单运单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@Mapper
public interface ReceiptShippingMapper extends EasyBaseMapper<ReceiptShipping> {

    List<ReceiptShipping> pageInfo(ReceiptShippingVo vo);
}
