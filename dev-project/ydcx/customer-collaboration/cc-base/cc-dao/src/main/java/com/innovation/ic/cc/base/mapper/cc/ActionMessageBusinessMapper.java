package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.ActionMessageBusiness;
import com.innovation.ic.cc.base.vo.ActionMessageBusinessVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 动作消息业务表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-22
 */
@Mapper
public interface ActionMessageBusinessMapper extends EasyBaseMapper<ActionMessageBusiness> {

    List<ActionMessageBusiness> pageInfo(ActionMessageBusinessVo vo);
}
