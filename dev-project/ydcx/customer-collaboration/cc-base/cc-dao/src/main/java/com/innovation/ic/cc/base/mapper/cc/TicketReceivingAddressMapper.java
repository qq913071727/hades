package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.ShippingAddress;
import com.innovation.ic.cc.base.model.cc.TicketReceivingAddress;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketReceivingAddressMapper extends EasyBaseMapper<TicketReceivingAddress> {
    
}
