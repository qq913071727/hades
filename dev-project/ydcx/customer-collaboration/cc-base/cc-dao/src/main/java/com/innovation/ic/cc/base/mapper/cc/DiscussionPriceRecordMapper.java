package com.innovation.ic.cc.base.mapper.cc;


import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.DiscussionPriceRecord;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 议价记录表。一次报价记录对应一个议价记录 Mapper 接口
 * </p>
 *
 * @author zengqinglong
 * @since 2023-04-26
 */
@Repository
public interface DiscussionPriceRecordMapper extends EasyBaseMapper<DiscussionPriceRecord> {

}
