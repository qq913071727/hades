package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.MyCompanyEnterprise;
import com.innovation.ic.cc.base.vo.MyCompanyVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 我的公司关联
 */
@Repository
public interface MyCompanyEnterpriseMapper extends EasyBaseMapper<MyCompanyEnterprise> {

    Integer checkRegistrationCompany(MyCompanyVo myCompanyVo);

    List<String> findByExternalId(@Param("externalId") String externalId);

    Integer containCompanyNameCount(MyCompanyVo myCompanyVo);
}