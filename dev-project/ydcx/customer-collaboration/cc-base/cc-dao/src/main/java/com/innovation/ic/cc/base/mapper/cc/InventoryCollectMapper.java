package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.InventoryCollect;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryCollectMapper extends EasyBaseMapper<InventoryCollect> {

}