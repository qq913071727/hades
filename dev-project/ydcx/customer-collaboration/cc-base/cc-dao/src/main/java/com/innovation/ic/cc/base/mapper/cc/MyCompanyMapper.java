package com.innovation.ic.cc.base.mapper.cc;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.MyCompany;
import com.innovation.ic.cc.base.pojo.MyCompanyPojo;
import com.innovation.ic.cc.base.vo.RelationMyCompanyNameVo;
import com.innovation.ic.cc.base.vo.SearchCompanyVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 上传文件
 */
@Repository
public interface MyCompanyMapper extends EasyBaseMapper<MyCompany> {

    void insertRetuenId(MyCompany myCompany);

    MyCompany findByName(@Param("name") String name);

    List<MyCompanyPojo> findByMyCompanyPage(SearchCompanyVo searchCompanyVo);

    Integer checkRegistrationCompanyId(RelationMyCompanyNameVo relationMyCompanyNameVo);

    MyCompany findByEnterpriseId(@Param("enterpriseId") String enterpriseId);

    List<MyCompany> findByMyCompanyList(SearchCompanyVo searchCompanyVo);

    /**
     * 根据creator_id查找我的公司名称列表
     * @param epId
     * @return
     */
    List<String> findByEpId(@Param("epId") String epId);
}