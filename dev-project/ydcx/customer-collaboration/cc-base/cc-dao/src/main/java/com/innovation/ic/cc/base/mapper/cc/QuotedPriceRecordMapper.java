package com.innovation.ic.cc.base.mapper.cc;


import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.QuotedPriceRecord;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 报价记录表。一次询价询价记录对应一个报价记录,议价也对应一条数据 Mapper 接口
 * </p>
 *
 * @author zengqinglong
 * @since 2023-04-26
 */
@Repository
public interface QuotedPriceRecordMapper extends EasyBaseMapper<QuotedPriceRecord> {

}
