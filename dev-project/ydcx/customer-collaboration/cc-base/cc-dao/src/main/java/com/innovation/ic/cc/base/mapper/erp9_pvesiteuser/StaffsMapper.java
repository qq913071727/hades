package com.innovation.ic.cc.base.mapper.erp9_pvesiteuser;

import com.innovation.ic.cc.base.mapper.EasyBaseMapper;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.StaffsTopView;

public interface StaffsMapper extends EasyBaseMapper<StaffsTopView> {
}
