package com.innovation.ic.cc.base.pojo.variable.quotedPrice;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 询价单
 * @Date 2023/2/24 13:57
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QuotedPricePojo", description = "询价单")
public class QuotedPricePojo {

    /**
     * 报价主键
     */
    private String id;

    /**
     * 询价id
     */
    private String inquiryPriceId;

    /**
     * 询价单号
     */
    private String inquiryPriceNumber;

    /**
     * 我的公司id，对应my_company表的id
     */
    private Integer myCompanyId;

    /**
     * 我的公司名称，对应my_company表的name
     */
    private String myCompanyName;

    /**
     * 币种。1:人民币，2:美元
     */
    private Integer currency;

    private String currencyDesc;

    /**
     * 截止时间
     */
    private Date deadline;

    private String deadlineTimestamp;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    private Integer inquiryType;

    private String inquiryTypeDesc;

    /**
     * 交货地。1:大陆，2;香港
     */
    private Integer deliveryPlace;

    private String deliveryPlaceDesc;

    /**
     * 发票类型。1:增值税普通发票（纸质）-13%，2:增值税专用发票（纸质）-13.01%，3:普通发票（纸质）-0%
     */
    private Integer invoiceType;

    private String invoiceTypeDesc;

    /**
     * 备注
     */
    private String note;

    /**
     * 型号列表，用空格分割
     */
    private String modelList;

    /**
     * 询价日期
     */
    private Date datetime;

    private String dateTimestamp;
    /**
     * BOM单号
     */
    private String bomNumber;

    /**
     * 询价人id，对应user表的id
     */
    private String userId;

    /**
     * 询价人真实姓名，对应user表的real_name
     */
    private String realName;

    /**
     * 状态。1:待采纳，2:部分采纳，3:超时未采纳，4:无效报价,5:已采纳
     */
    private Integer status;

    private String statusDesc;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最近修改时间
     */
    private Date updateTime;
}
