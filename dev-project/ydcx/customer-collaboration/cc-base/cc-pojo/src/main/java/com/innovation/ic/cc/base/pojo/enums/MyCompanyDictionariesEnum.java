package com.innovation.ic.cc.base.pojo.enums;

/**
 * 我的公司下拉枚举项
 */
public enum MyCompanyDictionariesEnum {
    COUNTRY_AND_REGION("country_and_region","国别地区"),
    ;


    private String name;
    private String cName;


    public static String of(String cName) {
        for (MyCompanyDictionariesEnum c : MyCompanyDictionariesEnum.values()) {
            if (c.getcName().equals(cName)) {
                return c.getName();
            }
        }
        return null;
    }



    MyCompanyDictionariesEnum(String name,String cName) {
        this.name = name;
        this.cName = cName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }



}