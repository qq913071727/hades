package com.innovation.ic.cc.base.pojo.constant;

/**
 * @desc   库存收藏用到的常量
 * @author linuo
 * @time   2023年5月17日09:29:34
 */
public class InventoryCollectConstants {
    /** 型号字段 */
    public static final String PART_NUMBER_FIELD = "part_number";

    /** 品牌字段 */
    public static final String BRAND_FIELD = "brand";

    /** 数量字段 */
    public static final String COUNT_FIELD = "count";

    /** 批次字段 */
    public static final String BATCH_FIELD = "batch";

    /** 封装字段 */
    public static final String PACKAGE_FIELD = "package";

    /** 价格字段 */
    public static final String PRICE_FIELD = "price";

    /** 库存所在地字段 */
    public static final String INVENTORY_HOME_FIELD = "inventory_home";
}
