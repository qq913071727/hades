package com.innovation.ic.cc.base.pojo.constant;

/**
 * @desc   cc数据库中表的名称
 * @author linuo
 * @time   2022年8月25日16:52:26
 */
public class CcTableName {
    /** 库存表 */
    public static final String INVENTORY = "inventory";

    /** 用户表 */
    public static final String USER = "user";
}
