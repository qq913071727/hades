package com.innovation.ic.cc.base.pojo.enums;

/**
 * 是否默认地址
 */
public enum ShippingAddressEnum {
    DEFAULT(1, "默认地址"),
    NO_DEFAULT(0, "非默认地址");

    private Integer code;
    private String desc;

     ShippingAddressEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }



    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}