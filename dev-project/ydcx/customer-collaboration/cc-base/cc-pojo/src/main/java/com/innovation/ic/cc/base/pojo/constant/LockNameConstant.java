package com.innovation.ic.cc.base.pojo.constant;

/**
 * @author zengqinglong
 * @desc 锁 常量
 * @Date 2023/2/27 13:22
 **/
public class LockNameConstant {
    /**
     * 询价 锁
     */
    public static final String INQUIRY_PRICE_STATUS_LOCK = "INQUIRY_PRICE_STATUS_LOCK";
}
