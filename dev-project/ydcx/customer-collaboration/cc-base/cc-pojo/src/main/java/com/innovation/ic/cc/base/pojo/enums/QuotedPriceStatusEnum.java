package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 报价状态 1:待采纳，2:部分采纳，3:已采纳，4:超时未采纳,5:无效报价
 * @Date 2023/2/15 11:40
 **/
public enum QuotedPriceStatusEnum {
    UNKNOWN(-100, "未知"),
    TO_BE_ADOPTED(1, "待采纳"),
    PARTIAL_ADOPTION(2, "部分采纳"),
    ADOPTED(3, "已采纳"),
    TIMEOUT_NOT_ADOPTED(4, "超时未采纳"),
    INVALID_QUOTATION(5, "无效报价"),
    ;

    private int status;

    private String desc;

    QuotedPriceStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static QuotedPriceStatusEnum parse(Integer status) {
        for (QuotedPriceStatusEnum quotedPriceStatusEnum : QuotedPriceStatusEnum.values()) {
            if (quotedPriceStatusEnum.getStatus() == status) {
                return quotedPriceStatusEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (QuotedPriceStatusEnum value : QuotedPriceStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(14);
            map.put("desc", value.getDesc());
            map.put("status", value.getStatus());
            list.add(map);
        }
        return list;
    }
}
