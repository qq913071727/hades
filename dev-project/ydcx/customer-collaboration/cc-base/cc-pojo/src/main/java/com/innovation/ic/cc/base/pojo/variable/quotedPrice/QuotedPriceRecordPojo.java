package com.innovation.ic.cc.base.pojo.variable.quotedPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 报价记录
 * @Date 2023/2/24 15:04
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QuotedPriceRecordPojo", description = "报价记录")
public class QuotedPriceRecordPojo {
    /**
     * 报价记录主键
     */
    private Integer id;

    /**
     * 询价表id
     */
    private Integer inquiryPriceId;

    /**
     * 询价记录表id
     */
    private Integer inquiryPriceRecordId;

    /**
     * 询价记录id 或 议价记录id
     */
    private Integer recordId;

    /**
     * 型号
     */
    private String model;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 数量
     */
    private Integer amount;

    /**
     * 批次
     */
    private String batch;

    /**
     * 封装
     */
    private String encapsulation;

    /**
     * 目标价
     */
    private BigDecimal acceptPrice;

    /**
     * 交货期
     */
    private Date deliveryDate;

    /**
     * 截止时间
     */
    private Date deadline;

    /**
     * mpq
     */
    private Integer mpq;

    /**
     * 包装
     */
    private String packing;

    /**
     * 备注
     */
    private String note;

    /**
     * bom单匹配选择后不可议价 议价标志: 1:可以议价 0:不可议价
     */
    private Boolean discussionPriceFlag;

    /**
     * 报价
     */
    private BigDecimal quotedPrice;

    /**
     * 报价类型.1:询价后的报价,  2:议价后的报价
     */
    private Integer quotedType;

    /**
     * 币种 1:人民币 2:美元
     */
    private Integer currency;
    private String currencyDesc;

    /**
     * 交货地。1:大陆，2:香港
     */
    private Integer deliveryPlace;
    private String deliveryPlaceDesc;

    /**
     * erp 针对型号的报价id
     */
    private String erpSubQuotedId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最近修改时间
     */
    private Date updateTime;
}
