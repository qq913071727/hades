package com.innovation.ic.cc.base.pojo.constant.third_party_api;

/**
 * @desc   支付宝相关常量参数
 * @author linuo
 * @time   2022年8月15日14:31:08
 */
public class AlipayConstants {
    /** 支付宝接口地址 */
    public static final String URL = "https://openapi.alipay.com/gateway.do";

    /** 获取授权地址 */
    public static final String GET_AUTH_URL = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm";

    /** 接口权限值，目前只支持 auth_user 和 auth_base 两个值 */
    public static final String SCOPE_USER = "auth_user";

    /** 接口权限值，目前只支持 auth_user 和 auth_base 两个值 */
    public static final String SCOPE_BASE = "auth_base";

    /** 商户自定义参数，用户授权后，重定向到 redirect_uri 时会原样回传给商户。
     * 为防止 CSRF 攻击，建议开发者请求授权时传入 state 参数，该参数要做到既不可预测，又可以证明客户端和当前第三方网站的登录认证状态存在关联，并且不能有中文。
     * 说明：参数由用户自定义拼接，拼接完成后将参数进行 base64 转码。最后将参数拼接为 state=XXXXX 即可，最大长度 100 位。
     */
    public static final String STATE = "state";

    /** app_id字段 */
    public static final String APP_ID_FIELD = "app_id";

    /** redirect_uri字段 */
    public static final String REDIRECT_URI_FIELD = "redirect_uri";

    /** 支付宝用户的唯一 userId */
    public static final String USER_ID = "userId";

    /** 回调地址url字段 */
    public static final String REDIRECT_URL_FIELD = "redirectUrl";
}