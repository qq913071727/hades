package com.innovation.ic.cc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 调用API的返回对象
 * @param <T>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiResult<T>", description = "接口返回值对象")
public class ApiResult<T> implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Integer")
    private Integer code;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result = null;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    /**
     * 3001表示参数为空
     */
    @ApiModelProperty(value = "3001表示参数为空", dataType = "Integer")
    public static final Integer PARAMETER_EMPTY = 3001;

    /**
     * 3001表示参数重名
     */
    @ApiModelProperty(value = "3001表示参数重名", dataType = "Integer")
    public static final Integer PARAMETER_DUPLICATE = 3002;

    /**
     * 3002表示token不合法或已经过期，需要重新登录
     */
    @ApiModelProperty(value = "3003表示token不合法或已经过期，需要重新登录", dataType = "Integer")
    public static final Integer TOKEN_ILLEGAL_EXPIRE = 3003;

    /**
     * 3004表示手机号已经重复，需要重新输入
     */
    @ApiModelProperty(value = "3004表示手机号已经重复，需要重新输入", dataType = "Integer")
    public static final Integer PHONE_EXPIRE = 3004;

    /**
     * 3005表示邮箱已经重复，需要重新输入
     */
    @ApiModelProperty(value = "3005表示邮箱已经重复，需要重新输入", dataType = "Integer")
    public static final Integer EMAIL_EXPIRE = 3005;

    /**
     * 3006表示账号已经重复，需要重新输入
     */
    @ApiModelProperty(value = "3006表示账号已经重复，需要重新输入", dataType = "Integer")
    public static final Integer USER_EXPIRE = 3006;

    /**
     * 3008表示手机获取验证码已经超过五次
     */
    @ApiModelProperty(value = "3008表示手机获取验证码已经超过五次", dataType = "Integer")
    public static final Integer PHONE_CODE_EXPIRE = 3008;

    public ApiResult(Integer code, String message, T result, Boolean success) {
        this.code = code;
        this.message = message;
        this.result = result;
        this.success = success;
    }

    public ApiResult(){}

    /**
     * @return
     * @description 成功
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:45
     */
    public static <T> ApiResult<T> ok(T data, String message) {
        return new ApiResult<T>(200, message, data, true);
    }

    /**
     * @return
     * @description 成功
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:45
     */
    public static <T> ApiResult<T> ok(String message) {
        return new ApiResult<T>(200, message, null, true);
    }

    /**
     * @return
     * @description 失败
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:45
     */
    public static <T> ApiResult<T> error(Integer code,String message) {
        return new ApiResult<T>(code, message, null, false);
    }
}