package com.innovation.ic.cc.base.pojo.constant;

/**
 * 日常使用的常量值
 */
public class Constants {

    /** id */
    public static final String ID = "id";

    /** 账号 */
    public static final String USERNAME = "username";

    /** 结果 */
    public static final String RESULT = "result";

    /** 主键id字段 */
    public static final String ID_FIELD = "ID";

    /** 数据字段 */
    public static final String DATA_FIELD = "data";

    /** 数量字段 */
    public static final String TOTAL_FIELD = "total";

    /** 数量 */
    public static final String COUNT = "count";

    /** 日期格式 */
    public static String DATE_PATTERN = "yyyy-MM-dd";

    /** 供应商id字段 */
    public static final String ENTERPRISE_ID_FIELD = "enterprise_id";

    /** 供应商id */
    public static final String ENTERPRISE_ID = "enterpriseId";

    /** 创建人id字段 */
    public static final String CREATE_USER_ID_FIELD = "create_user_id";

    /** 状态 */
    public static final String STATUS_ = "status";

    /** 消息体 */
    public static final String MSG_ = "msg";

    /** 生产运行环境 */
    public static final String PROD_RUN_ENVIRONMENT = "prod";

    /** 用户id */
    public static final String USER_ID = "userid";

    /** 状态码字段 */
    public static final String CODE_FIELD = "code";

    /** 阶梯价格id字段 */
    public static final String LADDER_PRICE_ID_FIELD = "ladder_price_id";

    /** 起订量字段 */
    public static final String MQ_FIELD = "mq";

    /** 国外标识 */
    public static final String ABROAD = "国外";

    /** HTTPS */
    public static final String HTTPS = "https://";
}
