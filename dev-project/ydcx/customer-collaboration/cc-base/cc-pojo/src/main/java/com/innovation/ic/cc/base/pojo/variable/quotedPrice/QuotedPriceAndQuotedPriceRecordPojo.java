package com.innovation.ic.cc.base.pojo.variable.quotedPrice;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 报价单详情
 * @Date 2023/2/24 15:02
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QuotedPriceAndQuotedPriceRecordPojo", description = "报价单详情")
public class QuotedPriceAndQuotedPriceRecordPojo {
    /**
     * 询价单
     */
    private QuotedPricePojo quotedPricePojo;
    /**
     * 询价单列表
     */
    private List<QuotedPriceRecordPojo> quotedPriceRecordPojoList;
}
