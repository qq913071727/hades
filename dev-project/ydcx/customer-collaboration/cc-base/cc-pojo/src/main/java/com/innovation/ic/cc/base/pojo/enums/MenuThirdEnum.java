package com.innovation.ic.cc.base.pojo.enums;

import com.innovation.ic.cc.base.pojo.constant.model.MenuType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 第三个菜单 枚举
 * @Date 2023/2/9 13:42
 **/
public enum MenuThirdEnum {

    // 系统管理: 基本信息: 修改密码 9-1-1
    SYSTEM_ESSENTIAL_INFORMATION_CHANGE_PASSWORD("9-1", "9-1-1", "修改密码", MenuType.BUTTON, "", ""),
    // 系统管理: 基本信息: 修改绑定 9-1-2
    SYSTEM_ESSENTIAL_INFORMATION_MODIFY_BINDING("9-1", "9-1-2", "修改绑定", MenuType.BUTTON, "", ""),
    // 系统管理: 基本信息: 退出登录 9-1-3
    SYSTEM_ESSENTIAL_INFORMATION_LOG_OUT("9-1", "9-1-3", "退出登录", MenuType.BUTTON, "", ""),

    // 系统管理: 账号管理: 新增 9-3-1
    SYSTEM_ACCOUNT_MANAGEMENT_ADD("9-3", "9-3-1", "新增", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 修改 9-3-2
    SYSTEM_ACCOUNT_MANAGEMENT_CHANGE("9-3", "9-3-2", "修改", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 重置密码 9-3-3
    SYSTEM_ACCOUNT_MANAGEMENT_RESET_PASSWORD("9-3", "9-3-3", "重置密码", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 人员管理 9-3-4
    SYSTEM_ACCOUNT_MANAGEMENT_PERSONNEL_MANAGEMENT("9-3", "9-3-4", "人员管理", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 启用 9-3-5
    SYSTEM_ACCOUNT_MANAGEMENT_ENABLE("9-3", "9-3-5", "启用", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 停用 9-3-6
    SYSTEM_ACCOUNT_MANAGEMENT_DEACTIVATE("9-3", "9-3-6", "停用", MenuType.BUTTON, "", ""),

    // 系统管理: 角色管理: 新增角色 9-4-1
    SYSTEM_ROLE_MANAGEMENT_NEW_ROLE("9-4", "9-4-1", "新增角色", MenuType.BUTTON, "", ""),

    // 系统管理: 我的公司管理: 新增公司 9-5-1
    SYSTEM_MY_COMPANY_MANAGEMENT_NEW_COMPANY("9-5", "9-5-1", "新增公司", MenuType.BUTTON, "", ""),

    // 系统管理: 地址管理: 新增收货地址 9-6-1
    SYSTEM_ADDRESS_MANAGEMENT_ADD_DELIVERY_ADDRESS("9-6", "9-6-1", "新增收货地址", MenuType.BUTTON, "", "");

    //父级菜单
    private String parentMenu;
    //id
    private String id;
    //名称
    private String name;
    //菜单标志
    private Integer menuType;
    //路由
    private String path;
    //图标
    private String icon;

    MenuThirdEnum(String parentMenu, String id, String name, Integer menuType, String path, String icon) {
        this.parentMenu = parentMenu;
        this.id = id;
        this.name = name;
        this.menuType = menuType;
        this.path = path;
        this.icon = icon;
    }

    public String getParentMenu() {
        return parentMenu;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }


    public static List<MenuThirdEnum> getThirdMenuList(String parentMenu) {
        List list = new ArrayList();
        for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.values()) {
            if (menuThirdEnum.getParentMenu().equals(parentMenu)) {
                list.add(menuThirdEnum);
            }
        }
        return list;
    }
}
