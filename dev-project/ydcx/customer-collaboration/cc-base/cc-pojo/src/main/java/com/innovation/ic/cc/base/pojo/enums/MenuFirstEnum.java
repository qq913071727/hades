package com.innovation.ic.cc.base.pojo.enums;

import com.innovation.ic.cc.base.pojo.constant.model.MenuType;

/**
 * @author zengqinglong
 * @desc 第一个菜单 枚举
 * @Date 2023/2/9 13:42
 **/
public enum MenuFirstEnum {
    // 首页 1
    HOME_PAGE("1", "首页", MenuType.MENU_ITEM, "/home/index", "icon-shouye"),
    //订单管理 2
    ORDER_MANAGEMENT("2", "订单管理", MenuType.MENU_ITEM, "/home/order", "icon-dingdan"),
    //询价管理 3
    INQUIRY_MANAGEMENT("3", "询价管理", MenuType.MENU_ITEM, "/home/publishEnquiry", "icon-inquiry"),
    //库存信息查询 4
    INVENTORY_INFORMATION_QUERY("4", "库存查询", MenuType.MENU_ITEM, "/home/inventory", "icon-kucunchaxun"),
    //我的收货单 5
    MY_RECEIPT("5", "我的收货单", MenuType.MENU_ITEM, "/home/myReceipt", "icon-wodeshouhuodan"),
    //售后管理 6
    AFTER_SALES_MANAGEMENT("6", "售后管理", MenuType.MENU_ITEM, "/home/replacement", "icon-shouhouguanli"),
    //财务管理 7
    FINANCIAL_MANAGEMENT("7", "财务管理", MenuType.MENU_ITEM, "/home/moneyManger", "icon-caiwuguanli"),
    //增值服务 8
    VALUE_ADDED_SERVICES("8", "增值服务", MenuType.MENU_ITEM, "/home/addService", "icon-zengzhifuwu"),
    //系统管理 9
    SYSTEM_MANAGEMENT("9", "系统管理", MenuType.MENU_ITEM, "/home/manageRole", "icon-xitongguanli"),
    //消息提醒 10
    MESSAGE_REMINDER("10", "系统管理", MenuType.MENU_ITEM, "/home/messagePrompt", "icon-xiaoxitixing");

    //id
    private String id;
    //名称
    private String name;
    //菜单标志
    private Integer menuType;
    //路由
    private String path;
    //图标
    private String icon;

    MenuFirstEnum(String id, String name, Integer menuType, String path, String icon) {
        this.id = id;
        this.name = name;
        this.menuType = menuType;
        this.path = path;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }

}
