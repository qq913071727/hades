package com.innovation.ic.cc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 人员管理存入redis pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserManagementRedisPojo", description = "人员管理存入redis pojo类")
public class UserManagementRedisPojo {

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String userName;

}
