package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 询价类型
 * @Date 2023/2/15 10:16
 **/
public enum InquiryTypeEnum {
    UNKNOWN(-100,"未知"),
    ACTUAL_INQUIRY(1, "实单询价"),
    GENERAL_ENQUIRY(2, "普通询价");

    private int type;

    private String desc;
    InquiryTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static InquiryTypeEnum parse(Integer type) {
        for (InquiryTypeEnum inquiryTypeEnum : InquiryTypeEnum.values()) {
            if (inquiryTypeEnum.getType() == type) {
                return inquiryTypeEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InquiryTypeEnum value : InquiryTypeEnum.values()) {
            map = new LinkedHashMap<String, Object>(8);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
