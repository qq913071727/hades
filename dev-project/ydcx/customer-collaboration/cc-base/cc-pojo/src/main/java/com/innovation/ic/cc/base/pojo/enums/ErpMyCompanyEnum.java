package com.innovation.ic.cc.base.pojo.enums;

public enum ErpMyCompanyEnum {

    NORMAL(200, "正常"),
    THE_BLACKLIST(400, "黑名单"),
    DISABLE(500, "禁用");

    private Integer code;
    private String desc;

    ErpMyCompanyEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
