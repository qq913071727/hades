package com.innovation.ic.cc.base.pojo.constant.third_party_api;

/**
 * @desc   短信发送平台相关常量参数
 * @author linuo
 * @time   2022年8月9日09:27:46
 */
public class SmsConstants {
    /** 用户名 */
    public static final String SMS_NAME = "sname";

    /** 密码 */
    public static final String SMS_PWD = "spwd";

    /** 默认参数 */
    public static final String SMS_SCORPID = "scorpid";

    /** 短信模板编号 */
    public static final String SMS_SPRDID = "sprdid";

    /** 接收短信的手机号 */
    public static final String SMS_MOBILE = "sdst";

    /** 短信内容 */
    public static final String SMS_MSG = "smsg";

    /** 短信验证码 */
    public static final String SMS_CODE = "smsCode";

    /** 短信发送状态 */
    public static final String SMS_SUBMIT_STATE = "CSubmitState";

    /** 短信发送状态 */
    public static final String STATE = "State";

    /** 短信验证码表name */
    public static final String SMS_PK_NAME = "SI";
}