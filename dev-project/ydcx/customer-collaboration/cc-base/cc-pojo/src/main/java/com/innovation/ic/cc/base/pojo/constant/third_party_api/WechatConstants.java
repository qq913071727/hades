package com.innovation.ic.cc.base.pojo.constant.third_party_api;

/**
 * @desc   微信相关常量参数
 * @author linuo
 * @time   2022年8月17日09:27:58
 */
public class WechatConstants {
    /** 获取授权URL地址 */
    public static final String GET_AUTH_URL = "https://open.weixin.qq.com/connect/qrconnect";

    /** 获取临时授权码URL地址 */
    public static final String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";

    /** 获取用户个人信息URL地址 */
    public static final String GET_USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo";

    /** app_id字段 */
    public static final String APP_ID_FIELD = "appid";

    /** redirect_uri字段 */
    public static final String REDIRECT_URI_FIELD = "redirect_uri";

    /** access_token字段 */
    public static final String ACCESS_TOKEN_FIELD = "access_token";

    /** access_token */
    public static final String ACCESS_TOKEN = "accessToken";

    /** openid字段 */
    public static final String OPEN_ID_FIELD = "openid";

    /** unionid字段 */
    public static final String UNION_ID_FIELD = "unionid";

    /** 回调地址url字段 */
    public static final String REDIRECT_URL_FIELD = "redirectUrl";
}