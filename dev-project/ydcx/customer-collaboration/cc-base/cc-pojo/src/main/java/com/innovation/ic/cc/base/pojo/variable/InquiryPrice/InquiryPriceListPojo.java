package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import lombok.Data;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价列表
 * @Date 2023/5/11 17:46
 **/
@Data
public class InquiryPriceListPojo {
    /**
     * 询价单
     */
    private InquiryPricePojo inquiryPricePojo;
    /**
     * 询价型号
     */
    private List<InquiryPriceRecordPojo> inquiryPriceRecordPojoList;
}
