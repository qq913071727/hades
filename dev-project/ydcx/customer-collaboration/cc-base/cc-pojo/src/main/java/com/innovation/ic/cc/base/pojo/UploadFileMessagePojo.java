package com.innovation.ic.cc.base.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *  发送注册公司消息pojo
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UploadFileMessagePojo {

    @ApiModelProperty(value = "文件名字", dataType = "String")
    private String FileName;

    @ApiModelProperty(value = "url", dataType = "String")
    private String Url;
}