package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 记录实体
 * @Date 2023/5/10 14:15
 **/
@Data
public class RecordPojo {
    /**
     * 记录id
     */
    private Integer id;
    /**
     * 记录类型 1:询价记录 2:报价记录 3:议价记录
     */
    private Integer recordType;
    /**
     * 询价表id
     */
    private Integer inquiryPriceId;

    /**
     * 询价记录表id
     */
    private Integer inquiryPriceRecordId;

    /**
     * 询价记录id 或 议价记录id
     */
    private Integer recordId;

    /**
     * 型号
     */
    private String model;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 数量
     */
    private Integer amount;

    /**
     * 批次
     */
    private String batch;

    /**
     * 封装
     */
    private String encapsulation;

    /**
     * 目标价
     */
    private BigDecimal acceptPrice;

    /**
     * 交货期
     */
    private Date deliveryDate;

    /**
     * 截止时间
     */
    private Date deadline;

    /**
     * mpq
     */
    private Integer mpq;

    /**
     * 包装
     */
    private String packing;

    /**
     * 状态。1:待询价，2:待报价，3:已报价，4:议价中，5:议价完成，6:已生成订单,7:议价失败, 8:超时未报价,9:无法报价, 10:无效报价,11:取消议价
     */
    private Integer status;

    private String statusDesc;

    /**
     * 备注
     */
    private String note;

    /**
     * bom单匹配选择后不可议价 议价标志: 1:可以议价 0:不可议价
     */
    private Boolean discussionPriceFlag;

    /**
     * 报价
     */
    private BigDecimal quotedPrice;

    /**
     * 报价类型.1:询价后的报价,  2:议价后的报价
     */
    private Integer quotedType;

    /**
     * 币种 1:人民币 2:美元
     */
    private Integer currency;
    private String currencyDesc;

    /**
     * 交货地。1:大陆，2:香港
     */
    private Integer deliveryPlace;
    private String deliveryPlaceDesc;

    /**
     * erp 针对型号的报价id
     */
    private String erpSubQuotedId;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    private Integer inquiryType;

    private String inquiryTypeDesc;

    /**
     * 发票名称
     */
    private String invoiceName;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最近修改时间
     */
    private Date updateTime;
}
