package com.innovation.ic.cc.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单收货单
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-05-17
 */
@ApiModel(value = "ReceiptOrderPojo", description = "订单收货单 响应类")
@Setter
@Getter
public class ReceiptOrderPojo {


    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "订单号")
    private String orderCode;

    @ApiModelProperty(value = "收货状态: 10待收货 20部分收货 30已收货")
    private Integer orderState;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderPrice;

    @ApiModelProperty(value = "币种：1USD 2RMD")
    private Integer orderCurrency;

    @ApiModelProperty(value = "订单交货方式")
    private Integer orderDelivery;

    @ApiModelProperty(value = "交货地")
    private String orderDeliveryAddress;

    @ApiModelProperty(value = "发票类型：1增值税专用发票")
    private Integer orderInvoiceType;

    @ApiModelProperty(value = "采购人ID")
    private String orderProcureUserId;

    @ApiModelProperty(value = "采购人名称")
    private String orderProcureUserName;

    @ApiModelProperty(value = "下单时间")
    private String orderTime;

    @ApiModelProperty(value = "")
    private String createTime;

    @ApiModelProperty(value = "")
    private String updateTime;

    @ApiModelProperty(value = "型号列表")
    private String modelList;

    public String getModelList(){
        List<String> collect = this.getReceiptOrderShippingPojos().stream().map(e -> e.getReceiptShippingPojo().getShippingModel()).collect(Collectors.toList());
        return String.join(",", collect);
    }

    public String getShippingCodeList() {
        List<String> collect = this.getReceiptOrderShippingPojos().stream().map(e -> e.getReceiptShippingPojo().getShippingCode()).collect(Collectors.toList());
        return String.join(",", collect);
    }

    @ApiModelProperty(value = "运单号列表")
    private String shippingCodeList;

    @ApiModelProperty(value = "收货单订单运单关系表")
    List<ReceiptOrderShippingPojo> receiptOrderShippingPojos;

}


