package com.innovation.ic.cc.base.pojo.enums;

/**
 * @desc   审核状态枚举类
 * @author linuo
 * @time   2022年8月29日16:27:36
 */
public enum AuditStatusEnum {
    // 待审批
    PENDING(100, 0),

    // 审批通过
    APPROVE(200, 1),
    
    // 审批否决
    APPROVAL_REFUSED(300, 2),
    ;

    private Integer erpStatusCode;
    private Integer scStatusCode;

    AuditStatusEnum(Integer erpStatusCode, Integer scStatusCode) {
        this.erpStatusCode = erpStatusCode;
        this.scStatusCode = scStatusCode;
    }

    public static Integer getErpStatusCode(Integer scStatusCode) {
        for (AuditStatusEnum c : AuditStatusEnum.values()) {
            if (c.getScStatusCode().equals(scStatusCode)) {
                return c.erpStatusCode;
            }
        }
        return null;
    }

    public static Integer getScStatusCode(Integer erpStatusCode) {
        for (AuditStatusEnum c :AuditStatusEnum.values()) {
            if (c.getErpStatusCode().equals(erpStatusCode)) {
                return c.scStatusCode;
            }
        }
        return null;
    }

    public Integer getErpStatusCode() {
        return erpStatusCode;
    }

    public void setErpStatusCode(Integer erpStatusCode) {
        this.erpStatusCode = erpStatusCode;
    }

    public Integer getScStatusCode() {
        return scStatusCode;
    }

    public void setScStatusCode(Integer scStatusCode) {
        this.scStatusCode = scStatusCode;
    }
}