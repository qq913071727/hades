package com.innovation.ic.cc.base.pojo.constant;

/**
 * @desc   调用大赢家接口时使用的常量类
 * @author linuo
 * @time   2023年5月15日14:04:22
 */
public class DyjInterfaceConstants {
    public static final String HOST = "host";

    public static final String USER_AGENT = "user-agent";

    public static final String KEY = "key";

    public static final String DATA = "data";

    /** 型号 */
    public static final String OBJ_PART_NO = "objpartno";
}