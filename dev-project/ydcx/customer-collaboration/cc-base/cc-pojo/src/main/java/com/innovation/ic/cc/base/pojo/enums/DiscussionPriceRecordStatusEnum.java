package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 议价状态 0:议价初始化（未发送成功erp就是） ,1:议价完成，2:议价中 3:议价失败,4:取消议价
 * @Date 2023/2/15 13:17
 **/
public enum DiscussionPriceRecordStatusEnum {
    UNKNOWN(-100, "未知"),
    DISCUSSION_PRICE_INIT(0,"议价初始化"),
    DISCUSSION_PRICE_COMPLETE(1, "议价完成"),
    DISCUSSION_PRICE_IN(2, "议价中"),
    DISCUSSION_PRICE_FAIL(3, "议价失败"),
    CANCEL_DISCUSSION_PRICE(4, "取消议价"),
    ;

    private int status;

    private String desc;

    DiscussionPriceRecordStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static DiscussionPriceRecordStatusEnum parse(Integer status) {
        for (DiscussionPriceRecordStatusEnum discussionPriceRecordStatusEnum : DiscussionPriceRecordStatusEnum.values()) {
            if (discussionPriceRecordStatusEnum.getStatus() == status) {
                return discussionPriceRecordStatusEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (DiscussionPriceRecordStatusEnum value : DiscussionPriceRecordStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(14);
            map.put("desc", value.getDesc());
            map.put("status", value.getStatus());
            list.add(map);
        }
        return list;
    }
}
