package com.innovation.ic.cc.base.pojo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyMessagePojo", description = "关联关系删除和新增")
public class MyCompanyMessagePojo implements Serializable {



    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String ID;
    @ApiModelProperty(value = "关联公司ID", dataType = "String")
    private String SubID;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;

    @ApiModelProperty(value = "附件", dataType = "List")
    private List<UploadFileMessagePojo> Files;

}
