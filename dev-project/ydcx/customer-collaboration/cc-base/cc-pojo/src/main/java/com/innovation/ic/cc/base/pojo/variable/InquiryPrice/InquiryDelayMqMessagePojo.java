package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 询价延迟mq消息实体
 * @Date 2023/4/28 14:58
 **/
@Data
public class InquiryDelayMqMessagePojo implements Serializable {
    /**
     * 询价id
     */
    private Integer id;

    /**
     * 当前状态 : 如果当前状态 和数据库状态不一致 以数据库为准，不一致认为当前消息无效
     */
    private Integer currentState;

    /**
     * 截止时间 : 询价截止时间可能会变 所以需要更改时间，需要重新设置延时
     */
    private Date deadline;
}
