package com.innovation.ic.cc.base.pojo.constant.model;

/**
 * 菜单项中应当排除的选项。不同的账号要排除不同的权限
 */
public class MenuSelected {

    /**
     * 已选择
     */
    public static final Integer YES = 1;

    /**
     * 未选择
     */
    public static final Integer NO = 0;

}
