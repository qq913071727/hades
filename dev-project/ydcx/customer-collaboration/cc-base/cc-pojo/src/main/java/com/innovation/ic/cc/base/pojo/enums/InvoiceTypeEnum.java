package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 发票类型
 * @Date 2023/2/15 10:23
 **/
public enum InvoiceTypeEnum {
    UNKNOWN(-100, "未知"),
    ORDINARY_VAT_INVOICE(1, "增值税普通发票"),
    VAT_SPECIAL_INVOICE(2, "增值税专用发票"),
    ORDINARY_INVOICE(3, "普通发票");

    private int type;

    private String desc;

    InvoiceTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static InvoiceTypeEnum parse(Integer type) {
        for (InvoiceTypeEnum invoiceTypeEnum : InvoiceTypeEnum.values()) {
            if (invoiceTypeEnum.getType() == type) {
                return invoiceTypeEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InvoiceTypeEnum value : InvoiceTypeEnum.values()) {
            map = new LinkedHashMap<String, Object>(8);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
