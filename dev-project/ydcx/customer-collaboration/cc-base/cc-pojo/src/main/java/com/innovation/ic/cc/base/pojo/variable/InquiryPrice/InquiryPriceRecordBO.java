package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 询价记录
 * @Date 2023/5/12 9:19
 **/
@Data
public class InquiryPriceRecordBO {
    /**
     * 询价记录主键
     */
    private Integer inquiryPriceRecordId;

    /**
     * 询价id
     */
    private Integer inquiryPriceId;

    /**
     * 型号
     */
    private String model;

    /**
     * 品牌
     */
    private String brand;


    /**
     * 数量
     */
    private Integer amount;

    /**
     * 批次
     */
    private String batch;

    /**
     * 封装
     */
    private String encapsulation;

    /**
     * 目标价
     */
    private BigDecimal acceptPrice;


    /**
     * 交货期
     */
    private Date deliveryDate;

    /**
     * 截止时间
     */
    private Date recordDeadline;

    /**
     * mpq
     */
    private Integer mpq;

    /**
     * 包装
     */
    private String packing;

    /**
     * 状态。
     */
    private Integer recordStatus;

    /**
     * 备注
     */
    private String recordNote;
    /**
     * bom单匹配选择后不可议价 议价标志: 1:可以议价 0:不可议价
     */
    private Boolean discussionPriceFlag;

    /**
     * erp 针对型号的询价id
     */
    private String erpSubInquiryId;

    /**
     * 创建时间
     */
    private Date recordCreateTime;

    /**
     * 最近修改时间
     */
    private Date recordUpdateTime;
}
