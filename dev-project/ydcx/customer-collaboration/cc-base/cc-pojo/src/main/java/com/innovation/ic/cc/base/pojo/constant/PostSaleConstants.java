package com.innovation.ic.cc.base.pojo.constant;

/**
 * 售后管理不同分支常量。
 */
public class PostSaleConstants {

    /**
     * 当前只有时间范围区间查询
     */
    public static final Integer TYPE_ONE= 1;

    /**
     * 五个条件查询都成立
     */
    public static final Integer TYPE_TWO = 2;

    /**
     * 四个条件查询条件成立（日期不成立）
     */
    public static final Integer TYPE = 3;
}
