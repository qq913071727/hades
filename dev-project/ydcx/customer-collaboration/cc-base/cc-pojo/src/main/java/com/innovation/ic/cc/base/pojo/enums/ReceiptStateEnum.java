package com.innovation.ic.cc.base.pojo.enums;

/**
 * @desc   短信验证状态枚举
 * @author linuo
 * @time   2022年8月8日13:45:12
 */

public enum ReceiptStateEnum {
    WAIT_RECEIPT(10, "待收货"),
    PART_RECEIPT(20, "部分收货"),
    RECEIPTED(30, "已收货");

    private Integer code;
    private String desc;

    ReceiptStateEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ReceiptStateEnum of(Integer code) {
        for (ReceiptStateEnum c : ReceiptStateEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (ReceiptStateEnum c : ReceiptStateEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}