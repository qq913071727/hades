package com.innovation.ic.cc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 子账号封装类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SubUsersPojo", description = "子账号封装类")
public class SubUsersPojo {

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "站点 1 供应商协同系统、2 芯达通系统", dataType = "Integer")
    private Integer webSite;

    @ApiModelProperty(value = "主账号的id，如果为空，则表示这是主账号", dataType = "String")
    private String fatherID;

//    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
//    private String enterpriseId;
//
//    @ApiModelProperty(value = "企业名称）", dataType = "String")
//    private String enterpriseName;

    @ApiModelProperty(value = "类型（1 卖家[供应商]） Seller Buyer", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "职位：1表示经理，2表示员工", dataType = "Integer")
    private String position;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;

    @ApiModelProperty(value = "创建日期", dataType = "String")
    private String createDate;

    @ApiModelProperty(value = "修改日期", dataType = "String")
    private String modifyDate;

    @ApiModelProperty(value = "状态（正常200 500停用）", dataType = "Integer")
    private Integer status;
}
