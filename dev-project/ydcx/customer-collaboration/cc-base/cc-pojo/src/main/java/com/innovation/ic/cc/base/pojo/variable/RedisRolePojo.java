package com.innovation.ic.cc.base.pojo.variable;

import com.innovation.ic.cc.base.pojo.variable.menu.Menu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * redis角色封装类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RedisRolePojo", description = "redis角色封装类")
public class RedisRolePojo {

    @ApiModelProperty(value = "主键", dataType = "ObjectId")
    private String _id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "是否可用。1表示可用，2表示停用", dataType = "Integer")
    private Integer available;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createTime;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    private Date updateTime;

    @ApiModelProperty(value = "菜单列表", dataType = "Date")
    private List<Menu> menuList;
}
