package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 询价方式
 * @Date 2023/4/26 16:17
 **/
public enum InquiryMethodEnum {
    UNKNOWN(-100,"未知"),
    STRIP_INQUIRY(1, "逐条询价"),
    BOM_INQUIRY(2, "BOM询价");

    private int type;

    private String desc;
    InquiryMethodEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static InquiryMethodEnum parse(Integer type) {
        for (InquiryMethodEnum inquiryMethodEnum : InquiryMethodEnum.values()) {
            if (inquiryMethodEnum.getType() == type) {
                return inquiryMethodEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InquiryMethodEnum value : InquiryMethodEnum.values()) {
            map = new LinkedHashMap<String, Object>(8);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
