package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 询价型号 对照表 0:全部 1:待询价 2:待报价 3:已报价 4:议价中 5:已生成订单 6:超时未报价 7:无效询价
 * @Date 2023/4/27 10:22
 **/
public enum InquiryPriceRecordCompareStatusEnum {
    UNKNOWN(-100, "未知"),
    ALL(0,"全部"),
    PENDING(1, "待询价"),
    TO_BE_QUOTATION(2, "待报价"),
    QUOTED(3, "已报价"),
    UNDER_NEGOTIATION(4, "议价中"),
    ORDER_GENERATED(5, "已生成订单"),
    NO_QUOTATION_OVERTIME(6, "超时未报价"),
    INVALID_INQUIRY(7,"无效询价");

    private int status;

    private String desc;

    InquiryPriceRecordCompareStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static InquiryPriceRecordCompareStatusEnum parse(Integer status) {
        for (InquiryPriceRecordCompareStatusEnum inquiryPriceRecordCompareStatusEnum : InquiryPriceRecordCompareStatusEnum.values()) {
            if (inquiryPriceRecordCompareStatusEnum.getStatus() == status) {
                return inquiryPriceRecordCompareStatusEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InquiryPriceRecordCompareStatusEnum value : InquiryPriceRecordCompareStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(14);
            map.put("desc", value.getDesc());
            map.put("status", value.getStatus());
            list.add(map);
        }
        return list;
    }
}
