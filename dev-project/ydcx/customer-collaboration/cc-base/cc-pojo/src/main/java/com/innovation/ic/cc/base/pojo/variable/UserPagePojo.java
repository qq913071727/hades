package com.innovation.ic.cc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 账号的pojo类
 * @Date 2022/12/7 16:34
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserPagePojo", description = "账号的pojo类")
public class UserPagePojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "角色名称", dataType = "String")
    private String roleNames;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;

    @ApiModelProperty(value = "职位：1表示经理，2表示员工", dataType = "Integer")
    private Integer position;

    @ApiModelProperty(value = "账号状态", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    private Date createDate;
}
