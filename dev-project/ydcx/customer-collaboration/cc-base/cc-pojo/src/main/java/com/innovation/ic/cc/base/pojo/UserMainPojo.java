package com.innovation.ic.cc.base.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 主账号pojp类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserMainPojo", description = "主账号pojp类")
public class UserMainPojo {

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "企业名称", dataType = "String")
    private String enterpriseName;

    @ApiModelProperty(value = "站点 1 供应商Sc,  2 客户Cc, 3 芯达通", dataType = "Integer")
    private Integer webSite;

    @ApiModelProperty(value = "类型 1 Seller 卖家(供应商)，2 Buyer 买家(客户)", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "销售id", dataType = "String")
    private String saleAccountId;

    @ApiModelProperty(value = "销售姓名", dataType = "String")
    private String saleName;

    @ApiModelProperty(value = "销售手机号", dataType = "String")
    private String saleMobile;

    @ApiModelProperty(value = "销售邮箱", dataType = "String")
    private String saleEmail;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;

    @ApiModelProperty(value = "人与公司关联id", dataType = "String")
    private String siteId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    @ApiModelProperty(value = "修改日期", dataType = "Date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date modifyDate;

    @ApiModelProperty(value = "判断主账号方式 1添加 3修改", dataType = "Integer")
    private Integer operationType;

    @ApiModelProperty(value = "状态（正常200 500停用）", dataType = "Integer")
    private Integer status;
}
