package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 交货地
 * @Date 2023/2/15 10:20
 **/
public enum DeliveryPlaceTypeEnum {
    UNKNOWN(-100,"未知"),
    MAINLAND(1, "大陆"),
    HK(2, "香港");

    private int type;

    private String desc;

    DeliveryPlaceTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static DeliveryPlaceTypeEnum parse(Integer type) {
        for (DeliveryPlaceTypeEnum deliveryPlaceTypeEnum : DeliveryPlaceTypeEnum.values()) {
            if (deliveryPlaceTypeEnum.getType() == type) {
                return deliveryPlaceTypeEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (DeliveryPlaceTypeEnum value : DeliveryPlaceTypeEnum.values()) {
            map = new LinkedHashMap<String, Object>(8);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
