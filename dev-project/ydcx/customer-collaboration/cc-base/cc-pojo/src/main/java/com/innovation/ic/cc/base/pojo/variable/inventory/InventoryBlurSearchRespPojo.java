package com.innovation.ic.cc.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @desc   库存模糊查询返回的pojo类
 * @author linuo
 * @time   2023年5月15日14:46:33
 **/
@Data
public class InventoryBlurSearchRespPojo implements Serializable {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Long")
    private Long count;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "价格", dataType = "BigDecimal")
    private BigDecimal price;

    @ApiModelProperty(value = "库存所在地(中国大陆、中国香港)", dataType = "String")
    private String inventoryHome;
}