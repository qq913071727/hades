package com.innovation.ic.cc.base.pojo.variable;


import com.github.pagehelper.Page;

import java.util.List;

/**
 * @author zengqinglong
 * @description:
 * @Date 2022/12/28 11:12:38
 */
public class PageNationPojo<T> {
    private final int DEFAULT_PAGE_SIZE = 5;

    private int pageNum;
    private int pageSize = DEFAULT_PAGE_SIZE;
    private long totalCount;
    private int totalPageCount;
    private List<T> list;


    public PageNationPojo(int pageNum, int pageSize, long totalCount, int totalPageCount, List<T> list) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.totalPageCount = totalPageCount;
        this.list = list;
    }

    public PageNationPojo() {
    }

    public static PageNationPojo fromPage(Page page) {
        return new PageNationPojo(page.getPageNum(), page.getPageSize(), page.getTotal(), page.getPages(), null);
    }

    public static PageNationPojo fromPage(Page page, List data) {
        return new PageNationPojo(page.getPageNum(), page.getPageSize(), page.getTotal(), page.getPages(), data);
    }

    public int getPageNum() {
        return pageNum;
    }

    public PageNationPojo setPageNum(int pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public PageNationPojo setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public PageNationPojo setTotalCount(long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public PageNationPojo setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
        return this;
    }

    public List<T> getList() {
        return list;
    }

    public PageNationPojo setList(List<T> list) {
        this.list = list;
        return this;
    }
}

