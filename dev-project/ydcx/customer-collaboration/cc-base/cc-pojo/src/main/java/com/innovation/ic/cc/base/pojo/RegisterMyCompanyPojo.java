package com.innovation.ic.cc.base.pojo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RegisterMyCompanyPojo", description = "发送注册公司消息")
public class RegisterMyCompanyPojo implements Serializable {

    private static final long serialVersionUID = 4228333579234098652L;

    @ApiModelProperty(value = "主账号id", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String Name;

    @ApiModelProperty(value = "国别地区", dataType = "String")
    private String District;

    @ApiModelProperty(value = "客户类型", dataType = "String")
    private String Type;

    @ApiModelProperty(value = "主要产品", dataType = "String")
    private String Products;

    @ApiModelProperty(value = "Creator", dataType = "String")
    private String Creator;


    @ApiModelProperty(value = "图片", dataType = "String")
    private List<UploadFileMessagePojo> Files;


}
