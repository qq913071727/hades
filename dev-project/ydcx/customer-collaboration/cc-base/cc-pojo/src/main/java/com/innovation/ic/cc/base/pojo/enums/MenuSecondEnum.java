package com.innovation.ic.cc.base.pojo.enums;

import com.innovation.ic.cc.base.pojo.constant.model.MenuType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 第二个菜单 枚举
 * @Date 2023/2/9 13:42
 **/
public enum MenuSecondEnum {
    // 订单管理: 我的订单 2-1
    ORDER_MY_ORDER("2", "2-1", "我的订单", MenuType.MENU_ITEM, "", ""),

    // 询价管理: 发布询价 3-1
    INQUIRY_PUBLISH("3", "3-1", "发布询价", MenuType.MENU_ITEM, "", ""),
    // 询价管理: 逐条询价单 3-2
    INQUIRY_ITEM_BY_INQUIRY("3", "3-2", "逐条询价单", MenuType.MENU_ITEM, "", ""),
    // 询价管理: BOM询价单 3-3
    INQUIRY_BARGAINING("3", "3-3", "我的议价", MenuType.MENU_ITEM, "", ""),

    // 财务管理: 对账单 7-1
    FINANCIAL_ACCOUNT_STATEMENT("7", "7-1", "对账单", MenuType.MENU_ITEM, "", ""),
    // 财务管理: 账户管理 7-2
    FINANCIAL_ACCOUNT_MANAGEMENT("7", "7-2", "账户管理", MenuType.MENU_ITEM, "", ""),
    // 财务管理: 发票管理 7-3
    FINANCIAL_CONTROL_OVER_INVOICES("7", "7-3", "发票管理", MenuType.MENU_ITEM, "", ""),
    // 财务管理: 合同管理 7-4
    FINANCIAL_CONTRACT_MANAGEMENT("7", "7-4", "合同管理", MenuType.MENU_ITEM, "", ""),


    // 增值服务: 型号替代 8-1
    VALUE_ADDED_SERVICES_MODEL_SUBSTITUTION("8", "8-1", "型号替代", MenuType.MENU_ITEM, "", ""),
    // 增值服务: 样品申请 8-2
    VALUE_ADDED_SERVICES_SAMPLE_APPLICATION("8", "8-2", "样品申请", MenuType.MENU_ITEM, "", ""),
    // 增值服务: 技术支持 8-3
    VALUE_ADDED_SERVICES_TECHNICAL_SUPPORT("8", "8-3", "技术支持", MenuType.MENU_ITEM, "", ""),
    // 增值服务: 方案查询 8-4
    VALUE_ADDED_SERVICES_SCHEME_QUERY("8", "8-4", "方案查询", MenuType.MENU_ITEM, "", ""),
    // 增值服务: 风险查询 8-5
    VALUE_ADDED_SERVICES_RISK_INQUIRY("8", "8-5", "风险查询", MenuType.MENU_ITEM, "", ""),
    // 增值服务: 仓储报关 8-6
    VALUE_ADDED_SERVICES_WAREHOUSING_CUSTOMS_DECLARATION("8", "8-6", "仓储报关", MenuType.MENU_ITEM, "", ""),
    // 增值服务: 检测服务 8-7
    VALUE_ADDED_SERVICES_DETECTION_SERVICE("8", "8-7", "检测服务", MenuType.MENU_ITEM, "", ""),
    // 增值服务: PCBA 8-8
    VALUE_ADDED_SERVICES_PCBA("8", "8-8", "PCBA", MenuType.MENU_ITEM, "", ""),

    // 系统管理: 基本信息 9-1
    SYSTEM_ESSENTIAL_INFORMATION("9", "9-1", "基本信息", MenuType.MENU_ITEM, "", ""),
    // 系统管理: 账号安全 9-2
    SYSTEM_ACCOUNT_SECURITY("9", "9-2", "账号安全", MenuType.MENU_ITEM, "", ""),
    // 系统管理: 账号管理 9-3
    SYSTEM_ACCOUNT_MANAGEMENT("9", "9-3", "账号管理", MenuType.MENU_ITEM, "", ""),
    // 系统管理: 角色管理 9-4
    SYSTEM_ROLE_MANAGEMENT("9", "9-4", "角色管理", MenuType.MENU_ITEM, "", ""),
    // 系统管理: 我的公司管理 9-5
    SYSTEM_MY_COMPANY_MANAGEMENT("9", "9-5", "我的公司管理", MenuType.MENU_ITEM, "", ""),
    // 系统管理: 地址管理 9-6
    SYSTEM_ADDRESS_MANAGEMENT("9", "9-6", "地址管理", MenuType.MENU_ITEM, "", ""),
    // 系统管理: 系统公告 9-7
    SYSTEM_ANNOUNCEMENT("9", "9-7", "系统公告", MenuType.MENU_ITEM, "", "");

    //父级菜单
    private String parentMenu;
    //id
    private String id;
    //名称
    private String name;
    //菜单标志
    private Integer menuType;
    //路由
    private String path;
    //图标
    private String icon;

    MenuSecondEnum(String parentMenu, String id, String name, Integer menuType, String path, String icon) {
        this.parentMenu = parentMenu;
        this.id = id;
        this.name = name;
        this.menuType = menuType;
        this.path = path;
        this.icon = icon;
    }

    public String getParentMenu() {
        return parentMenu;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }

    public static List<MenuSecondEnum> getSecondMenuList(String parentMenu) {
        List list = new ArrayList();
        for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.values()) {
            if (menuSecondEnum.getParentMenu().equals(parentMenu)) {
                list.add(menuSecondEnum);
            }
        }
        return list;
    }
}
