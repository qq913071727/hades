package com.innovation.ic.cc.base.pojo.constant;

/**
 * 账号常量
 */
public class UserConstants {

    /**
     * 账号标识
     */
    public static final String USER_TYPE = "userName";

    /**
     * 手机号标识
     */
    public static final String PHONE_TYPE = "phone";

    /**
     * 邮箱标识
     */
    public static final String EMAIL_TYPE = "email";

    /**
     * 主账号默认角色名称
     */
    public static final String ROLE_MAIN = "超级管理员";

    /**
     * 账号层级（经理）
     */
    public static final Integer POSITION_ONE = 1;

    /**
     * 账号层级(员工)
     */
    public static final Integer POSITION_TWO = 2;


    /**
     * 监听主账号方式 为添加
     */
    public static final Integer TYPE_ONE = 1;
}
