package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 报价型号状态 1:已报价，2:无法报价，3:无效报价 4:议价完成
 * @Date 2023/2/15 13:09
 **/
public enum QuotedPriceRecordStatusEnum {
    UNKNOWN(-100, "未知"),
    QUOTED(1, "已报价"),
    UNABLE_TO_QUOTE(2, "无法报价"),
    INVALID_QUOTE(3, "无效报价"),
    DISCUSSION_PRICE_COMPLETE(4, "议价完成"),
    ;

    private int status;

    private String desc;

    QuotedPriceRecordStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static QuotedPriceRecordStatusEnum parse(Integer status) {
        for (QuotedPriceRecordStatusEnum quotedPriceRecordStatusEnum : QuotedPriceRecordStatusEnum.values()) {
            if (quotedPriceRecordStatusEnum.getStatus() == status) {
                return quotedPriceRecordStatusEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (QuotedPriceRecordStatusEnum value : QuotedPriceRecordStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(14);
            map.put("desc", value.getDesc());
            map.put("status", value.getStatus());
            list.add(map);
        }
        return list;
    }
}
