package com.innovation.ic.cc.base.pojo.enums;

public enum MyCompanyScsEnum {

    ADD("add", "新增"),
    DELETE("delete", "删除");

    private String code;
    private String desc;

    MyCompanyScsEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
