package com.innovation.ic.cc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 收货地址pojo
 *
 * @author B1B
 */
@Data
@ApiModel(value = "ShippingAddressPojo", description = "收货地址pojo")
public class ShippingAddressPojo {


    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "收货人", dataType = "String")
    private String consignee;

    @ApiModelProperty(value = "省份关联id", dataType = "Integer")
    private Integer province;

    @ApiModelProperty(value = "省份名字", dataType = "String")
    private String provinceName;

    @ApiModelProperty(value = "市区id", dataType = "Integer")
    private Integer city;


    @ApiModelProperty(value = "市区名字", dataType = "String")
    private String cityName;

    @ApiModelProperty(value = "详细地址", dataType = "String")
    private String detailedAddress;

    @ApiModelProperty(value = "电话", dataType = "String")
    private String telephone;


    @ApiModelProperty(value = "1 为true,是否默认地址", dataType = "Integer")
    private Integer defaultAddress;


    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createTime;

    @ApiModelProperty(value = "创建人id", dataType = "Integer")
    private String createId;


}
