package com.innovation.ic.cc.base.pojo.enums;

/**
 * 存储redis结构的前缀
 */
public enum RedisKeyPrefixEnum {
    MY_COMPANY_KEY("myCompany", "我的公司"),
    MY_COMPANY_GET_DETAIL("api:v1:myCompany:getDetail", "我的公司获取详情前缀"),
    ACTION_MESSAGE_PAGE("api:v1:actionMessage:page", "动作消息分页显示"),
    ACTION_MESSAGE_UNREAD_TOTAL("api:v1:actionMessage:unReadTotal", "未读消息总数"),
    ADVANTAGE_MODEL_DATA("api:v1:advantageModel:advantageModelData", "优势型号数据前缀"),
    ADVANTAGE_MODEL_BRAND_DATA("api:v1:advantageModelBrand:advantageModelBrandData", "优势型号的所属品牌数据前缀"),
    INVENTORY_DATA("api:v1:inventory:inventoryData:data", "库存数据前缀"),
    INVENTORY_QUERY("api:v1:inventory:query:queryData", "库存信息列表查询方法"),
    PERSONNEL_MANAGEMENT_DATA("api:v1:personnelManagement:findUserId","人员管理数据前缀"),
    DEPARTMENT_DATA("api:v1:department:findUserIdAllDepartment","部门管理数据前缀"),
    USER_MANAGEMENT_DATA("api:v1:userManagement:findDepartmentUser","人员管理数据前缀"),
    USER_USERID_DATA("api:v1:user:findUserData","通过主键查询用户信息前缀"),
    CHENK_FINDUSERNAME_DATA("api:v1:check:findUserName","校验子账号添加时账号是否重复前缀"),
    CHENK_FINDPHONE_DATA("api:v1:check:findPhone","校验子账号添加时手机号是否重复前缀"),
    CHENK_FINDEMAIL_DATA("api:v1:check:findEmail","校验子账号添加时邮箱是否重复前缀"),
    ROLE_ID_DATA("api:v1:user:save","通过_id查询角色前缀");
    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    RedisKeyPrefixEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}