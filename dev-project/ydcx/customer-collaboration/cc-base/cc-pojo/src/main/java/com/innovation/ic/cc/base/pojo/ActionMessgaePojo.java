package com.innovation.ic.cc.base.pojo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ActionMessgaePojo {

    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "内容", dataType = "String")
    private String content;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUserId;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    private String createTime;

    @ApiModelProperty(value = "是否已读", dataType = "Integer")
    private String isRead;
}
