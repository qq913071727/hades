package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 询价记录
 * @Date 2023/2/24 15:04
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceRecordPojo", description = "询价记录")
public class InquiryPriceRecordPojo {
    /**
     * 询价记录主键
     */
    private Integer inquiryPriceRecordId;

    /**
     * 询价id
     */
    private Integer inquiryPriceId;

    /**
     * 型号
     */
    private String model;

    /**
     * 品牌
     */
    private String brand;


    /**
     * 数量
     */
    private Integer amount;

    /**
     * 批次
     */
    private String batch;

    /**
     * 封装
     */
    private String encapsulation;

    /**
     * 目标价
     */
    private BigDecimal acceptPrice;

    /**
     * 报价
     */
    private BigDecimal quotedPrice;
    /**
     * 交货期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deliveryDate;

    /**
     * 截止时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;

    /**
     * mpq
     */
    private Integer mpq;

    /**
     * 包装
     */
    private String packing;

    /**
     * 状态。
     */
    private Integer status;

    private String statusDesc;

    /**
     * 备注
     */
    private String note;
    /**
     * bom单匹配选择后不可议价 议价标志: 1:可以议价 0:不可议价
     */
    private Boolean discussionPriceFlag;

    /**
     * erp 针对型号的询价id
     */
    private String erpSubInquiryId;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    private Integer inquiryType;

    private String inquiryTypeDesc;

    /**
     * 发票名称
     */
    private String invoiceName;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 最近修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
