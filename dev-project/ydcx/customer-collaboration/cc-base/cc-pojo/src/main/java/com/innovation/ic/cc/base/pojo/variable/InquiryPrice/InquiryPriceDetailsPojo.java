package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import com.innovation.ic.cc.base.pojo.variable.discussionPrice.DiscussionPriceRecordPojo;
import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceRecordPojo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zengqinglong
 * @desc 询价详情
 * @Date 2023/5/8 14:24
 **/
@Data
public class InquiryPriceDetailsPojo {
    @ApiModelProperty(value = "询价详情")
    private InquiryPriceRecordPojo inquiryPriceRecordPojo;

    @ApiModelProperty(value = "报价信息")
    private QuotedPriceRecordPojo quotedPriceRecordPojo;

    @ApiModelProperty(value = "我的议价")
    private DiscussionPriceRecordPojo discussionPriceRecordPojo;

    @ApiModelProperty(value = "二次报价信息")
    private QuotedPriceRecordPojo quotedPriceRecordPojoTwo;
}
