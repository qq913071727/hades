package com.innovation.ic.cc.base.pojo.enums;

/**
 * @desc   短信验证状态枚举
 * @author linuo
 * @time   2022年8月8日13:45:12
 */

public enum SmsStatusEnum {
    READY(0, "初始状态"),
    CHECK_SUCCESS(1, "成功"),
    CHECK_FALSE(2, "失败");

    private Integer code;
    private String desc;

    SmsStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static SmsStatusEnum of(Integer code) {
        for (SmsStatusEnum c : SmsStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (SmsStatusEnum c : SmsStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}