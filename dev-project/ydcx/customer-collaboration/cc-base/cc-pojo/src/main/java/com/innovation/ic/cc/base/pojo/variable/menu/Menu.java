package com.innovation.ic.cc.base.pojo.variable.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Menu", description = "菜单节点")
public class Menu {
    @ApiModelProperty(value = "id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "类型。1表示菜单，2表示按钮", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "节点是否被选择/是否需要显示。1表示已选择/需要显示，0表示未选择/不需要显示", dataType = "Integer")
    private Integer selected;

    @ApiModelProperty(value = "路由", dataType = "String")
    private String path;

    @ApiModelProperty(value = "图标", dataType = "String")
    private String icon;

    @ApiModelProperty(value = "子节点", dataType = "List")
    private List<Menu> childMenuList = new ArrayList<Menu>();
}
