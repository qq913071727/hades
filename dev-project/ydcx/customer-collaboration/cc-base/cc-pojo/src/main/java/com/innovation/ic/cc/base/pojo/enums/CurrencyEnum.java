package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc  金额枚举
 * @Date 2023/02/15 10:21:11
 **/
public enum CurrencyEnum {
    UNKNOWN(-100,"未知"),
    RMB(1, "人民币"),
    DOLLAR(2, "美金");

    private int type;

    private String desc;

    CurrencyEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static CurrencyEnum parse(Integer type) {
        for (CurrencyEnum currencyEnum : CurrencyEnum.values()) {
            if (currencyEnum.getType() == type) {
                return currencyEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (CurrencyEnum value : CurrencyEnum.values()) {
            map = new LinkedHashMap<String, Object>(4);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
