package com.innovation.ic.cc.base.pojo.variable.discussionPrice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 议价记录
 * @Date 2023/5/8 15:06
 **/
@Data
public class DiscussionPriceRecordPojo {
    /**
     * 议价记录主键
     */
    private Integer id;

    /**
     * 报价id
     */
    private Integer quotedPriceId;

    /**
     * 询价id
     */
    private Integer inquiryPriceId;

    /**
     * 询价记录表id
     */
    private Integer inquiryPriceRecordId;

    /**
     * 型号
     */
    private String model;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 数量
     */
    private Integer amount;

    /**
     * 批次
     */
    private String batch;

    /**
     * 封装
     */
    private String encapsulation;

    /**
     * 目标价
     */
    private BigDecimal acceptPrice;

    /**
     * 交货期
     */
    private Date deliveryDate;

    /**
     * 截止时间
     */
    private Date deadline;

    /**
     * mpq
     */
    private Integer mpq;

    /**
     * 包装
     */
    private String packing;

    /**
     * 备注
     */
    private String note;

    /**
     * 币种 1:人民币 2:美元
     */
    private Integer currency;
    private String currencyDesc;

    /**
     * 交货地。1:大陆，2:香港
     */
    private Integer deliveryPlace;
    private String deliveryPlaceDesc;

    /**
     * erp 针对型号的议价id
     */
    private String erpSubDiscussionId;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最近修改时间
     */
    private Date updateTime;
}
