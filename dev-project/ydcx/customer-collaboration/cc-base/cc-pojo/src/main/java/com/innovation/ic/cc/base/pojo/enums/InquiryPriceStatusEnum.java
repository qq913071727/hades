package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 询价状态 0:初始化（逐步不需要状态） 1:待询价，2:待报价，3:部分报价，4:报价完成, 5:超时未报价,6:无效询价
 * @Date 2023/2/15 10:35
 **/
public enum InquiryPriceStatusEnum {
    UNKNOWN(-100,"未知"),
    INIT(0,"初始化"),
    TO_BE_INQUIRED(1, "待询价"),
    TO_BE_QUOTED(2, "待报价"),
    PARTIAL_OFFER(3, "部分报价"),
    QUOTATION_COMPLETED(4, "已报价"),
    NO_QUOTATION_FOR_OVERTIME(5, "超时未报价"),
    INVALID_INQUIRY(6, "无效询价"),
    ;
    private int status;
    private String desc;

    InquiryPriceStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
    public static InquiryPriceStatusEnum parse(Integer status) {
        for (InquiryPriceStatusEnum inquiryPriceStatusEnum : InquiryPriceStatusEnum.values()) {
            if (inquiryPriceStatusEnum.getStatus() == status) {
                return inquiryPriceStatusEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InquiryPriceStatusEnum value : InquiryPriceStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(8);
            map.put("desc", value.getDesc());
            map.put("status", value.getStatus());
            list.add(map);
        }
        return list;
    }
}
