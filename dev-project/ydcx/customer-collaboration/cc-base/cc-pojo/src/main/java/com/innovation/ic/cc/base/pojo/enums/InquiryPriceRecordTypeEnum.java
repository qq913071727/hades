package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 数据类型
 * @Date 2023/2/15 11:27
 **/
public enum InquiryPriceRecordTypeEnum {
    UNKNOWN(-100, "未知"),
    GENERAL_ENQUIRY(1, "普通询价"),
    BOM_INQUIRY(2, "BOM询价");

    private int type;

    private String desc;

    InquiryPriceRecordTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static InquiryPriceRecordTypeEnum parse(Integer type) {
        for (InquiryPriceRecordTypeEnum inquiryPriceRecordTypeEnum : InquiryPriceRecordTypeEnum.values()) {
            if (inquiryPriceRecordTypeEnum.getType() == type) {
                return inquiryPriceRecordTypeEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InquiryPriceRecordTypeEnum value : InquiryPriceRecordTypeEnum.values()) {
            map = new LinkedHashMap<String, Object>(8);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
