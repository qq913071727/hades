package com.innovation.ic.cc.base.pojo.enums;

/**
 * @desc   登录类型枚举
 * @author linuo
 * @time   2022年8月18日11:04:20
 */
public enum LoginTypeEnum {
    WECHAT(0, "微信"),
    QQ(1, "QQ"),
    ALIPAY(3, "支付宝");

    private Integer code;
    private String desc;

    LoginTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static LoginTypeEnum of(Integer code) {
        for (LoginTypeEnum c : LoginTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (LoginTypeEnum c : LoginTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}