package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价单
 * @Date 2023/2/24 13:57
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPricePojo", description = "询价单")
public class InquiryPricePojo {
    /**
     * 询价主键
     */
    private Integer id;

    /**
     * 询价单号
     */
    private String oddNumbers;

    /**
     * 询价方式 1:逐条询价 2:bom询价
     */
    private Integer inquiryMethod;

    /**
     * 我的公司id，对应my_company表的id
     */
    private Integer myCompanyId;

    /**
     * 我的公司名称，对应my_company表的name
     */
    private String myCompanyName;

    /**
     * 币种。1:表示人民币，2:表示美元
     */
    private Integer currency;

    private String currencyDesc;
    /**
     * 发票id
     */
    private String invoiceTypeId;

    /**
     * 发票名称
     */
    private String invoiceName;

    /**
     * 截止时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deadline;


    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    private Integer inquiryType;

    private String inquiryTypeDesc;

    /**
     * 交货地。1:大陆，2;香港
     */
    private Integer deliveryPlace;

    private String deliveryPlaceDesc;

    /**
     * 状态。
     */
    private Integer status;

    private String statusDesc;

    /**
     * 备注
     */
    private String note;


    /**
     * 询价人id，对应user表的id
     */
    private String userId;

    /**
     * 询价人真实姓名，对应user表的real_name
     */
    private String realName;

    /**
     * 创建时间,询价日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 最近修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
