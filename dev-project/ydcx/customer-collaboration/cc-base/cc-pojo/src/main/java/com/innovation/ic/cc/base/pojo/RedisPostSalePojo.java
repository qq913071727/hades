package com.innovation.ic.cc.base.pojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * Redis封装售后管理数据类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RedisPostSalePojo", description = "Redis封装售后管理数据类")
public class RedisPostSalePojo {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "订单项", dataType = "String")
    private String orderNo;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String model;

    @ApiModelProperty(value = "类型。1.退货 2.换货 3.补货", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "售后数量", dataType = "Integer")
    private Integer amount;

    @ApiModelProperty(value = "单价", dataType = "BigDecimal")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "售后金额", dataType = "BigDecimal")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "币种。1表示人民币 2美元", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "申请日期", dataType = "Long")
    private Long applicationDate;

    @ApiModelProperty(value = "运单号", dataType = "String")
    private String waybillNumber;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String note;
}
