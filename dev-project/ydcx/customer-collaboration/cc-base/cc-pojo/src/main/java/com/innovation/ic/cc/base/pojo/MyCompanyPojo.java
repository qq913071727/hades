package com.innovation.ic.cc.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 我的公司详情Pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyPojo", description = "我的公司")
public class MyCompanyPojo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "国别地区id", dataType = "String")
    private String countryRegionId;


    @ApiModelProperty(value = "国别地区id", dataType = "String")
    private String countryRegionName;



    @ApiModelProperty(value = "公司类型id", dataType = "String")
    private String companyTypeId;


    @ApiModelProperty(value = "公司类型名字", dataType = "String")
    private String companyTypeName;


    @ApiModelProperty(value = "主要产品", dataType = "String")
    private String mainProducts;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String currency;


    @ApiModelProperty(value = "发票类型", dataType = "String")
    private String invoiceType;


    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String createId;


    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "失败原因", dataType = "String")
    private String cause;


    @ApiModelProperty(value = "关联id,删除关联公司使用", dataType = "String")
    private String relationEnterpriseId;

    @ApiModelProperty(value = "是否主公司 1 为主公司", dataType = "Integer")
    private Integer mainCompany;

    @ApiModelProperty(value = "状态 ： 100 待审核  200 审核通过  300 审核拒绝", dataType = "Integer")
    private Integer state;

    @ApiModelProperty(value = "文件对象多个", dataType = "UploadFilePojo")
    List<UploadFilePojo> files;

}