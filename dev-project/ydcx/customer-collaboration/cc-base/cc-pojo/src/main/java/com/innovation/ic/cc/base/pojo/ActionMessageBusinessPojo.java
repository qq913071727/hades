package com.innovation.ic.cc.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 动作消息业务表
 *
 * @author myq 15536078022@163.com
 * @since 1.0.0 2023-02-22
 */
@ApiModel(value = "ActionMessageBusinessPojo", description = "动作消息业务表 响应类")
@Setter
@Getter
public class ActionMessageBusinessPojo {


    @ApiModelProperty(value = "")
    private Integer id;

    @ApiModelProperty(value = "业务类型： 1发货通知单 2订单")
    private Integer businessType;

    @ApiModelProperty(value = "业务单号")
    private String businessCode;

    @ApiModelProperty(value = "动作消息ID")
    private Integer actionMessageId;

    @ApiModelProperty(value = "")
    private String createTime;
}