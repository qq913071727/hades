package com.innovation.ic.cc.base.pojo.global;

import java.util.HashMap;
import java.util.Map;

/**
 * 上下文对象，用于存储数据
 */
public class Context {

    private static Map<String, Object> parameterMap = new HashMap<String, Object>();

    public static void remove(String key) {
        parameterMap.remove(key);
    }

    public static void put(String key, Object value) {
        parameterMap.put(key, value);
    }

    public static Object get(String key){
        return parameterMap.get(key);
    }
}
