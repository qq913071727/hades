package com.innovation.ic.cc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc 询价型号状态 1:待询价，2:待报价，3:已报价，4:议价中，5:议价完成，6:已生成订单,7:议价失败, 8:超时未报价,9:无法报价, 10:无效报价
 * @Date 2023/2/15 11:27
 **/
public enum InquiryPriceRecordStatusEnum {
    UNKNOWN(-100, "未知"),
    PENDING(1, "待询价"),
    TO_BE_QUOTATION(2, "待报价"),
    QUOTED(3, "已报价"),
    DISCUSSION_PRICE_IN(4, "议价中"),
    DISCUSSION_PRICE_COMPLETE(5, "议价完成"),
    ORDER_GENERATED(6, "已生成订单"),
    DISCUSSION_PRICE_FAIL(7, "议价失败"),
    NO_QUOTATION_OVERTIME(8, "超时未报价"),
    UNABLE_TO_QUOTE(9, "无法报价"),
    INVALID_QUOTE(10, "无效报价"),
    ;

    private  int status;

    private String desc;

    InquiryPriceRecordStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static InquiryPriceRecordStatusEnum parse(Integer status) {
        for (InquiryPriceRecordStatusEnum inquiryPriceRecordStatusEnum : InquiryPriceRecordStatusEnum.values()) {
            if (inquiryPriceRecordStatusEnum.getStatus() == status) {
                return inquiryPriceRecordStatusEnum;
            }
        }
        return UNKNOWN;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InquiryPriceRecordStatusEnum value : InquiryPriceRecordStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(14);
            map.put("desc", value.getDesc());
            map.put("status", value.getStatus());
            list.add(map);
        }
        return list;
    }
}
