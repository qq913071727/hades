package com.innovation.ic.cc.base.pojo.variable;


import lombok.Data;

import java.util.List;

/**
 * 分页返回工具类
 */
@Data
public class IPagePojo<T> {

    private List<T> list;
    private long pages;
    private long size;
    private long total;


    public IPagePojo(List<T> list, long pages, long size, long total) {
        this.list = list;
        this.pages = pages;
        this.size = size;
        this.total = total;
    }


}
