package com.innovation.ic.cc.base.pojo.variable.InquiryPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价单查询
 * @Date 2023/2/24 15:02
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryPriceAndInquiryPriceRecordBO", description = "询价单")
public class InquiryPriceAndInquiryPriceRecordBO {
    /**
     * 询价主键
     */
    @ApiModelProperty(value = "询价主键", dataType = "Integer")
    private Integer id;

    /**
     * 询价单号
     */
    @ApiModelProperty(value = "询价单号", dataType = "String")
    private String oddNumbers;

    /**
     * 询价方式 1:逐条询价 2:bom询价
     */
    @ApiModelProperty(value = "询价方式 1:逐条询价 2:bom询价", dataType = "Integer")
    private Integer inquiryMethod;

    /**
     * 我的公司id，对应my_company表的id
     */
    @ApiModelProperty(value = "我的公司id，对应my_company表的id", dataType = "Integer")
    private Integer myCompanyId;

    /**
     * 我的公司名称，对应my_company表的name
     */
    @ApiModelProperty(value = "我的公司名称，对应my_company表的name", dataType = "String")
    private String myCompanyName;

    /**
     * 客户id，询价必要条件
     */
    @ApiModelProperty(value = "客户id，询价必要条件", dataType = "String")
    private String siteId;

    /**
     * 国别地区，询价必要条件
     */
    @ApiModelProperty(value = "国别地区，询价必要条件", dataType = "String")
    private String countryRegionId;

    /**
     * 销售id，询价必要条件
     */
    @ApiModelProperty(value = "销售id，询价必要条件", dataType = "String")
    private String salerId;

    /**
     * 币种 1:人民币 2:美元
     */
    @ApiModelProperty(value = "币种 1:人民币 2:美元", dataType = "Integer")
    private Integer currency;

    /**
     * 发票id
     */
    @ApiModelProperty(value = "发票id", dataType = "String")
    private String invoiceTypeId;

    /**
     * 发票名称
     */
    @ApiModelProperty(value = "发票名称", dataType = "String")
    private String invoiceName;

    /**
     * 截止时间
     */
    @ApiModelProperty(value = "截止时间", dataType = "Date")
    private Date deadline;

    /**
     * 询价类型。1:实单询价，2:普通询价
     */
    @ApiModelProperty(value = "询价类型。1:实单询价，2:普通询价", dataType = "Integer")
    private Integer inquiryType;

    /**
     * 交货地。1:大陆，2:香港
     */
    @ApiModelProperty(value = "交货地。1:大陆，2:香港", dataType = "Integer")
    private Integer deliveryPlace;

    /**
     * 状态。1:待询价，2:待报价，3:已报价，4:议价中，5:议价完成，6:已生成订单,7:议价失败, 8:超时未报价,9：无法报价
     */
    @ApiModelProperty(value = "状态。", dataType = "Integer")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", dataType = "String")
    private String note;

    /**
     * 询价人id，对应user表的id
     */
    @ApiModelProperty(value = "询价人id，对应user表的id", dataType = "String")
    private String userId;

    /**
     * 询价人真实姓名，对应user表的real_name
     */
    @ApiModelProperty(value = "询价人真实姓名，对应user表的real_name", dataType = "String")
    private String realName;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createTime;

    /**
     * 最近修改时间
     */
    @ApiModelProperty(value = "最近修改时间", dataType = "Date")
    private Date updateTime;
    /**
     * 询价单列表
     */
    private List<InquiryPriceRecordBO> inquiryPriceRecordBOS;
}
