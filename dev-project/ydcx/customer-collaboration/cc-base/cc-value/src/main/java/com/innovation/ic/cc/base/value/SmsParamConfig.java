package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @desc   短信参数配置类
 * @author linuo
 * @time   2022年8月5日13:41:08
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "sms")
public class SmsParamConfig {
    /** 短信平台地址 */
    private String url;

    /** 用户名 */
    private String name;

    /** 密码 */
    private String password;

    /** 短信模板编号 */
    private String smsCode;

    /** 短信长度 */
    private Integer length;

    /** 是否真实发送短信 true(发送短信) false(不发送短信) */
    private Boolean enable;

    /** 短信验证最大次数 */
    private Integer validCount;

    /** 短信验证最大时长(单位:分钟) */
    private Integer validTime;

    /** ERP发送短信接口地址url */
    private String erpSendSmsUrl;
}