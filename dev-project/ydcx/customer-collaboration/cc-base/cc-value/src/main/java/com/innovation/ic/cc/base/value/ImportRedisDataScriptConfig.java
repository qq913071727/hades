package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   导入redis数据脚本配置类
 * @author zengqinglong
 * @time   2023-04-25
 */
@Data
@Component
@ConfigurationProperties(prefix = "importdata")
public class ImportRedisDataScriptConfig {
    String[] scripts;
}