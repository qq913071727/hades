package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @desc   短信参数配置类
 * @author songwenqi
 * @time   2022年8月5日13:41:08
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "login")
public class LoginConfig {
    /** 登录认证地址 */
    private String url;

    /** 重置密码 */
    private String resetPassword;

    /** 获取登录授权地址url */
    private String getAuthUrl;

    /** 登录地址url */
    private String loginUrl;

    /** 绑定第三方账户地址url */
    private String bandUrl;

    /** 解绑第三方账户地址url */
    private String unBandUrl;

    /** ERP短信验证码登录地址url */
    private String erpSmsLoginUrl;

    /** 修改密码 */
    private String updatePasswordUrl;
}