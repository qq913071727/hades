package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   Zookeeper配置类
 * @author zengqinglong
 * @time   2023-05-08
 */
@Data
@Component
@ConfigurationProperties(prefix = "zookeeper")
public class ZookeeperParamConfig {
    private Long waitingLockTime;

    private String host;

    private Integer baseSleepTimeMs;

    private Integer maxEntries;

    private Integer sessionTimeoutMs;

    private Integer connectionTimeoutMs;

    private String importRedisDataPath;
}