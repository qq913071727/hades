package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   大赢家接口地址
 * @author linuo
 * @time   2023年5月15日10:52:30
 */
@Data
@Component
@ConfigurationProperties(prefix = "dyjinterfaceaddress")
public class DyjInterfaceAddressConfig {
    /** 获取接口调用key */
    private String getJsrInfoByIdAddress;

    /** 模糊查询型号接口 */
    private String blurQueryPartNumberAddress;
}