package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   sftp配置类
 * @author linuo
 * @time   2022年8月10日17:12:19
 */
@Data
@Component
@ConfigurationProperties(prefix = "sftp")
public class SftpParamConfig {
    /** ip地址 */
    private String host;

    /** 端口号 */
    private int port;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /** 超时时间 */
    private int timeout;
}