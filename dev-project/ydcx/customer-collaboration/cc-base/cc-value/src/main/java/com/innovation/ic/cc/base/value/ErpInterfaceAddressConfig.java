package com.innovation.ic.cc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   erp接口地址配置类
 * @author swq
 * @time   2023年2月6日17:11:59
 */
@Data
@Component
@ConfigurationProperties(prefix = "erp")
public class ErpInterfaceAddressConfig {

    /** ERP发送新密码接口地址url */
    private String erpForgotPasswordUrl;

    /** ERP ERP推送询价地址*/
    private String erpPushInquiryPriceUrl;
}