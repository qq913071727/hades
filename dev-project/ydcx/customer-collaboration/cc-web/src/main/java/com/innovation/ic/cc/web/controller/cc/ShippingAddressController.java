package com.innovation.ic.cc.web.controller.cc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.ShippingAddress;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ShippingAddressService;
import com.innovation.ic.cc.base.vo.address.ShippingAddressQueryVo;
import com.innovation.ic.cc.base.vo.address.ShippingAddressVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 收货地址
 */
@Api(value = "收货地址API", tags = "ShippingAddressController")
@RestController
@RequestMapping("/api/v1/shippingAddress")
@Slf4j
public class ShippingAddressController extends AbstractController {

    @Resource
    private ShippingAddressService shippingAddressService;







    @ApiOperation(value = "新增收货地址")
    @ApiImplicitParam(name = "shippingAddressVo", value = "新增收货地址", required = true, dataType = "ShippingAddressVo")
    @PostMapping(value = "/addShippingAddress")
    @ResponseBody
    public ResponseEntity<ApiResult> addShippingAddress(@RequestBody ShippingAddressVo shippingAddressVo, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(shippingAddressVo.getConsignee())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("收货人不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (shippingAddressVo.getProvince() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("省份不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (shippingAddressVo.getCity() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("市不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(shippingAddressVo.getDetailedAddress())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("详细地址不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(shippingAddressVo.getTelephone())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("手机不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        shippingAddressVo.setCreateId(authenticationUser.getId());
        ServiceResult serviceResult = shippingAddressService.addShippingAddress(shippingAddressVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }



    @ApiOperation(value = "删除收货地址")
    @PostMapping(value = "/deleteShippingAddress")
    @ResponseBody
    public ResponseEntity<ApiResult> deleteShippingAddress(@RequestParam List<String> ids,HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (CollectionUtils.isEmpty(ids)) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("ids不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (!StringUtils.validateParameter(authenticationUser.getId())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("用户暂未登陆");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult serviceResult =  shippingAddressService.deleteShippingAddress(ids,authenticationUser);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "修改收货地址")
    @PostMapping(value = "/updateShippingAddress")
    @ResponseBody
    public ResponseEntity<ApiResult> updateShippingAddress(@RequestBody ShippingAddressVo shippingAddressVo,HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (shippingAddressVo.getId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(shippingAddressVo.getConsignee())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("收货人不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (shippingAddressVo.getProvince() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("省份不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (shippingAddressVo.getCity() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("市不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(shippingAddressVo.getDetailedAddress())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("详细地址不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(shippingAddressVo.getTelephone())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("手机不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        shippingAddressVo.setCreateId(authenticationUser.getId());
        shippingAddressService.updateShippingAddress(shippingAddressVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "查询收货地址列表")
    @GetMapping(value = "/getShippingAddressList")
    @ResponseBody
    public ResponseEntity<ApiResult> getShippingAddressList(ShippingAddressQueryVo shippingAddressQueryVo,HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        shippingAddressQueryVo.setCreateId(authenticationUser.getId());
        ServiceResult<IPage<ShippingAddress>> serviceResult = shippingAddressService.getShippingAddressList(shippingAddressQueryVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }




    @ApiOperation(value = "设置默认")
    @GetMapping(value = "/setUpDefault")
    @ResponseBody
    public ResponseEntity<ApiResult> setUpDefault(ShippingAddressQueryVo shippingAddressQueryVo,HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        shippingAddressQueryVo.setCreateId(authenticationUser.getId());
        shippingAddressService.setUpDefault(shippingAddressQueryVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("设置成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }





}
