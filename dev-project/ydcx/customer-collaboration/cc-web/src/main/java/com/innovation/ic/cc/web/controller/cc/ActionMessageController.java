package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.pojo.ActionMessgaePojo;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ActionMessageService;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName ActionMessageController
 * @Description 动作消息控制器
 * @Date 2022/10/8
 * @Author myq
 */
@Api(value = "消息提醒", tags = "ActionMessageController")
@RestController
@RequestMapping("api/v1/actionMessage")
@Slf4j
public class ActionMessageController extends AbstractController {


    @Resource
    private ActionMessageService actionMessageService;


    @ApiOperation("分页")
    @GetMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20")
    })
    public ResponseEntity<ApiResult<PageInfo<ActionMessgaePojo>>> page(int pageNo, int pageSize,
                                                                       HttpServletRequest request) {

        return new ResponseEntity<>(ApiResult.ok(actionMessageService.page(pageNo, pageSize, getUserId(request)).getResult(), "动作消息分页数据ok"), HttpStatus.OK);
    }


    @ApiOperation("全部已读")
    @GetMapping("isReadAll")
    public ResponseEntity<ApiResult> isReadAll(HttpServletRequest request) {

        actionMessageService.isReadAll(getUserId(request));
        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }


    @ApiOperation("未读消息总数")
    @GetMapping("unReadTotal")
    public ResponseEntity<ApiResult<Integer>> unReadTotal(HttpServletRequest request) {
        ServiceResult<Integer> res = actionMessageService.unReadTotal(getUserId(request));

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), "ok"), HttpStatus.OK);
    }
}
