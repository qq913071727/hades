package com.innovation.ic.cc.web;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.innovation.ic.cc")
@MapperScan("com.innovation.ic.cc.base.mapper")
@EnableSwagger2
@EnableDiscoveryClient
public class CCWebApplication {

    private static final Logger log = LoggerFactory.getLogger(CCWebApplication.class);

    public static void main(String[] args) {
        LogbackHelper.setLogbackPort(args);
        SpringApplication.run(CCWebApplication.class, args);
        log.info("cc-web服务启动成功");
    }
}
