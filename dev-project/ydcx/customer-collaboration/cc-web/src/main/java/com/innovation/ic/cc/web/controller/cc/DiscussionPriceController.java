//package com.innovation.ic.cc.web.controller.cc;
//
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.cc.base.pojo.variable.ApiResult;
//import com.innovation.ic.cc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
///**
// * @author zengqinglong
// * @desc 议价管理
// * @Date 2023/2/14 13:57
// **/
//@Api(value = "议价管理", tags = "DiscussionPriceController")
//@RestController
//@RequestMapping("/api/v1/discussionPrice")
//public class DiscussionPriceController extends AbstractController {
//    private static final Logger log = LoggerFactory.getLogger(DiscussionPriceController.class);
//}
