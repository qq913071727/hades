package com.innovation.ic.cc.web.schedule;

import com.innovation.ic.cc.base.service.cc.*;
import com.innovation.ic.cc.base.service.erp9_pvebcs.CompanyCurrencyService;
import com.innovation.ic.cc.base.service.erp9_pvebcs.ErpDictionariesService;
import com.innovation.ic.cc.base.service.erp9_pvecrm.CompanySCTopViewService;
import com.innovation.ic.cc.base.service.erp9_pvecrm.EnterprisesTopViewService;
import com.innovation.ic.cc.base.handler.cc.ModelHandler;
import com.innovation.ic.cc.base.service.ServiceHelper;
import com.innovation.ic.cc.base.service.erp9_pvesiteuser.StaffsService;
import com.innovation.ic.cc.base.service.erp9_pvesiteuser.UserClientOwnerService;
import com.innovation.ic.cc.base.service.erp9_pvesiteuser.UsersService;
import com.innovation.ic.cc.base.value.ErpInterfaceAddressConfig;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * 定时任务的抽象类
 */
public abstract class AbstractSchedule {
    @Resource
    protected CompanyCurrencyService companyCurrencyService;


    @Resource
    protected MyCompanyCurrencyService myCompanyCurrencyService;

    @Resource
    protected EnterprisesTopViewService enterprisesTopViewService;

    @Resource
    protected ErpMyCompanyNameService erpMyCompanyNameService;


    @Resource
    protected CompanySCTopViewService companySCTopViewService;

    @Resource
    protected ErpDictionariesService erpDictionariesService;


    @Resource
    protected DictionariesService dictionariesService;
    @Resource
    protected ServiceHelper serviceHelper;

    @Resource
    protected ModelHandler modelHandler;

    @Resource
    protected MenuTreeService menuTreeService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected UsersService usersService;

    @Autowired
    protected SaleService saleService;

    @Autowired
    protected UserClientOwnerService userClientOwnerService;

    @Autowired
    protected StaffsService staffsService;

    @Resource
    protected ErpInterfaceAddressConfig erpInterfaceAddressConfig;
}