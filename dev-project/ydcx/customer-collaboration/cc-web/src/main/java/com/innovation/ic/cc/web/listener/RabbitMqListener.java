package com.innovation.ic.cc.web.listener;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.cc.base.service.ServiceHelper;
import com.innovation.ic.cc.base.service.cc.*;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.*;
import com.innovation.ic.cc.base.service.cc.DictionariesService;
import com.innovation.ic.cc.base.service.cc.DictionariesTypeService;
import com.innovation.ic.cc.base.service.cc.ErpMyCompanyNameService;
import com.innovation.ic.cc.base.service.cc.InquiryPriceService;
import com.innovation.ic.cc.base.service.cc.MyCompanyService;
import com.innovation.ic.cc.base.service.cc.UserService;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenAddDistrictQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenAddDistrictTypeQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenAddErpCompanyQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenAuditErpCompanyQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenDeleteDistrictQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenDeleteErpCompanyQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenInquiryManagementDelayQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenModelAddUsersQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenModelUpdatePassWordQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenModelUpdateUsersQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenRelationErpCompanyQueueThread;
import com.innovation.ic.cc.base.thread.listener.rabbitmq.ListenUpdateErpCompanyQueueThread;
import com.rabbitmq.client.Channel;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * mq队列收到消息后，调用这个监听器的方法
 */
@Component
public class RabbitMqListener implements ApplicationRunner {
    @Resource
    private ServiceHelper serviceHelper;

    @Resource
    private ThreadPoolManager threadPoolManager;

    @Resource
    private InquiryPriceService inquiryPriceService;

    @Resource
    private UserService userService;

    @Resource
    private ErpMyCompanyNameService erpMyCompanyNameService;

    @Resource
    private DictionariesTypeService dictionariesTypeService;

    @Resource
    private DictionariesService dictionariesService;

    @Resource
    private MyCompanyService myCompanyService;

    @Resource
    private SaleService saleService;

    /**
     * 监听队列
     */
    @Override
    public void run(ApplicationArguments args) {
        Channel channel = serviceHelper.getRabbitMqManager().getChannel();

        // 监听询价管理 延时队列
        ListenInquiryManagementDelayQueueThread listenInquiryManagementDelayQueueThread = new ListenInquiryManagementDelayQueueThread(channel, inquiryPriceService);
        threadPoolManager.execute(listenInquiryManagementDelayQueueThread);

        // 监听主账号新增/修改队列
        ListenModelAddUsersQueueThread listenModelAddUsersQueueThread = new ListenModelAddUsersQueueThread(channel, userService, saleService);
        threadPoolManager.execute(listenModelAddUsersQueueThread);

        // 监听主账号状态队列
        ListenModelUpdateUsersQueueThread listenModelUpdateUsersQueueThread = new ListenModelUpdateUsersQueueThread(channel, userService);
        threadPoolManager.execute(listenModelUpdateUsersQueueThread);

        // 监听密码被重置队列
        ListenModelUpdatePassWordQueueThread listenModelUpdatePassWordQueueThread = new ListenModelUpdatePassWordQueueThread(channel, userService);
        threadPoolManager.execute(listenModelUpdatePassWordQueueThread);

        // 监听erp我的公司添加事件队列
        ListenAddErpCompanyQueueThread listenAddErpCompanyQueueThread = new ListenAddErpCompanyQueueThread(channel, erpMyCompanyNameService);
        threadPoolManager.execute(listenAddErpCompanyQueueThread);

        // 监听erp我的公司添加事件队列
        ListenUpdateErpCompanyQueueThread listenUpdateErpCompanyQueueThread = new ListenUpdateErpCompanyQueueThread(channel, erpMyCompanyNameService);
        threadPoolManager.execute(listenUpdateErpCompanyQueueThread);

        // 监听erp关联关系添加队列
        ListenRelationErpCompanyQueueThread listenRelationErpCompanyQueueThread = new ListenRelationErpCompanyQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenRelationErpCompanyQueueThread);

        // 监听关联关系审批队列
        ListenAuditErpCompanyQueueThread listenAuditErpCompanyQueueThread = new ListenAuditErpCompanyQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenAuditErpCompanyQueueThread);

        // 监听关联关系删除队列
        ListenDeleteErpCompanyQueueThread listenDeleteErpCompanyQueueThread = new ListenDeleteErpCompanyQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenDeleteErpCompanyQueueThread);


        // 监听添加枚举类型队列
        ListenAddDistrictTypeQueueThread listenAddDistrictTypeQueueThread = new ListenAddDistrictTypeQueueThread(channel, dictionariesTypeService);
        threadPoolManager.execute(listenAddDistrictTypeQueueThread);

        // 监听添加枚举值队列
        ListenAddDistrictQueueThread listenAddDistrictQueueThread = new ListenAddDistrictQueueThread(channel, dictionariesService);
        threadPoolManager.execute(listenAddDistrictQueueThread);

        // 监听删除枚举值队列
        ListenDeleteDistrictQueueThread listenDeleteDistrictQueueThread = new ListenDeleteDistrictQueueThread(channel, dictionariesService);
        threadPoolManager.execute(listenDeleteDistrictQueueThread);

        // 监听修改销售人队列
        ListenModelUpdateSaleIdQueueThread listenModelUpdateSaleIdQueueThread = new ListenModelUpdateSaleIdQueueThread(channel, saleService);
        threadPoolManager.execute(listenModelUpdateSaleIdQueueThread);

        // 监听修改销售信息队列
        ListenModelUpdateSaleDataQueueThread listenModelUpdateSaleDataQueueThread = new ListenModelUpdateSaleDataQueueThread(channel, saleService);
        threadPoolManager.execute(listenModelUpdateSaleDataQueueThread);
    }
}