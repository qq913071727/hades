package com.innovation.ic.cc.web.controller.cc;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.statementAccount.StatementAccountDetailReqVo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc   对账单相关接口
 * @author linuo
 * @time   2023年5月17日13:17:01
 */
@Api(value = "StatementAccountController", tags = "对账单")
@RestController
@RequestMapping("/api/v1/statementAccount")
@Slf4j
public class StatementAccountController {
    @ApiOperation(value = "对账单列表查询(参数type:1已出对账单、2未出对账单)")
    @RequestMapping(value = "/queryStatementAccountList/{type}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<JSONObject>> queryStatementAccountList(@PathVariable("type") Integer type) {
        if(type == null){
            String message = "调用接口【/api/v1/statementAccount/queryStatementAccountList】时，参数type不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ApiResult<JSONObject> apiResult = new ApiResult<>();
        JSONObject result = new JSONObject();

        List<JSONObject> list = new ArrayList<>();

        JSONObject json;
        // 已出对账单
        if(type == 1){
            json = new JSONObject();
            // 对账单号
            json.put("statementNumber", "B20230415823");
            // 对账单生成日期
            json.put("createDate", "2023-02-11");
            // 对账单所属期间
            json.put("owningPeriod", "2022-10-09至2022-10-16");
            // 结算日期
            json.put("settleDate", "2022-10-10");
            // 币种
            json.put("currency", "人民币");
            // 上期末未付
            json.put("unpaid", "200000.00");
            // 本期发货
            json.put("currentAmount", "100000.00");
            // 本期已付
            json.put("currentPaid", "0.00");
            // 本期末应付
            json.put("currentPayables", "300000.00");
            // 备注
            json.put("remark", "-");
            list.add(json);

            json = new JSONObject();
            // 对账单号
            json.put("statementNumber", "B20230415328");
            // 对账单生成日期
            json.put("createDate", "2023-01-11");
            // 对账单所属期间
            json.put("owningPeriod", "2022-08-09至2022-09-16");
            // 结算日期
            json.put("settleDate", "2022-09-10");
            // 币种
            json.put("currency", "人民币");
            // 上期末未付
            json.put("unpaid", "100000.00");
            // 本期发货
            json.put("currentAmount", "400000.00");
            // 本期已付
            json.put("currentPaid", "300000.00");
            // 本期末应付
            json.put("currentPayables", "200000.00");
            // 备注
            json.put("remark", "-");
            list.add(json);

            result.put("data", list);
        }

        // 未出对账单
        if(type == 2){
            // 账单日
            result.put("billingDate", "2023-06-10");
            // 消费金额
            result.put("consumptionAmount", "30000.00");
            // 笔数
            result.put("penCount", "4");

            json = new JSONObject();
            // 订单项
            json.put("orderItem", "DD22020000295-001");
            // 发货日期
            json.put("deliverDate", "2022-09-05");
            // 型号
            json.put("partNumber", "AHDJ2");
            // 数量
            json.put("count", "1000");
            // 单价
            json.put("unitPrice", "10.00");
            // 金额
            json.put("price", "10000.00");
            list.add(json);

            json = new JSONObject();
            // 订单项
            json.put("orderItem", "DD22020000295-002");
            // 发货日期
            json.put("deliverDate", "2022-09-04");
            // 型号
            json.put("partNumber", "JDHHHD1");
            // 数量
            json.put("count", "2000");
            // 单价
            json.put("unitPrice", "20.00");
            // 金额
            json.put("price", "40000.00");
            list.add(json);

            json = new JSONObject();
            // 订单项
            json.put("orderItem", "DD22020000295-003");
            // 发货日期
            json.put("deliverDate", "2022-09-03");
            // 型号
            json.put("partNumber", "DJJJD");
            // 数量
            json.put("count", "125000");
            // 单价
            json.put("unitPrice", "10.00");
            // 金额
            json.put("price", "1250000.00");
            list.add(json);

            json = new JSONObject();
            // 订单项
            json.put("orderItem", "DD22020000295-004");
            // 发货日期
            json.put("deliverDate", "2022-09-02");
            // 型号
            json.put("partNumber", "AHDJ2");
            // 数量
            json.put("count", "12000");
            // 单价
            json.put("unitPrice", "20.00");
            // 金额
            json.put("price", "24000.00");
            list.add(json);

            result.put("data", list);
        }

        apiResult.setResult(result);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @HystrixCommand
    @ApiOperation(value = "获取对账单详情")
    @ApiImplicitParam(name = "StatementAccountDetailReqVo", value = "获取对账单详情接口的Vo类", required = true, dataType = "StatementAccountDetailReqVo")
    @RequestMapping(value = "/getStatementAccountDetail", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<JSONObject>> getStatementAccountDetail(@RequestBody StatementAccountDetailReqVo statementAccountDetailReqVo) {
        if(Strings.isNullOrEmpty(statementAccountDetailReqVo.getStatementNumber())){
            String message = "获取对账单详情时，对账单号不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ApiResult<JSONObject> apiResult = new ApiResult<>();
        JSONObject result = new JSONObject();

        // 对账单号
        result.put("statementNumber", "B230415823");
        // 对账单生成日期
        result.put("createDate", "2022-11-11");
        // 结算日期
        result.put("settleDate", "2022-11-11");
        // 付款方式
        result.put("settleType", "月结30天");
        // 付款方式
        result.put("currency", "人民币");

        List<JSONObject> list = new ArrayList<>();
        JSONObject json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-001");
        // 发货日期
        json.put("deliverDate", "2022-09-05");
        // 型号
        json.put("partNumber", "AHDJ2");
        // 数量
        json.put("count", "1000");
        // 单价
        json.put("unitPrice", "10.00");
        // 金额
        json.put("price", "10000.00");
        list.add(json);

        json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-002");
        // 发货日期
        json.put("deliverDate", "2022-09-04");
        // 型号
        json.put("partNumber", "JDHHHD1");
        // 数量
        json.put("count", "2000");
        // 单价
        json.put("unitPrice", "20.00");
        // 金额
        json.put("price", "40000.00");
        list.add(json);

        json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-003");
        // 发货日期
        json.put("deliverDate", "2022-09-03");
        // 型号
        json.put("partNumber", "DJJJD");
        // 数量
        json.put("count", "125000");
        // 单价
        json.put("unitPrice", "10.00");
        // 金额
        json.put("price", "1250000.00");
        list.add(json);

        json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-004");
        // 发货日期
        json.put("deliverDate", "2022-09-02");
        // 型号
        json.put("partNumber", "AHDJ2");
        // 数量
        json.put("count", "12000");
        // 单价
        json.put("unitPrice", "20.00");
        // 金额
        json.put("price", "24000.00");
        list.add(json);

        // 订单数据
        result.put("orderList", list);

        // 金额合计
        result.put("totalAmount", "30000.00");

        // 已付金额
        result.put("amountPaid", "0.00");

        // 待付金额
        result.put("amountPayable", "95000.00");

        apiResult.setResult(result);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}