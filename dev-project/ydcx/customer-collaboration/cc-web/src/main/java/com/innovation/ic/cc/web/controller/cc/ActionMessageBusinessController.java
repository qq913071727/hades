package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.web.controller.AbstractController;
import com.innovation.ic.cc.base.service.cc.ActionMessageBusinessService;
import com.innovation.ic.cc.base.vo.ActionMessageBusinessVo;
import com.innovation.ic.cc.base.pojo.ActionMessageBusinessPojo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.annotation.Resource;
import java.util.List;


/**
 * 动作消息业务表
 *
 * @author myq
 * @since 1.0.0 2023-02-22
 */
@RestController
@RequestMapping("/api/v1/actionmessagebusiness")
@Api(tags = "动作消息业务表")
public class ActionMessageBusinessController extends AbstractController {

    @Resource
    private ActionMessageBusinessService ActionMessageBusinessService;

    @ApiOperation("分页")
    @PostMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "ActionMessageBusinessVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<PageInfo<ActionMessageBusinessPojo>>> page(int pageNo, int pageSize,
                                                   @RequestBody ActionMessageBusinessVo vo) {

        ServiceResult<PageInfo<ActionMessageBusinessPojo>> serviceResult = ActionMessageBusinessService.pageInfo(pageNo, pageSize, vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }

    @ApiOperation("列表")
    @PostMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ActionMessageBusinessVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<List<ActionMessageBusinessPojo>>> list(@RequestBody ActionMessageBusinessVo vo) {

        ServiceResult<List<ActionMessageBusinessPojo>> serviceResult = ActionMessageBusinessService.list(vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }


    @GetMapping("{id}")
    @ApiOperation("明细")
    public ResponseEntity<ApiResult<ActionMessageBusinessPojo>> get(@PathVariable("id") String id) {
        ServiceResult<ActionMessageBusinessPojo> serviceResult = ActionMessageBusinessService.get(id);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("保存")
    public ResponseEntity<ApiResult> save(ActionMessageBusinessVo vo) {
            ActionMessageBusinessService.save(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(ActionMessageBusinessVo vo) {
            ActionMessageBusinessService.update(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation("删除")
    public ResponseEntity<ApiResult> delete(String[] ids) {
            ActionMessageBusinessService.delete(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }



}