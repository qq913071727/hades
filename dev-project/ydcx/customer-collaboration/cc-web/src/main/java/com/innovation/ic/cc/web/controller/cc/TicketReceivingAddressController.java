package com.innovation.ic.cc.web.controller.cc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.TicketReceivingAddress;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.TicketReceivingAddressService;
import com.innovation.ic.cc.base.vo.address.TicketReceivingAddressQueryVo;
import com.innovation.ic.cc.base.vo.address.TicketReceivingAddressVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 收票地址
 */
@Api(value = "收票地址API", tags = "TicketReceivingAddressController")
@RestController
@RequestMapping("/api/v1/ticketReceivingAddress")
@Slf4j
public class TicketReceivingAddressController extends AbstractController {

    @Resource
    private TicketReceivingAddressService ticketReceivingAddressService;


    @ApiOperation(value = "新增收票地址")
    @ApiImplicitParam(name = "ticketReceivingAddressVo", value = "新增收票地址", required = true, dataType = "TicketReceivingAddressVo")
    @PostMapping(value = "/addTicketReceivingAddress")
    @ResponseBody
    public ResponseEntity<ApiResult> addTicketReceivingAddress(@RequestBody TicketReceivingAddressVo ticketReceivingAddressVo, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getConsignee())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("收货人不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (ticketReceivingAddressVo.getProvince() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("省份不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (ticketReceivingAddressVo.getCity() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("市不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getDetailedAddress())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("详细地址不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getTelephone())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("手机不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getMailbox())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("邮箱不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        ticketReceivingAddressVo.setCreateId(authenticationUser.getId());
        ServiceResult serviceResult = ticketReceivingAddressService.addTicketReceivingAddress(ticketReceivingAddressVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "删除收货地址")
    @PostMapping(value = "/deleteShippingAddress")
    @ResponseBody
    public ResponseEntity<ApiResult> deleteTicketReceivingAddress(@RequestParam List<String> ids, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (CollectionUtils.isEmpty(ids)) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("ids不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (!StringUtils.validateParameter(authenticationUser.getId())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("用户暂未登陆");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = ticketReceivingAddressService.deleteTicketReceivingAddress(ids, authenticationUser);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "修改收货地址")
    @PostMapping(value = "/updateTicketReceivingAddress")
    @ResponseBody
    public ResponseEntity<ApiResult> updateTicketReceivingAddress(@RequestBody TicketReceivingAddressVo ticketReceivingAddressVo, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (ticketReceivingAddressVo.getId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getConsignee())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("收货人不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (ticketReceivingAddressVo.getProvince() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("省份不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (ticketReceivingAddressVo.getCity() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("市不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getDetailedAddress())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("详细地址不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getTelephone())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("手机不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(ticketReceivingAddressVo.getMailbox())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("邮箱不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        ticketReceivingAddressVo.setCreateId(authenticationUser.getId());
        ticketReceivingAddressService.updateticketReceivingAddress(ticketReceivingAddressVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "查询收货地址列表")
    @GetMapping(value = "/getTicketReceivingAddressList")
    @ResponseBody
    public ResponseEntity<ApiResult> getTicketReceivingAddressList(TicketReceivingAddressQueryVo ticketReceivingAddressQueryVo, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        ticketReceivingAddressQueryVo.setCreateId(authenticationUser.getId());
        ServiceResult<IPage<TicketReceivingAddress>> serviceResult = ticketReceivingAddressService.getTicketReceivingAddressList(ticketReceivingAddressQueryVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "设置默认")
    @GetMapping(value = "/setUpDefault")
    @ResponseBody
    public ResponseEntity<ApiResult> setUpDefault(TicketReceivingAddressQueryVo ticketReceivingAddressQueryVo, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        ticketReceivingAddressQueryVo.setCreateId(authenticationUser.getId());
        ticketReceivingAddressService.setUpDefault(ticketReceivingAddressQueryVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage("设置成功");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }



}
