package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.handler.cc.InquiryPriceHandler;
import com.innovation.ic.cc.base.model.cc.InquiryPrice;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceDetailsPojo;
import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceListPojo;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPriceDelayedVo;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPricePageListVo;
import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPriceVo;
import com.innovation.ic.cc.base.vo.discussionPrice.DiscussionPriceCreatedVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 询价管理
 * @Date 2023/2/14 13:56
 **/
@Api(value = "询价管理", tags = "InquiryPriceController")
@RestController
@RequestMapping("/api/v1/inquiryPrice")
public class InquiryPriceController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(InquiryPriceController.class);


    /**
     * 询价管理: 发布询价
     *
     * @param inquiryPriceVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "询价管理: 发布询价")
    @ApiImplicitParam(name = "inquiryPriceVo", value = "发布询价入参", required = true, dataType = "InquiryPriceVo")
    @RequestMapping(value = "/releaseInquiryPrice", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<InquiryPrice>> releaseInquiryPrice(@RequestBody InquiryPriceVo inquiryPriceVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<InquiryPrice> apiResult;

        if (null == inquiryPriceVo || null == inquiryPriceVo.getMyCompanyId()
                || null == inquiryPriceVo.getInquiryType() || null == inquiryPriceVo.getDeadline()
                || inquiryPriceVo.getInquiryPriceRecordVoList().size() == 0) {
            String message = "调用接口【/api/v1/inquiryPrice/releaseInquiryPrice，参数公司Id,询价类型，截止时间，询价型号信息 不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        // 获取当前登录用户
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        inquiryPriceService.releaseInquiryPrice(inquiryPriceVo, authenticationUser);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 询价管理:  逐条询价列表
     *
     * @param inquiryPricePageListVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "询价管理: 逐条询价列表")
    @ApiImplicitParam(name = "inquiryPricePageListVo", value = "询价列表入参", required = true, dataType = "InquiryPricePageListVo")
    @RequestMapping(value = "/stripPageList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<InquiryPriceListPojo>>> pageList(@RequestBody InquiryPricePageListVo inquiryPricePageListVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<PageInfo<InquiryPriceListPojo>> apiResult;

        if (null == inquiryPricePageListVo || null == inquiryPricePageListVo.getMyCompanyId() || null == inquiryPricePageListVo.getStatus()) {
            String message = "调用接口【/api/v1/inquiryPrice/pageList，参数inquiryPricePageListVo不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        //  获取当前登录用户
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        PageInfo<InquiryPriceListPojo> inquiryPrice = inquiryPriceService.pageList(inquiryPricePageListVo, authenticationUser);
        apiResult = new ApiResult<>();

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(inquiryPrice);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 询价管理:询价详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "询价管理: 询价详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "询价主键", dataType = "String", required = true),
    })
    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<InquiryPriceDetailsPojo>> details(@PathVariable("id") String id) {
        ApiResult<InquiryPriceDetailsPojo> apiResult;
        if (!StringUtils.validateParameter(id)) {
            String message = "调用接口【/api/v1/inquiryPrice/details/{id}时，参数id不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<InquiryPriceDetailsPojo> serviceResult = inquiryPriceService.details(id);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @Resource
    InquiryPriceHandler inquiryPriceHandler;

    /**
     * 议价
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public void test() {
        inquiryPriceHandler.generateInquiryPriceNumber();
    }

    /**
     * 将inquiry_price表中的数据导入redis
     *
     * @return 返回处理结果
     */
    @RequestMapping(value = "/importInquiryNumberIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> importInquiryNumberIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("将inquiry_price表中当天的数据导入redis");
        // 将inquiry_price表中当天的数据导入redis
        List<String> numberList = inquiryPriceService.findAllSameDayNumber();
        if (!CollectionUtils.isEmpty(numberList)) {
            for (String number : numberList) {
                //过期时间24小时 86400秒
                serviceHelper.getRedisManager().sAdd(InquiryPriceHandler.InquiryPriceIdKey, 86400, number);
            }
        }
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 询价管理: 发起询价
     */
    @ApiOperation(value = "询价管理: 发起询价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "询价主键", dataType = "String", required = true),
    })
    @RequestMapping(value = "/launchInquiry/{ids}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> launchInquiry(@PathVariable("ids") List<String> ids, HttpServletRequest request) {
        ApiResult apiResult;
        // 将inquiry_price表中当天的数据导入redis
        if (CollectionUtils.isEmpty(ids)) {
            String message = "调用接口【/api/v1/inquiryPrice/launchInquiry/{ids}时，参数ids不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        // 获取当前登录用户
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        ServiceResult serviceResult = inquiryPriceService.launchInquiry(ids, authenticationUser.getToken());
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 询价管理: 取消议价
     */
    @ApiOperation(value = "询价管理: 取消议价")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "询价记录主键", dataType = "Integer", required = true),
    })
    @RequestMapping(value = "/cancelDiscussionPrice/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> cancelDiscussionPrice(@PathVariable("id") Integer id, HttpServletRequest request) {
        ApiResult apiResult;
        if (null == id) {
            String message = "调用接口【/api/v1/inquiryPrice/cancelDiscussionPrice/{id}时，参数id不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        // 获取当前登录用户
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        ServiceResult serviceResult = inquiryPriceService.cancelDiscussionPrice(id, authenticationUser.getToken());
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 询价管理: 延时询价
     */
    @ApiOperation(value = "询价管理: 延时询价")
    @ApiImplicitParam(name = "inquiryPriceDelayedVo", value = "延时询价入参", required = true, dataType = "InquiryPriceDelayedVo")
    @RequestMapping(value = "/delayedInquiryPrice", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> delayedInquiryPrice(@RequestBody InquiryPriceDelayedVo inquiryPriceDelayedVo, HttpServletRequest request) {
        ApiResult apiResult;
        if (null == inquiryPriceDelayedVo || null == inquiryPriceDelayedVo.getId() || null == inquiryPriceDelayedVo.getDeadline()) {
            String message = "调用接口【/api/v1/inquiryPrice/delayedInquiryPrice时，参数id 和 有效时间不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        // 获取当前登录用户
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        ServiceResult serviceResult = inquiryPriceService.delayedInquiryPrice(inquiryPriceDelayedVo, authenticationUser.getToken());
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 询价管理: 议价
     */
    @ApiOperation(value = "询价管理: 议价")
    @ApiImplicitParam(name = "discussionPriceCreatedVo", value = "延时询价入参", required = true, dataType = "DiscussionPriceCreatedVo")
    @RequestMapping(value = "/DiscussionPrice", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> discussionPrice(@RequestBody DiscussionPriceCreatedVo discussionPriceCreatedVo, HttpServletRequest request) {
        ApiResult apiResult;
        if (null == discussionPriceCreatedVo || null == discussionPriceCreatedVo.getInquiryPriceId() ||
                null == discussionPriceCreatedVo.getAcceptPrice() || null == discussionPriceCreatedVo.getAmount() ||
                null == discussionPriceCreatedVo.getDeliveryDate() || null == discussionPriceCreatedVo.getDeadline()) {
            String message = "调用接口【/api/v1/inquiryPrice/delayedInquiryPrice时，参数询价记录id、目标价、议价数量、交货日期、议价有效期不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        // 获取当前登录用户
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        ServiceResult serviceResult = inquiryPriceService.discussionPrice(discussionPriceCreatedVo, authenticationUser.getToken());
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

}
