package com.innovation.ic.cc.web.schedule;

import com.innovation.ic.cc.base.model.cc.Sale;
import com.innovation.ic.cc.base.model.cc.User;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.StaffsTopView;
import com.innovation.ic.cc.base.model.cc.erp9_pvesiteuser.UserClientOwnerTopView;
import com.innovation.ic.cc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author swq
 * @desc 定时更新mysql中的users表数据从sqlserver中的Users同步数据
 * @time 2023年5月5日14:23:25
 */
@Component
public class ImportUsersJobSchedule extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportUsersJobSchedule.class);


    @XxlJob("importUsersJob")
    public void importUsers() {
        try {
            Map<String, Sale> saleMap = new HashMap<>();//存放销售数据
            // 获取sql server中users数据
            List<ParentUsersTopView> userList = usersService.selectList();
            ArrayList<String> saleIds = new ArrayList<>();//list中存入 用户id当做条件
            for (ParentUsersTopView parentUsersTopView : userList) {
                saleIds.add(parentUsersTopView.getId());
            }
            //清空销售数据
            saleService.batchDelete(saleIds);
            //获取用户关联销售id集合
            List<UserClientOwnerTopView> ownerData = userClientOwnerService.selectList(saleIds);
            for (UserClientOwnerTopView data : ownerData) {
                Sale sale = new Sale();
                sale.setId(data.getUserId());//用户id
                sale.setEnterpriseId(data.getClientId());//企业id
                sale.setSaleAccountId(data.getOwnerId());//销售id
                saleMap.put(data.getOwnerId(),sale);
            }
            //通过销售id获取销售相关数据
            List<StaffsTopView> staffsData = staffsService.selectList(ownerData);
            for (StaffsTopView data : staffsData) {
                Sale sale = saleMap.get(data.getAdminId());
                sale.setUserName(data.getName());//姓名
                sale.setPhone(data.getMobile());//手机号
                sale.setEmail(data.getEmail());//邮箱
                saleMap.put(data.getAdminId(),sale);
            }
            //对本地mysql 销售相关数据进行批量添加
            saleService.insertAllNew(saleMap);
            if (userList.size() == 0 || userList.equals(null)) {
                log.info("定时更新mysql中的users表数据从sqlserver中的Users同步数据任务执行出现问题,原因sqlserver数据为空");
                return;
            }
            //先清空 MySQL中users主账号
            Map<String, Object> map = new HashMap<>();
            map.put("father_id", null);
            userService.batchDelete(map);
            //把sqlservice中数据同步到MySQL中user表中
            userService.insertAllNew(userList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("定时更新mysql中的users表数据从sqlserver中的Users同步数据任务执行出现问题,原因:", e);
        }
    }

    @XxlJob("testRedis")
    private void testRedis() {
        try {
            // 获取sql server中users数据
            List<ParentUsersTopView> parentUsersTopViewList = usersService.selectList();

            if (parentUsersTopViewList.size() == 0 || parentUsersTopViewList.equals(null)) {
                log.info("定时更新mysql中的users表数据从sqlserver中的Users同步数据任务执行出现问题,原因sqlserver数据为空");
                return;
            }
            //先清空 MySQL中users主账号
            Map<String, Object> map = new HashMap<>();
            map.put("father_id", null);
            userService.batchDelete(map);
            //把sql server中数据同步到MySQL中user表中
            userService.insertAll(parentUsersTopViewList);

            // 查找user表中所有记录
            ServiceResult<List<User>> serviceResult = userService.findAll();
            if (null != serviceResult && null != serviceResult.getResult()) {
                String keyAsc = RedisStorage.TABLE + RedisStorage.USER +
                        RedisStorage.CREATE_DATE_ASC;
                String keyDesc = RedisStorage.TABLE + RedisStorage.USER +
                        RedisStorage.CREATE_DATE_DESC;

                // 删除旧的数据
                serviceHelper.getRedisManager().del(keyAsc);
                serviceHelper.getRedisManager().del(keyDesc);

                // 向redis中导入数据
                List<User> userList = serviceResult.getResult();
                for (int i = 0; i < userList.size(); i++) {
                    User user = userList.get(i);
                    String value = modelHandler.toXmlStorageString(user);
                    serviceHelper.getRedisManager().zAdd(keyAsc, value, user.getCreateDate().getTime());
                    serviceHelper.getRedisManager().zAdd(keyDesc, value, -user.getCreateDate().getTime());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}