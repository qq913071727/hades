package com.innovation.ic.cc.web.controller.cc;

import com.google.common.base.Strings;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.inventory.InventoryBlurSearchRespPojo;
import com.innovation.ic.cc.base.vo.inventory.BlurSearchInventoryInfoReqVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @desc   库存相关接口
 * @author linuo
 * @time   2023年5月12日15:41:06
 */
@Api(value = "InventoryController", tags = "库存查询")
@RestController
@RequestMapping("/api/v1/inventory")
@Slf4j
public class InventoryController extends AbstractController {
    @ApiOperation(value = "模糊查询库存信息")
    @RequestMapping(value = "/blurSearchInventoryInfo", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<List<InventoryBlurSearchRespPojo>>> blurSearchInventoryInfo(@RequestBody BlurSearchInventoryInfoReqVo blurSearchInventoryInfoReqVo) throws UnsupportedEncodingException {
        ApiResult<List<InventoryBlurSearchRespPojo>> apiResult = new ApiResult<>();

        // 获取调用大赢家接口时的key值
        ServiceResult<String> keyResult = inventoryService.getDyjInterfaceKey();
        String key = keyResult.getResult();
        if(!Strings.isNullOrEmpty(key)){
            // 模糊查询库存信息
            ServiceResult<List<InventoryBlurSearchRespPojo>> serviceResult = inventoryService.blurSearchInventoryInfo(blurSearchInventoryInfoReqVo, key);
            apiResult.setResult(serviceResult.getResult());
        }

        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}