package com.innovation.ic.cc.web.controller.cc;

import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.inventoryCollect.InventoryCollectCancleReqVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   库存收藏相关接口
 * @author linuo
 * @time   2023年5月16日17:12:46
 */
@Api(value = "InventoryCollectController", tags = "库存收藏")
@RestController
@RequestMapping("/api/v1/inventoryCollect")
@Slf4j
public class InventoryCollectController extends AbstractController {
    @ApiOperation(value = "库存收藏数量查询")
    @RequestMapping(value = "/queryInventoryCollectCount", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Integer>> queryInventoryCollectCount() {
        ApiResult<Integer> apiResult = new ApiResult<>();

        ServiceResult<Integer> serviceResult = inventoryCollectService.queryInventoryCollectCount();
        apiResult.setResult(serviceResult.getResult());

        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 收藏库存数据
     * @return 返回结果
     */
    @HystrixCommand
    @ApiOperation(value = "收藏库存数据")
    @ApiImplicitParam(name = "InventoryCollectAddReqVo", value = "收藏、取消收藏库存数据接口的Vo类", required = true, dataType = "InventoryCollectAddReqVo")
    @RequestMapping(value = "/collect", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> collect(@RequestBody InventoryCollectCancleReqVo inventoryCollectCancleReqVo) {
        ServiceResult<Boolean> serviceResult = inventoryCollectService.add(inventoryCollectCancleReqVo);

        ApiResult<Boolean> apiResult = new ApiResult<>(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 取消收藏
     * @return 返回结果
     */
    @HystrixCommand
    @ApiOperation(value = "取消收藏")
    @ApiImplicitParam(name = "InventoryCollectAddReqVo", value = "收藏、取消收藏库存数据接口的Vo类", required = true, dataType = "InventoryCollectAddReqVo")
    @RequestMapping(value = "/cancleCollect", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> cancleCollect(@RequestBody InventoryCollectCancleReqVo inventoryCollectCancleReqVo) {
        ServiceResult<Boolean> serviceResult = inventoryCollectService.cancleCollect(inventoryCollectCancleReqVo);
        ApiResult<Boolean> apiResult = new ApiResult<>(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}