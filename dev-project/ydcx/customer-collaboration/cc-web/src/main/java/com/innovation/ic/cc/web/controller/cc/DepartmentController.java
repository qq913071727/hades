package com.innovation.ic.cc.web.controller.cc;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.Department;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.DepartmentVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

/**
 * 部门API
 */
@Api(value = "部门API", tags = "DepartmentController")
@RestController
@RequestMapping("/api/v1/department")
public class DepartmentController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

    /**
     * 添加部门
     *
     * @param request
     * @param response
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "添加部门")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "当前登录用户id", dataType = "String", required = true),
            @ApiImplicitParam(name = "departmentName", value = "部门名称", dataType = "Integer", required = true)
    })
    @RequestMapping(value = "/addDepartment", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> addDepartment(@RequestBody DepartmentVo departmentVo, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(departmentVo.getDepartmentName()) || !StringUtils.validateParameter(departmentVo.getUserId())) {
            String message = "调用接口【/api/v1/department/addDepartment，参数departmentName,userId不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = departmentService.addDepartment(departmentVo);

        if (serviceResult.getSuccess().equals(Boolean.FALSE)) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(serviceResult.getSuccess());
            apiResult.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            apiResult.setMessage(serviceResult.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询所有部门(当前登录用户下)
     *
     * @param request
     * @param response
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "查询所有部门(当前登录用户下)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "当前登录用户id", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findUserIdAllDepartment", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<Department>>> findUserIdAllDepartment(@RequestBody DepartmentVo departmentVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<List<Department>> apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(departmentVo.getUserId())) {
            String message = "调用接口【/api/v1/department/findUserIdAllDepartment，参数userId不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<Department>> serviceResult = departmentService.findUserIdAllDepartment(departmentVo.getUserId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除当前部门信息
     *
     * @param request
     * @param response
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "删除当前部门信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "部门id", dataType = "Integer", required = true)
    })
    @RequestMapping(value = "/dellDepartment", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> dellDepartment(@RequestBody DepartmentVo departmentVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<Boolean> apiResult;
        if (departmentVo.getId() == null) {
            String message = "调用接口【/api/v1/department/dellDepartment，参数id不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Boolean> serviceResult = departmentService.dellDepartment(departmentVo.getId());

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
