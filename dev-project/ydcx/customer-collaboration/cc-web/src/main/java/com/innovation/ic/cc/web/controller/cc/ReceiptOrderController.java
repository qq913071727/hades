package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.service.cc.ReceiptOrderService;
import com.innovation.ic.cc.base.vo.ReceiptOrderVo;
import com.innovation.ic.cc.base.pojo.ReceiptOrderPojo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.annotation.Resource;
import java.util.List;


/**
 * 订单收货单
 *
 * @author myq
 * @since 1.0.0 2023-05-17
 */
@RestController
@RequestMapping("/api/v1/receiptorder")
@Api(tags = "订单收货单")
public class ReceiptOrderController extends AbstractController {

    @Resource
    private ReceiptOrderService baseService;

    @ApiOperation("分页")
    @PostMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "ReceiptOrderVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<PageInfo<ReceiptOrderPojo>>> page(int pageNo, int pageSize,
                                                   @RequestBody ReceiptOrderVo vo) {

        ServiceResult<PageInfo<ReceiptOrderPojo>> serviceResult = baseService.pages(pageNo, pageSize, vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }

    @ApiOperation("列表")
    @PostMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ReceiptOrderVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<List<ReceiptOrderPojo>>> list(@RequestBody ReceiptOrderVo vo) {

        ServiceResult<List<ReceiptOrderPojo>> serviceResult = baseService.list(vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }


    @GetMapping("{id}")
    @ApiOperation("明细")
    public ResponseEntity<ApiResult<ReceiptOrderPojo>> get(@PathVariable("id") String id) {
        ServiceResult<ReceiptOrderPojo> serviceResult = baseService.get(id);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("保存")
    public ResponseEntity<ApiResult> save(ReceiptOrderVo vo) {
        baseService.save(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(ReceiptOrderVo vo) {
        baseService.update(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @PutMapping("/confirm")
    @ApiOperation("确认收货")
    public ResponseEntity<ApiResult> confirm(List<Long> ids) {
        baseService.confirm(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation("删除")
    public ResponseEntity<ApiResult> delete(String[] ids) {
        baseService.delete(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

}