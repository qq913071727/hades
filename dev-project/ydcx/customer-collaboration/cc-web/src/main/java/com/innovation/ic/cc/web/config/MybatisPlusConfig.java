package com.innovation.ic.cc.web.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.innovation.ic.cc.base.pojo.constant.config.DatabaseGlobal;
import org.apache.ibatis.logging.slf4j.Slf4jImpl;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@EnableTransactionManagement
@Configuration
@MapperScan("com.innovation.ic.cc.base.mapper")
public class MybatisPlusConfig {

    //需要注入的Bean
    @Bean
    public EasySqlInjector easySqlInjector() {
        return new EasySqlInjector();
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    @Bean(name = DatabaseGlobal.CC)
    @ConfigurationProperties(prefix = "spring.datasource.cc")
    public DataSource cc() {
        return DruidDataSourceBuilder.create().build();
    }


    @Bean(name = DatabaseGlobal.ERP9_PVEBCS)
    @ConfigurationProperties(prefix = "spring.datasource.erp9-pve-bcs")
    public DataSource erp9PveBcs() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name = DatabaseGlobal.ERP9_PVECRM)
    @ConfigurationProperties(prefix = "spring.datasource.erp9-pvecrm")
    public DataSource erp9PveCrm() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name = DatabaseGlobal.ERP9_PVESITEUSER)
    @ConfigurationProperties(prefix = "spring.datasource.erp9-pvesiteuser")
    public DataSource erp9PveSiteUser() {
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 动态数据源配置
     *
     * @return
     */
    @Bean
    @Primary
    public DataSource multipleDataSource(@Qualifier(DatabaseGlobal.CC) DataSource cc, @Qualifier(DatabaseGlobal.ERP9_PVEBCS) DataSource erp9PveBcs,
                                         @Qualifier(DatabaseGlobal.ERP9_PVECRM) DataSource erp9PveCrm, @Qualifier(DatabaseGlobal.ERP9_PVESITEUSER) DataSource erp9PveSiteUser) {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DatabaseGlobal.CC, cc);
        targetDataSources.put(DatabaseGlobal.ERP9_PVEBCS, erp9PveBcs);
        targetDataSources.put(DatabaseGlobal.ERP9_PVECRM, erp9PveCrm);
        targetDataSources.put(DatabaseGlobal.ERP9_PVESITEUSER, erp9PveSiteUser);
        dynamicDataSource.setTargetDataSources(targetDataSources);
        // 程序默认数据源，这个要根据程序调用数据源频次，经常把常调用的数据源作为默认
        dynamicDataSource.setDefaultTargetDataSource(cc());
        return dynamicDataSource;
    }

    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(multipleDataSource(cc(), erp9PveBcs(), erp9PveCrm(), erp9PveSiteUser()));
        MybatisConfiguration configuration = new MybatisConfiguration();
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(false);
        configuration.setLogImpl(StdOutImpl.class);
        sqlSessionFactory.setConfiguration(configuration);
        sqlSessionFactory.setGlobalConfig(globalConfiguration());
        // 乐观锁插件
        //PerformanceInterceptor(),OptimisticLockerInterceptor()
        // 分页插件
        sqlSessionFactory.setPlugins(paginationInterceptor());
        return sqlSessionFactory.getObject();
    }

    @Bean
    public GlobalConfig globalConfiguration() {
        GlobalConfig conf = new GlobalConfig();
        // 自定义的注入需要在这里进行配置
        conf.setSqlInjector(easySqlInjector());
        return conf;
    }
}
