package com.innovation.ic.cc.web.controller;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.cc.base.handler.cc.MenuHandler;
import com.innovation.ic.cc.base.pojo.constant.HttpHeader;
import com.innovation.ic.cc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.service.ServiceHelper;
import com.innovation.ic.cc.base.service.cc.*;
import com.innovation.ic.cc.base.value.FilterParamConfig;
import com.innovation.ic.cc.base.value.RedisParamConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 抽象controller
 */
public abstract class AbstractController {
    @Autowired
    protected ServiceHelper serviceHelper;

    @Autowired
    protected ClientService clientService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected SmsInfoService smsInfoService;

    @Autowired
    protected FilterParamConfig filterParamConfig;

    @Resource
    protected MapUserRoleService mapUserRoleService;

    @Resource
    protected MenuTreeService menuTreeService;

    @Resource
    protected RoleService roleService;

    @Resource
    protected MenuHandler menuHandler;

    @Resource
    protected LoginService loginService;

    @Autowired
    protected DepartmentService departmentService;

    @Autowired
    protected UserManagementService userManagementService;

    @Autowired
    protected InquiryPriceService inquiryPriceService;

//    @Autowired
//    protected QuotedPriceService quotedPriceService;
//
//    @Autowired
//    protected DiscussionPriceService discussionPriceService;

    @Autowired
    protected UploadFileService uploadFileService;

    @Autowired
    protected MyCompanyService myCompanyService;

    @Autowired
    protected DictionariesService dictionariesService;

    @Autowired
    protected InventoryService inventoryService;

    @Autowired
    protected InventoryCollectService inventoryCollectService;

    @Autowired
    protected UserLoginLogService userLoginLogService;

    @Autowired
    protected RedisParamConfig redisParamConfig;

    @Autowired
    protected PostSaleService postSaleService;

    /**
     * 从redis中根据token获取AuthenticationUser对
     *
     * @param request
     */
    protected AuthenticationUser getAuthenticationUser(HttpServletRequest request) {
        String token;
        String header = request.getHeader(filterParamConfig.getToken());
        if (StringUtils.isEmpty(header)) {
            return null;
        } else {
            String[] headerArray = header.split(HttpHeader.TOKEN_SPLIT);
            if (null == headerArray || headerArray.length != 2) {
                return null;
            } else {
                token = headerArray[1];
                Object tokenObject = serviceHelper.getRedisManager().get(RedisStorage.TOKEN_PREFIX + token);
                if (null == tokenObject) {
                    return null;
                }
                return JSON.parseObject(tokenObject.toString(), AuthenticationUser.class);
            }
        }
    }

    /**
     * 将AuthenticationUser对象存储到redis中，key为token，value是json字符串
     * @param authenticationUser authenticationUser
     */
    protected void setAuthenticationUser(AuthenticationUser authenticationUser) {
        serviceHelper.getRedisManager().set(RedisStorage.TOKEN_PREFIX + authenticationUser.getToken(), JSON.toJSONString(authenticationUser));
        serviceHelper.getRedisManager().expire(RedisStorage.TOKEN_PREFIX + authenticationUser.getToken(), redisParamConfig.getTokenTimeout());
    }

    /**
     * @Description: 获取用户ID
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/616:24
     */
    protected String getUserId(HttpServletRequest request) {
        AuthenticationUser authenticationUser = getAuthenticationUser(request);
        return authenticationUser.getId();
    }
}
