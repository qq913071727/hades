package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.service.cc.ReceiptShippingService;
import com.innovation.ic.cc.base.vo.ReceiptShippingVo;
import com.innovation.ic.cc.base.pojo.ReceiptShippingPojo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.annotation.Resource;
import java.util.List;


/**
 * 收货单运单
 *
 * @author myq
 * @since 1.0.0 2023-05-17
 */
@RestController
@RequestMapping("/api/v1/receiptshipping")
@Api(tags = "收货单运单")
public class ReceiptShippingController extends AbstractController {

    @Resource
    private ReceiptShippingService baseService;

    @ApiOperation("分页")
    @PostMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "ReceiptShippingVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<PageInfo<ReceiptShippingPojo>>> page(int pageNo, int pageSize,
                                                   @RequestBody ReceiptShippingVo vo) {

        ServiceResult<PageInfo<ReceiptShippingPojo>> serviceResult = baseService.pageInfo(pageNo, pageSize, vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }

    @ApiOperation("列表")
    @PostMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ReceiptShippingVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<List<ReceiptShippingPojo>>> list(@RequestBody ReceiptShippingVo vo) {

        ServiceResult<List<ReceiptShippingPojo>> serviceResult = baseService.list(vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }




    @GetMapping("{id}")
    @ApiOperation("明细")
    public ResponseEntity<ApiResult<ReceiptShippingPojo>> get(@PathVariable("id") String id) {
        ServiceResult<ReceiptShippingPojo> serviceResult = baseService.get(id);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(),"ok"), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("保存")
    public ResponseEntity<ApiResult> save(ReceiptShippingVo vo) {
        baseService.save(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(ReceiptShippingVo vo) {
        baseService.update(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @PutMapping("/confirm")
    @ApiOperation("确认收货")
    public ResponseEntity<ApiResult> confirm(List<Long> ids) {
        baseService.confirm(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation("删除")
    public ResponseEntity<ApiResult> delete(String[] ids) {
        baseService.delete(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"),HttpStatus.OK);
    }

}