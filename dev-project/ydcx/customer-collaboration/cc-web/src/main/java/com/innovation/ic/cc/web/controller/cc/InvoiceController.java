package com.innovation.ic.cc.web.controller.cc;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.invoice.GetInvoiceDetailReqVo;
import com.innovation.ic.cc.base.vo.invoice.NoticeBillingReqVo;
import com.innovation.ic.cc.base.vo.invoice.SetBillingDateReqVo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc   发票管理相关接口
 * @author linuo
 * @time   2023年5月17日14:18:39
 */
@Api(value = "InvoiceController", tags = "发票管理")
@RestController
@RequestMapping("/api/v1/invoice")
@Slf4j
public class InvoiceController {
    @ApiOperation(value = "发票列表查询(参数type:1待开票、2已通知、3已完成)")
    @RequestMapping(value = "/queryInvoiceList/{type}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<List<JSONObject>>> queryInvoiceList(@PathVariable("type") Integer type) {
        if(type == null){
            String message = "调用接口【/api/v1/invoice/queryInvoiceList】时，参数type不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ApiResult<List<JSONObject>> apiResult = new ApiResult<>();
        List<JSONObject> list = new ArrayList<>();
        JSONObject json;

        // 待开票、已通知
        if(type == 1 || type == 2) {
            json = new JSONObject();
            // 订单项
            json.put("orderItem", "DD22020000295-001");
            // 收货日期
            json.put("receiptDate", "2022-10-05");
            // 型号
            json.put("partNumber", "H1");
            // 数量
            json.put("count", "10");
            // 单价
            json.put("unitPrice", "10.00");
            // 订单金额
            json.put("orderPrice", "100.00");
            // 发票类型
            json.put("invoiceType", "增值税专用发票");
            list.add(json);

            json = new JSONObject();
            // 订单项
            json.put("orderItem", "DD22020000295-002");
            // 收货日期
            json.put("receiptDate", "2022-10-05");
            // 型号
            json.put("partNumber", "H1");
            // 数量
            json.put("count", "10");
            // 单价
            json.put("unitPrice", "10.00");
            // 订单金额
            json.put("orderPrice", "100.00");
            // 发票类型
            json.put("invoiceType", "增值税专用发票");
            list.add(json);
        }

        // 已完成
        if(type == 3){
            json = new JSONObject();
            // 发票号
            json.put("invoiceNumber", "FP00000001");
            // 开票日期
            json.put("drawingDate", "2022-10-05");
            // 开票金额
            json.put("drawingAmount", "100000");
            // 发票类型
            json.put("invoiceType", "增值税专用发票");
            // 运单号
            json.put("waybillNumber", "SF00000001");

            list.add(json);

            json = new JSONObject();
            // 发票号
            json.put("invoiceNumber", "FP00000002");
            // 开票日期
            json.put("drawingDate", "2022-10-04");
            // 开票金额
            json.put("drawingAmount", "200000");
            // 发票类型
            json.put("invoiceType", "增值税专用发票");
            // 运单号
            json.put("waybillNumber", "SF00000002");

            list.add(json);

            json = new JSONObject();
            // 发票号
            json.put("invoiceNumber", "FP00000003");
            // 开票日期
            json.put("drawingDate", "2022-10-03");
            // 开票金额
            json.put("drawingAmount", "300000");
            // 发票类型
            json.put("invoiceType", "增值税专用发票");
            // 运单号
            json.put("waybillNumber", "SF00000003");

            list.add(json);

            json = new JSONObject();
            // 发票号
            json.put("invoiceNumber", "FP00000004");
            // 开票日期
            json.put("drawingDate", "2022-10-02");
            // 开票金额
            json.put("drawingAmount", "6563000");
            // 发票类型
            json.put("invoiceType", "增值税专用发票");
            // 运单号
            json.put("waybillNumber", "SF00000004");

            list.add(json);
        }

        apiResult.setResult(list);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @HystrixCommand
    @ApiOperation(value = "通知开票")
    @ApiImplicitParam(name = "NoticeBillingReqVo", value = "通知开票接口的Vo类", required = true, dataType = "NoticeBillingReqVo")
    @RequestMapping(value = "/noticeBilling", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> noticeBilling(@RequestBody NoticeBillingReqVo noticeBillingReqVo) {
        if(noticeBillingReqVo.getOrderItemList() == null || noticeBillingReqVo.getOrderItemList().size() == 0){
            String message = "通知开票操作时，订单项不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setResult(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @HystrixCommand
    @ApiOperation(value = "设置开票日")
    @ApiImplicitParam(name = "SetBillingDateReqVo", value = "设置开票日接口的Vo类", required = true, dataType = "NoticeBillingReqVo")
    @RequestMapping(value = "/setBillingDate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> setBillingDate(@RequestBody SetBillingDateReqVo setBillingDateReqVo) {
        if(setBillingDateReqVo.getOrderItemList() == null || setBillingDateReqVo.getOrderItemList().size() == 0){
            String message = "设置开票日操作时，订单项不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setResult(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @HystrixCommand
    @ApiOperation(value = "获取发票详情")
    @ApiImplicitParam(name = "StatementAccountDetailReqVo", value = "获取对账单详情接口的Vo类", required = true, dataType = "StatementAccountDetailReqVo")
    @RequestMapping(value = "/getInvoiceDetail", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<JSONObject>> getInvoiceDetail(@RequestBody GetInvoiceDetailReqVo getInvoiceDetailReqVo) {
        if(Strings.isNullOrEmpty(getInvoiceDetailReqVo.getInvoiceNumber())){
            String message = "获取发票详情时，发票号不能为空";
            log.warn(message);
            return new ResponseEntity<>(ApiResult.error(HttpStatus.BAD_REQUEST.value(), message), HttpStatus.BAD_REQUEST);
        }

        ApiResult<JSONObject> apiResult = new ApiResult<>();
        JSONObject result = new JSONObject();

        // 发票号
        result.put("invoiceNumber", "FP00000001");
        // 开票金额
        result.put("drawingAmount", "100000");
        // 公司名称
        result.put("companyName", "测试专用有限责任公司");
        // 纳税人识别号
        result.put("TaxpayerIdNo", "9000000000000001");
        // 地址
        result.put("address", "北京市海淀区1+1大厦");
        // 电话
        result.put("mobile", "18200000001");
        // 开户银行
        result.put("openingBank", "北京市专用开户行");
        // 开户账号
        result.put("openingCardNo", "6220 0022 2220 001");
        // 发票类型
        result.put("invoiceType", "增值税专用发票");
        // 税率
        result.put("rate", "13%");
        // 金额
        result.put("amount", "26,100.00");
        // 税额
        result.put("taxAmount", "3000.00");

        List<JSONObject> list = new ArrayList<>();

        JSONObject json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-001");
        // 货物名称
        json.put("goodsName", "电子元器件");
        // 型号
        json.put("partNumber", "AHDJ2");
        // 单位
        json.put("unit", "个");
        // 数量
        json.put("count", "1000");
        // 单价
        json.put("unitPrice", "10.000000");
        // 金额
        json.put("price", "10000.00");
        // 税率
        result.put("rate", "13%");
        // 税额
        result.put("taxAmount", "130.00");
        list.add(json);

        json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-002");
        // 货物名称
        json.put("goodsName", "电子元器件");
        // 型号
        json.put("partNumber", "JDHHHD1");
        // 单位
        json.put("unit", "个");
        // 数量
        json.put("count", "1000");
        // 单价
        json.put("unitPrice", "10.000000");
        // 金额
        json.put("price", "10000.00");
        // 税率
        result.put("rate", "13%");
        // 税额
        result.put("taxAmount", "130.00");
        list.add(json);

        json = new JSONObject();
        // 订单项
        json.put("orderItem", "DD22020000295-003");
        // 货物名称
        json.put("goodsName", "电子元器件");
        // 型号
        json.put("partNumber", "DJJJD");
        // 单位
        json.put("unit", "个");
        // 数量
        json.put("count", "1000");
        // 单价
        json.put("unitPrice", "10.000000");
        // 金额
        json.put("price", "10000.00");
        // 税率
        result.put("rate", "13%");
        // 税额
        result.put("taxAmount", "130.00");
        list.add(json);

        // 订单集合
        result.put("data", list);

        // 价税合计(大写)
        result.put("totalValueTaxUpper", "贰万陆仟壹佰圆整");

        // 价税合计(小写)
        result.put("totalValueTaxLower", "261000.00");

        apiResult.setResult(result);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}