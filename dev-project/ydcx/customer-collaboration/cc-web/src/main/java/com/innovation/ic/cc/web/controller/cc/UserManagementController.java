package com.innovation.ic.cc.web.controller.cc;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.UserManagement;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.pojo.variable.UserManagementPojo;
import com.innovation.ic.cc.base.vo.UserManagementVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 人员管理API
 */
@Api(value = "人员管理API", tags = "UserManagementController")
@RestController
@RequestMapping("/api/v1/userManagement")
@DefaultProperties(defaultFallback = "defaultFallback")
public class UserManagementController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(UserManagementController.class);

    /**
     * 添加人员管理数据
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "添加人员管理数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "登录用户id", dataType = "String", required = true),
            @ApiImplicitParam(name = "departmentId", value = "部门id", dataType = "Integer", required = true),
            @ApiImplicitParam(name = "managedUserId", value = "被管理用户的id", dataType = "List", required = true)
    })
    @RequestMapping(value = "/addUserManagement", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> addUserManagement(@RequestBody UserManagementVo userManagementVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(userManagementVo.getUserId()) || userManagementVo.getDepartmentId() == null || userManagementVo.getManagedUserId() == null || userManagementVo.getManagedUserId().size() == 0) {
            String message = "调用接口【/api/v1/userManagement/addUserManagement，参数userId,departmentId,managedUserId不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //进行批量添加人员数据
        ServiceResult<Boolean> serviceResult = userManagementService.addUserManagement(userManagementVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询当前部门下用户的人员管理数据
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "查询当前部门下用户的人员管理数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "登录用户id", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findDepartmentUser", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<UserManagementPojo>>> findDepartmentUser(@RequestBody UserManagementVo userManagementVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<List<UserManagementPojo>> apiResult;
        if (!StringUtils.validateParameter(userManagementVo.getUserId())) {
            String message = "调用接口【/api/v1/userManagement/findDepartmentUser，参数userId不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //查询当前部门下用户的人员管理数据
        ServiceResult<List<UserManagementPojo>> serviceResult = userManagementService.findDepartmentUser(userManagementVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除人员管理数据
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "删除人员管理数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "人员管理主键id", dataType = "Integer", required = true)
    })
    @RequestMapping(value = "/dellUserManagement", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<UserManagement>>> dellUserManagement(@RequestBody UserManagementVo userManagementVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<List<UserManagement>> apiResult;
        if (userManagementVo.getId() == null) {
            String message = "调用接口【/api/v1/userManagement/dellUserManagement，参数id不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //删除人员管理数据
        ServiceResult<Boolean> serviceResult = userManagementService.dellUserManagement(userManagementVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
