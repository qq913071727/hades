package com.innovation.ic.cc.web.config;

import com.innovation.ic.cc.base.value.XxljobParamConfig;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;


/**
 * xxl配置类
 */

@Slf4j
public class XxlJobConfig {
    @Resource
    XxljobParamConfig xxljobParamConfig;

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        log.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(xxljobParamConfig.getAdminAddresses());
        xxlJobSpringExecutor.setAppname(xxljobParamConfig.getAppname());
        xxlJobSpringExecutor.setPort(xxljobParamConfig.getPort());
        xxlJobSpringExecutor.setAccessToken(xxljobParamConfig.getAccessToken());
        xxlJobSpringExecutor.setLogPath(xxljobParamConfig.getLogPath());
        xxlJobSpringExecutor.setLogRetentionDays(xxljobParamConfig.getLogRetentionDays());
        return xxlJobSpringExecutor;
    }

}