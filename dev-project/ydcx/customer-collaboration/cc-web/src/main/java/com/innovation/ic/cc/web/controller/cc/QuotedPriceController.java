//package com.innovation.ic.cc.web.controller.cc;
//
//import com.github.pagehelper.PageInfo;
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.cc.base.pojo.variable.ApiResult;
//import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
//import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceAndInquiryPriceRecordPojo;
//import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPricePojo;
//import com.innovation.ic.cc.base.pojo.variable.InquiryPrice.InquiryPriceRecordPojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceAndQuotedPriceRecordPojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPricePojo;
//import com.innovation.ic.cc.base.pojo.variable.quotedPrice.QuotedPriceRecordPojo;
//import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPriceDeleteVo;
//import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPricePageListVo;
//import com.innovation.ic.cc.base.vo.InquiryPrice.InquiryPriceRecordListVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPriceAdoptionOrNotAcceptedVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPricePageListVo;
//import com.innovation.ic.cc.base.vo.quotedPrice.QuotedPriceRecordListVo;
//import com.innovation.ic.cc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import org.simpleframework.xml.core.Validate;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
///**
// * @author zengqinglong
// * @desc 报价管理
// * @Date 2023/2/14 13:56
// **/
//@Api(value = "报价管理", tags = "QuotedPriceController")
//@RestController
//@RequestMapping("/api/v1/quotedPrice")
//public class QuotedPriceController extends AbstractController {
//    private static final Logger log = LoggerFactory.getLogger(QuotedPriceController.class);
//
//    /**
//     * 询价管理: 我收到的报价: 报价列表
//     *
//     * @param quotedPricePageListVo
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation(value = "询价管理: 我收到的报价: 报价列表")
//    @ApiImplicitParam(name = "quotedPricePageListVo", value = "报价列表", required = true, dataType = "QuotedPricePageListVo")
//    @RequestMapping(value = "/pageList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<PageInfo<QuotedPricePojo>>> pageList(@RequestBody @Validate QuotedPricePageListVo quotedPricePageListVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult<PageInfo<QuotedPricePojo>> apiResult;
//
//        if (null == quotedPricePageListVo) {
//            String message = "调用接口【/api/v1/quotedPrice/pageList，参数quotedPricePageListVo不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        // 获取当前登录用户
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser) {
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//        PageInfo<QuotedPricePojo> quotedPricePojoPageInfo = quotedPriceService.pageList(quotedPricePageListVo, authenticationUser);
//        apiResult = new ApiResult<>();
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setResult(quotedPricePojoPageInfo);
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 询价管理: 我收到的报价: 一键采纳
//     *
//     * @param ids
//     * @return
//     */
//    @ApiOperation(value = "询价管理: 我收到的报价: 一键采纳")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "询价主键", dataType = "String", required = true),
//    })
//    @RequestMapping(value = "/adopt", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> adopt(@PathVariable("ids") List<String> ids) {
//        ApiResult apiResult;
//        if (ids.size() == 0) {
//            String message = "调用接口【/api/v1/quotedPrice/pageList，参数quotedPricePageListVo不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        quotedPriceService.adopt(ids);
//        apiResult = new ApiResult<>();
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     *  询价管理: 我收到的报价: 报价详情
//     *
//     * @param id
//     * @return
//     */
//    @ApiOperation(value = "询价管理: 我收到的报价: 报价详情")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "报价主键", dataType = "String", required = true),
//    })
//    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<QuotedPriceAndQuotedPriceRecordPojo>> details(@PathVariable("id") String id) {
//        ApiResult<QuotedPriceAndQuotedPriceRecordPojo> apiResult;
//        if (!StringUtils.validateParameter(id)) {
//            String message = "调用接口【/api/v1/quotedPrice/details/{id}时，参数id不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        QuotedPriceAndQuotedPriceRecordPojo quotedPriceAndQuotedPriceRecordPojo  = quotedPriceService.details(id);
//
//        apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setResult(quotedPriceAndQuotedPriceRecordPojo);
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 询价管理: 我收到的报价: 报价详情: 筛选品牌型号
//     *
//     * @param quotedPriceRecordListVo
//     * @return
//     */
//    @ApiOperation(value = "询价管理: 我收到的报价: 报价详情: 筛选品牌型号")
//    @ApiImplicitParam(name = "quotedPriceRecordListVo", value = "筛选品牌型号", required = true, dataType = "QuotedPriceRecordListVo")
//    @RequestMapping(value = "/filterBrandModelList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<List<QuotedPriceRecordPojo>>> filterBrandModelList(@RequestBody  @Validate QuotedPriceRecordListVo quotedPriceRecordListVo){
//        ApiResult<List<QuotedPriceRecordPojo>> apiResult;
//        if (quotedPriceRecordListVo == null) {
//            String message = "调用接口【/api/v1/quotedPrice/filterBrandModelList，参数quotedPriceRecordListVo不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        List<QuotedPriceRecordPojo> inquiryPriceRecordPojoList = quotedPriceService.filterBrandModelList(quotedPriceRecordListVo);
//
//        apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setResult(inquiryPriceRecordPojoList);
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 询价管理: 我收到的报价: 报价详情: 一键采纳/不接受
//     *
//     * @param quotedPriceAdoptionOrNotAcceptedVo
//     * @return
//     */
//    @ApiOperation(value = "询价管理: 我收到的报价: 报价详情: 一键采纳/不接受")
//    @ApiImplicitParam(name = "quotedPriceAdoptionOrNotAcceptedVo", value = "一键采纳/不接受", required = true, dataType = "QuotedPriceAdoptionOrNotAcceptedVo")
//    @RequestMapping(value = "/adoptionOrNotAccepted", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> adoptionOrNotAccepted(@RequestBody @Validate QuotedPriceAdoptionOrNotAcceptedVo quotedPriceAdoptionOrNotAcceptedVo) {
//        ApiResult apiResult;
//        if (quotedPriceAdoptionOrNotAcceptedVo == null) {
//            String message = "调用接口【/api/v1/quotedPrice/adoptionOrNotAccepted时，参数quotedPriceAdoptionOrNotAcceptedVo不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        quotedPriceService.adoptionOrNotAccepted(quotedPriceAdoptionOrNotAcceptedVo);
//
//        apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//}
