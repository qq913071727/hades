package com.innovation.ic.cc.web.controller.cc_data;

import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "售后管理表数据到redis中", tags = "InitPostSaleController")
@RestController
@RequestMapping("/api/v2/postsale")
@Slf4j
public class InitPostSaleController extends AbstractController {
    @ApiOperation(value = "将post_sale表的数据导入到redis")
    @GetMapping("initPostSaleData")
    public ResponseEntity<ApiResult> initUserData() {
        ApiResult apiResult = new ApiResult();
        postSaleService.initPostSaleData();
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
