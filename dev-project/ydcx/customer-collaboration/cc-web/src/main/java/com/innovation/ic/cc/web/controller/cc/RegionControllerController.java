package com.innovation.ic.cc.web.controller.cc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.Region;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.RegionService;
import com.innovation.ic.cc.base.service.cc.ShippingAddressService;
import com.innovation.ic.cc.base.vo.address.RegionQueryVo;
import com.innovation.ic.cc.base.vo.address.ShippingAddressVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 国家地区
 */
@Api(value = "国家地区API", tags = "RegionControllerController")
@RestController
@RequestMapping("/api/v1/region")
@Slf4j
public class RegionControllerController extends AbstractController {


    @Resource
    private RegionService regionService;


    @ApiOperation(value = "查询国家地区")
    @GetMapping(value = "/getRegionList")
    @ResponseBody
    public ResponseEntity<ApiResult> getRegionList(RegionQueryVo regionQueryVo) {
        ApiResult apiResult = new ApiResult<>();
        ServiceResult<IPage<Region>> serviceResult = regionService.getRegionList(regionQueryVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


}
