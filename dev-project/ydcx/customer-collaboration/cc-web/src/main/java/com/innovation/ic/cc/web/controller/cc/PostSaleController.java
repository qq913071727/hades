package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.model.cc.PostSale;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.vo.PostSaleVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author post_sale
 */
@Api(value = "售后管理", tags = "PostSaleController")
@RestController
@RequestMapping("/api/v1/postSale")
@Slf4j
public class PostSaleController extends AbstractController {

    /**
     * 分页展示售后管理数据
     *
     * @param postSaleVo
     * @param request
     * @param response
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "分页展示售后管理数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "当前页", dataType = "Integer", required = true),
            @ApiImplicitParam(name = "pageSize", value = "显示几条", dataType = "Integer", required = true)
    })
    @RequestMapping(value = "/pagePostSale", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<PostSale>>> pagePostSale(@RequestBody PostSaleVo postSaleVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<PageInfo<PostSale>> apiResult;
        if (postSaleVo.getPageSize() == null || postSaleVo.getPageNo() == null) {
            String message = "调用接口【/api/v1/postSale/pagePostSale时，参数pageNo、pageSize不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<PageInfo<PostSale>> serviceResult = postSaleService.pagePostSale(postSaleVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

}
