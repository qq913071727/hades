package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cc.base.pojo.SysAnnouncementPojo;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.SysAnnouncementService;
import com.innovation.ic.cc.base.vo.SysAnnouncementVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * 系统公告
 * @author myq
 * @since 1.0.0 2023-02-20
 */
@RestController
@RequestMapping("/api/v1/sysannouncement")
@Api(tags = "系统公告")
public class SysAnnouncementController extends AbstractController {
    @Resource
    private SysAnnouncementService sysAnnouncementService;

    @ApiOperation("分页")
    @PostMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "SysAnnouncementVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<PageInfo<SysAnnouncementPojo>>> page(int pageNo, int pageSize,
                                                                         @RequestBody SysAnnouncementVo vo) {

        ServiceResult<PageInfo<SysAnnouncementPojo>> serviceResult = sysAnnouncementService.pageInfo(pageNo, pageSize, vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @ApiOperation("列表")
    @PostMapping("list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SysAnnouncementVo", value = "查询参数", dataType = "json", required = false)
    })
    public ResponseEntity<ApiResult<List<SysAnnouncementPojo>>> list(@RequestBody SysAnnouncementVo vo) {

        ServiceResult<List<SysAnnouncementPojo>> serviceResult = sysAnnouncementService.list(vo);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @GetMapping("{id}")
    @ApiOperation("明细")
    public ResponseEntity<ApiResult<SysAnnouncementPojo>> get(@PathVariable("id") String id) throws Exception {
        ServiceResult<SysAnnouncementPojo> serviceResult = sysAnnouncementService.get(id);

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("保存")
    public ResponseEntity<ApiResult> save(SysAnnouncementVo vo) throws Exception {
        sysAnnouncementService.save(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(SysAnnouncementVo vo) throws Exception {
        sysAnnouncementService.update(vo);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @DeleteMapping
    @ApiOperation("删除")
    public ResponseEntity<ApiResult> delete(String[] ids) {
        sysAnnouncementService.delete(ids);

        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }

    @ApiOperation("公告信息")
    @GetMapping()
    public ResponseEntity<ApiResult<SysAnnouncementPojo>> getAnnouncementInfo() throws Exception {

        ServiceResult<SysAnnouncementPojo> serviceResult = sysAnnouncementService.getAnnouncementInfo();

        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), "ok"), HttpStatus.OK);
    }
}