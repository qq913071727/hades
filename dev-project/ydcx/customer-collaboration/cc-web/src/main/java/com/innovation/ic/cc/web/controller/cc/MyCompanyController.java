package com.innovation.ic.cc.web.controller.cc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cc.base.model.cc.ErpMyCompanyName;
import com.innovation.ic.cc.base.pojo.MyCompanyPojo;
import com.innovation.ic.cc.base.pojo.variable.ApiResult;
import com.innovation.ic.cc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.cc.base.pojo.variable.ServiceResult;
import com.innovation.ic.cc.base.service.cc.ErpMyCompanyNameService;
import com.innovation.ic.cc.base.vo.MyCompanyVo;
import com.innovation.ic.cc.base.vo.SearchCompanyVo;
import com.innovation.ic.cc.base.vo.SearchErpMyCompanyNameVo;
import com.innovation.ic.cc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;


/**
 * @author Administrator
 */
@Api(value = "我的公司", tags = "MyCompanyController")
@RestController
@RequestMapping("/api/v1/myCompany")
@Slf4j
public class MyCompanyController extends AbstractController {


    @Resource
    private ErpMyCompanyNameService erpMyCompanyNameService;

    @ApiOperation(value = "模糊搜索公司")
    @GetMapping("searchCompany")
    public ResponseEntity<ApiResult<List<ErpMyCompanyName>>> searchCompany(SearchErpMyCompanyNameVo searchErpMyCompanyNameVo) {
        ApiResult<List<ErpMyCompanyName>> apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(searchErpMyCompanyNameVo.getName())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("搜索名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (searchErpMyCompanyNameVo.getName().length() < 3) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("最少为三个字符");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<ErpMyCompanyName>> serviceResult = erpMyCompanyNameService.findByLikeCompanyName(searchErpMyCompanyNameVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation(value = "校验公司是否已经存在注册关系")
    @GetMapping("checkRegistrationCompany")
    public ResponseEntity<ApiResult> checkRegistrationCompany(MyCompanyVo myCompanyVo, HttpServletRequest request) {
        ApiResult apiResult;

        if (!StringUtils.validateParameter(myCompanyVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("搜索名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        myCompanyVo.setEnterpriseId(authenticationUser.getEpId());
        ServiceResult serviceResult = myCompanyService.checkRegistrationCompany(myCompanyVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }




    @ApiImplicitParam(name = "myCompanyVo", value = "我的公司vo类", required = true, dataType = "MyCompanyVo")
    @ApiOperation(value = "添加公司")
    @PostMapping("registerCompany")
    public ResponseEntity<ApiResult> registerCompany(@RequestBody MyCompanyVo myCompanyVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(myCompanyVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("公司名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getCountryRegionId())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("所在地区不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }


        if (!StringUtils.validateParameter(myCompanyVo.getCompanyTypeId())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("公司类型不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }


        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        myCompanyVo.setCreateId(authenticationUser.getId());
        myCompanyVo.setEnterpriseId(authenticationUser.getEpId());
        myCompanyVo.setCreatorUserName(authenticationUser.getUsername());
        ServiceResult serviceResult = myCompanyService.registerCompany(myCompanyVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "searchCompanyVo", value = "我的公司vo类", required = true, dataType = "SearchCompanyVo")
    @ApiOperation(value = "分页查询公司")
    @PostMapping("findByMyCompany")
    public ResponseEntity<ApiResult<PageInfo<MyCompanyPojo>>> findByMyCompany(@RequestBody SearchCompanyVo searchCompanyVo, HttpServletRequest request) {
        ApiResult<PageInfo<MyCompanyPojo>> apiResult;

        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        searchCompanyVo.setEnterpriseId(authenticationUser.getEpId());
        searchCompanyVo.setEnterpriseName(authenticationUser.getEpName());
        ServiceResult<PageInfo<MyCompanyPojo>> serviceResult = myCompanyService.findByMyCompany(searchCompanyVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }




    @ApiImplicitParam(name = "id", value = "主键id", required = true, dataType = "String")
    @ApiOperation(value = "获取公司详情")
    @GetMapping("getDetail")
    public ResponseEntity<ApiResult<MyCompanyPojo>> getDetail(Integer id) {
        ApiResult<MyCompanyPojo> apiResult = new ApiResult<>();
        if (id == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<MyCompanyPojo> serviceResult = myCompanyService.getDetail(id);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "relationEnterpriseId", value = "关联主键id", dataType = "Integer")
    @ApiOperation(value = "删除公司关联")
    @GetMapping("deactivateMyCompany")
    public ResponseEntity<ApiResult> findByMyCompany(Integer relationEnterpriseId) throws IOException {
        ApiResult apiResult = new ApiResult();
        ServiceResult serviceResult = myCompanyService.deactivateMyCompany(relationEnterpriseId);
        apiResult.setSuccess(serviceResult.getSuccess());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }




}