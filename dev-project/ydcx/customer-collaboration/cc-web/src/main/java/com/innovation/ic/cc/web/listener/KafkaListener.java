//package com.innovation.ic.cc.web.listener;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.innovation.ic.cc.base.handler.cc.ModelHandler;
//import com.innovation.ic.cc.base.model.cc.User;
//import com.innovation.ic.cc.base.pojo.constant.DatabaseOperationType;
//import com.innovation.ic.cc.base.pojo.constant.CcTableName;
//import com.innovation.ic.cc.base.pojo.constant.config.DatabaseGlobal;
//import com.innovation.ic.cc.base.pojo.constant.handler.RedisStorage;
//import com.innovation.ic.cc.base.service.ServiceHelper;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.List;
//
///**
// * 当出现数据库操作后，调用这个监听器的方法
// */
//@Component
//@EnableScheduling
//public class KafkaListener {
//    private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    @Resource
//    private ServiceHelper serviceHelper;
//
//    @Resource
//    private ModelHandler modelHandler;
//
//    /**
//     * 当出现数据库操作后，调用这个方法
//     *
//     * @param consumer 数据库变动内容
//     */
//    @org.springframework.kafka.annotation.KafkaListener(topics = "sc")
//    public void listen(ConsumerRecord<?, ?> consumer) {
//        logger.info("topic【" + consumer.topic() + "】，key【" + consumer.key() + "】，" +
//                "分区位置【" + consumer.partition() + "】，下标【" + consumer.offset() + "】，" +
//                "value【" + consumer.value() + "】");
//
//        String json = (String) consumer.value();
//        JSONObject metaDataJsonObject = JSONObject.parseObject(json);
//        // 数据库名称
//        String database = metaDataJsonObject.getString("database");
//        // 表名称
//        String table = metaDataJsonObject.getString("table");
//        // 操作类型
//        String type = metaDataJsonObject.getString("type");
//        // 操作数据
//        JSONArray dataJsonArray = metaDataJsonObject.getJSONArray("data");
//
//        if (null != dataJsonArray && dataJsonArray.size() > 0) {
//            JSONObject dataJsonObject = dataJsonArray.getJSONObject(0);
//            // sc数据库
//            if (DatabaseGlobal.CC.equals(database)) {
//
//            }
//        }
//    }
//}