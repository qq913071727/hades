package com.innovation.ic.cc.web.config;

import com.innovation.ic.cc.base.pojo.constant.config.DatabaseGlobal;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
public class DataSourceAspect {

    private static final Logger log = LoggerFactory.getLogger(DataSourceAspect.class);

    @Pointcut("execution(* com.innovation.ic.cc.base.service.cc.impl..*.*(..))")
    private void ccAspect() {
    }


    @Pointcut("execution(* com.innovation.ic.cc.base.service.erp9_pvebcs.impl..*.*(..))")
    private void erp9PveBcsAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.cc.base.service.erp9_pvecrm.impl..*.*(..))")
    private void erp9PveCrmAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.cc.base.service.erp9_pvesiteuser.impl..*.*(..))")
    private void erp9PveSiteUserAspect() {
    }

    @Before("ccAspect()")
    public void sc() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.CC);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.CC);
    }

    @Before("erp9PveBcsAspect()")
    public void erp9PveBcs() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVEBCS);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVEBCS);
    }

    @Before("erp9PveCrmAspect()")
    public void erp9PveCrm() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVECRM);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVECRM);
    }

    @Before("erp9PveSiteUserAspect()")
    public void erp9PveSiteUser() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVESITEUSER);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVESITEUSER);
    }

}
