package com.innovation.ic.sc.gateway.schedule;

import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.global.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Set;

/**
 * @desc   定时更新本地Context中的clientId集合
 * @author linuo
 * @time   2022年5月25日10:23:25
 */
@Configuration
@EnableScheduling
public class ImportRedisClientIdsTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportRedisClientIdsTask.class);

    /**
     * 1. 删除地Context中的clientId集合
     * 2. 从redis中获取cilentId集合保存到本地Context中
     */
    @Scheduled(fixedRate = 60 * 1000)
    private void importRedisClientIds() {
        try {
            log.debug("定时更新本地Context中的clientId集合任务开始");
            Set<String> clientSet = (Set<String>) Context.get(RedisStorage.CLIENT);
            if(clientSet!= null && !clientSet.isEmpty()){
                log.debug("原Context上下文内容中有clientId集合信息,先删除原数据");
                Context.remove(RedisStorage.CLIENT);
            }
            // 将客户端信息集合存储到上下文中
            log.debug("将客户端信息集合存储到上下文中");
            contextHandler.putClientSet();
            log.info("----------本地Context中的CLIENT_SET = " + Context.get(RedisStorage.CLIENT) + "---------");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("定时更新本地Context中的clientId集合任务执行出现问题,原因:", e);
        }
    }
}