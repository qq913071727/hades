package com.innovation.ic.sc.gateway.schedule;

import com.innovation.ic.sc.base.handler.sc.ContextHandler;
import com.innovation.ic.sc.base.service.erp9_pvebcs.ErpDictionariesService;
import com.innovation.ic.sc.base.service.erp9_pvecrm.SpecialsSCTopViewService;
import com.innovation.ic.sc.base.service.erp9_pvesiteuser.UsersService;
import com.innovation.ic.sc.base.service.sc.AdvantageModelService;
import com.innovation.ic.sc.base.service.sc.DictionariesService;
import com.innovation.ic.sc.base.service.sc.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * 定时任务的抽象类
 */
public abstract class AbstractSchedule {

    @Resource
    protected ContextHandler contextHandler;

    @Autowired
    protected UserService userService;

    @Autowired
    protected UsersService usersService;

    @Resource
    protected ErpDictionariesService erpDictionariesService;

    @Resource
    protected DictionariesService dictionariesService;

    @Resource
    protected AdvantageModelService advantageModelService;

    @Resource
    protected SpecialsSCTopViewService specialsSCTopViewService;
}