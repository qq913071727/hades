package com.innovation.ic.sc.gateway;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com.innovation.ic.sc")
@MapperScan("com.innovation.ic.sc.base.mapper")
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {DruidDataSourceAutoConfigure.class})
public class SCGatewayApplication {

    private static final Logger log = LoggerFactory.getLogger(SCGatewayApplication.class);


    public static void main(String[] args) {
        LogbackHelper.setLogbackPort(args);
        SpringApplication.run(SCGatewayApplication.class, args);
        log.info("sc-gateway服务启动成功");
    }


}