package com.innovation.ic.sc.web.schedule;

import com.innovation.ic.sc.base.model.erp9_pvecrm.CompanySCTopView;
import com.innovation.ic.sc.base.model.erp9_pvecrm.EnterprisesTopView;
import com.innovation.ic.sc.base.model.sc.ErpMyCompanyName;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.web.controller.sc_data.InitClientController;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 同步erp,部分字典数据,和我的公司
 */
@Component
public class importErpEnterprisesJobSchedule extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(InitClientController.class);


    @XxlJob("importErpEnterprisesJob")
    private void importEnumerationJob() {
        try {
            //每次批处理500个
            Integer size = 500;
            //同步ERP公司数据
            //先找到数据总量
            ServiceResult<Integer> countEnterprises = enterprisesTopViewService.countEnterprises();

            //先清空表
            erpMyCompanyNameService.trunCateTable();

            Integer result = countEnterprises.getResult();
            if (result == null) {
                return;
            }
            log.info("同步erp我的公司总数 {} ",result);
            //算出多少页
            int page = 0;
            if (result % size != 0) {
                page = result / size + 1;
            } else {
                page = result / size;
            }
            List<String> myCompanyIds = new ArrayList<>();
            int count = 0 ;
            //分页查询公司数量
            for (int i = 0; i < page; i++) {
                ServiceResult<List<EnterprisesTopView>> enterprisesTopViewList = enterprisesTopViewService.findByEnterprisesTopView(i * size, size);
                List<EnterprisesTopView> enterprisesTopViewLists = enterprisesTopViewList.getResult();
                if (!CollectionUtils.isEmpty(enterprisesTopViewLists)) {
                    List<ErpMyCompanyName> erpMyCompanyNames = this.bindErpMyCompanyNameList(enterprisesTopViewLists, myCompanyIds);
                    //查询公司信息
                    ServiceResult<List<CompanySCTopView>> companySCTopViewResult = companySCTopViewService.findByIds(myCompanyIds);
                    erpMyCompanyNameService.saveBatch(this.bindErpMyCompanyNameList(erpMyCompanyNames, companySCTopViewResult));
                    count+=erpMyCompanyNames.size();
                    log.info("同步erp我的公司数据数量 {} ",count);
                    myCompanyIds.clear();
                }
            }
            XxlJobHelper.handleSuccess();
        } catch (Exception e) {
            XxlJobHelper.handleFail();
            log.error("同步我的公司发生错误 ", e);
        }
    }


    /**
     * 绑定参数
     *
     * @param enterprisesTopViewList
     * @return
     */
    private List<ErpMyCompanyName> bindErpMyCompanyNameList(List<EnterprisesTopView> enterprisesTopViewList, List<String> myCompanyIds) {
        List<ErpMyCompanyName> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(enterprisesTopViewList)) {
            return result;
        }
        for (EnterprisesTopView enterprisesTopView : enterprisesTopViewList) {
            myCompanyIds.add(enterprisesTopView.getId());
            ErpMyCompanyName erpMyCompanyName = new ErpMyCompanyName();
            erpMyCompanyName.setName(enterprisesTopView.getName());
            erpMyCompanyName.setCreateDate(new Date());
            erpMyCompanyName.setEnterpriseStatus(enterprisesTopView.getStatus());
            erpMyCompanyName.setExternalId(enterprisesTopView.getId());
            erpMyCompanyName.setDistrict(enterprisesTopView.getDistrict());
            result.add(erpMyCompanyName);
        }
        return result;
    }

    /**
     * 绑定完整参数
     *
     * @param erpMyCompanyNames
     * @param companySCTopViewResult
     * @return
     */
    private List<ErpMyCompanyName> bindErpMyCompanyNameList(List<ErpMyCompanyName> erpMyCompanyNames, ServiceResult<List<CompanySCTopView>> companySCTopViewResult) {
        if (CollectionUtils.isEmpty(erpMyCompanyNames)) {
            return erpMyCompanyNames;
        }
        if (CollectionUtils.isEmpty(erpMyCompanyNames) || CollectionUtils.isEmpty(companySCTopViewResult.getResult())) {
            return erpMyCompanyNames;
        }
        List<CompanySCTopView> companySCTopViewList = companySCTopViewResult.getResult();
        for (ErpMyCompanyName erpMyCompanyName : erpMyCompanyNames) {
            for (CompanySCTopView companySCTopView : companySCTopViewList) {
                if (erpMyCompanyName.getExternalId().equals(companySCTopView.getId())) {
                    erpMyCompanyName.setSupplierType(companySCTopView.getSupplierType());
                    erpMyCompanyName.setNature(companySCTopView.getNature());
                    erpMyCompanyName.setInvoiceType(companySCTopView.getInvoiceType());
                    erpMyCompanyName.setFiles(companySCTopView.getFiles());
                }
            }
        }
        return erpMyCompanyNames;
    }


}