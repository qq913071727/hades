package com.innovation.ic.sc.web.listener;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.*;
import com.innovation.ic.sc.base.thread.listener.rabbitmq.*;
import com.rabbitmq.client.*;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * mq队列收到消息后，调用这个监听器的方法
 */
@Component
public class RabbitMqListener implements ApplicationRunner {
    @Resource
    private ServiceHelper serviceHelper;

    @Resource
    private ThreadPoolManager threadPoolManager;

    @Resource
    private AdvantageModelService advantageModelService;

    @Resource
    private AdvantageBrandService advantageBrandService;

    @Resource
    private UserService userService;

    @Resource
    private ErpMyCompanyNameService erpMyCompanyNameService;

    @Resource
    private MyCompanyService myCompanyService;

    @Resource
    private DictionariesTypeService dictionariesTypeService;

    @Resource
    private DictionariesService dictionariesService;

    @Resource
    private ActionMessageService actionMessageService;

    @Resource
    private MyCompanyCurrencyService myCompanyCurrencyService;

    @Resource
    private InventoryService inventoryService;

    /**
     * 监听队列
     */
    @Override
    public void run(ApplicationArguments args) {
        Channel channel = serviceHelper.getRabbitMqManager().getChannel();

        // 监听优势型号新增/修改/延期队列
        ListenAdvantageModelAddQueueThread listenErpDeleteAccountQueueThread = new ListenAdvantageModelAddQueueThread(channel, advantageModelService);
        threadPoolManager.execute(listenErpDeleteAccountQueueThread);

        // 监听优势型号审批队列
        ListenAdvantageModelAuditQueueThread listenAdvantageModelAuditQueueThread = new ListenAdvantageModelAuditQueueThread(channel, advantageModelService);
        threadPoolManager.execute(listenAdvantageModelAuditQueueThread);

        // 监听取消优势型号队列
        ListenAdvantageModelCloseQueueThread listenAdvantageModelCloseQueueThread = new ListenAdvantageModelCloseQueueThread(channel, advantageModelService);
        threadPoolManager.execute(listenAdvantageModelCloseQueueThread);

        // 监听删除优势型号队列
        ListenAdvantageModelDeleteQueueThread listenAdvantageModelDeleteQueueThread = new ListenAdvantageModelDeleteQueueThread(channel, advantageModelService);
        threadPoolManager.execute(listenAdvantageModelDeleteQueueThread);

        // 监听主账号新增/修改队列
        ListenModelAddUsersQueueThread listenModelAddUsersQueueThread = new ListenModelAddUsersQueueThread(channel, userService);
        threadPoolManager.execute(listenModelAddUsersQueueThread);

        // 监听主账号状态队列
        ListenModelUpdateUsersQueueThread listenModelUpdateUsersQueueThread = new ListenModelUpdateUsersQueueThread(channel, userService);
        threadPoolManager.execute(listenModelUpdateUsersQueueThread);

        // 监听密码被重置队列
        ListenModelUpdatePassWordQueueThread listenModelUpdatePassWordQueueThread = new ListenModelUpdatePassWordQueueThread(channel, userService);
        threadPoolManager.execute(listenModelUpdatePassWordQueueThread);

//        // 监听添加子账号数据队列
//        ListenModelAddSubUsersQueueThread listenModelAddSubUsersQueueThread = new ListenModelAddSubUsersQueueThread(channel, userService);
//        ThreadPoolManager.getInstance().execute(listenModelAddSubUsersQueueThread);

        // 监听erp我的公司添加事件队列
        ListenAddErpCompanyQueueThread listenAddErpCompanyQueueThread = new ListenAddErpCompanyQueueThread(channel, erpMyCompanyNameService);
        threadPoolManager.execute(listenAddErpCompanyQueueThread);

        // 监听erp我的公司状态更新队列
        ListenUpdateErpCompanyQueueThread listenUpdateErpCompanyQueueThread = new ListenUpdateErpCompanyQueueThread(channel, erpMyCompanyNameService);
        threadPoolManager.execute(listenUpdateErpCompanyQueueThread);

        // 监听erp关联关系添加队列
        ListenRelationErpCompanyQueueThread listenRelationErpCompanyQueueThread = new ListenRelationErpCompanyQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenRelationErpCompanyQueueThread);

        // 监听关联关系审批队列
        ListenAuditErpCompanyQueueThread listenAuditErpCompanyQueueThread = new ListenAuditErpCompanyQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenAuditErpCompanyQueueThread);

        // 监听关联关系删除队列
        ListenDeleteErpCompanyQueueThread listenDeleteErpCompanyQueueThread = new ListenDeleteErpCompanyQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenDeleteErpCompanyQueueThread);

        // 监听添加枚举类型队列
        ListenAddDistrictTypeQueueThread listenAddDistrictTypeQueueThread = new ListenAddDistrictTypeQueueThread(channel, dictionariesTypeService);
        threadPoolManager.execute(listenAddDistrictTypeQueueThread);

        // 监听添加枚举值队列
        ListenAddDistrictQueueThread listenAddDistrictQueueThread = new ListenAddDistrictQueueThread(channel, dictionariesService);
        threadPoolManager.execute(listenAddDistrictQueueThread);

        // 监听删除枚举值队列
        ListenDeleteDistrictQueueThread listenDeleteDistrictQueueThread = new ListenDeleteDistrictQueueThread(channel, dictionariesService);
        threadPoolManager.execute(listenDeleteDistrictQueueThread);

        // 监听erp2sc的动作消息数据队列
        ListenActionMessageQueueThread listenActionMessageQueueThread = new ListenActionMessageQueueThread(channel, actionMessageService);
        threadPoolManager.execute(listenActionMessageQueueThread);

        // 监听优势品牌添加事件
        ListenAdvantageBrandAddQueueThread listenAdvantageBrandAddQueueThread = new ListenAdvantageBrandAddQueueThread(channel, advantageBrandService);
        threadPoolManager.execute(listenAdvantageBrandAddQueueThread);

        // 监听优势品牌审批事件
        ListenAdvantageBrandAuditQueueThread listenAdvantageBrandAuditQueueThread = new ListenAdvantageBrandAuditQueueThread(channel, advantageBrandService);
        threadPoolManager.execute(listenAdvantageBrandAuditQueueThread);

        // 监听优势品牌关闭事件
        ListenAdvantageBrandCloseQueueThread listenAdvantageBrandCloseQueueThread = new ListenAdvantageBrandCloseQueueThread(channel, advantageBrandService);
        threadPoolManager.execute(listenAdvantageBrandCloseQueueThread);

        // 监听优势品牌删除事件
        ListenAdvantageBrandDeleteQueueThread listenAdvantageBrandDeleteQueueThread = new ListenAdvantageBrandDeleteQueueThread(channel, advantageBrandService);
        threadPoolManager.execute(listenAdvantageBrandDeleteQueueThread);

        // 添加我的公司币种
        ListenAddCompanyCurrencyQueueThread listenAddCompanyCurrencyQueueThread = new ListenAddCompanyCurrencyQueueThread(channel, myCompanyCurrencyService);
        threadPoolManager.execute(listenAddCompanyCurrencyQueueThread);

        // 删除我的公司币种
        ListenDeleteCompanyCurrencyQueueThread listenDeleteCompanyCurrencyQueueThread = new ListenDeleteCompanyCurrencyQueueThread(channel, myCompanyCurrencyService);
        threadPoolManager.execute(listenDeleteCompanyCurrencyQueueThread);

        // 更新我的公司币种
        ListenUpdateCompanyCurrencyQueueThread listenUpdateCompanyCurrencyQueueThread = new ListenUpdateCompanyCurrencyQueueThread(channel, myCompanyCurrencyService);
        threadPoolManager.execute(listenUpdateCompanyCurrencyQueueThread);

        // 监听erp我的公司添加付款账户事件队列
        ListenAddCompanyBookaccountQueueThread listenAddCompanyBookaccountQueueThread = new ListenAddCompanyBookaccountQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenAddCompanyBookaccountQueueThread);

        // 监听erp我的公司添加付款账户成功事件队列
        ListenAddBookAccountSuccessQueueThread listenAddBookaccounSuccesstQueueThread = new ListenAddBookAccountSuccessQueueThread(channel, myCompanyService);
        threadPoolManager.execute(listenAddBookaccounSuccesstQueueThread);

        // 监听优势型号Erp通知队列
        ListenAdvantageModelNotifyQueueThread listenAdvantageModelNotifyQueueThread = new ListenAdvantageModelNotifyQueueThread(channel, advantageModelService, advantageBrandService, threadPoolManager);
        threadPoolManager.execute(listenAdvantageModelNotifyQueueThread);

        // 监听优势品牌Erp通知队列
        ListenAdvantageBrandNotifyQueueThread listenAdvantageBrandNotifyQueueThread = new ListenAdvantageBrandNotifyQueueThread(channel, advantageBrandService);
        threadPoolManager.execute(listenAdvantageBrandNotifyQueueThread);

        // 库存数据删除
        ListenInventoryDeleteQueueThread listenInventoryDeleteQueueThread = new ListenInventoryDeleteQueueThread(channel, inventoryService);
        threadPoolManager.execute(listenInventoryDeleteQueueThread);
    }
}