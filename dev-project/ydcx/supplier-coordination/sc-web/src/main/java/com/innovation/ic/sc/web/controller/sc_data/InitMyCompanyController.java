package com.innovation.ic.sc.web.controller.sc_data;

import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.base.service.sc_data.InitMyCompanyService;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Administrator
 */
@Api(value = "初始化我的公司", tags = "InitMyCompanyController")
@RestController
@RequestMapping("/api/v2/myCompany")
@Slf4j
public class InitMyCompanyController extends AbstractController {

    @ApiOperation(value = "初始化详情数据")
    @GetMapping("initGetDetail")
    public ResponseEntity<ApiResult> initGetDetail() {
        ApiResult apiResult = new ApiResult();
        initMyCompanyService.initGetDetail();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }



}