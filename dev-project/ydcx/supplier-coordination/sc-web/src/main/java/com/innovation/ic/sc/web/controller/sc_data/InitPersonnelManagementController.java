//package com.innovation.ic.sc.web.controller.sc_data;
//
//import com.innovation.ic.sc.base.mapper.sc.PersonnelManagementMapper;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * 先在不用了
// */
//@Api(value = "初始化人员管理controller数据", tags = "InitPersonnelManagementController")
//@RestController
//@RequestMapping("/api/v2/personnelManagement")
//@Slf4j
//public class InitPersonnelManagementController extends AbstractController {
//
//    /**
//     *
//     * @return
//     */
//    @ApiOperation(value = "初始化人员管理数据")
//    @GetMapping("initPersonnelManagementData")
//    public ResponseEntity<ApiResult> initPersonnelManagementData() {
//        ApiResult apiResult = new ApiResult();
//        personnelManagementService.initPersonnelManagementData();
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setResult(Boolean.TRUE);
//        apiResult.setMessage("初始化完成");
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//}
