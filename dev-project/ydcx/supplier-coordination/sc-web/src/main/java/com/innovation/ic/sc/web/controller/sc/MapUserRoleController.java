package com.innovation.ic.sc.web.controller.sc;

import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.model.sc.MapUserRole;
import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.menu.MapUserRoleVo;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户和角色的关系API
 */
@Api(value = "用户和角色的关系API", tags = "MapUserRoleController")
@RestController
@RequestMapping("/api/v1/mapUserRole")
//@DefaultProperties(defaultFallback = "defaultFallback")
public class MapUserRoleController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(MapUserRoleController.class);

    /**
     * 系统管理-账号管理-设置权限：添加用户和角色的关系（先删除旧的关系，再添加新的关系）
     *
     * @param request
     * @param response
     * @return
     */
//    @HystrixCommand
    @ApiOperation(value = "系统管理-账号管理-设置权限：添加用户和角色的关系（先删除旧的关系，再添加新的关系）")
    @ApiImplicitParam(name = "mapUserRoleVo", value = "用户和角色的关系", required = true, dataType = "MapUserRoleVo")
    @RequestMapping(value = "/saveOrUpdateMapUserRoleList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> saveOrUpdateMapUserRoleList(@RequestBody List<MapUserRoleVo> mapUserRoleVoList, HttpServletRequest request, HttpServletResponse response) {
        if (null == mapUserRoleVoList || mapUserRoleVoList.size() == 0) {
            String message = "调用接口【/api/v1/mapUserRole/saveOrUpdateMapUserRoleList时，参数mapUserRoleList不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = mapUserRoleService.saveOrUpdateMapUserRoleList(BeanPropertiesUtil.convertList(mapUserRoleVoList, MapUserRole.class));

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 系统管理-账号管理-设置权限：根据userId，返回这个用户的角色列表
     *
     * @param request
     * @param response
     * @return
     */
//    @HystrixCommand
    @ApiOperation(value = "系统管理-账号管理-设置权限：根据userId，返回这个用户的角色列表")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String")
    @RequestMapping(value = "/findByUserId/{userId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<MapUserRole>>> findByUserId(@PathVariable("userId") String userId, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(userId)) {
            String message = "调用接口【/api/v1/mapUserRole/findByUserId/{userId}时，参数userId不能为空";
            log.warn(message);
            ApiResult<List<MapUserRole>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<MapUserRole>> serviceResult = mapUserRoleService.findByUserId(userId);

        ApiResult<List<MapUserRole>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
