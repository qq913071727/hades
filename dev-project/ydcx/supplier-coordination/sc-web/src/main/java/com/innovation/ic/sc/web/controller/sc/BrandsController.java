//package com.innovation.ic.sc.web.controller.sc;
//
//import com.github.pagehelper.PageInfo;
//import com.innovation.ic.sc.base.model.sc.Brand;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.service.sc.BrandService;
//import com.innovation.ic.sc.base.vo.BrandsPageVo;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.*;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
///**
// * @Description: 产品品牌控制器 备注:此表暂时不用忽略此接口
// * @ClassName: BrandsController
// * @Author: myq
// * @Date: 2022/8/9 上午11:01
// * @Version: 1.0
// */
//@Api(value = "产品品牌,备注:此表暂时不用忽略此接口", tags = "BrandsController")
//@RestController
//@RequestMapping("/api/v1/brandsController")
//@Slf4j
//public class BrandsController extends AbstractController {
//
//    /**
//     * @return
//     * @description 查询品牌列表(分页)
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/9 上午11:05
//     */
//    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ApiOperation("查询品牌列表(分页) 备注:此表暂时不用忽略此接口")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "Integer", required = true, defaultValue = "1"),
//            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "Integer", required = true, defaultValue = "20"),
//            @ApiImplicitParam(name = "searchName", value = "品牌中文名称", dataType = "String", required = false)
//    })
//    public ResponseEntity<ApiResult<PageInfo<Brand>>> page(@RequestBody BrandsPageVo brandsPageVo) {
//        ApiResult apiResult = new ApiResult<>();
//        if (null == brandsPageVo ||
//                null == brandsPageVo.getPageNo() || null == brandsPageVo.getPageSize()) {
//            String message = "接口/api/v1/brandsController/page的参数pageSize、pageNo不能为空";
//            log.warn(message);
//
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        //ServiceResult<IPage<Brands>> res1 = brandsService.page(pageNo, pageSize, searchName);
//        //修改从mysql获取数据
//        //ServiceResult<PageInfo<Brand>> res = brandService.pageBrand(brandsPageVo.getPageNo(), brandsPageVo.getPageSize(), brandsPageVo.getSearchName());
//        //优化从redis获取数据
//        ServiceResult<PageInfo<Brand>> res = brandService.pageBrandRedis(brandsPageVo);
//        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
//    }
//
//
//    /**
//     * 查询品牌列表(分页)
//     *
//     * @param brandsPageVo
//     * @return
//     */
//    @RequestMapping(value = "/findByPage", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ApiOperation("查询品牌列表(分页) 备注:此表暂时不用忽略此接口")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "Integer", required = true, defaultValue = "1"),
//            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "Integer", required = true, defaultValue = "20"),
//            @ApiImplicitParam(name = "searchName", value = "品牌名称", dataType = "String", required = false)
//    })
//    public ResponseEntity<ApiResult<PageInfo<Brand>>> findByPage(@RequestBody BrandsPageVo brandsPageVo) {
//        ApiResult apiResult = new ApiResult<>();
//        if (null == brandsPageVo ||
//                null == brandsPageVo.getPageNo() || null == brandsPageVo.getPageSize()) {
//            String message = "接口/api/v1/brandsController/findByPage的参数pageSize、pageNo不能为空";
//            log.warn(message);
//
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        //ServiceResult<IPage<Brands>> res1 = brandsService.findByPage(pageNo, pageSize, searchName);
//        //修改从mysql获取数据
//        //ServiceResult<PageInfo<Brand>> res = brandService.pageFindBySearchNameBrand(brandsPageVo.getPageNo(), brandsPageVo.getPageSize(), brandsPageVo.getSearchName());
//        //优化从redis获取数据
//        ServiceResult<PageInfo<Brand>> res = brandService.pageFindBySearchNameBrandRedis(brandsPageVo);
//        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
//    }
//
//
//    /**
//     * @return
//     * @description 查询品牌列表
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/9 上午11:05
//     */
//    @ApiOperation("查询品牌列表 备注:此表暂时不用忽略此接口")
//    @PostMapping("/list")
//    @ApiParam(name = "searchName", value = "品牌名称", required = false, type = "string")
//    public ResponseEntity<ApiResult<List<Brand>>> list(String searchName) {
//        //ServiceResult<List<Brands>> res1 = brandsService.list(searchName);
//        //修改从mysql获取数据
//        //ServiceResult<List<Brand>> res = brandService.list(searchName);
//        //优化从redis获取数据
//        ServiceResult<List<Brand>> res = brandService.listRedis(searchName);
//        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
//    }
//}