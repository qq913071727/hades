package com.innovation.ic.sc.web.controller.sc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.enums.AdvantageBrandNatureEnum;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.vo.AdvantageBrandAddVo;
import com.innovation.ic.sc.base.vo.BrandsPageVo;
import com.innovation.ic.sc.base.vo.QueryAdvantageBrandAddVo;
import com.innovation.ic.sc.base.vo.UpdateAllAdvantageBrandVo;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Api(value = "优势品牌", tags = "AdvantageBrandController")
@RestController
@RequestMapping("/api/v1/advantageBrand")
@Slf4j
public class AdvantageBrandController extends AbstractController {

    /**
     * @return
     * @description 查询品牌列表(分页)
     * @parms
     * @author j
     * @datetime 2022/8/9 上午11:05
     */
    @RequestMapping(value = "/findBrand", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ApiOperation("查询品牌列表(分页)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "Integer", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "Integer", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "searchName", value = "品牌中文名称", dataType = "String", required = false)
    })
    public ResponseEntity<ApiResult> findBrand(@RequestBody BrandsPageVo brandsPageVo, HttpServletRequest request) {
        ApiResult apiResult = new ApiResult<>();
        if (null == brandsPageVo ||
                null == brandsPageVo.getPageNo() || null == brandsPageVo.getPageSize()) {
            String message = "接口/api/v1/brandsController/page的参数pageSize、pageNo不能为空";
            log.warn(message);

            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        brandsPageVo.setEnterpriseId(authenticationUser.getEpId());
        ServiceResult<PageInfo<Inventory>> res = inventoryService.findBrand(brandsPageVo);
        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
    }


    /**
     * 添加优势品牌
     *
     * @param advantageBrandAddVo
     * @param request
     * @return
     */
    @ApiOperation(value = "添加优势品牌")
    @PostMapping("addAdvantageBrand")
    public ResponseEntity<ApiResult> addAdvantageBrand(@RequestBody AdvantageBrandAddVo advantageBrandAddVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult;

        if (advantageBrandAddVo.getDominantNature() == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("dominantNature不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!advantageBrandAddVo.getDominantNature().equals(AdvantageBrandNatureEnum.AGENT.getCode()) && advantageBrandAddVo.getCertificateValidityEnd() == null   && advantageBrandAddVo.getCertificateValidityStart()   == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("有效期不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(advantageBrandAddVo.getBrand())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("brand不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        ServiceResult serviceResult = advantageBrandService.addAdvantageBrand(advantageBrandAddVo, authenticationUser);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    /**
     * 添加校验优势品牌
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "校验优势品牌是否存在")
    @GetMapping("checkAdvantageBrand")
    public ResponseEntity<ApiResult> checkAdvantageBrand(String brand, HttpServletRequest request) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        ServiceResult serviceResult = advantageBrandService.checkAdvantageBrand(brand, authenticationUser, request);
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "删除优势品牌")
    @PostMapping("deleteAdvantageBrand")
    public ResponseEntity<ApiResult> deleteAdvantageBrand(@RequestBody UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        if (updateAllAdvantageBrandVo.getIds() == null && (updateAllAdvantageBrandVo.getIsAll() == null || Boolean.FALSE.equals(updateAllAdvantageBrandVo.getIsAll()))) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("ids不能为空或者全量删除标识不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        updateAllAdvantageBrandVo.setEnterpriseId(authenticationUser.getEpId());
        advantageBrandService.deleteAdvantageBrand(updateAllAdvantageBrandVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "取消优势品牌")
    @PostMapping("cancelAdvantageBrand")
    public ResponseEntity<ApiResult> cancelAdvantageBrand(@RequestBody UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        if (updateAllAdvantageBrandVo.getIds() == null && (updateAllAdvantageBrandVo.getIsAll() == null || Boolean.FALSE.equals(updateAllAdvantageBrandVo.getIsAll()))) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("ids不能为空或者全量删除标识不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        updateAllAdvantageBrandVo.setEnterpriseId(authenticationUser.getEpId());
        advantageBrandService.cancelAdvantageBrand(updateAllAdvantageBrandVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "申请延期")
    @PostMapping("applicationForExtension")
    public ResponseEntity<ApiResult> applicationForExtension(@RequestBody AdvantageBrandAddVo advantageBrandAddVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        if (advantageBrandAddVo.getId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (advantageBrandAddVo.getCertificateValidityEnd() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("有效期不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        advantageBrandAddVo.setCreator(authenticationUser.getUsername());
        ServiceResult serviceResult = advantageBrandService.applicationForExtension(advantageBrandAddVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "再次发起申请")
    @PostMapping("reApply")
    public ResponseEntity<ApiResult> reApply(@RequestBody UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        if (updateAllAdvantageBrandVo.getId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
/*        if (updateAllAdvantageBrandVo.getStartDate() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("有效期开始时间不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (updateAllAdvantageBrandVo.getEndDate() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("有效期结束时间不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }*/
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        updateAllAdvantageBrandVo.setEnterpriseId(authenticationUser.getEpId());
        updateAllAdvantageBrandVo.setCreateUser(authenticationUser.getUsername());
        ServiceResult serviceResult = advantageBrandService.reApply(updateAllAdvantageBrandVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "查询列表带分页")
    @GetMapping("findByPage")
    public ResponseEntity<ApiResult<IPagePojo<AdvantageBrandRespPojo>>> findByPage(QueryAdvantageBrandAddVo queryAdvantageBrandAddVo, HttpServletRequest request) {
        ApiResult<IPagePojo<AdvantageBrandRespPojo>> apiResult;

        if (queryAdvantageBrandAddVo == null) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        queryAdvantageBrandAddVo.setEnterpriseId(authenticationUser.getEpId());
        ServiceResult<IPagePojo<AdvantageBrandRespPojo>> serviceResult = advantageBrandService.findByPage(queryAdvantageBrandAddVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


//    @ApiOperation(value = "查询当前优势品牌下的用户")
//    @PostMapping("findAdvantageBrandUser")
//    public ResponseEntity<ApiResult<List<AdvantageBrandApiRespPojo>>> findAdvantageBrandUser(@RequestBody List<String> brands) {
//        ApiResult<List<AdvantageBrandApiRespPojo>> apiResult = new ApiResult<>();
//        if (CollectionUtils.isEmpty(brands)) {
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage("brands不能为空");
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//        ServiceResult<List<AdvantageBrandApiRespPojo>> serviceResult = advantageBrandService.findAdvantageBrandUser(brands);
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setResult(serviceResult.getResult());
//        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }


    @ApiOperation(value = "确认提交图片")
    @PostMapping("confirmPicture")
    public ResponseEntity<ApiResult<List<AdvantageBrandApiRespPojo>>> confirmPicture(@RequestParam List<String> pctureIds, @RequestParam Integer id) throws IOException {
        ApiResult<List<AdvantageBrandApiRespPojo>> apiResult = new ApiResult<>();
        if (CollectionUtils.isEmpty(pctureIds)) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("pctureIds不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (id == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<AdvantageBrandApiRespPojo>> serviceResult = advantageBrandService.confirmPicture(pctureIds, id);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(serviceResult.getResult());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

}
