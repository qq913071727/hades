package com.innovation.ic.sc.web.controller.sc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.model.sc.Dictionaries;
import com.innovation.ic.sc.base.model.sc.ErpMyCompanyName;
import com.innovation.ic.sc.base.pojo.enums.MyCompanyDictionariesEnum;
import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.MyCompanyPojo;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.MyCompanyVo;
import com.innovation.ic.sc.base.vo.SearchCompanyVo;
import com.innovation.ic.sc.base.vo.SearchErpMyCompanyNameVo;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Api(value = "我的公司", tags = "MyCompanyController")
@RestController
@RequestMapping("/api/v1/myCompany")
@Slf4j
public class MyCompanyController extends AbstractController {



    @ApiOperation(value = "模糊搜索公司")
    @GetMapping("searchCompany")
    public ResponseEntity<ApiResult<List<ErpMyCompanyName>>> searchCompany(SearchErpMyCompanyNameVo searchErpMyCompanyNameVo) {
        ApiResult<List<ErpMyCompanyName>> apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(searchErpMyCompanyNameVo.getName())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("搜索名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (searchErpMyCompanyNameVo.getName().length() < 3) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("最少为三个字符");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<ErpMyCompanyName>> serviceResult = erpMyCompanyNameService.findByLikeCompanyName(searchErpMyCompanyNameVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "校验公司是否已经存在注册关系")
    @GetMapping("checkRegistrationCompany")
    public ResponseEntity<ApiResult> checkRegistrationCompany(MyCompanyVo myCompanyVo, HttpServletRequest request) {
        ApiResult apiResult;

        if (!StringUtils.validateParameter(myCompanyVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("搜索名字不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        myCompanyVo.setEnterpriseId(authenticationUser.getEpId());
        ServiceResult serviceResult = myCompanyService.checkRegistrationCompany(myCompanyVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "获取我的公司字典信息")
    @GetMapping("searchMyCompanyDistrict")
    public ResponseEntity<ApiResult<Map<String, List<Dictionaries>>>> searchDistrict() {
        ApiResult apiResult = new ApiResult<>();
        List<String> paramList = new ArrayList<>();
        paramList.add(MyCompanyDictionariesEnum.SUPPLIER_TYPE.getcName());
        paramList.add(MyCompanyDictionariesEnum.INVOICE_TYPE.getcName());
        paramList.add(MyCompanyDictionariesEnum.COUNTRY_AND_REGION.getcName());
        ServiceResult<Map<String, List<Dictionaries>>> serviceResult = dictionariesService.findByMyCompanyDcode(paramList);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }



    @ApiImplicitParam(name = "myCompanyVo", value = "我的公司vo类", required = true, dataType = "MyCompanyVo")
    @ApiOperation(value = "添加公司")
    @PostMapping("registerCompany")
    public ResponseEntity<ApiResult> registerCompany(@RequestBody MyCompanyVo myCompanyVo, HttpServletRequest request) throws IOException {
        ApiResult apiResult;
        if (!StringUtils.validateParameter(myCompanyVo.getName())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("公司名称不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getCountryRegionId())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("国别地区");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getCompanyTypeId())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("公司类型不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getCurrencyId())) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("币种不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        myCompanyVo.setCreatorId(authenticationUser.getId());
        myCompanyVo.setEnterpriseId(authenticationUser.getEpId());
        ServiceResult serviceResult = myCompanyService.registerCompany(myCompanyVo);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "searchCompanyVo", value = "我的公司vo类", required = true, dataType = "SearchCompanyVo")
    @ApiOperation(value = "分页查询公司")
    @PostMapping("findByMyCompany")
    public ResponseEntity<ApiResult<PageInfo<MyCompanyPojo>>> findByMyCompany(@RequestBody SearchCompanyVo searchCompanyVo, HttpServletRequest request) {
        ApiResult<PageInfo<MyCompanyPojo>> apiResult;

        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        searchCompanyVo.setEnterpriseId(authenticationUser.getEpId());
        searchCompanyVo.setEnterpriseName(authenticationUser.getEpName());
        ServiceResult<PageInfo<MyCompanyPojo>> serviceResult = myCompanyService.findByMyCompany(searchCompanyVo);
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "relationEnterpriseId", value = "关联主键id", dataType = "Integer")
    @ApiOperation(value = "停用我的公司关联")
    @GetMapping("deactivateMyCompany")
    public ResponseEntity<ApiResult> findByMyCompany(Integer relationEnterpriseId) throws IOException {
        ApiResult apiResult = new ApiResult();
        ServiceResult serviceResult = myCompanyService.deactivateMyCompany(relationEnterpriseId);
        apiResult.setSuccess(serviceResult.getSuccess());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiOperation(value = "查询关联公司id")
    @GetMapping("findByEpId")
    public ResponseEntity<ApiResult> findByEpId(HttpServletRequest request){
        ApiResult apiResult = new ApiResult();
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (null == authenticationUser) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        ServiceResult serviceResult = myCompanyService.findByEpId(authenticationUser.getEpId());
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "relationEnterpriseId", value = "列表relationEnterpriseId", required = true, dataType = "String")
    @ApiOperation(value = "获取公司详情")
    @GetMapping("getDetail")
    public ResponseEntity<ApiResult<MyCompanyPojo>> getDetail(Integer relationEnterpriseId) {
        ApiResult<MyCompanyPojo> apiResult = new ApiResult<>();
        if (relationEnterpriseId == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("relationEnterpriseId不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<MyCompanyPojo> serviceResult = myCompanyService.getRedisDetail(relationEnterpriseId);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(HttpStatus.OK.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "myCompanyVo", value = "我的公司vo类", required = true, dataType = "MyCompanyVo")
    @ApiOperation(value = "修改公司付款信息")
    @PostMapping("updateBookAccount")
    public ResponseEntity<ApiResult> updateBookAccount(@RequestBody MyCompanyVo myCompanyVo,HttpServletRequest request) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(myCompanyVo.getCurrencyId())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("币种不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getReceivingCompany())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("收款公司不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getBankOfDeposit())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("开户银行不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if (!StringUtils.validateParameter(myCompanyVo.getAccountNumber())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("账号不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //主公司不能修改
        ServiceResult<MyCompanyPojo> serviceDetail = myCompanyService.getDetail(myCompanyVo.getId());
        AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
        if (authenticationUser.getEpName().equals(serviceDetail.getResult().getName())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setMessage("主公司信息不能修改");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = myCompanyService.updateBookAccount(myCompanyVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }


    @ApiImplicitParam(name = "myCompanyVo", value = "我的公司vo类", required = true, dataType = "MyCompanyVo")
    @ApiOperation(value = "修改公司信息")
    @PostMapping("updateMyCompany")
    public ResponseEntity<ApiResult> updateMyCompany(@RequestBody MyCompanyVo myCompanyVo) throws IOException {
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(myCompanyVo.getCountryRegionId())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("国别地区不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (!StringUtils.validateParameter(myCompanyVo.getCompanyTypeId())) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("公司类型不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
//        if (!StringUtils.validateParameter(myCompanyVo.getInvoicTypeId())) {
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage("发票类型不能为空");
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
        if (myCompanyVo.getId() == null) {
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage("id不能为空");
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult serviceResult = myCompanyService.updateMyCompany(myCompanyVo);
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(serviceResult.getSuccess() ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }





}