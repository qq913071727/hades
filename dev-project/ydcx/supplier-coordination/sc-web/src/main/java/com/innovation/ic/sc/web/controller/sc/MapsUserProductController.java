//package com.innovation.ic.sc.web.controller.sc;
//
//import com.innovation.ic.sc.base.model.sc.MapsUserProducts;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.vo.MapsUserProductVo;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.util.Assert;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.constraints.NotEmpty;
//import java.util.List;
//
///**
// * @Description: 用户产品型号关系控制器
// * @ClassName: MapsIUserProductController
// * @Author: myq
// * @Date: 2022/8/5 下午1:55
// * @Version: 1.0
// */
//@RestController
//@RequestMapping("/api/v1/mapsUserProduct")
//@Api(value = "用户产品型号品牌关系API", tags = "MapsUserProductController")
//@Slf4j
//public class MapsUserProductController extends AbstractController {
//
//    /**
//     * @return
//     * @description 查询用户与产品型号品牌关系列表 （废弃）
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/5 下午2:31
//     */
//    @PostMapping(value = "page", produces = {"application/json; charset=utf-8"})
//    @ApiOperation("查询用户与产品型号品牌关系-分页(废弃)")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
//            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
//            @ApiImplicitParam(name = "searchName", value = "品牌中文名称", dataType = "string", required = false)
//    })
//    public ResponseEntity<ApiResult<List<MapsUserProductVo>>> list(@RequestParam("pageNo") int pageNo,
//                                                                   @RequestParam("pageSize") int pageSize,
//                                                                   @RequestParam("searchName") @NotEmpty String searchName) {
//        String userId = "";
//        ServiceResult<List<MapsUserProductVo>> res = mapsUserProductService.mapsUserProductList(pageNo, pageSize, searchName, userId);
//
//        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
//    }
//
//    /**
//     * @return
//     * @description 新增用户和品牌型号关系
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/8 下午1:42
//     */
//    @PostMapping(value = "add", produces = {"application/json; charset=utf-8"})
//    @ApiOperation(" 新增用户和品牌型号关系")
//    public ResponseEntity<ApiResult> add(@RequestBody List<MapsUserProducts> mapsUserProducts, HttpServletRequest request) {
//        Assert.notEmpty(mapsUserProducts, "参数不能为空");
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//
//        ServiceResult serviceResult = mapsUserProductService.add(authenticationUser.getId(), mapsUserProducts);
//
//        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), serviceResult.getMessage()), HttpStatus.OK);
//    }
//
//    /**
//     * @return
//     * @description 根据用户ID查询产品型号数据
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/9 下午3:45
//     */
//    @ApiOperation("根据用户ID查询产品型号数据")
//    @GetMapping("/get/{userId}")
//    @ApiParam(name = "userId", value = "用户ID", required = true)
//    public ResponseEntity<ApiResult<List<MapsUserProductVo>>> get(@PathVariable("userId") String userId) {
//
//        ServiceResult<List<MapsUserProductVo>> serviceResult = mapsUserProductService.get(userId);
//
//        return new ResponseEntity<>(ApiResult.ok(serviceResult.getResult(), serviceResult.getMessage()), HttpStatus.OK);
//    }
//}