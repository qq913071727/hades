package com.innovation.ic.sc.web.listener;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.sc.base.thread.listener.initdata.ImportRedisDataThread;
import com.innovation.ic.sc.base.value.ImportRedisDataScriptConfig;
import com.innovation.ic.sc.base.value.ZookeeperParamConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author linuo
 * @desc 自动加载导入数据任务
 * @time 2023年4月24日15:05:54
 */
@Slf4j
@Component
public class StartHandlerListener implements ApplicationListener<ApplicationStartedEvent> {


    @Resource
    private ImportRedisDataScriptConfig importRedisDataScriptConfig;

    @Resource
    private ThreadPoolManager threadPoolManager;

    @Autowired
    protected CuratorFramework curatorFramework;

    @Autowired
    protected ZookeeperParamConfig zookeeperParamConfig;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        try {
            InterProcessMutex lock = new InterProcessMutex(curatorFramework, zookeeperParamConfig.getImportRedisDataPath());
            if (lock.acquire(zookeeperParamConfig.getWaitingLockTime(), TimeUnit.MINUTES)) {
               ImportRedisDataThread importRedisDataThread = new ImportRedisDataThread(importRedisDataScriptConfig.getScripts());
               threadPoolManager.execute(importRedisDataThread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}