package com.innovation.ic.sc.web.controller.sc;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.enums.LadderPriceConfigTypeEnum;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.*;
import com.innovation.ic.sc.base.vo.ladderPrice.*;
import com.innovation.ic.sc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @desc   阶梯价格信息API
 * @author linuo
 * @time   2022年9月19日15:55:23
 */
@Api(value = "阶梯价格信息API", tags = "LadderPriceInfoController")
@RestController
@RequestMapping("/api/v1/ladderPrice")
@DefaultProperties(defaultFallback = "defaultFallback")
public class LadderPriceInfoController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(LadderPriceInfoController.class);

    /**
     * 按单价配置的阶梯价格信息列表查询
     * @return 返回查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "查询全部阶梯价格配置信息")
    @RequestMapping(value = "/queryAllLadderPriceInfo", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryAllLadderPriceInfo(HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        // 查询全部阶梯价格配置信息
        ServiceResult<List<LadderPriceBasicInfoRespPojo>> serviceResult = ladderPriceInfoService.queryAllLadderPriceInfo(authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 按单价配置的阶梯价格信息列表查询
     * @return 返回查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "按单价配置的阶梯价格信息列表查询")
    @RequestMapping(value = "/queryLadderPriceConfigListByAmount/{currency}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryLadderPriceConfigListByAmount(@PathVariable("currency") Integer currency, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        // 按单价配置的阶梯价格信息列表查询
        ServiceResult<JSONObject> serviceResult = ladderPriceInfoService.queryLadderPriceConfigListByAmount(currency, authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 按单价批量配置库存时的阶梯价格信息查询
     * @return 返回查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "按单价批量配置库存时的阶梯价格信息查询")
    @RequestMapping(value = "/queryBatchSetLadderPriceConfigListByAmount", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryBatchSetLadderPriceConfigListByAmount(@RequestBody LadderPriceBatchSetByAmountVo ladderPriceBatchSetByAmountVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        List<Integer> idList = ladderPriceBatchSetByAmountVo.getIdList();
        if(idList == null || idList.size() == 0 || ladderPriceBatchSetByAmountVo.getCurrency() == null){
            String message = "按单价配置阶梯价格时需要根据币种、库存id集合进行筛选,币种和库存id集合不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 按单价配置的阶梯价格信息列表查询
        ServiceResult<List<LadderPriceInfoBatchSetByAmountRespPojo>> serviceResult = ladderPriceInfoService.queryBatchSetLadderPriceConfigListByAmount(ladderPriceBatchSetByAmountVo, authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 按起订量配置的阶梯价格信息列表查询
     * @return 返回查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "按起订量配置的阶梯价格信息列表查询")
    @RequestMapping(value = "/queryLadderPriceConfigListByMoq", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryLadderPriceConfigListByMoq(HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }
        // 按起订量配置的阶梯价格信息列表查询
        ServiceResult<JSONObject> serviceResult = ladderPriceInfoService.queryLadderPriceConfigListByMoq(authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 按起订量批量配置库存时的阶梯价格信息查询
     * @return 返回查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "按起订量批量配置库存时的阶梯价格信息查询")
    @RequestMapping(value = "/queryBatchSetLadderPriceConfigListByMoq", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryBatchSetLadderPriceConfigListByMoq(@RequestBody LadderPriceBatchSetByMoqVo ladderPriceBatchSetByMoqVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        List<Integer> idList = ladderPriceBatchSetByMoqVo.getIdList();
        if(idList == null || idList.size() == 0){
            String message = "按单价配置阶梯价格时需要根据库存id集合进行筛选,库存id集合不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 按起订量批量配置库存时的阶梯价格信息查询
        ServiceResult<List<LadderPriceInfoBatchSetByMoqRespPojo>> serviceResult = ladderPriceInfoService.queryBatchSetLadderPriceConfigListByMoq(idList, authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 阶梯价格新增
     * @return 返回新增结果
     */
    @HystrixCommand
    @ApiOperation(value = "阶梯价格新增")
    @ApiImplicitParam(name = "ladderPriceAddVo", value = "阶梯价格新增接口的Vo类", required = true, dataType = "LadderPriceAddVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> add(@RequestBody LadderPriceAddVo ladderPriceAddVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(Strings.isNullOrEmpty(ladderPriceAddVo.getName()) || ladderPriceAddVo.getConfigType() == null){
            String message = "阶梯价格配置新增时，阶梯价格名称和配置方式不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 配置方式(1:按起订量配置)
        int configType = ladderPriceAddVo.getConfigType();
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode() && (ladderPriceAddVo.getLadderPriceConfigByMoq() == null || ladderPriceAddVo.getLadderPriceConfigByMoq().size() == 0)){
            String message = "按起订量配置时，起订量及降比的配置信息不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 配置方式(2:按单价配置)
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode() && (ladderPriceAddVo.getCurrency() == null || ladderPriceAddVo.getLadderPriceConfigByAmount() == null || ladderPriceAddVo.getLadderPriceConfigByAmount().size() == 0)){
            String message = "按单价配置时，币种、起订量及降比的配置信息不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 阶梯价格新增
        ServiceResult<Boolean> serviceResult = ladderPriceInfoService.add(ladderPriceAddVo, authenticationUser);

        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 阶梯价格修改
     * @return 返回修改结果
     */
    @HystrixCommand
    @ApiOperation(value = "阶梯价格修改")
    @ApiImplicitParam(name = "ladderPriceEditVo", value = "阶梯价格修改接口的Vo类", required = true, dataType = "LadderPriceEditVo")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> edit(@RequestBody LadderPriceEditVo ladderPriceEditVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(ladderPriceEditVo.getId() == null || Strings.isNullOrEmpty(ladderPriceEditVo.getName()) || ladderPriceEditVo.getConfigType() == null){
            String message = "阶梯价格配置修改时，阶梯价格名称和配置方式不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 配置方式(1:按起订量配置)
        int configType = ladderPriceEditVo.getConfigType();
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode() && (ladderPriceEditVo.getLadderPriceConfigByMoq() == null || ladderPriceEditVo.getLadderPriceConfigByMoq().size() == 0)){
            String message = "按起订量配置时，起订量及降比的配置信息不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 配置方式(2:按单价配置)
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode() && (ladderPriceEditVo.getCurrency() == null || ladderPriceEditVo.getLadderPriceConfigByAmount() == null || ladderPriceEditVo.getLadderPriceConfigByAmount().size() == 0)){
            String message = "按单价配置时，币种、起订量及降比的配置信息不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 阶梯价格修改
        ServiceResult<Boolean> serviceResult = ladderPriceInfoService.edit(ladderPriceEditVo, authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 阶梯价格信息查询
     * @return 返回阶梯价格信息查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "阶梯价格信息查询")
    @ApiImplicitParam(name = "id", value = "阶梯价格信息id", required = true, dataType = "Integer")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> info(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        if(id == null){
            String message = "调用接口【/api/v1/ladderPrice/info】时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 阶梯价格信息查询
        ServiceResult<LadderPriceInfoRespPojo> serviceResult = ladderPriceInfoService.info(id);

        ApiResult apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 阶梯价格信息删除
     * @return 返回删除结果
     */
    @HystrixCommand
    @ApiOperation(value = "阶梯价格信息删除")
    @ApiImplicitParam(name = "id", value = "阶梯价格信息id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> delete(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(id == null){
            String message = "调用接口【/api/v1/ladderPrice/delete】时，参数id不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 阶梯价格信息删除
        ServiceResult<Integer> serviceResult = ladderPriceInfoService.delete(id, authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getSuccess());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 判断阶梯价格是否被库存使用
     * @return 返回结果
     */
    @HystrixCommand
    @ApiOperation(value = "判断阶梯价格是否被库存使用")
    @ApiImplicitParam(name = "id", value = "阶梯价格信息id", required = true, dataType = "Integer")
    @RequestMapping(value = "/judgeIfUsedByInventory/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> judgeIfUsedByInventory(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(id == null){
            String message = "调用接口【/api/v1/ladderPrice/judgeIfUsedByInventory】时，参数id不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 判断阶梯价格是否被库存使用
        ServiceResult<Boolean> serviceResult = ladderPriceInfoService.judgeIfUsedByInventory(id, authenticationUser.getEpId());

        apiResult.setSuccess(serviceResult.getResult());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据单价、阶梯价格配置id获取阶梯价格数据
     * @return 返回查询结果
     */
    @HystrixCommand
    @ApiOperation(value = "根据单价、币种获取阶梯价格数据")
    @ApiImplicitParam(name = "LadderPriceGetDataVo", value = "根据单价获取阶梯价格数据的Vo类", required = true, dataType = "LadderPriceGetDataVo")
    @RequestMapping(value = "/getLadderPriceData", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> getLadderPriceData(@RequestBody LadderPriceGetDataVo ladderPriceGetDataVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult<>();

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(ladderPriceGetDataVo.getUnitPrice() == null){
            String message = "调用接口【/api/v1/getLadderPriceData】时，参数unitPrice不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 阶梯价格数据查询
        ServiceResult<List<LadderPriceDataRespPojo>> serviceResult = ladderPriceInfoService.getLadderPriceData(ladderPriceGetDataVo, authenticationUser);

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}