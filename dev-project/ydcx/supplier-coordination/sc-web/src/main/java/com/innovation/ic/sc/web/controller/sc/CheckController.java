package com.innovation.ic.sc.web.controller.sc;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.pojo.constant.UserType;
import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.UsersVo;
import com.innovation.ic.sc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author swq
 * @desc 校验API
 * @time 2022年12月28日10:29:23
 */
@Api(value = "校验API", tags = "CheckController")
@RestController
@RequestMapping("/api/v1/check")
@DefaultProperties(defaultFallback = "defaultFallback")
public class CheckController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(CheckController.class);

    /**
     * 校验子账号添加时账号是否重复
     *
     * @return 返回是否重复
     */
    @HystrixCommand
    @ApiOperation(value = "校验子账号添加时账号是否重复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "登入账户", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findUserName", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> findUserName(@RequestBody UsersVo usersVo,
                                                           HttpServletRequest request, HttpServletResponse response) {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(usersVo.getUserName())) {
            String message = "调用接口【/api/v1/check/findUserName时，参数userName不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Boolean> userNameData = userService.findData(usersVo.getUserName(), UserType.USER_TYPE);//判断当前账号是否重复
        if (userNameData.getSuccess().equals(Boolean.FALSE)) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(userNameData.getSuccess());
            apiResult.setCode(apiResult.USER_EXPIRE);
            apiResult.setMessage(userNameData.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(userNameData.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 校验子账号添加时手机号是否重复
     *
     * @return 返回是否重复
     */
    @HystrixCommand
    @ApiOperation(value = "校验子账号添加时手机号是否重复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findPhone", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> findPhone(@RequestBody UsersVo usersVo,
                                                        HttpServletRequest request, HttpServletResponse response) {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(usersVo.getPhone())) {
            String message = "调用接口【/api/v1/check/findPhone时，参数phone不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Boolean> phoneData = userService.findData(usersVo.getPhone(), UserType.PHONE_TYPE);//判断手机号是否重复
        if (phoneData.getSuccess().equals(Boolean.FALSE)) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(phoneData.getSuccess());
            apiResult.setCode(apiResult.PHONE_EXPIRE);
            apiResult.setMessage(phoneData.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(phoneData.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 校验子账号添加时邮箱是否重复
     *
     * @return 返回是否重复
     */
    @HystrixCommand
    @ApiOperation(value = "校验子账号添加时邮箱是否重复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "电子邮箱", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findEmail", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> findEmail(@RequestBody UsersVo usersVo,
                                                        HttpServletRequest request, HttpServletResponse response) {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(usersVo.getEmail())) {
            String message = "调用接口【/api/v1/check/findEmail时，参数email不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Boolean> emailData = userService.findData(usersVo.getEmail(), UserType.EMAIL_TYPE);//判断邮箱是否重复
        if (emailData.getSuccess().equals(Boolean.FALSE)) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(emailData.getSuccess());
            apiResult.setCode(apiResult.EMAIL_EXPIRE);
            apiResult.setMessage(emailData.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(emailData.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 校验账号修改密碼时原密碼是否重复
     *
     * @return 返回是否重复
     */
    @HystrixCommand
    @ApiOperation(value = "校验账号修改密碼时原密碼是否重复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "password", value = "密码", dataType = "String", required = true),
            @ApiImplicitParam(name = "userName", value = "登入账户", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findPassWord", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> findPassWord(@RequestBody UsersVo usersVo,
                                                           HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(usersVo.getPassword()) || !StringUtils.validateParameter(usersVo.getUserName())) {
            String message = "调用接口【/api/v1/check/findPassWord时，参数password、userName不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Boolean> passwordData = userService.findPassWord(usersVo);//判断密码是否重复

        if (passwordData.getSuccess().equals(Boolean.FALSE)) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(passwordData.getSuccess());
            apiResult.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            apiResult.setMessage(passwordData.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(passwordData.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 校验账号忘记密码时校验手机号是否不正确
     *
     * @return 返回验证
     */
    @HystrixCommand
    @ApiOperation(value = "校验账号忘记密码时校验手机号是否不正确")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findAccountPhone", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> findAccountPhone(@RequestBody UsersVo usersVo,
                                                               HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult<Boolean> apiResult;
        if (!StringUtils.validateParameter(usersVo.getPhone())) {
            String message = "调用接口【/api/v1/check/findAccountPhone时，参数phone不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Boolean> accountPhoneData = userService.findAccountPhone(usersVo);//判断手机号是否不正确

        if (accountPhoneData.getSuccess().equals(Boolean.FALSE)) {
            apiResult = new ApiResult<>();
            apiResult.setSuccess(accountPhoneData.getSuccess());
            apiResult.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            apiResult.setMessage(accountPhoneData.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }

        apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(accountPhoneData.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}