package com.innovation.ic.sc.web.config;

import com.innovation.ic.sc.base.pojo.constant.config.DatabaseGlobal;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
public class DataSourceAspect {

    private static final Logger log = LoggerFactory.getLogger(DataSourceAspect.class);

    @Pointcut("execution(* com.innovation.ic.sc.base.service.sc.impl..*.*(..))")
    private void scAspect() {
    }


    @Pointcut("execution(* com.innovation.ic.sc.base.service.sc_erp.impl..*.*(..))")
    private void scErpAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.sc.base.service.erp9_pvesiteuser.impl..*.*(..))")
    private void erp9PveSiteUserAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.sc.base.service.erp9_pvestandard.impl..*.*(..))")
    private void erp9PveStandardAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.sc.base.service.erp9_pvebcs.impl..*.*(..))")
    private void erp9PveBcsAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.sc.base.service.erp9_pvecrm.impl..*.*(..))")
    private void erp9PveCrmAspect() {
    }

    @Before("scAspect()")
    public void sc() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.SC);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.SC);
    }

    @Before("scErpAspect()")
    public void scErp() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.SC_ERP);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.SC_ERP);
    }

    @Before("erp9PveSiteUserAspect()")
    public void erp9PveSiteUser() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVESITEUSER);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVESITEUSER);
    }

    @Before("erp9PveStandardAspect()")
    public void erp9PveStandard() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVESTANDARD);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVESTANDARD);
    }

    @Before("erp9PveBcsAspect()")
    public void erp9PveBcs() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVEBCS);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVEBCS);
    }

    @Before("erp9PveCrmAspect()")
    public void erp9PveCrm() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVECRM);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVECRM);
    }
}
