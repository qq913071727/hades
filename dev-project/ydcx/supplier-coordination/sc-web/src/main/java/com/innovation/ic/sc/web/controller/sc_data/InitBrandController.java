//package com.innovation.ic.sc.web.controller.sc_data;
//
//import com.innovation.ic.sc.base.model.sc.Brand;
//import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
///**
// * @author zengqinglong
// * @desc 初始化Brand到redis中
// * @Date 2023/2/3 10:53
// **/
//@Api(value = "初始化Brand到redis中", tags = "InitBrandController")
//@RestController
//@RequestMapping("/api/v2/brand")
//@Slf4j
//public class InitBrandController extends AbstractController {
//
//    @ApiOperation(value = "初始化Brand到redis中")
//    @GetMapping("/initBrandToRedis")
//    public ResponseEntity<ApiResult> initBrandToRedis() {
//        log.info("初始化Brand到redis中,开始");
//        ApiResult apiResult = new ApiResult();
//        // 查找brand表中所有记录
//        ServiceResult<List<Brand>> serviceResult = brandService.findAll();
//        if (null != serviceResult && null != serviceResult.getResult()){
//            String keyAsc = RedisStorage.TABLE + RedisStorage.BRAND +
//                    RedisStorage.CREATE_DATE_ASC;
//            String keyDesc = RedisStorage.TABLE + RedisStorage.BRAND +
//                    RedisStorage.CREATE_DATE_DESC;
//
//            // 删除旧的数据
//            serviceHelper.getRedisManager().del(keyAsc);
//            serviceHelper.getRedisManager().del(keyDesc);
//
//            // 向redis中导入数据
//            List<Brand> brandList = serviceResult.getResult();
//            for (int i = 0; i < brandList.size(); i++) {
//                Brand brand = brandList.get(i);
//                String value = modelHandler.brandToXmlStorageString(brand);
//                serviceHelper.getRedisManager().zAdd(keyAsc, value, brand.getCreateDate().getTime());
//                serviceHelper.getRedisManager().zAdd(keyDesc, value, -brand.getCreateDate().getTime());
//            }
//        }
//        log.info("初始化Brand到redis中,结束");
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setMessage("初始化完成");
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//}
