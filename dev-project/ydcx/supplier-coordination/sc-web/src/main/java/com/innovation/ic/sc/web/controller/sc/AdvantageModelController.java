//package com.innovation.ic.sc.web.controller.sc;
//
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.base.Strings;
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.sc.base.pojo.constant.Constants;
//import com.innovation.ic.sc.base.pojo.enums.AdvantageModelSourceEnum;
//import com.innovation.ic.sc.base.pojo.variable.advantageModel.*;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.vo.advantageModel.*;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiOperation;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.text.ParseException;
//import java.util.List;
//
///**
// * @desc   优势型号信息API
// * @author linuo
// * @time   2022年8月29日13:55:18
// */
//@Api(value = "优势型号信息API", tags = "AdvantageModelController")
//@RestController
//@RequestMapping("/api/v1/advantageModel")
//@DefaultProperties(defaultFallback = "defaultFallback")
//public class AdvantageModelController extends AbstractController {
//    private static final Logger log = LoggerFactory.getLogger(AdvantageModelController.class);
//
////    /**
////     * 根据型号数组批量判断是否为优势型号
////     * @return 返回结果
////     */
////    @HystrixCommand
////    @ApiOperation(value = "根据型号数组批量判断是否为优势型号")
////    @ApiImplicitParam(name = "AdvantageModelBatchJudgeModelStatusVo", value = "批量判断是否为优势型号的Vo类", required = true, dataType = "AdvantageModelBatchJudgeModelStatusVo")
////    @RequestMapping(value = "/batchJudgeIfAdvantageModelByModelArray", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult<AdvantageModelBatchJudgeModelStatusRespPojo>> batchJudgeIfAdvantageModelByModelArray(@RequestBody AdvantageModelBatchJudgeModelStatusVo advantageModelBatchJudgeModelStatusVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
////        if (advantageModelBatchJudgeModelStatusVo == null
////                || advantageModelBatchJudgeModelStatusVo.getPartNumberList() == null
////                || advantageModelBatchJudgeModelStatusVo.getPartNumberList().size() == 0) {
////            String message = "调用接口[/api/v1/advantageModel/batchJudgeIfAdvantageModelByModelArray]时，参数getPartNumberList不能为空";
////            log.warn(message);
////            ApiResult<AdvantageModelBatchJudgeModelStatusRespPojo> apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////
////        // 根据型号数组批量判断是否为优势型号
////        ServiceResult<AdvantageModelBatchJudgeModelStatusRespPojo> listServiceResult = advantageModelService.batchJudgeIfAdvantageModelByModelArray(advantageModelBatchJudgeModelStatusVo);
////
////        ApiResult<AdvantageModelBatchJudgeModelStatusRespPojo> apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
////    /**
////     * 根据品牌、型号数组批量判断是否为优势型号
////     * @return 返回结果
////     */
////    @HystrixCommand
////    @ApiOperation(value = "根据品牌、型号数组批量判断是否为优势型号")
////    @ApiImplicitParam(name = "AdvantageModelBatchJudgeStatusVo", value = "批量判断是否为优势型号的Vo类", required = true, dataType = "AdvantageModelBatchJudgeStatusVo")
////    @RequestMapping(value = "/batchJudgeIfAdvantageModelByBrandModelArray", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult<AdvantageModelBatchJudgeStatusRespPojo>> batchJudgeIfAdvantageModelByBrandModelArray(@RequestBody AdvantageModelBatchJudgeStatusVo advantageModelBatchJudgeStatusVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
////        if (advantageModelBatchJudgeStatusVo == null
////                || advantageModelBatchJudgeStatusVo.getAdvantageModelList() == null
////                || advantageModelBatchJudgeStatusVo.getAdvantageModelList().size() == 0) {
////            String message = "调用接口[/api/v1/advantageModel/batchJudgeIfAdvantageModelByBrandModelArray]时，参数advantageModelList不能为空";
////            log.warn(message);
////            ApiResult<AdvantageModelBatchJudgeStatusRespPojo> apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////
////        // 根据品牌、型号数组批量判断是否为优势型号
////        ServiceResult<AdvantageModelBatchJudgeStatusRespPojo> listServiceResult = advantageModelService.batchJudgeIfAdvantageModelByBrandModelArray(advantageModelBatchJudgeStatusVo);
////
////        ApiResult<AdvantageModelBatchJudgeStatusRespPojo> apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
//    /**
//     * 根据型号查询所属品牌
//     * @return 返回结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "根据型号查询所属品牌")
//    @ApiImplicitParam(name = "AdvantageModelGetBrandsVo", value = "根据型号查询所属品牌的Vo类", required = true, dataType = "AdvantageModelGetBrandsVo")
//    @RequestMapping(value = "/queryBrandsByPartNumber", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> queryBrandsByPartNumber(@RequestBody AdvantageModelGetBrandsVo advantageModelGetBrandsVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        if (!StringUtils.validateParameter(advantageModelGetBrandsVo.getPartNumber())) {
//            String message = "调用接口[/api/v1/advantageModel/queryBrandByPartNumber]时，参数partNumber不能为空";
//            log.warn(message);
//            ApiResult<List<String>> apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            ApiResult apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        // 根据型号查询所属品牌
//        ServiceResult<List<String>> listServiceResult = advantageModelService.queryBrandsByPartNumber(advantageModelGetBrandsVo.getPartNumber(), authenticationUser);
//
//        ApiResult<List<String>> apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
////    /**
////     * 判断是否为优势型号
////     * @return 返回结果
////     */
////    @HystrixCommand
////    @ApiOperation(value = "判断是否为优势型号")
////    @ApiImplicitParam(name = "AdvantageModelJudgeStatusVo", value = "判断是否为优势型号的Vo类", required = true, dataType = "AdvantageModelJudgeStatusVo")
////    @RequestMapping(value = "/judgeIfAdvantageModel", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult<List<AdvantageModelJudgeStatusRespPojo>>> judgeIfAdvantageModel(@RequestBody AdvantageModelJudgeStatusVo advantageModelJudgeStatusVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
////        ApiResult apiResult;
////        if (!StringUtils.validateParameter(advantageModelJudgeStatusVo.getPartNumber())
////                || !StringUtils.validateParameter(advantageModelJudgeStatusVo.getBrand())
////                || advantageModelJudgeStatusVo.getNeedGoodsCount() == null) {
////            String message = "调用接口[/api/v1/advantageModel/judgeIfAdvantageModel]时，参数partNumber、brand和needGoodsCount不能为空";
////            log.warn(message);
////            apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////
////        // 判断是否为优势型号
////        ServiceResult<List<AdvantageModelJudgeStatusRespPojo>> listServiceResult = advantageModelService.judgeIfAdvantageModel(advantageModelJudgeStatusVo);
////
////        apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
//    /**
//     * 校验当前型号是否已添加
//     * @return 返回结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "校验当前型号是否已添加(true已添加、false未添加)")
//    @ApiImplicitParam(name = "advantageModelAddVo", value = "优势型号校验的Vo类", required = true, dataType = "AdvantageModelAddVo")
//    @RequestMapping(value = "/checkAdvantageModelIfAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> checkAdvantageModelIfAdd(@RequestBody AdvantageModelCheckVo advantageModelCheckVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        ApiResult apiResult;
//
//        if (!StringUtils.validateParameter(advantageModelCheckVo.getPartNumber()) || !StringUtils.validateParameter(advantageModelCheckVo.getBrand())) {
//            String message = "调用接口[/api/v1/advantageModel/checkAdvantageModelIfHave]时，参数partNumber和brand不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        // 校验当前型号是否已添加
//        ServiceResult<Boolean> listServiceResult = advantageModelService.checkAdvantageModelIfAdd(advantageModelCheckVo, authenticationUser);
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
////    /**
////     * 校验当前型号是否为已有型号
////     * @return 返回结果
////     */
////    @HystrixCommand
////    //@ApiOperation(value = "校验当前型号是否为已有型号(true是已有型号、false不是已有型号)")
////    @ApiImplicitParam(name = "advantageModelAddVo", value = "优势型号校验的Vo类", required = true, dataType = "AdvantageModelAddVo")
////    @RequestMapping(value = "/checkAdvantageModelIfHave", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult> checkAdvantageModelIfHave(@RequestBody AdvantageModelCheckVo advantageModelCheckVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
////        ApiResult apiResult;
////
////        if (!StringUtils.validateParameter(advantageModelCheckVo.getPartNumber()) || !StringUtils.validateParameter(advantageModelCheckVo.getBrand())) {
////            String message = "调用接口[/api/v1/advantageModel/checkAdvantageModelIfHave]时，参数partNumber和brand不能为空";
////            log.warn(message);
////            apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////
////        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
////        if (null == authenticationUser){
////            apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
////            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
////        }
////
////        // 校验当前型号是否为已有型号
////        ServiceResult<Boolean> listServiceResult = advantageModelService.checkAdvantageModelIfHave(advantageModelCheckVo, authenticationUser);
////
////        apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
//    /**
//     * 优势型号单条新增
//     * @return 返回导入结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "优势型号单条新增")
//    @ApiImplicitParam(name = "advantageModelAddVo", value = "优势型号单条新增的Vo类", required = true, dataType = "AdvantageModelAddVo")
//    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> add(@RequestBody AdvantageModelAddVo advantageModelAddVo, HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException {
//        ApiResult<Boolean> apiResult;
//
//        if (!StringUtils.validateParameter(advantageModelAddVo.getPartNumber()) || !StringUtils.validateParameter(advantageModelAddVo.getBrand())
//            || !StringUtils.validateParameter(advantageModelAddVo.getValidityDateStart()) || !StringUtils.validateParameter(advantageModelAddVo.getValidityDateEnd())) {
//            String message = "优势型号新增时，型号、品牌、有效期开始、结束时间均不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        // 优势型号单条新增
//        ServiceResult<Boolean> listServiceResult = advantageModelService.add(advantageModelAddVo, authenticationUser, AdvantageModelSourceEnum.USER_ADD.getCode());
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 优势型号批量导入
//     * @return 返回导入结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "优势型号批量导入")
//    @ApiImplicitParam(name = "advantageModelBatchAddVo", value = "优势型号批量导入的Vo类", required = true, dataType = "AdvantageModelBatchAddVo")
//    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> batchAdd(@RequestBody AdvantageModelBatchAddVo advantageModelBatchAddVo, HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException {
//        ApiResult apiResult;
//
//        if (advantageModelBatchAddVo.getList() == null || advantageModelBatchAddVo.getList().size() == 0
//            || Strings.isNullOrEmpty(advantageModelBatchAddVo.getValidityDateStart()) || Strings.isNullOrEmpty(advantageModelBatchAddVo.getValidityDateEnd())) {
//            String message = "优势型号批量新增时，数据列表、有效期开始、结束时间均不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        ServiceResult<AdvantageModelBatchAddExceptionRespPojo> listServiceResult = advantageModelService.batchAdd(advantageModelBatchAddVo, authenticationUser);
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), listServiceResult.getResult(), listServiceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 优势型号信息列表查询
//     * @return 返回优势型号信息列表
//     */
//    @HystrixCommand
//    @ApiOperation(value = "优势型号信息列表查询")
//    @ApiImplicitParam(name = "advantageModelRequestVo", value = "优势型号信息列表查询的请求Vo类", required = true, dataType = "AdvantageModelRequestVo")
//    @RequestMapping(value = "/query", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> query(@RequestBody AdvantageModelRequestVo advantageModelRequestVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult<JSONObject> apiResult;
//
//        if (advantageModelRequestVo.getPageSize() == null || advantageModelRequestVo.getPageNo() == null) {
//            String message = "调用接口[/api/v1/advantageModel/query]时，参数pageSize、pageNo不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        ServiceResult<List<AdvantageModelRespPojo>> listServiceResult = advantageModelService.query(advantageModelRequestVo, authenticationUser.getEpId());
//
//        // 获取数据数量
//        ServiceResult<Long> totalServiceResult = advantageModelService.queryTotal(advantageModelRequestVo, authenticationUser.getEpId());
//
//        JSONObject json = new JSONObject();
//        json.put(Constants.DATA_FIELD, listServiceResult.getResult());
//        json.put(Constants.TOTAL_FIELD, totalServiceResult.getResult());
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), listServiceResult.getMessage(), json, listServiceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 查询可申请优势型号的的库存型号
//     * @return 返回查询结果
//     */
////    @HystrixCommand
////    @ApiOperation(value = "查询可申请优势型号的的库存型号")
////    @RequestMapping(value = "/getMayApplyInventoryModel", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult> getMayApplyInventoryModel(HttpServletRequest request, HttpServletResponse response) {
////        ApiResult<List<MayApplyInventoryModelPojo>> apiResult;
////
////        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
////        if (null == authenticationUser){
////            apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
////            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
////        }
////
////        // 查询可申请优势型号的的库存型号
////        ServiceResult<List<MayApplyInventoryModelPojo>> serviceResult = advantageModelService.getMayApplyInventoryModel(authenticationUser.getEpId());
////
////        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
//    /**
//     * 优势型号信息删除
//     * @return 返回删除结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "优势型号信息删除")
//    @ApiImplicitParam(name = "AdvantageModelDeleteVo", value = "删除优势型号数据的Vo类", required = true, dataType = "AdvantageModelDeleteVo")
//    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> delete(@RequestBody AdvantageModelDeleteVo advantageModelDeleteVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        ApiResult apiResult;
//
//        if(advantageModelDeleteVo.getIdList() == null || advantageModelDeleteVo.getIdList().size() == 0){
//            String message = "调用接口[/api/v1/advantageModel/delete]时，参数idList不能为空";
//            log.warn(message);
//            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        // 判断优势型号数据中是否已保存erp的id
//        ServiceResult<Boolean> result = advantageModelService.judgeIfSaveErpId(advantageModelDeleteVo.getIdList());
//        if(!result.getResult()){
//            apiResult = new ApiResult(HttpStatus.OK.value(), result.getMessage(), null, Boolean.FALSE);
//            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        // 优势型号信息删除
//        ServiceResult<Boolean> serviceResult = advantageModelService.delete(advantageModelDeleteVo, authenticationUser.getEpId());
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 取消优势型号
//     * @return 返回处理结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "取消优势型号")
//    @ApiImplicitParam(name = "advantageModelCancleVo", value = "取消优势型号的Vo类", required = true, dataType = "AdvantageModelCancleVo")
//    @RequestMapping(value = "/cancleAdvantageModel", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> cancleAdvantageModel(@RequestBody AdvantageModelCancleVo advantageModelCancleVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        ApiResult<Boolean> apiResult;
//
//        if(advantageModelCancleVo.getIdList() == null || advantageModelCancleVo.getIdList().size() == 0){
//            String message = "调用接口[/api/v1/advantageModel/cancleAdvantageModel]时，参数idList不能为空";
//            log.warn(message);
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        // 判断优势型号数据中是否已保存erp的id
//        ServiceResult<Boolean> result = advantageModelService.judgeIfSaveErpId(advantageModelCancleVo.getIdList());
//        if(!result.getResult()){
//            apiResult = new ApiResult(HttpStatus.OK.value(), result.getMessage(), null, Boolean.FALSE);
//            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        // 取消优势型号
//        ServiceResult<Boolean> serviceResult = advantageModelService.cancleAdvantageModel(advantageModelCancleVo, authenticationUser.getEpId());
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
////    /**
////     * 查询拒绝原因
////     * @return 返回查询结果
////     */
////    @HystrixCommand
////    //@ApiOperation(value = "查询拒绝原因")
////    @ApiImplicitParams({
////            @ApiImplicitParam(name = "id", value = "优势型号id", required = true, dataType = "String")
////    })
////    @RequestMapping(value = "/queryRefusedReason/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult> queryRefusedReason(@PathVariable("id") String id, HttpServletRequest request, HttpServletResponse response) {
////        if(!StringUtils.validateParameter(id)){
////            String message = "调用接口[/api/v1/advantageModel/queryRefusedReason]时，参数id不能为空";
////            log.warn(message);
////            ApiResult<Boolean> apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////
////        // 查询拒绝原因
////        ServiceResult<String> serviceResult = advantageModelService.queryRefusedReason(id);
////
////        ApiResult apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
////    /**
////     * 申请延期
////     * @return 返回操作结果
////     */
////    @HystrixCommand
////    //@ApiOperation(value = "申请延期")
////    @ApiImplicitParam(name = "advantageModelApplyDelayVo", value = "取消优势型号的Vo类", required = true, dataType = "AdvantageModelApplyDelayVo")
////    @RequestMapping(value = "/applyDelay", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult> applyDelay(@RequestBody AdvantageModelApplyDelayVo advantageModelApplyDelayVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
////        if(advantageModelApplyDelayVo.getId() == null || advantageModelApplyDelayVo.getDelayDate() == null){
////            String message = "调用接口[/api/v1/advantageModel/applyDelay]时，参数id和delayDate不能为空";
////            log.warn(message);
////            ApiResult<Boolean> apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////
////        // 申请延期
////        ServiceResult<Boolean> serviceResult = advantageModelService.applyDelay(advantageModelApplyDelayVo);
////
////        ApiResult apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.OPERATE_SUCCESS, serviceResult.getResult(), serviceResult.getSuccess());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
//    /**
//     * 优势型号再次发起审核
//     * @return 返回处理结果
//     */
//    @HystrixCommand
//    @ApiOperation(value = "优势型号再次发起审核")
//    @ApiImplicitParam(name = "advantageModelAgainInitiateVo", value = "优势型号再次发起审核接口的Vo类", required = true, dataType = "AdvantageModelAgainInitiateVo")
//    @RequestMapping(value = "/againInitiate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> againInitiate(@RequestBody AdvantageModelAgainInitiateVo advantageModelAgainInitiateVo, HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException {
//        ApiResult<Boolean> apiResult = new ApiResult<>();
//        if(advantageModelAgainInitiateVo.getIdList() == null || advantageModelAgainInitiateVo.getIdList().size() == 0){
//            String message = "调用接口[/api/v1/advantageModel/againInitiate]时，参数validityDateStart、validityDateEnd和idList均不能为空";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        // 判断优势型号数据中是否已保存erp的id
//        ServiceResult<Boolean> result = advantageModelService.judgeIfSaveErpId(advantageModelAgainInitiateVo.getIdList());
//        if(!result.getResult()){
//            apiResult = new ApiResult(HttpStatus.OK.value(), result.getMessage(), null, Boolean.FALSE);
//            return new ResponseEntity(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        // 再次发起
//        ServiceResult<Boolean> serviceResult = advantageModelService.againInitiate(advantageModelAgainInitiateVo, authenticationUser);
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//}