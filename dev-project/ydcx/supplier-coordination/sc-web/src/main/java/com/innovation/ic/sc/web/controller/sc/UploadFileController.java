package com.innovation.ic.sc.web.controller.sc;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.pojo.enums.CheckImageFormat;
import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.sc.UploadFileService;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@Api(value = "文件上传", tags = "UploadFileController")
@RestController
@RequestMapping("/api/v1/uploadFile")
@Slf4j
public class UploadFileController extends AbstractController {

    @ApiOperation(value = "上传图片")
    @PostMapping("pictureUpload")
    public ResponseEntity<ApiResult> pictureUpload(MultipartFile file, HttpServletRequest request) {
        log.info("/api/v1/uploadFile 上传文件");
        ApiResult apiResult;
        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setSuccess(Boolean.FALSE);
        try {
            if (file == null) {
                apiResult = new ApiResult<>();
                apiResult.setSuccess(Boolean.FALSE);
                apiResult.setCode(HttpStatus.BAD_REQUEST.value());
                apiResult.setMessage("file不能为空");
                return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
            }

            //查看是否图片
            String originalFilename = file.getOriginalFilename();
            if (!StringUtils.validateParameter(originalFilename)) {
                apiResult = new ApiResult<>();
                apiResult.setSuccess(Boolean.FALSE);
                apiResult.setCode(HttpStatus.BAD_REQUEST.value());
                apiResult.setMessage("文件名字不能为空");
                return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
            }
            //后缀名字
            String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
            if (!CheckImageFormat.checkImageSuffix(suffix)) {
                apiResult = new ApiResult<>();
                apiResult.setSuccess(Boolean.FALSE);
                apiResult.setCode(HttpStatus.BAD_REQUEST.value());
                apiResult.setMessage("格式不符合规范");
                return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
            }

            AuthenticationUser authenticationUser = super.getAuthenticationUser(request);
            if (null == authenticationUser) {
                apiResult = new ApiResult<>();
                apiResult.setSuccess(Boolean.FALSE);
                apiResult.setCode(ApiResult.TOKEN_ILLEGAL_EXPIRE);
                return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
            }
             serviceResult = uploadFileService.fileUpload(file.getBytes(), originalFilename, suffix, authenticationUser.getId());
        } catch (Exception e) {
            log.error("上传文件错误 ",e);
        }
        apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }




}
