package com.innovation.ic.sc.web.schedule;

import com.innovation.ic.sc.base.model.erp9_pvecrm.ScCreditsTopView;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.model.sc_erp.AccountStatement;
import com.innovation.ic.sc.base.pojo.enums.ScErpCurrencyEnum;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author zengqinglong
 * @desc TODO
 * @Date 2022/12/20 9:27
 **/
@Component
@Slf4j
public class ImportAccountStatementSchedule extends AbstractSchedule {

    @XxlJob("importAccountStatementJob")
    public void importAccountStatement() {
        //查询erp当天需要生成的对账单数据
        List<ScCreditsTopView> allBySameDay = scCreditsTopViewService.findAllBySameDay();
        //判断当天是否需要生成对账单
        if (allBySameDay.size() == 0){
            log.info("当天没有对账单需要生成");
            return;
        }
        //查询主账号
        List<User> userFather = userService.findUserAllByFatherId(null);
        //循环主账号
        for (User user : userFather) {
            HashSet<String> enterpriseIds = new HashSet<>();
            enterpriseIds.add(user.getEnterpriseId());
            //判断set是否为空 不为空需要查找对应公司生成对账单
            if (user.getEnterpriseId() != null){
                List<String> byExternalId = myCompanyEnterpriseService.findByExternalId(user.getEnterpriseId());
                if (byExternalId != null && byExternalId.size() > 0){
                    for (ScCreditsTopView scCreditsTopView : allBySameDay) {
                        if (byExternalId.contains(scCreditsTopView.getSellerId())){
                            insetAccountStatement(scCreditsTopView,user.getId());
                        }
                    }
                }
            }
        }

    }

    /**
     * 保存对账单
     * @param scCreditsTopView
     * @param userId
     */
    public void insetAccountStatement(ScCreditsTopView scCreditsTopView,String userId){
        //查询account是否生成过对账单,如果没有生成过就创建2条对账单数据
        List<AccountStatement> accountStatementList = accountStatementService.findByScCreditsTopView(scCreditsTopView);
        if (accountStatementList.size() == 0){
            List<AccountStatement> list = new ArrayList<>();
            for (ScErpCurrencyEnum value : ScErpCurrencyEnum.values()) {
                AccountStatement accountStatement = new AccountStatement();
                accountStatement.setStatementNo(getStatementNo(scCreditsTopView,value));
                accountStatement.setGenerationDate(scCreditsTopView.getGenerationDate());
                accountStatement.setIntervalDateStart(scCreditsTopView.getIntervalDateStart());
                accountStatement.setIntervalDateEnd(scCreditsTopView.getIntervalDateEnd());
                accountStatement.setSettlementDate(scCreditsTopView.getSettlementDate());
                accountStatement.setSellerName(scCreditsTopView.getSellerName());
                accountStatement.setSellerId(scCreditsTopView.getSellerId());
                accountStatement.setBuyerName(scCreditsTopView.getBuyerName());
                accountStatement.setBuyerId(scCreditsTopView.getBuyerId());
                accountStatement.setCurrency(value.getType());
                accountStatement.setUserId(userId);
                accountStatement.setSettlementType(scCreditsTopView.getSettlementType());
                Date date = new Date();
                accountStatement.setCreateTime(date);
                accountStatement.setUpdateTime(date);
                list.add(accountStatement);
            }
            //生成对账单
            if (list.size() > 0){
                accountStatementService.batchInsert(list);
            }
        }
    }

    /**
     * 生成对账单号
     * @param scCreditsTopView
     * @param scErpCurrencyEnum
     * @return
     */
    public static String getStatementNo(ScCreditsTopView scCreditsTopView,ScErpCurrencyEnum scErpCurrencyEnum){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        StringBuffer statementNo = new StringBuffer();
        statementNo.append("DZ");
        statementNo.append(String.format("%2d", scErpCurrencyEnum.getType()).replace(" ", "0"));
        statementNo.append(format.format(new Date()));
        statementNo.append(scCreditsTopView.getSellerNumber());
        statementNo.append(scCreditsTopView.getBuyerNumber());
        return statementNo.toString();
    }
}
