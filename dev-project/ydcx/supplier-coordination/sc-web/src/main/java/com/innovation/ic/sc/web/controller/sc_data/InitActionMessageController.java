package com.innovation.ic.sc.web.controller.sc_data;

import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "初始化动作消息表数据到redis中", tags = "InitActionMessageController")
@RestController
@RequestMapping("/api/v2/actionMessage")
@Slf4j
public class InitActionMessageController extends AbstractController {

    /**
     * 将接口/api/v2/actionMessage/initPage需要的数据导入到redis
     * @return
     */
    @ApiOperation(value = "将接口/api/v2/actionMessage/initPage需要的数据导入到redis")
    @GetMapping("initPage")
    public ResponseEntity<ApiResult> initPage() {
        log.info("将接口/api/v2/actionMessage/initPage需要的数据导入到redis");

        ApiResult apiResult = new ApiResult();
        actionMessageService.initPage();
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 将接口/api/v2/actionMessage/initUnReadTotal需要的数据导入到redis
     * @return
     */
    @ApiOperation(value = "将接口/api/v2/actionMessage/initUnReadTotal需要的数据导入到redis")
    @GetMapping("initUnReadTotal")
    public ResponseEntity<ApiResult> initUnReadTotal() {
        log.info("将接口/api/v2/actionMessage/initUnReadTotal需要的数据导入到redis");

        ApiResult apiResult = new ApiResult();
        actionMessageService.initUnReadTotal();
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
