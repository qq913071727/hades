//package com.innovation.ic.sc.web.controller.sc;
//
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.sc.base.model.sc.PersonnelManagement;
//
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.service.sc.PersonnelManagementService;
//import com.innovation.ic.sc.base.vo.PersonnelManagementVo;
//
//import com.innovation.ic.sc.web.controller.AbstractController;
//import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiOperation;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import java.util.List;
//
///**
// * 人员管理API
// * 先在不用了
// */
//@Api(value = "人员管理API", tags = "PersonnelManagementController")
//@RestController
//@RequestMapping("/api/v1/personnelManagement")
//@DefaultProperties(defaultFallback = "defaultFallback")
//public class PersonnelManagementController extends AbstractController {
//    private static final Logger log = LoggerFactory.getLogger(UserController.class);
//
//    /**
//     * 当前主账号下关联的人员
//     *
//     * @return 返回当前主账号下关联的人员信息
//     */
//    @HystrixCommand
//    @ApiOperation(value = "当前主账号下关联的人员")
//    @ApiImplicitParam(name = "personnelManagementVo", value = "人员管理vo类", required = true, dataType = "PersonnelManagementVo")
//    @RequestMapping(value = "/findUserId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> findUserId(@RequestBody PersonnelManagementVo personnelManagementVo, HttpServletRequest request, HttpServletResponse response) {
//
//        if (!StringUtils.validateParameter(personnelManagementVo.getUserId())) {
//            String message = "调用接口【/api/v1/personnelManagement/findUserId时，参数userId不能为空";
//            log.warn(message);
//            ApiResult<Boolean> apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//        ServiceResult<List> listServiceResult = personnelManagementService.findUserId(personnelManagementVo.getUserId());
//        ApiResult<List> apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        apiResult.setResult(listServiceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 批量更新当前主账号下关联的人员
//     *
//     * @return
//     */
//    @HystrixCommand
//    @ApiOperation(value = "批量更新当前主账号下关联的人员")
//    @ApiImplicitParam(name = "personnelManagementVo", value = "人员管理vo类", required = true, dataType = "PersonnelManagementVo")
//    @RequestMapping(value = "/updateManagedUserIds", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> updateManagedUserIds(@RequestBody PersonnelManagementVo personnelManagementVo, HttpServletRequest request, HttpServletResponse response) {
//
//        if (!StringUtils.validateParameter(personnelManagementVo.getUserId()) || personnelManagementVo.getManagedUserIds() == null || personnelManagementVo.getManagedUserIds().size() == 0) {
//            String message = "调用接口【/api/v1/personnelManagement/updateManagedUserIds时，参数userId、managedUserIds不能为空";
//            log.warn(message);
//            ApiResult<Boolean> apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//        personnelManagementService.updateManagedUserIds(personnelManagementVo);
//
//        ApiResult<List<PersonnelManagement>> apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
////    /**
////     * 批量删除当前主账号下关联的人员
////     *
////     * @return
////     */
////    @HystrixCommand
////    @ApiOperation(value = "批量删除当前主账号下关联的人员")
////    @ApiImplicitParam(name = "personnelManagementVo", value = "人员管理vo类", required = true, dataType = "PersonnelManagementVo")
////    @RequestMapping(value = "/deleteManagedUserIds", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
////    @ResponseBody
////    public ResponseEntity<ApiResult> deleteManagedUserIds(@RequestBody PersonnelManagementVo personnelManagementVo, HttpServletRequest request, HttpServletResponse response) {
////
////        if (personnelManagementVo.getIds() == null || personnelManagementVo.getIds().size() == 0) {
////            String message = "调用接口【/api/v1/personnelManagement/deleteManagedUserIds时，参数ids不能为空";
////            log.warn(message);
////            ApiResult<Boolean> apiResult = new ApiResult<>();
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage(message);
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////        personnelManagementService.deleteManagedUserIds(personnelManagementVo);
////
////        ApiResult<List<PersonnelManagement>> apiResult = new ApiResult<>();
////        apiResult.setSuccess(Boolean.TRUE);
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setMessage(ServiceResult.DELETE_SUCCESS);
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//}
