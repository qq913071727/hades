package com.innovation.ic.sc.web.controller.sc;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.pojo.constant.Constants;
import com.innovation.ic.sc.base.pojo.enums.*;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryAddRespPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryGetListRespPojo;
import com.innovation.ic.sc.base.vo.inventory.*;
import com.innovation.ic.sc.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;

/**
 * @desc   库存信息API
 * @author linuo
 * @time   2022年8月23日09:30:29
 */
@Api(value = "库存信息API", tags = "InventoryController")
@RestController
@RequestMapping("/api/v1/inventory")
@DefaultProperties(defaultFallback = "defaultFallback")
public class InventoryController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(InventoryController.class);

    /**
     * 库存新增
     * @return 返回新增结果
     */
    @HystrixCommand
    @ApiOperation(value = "库存新增")
    @ApiImplicitParam(name = "inventoryAddVo", value = "库存新增接口的Vo类", required = true, dataType = "InventoryAddVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> add(@RequestBody InventoryAddVo inventoryAddVo, HttpServletRequest request, HttpServletResponse response) throws InterruptedException, IOException {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        // 校验库存新增接口入参
        JSONObject json = checkInsertParam(inventoryAddVo);
        if(json != null){
            String message = json.getString(Constants.RESULT);
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }

        // 库存新增
        ServiceResult<InventoryAddRespPojo> serviceResult = inventoryService.add(inventoryAddVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 库存信息列表查询
     * @return 返回库存信息列表信息
     */
    @HystrixCommand
    @ApiOperation(value = "库存信息列表查询")
    @ApiImplicitParam(name = "inventoryQueryVo", value = "库存信息列表查询接口的Vo类", required = true, dataType = "InventoryQueryVo")
    @RequestMapping(value = "/query", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> query(@RequestBody InventoryQueryVo inventoryQueryVo, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        ApiResult<JSONObject> apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if (inventoryQueryVo.getPageSize() == null || inventoryQueryVo.getPageNo() == null) {
            String message = "调用接口【/api/v1/inventory/query】时，参数pageSize、pageNo不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<JSONObject> listServiceResult = inventoryService.query(inventoryQueryVo, authenticationUser.getEpId());

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.SELECT_SUCCESS, listServiceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 库存批量导入
     * @return 返回导入结果
     */
    @HystrixCommand
    @ApiOperation(value = "库存批量导入")
    @ApiImplicitParam(name = "inventoryBatchAddVo", value = "库存批量导入接口的Vo类", required = true, dataType = "InventoryBatchAddVo")
    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> batchAdd(@RequestBody InventoryBatchAddVo inventoryBatchAddVo, HttpServletRequest request, HttpServletResponse response) throws InterruptedException, IOException {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, Boolean.FALSE, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        // inventoryList去重
        List<InventoryBatchAddParamVo> inventoryList = removeRepeatElement(inventoryBatchAddVo.getInventoryList());
        inventoryBatchAddVo.setInventoryList(inventoryList);

        // 校验库存批量导入接口入参
        JSONObject json = checkBatchAddParam(inventoryBatchAddVo);
        if(json != null){
            String message = json.getString(Constants.RESULT);
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, Boolean.FALSE, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.OK);
        }

        // 库存批量导入
        ServiceResult<InventoryAddRespPojo> serviceResult = inventoryService.batchAdd(inventoryBatchAddVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 优势型号状态变更
     * @return 返回操作结果
     */
    @HystrixCommand
    @ApiOperation(value = "优势型号状态变更")
    @ApiImplicitParam(name = "inventoryBatchAddVo", value = "库存批量导入接口的Vo类", required = true, dataType = "InventoryBatchAddVo")
    @RequestMapping(value = "/updateAdvantageStatus", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> updateAdvantageStatus(@RequestBody InventoryUpdateAdvantageStatusVo inventoryUpdateAdvantageStatusVo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, Boolean.FALSE, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(!inventoryUpdateAdvantageStatusVo.getIsAll() && (inventoryUpdateAdvantageStatusVo.getIdList() == null || inventoryUpdateAdvantageStatusVo.getIdList().size() == 0)){
            String message = "调用接口【/api/v1/inventory/updateAdvantageStatus】时，参数idList不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if(inventoryUpdateAdvantageStatusVo.getOperateType() == null || Strings.isNullOrEmpty(inventoryUpdateAdvantageStatusVo.getValidityDateStart()) || Strings.isNullOrEmpty(inventoryUpdateAdvantageStatusVo.getValidityDateEnd())){
            if(inventoryUpdateAdvantageStatusVo.getOperateType().intValue() == AdvantageStatusEnum.EFFECTIVE.getScStatusCode().intValue()){
                String message = "有效期不能为空";
                log.warn(message);
                apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
                return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
            }
        }

        // 优势型号状态变更
        ServiceResult<Boolean> serviceResult = inventoryService.updateAdvantageStatus(inventoryUpdateAdvantageStatusVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 更新库存时间
     * @return 返回更新结果
     */
    @HystrixCommand
    @ApiOperation(value = "更新库存时间")
    @ApiImplicitParam(name = "inventoryUpdateDateVo", value = "更新库存时间的Vo类", required = true, dataType = "InventoryUpdateDateVo")
    @RequestMapping(value = "/updateInventoryDate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> updateInventoryDate(@RequestBody InventoryUpdateDateVo inventoryUpdateDateVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult<Boolean> apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(!inventoryUpdateDateVo.getIsAll() && (inventoryUpdateDateVo.getIdList() == null || inventoryUpdateDateVo.getIdList().size() == 0)){
            String message = "调用接口【/api/v1/inventory/updateInventoryDate】时，参数idList不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 库存时间更新
        inventoryService.updateInventoryDate(inventoryUpdateDateVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.UPDATE_SUCCESS, Boolean.TRUE, Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 确认标准匹配结果
     * @return 返回查询结果
     */
    /*@HystrixCommand
    @ApiOperation(value = "确认标准匹配结果")
    @ApiImplicitParam(name = "InventoryConfirmMatchResultVo", value = "库存确认匹配结果的Vo类", required = true, dataType = "InventoryConfirmMatchResultVo")
    @RequestMapping(value = "/confirmStandardMatchResult", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> confirmStandardMatchResult(@RequestBody InventoryConfirmMatchResultVo inventoryConfirmMatchResultVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if (inventoryConfirmMatchResultVo.getDataList() == null || inventoryConfirmMatchResultVo.getDataList().size() == 0) {
            String message = "调用接口【/api/v1/inventory/confirmStandardMatchResult】时，参数dataList不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 校验dataList中的内容是否有问题
        for(InventoryMatchResultVo inventoryMatchResultVo : inventoryConfirmMatchResultVo.getDataList()) {
            if (inventoryMatchResultVo.getId() == null || Strings.isNullOrEmpty(inventoryMatchResultVo.getBzkBrand()) || Strings.isNullOrEmpty(inventoryMatchResultVo.getBzkPartNumber())) {
                String message = "调用接口【/api/v1/inventory/confirmStandardMatchResult】时，参数dataList内各条数据中的id、bzkPartNumber和bzkBrand均不能为空";
                log.warn(message);
                apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
                return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
            }
        }

        // 校验dataList中的标准匹配型号和标准匹配品牌是否被已有数据使用
        ServiceResult<List<Integer>> serviceResult = inventoryService.judgeStandardDataIfHaveUsed(inventoryConfirmMatchResultVo.getDataList(), authenticationUser.getEpId());
        if(serviceResult.getResult() != null && serviceResult.getResult().size() > 0){
            String message = "dataList中有标准匹配品牌和标准匹配型号数据重复或标准匹配信息已经被使用过，请删除有问题的数据";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.OK.value(), message, serviceResult.getResult(), Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 确认标准匹配结果
        ServiceResult<Boolean> listServiceResult = inventoryService.confirmStandardMatchResult(inventoryConfirmMatchResultVo);

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.OPERATE_SUCCESS, listServiceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }*/

    /**
     * 未匹配成功数据列表查询
     * @return 返回查询结果
     */
    /*@HystrixCommand
    @ApiOperation(value = "未匹配成功数据列表查询")
    @ApiImplicitParam(name = "DefaultQueryVo", value = "列表查询接口的默认Vo类", required = true, dataType = "DefaultQueryVo")
    @RequestMapping(value = "/queryUnMatchData", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryUnMatchData(@RequestBody DefaultQueryVo defaultQueryVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult<InventoryUnMatchDataQueryRespPojo> apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if (defaultQueryVo.getPageSize() == null || defaultQueryVo.getPageNo() == null) {
            String message = "调用接口【/api/v1/inventory/queryUnMatchData】时，参数pageSize、pageNo均不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<InventoryUnMatchDataQueryRespPojo> listServiceResult = inventoryService.queryUnMatchData(defaultQueryVo, authenticationUser.getEpId());

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.SELECT_SUCCESS, listServiceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }*/

    /**
     * 库存信息列表批量查询
     * @return 返回 库存信息列表信息
     */
    @HystrixCommand
    @ApiOperation(value = "库存信息列表批量查询")
    @ApiImplicitParam(name = "inventoryBatchQueryVo", value = "库存信息列表批量查询接口的Vo类", required = true, dataType = "InventoryBatchQueryVo")
    @RequestMapping(value = "/getInventoryList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> getInventoryList(@RequestBody InventoryBatchQueryVo inventoryBatchQueryVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if (inventoryBatchQueryVo.getIdList() == null || inventoryBatchQueryVo.getIdList().size() == 0){
            String message = "调用接口【/api/v1/inventory/getInventoryList】时，参数idList不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<InventoryGetListRespPojo>> inventoryServiceResult = inventoryService.batchQuery(inventoryBatchQueryVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.SELECT_SUCCESS, inventoryServiceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 批量导出
     * @return 返回库存信息列表信息
     */
    @HystrixCommand
    @ApiOperation(value = "批量导出")
    @ApiImplicitParam(name = "inventoryBatchQueryVo", value = "批量导出接口的Vo类", required = true, dataType = "InventoryBatchQueryVo")
    @RequestMapping(value = "/batchExport", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> batchExport(@RequestBody InventoryBatchQueryVo inventoryBatchQueryVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if (inventoryBatchQueryVo.getIdList() == null || inventoryBatchQueryVo.getIdList().size() == 0){
            // 查询当前供应商全部匹配上标准库的数据id集合
            ServiceResult<List<Integer>> serviceResult = inventoryService.getMatchStandardLibraryInventoryIds(authenticationUser.getEpId());
            inventoryBatchQueryVo.setIdList(serviceResult.getResult());
        }

        ServiceResult<List<InventoryGetListRespPojo>> inventoryServiceResult = inventoryService.batchQuery(inventoryBatchQueryVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.SELECT_SUCCESS, inventoryServiceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 库存信息编辑更新
     * @return 返回编辑结果
     */
    @HystrixCommand
    @ApiOperation(value = "库存信息编辑更新")
    @ApiImplicitParam(name = "inventoryAddVo", value = "库存信息编辑更新接口的Vo类", required = true, dataType = "InventoryAddVo")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> update(@RequestBody InventoryEditVo inventoryEditVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult apiResult;
        if (inventoryEditVo.getId() == null || inventoryEditVo.getCount() == null || !StringUtils.validateParameter(inventoryEditVo.getBatch())){
            String message = "调用接口【/api/v1/inventory/update】时，参数id、count和batch不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 库存信息编辑更新
        ServiceResult<Boolean> serviceResult = inventoryService.edit(inventoryEditVo);

        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 库存上架/下架
     * @return 返回更新结果
     */
    @HystrixCommand
    @ApiOperation(value = "库存上架/下架")
    @ApiImplicitParam(name = "inventoryStatusUpdateReqVo", value = "库存上架/下架接口的Vo类", required = true, dataType = "InventoryStatusUpdateReqVo")
    @RequestMapping(value = "/updateStatus", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> updateStatus(@RequestBody InventoryStatusUpdateReqVo inventoryStatusUpdateReqVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult<Boolean> apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(inventoryStatusUpdateReqVo.getUpdateStatus() == null || inventoryStatusUpdateReqVo.getIdList() == null || inventoryStatusUpdateReqVo.getIdList().size() == 0){
            String message = "调用接口【/api/v1/inventory/updateStatus】时，参数updateStatus和idList均不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if((inventoryStatusUpdateReqVo.getIsAll() == null || !inventoryStatusUpdateReqVo.getIsAll()) &&
                (inventoryStatusUpdateReqVo.getIdList() == null || inventoryStatusUpdateReqVo.getIdList().size() == 0)){
            String message = "调用接口【/api/v1/inventory/updateStatus】时，参数idList不能为空";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 库存上架/下架
        inventoryService.updateStatus(inventoryStatusUpdateReqVo, authenticationUser);

        /*ServiceResult<List<Map<String, String>>> listServiceResult = inventoryService.updateStatus(inventoryStatusUpdateReqVo, authenticationUser);

        // 库存上架/下架对优势品牌、优势型号状态不影响，暂时去掉此段逻辑代码
        List<Map<String, String>> result = listServiceResult.getResult();
        if(result != null && result.size() > 0){
            // 优势品牌、优势型号状态修改
            AdvantageStatusEditThread advantageStatusEditThread = new AdvantageStatusEditThread(advantageBrandService, advantageModelService, result, inventoryStatusUpdateReqVo.getUpdateStatus(), authenticationUser.getEpId());
            threadPoolManager.execute(advantageStatusEditThread);
        }*/

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.UPDATE_SUCCESS, Boolean.TRUE, Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

//    @HystrixCommand
//    @ApiOperation(value = "默认阶梯价格配置查询")
//    @RequestMapping(value = "/getDefaultLadderPrice", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    public ResponseEntity<ApiResult<List<DefaultLadderPricePojo>>> getDefaultLadderPrice(HttpServletRequest request) {
//        ApiResult<List<DefaultLadderPricePojo>> apiResult;
//
//        // 用户信息
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        ServiceResult<List<DefaultLadderPricePojo>> serviceResult = inventoryService.getDefaultLadderPrice(authenticationUser.getEpId());
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }

    @HystrixCommand
    @ApiOperation(value = "库存阶梯价格配置")
    @ApiImplicitParam(name = "inventoryLadderPriceVo", value = "更新阶梯价格和库存的Vo类", required = true, dataType = "InventoryLadderPriceVo")
    @RequestMapping(value = "/setLadderPrice", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult<Boolean>> setLadderPrice(@RequestBody InventoryLadderPriceVo inventoryLadderPriceVo, HttpServletRequest request) {
        ApiResult<Boolean> apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(inventoryLadderPriceVo.getInventoryList() == null || inventoryLadderPriceVo.getInventoryList().size() == 0){
            String message = "调用接口【/api/v1/inventory/setLadderPrice】时，参数inventoryList不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = inventoryService.setLadderPrice(authenticationUser.getEpId(), inventoryLadderPriceVo);

        apiResult = new ApiResult(HttpStatus.OK.value(), serviceResult.getMessage(), serviceResult.getResult(), serviceResult.getSuccess());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 库存信息删除
     * @return 返回删除结果
     */
    @HystrixCommand
    @ApiOperation(value = "库存信息删除")
    @ApiImplicitParam(name = "InventoryDeleteVo", value = "删除库存数据的Vo类", required = true, dataType = "InventoryDeleteVo")
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> delete(@RequestBody InventoryDeleteVo inventoryDeleteVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApiResult<Boolean> apiResult;

        // 用户信息
        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
        if (null == authenticationUser){
            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
        }

        if(inventoryDeleteVo.getIdList() == null || inventoryDeleteVo.getIdList().size() == 0){
            String message = "调用接口【/api/v1/inventory/delete】时，参数idList不能为空";
            log.warn(message);
            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 库存信息删除
        ServiceResult<Boolean> serviceResult = inventoryService.delete(inventoryDeleteVo, authenticationUser);

        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.DELETE_SUCCESS, serviceResult.getResult(), Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 模糊查询标准库型号
     * @return 返回模糊查询标准库型号结果
     */
//    @HystrixCommand
//    @ApiOperation(value = "模糊查询标准库型号")
//    @RequestMapping(value = "/blurQueryBzkPartNumberList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> blurQueryBzkPartNumberList(@RequestBody InventoryBlurQueryBzkPartNumberVo inventoryBlurQueryBzkPartNumberVo, HttpServletRequest request, HttpServletResponse response) throws ParseException {
//        ApiResult<JSONObject> apiResult;
//
//        // 用户信息
//        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
//        if (null == authenticationUser){
//            apiResult = new ApiResult(ApiResult.TOKEN_ILLEGAL_EXPIRE, null, null, Boolean.FALSE);
//            return new ResponseEntity(apiResult, HttpStatus.UNAUTHORIZED);
//        }
//
//        if (Strings.isNullOrEmpty(inventoryBlurQueryBzkPartNumberVo.getPartNumber())) {
//            String message = "调用接口【/api/v1/inventory/blurQueryBzkPartNumberList】时，参数partNumber不能为空";
//            log.warn(message);
//            apiResult = new ApiResult(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> listServiceResult = productsService.blurQueryBzkPartNumberList(inventoryBlurQueryBzkPartNumberVo.getPartNumber());
//
//        apiResult = new ApiResult(HttpStatus.OK.value(), ServiceResult.SELECT_SUCCESS, listServiceResult.getResult(), Boolean.TRUE);
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }

    /**
     * 校验库存批量导入接口入参
     * @param inventoryBatchAddVo 库存批量导入接口的Vo类
     * @return 返回校验结果
     */
    private JSONObject checkBatchAddParam(InventoryBatchAddVo inventoryBatchAddVo) {
        JSONObject json = null;

        if(inventoryBatchAddVo.getImportType() == null){
            json = new JSONObject();
            json.put(Constants.RESULT, "参数importType不能为空");
            return json;
        }

        List<InventoryBatchAddParamVo> inventoryList = inventoryBatchAddVo.getInventoryList();
        for(InventoryBatchAddParamVo inventoryBatchAddParamVo: inventoryList){
            if (!StringUtils.validateParameter(inventoryBatchAddParamVo.getPartNumber())
                    || !StringUtils.validateParameter(inventoryBatchAddParamVo.getBrand())
                    || inventoryBatchAddParamVo.getCount() == null
                    || !StringUtils.validateParameter(inventoryBatchAddParamVo.getBatch())) {
                json = new JSONObject();
                json.put(Constants.RESULT, "保存库存数据时，型号、品牌、数量和批次均不能为空，请您检查这几项数据后重新进行提交操作");
                return json;
            }
        }

        return json;
    }

    /**
     * 校验库存新增接口入参
     * @return 返回校验结果
     */
    private JSONObject checkInsertParam(InventoryAddVo inventoryAddVo) {
        JSONObject json = null;

        // 库存数据
        List<InventoryAddParamVo> inventoryList = inventoryAddVo.getInventoryList();
        if(inventoryList == null){
            json = new JSONObject();
            json.put(Constants.RESULT, "库存数据为空，请您将内容填写完整后再进行提交操作");
            return json;
        }

        // 校验库存数据中的参数
        for (InventoryAddParamVo inventoryAddParamVo : inventoryList) {
            if (!StringUtils.validateParameter(inventoryAddParamVo.getPartNumber())
                    || !StringUtils.validateParameter(inventoryAddParamVo.getBrand())
                    || inventoryAddParamVo.getCount() == null
                    || !StringUtils.validateParameter(inventoryAddParamVo.getBatch())
                    || inventoryAddParamVo.getCurrency() == null
                    || inventoryAddParamVo.getInventoryHome() == null
                    || inventoryAddParamVo.getUnitPrice() == null
            ) {
                json = new JSONObject();
                json.put(Constants.RESULT, "保存库存数据时，型号、品牌、数量、批次、币种、库存所在地和单价/最低阶梯价均不能为空，请您检查这几项数据后重新进行提交操作");
                return json;
            }

            // 假如单价类型含有阶梯价格，校验阶梯价格id是否为空
            if(inventoryAddParamVo.getUnitPriceType() != null && inventoryAddParamVo.getUnitPriceType().intValue() == UnitPriceTypeEnum.LADDER_PRICE.getCode().intValue()){
                if(inventoryAddParamVo.getLadderPriceId() == null){
                    json = new JSONObject();
                    json.put(Constants.RESULT, "库存内容的价格类型为阶梯价格时，需要选择阶梯价格配置，请您选择后再进行提交操作");
                    return json;
                }
            }
        }

        return json;
    }

    /**
     * 去除list中的重复元素
     * @param list list
     * @return 返回处理后的结果
     */
    private static List removeRepeatElement(List list){
        HashSet hashSet=new HashSet(list);
        list.clear();
        list.addAll(hashSet);
        return list;
    }
}