package com.innovation.ic.sc.web.controller.sc_data;

import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc   初始化优势型号表数据到redis中
 * @author linuo
 * @time   2023年1月29日10:58:27
 */
@Api(value = "初始化优势型号表数据到redis中", tags = "InitAdvantageModelController")
@RestController
@RequestMapping("/api/v2/advantageModel")
@Slf4j
public class InitAdvantageModelController extends AbstractController {
    @ApiOperation(value = "将advantage_model表的数据导入到redis")
    @GetMapping("initAdvantageModelData")
    public ResponseEntity<ApiResult> initAdvantageModelData() {
        ApiResult apiResult = new ApiResult();
        advantageModelService.initAdvantageModelData();
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation(value = "将advantage_model表中所有型号的所属品牌数据导入到redis，提供给根据型号查询所属品牌接口使用")
    @GetMapping("initAdvantageModelBrand")
    public ResponseEntity<ApiResult> initAdvantageModelBrand() {
        ApiResult apiResult = new ApiResult();
        advantageModelService.initAdvantageModelBrand();
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}