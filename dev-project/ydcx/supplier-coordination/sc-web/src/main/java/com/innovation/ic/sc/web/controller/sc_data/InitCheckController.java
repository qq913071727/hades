package com.innovation.ic.sc.web.controller.sc_data;

import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "初始用户表数据到redis中", tags = "InitUserController")
@RestController
@RequestMapping("/api/v2/check")
@Slf4j
public class InitCheckController extends AbstractController {

    @ApiOperation(value = "将check数据导入到redis")
    @GetMapping("initCheckData")
    public ResponseEntity<ApiResult> initCheckData() {
        ApiResult apiResult = new ApiResult();
        userService.initCheckData();
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setResult(Boolean.TRUE);
        apiResult.setMessage("初始化完成");
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

}
