package com.innovation.ic.sc.web.schedule;

import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.erp9_pvebcs.CompanyCurrencyService;
import com.innovation.ic.sc.base.service.erp9_pvebcs.ErpDictionariesService;
import com.innovation.ic.sc.base.service.erp9_pvecrm.CompanySCTopViewService;
import com.innovation.ic.sc.base.service.erp9_pvecrm.EnterprisesTopViewService;
import com.innovation.ic.sc.base.service.erp9_pvecrm.ScCreditsTopViewService;
import com.innovation.ic.sc.base.service.erp9_pvecrm.SpecialsSCTopViewService;
import com.innovation.ic.sc.base.service.erp9_pvesiteuser.MenusService;
import com.innovation.ic.sc.base.service.erp9_pvesiteuser.UsersService;
import com.innovation.ic.sc.base.service.erp9_pvestandard.BrandsService;
import com.innovation.ic.sc.base.service.sc.*;
import com.innovation.ic.sc.base.service.sc_erp.AccountStatementService;
import com.innovation.ic.sc.base.value.ErpInterfaceAddressConfig;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * 定时任务的抽象类
 */
public abstract class AbstractSchedule {
    @Resource
    protected ServiceHelper serviceHelper;

    @Resource
    protected ModelHandler modelHandler;

    @Resource
    protected MenusService menusService;

    @Resource
    protected MenuTreeService menuTreeService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected UsersService usersService;

    @Resource
    protected ErpDictionariesService erpDictionariesService;

    @Resource
    protected DictionariesService dictionariesService;

    @Resource
    protected AdvantageModelService advantageModelService;

    @Resource
    protected SpecialsSCTopViewService specialsSCTopViewService;

    @Resource
    protected EnterprisesTopViewService enterprisesTopViewService;

    @Resource
    protected ErpMyCompanyNameService erpMyCompanyNameService;

    @Resource
    protected CompanySCTopViewService companySCTopViewService;

    @Resource
    protected AdvantageBrandService advantageBrandService;

    @Resource
    protected AccountStatementService accountStatementService;

    @Resource
    protected ScCreditsTopViewService scCreditsTopViewService;

    @Resource
    protected MyCompanyEnterpriseService myCompanyEnterpriseService;

    @Resource
    protected MyCompanyService myCompanyService;

    @Resource
    protected CompanyCurrencyService companyCurrencyService;

    @Resource
    protected MyCompanyCurrencyService myCompanyCurrencyService;

    @Autowired
    protected BrandService brandService;

    @Autowired
    protected BrandsService brandsService;

    @Resource
    protected ErpInterfaceAddressConfig erpInterfaceAddressConfig;
}