//package com.innovation.ic.sc.web.interceptor;
//
//import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
//import com.baomidou.mybatisplus.extension.parser.JsqlParserSupport;
//import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
//import com.innovation.ic.b1b.framework.helper.LockHelper;
//import lombok.extern.slf4j.Slf4j;
//import net.sf.jsqlparser.expression.Expression;
//import net.sf.jsqlparser.expression.StringValue;
//import net.sf.jsqlparser.parser.SimpleNode;
//import net.sf.jsqlparser.schema.Column;
//import net.sf.jsqlparser.schema.Table;
//import net.sf.jsqlparser.statement.delete.Delete;
//import net.sf.jsqlparser.statement.insert.Insert;
//import net.sf.jsqlparser.statement.select.Join;
//import net.sf.jsqlparser.statement.select.OrderByElement;
//import net.sf.jsqlparser.statement.select.Select;
//import net.sf.jsqlparser.statement.update.Update;
//import org.apache.ibatis.executor.Executor;
//import org.apache.ibatis.executor.statement.StatementHandler;
//import org.apache.ibatis.mapping.BoundSql;
//import org.apache.ibatis.mapping.MappedStatement;
//import org.apache.ibatis.session.ResultHandler;
//import org.apache.ibatis.session.RowBounds;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.*;
//
//@Slf4j
//public class DistributedTransactionInterceptor extends JsqlParserSupport implements InnerInterceptor {
//
//    /**
//     * select之前执行，beforeQuery之前执行
//     * @param executor
//     * @param ms
//     * @param parameter
//     * @param rowBounds
//     * @param resultHandler
//     * @param boundSql
//     * @return
//     * @throws SQLException
//     */
//    @Override
//    public boolean willDoQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
//        log.info("进入DistributedTransactionInterceptor类的willDoQuery方法");
//        return true;
//    }
//
//    /**
//     * select之前执行，willDoQuery之后执行
//     * @param executor
//     * @param ms
//     * @param parameter
//     * @param rowBounds
//     * @param resultHandler
//     * @param boundSql
//     * @throws SQLException
//     */
//    @Override
//    public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds,
//                            ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
//        log.info("进入DistributedTransactionInterceptor类的beforeQuery方法");
//    }
//
//    /**
//     * update之前执行，在beforeUpdate之前执行
//     * @param executor
//     * @param ms
//     * @param parameter
//     * @return
//     * @throws SQLException
//     */
//    @Override
//    public boolean willDoUpdate(Executor executor, MappedStatement ms, Object parameter) throws SQLException {
//        log.info("进入DistributedTransactionInterceptor类的willDoUpdate方法");
//
//        return true;
//    }
//
//    /**
//     * update之前执行，在willDoUpdate之后执行
//     * @param executor
//     * @param ms
//     * @param parameter
//     * @throws SQLException
//     */
//    @Override
//    public void beforeUpdate(Executor executor, MappedStatement ms, Object parameter) throws SQLException {
//        log.info("进入DistributedTransactionInterceptor类的beforeUpdate方法");
//    }
//
//    /**
//     * willDoUpdate、beforeUpdate之后执行
//     * @param sh
//     * @param connection
//     * @param transactionTimeout
//     */
//    @Override
//    public void beforePrepare(StatementHandler sh, Connection connection, Integer transactionTimeout) {
//        log.info("进入DistributedTransactionInterceptor类的beforePrepare方法");
//
//        String zookeeperNode = LockHelper.getThreadLocal().get();
//        BoundSql boundSql = sh.getBoundSql();
//        PluginUtils.MPBoundSql mpBoundSql = PluginUtils.mpBoundSql(boundSql);
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("zookeeper_node", zookeeperNode);
//        log.info("sql中准备插入的参数为【zookeeper_node:{}】", zookeeperNode);
//        // 调用方法parserSingle后，出发processUpdate方法，map是其中的参数
//        mpBoundSql.sql(parserSingle(mpBoundSql.sql(), map));
//    }
//
//    @Override
//    public void setProperties(Properties properties) {
//        log.info("进入DistributedTransactionInterceptor类的setProperties方法");
//    }
//
//    @Override
//    protected void processSelect(Select select, int index, Object obj) {
//        log.info("进入DistributedTransactionInterceptor类的processSelect方法");
//    }
//
//    @Override
//    protected void processInsert(Insert insert, int index, Object obj) {
//        log.info("进入DistributedTransactionInterceptor类的processInsert方法");
//
////        Table table = insert.getTable();
////        if (table.getName().equals("inventory")){
////            List<Column> columnList = insert.getColumns();
////            Column column = new Column();
////            column.setColumnName("zookeeper_node");
////            columnList.add(column);
////            insert.setColumns(columnList);
////
////            Map map = (Map) obj;
////            String zookeeperNode = (String) map.get("zookeeper_node");
////            List<Expression> expressionList = insert..getExpressions();
////            Expression expression = new StringValue(zookeeperNode);
////            expressionList.add(expression);
////            insert.setExpressions(expressionList);
////
////            log.info("最终执行的sql为:{}", insert.toString());
////        }
//    }
//
//    @Override
//    protected void processDelete(Delete delete, int index, Object obj) {
//        log.info("进入DistributedTransactionInterceptor类的processDelete方法");
//
//        // 使用sql更新zookeeper_node字段
//
//        Table table = delete.getTable();
//        Expression deleteWhere = delete.getWhere();
//        SimpleNode simpleNode = deleteWhere.getASTNode();
//        List<Join> joinList = delete.getJoins();
//        List<Table> tableList = delete.getTables();
//        List<OrderByElement> orderByElementList = delete.getOrderByElements();
//        String str = delete.toString();
////
//        Update update = new Update();
//        update.setTable(table);
////
////        List<Column> columnList = new ArrayList<>();
////        Column column = new Column();
////        column.setColumnName("zookeeper_node");
////        columnList.add(column);
////        update.setColumns(columnList);
////
////        Map map = (Map) obj;
////        String zookeeperNode = (String) map.get("zookeeper_node");
////        List<Expression> expressionList = new ArrayList<>();
////        Expression expression = new StringValue(zookeeperNode);
////        expressionList.add(expression);
////        update.setExpressions(expressionList);
////        update.setWhere(deleteWhere);
////        this.processUpdate(update, index, obj);
//    }
//
//    @Override
//    protected void processUpdate(Update update, int index, Object obj) {
//        log.info("进入DistributedTransactionInterceptor类的processUpdate方法");
//
//        Table table = update.getTable();
//        if (table.getName().equals("inventory")){
//            List<Column> columnList = update.getColumns();
//            Column column = new Column();
//            column.setColumnName("zookeeper_node");
//            columnList.add(column);
//            update.setColumns(columnList);
//
//            Map map = (Map) obj;
//            String zookeeperNode = (String) map.get("zookeeper_node");
//            List<Expression> expressionList = update.getExpressions();
//            Expression expression = new StringValue(zookeeperNode);
//            expressionList.add(expression);
//            update.setExpressions(expressionList);
//
//            log.info("最终执行的sql为:{}", update.toString());
//        }
//    }
//}
