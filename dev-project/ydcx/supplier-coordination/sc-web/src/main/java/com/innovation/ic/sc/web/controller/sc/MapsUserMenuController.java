//package com.innovation.ic.sc.web.controller.sc;
//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.sc.base.model.sc.Menu;
//import com.innovation.ic.sc.base.pojo.variable.*;
//import com.innovation.ic.sc.base.vo.MapsUserMenuVo;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
//@Api(value = "菜单和用户关系API", tags = "MapsUserMenuController")
//@RestController
//@RequestMapping("/api/v1/mapsUserMenu")
//@Slf4j
//public class MapsUserMenuController extends AbstractController {
//
//    /**
//     * 主账号创建新的角色和对应的菜单权限，在点击确定按钮时使用
//     * @param request
//     * @param response
//     * @return
//     */
////    @HystrixCommand
//    @ApiOperation(value = "主账号创建新的角色和对应的菜单权限，在点击确定按钮时使用")
//    @ApiImplicitParam(name = "roleId", value = "角色id", required = true, dataType = "Integer")
//    @RequestMapping(value = "/addRoleAndMenu", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<List<MenuItemPojo>>> addRoleAndMenu(HttpServletRequest request, HttpServletResponse response) {
//
//        ApiResult<List<MenuItemPojo>> apiResult = new ApiResult<>();
////        apiResult.setSuccess(serviceResult.getSuccess());
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setMessage(serviceResult.getMessage());
////        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 主账号修改角色名称和对应的菜单权限时使用
//     * @param request
//     * @param response
//     * @return
//     */
////    @HystrixCommand
//    @ApiOperation(value = "主账号修改角色名称和对应的菜单权限时使用")
//    @RequestMapping(value = "/updateRoleAndMenu", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<List<MenuItemPojo>>> updateRoleAndMenu(HttpServletRequest request, HttpServletResponse response) {
//
//        ApiResult<List<MenuItemPojo>> apiResult = new ApiResult<>();
////        apiResult.setSuccess(serviceResult.getSuccess());
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setMessage(serviceResult.getMessage());
////        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 账号管理-权限设置。根据子账号id，展示对应的角色和菜单权限
//     * @param request
//     * @param response
//     * @return
//     */
////    @HystrixCommand
//    @ApiOperation(value = "账号管理-权限设置。根据子账号id，展示对应的角色和菜单权限")
//    @RequestMapping(value = "/showRoleAndMenuByUserId", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<List<MenuItemPojo>>> showRoleAndMenuByUserId(HttpServletRequest request, HttpServletResponse response) {
//
//        ApiResult<List<MenuItemPojo>> apiResult = new ApiResult<>();
////        apiResult.setSuccess(serviceResult.getSuccess());
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setMessage(serviceResult.getMessage());
////        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 账号管理-权限设置。根据子账号id，修改对应的角色和菜单权限
//     * @param request
//     * @param response
//     * @return
//     */
////    @HystrixCommand
//    @ApiOperation(value = "账号管理-权限设置。根据子账号id，修改对应的角色和菜单权限")
//    @RequestMapping(value = "/updateRoleAndMenuByUserId", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<List<MenuItemPojo>>> updateRoleAndMenuByUserId(HttpServletRequest request, HttpServletResponse response) {
//
//        ApiResult<List<MenuItemPojo>> apiResult = new ApiResult<>();
////        apiResult.setSuccess(serviceResult.getSuccess());
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setMessage(serviceResult.getMessage());
////        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
////    @ApiOperation(value = "添加权限")
////    @PostMapping("addMapsUserMenu")
////    public ResponseEntity<ApiResult> addMapsUserMenu(@RequestBody List<MapsUserMenuVo> mapsUserMenuVoList, HttpServletRequest request) {
////        ApiResult apiResult = new ApiResult<>();
////        if (mapsUserMenuVoList == null || mapsUserMenuVoList.size() == 0) {
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage("mapsUserMenuVo参数不能空");
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
////        if (authenticationUser == null) {
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage("请重新登陆");
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////        for (MapsUserMenuVo mapsUserMenuVo : mapsUserMenuVoList) {
////            if (!StringUtils.validateParameter(mapsUserMenuVo.getUserId())) {
////                apiResult.setSuccess(Boolean.FALSE);
////                apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////                apiResult.setMessage("userId不能为空");
////                return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////            }
////        }
////        mapsUserMenuService.addMapsUserMenu(mapsUserMenuVoList, authenticationUser.getId());
////        apiResult.setSuccess(Boolean.TRUE);
////        apiResult.setCode(HttpStatus.OK.value());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
////
////
////    @ApiOperation(value = "查询用户权限")
////    @ApiImplicitParams(
////            {@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String"),
////                    @ApiImplicitParam(name = "type", value = "1 展示所有一级菜单", required = true, dataType = "String"),
////                    @ApiImplicitParam(name = "id", value = "当前菜单id,传入展示当前菜单下所有子菜单", required = false, dataType = "String")
////            }
////    )
////    @GetMapping(value = "findByMapsUserMenu/{type}/{id}")
////    public ResponseEntity<ApiResult<List<Menu>>> findByMapsUserMenu(HttpServletRequest request, @PathVariable("type") String type, @PathVariable("id") String id) {
////        ApiResult apiResult = new ApiResult<>();
////        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
////        if (authenticationUser == null) {
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage("请重新登陆");
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////        ServiceResult<List<MenusPojo>> serviceResult = mapsUserMenuService.findByMapsUserMenu(authenticationUser.getId(), type, id);
////        apiResult.setSuccess(serviceResult.getSuccess());
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setResult(serviceResult.getResult());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
////
////    /**
////     * 分页展示主账号下子账号信息
////     *
////     * @return
////     */
////    @ApiOperation(value = "分页展示主账号下子账号信息")
////    @ApiImplicitParams(
////            {
////                    @ApiImplicitParam(name = "current", value = "当前页数", required = true, dataType = "int"),
////                    @ApiImplicitParam(name = "size", value = "每页长度", required = true, dataType = "int")
////            }
////    )
////    @GetMapping("findSonUserPage")
////    @ResponseBody
////    public ResponseEntity<ApiResult<IPage<UsersPojo>>> findSonUserPage(HttpServletRequest request ,@RequestParam("current") int current, @RequestParam("size") int size) {
////        ApiResult<IPage<UsersPojo>> apiResult = new ApiResult<>();
////        AuthenticationUser authenticationUser = this.getAuthenticationUser(request);
////        if (authenticationUser == null) {
////            apiResult.setSuccess(Boolean.FALSE);
////            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
////            apiResult.setMessage("请重新登陆");
////            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
////        }
////        ServiceResult<IPage<UsersPojo>> sonUserPage = userService.findSonUserPage(authenticationUser.getId(), current, size);
////        apiResult.setSuccess(Boolean.TRUE);
////        apiResult.setCode(HttpStatus.OK.value());
////        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
////        apiResult.setResult(sonUserPage.getResult());
////        return new ResponseEntity<>(apiResult, HttpStatus.OK);
////    }
//
//
//}
