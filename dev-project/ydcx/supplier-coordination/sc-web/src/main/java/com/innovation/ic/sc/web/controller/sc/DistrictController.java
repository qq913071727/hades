//package com.innovation.ic.sc.web.controller.sc;
//
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.sc.base.model.erp9_pvebcs.ErpDictionaries;
//import com.innovation.ic.sc.base.model.sc.Dictionaries;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.service.sc.DictionariesService;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.util.List;
//
//@Api(value = "查询字典信息", tags = "DistrictController")
//@RestController
//@RequestMapping("/api/v1/district")
//@Slf4j
//public class DistrictController extends AbstractController {
//
//    @ApiOperation(value = "获取字典信息")
//    @GetMapping("7")
//    public ResponseEntity<ApiResult<List<ErpDictionaries>>> searchDistrict(@RequestParam String dTypeId) {
//        ApiResult apiResult = new ApiResult<>();
//        if (!StringUtils.validateParameter(dTypeId)) {
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage("dTypeId不能为空");
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//        ServiceResult<List<Dictionaries>> serviceResult = dictionariesService.searchDistrict(dTypeId);
//        apiResult.setSuccess(serviceResult.getSuccess());
//        apiResult.setResult(serviceResult.getResult());
//        apiResult.setMessage(serviceResult.getMessage());
//        apiResult.setCode(HttpStatus.OK.value());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//
//}
