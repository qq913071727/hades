//package com.innovation.ic.sc.web.controller.sc;
//
//import com.innovation.ic.sc.base.model.sc.Product;
//import com.innovation.ic.sc.base.pojo.variable.ApiResult;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.service.sc.ProductService;
//import com.innovation.ic.sc.web.controller.AbstractController;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
///**
// * @Description: 产品型号控制器
// * @ClassName: BrandsController
// * @Author: myq
// * @Date: 2022/8/9 上午11:01
// * @Version: 1.0
// */
//@RestController
//@RequestMapping("/api/v1/productsController")
//@Api(value = "产品型号", tags = "ProductsController")
//@Slf4j
//public class ProductsController extends AbstractController {
//
//    /**
//     * @return
//     * @description 根据品牌ID查询产品列表
//     * 这个接口不再用了
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/9 上午11:05
//     */
//    @PostMapping("/list/{brandsId}/{productName}")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "brandsId", value = "品牌ID", type = "string", required = true),
//            @ApiImplicitParam(name = "productName", value = "产品型号", type = "string", required = false)
//    })
//    @ApiOperation("根据品牌ID查询产品列表")
//    public ResponseEntity<ApiResult<List<Product>>> list(@PathVariable("brandsId") String brandsId, @PathVariable("productName") String productName) {
//        //ServiceResult<List<Products>> res1 = productsService.listByBrandsId(brandsId, productName);
//        //修改从mysql获取数据
//        ServiceResult<List<Product>> res = productService.listByBrandsId(brandsId, productName);
//        return new ResponseEntity<>(ApiResult.ok(res.getResult(), res.getMessage()), HttpStatus.OK);
//    }
//}