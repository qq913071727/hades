package com.innovation.ic.sc.web.controller.sc_data;

import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc   初始化库存表数据到redis中
 * @author linuo
 * @time   2023年2月13日10:50:36
 */
@Api(value = "初始化库存表数据到redis中", tags = "InitInventoryController")
@RestController
@RequestMapping("/api/v2/inventory")
@Slf4j
public class InitInventoryController extends AbstractController {
    @ApiOperation(value = "将Inventory表的数据导入到redis")
    @GetMapping("initInventoryData")
    public ResponseEntity<ApiResult> initInventoryData() {
        inventoryService.initInventoryData();
        return new ResponseEntity<>(new ApiResult(HttpStatus.OK.value(), "初始化完成", Boolean.TRUE, Boolean.TRUE), HttpStatus.OK);
    }

    @ApiOperation(value = "将Inventory列表查询的结果数据导入到redis")
    @GetMapping("importQueryResultToRedis")
    public ResponseEntity<ApiResult> importQueryResultToRedis() {
        inventoryService.importQueryResultToRedis();
        return new ResponseEntity<>(new ApiResult(HttpStatus.OK.value(), "初始化完成", Boolean.TRUE, Boolean.TRUE), HttpStatus.OK);
    }
}