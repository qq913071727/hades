package com.innovation.ic.sc.web.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class UpdateBrandAdvantageStatusSchedule extends AbstractSchedule{

    @XxlJob("updateBrandAdvantageStatusJob")
    private void updateBrandAdvantageStatusJob() {
        advantageBrandService.updateBrandAdvantageStatus();
    }
}