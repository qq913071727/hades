package com.innovation.ic.sc.web.controller.sc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.sc.base.pojo.variable.ActionMessagePojo;
import com.innovation.ic.sc.base.pojo.variable.ApiResult;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName ActionMessageController
 * @Description 动作消息控制器
 * @Date 2022/10/8
 * @Author myq
 */
@Api(value = "消息提醒", tags = "ActionMessageController")
@RestController
@RequestMapping("api/v1/actionMessage")
@Slf4j
public class ActionMessageController extends AbstractController {

    /**
     * 消息提醒页面表格的分页显示
     * @param pageNo
     * @param pageSize
     * @param request
     * @return
     */
    @ApiOperation("消息提醒页面表格的分页显示")
    @GetMapping("page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20")
    })
    public ResponseEntity<ApiResult<PageInfo<ActionMessagePojo>>> page(int pageNo, int pageSize,
                                                                       HttpServletRequest request) {
        if (pageNo < 1 || pageSize < 1) {
            ApiResult apiResult = new ApiResult();
            String message = "调用接口【/api/v1/actionMessage/page时，参数pageNo和pageSize不合法";
            log.warn(message);
            apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

//        return new ResponseEntity<>(ApiResult.ok(actionMessageService.page(pageNo, pageSize, getUserId(request)).getResult(), "动作消息分页数据ok"), HttpStatus.OK);
        return new ResponseEntity<>(ApiResult.ok(actionMessageService.pageRedis(pageNo, pageSize, getUserId(request)).getResult(), "动作消息分页数据ok"), HttpStatus.OK);
    }


    @ApiOperation("全部已读")
    @GetMapping("isReadAll")
    public ResponseEntity<ApiResult> isReadAll(HttpServletRequest request) {

//        actionMessageService.isReadAll(getUserId(request));
        actionMessageService.isReadAllRedis(getUserId(request));
        return new ResponseEntity<>(ApiResult.ok("ok"), HttpStatus.OK);
    }


    @ApiOperation("未读消息总数")
    @GetMapping("unReadTotal")
    public ResponseEntity<ApiResult<Integer>> unReadTotal(HttpServletRequest request) {
//        ServiceResult<Integer> res = actionMessageService.unReadTotal(getUserId(request));
        ServiceResult<Integer> res = actionMessageService.unReadTotalRedis(getUserId(request));

        return new ResponseEntity<>(ApiResult.ok(res.getResult(), "ok"), HttpStatus.OK);
    }
}
