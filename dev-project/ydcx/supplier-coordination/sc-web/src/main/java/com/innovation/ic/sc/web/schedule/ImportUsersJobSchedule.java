package com.innovation.ic.sc.web.schedule;

import com.innovation.ic.sc.base.model.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author swq
 * @desc 定时更新mysql中的users表数据从sqlserver中的Users同步数据
 * @time 2022年8月24日16:23:25
 */
@Component
public class ImportUsersJobSchedule extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportUsersJobSchedule.class);

    @XxlJob("importUsersJob")
    private void importUsers() {
        try {
            // 获取sql server中users数据
            List<ParentUsersTopView> userList = usersService.selectList();

            if (userList.size() == 0 || userList.equals(null)) {
                log.info("定时更新mysql中的users表数据从sqlserver中的Users同步数据任务执行出现问题,原因sqlserver数据为空");
                return;
            }
            //先清空 MySQL中users主账号
            Map<String, Object> map = new HashMap<>();
            map.put("father_id", null);
            userService.batchDelete(map);
            //把sqlservice中数据同步到MySQL中user表中
            userService.insertAllNew(userList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("定时更新mysql中的users表数据从sqlserver中的Users同步数据任务执行出现问题,原因:", e);
        }
    }

    @XxlJob("testRedis")
    private void testRedis() {
        try {
            // 获取sql server中users数据
            List<ParentUsersTopView> parentUsersTopViewList = usersService.selectList();

            if (parentUsersTopViewList.size() == 0 || parentUsersTopViewList.equals(null)) {
                log.info("定时更新mysql中的users表数据从sqlserver中的Users同步数据任务执行出现问题,原因sqlserver数据为空");
                return;
            }
            //先清空 MySQL中users主账号
            Map<String, Object> map = new HashMap<>();
            map.put("father_id", null);
            userService.batchDelete(map);
            //把sql server中数据同步到MySQL中user表中
            userService.insertAll(parentUsersTopViewList);

            // 查找user表中所有记录
            ServiceResult<List<User>> serviceResult = userService.findAll();
            if (null != serviceResult && null != serviceResult.getResult()){
                String keyAsc = RedisStorage.TABLE + RedisStorage.USER +
                        RedisStorage.CREATE_DATE_ASC;
                String keyDesc = RedisStorage.TABLE + RedisStorage.USER +
                        RedisStorage.CREATE_DATE_DESC;

                // 删除旧的数据
                serviceHelper.getRedisManager().del(keyAsc);
                serviceHelper.getRedisManager().del(keyDesc);

                // 向redis中导入数据
                List<User> userList = serviceResult.getResult();
                for (int i = 0; i < userList.size(); i++) {
                    User user = userList.get(i);
                    String value = modelHandler.toXmlStorageString(user);
                    serviceHelper.getRedisManager().zAdd(keyAsc, value, user.getCreateDate().getTime());
                    serviceHelper.getRedisManager().zAdd(keyDesc, value, -user.getCreateDate().getTime());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}