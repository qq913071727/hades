echo 将my_company表的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/myCompany/initGetDetail -k

echo 删除redis中CLIENT:开头的数据,将client表中的数据导入redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/initClient/importClientIntoRedis -k

#echo 将advantage_model表的数据导入到redis
#curl https://out.bj.51db.com:36201/sc-web/api/v2/advantageModel/initAdvantageModelData -k

#echo 将inventory表的数据导入到redis
#curl https://out.bj.51db.com:36201/sc-web/api/v2/inventory/initInventoryData -k

#echo 将inventory列表查询的结果数据导入到redis
#curl https://out.bj.51db.com:36201/sc-web/api/v2/inventory/importQueryResultToRedis -k

echo 将接口/api/v1/actionMessage/page需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/actionMessage/initPage -k

echo 将接口/api/v1/actionMessage/unReadTotal需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/actionMessage/initUnReadTotal -k

echo 将接口/api/v1/department/initDepartmentData需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/department/initDepartmentData -k

echo 将接口/api/v1/userManagement/initUserManagementData需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/userManagement/initUserManagementData -k

echo 将接口/api/v1/check/initCheckData需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/check/initCheckData -k

echo 将接口/api/v1/user/initUserData需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/user/initUserData -k

echo 将接口/api/v1/role/initRoleData需要的数据导入到redis
curl https://out.bj.51db.com:36201/sc-web/api/v2/role/initRoleData -k

#echo 将personnel_management表的数据导入到redis
#curl https://out.bj.51db.com:36201/sc-web/api/v2/personnelManagement/initPersonnelManagementData -k

#echo 将brand表的数据导入到redis,目前不用此表数据不放redis
#curl https://out.bj.51db.com:36201/sc-web/api/v2/brand/initBrandToRedis -k