package com.innovation.ic.sc.base.service.sc.impl;

import cn.hutool.core.lang.Validator;
import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.b1b.framework.util.HanziToPinyinUtils;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.framework.util.XmlUtil;
import com.innovation.ic.sc.base.handler.sc.AuthenticationUserHandler;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.mapper.sc.UserMapper;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.UsersTopView;
import com.innovation.ic.sc.base.model.sc.Department;
import com.innovation.ic.sc.base.model.sc.MapUserRole;
import com.innovation.ic.sc.base.model.sc.Role;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.pojo.constant.*;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.constant.model.DefaultUserConstants;
import com.innovation.ic.sc.base.pojo.constant.model.RoleAvailableStatus;
import com.innovation.ic.sc.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.MyCompanyService;
import com.innovation.ic.sc.base.service.sc.UserService;
import com.innovation.ic.sc.base.value.ErpInterfaceAddressConfig;
import com.innovation.ic.sc.base.value.LoginConfig;
import com.innovation.ic.sc.base.value.UserConfig;
import com.innovation.ic.sc.base.vo.ForgetPasswordVo;
import com.innovation.ic.sc.base.vo.UpdatePasswordVo;
import com.innovation.ic.sc.base.vo.UsersVo;
import com.innovation.ic.sc.base.vo.VerificationCodeVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.bson.types.ObjectId;
import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    private static final Logger log = LoggerFactory.getLogger(ServiceImpl.class);

    @Resource
    private ServiceHelper serviceHelper;

    @Resource
    private MyCompanyService myCompanyService;

    /**
     * 获取所有主账号数据
     *
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<User>> getAllPrimaryAccountData() {
        List<User> list = serviceHelper.getUserMapper().getAllPrimaryAccountData();

        ServiceResult<List<User>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(list);
        return serviceResult;
    }

    @Override
    public List<String> findByEnterpriseId(String enterpriseID) {
        return this.baseMapper.findByEnterpriseId(enterpriseID);
    }

    /**
     * 更新账号评分
     *
     * @param id    主账号id
     * @param score 评分
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> updateScore(String id, BigDecimal score) {
        Boolean result = Boolean.FALSE;
        String message = ServiceResult.UPDATE_FAIL;

        int i = serviceHelper.getUserMapper().updateScore(id, score);
        if (i > 0) {
            result = Boolean.TRUE;
            message = ServiceResult.UPDATE_SUCCESS;
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(result);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 根据fatherId 查询主账号或关联的子账号
     *
     * @param fatherId
     * @return
     */
    @Override
    public List<User> findUserAllByFatherId(String fatherId) {

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isEmpty(fatherId)) {
            queryWrapper.isNull(User::getFatherId);
        } else {
            queryWrapper.eq(User::getFatherId, fatherId);
        }
        queryWrapper.eq(User::getStatus, 200);
        return baseMapper.selectList(queryWrapper);
    }

    /**
     * 判断当前密码数据是否重复
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> findPassWord(UsersVo usersVo) throws IOException {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        String loginData = HttpUtils.doPostTestTwo(serviceHelper.getLoginConfig().getUrl(), JSON.toJSONString(usersVo));
        JSONObject loginDatas = JSONObject.parseObject(loginData);
        Object status = loginDatas.get(Constants.STATUS_);
        if (status.equals(HttpStatus.HTTP_OK)) {
            serviceResult.setMessage("密码正确");
            serviceResult.setSuccess(Boolean.TRUE);
            return serviceResult;
        } else {
            log.warn("密码有误，请重新输入");
            serviceResult.setMessage("密码有误，请重新输入");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
    }

    /**
     * 判断手机号是否不正确
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> findAccountPhone(UsersVo usersVo) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        //从缓存中获取数据
        String redisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + usersVo.getPhone();
        Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
        if (redisDepartment == null) {//redis中不存在反查mysql
            QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
            userQueryWrapper.eq("phone", usersVo.getPhone());
            User user = serviceHelper.getUserMapper().selectOne(userQueryWrapper);
            if (user == null) {//当前数据并不存在存 缓存为空字符串
                String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + usersVo.getPhone();
                serviceHelper.getRedisManager().set(userRedisKey, RedisStorage.EMPTY_VALUE);
                log.warn("手机号不存在，请重新输入");
                serviceResult.setMessage("手机号不存在，请重新输入");
                serviceResult.setSuccess(Boolean.FALSE);
                return serviceResult;
            }
            //当前数据存入缓存中
            String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + user.getPhone();
            String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
            serviceHelper.getRedisManager().set(userRedisKey, apiResultJson);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setMessage("手机号已经存在");
            return serviceResult;
        }
        if (redisDepartment.equals(RedisStorage.EMPTY_VALUE)) {//为空字符串或根本不存在redis中数据
            log.warn("手机号不存在，请重新输入");
            serviceResult.setMessage("手机号不存在，请重新输入");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        serviceResult.setMessage("手机号已经存在");
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 通过主键获取用户详情
     *
     * @param id
     * @return
     */
    @Override
    public ServiceResult<AuthenticationUser> queryUsers(String id) {
        ServiceResult<AuthenticationUser> serviceResult = new ServiceResult<>();
        //从缓存中获取数据
        String redisKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + id;
        Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
        if (redisDepartment == null) {
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id", id);
            User user = serviceHelper.getUserMapper().selectOne(queryWrapper);
            if (user == null) {//当前数据并不存在存 缓存为空字符串
                String userRedisKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + id;
                serviceHelper.getRedisManager().set(userRedisKey, RedisStorage.EMPTY_VALUE);
                log.warn("用户不存在");
                serviceResult.setMessage("用户不存在");
                serviceResult.setSuccess(Boolean.FALSE);
                return serviceResult;
            }
            //mysql存在当前数据存入缓存中
            String redisUserIdKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + user.getId();
            String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
            serviceHelper.getRedisManager().set(redisUserIdKey, apiResultJson);
            //封装用户详情
            AuthenticationUser authenticationUser = this.encapsulationUser(user);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(authenticationUser);
            return serviceResult;
        }
        if (redisDepartment.equals(RedisStorage.EMPTY_VALUE)) {//为空字符串或根本不存在redis中数据
            log.warn("用户不存在");
            serviceResult.setMessage("用户不存在");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        User user = JSONObject.parseObject((String) redisDepartment, User.class);//反序列化数据
        //封装用户详情
        AuthenticationUser authenticationUser = this.encapsulationUser(user);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(authenticationUser);
        return serviceResult;
    }

    /**
     * 判断当前数据是否重复
     *
     * @param userData
     * @param key
     * @return
     */
    @Override
    public ServiceResult<Boolean> findData(String userData, String key) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        if (key.equals(UserType.USER_TYPE)) {//如果是账号数据
            //从缓存中获取数据
            String redisKey = RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode() + "_" + userData;
            Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
            if (redisDepartment == null) {//redis中不存在反查mysql
                QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
                userQueryWrapper.eq("user_name", userData);
                User user = serviceHelper.getUserMapper().selectOne(userQueryWrapper);//获取到当前用户信息
                if (user == null) {//当前数据并不存在存 缓存为空字符串
                    String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode() + "_" + userData;
                    serviceHelper.getRedisManager().set(userRedisKey, RedisStorage.EMPTY_VALUE);
                    serviceResult.setSuccess(Boolean.TRUE);
                    return serviceResult;
                }
                //当前数据存入缓存中
                String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode() + "_" + user.getUserName();
                String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(userRedisKey, apiResultJson);
                log.warn("账号已存在，请重新输入");
                serviceResult.setMessage("账号已存在，请重新输入");
                serviceResult.setSuccess(Boolean.FALSE);
                return serviceResult;
            }
            if (redisDepartment.equals(RedisStorage.EMPTY_VALUE)) {//为空字符串或根本不存在redis中数据
                serviceResult.setSuccess(Boolean.TRUE);
                return serviceResult;
            }
            log.warn("账号已存在，请重新输入");
            serviceResult.setMessage("账号已存在，请重新输入");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        } else if (key.equals(UserType.PHONE_TYPE)) {//如果是手机号数据
            //从缓存中获取数据
            String redisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + userData;
            Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
            if (redisDepartment == null) {//redis中不存在反查mysql
                QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
                userQueryWrapper.eq("phone", userData);
                User user = serviceHelper.getUserMapper().selectOne(userQueryWrapper);
                if (user == null) {//当前数据并不存在存 缓存为空字符串
                    String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + userData;
                    serviceHelper.getRedisManager().set(userRedisKey, RedisStorage.EMPTY_VALUE);
                    serviceResult.setSuccess(Boolean.TRUE);
                    return serviceResult;
                }
                //当前数据存入缓存中
                String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + user.getPhone();
                String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(userRedisKey, apiResultJson);
                log.warn("手机号已被绑定，请重新输入");
                serviceResult.setMessage("手机号已被绑定，请重新输入");
                serviceResult.setSuccess(Boolean.FALSE);
                return serviceResult;
            }
            if (redisDepartment.equals(RedisStorage.EMPTY_VALUE)) {//为空字符串或根本不存在redis中数据
                serviceResult.setSuccess(Boolean.TRUE);
                return serviceResult;
            }
            log.warn("手机号已被绑定，请重新输入");
            serviceResult.setMessage("手机号已被绑定，请重新输入");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        } else {//如果是邮箱数据
            //从缓存中获取数据
            String redisKey = RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode() + "_" + userData;
            Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
            if (redisDepartment == null) {//redis中不存在反查mysql
                QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
                userQueryWrapper.eq("email", userData);
                User user = serviceHelper.getUserMapper().selectOne(userQueryWrapper);
                if (user == null) {//当前数据并不存在存 缓存为空字符串
                    String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode() + "_" + userData;
                    serviceHelper.getRedisManager().set(userRedisKey, RedisStorage.EMPTY_VALUE);
                    serviceResult.setSuccess(Boolean.TRUE);
                    return serviceResult;
                }
                //当前数据存入缓存中
                String userRedisKey = RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode() + "_" + user.getEmail();
                String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(userRedisKey, apiResultJson);
                log.warn("邮箱已存在，请重新输入");
                serviceResult.setMessage("邮箱已存在，请重新输入");
                serviceResult.setSuccess(Boolean.FALSE);
                return serviceResult;
            }
            if (redisDepartment.equals(RedisStorage.EMPTY_VALUE)) {//为空字符串或根本不存在redis中数据
                serviceResult.setSuccess(Boolean.TRUE);
                return serviceResult;
            }
            log.warn("邮箱已存在，请重新输入");
            serviceResult.setMessage("邮箱已存在，请重新输入");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
    }

    /**
     * 查找所有User对象
     *
     * @return
     */
    @Override
    public ServiceResult<List<User>> findAll() {
        ServiceResult<List<User>> serviceResult = new ServiceResult<List<User>>();

        Wrapper<User> userWrapper = new QueryWrapper<>();
        List<User> userList = baseMapper.selectList(userWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userList);
        return serviceResult;
    }

    /**
     * 新增账号
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<String> saveUser(UsersVo usersVo, String id) throws IOException, ParseException {
        ServiceResult<String> serviceResult = new ServiceResult<>();
        SubUsersPojo userData = new SubUsersPojo();
        userData.setFatherID(id);
        userData.setWebSite(DefaultUserConstants.DEFAULT_WEBSITE);
        userData.setType(DefaultUserConstants.DEFAULT_TYPE);
        userData.setUserName(usersVo.getUserName());
        userData.setPassword(DefaultUserConstants.DEFAULT_PASSWORD);
        userData.setRealName(usersVo.getRealName());
        userData.setPhone(usersVo.getPhone());
        userData.setPosition(usersVo.getPosition().toString());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time = new Date();
        userData.setCreateDate(sdf.format(time));
        userData.setStatus(DefaultUserConstants.DEFAULT_STATUS);
        userData.setEmail(usersVo.getEmail());
        String erpUserData = HttpUtils.sendPostJson(serviceHelper.getUserConfig().getEnterSonUserUrl(), userData);
        Object parse = JSONObject.parse(erpUserData);
        Map entity = (Map) parse;
        String status = entity.get("status").toString().toString();
        if (status.equals(CodeConstants.FASE_CODE)) {//erp 状态码 为500
            String msg = entity.get("msg").toString().toString();//获取erp提示的错误信息
            serviceResult.setMessage(msg);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        String data = entity.get("data").toString();
        //转换数据
        SubUsersPojo subUsersPojo = JSON.parseObject(data, SubUsersPojo.class);
        this.bindUserData(subUsersPojo);//进行添加数据
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(subUsersPojo.getId());
        return serviceResult;
    }

    /**
     * 绑定参数关联传入mysql中
     *
     * @param subUsersPojo
     * @return
     */
    private void bindUserData(SubUsersPojo subUsersPojo) throws ParseException {
        Date date = new Date();
        User user = new User();
        user.setId(subUsersPojo.getId());//主键
        user.setFatherId(subUsersPojo.getFatherID());//主账号的id，如果为空，则表示这是主账号
        user.setWebSite(subUsersPojo.getWebSite());//站点 1 供应商协同系统、2 芯达通系统
        user.setType(subUsersPojo.getType());//类型（1 卖家[供应商]） Seller Buyer
        user.setUserName(subUsersPojo.getUserName());//登入账户
        user.setPassword(subUsersPojo.getPassword());//密码
        user.setRealName(subUsersPojo.getRealName());//名称
        user.setPinYin(HanziToPinyinUtils.getPinyin(subUsersPojo.getRealName()));//名称拼音
        user.setPhone(subUsersPojo.getPhone());//手机号
        user.setPosition(Integer.valueOf(subUsersPojo.getPosition()));//职位
        String createDate = subUsersPojo.getCreateDate();
        date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(createDate);
        user.setCreateDate(date);//创建时间
        user.setStatus(subUsersPojo.getStatus());//状态
        user.setEmail(subUsersPojo.getEmail());//邮箱
        serviceHelper.getUserMapper().insert(user);
    }

    /**
     * 当前主账号下分页展示子账号信息
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<List<User>> pageUser(UsersVo usersVo) {
        ServiceResult<List<User>> serviceResult = new ServiceResult<List<User>>();
        Integer pageSize = usersVo.getPageSize();
        Integer pageNo = usersVo.getPageNo() - 1;
        User user = serviceHelper.getUserMapper().selectById(usersVo.getId());
        if (StringUtils.isEmpty(user.getFatherId())) {
            List<User> sonUserPageList = serviceHelper.getUserMapper().findSonUserPage(pageSize, pageNo, usersVo.getUserName(), usersVo.getRealName(), usersVo.getPosition(), usersVo.getPhone(), usersVo.getStatus(), usersVo.getId());
            //进行降序展示
            sonUserPageList.sort(Comparator.comparing(User::getCreateDate).reversed());
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(sonUserPageList);
            return serviceResult;
        } else {
            List<User> sonUserPageList = serviceHelper.getUserMapper().findSonUserPage(pageSize, pageNo, usersVo.getUserName(), usersVo.getRealName(), usersVo.getPosition(), usersVo.getPhone(), usersVo.getStatus(), user.getFatherId());
            //进行降序展示
            sonUserPageList.sort(Comparator.comparing(User::getCreateDate).reversed());
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(sonUserPageList);
            return serviceResult;
        }
    }

    /**
     * 当前主账号下分页展示子账号信息
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<PageNationPojo<UserPagePojo>> pageUserNew(UsersVo usersVo) {
        com.github.pagehelper.Page<Object> page = PageHelper.startPage(usersVo.getPageNo(), usersVo.getPageSize(), "create_date desc");
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        //查询 角色
        if (StringUtils.isNotEmpty(usersVo.getRoleId())) {
            Set<String> userRoleIds = getUserRoleId(usersVo.getRoleId());
            if (userRoleIds != null && userRoleIds.size() > 0) {
                queryWrapper.in(User::getId, userRoleIds);
            } else {
                return ServiceResult.ok(PageNationPojo.fromPage(page, null), "查询用户列表数据成功");
            }
        }
        //登入账户
        if (StringUtils.isNotEmpty(usersVo.getUserName())) {
            queryWrapper.like(User::getUserName, usersVo.getUserName());
        }
        //名称
        if (StringUtils.isNotEmpty(usersVo.getRealName())) {
            //如果传递参数为拼音
            if (usersVo.getRealName().matches(ParameterType.pinYin)) {
                queryWrapper.like(User::getPinYin, usersVo.getRealName());
            } else {
                queryWrapper.like(User::getRealName, usersVo.getRealName());
            }
        }
        //职位：1表示经理，2表示员工
        if (usersVo.getPosition() != null) {
            queryWrapper.eq(User::getPosition, usersVo.getPosition());
        }
        //手机号
        if (StringUtils.isNotEmpty(usersVo.getPhone())) {
            queryWrapper.like(User::getPhone, usersVo.getPhone());
        }
        //账号状态
        if (usersVo.getStatus() != null) {
            queryWrapper.eq(User::getStatus, usersVo.getStatus());
        }
        //子账号也需要展示当前所以员工 排除自身
        if (StringUtils.isNotEmpty(usersVo.getFatherId())) {
            queryWrapper.eq(User::getFatherId, usersVo.getFatherId());
        } else {
            //查询子账号
            queryWrapper.eq(User::getFatherId, usersVo.getId());
        }
        List<User> list = baseMapper.selectList(queryWrapper);
        List<UserPagePojo> userPagePojoList = list.stream().map(user -> {
            UserPagePojo userPagePojo = new UserPagePojo();
            BeanUtils.copyProperties(user, userPagePojo);
            userPagePojo.setRoleNames(getRoleNames(user.getId()));
            return userPagePojo;
        }).collect(Collectors.toList());
        return ServiceResult.ok(PageNationPojo.fromPage(page, userPagePojoList), "查询用户列表数据成功");
    }

    /**
     * 获取用户和角色关系
     *
     * @param roleId
     * @param
     * @return
     */
    private Set<String> getUserRoleId(String roleId) {
        Query query = new Query(Criteria.where("roleId").is(roleId));
        List<MapUserRole> mapUserRoleList = serviceHelper.getMongodbManager().find(query, MapUserRole.class);
        Set<String> list = new HashSet<>();
        for (MapUserRole mapUserRole : mapUserRoleList) {
            list.add(mapUserRole.getUserId());
        }
        return list;
    }

    /**
     * 获取用户和角色关系的角色名称
     *
     * @param userId
     * @param
     * @return
     */
    private String getRoleNames(String userId) {
        Query query = new Query(Criteria.where("userId").is(userId));
        List<MapUserRole> mapUserRoleList = serviceHelper.getMongodbManager().find(query, MapUserRole.class);
        Set<String> list = new HashSet<>(mapUserRoleList.size());
        for (MapUserRole mapUserRole : mapUserRoleList) {
            list.add(mapUserRole.getRoleId());
        }
//        query = new Query(Criteria.where("_id").in(list));
//        List<Role> roleList = serviceHelper.getMongodbManager().find(query, Role.class);
        StringBuffer buffer = new StringBuffer();
        List<Role> roleList = this.selectRedisData(list);
        for (Role role : roleList) {
            //用户列表不显示分配停用的角色
            if (role.getAvailable().equals(RoleAvailableStatus.YES)){
                buffer.append(role.getName()).append(",");
            }
        }
        if (StringUtils.isEmpty(buffer.toString())) {
            return "";
        }
        return buffer.toString().substring(0, buffer.toString().length() - 1);
    }

    /**
     * 分页展示用户信息
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<List<User>> findUserPage(UsersVo usersVo) {
        ServiceResult<List<User>> serviceResult = new ServiceResult<List<User>>();
        Integer pageSize = usersVo.getPageSize();
        Integer pageNo = usersVo.getPageNo() - 1;
        List<User> userList = serviceHelper.getUserMapper().findUserPage(pageSize, pageNo, usersVo.getUserName(), usersVo.getRealName(), usersVo.getPosition(), usersVo.getPhone(), usersVo.getStatus());
        //进行降序展示
        userList.sort(Comparator.comparing(User::getCreateDate).reversed());
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userList);
        return serviceResult;
    }

    /**
     * 通过feign 传递账号密码 去第三方获取 token
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<AuthenticationUser> findUser(UsersVo usersVo) throws IOException {
        String userName = usersVo.getUserName();

        // 判断输入的用户名是否为手机号
        boolean judgeResult = Validator.isMobile(userName);
        if (judgeResult) {
            // 根据手机号查询账号
            List<String> list = serviceHelper.getUserMapper().selectAccountByPhoneNumber(userName);
            if (list != null && list.size() == 1) {
                usersVo.setUserName(list.get(0));
            }
        }

        String token = null;
        ServiceResult<AuthenticationUser> serviceResult = new ServiceResult<>();
        //通过feign 传递账号密码 去第三方获取 token
        //获取token
        //因当前系统为供应商所以给默认值
        usersVo.setWebSite(Constants.WEB_SITE_TYPE);//默认给供应商系统
        String loginData = HttpUtils.doPostTestTwo(serviceHelper.getLoginConfig().getUrl(), JSON.toJSONString(usersVo));
        log.info("调用接口【{}】的返回值为：{}", serviceHelper.getLoginConfig().getUrl(), loginData);
        JSONObject loginDatas = JSONObject.parseObject(loginData);
        Object status = loginDatas.get(Constants.STATUS_);
        if (status.equals(HttpStatus.HTTP_OK)) {
            log.info("通过erp接口获取token成功");
            AuthenticationUser authenticationUser = new AuthenticationUser();
            JSONObject data = (JSONObject) loginDatas.get(LoginConstants.DATA);
            if (data != null) {
                token = (String) data.get(LoginConstants.TOKEN);

                // 将token对象转换为接收用户信息vo类对象
                authenticationUser = AuthenticationUserHandler.toAuthenticationUser(token);

                // 我的公司名称列表
                ServiceResult<List<String>> myCompanyNameListServiceResult = myCompanyService.findByEpId(authenticationUser.getEpId());
                if (null != myCompanyNameListServiceResult && null != myCompanyNameListServiceResult.getResult()) {
                    authenticationUser.setMyCompanyNameList(myCompanyNameListServiceResult.getResult());
                }

                // 查询账号评分
                Float score = serviceHelper.getUserMapper().getScoreById(authenticationUser.getId());
                if (score == null) {
                    score = 0F;
                }
                authenticationUser.setScore(score);
            }
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(authenticationUser);
            return serviceResult;
        } else {
            log.warn("通过erp接口获取token失败,erp返回内容:[{}]", loginData);
            String msg = (String) loginDatas.get(Constants.MSG_);//erp实时返回错误消息体
            serviceResult.setMessage(msg);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
    }

    @Override
    public ServiceResult<IPage<UsersPojo>> findSonUserPage(String userId, int current, int size) {
        ServiceResult<IPage<UsersPojo>> serviceResult = new ServiceResult<>();
        LambdaQueryWrapper<User> userLambdaQueryWrapper = Wrappers.lambdaQuery();
        userLambdaQueryWrapper.eq(User::getFatherId, userId);
        userLambdaQueryWrapper.orderByDesc(User::getCreateDate);
        Page<User> userPage = new Page<>(current, size);
        IPage userIPage = serviceHelper.getUserMapper().selectPage(userPage, userLambdaQueryWrapper);
        List<UsersPojo> resultList = new ArrayList<>();
        List<User> records = userIPage.getRecords();
        for (User user : records) {
            UsersPojo usersPojo = new UsersPojo();
            usersPojo.setId(user.getId());
            usersPojo.setUserName(user.getUserName());
            usersPojo.setRealName(user.getRealName());
            usersPojo.setPosition(user.getPosition());
            usersPojo.setStatus(user.getStatus());
            resultList.add(usersPojo);
        }
        userIPage.setRecords(resultList);
        serviceResult.setResult(userIPage);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServiceResult<List<User>> page(String userId, int pageSize, int pageNo, String userName,
                                          String realName, Integer position, String phone, Integer status) {
        ServiceResult<List<User>> serviceResult = new ServiceResult<>();

        String field = RedisStorage.TABLE + RedisStorage.USER +
                RedisStorage.CREATE_DATE_ASC;

        User user = new User();
        user.setUserName(userName);
        user.setRealName(realName);
        user.setPosition(position);
        user.setPhone(phone);
        user.setStatus(status);
        String pattern = serviceHelper.getModelHandler().toXmlFilterString(user);
        List list = serviceHelper.getRedisManager().page(field, pattern);

        List subList = list.subList(pageSize * (pageNo - 1), pageSize * pageNo);
        List<User> userList = new ArrayList<>();
        if (null != subList && subList.size() > 0) {
            for (int i = 0; i < subList.size(); i++) {
                String xmlString = (String) subList.get(i);
                String jsonString = XmlUtil.xmlStringToJSON(xmlString).toString();
                userList.add(JSON.parseObject(jsonString, User.class));
            }
        }

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(userList);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 账号状态更改
     *
     * @param usersVo
     * @return
     */
    @Override
    public void updateStatus(UsersVo usersVo) {
        try {
            List<String> redisKeyList = new ArrayList<>();//存储redis缓存中key 用于清空缓存
            UsersIdsPojo usersIdsPojo = new UsersIdsPojo();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            String time = df.format(new Date());// new Date()为获取当前系统时间
//        Date date = df.parse(Time);
            String id = usersVo.getId();
            String[] ids = id.split(",");
            for (String userId : ids) {//清空所有缓存数据
                String redisUserNameKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + userId;
                redisKeyList.add(redisUserNameKey);
            }
            serviceHelper.getRedisManager().del(redisKeyList);//redis缓存清空
            //进行修改
            serviceHelper.getUserMapper().updateStatus(time, usersVo.getStatus(), ids);
//            //当前修改的数据通过mq传递erp
//            List<User> statusData = userMapper.selectIds(ids);
            usersIdsPojo.setIds(Arrays.asList(ids));//mq发送账号主键
            usersIdsPojo.setStatus(usersVo.getStatus());//mq发送账号状态
            usersIdsPojo.setWebSite(DefaultUserConstants.DEFAULT_WEBSITE);
            usersIdsPojo.setType(DefaultUserConstants.DEFAULT_TYPE);
            JSONObject json = (JSONObject) JSONObject.toJSON(usersIdsPojo);
//            String s = JSONObject.toJSONString(usersIdsPojo);
//            JSONArray json = JSONObject.parseArray(s);
//            JSONObject json = (JSONObject) JSONObject.toJSON(statusData);
//            JSONObject object = new JSONObject();
//            object.put("list",json);
            // 包装发送到rabbitmq的消息内容
            JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_ENABLE_SUBUSERS);
            log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_ENABLE_SUBUSERS, json.toString());
            serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_QUEUE_NAME, RabbitMqConstants.SC_ERP_ENABLE_SUBUSERS, null, result.toString().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改密码
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> updatePassword(UsersVo usersVo, AuthenticationUser authenticationUser) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        //用于清空缓存
        String redisUserNameKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + authenticationUser.getId();
        serviceHelper.getRedisManager().del(redisUserNameKey);//redis缓存清空
        UpdatePasswordVo updatePasswordVo = new UpdatePasswordVo();
        updatePasswordVo.setOldPassword(usersVo.getPassword());
        updatePasswordVo.setNewPassword(usersVo.getNewPassword());
        //向erp发送旧密码、新密码
        String data = HttpUtils.sendPostJson(serviceHelper.getLoginConfig().getUpdatePasswordUrl(), updatePasswordVo, usersVo.getToken());
        JSONObject jsonObject = JSON.parseObject(data);
        Boolean success = jsonObject.getBoolean("success");
        if (success.equals(Boolean.FALSE)) {
            serviceResult.setMessage(jsonObject.getString("msg"));
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 重置密码
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> resetPassword(UsersVo usersVo) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        //清空 主键查询账号详情缓存
        String redisUserNameKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + usersVo.getId();
        serviceHelper.getRedisManager().del(redisUserNameKey);//redis缓存清空
        String passwordData = HttpUtils.erpSendPost(serviceHelper.getLoginConfig().getResetPassword(), usersVo.getId(), usersVo.getToken());
        if (!StringUtils.isEmpty(passwordData)) {
            serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            return serviceResult;
        } else {
            serviceResult.setMessage(ServiceResult.UPDATE_FAIL);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
    }

    /**
     * 修改手机号
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Map> updatePhone(UsersVo usersVo) throws IOException {
        ServiceResult<Map> serviceResult = new ServiceResult<>();
        UpdateWrapper<User> usersUpdateWrapper = new UpdateWrapper<>();
        usersUpdateWrapper.eq("user_name", usersVo.getUserName());
        User userData = serviceHelper.getUserMapper().selectOne(usersUpdateWrapper);
        ArrayList<String> redisKeyList = new ArrayList<>();//存储所有key用于清空redis缓存
        String redisPhoneNewKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + usersVo.getPhone();//因为controller 先验证了传递手机号是否存在 造成了redis垃圾数据 需要进行清空
        redisKeyList.add(redisPhoneNewKey);
        //清空缓存
        String redisPhoneOldKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + userData.getPhone();//清空历史redis缓存数据
        redisKeyList.add(redisPhoneOldKey);
        String redisUserIdKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + userData.getId();//通过用户id获取账号详情
        redisKeyList.add(redisUserIdKey);
        serviceHelper.getRedisManager().del(redisKeyList);

        User user = new User();
        user.setPhone(usersVo.getPhone());
        user.setModifyDate(new Date());
        serviceHelper.getUserMapper().update(user, usersUpdateWrapper);
        User usermq = serviceHelper.getUserMapper().selectOne(usersUpdateWrapper);//修改后的数据
        User mqUser = new User();//mq推送消息体(因为不需要多余数据所以封装数据)
        mqUser.setId(usermq.getId());
        mqUser.setPhone(usermq.getPhone());
        mqUser.setModifyDate(usermq.getModifyDate());
        mqUser.setWebSite(DefaultUserConstants.DEFAULT_WEBSITE);
        mqUser.setType(DefaultUserConstants.DEFAULT_TYPE);
        //通过mq向erp中传递消息
        JSONObject json = (JSONObject) JSONObject.toJSON(mqUser);
        // 包装发送到rabbitmq的消息内容
        JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_UPDATE_PHONE);
        log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_UPDATE_PHONE, json.toString());
        serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_QUEUE_NAME, RabbitMqConstants.SC_ERP_UPDATE_PHONE, null, result.toString().getBytes());
        HashMap<String, String> map = new HashMap<>();
        map.put("phone", usermq.getPhone());
        serviceResult.setResult(map);
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 修改账号信息
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> updateUser(UsersVo usersVo) throws IOException {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        UpdateWrapper<User> usersUpdateWrapper = new UpdateWrapper<>();
        usersUpdateWrapper.eq("id", usersVo.getId());
        //获取库中数据
        User userData = baseMapper.selectOne(usersUpdateWrapper);//获取到当前用户数据 进行所有缓存清空
        List<String> redisUserList = this.deleteRedisUser(userData);//返回所有需要清空缓存的key
        serviceHelper.getRedisManager().del(redisUserList);
        User user = new User();
        user.setPhone(usersVo.getPhone());
        user.setEmail(usersVo.getEmail());
        user.setPosition(usersVo.getPosition());
        user.setModifyDate(new Date());
        serviceHelper.getUserMapper().update(user, usersUpdateWrapper);
        User usermq = serviceHelper.getUserMapper().selectOne(usersUpdateWrapper);
        //通过mq向erp中传递消息
        JSONObject json = (JSONObject) JSONObject.toJSON(usermq);
        log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_ADD_SUBUSERS, json.toString());
        // 包装发送到rabbitmq的消息内容
        JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_ADD_SUBUSERS);
        serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_QUEUE_NAME, RabbitMqConstants.SC_ERP_ADD_SUBUSERS, null, result.toString().getBytes());
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServiceResult<User> findByUserId(String userId) {
        ServiceResult<User> serviceResult = new ServiceResult<User>();
        User user = getById(userId);
        serviceResult.setResult(user);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 先清空 MySQL中users主账号
     *
     * @param map
     * @return
     */
    @Override
    public void batchDelete(Map<String, Object> map) {
        serviceHelper.getUserMapper().deleteByMap(map);
    }

    /**
     * 把sql server中数据同步到MySQL中user表中
     *
     * @param userList
     * @return
     */
    @Override
    public void insertAll(List<ParentUsersTopView> userList) {
        serviceHelper.getUserMapper().insertUserList(userList);
    }

    /**
     * 把sql server中数据同步到MySQL中user表中
     *
     * @param userList
     */
    @Override
    public void insertAllNew(List<ParentUsersTopView> userList) {
        List<User> users = userList.stream().map(parentUsersTopView -> {
            User user = new User();
            BeanUtils.copyProperties(parentUsersTopView, user);
            user.setPinYin(HanziToPinyinUtils.getPinyin(parentUsersTopView.getRealName()));
            if (StringUtils.isNotEmpty(parentUsersTopView.getPosition())) {
                user.setPosition(Integer.valueOf(parentUsersTopView.getPosition()));
            }
            return user;
        }).collect(Collectors.toList());
        Integer integer = serviceHelper.getUserMapper().insertBatchSomeColumn(users);
        System.out.println("插入user:" + integer + "条数据");
    }

    /**
     * 监听主账号新增/修改
     *
     * @param usersTopView sqlservice中相关数据
     */
    @Override
    public void handleModelAddUsersMqMsg(UsersTopView usersTopView) {
        //去mysql中查询一下当前数据是否存在(存在修改/不存在添加)
        User user = findExistence(usersTopView);
        if (user == null) {
            User userdata = new User();
            userdata.setId(usersTopView.getId());
//            userdata.setUserId(usersTopView.getId());
            userdata.setWebSite(usersTopView.getWebSite());
            userdata.setFatherId(usersTopView.getFatherId());
            userdata.setEnterpriseId(usersTopView.getEnterpriseId());
            userdata.setEnterpriseName(usersTopView.getEnterpriseName());
            userdata.setType(usersTopView.getType());
            userdata.setUserName(usersTopView.getUserName());
            userdata.setPassword(usersTopView.getPassword());
            userdata.setRealName(usersTopView.getRealName());
            userdata.setPinYin(HanziToPinyinUtils.getPinyin(usersTopView.getRealName()));//中文转拼音
            userdata.setPhone(usersTopView.getPhone());
            userdata.setPosition(usersTopView.getPosition());
            userdata.setEmail(usersTopView.getEmail());
            userdata.setCreateDate(usersTopView.getCreateDate());
            userdata.setStatus(usersTopView.getStatus());
            userdata.setModifyDate(usersTopView.getModifyDate());
            //添加我的公司关联
            myCompanyService.addUserRelation(userdata);
            serviceHelper.getUserMapper().insert(userdata);
        } else {
            UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
            userUpdateWrapper.eq("id", usersTopView.getId());
            User userdata = new User();
//            userdata.setId(usersTopView.getId());
//            userdata.setUserId(usersTopView.getId());
            userdata.setWebSite(usersTopView.getWebSite());
            userdata.setFatherId(usersTopView.getFatherId());
            userdata.setEnterpriseId(usersTopView.getEnterpriseId());
            userdata.setEnterpriseName(usersTopView.getEnterpriseName());
            userdata.setType(usersTopView.getType());
            userdata.setPassword(usersTopView.getPassword());
            userdata.setRealName(usersTopView.getRealName());
            userdata.setPinYin(HanziToPinyinUtils.getPinyin(usersTopView.getRealName()));//中文转拼音
            userdata.setPhone(usersTopView.getPhone());
            userdata.setPosition(usersTopView.getPosition());
            userdata.setEmail(usersTopView.getEmail());
            userdata.setCreateDate(usersTopView.getCreateDate());
            userdata.setStatus(usersTopView.getStatus());
            userdata.setModifyDate(usersTopView.getModifyDate());
            serviceHelper.getUserMapper().update(userdata, userUpdateWrapper);
        }
    }

    /**
     * 监听主账号状态
     *
     * @param usersTopView sqlservice中相关数据
     */
    @Override
    public void handleModelUpdateUsersMqMsg(UsersTopView usersTopView) {
        //去mysql中查询一下当前数据是否存在(存在修改/不存在添加)
        User user = findExistence(usersTopView);
        if (user == null) {
            log.info("监听主账号删除中mysql当前没有此请求数据");
            return;
        } else {
            //通过id 去修改账号状态
            UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
            userUpdateWrapper.eq("id", usersTopView.getId()).or().eq("father_id", usersTopView.getId());
            User userDelete = new User();
            userDelete.setStatus(usersTopView.getStatus());
            userDelete.setModifyDate(usersTopView.getModifyDate());
            serviceHelper.getUserMapper().update(userDelete, userUpdateWrapper);
        }

    }

    /**
     * 判断mysql中是否有当前数据
     *
     * @param usersTopView sqlservice中相关数据
     */
    private User findExistence(UsersTopView usersTopView) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("id", usersTopView.getId());
        User user = serviceHelper.getUserMapper().selectOne(userQueryWrapper);
        if (user == null) {
            return null;
        }
        //清空所有redis缓存
        List<String> redisKeyList = this.deleteRedisUser(user);
        serviceHelper.getRedisManager().del(redisKeyList);//redis缓存清空
        return user;
    }

    /**
     * 用于erp定时任务查询子账号数据
     *
     * @return
     */
    @Override
    public ServiceResult<List<User>> allSonUserPage(Integer pageNo, Integer pageSize, Date time) {
        //获取当前日期时间
        Date date = new Date();
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(time);
        rightNow.add(Calendar.DAY_OF_MONTH, -5);
        Date newTime = rightNow.getTime();
        ServiceResult<List<User>> serviceResult = new ServiceResult<List<User>>();
        List<User> userList = serviceHelper.getUserMapper().selectSonUserPage((pageNo - 1) * pageSize, pageSize, newTime, date);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userList);
        return serviceResult;
    }

    /**
     * 监听密码被重置
     *
     * @param usersTopView usersTopView
     */
    @Override
    public void handleModelUpdatePassWordMqMsg(UsersTopView usersTopView) {
        User user = new User();
        user.setPassword(usersTopView.getPassword());
        user.setModifyDate(new Date());
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
        userUpdateWrapper.eq("id", usersTopView.getId());
        serviceHelper.getUserMapper().update(user, userUpdateWrapper);
    }

    /**
     * 监听添加子账号数据
     *
     * @param usersTopView usersTopView
     */
    @Override
    public void handleModelAddSubUsersMqMsg(UsersTopView usersTopView) {
        User user = new User();
        user.setId(usersTopView.getId());//主键
        user.setFatherId(usersTopView.getFatherId());//主账号的id，如果为空，则表示这是主账号
        user.setWebSite(usersTopView.getWebSite());//站点 1 供应商协同系统、2 芯达通系统
        user.setType(usersTopView.getType());//类型（1 卖家[供应商]） Seller Buyer
        user.setUserName(usersTopView.getUserName());//登入账户
        user.setPassword(usersTopView.getPassword());//密码
        user.setRealName(usersTopView.getRealName());//名称
        user.setPinYin(HanziToPinyinUtils.getPinyin(usersTopView.getRealName()));//名称拼音
        user.setPhone(usersTopView.getPhone());//手机号
        user.setPosition(usersTopView.getPosition());//职位
        user.setCreateDate(usersTopView.getCreateDate());//创建时间
        user.setStatus(usersTopView.getStatus());//状态
        user.setEmail(usersTopView.getEmail());//邮箱
        serviceHelper.getUserMapper().insert(user);
    }

    /**
     * 通过账号查询手机号
     *
     * @param usersVo
     * @return 返回手机号
     */
    @Override
    public ServiceResult<User> findPhone(UsersVo usersVo) {
        ServiceResult<User> stringServiceResult = new ServiceResult<>();
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("user_name", usersVo.getUserName());
        User user = serviceHelper.getUserMapper().selectOne(userQueryWrapper);
        stringServiceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        stringServiceResult.setSuccess(Boolean.TRUE);
        stringServiceResult.setResult(user);
        return stringServiceResult;
    }

    /**
     * 忘记密码
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> forgetPassword(UsersVo usersVo) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        //通过手机号获取当前用户信息
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("phone", usersVo.getPhone());
        User user = baseMapper.selectOne(userQueryWrapper);
        //需要把通过主键获取用户详情缓存清空
        String redisUserNameKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + user.getId();
        serviceHelper.getRedisManager().del(redisUserNameKey);
        ForgetPasswordVo forgetPasswordVo = new ForgetPasswordVo();
        forgetPasswordVo.setPassWord(usersVo.getPassword());
        forgetPasswordVo.setPhoneNum(usersVo.getPhone());
        forgetPasswordVo.setCode(usersVo.getCode());
        forgetPasswordVo.setWebSite(DefaultUserConstants.DEFAULT_WEBSITE);
        forgetPasswordVo.setType(DefaultUserConstants.DEFAULT_TYPE);
        // 向erp 传递账号、密码、手机号、验证码
        String passwordData = HttpUtils.sendPostJson(serviceHelper.getErpInterfaceAddressConfig().getErpForgotPasswordUrl(), forgetPasswordVo);
        Object parse = JSONObject.parse(passwordData);
        Map entity = (Map) parse;
        Boolean success = (Boolean) entity.get("success");
        if (success) {//判断当前返回状态
            serviceResult.setSuccess(Boolean.TRUE);
            return serviceResult;
        } else {
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage(entity.get("data").toString());
            return serviceResult;
        }
    }

    /**
     * 调用erp那边接口校验验证码是否正确
     *
     * @param usersVo
     * @return
     */
    @Override
    public ServiceResult<Boolean> verificationCode(UsersVo usersVo) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        VerificationCodeVo verificationCodeVo = new VerificationCodeVo();
        verificationCodeVo.setPhoneNum(usersVo.getPhone());
        verificationCodeVo.setCode(usersVo.getCode());
        String codeData = HttpUtils.sendPostJson(serviceHelper.getUserConfig().getVerificationCodeUrl(), verificationCodeVo);
        Object parse = JSONObject.parse(codeData);
        Map entity = (Map) parse;
        Boolean success = (Boolean) entity.get("success");
        if (success) {//判断当前返回状态
            serviceResult.setSuccess(Boolean.TRUE);
            return serviceResult;
        } else {
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage(entity.get("data").toString());
            return serviceResult;
        }
    }

    //封装用户详情方法
    private AuthenticationUser encapsulationUser(User user) {
        AuthenticationUser authenticationUser = new AuthenticationUser();
        authenticationUser.setId(user.getId());//用户id
        authenticationUser.setUsername(user.getUserName());// 账号
        authenticationUser.setName(user.getRealName());    // 名称
        authenticationUser.setPosition(String.valueOf(user.getPosition()));// 职位：1表示经理，2表示员工
        authenticationUser.setPhone(user.getPhone());    //手机号
        authenticationUser.setEmail(user.getEmail());//邮箱
        authenticationUser.setStatus(String.valueOf(user.getStatus()));//状态
        Date create = user.getCreateDate();
        if (create != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String createDate = simpleDateFormat.format(create);
            authenticationUser.setCreateDate(createDate);//创建日期
        }
        authenticationUser.setEpId(user.getEnterpriseId());//供应商id
        if (StringUtils.isEmpty(user.getFatherId())) {//当前用户是否有主账号
            authenticationUser.setEpName(user.getUserName());//主账号账号
            authenticationUser.setIsMain(1);//是否是主账号
            return authenticationUser;
        } else {//表示当前用户是子账号去查主账号信息
            //从缓存中获取相关数据
            String redisKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + user.getFatherId();
            Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
            if (redisDepartment == null) {
                QueryWrapper<User> queryWrapperMain = new QueryWrapper<>();
                queryWrapperMain.eq("id", user.getFatherId());
                User userMain = serviceHelper.getUserMapper().selectOne(queryWrapperMain);//获取主账号
                if (userMain == null) { //存入缓存中 避免重复操作数据库
                    log.warn("因数据异常，子账号绑定主账号数据不存在");
                    String userRedisKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + user.getFatherId();
                    serviceHelper.getRedisManager().set(userRedisKey, RedisStorage.EMPTY_VALUE);
                    authenticationUser.setIsMain(0);//是否是主账号
                    return authenticationUser;
                }
                //mysql存在当前数据存入缓存中
                String redisUserIdKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + userMain.getId();
                String apiResultJson = JSONObject.toJSONString(userMain, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(redisUserIdKey, apiResultJson);
                authenticationUser.setEpName(userMain.getRealName());//主账号账号
                authenticationUser.setIsMain(0);//是否是主账号
                return authenticationUser;
            }
            if (redisDepartment.equals(RedisStorage.EMPTY_VALUE)) {//为空字符串或根本不存在redis中数据
                log.warn("因数据异常，子账号绑定主账号数据不存在");
                authenticationUser.setIsMain(0);//是否是主账号
                return authenticationUser;
            }
            User userMain = JSONObject.parseObject((String) redisDepartment, User.class);//反序列化
            authenticationUser.setEpName(userMain.getRealName());//主账号账号
            authenticationUser.setIsMain(0);//是否是主账号
            return authenticationUser;
        }
    }

    /**
     * 初始化用户相关所有数据
     *
     * @return
     */
    @Autowired
    public void initUserData() {
        // 删除redis中的主键相关数据
        Boolean deleteResult = serviceHelper.getRedisManager().delRedisDataByKeyPrefix(RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "*");
        if (deleteResult) {
            log.info("删除[{}]开头的数据成功", RedisKeyPrefixEnum.USER_USERID_DATA.getCode());
        }
        //获取user表所有数据
        List<User> userList = baseMapper.selectList(null);
        //账号当做key 手机存入redis中
        for (User user : userList) {
            String redisKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + user.getId();
            String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
            serviceHelper.getRedisManager().set(redisKey, apiResultJson);
        }
    }

    /**
     * 初始化校验相关所有数据
     *
     * @return
     */
    @Autowired
    public void initCheckData() {
        // 删除redis中的校验相关数据
        Boolean deleteUserNameResult = serviceHelper.getRedisManager().delRedisDataByKeyPrefix(RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode() + "*");
        Boolean deletePhoneResult = serviceHelper.getRedisManager().delRedisDataByKeyPrefix(RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "*");
        Boolean deleteEmailResult = serviceHelper.getRedisManager().delRedisDataByKeyPrefix(RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode() + "*");
        if (deleteUserNameResult) {
            log.info("删除[{}]开头的数据成功", RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode());
        }
        if (deletePhoneResult) {
            log.info("删除[{}]开头的数据成功", RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode());
        }
        if (deleteEmailResult) {
            log.info("删除[{}]开头的数据成功", RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode());
        }
        //获取user表所有数据
        List<User> userList = baseMapper.selectList(null);
        //分别 账号 邮箱 手机号 缓存各存遍
        for (User user : userList) {
            if (!StringUtils.isEmpty(user.getUserName())) {//必须保证账号不为空
                String redisKey = RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode() + "_" + user.getUserName();
                String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(redisKey, apiResultJson);
            }
            if (!StringUtils.isEmpty(user.getEmail())) {//必须保证邮箱不为空
                String redisKey = RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode() + "_" + user.getEmail();
                String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(redisKey, apiResultJson);
            }
            if (!StringUtils.isEmpty(user.getPhone())) {//必须保证手机号不为空
                String redisKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + user.getPhone();
                String apiResultJson = JSONObject.toJSONString(user, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(redisKey, apiResultJson);
            }
        }
    }

    private List<String> deleteRedisUser(User user) {//删除所有缓存key
        List<String> list = new ArrayList<>();//存放所有key
        String redisUserNameKey = RedisKeyPrefixEnum.CHENK_FINDUSERNAME_DATA.getCode() + "_" + user.getUserName();//校验子账号添加时账号是否重复缓存
        list.add(redisUserNameKey);
        String redisPhoneKey = RedisKeyPrefixEnum.CHENK_FINDPHONE_DATA.getCode() + "_" + user.getPhone();//校验子账号添加时手机号是否重复缓存
        list.add(redisPhoneKey);
        String redisEmailKey = RedisKeyPrefixEnum.CHENK_FINDEMAIL_DATA.getCode() + "_" + user.getEmail();//校验子账号添加时邮箱是否重复缓存
        list.add(redisEmailKey);
        String redisUserIdKey = RedisKeyPrefixEnum.USER_USERID_DATA.getCode() + "_" + user.getId();//校验子账号添加时邮箱是否重复缓存
        list.add(redisUserIdKey);
        return list;
    }

    private List<Role> selectRedisData(Set<String> roleListId) {
        List<Role> roles = new ArrayList<>();//存结果集合
        for (String roleId : roleListId) {//批量拼接 key
            String redisKey = RedisKeyPrefixEnum.ROLE_ID_DATA.getCode() + "_" + roleId;
            Object redisDepartment = serviceHelper.getRedisManager().get(redisKey);//redis中获取对应数据
            if (redisDepartment == null) {//不存在去数据库中查一下
                Query queryRole = Query.query(Criteria.where("_id").is(roleId));
                //获取对应的角色信息
                Role role = serviceHelper.getMongodbManager().findOne(queryRole, Role.class);
                if (role == null) {//数据库不存在存入缓存 避免重复对数据库添加压力
                    String roleRedisKey = RedisKeyPrefixEnum.ROLE_ID_DATA.getCode() + "_" + roleId;
                    serviceHelper.getRedisManager().set(roleRedisKey, RedisStorage.EMPTY_VALUE);
                    continue;
                }
                RedisRolePojo redisRolePojo = this.packaRedisRoleData(role);
                String rolekey = RedisKeyPrefixEnum.ROLE_ID_DATA.getCode() + "_" + redisRolePojo.get_id();
                String apiResultJson = JSONObject.toJSONString(redisRolePojo, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
                serviceHelper.getRedisManager().set(rolekey, apiResultJson);
                roles.add(role);
                continue;
            }
            if (RedisStorage.EMPTY_VALUE.equals(redisDepartment)) {//数据库中没此数据
                continue;
            }
            RedisRolePojo redisRolePojo = JSONObject.parseObject((String) redisDepartment, RedisRolePojo.class);//反序列化
            Role role = new Role();
            role.set_id(new ObjectId(redisRolePojo.get_id()));
            role.setName(redisRolePojo.getName());
            role.setAvailable(redisRolePojo.getAvailable());
            role.setCreateTime(redisRolePojo.getCreateTime());
            role.setMenuList(redisRolePojo.getMenuList());
            role.setUpdateTime(redisRolePojo.getUpdateTime());
            role.setCreatorId(redisRolePojo.getCreatorId());
            roles.add(role);
            continue;
        }
        return roles;
    }

    private RedisRolePojo packaRedisRoleData(Role role) {  //封装统一对象存入redis
        RedisRolePojo redisRolePojo = new RedisRolePojo();
        redisRolePojo.set_id(String.valueOf(role.get_id()));
        redisRolePojo.setAvailable(role.getAvailable());
        redisRolePojo.setCreateTime(role.getCreateTime());
        redisRolePojo.setMenuList(role.getMenuList());
        redisRolePojo.setName(role.getName());
        redisRolePojo.setUpdateTime(role.getUpdateTime());
        redisRolePojo.setCreatorId(role.getCreatorId());
        return redisRolePojo;
    }
}