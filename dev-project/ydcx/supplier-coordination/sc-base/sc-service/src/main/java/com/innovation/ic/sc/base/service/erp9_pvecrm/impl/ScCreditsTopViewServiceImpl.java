package com.innovation.ic.sc.base.service.erp9_pvecrm.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.erp9_pvecrm.ScCreditsTopViewMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.ScCreditsTopView;
import com.innovation.ic.sc.base.service.erp9_pvecrm.ScCreditsTopViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 对账单视图
 * @Date 2022/12/20 9:29
 **/
@Service
@Slf4j
public class ScCreditsTopViewServiceImpl extends ServiceImpl<ScCreditsTopViewMapper, ScCreditsTopView> implements ScCreditsTopViewService {
    /**
     * 查找视图当天所有数据
     *
     * @return
     */
    @Override
    public List<ScCreditsTopView> findAllBySameDay() {
        LambdaQueryWrapper<ScCreditsTopView> queryWrapper = new LambdaQueryWrapper<>();
        //获取当天的数据
        queryWrapper.ge(ScCreditsTopView::getGenerationDate, getStartDate(Calendar.getInstance()).getTime());
        queryWrapper.le(ScCreditsTopView::getGenerationDate, getEndDate(Calendar.getInstance()).getTime());
        return baseMapper.selectList(queryWrapper);
    }

    /**
     * 获得当天的起始时间
     *
     * @return
     */
    public static Calendar getStartDate(Calendar today) {
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        return today;
    }

    /**
     * 获取当天截止时间
     *
     * @return
     */
    public static Calendar getEndDate(Calendar endToday) {
        endToday.set(Calendar.HOUR_OF_DAY, 23);
        endToday.set(Calendar.MINUTE, 59);
        endToday.set(Calendar.SECOND, 59);
        endToday.set(Calendar.MILLISECOND, 59);
        return endToday;
    }
}
