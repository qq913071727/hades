package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.mapper.sc.ProductMapper;
import com.innovation.ic.sc.base.model.sc.Brand;
import com.innovation.ic.sc.base.model.sc.Product;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.sc.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 产品
 * @Date 2022/11/18 13:52
 **/
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
    /**
     * 根据品牌ID查询产品列表
     *
     * @param brandsId
     * @param productName
     * @return
     */
    @Override
    public ServiceResult<List<Product>> listByBrandsId(String brandsId, String productName) {
        LambdaQueryWrapper<Product> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Product::getBrandId, brandsId);
        if (StringUtils.validateParameter(productName)) {
            wrapper.likeRight(Product::getPartNumber, productName);
        }
        List<Product> products = baseMapper.selectList(wrapper);
        return ServiceResult.ok(products, "根据品牌ID查询产品型号列表成功");
    }
}
