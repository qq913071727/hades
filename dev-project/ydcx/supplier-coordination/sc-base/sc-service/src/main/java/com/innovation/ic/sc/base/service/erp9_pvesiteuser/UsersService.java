package com.innovation.ic.sc.base.service.erp9_pvesiteuser;

import com.innovation.ic.sc.base.model.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.UsersTopView;

import java.util.List;

public interface UsersService {

    /**
     * 获取sql server中users数据
     * @return
     */
    List<ParentUsersTopView> selectList();
}
