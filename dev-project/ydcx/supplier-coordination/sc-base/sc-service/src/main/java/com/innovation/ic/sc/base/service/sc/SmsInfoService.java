package com.innovation.ic.sc.base.service.sc;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.cms.AccountBindBySmsVo;
import java.io.IOException;

/**
 * @desc   SmsInfo的服务类接口
 * @author linuo
 * @time   2022年8月5日10:55:37
 */
public interface SmsInfoService {

    /**
     * 发送短信验证码
     * @param type 短信验证码类型,1: 登录
     * @param mobile 手机号
     * @return 返回发送结果
     */
    ServiceResult<Boolean> send(String type, String mobile);

    /**
     * 通过ERP发送短信验证码
     * @param mobile 手机号
     * @return 返回发送结果
     */
    ServiceResult<JSONObject> sendSmsCodeByErp(String mobile);

    /**
     * 绑定账号
     * @param accountBindBySmsVo 账号绑定的Vo类
     * @return 返回绑定结果
     */
    ServiceResult<JSONObject> bindAccount(AccountBindBySmsVo accountBindBySmsVo) throws IOException;

    /**
     * 解绑账号
     * @param authenticationUser 登录用户
     * @return 返回解绑结果
     */
    ServiceResult<Boolean> unBindAccount(AuthenticationUser authenticationUser);
}