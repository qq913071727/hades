package com.innovation.ic.sc.base.service.erp9_pvebcs.impl;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.sc.base.mapper.erp9_pvebcs.ErpDictionariesMapper;
import com.innovation.ic.sc.base.model.erp9_pvebcs.ErpDictionaries;
import com.innovation.ic.sc.base.pojo.enums.ImportEnumerationEnum;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.erp9_pvebcs.ErpDictionariesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 字典表
 */
@Slf4j
@Service
public class ErpDictionariesServiceImpl implements ErpDictionariesService {

    @Resource
    private ErpDictionariesMapper erpDictionariesMapper;

    @Override
    public ServiceResult<List<ErpDictionaries>> findErpDictionaries(ImportEnumerationEnum... importEnumerationEnums) {
        ServiceResult<List<ErpDictionaries>> serviceResult = new ServiceResult<>();
        List<ErpDictionaries> result = new ArrayList<>();
        if (importEnumerationEnums == null || importEnumerationEnums.length == 0) {
            return null;
        }
        for (ImportEnumerationEnum importEnumerationEnum : importEnumerationEnums) {
            String desc = importEnumerationEnum.getDesc();
            List<String> nameList = importEnumerationEnum.getNameList();
            List<ErpDictionaries> erpDictionaries = erpDictionariesMapper.findErpDictionaries(desc,nameList);
            log.info("查询erp字典表 code , {}  ,result {} ", desc, JSONObject.toJSONString(erpDictionaries));
            if (erpDictionaries != null && erpDictionaries.size() > 0) {
                result.addAll(erpDictionaries);
            }
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }
}
