package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.pojo.variable.advantageModel.*;
import com.innovation.ic.sc.base.pojo.variable.inventory.MayApplyInventoryModelPojo;
import com.innovation.ic.sc.base.vo.advantageModel.AdvantageModelDeleteVo;
import com.innovation.ic.sc.base.vo.advantageModel.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @desc   advantage_model的服务类接口
 * @author linuo
 * @time   2022年8月29日13:52:13
 */
public interface AdvantageModelService {
    /**
     * 清空表SpecialsSCTopView中的全部数据
     */
    void truncateTable();

    /**
     * 优势型号信息删除
     * @param advantageModelDeleteVo 删除数据的Vo类
     * @param enterpriseId 供应商id
     * @return 返回删除结果信息
     */
    ServiceResult<Boolean> delete(AdvantageModelDeleteVo advantageModelDeleteVo, String enterpriseId) throws IOException;

    /**
     * 查询可申请优势型号的的库存型号
     * @param epId 供应商id
     * @return 返回可申请优势型号的的库存型号
     */
    ServiceResult<List<MayApplyInventoryModelPojo>> getMayApplyInventoryModel(String epId);

    /**
     * 取消优势型号
     * @param advantageModelCancleVo 取消优势型号的Vo类
     * @param enterpriseId 供应商id
     * @return 返回处理结果
     */
    ServiceResult<Boolean> cancleAdvantageModel(AdvantageModelCancleVo advantageModelCancleVo, String enterpriseId) throws IOException;

    /**
     * 优势型号信息列表查询
     * @param advantageModelRequestVo 优势型号信息列表查询的请求Vo类
     * @param enterpriseId 供应商id
     * @return 返回优势型号信息
     */
    ServiceResult<List<AdvantageModelRespPojo>> query(AdvantageModelRequestVo advantageModelRequestVo, String enterpriseId);

    /**
     * 优势型号信息数据数量查询
     * @param advantageModelRequestVo 优势型号信息列表查询的请求Vo类
     * @param enterpriseId 供应商id
     * @return 返回优势型号信息
     */
    ServiceResult<Long> queryTotal(AdvantageModelRequestVo advantageModelRequestVo, String enterpriseId);

    /**
     * 查询拒绝原因
     * @param id 主键id
     * @return 返回拒绝原因
     */
    ServiceResult<String> queryRefusedReason(String id);

    /**
     * 优势型号批量导入
     * @param advantageModelBatchAddVo 优势型号批量导入的Vo类
     * @param authenticationUser 用户信息
     * @return 返回导入结果
     */
    ServiceResult<AdvantageModelBatchAddExceptionRespPojo> batchAdd(AdvantageModelBatchAddVo advantageModelBatchAddVo, AuthenticationUser authenticationUser) throws IOException, ParseException;

    /**
     * 优势型号单条新增
     * @param advantageModelAddVo 优势型号单条新增的Vo类
     * @param authenticationUser 用户信息
     * @param source 来源(1用户添加、2优势型号关联、3erp导入)
     * @return 返回新增结果
     */
    ServiceResult<Boolean> add(AdvantageModelAddVo advantageModelAddVo, AuthenticationUser authenticationUser, Integer source) throws IOException, ParseException;

    /**
     * 申请延期
     * @param advantageModelApplyDelayVo 优势型号申请延期的Vo类
     * @return 返回操作结果
     */
    ServiceResult<Boolean> applyDelay(AdvantageModelApplyDelayVo advantageModelApplyDelayVo) throws IOException;

    /**
     * 处理优势型号的rabbitmq消息
     * @param specialsScTopView 优势型号内容
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleAdvantageModelAddMqMsg(SpecialsSCTopView specialsScTopView);

    /**
     * 优势型号审批信息处理
     * @param erpId erpId
     * @param status 状态(200通过 300否决)
     * @param summary 备注
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleAdvantageModelAuditMqMsg(String erpId, String status, String summary);

    /**
     * 优势型号取消信息处理
     * @param specialsScTopViewCloseDelPojo 优势型号取消信息
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleAdvantageModelCloseMqMsg(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo);

    /**
     * 优势型号删除mq信息处理
     * @param specialsScTopViewCloseDelPojo 优势型号删除信息
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleAdvantageModelDeleteMqMsg(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo);

    /**
     * 删除erp导入的数据
     * @return 返回删除结果
     */
    ServiceResult<Boolean> deleteErpImportData();

    /**
     * 校验当前型号是否为已有型号
     * @param advantageModelCheckVo 优势型号校验的Vo类
     * @param authenticationUser 用户信息
     * @return 返回校验结果
     */
    ServiceResult<Boolean> checkAdvantageModelIfHave(AdvantageModelCheckVo advantageModelCheckVo, AuthenticationUser authenticationUser);

    /**
     * 优势品牌删除时,同时删除品牌下的型号,只删除状态为2
     */
    void deleteByRelationModel(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo) throws IOException;

    void cancelAdvantageBrand(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo) throws IOException;

    /**
     * 校验当前型号是否已添加
     * @param advantageModelCheckVo 优势型号校验的Vo类
     * @param authenticationUser 用户信息
     * @return 返回校验结果
     */
    ServiceResult<Boolean> checkAdvantageModelIfAdd(AdvantageModelCheckVo advantageModelCheckVo, AuthenticationUser authenticationUser);

    /**
     * 优势型号再次发起审核
     * @param advantageModelAgainInitiateVo 优势型号再次发起审核接口的Vo类
     * @param authenticationUser 用户信息
     * @return 返回发起审核结果
     */
    ServiceResult<Boolean> againInitiate(AdvantageModelAgainInitiateVo advantageModelAgainInitiateVo, AuthenticationUser authenticationUser) throws IOException, ParseException;

    /**
     * 判断是否为优势型号
     * @param advantageModelJudgeStatusVo 判断是否为优势型号的Vo类
     * @return 返回判断结果
     */
    ServiceResult<List<AdvantageModelJudgeStatusRespPojo>> judgeIfAdvantageModel(AdvantageModelJudgeStatusVo advantageModelJudgeStatusVo);

    /**
     * 根据品牌、型号数组批量判断是否为优势型号
     * @param advantageModelBatchJudgeStatusVo 批量判断是否为优势型号的Vo类
     * @return 返回判断结果
     */
    ServiceResult<AdvantageModelBatchJudgeStatusRespPojo> batchJudgeIfAdvantageModelByBrandModelArray(AdvantageModelBatchJudgeStatusVo advantageModelBatchJudgeStatusVo);

    /**
     * 根据型号数组批量判断是否为优势型号
     * @param advantageModelBatchJudgeModelStatusVo 批量判断是否为优势型号的Vo类
     * @return 返回判断结果
     */
    ServiceResult<AdvantageModelBatchJudgeModelStatusRespPojo> batchJudgeIfAdvantageModelByModelArray(AdvantageModelBatchJudgeModelStatusVo advantageModelBatchJudgeModelStatusVo);

    /**
     * 根据型号查询所属品牌
     * @param partNumber 型号
     * @return 返回查询结果
     */
    ServiceResult<List<String>> queryBrandsByPartNumber(String partNumber, AuthenticationUser authenticationUser);

    /**
     * 更新优势型号生效状态
     * @param map 参数
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateStatusByParam(Map<String, Object> map) throws IOException;

    /**
     * 初始化优势型号数据
     */
    void initAdvantageModelData();

    /**
     * 处理优势型号通知的mq消息
     * @param brand 品牌
     * @param partNumber 型号
     * @param enterpriseId 供应商id
     * @param id erpid
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleAdvantageModelNotifyMqMsg(String brand, String partNumber, String enterpriseId, String id);

    /**
     * 判断优势型号数据中是否已保存erp的id
     * @param idList id集合
     * @return 返回判断结果
     */
    ServiceResult<Boolean> judgeIfSaveErpId(List<Integer> idList);

    /**
     * 将advantage_model表中所有型号的所属品牌数据导入到redis，提供给根据型号查询所属品牌接口使用
     */
    void initAdvantageModelBrand();
}