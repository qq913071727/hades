package com.innovation.ic.sc.base.service.erp9_pvebcs;

import com.innovation.ic.sc.base.model.erp9_pvebcs.ErpDictionaries;
import com.innovation.ic.sc.base.pojo.enums.ImportEnumerationEnum;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

import java.util.List;

public interface ErpDictionariesService {


    ServiceResult<List<ErpDictionaries>> findErpDictionaries(ImportEnumerationEnum... importEnumerationEnums);

}
