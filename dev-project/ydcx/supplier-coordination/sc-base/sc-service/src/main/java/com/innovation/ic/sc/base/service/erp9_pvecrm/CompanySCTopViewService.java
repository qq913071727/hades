package com.innovation.ic.sc.base.service.erp9_pvecrm;

import com.innovation.ic.sc.base.model.erp9_pvecrm.CompanySCTopView;
import com.innovation.ic.sc.base.model.erp9_pvecrm.EnterprisesTopView;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

import java.util.List;

/**
 * 注册公司信息
 */
public interface CompanySCTopViewService {


    ServiceResult<List<CompanySCTopView>> findByIds(List<String> ids);


}