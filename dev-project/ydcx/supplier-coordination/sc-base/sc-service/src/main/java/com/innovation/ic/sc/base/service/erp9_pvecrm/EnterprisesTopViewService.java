package com.innovation.ic.sc.base.service.erp9_pvecrm;

import com.innovation.ic.sc.base.model.erp9_pvecrm.EnterprisesTopView;
import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author linuo
 * @desc SpecialsSCTopView的服务类接口
 * @time 2022年8月29日14:41:18
 */
public interface EnterprisesTopViewService {


    ServiceResult<Integer> countEnterprises();


    ServiceResult< List<EnterprisesTopView>> findByEnterprisesTopView(Integer offset, Integer rows);


}