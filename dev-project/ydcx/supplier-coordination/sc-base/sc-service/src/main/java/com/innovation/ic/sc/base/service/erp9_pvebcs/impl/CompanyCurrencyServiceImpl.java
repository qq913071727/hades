package com.innovation.ic.sc.base.service.erp9_pvebcs.impl;

import com.innovation.ic.sc.base.mapper.erp9_pvebcs.CompanyCurrencyMapper;
import com.innovation.ic.sc.base.model.erp9_pvebcs.CompanyCurrency;
import com.innovation.ic.sc.base.service.erp9_pvebcs.CompanyCurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * erp币种同步
 */
@Slf4j
@Service
public class CompanyCurrencyServiceImpl implements CompanyCurrencyService {

    @Autowired
    private CompanyCurrencyMapper companyCurrencyMapper;

    @Override
    public List<CompanyCurrency> findCompanyCurrency(List<String> nameList) {
        return companyCurrencyMapper.findCompanyCurrency(nameList);
    }
}
