package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.UploadFile;
import com.innovation.ic.sc.base.pojo.enums.UploadFileEnum;
import com.innovation.ic.sc.base.pojo.enums.UploadFileStatusEnum;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.FileVo;
import com.innovation.ic.sc.base.vo.RelationMyCompanyNameVo;
import com.jcraft.jsch.SftpException;

import java.util.List;

public interface UploadFileService {

    void updateRelationId(List<String> relationPictureIds, UploadFileEnum uploadFileEnum,Integer relationId);

    ServiceResult fileUpload(byte[] bytes, String originalFilename, String suffix, String creatorID) throws SftpException;


    ServiceResult<List<UploadFile>> findByRelationId(UploadFileEnum uploadFileEnum, String relationId);


    ServiceResult<List<UploadFile>> findByRelationId(UploadFileEnum uploadFileEnum, String relationId, UploadFileStatusEnum uploadFileStatusEnum);

    void saveRelationMyCompanyBatch(List<FileVo> list, RelationMyCompanyNameVo relationMyCompanyNameVo);

    void saveUploadFile(List<UploadFile> files);


    ServiceResult<List<UploadFile>> findByIds(List<String> ids);

    void updateRelationIdStatus(UploadFileEnum uploadFileEnum, String relationId, UploadFileStatusEnum uploadFileStatusEnum);

    ServiceResult pictureUploadToModel(byte[] bytes, String originalFilename, String suffix, String id, String model, String relationId);


    void updateRelationIdAndType(String id, UploadFileEnum uploadFileEnum,Integer relationId,Integer type);


}
