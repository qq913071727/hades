package com.innovation.ic.sc.base.service.erp9_pvesiteuser;

import com.innovation.ic.sc.base.model.erp9_pvesiteuser.Menus;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

import java.util.List;

public interface MenusService {

    /**
     * 查找所有菜单数据
     * @return
     */
    ServiceResult<List<Menus>> findAll();

}
