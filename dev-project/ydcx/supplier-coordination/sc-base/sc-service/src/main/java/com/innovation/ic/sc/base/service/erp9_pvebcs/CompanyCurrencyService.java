package com.innovation.ic.sc.base.service.erp9_pvebcs;

import com.innovation.ic.sc.base.model.erp9_pvebcs.CompanyCurrency;

import java.util.List;

public interface CompanyCurrencyService {
    List<CompanyCurrency> findCompanyCurrency( List<String> nameList);

}
