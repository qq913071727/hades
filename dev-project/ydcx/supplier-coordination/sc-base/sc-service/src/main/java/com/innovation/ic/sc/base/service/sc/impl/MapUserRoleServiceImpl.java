package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.MapUserRoleMapper;
import com.innovation.ic.sc.base.model.sc.MapUserRole;
import com.innovation.ic.sc.base.model.sc.Role;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.MapUserRoleService;
import com.innovation.ic.sc.base.vo.menu.MapUserRoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * MapUserRoleService的具体实现类
 */
@Service
@Transactional
public class MapUserRoleServiceImpl extends ServiceImpl<MapUserRoleMapper, MapUserRole> implements MapUserRoleService {

    @Resource
    private ServiceHelper serviceHelper;
    /**
     * 保存MapUserRole类型对象列表（先删除旧的关系，再添加新的关系）
     *
     * @param mapUserRoleList
     * @return
     */
    @Override
    public ServiceResult<Boolean> saveOrUpdateMapUserRoleList(List<MapUserRole> mapUserRoleList) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();

        // 删除
        Query query = Query.query(Criteria.where("userId").is(mapUserRoleList.get(0).getUserId()));
        serviceHelper.getMongodbManager().remove(query, MapUserRole.class);

        // 添加
        serviceHelper.getMongodbManager().bulkOpsInsert(MapUserRole.class,mapUserRoleList);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据userId，查找MapUserRole对象列表
     *
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<List<MapUserRole>> findByUserId(String userId) {
        ServiceResult<List<MapUserRole>> serviceResult = new ServiceResult<List<MapUserRole>>();

        Query query = new Query(Criteria.where("userId").is(userId));
        List<MapUserRole> mapUserRoleList = serviceHelper.getMongodbManager().find(query, MapUserRole.class);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(mapUserRoleList);
        return serviceResult;
    }
}
