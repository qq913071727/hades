package com.innovation.ic.sc.base.service.sc;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryAddRespPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryGetListRespPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryUnMatchDataQueryRespPojo;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.DefaultLadderPricePojo;
import com.innovation.ic.sc.base.vo.BrandsPageVo;
import com.innovation.ic.sc.base.vo.DefaultQueryVo;
import com.innovation.ic.sc.base.vo.inventory.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @desc   Inventory的服务类接口
 * @author linuo
 * @time   2022年8月23日09:46:53
 */
public interface InventoryService {

    /**
     * 库存新增
     * @param inventoryAddVo 库存新增接口的Vo类
     * @param authenticationUser 用户信息
     * @return 返回新增结果
     */
    ServiceResult<InventoryAddRespPojo> add(InventoryAddVo inventoryAddVo, AuthenticationUser authenticationUser) throws IOException, InterruptedException;

    /**
     * 库存批量导入
     * @param inventoryBatchAddVo 库存批量导入接口的Vo类
     * @param authenticationUser 用户信息
     * @return 返回导入结果
     */
    ServiceResult<InventoryAddRespPojo> batchAdd(InventoryBatchAddVo inventoryBatchAddVo, AuthenticationUser authenticationUser) throws IOException, InterruptedException;

    /**
     * 库存上架/下架
     * @param inventoryStatusUpdateReqVo 库存上架/下架接口的Vo类
     * @param authenticationUser 登录用户
     * @return 返回更新结果
     */
    ServiceResult<List<Map<String, String>>> updateStatus(InventoryStatusUpdateReqVo inventoryStatusUpdateReqVo, AuthenticationUser authenticationUser) throws IOException;

    /**
     * 库存信息删除
     * @param inventoryDeleteVo 删除库存数据的Vo类
     * @param authenticationUser 登录用户
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(InventoryDeleteVo inventoryDeleteVo, AuthenticationUser authenticationUser) throws IOException;

    /**
     * 库存时间更新
     * @param inventoryUpdateDateVo 库存时间更新接口的Vo类
     * @param authenticationUser 登录用户
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateInventoryDate(InventoryUpdateDateVo inventoryUpdateDateVo, AuthenticationUser authenticationUser) throws IOException;

    /**
     * 库存信息列表查询
     * @param inventoryQueryVo 库存信息列表查询接口的Vo类
     * @param epId 供应商id
     * @return 返回 库存信息列表信息
     */
    ServiceResult<JSONObject> query(InventoryQueryVo inventoryQueryVo, String epId) throws ParseException;

    /**
     * 库存信息列表批量查询
     * @param inventoryBatchQueryVo 库存信息列表批量查询接口的Vo类
     * @param authenticationUser 登录账户
     * @return 返回 库存信息列表信息
     */
    ServiceResult<List<InventoryGetListRespPojo>> batchQuery(InventoryBatchQueryVo inventoryBatchQueryVo, AuthenticationUser authenticationUser);

    /**
     * 默认阶梯价格配置查询
     * @param epId 供应商id
     * @return 返回查询结果
     */
    ServiceResult<List<DefaultLadderPricePojo>> getDefaultLadderPrice(String epId);

    /**
     * 库存阶梯价格配置
     * @param epId 供应商id
     * @param inventoryLadderPriceVo 更新阶梯价格和库存的Vo类
     * @return 返回配置结果
     */
    ServiceResult<Boolean> setLadderPrice(String epId, InventoryLadderPriceVo inventoryLadderPriceVo);

    /**
     * 库存信息编辑更新
     * @param inventoryEditVo 库存信息编辑更新接口的Vo类
     * @return 返回更新结果
     */
    ServiceResult<Boolean> edit(InventoryEditVo inventoryEditVo) throws IOException;

    ServiceResult<PageInfo<Inventory>> findBrand(BrandsPageVo brandsPageVo);

    /**
     * 确认标准匹配结果
     * @param inventoryConfirmMatchResultVo 库存确认匹配结果的Vo类
     * @return 返回处理结果
     */
    ServiceResult<Boolean> confirmStandardMatchResult(InventoryConfirmMatchResultVo inventoryConfirmMatchResultVo) throws IOException;

    /**
     * 查询当前供应商全部匹配上标准库的数据id集合
     * @param epId 供应商id
     * @return 返回查询结果
     */
    ServiceResult<List<Integer>> getMatchStandardLibraryInventoryIds(String epId);

    /**
     * 校验dataList中的标准匹配型号和标准匹配品牌是否被已有数据使用
     * @param dataList 库存确认匹配结果集合
     * @param epId 供应商id
     * @return 返回判断结果
     */
    ServiceResult<List<Integer>> judgeStandardDataIfHaveUsed(List<InventoryMatchResultVo> dataList, String epId);

    /**
     * 初始化库存数据
     */
    void initInventoryData();

    /**
     * 将Inventory列表查询的结果数据导入到redis
     */
    void importQueryResultToRedis();

    /**
     * 根据参数插入库存的redis数据
     * @param status 状态
     * @param inventoryHome 库存所在地
     * @param unitPriceType 价格类型
     * @param ladderPriceId 阶梯价格id
     * @param enterpriseId 供应商id
     */
    void updateInventoryRedisDataByParam(Integer status, Integer inventoryHome, Integer unitPriceType, Integer ladderPriceId, String enterpriseId);

    /**
     * 根据品牌、型号、供应商id查询库存中的数据
     * @param brand 品牌
     * @param partNumber 型号
     * @param enterpriseId 供应商id
     * @return 返回查询结果
     */
    ServiceResult<List<Inventory>> selectListByPartNumberBrandEpId(String brand, String partNumber, String enterpriseId);

    /**
     * 处理库存数据删除队列数据
     * @param id 库存id
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleInventoryDeleteQueueData(Integer id);

    /**
     * 插入库存数据
     * @param inventory 库存数据
     */
    int insertInventoryData(Inventory inventory);

    /**
     * 优势型号状态变更
     * @param inventoryUpdateAdvantageStatusVo 优势型号状态变更接口的Vo类
     * @param authenticationUser 用户信息
     * @return 返回操作结果
     */
    ServiceResult<Boolean> updateAdvantageStatus(InventoryUpdateAdvantageStatusVo inventoryUpdateAdvantageStatusVo, AuthenticationUser authenticationUser) throws Exception;
}