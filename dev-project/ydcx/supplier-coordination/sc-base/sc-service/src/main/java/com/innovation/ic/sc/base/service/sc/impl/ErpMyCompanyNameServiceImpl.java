package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.mapper.sc.ErpMyCompanyNameMapper;
import com.innovation.ic.sc.base.model.sc.Dictionaries;
import com.innovation.ic.sc.base.model.sc.ErpMyCompanyName;
import com.innovation.ic.sc.base.pojo.enums.ErpMyCompanyEnum;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.sc.DictionariesService;
import com.innovation.ic.sc.base.service.sc.ErpMyCompanyNameService;
import com.innovation.ic.sc.base.service.sc.MyCompanyService;
import com.innovation.ic.sc.base.vo.ErpMyCompanyNameVo;
import com.innovation.ic.sc.base.vo.SearchErpMyCompanyNameVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ErpMyCompanyNameServiceImpl extends ServiceImpl<ErpMyCompanyNameMapper, ErpMyCompanyName> implements ErpMyCompanyNameService {

    @Resource
    private MyCompanyService myCompanyService;

    @Resource
    private DictionariesService dictionariesService;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBatch(List<ErpMyCompanyName> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        this.getBaseMapper().insertBatchSomeColumn(list);
    }

    @Override
    public ServiceResult<List<ErpMyCompanyName>> findByLikeCompanyName(SearchErpMyCompanyNameVo searchErpMyCompanyNameVo) {
        ServiceResult<List<ErpMyCompanyName>> serviceResult = new ServiceResult<>();
/*        Integer pageSize = searchErpMyCompanyNameVo.getPageSize();
        Integer pageNum = searchErpMyCompanyNameVo.getPageNum() - 1;
        searchErpMyCompanyNameVo.setPageParam(pageNum * pageSize);*/
        searchErpMyCompanyNameVo.setPageParam(0);
        searchErpMyCompanyNameVo.setPageNum(20);
        //根据要求,只会查正常状态的公司
        searchErpMyCompanyNameVo.setStatus(ErpMyCompanyEnum.NORMAL.getCode());
        List<ErpMyCompanyName> erpMyCompanyNameList = this.baseMapper.findByLikeCompanyName(searchErpMyCompanyNameVo);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erpMyCompanyNameList);
        return serviceResult;
    }

    @Override
    public void saveOrUpdateErpCompany(ErpMyCompanyNameVo erpMyCompanyNameVo) {
        if (!StringUtils.validateParameter(erpMyCompanyNameVo.getName())) {
            return;
        }
        //验重,如果名字一样,就不进行添加
        Map<String, Object> param = new HashMap<>();
        param.put("name", erpMyCompanyNameVo.getName());
        List<ErpMyCompanyName> erpMyCompanyNames = this.getBaseMapper().selectByMap(param);
/*

        String invoiceTypeId = "";
        List<String> paramDcode = new ArrayList<>();
        paramDcode.add(MyCompanyDictionariesEnum.INVOICE_TYPE.getcName());
        List<Dictionaries> byDcodes = dictionariesService.findByDcodes(paramDcode);
        for (Dictionaries dictionaries : byDcodes) {
            if(dictionaries.getDValue().equals(erpMyCompanyNameVo.getInvoiceType())){
                invoiceTypeId = dictionaries.getId();
            }
        }
        erpMyCompanyNameVo.setInvoiceType(invoiceTypeId);*/
        String supplier = null;
        String invoiceType = null;
        String district = null;
        //公司类型
        Dictionaries supplierDictionaries = dictionariesService.findByDictionariesId(erpMyCompanyNameVo.getSupplierType());
        if (supplierDictionaries != null) {
            supplier = supplierDictionaries.getId();
        }

        //发票
        Dictionaries invoiceTypeDictionaries = dictionariesService.findByDictionariesId(erpMyCompanyNameVo.getInvoiceType());
        if (invoiceTypeDictionaries != null) {
            invoiceType = invoiceTypeDictionaries.getId();
        }


        //国别
        Dictionaries districtDictionaries = dictionariesService.findByDictionariesId(erpMyCompanyNameVo.getDistrict());
        if (districtDictionaries != null) {
            district = districtDictionaries.getId();
        }


        if (!CollectionUtils.isEmpty(erpMyCompanyNames)) {
            //更新
            ErpMyCompanyName updateErpMyCompanyName = erpMyCompanyNames.get(0);
            updateErpMyCompanyName.setExternalId(erpMyCompanyNameVo.getID());
            updateErpMyCompanyName.setDistrict(district);
            updateErpMyCompanyName.setSupplierType(supplier);
            //updateErpMyCompanyName.setNature(erpMyCompanyNameVo.getNature());
            updateErpMyCompanyName.setInvoiceType(invoiceType);
            updateErpMyCompanyName.setEnterpriseStatus(erpMyCompanyNameVo.getStatus());
            this.baseMapper.updateById(updateErpMyCompanyName);
        } else {
            ErpMyCompanyName erpMyCompanyName = new ErpMyCompanyName();
            erpMyCompanyName.setName(erpMyCompanyNameVo.getName());
            erpMyCompanyName.setCreateDate(new Date());
            //默认通过
            erpMyCompanyName.setEnterpriseStatus(erpMyCompanyNameVo.getStatus());
            erpMyCompanyName.setExternalId(erpMyCompanyNameVo.getID());
            erpMyCompanyName.setDistrict(district);

            erpMyCompanyName.setSupplierType(supplier);


            //erpMyCompanyName.setNature(erpMyCompanyNameVo.getNature());

            erpMyCompanyName.setInvoiceType(invoiceType);

            this.baseMapper.insert(erpMyCompanyName);
        }
        myCompanyService.updateMyCompanyExternalId(erpMyCompanyNameVo);
    }

    @Override
    public void updateErpCompany(ErpMyCompanyNameVo erpMyCompanyNameVo) {
        if (!StringUtils.validateParameter(erpMyCompanyNameVo.getID())) {
            return;
        }
        if (erpMyCompanyNameVo.getStatus() == null) {
            return;
        }
        UpdateWrapper<ErpMyCompanyName> wrapper = Wrappers.update();
        wrapper.lambda()
                .set(ErpMyCompanyName::getEnterpriseStatus, erpMyCompanyNameVo.getStatus())
                .eq(ErpMyCompanyName::getExternalId, erpMyCompanyNameVo.getID());
        update(null, wrapper);
    }

    @Override
    public Integer findByCompanyName(String name) {
        return this.baseMapper.findByCompanyName(name);
    }

    @Override
    public ErpMyCompanyName findById(String id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public ErpMyCompanyName findByExternalId(String externalId) {
        Map<String, Object> param = new HashMap<>();
        param.put("external_id", externalId);
        List<ErpMyCompanyName> erpMyCompanyNames = this.baseMapper.selectByMap(param);
        if (!CollectionUtils.isEmpty(erpMyCompanyNames)) {
            return erpMyCompanyNames.get(0);
        }
        return null;
    }

    @Override
    public ErpMyCompanyName findByNameMap(Map<String, Object> paramMap) {
        List<ErpMyCompanyName> erpMyCompanyNames = this.baseMapper.selectByMap(paramMap);
        if (!CollectionUtils.isEmpty(erpMyCompanyNames)) {
            return erpMyCompanyNames.get(0);
        }
        return null;
    }

    @Override
    public void trunCateTable() {
        this.baseMapper.trunCateTable();
    }


}
