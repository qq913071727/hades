package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.mapper.sc.DictionariesMapper;
import com.innovation.ic.sc.base.model.sc.Dictionaries;
import com.innovation.ic.sc.base.model.sc.DictionariesType;
import com.innovation.ic.sc.base.model.sc.MyCompanyCurrency;
import com.innovation.ic.sc.base.pojo.enums.DictionariesAlias;
import com.innovation.ic.sc.base.pojo.enums.MyCompanyDictionariesEnum;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.sc.DictionariesService;
import com.innovation.ic.sc.base.service.sc.DictionariesTypeService;
import com.innovation.ic.sc.base.service.sc.MyCompanyCurrencyService;
import com.innovation.ic.sc.base.vo.DictionariesErpVo;
import com.innovation.ic.sc.base.vo.DictionariesVo;
import com.innovation.ic.sc.base.vo.ErpDictionariesVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;


@Service
@Slf4j
public class DictionariesServiceImpl extends ServiceImpl<DictionariesMapper, Dictionaries> implements DictionariesService {

    @Resource
    private MyCompanyCurrencyService myCompanyCurrencyService;

    @Resource
    private DictionariesTypeService dictionariesTypeService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveBatch(List<DictionariesVo> dictionariesVoS) {
        if (dictionariesVoS == null || dictionariesVoS.size() == 0) {
            return;
        }
        //需要保存的值
        List<Dictionaries> list = new ArrayList<>();

        //需要保存的类型
        List<DictionariesType> DTList = new ArrayList<>();

        //需要添加类型,需要去重
        Map<String, Integer> dictionariesTypeMap = new HashMap<>();

        //需要删除值
        HashSet<String> dictionariesTypeIdS = new HashSet<>();
        for (DictionariesVo dictionaryVo : dictionariesVoS) {
            Dictionaries dictionaries = new Dictionaries();
            BeanUtils.copyProperties(dictionaryVo, dictionaries);
            dictionariesTypeIdS.add(dictionaryVo.getDTypeId());
            list.add(dictionaries);
            Integer isNull = dictionariesTypeMap.get(dictionaryVo.getDTypeId());
            if (isNull == null) {
                //新增
                DictionariesType dictionariesTypeBean = new DictionariesType();
                dictionariesTypeBean.setId(dictionaryVo.getDTypeId());
                dictionariesTypeBean.setName(dictionaryVo.getDCode());
                DTList.add(dictionariesTypeBean);
                dictionariesTypeMap.put(dictionaryVo.getDTypeId(), 1);
            }
        }
        //删除类型
        dictionariesTypeService.deleteBatchIds(dictionariesTypeIdS);
        //删除类型下所有的值
        for (String dictionariesTypeId : dictionariesTypeIdS) {
            Map<String, Object> param = new HashMap<>();
            param.put("d_type_id", dictionariesTypeId);
            this.baseMapper.deleteByMap(param);
        }
        //保存类型
        dictionariesTypeService.insertBatchSomeColumn(DTList);
        //保存类型下的值
        this.saveBatch(list);
    }

    @Override
    public ServiceResult<List<Dictionaries>> searchDistrict(String dTypeId) {
        ServiceResult<List<Dictionaries>> serviceResult = new ServiceResult<>();
        Map<String, Object> param = new HashMap<>();
        param.put("d_type_id", dTypeId);
        List<Dictionaries> dictionaries = this.baseMapper.selectByMap(param);
        serviceResult.setResult(dictionaries);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public void saveDictionaries(DictionariesErpVo dictionariesErpVo) {
        if (!StringUtils.validateParameter(dictionariesErpVo.getArgumentID())) {
            return;
        }
        if (!StringUtils.validateParameter(dictionariesErpVo.getID())) {
            return;
        }
        if (!StringUtils.validateParameter(dictionariesErpVo.getText())) {
            return;
        }
        Dictionaries dictionaries = new Dictionaries();
        dictionaries.setId(dictionariesErpVo.getArgumentID());
        dictionaries.setDTypeId(dictionariesErpVo.getID());
        dictionaries.setDName(dictionariesErpVo.getText());
        dictionaries.setCreateDate(new Date());
        //查询type
        DictionariesType dictionariesType = dictionariesTypeService.selectById(dictionariesErpVo.getID());
        if (dictionariesType != null) {
            dictionaries.setDCode(dictionariesType.getName());
        }
        save(dictionaries);
    }

    @Override
    public void deleteById(String id) {
        if (StringUtils.validateParameter(id)) {
            this.baseMapper.deleteById(id);
        }
    }

    @Override
    public ServiceResult<List<Dictionaries>> findByIds(List<String> ids) {
        ServiceResult<List<Dictionaries>> serviceResult = new ServiceResult<>();
        if (CollectionUtils.isEmpty(ids)) {
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("ids不能为空");
            return serviceResult;
        }
        List<Dictionaries> dictionaries = this.baseMapper.selectBatchIds(ids);
        serviceResult.setResult(dictionaries);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public void listenAddDistrict(ErpDictionariesVo erpDictionariesVo) {
        DictionariesType dictionariesType = dictionariesTypeService.selectById(erpDictionariesVo.getID());
        if (dictionariesType == null) {
            return;
        }
        Dictionaries dictionaries = new Dictionaries();
        dictionaries.setId(erpDictionariesVo.getArgumentID());
        dictionaries.setDCode(dictionariesType.getName());
        dictionaries.setDName(erpDictionariesVo.getText());
        dictionaries.setCreateDate(new Date());
        dictionaries.setDTypeId(dictionariesType.getId());
        dictionaries.setDValue(erpDictionariesVo.getValue());
        this.baseMapper.insert(dictionaries);
    }

    @Override
    public Dictionaries findByDictionariesId(String id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public ServiceResult<Dictionaries> findById(String id) {
        ServiceResult<Dictionaries> serviceResult = new ServiceResult<>();
        serviceResult.setResult(this.baseMapper.selectById(id));
        return serviceResult;
    }

    @Override
    public List<Dictionaries> findAll() {
        return this.baseMapper.selectByMap(new HashMap<>());
    }

    @Override
    public ServiceResult<Map<String, List<Dictionaries>>> findByDcode(List<String> paramList) {
        ServiceResult<Map<String, List<Dictionaries>>> serviceResult = new ServiceResult<>();
        Map<String, List<Dictionaries>> resultMap = new HashMap<>();
        List<Dictionaries> dictionariesList = baseMapper.findByDcode(paramList);
        for (Dictionaries dictionaries : dictionariesList) {
            List<Dictionaries> dictionariesMapList = resultMap.get(MyCompanyDictionariesEnum.of(dictionaries.getDCode()));
            if (CollectionUtils.isEmpty(dictionariesMapList)) {
                List<Dictionaries> list = new ArrayList<>();
                list.add(dictionaries);
                resultMap.put(MyCompanyDictionariesEnum.of(dictionaries.getDCode()), list);
            } else {
                dictionariesMapList.add(dictionaries);
            }
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(resultMap);
        return serviceResult;
    }

    @Override
    public List<Dictionaries> findByDcodes(List<String> paramList) {
        return baseMapper.findByDcode(paramList);
    }

    @Override
    public ServiceResult<Map<String, List<Dictionaries>>> findByMyCompanyDcode(List<String> paramList) {
        ServiceResult<Map<String, List<Dictionaries>>> serviceResult = new ServiceResult<>();
        Map<String, List<Dictionaries>> resultMap = new HashMap<>();
        List<Dictionaries> dictionariesList = baseMapper.findByDcode(paramList);
        for (Dictionaries dictionaries : dictionariesList) {
            List<Dictionaries> dictionariesMapList = resultMap.get(MyCompanyDictionariesEnum.of(dictionaries.getDCode()));
            if (CollectionUtils.isEmpty(dictionariesMapList)) {
                List<Dictionaries> list = new ArrayList<>();
                list.add(dictionaries);
                resultMap.put(MyCompanyDictionariesEnum.of(dictionaries.getDCode()), list);
            } else {
                dictionariesMapList.add(dictionaries);
            }
        }
        //币种需要单独处理
        List<MyCompanyCurrency> myCompanyCurrencyList = myCompanyCurrencyService.findAll();
        if (!CollectionUtils.isEmpty(myCompanyCurrencyList)) {
            List<Dictionaries> dictionaries = new ArrayList<>();
            for (MyCompanyCurrency myCompanyCurrency : myCompanyCurrencyList) {
                Dictionaries dictionariesBean = new Dictionaries();
                dictionariesBean.setId(myCompanyCurrency.getId() + "");
                dictionariesBean.setDName(myCompanyCurrency.getName());
                dictionariesBean.setDCode(MyCompanyDictionariesEnum.CURRENCY.getName());
                dictionariesBean.setDValue(myCompanyCurrency.getId() + "");
                dictionariesBean.setDTypeId(myCompanyCurrency.getEnName());
                dictionaries.add(dictionariesBean);
            }
            resultMap.put(MyCompanyDictionariesEnum.of(MyCompanyDictionariesEnum.CURRENCY.getcName()), dictionaries);
        }
        //遍历map转换别名
        resultMap.forEach((key, value) -> {
            if (key.equals(MyCompanyDictionariesEnum.SUPPLIER_TYPE.getName())) {
                for (Dictionaries dictionaries : value) {
                    DictionariesAlias dictionariesAlias = DictionariesAlias.of(dictionaries.getDName());
                    dictionaries.setDName(dictionariesAlias.getTarget());
                }
            }
        });
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(resultMap);
        return serviceResult;
    }

}
