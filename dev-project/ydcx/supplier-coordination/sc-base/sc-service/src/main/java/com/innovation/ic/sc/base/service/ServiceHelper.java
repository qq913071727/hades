package com.innovation.ic.sc.base.service;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.manager.*;
import com.innovation.ic.sc.base.handler.sc.MenuHandler;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.handler.sc.PriceCalculateHandler;
import com.innovation.ic.sc.base.mapper.sc.*;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.service.erp9_pvestandard.ProductsService;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.innovation.ic.sc.base.value.DyjInterfaceAddressConfig;
import com.innovation.ic.sc.base.value.*;
import lombok.Getter;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

@Getter
@Component
public class ServiceHelper {

    /********************************************* mapper ********************************************/
    @Resource
    private AdvantageModelMapper advantageModelMapper;

    @Resource
    private DepartmentMapper departmentMapper;

    @Resource
    private UserManagementMapper userManagementMapper;

    @Resource
    private AdvantageBrandMapper advantageBrandMapper;

    @Resource
    private MyCompanyEnterpriseMapper myCompanyEnterpriseMapper;

    @Resource
    private ClientMapper clientMapper;

    @Resource
    private InventoryMapper inventoryMapper;

    @Resource
    private ActionMessageUserLinkMapper actionMessageUserLinkMapper;

    @Resource
    private ActionMessageMapper actionMessageMapper;

    @Resource
    private LadderPriceInfoMapper ladderPriceInfoMapper;

    @Resource
    private DefaultLadderPriceMapper defaultLadderPriceMapper;

    @Resource
    private LadderPriceConfigMapper ladderPriceConfigMapper;

    @Resource
    private LadderPriceAmountConfigMapper ladderPriceAmountConfigMapper;

    @Resource
    private DictionariesTypeMapper dictionariesTypeMapper;

    @Resource
    private PersonnelManagementMapper personnelManagementMapper;

    @Resource
    private MyCompanyMapper myCompanyMapper;

    @Resource
    private UserLoginLogMapper userLoginLogMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private SmsInfoMapper smsInfoMapper;

    /********************************************* manager ********************************************/
    @Resource
    private RedisManager redisManager;

    @Resource
    private RabbitMqManager rabbitMqManager;

    @Resource
    private SftpChannelManager sftpChannelManager;

    @Resource
    private MongodbManager mongodbManager;

    /********************************************* handler ********************************************/
    @Resource
    private PriceCalculateHandler priceCalculateHandler;

    @Resource
    private ModelHandler modelHandler;

    @Resource
    private MenuHandler menuHandler;

    /********************************************* config ********************************************/
    @Resource
    private DyjInterfaceAddressConfig dyjInterfaceAddressConfig;

    @Resource
    private LoginConfig loginConfig;

    @Resource
    private ErpInterfaceAddressConfig erpInterfaceAddressConfig;

    @Resource
    private UserConfig userConfig;

    @Resource
    private FileParamConfig fileParamConfig;

    @Resource
    private SmsParamConfig smsParamConfig;

    @Resource
    private RedisParamConfig redisParamConfig;

    @Resource
    private MyCompanyConfig myCompanyConfig;

    @Resource
    private TYCParamConfig tYCParamConfig;

    /********************************************* service ********************************************/
    @Resource
    private ProductsService productsService;

    @Resource
    private InventoryService inventoryService;

    /**
     * 包装发送到rabbitmq的消息内容
     * @param messageName 操作名字
     * @param messageBody 数据
     * @return 返回处理后的mq消息内容
     */
    public JSONObject packageSendRabbitMqMsgData(JSONObject messageBody, String messageName) {
        JSONObject result = new JSONObject();
        result.put(RabbitMqConstants.MESSAGE_NAME, messageName);
        result.put(RabbitMqConstants.MESSAGE_BODY, messageBody);
        return result;
    }
}
