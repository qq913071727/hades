package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import com.innovation.ic.sc.base.pojo.variable.AdvantageBrandApiRespPojo;
import com.innovation.ic.sc.base.pojo.variable.AdvantageBrandRespPojo;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.IPagePojo;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.SpecialsScTopViewAuditPojo;
import com.innovation.ic.sc.base.pojo.variable.SpecialsScTopViewCloseDelPojo;
import com.innovation.ic.sc.base.vo.AdvantageBrandAddVo;
import com.innovation.ic.sc.base.vo.QueryAdvantageBrandAddVo;
import com.innovation.ic.sc.base.vo.UpdateAllAdvantageBrandVo;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @desc   advantage_brand的服务类接口
 * @author linuo
 * @time   2022年9月1日16:38:14
 */
public interface AdvantageBrandService {
    ServiceResult addAdvantageBrand(AdvantageBrandAddVo advantageBrandAddVo, AuthenticationUser authenticationUser) throws IOException;

    void deleteAdvantageBrand(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) throws IOException;

    void cancelAdvantageBrand(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) throws IOException;

    ServiceResult applicationForExtension(AdvantageBrandAddVo advantageBrandAddVo) throws IOException;

    ServiceResult reApply(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) throws IOException;

    ServiceResult<IPagePojo<AdvantageBrandRespPojo>> findByPage(QueryAdvantageBrandAddVo queryAdvantageBrandAddVo);

    void listenAddAdvantageBrand(SpecialsSCTopView specialsScTopView);

    void handleAdvantageModelAuditMqMsg(SpecialsScTopViewAuditPojo specialsScTopViewAuditPojo);

    void handleAdvantageModelCloseMqMsg(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo);

    void handleAdvantageModelDeleteMqMsg(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo);

    ServiceResult<List<AdvantageBrandApiRespPojo>> findAdvantageBrandUser(List<String> brands);

    void updateBrandAdvantageStatus();

    ServiceResult checkAdvantageBrand( String brand,AuthenticationUser authenticationUser, HttpServletRequest request);

    ServiceResult<List<AdvantageBrandApiRespPojo>> confirmPicture(List<String> pctureIds, Integer id) throws IOException;

    /**
     * 更新优势品牌生效状态
     * @param map 参数
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateStatusByParam(Map<String, Object> map) throws IOException;

    /**
     * 处理优势品牌通知的mq消息
     * @param brand 品牌
     * @param enterpriseId 供应商id
     * @param id erpId
     * @return 返回处理结果
     */
    ServiceResult<Boolean> handleAdvantageModelNotifyMqMsg(String brand, String enterpriseId, String id);

    //void listenAddAdvantageBrand(ErpAdvantageBrandVo erpAdvantageBrandVo);
}