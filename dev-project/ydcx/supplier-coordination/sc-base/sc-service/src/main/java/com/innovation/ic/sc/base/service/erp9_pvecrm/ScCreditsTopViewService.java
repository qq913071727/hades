package com.innovation.ic.sc.base.service.erp9_pvecrm;

import com.innovation.ic.sc.base.model.erp9_pvecrm.ScCreditsTopView;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 对账单视图
 * @Date 2022/12/20 9:29
 **/
public interface ScCreditsTopViewService {
    /**
     * 查找视图当天所有数据
     * @return
     */
    List<ScCreditsTopView> findAllBySameDay();
}
