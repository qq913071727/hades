package com.innovation.ic.sc.base.service.sc;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.*;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.ladderPrice.*;
import java.util.List;

/**
 * @desc   LadderPriceInfo的服务类接口
 * @author linuo
 * @time   2022年8月23日11:17:15
 */
public interface LadderPriceInfoService {

    /**
     * 阶梯价格新增
     * @param ladderPriceAddVo 阶梯价格新增接口的Vo类
     * @param authenticationUser 用户信息
     * @return 返回新增接口
     */
    ServiceResult<Boolean> add(LadderPriceAddVo ladderPriceAddVo, AuthenticationUser authenticationUser);

    /**
     * 阶梯价格信息查询
     * @param id 阶梯价格信息id
     * @return 返回阶梯价格查询结果
     */
    ServiceResult<LadderPriceInfoRespPojo> info(Integer id);

    /**
     * 按起订量配置的阶梯价格信息列表查询
     * @param epId 供应商id
     * @return 返回阶梯价格列表查询结果
     */
    ServiceResult<JSONObject> queryLadderPriceConfigListByMoq(String epId);

    /**
     * 按单价批量配置库存时的阶梯价格信息查询
     * @param ladderPriceBatchSetByAmountVo 通过单价批量配置阶梯价格的Vo类
     * @param epId 供应商id
     * @return 返回阶梯价格列表查询结果
     */
    ServiceResult<List<LadderPriceInfoBatchSetByAmountRespPojo>> queryBatchSetLadderPriceConfigListByAmount(LadderPriceBatchSetByAmountVo ladderPriceBatchSetByAmountVo, String epId);

    /**
     * 阶梯价格信息删除
     * @param id 阶梯价格信息id
     * @param epId 供应商id
     * @return 返回删除结果
     */
    ServiceResult<Integer> delete(Integer id, String epId);

    /**
     * 阶梯价格修改
     * @param ladderPriceEditVo 阶梯价格修改接口的Vo类
     * @param epId 供应商id
     * @return 返回修改结果
     */
    ServiceResult<Boolean> edit(LadderPriceEditVo ladderPriceEditVo, String epId);

    /**
     * 阶梯价格数据查询
     * @param ladderPriceGetDataVo 阶梯价格数据获取的Vo类
     * @param authenticationUser 用户信息
     * @return 返回查询结果
     */
    ServiceResult<List<LadderPriceDataRespPojo>> getLadderPriceData(LadderPriceGetDataVo ladderPriceGetDataVo, AuthenticationUser authenticationUser);

    /**
     * 判断阶梯价格是否被库存使用
     * @param id 阶梯价格id
     * @param epId 供应商id
     * @return 返回结果
     */
    ServiceResult<Boolean> judgeIfUsedByInventory(Integer id, String epId);

    /**
     * 按单价配置的阶梯价格信息列表查询
     * @param currency 币种(1:人民币、2:美元)
     * @param epId 供应商id
     * @return 返回查询结果
     */
    ServiceResult<JSONObject> queryLadderPriceConfigListByAmount(Integer currency, String epId);

    /**
     * 按起订量批量配置库存时的阶梯价格信息查询
     * @param idList id集合
     * @param epId 供应商id
     * @return 返回查询结果
     */
    ServiceResult<List<LadderPriceInfoBatchSetByMoqRespPojo>> queryBatchSetLadderPriceConfigListByMoq(List<Integer> idList, String epId);

    /**
     * 查询全部阶梯价格配置信息
     * @param epId 供应商id
     * @return 返回查询结果
     */
    ServiceResult<List<LadderPriceBasicInfoRespPojo>> queryAllLadderPriceInfo(String epId);
}