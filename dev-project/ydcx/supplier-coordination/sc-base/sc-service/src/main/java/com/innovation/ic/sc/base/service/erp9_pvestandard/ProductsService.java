package com.innovation.ic.sc.base.service.erp9_pvestandard;

import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import com.innovation.ic.sc.base.pojo.variable.product.ProductInfoPojo;
import java.util.List;

/**
 * @Description: 产品型号
 * @ClassName: ProductsService
 * @Author: myq
 * @Date: 2022/8/5 下午5:39
 * @Version: 1.0
 */
public interface ProductsService {
//    List<Products> selectProducts(List<String> productIds);
//
//    List<Products> selectProductsByBrands(List<String> brands);
//
//    ServiceResult<List<Products>> listByBrandsId(String brandsId, String productName);
//
//    List<Products> selectProductsByUserIds(Set<String> ids);

    /**
     * 根据型号、品牌查询产品信息
     * @param brand 型号
     * @param partNumber 品牌
     * @return 返回查询结果
     */
    List<ProductInfoPojo> selectProduceInfoByBrandPartNumber(String brand, String partNumber);

    /**
     * 模糊查询标准库型号
     * @param partNumber 型号
     * @return 返回查询结果
     */
    ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> blurQueryBzkPartNumberList(String partNumber);
}