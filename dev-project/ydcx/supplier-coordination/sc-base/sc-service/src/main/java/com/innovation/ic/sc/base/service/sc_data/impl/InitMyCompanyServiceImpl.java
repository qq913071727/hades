package com.innovation.ic.sc.base.service.sc_data.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.innovation.ic.sc.base.model.sc.Dictionaries;
import com.innovation.ic.sc.base.model.sc.MyCompany;
import com.innovation.ic.sc.base.model.sc.MyCompanyCurrency;
import com.innovation.ic.sc.base.model.sc.UploadFile;
import com.innovation.ic.sc.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.sc.base.pojo.enums.UploadFileEnum;
import com.innovation.ic.sc.base.pojo.variable.MyCompanyCollectionInformationPojo;
import com.innovation.ic.sc.base.pojo.variable.MyCompanyPojo;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.UploadFilePojo;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.DictionariesService;
import com.innovation.ic.sc.base.service.sc.MyCompanyCurrencyService;
import com.innovation.ic.sc.base.service.sc.MyCompanyService;
import com.innovation.ic.sc.base.service.sc.UploadFileService;
import com.innovation.ic.sc.base.service.sc_data.InitMyCompanyService;
import com.innovation.ic.sc.base.vo.SearchCompanyVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class InitMyCompanyServiceImpl implements InitMyCompanyService {

    @Resource
    private MyCompanyService myCompanyService;

    @Resource
    private DictionariesService dictionariesService;

    @Resource
    private UploadFileService uploadFileService;

    @Resource
    private ServiceHelper serviceHelper;


    @Resource
    private MyCompanyCurrencyService myCompanyCurrencyService;

    @Override
    public void initGetDetail() {
        //找到所有枚举
        Map<String, String> dictionariesMap = new HashMap<>();
        List<Dictionaries> dictionariesList = dictionariesService.findAll();
        if (!CollectionUtils.isEmpty(dictionariesList)) {
            for (Dictionaries dictionaries : dictionariesList) {
                dictionariesMap.put(dictionaries.getId(), dictionaries.getDName());
            }
        }

        Map<String, String> currencyMap = new HashMap<>();
        //找到币种
        List<MyCompanyCurrency> myCompanyCurrencyList = myCompanyCurrencyService.findAll();
        for (MyCompanyCurrency myCompanyCurrency : myCompanyCurrencyList) {
            currencyMap.put(myCompanyCurrency.getId() + "", myCompanyCurrency.getName());
        }

        SearchCompanyVo searchCompanyVo = null;
        Integer page = 0;
        Integer pageSize = 20;
        for (; ; ) {
            searchCompanyVo = new SearchCompanyVo();
            searchCompanyVo.setPage(++page);
            searchCompanyVo.setPageSize(pageSize);
            List<MyCompany> myCompanyList = myCompanyService.findByMyCompanyList(searchCompanyVo);
            if (CollectionUtils.isEmpty(myCompanyList)) {
                break;
            }
            for (MyCompany myCompany : myCompanyList) {
                this.assembleMyCompanyPojo(myCompany, dictionariesMap, currencyMap);
            }
        }
    }

    @Override
    public void savaRedistMyCompany(MyCompany myCompany) {
        //找到所有枚举
        Map<String, String> dictionariesMap = new HashMap<>();
        List<Dictionaries> dictionariesList = dictionariesService.findAll();
        if (!CollectionUtils.isEmpty(dictionariesList)) {
            for (Dictionaries dictionaries : dictionariesList) {
                dictionariesMap.put(dictionaries.getId(), dictionaries.getDName());
            }
        }
        //找到币种
        Map<String, String> currencyMap = new HashedMap<>();
        List<MyCompanyCurrency> myCompanyCurrencyList = myCompanyCurrencyService.findAll();
        for (MyCompanyCurrency myCompanyCurrency : myCompanyCurrencyList) {
            currencyMap.put(myCompanyCurrency.getId() + "", myCompanyCurrency.getName());
        }
        this.assembleMyCompanyPojo(myCompany, dictionariesMap, currencyMap);
    }

    /**
     * 组装数据并存入redis
     *
     * @param myCompany
     * @param dictionariesMap
     */
    private void assembleMyCompanyPojo(MyCompany myCompany, Map<String, String> dictionariesMap, Map<String, String> currencyMap) {
        MyCompanyPojo myCompanyPojo = new MyCompanyPojo();
        //copy对象
        BeanUtil.copyProperties(myCompany, myCompanyPojo);
        myCompanyPojo.setId(myCompany.getId());
        myCompanyPojo.setName(myCompany.getName());
        if (!dictionariesMap.isEmpty()) {
            myCompanyPojo.setCountryRegion(myCompany.getCountryRegionId());
            myCompanyPojo.setCountryRegionName(dictionariesMap.get(myCompany.getCountryRegionId()));

            myCompanyPojo.setCompanyType(myCompany.getCompanyTypeId());
            myCompanyPojo.setCompanyTypeName(dictionariesMap.get(myCompany.getCompanyTypeId()));


            myCompanyPojo.setInvoicType(myCompany.getInvoicTypeId());
            myCompanyPojo.setInvoicTypeName(dictionariesMap.get(myCompany.getInvoicTypeId()));

        }
        myCompanyPojo.setCurrencyName(currencyMap.get(myCompany.getCurrencyId()));
        //查找附件集合
        ServiceResult<List<UploadFile>> uploadFileResult = uploadFileService.findByRelationId(UploadFileEnum.MY_COMPANY, myCompany.getId() + "");
        if (!CollectionUtils.isEmpty(uploadFileResult.getResult())) {
            List<UploadFile> uploadFile = uploadFileResult.getResult();
            List<UploadFilePojo> uploadFilePojoList = new ArrayList<>();
            for (UploadFile file : uploadFile) {
                UploadFilePojo uploadFilePojo = new UploadFilePojo();
                uploadFilePojo.setFileName(file.getFileName());
                uploadFilePojo.setFullPath(file.getFullPath());
                uploadFilePojoList.add(uploadFilePojo);
            }
            //包装result
            myCompanyPojo.setFiles(uploadFilePojoList);
        }
        List<MyCompanyCollectionInformationPojo> myCompanyCollectionInformationPojos = new ArrayList<>();
        MyCompanyCollectionInformationPojo myCompanyCollectionInformationPojo = new MyCompanyCollectionInformationPojo();
        myCompanyCollectionInformationPojo.setId(myCompany.getId());
        myCompanyCollectionInformationPojo.setCurrency(myCompanyPojo.getCurrencyName());
        myCompanyCollectionInformationPojo.setReceivingCompany(myCompanyPojo.getReceivingCompany());
        myCompanyCollectionInformationPojo.setBankOfDeposit(myCompanyPojo.getBankOfDeposit());
        myCompanyCollectionInformationPojo.setAccountNumber(myCompanyPojo.getAccountNumber());
        myCompanyCollectionInformationPojo.setBankAddress(myCompanyPojo.getBankAddress());
        myCompanyCollectionInformationPojo.setSwiftCode(myCompanyPojo.getSwiftCode());
        myCompanyCollectionInformationPojo.setTransitBank(myCompanyPojo.getTransitBank());
        myCompanyCollectionInformationPojos.add(myCompanyCollectionInformationPojo);
        myCompanyPojo.setMyCompanyCollectionInformationPojo(myCompanyCollectionInformationPojos);
        ServiceResult<MyCompanyPojo> serviceResult = new ServiceResult<>();
        serviceResult.setResult(myCompanyPojo);
        serviceResult.setSuccess(Boolean.TRUE);
        String key = RedisKeyPrefixEnum.MY_COMPANY_GET_DETAIL.getCode() + "_" + myCompany.getId();
        String apiResultJson = JSONObject.toJSONString(serviceResult, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
        serviceHelper.getRedisManager().set(key, apiResultJson);
    }


}
