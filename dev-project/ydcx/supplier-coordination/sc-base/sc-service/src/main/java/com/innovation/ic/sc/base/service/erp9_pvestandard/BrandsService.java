package com.innovation.ic.sc.base.service.erp9_pvestandard;

import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;

import java.util.List;

//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import java.util.List;
//import java.util.Set;
//
///**
// * @Description: 品牌型号
// * @ClassName: BrandsService
// * @Author: myq
// * @Date: 2022/8/5 下午5:35
// * @Version: 1.0
// */
public interface BrandsService {
//    List<Brands> selectBrands(int pageNo, int pageSize, String searchName);
//
//    List<Brands> selectBrands(String searchName);
//
//    ServiceResult<List<Brands>> list(String searchName);
//
//    ServiceResult<IPage<Brands>> page(int pageNo, int pageSize, String searchName);
//
//
//    ServiceResult<IPage<Brands>> findByPage(int pageNo, int pageSize, String searchName);
//
//
//    List<Brands> selectByProductIds(Set<String> brandIds);


    List<Brands> selectAllBrands();
}