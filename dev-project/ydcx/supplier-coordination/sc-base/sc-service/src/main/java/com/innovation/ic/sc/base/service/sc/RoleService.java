package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.Role;
import com.innovation.ic.sc.base.pojo.variable.PageNationPojo;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.menu.RolePojo;
import com.innovation.ic.sc.base.vo.menu.RoleVo;

import java.util.List;

/**
 * Role的接口类
 */
public interface RoleService {

    /**
     * 查找所有角色
     *
     * @return
     */
    ServiceResult<List<Role>> findAllByUserId(String userMainId);

    /**
     * 根据id，改变角色的状态
     *
     * @param id
     * @return
     */
    ServiceResult<Boolean> changeAvailableStatus(String id, Integer status);

    /**
     * 插入Role对象和对应的Menu对象
     *
     * @param role
     * @return
     */
    ServiceResult<String> saveRole(Role role);

    /**
     * 主账号修改角色名称和对应的菜单权限时，返回菜单树形目录（不包括账号管理和角色管理），其中有权限的菜单项被选中
     *
     * @param role
     * @return
     */
    ServiceResult<Role> showMenuForUpdateRole(RoleVo role);

    /**
     * 系统管理-职位管理-查询：查询角色名称和对应的菜单权限时，返回角色和对应的菜单树形目录（不包括账号管理和角色管理），其中有权限的菜单项被选中
     *
     * @param roleId
     * @return
     */
    ServiceResult<Role> showMenuForRoleById(String roleId);

    /**
     * 根据name查找角色
     *
     * @param name
     * @return
     */
    ServiceResult<Role> findByName(String name, String creatorId);

    /**
     * 分页查询
     *
     * @param userMainId
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageNationPojo<RolePojo> pageFindAllByUserId(String userMainId, Integer pageNo, Integer pageSize);

    /**
     * 进行用户与角色关联
     *
     * @param roleListId
     * @param id
     * @return
     */
    void saveUserToRole(List<String> roleListId, String id,String mainId, Integer isManager);

    /**
     * 初始化角色所有数据
     * @return
     */
    void initRoleData();

//    void saveUserToRole(List<RolePojo> roleList,String id);
}
