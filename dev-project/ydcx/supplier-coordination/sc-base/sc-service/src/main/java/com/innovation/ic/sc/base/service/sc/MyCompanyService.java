package com.innovation.ic.sc.base.service.sc;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.sc.base.model.sc.MyCompany;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.pojo.variable.MyCompanyPojo;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.*;

import java.io.IOException;
import java.util.List;

public interface MyCompanyService {



    ServiceResult registerCompany(MyCompanyVo myCompanyVo) throws IOException;




    ServiceResult<MyCompanyPojo> getDetail(Integer id);

    ServiceResult delete(String ids);



    ServiceResult checkRegistrationCompany(MyCompanyVo myCompanyVo);

    ServiceResult<PageInfo<MyCompanyPojo>> findByMyCompany(SearchCompanyVo searchCompanyVo);

    ServiceResult deactivateMyCompany(Integer relationEnterpriseId) throws IOException;

    void relationMyCompanyNameVo(RelationMyCompanyNameVo relationMyCompanyNameVo) throws IOException;

    void listenAuditErpCompany(RelationMyCompanyNameVo relationMyCompanyNameVo) throws IOException;

    void listenDeleteErpCompany(RelationMyCompanyNameVo relationMyCompanyNameVo) throws IOException;

    void addUserRelation(User userdata);

    void updateMyCompanyExternalId(ErpMyCompanyNameVo erpMyCompanyNameVo);

    MyCompany findByName(String Name);

    List<MyCompany> findByMyCompanyList(SearchCompanyVo searchCompanyVo);

    ServiceResult<MyCompanyPojo> getRedisDetail(Integer relationEnterpriseId);

    /**
     * 根据creator_id查找我的公司名称列表
     * @param creatorId
     * @return
     */
    ServiceResult<List<String>> findByEpId(String creatorId);

    void ListenAddCompanyBookaccount(ErpBookAccountVo erpBookAccountVo);

    ServiceResult updateBookAccount(MyCompanyVo myCompanyVo) throws IOException;

    void ListenAddBookaccountSuccess(ErpBookAccountVo erpBookAccountVo);

    void ListenAddBookaccountClose(ErpBookAccountVo erpBookAccountVo);


    ServiceResult updateMyCompany(MyCompanyVo myCompanyVo) throws IOException;
}
