package com.innovation.ic.sc.base.service.sc;


import com.innovation.ic.sc.base.model.sc.DictionariesType;
import com.innovation.ic.sc.base.vo.DictionariesTypeVo;
import com.innovation.ic.sc.base.vo.ErpDictionariesVo;

import java.util.HashSet;
import java.util.List;

public interface DictionariesTypeService {

    void saveDictionariesType(DictionariesTypeVo dictionariesTypeVo);


    void listenAddDistrictType(ErpDictionariesVo erpDictionariesVo);

    void deleteBatchIds(HashSet<String> dictionariesTypeIdS);

    void insertBatchSomeColumn(List<DictionariesType> dtList);

    DictionariesType selectById(String id);
}
