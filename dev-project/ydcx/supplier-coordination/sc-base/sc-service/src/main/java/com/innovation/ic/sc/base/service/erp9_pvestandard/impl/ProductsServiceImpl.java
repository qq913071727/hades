package com.innovation.ic.sc.base.service.erp9_pvestandard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.erp9_pvestandard.ProductsMapper;
import com.innovation.ic.sc.base.model.erp9_pvestandard.Products;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import com.innovation.ic.sc.base.pojo.variable.product.ProductInfoPojo;
import com.innovation.ic.sc.base.service.erp9_pvestandard.ProductsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @Description: 产品型号实现类
 * @ClassName: ProductsServiceImpl
 * @Author: myq
 * @Date: 2022/8/5 下午5:39
 * @Version: 1.0
 */
@Service
@Slf4j
public class ProductsServiceImpl extends ServiceImpl<ProductsMapper,Products> implements ProductsService {
    @Autowired
    private ProductsMapper productsMapper;

    /**
     * 根据型号、品牌查询产品信息
     * @param brand 型号
     * @param partNumber 品牌
     * @return 返回查询结果
     */
    @Override
    public List<ProductInfoPojo> selectProduceInfoByBrandPartNumber(String brand, String partNumber) {
        return productsMapper.selectProduceInfoByBrandPartNumber(brand, partNumber);
    }

    /**
     * 模糊查询标准库型号
     * @param partNumber 型号
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> blurQueryBzkPartNumberList(String partNumber) {
        String message = ServiceResult.SELECT_FAIL;

        List<InventoryBlurQueryBzkPartNumberListRespPojo> result = productsMapper.blurQueryBzkPartNumberList("%" + partNumber + "%");
        if(result != null && result.size() > 0){
            message = ServiceResult.SELECT_SUCCESS;
        }

        ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(message);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }


    /**
     * @description 查询所有产品型号
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/5 下午2:16
     *//*
    @Override
    public List<Products> selectProducts(List<String> productIds) {
        return  productsMapper.selectProducts(productIds);
    }

    *//**
     * @description 根据品牌ids查询产品列表
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/8 下午3:41
     *//*
    @Override
    public List<Products> selectProductsByBrands(List<String> brands) {
        LambdaQueryWrapper<Products> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(Products::getBrandID,brands);
        return baseMapper.selectList(wrapper);
    }

    *//**
     * @description 根据品牌ID查询产品型号列表
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/9 上午11:34
     *//*
    @Override
    public ServiceResult<List<Products>> listByBrandsId(String brandsId, String productName) {
        LambdaQueryWrapper<Products> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Products::getBrandID,brandsId);
        if(StringUtils.validateParameter(productName)){
            wrapper.likeRight(Products::getPartNumber,productName);
        }
        return ServiceResult.ok(baseMapper.selectList(wrapper),"根据品牌ID查询产品型号列表成功");
    }

    *//**
     * @description 根据用户品牌关系查询产品列表
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/9 下午3:57
     *//*
    @Override
    public List<Products> selectProductsByUserIds(Set<String> ids) {
        return CollectionUtils.isEmpty(ids) ? Collections.emptyList(): baseMapper.selectBatchIds(ids);
    }*/
}