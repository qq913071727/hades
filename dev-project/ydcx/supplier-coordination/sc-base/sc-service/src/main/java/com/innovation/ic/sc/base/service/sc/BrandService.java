package com.innovation.ic.sc.base.service.sc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;
import com.innovation.ic.sc.base.model.sc.Brand;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.BrandsPageVo;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌
 * @Date 2022/11/18 13:50
 **/
public interface BrandService {
    /**
     * 分页查询品牌
     * @param pageNo
     * @param pageSize
     * @param searchName
     * @return
     */
    ServiceResult<PageInfo<Brand>> pageBrand(Integer pageNo, Integer pageSize, String searchName);

    /**
     * 根据品牌名称模糊查询品牌
     * @param pageNo
     * @param pageSize
     * @param searchName
     * @return
     */
    ServiceResult<PageInfo<Brand>> pageFindBySearchNameBrand(Integer pageNo, Integer pageSize, String searchName);

    /**
     * 查询品牌列表
     * @param searchName
     * @return
     */
    ServiceResult<List<Brand>> list(String searchName);


    void insertBatch(List<Brands> erpBrands);


    void truncate();

    /**
     * 查询数据库所以品牌
     * @return
     */
    ServiceResult<List<Brand>> findAll();

    /**
     * 分页查询数据 右匹配
     * @param brandsPageVo
     * @return
     */
    ServiceResult<PageInfo<Brand>> pageBrandRedis(BrandsPageVo brandsPageVo);

    /**
     * 分页查询数据 全匹配
     * @param brandsPageVo
     * @return
     */
    ServiceResult<PageInfo<Brand>> pageFindBySearchNameBrandRedis(BrandsPageVo brandsPageVo);

    /**
     * 查询数据 右匹配
     * @param searchName
     * @return
     */
    ServiceResult<List<Brand>> listRedis(String searchName);
}
