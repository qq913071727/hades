package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.Product;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 产品
 * @Date 2022/11/18 13:51
 **/
public interface ProductService {
    /**
     * 根据品牌ID查询产品列表
     *
     * @param brandsId
     * @param productName
     * @return
     */
    ServiceResult<List<Product>> listByBrandsId(String brandsId, String productName);
}
