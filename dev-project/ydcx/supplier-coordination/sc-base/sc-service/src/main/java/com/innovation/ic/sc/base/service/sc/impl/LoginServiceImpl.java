package com.innovation.ic.sc.base.service.sc.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.b1b.framework.util.MiscUtil;
import com.innovation.ic.sc.base.handler.sc.*;
import com.innovation.ic.sc.base.mapper.sc.MyCompanyMapper;
import com.innovation.ic.sc.base.mapper.sc.SmsInfoMapper;
import com.innovation.ic.sc.base.mapper.sc.UserMapper;
import com.innovation.ic.sc.base.model.sc.SmsInfo;
import com.innovation.ic.sc.base.pojo.constant.CodeConstants;
import com.innovation.ic.sc.base.pojo.constant.Constants;
import com.innovation.ic.sc.base.pojo.constant.LoginConstants;
import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.enums.LoginTypeEnum;
import com.innovation.ic.sc.base.pojo.enums.SmsStatusEnum;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.LoginService;
import com.innovation.ic.sc.base.value.LoginConfig;
import com.innovation.ic.sc.base.value.RedisParamConfig;
import com.innovation.ic.sc.base.value.SmsParamConfig;
import com.innovation.ic.sc.base.vo.login.BindThirdAccountVo;
import com.innovation.ic.sc.base.vo.login.SmsLoginVo;
import com.innovation.ic.sc.base.vo.UsersVo;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * @desc   loginService的实现类
 * @author linuo
 * @time   2022年8月8日16:04:24
 */
@Service
@Transactional
public class LoginServiceImpl implements LoginService {
    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 通过短信验证码登录
     * @param smsLoginVo 短信验证码登录参数
     * @return 返回登录结果
     */
    @Override
    public ServiceResult<JSONObject> loginBySms(SmsLoginVo smsLoginVo) throws Exception {
        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        Boolean success = Boolean.FALSE;
        JSONObject result = null;
        String message = null;

        // 调用erp9接口进行登录操作
        JSONObject param = (JSONObject) JSON.toJSON(smsLoginVo);
        String bandResult = HttpUtils.sendPost(serviceHelper.getLoginConfig().getErpSmsLoginUrl(), param.toString());
        if(bandResult != null) {
            JSONObject jsonObject = (JSONObject) JSONObject.parse(bandResult);
            Integer status = jsonObject.getInteger(LoginConstants.STATUS_FIELD);
            String data = jsonObject.getString(LoginConstants.DATA);
            Boolean successResult = jsonObject.getBoolean(LoginConstants.SUCCESS_FIELD);

            // 登录成功
            if(status == null && successResult){
                success = Boolean.TRUE;
                message = "登录成功";
                String token = (String) jsonObject.get(LoginConstants.TOKEN);
                if(!Strings.isNullOrEmpty(token)){
                    // 通过token创建登录返回结果
                    result = createLoginResultByToken(token);
                }
            }

            // 登录失败
            if(status != null && (status.toString().equals(String.valueOf(HttpStatus.SC_INTERNAL_SERVER_ERROR)) || status.toString().equals(String.valueOf(CodeConstants.UN_BOUNDED)))){
                if(!Strings.isNullOrEmpty(data)){
                    message = data;
                }else{
                    message = jsonObject.getString(LoginConstants.MSG);
                }

                result = new JSONObject();
                result.put(LoginConstants.STATUS, status);
            }
        }

        serviceResult.setSuccess(success);
        serviceResult.setMessage(message);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 获取支付宝登录授权地址
     * @return 返回支付宝登录授权地址
     */
    @Override
    public ServiceResult<String> getAlipayAuthUrl(String redirectUrl) throws UnsupportedEncodingException {
        ServiceResult<String> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);

        // 对回调地址进行加密
        String encodeStr = URLEncoder.encode(redirectUrl,"UTF-8");

        serviceResult.setResult(AlipayHandler.getAuthUrl(serviceHelper.getLoginConfig().getGetAuthUrl(), encodeStr));
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 获取微信登录授权地址
     * @return 返回微信登录授权地址
     */
    @Override
    public ServiceResult<String> getWechatAuthUrl(String redirectUrl) throws UnsupportedEncodingException {
        ServiceResult<String> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);

        // 对回调地址进行加密
        String encodeStr = URLEncoder.encode(redirectUrl,"UTF-8");

        serviceResult.setResult(WechatHandler.getAuthUrl(serviceHelper.getLoginConfig().getGetAuthUrl(), encodeStr));
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 获取QQ登录授权地址
     * @param redirectUrl 回调地址
     * @return 返回QQ登录授权地址
     */
    @Override
    public ServiceResult<String> getQQAuthUrl(String redirectUrl) throws UnsupportedEncodingException {
        ServiceResult<String> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);

        // 对回调地址进行加密
        String encodeStr = URLEncoder.encode(redirectUrl,"UTF-8");

        serviceResult.setResult(QQHandler.getAuthUrl(serviceHelper.getLoginConfig().getGetAuthUrl(), encodeStr));
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 微信登录
     * @param authCode 临时授权码
     * @return 返回登录结果及登录信息
     */
    @Override
    public ServiceResult<JSONObject> loginByWechat(String authCode) {
        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        Boolean success = Boolean.FALSE;
        JSONObject result = new JSONObject();
        String message = null;

        // 通过临时授权码获取用户个人信息
        JSONObject jsonObject = ErpHandler.loginByAuthCode(serviceHelper.getLoginConfig().getLoginUrl(), authCode, LoginTypeEnum.WECHAT.getCode());
        if(jsonObject != null){
            Integer status = jsonObject.getInteger(LoginConstants.STATUS_FIELD);

            // 登录成功
            if(status != null && status == HttpStatus.SC_OK){
                success = Boolean.TRUE;

                JSONObject data = (JSONObject) jsonObject.get(LoginConstants.RESULT);
                if (data != null) {
                    String token = (String) data.get(LoginConstants.TOKEN);
                    // 通过token创建登录返回结果
                    result = createLoginResultByToken(token);
                }
            }

            // 登录失败
            if(status != null && status == HttpStatus.SC_INTERNAL_SERVER_ERROR){
                message = jsonObject.getString(LoginConstants.MESSAGE);
                result.put(LoginConstants.STATUS, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }

            // 账号未绑定
            if(status != null && status == CodeConstants.UN_BOUNDED){
                message = jsonObject.getString(LoginConstants.MESSAGE);
                result.put(LoginConstants.OPEN_ID, jsonObject.getString(LoginConstants.OPEN_ID));
                result.put(LoginConstants.UNION_ID, jsonObject.getString(LoginConstants.UNION_ID));
                result.put(LoginConstants.STATUS, CodeConstants.UN_BOUNDED);
            }
        }

        serviceResult.setSuccess(success);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * QQ登录
     * @param authCode 临时授权码
     * @return 返回登录结果及登录信息
     */
    @Override
    public ServiceResult<JSONObject> loginByQQ(String authCode) {
        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        Boolean success = Boolean.FALSE;
        JSONObject result = new JSONObject();
        String message = null;

        // 通过临时授权码获取用户个人信息
        JSONObject jsonObject = ErpHandler.loginByAuthCode(serviceHelper.getLoginConfig().getLoginUrl(), authCode, LoginTypeEnum.QQ.getCode());
        if(jsonObject != null){
            Integer status = jsonObject.getInteger(LoginConstants.STATUS_FIELD);

            // 登录成功
            if(status != null && status == HttpStatus.SC_OK){
                success = Boolean.TRUE;

                JSONObject data = (JSONObject) jsonObject.get(LoginConstants.RESULT);
                if (data != null) {
                    String token = (String) data.get(LoginConstants.TOKEN);
                    // 通过token创建登录返回结果
                    result = createLoginResultByToken(token);
                }
            }

            // 登录失败
            if(status != null && status == HttpStatus.SC_INTERNAL_SERVER_ERROR){
                message = jsonObject.getString(LoginConstants.MESSAGE);
                result.put(LoginConstants.STATUS, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }

            // 账号未绑定
            if(status != null && status == CodeConstants.UN_BOUNDED){
                message = jsonObject.getString(LoginConstants.MESSAGE);
                result.put(LoginConstants.OPEN_ID, jsonObject.getString(LoginConstants.OPEN_ID));
                result.put(LoginConstants.STATUS, CodeConstants.UN_BOUNDED);
            }
        }

        serviceResult.setSuccess(success);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 通过token创建登录返回结果
     * @param token token
     * @return 返回登录结果
     */
    private JSONObject createLoginResultByToken(String token){
        if(Strings.isNullOrEmpty(token)){
            return new JSONObject();
        }

        // 将token对象转换为接收用户信息vo类对象
        AuthenticationUser authenticationUser = AuthenticationUserHandler.toAuthenticationUser(token);

        // 我的公司名称列表
        List<String> myCompanyNameListServiceResult = findMyCompanyNameListByCreatorId(authenticationUser.getId());
        if (null != myCompanyNameListServiceResult && myCompanyNameListServiceResult.size() > 0) {
            authenticationUser.setMyCompanyNameList(myCompanyNameListServiceResult);
        }

        // 查询账号评分
        Float score = serviceHelper.getUserMapper().getScoreById(authenticationUser.getId());
        if(score == null){
            score = 0F;
        }
        authenticationUser.setScore(score);

        // 将用户信息存储到redis中
        this.setAuthenticationUser(authenticationUser);

        return (JSONObject) JSON.toJSON(authenticationUser);
    }

    /**
     * 根据creator_id查找我的公司名称列表
     * @param creatorId 创建人id
     * @return 返回查询结果
     */
    public List<String> findMyCompanyNameListByCreatorId(String creatorId) {
        return serviceHelper.getMyCompanyMapper().findByEpId(creatorId);
    }

    /**
     * 绑定第三方账户
     * @param bindThirdAccountVo 绑定第三方账号的Vo类
     * @return 返回绑定结果
     */
    @Override
    public ServiceResult<JSONObject> bindThirdAccount(BindThirdAccountVo bindThirdAccountVo) throws IOException {
        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        Boolean success = Boolean.FALSE;
        JSONObject result = new JSONObject();
        String message = null;

        // 调用erp接口进行第三方账户绑定操作
        JSONObject param = (JSONObject) JSON.toJSON(bindThirdAccountVo);
        String bandResult = HttpUtils.sendPost(serviceHelper.getLoginConfig().getBandUrl(), param.toString());
        if(bandResult != null){
            JSONObject parse = (JSONObject) JSONObject.parse(bandResult);
            if(parse != null){
                Integer status = parse.getInteger(LoginConstants.STATUS_FIELD);

                // 成功
                if(status == HttpStatus.SC_OK){
                    message = "绑定成功";
                    success = Boolean.TRUE;

                    // 调用ERP登录接口进行登录操作
                    JSONObject jsonObject = loginThroughErp(bindThirdAccountVo);
                    if(jsonObject != null){
                        result = jsonObject.getJSONObject(Constants.RESULT);
                    }else{
                        message = "绑定失败,请重试";
                    }
                }

                // 将失败信息返回给前端响应
                if(status == HttpStatus.SC_INTERNAL_SERVER_ERROR){
                    message = "绑定失败,请重试";
                    String msg = parse.getString(LoginConstants.MSG);
                    if(!Strings.isNullOrEmpty(msg)){
                        message = msg;
                    }
                }
            }
        }

        serviceResult.setSuccess(success);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 解绑第三方账户
     * @param type 解绑账号类型(0 微信、1 QQ、3 支付宝)
     * @param userId 用户id
     * @return 返回解绑结果
     */
    @Override
    public ServiceResult<JSONObject> unBindThirdAccount(String type, String userId) {
        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        Boolean success = Boolean.FALSE;
        JSONObject result = new JSONObject();
        String message = null;

        // 调用erp接口进行第三方账户解绑操作
        JSONObject param = new JSONObject();
        param.put(LoginConstants.TYPE, type);
        param.put(LoginConstants.USER_ID_FIELD, userId);
        String bandResult = HttpUtils.sendPost(serviceHelper.getLoginConfig().getUnBandUrl(), param.toString());
        if(bandResult != null){
            JSONObject parse = (JSONObject) JSONObject.parse(bandResult);
            if(parse != null){
                Integer status = parse.getInteger(LoginConstants.STATUS_FIELD);
                result.put(LoginConstants.STATUS_FIELD, status);

                // 成功
                if(status == HttpStatus.SC_OK){
                    message = "解绑成功";
                    success = Boolean.TRUE;
                }else{
                    message = "绑定失败,请重试";
                }
            }
        }

        serviceResult.setSuccess(success);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 调用ERP登录接口进行登录操作
     * @param bindThirdAccountVo 绑定第三方账号的Vo类
     * @return 返回登录用户信息
     */
    private JSONObject loginThroughErp(BindThirdAccountVo bindThirdAccountVo) throws IOException {
        UsersVo usersVo = new UsersVo();
        usersVo.setUserName(bindThirdAccountVo.getUserName());
        usersVo.setPassword(bindThirdAccountVo.getPassWord());
        String result = HttpUtils.doPostTestTwo(serviceHelper.getLoginConfig().getUrl(), JSON.toJSONString(usersVo));
        if(!Strings.isNullOrEmpty(result)) {
            JSONObject json = (JSONObject) JSONObject.parse(result);
            Integer status = json.getInteger(LoginConstants.STATUS_FIELD);
            // 成功
            if(status == HttpStatus.SC_OK) {
                JSONObject jsonObject = new JSONObject();
                JSONObject data = (JSONObject) json.get(LoginConstants.DATA);
                if (data != null) {
                    String token = (String) data.get(LoginConstants.TOKEN);
                    // 将token对象转换为接收用户信息vo类对象
                    AuthenticationUser authenticationUser = AuthenticationUserHandler.toAuthenticationUser(token);
                    if (authenticationUser != null) {

                        // 将用户信息存储到redis中
                        this.setAuthenticationUser(authenticationUser);

                        // 查询账号评分
                        Float score = serviceHelper.getUserMapper().getScoreById(authenticationUser.getId());
                        if(score == null){
                            score = 0F;
                        }
                        authenticationUser.setScore(score);
                        jsonObject.put(Constants.RESULT, JSON.toJSON(authenticationUser));
                    }
                    jsonObject.put(LoginConstants.STATUS_FIELD, status);
                }
                return jsonObject;
            }
        }
        return null;
    }

    /**
     * 将AuthenticationUser对象存储到redis中，key为token，value是json字符串
     * @param authenticationUser 接收用户信息vo类
     */
    protected void setAuthenticationUser(AuthenticationUser authenticationUser) {
        serviceHelper.getRedisManager().set(RedisStorage.TOKEN_PREFIX + authenticationUser.getToken(), JSON.toJSONString(authenticationUser));
        serviceHelper.getRedisManager().expire(RedisStorage.TOKEN_PREFIX + authenticationUser.getToken(), serviceHelper.getRedisParamConfig().getTokenTimeout());
    }

    /**
     * 支付宝登录
     * @param authCode 临时授权码
     * @return 返回登录结果及登录信息
     */
    @Override
    public ServiceResult<JSONObject> loginByAlipay(String authCode) {
        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        Boolean success = Boolean.FALSE;
        JSONObject result = new JSONObject();
        String message = null;

        // 通过临时授权码获取用户个人信息
        JSONObject jsonObject = ErpHandler.loginByAuthCode(serviceHelper.getLoginConfig().getLoginUrl(), authCode, LoginTypeEnum.ALIPAY.getCode());
        if(jsonObject != null){
            Integer status = jsonObject.getInteger(LoginConstants.STATUS_FIELD);

            // 登录成功
            if(status != null && status == HttpStatus.SC_OK){
                success = Boolean.TRUE;

                JSONObject data = (JSONObject) jsonObject.get(LoginConstants.RESULT);
                if (data != null) {
                    String token = (String) data.get(LoginConstants.TOKEN);
                    // 通过token创建登录返回结果
                    result = createLoginResultByToken(token);
                }
            }

            // 登录失败
            if(status != null && status == HttpStatus.SC_INTERNAL_SERVER_ERROR){
                message = jsonObject.getString(LoginConstants.MESSAGE);
                result.put(LoginConstants.STATUS, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }

            // 账号未绑定
            if(status != null && status == CodeConstants.UN_BOUNDED){
                message = jsonObject.getString(LoginConstants.MESSAGE);
                result.put(LoginConstants.OPEN_ID, jsonObject.getString(LoginConstants.OPEN_ID));
                result.put(LoginConstants.STATUS, CodeConstants.UN_BOUNDED);
            }
        }

        serviceResult.setSuccess(success);
        serviceResult.setResult(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 校验短信验证码
     * @param mobile 手机号
     * @param smsCode 短信验证码
     */
    private JSONObject checkSmsCode(String mobile, String smsCode){
        JSONObject json = new JSONObject();

        String message;

        // 查询最近一条短信验证码信息
        List<SmsInfo> smsInfos = serviceHelper.getSmsInfoMapper().selectSmsInfoList(mobile);
        if(smsInfos == null || smsInfos.size() == 0){
            message = "未查询到短信验证码,请重新获取";
            logger.warn(message);
            json.put(Constants.RESULT, Boolean.FALSE);
            json.put(LoginConstants.MESSAGE, message);
            return json;
        }

        // 取出时间最近一次的排序
        SmsInfo smsInfo = smsInfos.get(0);

        // 判断验证次数
        if (smsInfo.getVerifyTimes() >= serviceHelper.getSmsParamConfig().getValidCount()) {
            // 更新短信验证码验证次数及状态
            updateSmsVerifyTimes(smsInfo.getId(), smsInfo.getVerifyTimes() + 1, SmsStatusEnum.CHECK_FALSE.getCode());

            message = "短信验证码超出最大验证次数";
            logger.warn(message);
            json.put(Constants.RESULT, Boolean.FALSE);
            json.put(LoginConstants.MESSAGE, message);
            return json;
        }

        // 判断短信是否超时
        boolean checkTime = MiscUtil.checkValidTime(smsInfo.getCreateTime(), new Date(System.currentTimeMillis()), serviceHelper.getSmsParamConfig().getValidTime());
        if (!checkTime) {
            // 更新短信验证码验证次数及状态
            updateSmsVerifyTimes(smsInfo.getId(), smsInfo.getVerifyTimes() + 1, SmsStatusEnum.CHECK_FALSE.getCode());
            message = "短信验证码已失效,请重新获取";
            logger.warn(message);
            json.put(Constants.RESULT, Boolean.FALSE);
            json.put(LoginConstants.MESSAGE, message);
            return json;
        }

        // 判断短信验证码是否正确
        if(!smsCode.equals(smsInfo.getSmsCode())){
            // 更新短信验证码验证次数及状态
            updateSmsVerifyTimes(smsInfo.getId(), smsInfo.getVerifyTimes() + 1, SmsStatusEnum.CHECK_FALSE.getCode());
            message = "短信验证码错误";
            logger.warn(message);
            json.put(Constants.RESULT, Boolean.FALSE);
            json.put(LoginConstants.MESSAGE, message);
            return json;
        }

        updateSmsVerifyTimes(smsInfo.getId(), smsInfo.getVerifyTimes() + 1, SmsStatusEnum.CHECK_SUCCESS.getCode());
        json.put(Constants.RESULT, Boolean.TRUE);
        logger.info("短信验证码校验通过");
        return json;
    }

    /**
     * 更新短信验证码验证次数及状态
     * @param id 主键id
     * @param verifyTimes 验证次数
     * @param smsStatus 验证状态
     */
    private void updateSmsVerifyTimes(Integer id, int verifyTimes, Integer smsStatus) {
        SmsInfo smsInfo = new SmsInfo();
        smsInfo.setVerifyTimes(verifyTimes);
        smsInfo.setSmsStatus(smsStatus);

        UpdateWrapper<SmsInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("ID", id);
        int update = serviceHelper.getSmsInfoMapper().update(smsInfo, updateWrapper);
        if(update > 0){
            logger.info("更新id为[{}]的短信验证码数据成功,更新后验证次数为[{}],验证结果为[{}]", id, verifyTimes, SmsStatusEnum.getDesc(smsStatus));
        }
    }
}