package com.innovation.ic.sc.base.service.sc.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.b1b.framework.util.XmlUtil;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.mapper.sc.BrandMapper;
import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;
import com.innovation.ic.sc.base.model.sc.Brand;
import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.BrandService;
import com.innovation.ic.sc.base.vo.BrandsPageVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌
 * @Date 2022/11/18 13:51
 **/
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 分页查询品牌
     *
     * @param pageNo
     * @param pageSize
     * @param searchName
     * @return
     */
    @Override
    public ServiceResult<PageInfo<Brand>> pageBrand(Integer pageNo, Integer pageSize, String searchName) {
        PageHelper.startPage(pageNo, pageSize);
        LambdaQueryWrapper<Brand> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
        brandLambdaQueryWrapper.likeRight(StringUtils.validateParameter(searchName), Brand::getName, searchName);
        brandLambdaQueryWrapper.orderByDesc(Brand::getCreateDate);
        List<Brand> list = baseMapper.selectList(brandLambdaQueryWrapper);
        return ServiceResult.ok(new PageInfo<>(list), "查询品牌列表数据成功");
    }

    /**
     * 根据品牌名称模糊查询品牌
     *
     * @param pageNo
     * @param pageSize
     * @param searchName
     * @return
     */
    @Override
    public ServiceResult<PageInfo<Brand>> pageFindBySearchNameBrand(Integer pageNo, Integer pageSize, String searchName) {
        PageHelper.startPage(pageNo, pageSize);
        LambdaQueryWrapper<Brand> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
        brandLambdaQueryWrapper.like(StringUtils.validateParameter(searchName), Brand::getName, searchName);
        brandLambdaQueryWrapper.orderByDesc(Brand::getCreateDate);
        List<Brand> list = baseMapper.selectList(brandLambdaQueryWrapper);
        return ServiceResult.ok(new PageInfo<>(list), "查询品牌列表数据成功");
    }

    /**
     * 查询品牌列表
     *
     * @param searchName
     * @return
     */
    @Override
    public ServiceResult<List<Brand>> list(String searchName) {
        LambdaQueryWrapper<Brand> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
        brandLambdaQueryWrapper.likeRight(StringUtils.isNotEmpty(searchName), Brand::getCnName, searchName);
        brandLambdaQueryWrapper.orderByDesc(Brand::getId);
        return ServiceResult.ok(baseMapper.selectList(brandLambdaQueryWrapper), "查询品牌列表数据成功");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertBatch(List<Brands> erpBrands) {
        //清空表
        List<Brand> brandList = new ArrayList<>();
        for (Brands erpBrand : erpBrands) {
            Brand brand = new Brand();
            BeanUtils.copyProperties(erpBrand, brand);
            brand.setName(erpBrand.getName());
            brandList.add(brand);
        }
        saveBatch(brandList, 1000);
    }

    @Override
    public void truncate() {
        //清空表
        this.baseMapper.truncate();
    }

    /**
     * 查询数据库所有品牌
     * @return
     */
    @Override
    public ServiceResult<List<Brand>> findAll() {
        LambdaQueryWrapper<Brand> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
        return ServiceResult.ok(baseMapper.selectList(brandLambdaQueryWrapper), "查询数据库所有品牌成功");
    }

    /**
     * 分页查询数据
     * @param brandsPageVo
     * @return
     */
    @Override
    public ServiceResult<PageInfo<Brand>> pageBrandRedis(BrandsPageVo brandsPageVo) {
        String field = RedisStorage.TABLE + RedisStorage.BRAND +
                RedisStorage.CREATE_DATE_DESC;
        Brand brand = new Brand();
        brand.setName(brandsPageVo.getSearchName());
        //右模糊查询
        String pattern = serviceHelper.getModelHandler().brandToXmlFilterString(brand, false);
        List list = serviceHelper.getRedisManager().pageSort(field, pattern, true);
        return ServiceResult.ok(listPage(list, brandsPageVo.getPageNo(), brandsPageVo.getPageSize()), "查询品牌列表数据成功");
    }

    /**
     * 分页查询数据 全匹配
     *
     * @param brandsPageVo
     * @return
     */
    @Override
    public ServiceResult<PageInfo<Brand>> pageFindBySearchNameBrandRedis(BrandsPageVo brandsPageVo) {
        String field = RedisStorage.TABLE + RedisStorage.BRAND +
                RedisStorage.CREATE_DATE_DESC;
        Brand brand = new Brand();
        brand.setName(brandsPageVo.getSearchName());
        String pattern = serviceHelper.getModelHandler().brandToXmlFilterString(brand, true);
        List list = serviceHelper.getRedisManager().pageSort(field, pattern, true);
        return ServiceResult.ok(listPage(list, brandsPageVo.getPageNo(), brandsPageVo.getPageSize()), "查询品牌列表数据成功");
    }

    /**
     * 查询数据 右匹配
     *
     * @param searchName
     * @return
     */
    @Override
    public ServiceResult<List<Brand>> listRedis(String searchName) {
        String field = RedisStorage.TABLE + RedisStorage.BRAND +
                RedisStorage.CREATE_DATE_DESC;
        Brand brand = new Brand();
        brand.setCnName(searchName);
        //右模糊查询
        String pattern = serviceHelper.getModelHandler().brandToXmlFilterString(brand, false);
        List list = serviceHelper.getRedisManager().pageSort(field, pattern, true);
        List<Brand> brandList = new ArrayList<>();
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                String xmlString = (String) list.get(i);
                String jsonString = XmlUtil.xmlStringToJSON(xmlString).toString();
                brandList.add(JSON.parseObject(jsonString, Brand.class));
            }
        }
        return ServiceResult.ok(brandList, "查询品牌列表数据成功");
    }

    /**
     * 获取格式化数据
     *
     * @param list
     * @param currentPage
     * @param pageSize
     * @return
     */
    public PageInfo<Brand> listPage(List<String> list, int currentPage, int pageSize) {
        PageInfo<Brand> pageInfo = new PageInfo<>();
        //list总数
        int totalCount = list.size();
        //总页数
        int pageCount = 0;
        List<String> subyList = null;
        int m = totalCount % pageSize;
        if (m > 0) {
            pageCount = totalCount / pageSize + 1;
        } else {
            pageCount = totalCount / pageSize;
        }

        if (m == 0) {
            subyList = list.subList((currentPage - 1) * pageSize, pageSize * (currentPage));
        } else {
            if (currentPage == pageCount) {
                subyList = list.subList((currentPage - 1) * pageSize, totalCount);
            }
            if (currentPage < pageCount) {
                subyList = list.subList((currentPage - 1) * pageSize, pageSize * (currentPage));
            }
        }
        if (currentPage > pageCount) {
            return null;
        }

        List<Brand> brandList = new ArrayList<>();
        if (null != subyList && subyList.size() > 0) {
            for (int i = 0; i < subyList.size(); i++) {
                String xmlString = (String) subyList.get(i);
                String jsonString = XmlUtil.xmlStringToJSON(xmlString).toString();
                brandList.add(JSON.parseObject(jsonString, Brand.class));
            }
        }
        //封装返回pageInfo
        pageInfo.setTotal(totalCount);
        pageInfo.setPages(pageCount);
        pageInfo.setPageNum(currentPage);
        pageInfo.setPageSize(pageSize);
        pageInfo.setList(brandList);
        pageInfo.setSize(brandList.size());
        pageInfo.calcByNavigatePages(8);
        return pageInfo;
    }
}
