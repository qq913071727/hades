package com.innovation.ic.sc.base.service.sc_erp;

import com.innovation.ic.sc.base.model.erp9_pvecrm.ScCreditsTopView;
import com.innovation.ic.sc.base.model.sc_erp.AccountStatement;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 账户对账单管理
 * @Date 2022/12/20 9:17
 **/
public interface AccountStatementService {
    /**
     * 批量插入对账单数据
     * @param accountStatementList
     */
    void batchInsert(List<AccountStatement> accountStatementList);

    /**
     * 校验是否已经生成过
     * @param scCreditsTopView
     * @return
     */
    List<AccountStatement> findByScCreditsTopView(ScCreditsTopView scCreditsTopView);
}
