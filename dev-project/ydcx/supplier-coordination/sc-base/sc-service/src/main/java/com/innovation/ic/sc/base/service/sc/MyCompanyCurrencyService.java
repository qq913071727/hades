package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.MyCompanyCurrency;
import com.innovation.ic.sc.base.vo.CompanyCurrencyVo;

import java.util.List;

public interface MyCompanyCurrencyService {

    void saveBatch(List<MyCompanyCurrency> bindMyCompanyCurrency);

    void truncate();

    void listenDeleteCompanyCurrency(CompanyCurrencyVo companyCurrencyVo);

    void listenUpdateCompanyCurrency(CompanyCurrencyVo companyCurrencyVo);

    void listenAddCompanyCurrency(CompanyCurrencyVo companyCurrencyVo);

    List<MyCompanyCurrency> findAll();

    MyCompanyCurrency findByExternalId(String currency);


    MyCompanyCurrency findById(String id);

    MyCompanyCurrency findByEnName(String currency);

    MyCompanyCurrency findByName(String currency);
}
