package com.innovation.ic.sc.base.service.erp9_pvecrm.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.erp9_pvecrm.CompanySCTopViewMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.CompanySCTopView;
import com.innovation.ic.sc.base.model.erp9_pvecrm.EnterprisesTopView;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.erp9_pvecrm.CompanySCTopViewService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanySCTopViewServiceImpl  extends ServiceImpl<CompanySCTopViewMapper, CompanySCTopView> implements CompanySCTopViewService {


    @Override
    public ServiceResult<List<CompanySCTopView>> findByIds(List<String> ids) {
        ServiceResult<List<CompanySCTopView>> serviceResult = new ServiceResult<>();
        List<CompanySCTopView> companySCTopViews = this.baseMapper.selectBatchIds(ids);
        serviceResult.setResult(companySCTopViews);
        return serviceResult;
    }
}
