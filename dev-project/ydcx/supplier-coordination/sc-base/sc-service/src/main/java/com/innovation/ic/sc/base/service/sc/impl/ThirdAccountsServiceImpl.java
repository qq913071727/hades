package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.ThirdAccountsMapper;
import com.innovation.ic.sc.base.model.sc.ThirdAccounts;
import com.innovation.ic.sc.base.service.sc.ThirdAccountsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @desc   ThirdAccounts的具体实现类
 * @author linuo
 * @time   2022年8月16日16:31:51
 */
@Service
@Transactional
public class ThirdAccountsServiceImpl extends ServiceImpl<ThirdAccountsMapper, ThirdAccounts> implements ThirdAccountsService {

}