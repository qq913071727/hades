package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.PersonnelManagement;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.PersonnelManagementVo;

import java.util.List;

public interface PersonnelManagementService {

    /**
     * 当前主账号下关联的人员
     *
     * @return 返回当前主账号下关联的人员信息
     */
    ServiceResult<List> findUserId(String userId);

    /**
     * 批量更新当前主账号下关联的人员
     *
     * @return
     */
    void updateManagedUserIds(PersonnelManagementVo personnelManagementVo);

    /**
     * 初始化人员管理数据
     */
    void initPersonnelManagementData();

//    /**
//     * 批量删除当前主账号下关联的人员
//     *
//     * @return
//     */
//    void deleteManagedUserIds(PersonnelManagementVo personnelManagementVo);
}
