package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.ErpMyCompanyName;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.ErpMyCompanyNameVo;
import com.innovation.ic.sc.base.vo.SearchErpMyCompanyNameVo;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface ErpMyCompanyNameService {


     void saveBatch(List<ErpMyCompanyName> list);


    ServiceResult<List<ErpMyCompanyName>> findByLikeCompanyName(SearchErpMyCompanyNameVo searchErpMyCompanyNameVo);

    void saveOrUpdateErpCompany(ErpMyCompanyNameVo erpMyCompanyNameVo);

    void updateErpCompany(ErpMyCompanyNameVo erpMyCompanyNameVo);

    Integer findByCompanyName(String name);

    ErpMyCompanyName findById(String id);

    ErpMyCompanyName findByExternalId(String externalId);

    ErpMyCompanyName findByNameMap(Map<String, Object> paramMap);

    void trunCateTable();
}