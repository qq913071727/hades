package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.MyCompanyCurrencyMapper;
import com.innovation.ic.sc.base.model.sc.MyCompanyCurrency;
import com.innovation.ic.sc.base.service.sc.MyCompanyCurrencyService;
import com.innovation.ic.sc.base.vo.CompanyCurrencyVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Slf4j
@Service
public class MyCompanyCurrencyServiceImpl extends ServiceImpl<MyCompanyCurrencyMapper, MyCompanyCurrency> implements MyCompanyCurrencyService {
    @Override
    public void saveBatch(List<MyCompanyCurrency> myCompanyCurrencyList) {
        if (CollectionUtils.isEmpty(myCompanyCurrencyList)) {
            return;
        }
        this.baseMapper.insertBatchSomeColumn(myCompanyCurrencyList);
    }

    @Override
    public void truncate() {
        this.baseMapper.truncate();
    }

    @Override
    public void listenDeleteCompanyCurrency(CompanyCurrencyVo companyCurrencyVo) {
        String id = companyCurrencyVo.getID();
        if (StringUtils.isBlank(id)) {
            return;
        }
        String[] isSplit = id.split(",");
        for (String externalId : isSplit) {
            Map<String, Object> param = new HashedMap<>();
            param.put("external_id", externalId);
            this.baseMapper.deleteByMap(param);
        }
    }

    @Override
    public void listenUpdateCompanyCurrency(CompanyCurrencyVo companyCurrencyVo) {
        UpdateWrapper<MyCompanyCurrency> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("external_id", companyCurrencyVo.getID());
        updateWrapper.set("order_index", companyCurrencyVo.getOrderIndex());
        this.baseMapper.update(null, updateWrapper);
    }

    @Override
    public void listenAddCompanyCurrency(CompanyCurrencyVo companyCurrencyVo) {
        if (companyCurrencyVo == null) {
            return;
        }
        MyCompanyCurrency myCompanyCurrency = new MyCompanyCurrency();
        myCompanyCurrency.setCreateDate(new Date());
        myCompanyCurrency.setName(companyCurrencyVo.getName());
        myCompanyCurrency.setEnName(companyCurrencyVo.getEnName());
        myCompanyCurrency.setSymbol(companyCurrencyVo.getSymbol());
        myCompanyCurrency.setShortSymbol(companyCurrencyVo.getShortSymbol());
        myCompanyCurrency.setCumericCode(companyCurrencyVo.getNumericCode());
        myCompanyCurrency.setOrderIndex(companyCurrencyVo.getOrderIndex());
        myCompanyCurrency.setExternalId(companyCurrencyVo.getID() + "");
        this.baseMapper.insert(myCompanyCurrency);
    }

    @Override
    public List<MyCompanyCurrency> findAll() {
        return this.baseMapper.selectByMap(new HashedMap<>());
    }

    @Override
    public MyCompanyCurrency findByExternalId(String currency) {
        return this.baseMapper.findByExternalId(currency);
    }

    @Override
    public MyCompanyCurrency findById(String id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public MyCompanyCurrency findByEnName(String currency) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("en_name", currency);
        List<MyCompanyCurrency> myCompanyCurrencyList = this.baseMapper.selectByMap(paramMap);
        if (CollectionUtils.isEmpty(myCompanyCurrencyList)) {
            return null;
        } else {
            return myCompanyCurrencyList.get(0);
        }
    }

    @Override
    public MyCompanyCurrency findByName(String currency) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", currency);
        List<MyCompanyCurrency> myCompanyCurrencyList = this.baseMapper.selectByMap(paramMap);
        if (CollectionUtils.isEmpty(myCompanyCurrencyList)) {
            return null;
        } else {
            return myCompanyCurrencyList.get(0);
        }
    }
}
