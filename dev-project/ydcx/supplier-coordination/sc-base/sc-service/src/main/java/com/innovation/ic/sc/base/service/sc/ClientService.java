package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.Client;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import java.util.List;

/**
 * @desc   Client的服务类接口
 * @author linuo
 * @time   2022年8月11日15:24:30
 */
public interface ClientService {
    /**
     * 查找client表中所有数据
     * @return
     */
    ServiceResult<List<Client>> findAll();
}