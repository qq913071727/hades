package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.MenuTree;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

/**
 * MenuTree的服务类接口
 */
public interface MenuTreeService {

    /**
     * 返回主账号的所有菜单
     * @return
     */
//    ServiceResult<List<MenuItemPojo>> findByMainAccountId();

    /**
     * 根据经理userId，返回子账号的所有菜单
     * @param userId
     * @return
     */
//    ServiceResult<List<MenuItemPojo>> findByManagerAccountId(String userId);

    /**
     * 根据员工userId，返回子账号的所有菜单
     * @param userId
     * @return
     */
//    ServiceResult<List<MenuItemPojo>> findByStaffAccountId(String userId);

    /**
     * 主账号创建新角色时需要展示的菜单树形目录（不包括账号管理和角色管理），初始时没有任何选项被选中
     * @return
     */
//    ServiceResult<List<MenuItemPojo>> showMenuForAddRole();

    /**
     * 删除menu表中的数据
     */
//    ServiceResult<Boolean> truncateTableMenu();

    /**
     * 插入Menu类型对象列表
     * @param menuList
     * @return
     */
//    ServiceResult<Boolean> insertMenuList(List<Menu> menuList);

    /**
     * 先删除旧的，再添加新的MenuTree对象
     * @param menuTree
     * @return
     */
    ServiceResult<MenuTree> saveOrUpdateMenuTree(MenuTree menuTree);

    /**
     * 根据userId，查找MenuTree对象
     * @param userId
     * @return
     */
    ServiceResult<MenuTree> findByUserId(String userId);
}
