package com.innovation.ic.sc.base.service.erp9_pvecrm;

import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import java.util.List;

/**
 * @desc  SpecialsSCTopView的服务类接口
 * @author linuo
 * @time   2022年8月29日14:41:18
 */
public interface SpecialsSCTopViewService {

    /**
     * 获取erp库中优势型号数据
     * @return 返回erp库中优势型号数据
     */
    ServiceResult<List<SpecialsSCTopView>> getErpAdvantageModelData();
}