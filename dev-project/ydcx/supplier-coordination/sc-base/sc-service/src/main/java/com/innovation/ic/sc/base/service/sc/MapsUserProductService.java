package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.MapsUserProducts;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.vo.MapsUserProductVo;
import java.util.List;

/**
 * @Description: 用户产品型号关系接口
 * @ClassName: MapsIUserProductService
 * @Author: myq
 * @Date: 2022/8/5 下午1:58
 * @Version: 1.0
 */
public interface MapsUserProductService {
    ServiceResult<List<MapsUserProductVo>> mapsUserProductList(int pageNo, int pageSize, String searchName, String userId);

    ServiceResult add(String userId,List<MapsUserProducts> mapsUserProducts);

    ServiceResult<List<MapsUserProductVo>> get(String userId);
}