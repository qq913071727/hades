package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.LadderPriceConfigMapper;
import com.innovation.ic.sc.base.model.sc.LadderPriceConfig;
import com.innovation.ic.sc.base.service.sc.LadderPriceConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

/**
 * @desc   LadderPriceConfig的具体实现类
 * @author linuo
 * @time   2022年9月19日15:51:13
 */
@Service
@Transactional
public class LadderPriceConfigServiceImpl extends ServiceImpl<LadderPriceConfigMapper, LadderPriceConfig> implements LadderPriceConfigService {

}