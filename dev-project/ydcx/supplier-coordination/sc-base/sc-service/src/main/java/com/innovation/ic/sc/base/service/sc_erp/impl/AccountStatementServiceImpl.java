package com.innovation.ic.sc.base.service.sc_erp.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc_erp.AccountStatementMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.ScCreditsTopView;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.model.sc_erp.AccountStatement;
import com.innovation.ic.sc.base.service.sc_erp.AccountStatementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 账户对账单管理
 * @Date 2022/12/20 9:18
 **/
@Service
@Slf4j
public class AccountStatementServiceImpl extends ServiceImpl<AccountStatementMapper, AccountStatement>  implements AccountStatementService {
    /**
     * 批量插入对账单数据
     * @param accountStatementList
     */
    @Override
    public void batchInsert(List<AccountStatement> accountStatementList) {
        baseMapper.batchInsert(accountStatementList);
    }

    /**
     * 校验是否已经生成过
     * @param scCreditsTopView
     * @return
     */
    @Override
    public List<AccountStatement> findByScCreditsTopView(ScCreditsTopView scCreditsTopView) {
        LambdaQueryWrapper<AccountStatement> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AccountStatement::getSellerName,scCreditsTopView.getSellerName());
        queryWrapper.eq(AccountStatement::getSellerId,scCreditsTopView.getSellerId());
        queryWrapper.eq(AccountStatement::getBuyerName,scCreditsTopView.getBuyerName());
        queryWrapper.eq(AccountStatement::getBuyerId,scCreditsTopView.getBuyerId());
        queryWrapper.eq(AccountStatement::getSettlementType,scCreditsTopView.getSettlementType());
        queryWrapper.eq(AccountStatement::getGenerationDate,scCreditsTopView.getGenerationDate());
        queryWrapper.eq(AccountStatement::getSettlementDate,scCreditsTopView.getSettlementDate());
        queryWrapper.eq(AccountStatement::getIntervalDateStart,scCreditsTopView.getIntervalDateStart());
        queryWrapper.eq(AccountStatement::getIntervalDateEnd,scCreditsTopView.getIntervalDateEnd());
        return baseMapper.selectList(queryWrapper);
    }
}
