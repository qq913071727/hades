package com.innovation.ic.sc.base.service.erp9_pvesiteuser.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.erp9_pvesiteuser.MenusMapper;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.Menus;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.erp9_pvesiteuser.MenusService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class MenusServiceImpl extends ServiceImpl<MenusMapper, Menus> implements MenusService {

    @Resource
    private MenusMapper menusMapper;

    /**
     * 查找所有菜单数据
     * @return
     */
    @Override
    public ServiceResult<List<Menus>> findAll() {
        ServiceResult<List<Menus>> serviceResult = new ServiceResult<>();

        QueryWrapper<Menus> queryWrapper = new QueryWrapper<>();
        List<Menus> menusList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(menusList);
        return serviceResult;
    }
}
