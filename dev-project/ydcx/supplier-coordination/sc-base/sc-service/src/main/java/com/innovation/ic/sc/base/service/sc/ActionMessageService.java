package com.innovation.ic.sc.base.service.sc;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.sc.base.model.sc.ActionMessage;
import com.innovation.ic.sc.base.pojo.variable.ActionMessagePojo;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

import java.util.List;

public interface ActionMessageService {


    /**
     * @Description: 分页
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/816:44
     */
    ServiceResult<PageInfo<ActionMessagePojo>> page(int pageNo, int pageSize, String id);

    /**
     * @Description: 分页，从redis中取数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/816:44
     */
    ServiceResult<PageInfo<ActionMessagePojo>> pageRedis(int pageNo, int pageSize, String id);

    /**
     * @Description: 处理队列消息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/816:56
     */
    ServiceResult handleActionMessage(ActionMessage actionMessage, List<String> userIds);

    /**
     * @Description: 处理队列消息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/816:56
     */
    public ServiceResult handleActionMessageByUserName(ActionMessage actionMessage, List<String> userNames);


    /**
     * @param userId
     * @Description: 全部已读
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/616:26
     */
    void isReadAll(String userId);

    /**
     * @param userId
     * @Description: 全部已读
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/1/616:26
     */
    void isReadAllRedis(String userId);

    /**
     * @Description: 返回当前用户的未读消息总数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/1417:11
     */
    ServiceResult<Integer> unReadTotal(String userId);

    /**
     * @Description: 从redis中，返回当前用户的未读消息总数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2023/2/1417:11
     */
    ServiceResult<Integer> unReadTotalRedis(String userId);

    /**
     * 将接口/api/v2/actionMessage/initPage需要的数据导入到redis
     */
    void initPage();

    /**
     * 将接口/api/v2/actionMessage/initUnReadTotal需要的数据导入到redis
     */
    void initUnReadTotal();
}
