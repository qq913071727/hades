package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.MenuTreeMapper;
import com.innovation.ic.sc.base.model.sc.MapUserRole;
import com.innovation.ic.sc.base.model.sc.MenuTree;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.menu.Menu;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.MenuTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * MenuService的具体实现类
 */
@Service
@Transactional
public class MenuTreeServiceImpl extends ServiceImpl<MenuTreeMapper, MenuTree> implements MenuTreeService {

    @Resource
    private ServiceHelper serviceHelper;
    /**
     * 先删除旧的，再添加新的MenuTree对象
     * @param menuTree
     * @return
     */
    @Override
    public ServiceResult<MenuTree> saveOrUpdateMenuTree(MenuTree menuTree) {
        ServiceResult<MenuTree> serviceResult = new ServiceResult<MenuTree>();

        // 删除
        Query query = Query.query(Criteria.where("userId").is(menuTree.getUserId()));
        serviceHelper.getMongodbManager().remove(query, MenuTree.class);

        // 添加
        MenuTree result =  serviceHelper.getMongodbManager().insert(menuTree);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 根据userId，查找MenuTree对象
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<MenuTree> findByUserId(String userId) {
        ServiceResult<MenuTree> serviceResult = new ServiceResult<MenuTree>();

        // 删除
        Query query = Query.query(Criteria.where("userId").is(userId));
        MenuTree menuTree =  serviceHelper.getMongodbManager().findOne(query, MenuTree.class);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(menuTree);
        return serviceResult;
    }


    /**
     * 返回主账号的所有菜单
     *
     * @return
     */
//    @Override
//    public ServiceResult<List<MenuItemPojo>> findByMainAccountId() {
//        ServiceResult<List<MenuItemPojo>> serviceResult = new ServiceResult<List<MenuItemPojo>>();
//        List<MenuItemPojo> menuItemPojoList = new ArrayList<>();
//
//        // 一级目录
//        List<Menu> firstMenuList = menuMapper.findByType(MenuType.FIRST_MENU);
//        if (null != firstMenuList && firstMenuList.size() > 0) {
//            for (int i = 0; i < firstMenuList.size(); i++) {
//
//                Menu firstMenu = firstMenuList.get(i);
//                MenuItemPojo firstMenuItemPojo = new MenuItemPojo();
//                firstMenuItemPojo.setId(firstMenu.getId());
//                firstMenuItemPojo.setName(firstMenu.getName());
//                firstMenuItemPojo.setPath(firstMenu.getPath());
//                firstMenuItemPojo.setType(MenuType.FIRST_MENU);
//                MenuItemPojo.Meta firstMeta = firstMenuItemPojo.new Meta();
//                firstMeta.setTitle(firstMenuItemPojo.getName());
//                firstMenuItemPojo.setMeta(firstMeta);
//                menuItemPojoList.add(firstMenuItemPojo);
//
//                // 二级目录
//                List<Menu> secondMenuList = menuMapper.findMainAccountMenuChildById(firstMenu.getId());
//                if (null != secondMenuList && secondMenuList.size() > 0) {
//                    for (int j = 0; j < secondMenuList.size(); j++) {
//
//                        Menu secondMenu = secondMenuList.get(j);
//                        MenuItemPojo secondMenuItemPojo = new MenuItemPojo();
//                        secondMenuItemPojo.setId(secondMenu.getId());
//                        secondMenuItemPojo.setName(secondMenu.getName());
//                        secondMenuItemPojo.setPath(secondMenu.getPath());
//                        // 首页没有二级菜单，只有按钮
//                        if (MenuSelected.HOME_FIRST_MENU_ITEM.equals(firstMenu.getName())) {
//                            secondMenuItemPojo.setType(MenuType.BUTTON);
//                        } else {
//                            secondMenuItemPojo.setType(MenuType.SECOND_MENU);
//                        }
//                        MenuItemPojo.Meta secondMeta = secondMenuItemPojo.new Meta();
//                        secondMeta.setTitle(secondMenuItemPojo.getName());
//                        secondMenuItemPojo.setMeta(secondMeta);
//                        firstMenuItemPojo.getChildren().add(secondMenuItemPojo);
//
//                        // 三级目录（按钮）
//                        List<Menu> thirdMenuList = menuMapper.findMainAccountMenuChildById(secondMenu.getId());
//                        if (null != thirdMenuList && thirdMenuList.size() > 0) {
//                            for (int x = 0; x < thirdMenuList.size(); x++) {
//
//                                Menu thirdMenu = thirdMenuList.get(x);
//                                // 主账号没有系统管理-账号管理-人员管理按钮
//                                if (firstMenu.getName().equals(MenuSelected.SYSTEM_MANAGEMENT_FIRST_MENU_ITEM)
//                                        && secondMenu.getName().equals(MenuSelected.ROLE_MANAGEMENT_SECOND_MENU_ITEM)
//                                        && thirdMenu.getName().equals(MenuSelected.PERSONNEL_MANAGEMENT_BUTTON)) {
//                                    continue;
//                                }
//                                MenuItemPojo thirdMenuItemPojo = new MenuItemPojo();
//                                thirdMenuItemPojo.setId(thirdMenu.getId());
//                                thirdMenuItemPojo.setName(thirdMenu.getName());
//                                thirdMenuItemPojo.setPath(thirdMenu.getPath());
//                                thirdMenuItemPojo.setType(MenuType.BUTTON);
//                                MenuItemPojo.Meta thirdMeta = thirdMenuItemPojo.new Meta();
//                                thirdMeta.setTitle(thirdMenuItemPojo.getName());
//                                thirdMenuItemPojo.setMeta(thirdMeta);
//                                secondMenuItemPojo.getChildren().add(thirdMenuItemPojo);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        serviceResult.setSuccess(Boolean.TRUE);
//        serviceResult.setResult(menuItemPojoList);
//        return serviceResult;
//    }

    /**
     * 根据经理userId，返回子账号的所有菜单
     *
     * @param userId
     * @return
     */
//    @Override
//    public ServiceResult<List<MenuItemPojo>> findByManagerAccountId(String userId) {
//        ServiceResult<List<MenuItemPojo>> serviceResult = new ServiceResult<List<MenuItemPojo>>();
//        List<MenuItemPojo> menuItemPojoList = new ArrayList<>();
//
//        // 一级目录
//        List<Menu> firstMenuList = menuMapper.findByTypeAndUserId(MenuType.FIRST_MENU, userId);
//        if (null != firstMenuList && firstMenuList.size() > 0) {
//            for (int i = 0; i < firstMenuList.size(); i++) {
//
//                Menu firstMenu = firstMenuList.get(i);
//                MenuItemPojo firstMenuItemPojo = new MenuItemPojo();
//                firstMenuItemPojo.setId(firstMenu.getId());
//                firstMenuItemPojo.setName(firstMenu.getName());
//                firstMenuItemPojo.setPath(firstMenu.getPath());
//                firstMenuItemPojo.setType(MenuType.FIRST_MENU);
//                MenuItemPojo.Meta firstMeta = firstMenuItemPojo.new Meta();
//                firstMeta.setTitle(firstMenuItemPojo.getName());
//                firstMenuItemPojo.setMeta(firstMeta);
//                menuItemPojoList.add(firstMenuItemPojo);
//
//                // 二级目录
//                List<Menu> secondMenuList = menuMapper.findSubAccountMenuChildById(firstMenu.getId(), userId);
//                if (null != secondMenuList && secondMenuList.size() > 0) {
//                    for (int j = 0; j < secondMenuList.size(); j++) {
//
//                        Menu secondMenu = secondMenuList.get(j);
//                        // 经理没有系统管理-角色管理
//                        if (firstMenu.getName().equals(MenuSelected.SYSTEM_MANAGEMENT_FIRST_MENU_ITEM)
//                                && secondMenu.getName().equals(MenuSelected.ROLE_MANAGEMENT_SECOND_MENU_ITEM)) {
//                            continue;
//                        }
//                        MenuItemPojo secondMenuItemPojo = new MenuItemPojo();
//                        secondMenuItemPojo.setId(secondMenu.getId());
//                        secondMenuItemPojo.setName(secondMenu.getName());
//                        secondMenuItemPojo.setPath(secondMenu.getPath());
//                        // 首页没有二级菜单，只有按钮
//                        if (MenuSelected.HOME_FIRST_MENU_ITEM.equals(firstMenu.getName())) {
//                            secondMenuItemPojo.setType(MenuType.BUTTON);
//                        } else {
//                            secondMenuItemPojo.setType(MenuType.SECOND_MENU);
//                        }
//                        MenuItemPojo.Meta secondMeta = secondMenuItemPojo.new Meta();
//                        secondMeta.setTitle(secondMenuItemPojo.getName());
//                        secondMenuItemPojo.setMeta(secondMeta);
//                        firstMenuItemPojo.getChildren().add(secondMenuItemPojo);
//
//                        // 三级目录（按钮）
//                        List<Menu> thirdMenuList = menuMapper.findSubAccountMenuChildById(secondMenu.getId(), userId);
//                        if (null != thirdMenuList && thirdMenuList.size() > 0) {
//                            for (int x = 0; x < thirdMenuList.size(); x++) {
//
//                                Menu thirdMenu = thirdMenuList.get(x);
//                                // 经理没有系统管理-账号管理-新增、重置密码、设置权限、启用、停用
//                                if (firstMenu.getName().equals(MenuSelected.SYSTEM_MANAGEMENT_FIRST_MENU_ITEM)
//                                        && secondMenu.getName().equals(MenuSelected.ACCOUNT_MANAGEMENT_SECOND_MENU_ITEM)
//                                        && (thirdMenu.getName().equals(MenuSelected.ADD_BUTTON)
//                                        || thirdMenu.getName().equals(MenuSelected.RESET_PASSWORD_BUTTON)
//                                        || thirdMenu.getName().equals(MenuSelected.SET_PRIVILEGE_BUTTON)
//                                        || thirdMenu.getName().equals(MenuSelected.START_BUTTON)
//                                        || thirdMenu.getName().equals(MenuSelected.STOP_BUTTON))) {
//                                    continue;
//                                }
//                                MenuItemPojo thirdMenuItemPojo = new MenuItemPojo();
//                                thirdMenuItemPojo.setId(thirdMenu.getId());
//                                thirdMenuItemPojo.setName(thirdMenu.getName());
//                                thirdMenuItemPojo.setPath(thirdMenu.getPath());
//                                thirdMenuItemPojo.setType(MenuType.BUTTON);
//                                MenuItemPojo.Meta thirdMeta = thirdMenuItemPojo.new Meta();
//                                thirdMeta.setTitle(thirdMenuItemPojo.getName());
//                                thirdMenuItemPojo.setMeta(thirdMeta);
//                                secondMenuItemPojo.getChildren().add(thirdMenuItemPojo);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        serviceResult.setSuccess(Boolean.TRUE);
//        serviceResult.setResult(menuItemPojoList);
//        return serviceResult;
//    }

    /**
     * 根据员工userId，返回子账号的所有菜单
     *
     * @param userId
     * @return
     */
//    @Override
//    public ServiceResult<List<MenuItemPojo>> findByStaffAccountId(String userId) {
//        ServiceResult<List<MenuItemPojo>> serviceResult = new ServiceResult<List<MenuItemPojo>>();
//        List<MenuItemPojo> menuItemPojoList = new ArrayList<>();
//
//        // 一级目录
//        List<Menu> firstMenuList = menuMapper.findByTypeAndUserId(MenuType.FIRST_MENU, userId);
//        if (null != firstMenuList && firstMenuList.size() > 0) {
//            for (int i = 0; i < firstMenuList.size(); i++) {
//
//                Menu firstMenu = firstMenuList.get(i);
//                MenuItemPojo firstMenuItemPojo = new MenuItemPojo();
//                firstMenuItemPojo.setId(firstMenu.getId());
//                firstMenuItemPojo.setName(firstMenu.getName());
//                firstMenuItemPojo.setPath(firstMenu.getPath());
//                firstMenuItemPojo.setType(MenuType.FIRST_MENU);
//                MenuItemPojo.Meta firstMeta = firstMenuItemPojo.new Meta();
//                firstMeta.setTitle(firstMenuItemPojo.getName());
//                firstMenuItemPojo.setMeta(firstMeta);
//                menuItemPojoList.add(firstMenuItemPojo);
//
//                // 二级目录
//                List<Menu> secondMenuList = menuMapper.findSubAccountMenuChildById(firstMenu.getId(), userId);
//                if (null != secondMenuList && secondMenuList.size() > 0) {
//                    for (int j = 0; j < secondMenuList.size(); j++) {
//
//                        Menu secondMenu = secondMenuList.get(j);
//                        // 经理没有系统管理-账号管理、角色管理
//                        if (firstMenu.getName().equals(MenuSelected.SYSTEM_MANAGEMENT_FIRST_MENU_ITEM)
//                                && (secondMenu.getName().equals(MenuSelected.ACCOUNT_MANAGEMENT_SECOND_MENU_ITEM)
//                                || secondMenu.getName().equals(MenuSelected.ROLE_MANAGEMENT_SECOND_MENU_ITEM))) {
//                            continue;
//                        }
//                        MenuItemPojo secondMenuItemPojo = new MenuItemPojo();
//                        secondMenuItemPojo.setId(secondMenu.getId());
//                        secondMenuItemPojo.setName(secondMenu.getName());
//                        secondMenuItemPojo.setPath(secondMenu.getPath());
//                        // 首页没有二级菜单，只有按钮
//                        if (MenuSelected.HOME_FIRST_MENU_ITEM.equals(firstMenu.getName())) {
//                            secondMenuItemPojo.setType(MenuType.BUTTON);
//                        } else {
//                            secondMenuItemPojo.setType(MenuType.SECOND_MENU);
//                        }
//                        MenuItemPojo.Meta secondMeta = secondMenuItemPojo.new Meta();
//                        secondMeta.setTitle(secondMenuItemPojo.getName());
//                        secondMenuItemPojo.setMeta(secondMeta);
//                        firstMenuItemPojo.getChildren().add(secondMenuItemPojo);
//
//                        // 三级目录（按钮）
//                        List<Menu> thirdMenuList = menuMapper.findSubAccountMenuChildById(secondMenu.getId(), userId);
//                        if (null != thirdMenuList && thirdMenuList.size() > 0) {
//                            for (int x = 0; x < thirdMenuList.size(); x++) {
//
//                                Menu thirdMenu = thirdMenuList.get(x);
//                                MenuItemPojo thirdMenuItemPojo = new MenuItemPojo();
//                                thirdMenuItemPojo.setId(thirdMenu.getId());
//                                thirdMenuItemPojo.setName(thirdMenu.getName());
//                                thirdMenuItemPojo.setPath(thirdMenu.getPath());
//                                thirdMenuItemPojo.setType(MenuType.BUTTON);
//                                MenuItemPojo.Meta thirdMeta = thirdMenuItemPojo.new Meta();
//                                thirdMeta.setTitle(thirdMenuItemPojo.getName());
//                                thirdMenuItemPojo.setMeta(thirdMeta);
//                                secondMenuItemPojo.getChildren().add(thirdMenuItemPojo);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        serviceResult.setSuccess(Boolean.TRUE);
//        serviceResult.setResult(menuItemPojoList);
//        return serviceResult;
//    }

    /**
     * 主账号创建新角色时需要展示的菜单树形目录（不包括账号管理和角色管理），初始时没有任何选项被选中
     *
     * @return
     */
//    @Override
//    public ServiceResult<List<MenuItemPojo>> showMenuForAddRole() {
//        ServiceResult<List<MenuItemPojo>> serviceResult = new ServiceResult<List<MenuItemPojo>>();
//        List<MenuItemPojo> menuItemPojoList = new ArrayList<>();
//
//        // 一级目录
//        List<Menu> firstMenuList = menuMapper.findByType(MenuType.FIRST_MENU);
//        if (null != firstMenuList && firstMenuList.size() > 0) {
//            for (int i = 0; i < firstMenuList.size(); i++) {
//
//                Menu firstMenu = firstMenuList.get(i);
//                MenuItemPojo firstMenuItemPojo = new MenuItemPojo();
//                firstMenuItemPojo.setId(firstMenu.getId());
//                firstMenuItemPojo.setName(firstMenu.getName());
//                firstMenuItemPojo.setPath(firstMenu.getPath());
//                firstMenuItemPojo.setType(MenuType.FIRST_MENU);
//                MenuItemPojo.Meta firstMeta = firstMenuItemPojo.new Meta();
//                firstMeta.setTitle(firstMenuItemPojo.getName());
//                firstMenuItemPojo.setMeta(firstMeta);
//                menuItemPojoList.add(firstMenuItemPojo);
//
//                // 二级目录
//                List<Menu> secondMenuList = menuMapper.findMainAccountMenuChildById(firstMenu.getId());
//                if (null != secondMenuList && secondMenuList.size() > 0) {
//                    for (int j = 0; j < secondMenuList.size(); j++) {
//
//                        Menu secondMenu = secondMenuList.get(j);
//                        // 经理没有系统管理-账号管理、角色管理
//                        if (firstMenu.getName().equals(MenuSelected.SYSTEM_MANAGEMENT_FIRST_MENU_ITEM)
//                                && (secondMenu.getName().equals(MenuSelected.ACCOUNT_MANAGEMENT_SECOND_MENU_ITEM)
//                                || secondMenu.getName().equals(MenuSelected.ROLE_MANAGEMENT_SECOND_MENU_ITEM))) {
//                            continue;
//                        }
//                        MenuItemPojo secondMenuItemPojo = new MenuItemPojo();
//                        secondMenuItemPojo.setId(secondMenu.getId());
//                        secondMenuItemPojo.setName(secondMenu.getName());
//                        secondMenuItemPojo.setPath(secondMenu.getPath());
//                        // 首页没有二级菜单，只有按钮
//                        if (MenuSelected.HOME_FIRST_MENU_ITEM.equals(firstMenu.getName())) {
//                            secondMenuItemPojo.setType(MenuType.BUTTON);
//                        } else {
//                            secondMenuItemPojo.setType(MenuType.SECOND_MENU);
//                        }
//                        MenuItemPojo.Meta secondMeta = secondMenuItemPojo.new Meta();
//                        secondMeta.setTitle(secondMenuItemPojo.getName());
//                        secondMenuItemPojo.setMeta(secondMeta);
//                        firstMenuItemPojo.getChildren().add(secondMenuItemPojo);
//
//                        // 三级目录（按钮）
//                        List<Menu> thirdMenuList = menuMapper.findMainAccountMenuChildById(secondMenu.getId());
//                        if (null != thirdMenuList && thirdMenuList.size() > 0) {
//                            for (int x = 0; x < thirdMenuList.size(); x++) {
//
//                                Menu thirdMenu = thirdMenuList.get(x);
//                                MenuItemPojo thirdMenuItemPojo = new MenuItemPojo();
//                                thirdMenuItemPojo.setId(thirdMenu.getId());
//                                thirdMenuItemPojo.setName(thirdMenu.getName());
//                                thirdMenuItemPojo.setPath(thirdMenu.getPath());
//                                thirdMenuItemPojo.setType(MenuType.BUTTON);
//                                MenuItemPojo.Meta thirdMeta = thirdMenuItemPojo.new Meta();
//                                thirdMeta.setTitle(thirdMenuItemPojo.getName());
//                                thirdMenuItemPojo.setMeta(thirdMeta);
//                                secondMenuItemPojo.getChildren().add(thirdMenuItemPojo);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        serviceResult.setSuccess(Boolean.TRUE);
//        serviceResult.setResult(menuItemPojoList);
//        return serviceResult;
//    }

    /**
     * 删除menu表中的数据
     */
//    @Override
//    public ServiceResult<Boolean> truncateTableMenu() {
//        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
//
//        menuMapper.truncateTableMenu();
//
//        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
//        serviceResult.setSuccess(Boolean.TRUE);
//        return serviceResult;
//    }

    /**
     * 插入Menu类型对象列表
     *
     * @param menuList
     * @return
     */
//    @Override
//    public ServiceResult<Boolean> insertMenuList(List<Menu> menuList) {
//        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
//        try {
//            Integer count = menuMapper.insertBatchSomeColumn(menuList);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
//        serviceResult.setSuccess(Boolean.TRUE);
//        return serviceResult;
//    }
}
