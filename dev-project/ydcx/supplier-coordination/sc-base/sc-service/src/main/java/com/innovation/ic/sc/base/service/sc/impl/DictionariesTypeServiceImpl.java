package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.mapper.sc.DictionariesMapper;
import com.innovation.ic.sc.base.mapper.sc.DictionariesTypeMapper;
import com.innovation.ic.sc.base.model.sc.Dictionaries;
import com.innovation.ic.sc.base.model.sc.DictionariesType;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.DictionariesService;
import com.innovation.ic.sc.base.service.sc.DictionariesTypeService;
import com.innovation.ic.sc.base.vo.DictionariesTypeVo;
import com.innovation.ic.sc.base.vo.DictionariesVo;
import com.innovation.ic.sc.base.vo.ErpDictionariesVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
public class DictionariesTypeServiceImpl extends ServiceImpl<DictionariesTypeMapper, DictionariesType> implements DictionariesTypeService {

    @Resource
    private ServiceHelper serviceHelper;

    @Override
    public void saveDictionariesType(DictionariesTypeVo dictionariesTypeVo) {
        if (StringUtils.validateParameter(dictionariesTypeVo.getID())) {
            return;
        }
        if (StringUtils.validateParameter(dictionariesTypeVo.getName())) {
            return;
        }
        DictionariesType dictionariesType = new DictionariesType();
        dictionariesType.setName(dictionariesTypeVo.getName());
        dictionariesType.setId(dictionariesTypeVo.getID());
        dictionariesType.setCreateDate(new Date());
        serviceHelper.getDictionariesTypeMapper().insert(dictionariesType);
    }

    @Override
    public void listenAddDistrictType(ErpDictionariesVo erpDictionariesVo) {
        String id = erpDictionariesVo.getID();
        String name = erpDictionariesVo.getName();
        if (!StringUtils.validateParameter(id) || !StringUtils.validateParameter(name)) {
            return;
        }
        DictionariesType containDictionariesType = this.baseMapper.selectById(id);
        if(containDictionariesType == null){
            DictionariesType dictionariesType = new DictionariesType();
            dictionariesType.setId(id);
            dictionariesType.setName(name);
            dictionariesType.setCreateDate(new Date());
            save(dictionariesType);
        }else{
            containDictionariesType.setName(name);
            this.baseMapper.updateById(containDictionariesType);
        }
    }

    @Override
    public void deleteBatchIds(HashSet<String> dictionariesTypeIdS) {
        this.baseMapper.deleteBatchIds(dictionariesTypeIdS);
    }

    @Override
    public void insertBatchSomeColumn(List<DictionariesType> dtList) {
        this.baseMapper.insertBatchSomeColumn(dtList);
    }

    @Override
    public DictionariesType selectById(String id) {
        return this.baseMapper.selectById(id);
    }
}
