package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.MyCompanyEnterpriseMapper;
import com.innovation.ic.sc.base.model.sc.MyCompanyEnterprise;
import com.innovation.ic.sc.base.service.sc.MyCompanyEnterpriseService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MyCompanyEnterpriseServiceImpl extends ServiceImpl<MyCompanyEnterpriseMapper, MyCompanyEnterprise> implements MyCompanyEnterpriseService {
    /**
     * 根据externatId查找对应关系
     *
     * @param externalId
     * @return
     */
    @Override
    public List<String> findByExternalId(String externalId) {
        return baseMapper.findByExternalId(externalId);
    }

    @Override
    public List<MyCompanyEnterprise> findByMyCompanyId(Integer id) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("my_company_id",id);
        return this.baseMapper.selectByMap(paramMap);
    }
}
