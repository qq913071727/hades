package com.innovation.ic.sc.base.service.sc;


import com.innovation.ic.sc.base.model.sc.MyCompanyEnterprise;

import java.util.List;

public interface MyCompanyEnterpriseService {

    /**
     * 根据externatId查找对应关系
     *
     * @param externalId
     * @return
     */
    List<String> findByExternalId(String externalId);

    List<MyCompanyEnterprise> findByMyCompanyId(Integer id);
}
