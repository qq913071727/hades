package com.innovation.ic.sc.base.service.sc;

import com.innovation.ic.sc.base.model.sc.MapUserRole;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;

import java.util.List;

public interface MapUserRoleService {

    /**
     * 保存MapUserRole类型对象列表（先删除旧的关系，再添加新的关系）
     * @param mapUserRoleList
     * @return
     */
    ServiceResult<Boolean> saveOrUpdateMapUserRoleList(List<MapUserRole> mapUserRoleList);

    /**
     * 根据userId，查找MapUserRole对象列表
     * @param userId
     * @return
     */
    ServiceResult<List<MapUserRole>> findByUserId(String userId);
}
