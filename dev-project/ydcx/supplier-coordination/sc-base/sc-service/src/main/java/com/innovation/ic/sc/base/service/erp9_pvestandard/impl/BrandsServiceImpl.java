package com.innovation.ic.sc.base.service.erp9_pvestandard.impl;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.innovation.ic.b1b.framework.util.StringUtils;
//import com.innovation.ic.sc.base.mapper.erp9_pvestandard.BrandsMapper;
//import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;
//import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
//import com.innovation.ic.sc.base.service.erp9_pvestandard.BrandsService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.util.CollectionUtils;
//import java.util.Collections;
//import java.util.List;
//import java.util.Set;
//
///**
// * @Description: 品牌实现类
// * @ClassName: BrandsServiceImpl
// * @Author: myq
// * @Date: 2022/8/5 下午5:38
// * @Version: 1.0
// */

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.erp9_pvestandard.BrandsMapper;
import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;
import com.innovation.ic.sc.base.service.erp9_pvestandard.BrandsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class BrandsServiceImpl extends ServiceImpl<BrandsMapper, Brands> implements BrandsService {

    @Override
    public List<Brands> selectAllBrands() {
        Map<String, Object> paramMap =new HashMap<>();
        return this.baseMapper.selectByMap(paramMap);
    }
//
//    /**
//     * @return
//     * @description 查询列表(分页)
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/9 上午11:10
//     */
//    @Override
//    public ServiceResult<IPage<Brands>> page(int pageNo, int pageSize, String searchName) {
//        LambdaQueryWrapper<Brands> brandsLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        brandsLambdaQueryWrapper.likeRight(StringUtils.isNotEmpty(searchName), Brands::getCnName, searchName);
//        brandsLambdaQueryWrapper.orderByDesc(Brands::getId);
//
//        IPage<Brands> brandsPage = baseMapper.selectPage(new Page<>(pageNo, pageSize), brandsLambdaQueryWrapper);
//        return ServiceResult.ok(brandsPage, "查询品牌列表数据成功");
//    }
//
//    @Override
//    public ServiceResult<IPage<Brands>> findByPage(int pageNo, int pageSize, String searchName) {
//        LambdaQueryWrapper<Brands> brandsLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        brandsLambdaQueryWrapper.like(StringUtils.isNotEmpty(searchName), Brands::getName, searchName);
//        brandsLambdaQueryWrapper.orderByDesc(Brands::getCreateDate);
//        IPage<Brands> brandsPage = baseMapper.selectPage(new Page<>(pageNo, pageSize), brandsLambdaQueryWrapper);
//        return ServiceResult.ok(brandsPage, "查询品牌列表数据成功");
//    }
//
//    /**
//     * @param pageNo
//     * @param pageSize
//     * @param searchName
//     * @description 查询品牌列表
//     * @author Mr.myq
//     * @datetime 2022/8/5 下午5:42
//     */
//    @Override
//    public List<Brands> selectBrands(int pageNo, int pageSize, String searchName) {
//        return brandsMapper.selectBrands(new Page<>(pageNo, pageSize), searchName);
//    }
//
//
//    /**
//     * @return
//     * @description 查询品牌列表
//     * @parms
//     * @author Mr.myq
//     * @datetime 2022/8/8 下午3:35
//     */
//    @Override
//    public List<Brands> selectBrands(String searchName) {
//        LambdaQueryWrapper<Brands> brandsLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        brandsLambdaQueryWrapper.likeRight(StringUtils.isNotEmpty(searchName), Brands::getCnName, searchName);
//        brandsLambdaQueryWrapper.orderByDesc(Brands::getId);
//        return baseMapper.selectList(brandsLambdaQueryWrapper);
//    }
//
//
//    /**
//     * @description 查询品牌列表
//     * @parms
//     * @return
//     * @author Mr.myq
//     * @datetime 2022/8/9 上午11:44
//     */
//    @Override
//    public ServiceResult<List<Brands>> list(String searchName) {
//        return ServiceResult.ok(this.selectBrands(searchName),"查询品牌列表成功");
//    }
//
//    /**
//     * @description 通过品牌列表查询数据
//     * @parms
//     * @return
//     * @author Mr.myq
//     * @datetime 2022/8/9 下午4:04
//     */
//    @Override
//    public List<Brands> selectByProductIds(Set<String> brandIds) {
//        return CollectionUtils.isEmpty(brandIds) ? Collections.emptyList():baseMapper.selectBatchIds(brandIds);
//    }
}