package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.DefaultLadderPriceMapper;
import com.innovation.ic.sc.base.model.sc.DefaultLadderPrice;
import com.innovation.ic.sc.base.service.sc.DefaultLadderPriceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

/**
 * @desc   DefaultLadderPrice的具体实现类
 * @author linuo
 * @time   2022年8月23日11:33:45
 */
@Service
@Transactional
public class DefaultLadderPriceServiceImpl extends ServiceImpl<DefaultLadderPriceMapper, DefaultLadderPrice> implements DefaultLadderPriceService {

}