package com.innovation.ic.sc.base.service.sc;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.UsersTopView;
import com.innovation.ic.sc.base.model.sc.User;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.vo.UsersVo;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface UserService {

    /**
     * 查找所有User对象
     * @return
     */
    ServiceResult<List<User>> findAll();

    /**
     * 新增账号
     *
     * @param usersVo
     * @return
     */
    ServiceResult<String> saveUser(UsersVo usersVo,String id) throws IOException, ParseException;

    /**
     * 分页展示用户信息
     *
     * @param usersVo
     * @return
     */
    ServiceResult<List<User>> findUserPage(UsersVo usersVo);

    /**
     * 通过feign 传递账号密码 去第三方获取 token
     *
     * @param usersVo
     * @return
     */
    ServiceResult<AuthenticationUser>  findUser(UsersVo usersVo) throws IOException;

    ServiceResult<IPage<UsersPojo>> findSonUserPage(String userId, int current, int size);

    ServiceResult<List<User>> page(String userId, int pageSize, int pageNo, String userName,
                                   String realName, Integer position, String phone, Integer status);

    /**
     * 账号状态更改
     *
     * @param usersVo
     * @return
     */
    void updateStatus(UsersVo usersVo);

    /**
     * 修改密码
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> updatePassword(UsersVo usersVo, AuthenticationUser authenticationUser);
    /**
     * 修改手机号
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Map> updatePhone(UsersVo usersVo) throws IOException;

    /**
     * 先清空 MySQL中users主账号
     *
     * @param map
     * @return
     */
    void batchDelete(Map<String, Object> map);

    /**
     * 把sql server中数据同步到MySQL中user表中
     *
     * @param userList
     * @return
     */
    void insertAll(List<ParentUsersTopView> userList);

    /**
     *  把sql server中数据同步到MySQL中user表中 新
     * @param userList
     */
    void insertAllNew(List<ParentUsersTopView> userList);

    /**
     * 监听主账号新增/修改
     * @param usersTopView sqlservice中相关数据
     */
    void handleModelAddUsersMqMsg(UsersTopView usersTopView);

    /**
     * 监听主账号状态
     * @param usersTopView sqlservice中相关数据
     */
    void handleModelUpdateUsersMqMsg(UsersTopView usersTopView);

    /**
     * 用于erp定时任务查询子账号数据
     * @return
     */
    ServiceResult<List<User>> allSonUserPage(Integer pageNo, Integer pageSize , Date time);

    /**
     * 监听密码被重置
     * @param usersTopView usersTopView
     */
    void handleModelUpdatePassWordMqMsg(UsersTopView usersTopView);

    /**
     * 监听添加子账号数据
     *
     * @param usersTopView usersTopView
     */
    void handleModelAddSubUsersMqMsg(UsersTopView usersTopView);

    /**
     * 通过账号查询手机号
     *
     * @param usersVo
     * @return 返回手机号
     */
    ServiceResult<User> findPhone(UsersVo usersVo);

    /**
     * 忘记密码
     *
     *
     * @param usersVo
     *  @return
     */
    ServiceResult<Boolean> forgetPassword(UsersVo usersVo);

    /**
     * 重置密码
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> resetPassword(UsersVo usersVo);

    /**
     * 当前主账号下分页展示子账号信息
     *
     * @param usersVo
     * @return
     */
    ServiceResult<List<User>> pageUser(UsersVo usersVo);

    /**
     * 当前主账号下分页展示子账号信息
     *
     * @param usersVo
     * @return
     */
    ServiceResult<PageNationPojo<UserPagePojo>> pageUserNew(UsersVo usersVo);

    /**
     * 修改账号信息
     *
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> updateUser(UsersVo usersVo) throws IOException;

    /**
     * 根据id查询用户
     *
     * @param userId
     * @return
     */
    ServiceResult<User> findByUserId(String userId);

    /**
     * 获取所有主账号数据
     * @return 返回查询结果
     */
    ServiceResult<List<User>> getAllPrimaryAccountData();

    List<String> findByEnterpriseId(String enterpriseID);

    /**
     * 更新账号评分
     * @param id 主账号id
     * @param score 评分
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateScore(String id, BigDecimal score);

    /**
     * 根据fatherId 查询主账号或关联的子账号
     * @param fatherId
     * @return
     */
    List<User> findUserAllByFatherId(String fatherId);

    /**
     * 判断当前数据是否重复
     * @param userData
     * @param key
     * @return
     */
    ServiceResult<Boolean> findData(String userData,String key);

    /**
     * 调用erp那边接口校验验证码是否正确
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> verificationCode(UsersVo usersVo);

    /**
     * 判断当前密码数据是否重复
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> findPassWord(UsersVo usersVo) throws IOException;

    /**
     * 判断手机号是否不正确
     * @param usersVo
     * @return
     */
    ServiceResult<Boolean> findAccountPhone(UsersVo usersVo);

    /**
     * 通过主键获取用户详情
     * @param id
     * @return
     */
    ServiceResult<AuthenticationUser> queryUsers(String id);

    /**
     * 初始化用户相关所有数据
     * @return
     */
    void initUserData();

    /**
     * 初始化校验相关所有数据
     * @return
     */
    void initCheckData();
}