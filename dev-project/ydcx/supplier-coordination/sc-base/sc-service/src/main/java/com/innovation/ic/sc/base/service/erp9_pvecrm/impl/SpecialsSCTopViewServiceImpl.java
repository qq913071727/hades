package com.innovation.ic.sc.base.service.erp9_pvecrm.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.erp9_pvecrm.SpecialsSCTopViewMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageModelConstants;
import com.innovation.ic.sc.base.pojo.constant.model.DefaultUserConstants;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.erp9_pvecrm.SpecialsSCTopViewService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   SpecialsSCTopViewService的具体实现类
 * @author linuo
 * @time   2022年8月29日13:52:55
 */
@Service
@Transactional
public class SpecialsSCTopViewServiceImpl extends ServiceImpl<SpecialsSCTopViewMapper, SpecialsSCTopView> implements SpecialsSCTopViewService {
    @Resource
    private SpecialsSCTopViewMapper specialsSCTopViewMapper;

    /**
     * 获取erp库中优势型号数据
     * @return 返回erp库中优势型号数据
     */
    @Override
    public ServiceResult<List<SpecialsSCTopView>> getErpAdvantageModelData() {
        ServiceResult<List<SpecialsSCTopView>> serviceResult = new ServiceResult<>();

        QueryWrapper<SpecialsSCTopView> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(AdvantageModelConstants.STATUS_UPPER_FIELD, DefaultUserConstants.DEFAULT_STATUS);
        List<SpecialsSCTopView> list = specialsSCTopViewMapper.selectList(queryWrapper);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(list);
        return serviceResult;
    }
}