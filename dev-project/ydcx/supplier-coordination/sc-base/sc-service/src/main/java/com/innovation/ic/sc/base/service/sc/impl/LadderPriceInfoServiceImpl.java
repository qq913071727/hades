package com.innovation.ic.sc.base.service.sc.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.LadderPriceInfoMapper;
import com.innovation.ic.sc.base.model.sc.LadderPriceAmountConfig;
import com.innovation.ic.sc.base.model.sc.LadderPriceConfig;
import com.innovation.ic.sc.base.model.sc.LadderPriceInfo;
import com.innovation.ic.sc.base.pojo.constant.Constants;
import com.innovation.ic.sc.base.pojo.constant.model.LadderPriceConstants;
import com.innovation.ic.sc.base.pojo.enums.LadderPriceConfigTypeEnum;
import com.innovation.ic.sc.base.pojo.enums.LadderPriceCorrelationTypeEnum;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.*;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.LadderPriceInfoService;
import com.innovation.ic.sc.base.vo.ladderPrice.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc   LadderPriceInfo的具体实现类
 * @author linuo
 * @time   2022年9月19日15:50:49
 */
@Service
@Transactional
public class LadderPriceInfoServiceImpl extends ServiceImpl<LadderPriceInfoMapper, LadderPriceInfo> implements LadderPriceInfoService {
    private static final Logger log = LoggerFactory.getLogger(LadderPriceInfoServiceImpl.class);

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 阶梯价格数据查询
     * @param ladderPriceGetDataVo 阶梯价格数据获取的Vo类
     * @param authenticationUser 用户信息
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<LadderPriceDataRespPojo>> getLadderPriceData(LadderPriceGetDataVo ladderPriceGetDataVo, AuthenticationUser authenticationUser) {
        List<LadderPriceDataRespPojo> result = new ArrayList<>();

        // 查询阶梯价格配置数据
        QueryWrapper<LadderPriceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.ENTERPRISE_ID_FIELD, authenticationUser.getEpId());
        List<LadderPriceInfo> list = serviceHelper.getLadderPriceInfoMapper().selectList(queryWrapper);
        if(list != null && list.size() > 0){
            for(LadderPriceInfo ladderPriceInfo : list){
                // 配置方式(1:按起订量配置、2:按单价配置)
                Integer configType = ladderPriceInfo.getConfigType();
                if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode()){
                    // 获取阶梯价格数据
                    List<LadderPriceDataPojo> ladderPriceDataPojos = getLadderPriceDataPojos(ladderPriceGetDataVo.getUnitPrice(), ladderPriceInfo.getId());
                    LadderPriceDataRespPojo ladderPriceDataRespPojo = new LadderPriceDataRespPojo();
                    ladderPriceDataRespPojo.setId(ladderPriceInfo.getId());
                    ladderPriceDataRespPojo.setName(ladderPriceInfo.getName());
                    ladderPriceDataRespPojo.setLadderPriceConfigData(ladderPriceDataPojos);
                    result.add(ladderPriceDataRespPojo);
                }else{
                    // 判断查询条件中的币种是否与当前阶梯价格配置中的币种一致
                    Integer currency = ladderPriceGetDataVo.getCurrency();
                    if(currency != null && ladderPriceInfo.getCurrency() != null && currency.intValue() != ladderPriceInfo.getCurrency()){
                        continue;
                    }

                    // 查询阶梯价格单价配置数据
                    QueryWrapper<LadderPriceAmountConfig> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq(LadderPriceConstants.LADDER_PRICE_ID_FIELD, ladderPriceInfo.getId());
                    List<LadderPriceAmountConfig> ladderPriceAmountConfigs = serviceHelper.getLadderPriceAmountConfigMapper().selectList(queryWrapper1);
                    if(ladderPriceAmountConfigs != null && ladderPriceAmountConfigs.size() > 0){
                        for(LadderPriceAmountConfig ladderPriceAmountConfig : ladderPriceAmountConfigs){
                            BigDecimal minPrice = ladderPriceAmountConfig.getMinPrice();
                            BigDecimal maxPrice = ladderPriceAmountConfig.getMaxPrice();
                            BigDecimal price = ladderPriceGetDataVo.getUnitPrice();
                            if ((minPrice == null && price.compareTo(maxPrice) >= 0) || (minPrice != null && maxPrice != null && price.compareTo(minPrice) > 0 && price.compareTo(maxPrice) <= 0) || (minPrice != null && maxPrice == null && price.compareTo(minPrice) > 0)) {
                                // 获取阶梯价格数据
                                List<LadderPriceDataPojo> ladderPriceDataPojos = getLadderPriceDataPojos(ladderPriceGetDataVo.getUnitPrice(), ladderPriceAmountConfig.getId());
                                LadderPriceDataRespPojo ladderPriceDataRespPojo = new LadderPriceDataRespPojo();
                                ladderPriceDataRespPojo.setId(ladderPriceInfo.getId());
                                ladderPriceDataRespPojo.setName(ladderPriceInfo.getName());
                                ladderPriceDataRespPojo.setLadderPriceConfigData(ladderPriceDataPojos);
                                result.add(ladderPriceDataRespPojo);
                                break;
                            }
                        }
                    }
                }
            }
        }

        ServiceResult<List<LadderPriceDataRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 阶梯价格信息删除
     * @param ladderPriceId 阶梯价格信息id
     * @param epId 供应商id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Integer> delete(Integer ladderPriceId, String epId) {
        boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;
        int code = HttpStatus.INTERNAL_SERVER_ERROR.value();

        // 查询阶梯价格信息
        LadderPriceInfo ladderPriceInfo = serviceHelper.getLadderPriceInfoMapper().selectById(ladderPriceId);
        if(ladderPriceId == null){
            message = "当前阶梯价格配置已删除,请勿重复操作";
        }else{
            // 删除关联数据
            deleteCorrelationData(ladderPriceInfo.getConfigType(), ladderPriceId);

            // 删除阶梯价格数据
            int i = serviceHelper.getLadderPriceInfoMapper().deleteById(ladderPriceId);
            if(i > 0){
                log.info("删除阶梯价格数据成功");
                result = Boolean.TRUE;
                code = HttpStatus.OK.value();
                message = ServiceResult.DELETE_SUCCESS;
            }
        }

        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        serviceResult.setResult(code);
        serviceResult.setSuccess(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 判断阶梯价格是否被库存使用
     * @param id 阶梯价格id
     * @param epId 供应商id
     * @return 返回结果
     */
    @Override
    public ServiceResult<Boolean> judgeIfUsedByInventory(Integer id, String epId) {
        Boolean result = Boolean.FALSE;
        String message = null;

        // 查询当前阶梯价格信息是否被库存信息使用
        Long count = serviceHelper.getInventoryMapper().selectUseLadderPriceIdInventoryCount(id, epId);
        if(count > 0){
            result = Boolean.TRUE;
            message = "当前阶梯价格正在使用";
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setSuccess(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 按单价配置的阶梯价格信息列表查询
     * @param currency 币种(1:人民币、2:美元)
     * @param epId 供应商id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<JSONObject> queryLadderPriceConfigListByAmount(Integer currency, String epId) {
        List<LadderPriceInfoByAmountRespPojo> result = new ArrayList<>();

        // 查询阶梯价格配置信息
        QueryWrapper<LadderPriceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(LadderPriceConstants.CURRENCY_FIELD, currency);
        queryWrapper.eq(LadderPriceConstants.CONFIG_TYPE_FIELD, LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode());
        queryWrapper.eq(Constants.ENTERPRISE_ID_FIELD, epId);
        List<LadderPriceInfo> ladderPriceInfos = serviceHelper.getLadderPriceInfoMapper().selectList(queryWrapper);
        if(ladderPriceInfos != null && ladderPriceInfos.size() > 0){
            for(LadderPriceInfo ladderPriceInfo: ladderPriceInfos){
                LadderPriceInfoByAmountRespPojo ladderPriceInfoByAmountRespPojo = new LadderPriceInfoByAmountRespPojo();
                BeanUtils.copyProperties(ladderPriceInfo, ladderPriceInfoByAmountRespPojo);

                // 查询阶梯价格单价配置数据
                QueryWrapper<LadderPriceAmountConfig> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq(LadderPriceConstants.LADDER_PRICE_ID_FIELD, ladderPriceInfo.getId());
                List<LadderPriceAmountConfig> ladderPriceAmountConfigs = serviceHelper.getLadderPriceAmountConfigMapper().selectList(queryWrapper1);

                if(ladderPriceAmountConfigs != null && ladderPriceAmountConfigs.size() > 0){
                    // 按金额配置的数据
                    List<LadderPriceAmountConfigRespPojo> ladderPriceAmountConfigData = new ArrayList<>();
                    for(LadderPriceAmountConfig ladderPriceAmountConfig : ladderPriceAmountConfigs){
                        LadderPriceAmountConfigRespPojo ladderPriceAmountConfigRespPojo = new LadderPriceAmountConfigRespPojo();
                        BeanUtils.copyProperties(ladderPriceAmountConfig, ladderPriceAmountConfigRespPojo);

                        // 查询阶梯价格配置信息
                        List<LadderPriceConfigRespPojo> configVos = serviceHelper.getLadderPriceConfigMapper().selectLadderPriceConfigList(ladderPriceAmountConfig.getId());
                        if(configVos != null && configVos.size() > 0){
                            ladderPriceAmountConfigRespPojo.setLadderPriceConfig(configVos);
                        }
                        ladderPriceAmountConfigData.add(ladderPriceAmountConfigRespPojo);
                    }
                    ladderPriceInfoByAmountRespPojo.setLadderPriceAmountConfigData(ladderPriceAmountConfigData);
                }
                result.add(ladderPriceInfoByAmountRespPojo);
            }
        }

        // 组装返回数据
        JSONObject json = new JSONObject();
        if(ladderPriceInfos != null){
            json.put(Constants.COUNT, ladderPriceInfos.size());
        }else{
            json.put(Constants.COUNT, 0);
        }
        json.put(Constants.DATA_FIELD, result);

        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(json);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 按起订量批量配置库存时的阶梯价格信息查询
     * @param idList id集合
     * @param epId   供应商id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<LadderPriceInfoBatchSetByMoqRespPojo>> queryBatchSetLadderPriceConfigListByMoq(List<Integer> idList, String epId) {
        List<LadderPriceInfoBatchSetByMoqRespPojo> result = new ArrayList<>();

        // 根据id集合获取最小起订量的最大值
        BigDecimal maxMoq = serviceHelper.getInventoryMapper().getMaxMoqByIdList(idList);

        // 查询阶梯价格配置信息
        QueryWrapper<LadderPriceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(LadderPriceConstants.CONFIG_TYPE_FIELD, LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode());
        queryWrapper.eq(Constants.ENTERPRISE_ID_FIELD, epId);
        List<LadderPriceInfo> ladderPriceInfos = serviceHelper.getLadderPriceInfoMapper().selectList(queryWrapper);
        if(ladderPriceInfos != null && ladderPriceInfos.size() > 0){
            for(LadderPriceInfo ladderPriceInfo : ladderPriceInfos){
                LadderPriceInfoBatchSetByMoqRespPojo ladderPriceInfoBatchSetByMoqRespPojo = new LadderPriceInfoBatchSetByMoqRespPojo();
                BeanUtils.copyProperties(ladderPriceInfo, ladderPriceInfoBatchSetByMoqRespPojo);

                // 是否可以返回
                boolean ifReturn = Boolean.FALSE;

                // 查询阶梯价格配置信息
                List<LadderPriceConfigRespPojo> configVos = serviceHelper.getLadderPriceConfigMapper().selectLadderPriceConfigList(ladderPriceInfo.getId());
                if(configVos != null && configVos.size() > 0){
                    if(maxMoq != null && maxMoq.compareTo(new BigDecimal(configVos.get(0).getMq())) >= 0){
                        ifReturn = Boolean.TRUE;
                        ladderPriceInfoBatchSetByMoqRespPojo.setLadderPriceConfigData(configVos);
                    }

                    if(maxMoq == null){
                        ifReturn = Boolean.TRUE;
                        ladderPriceInfoBatchSetByMoqRespPojo.setLadderPriceConfigData(configVos);
                    }
                }

                if(ifReturn){
                    result.add(ladderPriceInfoBatchSetByMoqRespPojo);
                }
            }
        }

        ServiceResult<List<LadderPriceInfoBatchSetByMoqRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 查询全部阶梯价格配置信息
     * @param epId 供应商id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<LadderPriceBasicInfoRespPojo>> queryAllLadderPriceInfo(String epId) {
        List<LadderPriceBasicInfoRespPojo> result = serviceHelper.getLadderPriceInfoMapper().queryAllLadderPriceInfo(epId);

        ServiceResult<List<LadderPriceBasicInfoRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 按单价批量配置库存时的阶梯价格信息查询
     * @param ladderPriceBatchSetByAmountVo 通过单价批量配置阶梯价格的Vo类
     * @param epId 供应商id
     * @return 返回阶梯价格列表查询结果
     */
    @Override
    public ServiceResult<List<LadderPriceInfoBatchSetByAmountRespPojo>> queryBatchSetLadderPriceConfigListByAmount(LadderPriceBatchSetByAmountVo ladderPriceBatchSetByAmountVo, String epId) {
        List<LadderPriceInfoBatchSetByAmountRespPojo> result = new ArrayList<>();

        List<Integer> idList = ladderPriceBatchSetByAmountVo.getIdList();

        // 根据id集合获取最小起订量的最大值
        BigDecimal maxMoq = serviceHelper.getInventoryMapper().getMaxMoqByIdList(idList);

        // 查询阶梯价格配置信息
        QueryWrapper<LadderPriceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(LadderPriceConstants.CONFIG_TYPE_FIELD, LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode());
        queryWrapper.eq(LadderPriceConstants.CURRENCY_FIELD, ladderPriceBatchSetByAmountVo.getCurrency());
        queryWrapper.eq(Constants.ENTERPRISE_ID_FIELD, epId);
        List<LadderPriceInfo> ladderPriceInfos = serviceHelper.getLadderPriceInfoMapper().selectList(queryWrapper);
        if(ladderPriceInfos != null && ladderPriceInfos.size() > 0){
            for(LadderPriceInfo ladderPriceInfo: ladderPriceInfos){
                LadderPriceInfoBatchSetByAmountRespPojo ladderPriceInfoBatchSetByAmountRespPojo = new LadderPriceInfoBatchSetByAmountRespPojo();
                ladderPriceInfoBatchSetByAmountRespPojo.setId(ladderPriceInfo.getId());
                ladderPriceInfoBatchSetByAmountRespPojo.setName(ladderPriceInfo.getName());

                // 是否可以返回
                boolean ifReturn = Boolean.FALSE;

                // 查询阶梯价格单价配置数据
                QueryWrapper<LadderPriceAmountConfig> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq(LadderPriceConstants.LADDER_PRICE_ID_FIELD, ladderPriceInfo.getId());
                queryWrapper1.orderByAsc();
                List<LadderPriceAmountConfig> ladderPriceAmountConfigs = serviceHelper.getLadderPriceAmountConfigMapper().selectList(queryWrapper1);
                if(ladderPriceAmountConfigs != null && ladderPriceAmountConfigs.size() > 0){
                    List<JSONObject> ladderPriceConfigData = new ArrayList<>();
                    for(LadderPriceAmountConfig ladderPriceAmountConfig : ladderPriceAmountConfigs){
                        // 查询阶梯价格配置信息
                        List<LadderPriceConfigRespPojo> configVos = serviceHelper.getLadderPriceConfigMapper().selectLadderPriceConfigList(ladderPriceAmountConfig.getId());
                        if(configVos != null && configVos.size() > 0){
                            if(ifReturn || (maxMoq != null && maxMoq.compareTo(new BigDecimal(configVos.get(0).getMq())) >= 0)){
                                ifReturn = Boolean.TRUE;
                                for(LadderPriceConfigRespPojo ladderPriceConfigRespPojo : configVos){
                                    JSONObject json = new JSONObject();
                                    json.put(LadderPriceConstants.MIN_PRICE, ladderPriceAmountConfig.getMinPrice());
                                    json.put(LadderPriceConstants.MAX_PRICE, ladderPriceAmountConfig.getMaxPrice());
                                    json.put(LadderPriceConstants.MQ_FIELD, ladderPriceConfigRespPojo.getMq());
                                    json.put(LadderPriceConstants.LOWER_FIELD, ladderPriceConfigRespPojo.getLower());
                                    ladderPriceConfigData.add(json);
                                }
                            }

                            if(maxMoq == null){
                                ifReturn = Boolean.TRUE;
                                for(LadderPriceConfigRespPojo ladderPriceConfigRespPojo : configVos){
                                    JSONObject json = new JSONObject();
                                    json.put(LadderPriceConstants.MIN_PRICE, ladderPriceAmountConfig.getMinPrice());
                                    json.put(LadderPriceConstants.MAX_PRICE, ladderPriceAmountConfig.getMaxPrice());
                                    json.put(LadderPriceConstants.MQ_FIELD, ladderPriceConfigRespPojo.getMq());
                                    json.put(LadderPriceConstants.LOWER_FIELD, ladderPriceConfigRespPojo.getLower());
                                    ladderPriceConfigData.add(json);
                                }
                            }

                        }
                    }
                    ladderPriceInfoBatchSetByAmountRespPojo.setLadderPriceConfigData(ladderPriceConfigData);
                }

                if(ifReturn){
                    result.add(ladderPriceInfoBatchSetByAmountRespPojo);
                }
            }
        }

        ServiceResult<List<LadderPriceInfoBatchSetByAmountRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 按起订量配置的阶梯价格信息列表查询
     * @param epId 供应商id
     * @return 返回阶梯价格列表查询结果
     */
    @Override
    public ServiceResult<JSONObject> queryLadderPriceConfigListByMoq(String epId) {
        List<LadderPriceInfoByMoqRespPojo> result = new ArrayList<>();

        // 查询阶梯价格配置信息
        QueryWrapper<LadderPriceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(LadderPriceConstants.CONFIG_TYPE_FIELD, LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode());
        queryWrapper.eq(Constants.ENTERPRISE_ID_FIELD, epId);
        List<LadderPriceInfo> ladderPriceInfos = serviceHelper.getLadderPriceInfoMapper().selectList(queryWrapper);
        if(ladderPriceInfos != null && ladderPriceInfos.size() > 0){
            for(LadderPriceInfo ladderPriceInfo: ladderPriceInfos){
                LadderPriceInfoByMoqRespPojo ladderPriceInfoByMoqRespPojo = new LadderPriceInfoByMoqRespPojo();
                ladderPriceInfoByMoqRespPojo.setId(ladderPriceInfo.getId());
                ladderPriceInfoByMoqRespPojo.setName(ladderPriceInfo.getName());

                // 查询阶梯价格配置信息
                List<LadderPriceConfigRespPojo> configVos = serviceHelper.getLadderPriceConfigMapper().selectLadderPriceConfigList(ladderPriceInfo.getId());
                if(configVos != null && configVos.size() > 0){
                    ladderPriceInfoByMoqRespPojo.setLadderPriceConfigData(configVos);
                }
                result.add(ladderPriceInfoByMoqRespPojo);
            }
        }

        // 组装返回数据
        JSONObject json = new JSONObject();
        if(ladderPriceInfos != null){
            json.put(Constants.COUNT, ladderPriceInfos.size());
        }else{
            json.put(Constants.COUNT, 0);
        }
        json.put(Constants.DATA_FIELD, result);

        ServiceResult<JSONObject> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(json);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 阶梯价格新增
     * @param ladderPriceAddVo   阶梯价格新增接口的Vo类
     * @param authenticationUser 用户信息
     * @return 返回新增接口
     */
    @Override
    public ServiceResult<Boolean> add(LadderPriceAddVo ladderPriceAddVo, AuthenticationUser authenticationUser) {
        Boolean result = Boolean.FALSE;

        // 保存阶梯价格信息
        LadderPriceInfo ladderPriceInfo = new LadderPriceInfo();
        BeanUtils.copyProperties(ladderPriceAddVo, ladderPriceInfo);
        ladderPriceInfo.setEnterpriseId(authenticationUser.getEpId());
        int insert = serviceHelper.getLadderPriceInfoMapper().insert(ladderPriceInfo);
        if(insert > 0){
            log.info("阶梯价格新增成功,阶梯价格id为[{}]", ladderPriceInfo.getId());

            // 保存阶梯价格配置信息
            saveLadderPriceConfig(ladderPriceAddVo, ladderPriceInfo);

            result = Boolean.TRUE;
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(result);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 阶梯价格修改
     * @param ladderPriceEditVo 阶梯价格修改接口的Vo类
     * @param epId 供应商id
     * @return 返回修改结果
     */
    @Override
    public ServiceResult<Boolean> edit(LadderPriceEditVo ladderPriceEditVo, String epId) {
        Boolean result = Boolean.FALSE;

        // 更新阶梯价格信息
        UpdateWrapper<LadderPriceInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.ID, ladderPriceEditVo.getId());

        LadderPriceInfo ladderPriceInfo = new LadderPriceInfo();
        BeanUtils.copyProperties(ladderPriceEditVo, ladderPriceInfo);

        int update = serviceHelper.getLadderPriceInfoMapper().update(ladderPriceInfo, updateWrapper);
        if(update > 0){
            log.info("更新阶梯价格信息成功");

            // 查询历史数据
            LadderPriceInfo historyData = serviceHelper.getLadderPriceInfoMapper().selectById(ladderPriceEditVo.getId());

            // 删除关联数据
            deleteCorrelationData(historyData.getConfigType(), ladderPriceEditVo.getId());

            // 添加阶梯价格配置数据
            LadderPriceAddVo ladderPriceAddVo = new LadderPriceAddVo();
            BeanUtils.copyProperties(ladderPriceEditVo, ladderPriceAddVo);
            saveLadderPriceConfig(ladderPriceAddVo, historyData);

            result = Boolean.TRUE;
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(result);
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return serviceResult;
    }

    /**
     * 阶梯价格信息查询
     * @param id 阶梯价格信息id
     * @return 返回阶梯价格查询结果
     */
    @Override
    public ServiceResult<LadderPriceInfoRespPojo> info(Integer id) {
        // 查询阶梯价格信息
        LadderPriceInfo ladderPriceInfo = serviceHelper.getLadderPriceInfoMapper().selectById(id);
        if(ladderPriceInfo == null){
            ServiceResult<LadderPriceInfoRespPojo> serviceResult = new ServiceResult<>();
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("阶梯价格信息查询失败");
            return serviceResult;
        }

        LadderPriceInfoRespPojo ladderPriceInfoRespPojo = new LadderPriceInfoRespPojo();
        BeanUtils.copyProperties(ladderPriceInfo, ladderPriceInfoRespPojo);

        Integer configType = ladderPriceInfo.getConfigType();

        // 按起订量配置
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode()){
            // 查询阶梯价格配置信息
            List<LadderPriceConfigRespPojo> configVos = serviceHelper.getLadderPriceConfigMapper().selectLadderPriceConfigList(id);
            ladderPriceInfoRespPojo.setLadderPriceConfigByMoq(configVos);
        }

        // 按单价配置
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode()){
            QueryWrapper<LadderPriceAmountConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(Constants.LADDER_PRICE_ID_FIELD, id);
            List<LadderPriceAmountConfig> list = serviceHelper.getLadderPriceAmountConfigMapper().selectList(queryWrapper);
            if(list != null && list.size() > 0){
                List<LadderPriceAmountConfigRespPojo> ladderPriceAmountConfigData = new ArrayList<>();
                for(LadderPriceAmountConfig ladderPriceAmountConfig : list){
                    LadderPriceAmountConfigRespPojo ladderPriceAmountConfigRespPojo = new LadderPriceAmountConfigRespPojo();
                    BeanUtils.copyProperties(ladderPriceAmountConfig, ladderPriceAmountConfigRespPojo);

                    // 查询阶梯价格配置信息
                    List<LadderPriceConfigRespPojo> ladderPriceConfig = serviceHelper.getLadderPriceConfigMapper().selectLadderPriceConfigList(ladderPriceAmountConfig.getId());
                    ladderPriceAmountConfigRespPojo.setLadderPriceConfig(ladderPriceConfig);
                    ladderPriceAmountConfigData.add(ladderPriceAmountConfigRespPojo);
                }
                ladderPriceInfoRespPojo.setLadderPriceConfigByAmount(ladderPriceAmountConfigData);
            }
        }

        ServiceResult<LadderPriceInfoRespPojo> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(ladderPriceInfoRespPojo);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 获取阶梯价格数据
     * @param unitPrice 单价
     * @param id 阶梯价格id
     * @return 返回结果
     */
    private List<LadderPriceDataPojo> getLadderPriceDataPojos(BigDecimal unitPrice, Integer id){
        List<LadderPriceDataPojo> result = new ArrayList<>();

        QueryWrapper<LadderPriceConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(LadderPriceConstants.CORRELATION_ID_FIELD, id);
        queryWrapper.orderByAsc(Constants.MQ_FIELD);
        List<LadderPriceConfig> ladderPriceConfigs = serviceHelper.getLadderPriceConfigMapper().selectList(queryWrapper);
        if(ladderPriceConfigs != null && ladderPriceConfigs.size() > 0){
            for(LadderPriceConfig ladderPriceConfig : ladderPriceConfigs){
                LadderPriceDataPojo ladderPriceDataPojo = new LadderPriceDataPojo();
                ladderPriceDataPojo.setMq(ladderPriceConfig.getMq());

                // 根据降比计算价格
                BigDecimal price = serviceHelper.getPriceCalculateHandler().calculateUnitPriceByLower(unitPrice, ladderPriceConfig.getLower());
                ladderPriceDataPojo.setPrice(price);

                result.add(ladderPriceDataPojo);
            }
        }

        return result;
    }

    /**
     * 删除关联数据
     * @param configType 配置方式(1:按起订量配置、2:按单价配置)
     * @param ladderPriceId 阶梯价格id
     */
    private void deleteCorrelationData(Integer configType, Integer ladderPriceId){
        // 按起订量配置
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode()){
            // 删除阶梯价格配置数据
            UpdateWrapper<LadderPriceConfig> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq(LadderPriceConstants.CORRELATION_ID_FIELD, ladderPriceId);
            int delete = serviceHelper.getLadderPriceConfigMapper().delete(updateWrapper);
            if(delete > 0){
                log.info("删除阶梯价格配置数据成功");
            }
        }

        // 按单价配置
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode()){
            // 查询阶梯价格单价配置数据
            QueryWrapper<LadderPriceAmountConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(LadderPriceConstants.LADDER_PRICE_ID_FIELD, ladderPriceId);
            List<LadderPriceAmountConfig> ladderPriceAmountConfigs = serviceHelper.getLadderPriceAmountConfigMapper().selectList(queryWrapper);
            if(ladderPriceAmountConfigs != null && ladderPriceAmountConfigs.size() > 0){
                for(LadderPriceAmountConfig ladderPriceAmountConfig : ladderPriceAmountConfigs){
                    Integer id = ladderPriceAmountConfig.getId();
                    // 删除阶梯价格配置数据
                    UpdateWrapper<LadderPriceConfig> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq(LadderPriceConstants.CORRELATION_ID_FIELD, id);
                    int delete = serviceHelper.getLadderPriceConfigMapper().delete(updateWrapper);
                    if(delete > 0){
                        log.info("删除阶梯价格配置数据成功");
                    }

                    // 删除阶梯价格单价配置数据
                    int i = serviceHelper.getLadderPriceAmountConfigMapper().deleteById(ladderPriceAmountConfig.getId());
                    if(i > 0){
                        log.info("删除阶梯价格单价配置数据成功");
                    }
                }
            }
        }
    }

    /**
     * 保存阶梯价格配置信息
     * @param ladderPriceAddVo 阶梯价格新增接口的Vo类
     * @param ladderPriceInfo 阶梯价格信息
     */
    private void saveLadderPriceConfig(LadderPriceAddVo ladderPriceAddVo, LadderPriceInfo ladderPriceInfo) {
        Integer configType = ladderPriceAddVo.getConfigType();

        // 按起订量配置
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_MOQ.getCode()){
            List<LadderPriceConfigVo> ladderPriceConfigByMoq = ladderPriceAddVo.getLadderPriceConfigByMoq();
            if(ladderPriceConfigByMoq != null && ladderPriceConfigByMoq.size() > 0){
                List<LadderPriceConfig> addData = new ArrayList<>();

                for(LadderPriceConfigVo ladderPriceConfigVo : ladderPriceConfigByMoq){
                    LadderPriceConfig ladderPriceConfig = new LadderPriceConfig();
                    BeanUtils.copyProperties(ladderPriceConfigVo, ladderPriceConfig);
                    ladderPriceConfig.setCorrelationId(ladderPriceInfo.getId());
                    ladderPriceConfig.setCorrelationType(LadderPriceCorrelationTypeEnum.CORRELATION_BY_MOQ.getCode());
                    ladderPriceConfig.setEnterpriseId(ladderPriceInfo.getEnterpriseId());
                    addData.add(ladderPriceConfig);
                }
                Integer integer = serviceHelper.getLadderPriceConfigMapper().insertBatchSomeColumn(addData);
                if(integer > 0){
                    log.info("阶梯价格配置信息保存成功");
                }
            }else{
                log.info("保存阶梯价格配置信息时出现问题,按起订量配置的数据为空,无法保存阶梯价格配置数据,请确认参数后再进行重试操作");
            }
        }

        // 按单价配置
        if(configType == LadderPriceConfigTypeEnum.CONFIG_BY_AMOUNT.getCode()){
            List<LadderPriceAmountConfigVo> ladderPriceConfigByAmount = ladderPriceAddVo.getLadderPriceConfigByAmount();
            if(ladderPriceConfigByAmount != null && ladderPriceConfigByAmount.size() > 0){
                for (LadderPriceAmountConfigVo ladderPriceAmountConfigVo : ladderPriceConfigByAmount) {
                    LadderPriceAmountConfig ladderPriceAmountConfig = new LadderPriceAmountConfig();
                    ladderPriceAmountConfig.setLadderPriceId(ladderPriceInfo.getId());
                    BeanUtils.copyProperties(ladderPriceAmountConfigVo, ladderPriceAmountConfig);
                    ladderPriceAmountConfig.setEnterpriseId(ladderPriceInfo.getEnterpriseId());

                    int insert = serviceHelper.getLadderPriceAmountConfigMapper().insert(ladderPriceAmountConfig);
                    if (insert > 0) {
                        log.info("阶梯价格单价配置数据保存成功,数据id:[{}]", ladderPriceAmountConfig.getId());

                        List<LadderPriceConfigVo> ladderPriceConfig = ladderPriceAmountConfigVo.getLadderPriceConfig();
                        if (ladderPriceConfig != null && ladderPriceConfig.size() > 0) {
                            List<LadderPriceConfig> addData = new ArrayList<>();
                            for (LadderPriceConfigVo ladderPriceConfigVo : ladderPriceConfig) {
                                LadderPriceConfig ladderPriceConfig1 = new LadderPriceConfig();
                                BeanUtils.copyProperties(ladderPriceConfigVo, ladderPriceConfig1);
                                ladderPriceConfig1.setCorrelationType(LadderPriceCorrelationTypeEnum.CORRELATION_BY_AMOUNT.getCode());
                                ladderPriceConfig1.setCorrelationId(ladderPriceAmountConfig.getId());
                                ladderPriceConfig1.setEnterpriseId(ladderPriceAmountConfig.getEnterpriseId());
                                addData.add(ladderPriceConfig1);
                            }
                            Integer integer = serviceHelper.getLadderPriceConfigMapper().insertBatchSomeColumn(addData);
                            if (integer > 0) {
                                log.info("阶梯价格配置信息保存成功");
                            }
                        } else {
                            log.info("保存阶梯价格配置信息时出现问题,按单价配置的数据为空,无法保存阶梯价格配置数据,请确认参数后再进行重试操作");
                        }
                    }
                }
            }
        }
    }
}