package com.innovation.ic.sc.base.service.sc.impl;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.mapper.sc.AdvantageBrandMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import com.innovation.ic.sc.base.model.sc.*;
import com.innovation.ic.sc.base.pojo.constant.Constants;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageBrandConstants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageModelConstants;
import com.innovation.ic.sc.base.pojo.enums.*;
import com.innovation.ic.sc.base.pojo.variable.*;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.ActionMessageService;
import com.innovation.ic.sc.base.service.sc.AdvantageBrandService;
import com.innovation.ic.sc.base.service.sc.UploadFileService;
import com.innovation.ic.sc.base.service.sc.UserService;
import com.innovation.ic.sc.base.vo.AdvantageBrandAddVo;
import com.innovation.ic.sc.base.vo.FileVo;
import com.innovation.ic.sc.base.vo.QueryAdvantageBrandAddVo;
import com.innovation.ic.sc.base.vo.UpdateAllAdvantageBrandVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author linuo
 * @desc AdvantageBrandService的具体实现类
 * @time 2022年9月1日16:39:34
 */
@Service
@Transactional
@Slf4j
public class AdvantageBrandServiceImpl extends ServiceImpl<AdvantageBrandMapper, AdvantageBrand> implements AdvantageBrandService {
    @Resource
    private ServiceHelper serviceHelper;

    @Resource
    private UploadFileService uploadFileService;

    @Resource
    private UserService userService;

    @Resource
    private ActionMessageService actionMessageService;


    /**
     * 更新优势品牌生效状态
     *
     * @param map 参数
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Boolean> updateStatusByParam(Map<String, Object> map) throws IOException {
        Boolean result = Boolean.FALSE;
        String message = ServiceResult.UPDATE_FAIL;

        int i = serviceHelper.getAdvantageBrandMapper().updateStatusByParam(map);
        if (i > 0) {
            result = Boolean.TRUE;
            message = ServiceResult.UPDATE_SUCCESS;

            String brand = (String) map.get(AdvantageModelConstants.BRAND_FIELD);
            Integer status = (Integer) map.get(AdvantageModelConstants.ADVANTAGE_STATUS);
            String enterpriseId = (String) map.get(Constants.ENTERPRISE_ID);

            if (status.intValue() == InventoryOperateTypeEnum.DOWN_SHELVES.getCode().intValue()) {
                // 库存下架 -> 需要取消优势品牌推送mq消息
                AdvantageBrand advantageBrand = new AdvantageBrand();
                advantageBrand.setBrand(brand);
                advantageBrand.setEnterpriseId(enterpriseId);
                sendCancelAdvantageBrand(advantageBrand);
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setSuccess(result);
        serviceResult.setMessage(message);
        return serviceResult;
    }

    /**
     * 处理优势品牌通知的mq消息
     *
     * @param brand        品牌
     * @param enterpriseId 供应商id
     * @param id           erpId
     * @return 返回处理结果
     */
    @Override
    public ServiceResult<Boolean> handleAdvantageModelNotifyMqMsg(String brand, String enterpriseId, String id) {
        Boolean result = Boolean.FALSE;

        // 根据条件查询优势品牌数据id集合
        Map<String, Object> map = new HashMap<>();
        map.put(AdvantageBrandConstants.BRAND_FIELD, brand);
        map.put(Constants.ENTERPRISE_ID, enterpriseId);
        List<Integer> idList = serviceHelper.getAdvantageBrandMapper().selectIdListByParam(map);
        if (idList != null && !idList.isEmpty()) {
            UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
            updateWrapper.in(Constants.ID, idList);

            AdvantageBrand advantageBrand = new AdvantageBrand();
            advantageBrand.setErpId(id);

            int update = serviceHelper.getAdvantageBrandMapper().update(advantageBrand, updateWrapper);
            if (update > 0) {
                log.info("优势品牌数据补全erpId成功,id:[{}]", idList);
                result = Boolean.TRUE;
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setSuccess(result);
        serviceResult.setResult(result);
        return serviceResult;
    }

    @Override
    public ServiceResult addAdvantageBrand(AdvantageBrandAddVo advantageBrandAddVo, AuthenticationUser authenticationUser) throws IOException {
        ServiceResult serviceResult = new ServiceResult();
        //添加的时候校验
        //1、当前供应商下是否有,待审核或者审核通过和正常状态
        //待审核状态
        Map<String, Object> param = new HashMap<>();
        param.put("enterprise_id", authenticationUser.getEpId());
        param.put("audit_status", AuditStatusEnum.PENDING.getScStatusCode());
        param.put("brand", advantageBrandAddVo.getBrand());
        List<AdvantageBrand> advantageBrands = this.baseMapper.selectByMap(param);
        if (advantageBrandAddVo != null && advantageBrands.size() > 0) {
            serviceResult.setMessage("当前品牌存在待审核状态记录,添加失败");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        //审核状态为正常,且优势状态为正常
        param.put("audit_status", AuditStatusEnum.APPROVE.getScStatusCode());
        param.put("advantage_status", AdvantageStatusEnum.EFFECTIVE.getScStatusCode());
        List<AdvantageBrand> advantageBrandList = this.baseMapper.selectByMap(param);
        if (advantageBrandList != null && advantageBrandList.size() > 0) {
            serviceResult.setMessage("当前品牌存在审核成功且有效记录,添加失败");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        if (advantageBrandAddVo.getDominantNature().equals(AdvantageBrandNatureEnum.AGENT.getCode())) {
            advantageBrandAddVo.setCertificateValidityStart(new Date());
            advantageBrandAddVo.setCertificateValidityEnd(agentDate());
        }

        int relationId = bindAdvantageBrand(advantageBrandAddVo, authenticationUser);
        //回填文件表状态
        List<String> relationPictureIds = advantageBrandAddVo.getRelationPictureIds();
        if (!CollectionUtils.isEmpty(relationPictureIds)) {
            uploadFileService.updateRelationId(relationPictureIds, UploadFileEnum.ADVANTAGE_BRAND, relationId);
        }
        //推送数据,
        this.sendAddAdvantageBrand(advantageBrandAddVo, authenticationUser);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("添加成功");
        return serviceResult;
    }

    /**
     * 推送添加优势品牌MQ消息
     *
     * @param advantageBrandAddVo
     * @throws IOException
     */
    private void sendAddAdvantageBrand(AdvantageBrandAddVo advantageBrandAddVo, AuthenticationUser authenticationUser) throws IOException {
        SpecialsSCTopView specialsSCTopView = new SpecialsSCTopView();
        specialsSCTopView.setEnterpriseID(authenticationUser.getEpId());
        specialsSCTopView.setType(advantageBrandAddVo.getDominantNature());
        specialsSCTopView.setBrand(advantageBrandAddVo.getBrand());
        //specialsSCTopView.setEndDateString(DateUtils.formatDate(advantageBrandAddVo.getCertificateValidityEnd(), DateUtils.DEFAULT_FORMAT));
        specialsSCTopView.setEndDate(advantageBrandAddVo.getCertificateValidityEnd());

        //specialsSCTopView.setStartDateString(DateUtils.formatDate(advantageBrandAddVo.getCertificateValidityStart(), DateUtils.DEFAULT_FORMAT));
        specialsSCTopView.setStartDate(advantageBrandAddVo.getCertificateValidityStart());

        specialsSCTopView.setStatus(AuditStatusEnum.PENDING.getErpStatusCode());
        specialsSCTopView.setCreator(authenticationUser.getUsername());
        //specialsSCTopView.setSummary(advantageBrandAddVo.getRemark());
        List<UploadFileMessagePojo> files = new ArrayList<>();
        List<String> relationPictureIds = advantageBrandAddVo.getRelationPictureIds();
        if (!CollectionUtils.isEmpty(relationPictureIds)) {
            ServiceResult<List<UploadFile>> serviceResult = uploadFileService.findByIds(relationPictureIds);
            if (serviceResult != null && !CollectionUtils.isEmpty(serviceResult.getResult())) {
                List<UploadFile> uploadFileLisr = serviceResult.getResult();
                for (UploadFile uploadFile : uploadFileLisr) {
                    UploadFileMessagePojo uploadFileMessagePojo = new UploadFileMessagePojo();
                    uploadFileMessagePojo.setUrl(uploadFile.getFullPath());
                    uploadFileMessagePojo.setFileName(uploadFile.getFileName());
                    files.add(uploadFileMessagePojo);
                }
            }
        }
        specialsSCTopView.setFileVoList(files);
        JSONObject json = (JSONObject) JSONObject.toJSON(specialsSCTopView);
        // 包装发送到rabbitmq的消息内容
        JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_ADD_QUEUE);
        log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_BRAND_ADD_QUEUE, result.toString());
        serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_CRM_QUEUE_NAME, RabbitMqConstants.SC_ERP_BRAND_ADD_QUEUE, null, result.toString().getBytes());
    }

    /**
     * 绑定参数
     *
     * @param advantageBrandAddVo
     * @param authenticationUser
     * @return
     */
    private int bindAdvantageBrand(AdvantageBrandAddVo advantageBrandAddVo, AuthenticationUser authenticationUser) {
        //目标
        AdvantageBrand advantageBrand = new AdvantageBrand();
        BeanUtils.copyProperties(advantageBrandAddVo, advantageBrand);
        advantageBrand.setCreateUserId(authenticationUser.getId());
        advantageBrand.setCreateUser(authenticationUser.getUsername());
        advantageBrand.setCreateDate(new Date());
        advantageBrand.setAuditStatus(AuditStatusEnum.PENDING.getScStatusCode());
        advantageBrand.setAdvantageStatus(AdvantageStatusEnum.INVALID.getScStatusCode());
        advantageBrand.setEnterpriseId(authenticationUser.getEpId());
        advantageBrand.setCertificateValidityStart(advantageBrandAddVo.getCertificateValidityStart());
        advantageBrand.setCertificateValidityEnd(advantageBrandAddVo.getCertificateValidityEnd());
        this.baseMapper.insert(advantageBrand);
        return advantageBrand.getId();
    }

    @Override
    public void deleteAdvantageBrand(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) throws IOException {
        String[] byIds = this.findByIds(updateAllAdvantageBrandVo);
        if (byIds == null || byIds.length == 0) {
            return;
        }
        for (String advantageBrandId : byIds) {
            AdvantageBrand advantageBrand = this.baseMapper.selectById(advantageBrandId);
            if (advantageBrand == null) {
                return;
            }
            this.sendDeleteAdvantageBrand(advantageBrandId);
            this.baseMapper.deleteById(advantageBrandId);
        }
    }

    /**
     * 删除优势品牌
     *
     * @param ids
     */
    private void sendDeleteAdvantageBrand(String ids) throws IOException {
        if (!StringUtils.validateParameter(ids)) {
            return;
        }
        List<AdvantageBrand> advantageBrands = this.baseMapper.selectBatchIds(Arrays.asList(ids.split(",")));
        for (AdvantageBrand advantageBrand : advantageBrands) {
/*
            SpecialsSCTopView specialsSCTopView = new SpecialsSCTopView();
            specialsSCTopView.setEnterpriseID(advantageBrand.getEnterpriseId());
            specialsSCTopView.setBrand(advantageBrand.getBrand());
*/

            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("ID", advantageBrand.getErpId());
            JSONObject json = (JSONObject) JSONObject.toJSON(paramMap);
            // 包装发送到rabbitmq的消息内容
            JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_UPDATE_SPECIAL_QUEUE);
            log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_BRAND_DEL_QUEUE, result.toString());
            serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_CRM_QUEUE_NAME, RabbitMqConstants.SC_ERP_BRAND_DEL_QUEUE, null, result.toString().getBytes());
        }
    }

    @Override
    public void cancelAdvantageBrand(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) throws IOException {
        String[] byIds = this.findByIds(updateAllAdvantageBrandVo);
        if (byIds == null || byIds.length == 0) {
            return;
        }
        for (String advantageBrandId : byIds) {
            AdvantageBrand advantageBrand = this.baseMapper.selectById(advantageBrandId);
            //品牌已经失效的，跳过
            if (!AdvantageStatusEnum.EFFECTIVE.getScStatusCode().equals(advantageBrand.getAdvantageStatus())) {
                continue;
            }
            UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("advantage_status", AdvantageStatusEnum.INVALID.getScStatusCode());
            updateWrapper.eq("id", advantageBrandId);
            this.update(updateWrapper);
            //发送删除优势品牌消息,所有类型都要发送
            this.sendCancelAdvantageBrand(advantageBrand);
        }
    }

    /**
     * 取消优势品牌
     *
     * @param advantageBrand
     */
    private void sendCancelAdvantageBrand(AdvantageBrand advantageBrand) throws IOException {
       /* SpecialsSCTopView specialsSCTopView = new SpecialsSCTopView();
        specialsSCTopView.setId(advantageBrand.getEnterpriseId());
        specialsSCTopView.setBrand(advantageBrand.getBrand());*/
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("ID", advantageBrand.getErpId());
        JSONObject json = (JSONObject) JSONObject.toJSON(paramMap);
        // 包装发送到rabbitmq的消息内容
        JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_UPDATE_SPECIAL_QUEUE);
        log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_BRAND_CLOSE_QUEUE, result.toString());
        serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_CRM_QUEUE_NAME, RabbitMqConstants.SC_ERP_BRAND_CLOSE_QUEUE, null, result.toString().getBytes());
    }

    @Override
    public ServiceResult applicationForExtension(AdvantageBrandAddVo advantageBrandAddVo) throws IOException {
        ServiceResult serviceResult = new ServiceResult();
        Integer id = advantageBrandAddVo.getId();
        AdvantageBrand advantageBrand = this.baseMapper.selectById(id);
        if (!advantageBrand.getAdvantageStatus().equals(AdvantageStatusEnum.EFFECTIVE.getScStatusCode())) {
            serviceResult.setMessage("只有优势状态为正常才能申请");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        if (AdvantageBrandNatureEnum.AGENT.getCode().equals(advantageBrandAddVo.getDominantNature())) {
            serviceResult.setMessage("原厂不可以申请延期");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        if (!CollectionUtils.isEmpty(advantageBrandAddVo.getRelationPictureIds())) {
            uploadFileService.updateRelationId(advantageBrandAddVo.getRelationPictureIds(), UploadFileEnum.ADVANTAGE_BRAND, id);
        }
        UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
        //updateWrapper.set("certificate_validity_end", advantageBrand.getCertificateValidityEnd());
        //updateWrapper.set("primary_certificate_validity_end", advantageBrand.getCertificateValidityEnd());
        updateWrapper.set("remark", advantageBrandAddVo.getDescription());
        updateWrapper.set("audit_status", AuditStatusEnum.PENDING.getScStatusCode());
        updateWrapper.set("apply_for_extension", 1);
        updateWrapper.eq("id", id);
        this.update(updateWrapper);
        advantageBrand.setCreateUser(advantageBrandAddVo.getCreator());
        //advantageBrand.setCertificateValidityStart(advantageBrandAddVo.getCertificateValidityStart());
        advantageBrand.setCertificateValidityEnd(advantageBrandAddVo.getCertificateValidityEnd());
        advantageBrand.setRemark(advantageBrandAddVo.getRemark());
        sendReApplyOrReissueAdvantageBrand(advantageBrand);
        serviceResult.setMessage("申请已提交，正在审核中，审核通过后截止时间更新");
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServiceResult reApply(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) throws IOException {
        ServiceResult serviceResult = new ServiceResult();
        AdvantageBrand advantageBrand = this.getBaseMapper().selectById(updateAllAdvantageBrandVo.getId());
        if(!advantageBrand.getDominantNature().equals(AdvantageBrandNatureEnum.AGENT.getCode()) && updateAllAdvantageBrandVo.getStartDate() == null){
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("有效期开始时间不能为空");
            return serviceResult;
        }
        if(!advantageBrand.getDominantNature().equals(AdvantageBrandNatureEnum.AGENT.getCode()) && updateAllAdvantageBrandVo.getEndDate() == null){
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("有效期结束不能为空");
            return serviceResult;
        }
        if (!advantageBrand.getAdvantageStatus().equals(AdvantageStatusEnum.INVALID.getScStatusCode())) {
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("优势状态为失效才能发起");
            return serviceResult;
        }
        if (advantageBrand.getAuditStatus().equals(AuditStatusEnum.PENDING.getScStatusCode())) {
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("审核状态为,审核拒绝或者审核通过才能发起");
            return serviceResult;
        }
        if (!CollectionUtils.isEmpty(updateAllAdvantageBrandVo.getRelationPictureIds())) {
            uploadFileService.updateRelationId(updateAllAdvantageBrandVo.getRelationPictureIds(), UploadFileEnum.ADVANTAGE_BRAND, updateAllAdvantageBrandVo.getId());
        }

        if (updateAllAdvantageBrandVo.getStartDate() == null && updateAllAdvantageBrandVo.getEndDate() == null) {
            updateAllAdvantageBrandVo.setStartDate(new Date());
            updateAllAdvantageBrandVo.setEndDate(agentDate());
        }

        //重新发起MQ申请,修改状态,所有类型都要推送erp
        advantageBrand.setCreateUser(updateAllAdvantageBrandVo.getCreateUser());
        advantageBrand.setCertificateValidityStart(updateAllAdvantageBrandVo.getStartDate());
        advantageBrand.setCertificateValidityEnd(updateAllAdvantageBrandVo.getEndDate());
        advantageBrand.setRemark(updateAllAdvantageBrandVo.getRemark());
        sendReApplyOrReissueAdvantageBrand(advantageBrand);

        UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", updateAllAdvantageBrandVo.getId());
        updateWrapper.eq("enterprise_id", advantageBrand.getEnterpriseId());
        updateWrapper.eq("brand", advantageBrand.getBrand());
        updateWrapper.set("audit_status", AuditStatusEnum.PENDING.getScStatusCode());
        updateWrapper.set("certificate_validity_start", updateAllAdvantageBrandVo.getStartDate());
        updateWrapper.set("certificate_validity_end", updateAllAdvantageBrandVo.getEndDate());
        updateWrapper.set("remark", updateAllAdvantageBrandVo.getRemark());
        update(updateWrapper);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 重新发起或者延期 发送消息
     *
     * @param advantageBrand
     * @throws IOException
     */
    private void sendReApplyOrReissueAdvantageBrand(AdvantageBrand advantageBrand) throws IOException {
        SpecialsScTopViewAuditPojo specialsScTopViewAuditPojo = new SpecialsScTopViewAuditPojo();
        specialsScTopViewAuditPojo.setEnterpriseID(advantageBrand.getEnterpriseId());
        specialsScTopViewAuditPojo.setBrand(advantageBrand.getBrand());
        specialsScTopViewAuditPojo.setSummary(advantageBrand.getRemark());
        specialsScTopViewAuditPojo.setEndDate(advantageBrand.getCertificateValidityEnd());
        specialsScTopViewAuditPojo.setStartDate(advantageBrand.getCertificateValidityStart());
        specialsScTopViewAuditPojo.setCreator(advantageBrand.getCreateUser());
        specialsScTopViewAuditPojo.setID(advantageBrand.getErpId());
        specialsScTopViewAuditPojo.setType(advantageBrand.getDominantNature());
        List<UploadFileMessagePojo> files = new ArrayList<>();
        ServiceResult<List<UploadFile>> serviceResult = uploadFileService.findByRelationId(UploadFileEnum.ADVANTAGE_BRAND, advantageBrand.getId() + "");
        if (serviceResult != null && !CollectionUtils.isEmpty(serviceResult.getResult())) {
            List<UploadFile> uploadFileLisr = serviceResult.getResult();
            for (UploadFile uploadFile : uploadFileLisr) {
                UploadFileMessagePojo uploadFileMessagePojo = new UploadFileMessagePojo();
                uploadFileMessagePojo.setUrl(uploadFile.getFullPath());
                uploadFileMessagePojo.setFileName(uploadFile.getFileName());
                files.add(uploadFileMessagePojo);
            }
        }
        specialsScTopViewAuditPojo.setFileVoList(files);
        JSONObject json = (JSONObject) JSONObject.toJSON(specialsScTopViewAuditPojo);
        // 包装发送到rabbitmq的消息内容
        JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_UPDATE_BRAND_QUEUE);
        log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_UPDATE_BRAND_QUEUE, result.toString());
        serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_CRM_QUEUE_NAME, RabbitMqConstants.SC_ERP_UPDATE_BRAND_QUEUE, null, result.toString().getBytes());
    }

    @Override
    public ServiceResult<IPagePojo<AdvantageBrandRespPojo>> findByPage(QueryAdvantageBrandAddVo queryAdvantageBrandAddVo) {
        ServiceResult<IPagePojo<AdvantageBrandRespPojo>> serviceResult = new ServiceResult();
        LambdaQueryWrapper<AdvantageBrand> brandsLambdaQueryWrapper = new LambdaQueryWrapper<>();

        if (StringUtils.validateParameter(queryAdvantageBrandAddVo.getBrand())) {
            brandsLambdaQueryWrapper.like(AdvantageBrand::getBrand, queryAdvantageBrandAddVo.getBrand());
        }

        if (StringUtils.validateParameter(queryAdvantageBrandAddVo.getCreateUser())) {
            brandsLambdaQueryWrapper.like(AdvantageBrand::getCreateUser, queryAdvantageBrandAddVo.getCreateUser());
        }
        if (queryAdvantageBrandAddVo.getAuditStatus() != null) {
            brandsLambdaQueryWrapper.eq(AdvantageBrand::getAuditStatus, queryAdvantageBrandAddVo.getAuditStatus());
        }
        if (queryAdvantageBrandAddVo.getAdvantageStatus() != null) {
            brandsLambdaQueryWrapper.eq(AdvantageBrand::getAdvantageStatus, queryAdvantageBrandAddVo.getAdvantageStatus());
        }
        brandsLambdaQueryWrapper.eq(AdvantageBrand::getEnterpriseId, queryAdvantageBrandAddVo.getEnterpriseId());
        brandsLambdaQueryWrapper.orderByDesc(AdvantageBrand::getCreateDate);
        List<AdvantageBrandRespPojo> advantageBrandRespPojoList = new ArrayList<>();
        //Page<AdvantageBrand> brandsPage = baseMapper.selectPage(new Page<>(queryAdvantageBrandAddVo.getPageNo(), queryAdvantageBrandAddVo.getPageSize()), brandsLambdaQueryWrapper);

        PageHelper.startPage(queryAdvantageBrandAddVo.getPageNo(), queryAdvantageBrandAddVo.getPageSize());
        List<AdvantageBrand> advantageBrands = baseMapper.selectList(brandsLambdaQueryWrapper);
        PageInfo<AdvantageBrand> pageInfo = new PageInfo(advantageBrands);


        List<AdvantageBrand> records = pageInfo.getList();
        if (!CollectionUtils.isEmpty(records)) {
            for (AdvantageBrand record : records) {
                AdvantageBrandRespPojo advantageBrandRespPojo = new AdvantageBrandRespPojo();
                BeanUtils.copyProperties(record, advantageBrandRespPojo);
                //附件表,封装附件
                ServiceResult<List<UploadFile>> uploadFilesResult = uploadFileService.findByRelationId(UploadFileEnum.ADVANTAGE_BRAND, record.getId() + "");
                List<UploadFilePojo> uploadFilePojoList = new ArrayList<>();
                if (uploadFilesResult != null && !CollectionUtils.isEmpty(uploadFilesResult.getResult())) {
                    List<UploadFile> uploadFiles = uploadFilesResult.getResult();
                    for (UploadFile uploadFile : uploadFiles) {
                        UploadFilePojo uploadFilePojo = new UploadFilePojo();
                        BeanUtils.copyProperties(uploadFile, uploadFilePojo);
                        uploadFilePojoList.add(uploadFilePojo);
                    }
                }
                advantageBrandRespPojo.setFiles(uploadFilePojoList);
                advantageBrandRespPojoList.add(advantageBrandRespPojo);
            }
        }
        IPagePojo<AdvantageBrandRespPojo> result = new IPagePojo<>(advantageBrandRespPojoList, pageInfo.getPages(), pageInfo.getSize(), pageInfo.getTotal());
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("添加成功");
        serviceResult.setResult(result);
        return serviceResult;
    }

    @Override
    public void listenAddAdvantageBrand(SpecialsSCTopView specialsScTopView) {
        if (!StringUtils.validateParameter(specialsScTopView.getEnterpriseID())) {
            return;
        }
        if (specialsScTopView.getType() == null) {
            return;
        }
        if (!StringUtils.validateParameter(specialsScTopView.getBrand())) {
            return;
        }
        if (!StringUtils.validateParameter(specialsScTopView.getCreator())) {
            return;
        }
        if (specialsScTopView.getStartDate() == null) {
            return;
        }
        if (specialsScTopView.getEndDate() == null) {
            return;
        }
        //绑定参数
        AdvantageBrand advantageBrand = this.bindAdvantageBrand(specialsScTopView);
        this.baseMapper.insert(advantageBrand);
        //处理文件
        String filesString = specialsScTopView.getFiles();
        if (StringUtils.validateParameter(filesString)) {
            List<FileVo> uploadFileMessagePojoList = JSONObject.parseArray(filesString, FileVo.class);
            List<UploadFile> uploadFiles = new ArrayList<>();
            for (FileVo file : uploadFileMessagePojoList) {
                UploadFile uploadFile = new UploadFile();
                String url = file.getUrl();
                String fileName = file.getFileName();

                uploadFile.setFileName(fileName);
                String fileUrl = downloadPicture(url,fileName);
                if (!StringUtils.validateParameter(url)) {
                    continue;
                }
                uploadFile.setFileName(fileName);
                uploadFile.setSuffix(fileName.substring(fileName.lastIndexOf(".") + 1));
                uploadFile.setFullPath(fileUrl);
                uploadFile.setCreatorId(specialsScTopView.getCreator());
                uploadFile.setCreateDate(new Date());
                uploadFile.setRelationId(advantageBrand.getId().toString());
                uploadFile.setModel(UploadFileEnum.ADVANTAGE_BRAND.getCode());
                uploadFiles.add(uploadFile);
            }
            uploadFileService.saveUploadFile(uploadFiles);
        }
    }

    @Override
    public void handleAdvantageModelAuditMqMsg(SpecialsScTopViewAuditPojo specialsScTopViewAuditPojo) {
        if (!StringUtils.validateParameter(specialsScTopViewAuditPojo.getID())) {
            return;
        }
        if (specialsScTopViewAuditPojo.getStatus() == null) {
            return;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("erp_id", specialsScTopViewAuditPojo.getID());
        //param.put("brand", specialsScTopViewAuditPojo.getBrand());
        List<AdvantageBrand> advantageBrands = this.baseMapper.selectByMap(param);
        Integer id;
        if (CollectionUtils.isEmpty(advantageBrands)) {
            return;
        }
        AdvantageBrand advantageBrand = advantageBrands.get(0);
        id = advantageBrand.getId();
        //如果是申请延期失败操作,直接更新状态为成功
        if (advantageBrand.getApplyForExtension() != null && 1 == advantageBrand.getApplyForExtension() && AuditStatusEnum.APPROVAL_REFUSED.getErpStatusCode().equals(specialsScTopViewAuditPojo.getStatus())) {
            UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id", id);
            updateWrapper.set("audit_status", AuditStatusEnum.APPROVE.getScStatusCode());
            updateWrapper.set("apply_for_extension", 0);
            update(updateWrapper);

        } else {
            UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id", id);
            updateWrapper.set("audit_status", AuditStatusEnum.getScStatusCode(specialsScTopViewAuditPojo.getStatus()));
            updateWrapper.set("audit_reason", specialsScTopViewAuditPojo.getSummary());
            if (advantageBrand.getCertificateValidityEnd().getTime() > System.currentTimeMillis() && AuditStatusEnum.APPROVE.getErpStatusCode().equals(specialsScTopViewAuditPojo.getStatus())) {
                updateWrapper.set("advantage_status", AdvantageStatusEnum.EFFECTIVE.getScStatusCode());
            }
            if (AuditStatusEnum.APPROVE.getErpStatusCode().equals(specialsScTopViewAuditPojo.getStatus())) {
                //更新图片全部为审核通过
                uploadFileService.updateRelationIdStatus(UploadFileEnum.ADVANTAGE_BRAND, id + "", UploadFileStatusEnum.NORMAL);
                if (specialsScTopViewAuditPojo.getEndDate() != null) {
                    updateWrapper.set("certificate_validity_end", specialsScTopViewAuditPojo.getEndDate());
                }
            }
            update(updateWrapper);
        }
        //保存消息表
        //通过
        String content = "";
        if (AuditStatusEnum.APPROVE.getErpStatusCode().equals(specialsScTopViewAuditPojo.getStatus())) {
            content = "恭喜您,您的优势品牌(" + advantageBrand.getBrand() + ")" + "已通过审核";
        } else {
            content = "很遗憾,您的优势品牌(" + advantageBrand.getBrand() + ")" + "申请被拒绝";
        }
        //查找当前公司下所有用户ids
        //List<String> userIds = userService.findByEnterpriseId(advantageBrand.getEnterpriseId());
        List<String> userIds = new ArrayList<>();
        userIds.add(advantageBrand.getCreateUserId());
        ActionMessage actionMessage = new ActionMessage();
        actionMessage.setContent(content);
        actionMessage.setCreateTime(new Date());
        actionMessageService.handleActionMessage(actionMessage, userIds);
    }

    @Override
    public void handleAdvantageModelCloseMqMsg(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo) {
        if (!StringUtils.validateParameter(specialsScTopViewCloseDelPojo.getID())) {
            return;
        }
        //取消优势品牌
        UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
       /* updateWrapper.eq("enterprise_id", specialsScTopViewCloseDelPojo.getEnterpriseID());
        updateWrapper.eq("brand", specialsScTopViewCloseDelPojo.getBrand());*/
        updateWrapper.eq("erp_id", specialsScTopViewCloseDelPojo.getID());
        updateWrapper.set("advantage_status", AdvantageStatusEnum.INVALID.getScStatusCode());
        update(updateWrapper);
    }

    @Override
    public void handleAdvantageModelDeleteMqMsg(SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo) {
        if (!StringUtils.validateParameter(specialsScTopViewCloseDelPojo.getID())) {
            return;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("erp_id", specialsScTopViewCloseDelPojo.getID());
        this.baseMapper.deleteByMap(param);
    }

    @Override
    public ServiceResult<List<AdvantageBrandApiRespPojo>> findAdvantageBrandUser(List<String> brands) {
        ServiceResult<List<AdvantageBrandApiRespPojo>> serviceResult = new ServiceResult<>();
        //先找到所有主账号
        ServiceResult<List<User>> allPrimaryAccountData = userService.getAllPrimaryAccountData();
        Set<String> accordWith = new HashSet<>();
        List<AdvantageBrandApiRespPojo> respPojoList = new ArrayList<>();
        for (String brand : brands) {
            //返回对象
            AdvantageBrandApiRespPojo advantageBrandApiRespPojo = new AdvantageBrandApiRespPojo();
            advantageBrandApiRespPojo.setBrand(brand);
            //每个品牌包含主账号
            List<AdvantageBrandUserApiPojo> resultAdvantageBrandUserList = new ArrayList<>();
            //找到这个品牌下所有符合规范的公司
            Map<String, Object> param = new HashMap<>();
            param.put("brand", brand);
            param.put("audit_status", AuditStatusEnum.APPROVE.getScStatusCode());
            param.put("advantage_status", AdvantageStatusEnum.EFFECTIVE.getScStatusCode());
            List<AdvantageBrand> advantageBrands = this.baseMapper.selectByMap(param);
            //生产一个set区分优势品牌
            for (AdvantageBrand advantageBrand : advantageBrands) {
                accordWith.add(advantageBrand.getEnterpriseId());
            }
            if (allPrimaryAccountData != null && !CollectionUtils.isEmpty(allPrimaryAccountData.getResult())) {
                List<User> userList = allPrimaryAccountData.getResult();
                for (User user : userList) {
                    AdvantageBrandUserApiPojo advantageBrandUserApiPojo = new AdvantageBrandUserApiPojo();
                    advantageBrandUserApiPojo.setUserId(user.getId());
                    advantageBrandUserApiPojo.setUserName(user.getUserName());
                    if (StringUtils.validateParameter(user.getEnterpriseId()) && !CollectionUtils.isEmpty(accordWith)) {
                        advantageBrandUserApiPojo.setAccordWith(accordWith.contains(user.getEnterpriseId()));
                    }
                    resultAdvantageBrandUserList.add(advantageBrandUserApiPojo);
                }
            }
            advantageBrandApiRespPojo.setAdvantageBrandUser(resultAdvantageBrandUserList);
            respPojoList.add(advantageBrandApiRespPojo);
            accordWith.clear();
        }
        serviceResult.setResult(respPojoList);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public void updateBrandAdvantageStatus() {
        int pageNo = 0;
        int pageSize = 20;
        //当前时间
        long now = System.currentTimeMillis();
        for (; ; ) {
            pageNo++;
            PageHelper.startPage(pageNo, pageSize);
            LambdaQueryWrapper<AdvantageBrand> brandLambdaQueryWrapper = new LambdaQueryWrapper<>();
            brandLambdaQueryWrapper.eq(AdvantageBrand::getDominantNature, AdvantageBrandNatureEnum.DISTRIBUTION.getCode());
            brandLambdaQueryWrapper.orderByDesc(AdvantageBrand::getId);
            List<AdvantageBrand> list = baseMapper.selectList(brandLambdaQueryWrapper);
            if (CollectionUtils.isEmpty(list)) {
                break;
            }
            for (AdvantageBrand advantageBrand : list) {
                Date certificateValidity = advantageBrand.getCertificateValidityEnd();
                if (certificateValidity != null && certificateValidity.getTime() < now) {
                    UpdateWrapper<AdvantageBrand> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq("id", advantageBrand.getId());
                    updateWrapper.set("advantage_status", AdvantageStatusEnum.INVALID.getScStatusCode());
                    this.baseMapper.update(null, updateWrapper);
                }
            }
        }
    }

    @Override
    public ServiceResult checkAdvantageBrand(String brand, AuthenticationUser authenticationUser, HttpServletRequest request) {
        ServiceResult serviceResult = new ServiceResult();
        Map<String, Object> param = new HashMap<>();
        param.put("enterprise_id", authenticationUser.getEpId());
        param.put("audit_status", AuditStatusEnum.PENDING.getScStatusCode());
        param.put("brand", brand);
        List<AdvantageBrand> advantageBrands = this.baseMapper.selectByMap(param);
        if (advantageBrands != null && advantageBrands.size() > 0) {
            serviceResult.setMessage("当前品牌存在待审核状态记录,添加失败");
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    @Override
    public ServiceResult<List<AdvantageBrandApiRespPojo>> confirmPicture(List<String> pctureIds, Integer id) throws IOException {
        ServiceResult serviceResult = new ServiceResult();
        List<UploadFileMessagePojo> uploadFileMessagePojoList = new ArrayList<>();
        uploadFileService.updateRelationId(pctureIds, UploadFileEnum.ADVANTAGE_BRAND, id);
        //推送 消息
        ServiceResult<List<UploadFile>> uploadFileServiceResult = uploadFileService.findByIds(pctureIds);
        if (uploadFileServiceResult != null) {
            List<UploadFile> result = uploadFileServiceResult.getResult();
            if (!CollectionUtils.isEmpty(result)) {
                for (UploadFile uploadFile : result) {
                    UploadFileMessagePojo uploadFileMessagePojo = new UploadFileMessagePojo();
                    uploadFileMessagePojo.setFileName(uploadFile.getFileName());
                    uploadFileMessagePojo.setUrl(uploadFile.getFullPath());
                    uploadFileMessagePojoList.add(uploadFileMessagePojo);
                }
            }
        }
        if (!CollectionUtils.isEmpty(uploadFileMessagePojoList)) {
            AdvantageBrand advantageBrand = this.baseMapper.selectById(id);

            SubmitFileMessagePojo submitFileMessagePojo = new SubmitFileMessagePojo();
            submitFileMessagePojo.setID(advantageBrand.getErpId());
            submitFileMessagePojo.setFiles(uploadFileMessagePojoList);
            JSONObject json = (JSONObject) JSONObject.toJSON(submitFileMessagePojo);
            // 包装发送到rabbitmq的消息内容
            JSONObject result = serviceHelper.packageSendRabbitMqMsgData(json, RabbitMqConstants.SC_ERP_SUBMIT_FILE_QUEUE);
            log.info("推送给rabbitmq的交换机:[{}],队列:[{}]的消息内容为:[{}]", RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_ERP_SUBMIT_FILE_QUEUE, result.toString());
            serviceHelper.getRabbitMqManager().basicPublish(RabbitMqConstants.SC_ERP_EXCHANGE, RabbitMqConstants.SC_TO_ERP_CRM_QUEUE_NAME, RabbitMqConstants.SC_ERP_SUBMIT_FILE_QUEUE, null, result.toString().getBytes());
        }
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage("提交成功");
        return serviceResult;
    }

   /* @Override
       public void listenAddAdvantageBrand(ErpAdvantageBrandVo erpAdvantageBrandVo) {
        if (!StringUtils.validateParameter(erpAdvantageBrandVo.getEnterpriseID())) {
            return;
        }
        if (erpAdvantageBrandVo.getType() == null) {
            return;
        }
        if (!StringUtils.validateParameter(erpAdvantageBrandVo.getBrand())) {
            return;
        }
        if (!StringUtils.validateParameter(erpAdvantageBrandVo.getCreator())) {
            return;
        }
        if (erpAdvantageBrandVo.getStartDate() == null) {
            return;
        }
        if (erpAdvantageBrandVo.getEndDate() == null) {
            return;
        }
        //绑定参数
        AdvantageBrand advantageBrand = this.bindAdvantageBrand(erpAdvantageBrandVo);
        this.baseMapper.insert(advantageBrand);
        //处理文件
        List<FileVo> files = erpAdvantageBrandVo.getFiles();
        List<UploadFile> uploadFiles = new ArrayList<>();
        if (!CollectionUtils.isEmpty(files)) {
            for (FileVo file : files) {
                UploadFile uploadFile = new UploadFile();
                uploadFile.setFileName(file.getFileName());
                uploadFile.setFullPath(file.getUrl());
                uploadFile.setCreatorId(erpAdvantageBrandVo.getCreator());
                uploadFile.setCreateDate(new Date());
                uploadFile.setRelationId(UploadFileEnum.MY_COMPANY.getCode() + advantageBrand.getId());
                uploadFiles.add(uploadFile);
            }
        }
        uploadFileService.saveUploadFile(uploadFiles);
    }*/

    /* *//**
     * 绑定参数
     *
     * @param erpAdvantageBrandVo
     * @return
     *//*
    private AdvantageBrand bindAdvantageBrand(ErpAdvantageBrandVo erpAdvantageBrandVo) {
        AdvantageBrand advantageBrand = new AdvantageBrand();
        advantageBrand.setDominantNature(erpAdvantageBrandVo.getType());
        advantageBrand.setBrand(erpAdvantageBrandVo.getBrand());
        advantageBrand.setCreateUserId(erpAdvantageBrandVo.getCreator());
        ServiceResult<User> serviceResult = userService.findByUserId(erpAdvantageBrandVo.getCreator());
        if (serviceResult != null && serviceResult.getResult() != null) {
            advantageBrand.setCreateUser(serviceResult.getResult().getUserName());
        }
        advantageBrand.setCreateDate(new Date());

        advantageBrand.setRemark(erpAdvantageBrandVo.getSummary());
        advantageBrand.setEnterpriseId(erpAdvantageBrandVo.getEnterpriseID());
        AdvantageStatusEnum.getErpStatusCode(erpAdvantageBrandVo.getStatus());

        //有效期待定


        //处理文件
        return advantageBrand;
    }*/

    /**
     * 绑定参数
     *
     * @param specialsScTopView
     * @return
     */
    private AdvantageBrand bindAdvantageBrand(SpecialsSCTopView specialsScTopView) {
        AdvantageBrand advantageBrand = new AdvantageBrand();
        advantageBrand.setDominantNature(specialsScTopView.getType());
        advantageBrand.setBrand(specialsScTopView.getBrand());
        advantageBrand.setCreateUserId(specialsScTopView.getCreator());
        ServiceResult<User> serviceResult = userService.findByUserId(specialsScTopView.getCreator());
        if (serviceResult != null && serviceResult.getResult() != null) {
            advantageBrand.setCreateUser(serviceResult.getResult().getUserName());
        }
        //advantageBrand.setRemark(specialsScTopView.getSummary());
        advantageBrand.setEnterpriseId(specialsScTopView.getEnterpriseID());
        advantageBrand.setAdvantageStatus(AdvantageStatusEnum.INVALID.getScStatusCode());
        advantageBrand.setAuditStatus(AdvantageStatusEnum.getScStatusCode(specialsScTopView.getStatus()));
        advantageBrand.setCreateDate(specialsScTopView.getStartDate());
        advantageBrand.setCertificateValidityEnd(specialsScTopView.getEndDate());
        advantageBrand.setCertificateValidityStart(specialsScTopView.getStartDate());
        advantageBrand.setErpId(specialsScTopView.getId());
        return advantageBrand;
    }

    /**
     * 查询当前这个人的数据权限
     *
     * @param updateAllAdvantageBrandVo
     * @return
     */
    public String[] findByIds(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo) {
        String[] resultIds = null;
        if (updateAllAdvantageBrandVo.getIsAll() == null || Boolean.FALSE.equals(updateAllAdvantageBrandVo.getIsAll())) {
            resultIds = updateAllAdvantageBrandVo.getIds();
        } else {
            //查询当前人的列表权限
            String byEmpIds = this.baseMapper.findByEmpId(updateAllAdvantageBrandVo);
            if (StringUtils.validateParameter(byEmpIds)) {
                resultIds = byEmpIds.split(",");
            }
        }
        return resultIds;
    }


    /**
     * 原厂到期时间
     *
     * @return
     */
    private Date agentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return dateFormat.parse("2099-12-31");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String downloadPicture(String url, String fileName) {
        if(!StringUtils.validateParameter(url)){
            return null;
        }
        String finalUrl = url;
        if(!url.startsWith(Constants.HTTPS)){
            finalUrl = Constants.HTTPS+url;
        }
        //后缀名字
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        //生成唯一文件名
        String onlyFileName = IdUtil.simpleUUID() + "." + suffix;
        byte[] bytes = null;
        try {
            bytes = HttpUtils.downloadPicture(finalUrl);
        } catch (IOException e) {
            log.error("下载图片发生错误",e);
            return null;
        }
        //上传文件
        serviceHelper.getSftpChannelManager().upload(serviceHelper.getFileParamConfig().getFileUrl(), bytes, onlyFileName);
        return serviceHelper.getFileParamConfig().getImageExhibitionUrl() + onlyFileName;
    }

}