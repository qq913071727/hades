package com.innovation.ic.sc.base.service.sc.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.sc.base.mapper.sc.LadderPriceAmountConfigMapper;
import com.innovation.ic.sc.base.model.sc.LadderPriceAmountConfig;
import com.innovation.ic.sc.base.service.sc.LadderPriceAmountConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @desc   LadderPriceAmountConfig的具体实现类
 * @author linuo
 * @time   2023年4月14日09:48:00
 */
@Service
@Transactional
public class LadderPriceAmountConfigServiceImpl extends ServiceImpl<LadderPriceAmountConfigMapper, LadderPriceAmountConfig> implements LadderPriceAmountConfigService {

}