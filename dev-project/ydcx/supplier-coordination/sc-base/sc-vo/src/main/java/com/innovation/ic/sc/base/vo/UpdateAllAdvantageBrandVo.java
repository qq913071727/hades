package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/***
 * 修改全量数据
 */
@Data
@ApiModel(value = "UpdateAllAdvantageBrandVo", description = "修改全量数据Vo")
public class UpdateAllAdvantageBrandVo {


    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;


    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "审核状态 (0待审批  1审批通过  2审批否决) ", dataType = "Integer")
    private Integer auditStatus;

    @ApiModelProperty(value = "优势状态 0:无效、1:正常", dataType = "Integer")
    private Integer advantageStatus;


    @ApiModelProperty(value = "操作ids", dataType = "String")
    private String[] ids;


    @ApiModelProperty(value = "是否全量删除,TRUE为全部删", dataType = "Boolean")
    private Boolean isAll;

    @ApiModelProperty(value = "开始时间", dataType = "Date")
    private Date startDate;

    @ApiModelProperty(value = "结束时间", dataType = "Date")
    private Date endDate;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "关联图片ids", dataType = "String")
    private List<String> relationPictureIds;

    @ApiModelProperty(value = "操作id", dataType = "Integer")
    private Integer id;


}