package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 人员管理中被管理用户的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ManagementIdVo", description = "人员管理中被管理用户的vo类")
public class ManagementIdVo {

//    @ApiModelProperty(value = "主键", dataType = "Integer")
//    private Integer id;

    @ApiModelProperty(value = "被管理用户的id", dataType = "String")
    private String managedUserId;

}
