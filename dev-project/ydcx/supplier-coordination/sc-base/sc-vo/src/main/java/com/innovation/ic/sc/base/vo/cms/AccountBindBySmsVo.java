package com.innovation.ic.sc.base.vo.cms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号单条新增的Vo类
 * @author linuo
 * @time   2022年8月31日16:29:10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountBindBySmsVo", description = "账号绑定的Vo类")
public class AccountBindBySmsVo {
    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String passWord;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "短信验证码", dataType = "String")
    private String code;
}
