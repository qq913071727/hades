package com.innovation.ic.sc.base.vo;

import com.innovation.ic.sc.base.pojo.variable.UploadFilePojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 我的公司
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyVo", description = "我的公司")
public class MyCompanyVo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "国别地区id", dataType = "String")
    private String countryRegionId;


    @ApiModelProperty(value = "公司类型id", dataType = "String")
    private String companyTypeId;


    @ApiModelProperty(value = "发票类型id", dataType = "String")
    private String invoicTypeId;


    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;


    @ApiModelProperty(value = "币种id", dataType = "String")
    private String currencyId;


    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;


    @ApiModelProperty(value = "创建人名字", dataType = "String")
    private String creatorUserName;

    @ApiModelProperty(value = "收款公司", dataType = "String")
    private String receivingCompany;

    @ApiModelProperty(value = "开户银行", dataType = "String")
    private String bankOfDeposit;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String accountNumber;

    @ApiModelProperty(value = "银行地址", dataType = "String")
    private String bankAddress;


    @ApiModelProperty(value = "swiftCode", dataType = "String")
    private String swiftCode;

    @ApiModelProperty(value = "中转银行", dataType = "String")
    private String transitBank;

    @ApiModelProperty(value = "是否是更新 true 更新", dataType = "Boolean")
    private Boolean update;


    @ApiModelProperty(value = "上传图片,包含类型", dataType = "UploadFileVo")
    private List<UploadFileVo> uploadFileVo;


}
