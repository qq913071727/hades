package com.innovation.ic.sc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "GetAuthUrlVo", description = "获取授权地址的Vo类")
public class GetAuthUrlVo {
    @ApiModelProperty(value = "回调地址", required = true, dataType = "String")
    private String redirectUrl;
}