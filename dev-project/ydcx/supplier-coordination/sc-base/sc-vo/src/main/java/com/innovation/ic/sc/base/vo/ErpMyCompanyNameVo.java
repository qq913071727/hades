package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 保存我的公司,同步erp
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErpMyCompanyNameVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private String ID;

    @ApiModelProperty(value = "name", dataType = "String")
    private String Name;

    @ApiModelProperty(value = "国别", dataType = "String")
    private String District;

    @ApiModelProperty(value = "供应商类型", dataType = "String")
    private String SupplierType;

    @ApiModelProperty(value = "企业性质", dataType = "String")
    private String Nature;

    @ApiModelProperty(value = "发票类型", dataType = "String")
    private String InvoiceType;

    @ApiModelProperty(value = "状态", dataType = "String")
    private Integer Status;

    @ApiModelProperty(value = "外部erpid", dataType = "String")
    private String externalId;

    @ApiModelProperty(value = "注册地址", dataType = "String")
    private String RegAddress;

    @ApiModelProperty(value = "币种", dataType = "String")
    private String Currency;

    @ApiModelProperty(value = "国家/地区", dataType = "String")
    private String Place;


}