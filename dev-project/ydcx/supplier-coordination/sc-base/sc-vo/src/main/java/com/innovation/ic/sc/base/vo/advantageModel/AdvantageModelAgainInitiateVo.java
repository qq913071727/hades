package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   优势型号再次发起审核接口的Vo类
 * @author linuo
 * @time   2022年9月22日09:25:35
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelAgainInitiateVo", description = "优势型号再次发起审核接口的Vo类")
public class AdvantageModelAgainInitiateVo {
    @ApiModelProperty(value = "库存ID集合", dataType = "String")
    private List<Integer> idList;

    @ApiModelProperty(value = "是否全部(true是、false否)", dataType = "Boolean")
    private Boolean isAll;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "审核状态", dataType = "String")
    private String auditStatus;

    @ApiModelProperty(value = "优势状态", dataType = "String")
    private String advantageStatus;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    private String validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    private String validityDateEnd;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;
}