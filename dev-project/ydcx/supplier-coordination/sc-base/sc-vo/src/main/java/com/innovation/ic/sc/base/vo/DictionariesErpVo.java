package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典表Vo
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DictionariesErpVo {
    @ApiModelProperty(value = "枚举id", dataType = "String")
    private String ID;

    @ApiModelProperty(value = "枚举值id", dataType = "String")
    private String ArgumentID;

    @ApiModelProperty(value = "值解释", dataType = "String")
    private String Text;

    @ApiModelProperty(value = "值解释", dataType = "String")
    private String Value;
}