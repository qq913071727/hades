package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   库存上架/下架接口的Vo类
 * @author linuo
 * @time   2022年9月20日11:15:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryStatusUpdateVo", description = "库存上架/下架接口的Vo类")
public class InventoryStatusUpdateVo {
    @ApiModelProperty(value = "库存id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;
}