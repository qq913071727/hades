package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   优势型号状态变更接口的Vo类
 * @author linuo
 * @time   2023年4月1日14:17:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryUpdateAdvantageStatusVo", description = "优势型号状态变更接口的Vo类")
public class InventoryUpdateAdvantageStatusVo {
    @ApiModelProperty(value = "操作类型(0:取消优势型号、1:设为优势型号)", dataType = "Integer")
    private Integer operateType;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    private String validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    private String validityDateEnd;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "修改优势型号状态数据的id集合", dataType = "String")
    private List<Integer> idList;

    @ApiModelProperty(value = "是否全部(true是、false否)", dataType = "Boolean")
    private Boolean isAll;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建日期开始", dataType = "String")
    private String startDate;

    @ApiModelProperty(value = "创建日期结束", dataType = "String")
    private String endDate;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;
}