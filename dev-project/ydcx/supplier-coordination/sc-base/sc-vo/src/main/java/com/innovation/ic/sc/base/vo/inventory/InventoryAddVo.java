package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   库存新增接口的Vo类
 * @author linuo
 * @time   2022年8月23日09:52:52
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryAddVo", description = "库存新增接口的Vo类")
public class InventoryAddVo {
    @ApiModelProperty(value = "库存数据", dataType = "String")
    private List<InventoryAddParamVo> inventoryList;
}