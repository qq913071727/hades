package com.innovation.ic.sc.base.vo.inventory;

import com.innovation.ic.sc.base.vo.ladderPrice.LadderPriceVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryLadderPriceVo", description = "更新阶梯价格和库存的Vo类")
public class InventoryLadderPriceVo {
    @ApiModelProperty(value = "库存数据", required = true, dataType = "List")
    private List<LadderPriceVo> inventoryList;
}
