package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   列表查询接口的默认Vo类
 * @author linuo
 * @time   2023年2月9日16:50:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DefaultQueryVo", description = "列表查询接口的默认Vo类")
public class DefaultQueryVo {
    @ApiModelProperty(value = "查询页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数据数量", dataType = "Integer")
    private Integer pageSize;
}
