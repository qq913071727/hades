package com.innovation.ic.sc.base.vo.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RoleVo", description = "角色")
public class RoleVo {
    @ApiModelProperty(value = "id", dataType = "String")
    private String _id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "菜单列表", dataType = "List")
    private List<MenuVo> menuVoList = new ArrayList<MenuVo>();
}
