package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   模糊查询标准库型号接口的Vo类
 * @author linuo
 * @time   2023年4月2日09:18:16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBlurQueryBzkPartNumberVo", description = "模糊查询标准库型号接口的Vo类")
public class InventoryBlurQueryBzkPartNumberVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;
}
