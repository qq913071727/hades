package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.List;

/**
 * @desc   阶梯价格按单价配置的Vo类
 * @author linuo
 * @time   2023年4月13日16:56:08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceAmountConfigVo", description = "阶梯价格按单价配置的Vo类")
public class LadderPriceAmountConfigVo {
    @ApiModelProperty(value = "最小金额", dataType = "BigDecimal")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最大金额", dataType = "BigDecimal")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "阶梯价格配置", dataType = "List")
    private List<LadderPriceConfigVo> ladderPriceConfig;
}