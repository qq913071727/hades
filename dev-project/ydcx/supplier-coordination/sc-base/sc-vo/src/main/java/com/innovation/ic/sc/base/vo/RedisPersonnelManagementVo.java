package com.innovation.ic.sc.base.vo;

import com.innovation.ic.sc.base.model.sc.PersonnelManagement;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/***
 * 推送redis 人员管理封装类
 */
@Data
@ApiModel(value = "RedisPersonnelManagementVo", description = "推送redis 人员管理封装类")
public class RedisPersonnelManagementVo {
    private List<PersonnelManagement> personnelManagements;
}
