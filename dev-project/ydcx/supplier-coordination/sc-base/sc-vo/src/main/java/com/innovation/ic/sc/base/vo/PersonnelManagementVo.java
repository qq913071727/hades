package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 人员管理的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PersonnelManagementVo", description = "人员管理的vo类")
public class PersonnelManagementVo {

    @ApiModelProperty(value = "用户id", dataType = "Integer")
    private List<ManagementIdVo> ids;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "被管理用户的id", dataType = "List")
    private List<ManagementIdVo> managedUserIds;

}
