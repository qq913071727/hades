package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   库存批量导入接口的Vo类
 * @author linuo
 * @time   2022年8月23日14:38:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBatchAddVo", description = "库存批量导入接口的Vo类")
public class InventoryBatchAddVo {
    @ApiModelProperty(value = "导入方式(1:添加方式、2:覆盖方式)", dataType = "Integer")
    private Integer importType;

    @ApiModelProperty(value = "库存数据", dataType = "List")
    private List<InventoryBatchAddParamVo> inventoryList;
}