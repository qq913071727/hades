package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   库存确认匹配结果的Vo类
 * @author linuo
 * @time   2023年2月10日13:10:55
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryConfirmMatchResultVo", description = "库存确认匹配结果的Vo类")
public class InventoryConfirmMatchResultVo {
    @ApiModelProperty(value = "库存确认匹配结果集合", dataType = "String")
    private List<InventoryMatchResultVo> dataList;
}
