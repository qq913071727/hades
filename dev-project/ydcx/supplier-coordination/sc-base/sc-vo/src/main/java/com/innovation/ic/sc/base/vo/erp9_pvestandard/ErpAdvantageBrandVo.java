package com.innovation.ic.sc.base.vo.erp9_pvestandard;

import com.innovation.ic.sc.base.vo.FileVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/***
 * erp优势品牌添加
 */
@Data
@ApiModel(value = "ErpAdvantageBrandVo", description = "添加优势品牌")
public class ErpAdvantageBrandVo {

    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "优势性质 1代理  2原厂  3分销", dataType = "Integer")
    private Integer Type;


    @ApiModelProperty(value = "型号", dataType = "String")
    private String PartNumber;


    @ApiModelProperty(value = "品牌", dataType = "String")
    private String Brand;


    @ApiModelProperty(value = "有效期开始", dataType = "String")
    private Date StartDate;

    @ApiModelProperty(value = "有效期截至", dataType = "String")
    private Date EndDate;


    @ApiModelProperty(value = "特色状态  100 待审批  200 审批通过  ", dataType = "Integer")
    private Integer Status;


    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;

    @ApiModelProperty(value = "附件", dataType = "List")
    List<FileVo> Files;


    @ApiModelProperty(value = "备注", dataType = "String")
    private String Summary;
}