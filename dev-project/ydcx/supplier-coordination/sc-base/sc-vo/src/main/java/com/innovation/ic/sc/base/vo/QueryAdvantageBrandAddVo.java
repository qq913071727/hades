package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.Date;
import java.util.List;

/***
 * 优势品牌添加
 */
@Data
@ApiModel(value = "QueryAdvantageBrandAddVo", description = "添加优势品牌")
public class QueryAdvantageBrandAddVo {


    @ApiModelProperty(value = "每页显示多少", dataType = "Integer")
    private Integer pageSize;


    @ApiModelProperty(value = "当前页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;


    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "审核状态 (0待审批  1审批通过  2审批否决) ", dataType = "Integer")
    private Integer auditStatus;

    @ApiModelProperty(value = "优势状态 0:无效、1:正常", dataType = "Integer")
    private Integer advantageStatus;



}