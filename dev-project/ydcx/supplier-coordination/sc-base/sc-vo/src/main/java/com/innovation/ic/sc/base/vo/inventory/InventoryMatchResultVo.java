package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   库存确认结果的Vo类
 * @author linuo
 * @time   2023年2月10日15:16:03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryMatchResultVo", description = "库存确认结果的Vo类")
public class InventoryMatchResultVo {
    @ApiModelProperty(value = "库存ID", dataType = "String")
    private Integer id;

    @ApiModelProperty(value = "标准匹配型号", dataType = "String")
    private String bzkPartNumber;

    @ApiModelProperty(value = "标准匹配品牌", dataType = "String")
    private String bzkBrand;
}
