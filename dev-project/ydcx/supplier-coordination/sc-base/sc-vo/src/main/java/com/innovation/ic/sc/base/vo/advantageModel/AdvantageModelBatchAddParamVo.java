package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号批量导入的参数Vo类
 * @author linuo
 * @time   2022年8月31日14:25:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelBatchAddParamVo", description = "优势型号批量导入的参数Vo类")
public class AdvantageModelBatchAddParamVo {
    @ApiModelProperty(value = "表格名称", dataType = "String")
    private String tableName;

    @ApiModelProperty(value = "优势型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;
}
