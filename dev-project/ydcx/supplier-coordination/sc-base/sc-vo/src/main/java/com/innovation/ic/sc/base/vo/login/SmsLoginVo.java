package com.innovation.ic.sc.base.vo.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   短信验证码登录的Vo类
 * @author linuo
 * @time   2022年8月8日13:16:11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SmsLoginVo", description = "短信验证码登录的Vo类")
public class SmsLoginVo {
    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "短信验证码", dataType = "String")
    private String code;
}