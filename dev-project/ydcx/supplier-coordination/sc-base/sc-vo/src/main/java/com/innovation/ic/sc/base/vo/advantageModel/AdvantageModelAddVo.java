package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号单条新增的Vo类
 * @author linuo
 * @time   2022年8月31日16:29:10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelAddVo", description = "优势型号单条新增的Vo类")
public class AdvantageModelAddVo {
    @ApiModelProperty(value = "优势型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    private String validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    private String validityDateEnd;
}
