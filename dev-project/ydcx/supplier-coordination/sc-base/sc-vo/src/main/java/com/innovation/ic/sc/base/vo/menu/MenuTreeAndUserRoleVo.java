package com.innovation.ic.sc.base.vo.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 用户和角色还有菜单的关系
 * @Date 2023/1/29 14:08
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MenuTreeAndUserRoleVo", description = "用户和角色还有菜单的关系")
public class MenuTreeAndUserRoleVo {
    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "菜单节点列表", dataType = "List")
    private List<MenuVo> menuNodeList = new ArrayList<MenuVo>();

    @ApiModelProperty(value = "角色id列表", dataType = "List")
    private List<String> roleIdList;
}
