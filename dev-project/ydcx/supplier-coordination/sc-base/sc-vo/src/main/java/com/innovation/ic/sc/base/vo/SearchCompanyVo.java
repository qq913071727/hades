package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class SearchCompanyVo implements Serializable {
    private static final long serialVersionUID = -6350725665280412897L;

    @ApiModelProperty(value = "是否需要根据id查", dataType = "String")
    private String id;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "公司名字", dataType = "String")
    private String enterpriseName;


    @ApiModelProperty(value = "当前页数", dataType = "int")
    private Integer page;

    @ApiModelProperty(value = "每页展示", dataType = "int")
    private Integer pageSize;
}