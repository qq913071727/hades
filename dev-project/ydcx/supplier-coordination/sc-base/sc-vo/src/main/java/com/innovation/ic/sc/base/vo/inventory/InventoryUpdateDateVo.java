package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   库存时间更新接口的Vo类
 * @author linuo
 * @time   2022年8月23日16:28:26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryUpdateDateVo", description = "库存时间更新接口的Vo类")
public class InventoryUpdateDateVo {
    @ApiModelProperty(value = "库存ID集合", dataType = "String")
    private List<Integer> idList;

    @ApiModelProperty(value = "是否全部(true是、false否)", dataType = "Boolean")
    private Boolean isAll;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建日期开始", dataType = "String")
    private String startDate;

    @ApiModelProperty(value = "创建日期结束", dataType = "String")
    private String endDate;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;
}