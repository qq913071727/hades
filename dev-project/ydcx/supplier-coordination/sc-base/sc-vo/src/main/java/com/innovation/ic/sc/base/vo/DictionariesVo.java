package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 字典表Vo
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DictionariesVo {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "字典描述", dataType = "String")
    private String dName;

    @ApiModelProperty(value = "外部唯一id", dataType = "String")
    private String externalUniqueId;

    @ApiModelProperty(value = "字典编码", dataType = "String")
    private String dCode;

    @ApiModelProperty(value = "枚举类型", dataType = "String")
    private String dTypeId;

    @ApiModelProperty(value = "erp中的value", dataType = "String")
    private String dValue;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    private Date createDate;
}