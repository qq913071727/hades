package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 保存我的公司,同步erp
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ErpBookAccountVo {
    @ApiModelProperty(value = "收款账号ID", dataType = "Integer")
    private String ID;

    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "收款公司", dataType = "String")
    private String Title;

    @ApiModelProperty(value = "开户行", dataType = "String")
    private String Bank;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String Account;

    @ApiModelProperty(value = "银行地址", dataType = "String")
    private String BankAddress;

    @ApiModelProperty(value = "SwiftCode", dataType = "String")
    private String SwiftCode;

    @ApiModelProperty(value = "中转银行", dataType = "String")
    private String Transfer;

    @ApiModelProperty(value = "币种", dataType = "String")
    private String Currency;
}