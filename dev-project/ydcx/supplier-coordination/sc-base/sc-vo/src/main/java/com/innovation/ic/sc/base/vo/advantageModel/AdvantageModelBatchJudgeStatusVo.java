package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   批量判断是否为优势型号的Vo类
 * @author linuo
 * @time   2022年9月28日13:24:08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelBatchJudgeStatusVo", description = "批量判断是否为优势型号的Vo类")
public class AdvantageModelBatchJudgeStatusVo {
    @ApiModelProperty(value = "型号信息集合", dataType = "String")
    private List<AdvantageModelJudgeStatusVo> advantageModelList;
}
