package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @time   根据单价获取阶梯价格数据的Vo类
 * @author linuo
 * @time   2023年4月24日09:28:57
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceGetDataVo", description = "根据单价获取阶梯价格数据的Vo类")
public class LadderPriceGetDataVo {
    @ApiModelProperty(value = "单价", dataType = "BigDecimal")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    private Integer currency;
}