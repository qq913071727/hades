package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   通过起订量批量配置阶梯价格的Vo类
 * @author linuo
 * @time   2023年4月23日10:22:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceBatchSetByMoqVo", description = "通过起订量批量配置阶梯价格的Vo类")
public class LadderPriceBatchSetByMoqVo {
    @ApiModelProperty(value = "库存数据id列表", dataType = "List")
    private List<Integer> idList;
}