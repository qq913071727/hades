package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @time   库存信息编辑更新接口的Vo类
 * @author linuo
 * @time   2022年9月22日14:27:22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryEditVo", description = "库存信息编辑更新接口的Vo类")
public class InventoryEditVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    private Integer packing;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "单价/最低阶梯价", dataType = "Integer")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;
}