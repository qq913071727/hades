package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   库存信息列表查询接口的Vo类
 * @author swq
 * @time   2022年8月23日16:24:52
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryQueryVo", description = "库存信息列表查询接口的Vo类")
public class InventoryQueryVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建日期开始", dataType = "String")
    private String startDate;

    @ApiModelProperty(value = "创建日期结束", dataType = "String")
    private String endDate;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "String")
    private String status;

    @ApiModelProperty(value = "库存所在地", dataType = "String")
    private String inventoryHome;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;

    @ApiModelProperty(value = "查询页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数据数量", dataType = "Integer")
    private Integer pageSize;
}
