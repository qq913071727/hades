package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   判断是否为优势型号的Vo类
 * @author linuo
 * @time   2022年9月28日09:26:09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelJudgeStatusVo", description = "判断是否为优势型号的Vo类")
public class AdvantageModelJudgeStatusVo {
    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "需要的货物数量", dataType = "Integer")
    private Integer needGoodsCount;
}
