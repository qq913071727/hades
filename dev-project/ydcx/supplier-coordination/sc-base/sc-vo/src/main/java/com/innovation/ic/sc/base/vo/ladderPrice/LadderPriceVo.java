package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceVo", description = "阶梯价格配置的Vo类")
public class LadderPriceVo {
    @ApiModelProperty(value = "库存id", dataType = "String")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格id", dataType = "String")
    private Integer ladderPriceId;
}