package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   阶梯价格配置的Vo类
 * @author linuo
 * @time   2022年9月19日16:03:09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceConfigVo", description = "阶梯价格配置的Vo类")
public class LadderPriceConfigVo {
    @ApiModelProperty(value = "起订量", dataType = "Integer")
    private Integer mq;

    @ApiModelProperty(value = "降比", dataType = "BigDecimal")
    private BigDecimal lower;
}