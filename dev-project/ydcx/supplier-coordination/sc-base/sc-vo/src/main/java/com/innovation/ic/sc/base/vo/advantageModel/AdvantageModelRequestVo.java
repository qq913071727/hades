package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号信息列表查询的请求Vo类
 * @author linuo
 * @time   2022年8月31日11:21:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelRequestVo", description = "优势型号信息列表查询的请求Vo类")
public class AdvantageModelRequestVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "审核状态", dataType = "String")
    private String auditStatus;

    @ApiModelProperty(value = "优势状态", dataType = "String")
    private String advantageStatus;

    @ApiModelProperty(value = "创建日期开始", dataType = "String")
    private String startDate;

    @ApiModelProperty(value = "创建日期结束", dataType = "String")
    private String endDate;

    @ApiModelProperty(value = "查询页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数据数量", dataType = "Integer")
    private Integer pageSize;
}
