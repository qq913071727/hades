package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/***
 * 优势品牌添加
 */
@Data
@ApiModel(value = "AdvantageBrandAddVo", description = "添加优势品牌")
public class AdvantageBrandAddVo {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "优势性质 1代理  2原厂  3分销", dataType = "Integer")
    private Integer dominantNature;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;


    @ApiModelProperty(value = "有效期开始", dataType = "Date")
    private Date certificateValidityStart;


    @ApiModelProperty(value = "有效期结束", dataType = "Date")
    private Date certificateValidityEnd;


    @ApiModelProperty(value = "关联图片ids", dataType = "String")
    private List<String> relationPictureIds;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "情况说明", dataType = "String")
    private String description;


    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;
}