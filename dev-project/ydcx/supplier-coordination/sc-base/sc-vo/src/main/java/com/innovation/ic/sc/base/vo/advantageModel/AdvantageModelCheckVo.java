package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号校验的Vo类
 * @author linuo
 * @time   2022年9月2日15:30:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelCheckVo", description = "优势型号校验的Vo类")
public class AdvantageModelCheckVo {
    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;
}
