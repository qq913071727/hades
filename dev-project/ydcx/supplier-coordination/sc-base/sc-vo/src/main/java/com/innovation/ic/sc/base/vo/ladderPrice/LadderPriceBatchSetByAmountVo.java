package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   阶梯价格批量配置接口的Vo类
 * @author linuo
 * @time   2023年4月23日10:22:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceBatchSetByAmountVo", description = "通过单价批量配置阶梯价格的Vo类")
public class LadderPriceBatchSetByAmountVo {
    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "库存数据id列表", dataType = "List")
    private List<Integer> idList;
}