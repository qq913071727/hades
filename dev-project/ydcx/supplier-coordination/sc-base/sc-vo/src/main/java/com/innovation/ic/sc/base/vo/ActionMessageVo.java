package com.innovation.ic.sc.base.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ActionMessageVo {

    private String content;

    private List<String> createUserId;

    private List<String> companyName;
}
