package com.innovation.ic.sc.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @time   库存批量导入参数的Vo类
 * @author linuo
 * @time   2022年8月23日14:41:01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBatchAddParamVo", description = "库存批量导入参数的Vo类")
public class InventoryBatchAddParamVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "String")
    private String packing;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "单价/最低阶梯价", dataType = "Integer")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "String")
    private String currency;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "String")
    private String inventoryHome;
}