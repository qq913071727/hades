package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   优势型号批量导入的Vo类
 * @author linuo
 * @time   2022年8月31日14:26:04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelBatchAddVo", description = "优势型号批量导入的Vo类")
public class AdvantageModelBatchAddVo {
    @ApiModelProperty(value = "优势型号数据", dataType = "AdvantageModelBatchAddVo")
    private List<AdvantageModelBatchAddParamVo> list;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    private String validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    private String validityDateEnd;
}
