package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UploadFileVo", description = "上传文件vo类")
public class UploadFileVo {


    @ApiModelProperty(value = "图片id", dataType = "Integer")
    private Integer id;

    /**
     *自定义 1 营业执照  2身份证 正面  3、身份证反面  4 代理证明文件
     */
    @ApiModelProperty(value = "自定义 1 营业执照  2身份证 正面  3、身份证反面  4 代理证明文件", dataType = "Integer")
    private Integer customType;



}
