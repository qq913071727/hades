package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号申请延期的Vo类
 * @author linuo
 * @time   2022年8月31日17:19:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelApplyDelayVo", description = "优势型号申请延期的Vo类")
public class AdvantageModelApplyDelayVo {
    @ApiModelProperty(value = "优势型号id", dataType = "String")
    private Integer id;

    @ApiModelProperty(value = "延期日期(格式为yyyy-MM-dd)", dataType = "String")
    private String delayDate;
}