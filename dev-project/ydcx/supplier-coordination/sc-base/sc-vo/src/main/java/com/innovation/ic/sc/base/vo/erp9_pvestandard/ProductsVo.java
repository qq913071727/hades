package com.innovation.ic.sc.base.vo.erp9_pvestandard;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Description: 产品表
 * @ClassName: Products
 * @Author: myq
 * @Date: 2022/8/5 上午11:28
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("产品型号")
public class ProductsVo implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌Id", dataType = "String")
    private String brandID;



}
