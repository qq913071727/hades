package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   阶梯价格修改接口的Vo类
 * @author linuo
 * @time   2022年9月21日11:03:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceEditVo", description = "阶梯价格修改接口的Vo类")
public class LadderPriceEditVo {
    @ApiModelProperty(value = "阶梯价格信息id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "配置方式(1:按起订量配置、2:按单价配置)", dataType = "Integer")
    private Integer configType;

    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "按单价配置的数据", dataType = "List")
    private List<LadderPriceAmountConfigVo> ladderPriceConfigByAmount;

    @ApiModelProperty(value = "按起订量配置的数据", dataType = "List")
    private List<LadderPriceConfigVo> ladderPriceConfigByMoq;
}