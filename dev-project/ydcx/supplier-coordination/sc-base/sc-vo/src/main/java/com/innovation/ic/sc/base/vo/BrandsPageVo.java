package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * 品牌分页显示时的VO
 */
@Data
@ApiModel(value = "BrandsPageVo", description = "品牌分页显示时的VO")
public class BrandsPageVo {

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "品牌中文名称", dataType = "String")
    private String searchName;


    @ApiModelProperty(value = "供应商id", dataType = "String")
    private String enterpriseId;



}
