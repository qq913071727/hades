package com.innovation.ic.sc.base.vo.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DefaultLadderPriceVo", description = "阶梯价格的Vo类")
public class DefaultLadderPriceVo {
    @ApiModelProperty(value = "起订量", dataType = "String")
    private String mq;

    @ApiModelProperty(value = "降比", dataType = "Float")
    private Float lower;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "Float")
    private Float enterpriseID;
}