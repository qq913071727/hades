package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   根据型号查询所属品牌的Vo类
 * @author linuo
 * @time   2022年12月5日16:03:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelGetBrandsVo", description = "根据型号查询所属品牌的Vo类")
public class AdvantageModelGetBrandsVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;
}
