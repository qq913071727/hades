package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 验证code的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VerificationCodeVo", description = "验证code的vo类")
public class VerificationCodeVo {

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phoneNum;

    @ApiModelProperty(value = "短信验证码", dataType = "String")
    private String code;

}
