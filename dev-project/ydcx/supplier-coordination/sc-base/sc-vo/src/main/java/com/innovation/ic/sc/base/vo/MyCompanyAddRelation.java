package com.innovation.ic.sc.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class MyCompanyAddRelation implements Serializable {
    private static final long serialVersionUID = 7014286474003830460L;

    @ApiModelProperty(value = "公司id", dataType = "string")
    private String id;

    @ApiModelProperty(value = "关联公司id", dataType = "string")
    private String subId;

    @ApiModelProperty(value = "附件主键id,多个用逗号分隔", dataType = "String")
    private String files;

    @ApiModelProperty(value = "创建人", dataType = "string")
    private String creator;
}