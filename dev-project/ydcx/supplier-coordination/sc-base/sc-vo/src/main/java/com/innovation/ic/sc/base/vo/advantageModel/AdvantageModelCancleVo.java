package com.innovation.ic.sc.base.vo.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   取消优势型号的Vo类
 * @author linuo
 * @time   2022年8月31日10:37:07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelCancleVo", description = "取消优势型号的Vo类")
public class AdvantageModelCancleVo {
    @ApiModelProperty(value = "取消优势型号的id集合", dataType = "String")
    private List<Integer> idList;

    @ApiModelProperty(value = "是否全部(true是、false否)", dataType = "Boolean")
    private Boolean isAll;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "审核状态", dataType = "String")
    private String auditStatus;

    @ApiModelProperty(value = "优势状态", dataType = "String")
    private String advantageStatus;
}