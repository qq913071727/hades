package com.innovation.ic.sc.base.vo;

import com.innovation.ic.sc.base.vo.erp9_pvestandard.ProductsVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 用户产品关系VO类
 * @ClassName: MapsUserProductVo
 * @Author: myq
 * @Date: 2022/8/5 下午2:04
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class MapsUserProductVo {
    @ApiModelProperty(value = "品牌Id",dataType = "string")
    private String 	BrandsID;

    @ApiModelProperty(value = "品牌中文名称",dataType = "string")
    private String BrandsCN;

    @ApiModelProperty(value = "子菜单",dataType = "list")
    private List<ProductsVo> childMapsList = new ArrayList<ProductsVo>();
}