package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   导入方式枚举
 * @author linuo
 * @time   2022年8月23日14:49:54
 */
public enum ImportTypeEnum {
    ADD(1, "添加方式"),
    COVER(2, "覆盖方式");

    private Integer code;
    private String desc;

    ImportTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ImportTypeEnum of(Integer code) {
        for (ImportTypeEnum c : ImportTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (ImportTypeEnum c : ImportTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}