package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   取消优势型号的pojo类
 * @author linuo
 * @time   2022年9月1日14:52:09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SpecialsScTopViewClosePojo", description = "取消优势型号的pojo类")
public class SpecialsScTopViewCloseDelPojo {

    @ApiModelProperty(value = "ERPID", dataType = "String")
    private String ID;

    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String PartNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String Brand;
}