package com.innovation.ic.sc.base.pojo.constant;

/**
 * @desc   sc数据库中表的名称
 * @author linuo
 * @time   2022年8月25日16:52:26
 */
public class ScTableName {
    /** 库存表 */
    public static final String INVENTORY = "inventory";

    /** 用户表 */
    public static final String USER = "user";

    /** 优势品牌表 */
    public static final String ADVANTAGE_BRAND = "advantage_brand";

    /** 优势型号表 */
    public static final String ADVANTAGE_MODEL = "advantage_model";

    /** 人员管理表 */
    public static final String PERSONNEL_MANAGEMENT = "personnel_management";

    /** 品牌表 */
    public static final String BRAND = "brand";
}