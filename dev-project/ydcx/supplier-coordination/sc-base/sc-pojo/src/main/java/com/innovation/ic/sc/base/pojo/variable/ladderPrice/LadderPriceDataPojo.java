package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   阶梯价格数据的Pojo类
 * @author linuo
 * @time   2023年1月11日14:33:46
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceDataPojo", description = "阶梯价格数据的Pojo类")
public class LadderPriceDataPojo {
    @ApiModelProperty(value = "起订量", dataType = "Integer")
    private Integer mq;

    @ApiModelProperty(value = "金额", dataType = "BigDecimal")
    private BigDecimal price;
}