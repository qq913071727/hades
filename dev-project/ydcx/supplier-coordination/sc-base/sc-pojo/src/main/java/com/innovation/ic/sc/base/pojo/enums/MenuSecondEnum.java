package com.innovation.ic.sc.base.pojo.enums;



import com.innovation.ic.sc.base.pojo.constant.model.MenuRouteConstants;
import com.innovation.ic.sc.base.pojo.constant.model.MenuType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 第二个菜单 枚举
 * @Date 2023/2/9 13:42
 **/
public enum MenuSecondEnum {
    // 订单管理: 我的订单 2-1
    ORDER_MY_ORDER("2", "2-1", "我的订单", MenuType.MENU_ITEM, MenuRouteConstants.MY_ORDER, ""),

    // 报价管理: 报价任务 3-1
    QUOTATION_QUOTATION_TASK("3", "3-1", "报价任务", MenuType.MENU_ITEM, MenuRouteConstants.QUOTATION_TASK, ""),
    // 报价管理: 我的议价 3-2
    QUOTATION_MY_BARGAINING_PRICE("3", "3-2", "我的议价", MenuType.MENU_ITEM, MenuRouteConstants.MY_BRAGINING, ""),
    // 报价管理: 搜商机 3-3
    QUOTATION_SEARCH_ENGINE("3", "3-3", "搜商机", MenuType.MENU_ITEM, MenuRouteConstants.SOMMER, ""),

    // 库存管理: 上传库存 4-1
    INVENTORY_UPLOAD_INVENTORY("4", "4-1", "上传库存", MenuType.MENU_ITEM, MenuRouteConstants.UPLOAD_STOCK, ""),
    // 库存管理: 阶梯价格管理 4-2
    INVENTORY_LADDER_PRICE_MANAGEMENT("4", "4-2", "阶梯价格管理", MenuType.MENU_ITEM,  MenuRouteConstants.STEPPED_PRICE, ""),
    // 库存管理: 管理库存 4-3
    INVENTORY_MANAGE_INVENTORY("4", "4-3", "管理库存", MenuType.MENU_ITEM, MenuRouteConstants.MANAGE_STOCK, ""),
    // 库存管理: 优势品牌 4-4
    INVENTORY_ADVANTAGE_BRANDS("4", "4-4", "优势品牌", MenuType.MENU_ITEM, MenuRouteConstants.ADVANTAGE_BRAND, ""),

    // 财务管理: 对账单 5-1
    FINANCIAL_ACCOUNT_STATEMENT("5", "5-1", "对账单", MenuType.MENU_ITEM, MenuRouteConstants.AWAIT_BILL, ""),
    // 财务管理: 待开发票 5-2
    FINANCIAL_TO_BE_INVOICED("5", "5-2", "待开发票", MenuType.MENU_ITEM, MenuRouteConstants.AWAIT_INVOICED, ""),
    // 财务管理: 已开发票 5-3
    FINANCIAL_INVOICED("5", "5-3", "已开发票", MenuType.MENU_ITEM, MenuRouteConstants.INVOICED, ""),
    // 财务管理: 合同管理 5-4
    FINANCIAL_CONTRACT_MANAGEMENT("5", "5-4", "合同管理", MenuType.MENU_ITEM, MenuRouteConstants.CONTRACT_MANAGE, ""),

    // 系统管理: 账号管理 6-1
    SYSTEM_ACCOUNT_MANAGEMENT("6", "6-1", "账号管理", MenuType.MENU_ITEM, MenuRouteConstants.MANAGE_ACCOUNT, ""),
    // 系统管理: 角色管理 6-2
    SYSTEM_ROLE_MANAGEMENT("6", "6-2", "角色管理", MenuType.MENU_ITEM,  MenuRouteConstants.MANAGE_ROLE, ""),
    // 系统管理: 我的公司管理 6-3
    SYSTEM_MY_COMPANY_MANAGEMENT("6", "6-3", "我的公司管理", MenuType.MENU_ITEM, MenuRouteConstants.MY_COMPANY, ""),
    // 系统管理: 消息提醒 6-4
    SYSTEM_MESSAGE_REMINDER("6", "6-4", "消息提醒", MenuType.MENU_ITEM, MenuRouteConstants.NEWS, "");

    //父级菜单
    private String parentMenu;
    //id
    private String id;
    //名称
    private String name;
    //菜单标志
    private Integer menuType;
    //路由
    private String path;
    //图标
    private String icon;

    MenuSecondEnum(String parentMenu, String id, String name, Integer menuType, String path, String icon) {
        this.parentMenu = parentMenu;
        this.id = id;
        this.name = name;
        this.menuType = menuType;
        this.path = path;
        this.icon = icon;
    }

    public String getParentMenu() {
        return parentMenu;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }

    public static List<MenuSecondEnum> getSecondMenuList(String parentMenu) {
        List list = new ArrayList();
        for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.values()) {
            if (menuSecondEnum.getParentMenu().equals(parentMenu)) {
                list.add(menuSecondEnum);
            }
        }
        return list;
    }
}
