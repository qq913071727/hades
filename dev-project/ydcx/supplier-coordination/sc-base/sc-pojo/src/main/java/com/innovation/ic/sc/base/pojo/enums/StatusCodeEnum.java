package com.innovation.ic.sc.base.pojo.enums;

/**
 * @description 状态枚举类
 * @parms
 * @return
 * @author Mr.myq
 * @datetime 2022/8/10 上午10:43
 */
public enum StatusCodeEnum {

    /**
     *  错误码 500开头皆为服务器内部错误
     *  50010 参数错误, 50020 逻辑错误 。。。
     */
    INTERNAL_ERROR(500, "服务器内部错误"),
    PARAM_ERROR(50010, "参数错误"),
    PARAM_MATCH_ERROR(50011, "参数规则不匹配"),
    LOGIC_ERROR(50020, "逻辑错误"),
    OTHER_ERROR(50090, "其他错误,请联系管理员"),
    ;

    private Integer code;
    private String desc;

    StatusCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}