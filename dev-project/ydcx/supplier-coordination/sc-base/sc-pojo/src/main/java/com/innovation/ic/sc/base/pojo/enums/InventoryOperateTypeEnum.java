package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   库存操作类型枚举
 * @author linuo
 * @time   2022年9月22日15:06:48
 */
public enum InventoryOperateTypeEnum {
    DOWN_SHELVES(0, "下架"),
    UP_SHELVES(1, "上架"),
    DELETE(2, "删除");

    private Integer code;
    private String desc;

    InventoryOperateTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static InventoryOperateTypeEnum of(Integer code) {
        for (InventoryOperateTypeEnum c : InventoryOperateTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (InventoryOperateTypeEnum c : InventoryOperateTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}