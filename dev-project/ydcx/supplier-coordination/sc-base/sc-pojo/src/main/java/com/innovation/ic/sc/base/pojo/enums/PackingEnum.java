package com.innovation.ic.sc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   包装枚举
 * @author linuo
 * @time   2022年12月28日15:15:40
 **/
public enum PackingEnum {
    TR(1, "卷带(TR)"),
    PACK(2, "Pack");

    private int type;

    private String desc;

    PackingEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static PackingEnum parse(Integer type) {
        for (PackingEnum scErpCurrencyEnum : PackingEnum.values()) {
            if (scErpCurrencyEnum.getType() == type) {
                return scErpCurrencyEnum;
            }
        }
        return null;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (PackingEnum value : PackingEnum.values()) {
            map = new LinkedHashMap<String, Object>(4);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}