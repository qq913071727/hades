package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageBrandUserApiPojo", description = "外部接口返回优势品牌主账号信息")
public class AdvantageBrandUserApiPojo {


    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "用户名字", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "是否主账号", dataType = "boolean")
    private boolean accordWith;


}
