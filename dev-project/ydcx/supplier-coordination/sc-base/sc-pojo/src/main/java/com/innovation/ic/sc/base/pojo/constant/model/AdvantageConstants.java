package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @desc   优势功能使用的常量值
 * @author linuo
 * @time   2023年2月7日14:45:14
 */
public class AdvantageConstants {
    /** 品牌 */
    public static final String BRAND = "Brand";

    /** 型号 */
    public static final String PART_NUMBER = "PartNumber";

    /** 供应商id */
    public static final String ENTERPRISE_ID = "EnterpriseID";

    /** id */
    public static final String ID = "ID";

    /** erpId字段 */
    public static final String ERP_ID_FIELD = "erp_id";
}