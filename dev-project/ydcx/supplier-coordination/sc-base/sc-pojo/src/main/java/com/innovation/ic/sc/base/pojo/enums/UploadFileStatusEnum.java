package com.innovation.ic.sc.base.pojo.enums;

/**
 * 上传文件类型枚举
 */
public enum UploadFileStatusEnum {
    DEFAULT("100", "默认"),
    NORMAL("200", "正常"),
    ;

    private String code;
    private String desc;

    UploadFileStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}