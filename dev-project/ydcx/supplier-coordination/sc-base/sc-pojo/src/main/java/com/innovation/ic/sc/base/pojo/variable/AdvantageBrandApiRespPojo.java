package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * 优势品牌返回pojo,针对以外部接口
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageBrandRespPojo", description = "优势品牌返回pojo,针对以外部接口")
public class AdvantageBrandApiRespPojo {


    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "主账号信息", dataType = "List")
    private List<AdvantageBrandUserApiPojo> advantageBrandUser;



}
