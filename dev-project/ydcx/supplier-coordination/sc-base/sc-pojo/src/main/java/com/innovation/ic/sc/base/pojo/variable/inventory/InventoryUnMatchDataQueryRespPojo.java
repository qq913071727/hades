package com.innovation.ic.sc.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   未匹配成功数据返回的pojo类
 * @author linuo
 * @time   2023年2月9日16:54:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryUnMatchDataQueryRespPojo", description = "未匹配成功数据返回的pojo类")
public class InventoryUnMatchDataQueryRespPojo {
    @ApiModelProperty(value = "未匹配成功的数据", dataType = "List")
    private List<InventoryUnMatchDataPojo> data;

    @ApiModelProperty(value = "未匹配成功数据的数量", dataType = "Integer")
    private Integer count;
}