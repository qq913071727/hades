package com.innovation.ic.sc.base.pojo.constant.handler;

/**
 * @desc   rabbitMq相关常量参数
 * @author linuo
 * @time   2022年9月1日10:11:24
 */
public class RabbitMqConstants {
    /**
     * 库存新增/修改队列(sc向erp推送消息)
     */
    public static final String SC_ERP_INVENTORY_ADD_EDIT_QUEUE = "sc.to.erp.inventory.add.edit";
    /**
     * 库存上架/下架/删除队列(sc向erp推送消息)
     */
    public static final String SC_ERP_INVENTORY_UPDATE_STATUS_DELETE_QUEUE = "sc.to.erp.inventory.update.status.delete";
    /**
     * 我的公司建立关联关系(sc向scs推送消息)
     */
    public static final String SC_ERP_ADD_RELATION_COMPANY_QUEUE = "sc.to.erp.scs.company.relation.add";

    /**
     * 我的公司删除关联关系(sc向erp推送消息)
     */
    public static final String SC_ERP_DEL_RELATION_COMPANY_QUEUE = "sc.to.erp.scs.company.relation.delete";

    /**
     * 子账号数据新增/修改/(sc向erp推送消息)
     */
    public static final String SC_ERP_ADD_SUBUSERS = "sc.to.erp.special.subusers.add";

    /**
     * 子账号数据修改状态/(sc向erp推送消息)
     */
    public static final String SC_ERP_ENABLE_SUBUSERS = "sc.to.erp.special.users.enable";

    /**
     * 子账号数据修改手机号/(sc向erp推送消息)
     */
    public static final String SC_ERP_UPDATE_PHONE = "sc.to.erp.special.users.phone.modify";

    /**
     * 注册公司(sc向erp推送消息)
     */
    public static final String SC_ERP_REGISTER_COMPANY = "sc.to.erp.company.register";
    /**
     * 注册公司(sc向erp推送消息)
     */
    public static final String SC_ERP_UPDATE_COMPANY = "sc.to.erp.company.update";

    /**
     * 我的公司删除关联关系(sc向erp推送消息)
     */
    public static final String  SC_ERP_DELETE_COMPANY = "sc.to.erp.company.relation.del";

    /**
     * 我的公司建立关联关系(sc向erp推送消息)
     */
    public static final String SC_ERP_RELATION_COMPANY = "sc.to.erp.company.relation.add";

//    /**
//     * 子账号数据新增(erp向sc推送消息)
//     */
//    public static final String ERP_SC_ADD_SUBUSERS = "erp.to.sc.special.subusers.add";

    /**
     * 主账号数据新增/修改/(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_USERS = "erp.to.sc.special.users.add";


    /**
     * 我的公司付款账户新增(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_BOOKACCOUNT = "erp.to.sc.bookaccount.add";


    /**
     * 我的公司付款账户新增成功(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_BOOKACCOUNT_SUCCESS = "erp.to.sc.bookaccount.add.notify";
    /**
     * 我的公司付款账号关闭(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_BOOKACCOUNT_CLOSE = "erp.to.sc.bookaccount.close";

    /**
     * 密码重置(erp向sc推送消息)
     */
    public static final String ERP_SC_RESET_USERS = "erp.to.sc.special.users.reset";

    /**
     * 主账号数据删除/(erp向sc推送消息)
     */
    public static final String ERP_SC_DELETE_ENBLE = "erp.to.sc.special.users.enable";

    /**
     * 优势型号新增/修改/延期队列(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_QUEUE = "erp.to.sc.special.add";

    /**
     * 优势型号Erp通知(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_NOTIFY_QUEUE = "erp.to.sc.special.add.notify";

    /**
     * 优势型号品牌通知(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_BRAND_NOTIFY_QUEUE = "erp.to.sc.brand.add.notify";

    /**
     * 库存标准品牌、型号确认(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_INVENTORY_MATCH_QUEUE = "erp.to.sc.inventory.match";

    /**
     * 库存数据删除(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_INVENTORY_DEL_QUEUE = "erp.to.sc.inventory.del";

    /**
     * 优势型号新增/修改/延期队列(sc向erp推送消息)
     */
    public static final String SC_ERP_ADD_QUEUE = "sc.to.erp.special.add";

    /**
     * 优势型号新增/修改/延期队列(sc向erp推送消息)
     */
    public static final String SC_ERP_UPDATE_SPECIAL_QUEUE = "sc.to.erp.special.update";
    /**
     * 优势型号再次上传(sc向erp推送消息)
     */
    public static final String SC_ERP_SUBMIT_FILE_QUEUE = "sc.to.erp.brand.addfiles";

    /**
     * 优势型号审批(erp向sc推送消息)
     */
    public static final String ERP_SC_AUDIT_QUEUE = "erp.to.sc.special.audit";

    /**
     * 优势型号审批(sc向erp推送消息)
     */
    public static final String SC_ERP_AUDIT_QUEUE = "sc.to.erp.special.audit";

    /**
     * 取消优势型号(erp向sc推送消息)
     */
    public static final String ERP_SC_CLOSE_QUEUE = "erp.to.sc.special.close";

    /**
     * 取消优势型号(sc向erp推送消息)
     */
    public static final String SC_ERP_CLOSE_QUEUE = "sc.to.erp.special.close";

    /**
     * 优势型号状态恢复为可用(sc向erp推送消息)
     */
    public static final String SC_ERP_ENABLE_QUEUE = "sc.to.erp.special.enable";

    /**
     * 删除优势型号(erp向sc推送消息)
     */
    public static final String ERP_SC_DEL_QUEUE = "erp.to.sc.special.del";

    /**
     * 删除优势型号(sc向erp推送消息)
     */
    public static final String SC_ERP_DEL_QUEUE = "sc.to.erp.special.del";

    /**
     * 添加erp我的公司(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_COMPANY = "erp.to.sc.enterprise.add";

    /**
     * 修改我的公司(erp向sc推送消息)
     */
    public static final String ERP_SC_UPDATE_COMPANY = "erp.to.sc.enterprise.update";

    /**
     * 删除关联关系(erp向sc推送消息)
     */
    public static final String ERP_SC_DELETE_COMPANY = "erp.to.sc.company.relation.del";

    /**
     * 添加枚举类型(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_ENUM = "erp.to.sc.enum.add";

    /**
     * 删除枚举值(erp向sc推送消息)
     */
    public static final String ERP_SC_DELETE_ENUM_ITEM = "erp.to.sc.enum.item.del";

    /**
     * 添加枚举值(erp向sc推送消息)
     */
    public static final String ERP_SC_ADD_ENUM_ITEM = "erp.to.sc.enum.add";

    /**
     * 关联关系审核(erp向sc推送消息)
     */
    public static final String ERP_SC_AUDIT_COMPANY = "erp.to.sc.company.relation.audit";

    /**
     * 添加我的公司(erp向sc推送消息)
     */
    public static final String ERP_SC_RELATION_COMPANY = "erp.to.sc.company.relation.add";

    /**
     * 添加枚举值(erp向sc推送消息)
     */
    public static final String SC_ERP_ADD_DICTIONARIES_TYPE = "erp.to.sc.enum.add";

    /**
     * 添加枚举值(erp向sc推送消息)
     */
    public static final String SC_ERP_ADD_DICTIONARIES = "erp.to.sc.enum.item.add";

    /**
     * 删除枚举值(erp向sc推送消息)
     */
    public static final String SC_ERP_DEL_DICTIONARIES = "erp.to.sc.enum.item.del";



    /**
     * sc与erp的交换机
     */
    public static final String SC_ERP_EXCHANGE = "SC_ERP";

    /**
     * 队列类型
     */
    public static final String ERP_TO_SC_QUEUE_NAME = "erp.to.sc";

    /**
     * 队列类型
     */
    public static final String SC_TO_ERP_QUEUE_NAME = "sc.to.erp";


    /**
     * 队列类型
     */
    public static final String SC_TO_ERP_CRM_QUEUE_NAME = "sc.to.erp.crm";

    /**
     * 队列类型
     */
    public static final String DIRECT_TYPE = "direct";

    /**
     * 操作名字
     */
    public static final String MESSAGE_NAME = "MessageName";

    /**
     * 数据
     */
    public static final String MESSAGE_BODY = "MessageBody";

    /**
     *  erp推送节点动作消息到sc的action_message表中 quue
     */
    public static final String ERP_2_SC_ACTION_MESSAGE_QUEUE = "erp.2.sc.action.message.queue";

    /**
     *  erp推送节点动作消息到sc的action_message表中 EXCHANGE
     */
    public static final String ERP_2_SC_ACTION_MESSAGE_EXCHANGE = "erp.2.sc.action.message.exchange";

    /**
     *  erp推送节点动作消息到sc的action_message表中 ROUTING_KEY
     */
    public static final String ERP_2_SC_ACTION_MESSAGE_ROUTING_KEY = "erp.2.sc.action.message.routing.key";

    /**
     * 优势品牌新增(erp向sc推送消息)
     */
    public static final String ERP_SC_BRAND_ADD_QUEUE = "erp.to.sc.brand.add";

    /**
     * 优势型号审批(erp_sc)
     */
    public static final String ERP_SC_BRAND_AUDIT_QUEUE = "erp.to.sc.brand.audit";

    /**
     * 优势品牌关闭(erp_sc)
     */
    public static final String ERP_SC_BRAND_CLOSE_QUEUE = "erp.to.sc.brand.close";

    /**
     * 优势型号删除(erp_sc)
     */
    public static final String ERP_SC_BRAND_DEL_QUEUE = "erp.to.sc.brand.del";

    /**
     * 优势型号删除(sc_erp)
     */
    public static final String SC_ERP_UPDATE_BRAND_QUEUE = "sc.to.erp.brand.update";

    /**
     * 优势型号关闭(sc_erp)
     */
    public static final String SC_ERP_BRAND_CLOSE_QUEUE = "sc.to.erp.brand.close";

    /**
     * 优势型号删除(sc_erp)
     */
    public static final String SC_ERP_BRAND_DEL_QUEUE = "sc.to.erp.brand.del";

    /**
     * 优势型号新增(sc_erp)
     */
    public static final String SC_ERP_BRAND_ADD_QUEUE = "sc.to.erp.brand.add";

    /**
     * 添加我的公司币种(erp向sc推送消息)
     */
    public static final String ERP_SC_CURRENCY_ADD = "erp.to.sc.currency.add";

    /**
     * 删除我的公司币种(erp向sc推送消息)
     */
    public static final String ERP_SC_CURRENCY_DEL = "erp.to.sc.currency.del";

    /**
     * 更新我的公司币种(erp向sc推送消息)
     */
    public static final String ERP_SC_CURRENCY_UPDATE = "erp.to.sc.currency.update";

    /**
     * 更新我的公司币种(sc向ERP推送消息)
     */
    public static final String SC_ERP_CURRENCY_UPDATE = "sc.to.erp.bookaccount.update";
}