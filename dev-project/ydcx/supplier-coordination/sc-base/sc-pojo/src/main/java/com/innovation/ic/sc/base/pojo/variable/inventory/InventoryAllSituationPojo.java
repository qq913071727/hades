package com.innovation.ic.sc.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   库存所有情况的pojo类
 * @author linuo
 * @time   2023年2月13日15:20:13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryAllSituationPojo", description = "库存所有情况的pojo类")
public class InventoryAllSituationPojo {
    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;

    @ApiModelProperty(value = "供应商id", dataType = "String")
    private String enterpriseId;
}