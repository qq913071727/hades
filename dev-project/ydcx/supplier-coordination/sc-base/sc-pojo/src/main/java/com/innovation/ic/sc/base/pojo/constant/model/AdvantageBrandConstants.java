package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @desc   优势品牌功能使用的常量值
 * @author linuo
 * @time   2023年1月13日09:52:31
 */
public class AdvantageBrandConstants {
    /** 品牌字段 */
    public static final String BRAND_FIELD = "brand";

    /** 优势状态字段 */
    public static final String ADVANTAGE_STATUS_FIELD = "advantage_status";
}