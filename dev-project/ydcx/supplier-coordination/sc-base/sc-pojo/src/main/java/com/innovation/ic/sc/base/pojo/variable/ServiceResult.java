package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * 调用服务层的返回对象
 * @param <T>
 */
@Data
@ApiModel(value = "ServiceResult<T>", description = "service返回值对象")
public class ServiceResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    /**
     * 查询成功
     */
    public static final String SELECT_SUCCESS = "查询成功";

    /**
     * 查询失败
     */
    public static final String SELECT_FAIL = "查询失败";

    /**
     * 更新成功
     */
    public static final String UPDATE_SUCCESS = "更新成功";

    /**
     * 更新失败
     */
    public static final String UPDATE_FAIL = "更新失败";

    /**
     * 插入成功
     */
    public static final String INSERT_SUCCESS = "插入成功";

    /**
     * 插入失败
     */
    public static final String INSERT_FAIL = "插入失败";

    /**
     * 导入成功
     */
    public static final String IMPORT_SUCCESS = "导入成功";

    /**
     * 导入失败
     */
    public static final String IMPORT_FAIL = "导入失败";

    /**
     * 删除成功
     */
    public static final String DELETE_SUCCESS = "删除成功";

    /**
     * 删除失败
     */
    public static final String DELETE_FAIL = "删除失败";

    /**
     * 操作成功
     */
    public static final String OPERATE_SUCCESS = "操作成功";

    /**
     * 操作失败
     */
    public static final String OPERATE_FAIL = "操作失败";

    /**
     * 同步成功
     */
    public static final String SYNC_SUCCESS = "同步成功";

    /**
     * 同步失败
     */
    public static final String SYNC_FAIL = "同步失败";

    /**
     * 超时
     */
    public static final String TIME_OUT = "接口调用超时,请5秒后重试";

    public ServiceResult(T data, boolean success, String message) {
        this.result = data;
        this.success = success;
        this.message = message;
    }

    public ServiceResult() {
    }

    /**
     * @return
     * @description 成功
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:45
     */
    public static <T> ServiceResult<T> ok(T data, String message) {
        return new ServiceResult<T>(data,true,  message);
    }

    /**
     * @return
     * @description 成功 不带返回值
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:45
     */
    public static ServiceResult ok(String message) {
        return new ServiceResult(null,true,  message);
    }

    /**
     * @return
     * @description 失败
     * @parms
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:45
     */
    public static <T> ServiceResult<T> error(String message) {
        return new ServiceResult<T>(null,false,  message);
    }
}