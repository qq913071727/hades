package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   库存结果枚举
 * @author linuo
 * @time   2022年9月28日09:35:38
 */
public enum InventoryResultEnum {
    SUFFICIENT(1, "充足"),
    INSUFFICIENT(2, "不足"),
    NO_HAVE(4, "无");

    private Integer code;
    private String desc;

    InventoryResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static InventoryResultEnum of(Integer code) {
        for (InventoryResultEnum c : InventoryResultEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (InventoryResultEnum c : InventoryResultEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}