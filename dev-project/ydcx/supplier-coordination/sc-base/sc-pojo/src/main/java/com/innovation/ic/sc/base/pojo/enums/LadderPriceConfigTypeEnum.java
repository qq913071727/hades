package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   阶梯价格配置方式枚举类
 * @author linuo
 * @time   2023年4月13日17:01:50
 */
public enum LadderPriceConfigTypeEnum {
    CONFIG_BY_MOQ(1, "按起订量配置"),
    CONFIG_BY_AMOUNT(2, "按单价配置");

    private int code;
    private String desc;

    LadderPriceConfigTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}