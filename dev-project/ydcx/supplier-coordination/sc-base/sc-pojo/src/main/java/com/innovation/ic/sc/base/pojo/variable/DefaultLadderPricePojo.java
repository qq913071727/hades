package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @desc   阶梯价格配置实体类
 * @author linuo
 * @time   2022年8月23日11:15:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DefaultLadderPricePojo", description = "默认阶梯价格配置表")
public class DefaultLadderPricePojo {


    @ApiModelProperty(value = "起订量", dataType = "Integer")
    private Integer mq;

    @ApiModelProperty(value = "降比", dataType = "BigDecimal")
    private BigDecimal lower;


}