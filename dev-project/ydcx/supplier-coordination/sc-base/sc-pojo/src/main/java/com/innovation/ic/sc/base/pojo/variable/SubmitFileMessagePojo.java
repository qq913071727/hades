package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 *  提交图片推送优势品牌消息
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SubmitFileMessagePojo {

    @ApiModelProperty(value = "ERP优势ID", dataType = "String")
    private String ID;

    @ApiModelProperty(value = "文件集合", dataType = "List")
    private List<UploadFileMessagePojo> Files;
}