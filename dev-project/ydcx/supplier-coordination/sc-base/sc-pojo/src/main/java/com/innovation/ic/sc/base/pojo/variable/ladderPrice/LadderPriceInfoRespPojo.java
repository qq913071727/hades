package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   阶梯价格信息查询返回的Pojo类
 * @author linuo
 * @time   2023年4月14日13:25:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceInfoRespPojo", description = "阶梯价格信息查询返回的Pojo类")
public class LadderPriceInfoRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "配置方式(1:按起订量配置、2:按单价配置)", dataType = "Integer")
    private Integer configType;

    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "按起订量配置的数据", dataType = "List")
    private List<LadderPriceConfigRespPojo> ladderPriceConfigByMoq;

    @ApiModelProperty(value = "按金额配置的数据", dataType = "List")
    private List<LadderPriceAmountConfigRespPojo>  ladderPriceConfigByAmount;


}
