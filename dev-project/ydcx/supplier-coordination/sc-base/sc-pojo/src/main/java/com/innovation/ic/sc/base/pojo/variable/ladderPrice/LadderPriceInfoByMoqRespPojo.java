package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   按起订量配置的阶梯价格查询返回的Pojo类
 * @author linuo
 * @time   2023年4月14日13:25:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceInfoByMoqRespPojo", description = "按起订量配置的阶梯价格查询返回的Pojo类")
public class LadderPriceInfoByMoqRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "按起订量配置的数据", dataType = "List")
    private List<LadderPriceConfigRespPojo> ladderPriceConfigData;
}
