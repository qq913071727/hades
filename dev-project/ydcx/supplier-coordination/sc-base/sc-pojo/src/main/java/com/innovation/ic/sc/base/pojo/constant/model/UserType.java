package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * 账号类型
 */
public class UserType {

    /**
     * 主账号
     */
    public static final Integer MAIN_ACCOUNT = 1;

    /**
     * 子账号
     */
    public static final Integer SUB_ACCOUNT = 0;

}
