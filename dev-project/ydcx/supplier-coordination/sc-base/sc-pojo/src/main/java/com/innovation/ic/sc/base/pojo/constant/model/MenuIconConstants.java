package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @author zengqinglong
 * @desc 菜单静态图标
 * @Date 2023/2/1 17:06
 **/
public class MenuIconConstants {
    /**
     *首页
     */
    public static final String DASHBOARD = "dashboard";

    /**
     * 库存管理
     */
    public static final String DICT = "dict";

    /**
     * 报价管理
     */
    public static final String POST = "post";

    /**
     * 订单管理
     */
    public static final String CHART = "chart";

    /**
     * 财务管理
     */
    public static final String MONEY = "money";

    /**
     * 系统管理
     */
    public static final String BUILD = "build";

}
