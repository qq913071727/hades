package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @author zengqinglong
 * @desc 菜单路由
 * @Date 2023/2/1 11:41
 **/
public class MenuRouteConstants {
    /**
     * 首页
     */
    public static final String HOME_PAGE = "/";
    /**
     * 库存管理
     */
    public static final String STOCK = "/stock";
    /**
     * 上传库存
     */
    public static final String UPLOAD_STOCK = "/uploadStock";
    /**
     * 阶梯价格管理
     */
    public static final String STEPPED_PRICE = "/steppedPrice";
    /**
     * 管理库存
     */
    public static final String MANAGE_STOCK = "/manageStock";
    /**
     * 优势品牌
     */
    public static final String ADVANTAGE_BRAND = "/advantageBrand";
    /**
     * 优势型号
     */
    public static final String ADVANTAGE_MODEL = "/advantageModel";
    /**
     * 报价管理
     */
    public static final String MANAGE_OFFER = "/manageOffer";
    /**
     * 报价任务
     */
    public static final String QUOTATION_TASK = "/quotationTask";
    /**
     * 实单报价
     */
    public static final String ACTUAL_OFFER = "/actualOffer";
    /**
     * 普通报价
     */
    public static final String COMMON_OFFER = "/commonOffer";
    /**
     * 搜商机
     */
    public static final String SOMMER = "/sommer";
    /**
     * 我的议价
     */
    public static final String MY_BRAGINING = "/myBragining";
    /**
     * 订单管理
     */
    public static final String ORDER_STOCK = "/orderStock";
    /**
     * 我的订单
     */
    public static final String MY_ORDER = "/myOrder";

    /**
     * 财务管理
     */
    public static final String FINANCE = "/finance";
    /**
     * 对账单
     */
    public static final String AWAIT_BILL = "/awaitBill";

    /**
     * 待开发票
     */
    public static final String AWAIT_INVOICED = "/awaitInvoiced";

    /**
     * 已开发票
     */
    public static final String INVOICED = "/invoiced";

    /**
     * 合同管理
     */
    public static final String CONTRACT_MANAGE = "/contractManage";

    /**
     * 系统管理
     */
    public static final String SYSTEM = "/system";

    /**
     * 账号管理
     */
    public static final String MANAGE_ACCOUNT = "/manageAccount";

    /**
     * 角色管理
     */
    public static final String MANAGE_ROLE = "/manageRole";

    /**
     * 系统公告
     */
    public static final String SYSTEM_NOTICE = "/systemNotice";

    /**
     * 消息提醒
     */
    public static final String NEWS = "/news";

    /**
     * 我的公司管理
     */
    public static final String MY_COMPANY = "/myCompany";
}
