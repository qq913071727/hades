package com.innovation.ic.sc.base.pojo.constant;

/**
 * 根据用户的真实姓名模糊查询。判断参数是那种数据类型。
 */
public class ParameterType {
    /**
     * 参数为字母
     */
    public static final String pinYin ="[a-zA-Z]+";
}
