package com.innovation.ic.sc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zengqinglong
 * @desc sc_erp 金额枚举
 * @Date 2022/12/21 16:08
 **/
public enum ScErpCurrencyEnum {
    RMB(1, "人民币"),
    DOLLAR(2, "美金");

    private int type;

    private String desc;

    ScErpCurrencyEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static ScErpCurrencyEnum parse(Integer type) {
        for (ScErpCurrencyEnum scErpCurrencyEnum : ScErpCurrencyEnum.values()) {
            if (scErpCurrencyEnum.getType() == type) {
                return scErpCurrencyEnum;
            }
        }
        return null;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (ScErpCurrencyEnum value : ScErpCurrencyEnum.values()) {
            map = new LinkedHashMap<String, Object>(4);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}
