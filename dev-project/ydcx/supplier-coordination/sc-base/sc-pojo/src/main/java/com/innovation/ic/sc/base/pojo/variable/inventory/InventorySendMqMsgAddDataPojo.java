package com.innovation.ic.sc.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @desc   新增库存发送到mq消息数据的Pojo类
 * @author linuo
 * @time   2022年9月22日13:33:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBatchQueryRespPojo", description = "库存批量查询返回的Pojo类")
public class InventorySendMqMsgAddDataPojo {
    @ApiModelProperty(value = "库存id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "起订量集合", dataType = "List")
    private List<Integer> mqList;

    @ApiModelProperty(value = "单价集合", dataType = "List")
    private List<BigDecimal> unitPriceList;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    private Integer packing;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "创建用户", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "创建用户id", dataType = "String")
    private String createUserId;

    @ApiModelProperty(value = "创建日期", dataType = "String")
    private String createDate;

    @ApiModelProperty(value = "更新日期", dataType = "String")
    private String modifyDate;

    @ApiModelProperty(value = "优势状态(0:无效、1:有效)", dataType = "Integer")
    private Integer advantageStatus;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    private Date validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    private Date validityDateEnd;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    private String enterpriseId;
}