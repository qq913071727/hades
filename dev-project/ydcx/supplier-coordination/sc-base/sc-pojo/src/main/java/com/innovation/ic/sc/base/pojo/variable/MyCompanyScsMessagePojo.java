package com.innovation.ic.sc.base.pojo.variable;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyScsMessagePojo", description = "发送给Scs的消息")
public class MyCompanyScsMessagePojo implements Serializable {


    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "关联公司ID", dataType = "String")
    private String relationEnterpriseId;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "类型 add 新增   delete删除", dataType = "String")
    private String type;


}
