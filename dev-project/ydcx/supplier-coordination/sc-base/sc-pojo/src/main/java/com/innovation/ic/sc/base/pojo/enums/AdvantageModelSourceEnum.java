package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   优势型号来源枚举类
 * @author linuo
 * @time   2022年9月2日13:37:01
 */
public enum AdvantageModelSourceEnum {
    USER_ADD(1, "用户添加"),
    ADVANTAGE_BRAND_ASSOCIATED(2, "优势品牌关联"),
    ERP_IMPORT(3, "erp导入");

    private Integer code;
    private String desc;

    AdvantageModelSourceEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AdvantageModelSourceEnum of(Integer code) {
        for (AdvantageModelSourceEnum c : AdvantageModelSourceEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (AdvantageModelSourceEnum c : AdvantageModelSourceEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}