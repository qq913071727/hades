package com.innovation.ic.sc.base.pojo.enums;

/**
 * @Description: 主键枚举
 * @ClassName: PrimaryKeysEnum
 * @Author: myq
 * @Date: 2022/8/10 上午9:18
 * @Version: 1.0
 */
public enum PrimaryKeysEnum {

    MUP("MUP","MapsUserProduct"),
    MUM("MUM","MapsUserMenu"),
    UP("UP","UploadFile"),
    I("I","Inventory"),
    LP("LP","LadderPrice"),
    DLP("DLP","DefaultLadderPrice"),
    UC("UC","UsersColumn"),
    USR("USR","Users"),
    DICT("DICT","dictionaries");

    private String key;
    private String desc;

    PrimaryKeysEnum(String key,String desc){
        this.key = key;
        this.desc = desc;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
