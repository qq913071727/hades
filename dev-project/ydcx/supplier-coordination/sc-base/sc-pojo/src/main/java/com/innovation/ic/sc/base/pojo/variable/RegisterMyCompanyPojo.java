package com.innovation.ic.sc.base.pojo.variable;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author B1B
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RegisterMyCompanyPojo", description = "发送注册公司消息")
public class RegisterMyCompanyPojo implements Serializable {

    private static final long serialVersionUID = 4228333579234098652L;


    @ApiModelProperty(value = "主账号id", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String Name;

    @ApiModelProperty(value = "国别地区", dataType = "String")
    private String District;

    @ApiModelProperty(value = "供应商类型", dataType = "String")
    private String SupplierType;




    @ApiModelProperty(value = "发票类型", dataType = "String")
    private Integer InvoiceType;

    @ApiModelProperty(value = "币种", dataType = "String")
    private String Currency;

    @ApiModelProperty(value = "收款公司,同公司名称", dataType = "String")
    private String Title;

    @ApiModelProperty(value = "开户行", dataType = "String")
    private String Bank;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String Account;


    @ApiModelProperty(value = "银行地址", dataType = "String")
    private String BankAddress;

    @ApiModelProperty(value = "SwiftCode", dataType = "String")
    private String SwiftCode;
    @ApiModelProperty(value = "中转银行", dataType = "String")
    private String Transfer;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;

    @ApiModelProperty(value = "图片", dataType = "String")
    private List<UploadFileMessagePojo> Files;




}
