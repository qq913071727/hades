package com.innovation.ic.sc.base.pojo.enums;

public enum MyCompanyEnterpriseEnum {

    ACCORD_WITH(1, "主公司"),
    NO_ACCORD_WITH(0, "非主公司");

    private Integer code;
    private String desc;

    MyCompanyEnterpriseEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
