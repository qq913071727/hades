package com.innovation.ic.sc.base.pojo.enums;

/**
 * 上传文件类型枚举
 */
public enum UploadFileTypeEnum {
    IMAGE(1, "图片"),
    ;

    private Integer code;
    private String desc;

    UploadFileTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}