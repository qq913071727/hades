package com.innovation.ic.sc.base.pojo.variable.inventory;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @time   未匹配成功数据
 * @author linuo
 * @time   2023年2月10日10:41:01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryUnMatchData", description = "未匹配成功数据")
public class InventoryUnMatchData {
    @ApiModelProperty(value = "库存id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "对应的标准库型号", dataType = "String")
    private String bzkPartNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "对应的标准库品牌", dataType = "String")
    private String bzkBrand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "单价", dataType = "BigDecimal")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String ladderPriceName;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    private Integer packing;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createDate;
}