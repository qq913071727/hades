package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   按单价配置的阶梯价格查询返回的Pojo类
 * @author linuo
 * @time   2023年4月14日14:27:54
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceInfoByAmountRespPojo", description = "按单价配置的阶梯价格查询返回的Pojo类")
public class LadderPriceInfoByAmountRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "按金额配置的数据", dataType = "List")
    private List<LadderPriceAmountConfigRespPojo> ladderPriceAmountConfigData;
}