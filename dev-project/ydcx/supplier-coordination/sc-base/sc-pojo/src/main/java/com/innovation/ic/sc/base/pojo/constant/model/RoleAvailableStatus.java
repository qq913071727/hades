package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * 角色是否可用
 */
public class RoleAvailableStatus {

    /**
     * 可用
     */
    public static final Integer YES = 1;

    /**
     * 停用
     */
    public static final Integer NO = 2;
}
