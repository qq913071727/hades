package com.innovation.ic.sc.base.pojo.variable.inventory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.LadderPriceConfigPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.util.List;

/**
 * @desc   库存批量查询返回的Pojo类
 * @author linuo
 * @time   2022年9月20日13:16:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBatchQueryRespPojo", description = "库存批量查询返回的Pojo类")
public class InventoryBatchQueryRespPojo {
    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String ladderPriceName;

    @ApiModelProperty(value = "起订量集合", dataType = "List")
    private List<Integer> mqList;

    @ApiModelProperty(value = "单价集合", dataType = "List")
    private List<String> unitPriceList;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    private Integer packing;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "更新日期", dataType = "Date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date modifyDate;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createDate;

    @ApiModelProperty(value = "库存id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "起订量", dataType = "Integer")
    private Integer mq;

    @ApiModelProperty(value = "单价", dataType = "BigDecimal")
    private String unitPrice;

    @ApiModelProperty(value = "阶梯价格配置", dataType = "List")
    private List<LadderPriceConfigPojo> unitPriceConfig;

    @ApiModelProperty(value = "当前品牌是否为优势品牌(0否、1是)", dataType = "Integer")
    private Integer ifAdvantageBrand;

    @ApiModelProperty(value = "当前型号是否为优势型号(0否、1是)", dataType = "Integer")
    private Integer ifAdvantageModel;
}