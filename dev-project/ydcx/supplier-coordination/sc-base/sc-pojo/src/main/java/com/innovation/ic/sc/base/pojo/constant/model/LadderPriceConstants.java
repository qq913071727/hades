package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @desc   阶梯价格相关常量参数
 * @author linuo
 * @time   2022年8月23日11:21:03
 */
public class LadderPriceConstants {
    /** 阶梯价格id字段 */
    public static String LADDER_PRICE_ID_FIELD = "ladder_price_id";

    /** 阶梯价格配置 */
    public static String LADDER_PRICE_CONFIG = "ladderPriceConfig";

    /** 最小金额字段 */
    public static String MIN_PRICE_FIELD = "min_price";

    /** 关联id字段 */
    public static String CORRELATION_ID_FIELD = "correlation_id";

    /** 起订量字段 */
    public static String MQ_FIELD = "mq";

    /** 降比字段 */
    public static String LOWER_FIELD = "lower";

    /** 单价 */
    public static String UNIT_PRICE = "unitPrice";

    /** 最低价格 */
    public static String MIN_PRICE = "minPrice";

    /** 最高价格 */
    public static String MAX_PRICE = "maxPrice";

    /** 配置类型字段 */
    public static String CONFIG_TYPE_FIELD = "config_type";

    /** 币种字段 */
    public static String CURRENCY_FIELD = "currency";
}