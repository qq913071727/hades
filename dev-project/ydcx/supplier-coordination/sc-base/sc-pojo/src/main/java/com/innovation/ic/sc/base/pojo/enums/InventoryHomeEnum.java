package com.innovation.ic.sc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   库存所在地枚举
 * @author linuo
 * @time   2022年12月28日15:24:51
 **/
public enum InventoryHomeEnum {
    MAINLAND(1, "中国大陆"),
    HONGKONG(2, "中国香港");

    private int type;

    private String desc;

    InventoryHomeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static InventoryHomeEnum parse(Integer type) {
        for (InventoryHomeEnum scErpCurrencyEnum : InventoryHomeEnum.values()) {
            if (scErpCurrencyEnum.getType() == type) {
                return scErpCurrencyEnum;
            }
        }
        return null;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InventoryHomeEnum value : InventoryHomeEnum.values()) {
            map = new LinkedHashMap<String, Object>(4);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}