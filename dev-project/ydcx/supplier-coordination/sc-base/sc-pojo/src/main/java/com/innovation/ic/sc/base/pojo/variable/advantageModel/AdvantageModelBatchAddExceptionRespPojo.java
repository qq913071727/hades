package com.innovation.ic.sc.base.pojo.variable.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   优势型号信息批量导入异常数据的返回Pojo类
 * @author linuo
 * @time   2023年2月3日16:07:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelBatchAddExceptionRespPojo", description = "优势型号信息批量导入异常数据的返回Pojo类")
public class AdvantageModelBatchAddExceptionRespPojo {
    @ApiModelProperty(value = "待审核数据数量", dataType = "Integer")
    private Integer reviewedDataCount;

    @ApiModelProperty(value = "待审核数据", dataType = "List")
    private List<AdvantageModelBatchAddRespPojo> reviewedData;

    @ApiModelProperty(value = "已审核通过的数据数量", dataType = "Integer")
    private Integer approvedDataCount;

    @ApiModelProperty(value = "已审核通过的数据", dataType = "List")
    private List<AdvantageModelBatchAddRespPojo> approvedData;

    @ApiModelProperty(value = "库存不存在的数据数量", dataType = "Integer")
    private Integer inventoryNoHaveDataCount;

    @ApiModelProperty(value = "库存不存在的数据", dataType = "List")
    private List<AdvantageModelBatchAddRespPojo> inventoryNoHaveData;
}
