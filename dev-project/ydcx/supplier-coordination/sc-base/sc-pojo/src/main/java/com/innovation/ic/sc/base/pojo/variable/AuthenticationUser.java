package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.List;

/**
 * 接收用户信息vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AuthenticationUser", description = "接收用户信息vo类")
public class AuthenticationUser implements Serializable {
    private static final long serialVersionUID = 4520116228065742486L;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String username;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "职位：1表示经理，2表示员工", dataType = "Integer")
    private String position;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;

    @ApiModelProperty(value = "状态", dataType = "String")
    private String status;

    @ApiModelProperty(value = "创建日期", dataType = "String")
    private String createDate;

    @ApiModelProperty(value = "公司id", dataType = "String")
    private String epId;

    @ApiModelProperty(value = "主账号下的账号", dataType = "String")
    private String epName;

    @ApiModelProperty(value = "主账号的id", dataType = "String")
    private String fatherId;

    @ApiModelProperty(value = "token过期时间", dataType = "String")
    private String expiration;

    @ApiModelProperty(value = "1表示主账号，0表示子账号", dataType = "Integer")
    private Integer isMain;

    @ApiModelProperty(value = "认证token", dataType = "String")
    private String token;

    @ApiModelProperty(value = "我的公司名称列表", dataType = "List")
    private List<String> myCompanyNameList;

    @ApiModelProperty(value = "评分", dataType = "Float")
    private Float score;
}