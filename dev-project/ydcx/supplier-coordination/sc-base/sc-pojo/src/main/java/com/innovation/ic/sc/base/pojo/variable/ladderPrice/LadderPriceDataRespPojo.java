package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   阶梯价格数据查询返回的Pojo类
 * @author linuo
 * @time   2023年1月10日17:41:56
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceDataRespPojo", description = "阶梯价格数据查询返回的Pojo类")
public class LadderPriceDataRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "阶梯价格配置", dataType = "List")
    private List<LadderPriceDataPojo> ladderPriceConfigData;
}