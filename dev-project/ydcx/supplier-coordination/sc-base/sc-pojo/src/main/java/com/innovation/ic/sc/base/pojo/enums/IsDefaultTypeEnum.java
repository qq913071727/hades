package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   是否默认枚举
 * @author linuo
 * @time   2022年8月23日11:24:50
 */
public enum IsDefaultTypeEnum {
    NO(0, "否"),
    YES(1, "是");

    private Integer code;
    private String desc;

    IsDefaultTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static IsDefaultTypeEnum of(Integer code) {
        for (IsDefaultTypeEnum c : IsDefaultTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (IsDefaultTypeEnum c : IsDefaultTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}