package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   全部阶梯价格配置查询返回的Pojo类
 * @author linuo
 * @time   2023年4月23日15:40:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceInfoAllRespPojo", description = "全部阶梯价格配置查询返回的Pojo类")
public class LadderPriceInfoAllRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "配置方式(1:按起订量配置、2:按单价配置)", dataType = "Integer")
    private Integer configType;

    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "全部阶梯价格配置数据", dataType = "List")
    private List<JSONObject> ladderPriceConfigData;
}