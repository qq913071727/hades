package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 推送mq 批量修改状态封装类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UsersIdsPojo", description = "推送mq 批量修改状态封装类")
public class UsersIdsPojo {

    private List<String> ids;//主键

    private Integer status;//状态

    private Integer webSite;//站点 1 供应商协同系统、2 客户协同系统、3 芯达通系统

    private Integer type;// 1 Seller 卖家(供应商)，2 Buyer 买家(客户)

}
