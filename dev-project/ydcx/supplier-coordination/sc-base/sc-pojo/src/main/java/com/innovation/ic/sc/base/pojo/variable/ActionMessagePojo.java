package com.innovation.ic.sc.base.pojo.variable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ActionMessagePojo {

    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "内容", dataType = "String")
    private String content;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String createTime;

    @ApiModelProperty(value = "是否已读", dataType = "Integer")
    private Integer isRead;
}
