package com.innovation.ic.sc.base.pojo.enums;

/**
 * 我的公司下拉枚举项
 */
public enum MyCompanyDictionariesEnum {
    ENTERPRISE_NATURE("enterprise_nature","企业性质"),
    SUPPLIER_TYPE("supplier_type","供应商类型"),
    CURRENCY("currency","币种"),
    COUNTRY_AND_REGION("country_and_region","国别地区"),
    INVOICE_TYPE("invoice_type","发票类型"),
    COUNTRY_OF_ORIGIN("country_of_origin","原产地"),
    ;


    private String name;
    private String cName;


    public static String of(String cName) {
        for (MyCompanyDictionariesEnum c : MyCompanyDictionariesEnum.values()) {
            if (c.getcName().equals(cName)) {
                return c.getName();
            }
        }
        return null;
    }



    MyCompanyDictionariesEnum(String name,String cName) {
        this.name = name;
        this.cName = cName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }



}