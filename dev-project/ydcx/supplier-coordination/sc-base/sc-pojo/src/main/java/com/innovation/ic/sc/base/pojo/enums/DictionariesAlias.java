package com.innovation.ic.sc.base.pojo.enums;

/**
 * 我的公司别名转换
 */
public enum DictionariesAlias {

    ORIGINAL_FACTORY("生产商", "原厂"),
    AGENT("授权代理商", "代理商"),
    DISTRIBUTOR("分销商", "分销商");

    private String source;
    private String target;

    DictionariesAlias(String source, String target) {
        this.source = source;
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public static DictionariesAlias of(String source) {
        for (DictionariesAlias d : DictionariesAlias.values()) {
            if (d.getSource().equals(source)) {
                return d;
            }
        }
        return null;
    }

}
