package com.innovation.ic.sc.base.pojo.enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   标准库库存数据匹配状态
 * @author linuo
 * @time   2023年2月9日15:05:40
 **/
public enum InventoryMatchStatusEnum {
    UNMATCHED(0, "未匹配"),
    MATCHED(1, "已匹配"),
    WAITING(2, "采购已匹配待用户确认");

    private int type;

    private String desc;

    InventoryMatchStatusEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static InventoryMatchStatusEnum parse(Integer type) {
        for (InventoryMatchStatusEnum scErpCurrencyEnum : InventoryMatchStatusEnum.values()) {
            if (scErpCurrencyEnum.getType() == type) {
                return scErpCurrencyEnum;
            }
        }
        return null;
    }

    public static List<Map> dictionary() {
        List list = new ArrayList();
        LinkedHashMap<String, Object> map = null;
        for (InventoryMatchStatusEnum value : InventoryMatchStatusEnum.values()) {
            map = new LinkedHashMap<String, Object>(4);
            map.put("desc", value.getDesc());
            map.put("type", value.getType());
            list.add(map);
        }
        return list;
    }
}