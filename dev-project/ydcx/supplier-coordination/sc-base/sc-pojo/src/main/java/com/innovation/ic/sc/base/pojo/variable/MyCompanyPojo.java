package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 我的公司详情Pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyPojo", description = "我的公司")
public class MyCompanyPojo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String name;


    @ApiModelProperty(value = "国别地区", dataType = "String")
    private String countryRegion;


    @ApiModelProperty(value = "国别地区名字", dataType = "String")
    private String countryRegionName;

    @ApiModelProperty(value = "公司类型id", dataType = "String")
    private String companyType;

    @ApiModelProperty(value = "公司类型名字", dataType = "String")
    private String companyTypeName;





    @ApiModelProperty(value = "发票类型id", dataType = "String")
    private String invoicType;

    @ApiModelProperty(value = "文件对象多个", dataType = "UploadFilePojo")
    List<UploadFilePojo> files;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String currency;


    @ApiModelProperty(value = "收款公司", dataType = "String")
    private String receivingCompany;

    @ApiModelProperty(value = "开户银行", dataType = "String")
    private String bankOfDeposit;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String accountNumber;

    @ApiModelProperty(value = "银行地址", dataType = "String")
    private String bankAddress;


    @ApiModelProperty(value = "swiftCode", dataType = "String")
    private String swiftCode;

    @ApiModelProperty(value = "中转银行", dataType = "String")
    private String transitBank;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String currencyId;

    @ApiModelProperty(value = "币种名字:  人民币   美金 ", dataType = "String")
    private String currencyName;


    @ApiModelProperty(value = "关联主键id", dataType = "String")
    private String relationEnterpriseId;

    @ApiModelProperty(value = "状态: 100 待审核  200 审核通过  300 审核拒绝", dataType = "Integer")
    private Integer state;

    @ApiModelProperty(value = "是否是主公司 1是  2否", dataType = "Integer")
    private Integer mainCompany;


    @ApiModelProperty(value = "发票类型名字", dataType = "String")
    private String invoicTypeName;


    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "原因", dataType = "String")
    private String cause;


    @ApiModelProperty(value = "1 为erp推送数据", dataType = "Integer")
    private Integer erpReceive;

    @ApiModelProperty(value = "我的公司收款信息List详情", dataType = "List")
    private List<MyCompanyCollectionInformationPojo>  myCompanyCollectionInformationPojo;

}