package com.innovation.ic.sc.base.pojo.variable.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   批量判断是否为优势型号的返回Vo类
 * @author linuo
 * @time   2022年9月28日13:27:16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelBatchJudgeModelStatusRespPojo", description = "批量判断是否为优势型号的返回Vo类")
public class AdvantageModelBatchJudgeModelStatusRespPojo {
    @ApiModelProperty(value = "结果集合", dataType = "List")
    private List<AdvantageModelJudgeModelStatusRespPojo> resultList;
}