package com.innovation.ic.sc.base.pojo.variable;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author swq
 * @desc 部门管理
 * @time 2023年3月10日11:49:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DepartmentPojo", description = "部门管理封装redis")
public class DepartmentPojo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "部门名称", dataType = "String")
    private String departmentName;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "主账号的id，如果为空，则表示这是主账号", dataType = "String")
    private String fatherId;

}
