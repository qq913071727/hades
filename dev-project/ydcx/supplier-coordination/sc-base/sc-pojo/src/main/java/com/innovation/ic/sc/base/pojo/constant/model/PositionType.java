package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * 职位类型
 */
public class PositionType {

    /**
     * 经理
     */
    public static final Integer MANAGER = 1;

    /**
     * 员工
     */
    public static final Integer STAFF = 2;
}
