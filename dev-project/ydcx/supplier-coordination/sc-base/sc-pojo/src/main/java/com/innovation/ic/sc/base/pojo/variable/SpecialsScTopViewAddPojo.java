package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   优势型号新增/修改/延期的pojo类
 * @author linuo
 * @time   2022年9月1日14:13:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SpecialsScTopViewAddPojo", description = "优势型号新增/修改/延期的pojo类")
public class SpecialsScTopViewAddPojo {
    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "特色类型(1代理、2生产、3分销)", dataType = "Integer")
    private Integer Type;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String PartNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String Brand;

    @ApiModelProperty(value = "有效期开始", dataType = "Date")
    private Date StartDate;

    @ApiModelProperty(value = "有效期截至", dataType = "Date")
    private Date EndDate;

    @ApiModelProperty(value = "创建姓名", dataType = "String")
    private String Creator;

    @ApiModelProperty(value = "创建用户", dataType = "String")
    private String CreatorName;

    @ApiModelProperty(value = "附件", dataType = "String")
    private String Files;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String Summary;
}