package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   关联阶梯价格信息表的类型枚举类
 * @author linuo
 * @time   2023年4月14日09:37:55
 */
public enum LadderPriceCorrelationTypeEnum {
    CORRELATION_BY_MOQ(1, "按起订量配置关联阶梯价格信息表"),
    CORRELATION_BY_AMOUNT(2, "按单价配置关联阶梯价格单价配置表");

    private int code;
    private String desc;

    LadderPriceCorrelationTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}