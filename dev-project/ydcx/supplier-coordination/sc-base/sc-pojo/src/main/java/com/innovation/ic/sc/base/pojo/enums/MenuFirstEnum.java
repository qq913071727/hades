package com.innovation.ic.sc.base.pojo.enums;


import com.innovation.ic.sc.base.pojo.constant.model.MenuIconConstants;
import com.innovation.ic.sc.base.pojo.constant.model.MenuRouteConstants;
import com.innovation.ic.sc.base.pojo.constant.model.MenuType;

/**
 * @author zengqinglong
 * @desc 第一个菜单 枚举
 * @Date 2023/2/9 13:42
 **/
public enum MenuFirstEnum {
    // 首页 1
    HOME_PAGE("1", "首页", MenuType.MENU_ITEM, MenuRouteConstants.HOME_PAGE, MenuIconConstants.DASHBOARD),
    //订单管理 2
    ORDER_MANAGEMENT("2", "订单管理", MenuType.MENU_ITEM, MenuRouteConstants.ORDER_STOCK, MenuIconConstants.CHART),
    //报价管理 3
    QUOTATION_MANAGEMENT("3", "报价管理", MenuType.MENU_ITEM, MenuRouteConstants.MANAGE_OFFER, MenuIconConstants.POST),
    //库存管理 4
    INVENTORY_MANAGEMENT("4", "库存管理", MenuType.MENU_ITEM, MenuRouteConstants.STOCK, MenuIconConstants.DICT),
    //财务管理 5
    FINANCIAL_MANAGEMENT("5", "财务管理", MenuType.MENU_ITEM, MenuRouteConstants.FINANCE, MenuIconConstants.MONEY),
    //系统管理 6
    SYSTEM_MANAGEMENT("6", "系统管理", MenuType.MENU_ITEM, MenuRouteConstants.SYSTEM, MenuIconConstants.BUILD);

    //id
    private String id;
    //名称
    private String name;
    //菜单标志
    private Integer menuType;
    //路由
    private String path;
    //图标
    private String icon;

    MenuFirstEnum(String id, String name, Integer menuType, String path, String icon) {
        this.id = id;
        this.name = name;
        this.menuType = menuType;
        this.path = path;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }

}
