package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.List;

/**
 * @desc   按单价配置的数据Pojo类
 * @author linuo
 * @time   2023年4月14日13:25:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceAmountConfigRespPojo", description = "按单价配置的数据Pojo类")
public class LadderPriceAmountConfigRespPojo {
    @ApiModelProperty(value = "最小金额", dataType = "BigDecimal")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最大金额", dataType = "BigDecimal")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "阶梯价格配置", dataType = "List")
    private List<LadderPriceConfigRespPojo> ladderPriceConfig;
}