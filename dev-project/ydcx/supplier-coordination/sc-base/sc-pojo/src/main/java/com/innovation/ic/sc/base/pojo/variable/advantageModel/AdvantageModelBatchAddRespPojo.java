package com.innovation.ic.sc.base.pojo.variable.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   优势型号信息批量导入的返回Pojo类
 * @author linuo
 * @time   2022年12月14日10:00:03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelBatchAddRespPojo", description = "优势型号信息批量导入的返回Pojo类")
public class AdvantageModelBatchAddRespPojo {
    @ApiModelProperty(value = "表格名称", dataType = "String")
    private String tableName;

    @ApiModelProperty(value = "优势型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;
}
