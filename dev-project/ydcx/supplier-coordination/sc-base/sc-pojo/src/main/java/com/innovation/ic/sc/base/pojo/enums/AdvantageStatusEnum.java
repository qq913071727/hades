package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   优势状态枚举类
 * @author linuo
 * @time   2022年8月30日13:18:31
 */
public enum AdvantageStatusEnum {
    // 有效
    EFFECTIVE(200, 1),

    // 无效
    INVALID(300, 0),

    // 无效(待审批)
    PENDING(100, 0),
    ;

    private Integer erpStatusCode;
    private Integer scStatusCode;

    AdvantageStatusEnum(Integer erpStatusCode, Integer scStatusCode) {
        this.erpStatusCode = erpStatusCode;
        this.scStatusCode = scStatusCode;
    }

    public static Integer getErpStatusCode(Integer scStatusCode) {
        for (AdvantageStatusEnum c : AdvantageStatusEnum.values()) {
            if (c.getScStatusCode().equals(scStatusCode)) {
                return c.erpStatusCode;
            }
        }
        return null;
    }

    public static Integer getScStatusCode(Integer erpStatusCode) {
        for (AdvantageStatusEnum c : AdvantageStatusEnum.values()) {
            if (c.getErpStatusCode().equals(erpStatusCode)) {
                return c.scStatusCode;
            }
        }
        return null;
    }

    public Integer getErpStatusCode() {
        return erpStatusCode;
    }

    public void setErpStatusCode(Integer erpStatusCode) {
        this.erpStatusCode = erpStatusCode;
    }

    public Integer getScStatusCode() {
        return scStatusCode;
    }

    public void setScStatusCode(Integer scStatusCode) {
        this.scStatusCode = scStatusCode;
    }
}