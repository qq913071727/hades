package com.innovation.ic.sc.base.pojo.enums;

/**
 * 上传文件枚举,用来区分前缀,因为数据库id为自增
 */
public enum UploadFileEnum {
    MY_COMPANY("my_company", "我的公司模块"),
    ADVANTAGE_BRAND("advantage_brand", "优势品牌"),
    ;

    private String code;
    private String desc;

    UploadFileEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}