package com.innovation.ic.sc.base.pojo.constant.handler;

/**
 * redis存储时使用的常量
 */
public class RedisStorage {
    /**
     * client表
     */
    public static final String CLIENT = "CLIENT";

    /**
     * 存储在redis中的token的前缀
     */
    public static final String TOKEN_PREFIX = "TOKEN:";

    /**
     * 存储在redis中的表的前缀
     */
    public static final String TABLE = "TABLE:";

    /**
     * 存储在redis中的action_message表的前缀
     */
    public static final String ACTION_MESSAGE = "ACTION_MESSAGE:";

    /**
     * 存储在redis中的user表的前缀
     */
    public static final String USER = "USER:";

    /**
     * 存储在redis中的brand表的前缀
     */
    public static final String BRAND = "BRAND:";

    /**
     * 存储在redis中，user表按照create_date字段降序排列
     */
    public static final String CREATE_DATE_DESC = "CREATE_DATE_DESC";

    /**
     * 存储在redis中，user表按照create_date字段升序排列
     */
    public static final String CREATE_DATE_ASC = "CREATE_DATE_ASC";

    /**
     * 存储在redis中的冒号
     */
    public static final String COLON = ":";

    /**
     * 在redis中模糊查询时的*
     */
    public static final String ASTERISK = "*";

    /**
     * 数据库中没有对应的数据，但redis中仍然需要有个占位符
     */
    public static final String EMPTY_VALUE = "empty_value";
}
