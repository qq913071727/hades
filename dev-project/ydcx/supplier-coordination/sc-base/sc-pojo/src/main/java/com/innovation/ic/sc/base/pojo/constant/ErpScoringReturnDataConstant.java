package com.innovation.ic.sc.base.pojo.constant;

/**
 * @desc   erp评分相关返回数据常量类
 * @author linuo
 * @time   2022年12月1日16:56:50
 */
public class ErpScoringReturnDataConstant {
    /** 询价数量 */
    public static final String INQUIRY_COUNT = "inquiryCount";

    /** 报价数量 */
    public static final String QUOTE_COUNT = "quoteCount";

    /** 未报价数量 */
    public static final String UN_QUOTE_COUNT = "unQuoteCount";

    /** 20% 时间内报价任务总数 */
    public static final String QUOTE_COUNT1 = "quoteCount1";

    /** 20% - 100% 时间内报价任务总数 */
    public static final String QUOTE_COUNT2 = "quoteCount2";

    /** 采纳数量 */
    public static final String ACCEPT_COUNT = "acceptCount";
}
