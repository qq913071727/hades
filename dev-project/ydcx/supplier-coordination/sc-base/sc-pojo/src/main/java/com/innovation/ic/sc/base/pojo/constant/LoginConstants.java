package com.innovation.ic.sc.base.pojo.constant;

/**
 * @desc   登录功能使用的常量值
 * @author linuo
 * @time   2022年9月21日15:22:01
 */
public class LoginConstants {
    /** 消息 */
    public static final String MESSAGE = "message";

    /** 消息 */
    public static final String MSG = "msg";

    /** token */
    public static final String TOKEN = "token";

    /** 数据 */
    public static final String DATA = "data";

    /** 结果 */
    public static final String RESULT = "result";

    /** 状态 */
    public static final String STATUS_FIELD = "status";

    /** 短信验证码 */
    public static final String CODE_FIELD = "code";

    /** json   */
    public static final String JSON = "json";

    /** GBK编码   */
    public static final String CHARSET_GBK = "GBK";

    /** RSA2   */
    public static final String RSA2 = "RSA2";

    /** authorization_code */
    public static final String AUTHORIZATION_CODE = "authorization_code";

    /** user_id */
    public static final String USER_ID = "user_id";

    /** unionid字段 */
    public static final String UNION_ID = "unionId";

    /** openId字段 */
    public static final String OPEN_ID = "openId";

    /** 类型 */
    public static final String TYPE = "type";

    /** 成功状态字段 */
    public static final String SUCCESS_FIELD = "success";

    /** 状态 */
    public static final String STATUS = "status";

    /** 用户id字段 */
    public static final String USER_ID_FIELD = "userID";

    /** 状态码   */
    public static final String CODE = "code";

    /** 等号 */
    public static final String EQUAL_SIGN = "=";

    /** 账号密码登录方式**/
    public static final Integer ACCOUNT_PASSWORD_LOGIN = 1;

    /** 手机短信登录方式**/
    public static final Integer PHONE_LOGIN = 2;

    /** 支付宝登录方式**/
    public static final Integer AlIPAY_LOGIN = 5;

    /** 微信登录方式**/
    public static final Integer WECHAT_LOGIN = 3;

    /** qq登录方式**/
    public static final Integer QQ_LOGIN = 3;

}