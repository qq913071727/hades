package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * 菜单类型
 */
public class MenuType {

    /**
     * 菜单
     */
    public static final Integer MENU_ITEM = 1;

    /**
     * 按钮
     */
    public static final Integer BUTTON = 2;

}
