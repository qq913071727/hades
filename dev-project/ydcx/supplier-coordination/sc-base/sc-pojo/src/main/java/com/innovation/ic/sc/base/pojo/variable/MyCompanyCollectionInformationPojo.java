package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 我的公司收款信息Pojo,前端要求分开修改为 list数组
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyCollectionInformationPojo", description = "我的公司收款信息")
public class MyCompanyCollectionInformationPojo {


    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String currency;


    @ApiModelProperty(value = "收款公司", dataType = "String")
    private String receivingCompany;

    @ApiModelProperty(value = "开户银行", dataType = "String")
    private String bankOfDeposit;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String accountNumber;

    @ApiModelProperty(value = "银行地址", dataType = "String")
    private String bankAddress;

    @ApiModelProperty(value = "swiftCode", dataType = "String")
    private String swiftCode;

    @ApiModelProperty(value = "中转银行", dataType = "String")
    private String transitBank;


}