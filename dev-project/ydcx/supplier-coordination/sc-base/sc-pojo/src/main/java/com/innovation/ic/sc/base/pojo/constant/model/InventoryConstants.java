package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @desc   库存功能使用的常量值
 * @author linuo
 * @time   2022年9月22日15:10:19
 */
public class InventoryConstants {
    /** 标准库数据匹配状态(0：未匹配、1：已匹配) */
    public static final String BZK_MATCH_STATUS = "bzk_match_status";

    /** 库存id */
    public static final String ID_FIELD = "id";

    /** 标准匹配型号 */
    public static final String BZK_PART_NUMBER = "bzkPartNumber";

    /** 标准匹配品牌 */
    public static final String BZK_BRAND = "bzkBrand";

    /** 操作类型(0:保存；1:确认) */
    public static final String TYPE_BRAND = "type";

    /** 操作类型字段 */
    public static String OPERATE_TYPE_FIELD = "operateType";

    /** 状态字段 */
    public static String STATUS_FIELD = "status";

    /** 品牌字段 */
    public static String BRAND_FIELD = "brand";

    /** 数量字段 */
    public static String COUNT_FIELD = "count";

    /** 型号 */
    public static String PART_NUMBER = "partNumber";

    /** 型号字段 */
    public static String PART_NUMBER_FIELD = "part_number";

    /** 开始时间 */
    public static String START_DATE = "startDate";

    /** 结束时间 */
    public static String END_DATE = "endDate";

    /** 库存所在地 */
    public static String INVENTORY_HOME = "inventoryHome";

    /** 价格类型 */
    public static String UNIT_PRICE_TYPE = "unitPriceType";

    /** 阶梯价格id */
    public static String LADDER_PRICE_ID = "ladderPriceId";

    /** 供应商id */
    public static String ENTERPRISE_ID = "enterpriseId";

    /** 封装字段 */
    public static String PACKAGE_FIELD = "package";

    /** 批次字段 */
    public static String BATCH_FIELD = "batch";

    /** 包装字段 */
    public static String PACKING_FIELD = "packing";

    /** 最小起订量字段 */
    public static String MOQ_FIELD = "moq";

    /** 最小包装量字段 */
    public static String MPQ_FIELD = "mpq";

    /** 币种字段 */
    public static String CURRENCY_FIELD = "currency";

    /** 库存所在地字段 */
    public static String INVENTORY_HOME_FIELD = "inventoryHome";

    /** 价格类型字段 */
    public static String UNIT_PRICE_TYPE_FIELD = "unitPriceType";

    /** 单价/最低阶梯价字段 */
    public static String UNIT_PRICE_FIELD = "unitPrice";

    /** 阶梯价格id字段 */
    public static String LADDER_PRICE_ID_FIELD = "ladderPriceId";

    /** 匹配类型 */
    public static String MATCH_TYPE = "matchType";

    /** 标准厂家 */
    public static String MANUFACTURER = "标准厂家";

    /** 标准型号 */
    public static String MODEL_NUMBER = "标准型号";

    /** 匹配类型 */
    public static String MATCH_TYPE_CN = "匹配类型";

    /** 完全匹配 */
    public static String ALL_MATCH = "完全匹配";

    /** 推荐匹配 */
    public static String RECOMMEND_MATCH = "推荐匹配";

    /** 审核状态 */
    public static String AUDIT_STATUS = "auditStatus";

    /** 优势状态 */
    public static String ADVANTAGE_STATUS = "advantageStatus";
}