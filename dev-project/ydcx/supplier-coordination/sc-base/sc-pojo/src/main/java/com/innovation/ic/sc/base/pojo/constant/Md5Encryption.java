package com.innovation.ic.sc.base.pojo.constant;

public class Md5Encryption {

    /**
     * MD5第一次加盐
     */
    public static final String MD5_UPPERCASE = "X";

    /**
     * MD5拼接字符串
     */
    public static final String MD5_DATA = "^yj201￥5PK_&(*)%dc——）M::aa\"as\"s\\t\\a\\\\\\r\\n\"NQSC&$%";

    /**
     * MD5第二次加盐
     */
    public static final String MD5_LOWERCASE = "x";
}
