package com.innovation.ic.sc.base.pojo.constant.config;

public class DatabaseGlobal {

    /**
     * erp9系统的PveSiteUser数据库
     */
    public static final String ERP9_PVESITEUSER = "erp9-pvesiteuser";

    /**
     * sc系统的sc数据库
     */
    public static final String SC = "sc";

    /**
     * sc系统的sc-test数据库
     */
    public static final String SC_TEST = "sc-test";

    /**
     * sc系统的sc-prod数据库
     */
    public static final String SC_PROD = "sc-prod";

    /**
     * erp9系统的PveStandard数据库
     */
    public static final String ERP9_PVESTANDARD = "erp9-pvestandard";


    /**
     * erp9系统的PveBcs数据库
     */
    public static final String ERP9_PVEBCS = "erp9-pve-bcs";


    /**
     * erp9系统的PveCrm数据库
     */
    public static final String ERP9_PVECRM = "erp9-pve-crm";

    /**
     * erp对账单数据
     */
    public static final String SC_ERP = "sc-erp";
}
