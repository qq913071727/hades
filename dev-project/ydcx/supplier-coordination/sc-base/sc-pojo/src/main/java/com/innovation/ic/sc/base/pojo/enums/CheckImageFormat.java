package com.innovation.ic.sc.base.pojo.enums;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * 校验图片格式
 */

public enum CheckImageFormat {

    JPG("jpg"),

    PNG("png"),

    GIF("gif"),

    PDF("pdf"),


    JPG_CAPITAL("JPG"),

    PNG_CAPITAL("PNG"),

    GIF_CAPITAL("GIF"),

    PDF_CAPITAL("PDF");

    private static List<String> imageList = new ArrayList<String>(8);

    static {
        CheckImageFormat[] values = CheckImageFormat.values();
        for (CheckImageFormat value : values) {
            imageList.add(value.key);
        }
    }

    private String key;

    CheckImageFormat(String key) {
        this.key = key;
    }

    /**
     * 失败为false
     *
     * @param suffix
     * @return
     */
    public static boolean checkImageSuffix(String suffix) {
        if (StringUtils.isEmpty(suffix)) {
            return false;
        }
        return imageList.contains(suffix);
    }

}
