package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   阶梯价格配置的Pojo类
 * @author linuo
 * @time   2022年12月22日10:51:51
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceConfigPojo", description = "阶梯价格配置的Pojo类")
public class LadderPriceConfigPojo {
    @ApiModelProperty(value = "起订量", dataType = "Integer")
    private Integer mq;

    @ApiModelProperty(value = "单价", dataType = "BigDecimal")
    private String unitPrice;
}