package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @desc   优势型号审批的pojo类
 * @author linuo
 * @time   2022年9月1日17:16:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SpecialsScTopViewAuditPojo", description = "优势型号审批的pojo类")
public class SpecialsScTopViewAuditPojo {

    @ApiModelProperty(value = "优势ID", dataType = "String")
    private String ID;


    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String PartNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String Brand;

    @ApiModelProperty(value = "特色类型 1代理2生产 3分销", dataType = "Integer")
    private Integer Type;


    @ApiModelProperty(value = "有效期开始", dataType = "Date")
    private Date StartDate;

    @ApiModelProperty(value = "有效期结束", dataType = "Date")
    private Date EndDate;


    @ApiModelProperty(value = "文件集合", dataType = "List")
    private List<UploadFileMessagePojo> fileVoList;

    @ApiModelProperty(value = "特色状态(100 待审批、200 审批通过、传200则代表不需要审批)", dataType = "Integer")
    private Integer Status;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String Summary;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;

    @ApiModelProperty(value = "有效期结束", dataType = "String")
    private String endDateString;

    @ApiModelProperty(value = "有效期结束", dataType = "String")
    private String startDateString;


}