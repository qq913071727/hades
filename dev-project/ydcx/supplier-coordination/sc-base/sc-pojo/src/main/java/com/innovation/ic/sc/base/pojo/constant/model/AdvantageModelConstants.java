package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * @desc   优势型号功能使用的常量值
 * @author linuo
 * @time   2022年9月21日15:13:01
 */
public class AdvantageModelConstants {
    /** 来源字段 */
    public static final String SOURCE_FIELD = "source";

    /** 状态 */
    public static final String STATUS = "Status";

    /** 备注 */
    public static final String SUMMARY = "Summary";

    /** 型号字段 */
    public static String PART_NUMBER_FIELD = "part_number";

    /** 型号 */
    public static final String PART_NUMBER = "partNumber";

    /** 品牌字段 */
    public static String BRAND_FIELD = "brand";

    /** 创建用户 */
    public static String CREATE_USER = "createUser";

    /** 审核状态字段 */
    public static String AUDIT_STATUS_FIELD = "audit_status";

    /** 审核状态 */
    public static String AUDIT_STATUS = "auditStatus";

    /** 优势状态字段 */
    public static String ADVANTAGE_STATUS_FIELD = "advantage_status";

    /** 优势状态 */
    public static String ADVANTAGE_STATUS = "advantageStatus";

    /** 状态 */
    public static final String STATUS_UPPER_FIELD = "Status";

    /** 数据占用状态码 */
    public static final Integer DATA_OCCUPY_STATUS = 1001;
}