package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 上传文件Pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UploadFilePojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "文件名", dataType = "String")
    private String fileName;

    @ApiModelProperty(value = "文件后缀", dataType = "String")
    private String suffix;

    @ApiModelProperty(value = "完整服务器路径", dataType = "String")
    private String fullPath;

    @ApiModelProperty(value = "自定义类型", dataType = "Integer")
    private Integer customType;
}