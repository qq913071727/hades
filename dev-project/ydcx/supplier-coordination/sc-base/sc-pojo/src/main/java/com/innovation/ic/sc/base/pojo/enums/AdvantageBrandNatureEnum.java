package com.innovation.ic.sc.base.pojo.enums;

/**
 * 优势品牌性质
 */
public enum AdvantageBrandNatureEnum {
    DISTRIBUTION(1, "代理"),
    AGENT(2 , "原厂"),
    ORIGINAL_FACTORY(3, "分销");

    private Integer code;
    private String desc;

    AdvantageBrandNatureEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public static AdvantageBrandNatureEnum of(Integer code) {
        for (AdvantageBrandNatureEnum c : AdvantageBrandNatureEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }

        }
        return null;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}