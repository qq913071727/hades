package com.innovation.ic.sc.base.pojo.constant;

/**
 * http的头部属性
 */
public class HttpHeader {

    /**
     * 存储在header中的token
     */
    public static final String TOKEN_SPLIT = " ";
}
