package com.innovation.ic.sc.base.pojo.variable.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   根据创建用户分组查询优势型号数据的Pojo类
 * @author linuo
 * @time   2022年9月28日15:43:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelGroupByUserPojo", description = "根据创建用户分组查询优势型号数据的Pojo类")
public class AdvantageModelGroupByUserPojo {
    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "主账号id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "供应商id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "主账号名称", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "审核状态(0:待审核、1:审核通过、2:审核拒绝)", dataType = "Integer")
    private Integer auditStatus;

    @ApiModelProperty(value = "优势状态(0:无效、1:有效)", dataType = "Integer")
    private Integer advantageStatus;
}
