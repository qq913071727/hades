package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   按起订量配置的数据Pojo类
 * @author linuo
 * @time   2022年9月23日14:35:08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceConfigRespPojo", description = "按起订量配置的数据Pojo类")
public class LadderPriceConfigRespPojo {
    @ApiModelProperty(value = "起订量", dataType = "Integer")
    private Integer mq;

    @ApiModelProperty(value = "降比", dataType = "BigDecimal")
    private BigDecimal lower;
}