package com.innovation.ic.sc.base.pojo.variable.ladderPrice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   阶梯价格基本信息查询返回的Pojo类
 * @author linuo
 * @time   2023年5月4日16:15:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceBasicInfoRespPojo", description = "阶梯价格基本信息查询返回的Pojo类")
public class LadderPriceBasicInfoRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    private String name;
}
