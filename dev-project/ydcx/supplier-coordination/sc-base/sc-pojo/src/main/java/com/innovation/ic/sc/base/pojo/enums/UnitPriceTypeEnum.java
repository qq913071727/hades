package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   单价类型枚举
 * @author linuo
 * @time   2022年8月23日10:20:39
 */
public enum UnitPriceTypeEnum {
    SINGLE_PRICE(1, "单一价格"),
    LADDER_PRICE(2, "阶梯价格");

    private Integer code;
    private String desc;

    UnitPriceTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static UnitPriceTypeEnum of(Integer code) {
        for (UnitPriceTypeEnum c : UnitPriceTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (UnitPriceTypeEnum c : UnitPriceTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}