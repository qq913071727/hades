package com.innovation.ic.sc.base.pojo.constant;

/**
 * @desc   日常使用的状态码常量值
 * @author linuo
 * @time   2022年8月16日16:40:46
 */
public class CodeConstants {
    /** id */
    public static final int FAIL = 500;

    /** 未绑定 */
    public static final int UN_BOUNDED = 40003;

    /** erp 返回状态码为 500时 */
    public static final String FASE_CODE = "500";
}