package com.innovation.ic.sc.base.pojo.variable;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 账号的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserManagementPojo", description = "账号的pojo类")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserManagementPojo {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "被管理用户的id）", dataType = "String")
    private String managedUserId;

    @ApiModelProperty(value = "部门id", dataType = "Integer")
    private Integer departmentId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
