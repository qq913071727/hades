package com.innovation.ic.sc.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   可申请优势型号的的库存型号的Vo类
 * @author linuo
 * @time   2022年8月30日17:06:44
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MayApplyInventoryModelVo", description = "可申请优势型号的的库存型号的Pojo类")
public class MayApplyInventoryModelPojo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;
}