package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   优势型号状态恢复为可用的pojo类
 * @author linuo
 * @time   2023年2月2日16:20:40
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SpecialsScTopViewEnablePojo", description = "优势型号状态恢复为可用的pojo类")
public class SpecialsScTopViewEnablePojo {
    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String PartNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String Brand;
}