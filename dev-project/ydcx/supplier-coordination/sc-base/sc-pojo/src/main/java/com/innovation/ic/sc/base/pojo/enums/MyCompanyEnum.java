package com.innovation.ic.sc.base.pojo.enums;

public enum MyCompanyEnum {

    TO_BE_REVIEWED(100, "待审核"),
    ADOPT(200, "审核通过"),
    REFUSE(300, "审核拒绝");

    private Integer code;
    private String desc;

    MyCompanyEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
