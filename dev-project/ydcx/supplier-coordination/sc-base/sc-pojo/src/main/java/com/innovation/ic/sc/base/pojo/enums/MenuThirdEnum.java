package com.innovation.ic.sc.base.pojo.enums;


import com.innovation.ic.sc.base.pojo.constant.model.MenuType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 第三个菜单 枚举
 * @Date 2023/2/9 13:42
 **/
public enum MenuThirdEnum {
    // 库存管理: 上传库存: 新增 4-1-1
    INVENTORY_UPLOAD_INVENTORY_ADD("4-1", "4-1-1", "新增", MenuType.BUTTON, "", ""),

    // 库存管理: 管理库存: 编辑 4-3-1
    INVENTORY_MANAGE_INVENTORY_EDIT("4-3", "4-3-1", "编辑", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 设为优势型号 4-3-2
    INVENTORY_MANAGE_INVENTORY_SET_AS_ADVANTAGE_MODEL("4-3", "4-3-2", "设为优势型号", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 取消优势型号 4-3-3
    INVENTORY_MANAGE_INVENTORY_CANCEL_ADVANTAGE_MODELS("4-3", "4-3-3", "取消优势型号", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 上架 4-3-4
    INVENTORY_MANAGE_INVENTORY_PUT_ON_SHELF("4-3", "4-3-4", "上架", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 下架 4-3-5
    INVENTORY_MANAGE_INVENTORY_OFF_SHELF("4-3", "4-3-5", "下架", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 删除 4-3-6
    INVENTORY_MANAGE_INVENTORY_DELETE("4-3", "4-3-6", "删除", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 阶梯价格配置 4-3-7
    INVENTORY_MANAGE_INVENTORY_LADDER_PRICE_CONFIGURATION("4-3", "4-3-7", "阶梯价格配置", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 批量导出 4-3-8
    INVENTORY_MANAGE_INVENTORY_BATCH_EXPORT("4-3", "4-3-8", "批量导出", MenuType.BUTTON, "", ""),
    // 库存管理: 管理库存: 更新库存时间 4-3-9
    INVENTORY_MANAGE_INVENTORY_UPDATE_INVENTORY_TIME("4-3", "4-3-9", "更新库存时间", MenuType.BUTTON, "", ""),

    // 库存管理: 优势品牌: 新增 4-4-1
    INVENTORY_ADVANTAGE_BRANDS_ADD("4-4", "4-4-1", "新增", MenuType.BUTTON, "", ""),
    // 库存管理: 优势品牌: 取消 4-4-2
    INVENTORY_ADVANTAGE_BRANDS_CANCEL("4-4", "4-4-2", "取消", MenuType.BUTTON, "", ""),
    // 库存管理: 优势品牌: 申请延期 4-4-3
    INVENTORY_ADVANTAGE_BRANDS_APPLY_FOR_EXTENSION("4-4", "4-4-3", "申请延期", MenuType.BUTTON, "", ""),
    // 库存管理: 优势品牌: 再次发起 4-4-4
    INVENTORY_ADVANTAGE_BRANDS_INITIATE_AGAIN("4-4", "4-4-4", "再次发起", MenuType.BUTTON, "", ""),
    // 库存管理: 优势品牌: 删除 4-4-5
    INVENTORY_ADVANTAGE_BRANDS_DELETE("4-4", "4-4-5", "删除", MenuType.BUTTON, "", ""),


    // 财务管理: 待开发票: 申请开票 5-2-1
    FINANCIAL_TO_BE_INVOICED_REQUEST_INVOICING("5-2", "5-2-1", "申请开票", MenuType.BUTTON, "", ""),

    // 系统管理: 账号管理: 新增 6-1-1
    SYSTEM_ACCOUNT_MANAGEMENT_ADD("6-1", "6-1-1", "新增", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 修改 6-1-2
    SYSTEM_ACCOUNT_MANAGEMENT_CHANGE("6-1", "6-1-2", "修改", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 重置密码 6-1-3
    SYSTEM_ACCOUNT_MANAGEMENT_RESET_PASSWORD("6-1", "6-1-3", "重置密码", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 人员管理 6-1-4
    SYSTEM_ACCOUNT_MANAGEMENT_PERSONNEL_MANAGEMENT("6-1", "9-3-4", "人员管理", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 启用 6-1-5
    SYSTEM_ACCOUNT_MANAGEMENT_ENABLE("6-1", "6-1-5", "启用", MenuType.BUTTON, "", ""),
    // 系统管理: 账号管理: 停用 6-1-6
    SYSTEM_ACCOUNT_MANAGEMENT_DEACTIVATE("6-1", "6-1-6", "停用", MenuType.BUTTON, "", ""),

    // 系统管理: 角色管理: 新增角色 6-2-1
    SYSTEM_ROLE_MANAGEMENT_NEW_ROLE("6-2", "6-2-1", "新增角色", MenuType.BUTTON, "", ""),

    // 系统管理: 我的公司管理: 新增公司 6-3-1
    SYSTEM_MY_COMPANY_MANAGEMENT_NEW_COMPANY("6-3", "6-3-1", "新增公司", MenuType.BUTTON, "", "");

    //父级菜单
    private String parentMenu;
    //id
    private String id;
    //名称
    private String name;
    //菜单标志
    private Integer menuType;
    //路由
    private String path;
    //图标
    private String icon;

    MenuThirdEnum(String parentMenu, String id, String name, Integer menuType, String path, String icon) {
        this.parentMenu = parentMenu;
        this.id = id;
        this.name = name;
        this.menuType = menuType;
        this.path = path;
        this.icon = icon;
    }

    public String getParentMenu() {
        return parentMenu;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public String getPath() {
        return path;
    }

    public String getIcon() {
        return icon;
    }


    public static List<MenuThirdEnum> getThirdMenuList(String parentMenu) {
        List list = new ArrayList();
        for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.values()) {
            if (menuThirdEnum.getParentMenu().equals(parentMenu)) {
                list.add(menuThirdEnum);
            }
        }
        return list;
    }
}
