package com.innovation.ic.sc.base.pojo.variable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 优势品牌返回pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageBrandRespPojo", description = "优势品牌信息列表查询的返回Pojo类")
public class AdvantageBrandRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "优势性质 1分销  2代理  3原厂", dataType = "Integer")
    private Integer dominantNature;


    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "审核状态(0:待审核、1:审核通过、2:审核拒绝)", dataType = "Integer")
    private Integer auditStatus;

    @ApiModelProperty(value = "优势状态(0:无效、1:有效)", dataType = "Integer")
    private Integer advantageStatus;



    @ApiModelProperty(value = "证件有效期开始", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date certificateValidityStart;


    @ApiModelProperty(value = "证件有效期结束", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date certificateValidityEnd;

    @ApiModelProperty(value = "证明文件集合", dataType = "List")
    List<UploadFilePojo> files;


    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "审核原因", dataType = "String")
    private String auditReason;
}
