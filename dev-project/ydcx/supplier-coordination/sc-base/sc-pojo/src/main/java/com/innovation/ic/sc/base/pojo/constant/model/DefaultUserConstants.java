package com.innovation.ic.sc.base.pojo.constant.model;

/**
 * 默认用户常量类
 */
public class DefaultUserConstants {
    /** 用户名 */
    public static final String USER_NAME = "username";

    /** 名称 */
    public static final String LOWER_NAME = "name";

    /** 职位 */
    public static final String POSITION_NAME = "position";

    /** 手机号 */
    public static final String PHONE_NAME = "phone";

    /** 邮箱 */
    public static final String EMAIL_NAME = "email";

    /** 状态 */
    public static final String STATUS_NAME = "status";

    /** 创建时间 */
    public static final String CREATE_DATE_NAME = "createDate";

    /** 供应商id */
    public static final String EP_ID_NAME = "epId";

    /** 过期时间 */
    public static final String EXPIRATION = "expiration";

    /** 是否是主账号 */
    public static final String IS_MAIN = "isMain";

    /** 默认站点 */
    public static final Integer DEFAULT_WEBSITE = 1;

    /** 默认类型 */
    public static final Integer DEFAULT_TYPE = 1;

    /** 默认状态 */
    public static final Integer DEFAULT_STATUS = 200;

    /** 默认密码 */
    public static final String DEFAULT_PASSWORD = "123456";

    /** 主账号 账号 */
    public static final String EP_NAME = "epName";

    /** 主账号 id */
    public static final String FATHERID = "fatherId";
}