package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   优势型号再次发起/延期的pojo类
 * @author linuo
 * @time   2022年10月21日11:36:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SpecialsScTopViewUpdatePojo", description = "优势型号再次发起/延期的pojo类")
public class SpecialsScTopViewUpdatePojo {
    @ApiModelProperty(value = "优势ID", dataType = "String")
    private String ID;

    @ApiModelProperty(value = "公司ID", dataType = "String")
    private String EnterpriseID;

    @ApiModelProperty(value = "特色类型(1代理、2生产、3分销)", dataType = "Integer")
    private Integer Type;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String PartNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String Brand;

    @ApiModelProperty(value = "有效期开始", dataType = "Date")
    private Date StartDate;

    @ApiModelProperty(value = "有效期截至", dataType = "Date")
    private Date EndDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String Creator;

    @ApiModelProperty(value = "附件", dataType = "String")
    private String Files;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String Summary;
}