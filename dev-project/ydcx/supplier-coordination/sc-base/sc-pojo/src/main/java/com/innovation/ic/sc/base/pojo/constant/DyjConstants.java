package com.innovation.ic.sc.base.pojo.constant;

/**
 * @desc   大赢家接口使用的常量值
 * @author linuo
 * @time   2023年1月10日09:55:35
 */
public class DyjConstants {
    public static final String REQUEST_SERVICE = "RequestService";

    public static final String ES_BOM_B1B = "ES_BOM_B1B";

    public static final String REQUEST_ITEM = "RequestItem";

    public static final String SEARCH = "Search";

    public static final String TOKEN = "Token";

    public static final String QUERY = "Query";

    public static final String DATA = "Data";

    public static final String STATUS_CODE = "StatusCode";

    public static final String DATA_LOWER = "data";
}