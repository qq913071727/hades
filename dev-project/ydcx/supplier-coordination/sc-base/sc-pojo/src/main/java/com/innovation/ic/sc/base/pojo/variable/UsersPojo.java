package com.innovation.ic.sc.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 账号的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UsersPojo", description = "账号的pojo类")
public class UsersPojo {


    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "职位：1表示经理，2表示员工", dataType = "Integer")
    private Integer position;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "显示几条", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "账号状态", dataType = "Integer")
    private Integer status;
}
