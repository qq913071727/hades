package com.innovation.ic.sc.base.pojo.variable.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc   产品信息的pojo类
 * @author linuo
 * @time   2023年3月22日15:18:31
 */
@Data
public class ProductInfoPojo {
    @ApiModelProperty(value = "品牌", dataType = "Long")
    private String brand;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;
}