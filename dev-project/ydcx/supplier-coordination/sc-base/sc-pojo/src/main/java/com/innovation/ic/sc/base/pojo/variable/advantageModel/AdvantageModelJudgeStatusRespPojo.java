package com.innovation.ic.sc.base.pojo.variable.advantageModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   判断是否为优势型号的返回Vo类
 * @author linuo
 * @time   2022年9月28日09:29:52
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelJudgeStatusRespPojo", description = "优势型号信息列表查询的返回Pojo类")
public class AdvantageModelJudgeStatusRespPojo {
    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "主账号id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "主账号名称", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "品牌是否优势(true:是、false:否)", dataType = "Boolean")
    private Boolean brandIfAdvantage;

    @ApiModelProperty(value = "型号是否优势(true:是、false:否)", dataType = "Boolean")
    private Boolean modelIfAdvantage;

    @ApiModelProperty(value = "库存结果（充足、不足、无）", dataType = "Integer")
    private Integer inventoryResult;
}
