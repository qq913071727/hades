package com.innovation.ic.sc.base.pojo.variable.advantageModel;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @time   优势型号信息列表查询的返回Vo类
 * @author linuo
 * @time   2022年8月31日11:35:22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModelRespPojo", description = "优势型号信息列表查询的返回Pojo类")
public class AdvantageModelRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "审核状态(0:待审核、1:审核通过、2:审核拒绝)", dataType = "Integer")
    private Integer auditStatus;

    @ApiModelProperty(value = "优势状态(0:无效、1:有效)", dataType = "Integer")
    private Integer advantageStatus;

    /*@ApiModelProperty(value = "有效期", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd" ,timezone = "GMT+8")
    private Date effectiveDate;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd" ,timezone = "GMT+8")
    private Date createDate;*/

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "拒绝原因", dataType = "String")
    private String refusedReason;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd" ,timezone = "GMT+8")
    private Date validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd" ,timezone = "GMT+8")
    private Date validityDateEnd;

    @ApiModelProperty(value = "库存数量", dataType = "Integer")
    private Integer inventoryCount;

    @ApiModelProperty(value = "当前品牌是否为优势品牌(0否、1是)", dataType = "Integer")
    private Integer ifAdvantageBrand;
}