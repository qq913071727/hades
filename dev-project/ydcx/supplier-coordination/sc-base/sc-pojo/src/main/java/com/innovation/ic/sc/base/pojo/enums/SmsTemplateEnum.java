package com.innovation.ic.sc.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   短信模板枚举
 * @author linuo
 * @time   2022年8月5日16:26:40
 */
public enum SmsTemplateEnum {
    /** 注册短信模板 */
    REGISTERED("1","尊敬的用户您好，您注册供应商协同系统登录账号的验证码是smsCode，有效期5分钟。我们不会以任何理由向您索要验证码。【比一比】");

    private String code;
    private String desc;

    SmsTemplateEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static SmsTemplateEnum of(String code) {
        for (SmsTemplateEnum c : SmsTemplateEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(String code) {
        for (SmsTemplateEnum c : SmsTemplateEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (SmsTemplateEnum e : SmsTemplateEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}