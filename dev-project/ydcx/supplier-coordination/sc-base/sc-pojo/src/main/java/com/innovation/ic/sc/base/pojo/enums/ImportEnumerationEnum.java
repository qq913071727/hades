package com.innovation.ic.sc.base.pojo.enums;

import java.util.ArrayList;
import java.util.List;

public enum ImportEnumerationEnum {

    COMPANYTYPE(2, "供应商类型",companyTypeList());

    private Integer code;
    private String desc;
    private List<String> nameList;

    ImportEnumerationEnum(Integer code, String desc, List<String> nameList) {
        this.code = code;
        this.desc = desc;
        this.nameList = nameList;
    }

    public List<String> getNameList() {
        return nameList;
    }

    public void setNameList(List<String> nameList) {
        this.nameList = nameList;
    }

    private  static  List<String> companyTypeList() {
        List<String> arrayList = new ArrayList();
        arrayList.add("生产商");
        arrayList.add("授权代理商");
        arrayList.add("分销商");
        return arrayList;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
