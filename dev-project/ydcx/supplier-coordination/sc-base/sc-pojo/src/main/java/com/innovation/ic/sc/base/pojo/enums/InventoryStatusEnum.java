package com.innovation.ic.sc.base.pojo.enums;

/**
 * @desc   上架状态枚举
 * @author linuo
 * @time   2022年8月23日13:27:56
 */
public enum InventoryStatusEnum {
    NO_SHELVES(0, "已下架"),
    SHELVES(1, "已上架");

    private Integer code;
    private String desc;

    InventoryStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static InventoryStatusEnum of(Integer code) {
        for (InventoryStatusEnum c : InventoryStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (InventoryStatusEnum c : InventoryStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}