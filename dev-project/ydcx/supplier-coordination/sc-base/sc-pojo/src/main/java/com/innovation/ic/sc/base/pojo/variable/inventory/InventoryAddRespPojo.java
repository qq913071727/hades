package com.innovation.ic.sc.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   库存添加返回的Pojo类
 * @author linuo
 * @time   2023年2月28日16:48:15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryAddRespPojo", description = "库存添加返回的Pojo类")
public class InventoryAddRespPojo {
    @ApiModelProperty(value = "上传库存的全部数据数量", dataType = "Integer")
    private Integer allCount = 0;

    @ApiModelProperty(value = "上传成功的数据数量", dataType = "Integer")
    private Integer successCount = 0;

    @ApiModelProperty(value = "上传失败的数据数量", dataType = "Integer")
    private Integer failCount = 0;
}