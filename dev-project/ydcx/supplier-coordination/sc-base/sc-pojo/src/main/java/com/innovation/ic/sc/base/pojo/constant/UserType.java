package com.innovation.ic.sc.base.pojo.constant;

/**
 * 账号添加修改时候校验标识
 */
public class UserType {

    /**
     * 账号标识
     */
    public static final String USER_TYPE = "userName";

    /**
     * 手机号标识
     */
    public static final String PHONE_TYPE = "phone";

    /**
     * 邮箱标识
     */
    public static final String EMAIL_TYPE = "email";
}
