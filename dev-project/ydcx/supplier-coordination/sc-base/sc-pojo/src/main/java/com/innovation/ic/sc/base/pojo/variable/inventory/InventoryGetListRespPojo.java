package com.innovation.ic.sc.base.pojo.variable.inventory;

import com.innovation.ic.sc.base.pojo.variable.ladderPrice.LadderPriceInfoAllRespPojo;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.LadderPriceInfoByMoqPojo;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.LadderPriceInfoByMoqRespPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @desc   库存信息列表批量查询返回的Pojo类
 * @author linuo
 * @time   2023年1月31日10:50:31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryGetListRespPojo", description = "库存信息列表批量查询返回的Pojo类")
public class InventoryGetListRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "批次", dataType = "String")
    private String batch;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    private Integer packing;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "Integer")
    private Integer currency;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    private Integer inventoryHome;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    private Integer unitPriceType;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "创建人Id", dataType = "String")
    private String createUserId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "更新日期", dataType = "Date")
    private Date modifyDate;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "单价/最低阶梯价", dataType = "BigDecimal")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    private Integer ladderPriceId;

    @ApiModelProperty(value = "全部阶梯价格配置", dataType = "List")
    List<LadderPriceInfoAllRespPojo> allLadderPriceInfoPojoList;

    @ApiModelProperty(value = "按起订量配置的阶梯价格配置", dataType = "List")
    List<LadderPriceInfoByMoqPojo> ladderPriceInfoByMoqRespPojoList;

    @ApiModelProperty(value = "按单价配置的阶梯价格配置", dataType = "List")
    List<LadderPriceInfoAllRespPojo> ladderPriceInfoByAmountRespPojoList;
}