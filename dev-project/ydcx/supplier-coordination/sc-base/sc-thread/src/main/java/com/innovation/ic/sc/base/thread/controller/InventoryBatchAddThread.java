package com.innovation.ic.sc.base.thread.controller;

import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.innovation.ic.sc.base.vo.inventory.InventoryBatchAddVo;
import lombok.SneakyThrows;

/**
 * @desc   库存数据批量添加
 * @author linuo
 * @time   2022年10月18日16:05:08
 */
public class InventoryBatchAddThread extends AbstractControllerThread implements Runnable {

    public InventoryBatchAddThread(InventoryService inventoryService, InventoryBatchAddVo inventoryBatchAddVo, AuthenticationUser authenticationUser) {
        this.inventoryService = inventoryService;
        this.inventoryBatchAddVo = inventoryBatchAddVo;
        this.authenticationUser = authenticationUser;
    }

    @Override
    @SneakyThrows
    public void run() {
        inventoryService.batchAdd(inventoryBatchAddVo, authenticationUser);
    }
}