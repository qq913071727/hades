package com.innovation.ic.sc.base.thread.controller;

import com.innovation.ic.sc.base.pojo.constant.Constants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageBrandConstants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageModelConstants;
import com.innovation.ic.sc.base.service.sc.AdvantageBrandService;
import com.innovation.ic.sc.base.service.sc.AdvantageModelService;
import lombok.SneakyThrows;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   优势品牌、优势型号状态修改
 * @author linuo
 * @time   2023年1月13日09:44:22
 */
public class AdvantageStatusEditThread extends AbstractControllerThread implements Runnable {

    private List<Map<String, String>> list;
    private Integer status;
    private String epId;

    /**
     * 修改优势品牌、优势型号状态
     * @param advantageBrandService 优势品牌Service
     * @param advantageModelService 优势型号Service
     * @param list 品牌、型号集合
     * @param status 状态
     * @param epId 供应商id
     */
    public AdvantageStatusEditThread(AdvantageBrandService advantageBrandService, AdvantageModelService advantageModelService, List<Map<String, String>> list, Integer status, String epId) {
        this.advantageBrandService = advantageBrandService;
        this.advantageModelService = advantageModelService;
        this.list = list;
        this.status = status;
        this.epId = epId;
    }

    @Override
    @SneakyThrows
    public void run() {
        for(Map<String, String> map : list){
            String brand = map.get(AdvantageModelConstants.BRAND_FIELD);
            String partNumber = map.get(AdvantageModelConstants.PART_NUMBER);

            // 更新优势品牌生效状态
            if(brand != null){
                Map<String, Object> param = new HashMap<String, Object>();
                param.put(AdvantageBrandConstants.BRAND_FIELD, brand);
                param.put(AdvantageBrandConstants.ADVANTAGE_STATUS_FIELD, status);
                param.put(Constants.ENTERPRISE_ID, epId);
                advantageBrandService.updateStatusByParam(param);
            }

            // 更新优势型号生效状态
            if(brand != null && partNumber != null){
                Map<String, Object> param = new HashMap<String, Object>();
                param.put(AdvantageModelConstants.BRAND_FIELD, brand);
                param.put(AdvantageModelConstants.PART_NUMBER, partNumber);
                param.put(AdvantageModelConstants.ADVANTAGE_STATUS, status);
                param.put(Constants.ENTERPRISE_ID, epId);
                advantageModelService.updateStatusByParam(param);
            }
        }
    }
}