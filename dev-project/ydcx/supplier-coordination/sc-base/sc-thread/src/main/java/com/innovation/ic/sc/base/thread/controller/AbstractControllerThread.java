package com.innovation.ic.sc.base.thread.controller;

import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.service.sc.AdvantageBrandService;
import com.innovation.ic.sc.base.service.sc.AdvantageModelService;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.innovation.ic.sc.base.vo.inventory.InventoryAddVo;
import com.innovation.ic.sc.base.vo.inventory.InventoryBatchAddVo;
import com.innovation.ic.sc.base.vo.inventory.InventoryEditVo;
import lombok.extern.slf4j.Slf4j;

/**
 * 针对controller类的抽象线程类
 */
@Slf4j
public abstract class AbstractControllerThread {

    protected AdvantageBrandService advantageBrandService;
    protected AdvantageModelService advantageModelService;
    protected InventoryService inventoryService;

    protected AuthenticationUser authenticationUser;

    protected InventoryEditVo inventoryEditVo;
    protected InventoryAddVo inventoryAddVo;
    protected InventoryBatchAddVo inventoryBatchAddVo;
}
