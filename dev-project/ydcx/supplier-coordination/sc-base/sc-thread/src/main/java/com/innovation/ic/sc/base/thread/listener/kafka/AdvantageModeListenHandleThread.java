package com.innovation.ic.sc.base.thread.listener.kafka;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.model.sc.AdvantageModel;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.constant.DatabaseOperationType;
import com.innovation.ic.sc.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryAllSituationPojo;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.innovation.ic.sc.base.thread.listener.rabbitmq.AbstractRabbitmqThread;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.List;

/**
 * @desc   优势型号表监听处理线程类
 * @author linuo
 * @time   2023年2月6日14:20:35
 */
public class AdvantageModeListenHandleThread extends AbstractKafkaThread implements Runnable {

    public AdvantageModeListenHandleThread(String type, ConsumerRecord<?, ?> consumer, JSONObject dataJsonObject, ModelHandler modelHandler, ServiceHelper serviceHelper, InventoryService inventoryService) {
        this.type = type;
        this.consumer = consumer;
        this.dataJsonObject = dataJsonObject;
        this.modelHandler = modelHandler;
        this.serviceHelper = serviceHelper;
        this.inventoryService = inventoryService;
    }

    @Override
    @SneakyThrows
    public void run() {
        if (type.equals(DatabaseOperationType.INSERT)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 添加redis中的优势型号数据
            addAdvantageModelDataFromRedis(dataJsonObject);

            // 添加、删除、更新优势型号数据后，若与库存数据相关，需要更新库存数据中的优势型号状态
            updateInventoryRedisData(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.DELETE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 删除redis数据
            deleteAdvantageModelDataFromRedis(dataJsonObject);

            // 添加、删除、更新优势型号数据后，若与库存数据相关，需要更新库存数据中的优势型号状态
            updateInventoryRedisData(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.UPDATE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 删除redis数据
            deleteAdvantageModelDataFromRedis(dataJsonObject);

            // 添加redis中的优势型号数据
            addAdvantageModelDataFromRedis(dataJsonObject);

            // 添加、删除、更新优势型号数据后，若与库存数据相关，需要更新库存数据中的优势型号状态
            updateInventoryRedisData(dataJsonObject);
        }
    }

    /**
     * 添加、删除、更新优势型号数据后，若与库存数据相关，需要更新库存数据中的优势型号状态
     * @param json json
     */
    private void updateInventoryRedisData(JSONObject json) {
        AdvantageModel advantageModel = modelHandler.jsonObjectToAdvantageModel(json);
        String brand = advantageModel.getBrand();
        String partNumber = advantageModel.getPartNumber();
        String enterpriseId = advantageModel.getEnterpriseId();

        // 根据品牌、型号、供应商id查询库存中的数据
        ServiceResult<List<Inventory>> serviceResult = inventoryService.selectListByPartNumberBrandEpId(brand, partNumber, enterpriseId);
        if(serviceResult.getResult() != null && serviceResult.getResult().size() > 0){
            for(Inventory inventory : serviceResult.getResult()){
                // 根据库存数据组装库存所有情况的pojo类集合
                List<InventoryAllSituationPojo> list = getAllSituationPojoListByInventoryData(inventory);

                // 根据参数更新库存的redis数据
                for(InventoryAllSituationPojo pojo : list){
                    inventoryService.updateInventoryRedisDataByParam(pojo.getStatus(), pojo.getInventoryHome(), pojo.getUnitPriceType(), pojo.getLadderPriceId(), pojo.getEnterpriseId());
                }
            }
        }
    }

    /**
     * 添加redis中的优势型号数据
     * @param json json
     */
    private void addAdvantageModelDataFromRedis(JSONObject json) {
        AdvantageModel advantageModel = modelHandler.jsonObjectToAdvantageModel(json);
        String key = RedisKeyPrefixEnum.ADVANTAGE_MODEL_DATA.getCode() + "_" + advantageModel.getPartNumber() + "_" + advantageModel.getBrand() + "_" + advantageModel.getEnterpriseId();
        String apiResultJson = JSONObject.toJSONString(advantageModel, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
        serviceHelper.getRedisManager().set(key, apiResultJson);
    }

    /**
     * 删除redis中的优势型号数据
     * @param json json
     */
    private void deleteAdvantageModelDataFromRedis(JSONObject json) {
        AdvantageModel advantageModel = modelHandler.jsonObjectToAdvantageModel(json);
        String key = RedisKeyPrefixEnum.ADVANTAGE_MODEL_DATA.getCode() + "_" + advantageModel.getPartNumber() + "_" + advantageModel.getBrand() + "_" + advantageModel.getEnterpriseId();
        serviceHelper.getRedisManager().del(key);
    }

    @Override
    protected void doWork() {

    }
}