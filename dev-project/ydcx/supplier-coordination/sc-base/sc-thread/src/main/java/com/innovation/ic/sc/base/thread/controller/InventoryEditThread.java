package com.innovation.ic.sc.base.thread.controller;

import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.innovation.ic.sc.base.vo.inventory.InventoryEditVo;
import lombok.SneakyThrows;

/**
 * @desc   库存信息编辑更新
 * @author linuo
 * @time   2022年10月18日16:05:08
 */
public class InventoryEditThread extends AbstractControllerThread implements Runnable {

    public InventoryEditThread(InventoryService inventoryService, InventoryEditVo inventoryEditVo) {
        this.inventoryService = inventoryService;
        this.inventoryEditVo = inventoryEditVo;
    }

    @Override
    @SneakyThrows
    public void run() {
        inventoryService.edit(inventoryEditVo);
    }
}