package com.innovation.ic.sc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.variable.SpecialsScTopViewCloseDelPojo;
import com.innovation.ic.sc.base.service.sc.AdvantageModelService;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

/**
 * @desc   监听删除优势型号队列
 * @author linuo
 * @time   2022年10月18日15:06:14
 */
public class ListenAdvantageModelDeleteQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private AdvantageModelService advantageModelService;

    public ListenAdvantageModelDeleteQueueThread(Channel channel, AdvantageModelService advantageModelService) {
        this.channel = channel;
        this.advantageModelService = advantageModelService;
    }

    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.SC_ERP_EXCHANGE;
        //String queue = RabbitMqConstants.ERP_TO_SC_QUEUE_NAME;
        String routingKey = RabbitMqConstants.ERP_SC_DEL_QUEUE;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyString = new String(body);
                logger.info("接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            SpecialsScTopViewCloseDelPojo specialsScTopViewCloseDelPojo = JSONObject.parseObject(messageBody, SpecialsScTopViewCloseDelPojo.class);

                            // 处理优势型号删除信息
                            if (specialsScTopViewCloseDelPojo != null && !Strings.isNullOrEmpty(specialsScTopViewCloseDelPojo.getPartNumber()) && !Strings.isNullOrEmpty(specialsScTopViewCloseDelPojo.getBrand())) {
                                advantageModelService.handleAdvantageModelDeleteMqMsg(specialsScTopViewCloseDelPojo);
                            }

                        }
                    }
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}