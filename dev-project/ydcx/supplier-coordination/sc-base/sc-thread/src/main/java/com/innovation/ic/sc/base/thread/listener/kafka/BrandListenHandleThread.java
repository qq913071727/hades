package com.innovation.ic.sc.base.thread.listener.kafka;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.model.sc.Brand;
import com.innovation.ic.sc.base.pojo.constant.DatabaseOperationType;
import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.thread.listener.rabbitmq.AbstractRabbitmqThread;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @desc   用户监听处理线程类
 * @author lishen
 * @time   2023年2月6日16:34:12
 */
public class BrandListenHandleThread extends AbstractKafkaThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public BrandListenHandleThread(String type, ConsumerRecord<?, ?> consumer, JSONObject dataJsonObject, ModelHandler modelHandler, ServiceHelper serviceHelper) {
        this.type = type;
        this.consumer = consumer;
        this.dataJsonObject = dataJsonObject;
        this.modelHandler = modelHandler;
        this.serviceHelper = serviceHelper;
    }

    @Override
    @SneakyThrows
    public void run() {
        if (type.equals(DatabaseOperationType.INSERT)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);
            //排序
            String keyAsc = RedisStorage.TABLE + RedisStorage.BRAND +
                    RedisStorage.CREATE_DATE_ASC;
            String keyDesc = RedisStorage.TABLE + RedisStorage.BRAND +
                    RedisStorage.CREATE_DATE_DESC;

            Brand brand = modelHandler.jsonObjectToBrand(dataJsonObject);
            //转化成对应的xml
            String value = modelHandler.brandToXmlStorageString(brand);
            //放入redis
            serviceHelper.getRedisManager().zAdd(keyAsc, value, brand.getCreateDate().getTime());
            serviceHelper.getRedisManager().zAdd(keyDesc, value, -brand.getCreateDate().getTime());
        }
        if (type.equals(DatabaseOperationType.DELETE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);
            //排序
            String keyAsc = RedisStorage.TABLE + RedisStorage.BRAND +
                    RedisStorage.CREATE_DATE_ASC;
            String keyDesc = RedisStorage.TABLE + RedisStorage.BRAND +
                    RedisStorage.CREATE_DATE_DESC;
            Brand brand = modelHandler.jsonObjectToBrand(dataJsonObject);
            //转化成对应的xml
            String value = modelHandler.brandToXmlStorageString(brand);
            //删除缓存
            serviceHelper.getRedisManager().zRemove(keyAsc, value);
            serviceHelper.getRedisManager().zRemove(keyDesc, value);
        }
        if (type.equals(DatabaseOperationType.UPDATE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);
            //排序
            String keyAsc = RedisStorage.TABLE + RedisStorage.BRAND +
                    RedisStorage.CREATE_DATE_ASC;
            String keyDesc = RedisStorage.TABLE + RedisStorage.BRAND +
                    RedisStorage.CREATE_DATE_DESC;
            Brand brand = modelHandler.jsonObjectToBrand(dataJsonObject);
            Brand newBrand = new Brand();
            newBrand.setId(brand.getId());
            String newBrandString = modelHandler.brandToXmlFilterString(newBrand, false);

            List ascList = serviceHelper.getRedisManager().page(keyAsc, newBrandString);
            if (null != ascList && ascList.size() > 0) {
                for (int i = 0; i < ascList.size(); i++) {
                    String xmlString = (String) ascList.get(i);
                    serviceHelper.getRedisManager().zRemove(keyAsc, xmlString);
                    String value = modelHandler.brandToXmlStorageString(brand);
                    double score = brand.getCreateDate().getTime();
                    serviceHelper.getRedisManager().zAdd(keyAsc, value, score);
                }
            }

            List descList = serviceHelper.getRedisManager().page(keyDesc, newBrandString);
            if (null != descList && descList.size() > 0) {
                for (int i = 0; i < descList.size(); i++) {
                    String xmlString = (String) descList.get(i);
                    serviceHelper.getRedisManager().zRemove(keyDesc, xmlString);
                    String value = modelHandler.brandToXmlStorageString(brand);
                    double score = brand.getCreateDate().getTime();
                    serviceHelper.getRedisManager().zAdd(keyDesc, value, score);
                }
            }
        }
    }

    @Override
    protected void doWork() {

    }
}