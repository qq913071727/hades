package com.innovation.ic.sc.base.thread.listener.kafka;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.innovation.ic.sc.base.mapper.sc.PersonnelManagementMapper;
import com.innovation.ic.sc.base.model.sc.PersonnelManagement;
import com.innovation.ic.sc.base.pojo.constant.DatabaseOperationType;
import com.innovation.ic.sc.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.thread.listener.rabbitmq.AbstractRabbitmqThread;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import java.util.List;

public class PersonnelManagementListenHandleThread extends AbstractKafkaThread implements Runnable {

    public PersonnelManagementListenHandleThread(String type, ConsumerRecord<?, ?> consumer, JSONObject dataJsonObject, ServiceHelper serviceHelper, PersonnelManagementMapper personnelManagementMapper) {
        this.type = type;
        this.consumer = consumer;
        this.dataJsonObject = dataJsonObject;
        this.serviceHelper = serviceHelper;
        this.personnelManagementMapper = personnelManagementMapper;
    }

    @Override
    @SneakyThrows
    public void run() {
        if (type.equals(DatabaseOperationType.INSERT)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 添加redis中的人员管理数据
            addPersonnelManagementDataFromRedis(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.DELETE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 删除redis数据
            deletePersonnelManagementDataFromRedis(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.UPDATE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 删除redis数据
            deletePersonnelManagementDataFromRedis(dataJsonObject);

            // 添加redis中的人员管理数据
            addPersonnelManagementDataFromRedis(dataJsonObject);
        }
    }

    /**
     * 添加redis中的人员管理数据
     *
     * @param json json
     */
    private void addPersonnelManagementDataFromRedis(JSONObject json) {
        // 主账号条件
        String userId = json.getString("user_id");
        QueryWrapper<PersonnelManagement> personnelManagementQueryWrapper = new QueryWrapper<PersonnelManagement>();
        personnelManagementQueryWrapper.eq("user_id", userId);
        List<PersonnelManagement> personnelManagementList = personnelManagementMapper.selectList(personnelManagementQueryWrapper);
        String key = RedisKeyPrefixEnum.PERSONNEL_MANAGEMENT_DATA.getCode() + "_" + userId;
        String apiResultJson = JSONObject.toJSONString(personnelManagementList, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
        serviceHelper.getRedisManager().set(key, apiResultJson);
    }

    /**
     * 删除redis中的人员管理数据
     *
     * @param json json
     */
    private void deletePersonnelManagementDataFromRedis(JSONObject json) {
        // 主账号条件(redis中key)
        String userId = json.getString("user_id");
        String key = RedisKeyPrefixEnum.PERSONNEL_MANAGEMENT_DATA.getCode() + "_" + userId;
        serviceHelper.getRedisManager().del(key);
    }

    @Override
    protected void doWork() {

    }
}