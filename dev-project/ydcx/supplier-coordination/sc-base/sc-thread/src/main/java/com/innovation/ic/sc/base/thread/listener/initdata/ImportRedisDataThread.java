package com.innovation.ic.sc.base.thread.listener.initdata;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportRedisDataThread implements Runnable{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String[] scripts;

    public ImportRedisDataThread(String[] scripts) {
        this.scripts = scripts;
    }

    @SneakyThrows
    @Override
    public void run() {
        if(scripts != null){
            logger.info("-----开始执行命令-----");
            for(String str : scripts){
                logger.info("执行脚本 -> {}", str);
                Thread.sleep(1000 * 5);
                try {
                    Process exec = Runtime.getRuntime().exec(str);
                    exec.waitFor();
                }catch (Exception e){
                    logger.error("执行脚本[{}]出现问题,原因:", str, e);
                }
            }
            logger.info("-----执行命令结束-----");
        }
    }
}