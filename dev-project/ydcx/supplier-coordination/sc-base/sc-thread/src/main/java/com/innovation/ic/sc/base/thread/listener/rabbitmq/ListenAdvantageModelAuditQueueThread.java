package com.innovation.ic.sc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageConstants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageModelConstants;
import com.innovation.ic.sc.base.service.sc.AdvantageModelService;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

/**
 * @desc   监听优势型号审批队列
 * @author linuo
 * @time   2022年10月18日15:02:53
 */
public class ListenAdvantageModelAuditQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private AdvantageModelService advantageModelService;

    public ListenAdvantageModelAuditQueueThread(Channel channel, AdvantageModelService advantageModelService) {
        this.channel = channel;
        this.advantageModelService = advantageModelService;
    }

    @Override
    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.SC_ERP_EXCHANGE;
        //String queue = RabbitMqConstants.ERP_TO_SC_QUEUE_NAME;
        String routingKey = RabbitMqConstants.ERP_SC_AUDIT_QUEUE;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyString = new String(body);
                logger.info("接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            JSONObject json = (JSONObject) JSON.parse(messageBody);
                            if(json != null && !json.isEmpty()){
                                String id = json.getString(AdvantageConstants.ID);
                                String status = json.getString(AdvantageModelConstants.STATUS);
                                String summary = json.getString(AdvantageModelConstants.SUMMARY);
                                if(!Strings.isNullOrEmpty(id) && !Strings.isNullOrEmpty(status)){
                                    // 处理优势型号审批信息
                                    advantageModelService.handleAdvantageModelAuditMqMsg(id, status, summary);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}