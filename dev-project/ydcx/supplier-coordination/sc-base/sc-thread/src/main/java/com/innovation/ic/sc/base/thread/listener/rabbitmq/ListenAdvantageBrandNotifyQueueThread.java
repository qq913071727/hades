package com.innovation.ic.sc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageConstants;
import com.innovation.ic.sc.base.service.sc.AdvantageBrandService;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

/**
 * @desc   监听优势型号Erp通知队列
 * @author linuo
 * @time   2023年2月7日09:22:08
 */
public class ListenAdvantageBrandNotifyQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private AdvantageBrandService advantageBrandService;

    public ListenAdvantageBrandNotifyQueueThread(Channel channel, AdvantageBrandService advantageBrandService) {
        this.channel = channel;
        this.advantageBrandService = advantageBrandService;
    }

    @Override
    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.SC_ERP_EXCHANGE;
        String routingKey = RabbitMqConstants.ERP_SC_ADD_BRAND_NOTIFY_QUEUE;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyString = new String(body);
                logger.info("优势品牌新增erpId  接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            JSONObject json = (JSONObject) JSON.parse(messageBody);
                            if(json != null && !json.isEmpty()){
                                String brand = json.getString(AdvantageConstants.BRAND);
                                String enterpriseId = json.getString(AdvantageConstants.ENTERPRISE_ID);
                                String id = json.getString(AdvantageConstants.ID);
                                // 处理优势品牌通知的mq消息
                                if(!Strings.isNullOrEmpty(brand) && !Strings.isNullOrEmpty(enterpriseId) && !Strings.isNullOrEmpty(id)){
                                    advantageBrandService.handleAdvantageModelNotifyMqMsg(brand, enterpriseId, id);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}