package com.innovation.ic.sc.base.thread.listener.kafka;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.model.sc.AdvantageBrand;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.constant.DatabaseOperationType;
import com.innovation.ic.sc.base.pojo.variable.ServiceResult;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryAllSituationPojo;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import java.util.List;

/**
 * @desc   优势品牌表监听处理线程类
 * @author linuo
 * @time   2023年2月15日15:25:37
 */
public class AdvantageBrandListenHandleThread extends AbstractKafkaThread implements Runnable {

    public AdvantageBrandListenHandleThread(String type, ConsumerRecord<?, ?> consumer, JSONObject dataJsonObject, ModelHandler modelHandler, InventoryService inventoryService) {
        this.type = type;
        this.consumer = consumer;
        this.dataJsonObject = dataJsonObject;
        this.modelHandler = modelHandler;
        this.inventoryService = inventoryService;
    }

    @Override
    @SneakyThrows
    public void run() {
        if (type.equals(DatabaseOperationType.INSERT)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 添加、删除、更新优势品牌数据后，若与库存数据相关，需要更新库存数据中的优势品牌状态
            updateInventoryRedisData(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.DELETE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 添加、删除、更新优势品牌数据后，若与库存数据相关，需要更新库存数据中的优势品牌状态
            updateInventoryRedisData(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.UPDATE)) {
            // 打印监听到的数据库变动内容
            printConsumerContent(consumer);

            // 添加、删除、更新优势品牌数据后，若与库存数据相关，需要更新库存数据中的优势品牌状态
            updateInventoryRedisData(dataJsonObject);
        }
    }

    /**
     * 添加、删除、更新优势品牌数据后，若与库存数据相关，需要更新库存数据中的优势品牌状态
     * @param json json
     */
    private void updateInventoryRedisData(JSONObject json) {
        AdvantageBrand advantageBrand = modelHandler.jsonObjectToAdvantageBrand(json);
        String brand = advantageBrand.getBrand();
        String enterpriseId = advantageBrand.getEnterpriseId();

        // 根据品牌、型号、供应商id查询库存中的数据
        ServiceResult<List<Inventory>> serviceResult = inventoryService.selectListByPartNumberBrandEpId(brand, null, enterpriseId);
        if(serviceResult.getResult() != null && serviceResult.getResult().size() > 0){
            for(Inventory inventory : serviceResult.getResult()){
                // 根据库存数据组装库存所有情况的pojo类集合
                List<InventoryAllSituationPojo> list = getAllSituationPojoListByInventoryData(inventory);

                // 根据参数更新库存的redis数据
                for(InventoryAllSituationPojo pojo : list){
                    inventoryService.updateInventoryRedisDataByParam(pojo.getStatus(), pojo.getInventoryHome(), pojo.getUnitPriceType(), pojo.getLadderPriceId(), pojo.getEnterpriseId());
                }
            }
        }
    }

    @Override
    protected void doWork() {

    }
}