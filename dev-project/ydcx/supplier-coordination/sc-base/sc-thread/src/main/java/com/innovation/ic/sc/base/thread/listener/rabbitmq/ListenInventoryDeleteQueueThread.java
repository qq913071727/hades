package com.innovation.ic.sc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.constant.model.InventoryConstants;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

/**
 * @desc   库存数据删除
 * @author linuo
 * @time   2023年2月16日10:04:54
 */
public class ListenInventoryDeleteQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private InventoryService inventoryService;

    public ListenInventoryDeleteQueueThread(Channel channel , InventoryService inventoryService) {
        this.channel = channel;
        this.inventoryService = inventoryService;
    }

    @Override
    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.SC_ERP_EXCHANGE;
        String routingKey = RabbitMqConstants.ERP_SC_ADD_INVENTORY_DEL_QUEUE;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyString = new String(body);
                logger.info("接收到库存数据删除队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            JSONObject json = (JSONObject) JSON.parse(messageBody);
                            if(json != null && !json.isEmpty()){
                                Integer id = json.getInteger(InventoryConstants.ID_FIELD);
                                if(id != null){
                                    // 处理库存数据删除队列数据
                                    inventoryService.handleInventoryDeleteQueueData(id);
                                }else{
                                    logger.info("库存数据删除队列数据中没有库存id,无法处理");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}