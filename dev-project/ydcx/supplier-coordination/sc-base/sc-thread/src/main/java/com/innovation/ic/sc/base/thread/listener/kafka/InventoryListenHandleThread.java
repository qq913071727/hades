package com.innovation.ic.sc.base.thread.listener.kafka;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.innovation.ic.sc.base.handler.sc.ModelHandler;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.constant.DatabaseOperationType;
import com.innovation.ic.sc.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryAllSituationPojo;
import com.innovation.ic.sc.base.service.ServiceHelper;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.List;

/**
 * @desc   库存表监听处理线程类
 * @author linuo
 * @time   2023年2月13日13:31:06
 */
public class InventoryListenHandleThread extends AbstractKafkaThread implements Runnable {

    public InventoryListenHandleThread(String type, ConsumerRecord<?, ?> consumer, JSONObject dataJsonObject,
                                       ModelHandler modelHandler, ServiceHelper serviceHelper,
                                       InventoryService inventoryService) {
        this.type = type;
        this.consumer = consumer;
        this.dataJsonObject = dataJsonObject;
        this.modelHandler = modelHandler;
        this.serviceHelper = serviceHelper;
        this.inventoryService = inventoryService;
    }

    @Override
    @SneakyThrows
    public void run() {
        doInRunMethod(consumer);
    }

    /**
     * 更新redis中Inventory列表查询的结果数据
     * @param json json
     */
    private void updateInventoryQueryRedisData(JSONObject json){
        Inventory inventory = modelHandler.jsonObjectToInventory(json);

        // 根据库存数据组装库存所有情况的pojo类集合
        List<InventoryAllSituationPojo> list = getAllSituationPojoListByInventoryData(inventory);

        // 根据参数更新库存的redis数据
        for(InventoryAllSituationPojo pojo : list){
            inventoryService.updateInventoryRedisDataByParam(pojo.getStatus(), pojo.getInventoryHome(), pojo.getUnitPriceType(), pojo.getLadderPriceId(), pojo.getEnterpriseId());
        }
    }

    /**
     * 添加redis中的库存数据
     * @param json json
     */
    private void addInventoryDataFromRedis(JSONObject json) {
        Inventory inventory = modelHandler.jsonObjectToInventory(json);
        String key = RedisKeyPrefixEnum.INVENTORY_DATA.getCode() + "_" + inventory.getId();
        String apiResultJson = JSONObject.toJSONString(inventory, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
        serviceHelper.getRedisManager().set(key, apiResultJson);
    }

    /**
     * 删除redis中的库存数据
     * @param json json
     */
    private void deleteInventoryDataFromRedis(JSONObject json) {
        Inventory inventory = modelHandler.jsonObjectToInventory(json);
        String key = RedisKeyPrefixEnum.INVENTORY_DATA.getCode() + "_" + inventory.getId();
        serviceHelper.getRedisManager().del(key);
    }

    @Override
    protected void doWork() {
        if (type.equals(DatabaseOperationType.INSERT)) {
            // 添加redis中的库存数据
            addInventoryDataFromRedis(dataJsonObject);

            // 更新redis中Inventory列表查询的结果数据
            updateInventoryQueryRedisData(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.DELETE)) {
            // 删除redis数据
            deleteInventoryDataFromRedis(dataJsonObject);

            // 更新redis中Inventory列表查询的结果数据
            updateInventoryQueryRedisData(dataJsonObject);
        }

        if (type.equals(DatabaseOperationType.UPDATE)) {
            // 删除redis数据
            deleteInventoryDataFromRedis(dataJsonObject);

            // 添加redis中的库存数据
            addInventoryDataFromRedis(dataJsonObject);

            // 更新redis中Inventory列表查询的结果数据
            updateInventoryQueryRedisData(dataJsonObject);
        }
    }
}