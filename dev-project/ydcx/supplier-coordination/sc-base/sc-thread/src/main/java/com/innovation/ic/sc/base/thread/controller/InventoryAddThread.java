package com.innovation.ic.sc.base.thread.controller;

import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import com.innovation.ic.sc.base.service.sc.InventoryService;
import com.innovation.ic.sc.base.vo.inventory.InventoryAddVo;
import lombok.SneakyThrows;

/**
 * @desc   库存新增
 * @author linuo
 * @time   2022年10月18日16:19:46
 */
public class InventoryAddThread extends AbstractControllerThread implements Runnable {

    public InventoryAddThread(InventoryService inventoryService, InventoryAddVo inventoryAddVo, AuthenticationUser authenticationUser) {
        this.inventoryService = inventoryService;
        this.inventoryAddVo = inventoryAddVo;
        this.authenticationUser = authenticationUser;
    }

    @Override
    @SneakyThrows
    public void run() {
        inventoryService.add(inventoryAddVo, authenticationUser);
    }
}