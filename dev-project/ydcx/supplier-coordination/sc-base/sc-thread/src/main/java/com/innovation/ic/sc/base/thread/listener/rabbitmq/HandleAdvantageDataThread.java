package com.innovation.ic.sc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.constant.model.AdvantageConstants;
import com.innovation.ic.sc.base.service.sc.AdvantageBrandService;
import com.innovation.ic.sc.base.service.sc.AdvantageModelService;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc   处理优势数据
 * @author linuo
 * @time   2023年3月16日09:26:28
 */
public class HandleAdvantageDataThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String queue;
    private String bodyString;
    private AdvantageModelService advantageModelService;
    private AdvantageBrandService advantageBrandService;

    public HandleAdvantageDataThread(String queue, String bodyString, AdvantageModelService advantageModelService, AdvantageBrandService advantageBrandService) {
        this.queue = queue;
        this.bodyString = bodyString;
        this.advantageModelService = advantageModelService;
        this.advantageBrandService = advantageBrandService;
    }

    @Override
    @SneakyThrows
    public void run() {
        logger.info("接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
        try {
            Thread.sleep(2000);
            if (!Strings.isNullOrEmpty(bodyString)) {
                // 获取消息体中的数据内容
                String messageBody = getMessageBody(bodyString);
                if (!Strings.isNullOrEmpty(messageBody)) {
                    JSONObject json = (JSONObject) JSON.parse(messageBody);
                    if(json != null && !json.isEmpty()){
                        String brand = json.getString(AdvantageConstants.BRAND);
                        String partNumber = json.getString(AdvantageConstants.PART_NUMBER);
                        String enterpriseId = json.getString(AdvantageConstants.ENTERPRISE_ID);
                        String id = json.getString(AdvantageConstants.ID);

                        // 处理优势型号通知的mq消息
                        if(!Strings.isNullOrEmpty(brand) && !Strings.isNullOrEmpty(partNumber) && !Strings.isNullOrEmpty(enterpriseId) && !Strings.isNullOrEmpty(id)){
                            logger.info("处理优势型号通知的mq消息");
                            advantageModelService.handleAdvantageModelNotifyMqMsg(brand, partNumber, enterpriseId, id);
                        }

                        // 处理优势品牌通知的mq消息
                        if(!Strings.isNullOrEmpty(brand) && Strings.isNullOrEmpty(partNumber) && !Strings.isNullOrEmpty(enterpriseId) && !Strings.isNullOrEmpty(id)){
                            logger.info("处理优势品牌通知的mq消息");
                            advantageBrandService.handleAdvantageModelNotifyMqMsg(brand, enterpriseId, id);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
        }
    }
}