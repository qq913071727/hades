package com.innovation.ic.sc.base.thread.listener.rabbitmq;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.sc.base.pojo.constant.handler.RabbitMqConstants;
import com.innovation.ic.sc.base.pojo.variable.SpecialsScTopViewAuditPojo;
import com.innovation.ic.sc.base.service.sc.AdvantageBrandService;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @desc   监听优势品牌审批队列
 * @author linuo
 * @time   2022年10月18日15:02:53
 */
public class ListenAdvantageBrandAuditQueueThread extends AbstractRabbitmqThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Channel channel;
    private AdvantageBrandService advantageBrandService;

    public ListenAdvantageBrandAuditQueueThread(Channel channel, AdvantageBrandService advantageBrandService) {
        this.channel = channel;
        this.advantageBrandService = advantageBrandService;
    }

    @SneakyThrows
    public void run() {
        String exchange = RabbitMqConstants.SC_ERP_EXCHANGE;
        String routingKey = RabbitMqConstants.ERP_SC_BRAND_AUDIT_QUEUE;
        final String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyString = new String(body);
                logger.info("优势品牌审批 接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    if (!Strings.isNullOrEmpty(bodyString)) {
                        // 获取消息体中的数据内容
                        String messageBody = getMessageBody(bodyString);
                        if (!Strings.isNullOrEmpty(messageBody)) {
                            SpecialsScTopViewAuditPojo specialsScTopViewAuditPojo = JSONObject.parseObject(messageBody, SpecialsScTopViewAuditPojo.class);
                            // 处理优势品牌审批信息
                            if (specialsScTopViewAuditPojo != null && !Strings.isNullOrEmpty(specialsScTopViewAuditPojo.getID())) {
                                advantageBrandService.handleAdvantageModelAuditMqMsg(specialsScTopViewAuditPojo);
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("优势品牌审批 监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}