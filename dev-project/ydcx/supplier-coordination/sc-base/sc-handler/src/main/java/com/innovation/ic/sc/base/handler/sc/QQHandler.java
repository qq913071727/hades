package com.innovation.ic.sc.base.handler.sc;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.sc.base.pojo.constant.LoginConstants;
import com.innovation.ic.sc.base.pojo.constant.third_party_api.WechatConstants;
import com.innovation.ic.sc.base.pojo.enums.LoginTypeEnum;
import org.apache.http.HttpStatus;

/**
 * @desc   QQ相关工具类
 * @author linuo
 * @time   2022年8月17日09:25:58
 */
public class QQHandler {
    /**
     * 获取QQ登录授权地址
     * @param getAuthUrl 获取登录授权地址url
     * @param redirectUrl 回调地址
     * @return 返回
     */
    public static String getAuthUrl(String getAuthUrl, String redirectUrl){
        String url = null;
        String param = LoginConstants.TYPE + LoginConstants.EQUAL_SIGN + LoginTypeEnum.QQ.getCode().toString() + "&" +
                WechatConstants.REDIRECT_URL_FIELD + LoginConstants.EQUAL_SIGN + redirectUrl;
        String result = HttpUtils.sendGet(getAuthUrl, param, null);
        if(!Strings.isNullOrEmpty(result)){
            JSONObject json = (JSONObject) JSONObject.parse(result);
            Integer status = json.getInteger(LoginConstants.STATUS_FIELD);
            if(status.intValue() == HttpStatus.SC_OK){
                url = json.getString(LoginConstants.DATA);
            }
        }
        return url;
    }
}