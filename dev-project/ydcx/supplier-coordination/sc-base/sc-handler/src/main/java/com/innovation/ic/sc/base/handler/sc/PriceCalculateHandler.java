package com.innovation.ic.sc.base.handler.sc;

import org.springframework.stereotype.Component;
import java.math.BigDecimal;

/**
 * @desc   价格计算的处理类
 * @author linuo
 * @time   2023年2月1日16:41:12
 */
@Component
public class PriceCalculateHandler {

    /**
     * 根据降比计算价格
     * @param unitPrice 单价
     * @param lower 降比
     * @return 返回计算结果
     */
    public BigDecimal calculateUnitPriceByLower(BigDecimal unitPrice, BigDecimal lower){
        if(unitPrice == null){
            return BigDecimal.ZERO;
        }
        BigDecimal lowerPrice = unitPrice.multiply(lower);
        lowerPrice = lowerPrice.setScale(6, BigDecimal.ROUND_HALF_UP);
        lowerPrice = lowerPrice.divide(new BigDecimal(100));
        lowerPrice = lowerPrice.setScale(6, BigDecimal.ROUND_HALF_UP);

        unitPrice = unitPrice.subtract(lowerPrice);
        unitPrice = unitPrice.setScale(6, BigDecimal.ROUND_HALF_UP);

        return unitPrice;
    }
}