package com.innovation.ic.sc.base.handler.sc;

import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.global.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Set;

@Component
public class ContextHandler extends SCAbstractHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 向Context中添加clientId列表
     */
    public void putClientSet() {
        logger.debug("向Context中添加clientId列表");

        Set<String> clientSet = redisManager.sMembers(RedisStorage.CLIENT);
        Context.put(RedisStorage.CLIENT, clientSet);
    }
}