package com.innovation.ic.sc.base.handler.sc;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.sc.base.model.sc.MenuTree;
import com.innovation.ic.sc.base.pojo.constant.model.MenuIconConstants;
import com.innovation.ic.sc.base.pojo.constant.model.MenuRouteConstants;
import com.innovation.ic.sc.base.pojo.constant.model.MenuSelected;
import com.innovation.ic.sc.base.pojo.constant.model.MenuType;
import com.innovation.ic.sc.base.pojo.enums.MenuFirstEnum;
import com.innovation.ic.sc.base.pojo.enums.MenuSecondEnum;
import com.innovation.ic.sc.base.pojo.enums.MenuThirdEnum;
import com.innovation.ic.sc.base.pojo.variable.menu.Menu;
import org.springframework.stereotype.Component;

@Component
public class MenuHandler extends SCAbstractHandler {

//    public static void main(String[] args) {
//        MenuHandler menuHandler = new MenuHandler();
//        MenuTree mainAccountMenuTree = menuHandler.createSubAccountManagerMenuTree();
//        System.out.println(mainAccountMenuTree);
//        System.out.println(JSON.toJSONString(menuHandler.createFullMenuList()));
//        System.out.println(JSON.toJSONString(menuHandler.createMainAccountMenuList()));
//        System.out.println(JSON.toJSONString(menuHandler.createSubAccountManagerMenuList()));
//        System.out.println(JSON.toJSONString(menuHandler.createSubAccountPersonnelMenuList()));
//    }

    /**
     * 生成所有菜单
     */
    public MenuTree createFullMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();

        // 首页
        {
            Menu firstMenu = new Menu();
            firstMenu.setName("首页");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);
        }

        // 库存管理
        {
            Menu firstMenu = new Menu();
            firstMenu.setName("库存管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);

            // 上传库存
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("上传库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 阶梯价格管理
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("阶梯价格管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 修改
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("修改");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 管理库存
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("管理库存");
                secondMenu.setType(1);
                secondMenu.setSelected(0);
                firstMenu.getChildMenuList().add(secondMenu);

                // 显示阶梯价格
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("显示阶梯价格");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上/下架
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("上/下架");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 编辑/阶梯价格配置/删除
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("编辑/阶梯价格配置/删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 批量导出
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("批量导出");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 更新库存时间
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("更新库存时间");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势品牌
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("设置优势品牌");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/再次发起/申请延期
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("单条新增/再次发起/申请延期");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上传
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势型号
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("设置优势型号");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/批量新增/再次发起
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("单条新增/批量新增/再次发起");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 报价管理
        {
            Menu firstMenu = new Menu();
            firstMenu.setName("报价管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);

            // 实单报价
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("实单报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 普通报价
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("普通报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 搜商机
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("搜商机");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 我的议价
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("我的议价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 议价
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 订单管理
        {
            Menu firstMenu = new Menu();
            firstMenu.setName("订单管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);

            // 我的订单
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("我的订单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 财务管理
        {
            Menu firstMenu = new Menu();
            firstMenu.setName("财务管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);

            // 对账单
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("对账单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 确认
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("确认");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 待开发票
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("待开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 申请开票
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("申请开票");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 已开发票
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("已开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 合同管理
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("合同管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 系统管理
        {
            Menu firstMenu = new Menu();
            firstMenu.setName("系统管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            menuTree.getFirstMenuList().add(firstMenu);

            // 账号管理
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("账号管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 人员管理
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("人员管理");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 新增/重置密码/设置权限/启用/停用
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("新增/重置密码/设置权限/启用/停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 角色管理
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("角色管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 系统公告
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("系统公告");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 消息提醒
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("消息提醒");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 我的公司管理
            {
                Menu secondMenu = new Menu();
                secondMenu.setName("我的公司管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 停用
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setName("停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        return menuTree;
    }

    /**
     * 生成主账号的菜单。主账号：有所有菜单和按钮权限，除了系统管理-账号管理-人员管理
     */
    public MenuTree createMainAccountMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();

      /*  // 首页 1
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("1");
            firstMenu.setName("首页");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(MenuRouteConstants.HOME_PAGE);
            firstMenu.setIcon(MenuIconConstants.DASHBOARD);
            menuTree.getFirstMenuList().add(firstMenu);
        }

        // 库存管理 2
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("2");
            firstMenu.setName("库存管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(MenuRouteConstants.STOCK);
            firstMenu.setIcon(MenuIconConstants.DICT);
            menuTree.getFirstMenuList().add(firstMenu);

            // 上传库存 2-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-1");
                secondMenu.setName("上传库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.UPLOAD_STOCK);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 阶梯价格管理 2-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-2");
                secondMenu.setName("阶梯价格管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.STEPPED_PRICE);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增 2-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 修改 2-2-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-2");
                    thirdMenu.setName("修改");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-2-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-3");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 管理库存 2-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-3");
                secondMenu.setName("管理库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(0);
                secondMenu.setPath(MenuRouteConstants.MANAGE_STOCK);
                firstMenu.getChildMenuList().add(secondMenu);

                // 显示阶梯价格 2-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-1");
                    thirdMenu.setName("显示阶梯价格");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上/下架 2-3-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-2");
                    thirdMenu.setName("上/下架");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 编辑/阶梯价格配置/删除 2-3-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-3");
                    thirdMenu.setName("编辑/阶梯价格配置/删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 批量导出 2-3-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-4");
                    thirdMenu.setName("批量导出");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 更新库存时间 2-3-5
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-5");
                    thirdMenu.setName("更新库存时间");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势品牌 2-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-4");
                secondMenu.setName("优势品牌");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.ADVANTAGE_BRAND);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/再次发起/申请延期 2-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-1");
                    thirdMenu.setName("单条新增/再次发起/申请延期");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-4-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-2");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消 2-4-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-3");
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上传 2-4-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-4");
                    thirdMenu.setName("上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势型号 2-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-5");
                secondMenu.setName("优势型号");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.ADVANTAGE_MODEL);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/批量新增/再次发起 2-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-1");
                    thirdMenu.setName("单条新增/批量新增/再次发起");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消 2-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-2");
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-5-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-3");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 报价管理 3
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("3");
            firstMenu.setName("报价管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(MenuRouteConstants.MANAGE_OFFER);
            firstMenu.setIcon(MenuIconConstants.POST);
            menuTree.getFirstMenuList().add(firstMenu);

            // 实单报价 3-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-1");
                secondMenu.setName("实单报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.ACTUAL_OFFER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价 3-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-1-1");
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 普通报价 3-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-2");
                secondMenu.setName("普通报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.COMMON_OFFER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价 3-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-2-1");
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 搜商机 3-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-3");
                secondMenu.setName("搜商机");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.SOMMER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价 3-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-1");
                    thirdMenu.setName("报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 我的议价 3-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-4");
                secondMenu.setName("我的议价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.MY_BRAGINING);
                firstMenu.getChildMenuList().add(secondMenu);

                // 议价 3-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-4-1");
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 订单管理 4
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("4");
            firstMenu.setName("订单管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(MenuRouteConstants.ORDER_STOCK);
            firstMenu.setIcon(MenuIconConstants.CHART);
            menuTree.getFirstMenuList().add(firstMenu);

            // 我的订单 4-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("4-1");
                secondMenu.setName("我的订单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.MY_ORDER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传 4-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-1");
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 财务管理 5
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("5");
            firstMenu.setName("财务管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(MenuRouteConstants.FINANCE);
            firstMenu.setIcon(MenuIconConstants.MONEY);
            menuTree.getFirstMenuList().add(firstMenu);

            // 对账单 5-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-1");
                secondMenu.setName("对账单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.AWAIT_BILL);
                firstMenu.getChildMenuList().add(secondMenu);

                // 确认 5-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-1-1");
                    thirdMenu.setName("确认");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 待开发票 5-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-2");
                secondMenu.setName("待开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.AWAIT_INVOICED);
                firstMenu.getChildMenuList().add(secondMenu);

                // 申请开票 5-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-2-1");
                    thirdMenu.setName("申请开票");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 已开发票 5-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-3");
                secondMenu.setName("已开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.INVOICED);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 合同管理 5-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-4");
                secondMenu.setName("合同管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.CONTRACT_MANAGE);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传 5-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-4-1");
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 系统管理 6
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("6");
            firstMenu.setName("系统管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(MenuRouteConstants.SYSTEM);
            firstMenu.setIcon(MenuIconConstants.BUILD);
            menuTree.getFirstMenuList().add(firstMenu);

            // 账号管理 6-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-1");
                secondMenu.setName("账号管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.MANAGE_ACCOUNT);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增/重置密码/设置权限/启用/停用 6-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-1-1");
                    thirdMenu.setName("新增/重置密码/设置权限/启用/停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 角色管理 6-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-2");
                secondMenu.setName("角色管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.MANAGE_ROLE);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 系统公告 6-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-3");
                secondMenu.setName("系统公告");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.SYSTEM_NOTICE);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 消息提醒 6-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-4");
                secondMenu.setName("消息提醒");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.NEWS);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 我的公司管理 6-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-5");
                secondMenu.setName("我的公司管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(MenuRouteConstants.MY_COMPANY);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增 6-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-5-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 停用 6-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-5-2");
                    thirdMenu.setName("停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.YES);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }*/

        //获取第一级菜单
        for (MenuFirstEnum menuFirstEnum : MenuFirstEnum.values()) {
            Menu firstMenu = new Menu();
            firstMenu.setId(menuFirstEnum.getId());
            firstMenu.setName(menuFirstEnum.getName());
            firstMenu.setType(menuFirstEnum.getMenuType());
            firstMenu.setSelected(MenuSelected.YES);
            firstMenu.setPath(menuFirstEnum.getPath());
            firstMenu.setIcon(menuFirstEnum.getIcon());
            menuTree.getFirstMenuList().add(firstMenu);
            //获取二级菜单
            for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.getSecondMenuList(firstMenu.getId())) {
                Menu secondMenu = new Menu();
                secondMenu.setId(menuSecondEnum.getId());
                secondMenu.setName(menuSecondEnum.getName());
                secondMenu.setType(menuSecondEnum.getMenuType());
                secondMenu.setSelected(MenuSelected.YES);
                secondMenu.setPath(menuSecondEnum.getPath());
                secondMenu.setIcon(menuSecondEnum.getIcon());
                firstMenu.getChildMenuList().add(secondMenu);
                //获取三级菜单
                for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.getThirdMenuList(secondMenu.getId())) {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId(menuThirdEnum.getId());
                    thirdMenu.setName(menuThirdEnum.getName());
                    thirdMenu.setType(menuThirdEnum.getMenuType());
                    thirdMenu.setSelected(MenuSelected.YES);
                    thirdMenu.setPath(menuThirdEnum.getPath());
                    thirdMenu.setIcon(menuThirdEnum.getIcon());
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        return menuTree;
    }

    /**
     * 生成子账号（经理）的菜单。子账号(经理)：没有系统管理-角色管理，只有系统管理-账号管理-查看、人员管理按钮
     */
    public MenuTree createSubAccountManagerMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();

       /* // 首页 1
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("1");
            firstMenu.setName("首页");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.HOME_PAGE);
            firstMenu.setIcon(MenuIconConstants.DASHBOARD);
            menuTree.getFirstMenuList().add(firstMenu);
        }

        // 库存管理 2
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("2");
            firstMenu.setName("库存管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.STOCK);
            firstMenu.setIcon(MenuIconConstants.DICT);
            menuTree.getFirstMenuList().add(firstMenu);

            // 上传库存 2-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-1");
                secondMenu.setName("上传库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.UPLOAD_STOCK);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 阶梯价格管理 2-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-2");
                secondMenu.setName("阶梯价格管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.STEPPED_PRICE);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增 2-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 修改 2-2-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-2");
                    thirdMenu.setName("修改");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-2-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-3");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 管理库存 2-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-3");
                secondMenu.setName("管理库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MANAGE_STOCK);
                firstMenu.getChildMenuList().add(secondMenu);

                // 显示阶梯价格 2-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-1");
                    thirdMenu.setName("显示阶梯价格");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上/下架 2-3-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-2");
                    thirdMenu.setName("上/下架");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 编辑/阶梯价格配置/删除 2-3-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-3");
                    thirdMenu.setName("编辑/阶梯价格配置/删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 批量导出 2-3-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-4");
                    thirdMenu.setName("批量导出");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 更新库存时间 2-3-5
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-5");
                    thirdMenu.setName("更新库存时间");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势品牌 2-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-4");
                secondMenu.setName("优势品牌");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.ADVANTAGE_BRAND);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/再次发起/申请延期 2-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-1");
                    thirdMenu.setName("单条新增/再次发起/申请延期");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-4-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-2");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消 2-4-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-3");
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上传 2-4-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-4");
                    thirdMenu.setName("上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势型号 2-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-5");
                secondMenu.setName("优势型号");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.ADVANTAGE_MODEL);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/批量新增/再次发起 2-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-1");
                    thirdMenu.setName("单条新增/批量新增/再次发起");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消 2-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-2");
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-5-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-3");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 报价管理 3
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("3");
            firstMenu.setName("报价管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.MANAGE_OFFER);
            firstMenu.setIcon(MenuIconConstants.POST);
            menuTree.getFirstMenuList().add(firstMenu);

            // 实单报价 3-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-1");
                secondMenu.setName("实单报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.ACTUAL_OFFER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价 3-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-1-1");
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 普通报价 3-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-2");
                secondMenu.setName("普通报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.COMMON_OFFER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价 3-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-2-1");
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 搜商机 3-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-3");
                secondMenu.setName("搜商机");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.SOMMER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价 3-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-1");
                    thirdMenu.setName("报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 我的议价 3-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-4");
                secondMenu.setName("我的议价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MY_BRAGINING);
                firstMenu.getChildMenuList().add(secondMenu);

                // 议价 3-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-4-1");
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 订单管理 4
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("4");
            firstMenu.setName("订单管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.ORDER_STOCK);
            firstMenu.setIcon(MenuIconConstants.CHART);
            menuTree.getFirstMenuList().add(firstMenu);

            // 我的订单 4-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("4-1");
                secondMenu.setName("我的订单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MY_ORDER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传 4-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-1");
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 财务管理 5
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("5");
            firstMenu.setName("财务管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.FINANCE);
            firstMenu.setIcon(MenuIconConstants.MONEY);
            menuTree.getFirstMenuList().add(firstMenu);

            // 对账单 5-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-1");
                secondMenu.setName("对账单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.AWAIT_BILL);
                firstMenu.getChildMenuList().add(secondMenu);

                // 确认 5-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-1-1");
                    thirdMenu.setName("确认");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 待开发票 5-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-2");
                secondMenu.setName("待开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.AWAIT_INVOICED);
                firstMenu.getChildMenuList().add(secondMenu);

                // 申请开票 5-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-2-1");
                    thirdMenu.setName("申请开票");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 已开发票 5-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-3");
                secondMenu.setName("已开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.INVOICED);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 合同管理 5-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-4");
                secondMenu.setName("合同管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.CONTRACT_MANAGE);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传 5-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-4-1");
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 系统管理
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("6");
            firstMenu.setName("系统管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.SYSTEM);
            firstMenu.setIcon(MenuIconConstants.BUILD);
            menuTree.getFirstMenuList().add(firstMenu);

            // 账号管理 6-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-1");
                secondMenu.setName("账号管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MANAGE_ACCOUNT);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 系统公告 6-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-3");
                secondMenu.setName("系统公告");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.SYSTEM_NOTICE);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 消息提醒 6-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-4");
                secondMenu.setName("消息提醒");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.NEWS);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 我的公司管理 6-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-5");
                secondMenu.setName("我的公司管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MY_COMPANY);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增 6-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-5-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 停用 6-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-5-2");
                    thirdMenu.setName("停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }*/

        //获取第一级菜单
        for (MenuFirstEnum menuFirstEnum : MenuFirstEnum.values()) {
            Menu firstMenu = new Menu();
            firstMenu.setId(menuFirstEnum.getId());
            firstMenu.setName(menuFirstEnum.getName());
            firstMenu.setType(menuFirstEnum.getMenuType());
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(menuFirstEnum.getPath());
            firstMenu.setIcon(menuFirstEnum.getIcon());
            menuTree.getFirstMenuList().add(firstMenu);
            //获取二级菜单
            for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.getSecondMenuList(firstMenu.getId())) {
                //经理排除角色管理菜单
                if (!menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ROLE_MANAGEMENT.getId())) {
                    Menu secondMenu = new Menu();
                    secondMenu.setId(menuSecondEnum.getId());
                    secondMenu.setName(menuSecondEnum.getName());
                    secondMenu.setType(menuSecondEnum.getMenuType());
                    secondMenu.setSelected(MenuSelected.NO);
                    secondMenu.setPath(menuSecondEnum.getPath());
                    secondMenu.setIcon(menuSecondEnum.getIcon());
                    firstMenu.getChildMenuList().add(secondMenu);
                    //经理只能看列表不能操作
                    if (!menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ACCOUNT_MANAGEMENT.getId())) {
                        //获取三级菜单
                        for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.getThirdMenuList(secondMenu.getId())) {
                            Menu thirdMenu = new Menu();
                            thirdMenu.setId(menuThirdEnum.getId());
                            thirdMenu.setName(menuThirdEnum.getName());
                            thirdMenu.setType(menuThirdEnum.getMenuType());
                            thirdMenu.setSelected(MenuSelected.NO);
                            thirdMenu.setPath(menuThirdEnum.getPath());
                            thirdMenu.setIcon(menuThirdEnum.getIcon());
                            secondMenu.getChildMenuList().add(thirdMenu);
                        }
                    }
                }
            }
        }
        return menuTree;
    }

    /**
     * 子账号(员工)：没有系统管理-账号管理、角色管理
     */
    public MenuTree createSubAccountPersonnelMenuTree() {
        // 根目录
        MenuTree menuTree = new MenuTree();

       /* // 首页 1
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("1");
            firstMenu.setName("首页");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.HOME_PAGE);
            firstMenu.setIcon(MenuIconConstants.DASHBOARD);
            menuTree.getFirstMenuList().add(firstMenu);
        }

        // 库存管理 2
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("2");
            firstMenu.setName("库存管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.STOCK);
            firstMenu.setIcon(MenuIconConstants.DICT);
            menuTree.getFirstMenuList().add(firstMenu);

            // 上传库存 2-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-1");
                secondMenu.setName("上传库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.UPLOAD_STOCK);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 阶梯价格管理 2-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-2");
                secondMenu.setName("阶梯价格管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.STEPPED_PRICE);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增 2-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 修改 2-2-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-2");
                    thirdMenu.setName("修改");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-2-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-2-3");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 管理库存 2-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-3");
                secondMenu.setName("管理库存");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MANAGE_STOCK);
                firstMenu.getChildMenuList().add(secondMenu);

                // 显示阶梯价格 2-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-1");
                    thirdMenu.setName("显示阶梯价格");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上/下架 2-3-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-2");
                    thirdMenu.setName("上/下架");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 编辑/阶梯价格配置/删除 2-3-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-3");
                    thirdMenu.setName("编辑/阶梯价格配置/删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 批量导出 2-3-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-4");
                    thirdMenu.setName("批量导出");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 更新库存时间 2-3-5
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-3-5");
                    thirdMenu.setName("更新库存时间");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势品牌 2-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-4");
                secondMenu.setName("优势品牌");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.ADVANTAGE_BRAND);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/再次发起/申请延期 2-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-1");
                    thirdMenu.setName("单条新增/再次发起/申请延期");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-4-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-2");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消 2-4-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-3");
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 上传 2-4-4
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-4-4");
                    thirdMenu.setName("上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 设置优势型号 2-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("2-5");
                secondMenu.setName("优势型号");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.ADVANTAGE_MODEL);
                firstMenu.getChildMenuList().add(secondMenu);

                // 单条新增/批量新增/再次发起 2-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-1");
                    thirdMenu.setName("单条新增/批量新增/再次发起");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 取消 2-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-2");
                    thirdMenu.setName("取消");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 删除 2-5-3
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("2-5-3");
                    thirdMenu.setName("删除");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 报价管理 3
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("3");
            firstMenu.setName("报价管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.MANAGE_OFFER);
            firstMenu.setIcon(MenuIconConstants.POST);
            menuTree.getFirstMenuList().add(firstMenu);

            // 实单报价 3-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-1");
                secondMenu.setName("实单报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.ACTUAL_OFFER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价 3-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-1-1");
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 普通报价 3-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-2");
                secondMenu.setName("普通报价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.COMMON_OFFER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价/再次报价 3-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-2-1");
                    thirdMenu.setName("报价/再次报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 搜商机 3-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-3");
                secondMenu.setName("搜商机");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.SOMMER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 报价 3-3-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-3-1");
                    thirdMenu.setName("报价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 我的议价 3-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("3-4");
                secondMenu.setName("我的议价");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MY_BRAGINING);
                firstMenu.getChildMenuList().add(secondMenu);

                // 议价 3-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("3-4-1");
                    thirdMenu.setName("议价");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 订单管理 4
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("4");
            firstMenu.setName("订单管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.ORDER_STOCK);
            firstMenu.setIcon(MenuIconConstants.CHART);
            menuTree.getFirstMenuList().add(firstMenu);

            // 我的订单 4-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("4-1");
                secondMenu.setName("我的订单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MY_ORDER);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传 4-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("4-1-1");
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 财务管理 5
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("5");
            firstMenu.setName("财务管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.FINANCE);
            firstMenu.setIcon(MenuIconConstants.MONEY);
            menuTree.getFirstMenuList().add(firstMenu);

            // 对账单 5-1
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-1");
                secondMenu.setName("对账单");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.AWAIT_BILL);
                firstMenu.getChildMenuList().add(secondMenu);

                // 确认 5-1-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-1-1");
                    thirdMenu.setName("确认");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 待开发票 5-2
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-2");
                secondMenu.setName("待开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.AWAIT_INVOICED);
                firstMenu.getChildMenuList().add(secondMenu);

                // 申请开票 5-2-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-2-1");
                    thirdMenu.setName("申请开票");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }

            // 已开发票 5-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-3");
                secondMenu.setName("已开发票");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.INVOICED);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 合同管理 5-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("5-4");
                secondMenu.setName("合同管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.CONTRACT_MANAGE);
                firstMenu.getChildMenuList().add(secondMenu);

                // 下载/上传 5-4-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("5-4-1");
                    thirdMenu.setName("下载/上传");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }

        // 系统管理 6
        {
            Menu firstMenu = new Menu();
            firstMenu.setId("6");
            firstMenu.setName("系统管理");
            firstMenu.setType(MenuType.MENU_ITEM);
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(MenuRouteConstants.SYSTEM);
            firstMenu.setIcon(MenuIconConstants.BUILD);
            menuTree.getFirstMenuList().add(firstMenu);

            // 系统公告 6-3
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-3");
                secondMenu.setName("系统公告");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.SYSTEM_NOTICE);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 消息提醒 6-4
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-4");
                secondMenu.setName("消息提醒");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.NEWS);
                firstMenu.getChildMenuList().add(secondMenu);
            }

            // 我的公司管理 6-5
            {
                Menu secondMenu = new Menu();
                secondMenu.setId("6-5");
                secondMenu.setName("我的公司管理");
                secondMenu.setType(MenuType.MENU_ITEM);
                secondMenu.setSelected(MenuSelected.NO);
                secondMenu.setPath(MenuRouteConstants.MY_COMPANY);
                firstMenu.getChildMenuList().add(secondMenu);

                // 新增 6-5-1
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-5-1");
                    thirdMenu.setName("新增");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }

                // 停用 6-5-2
                {
                    Menu thirdMenu = new Menu();
                    thirdMenu.setId("6-5-2");
                    thirdMenu.setName("停用");
                    thirdMenu.setType(MenuType.BUTTON);
                    thirdMenu.setSelected(MenuSelected.NO);
                    secondMenu.getChildMenuList().add(thirdMenu);
                }
            }
        }*/
        //获取第一级菜单
        for (MenuFirstEnum menuFirstEnum : MenuFirstEnum.values()) {
            Menu firstMenu = new Menu();
            firstMenu.setId(menuFirstEnum.getId());
            firstMenu.setName(menuFirstEnum.getName());
            firstMenu.setType(menuFirstEnum.getMenuType());
            firstMenu.setSelected(MenuSelected.NO);
            firstMenu.setPath(menuFirstEnum.getPath());
            firstMenu.setIcon(menuFirstEnum.getIcon());
            menuTree.getFirstMenuList().add(firstMenu);
            //获取二级菜单
            for (MenuSecondEnum menuSecondEnum : MenuSecondEnum.getSecondMenuList(firstMenu.getId())) {
                //排除角色管理菜单 和 账户管理
                if (!menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ROLE_MANAGEMENT.getId()) &&
                        !menuSecondEnum.getId().equals(MenuSecondEnum.SYSTEM_ACCOUNT_MANAGEMENT.getId())) {
                    Menu secondMenu = new Menu();
                    secondMenu.setId(menuSecondEnum.getId());
                    secondMenu.setName(menuSecondEnum.getName());
                    secondMenu.setType(menuSecondEnum.getMenuType());
                    secondMenu.setSelected(MenuSelected.NO);
                    secondMenu.setPath(menuSecondEnum.getPath());
                    secondMenu.setIcon(menuSecondEnum.getIcon());
                    firstMenu.getChildMenuList().add(secondMenu);

                    //获取三级菜单
                    for (MenuThirdEnum menuThirdEnum : MenuThirdEnum.getThirdMenuList(secondMenu.getId())) {
                        Menu thirdMenu = new Menu();
                        thirdMenu.setId(menuThirdEnum.getId());
                        thirdMenu.setName(menuThirdEnum.getName());
                        thirdMenu.setType(menuThirdEnum.getMenuType());
                        thirdMenu.setSelected(MenuSelected.NO);
                        thirdMenu.setPath(menuThirdEnum.getPath());
                        thirdMenu.setIcon(menuThirdEnum.getIcon());
                        secondMenu.getChildMenuList().add(thirdMenu);
                    }

                }
            }
        }
        return menuTree;
    }
}
