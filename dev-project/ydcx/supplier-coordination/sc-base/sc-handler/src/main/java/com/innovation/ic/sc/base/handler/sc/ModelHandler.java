package com.innovation.ic.sc.base.handler.sc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.b1b.framework.util.HanziToPinyinUtils;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.sc.base.model.sc.*;
import com.innovation.ic.sc.base.pojo.constant.handler.RedisStorage;
import com.innovation.ic.sc.base.pojo.constant.model.RoleAvailableStatus;
import com.innovation.ic.sc.base.pojo.variable.ActionMessagePojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryBatchQueryPojo;
import com.innovation.ic.sc.base.pojo.variable.menu.Menu;
import com.innovation.ic.sc.base.vo.menu.MenuTreeAndUserRoleVo;
import com.innovation.ic.sc.base.vo.menu.MenuTreeVo;
import com.innovation.ic.sc.base.vo.menu.RoleVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * model包中类的处理器
 */
@Component
public class ModelHandler extends SCAbstractHandler {
    private static final Logger logger = LoggerFactory.getLogger(ModelHandler.class);

    /**
     * 将ParentUsersTopView类型对象转换为xml字符串。用于存储到redis
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     * 4.没有password字段。
     *
     * @param user
     * @return
     */
    public String toXmlStorageString(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == user) {
            return null;
        } else {
            if (null != user.getId()) {
                stringBuilder.append("<id>").append(user.getId()).append("</id>");
            }
//            if (null != user.getUserId()) {
//                stringBuilder.append("<userId>").append(user.getUserId()).append("</userId>");
//            }
            if (null != user.getFatherId()) {
                stringBuilder.append("<webSite>").append(user.getFatherId()).append("</webSite>");
            }
            if (null != user.getWebSite()) {
                stringBuilder.append("<fatherId>").append(user.getWebSite()).append("</fatherId>");
            }
            if (null != user.getEnterpriseId()) {
                stringBuilder.append("<enterpriseId>").append(user.getEnterpriseId()).append("</enterpriseId>");
            }
            if (null != user.getEnterpriseName()) {
                stringBuilder.append("<enterpriseName>").append(user.getEnterpriseName()).append("</enterpriseName>");
            }
            if (null != user.getType()) {
                stringBuilder.append("<type>").append(user.getType()).append("</type>");
            }
            if (!StringUtils.isEmpty(user.getUserName())) {
                stringBuilder.append("<userName>").append(user.getUserName()).append("</userName>");
            }
            if (!StringUtils.isEmpty(user.getRealName())) {
                stringBuilder.append("<realName>").append(user.getRealName()).append("</realName>");
            }
            if (null != user.getPosition()) {
                stringBuilder.append("<position>").append(user.getPosition()).append("</position>");
            }
            if (!StringUtils.isEmpty(user.getPhone())) {
                stringBuilder.append("<phone>").append(user.getPhone()).append("</phone>");
            }
            if (!StringUtils.isEmpty(user.getEmail())) {
                stringBuilder.append("<email>").append(user.getEmail()).append("</email>");
            }
            if (null != user.getCreateDate()) {

                stringBuilder.append("<createDate>").append(DateUtils.formatDate(user.getCreateDate(), DateUtils.DEFAULT_FORMAT)).append("</createDate>");
            }
            if (null != user.getModifyDate()) {
                stringBuilder.append("<modifyDate>").append(DateUtils.formatDate(user.getModifyDate(), DateUtils.DEFAULT_FORMAT)).append("</modifyDate>");
            }
            if (null != user.getStatus()) {
                stringBuilder.append("<status>").append(user.getStatus()).append("</status>");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * 将ParentUsersTopView类型对象转换为xml字符串。用于模糊查询
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     * 4.每个元素之间要有*。
     *
     * @param user
     * @return
     */
    public String toXmlFilterString(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == user) {
            return null;
        } else {
            if (!StringUtils.isEmpty(user.getUserName())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<userName>")
                        .append(RedisStorage.ASTERISK).append(user.getUserName())
                        .append(RedisStorage.ASTERISK).append("</userName>");
            }
            if (!StringUtils.isEmpty(user.getRealName())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<realName>")
                        .append(RedisStorage.ASTERISK).append(user.getRealName())
                        .append(RedisStorage.ASTERISK).append("</realName>");
            }
            if (null != user.getPosition()) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<position>")
                        .append(RedisStorage.ASTERISK).append(user.getPosition())
                        .append(RedisStorage.ASTERISK).append("</position>");
            }
            if (!StringUtils.isEmpty(user.getPhone())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<phone>")
                        .append(RedisStorage.ASTERISK).append(user.getPhone())
                        .append(RedisStorage.ASTERISK).append("</phone>");
            }
            if (null != user.getStatus()) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<status>")
                        .append(user.getStatus()).append("</status>");
            }
            stringBuilder.append(RedisStorage.ASTERISK);
            return stringBuilder.toString();
        }
    }

    /**
     * 将JSONObject对象转换为User对象
     *
     * @param jsonObject
     * @return
     */
    public User jsonObjectToUser(JSONObject jsonObject) {
        if (null != jsonObject) {
            User user = new User();
            if (null != jsonObject.getString("id")) {
                user.setId(jsonObject.getString("id"));
            }
//            if (null != jsonObject.getString("user_id")){
//                user.setUserId(jsonObject.getString("user_id"));
//            }
            if (null != jsonObject.getInteger("web_site")) {
                user.setWebSite(jsonObject.getInteger("web_site"));
            }
            if (null != jsonObject.getString("father_id")) {
                user.setFatherId(jsonObject.getString("father_id"));
            }
            if (null != jsonObject.getString("enterprise_id")) {
                user.setEnterpriseId(jsonObject.getString("enterprise_id"));
            }
            if (null != jsonObject.getString("enterprise_name")) {
                user.setEnterpriseName(jsonObject.getString("enterprise_name"));
            }
            if (null != jsonObject.getInteger("type")) {
                user.setType(jsonObject.getInteger("type"));
            }
            if (null != jsonObject.getString("user_name")) {
                user.setUserName(jsonObject.getString("user_name"));
            }
            if (null != jsonObject.getString("password")) {
                user.setPassword(jsonObject.getString("password"));
            }
            if (null != jsonObject.getString("real_name")) {
                user.setRealName(jsonObject.getString("real_name"));
                user.setPinYin(HanziToPinyinUtils.getPinyin(jsonObject.getString("real_name")));
            }
            if (null != jsonObject.getString("position")) {
                user.setPosition(jsonObject.getInteger("position"));
            }
            if (null != jsonObject.getString("phone")) {
                user.setPhone(jsonObject.getString("phone"));
            }
            if (null != jsonObject.getString("email")) {
                user.setEmail(jsonObject.getString("email"));
            }
            if (null != jsonObject.getDate("create_date")) {
                user.setCreateDate(jsonObject.getDate("create_date"));
            }
            if (null != jsonObject.getDate("modify_date")) {
                user.setModifyDate(jsonObject.getDate("modify_date"));
            }
            if (null != jsonObject.getInteger("status")) {
                user.setStatus(jsonObject.getInteger("status"));
            }
            return user;
        } else {
            return null;
        }
    }

    /**
     * 将RoleVo转换为Role类型
     *
     * @param roleVo
     * @param userId
     * @return
     */
    public Role roleVoToRole(RoleVo roleVo, String userId) {
        Role role = new Role();
        if (null != roleVo && !StringUtils.isEmpty(roleVo.getName())) {
            role.setName(roleVo.getName());
        }
        role.setAvailable(RoleAvailableStatus.YES);
        role.setCreateTime(new Date());
        if (!StringUtils.isEmpty(userId)) {
            role.setCreatorId(userId);
        }
        if (null != roleVo && null != roleVo.getMenuVoList()) {
            role.setMenuList(BeanPropertiesUtil.convertList(roleVo.getMenuVoList(), Menu.class));
        }

        return role;
    }


    /**
     * 将MenuTreeVo类型的对象转换为MenuTree类型的对象
     *
     * @param menuTreeVo
     * @param loginUserId
     * @return
     */
    public MenuTree menuTreeVoToMenuTree(MenuTreeVo menuTreeVo, String loginUserId) {
        MenuTree menuTree = new MenuTree();
        if (null != menuTreeVo && StringUtils.validateParameter(menuTreeVo.getUserId())) {
            menuTree.setUserId(menuTreeVo.getUserId());
        }
        menuTree.setCreatorId(loginUserId);
        menuTree.setCreateTime(new Date());
        if (null != menuTreeVo && null != menuTreeVo.getMenuNodeList()) {
            menuTree.setFirstMenuList(BeanPropertiesUtil.convertList(menuTreeVo.getMenuNodeList(), Menu.class));
        }

        return menuTree;
    }

    /**
     * 将MenuTreeAndUserRoleVo类型的对象转换为MenuTree类型的对象
     *
     * @param menuTreeAndUserRoleVo
     * @param loginUserId
     * @return
     */
    public MenuTree menuTreeAndUserRoleVoToMenuTree(MenuTreeAndUserRoleVo menuTreeAndUserRoleVo, String loginUserId) {
        MenuTree menuTree = new MenuTree();
        if (null != menuTreeAndUserRoleVo && StringUtils.validateParameter(menuTreeAndUserRoleVo.getUserId())) {
            menuTree.setUserId(menuTreeAndUserRoleVo.getUserId());
        }
        menuTree.setCreatorId(loginUserId);
        menuTree.setCreateTime(new Date());
        if (null != menuTreeAndUserRoleVo && null != menuTreeAndUserRoleVo.getMenuNodeList()) {
            menuTree.setFirstMenuList(BeanPropertiesUtil.convertList(menuTreeAndUserRoleVo.getMenuNodeList(), Menu.class));
        }
        return menuTree;
    }

    /**
     * 将JSONObject对象转换为AdvantageModel对象
     * @param jsonObject jsonObject
     * @return 返回转换后的对象
     */
    public AdvantageModel jsonObjectToAdvantageModel(JSONObject jsonObject) {
        if (null != jsonObject) {
            AdvantageModel advantageModel = new AdvantageModel();
            if (null != jsonObject.getString("id")) {
                advantageModel.setId(jsonObject.getInteger("id"));
            }
            if (null != jsonObject.getString("type_")) {
                advantageModel.setType(jsonObject.getInteger("type_"));
            }
            if (null != jsonObject.getString("part_number")) {
                advantageModel.setPartNumber(jsonObject.getString("part_number"));
            }
            if (null != jsonObject.getString("brand")) {
                advantageModel.setBrand(jsonObject.getString("brand"));
            }
            if (null != jsonObject.getString("audit_status")) {
                advantageModel.setAuditStatus(jsonObject.getInteger("audit_status"));
            }
            if (null != jsonObject.getString("audit_summary")) {
                advantageModel.setAuditSummary(jsonObject.getString("audit_summary"));
            }
            if (null != jsonObject.getString("advantage_status")) {
                advantageModel.setAdvantageStatus(jsonObject.getInteger("advantage_status"));
            }
            if (null != jsonObject.getString("create_user")) {
                advantageModel.setCreateUser(jsonObject.getString("create_user"));
            }
            if (null != jsonObject.getString("create_user_id")) {
                advantageModel.setCreateUserId(jsonObject.getString("create_user_id"));
            }
            if (null != jsonObject.getString("create_date")) {
                advantageModel.setCreateDate(jsonObject.getDate("create_date"));
            }
            if (null != jsonObject.getString("remark")) {
                advantageModel.setRemark(jsonObject.getString("remark"));
            }
            if (null != jsonObject.getString("enterprise_id")) {
                advantageModel.setEnterpriseId(jsonObject.getString("enterprise_id"));
            }
            if (null != jsonObject.getString("source")) {
                advantageModel.setSource(jsonObject.getInteger("source"));
            }
            if (null != jsonObject.getString("validity_date_start")) {
                advantageModel.setValidityDateStart(jsonObject.getDate("validity_date_start"));
            }
            if (null != jsonObject.getString("validity_date_end")) {
                advantageModel.setValidityDateEnd(jsonObject.getDate("validity_date_end"));
            }
            return advantageModel;
        } else {
            return null;
        }
    }


    /**
     * 将JSONObject对象转换为AdvantageBrand对象
     * @param jsonObject jsonObject
     * @return 返回转换后的对象
     */
    public AdvantageBrand jsonObjectToAdvantageBrand(JSONObject jsonObject) {
        if (null != jsonObject) {
            AdvantageBrand advantageBrand = new AdvantageBrand();
            if (null != jsonObject.getString("id")) {
                advantageBrand.setId(jsonObject.getInteger("id"));
            }
            if (null != jsonObject.getString("dominant_nature")) {
                advantageBrand.setDominantNature(jsonObject.getInteger("dominant_nature"));
            }
            if (null != jsonObject.getString("brand")) {
                advantageBrand.setBrand(jsonObject.getString("brand"));
            }
            if (null != jsonObject.getString("create_user_id")) {
                advantageBrand.setCreateUserId(jsonObject.getString("create_user_id"));
            }
            if (null != jsonObject.getString("create_user")) {
                advantageBrand.setCreateUser(jsonObject.getString("create_user"));
            }
            if (null != jsonObject.getString("create_date")) {
                advantageBrand.setCreateDate(jsonObject.getDate("create_date"));
            }
            if (null != jsonObject.getString("audit_status")) {
                advantageBrand.setAuditStatus(jsonObject.getInteger("audit_status"));
            }
            if (null != jsonObject.getString("advantage_status")) {
                advantageBrand.setAdvantageStatus(jsonObject.getInteger("advantage_status"));
            }
            if (null != jsonObject.getString("certificate_validity_end")) {
                advantageBrand.setCertificateValidityEnd(jsonObject.getDate("certificate_validity_end"));
            }
            if (null != jsonObject.getString("certificate_validity_start")) {
                advantageBrand.setCertificateValidityStart(jsonObject.getDate("certificate_validity_start"));
            }
            if (null != jsonObject.getString("remark")) {
                advantageBrand.setRemark(jsonObject.getString("remark"));
            }
            if (null != jsonObject.getString("enterprise_id")) {
                advantageBrand.setEnterpriseId(jsonObject.getString("enterprise_id"));
            }
            if (null != jsonObject.getString("erp_id")) {
                advantageBrand.setErpId(jsonObject.getString("erp_id"));
            }
            if (null != jsonObject.getString("audit_reason")) {
                advantageBrand.setAuditReason(jsonObject.getString("audit_reason"));
            }
            return advantageBrand;
        } else {
            return null;
        }
    }

    /**
     * 将JSONObject对象转换为Inventory对象
     * @param jsonObject jsonObject
     * @return 返回转换后的对象
     */
    public Inventory jsonObjectToInventory(JSONObject jsonObject) {
        if (null != jsonObject) {
            Inventory inventory = new Inventory();
            if (null != jsonObject.getString("id")) {
                inventory.setId(jsonObject.getInteger("id"));
            }
            if (null != jsonObject.getString("part_number")) {
                inventory.setPartNumber(jsonObject.getString("part_number"));
            }
            if (null != jsonObject.getString("brand")) {
                inventory.setBrand(jsonObject.getString("brand"));
            }
            if (null != jsonObject.getString("count")) {
                inventory.setCount(jsonObject.getInteger("count"));
            }
            if (null != jsonObject.getString("package")) {
                inventory.setPackages(jsonObject.getString("package"));
            }
            if (null != jsonObject.getString("batch")) {
                inventory.setBatch(jsonObject.getString("batch"));
            }
            if (null != jsonObject.getString("packing")) {
                inventory.setPacking(jsonObject.getInteger("packing"));
            }
            if (null != jsonObject.getString("moq")) {
                inventory.setMoq(jsonObject.getInteger("moq"));
            }
            if (null != jsonObject.getString("mpq")) {
                inventory.setMpq(jsonObject.getInteger("mpq"));
            }
            if (null != jsonObject.getString("currency")) {
                inventory.setCurrency(jsonObject.getInteger("currency"));
            }
            if (null != jsonObject.getString("inventory_home")) {
                inventory.setInventoryHome(jsonObject.getInteger("inventory_home"));
            }
            if (null != jsonObject.getString("unit_price_type")) {
                inventory.setUnitPriceType(jsonObject.getInteger("unit_price_type"));
            }
            if (null != jsonObject.getString("create_user")) {
                inventory.setCreateUser(jsonObject.getString("create_user"));
            }
            if (null != jsonObject.getString("create_user_id")) {
                inventory.setCreateUserId(jsonObject.getString("create_user_id"));
            }
            if (null != jsonObject.getString("create_date")) {
                inventory.setCreateDate(jsonObject.getDate("create_date"));
            }
            if (null != jsonObject.getString("modify_date")) {
                inventory.setModifyDate(jsonObject.getDate("modify_date"));
            }
            if (null != jsonObject.getString("status")) {
                inventory.setStatus(jsonObject.getInteger("status"));
            }
            if (null != jsonObject.getString("enterprise_id")) {
                inventory.setEnterpriseId(jsonObject.getString("enterprise_id"));
            }
            if (null != jsonObject.getString("unit_price")) {
                inventory.setUnitPrice(jsonObject.getBigDecimal("unit_price"));
            }
            if (null != jsonObject.getString("ladder_price_id")) {
                inventory.setLadderPriceId(jsonObject.getInteger("ladder_price_id"));
            }
            /*if (null != jsonObject.getString("bzk_match_status")) {
                inventory.setBzkMatchStatus(jsonObject.getInteger("bzk_match_status"));
            }
            if (null != jsonObject.getString("bzk_part_number")) {
                inventory.setBzkPartNumber(jsonObject.getString("bzk_part_number"));
            }
            if (null != jsonObject.getString("bzk_brand")) {
                inventory.setBzkBrand(jsonObject.getString("bzk_brand"));
            }*/
            return inventory;
        } else {
            return null;
        }
    }

    /**
     * 将JSONObject对象转换为PersonnelManagement对象
     *
     * @param jsonObject jsonObject
     * @return 返回转换后的对象
     */
    public PersonnelManagement jsonObjectToPersonnelManagement(JSONObject jsonObject) {
        if (null != jsonObject) {
            PersonnelManagement personnelManagement = new PersonnelManagement();
            if (null != jsonObject.getString("id")) {
                personnelManagement.setId(jsonObject.getInteger("id"));
            }
            if (null != jsonObject.getString("user_id")) {
                personnelManagement.setUserId(jsonObject.getString("user_id"));
            }
            if (null != jsonObject.getString("managed_user_id")) {
                personnelManagement.setManagedUserId(jsonObject.getString("managed_user_id"));
            }
            return personnelManagement;
        } else {
            return null;
        }
    }


    /**
     * 将Brands对象转换为xml字符串。用于存储到redis
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     *
     * @param brand
     * @return
     */
    public String brandToXmlStorageString(Brand brand) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == brand) {
            return null;
        } else {
            if (StringUtils.isNotEmpty(brand.getId())) {
                stringBuilder.append("<id>").append(brand.getId()).append("</id>");
            }
            if (StringUtils.isNotEmpty(brand.getName())) {
                stringBuilder.append("<name>").append(brand.getName()).append("</name>");
            }
            if (StringUtils.isNotEmpty(brand.getCnName())) {
                stringBuilder.append("<cnName>").append(brand.getCnName()).append("</cnName>");
            }

            if (StringUtils.isNotEmpty(brand.getEnName())) {
                stringBuilder.append("<enName>").append(brand.getEnName()).append("</enName>");
            }

            if (StringUtils.isNotEmpty(brand.getShortName())) {
                stringBuilder.append("<shortName>").append(brand.getShortName()).append("</shortName>");
            }

            if (StringUtils.isNotEmpty(brand.getLetter())) {
                stringBuilder.append("<letter>").append(brand.getLetter()).append("</letter>");
            }

            if (StringUtils.isNotEmpty(brand.getWebSite())) {
                stringBuilder.append("<webSite>").append(brand.getWebSite()).append("</webSite>");
            }

            if (StringUtils.isNotEmpty(brand.getManufacture())) {
                stringBuilder.append("<manufacture>").append(brand.getManufacture()).append("</manufacture>");
            }

            if (null != brand.getIsAgent()) {
                stringBuilder.append("<isAgent>").append(brand.getIsAgent()).append("</isAgent>");
            }
            if (StringUtils.isNotEmpty(brand.getSummary())) {
                stringBuilder.append("<summary>").append(brand.getSummary()).append("</summary>");
            }

            if (StringUtils.isNotEmpty(brand.getImage())) {
                stringBuilder.append("<image>").append(brand.getImage()).append("</image>");
            }

            if (null != brand.getCreateDate()) {
                stringBuilder.append("<createDate>").append(DateUtils.formatDate(brand.getCreateDate(), DateUtils.DEFAULT_FORMAT)).append("</createDate>");
            }
            if (null != brand.getModifyDate()) {
                stringBuilder.append("<modifyDate>").append(DateUtils.formatDate(brand.getModifyDate(), DateUtils.DEFAULT_FORMAT)).append("</modifyDate>");
            }
            if (StringUtils.isNotEmpty(brand.getCreatorId())) {
                stringBuilder.append("<creatorId>").append(brand.getCreatorId()).append("</creatorId>");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 将brand对象转换为xml字符串。用于模糊查询
     * 注意：
     * 1.没有头部。
     * 2.没有根元素。
     * 3.元素之间顺序不能错。
     * 4.每个元素之间要有*。
     *
     * @param brand
     * @return
     */
    public String brandToXmlFilterString(Brand brand, Boolean allKey) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null == brand) {
            return null;
        } else {
            if (StringUtils.isNotEmpty(brand.getId())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<id>");
                stringBuilder.append(brand.getId()).append("</id>");
            }
            if (StringUtils.isNotEmpty(brand.getName())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<name>");
                //判断是否需全匹配查询
                if (allKey) {
                    stringBuilder.append(RedisStorage.ASTERISK);
                }
                stringBuilder.append(brand.getName()).append(RedisStorage.ASTERISK).append("</name>");
            }
            if (StringUtils.isNotEmpty(brand.getCnName())) {
                stringBuilder.append(RedisStorage.ASTERISK).append("<cnName>");
                stringBuilder.append(brand.getCnName()).append(RedisStorage.ASTERISK).append("</cnName>");
            }
            stringBuilder.append(RedisStorage.ASTERISK);
            return stringBuilder.toString();
        }
    }


    /**
     * 将JSONObject对象转换为brand对象
     *
     * @param jsonObject
     * @return
     */
    public Brand jsonObjectToBrand(JSONObject jsonObject) {
        if (null != jsonObject) {
            Brand brand = new Brand();
            if (StringUtils.isNotEmpty(jsonObject.getString("id"))) {
                brand.setId(jsonObject.getString("id"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("name"))) {
                brand.setName(jsonObject.getString("name"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("cn_name"))) {
                brand.setCnName(jsonObject.getString("cn_name"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("en_name"))) {
                brand.setEnName(jsonObject.getString("en_name"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("short_name"))) {
                brand.setShortName(jsonObject.getString("short_name"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("letter"))) {
                brand.setLetter(jsonObject.getString("letter"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("web_site"))) {
                brand.setWebSite(jsonObject.getString("web_site"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("manufacture"))) {
                brand.setManufacture(jsonObject.getString("manufacture"));
            }

            if (null != jsonObject.getInteger("is_agent")) {
                brand.setIsAgent(jsonObject.getInteger("is_agent"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("summary"))) {
                brand.setSummary(jsonObject.getString("summary"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("image"))) {
                brand.setImage(jsonObject.getString("image"));
            }

            if (null != jsonObject.getDate("create_date")) {
                brand.setCreateDate(jsonObject.getDate("create_date"));
            }

            if (null != jsonObject.getDate("modify_date")) {
                brand.setModifyDate(jsonObject.getDate("modify_date"));
            }

            if (StringUtils.isNotEmpty(jsonObject.getString("creator_id"))) {
                brand.setCreatorId(jsonObject.getString("creator_id"));
            }
            return brand;
        } else {
            return null;
        }
    }

    /**
     * 将Set<String>类型对象转换为List<InventoryBatchQueryPojo>类型对象
     * @param set 数据
     * @return 返回处理后的list
     */
    public List<InventoryBatchQueryPojo> toInventoryList(Set<String> set) {
        List<InventoryBatchQueryPojo> dataList = new ArrayList<>();
        for (String str : set) {
            JSONObject jsonObject = JSON.parseObject(str);
            InventoryBatchQueryPojo data = JSON.parseObject(String.valueOf(jsonObject), InventoryBatchQueryPojo.class);
            dataList.add(data);
        }
        return dataList;
    }

    /**
     * 将Set<String>类型转换为List<ActionMessagePojo>类型
     * @param stringSet
     * @return
     */
    public List<ActionMessagePojo> stringSetToActionMessagePojoList(Set<String> stringSet){
        List<ActionMessagePojo> actionMessagePojoList = new ArrayList<>();
        Iterator<String> iterator = stringSet.iterator();
        while (iterator.hasNext()){
            String str = iterator.next();
            ActionMessagePojo actionMessagePojo = JSONObject.parseObject(str, ActionMessagePojo.class);
            actionMessagePojoList.add(actionMessagePojo);
        }
        return actionMessagePojoList;
    }
}