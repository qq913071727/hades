package com.innovation.ic.sc.base.handler.sc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.HttpUtils;
import com.innovation.ic.sc.base.pojo.constant.CodeConstants;
import com.innovation.ic.sc.base.pojo.constant.Constants;
import com.innovation.ic.sc.base.pojo.constant.LoginConstants;
import com.innovation.ic.sc.base.pojo.constant.third_party_api.WechatConstants;
import com.innovation.ic.sc.base.pojo.enums.LoginTypeEnum;
import com.innovation.ic.sc.base.pojo.variable.AuthenticationUser;
import org.apache.http.HttpStatus;

/**
 * @desc   ERP相关工具类
 * @author linuo
 * @time   2022年8月18日11:06:09
 */
public class ErpHandler {
    /**
     * 通过临时授权码登录
     * @param loginUrl 登录地址url
     * @param authCode 临时授权码
     * @param type 登录类型(0 微信、1 QQ、3 支付宝)
     * @return 返回登录结果
     */
    public static JSONObject loginByAuthCode(String loginUrl, String authCode, Integer type){
        JSONObject jsonObject = null;
        JSONObject param = new JSONObject();
        param.put(LoginConstants.TYPE, type);
        param.put(LoginConstants.CODE, authCode);
        String result = HttpUtils.sendPost(loginUrl, param.toString());
        if(!Strings.isNullOrEmpty(result)) {
            JSONObject json = (JSONObject) JSONObject.parse(result);
            Integer status = json.getInteger(LoginConstants.STATUS_FIELD);
            // 成功
            if(status == HttpStatus.SC_OK){
                jsonObject = new JSONObject();
                JSONObject data = (JSONObject) json.get(LoginConstants.DATA);
                if (data != null) {
                    String token = (String) data.get(LoginConstants.TOKEN);
                    // 将token对象转换为接收用户信息vo类对象
                    AuthenticationUser authenticationUser = AuthenticationUserHandler.toAuthenticationUser(token);
                    if(authenticationUser != null){
                        jsonObject.put(Constants.RESULT, JSON.toJSON(authenticationUser));
                    }
                    jsonObject.put(LoginConstants.STATUS_FIELD, status);
                }
                return jsonObject;
            }else{
                // 失败
                jsonObject = new JSONObject();
                jsonObject.put(LoginConstants.STATUS_FIELD, status);

                // 首次登录时做特殊处理
                if(status == CodeConstants.UN_BOUNDED){
                    jsonObject.put(LoginConstants.MESSAGE, "当前用户首次使用" + LoginTypeEnum.getDesc(type) + "扫码登录,需绑定账号");

                    JSONObject data = (JSONObject) json.get(LoginConstants.DATA);
                    if(data != null){
                        // 用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。
                        String unionId = data.getString(WechatConstants.UNION_ID_FIELD);
                        if(!Strings.isNullOrEmpty(unionId)){
                            jsonObject.put(LoginConstants.UNION_ID, unionId);
                        }

                        String openId = data.getString(WechatConstants.OPEN_ID_FIELD);
                        if(!Strings.isNullOrEmpty(openId)){
                            jsonObject.put(LoginConstants.OPEN_ID, openId);
                        }
                    }
                }

                // 登录失败时返回描述信息
                if(status == HttpStatus.SC_INTERNAL_SERVER_ERROR){
                    jsonObject.put(LoginConstants.MESSAGE, "登录失败,请重试");
                }

                return jsonObject;
            }
        }
        return jsonObject;
    }
}