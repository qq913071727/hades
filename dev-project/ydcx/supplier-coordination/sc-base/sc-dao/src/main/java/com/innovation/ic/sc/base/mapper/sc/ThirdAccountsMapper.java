package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.ThirdAccounts;
import org.springframework.stereotype.Repository;

/**
 * @desc   ThirdAccounts表的mapper类
 * @author linuo
 * @time   2022年8月16日16:31:09
 */
@Repository
public interface ThirdAccountsMapper extends EasyBaseMapper<ThirdAccounts> {

}