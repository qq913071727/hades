package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.LadderPriceInfo;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.LadderPriceBasicInfoRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   LadderPriceInfo表的mapper类
 * @author linuo
 * @time   2022年8月23日11:18:33
 */
@Repository
public interface LadderPriceInfoMapper extends EasyBaseMapper<LadderPriceInfo> {
    /**
     * 查询全部阶梯价格配置信息
     * @param epId 供应商id
     * @return 返回查询结果
     */
    List<LadderPriceBasicInfoRespPojo> queryAllLadderPriceInfo(@Param("epId") String epId);
}