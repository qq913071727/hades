package com.innovation.ic.sc.base.mapper.erp9_pvecrm;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.SpecialsSCTopView;
import org.springframework.stereotype.Repository;

/**
 * @desc   SpecialsSCTopView表的mapper类
 * @author linuo
 * @time   2022年8月29日14:43:29
 */
@Repository
public interface SpecialsSCTopViewMapper extends EasyBaseMapper<SpecialsSCTopView> {

}