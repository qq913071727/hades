package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.Dictionaries;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 字典
 */
@Repository
public interface DictionariesMapper extends EasyBaseMapper<Dictionaries> {

    List<Dictionaries> findByDcode(@Param("codes") List<String> paramList);
}