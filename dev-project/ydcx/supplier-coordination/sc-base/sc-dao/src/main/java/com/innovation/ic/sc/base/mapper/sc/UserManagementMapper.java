package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;

import com.innovation.ic.sc.base.model.sc.UserManagement;
import org.springframework.stereotype.Repository;

@Repository
public interface UserManagementMapper extends EasyBaseMapper<UserManagement> {
}
