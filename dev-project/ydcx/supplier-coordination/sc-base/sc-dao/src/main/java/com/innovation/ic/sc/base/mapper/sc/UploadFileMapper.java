package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.UploadFile;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 上传文件
 */
@Repository
public interface UploadFileMapper extends EasyBaseMapper<UploadFile> {

    void updateRelationId(@Param("ids") List<String> ids, @Param("relationId")String relationId,@Param("model")String model);


    void updateRelationIdAndType(@Param("id") String id, @Param("relationId")String relationId,@Param("model")String model,@Param("type") Integer type);
}