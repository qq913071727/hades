package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMapper extends EasyBaseMapper<Role> {

}
