package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.ActionMessage;
import com.innovation.ic.sc.base.pojo.variable.ActionMessagePojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionMessageMapper extends EasyBaseMapper<ActionMessage> {


    List<ActionMessagePojo> pageInfo(String toUserId);

    int getTotalOfRedis(String id);
}
