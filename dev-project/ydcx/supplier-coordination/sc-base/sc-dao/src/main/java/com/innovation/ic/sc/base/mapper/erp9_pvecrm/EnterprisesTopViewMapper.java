package com.innovation.ic.sc.base.mapper.erp9_pvecrm;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.EnterprisesTopView;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @desc   SpecialsSCTopView表的mapper类
 * @author linuo
 * @time   2022年8月29日14:43:29
 */
@Repository
public interface EnterprisesTopViewMapper extends EasyBaseMapper<EnterprisesTopView> {


    Integer countEnterprises();


    List<EnterprisesTopView> findByEnterprisesTopView(@Param("offset") Integer offset, @Param("rows")Integer rows);

}