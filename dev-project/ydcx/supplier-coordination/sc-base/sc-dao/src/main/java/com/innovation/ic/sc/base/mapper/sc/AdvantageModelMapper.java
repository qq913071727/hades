package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.AdvantageModel;
import com.innovation.ic.sc.base.pojo.variable.advantageModel.AdvantageModelGroupByUserPojo;
import com.innovation.ic.sc.base.pojo.variable.advantageModel.AdvantageModelRespPojo;
import com.innovation.ic.sc.base.vo.advantageModel.AdvantageModelCheckVo;
import com.innovation.ic.sc.base.vo.advantageModel.AdvantageModelRequestVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
 * @desc   AdvantageModel表的mapper类
 * @author linuo
 * @time   2022年8月29日13:53:43
 */
@Repository
public interface AdvantageModelMapper extends EasyBaseMapper<AdvantageModel> {
    /**
     * 清空表SpecialsSCTopView中的全部数据
     */
    void truncateTable();

    /**
     * 更新优势型号的优势状态
     * @param id 更新优势型号优势状态的主键id
     * @param advantageStatus 优势状态(0:无效、1:有效)
     * @return 返回操作结果
     */
    int updateAdvantageModelStatus(@Param("id") Integer id, @Param("advantageStatus") Integer advantageStatus);

    /**
     * 优势型号信息列表查询
     * @param advantageModelRequestVo 优势型号信息列表查询的请求Vo类
     * @param enterpriseId 供应商id
     * @return 返回优势型号信息
     */
    List<AdvantageModelRespPojo> query(@Param("advantageModelRequestVo") AdvantageModelRequestVo advantageModelRequestVo, @Param("enterpriseId") String enterpriseId);

    /**
     * 优势型号信息数据数量查询
     * @param advantageModelRequestVo 优势型号信息列表查询的请求Vo类
     * @param enterpriseId 供应商id
     * @return 返回优势型号信息
     */
    Long queryTotal(@Param("advantageModelRequestVo") AdvantageModelRequestVo advantageModelRequestVo, @Param("enterpriseId") String enterpriseId);

    /**
     * 查询拒绝原因
     * @param id 主键id
     * @return 返回拒绝原因
     */
    String queryRefusedReason(@Param("id") String id);

    /**
     * 更新优势型号的审核状态
     * @param id 更新优势型号审核状态的主键id
     * @param auditStatus 审核状态(0:待审核、1:审核通过、2:审核拒绝)
     * @return 返回操作结果
     */
    int updateAuditStatus(@Param("id") Integer id, @Param("auditStatus") Integer auditStatus);

    /**
     * 更新优势型号信息
     * @param advantageModel 优势型号信息
     * @return 返回更新结果
     */
    int updateAdvantageModel(@Param("advantageModel") AdvantageModel advantageModel);

    /**
     * 根据品牌、型号查询优势型号数量
     * @param advantageModelCheckVo 优势型号校验的Vo类
     * @param epId 供应商id
     * @return 返回查询结果
     */
    int selectAdvantageModelCount(@Param("advantageModelCheckVo") AdvantageModelCheckVo advantageModelCheckVo, @Param("epId") String epId);

    /**
     * 根据创建用户分组查询优势型号数据
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<AdvantageModelGroupByUserPojo> selecAdvantageModeltListGroupByUser(@Param("map") Map<String, Object> map);

    /**
     * 根据查询条件获取需要取消优势型号的数据id
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<Integer> selectCancleDataIdList(@Param("map") Map<String, String> map);

    /**
     * 根据查询条件获取需要发起审核的数据id
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<Integer> selectAgainInitiateIdList(@Param("map") Map<String, String> map);

    /**
     * 根据查询条件获取需要删除优势型号的数据id
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<Integer> selectIdListByParam(@Param("map") Map<String, String> map);

    /**
     * 更新优势型号生效状态
     * @param map 参数
     * @return 返回更新结果
     */
    int updateStatusByParam(@Param("map") Map<String, Object> map);

    /**
     * 查询优势型号表中的所有供应商id
     * @return 返回查询结果
     */
    List<String> selectEnterpriseIdList();

    /**
     * 查询当前供应商下的所有型号数据
     * @param enterpriseId 供应商id
     * @return 返回查询结果
     */
    List<String> selectPartNumberListByEnterpriseId(@Param("enterpriseId") String enterpriseId);
}