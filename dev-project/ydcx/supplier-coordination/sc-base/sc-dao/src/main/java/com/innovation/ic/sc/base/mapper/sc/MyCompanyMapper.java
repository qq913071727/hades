package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.MyCompany;
import com.innovation.ic.sc.base.pojo.variable.MyCompanyPojo;
import com.innovation.ic.sc.base.vo.RelationMyCompanyNameVo;
import com.innovation.ic.sc.base.vo.SearchCompanyVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 上传文件
 */
@Repository
public interface MyCompanyMapper extends EasyBaseMapper<MyCompany> {

    void insertRetuenId(MyCompany myCompany);

    MyCompany findByName(@Param("name") String name);

    List<MyCompanyPojo> findByMyCompanyPage(SearchCompanyVo searchCompanyVo);

    Integer checkRegistrationCompanyId(RelationMyCompanyNameVo relationMyCompanyNameVo);

    MyCompany findByEnterpriseId(@Param("enterpriseId")String enterpriseId);

    List<MyCompany> findByMyCompanyList(SearchCompanyVo searchCompanyVo);

    /**
     * 根据creator_id查找我的公司名称列表
     * @param epId
     * @return
     */
    List<String> findByEpId(@Param("epId") String epId);
}