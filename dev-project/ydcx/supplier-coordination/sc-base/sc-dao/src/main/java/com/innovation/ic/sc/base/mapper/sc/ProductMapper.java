package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.Product;

/**
 * @author zengqinglong
 * @desc 产品表
 * @Date 2022/11/18 13:19
 **/
public interface ProductMapper extends EasyBaseMapper<Product> {
}
