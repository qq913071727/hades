package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.LadderPriceAmountConfig;
import org.springframework.stereotype.Repository;

/**
 * @desc   LadderPriceAmountConfig表的mapper类
 * @author linuo
 * @time   2023年4月14日09:48:31
 */
@Repository
public interface LadderPriceAmountConfigMapper extends EasyBaseMapper<LadderPriceAmountConfig> {

}