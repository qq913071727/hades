package com.innovation.ic.sc.base.mapper.erp9_pvestandard;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.sc.base.model.erp9_pvestandard.Products;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import com.innovation.ic.sc.base.pojo.variable.product.ProductInfoPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @Description: 产品型号
 * @ClassName: ProductMapper
 * @Author: myq
 * @Date: 2022/8/5 下午2:13
 * @Version: 1.0
 */
@Repository
public interface ProductsMapper extends BaseMapper<Products> {
    /**
     * @description 查询所有产品型号
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/5 下午2:16
     */
    List<Products> selectProducts(List<String> productIds);

    /**
     * 根据型号、品牌查询产品信息
     * @param brand 型号
     * @param partNumber 品牌
     * @return 返回查询结果
     */
    List<ProductInfoPojo> selectProduceInfoByBrandPartNumber(@Param("brand") String brand, @Param("partNumber") String partNumber);

    /**
     * 模糊查询标准库型号
     * @param partNumber 型号
     * @return 返回查询结果
     */
    List<InventoryBlurQueryBzkPartNumberListRespPojo> blurQueryBzkPartNumberList(@Param("partNumber") String partNumber);
}