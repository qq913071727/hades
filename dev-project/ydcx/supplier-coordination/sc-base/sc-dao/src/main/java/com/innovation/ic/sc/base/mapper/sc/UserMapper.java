package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.ParentUsersTopView;
import com.innovation.ic.sc.base.model.sc.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface UserMapper extends EasyBaseMapper<User> {
    /**
     * 通过size和page以及其他参数 去分页查询账号数据
     *
     * @param pageSize
     * @param pageNo
     * @return
     */
    List<User> findUserPage(@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo, @Param("userName") String userName, @Param("realName") String realName, @Param("position") Integer position, @Param("phone") String phone, @Param("status") Integer status);

    /**
     * 把list中sql server中的数据 同步的到mysql user表中
     *
     * @param userList
     * @return
     */
    Integer insertUserList(@Param("userList") List<ParentUsersTopView> userList);

    /**
     * 用于erp定时任务查询子账号数据
     */
    List<User> selectSonUserPage(@Param("pageNo")Integer pageNo, @Param("pageSize")Integer pageSize,  @Param("newTime")Date newTime, @Param("date")Date date);

    /**
     * 通过size和page以及其他参数 去分页查询子账号数据
     *
     * @param pageSize
     * @param pageNo
     * @param userName
     * @param realName
     * @param position
     * @param phone
     * @param status
     * @param id
     * @return
     */
    List<User> findSonUserPage(@Param("pageSize") Integer pageSize, @Param("pageNo") Integer pageNo, @Param("userName") String userName, @Param("realName") String realName, @Param("position") Integer position, @Param("phone") String phone, @Param("status") Integer status,@Param("id") String id);

    /**
     * 获取所有主账号数据
     * @return 返回查询结果
     */
    List<User> getAllPrimaryAccountData();

    List<String> findByEnterpriseId(@Param("enterpriseID")String enterpriseID);

    /**
     * 更新账号评分
     * @param id 主账号id
     * @param score 评分
     * @return 返回更新结果
     */
    int updateScore(@Param("id") String id, @Param("score") BigDecimal score);

    /**
     * 查询账号评分
     * @param id 账号id
     * @return 返回查询结果
     */
    Float getScoreById(@Param("id") String id);

    /**
     * 通过id修改日期时间和状态
     * @param time 日期时间
     * @param status 账号状态
     * @param ids 账号ids
     */
    void updateStatus(@Param("time") String time, @Param("status") Integer status, @Param("ids") String[] ids);

    /**
     * 通过id查询数据
     * @param ids 账号ids
     * @return 返回查询结果
     */
    List<User> selectUpdateStatus(@Param("ids") String[] ids);

    /**
     * 根据手机号查询账号
     * @param phoneNumber 手机号
     * @return 返回查询结果
     */
    List<String> selectAccountByPhoneNumber(@Param("phoneNumber") String phoneNumber);
}