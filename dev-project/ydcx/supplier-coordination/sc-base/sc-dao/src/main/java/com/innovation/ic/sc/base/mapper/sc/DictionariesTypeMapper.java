package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.DictionariesType;
import org.springframework.stereotype.Repository;

/***
 * 字典类型
 */
@Repository
public interface DictionariesTypeMapper extends EasyBaseMapper<DictionariesType> {

}