package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.DefaultLadderPrice;
import org.springframework.stereotype.Repository;

/**
 * @desc   DefaultLadderPrice表的mapper类
 * @author linuo
 * @time   2022年8月23日11:34:33
 */
@Repository
public interface DefaultLadderPriceMapper extends EasyBaseMapper<DefaultLadderPrice> {

}