package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryAllSituationPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryBatchQueryPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryGetListRespPojo;
import com.innovation.ic.sc.base.pojo.variable.inventory.InventoryUnMatchData;
import com.innovation.ic.sc.base.vo.BrandsPageVo;
import com.innovation.ic.sc.base.vo.DefaultQueryVo;
import com.innovation.ic.sc.base.vo.advantageModel.AdvantageModelCheckVo;
import com.innovation.ic.sc.base.vo.inventory.InventoryQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @desc   Inventory表的mapper类
 * @author linuo
 * @time   2022年8月23日09:48:26
 */
@Repository
public interface InventoryMapper extends EasyBaseMapper<Inventory> {

    int updateInventory(@Param("id") Integer id, @Param("batch") String batch);

    /**
     * 库存信息列表查询
     * @return 返回 库存信息列表信息
     */
    List<InventoryBatchQueryPojo> query(@Param("inventoryQueryVo") InventoryQueryVo inventoryQueryVo, @Param("epId") String epId);

    /**
     * 获取库存数据总条数
     * @param inventoryQueryVo 库存信息列表查询接口的Vo类
     * @param epId 供应商id
     * @return 返回库存数据总条数
     */
    Long queryCount(@Param("inventoryQueryVo") InventoryQueryVo inventoryQueryVo, @Param("epId") String epId);

    /**
     * 查询当前供应商去重后的库存型号
     * @param epId 供应商id
     * @return 返回当前供应商去重后的库存型号
     */
    List<Inventory> selectDistinctInventoryList(@Param("epId") String epId);

    /**
     * 查询库存表中包含当前品牌、型号数据数量
     * @param advantageModelCheckVo 优势型号校验的Vo类
     * @param epId 供应商id
     * @return 返回查询结果
     */
    Long selectInventoryCount(@Param("advantageModelCheckVo") AdvantageModelCheckVo advantageModelCheckVo, @Param("epId") String epId);

    /**
     * 根据主键id更新阶梯价格id数据
     * @param ladderPriceId 阶梯价格id
     * @param id 主键id
     * @return 返回更新结果
     */
    int updateLadderPriceIdById(@Param("ladderPriceId") Integer ladderPriceId, @Param("id") Integer id);

    /**
     * 获取库存数据总条数
     * @param epId 供应商id
     * @return 返回库存数据总条数
     */
    Long selectInventoryCountByEpId(@Param("epId") String epId);

    /**
     * 查询使用了阶梯价格id的库存信息的数量
     * @param ladderPriceId 阶梯价格id
     * @param epId 供应商id
     * @return 返回查询结果
     */
    Long selectUseLadderPriceIdInventoryCount(@Param("ladderPriceId") Integer ladderPriceId, @Param("epId") String epId);

    /**
     * 根据型号查询所属品牌
     * @param partNumber 型号
     * @return 返回查询结果
     */
    List<String> queryBrandsByPartNumber(@Param("partNumber") String partNumber, @Param("epId") String epId);

    /**
     * 根据查询条件查询数据数量
     * @param param 查询条件
     * @return 返回查询结果
     */
    int selectCountByParam(@Param("param") Map<String, Object> param);

    /**
     * 根据参数查询需要删除的id集合
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<Integer> selectDeleteDataIdsByParam(@Param("param") Map<String, Object> map);

    /**
     * 根据条件查询库存id
     * @param map 查询条件
     * @return 返回查询结果
     */
    Integer selectIdByParam(@Param("param") Map<String, Object> map);

    /**
     * 根据id更新数量
     * @param count 数量
     * @param id 库存id
     */
    Integer updateCountById(@Param("count") Integer count, @Param("id") Integer id);

    /**
     * 将阶梯价格id字段置为空
     * @param id 库存id
     * @return 返回修改结果
     */
    int setLadderPriceIdNullById(@Param("id") Integer id);

    /**
     * 根据id集合查询库存数据
     * @param ids id集合
     * @return 返回查询结果
     */
    List<InventoryGetListRespPojo> selectInventoryByIds(@Param("ids") List<Integer> ids);

    List<Inventory> findBrand(BrandsPageVo brandsPageVo);

    /**
     * 未匹配成功数据列表查询
     * @param defaultQueryVo 列表查询接口的默认Vo类
     * @param epId 供应商id
     * @return 返回查询结果
     */
    List<InventoryUnMatchData> queryUnMatchData(@Param("defaultQueryVo") DefaultQueryVo defaultQueryVo, @Param("epId") String epId);

    /**
     * 查询未匹配成功数据数量
     * @param epId 供应商id
     * @return 返回查询结果
     */
    Integer selectUnMatchDataCount(@Param("epId") String epId);

    /**
     * 查询当前供应商全部匹配上标准库的数据id集合
     * @param epId 供应商id
     * @return 返回查询结果
     */
    List<Integer> getMatchStandardLibraryInventoryIds(@Param("epId") String epId);

    /**
     * 判断dataList中的品牌和型号是否有历史数据使用
     * @param bzkBrand 标准库的品牌
     * @param bzkPartNumber 标准库型号
     * @param epId 供应商id
     * @return 返回符合条件的数据条数
     */
    Integer selectUseStandardDataCount(@Param("bzkBrand") String bzkBrand, @Param("bzkPartNumber") String bzkPartNumber, @Param("epId") String epId);

    /**
     * 获取根据状态、库存所在地、价格类型、阶梯价格id、供应商id获取数据库中包含的所有情况
     * @return 返回查询结果
     */
    List<InventoryAllSituationPojo> selectAllSituationByParam();

    /**
     * 根据条件查询数据
     * @param inventoryAllSituationPojo 库存所有情况的pojo类
     * @return 返回查询结果
     */
    List<InventoryBatchQueryPojo> queryDataByParam(@Param("param") InventoryAllSituationPojo inventoryAllSituationPojo);

    /**
     * 查询全部供应商id
     * @return 返回查询结果
     */
    List<String> getEnterpriseIds();

    /**
     * 查询当前供应商使用的阶梯价格id
     * @param epId 供应商id
     * @return 返回查询结果
     */
    List<Integer> selectLadderPriceIdsByEpId(@Param("epId") String epId);

    /**
     * 更新优势型号信息
     * @param inventory 库存数据
     * @return 返回更新结果
     */
    int updateAdvantageModel(@Param("inventory") Inventory inventory);

    /**
     * 根据查询条件获取数据id
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<Integer> selectIdListByParam(@Param("map") Map<String, String> map);

    /**
     * 更新库存表优势型号的优势状态
     * @param id 库存id
     * @param advantageStatus 优势状态
     * @return 返回更新结果
     */
    int updateAdvantageStatus(@Param("id") Integer id, @Param("advantageStatus") Integer advantageStatus);

    /**
     * 根据id集合获取最小起订量的最大值
     * @param idList id集合
     * @return 返回查询结果
     */
    BigDecimal getMaxMoqByIdList(@Param("idList") List<Integer> idList);
}