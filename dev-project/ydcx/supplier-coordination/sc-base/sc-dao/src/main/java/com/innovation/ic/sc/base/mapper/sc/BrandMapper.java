package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.Brand;

/**
 * @author zengqinglong
 * @desc 品牌表
 * @Date 2022/11/18 13:15
 **/
public interface BrandMapper extends EasyBaseMapper<Brand> {
    void truncate();
}
