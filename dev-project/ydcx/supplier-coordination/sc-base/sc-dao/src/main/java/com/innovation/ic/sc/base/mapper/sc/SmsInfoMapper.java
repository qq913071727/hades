package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.SmsInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   SmsInfo的mapper
 * @author linuo
 * @time   2022年8月5日10:58:01
 */
@Repository
public interface SmsInfoMapper extends EasyBaseMapper<SmsInfo> {

    /**
     * 查询最近一条短信验证码信息
     * @param mobile 手机号
     * @return 返回短信验证码
     */
    List<SmsInfo> selectSmsInfoList(@Param("mobile") String mobile);
}