package com.innovation.ic.sc.base.mapper.erp9_pvebcs;

import com.innovation.ic.sc.base.model.erp9_pvebcs.CompanyCurrency;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CompanyCurrencyMapper {

    List<CompanyCurrency> findCompanyCurrency(@Param("names") List<String> nameList);
}
