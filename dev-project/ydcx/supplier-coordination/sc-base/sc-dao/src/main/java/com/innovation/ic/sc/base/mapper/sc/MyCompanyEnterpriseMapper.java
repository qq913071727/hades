package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.MyCompanyEnterprise;
import com.innovation.ic.sc.base.vo.MyCompanyVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/***
 * 我的公司关联
 */
@Repository
public interface MyCompanyEnterpriseMapper extends EasyBaseMapper<MyCompanyEnterprise> {

    Integer checkRegistrationCompany(MyCompanyVo myCompanyVo);

    List<String> findByExternalId(@Param("externalId") String externalId);

    Integer containCompanyNameCount(MyCompanyVo myCompanyVo);

    Integer containCompanyNameState(MyCompanyVo myCompanyVo);
}