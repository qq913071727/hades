package com.innovation.ic.sc.base.mapper.sc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.sc.base.model.sc.MapsUserProducts;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MapsUserProductsMapper extends BaseMapper<MapsUserProducts> {


    /**
     * @description 更新用户操作数据关系
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/9 上午10:29
     */
    int addMapsUserProduct(List<MapsUserProducts> mapsUserProducts);


    /**
     * @description 根据登录用户查询用户产品型号关系列表
     * @parms
     * @return
     * @author Mr.myq
     * @datetime 2022/8/8 上午10:13
     */
//    List<MapsUserProducts> getListByLoginUserId(IPage<MapsUserProducts> page, @Param("userId") String userId);

}
