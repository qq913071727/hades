package com.innovation.ic.sc.base.mapper.erp9_pvesiteuser;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.erp9_pvesiteuser.Menus;

public interface MenusMapper extends EasyBaseMapper<Menus> {
}
