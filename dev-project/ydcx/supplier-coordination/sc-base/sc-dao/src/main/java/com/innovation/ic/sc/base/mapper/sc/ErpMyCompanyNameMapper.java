package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.ErpMyCompanyName;
import com.innovation.ic.sc.base.vo.SearchErpMyCompanyNameVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 我的公司,已经注册过的
 */
@Repository
public interface ErpMyCompanyNameMapper extends EasyBaseMapper<ErpMyCompanyName> {

    List<ErpMyCompanyName> findByLikeCompanyName(SearchErpMyCompanyNameVo searchErpMyCompanyName);

    Integer findByCompanyName(@Param("name") String name);

    void trunCateTable();
}