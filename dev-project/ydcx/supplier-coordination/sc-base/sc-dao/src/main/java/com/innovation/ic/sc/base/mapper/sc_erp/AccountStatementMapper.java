package com.innovation.ic.sc.base.mapper.sc_erp;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc_erp.AccountStatement;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 对账单
 * @Date 2022/12/20 9:45
 **/
public interface AccountStatementMapper extends EasyBaseMapper<AccountStatement> {
    /**
     * 批量插入
     * @param accountStatementList
     */
    void batchInsert(@Param("list") List<AccountStatement> accountStatementList);
}
