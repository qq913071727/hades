package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.AdvantageBrand;
import com.innovation.ic.sc.base.vo.UpdateAllAdvantageBrandVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
 * @desc   AdvantageBrand表的mapper类
 * @author linuo
 * @time   2022年9月1日16:40:54
 */
@Repository
public interface AdvantageBrandMapper extends EasyBaseMapper<AdvantageBrand> {
    /**
     * 查询优势品牌数据数量
     * @param map 查询参数
     * @return 返回查询结果
     */
    Long selectAdvantageBrandCount(@Param("map") Map<String, Object> map);

    String findByEmpId(UpdateAllAdvantageBrandVo updateAllAdvantageBrandVo);

    /**
     * 更新优势品牌生效状态
     * @param map 参数
     * @return 返回更新结果
     */
    int updateStatusByParam(@Param("map") Map<String, Object> map);

    /**
     * 根据条件查询优势品牌数据id集合
     * @param map 查询条件
     * @return 返回查询结果
     */
    List<Integer> selectIdListByParam(@Param("map") Map<String, Object> map);
}