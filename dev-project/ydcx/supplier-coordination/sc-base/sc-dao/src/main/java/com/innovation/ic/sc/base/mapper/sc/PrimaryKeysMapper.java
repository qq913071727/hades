package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.PrimaryKeys;
import org.apache.ibatis.annotations.Param;

public interface PrimaryKeysMapper extends EasyBaseMapper<PrimaryKeys> {
    /**
     * 根据name获取新的id
     * @param name 键值名称
     * @return 返回id
     */
    String getPrimaryId(@Param("name") String name);

    /**
     * 修改PrimaryKeys表的value值
     * @param name 键值名称
     * @return 返回结果
     */
    int updateValueByName(@Param("name") String name);
}