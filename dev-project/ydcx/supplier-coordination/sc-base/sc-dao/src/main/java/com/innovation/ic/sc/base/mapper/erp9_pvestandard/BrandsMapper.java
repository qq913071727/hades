package com.innovation.ic.sc.base.mapper.erp9_pvestandard;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.innovation.ic.sc.base.model.erp9_pvestandard.Brands;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description: 品牌
 * @ClassName: BrandsMapper
 * @Author: myq
 * @Date: 2022/8/5 下午2:24
 * @Version: 1.0
 */
@Repository
public interface BrandsMapper extends BaseMapper<Brands> {


    List<Brands> selectBrands(IPage<Brands> page, @Param("searchName")String searchName);
}
