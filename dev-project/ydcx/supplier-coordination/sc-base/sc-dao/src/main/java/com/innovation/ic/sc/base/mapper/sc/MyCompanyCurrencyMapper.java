package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.MyCompanyCurrency;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MyCompanyCurrencyMapper extends EasyBaseMapper<MyCompanyCurrency> {


    void truncate();

    MyCompanyCurrency findByExternalId(@Param("externalId") String externalId);
}
