package com.innovation.ic.sc.base.mapper.erp9_pvecrm;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.erp9_pvecrm.ScCreditsTopView;

/**
 * @author zengqinglong
 * @desc 对账单视图
 * @Date 2022/12/20 9:43
 **/
public interface ScCreditsTopViewMapper extends EasyBaseMapper<ScCreditsTopView> {
}
