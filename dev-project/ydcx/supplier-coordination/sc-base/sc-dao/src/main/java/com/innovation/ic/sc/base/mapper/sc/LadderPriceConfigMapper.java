package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.LadderPriceConfig;
import com.innovation.ic.sc.base.pojo.variable.ladderPrice.LadderPriceConfigRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   LadderPriceConfig表的mapper类
 * @author linuo
 * @time   2022年9月19日15:51:21
 */
@Repository
public interface LadderPriceConfigMapper extends EasyBaseMapper<LadderPriceConfig> {

    /**
     * 查询阶梯价格配置信息
     * @param correlationId 关联id
     * @return 返回阶梯价格配置信息
     */
    List<LadderPriceConfigRespPojo> selectLadderPriceConfigList(@Param("correlationId") Integer correlationId);
}