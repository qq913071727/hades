package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.PersonnelManagement;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonnelManagementMapper extends EasyBaseMapper<PersonnelManagement> {
}
