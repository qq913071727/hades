package com.innovation.ic.sc.base.mapper.sc;

import com.innovation.ic.sc.base.mapper.EasyBaseMapper;
import com.innovation.ic.sc.base.model.sc.MenuTree;
import com.innovation.ic.sc.base.pojo.variable.menu.Menu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuTreeMapper extends EasyBaseMapper<MenuTree> {

    /**
     * 根据用户id，查找对应的菜单列表
     * @param userId
     * @return
     */
    List<Menu> findByUserId(@Param("userId") String userId);

    /**
     * 根据type，返回Menu类型对象列表
     * @param type
     * @return
     */
    List<Menu> findByType(@Param("type") Integer type);

    /**
     * 根据type和userId，返回Menu类型对象列表
     * @param type
     * @return
     */
    List<Menu> findByTypeAndUserId(@Param("type") Integer type, @Param("userId") String userId);

    /**
     * 根据主账号id，返回子菜单
     * @param id
     * @return
     */
    List<Menu> findMainAccountMenuChildById(@Param("id") String id);

    /**
     * 根据子账号id，返回子菜单
     * @param id
     * @return
     */
    List<Menu> findSubAccountMenuChildById(@Param("id") String id, @Param("userId") String userId);

    /**
     * 查找所有记录
     * @return
     */
    List<Menu> findAll();

    List<Menu> findByMenuToType(@Param("type") String type, @Param("id") String id);

    /**
     * 清空menu表
     */
    void truncateTableMenu();

}
