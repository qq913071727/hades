package com.innovation.ic.sc.base.mapper.erp9_pvebcs;

import com.innovation.ic.sc.base.model.erp9_pvebcs.ErpDictionaries;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author erp字典
 */
@Repository
public interface ErpDictionariesMapper{


    List<ErpDictionaries>  findErpDictionaries(@Param("dCode") String dCode,@Param("nameList")List<String> nameList);
}
