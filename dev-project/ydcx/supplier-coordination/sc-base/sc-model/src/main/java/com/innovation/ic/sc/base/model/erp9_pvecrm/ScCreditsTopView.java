package com.innovation.ic.sc.base.model.erp9_pvecrm;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc erp 对账单视图
 * @Date 2022/12/20 9:31
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ScCreditsTopView", description = "对账单视图")
@TableName("ScCreditsTopView")
public class ScCreditsTopView {
    @ApiModelProperty(value = "卖家名称", dataType = "String")
    @TableField(value = "seller_name")
    private String sellerName;

    @ApiModelProperty(value = "卖家id", dataType = "String")
    @TableField(value = "seller_id")
    private String sellerId;

    @ApiModelProperty(value = "卖家编号", dataType = "String")
    @TableField(value = "seller_number")
    private String sellerNumber;

    @ApiModelProperty(value = "买家名称", dataType = "String")
    @TableField(value = "buyer_name")
    private String buyerName;

    @ApiModelProperty(value = "买家id", dataType = "String")
    @TableField(value = "buyer_id")
    private String buyerId;

    @ApiModelProperty(value = "买家编号", dataType = "String")
    @TableField(value = "buyer_number")
    private String buyerNumber;

    @ApiModelProperty(value = "结算类型：1:月结 2:周结", dataType = "Integer")
    @TableField(value = "settlement_type")
    private Integer settlementType;

    @ApiModelProperty(value = "对账单生成日期", dataType = "Date")
    @TableField(value = "generation_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date generationDate;

    @ApiModelProperty(value = "对账单结算日期", dataType = "Date")
    @TableField(value = "settlement_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date settlementDate;

    @ApiModelProperty(value = "对账单所属时间区间开始", dataType = "Date")
    @TableField(value = "interval_date_start")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intervalDateStart;

    @ApiModelProperty(value = "对账单所属时间区间结束", dataType = "Date")
    @TableField(value = "interval_date_end")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intervalDateEnd;
}
