package com.innovation.ic.sc.base.model.erp9_pvestandard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 产品表
 * @ClassName: Products
 * @Author: myq
 * @Date: 2022/8/5 上午11:28
 * @Version: 1.0
 */
@Data
@ApiModel
@TableName("Products")
public class Products implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField("PartNumber")
    private String partNumber;

    @ApiModelProperty(value = "品牌Id", dataType = "String")
    @TableField("BrandID")
    private String brandID;



}
