package com.innovation.ic.sc.base.model.erp9_pvesiteuser;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 账号详细信息（只为主账号）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ParentUsersTopView", description = "账号详细信息（只为主账号）")
@TableName("ParentUsersTopView")
public class ParentUsersTopView {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "站点 1 供应商协同系统、2 芯达通系统", dataType = "Integer")
    @TableField(value = "WebSite")
    private Integer webSite;

    @ApiModelProperty(value = "这个子账号的主账号id，如果为空，则表示这是子账号", dataType = "String")
    @TableField(value = "FatherID")
    private String fatherId;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "EnterpriseID")
    private String enterpriseId;

    @ApiModelProperty(value = "企业名称", dataType = "String")
    @TableField(value = "EnterpriseName")
    private String enterpriseName;

    @ApiModelProperty(value = "类型（1 卖家[供应商]） Seller Buyer", dataType = "Integer")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "登入账户", dataType = "String")
    @TableField(value = "UserName")
    private String userName;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "Password")
    private String password;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "RealName")
    private String realName;

    @ApiModelProperty(value = "职位", dataType = "String")
    @TableField(value = "Position")
    private String position;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @TableField(value = "Phone")
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    @TableField(value = "Email")
    private String email;

//    @ApiModelProperty(value = "创建人ID", dataType = "String")
//    @TableField(value = "CreatorID")
//    private String creatorId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

//    @ApiModelProperty(value = "修改人ID", dataType = "String")
//    @TableField(value = "ModifierID")
//    private String modifierId;

    @ApiModelProperty(value = "修改日期", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态（正常200 500停用）", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;
}
