package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 菜单
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompany", description = "我的公司")
@TableName("my_company")
public class MyCompany {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "国别地区", dataType = "String")
    private String countryRegionId;

    @ApiModelProperty(value = "公司类型", dataType = "String")
    private String companyTypeId;


    @ApiModelProperty(value = "发票类型", dataType = "String")
    private String invoicTypeId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;


    @ApiModelProperty(value = "币种", dataType = "String")
    private String currencyId;

    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;

    @ApiModelProperty(value = "收款公司", dataType = "String")
    private String receivingCompany;

    @ApiModelProperty(value = "开户银行", dataType = "String")
    private String bankOfDeposit;

    @ApiModelProperty(value = "账号", dataType = "String")
    private String accountNumber;

    @ApiModelProperty(value = "银行地址", dataType = "String")
    private String bankAddress;


    @ApiModelProperty(value = "swiftCode", dataType = "String")
    private String swiftCode;

    @ApiModelProperty(value = "中转银行", dataType = "String")
    private String transitBank;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;


    @ApiModelProperty(value = "收款账号ID", dataType = "String")
    private String bookAccountId;


    @ApiModelProperty(value = "erp推送过来的", dataType = "String")
    private Integer erpReceive;


}
