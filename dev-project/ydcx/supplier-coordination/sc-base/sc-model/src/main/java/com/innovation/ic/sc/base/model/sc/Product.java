package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Date;

/**
 * @author zengqinglong
 * @desc 产品实体类
 * @Date 2022/11/18 13:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Product", description = "产品信息")
@TableName("product")
public class Product {
    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "型号(产品名称)", dataType = "String")
    @TableField(value = "part_number")
    private String partNumber;

    @ApiModelProperty(value = "类别id", dataType = "String")
    @TableField(value = "category_id")
    private String categoryId;

    @ApiModelProperty(value = "类别组", dataType = "String")
    @TableField(value = "category_addr")
    private String categoryAddr;

    @ApiModelProperty(value = "品牌id", dataType = "String")
    @TableField(value = "brand_id")
    private String brandId;

    @ApiModelProperty(value = "所属行业", dataType = "String")
    @TableField(value = "industry_id")
    private String industryId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date")
    private String createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "modify_date")
    private Date modifyDate;
}
