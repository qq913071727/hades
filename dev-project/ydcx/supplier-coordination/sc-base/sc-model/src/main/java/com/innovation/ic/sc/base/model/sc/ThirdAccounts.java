package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   第三方账户
 * @author linuo
 * @time   2022年8月16日16:29:03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ThirdAccounts", description = "客户端信息")
@TableName("ThirdAccounts")
public class ThirdAccounts {
    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "类型：0 微信、1 QQ 、3 支付宝", dataType = "String")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "UnionID", dataType = "String")
    @TableField(value = "UnionID")
    private String unionID;

    @ApiModelProperty(value = "第三方唯一标识", dataType = "String")
    @TableField(value = "OpenID")
    private String openID;

    @ApiModelProperty(value = "UserID", dataType = "String")
    @TableField(value = "UserID")
    private String userID;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态：200正常、400解绑", dataType = "String")
    @TableField(value = "Status")
    private Integer status;
}