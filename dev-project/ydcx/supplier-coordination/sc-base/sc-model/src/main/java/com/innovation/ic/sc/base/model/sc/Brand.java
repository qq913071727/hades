package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 品牌实体类
 * @Date 2022/11/18 13:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Brand", description = "品牌信息")
@TableName("brand")
public class Brand {
    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "中文名称", dataType = "String")
    @TableField(value = "cn_name")
    private String cnName;

    @ApiModelProperty(value = "英文名称", dataType = "String")
    @TableField(value = "en_name")
    private String enName;

    @ApiModelProperty(value = "简称", dataType = "String")
    @TableField(value = "short_name")
    private String shortName;

    @ApiModelProperty(value = "首字母", dataType = "String")
    @TableField(value = "letter")
    private String letter;

    @ApiModelProperty(value = "网址", dataType = "String")
    @TableField(value = "web_site")
    private String webSite;

    @ApiModelProperty(value = "生产商", dataType = "String")
    @TableField(value = "manufacture")
    private String manufacture;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "Integer")
    @TableField(value = "is_agent")
    private Integer isAgent;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "summary")
    private String summary;

    @ApiModelProperty(value = "图标路径", dataType = "String")
    @TableField(value = "image")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "modify_date")
    private Date modifyDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

}
