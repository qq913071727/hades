package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   优势品牌表实体类
 * @author linuo
 * @time   2022年9月1日16:13:14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageBrand", description = "优势品牌表信息")
@TableName("advantage_brand")
public class AdvantageBrand {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "优势性质 1分销  2代理  3原厂", dataType = "Integer")
    @TableField(value = "dominant_nature")
    private Integer dominantNature;

    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "brand")
    private String brand;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建人姓名", dataType = "String")
    @TableField(value = "create_user")
    private String createUser;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "审核状态(0:待审核、1:审核通过、2:审核拒绝)", dataType = "Integer")
    @TableField(value = "audit_status")
    private Integer auditStatus;

    @ApiModelProperty(value = "优势状态(0:无效、1:正常)", dataType = "Integer")
    @TableField(value = "advantage_status")
    private Integer advantageStatus;

    @ApiModelProperty(value = "1为点击一次申请延期", dataType = "Integer")
    @TableField(value = "apply_for_extension")
    private Integer applyForExtension;

    @ApiModelProperty(value = "证件有效期开始", dataType = "Date")
    @TableField(value = "certificate_validity_start")
    private Date certificateValidityStart;

    @ApiModelProperty(value = "证件有效期结束", dataType = "Date")
    @TableField(value = "certificate_validity_end")
    private Date certificateValidityEnd;


    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "remark")
    private String remark;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;

    @ApiModelProperty(value = "Erp系统id", dataType = "String")
    @TableField(value = "erp_id")
    private String erpId;

    @ApiModelProperty(value = "审核原因", dataType = "String")
    @TableField(value = "audit_reason")
    private String auditReason;
}