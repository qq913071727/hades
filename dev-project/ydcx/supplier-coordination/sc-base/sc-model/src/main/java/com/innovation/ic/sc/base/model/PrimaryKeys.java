package com.innovation.ic.sc.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 主键表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PrimaryKeys", description = "主键表")
@TableName("PrimaryKeys")
public class PrimaryKeys {

    @ApiModelProperty(value = "键值名称", dataType = "String")
    @TableId(value = "Name", type = IdType.INPUT)
    private String name;

    @ApiModelProperty(value = "1 正常返回 2 时间有关返回", dataType = "Integer")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "补长", dataType = "Integer")
    @TableField(value = "Length")
    private Integer length;

    @ApiModelProperty(value = "当前序号", dataType = "Integer")
    @TableField(value = "Value")
    private Integer value;

    @ApiModelProperty(value = "当前日期", dataType = "Integer")
    @TableField(value = "Day")
    private Integer day;
}
