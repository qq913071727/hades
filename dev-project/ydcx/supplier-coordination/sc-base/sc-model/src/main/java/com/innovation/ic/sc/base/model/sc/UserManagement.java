package com.innovation.ic.sc.base.model.sc;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 人员管理数据
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserManagement", description = "人员管理数据")
@TableName("user_management")
public class UserManagement {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "user_id")
    private String userId;

    @ApiModelProperty(value = "部门下的用户）", dataType = "String")
    @TableField(value = "managed_user_id")
    private String managedUserId;

    @ApiModelProperty(value = "部门id", dataType = "Integer")
    @TableField(value = "department_id")
    private Integer departmentId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date")
    private Date createTime;
}
