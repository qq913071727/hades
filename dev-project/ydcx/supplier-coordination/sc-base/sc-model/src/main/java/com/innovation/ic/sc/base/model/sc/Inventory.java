package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc   库存表实体类
 * @author linuo
 * @time   2023年3月31日10:17:15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Inventory", description = "库存表信息")
@TableName("inventory")
public class Inventory {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "状态(0:已下架、1:已上架)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "part_number")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "brand")
    private String brand;

    @ApiModelProperty(value = "数量", dataType = "Integer")
    @TableField(value = "count")
    private Integer count;

    @ApiModelProperty(value = "批次", dataType = "String")
    @TableField(value = "batch")
    private String batch;

    @ApiModelProperty(value = "封装", dataType = "String")
    @TableField(value = "package")
    private String packages;

    @ApiModelProperty(value = "价格类型(1:单一价格、2:阶梯价格)", dataType = "Integer")
    @TableField(value = "unit_price_type")
    private Integer unitPriceType;

    @ApiModelProperty(value = "阶梯价格id", dataType = "Integer")
    @TableField(value = "ladder_price_id")
    private Integer ladderPriceId;

    @ApiModelProperty(value = "单价/最低阶梯价", dataType = "BigDecimal")
    @TableField(value = "unit_price")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "币种(1:人民币(含税)、2:美金)", dataType = "Integer")
    @TableField(value = "currency")
    private Integer currency;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    @TableField(value = "moq")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    @TableField(value = "mpq")
    private Integer mpq;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    @TableField(value = "packing")
    private Integer packing;

    @ApiModelProperty(value = "库存所在地(1:中国大陆、2:中国香港)", dataType = "Integer")
    @TableField(value = "inventory_home")
    private Integer inventoryHome;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "create_user")
    private String createUser;

    @ApiModelProperty(value = "创建人Id", dataType = "String")
    @TableField(value = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "更新日期", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;

    @ApiModelProperty(value = "优势状态(0:无效、1:有效)", dataType = "Integer")
    @TableField(value = "advantage_status")
    private Integer advantageStatus;

    @ApiModelProperty(value = "审核状态(0:待审核、1:审核通过、2:审核拒绝)", dataType = "Integer")
    @TableField(value = "audit_status")
    private Integer auditStatus;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    @TableField(value = "validity_date_start")
    private Date validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    @TableField(value = "validity_date_end")
    private Date validityDateEnd;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "remark")
    private String remark;

    @ApiModelProperty(value = "特色类型(1代理、2生产、3分销)", dataType = "Integer")
    @TableField(value = "type_")
    private Integer type;

    @ApiModelProperty(value = "Erp系统id", dataType = "String")
    @TableField(value = "erp_id")
    private String erpId;

    @ApiModelProperty(value = "来源(1用户添加、2优势型号关联、3erp导入)", dataType = "Integer")
    @TableField(value = "source")
    private Integer source;

    @ApiModelProperty(value = "zookeeper节点名，用于分布式事务", dataType = "String")
    @TableField(value = "zookeeper_node")
    private String zookeeperNode;
}