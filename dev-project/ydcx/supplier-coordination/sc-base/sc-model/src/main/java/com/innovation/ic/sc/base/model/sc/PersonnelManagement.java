package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 人员管理信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PersonnelManagement", description = "人员管理信息")
@TableName("personnel_management")
public class PersonnelManagement {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    @ApiModelProperty(value = "用户id", dataType = "String")
    @TableField(value = "user_id")
    private String userId;

    @ApiModelProperty(value = "被管理用户的id）", dataType = "String")
    @TableField(value = "managed_user_id")
    private String managedUserId;

}
