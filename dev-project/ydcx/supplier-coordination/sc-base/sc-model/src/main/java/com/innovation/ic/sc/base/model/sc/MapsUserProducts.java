package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 用户产品关系
 * @ClassName: MapsUserProductVo
 * @Author: myq
 * @Date: 2022/8/5 下午2:04
 * @Version: 1.0
 */
@Getter
@Setter
@ApiModel("用户产品型号实体类")
@TableName("map_user_product")
public class MapsUserProducts implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "用户ID", dataType = "string")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "产品型号ID", dataType = "String")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty(value = "创建人", dataType = "string")
    @TableField("create_id")
    private String createId;

    @ApiModelProperty(value = "创建时间", dataType = "datetime")
    @TableField("create_date")
    private String createDate;

}
