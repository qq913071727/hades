package com.innovation.ic.sc.base.model.sc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 用户和角色的关系
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MapUserRole", description = "用户和角色的关系")
@Document(collection = "map_user_role")
public class MapUserRole {

    @ApiModelProperty(value = "主键", dataType = "ObjectId")
    private ObjectId _id;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "角色id", dataType = "String")
    private String roleId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createTime = new Date();
}
