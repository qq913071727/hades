package com.innovation.ic.sc.base.model.sc;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 部門管理相关数据
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Department", description = "部門管理相关数据")
@TableName("department")
public class Department {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "部门名称", dataType = "String")
    @TableField(value = "department_name")
    private String departmentName;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "user_id")
    private String userId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_date")
    private Date createTime;
}
