package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   阶梯价格信息实体类
 * @author linuo
 * @time   2022年8月23日11:15:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceInfo", description = "阶梯价格信息")
@TableName("ladder_price_info")
public class LadderPriceInfo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "阶梯价格名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "配置方式(1:按起订量配置、2:按单价配置)", dataType = "Integer")
    @TableField(value = "config_type")
    private Integer configType;

    @ApiModelProperty(value = "币种(1:人民币、2:美元)", dataType = "Integer")
    @TableField(value = "currency")
    private Integer currency;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;
}