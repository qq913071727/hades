package com.innovation.ic.sc.base.model.sc;

import com.innovation.ic.sc.base.pojo.variable.menu.Menu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 菜单树
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MenuTree", description = "菜单树")
@Document(collection = "menu_tree")
public class MenuTree {

    @ApiModelProperty(value = "主键", dataType = "ObjectId")
    private ObjectId _id;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createTime;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    private Date updateTime;

    @ApiModelProperty(value = "菜单列表", dataType = "List")
    private List<Menu> firstMenuList = new ArrayList<Menu>();
}
