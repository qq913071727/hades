package com.innovation.ic.sc.base.model.erp9_pvesiteuser;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 菜单
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Menus", description = "菜单")
@TableName("Menus")
public class Menus {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "系统id", dataType = "String")
    @TableField(value = "SystemID")
    private String systemId;

    @ApiModelProperty(value = "父亲 :  父亲为空时表示系统级；深度为1时表示业务级；深度为2时表示菜单头；深度为3时表示菜单项", dataType = "String")
    @TableField(value = "FatherID")
    private String fatherId;

    @ApiModelProperty(value = "1 菜单、2 菜单项、3 按钮权限", dataType = "Integer")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "菜单名称", dataType = "String")
    @TableField(value = "Name")
    private String name;

    @ApiModelProperty(value = "图标", dataType = "String")
    @TableField(value = "Icon")
    private String icon;

    @ApiModelProperty(value = "路由地址", dataType = "String")
    @TableField(value = "Path")
    private String path;

    @ApiModelProperty(value = "组件路径", dataType = "String")
    @TableField(value = "Component")
    private String component;

    @ApiModelProperty(value = "权限串", dataType = "String")
    @TableField(value = "Perms")
    private String perms;

    @ApiModelProperty(value = "", dataType = "Integer")
    @TableField(value = "Hidden")
    private Integer hidden;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "", dataType = "Integer")
    @TableField(value = "OrderIndex")
    private Integer orderIndex;

    @ApiModelProperty(value = "创建人ID", dataType = "String")
    @TableField(value = "CreatorID")
    private String creatorId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改人ID", dataType = "String")
    @TableField(value = "ModifierID")
    private String modifierId;

    @ApiModelProperty(value = "修改日期", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态（正常200 400删除）", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;
}
