package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "ActionMessage", description = "动作消息")
@TableName("action_message")
@Getter
@Setter
public class ActionMessage implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "内容", dataType = "String")
    @TableField(value = "content")
    private String content;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

}