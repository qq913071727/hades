package com.innovation.ic.sc.base.model.sc;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 字典表
 *
 * @author Administrator
 */

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("dictionaries_type")
public class DictionariesType {


    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;


    @ApiModelProperty(value = "字典描述", dataType = "String")
    private String name;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;





}
