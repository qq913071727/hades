package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   阶梯价格单价配置实体类
 * @author linuo
 * @time   2023年4月13日16:35:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceAmountConfig", description = "阶梯价格单价配置")
@TableName("ladder_price_amount_config")
public class LadderPriceAmountConfig {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "阶梯价格信息id", dataType = "Integer")
    @TableField(value = "ladder_price_id")
    private Integer ladderPriceId;

    @ApiModelProperty(value = "最小金额", dataType = "BigDecimal")
    @TableField(value = "min_price")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最大金额", dataType = "BigDecimal")
    @TableField(value = "max_price")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;
}