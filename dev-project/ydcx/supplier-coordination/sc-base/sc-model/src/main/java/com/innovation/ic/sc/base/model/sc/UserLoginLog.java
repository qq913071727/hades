package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户登录日志信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginLog", description = "用户登录日志信息")
@TableName("user_login_log")
public class UserLoginLog {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "登录类型。1表示账号密码登录，2表示手机免密登录，3表示微信登录，4表示QQ登录，5表示支付宝登录", dataType = "Integer")
    @TableField(value = "login_type")
    private Integer loginType;

    @ApiModelProperty(value = "客户端ip", dataType = "String")
    @TableField(value = "remote_address")
    private String remoteAddress;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}
