package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 用户动作消息关系表
 *
 * @author myq
 * @since 1.0.0 2023-01-06
 */
@TableName("action_message_user_link")
@Setter
@Getter
public class ActionMessageUserLink implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "消息主键", dataType = "Integer")
    private Integer actionMessageId;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "是否已读: 0否 1是", dataType = "Integer")
    private Integer isRead;
}