package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   优势型号表实体类
 * @author linuo
 * @time   2022年8月29日13:48:38
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AdvantageModel", description = "优势型号表信息")
@TableName("advantage_model")
public class AdvantageModel {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "特色类型(1代理、2生产、3分销)", dataType = "Integer")
    @TableField(value = "type_")
    private Integer type;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "part_number")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "brand")
    private String brand;

    @ApiModelProperty(value = "审核状态(0:待审核、1:审核通过、2:审核拒绝)", dataType = "Integer")
    @TableField(value = "audit_status")
    private Integer auditStatus;

    @ApiModelProperty(value = "审批备注", dataType = "String")
    @TableField(value = "audit_summary")
    private String auditSummary;

    @ApiModelProperty(value = "优势状态(0:无效、1:有效)", dataType = "Integer")
    @TableField(value = "advantage_status")
    private Integer advantageStatus;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "create_user")
    private String createUser;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "remark")
    private String remark;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;

    @ApiModelProperty(value = "来源(1用户添加、2优势型号关联)", dataType = "Integer")
    @TableField(value = "source")
    private Integer source;

    @ApiModelProperty(value = "有效日期开始", dataType = "Date")
    @TableField(value = "validity_date_start")
    private Date validityDateStart;

    @ApiModelProperty(value = "有效日期结束", dataType = "Date")
    @TableField(value = "validity_date_end")
    private Date validityDateEnd;

    @ApiModelProperty(value = "Erp系统id", dataType = "String")
    @TableField(value = "erp_id")
    private String erpId;
}