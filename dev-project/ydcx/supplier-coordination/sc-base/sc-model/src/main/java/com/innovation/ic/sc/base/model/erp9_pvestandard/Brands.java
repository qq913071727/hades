package com.innovation.ic.sc.base.model.erp9_pvestandard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 品牌表
 * @ClassName: Brands
 * @Author: myq
 * @Date: 2022/8/5 下午1:50
 * @Version: 1.0
 */
@Data
@ApiModel
@TableName("Brands")
public class Brands implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField("Name")
    private String Name;


    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField("CreateDate")
    private Date createDate;


    @ApiModelProperty(value = "品牌名称", dataType = "String")
    @TableField("CNName")
    private String cnName;



    @ApiModelProperty(value = "英文名称", dataType = "String")
    @TableField(value = "ENName")
    private String enName;

    @ApiModelProperty(value = "简称", dataType = "String")
    @TableField(value = "ShortName")
    private String shortName;

    @ApiModelProperty(value = "首字母", dataType = "String")
    @TableField(value = "Letter")
    private String letter;

    @ApiModelProperty(value = "网址", dataType = "String")
    @TableField(value = "WebSite")
    private String webSite;

    @ApiModelProperty(value = "生产商", dataType = "String")
    @TableField(value = "Manufacturer")
    private String manufacture;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "Integer")
    @TableField(value = "IsAgent")
    private Integer isAgent;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "Integer")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "图标路径", dataType = "String")
    @TableField(value = "Image")
    private String image;



}
