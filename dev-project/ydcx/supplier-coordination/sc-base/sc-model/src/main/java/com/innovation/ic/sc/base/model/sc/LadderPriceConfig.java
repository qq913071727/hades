package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * @desc   阶梯价格配置实体类
 * @author linuo
 * @time   2022年8月23日11:15:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LadderPriceConfig", description = "阶梯价格配置")
@TableName("ladder_price_config")
public class LadderPriceConfig {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "关联类型(1:按起订量配置关联阶梯价格信息表、2:按单价配置关联阶梯价格单价配置表)", dataType = "Integer")
    @TableField(value = "correlation_type")
    private Integer correlationType;

    @ApiModelProperty(value = "关联id", dataType = "Integer")
    @TableField(value = "correlation_id")
    private Integer correlationId;

    @ApiModelProperty(value = "起订量", dataType = "Integer")
    @TableField(value = "mq")
    private Integer mq;

    @ApiModelProperty(value = "降比", dataType = "BigDecimal")
    @TableField(value = "lower")
    private BigDecimal lower;

    @ApiModelProperty(value = "企业ID（供应商ID）", dataType = "String")
    @TableField(value = "enterprise_id")
    private String enterpriseId;
}