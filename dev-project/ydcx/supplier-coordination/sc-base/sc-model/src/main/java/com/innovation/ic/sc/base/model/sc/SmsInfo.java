package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   短信验证码实体类
 * @author linuo
 * @time   2022年8月5日10:53:11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SmsInfo", description = "菜单和用户实体表")
@TableName("sms_info")
public class SmsInfo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @TableField(value = "mobile")
    private String mobile;

    @ApiModelProperty(value = "短信验证码", dataType = "String")
    @TableField(value = "sms_code")
    private String smsCode;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "短信验证码类型,1: 登录", dataType = "String")
    @TableField(value = "type_")
    private String type;

    @ApiModelProperty(value = "验证次数", dataType = "Integer")
    @TableField(value = "verify_times")
    private Integer verifyTimes;

    @ApiModelProperty(value = "短信验证状态(0:初始状态;1:成功;2:失败)", dataType = "Integer")
    @TableField(value = "sms_status")
    private Integer smsStatus;
}