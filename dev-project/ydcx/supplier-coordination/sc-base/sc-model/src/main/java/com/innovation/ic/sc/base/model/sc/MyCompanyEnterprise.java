package com.innovation.ic.sc.base.model.sc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MyCompanyEnterprise", description = "我的公司")
@TableName("my_company_enterprise")
public class MyCompanyEnterprise {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "公司id", dataType = "String")
    private String enterpriseId;


    @ApiModelProperty(value = "关联sc表公司id", dataType = "Integer")
    private Integer myCompanyId;


    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;


    @ApiModelProperty(value = "失败原因", dataType = "String")
    private String cause;


    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "0 待审核  1 审核通过 2、审核拒绝 ", dataType = "String")
    private Integer state;

    @ApiModelProperty(value = "是否主账号 1 为主公司", dataType = "String")
    private Integer mainCompany;
}
