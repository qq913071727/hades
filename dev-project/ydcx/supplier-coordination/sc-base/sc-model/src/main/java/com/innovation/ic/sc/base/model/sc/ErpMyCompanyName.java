package com.innovation.ic.sc.base.model.sc;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 保存我的公司
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("erp_my_company_name")
public class ErpMyCompanyName {


    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "name", dataType = "String")
    private String name;


    @ApiModelProperty(value = "创建日期", dataType = "Date")
    private Date createDate;


    @ApiModelProperty(value = "公司状态:  500禁用  400黑名单 200正常", dataType = "String")
    private Integer enterpriseStatus;

    @ApiModelProperty(value = "外部主键id", dataType = "String")
    private String externalId;


    @ApiModelProperty(value = "国别", dataType = "String")
    private String district;

    @ApiModelProperty(value = "供应商类型", dataType = "String")
    private String supplierType;

    @ApiModelProperty(value = "企业性质", dataType = "String")
    private String nature;

    @ApiModelProperty(value = "发票类型", dataType = "String")
    private String invoiceType;

    @ApiModelProperty(value = "文件", dataType = "String")
    private String files;





}
