package com.innovation.ic.sc.base.model.sc_erp;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author zengqinglong
 * @desc 账户对账单
 * @Date 2022/12/19 16:40
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountStatement", description = "账户对账单")
@TableName("account_statement")
public class AccountStatement {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "对账单号", dataType = "String")
    @TableField(value = "statement_no")
    private String statementNo;

    @ApiModelProperty(value = "对账单生成日期", dataType = "Date")
    @TableField(value = "generation_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date generationDate;

    @ApiModelProperty(value = "对账单所属时间区间开始", dataType = "Date")
    @TableField(value = "interval_date_start")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intervalDateStart;

    @ApiModelProperty(value = "对账单所属时间区间结束", dataType = "Date")
    @TableField(value = "interval_date_end")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date intervalDateEnd;

    @ApiModelProperty(value = "对账单结算日期", dataType = "Date")
    @TableField(value = "settlement_date")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date settlementDate;

    @ApiModelProperty(value = "卖家名称", dataType = "String")
    @TableField(value = "seller_name")
    private String sellerName;

    @ApiModelProperty(value = "卖家id", dataType = "String")
    @TableField(value = "seller_id")
    private String sellerId;

    @ApiModelProperty(value = "买家名称", dataType = "String")
    @TableField(value = "buyer_name")
    private String buyerName;

    @ApiModelProperty(value = "买家id", dataType = "String")
    @TableField(value = "buyer_id")
    private String buyerId;

    @ApiModelProperty(value = "币种：1:人民币 2:美金", dataType = "Integer")
    @TableField(value = "currency")
    private Integer currency;

    @ApiModelProperty(value = "主账号id", dataType = "String")
    @TableField(value = "user_id")
    private String userId;

    @ApiModelProperty(value = "结算类型：1:月结 2:周结", dataType = "Integer")
    @TableField(value = "settlement_type")
    private Integer settlementType;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "update_time")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
