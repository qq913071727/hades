package com.innovation.ic.sc.base.model.erp9_pvecrm;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.innovation.ic.sc.base.model.sc.Inventory;
import com.innovation.ic.sc.base.pojo.variable.UploadFileMessagePojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.util.List;

/**
 * @desc   ERP9的优势型号信息
 * @author linuo
 * @time   2022年8月29日14:17:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SpecialsSCTopView", description = "ERP9的优势型号信息")
@TableName("SpecialsSCTopView")
public class SpecialsSCTopView {
    @ApiModelProperty(value = "id", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "供应商id", dataType = "String")
    @TableField(value = "EnterpriseID")
    private String enterpriseID;

    @ApiModelProperty(value = "特色类型(1代理、2生产、3分销)", dataType = "Integer")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField(value = "PartNumber")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    @TableField(value = "Brand")
    private String brand;

    @ApiModelProperty(value = "有效期开始", dataType = "Date")
    @TableField(value = "StartDate")
    private Date startDate;

    @ApiModelProperty(value = "有效期结束", dataType = "Date")
    @TableField(value = "EndDate")
    private Date endDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "Creator")
    private String creator;

    @ApiModelProperty(value = "创建人姓名", dataType = "String")
    @TableField(value = "CreatorName")
    private String creatorName;

    @ApiModelProperty(value = "附件", dataType = "String")
    @TableField(value = "Files")
    private String files;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "审批状态(100待审批、200审批通过、300审批拒绝)", dataType = "Integer")
    @TableField(value = "AuditStatus")
    private Integer auditStatus;

    @ApiModelProperty(value = "审批备注", dataType = "String")
    @TableField(value = "AuditSummary")
    private String auditSummary;

   /* @ApiModelProperty(value = "有效期结束", dataType = "String")
    @Transient
    private String endDateString;
*/

/*    @ApiModelProperty(value = "有效期结束", dataType = "String")
    @Transient
    private String startDateString;*/

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "优势状态(200有效、300无效)", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;

    @ApiModelProperty(value = "", dataType = "List")
    @TableField(exist = false)
    private List<UploadFileMessagePojo> fileVoList;

    /**
     * SpecialsSCTopView转换为Inventory
     * @param specialsScTopView 优势型号信息
     * @param auditStatus 审批状态
     * @param advantageStatus 优势状态
     * @return 返回AdvantageModel
     */
    public Inventory toAdvantageModel(SpecialsSCTopView specialsScTopView, Integer auditStatus, Integer advantageStatus){
        Inventory inventory = new Inventory();
        inventory.setPartNumber(specialsScTopView.getPartNumber());
        inventory.setType(specialsScTopView.getType());
        inventory.setBrand(specialsScTopView.getBrand());
        inventory.setAuditStatus(auditStatus);
        inventory.setRemark(specialsScTopView.getSummary());
        inventory.setAdvantageStatus(advantageStatus);
        inventory.setCreateUserId(specialsScTopView.getCreator());
        inventory.setCreateUser(specialsScTopView.getCreatorName());
        inventory.setCreateDate(specialsScTopView.getCreateDate());
        inventory.setEnterpriseId(specialsScTopView.getEnterpriseID());
        inventory.setValidityDateStart(specialsScTopView.getStartDate());
        inventory.setValidityDateEnd(specialsScTopView.getEndDate());
        inventory.setErpId(specialsScTopView.getId());
        return inventory;
    }
}