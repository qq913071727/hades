package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Data
@Component
@ConfigurationProperties(prefix = "importdata")
public class ImportRedisDataScriptConfig {
    String[] scripts;
}