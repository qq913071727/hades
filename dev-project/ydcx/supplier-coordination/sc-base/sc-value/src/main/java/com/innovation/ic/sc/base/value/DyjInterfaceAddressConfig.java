package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   大赢家接口地址配置类
 * @author linuo
 * @time   2023年1月9日15:22:46
 */
@Data
@Component
@ConfigurationProperties(prefix = "dyj")
public class DyjInterfaceAddressConfig {
    /** 查询大赢家中是否包含型号数据 */
    private String bom;

    /** 查询大赢家接口的token */
    private String token;
}