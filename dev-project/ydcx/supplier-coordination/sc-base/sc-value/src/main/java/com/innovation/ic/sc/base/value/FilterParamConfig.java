package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @desc   过滤器参数配置类
 * @author linuo
 * @time   2022年8月10日16:20:15
 */
@Data
@Component
@ConfigurationProperties(prefix = "filter")
public class FilterParamConfig {
    /**
     * 允许通过的路径
     */
    private List<String> allowPathList;

    /**
     * 登录相关的路径
     */
    private List<String> loginPathList;

    /**
     * 验证码相关的路径
     */
    private List<String> verificationCodePathList;

    /**
     * 键值对中的key
     */
    private String authorization;

    /**
     * 存储在header中的token
     */
    private String token;


}