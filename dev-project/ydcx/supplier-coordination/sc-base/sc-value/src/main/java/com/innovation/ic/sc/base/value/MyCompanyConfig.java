package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "company")
public class MyCompanyConfig {
    /**
     * erp地址
     */
    private String erpServiceUrl;

    /**
     * 模糊搜索公司接口
     */
    private String searchCompany;

    /**
     * 获取国别地区
     */
    private String searchDistrict;

    /**
     * 获取字典表数据
     */
    private String searchDictionaries;

    /**
     * 获取发票类型
     */
    private String searchInvoiceType;

    /**
     * 获取币种
     */
    private String searchCurrecySelector;

    /**
     * 注册公司接口
     */
    private String register;

    /**
     * 查询公司列表
     */
    private String searchList;

    /**
     * 查询详情
     */
    private String detail;

    /**
     * 删除
     */
    private String delete;

    /**
     * 删除
     */
    private String addRelation;
}