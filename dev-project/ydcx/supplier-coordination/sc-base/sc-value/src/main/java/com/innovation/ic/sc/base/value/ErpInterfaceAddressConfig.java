package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   erp接口地址配置类
 * @author linuo
 * @time   2022年7月26日16:56:59
 */
@Data
@Component
@ConfigurationProperties(prefix = "erp")
public class ErpInterfaceAddressConfig {
    /** 查询erp的订单统计数据 */
    private String getSupplierScoringStatistics;

    /** ERP发送新密码接口地址url */
    private String erpForgotPasswordUrl;

    /** ERP根据账号密码绑定手机号接口url */
    private String erpBindMobileByPwdUrl;

    /** ERP解绑账号接口url */
    private String erpUnBindUrl;
}