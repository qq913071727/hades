package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   redis配置类
 * @author linuo
 * @time   2022年8月11日15:21:21
 */
@Data
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisParamConfig {
    /** 连接超时时间 */
    private Integer timeout;

    /** 属性值超时时间 */
    private Integer tokenTimeout;
}