package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "tyc")
public class TYCParamConfig {


    /**
     * 天眼查erp的url
     */
    private String erpUrl;


    /**
     * 连接超时和读取超时
     */
    private Integer timeOut;

}
