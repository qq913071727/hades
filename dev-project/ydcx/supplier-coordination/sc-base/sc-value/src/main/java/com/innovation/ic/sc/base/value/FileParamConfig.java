package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   文件配置类
 * @author linuo
 * @time   2022年8月10日17:01:38
 */
@Data
@Component
@ConfigurationProperties(prefix = "file")
public class FileParamConfig {
    /** 文件地址 */
    private String fileUrl;

    /** 图片展示路径 */
    private String imageExhibitionUrl;

    /** 历史文件地址 */
    private String fileHistoryUrl;
}