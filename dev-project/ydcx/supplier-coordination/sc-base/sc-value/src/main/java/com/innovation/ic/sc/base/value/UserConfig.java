package com.innovation.ic.sc.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @desc   账号相关
 * @author songwenqi
 * @time   2022年11月30日17:41:08
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "user")
public class UserConfig {
    /** 登录认证地址 */
    private String enterSonUserUrl;

    /** 验证码认证地址 */
    private String verificationCodeUrl;
}
