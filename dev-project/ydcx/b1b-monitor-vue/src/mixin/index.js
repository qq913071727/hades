export const mixins = {
    data() {
        return {};
    },
    computed: {},
    created() {
        window.addEventListener("keydown", this.handkeyCode, true);
    },
    beforeDestroy() {
        console.log("beforeDestroy");
        window.removeEventListener("keydown", this.handkeyCode);
    },
    mounted() { },
    methods: {
        handkeyCode(e) {
            if (e.keyCode === 13) {
                this.handle("search");
            }
        },
    },
};