import Vue from 'vue';
import Router from 'vue-router';

// const originalPush = Router.prototype.push
// Router.prototype.push = function push(location) {
//     return originalPush.call(this, location).catch(err => err)
// }
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: "/home/index"
        },
        {
            path: "/home",
            name: 'home',
            component: () =>
                import('@/views/home'),
            children: [
                {
                    // 首页 
                    path: "index",
                    name: 'index',
                    component: () =>
                        import('@/views/index'), // 首页
                    meta: { title: '首页' }
                },
                {
                    // 监控系统  
                    path: "controlSystem",
                    name: 'controlSystem',
                    component: () =>
                        import('@/views/controlSystem'),
                    meta: { title: '监控系统' }
                },
                {
                    // 监控系统 -服务存活-日志 
                    path: "serviceSurviveLog",
                    name: 'serviceSurviveLog',
                    component: () =>
                        import('@/views/controlSystem/serviceSurvive/log'),
                    meta: { title: '监控系统' }
                },
                {
                    // 监控系统 -服务存活-日志 
                    path: "serviceSurviveTask",
                    name: 'serviceSurviveTask',
                    component: () =>
                        import('@/views/controlSystem/serviceSurvive/task'),
                    meta: { title: '监控系统' }
                },
                {
                    // 监控系统 -接口可用-日志 
                    path: "interfaceAvailableLog",
                    name: 'interfaceAvailableLog',
                    component: () =>
                        import('@/views/controlSystem/interfaceAvailable/log'),
                    meta: { title: '监控系统' }
                },
                {
                    // 监控系统 -接口可用-日志 
                    path: "interfaceAvailableTask",
                    name: 'interfaceAvailableTask',
                    component: () =>
                        import('@/views/controlSystem/interfaceAvailable/task'),
                    meta: { title: '监控系统' }
                },
                {
                    // 服务器监控 
                    path: "serverMonitor",
                    name: 'serverMonitor',
                    component: () =>
                        import('@/views/serverMonitor'),
                    meta: { title: '服务器监控' }
                },
                {
                    // 服务器任务 
                    path: "serverTask",
                    name: 'serverTask',
                    component: () =>
                        import('@/views/serverMonitor/task'),
                    meta: { title: '服务器监控' }
                },
                {
                    // 服务器日志 
                    path: "serverRecord",
                    name: 'serverRecord',
                    component: () =>
                        import('@/views/serverMonitor/record'),
                    meta: { title: '服务器监控' }
                },
                {
                    // 中间件监控 
                    path: "middleware",
                    name: 'middleware',
                    component: () =>
                        import('@/views/middleware'),
                    meta: { title: '中间件监控' }
                },
                {
                    // sql server任务
                    path: "sqlserverTask",
                    name: 'sqlserverTask',
                    component: () =>
                        import('@/views/middleware/sqlServer/task'),
                    meta: { title: '中间件监控' }
                },
                {
                    // sql server日志
                    path: "sqlserverRecord",
                    name: 'sqlserverRecord',
                    component: () =>
                        import('@/views/middleware/sqlServer/record'),
                    meta: { title: '中间件监控' }
                },
                {
                    // sql server任务
                    path: "mysqlTask",
                    name: 'mysqlTask',
                    component: () =>
                        import('@/views/middleware/mySql/task'),
                    meta: { title: '中间件监控' }
                },
                {
                    // mysql日志
                    path: "mysqlRecord",
                    name: 'mysqlRecord',
                    component: () =>
                        import('@/views/middleware/mySql/record'),
                    meta: { title: '中间件监控' }
                },
                {
                    // elasticsearch任务
                    path: "elasticsearchTask",
                    name: 'elasticsearchTask',
                    component: () =>
                        import('@/views/middleware/elasticsearch/task'),
                    meta: { title: '中间件监控' }
                },
                {
                    // elasticsearch日志
                    path: "elasticsearchRecord",
                    name: 'elasticsearchRecord',
                    component: () =>
                        import('@/views/middleware/elasticsearch/record'),
                    meta: { title: '中间件监控' }
                },
                {
                    // mongodb任务
                    path: "mongodbTask",
                    name: 'mongodbTask',
                    component: () =>
                        import('@/views/middleware/mongodb/task'),
                    meta: { title: '中间件监控' }
                },
                {
                    // elasticsearch日志
                    path: "mongodbRecord",
                    name: 'mongodbRecord',
                    component: () =>
                        import('@/views/middleware/mongodb/record'),
                    meta: { title: '中间件监控' }
                },
                {
                    // rabbitmq任务
                    path: "rabbitmqTask",
                    name: 'rabbitmqTask',
                    component: () =>
                        import('@/views/middleware/rabbitmq/task'),
                    meta: { title: '中间件监控' }
                },
                {
                    // rabbitmq日志
                    path: "rabbitmqRecord",
                    name: 'rabbitmqRecord',
                    component: () =>
                        import('@/views/middleware/rabbitmq/record'),
                    meta: { title: '中间件监控' }
                },
                {
                    // redis任务
                    path: "redisTask",
                    name: 'redisTask',
                    component: () =>
                        import('@/views/middleware/redis/task'),
                    meta: { title: '中间件监控' }
                },
                {
                    // redis日志
                    path: "redisRecord",
                    name: 'redisRecord',
                    component: () =>
                        import('@/views/middleware/redis/record'),
                    meta: { title: '中间件监控' }
                },
                //日志监控
                {
                    path: "logMonitor",
                    name: 'logMonitor',
                    component: () =>
                        import('@/views/logMonitor'),
                    meta: { title: '日志监控' }
                },
                {
                    //日志监控-任务
                    path: "logMonitorTask",
                    name: 'logMonitorTask',
                    component: () =>
                        import('@/views/logMonitor/task'),
                    meta: { title: '任务' }
                },
                {
                    //日志监控-日志
                    path: "logMonitorLog",
                    name: 'logMonitorLog',
                    component: () =>
                        import('@/views/logMonitor/record'),
                    meta: { title: '日志' }
                },
                {
                    //日志分析
                    path: "logAnalysis",
                    name: 'logAnalysis',
                    component: () =>
                        import('@/views/logAnalysis'),
                    meta: { title: '日志分析' }
                },
                // 页面访问日志
                {
                    path: "pageLog",
                    name: 'pageLog',
                    component: () =>
                        import('@/views/logAnalysis/pageLog'),
                    meta: { title: '页面访问日志' }
                },
                // 用户登录日志
                {
                    path: "userLog",
                    name: 'userLog',
                    component: () =>
                        import('@/views/logAnalysis/userLog'),
                    meta: { title: '用户登录日志' }
                },
                // 接口调用日志
                {
                    path: "joggleLog",
                    name: 'joggleLog',
                    component: () =>
                        import('@/views/logAnalysis/joggleLog'),
                    meta: { title: '接口调用日志' }
                },
                {
                    //配置管理
                    path: "configuration",
                    name: 'configuration',
                    component: () =>
                        import('@/views/configuration'),
                    meta: { title: '配置管理' }
                },

                // ======= 配置管理  =======
                {
                    //配置管理-系统
                    path: "confSystem",
                    name: 'confSystem',
                    component: () =>
                        import('@/views/configuration/system'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-环境
                    path: "confEnvironment",
                    name: 'confEnvironment',
                    component: () =>
                        import('@/views/configuration/environment'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-服务
                    path: "confService",
                    name: 'confService',
                    component: () =>
                        import('@/views/configuration/service'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-模块
                    path: "confModel",
                    name: 'confModel',
                    component: () =>
                        import('@/views/configuration/model'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-接口
                    path: "confInterface",
                    name: 'confInterface',
                    component: () =>
                        import('@/views/configuration/interface'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-服务器
                    path: "confServer",
                    name: 'confServer',
                    component: () =>
                        import('@/views/configuration/server'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-日志
                    path: "confLog",
                    name: 'confLog',
                    component: () =>
                        import('@/views/configuration/log'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-邮箱
                    path: "confEmail",
                    name: 'confEmail',
                    component: () =>
                        import('@/views/configuration/email'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-短信
                    path: "confShortMessage",
                    name: 'confShortMessage',
                    component: () =>
                        import('@/views/configuration/shortMessage'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-sqlServe
                    path: "confSqlServer",
                    name: 'confSqlServer',
                    component: () =>
                        import('@/views/configuration/sqlServer'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-mysql
                    path: "confMysql/:type",
                    name: 'confMysql',
                    component: () =>
                        import('@/views/configuration/mysql'),
                    meta: { title: '配置管理' }
                },

                {
                    //配置管理-elasticsearch
                    path: "elasticsearch",
                    name: 'elasticsearch',
                    component: () =>
                        import('@/views/configuration/elasticsearch'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-mongodb
                    path: "mongodb",
                    name: 'mongodb',
                    component: () =>
                        import('@/views/configuration/mongodb'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-mysql
                    path: "rabbitmq",
                    name: 'rabbitmq',
                    component: () =>
                        import('@/views/configuration/rabbitmq'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-mysql
                    path: "redis",
                    name: 'redis',
                    component: () =>
                        import('@/views/configuration/redis'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-kafka
                    path: "kafka",
                    name: 'kafka',
                    component: () =>
                        import('@/views/configuration/kafka'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-zookeeper
                    path: "zookeeper",
                    name: 'zookeeper',
                    component: () =>
                        import('@/views/configuration/zookeeper'),
                    meta: { title: '配置管理' }
                },
                {
                    //配置管理-cancal
                    path: "cancal",
                    name: 'cancal',
                    component: () =>
                        import('@/views/configuration/cancal'),
                    meta: { title: '配置管理' }
                },

            ]
        }
    ]
})