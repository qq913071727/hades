import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from "./store";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import './utils/directive'; //包含自定义指令
import './assets/reset.css'
import "@/assets/iconfont/index.css";


// 全局设置 elementUI的$message的默认显示时间
const messages = ['success', 'warning', 'info', 'error'];
messages.forEach(type => {
  ElementUI.Message[type] = options => {
    if (typeof options === 'string') {
      options = {
          message: options
      };
      // 默认配置
      options.duration = 2000;
      options.showClose = true;
    }
    options.type = type;
    return ElementUI.Message(options);
  }
})

Vue.use(ElementUI)
Vue.config.productionTip = false

Vue.directive('focus', {
  inserted: function(el) {
    el.querySelector('input').focus();
  }
});

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
