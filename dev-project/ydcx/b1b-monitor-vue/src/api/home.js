import request from "@/utils/request";

// 首页数据
export function homeList(){
    return request({
        url:"/api/v1/home/findAllHome",
        method:"post"
    })
}