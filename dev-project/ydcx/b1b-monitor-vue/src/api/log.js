import request from '@/utils/request'
// 页面访问日志
export function pageVisitLog(data) {
    return request({
        url:'/api/v1/pageVisitLog/page',
        method: 'post',
        data
    })
}
// 系统名称
export function systemFind(data) {
    return request({
        url:'/api/v1/system/findByPage'+ data,
        method: 'get',
        
    })
}
// 环境名称
export function environmentData(data) {
    return request({
        url:'/api/v1/environment/dropDown'+ data,
        method: 'get',
        
    })
}
// 服务名称
export function serviceData(data) {
    return request({
        url:'/api/v1/service/dropDown',
        method: 'get',
        data
    })
}
//用户登录日志
export function userLoginLog(data) {
    return request({
        url:'/api/v1/userLoginLog/page',
        method: 'post',
        data
    })
}
//接口调用日志
export function interfaceLog(data) {
    return request({
        url:'/api/v1/interfaceCallLog/page',
        method: 'post',
        data
    })
}

