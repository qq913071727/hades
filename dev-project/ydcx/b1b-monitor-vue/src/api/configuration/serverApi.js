import request from "@/utils/request";

// 配置-服务器

// 服务器添加
export async function addServer(data){
    return  request({
        url:"/api/v1/server/addServer",
        method:'post',
        data
    })
}

// 服务器删除
export async function delServer(data){
    return  request({
        url:"/api/v1/server/deleteServer",
        method:'post',
        data
    })
}


// 服务器列表
export async function getServerData(data){
    return  request({
        url:`/api/v1/server/findByPage${data}`,
        method:'get',
        data
    })
}




