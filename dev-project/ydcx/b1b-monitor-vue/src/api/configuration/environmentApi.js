import request from "@/utils/request";

// 配置-环境

// 环境添加
export async function addEnvironment(data){
    return  request({
        url:"/api/v1/environment/addEnvironment",
        method:'post',
        data
    })
}

// 环境删除
export async function delEnvironment(data){
    return  request({
        url:"/api/v1/environment/deleteEnvironment",
        method:'post',
        data
    })
}


// 环境列表
export async function getEnvironmentData(data){
    return  request({
        url:`/api/v1/environment/findByPage${data}`,
        method:'get',
        data
    })
}

// 环境下拉列表
export async function getEnvironmentDropDown(data){
    return  request({
        url:`/api/v1/environment/dropDown${data}`,
        method:'get',
        data
    })
}
