import request from "@/utils/request";

// 配置-接口

// 接口添加
export async function addInterface(data){
    return  request({
        url:"/api/v1/apiController/addApi",
        method:'post',
        data
    })
}

// 接口删除
export async function delInterface(data){
    return  request({
        url:"/api/v1/apiController/deleteApi",
        method:'post',
        data
    })
}


// 接口列表
export async function getInterfaceData(data){
    return  request({
        url:`/api/v1/apiController/findByPage${data}`,
        method:'get',
        data
    })
}




