import request from "@/utils/request";

// 配置-系统

// 系统添加
export async function addSystem(data){
    return  request({
        url:"/api/v1/system/addSystem",
        method:'post',
        data
    })
}

// 系统删除
export async function delSystem(data){
    return  request({
        url:"/api/v1/system/deleteSystem",
        method:'post',
        data
    })
}


// 系统列表
export async function getSystemData(data){
    return  request({
        url:`/api/v1/system/findByPage${data}`,
        method:'get',
        data
    })
}


// 系统下拉
export async function getSystemDropDown(data){
    return  request({
        url:`/api/v1/system/dropDown${data}`,
        method:'get',
        data
    })
}




