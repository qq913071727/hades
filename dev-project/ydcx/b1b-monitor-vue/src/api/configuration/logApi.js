import request from "@/utils/request";

// 配置-日志

// 日志添加
export async function addLog(data){
    return  request({
        url:"/api/v1/log/addLog",
        method:'post',
        data
    })
}

// 日志删除
export async function delLog(data){
    return  request({
        url:"/api/v1/log/deleteLog",
        method:'post',
        data
    })
}


// 日志列表
export async function getLogData(data){
    return  request({
        url:`/api/v1/log/findByPage${data}`,
        method:'get',
        data
    })
}




