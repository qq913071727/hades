import request from "@/utils/request";

// 配置-服务

// 服务添加
export async function addService(data){
    return  request({
        url:"/api/v1/service/addService",
        method:'post',
        data
    })
}

// 服务删除
export async function delService(data){
    return  request({
        url:"/api/v1/service/deleteService",
        method:'post',
        data
    })
}


// 服务列表
export async function getServiceData(data){
    return  request({
        url:`/api/v1/service/findByPage${data}`,
        method:'get',
        data
    })
}





