import request from "@/utils/request";

// 配置-短信

// 短信添加
export async function addShortMsg(data){
    return  request({
        url:"/api/v1/cellPhone/addCellPhone",
        method:'post',
        data
    })
}

// 短信删除
export async function delShortMsg(data){
    return  request({
        url:"/api/v1/cellPhone/deleteCellPhone",
        method:'post',
        data
    })
}


// 短信列表
export async function getShortMsgData(data){
    return  request({
        url:`/api/v1/cellPhone/findByPage${data}`,
        method:'get',
        data
    })
}




