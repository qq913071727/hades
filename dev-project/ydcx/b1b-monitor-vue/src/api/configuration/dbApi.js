import request from "@/utils/request";

// 配置-mysql

//mysql添加
export async function addMySql(data){
    return  request({
        url:`/api/v1/mySql/addMySql`,
        method:'post',
        data
    })
}

//mysql删除
export async function delMySql(data){
    return  request({
        url:"/api/v1/mySql/deleteMySql",
        method:'post',
        data
    })
}


// mysql列表
export async function getMySqlData(data){
    return  request({
        url:`/api/v1/mySql/findByPage${data}`,
        method:'get',
        data
    })
}



// 配置-sqlServer

//SqlServer添加
export async function addSqlServer(data){
    return  request({
        url:`/api/v1/sqlServer/addSqlServer`,
        method:'post',
        data
    })
}

//SqlServer删除
export async function delSqlServer(data){
    return  request({
        url:"/api/v1/sqlServer/deleteSqlServer",
        method:'post',
        data
    })
}


// SqlServer列表
export async function getSqlServerData(data){
    return  request({
        url:`/api/v1/sqlServer/findByPage${data}`,
        method:'get',
        data
    })
}


// 配置-elasticsearch

//elasticsearch添加
export async function addES(data){
    return  request({
        url:`/api/v1/elasticsearch/addElasticsearch`,
        method:'post',
        data
    })
}

//elasticsearch删除
export async function delES(data){
    return  request({
        url:"/api/v1/elasticsearch/deleteElasticsearch",
        method:'post',
        data
    })
}


// elasticsearch列表
export async function getESData(data){
    return  request({
        url:`/api/v1/elasticsearch/findByPage${data}`,
        method:'get',
        data
    })
}


// 配置-Mongodb

//Mongodb添加
export async function addMongodb(data){
    return  request({
        url:`/api/v1/mongodb/addMongodb`,
        method:'post',
        data
    })
}

//Mongodb删除
export async function delMongodb(data){
    return  request({
        url:"/api/v1/mongodb/deleteMongodb",
        method:'post',
        data
    })
}


// Mongodb列表
export async function getMongodbData(data){
    return  request({
        url:`/api/v1/mongodb/findByPage${data}`,
        method:'get',
        data
    })
}


// 配置-Rabbitmq

//Rabbitmq添加
export async function addRabbitmq(data){
    return  request({
        url:`/api/v1/rabbitmq/addRabbitmq`,
        method:'post',
        data
    })
}

//Rabbitmq删除
export async function delRabbitmq(data){
    return  request({
        url:"/api/v1/rabbitmq/deleteRabbitmq",
        method:'post',
        data
    })
}


// Rabbitmq列表
export async function getRabbitmqData(data){
    return  request({
        url:`/api/v1/rabbitmq/findByPage${data}`,
        method:'get',
        data
    })
}


// 配置-redis

//redis添加
export async function addRedis(data){
    return  request({
        url:`/api/v1/redis/addRedis`,
        method:'post',
        data
    })
}

//redis删除
export async function delRedis(data){
    return  request({
        url:"/api/v1/redis/deleteRedis",
        method:'post',
        data
    })
}


// redis列表
export async function getRedisData(data){
    return  request({
        url:`/api/v1/redis/findByPage${data}`,
        method:'get',
        data
    })
}




