import request from "@/utils/request";

// 配置-邮箱

// 邮箱添加
export async function addEmail(data){
    return  request({
        url:"/api/v1/mail/addEmail",
        method:'post',
        data
    })
}

// 邮箱删除
export async function delEmail(data){
    return  request({
        url:"/api/v1/mail/deleteEmail",
        method:'post',
        data
    })
}


// 邮箱列表
export async function getEmailData(data){
    return  request({
        url:`/api/v1/mail/findByPage${data}`,
        method:'get',
        data
    })
}




