import request from '@/utils/request'

//任务查询列表带分页
export function serveTaskList(data) {
    return request({
        url:'/api/v1/serverPerformanceJobController/queryList',
        method: 'post',
        data
    })
}
// 任务新增
export function addserveTask(data) {
    return request({
        url:'/api/v1/serverPerformanceJobController/add',
        method: 'post',
        data
    })
}
// 任务查看
export function serveTaskInfo(params) {
    return request({
        url:'/api/v1/serverPerformanceJobController/info/' + params,
        method: 'get',
    })
}
// 任务编辑
export function updateserveTask(data) {
    return request({
        url:'/api/v1/serverPerformanceJobController/update',
        method: 'post',
        data
    })
}
// 任务删除
export function deletServeTask(params) {
    return request({
        url:'/api/v1/serverPerformanceJobController/delete/' + params,
        method: 'get',
    })
}
// 任务执行
export function executeTask(params) {
    return request({
        url:'/api/v1/serverPerformanceJobController/execute/' + params,
        method: 'get',
    })
}

// 日志查询列表带分页
export function serveRecordList(data) {
    return request({
        url:'/api/v1/serverPerformanceJobLogController/queryList',
        method: 'post',
        data
    })
}
// 查询报警手机号列表
export function queryAlarmCellPhoneList(data) {
    return request({
        url:'/api/v1/cellPhone/queryAlarmCellPhoneList',
        method: 'get',
        data
    })
}
// 查询报警邮箱列表
export function queryAlarmEmailList(data) {
    return request({
        url:'/api/v1/mail/queryAlarmEmailList',
        method: 'get',
        data
    })
}
// 查询服务器信息列表
export function queryServerInfoList(data) {
    return request({
        url:'/api/v1/server/queryServerInfoList',
        method: 'get',
        data
    })
}