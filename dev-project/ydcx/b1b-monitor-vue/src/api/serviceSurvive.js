import request from '@/utils/request'

// 列表
export function serviceTaskList(data) {
    return request({
        url:'/api/v1/serviceActiveJobController/queryList',
        method: 'post',
        data
    })
}
// 新增 
export function addTaskList(data) {
    return request({
        url:'/api/v1/serviceActiveJobController/add',
        method: 'post',
        data
    })
}
// 删除
export function deleteTaskList(data) {
    return request({
        url:'/api/v1/serviceActiveJobController/delete/' + data,
        method: 'post',
    })
}
// 获取数据
export function TaskInfo(data) {
    return request({
        url:'/api/v1/serviceActiveJobController/info/' + data,
        method: 'post',
    })
}
// 执行
export function executeTask(data) {
    return request({
        url:'/api/v1/serviceActiveJobController/execute/' + data,
        method: 'post',
    })
}
// 日志列表
export function servicLogList(data) {
    return request({
        url:'/api/v1/serviceActiveJobLogController/queryList',
        method: 'post',
        data
    })
}

