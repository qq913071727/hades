import request from "@/utils/request";

//报警手机号
export function phoneList(){
    return request({
        url:"/api/v1/cellPhone/queryAlarmCellPhoneList",
        method:"get",
    })
}

//报警邮箱
export function emailList(){
    return request({
        url:'/api/v1/mail/queryAlarmEmailList',
        method:"get"
    })
}

// sql derver

// sql server 任务添加
export function addSqlServerTask(data){
    return request({
        url:"/api/v1/sqlServerAvailableJobController/add",
        method:'post',
        data
    })
}

// sql server查询监控任务列表
export function sqlServerList(data){
    return request({
        url:"/api/v1/sqlServerAvailableJobController/queryList",
        method:"post",
        data
    })
}

// 删除sql server监控任务
export function delSqlServerTask(params){
    return request({
        url:"/api/v1/sqlServerAvailableJobController/delete/" + params,
        method:"get"
    })
}

// 查询sql server任务详情
export function detailSqlServerTask(params){
    return request({
        url:"/api/v1/sqlServerAvailableJobController/info/" + params,
        method:"get"
    })
}

// 编辑sql server任务
export function editSqlServerTask(data){
    return request({
        url:"/api/v1/sqlServerAvailableJobController/update",
        method:"post",
        data
    })
}

// 查询sql server日志列表
export function sqlServerRecordList(data){
    return request({
        url:"/api/v1/sqlServerAvailableJobLogController/queryList",
        method:"post",
        data
    })
}

//sql server名称列表
export function sqlServerNameList(){
    return request({
        url:"/api/v1/sqlServer/querySqlServerList",
        method:"get",
    })
}

// 执行任务接口
export function sqlServerExecute(params){
    return request({
        url:"/api/v1/sqlServerAvailableJobController/execute/" + params,
        method:"get"
    })
}
// mysql

// 查询mysql任务列表
export function  mysqlList(data){
    return request({
        url:"/api/v1/mysqlAvailableJobController/queryList",
        method:"post",
        data
    })
}

// 删除mysql监控任务
export function delmysqlTask(params){
    return request({
       url:"/api/v1/mysqlAvailableJobController/delete/" + params,
       method:"get",
    })
}

// 获取mysql监控任务详情
export function detailmysqlTask(params){
    return request({
        url:"/api/v1/mysqlAvailableJobController/info/" + params,
        method:"get"
    })
}

// 添加mysql监控任务
export function addmysqlTask(data){
    return request({
        url:"/api/v1/mysqlAvailableJobController/add",
        method:"post",
        data
    })
}

// 编辑mysql监控任务
export function editmysqlTask(data){
    return request({
        url:"/api/v1/mysqlAvailableJobController/update",
        method:"post",
        data
    })
}

// 查询mysql监控任务日志列表
export function mysqlRecordList(data){
    return request({
        url:"/api/v1/mysqlAvailableJobLogController/queryList",
        method:"post",
        data
    })
}

// 查询mysql名列表
export function mysqlNameList(){
    return request({
        url:"/api/v1/mySql/queryMysqlList",
        method:"get"
    })
}

// 执行任务接口
export function mysqlExecute(params){
    return request({
        url:"/api/v1/mysqlAvailableJobController/execute/" + params,
        method:"get"
    })
}


// elasticsearch

// 添加任务接口
export function esTaskAdd(data){
    return request({
        url:"/api/v1/elasticsearchAvailableJobController/add",
        method:"post",
        data
    })
}

// 删除任务
export function esTaskDel(params){
    return request({
        url:"/api/v1/elasticsearchAvailableJobController/delete/" + params,
        method:"get"
    })
}

// 任务详情
export function esTaskDetail(params){
    return request({
        url:"/api/v1/elasticsearchAvailableJobController/info/" + params,
        method:"get"
    })
}

// 任务列表
export function esTaskList(data){
    return request({
        url:"/api/v1/elasticsearchAvailableJobController/queryList",
        method:"post",
        data
    })
}

// 编辑任务
export function esTaskEdit(data){
    return request({
        url:"/api/v1/elasticsearchAvailableJobController/update",
        method:"post",
        data
    })
}

// 执行任务
export function esExecute(params){
    return request({
        url:"/api/v1/elasticsearchAvailableJobController/execute/" + params,
        method:"get",
    })
}
// es日志列表
export function esRecordList(data){
    return request({
        url:"/api/v1/elasticsearchAvailableJobLogController/queryList",
        method:"post",
        data
    })
}

// es名称列表
export function esNameList(){
    return request({
        url:"/api/v1/elasticsearch/queryElasticsearchInfoList",
        method:"get"
    })
}

// mongodb

// 添加任务
export function mongodbTaskAdd(data){
    return request({
        url:"/api/v1/mongodbAvailableJobController/add",
        method:"post",
        data
    })
}

// 编辑任务
export function mongodbTaskEdit(data){
    return request({
        url:"/api/v1/mongodbAvailableJobController/update",
        method:"post",
        data
    })
}

// 删除任务
export function mongodbTaskDel(params){
    return request({
        url:"/api/v1/mongodbAvailableJobController/delete/" + params,
        method:"get",
    })
}

// 任务详情
export function mongodbTaskDetail(params){
    return request({
        url:"/api/v1/mongodbAvailableJobController/info/" + params,
        method:"get"
    })
}

// 任务列表
export function mongodbTaskList(data){
    return request({
        url:"/api/v1/mongodbAvailableJobController/queryList",
        method:"post",
        data
    })
}

// 执行任务
export function mongodbExecute(params){
    return request({
        url:"/api/v1/mongodbAvailableJobController/execute/" + params,
        method:"get"
    })
}

// 日志列表
export function mongodbRecordList(data){
    return request({
        url:"/api/v1/mongodbAvailableJobLogController/queryList",
        method:"post",
        data
    })
}

// mongodb名称
export function mongodbNameList(){
    return request({
        url:"/api/v1/mongodb/queryMongodbInfoList",
        method:"get"
    })
}
// rabbitmq

// 新增任务
export function rabbitmqTaskAdd(data){
    return request({
        url:"/api/v1/rabbitmqAvailableJobController/add",
        method:"post",
        data
    })
}

// 编辑任务
export function rabbitmqTaskEdit(data){
    return request({
        url:"/api/v1/rabbitmqAvailableJobController/update",
        method:"post",
        data
    })
}

// 任务列表
export function rabbitmqTaskList(data){
    return request({
        url:"/api/v1/rabbitmqAvailableJobController/queryList",
        method:"post",
        data
    })
}

// 删除任务
export function rabbitmqTaskDel(params){
    return request({
        url:"/api/v1/rabbitmqAvailableJobController/delete/" + params,
        method:"get",
    })
}

// 任务详情
export function rabbitmqTaskDetail(params){
    return request({
        url:"/api/v1/rabbitmqAvailableJobController/info/" + params,
        method:"get",
    })
}

// 执行
export function rabbitmqTaskExecute(params){
    return request({
        url:"/api/v1/rabbitmqAvailableJobController/execute/" + params,
        method:"get",
    })
}

// 日志列表
export function rabbitmqRecordList(data){
    return request({
        url:"/api/v1/rabbitmqAvailableJobLogController/queryList",
        method:"post",
        data
    })
}

// rabbitmq名称
export function rabbitmqNameList(){
    return request({
        url:"/api/v1/rabbitmq/queryRabbitmqList",
        method:"get"
    })
}

// redis

//新增任务 
export function redisTaskAdd(data){
    return request({
        url:"/api/v1/redisAvailableJobController/add",
        method:"post",
        data
    })
}

// 编辑任务
export function redisTaskEdit(data){
    return request({
        url:"/api/v1/redisAvailableJobController/update",
        method:"post",
        data
    })
}

// 任务列表
export function redisTaskList(data){
    return request({
        url:"/api/v1/redisAvailableJobController/queryList",
        method:"post",
        data
    })
}

// 删除任务
export function redisTaskDel(params){
    return request({
        url:"/api/v1/redisAvailableJobController/delete/" + params,
        method:"get",
    })
}

// 任务详情
export function redisTaskDetail(params){
    return request({
        url:"/api/v1/redisAvailableJobController/info/" + params,
        method:"get",
    })
}

// 执行
export function redisTaskExecute(params){
    return request({
        url:"/api/v1/redisAvailableJobController/execute/" + params,
        method:"get",
    })
}

// 日志列表
export function redisRecordList(data){
    return request({
        url:"/api/v1/redisAvailableJobLogController/queryList",
        method:"post",
        data
    })
}

// redis名称
export function redisNameList(){
    return request({
        url:"/api/v1/redis/queryRedisList",
        method:"get"
    })
}
