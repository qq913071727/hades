import request from "@/utils/request";

// 新增任务
export function joggleAdd(data){
    return request({
        url:"/api/v1/ApiAvailableJobController/add",
        method:"post",
        data
    })
}
// 编辑任务
export function joggleEdit(data){
    return request({
        url:"/api/v1/ApiAvailableJobController/update",
        method:"post",
        data
    })
}
// 任务详情
export function joggleDetail(id,isMain){
    return request({
        url:`/api/v1/ApiAvailableJobController/info/${id}/${isMain}`,
        method:"get"
    })
}
// 任务列表
export function joggleList(data){
    return request({
        url:"/api/v1/ApiAvailableJobController/queryList",
        method:"post",
        data
    })
}
// 执行任务
export function joggleExecute(params){
    return request({
        url:"/api/v1/ApiAvailableJobController/execute/" + params,
        method:"get"
    })
}
// 删除任务
export function joggleDel(params){
    return request({
        url:"/api/v1/ApiAvailableJobController/delete/" + params,
        method:"get"
    })
}
// 系统名称
export function systemFind(params) {
    return request({
        url:'/api/v1/system/findByPage'+ params,
        method: 'get',
        
    })
}
// 环境名称
export function environmentData(params) {
    return request({
        url:'/api/v1/environment/dropDown'+ params,
        method: 'get',
        
    })
}
// 服务名称
export function serviceData(params) {
    return request({
        url:'/api/v1/service/findByPage'+ params,
        method: 'get',
        
    })
}
// 接口列表
export function apiData(params){
    return  request({
        url:'/api/v1/apiController/findByPage' + params,
        method:'get',
    })
}
//报警手机号
export function phoneList(){
    return request({
        url:"/api/v1/cellPhone/queryAlarmCellPhoneList",
        method:"get",
    })
}

//报警邮箱
export function emailList(){
    return request({
        url:'/api/v1/mail/queryAlarmEmailList',
        method:"get"
    })
}

// 日志列表
export function logList(data){
    return request({
        url:"/api/v1/ApiAvailableJobLogController/queryList",
        method:"post",
        data
    })
}