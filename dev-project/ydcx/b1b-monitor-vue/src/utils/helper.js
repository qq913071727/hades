/**
     * js将post请求的参数对象转换成get的形式拼接在url上
     * @param param
     * @returns {string}
     */
export const changeParam = (param) => {
    return JSON.stringify(param).replace(/:/g, '=').replace(/,/g, '&').replace(/{/g, '?').replace(/}/g, '').replace(/"/g, '');
}
