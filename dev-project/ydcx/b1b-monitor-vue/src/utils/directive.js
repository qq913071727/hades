import { addResizeListener, removeResizeListener } from 'element-ui/src/utils/resize-event'
import vue from 'vue'
const doResize = async(el, binding, vnode) => {
    const { componentInstance: $table } = await vnode
    const { value } = binding
    if (!$table.height) {
        throw new Error(`el-$table must set the height. Such as height='100px'`)
    }
    const bottomOffset = (value && value.bottomOffset) || 30
    if (!$table) return
    const height = window.innerHeight - el.getBoundingClientRect().top - bottomOffset
    $table.layout.setHeight(height)
    $table.doLayout()

}
vue.directive('adaptive', {
    bind(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            el.resizeListener = async() => {
                await doResize(el, binding, vnode)
            }
            addResizeListener(el, el.resizeListener)
            addResizeListener(window.document.body, el.resizeListener)
        }
    },
    async inserted(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            await doResize(el, binding, vnode)
        }
    },
    async componentUpdated(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            await doResize(el, binding, vnode)
        }
    },
    unbind(el) {
        removeResizeListener(el, el.resizeListener)
    },
})