export const interFaceColumns = [
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "服务名称", value: "serviceName" },
    { key: "接口名称", value: "apiName" },
    { key: "是否启用", value: "enable" },
    // { key: "任务处理器", value: "taskProcessor" },
    { key: "调度表达式", value: "scheduleExpression" },
    { key: "报警邮箱", value: "alarmEmail" },
    { key: "报警手机号", value: "alarmCellPhone" },
    // { key: "ip", value: "ip" },
    // { key: "端口号", value: "port" },
    // { key: "用户名", value: "userName" },
    // { key: "密码", value: "password" },
    { key: "正则表达式", value: "responseSuccessRegularExpression" },
    { key: "创建时间", value: "createTime" },
]

export const interFaceTableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "系统名称", prop: "systemName", width: 150 },
        { sort: null, label: "环境名称", prop: "environmentName", width: 150 },
        { sort: null, label: "服务名称", prop: "serviceName", width: 150 },
        { sort: null, label: "接口名称", prop: "apiName", width: 150 },
        { sort: null, label: "是否启用", prop: "enable",type: "slot", width: 80 },
        // {
        //     sort: null,
        //     label: "任务处理器",
        //     prop: "taskProcessor",
        //     width: 120,
        // },
        { sort: null, label: "调度表达式", prop: "scheduleExpression", width: 150 },
        { sort: null, label: "报警邮箱", prop: "alarmEmail", width: 150 },
        { sort: null, label: "报警手机号", prop: "alarmCellPhone", width: 150 },

        // { sort: null, label: "ip", prop: "ip", width: 120 },
        // { sort: null, label: "端口", prop: "port", width: 120 },
        // { sort: null, label: "用户名", prop: "userName", width: 120 },
        // { sort: null, label: "密码", prop: "password", width: 120 },
        { sort: null, label: "正则表达式", prop: "responseSuccessRegularExpression", width: 150,type:'slot' },
        { sort: null, label: "创建时间", prop: "createTime", width: 200 },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            fixed:'right',
            width: 200,
        },
    ],
    // checkedAll: false, //全选所有
    // checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 0,
    total: 0,
}


export const serviceColumns = [
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "服务名称", value: "serviceName" },
    { key: "是否启用", value: "enable" },
    // { key: "任务处理器", value: "taskProcessor" },
    { key: "调度表达式", value: "scheduleExpression" },
    { key: "报警邮箱", value: "alarmEmail" },
    { key: "报警手机号", value: "alarmCellPhone" },
    { key: "ip", value: "ip" },
    { key: "端口号", value: "port" },
    { key: "用户名", value: "username" },
    { key: "密码", value: "password" },
    { key: "正则表达式", value: "processRegularExpression" },
    { key: "创建时间", value: "createTime" },
]
// 服务器监控任务查看
export const serviceLookColumns = [
    { key: "服务器名称", value: "name" },
    { key: "调度表达式", value: "scheduleExpression" },
    // { key: "任务处理器", value: "jobHandler" },
    { key: "是否启用", value: "enable" },
    { key: "报警邮箱", value: "alarmEmail" },
    { key: "报警手机号", value: "alarmCellPhone" },
    { key: "创建时间", value: "createTime" },
]

export const serviceTableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "系统名称", prop: "systemName", width: 120 },
        { sort: null, label: "环境名称", prop: "environmentName", width: 100 },
        { sort: null, label: "服务名称", prop: "serviceName", width: 120 },
        { sort: null, label: "是否启用", prop: "enable", width: 80,type:'slot' },
        { sort: null, label: "调度表达式", prop: "scheduleExpression", width: 120 },
        { sort: null, label: "报警邮箱", prop: "alarmEmail", width: 120 },
        { sort: null, label: "报警手机号", prop: "alarmCellPhone", width: 120 },
        { sort: null, label: "ip", prop: "ip", width: 120 },
        { sort: null, label: "端口", prop: "port", },
        { sort: null, label: "用户名", prop: "username", },
        { sort: null, label: "正则表达式", prop: "processRegularExpression", width: 120 },
        { sort: null, label: "创建时间", prop: "createTime", width: 180 },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            fixed:'right',
            width: 200,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 1,
    total: 3,
}

