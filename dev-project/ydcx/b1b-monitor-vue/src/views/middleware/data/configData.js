//中间件监控sql server查看
export const middleSqlServerColumns =[
    {key:"sql server名称", value:"name"},
    {key:"调用表达式", value:"scheduleExpression"},
    // {key:"任务处理器", value:"jobHandler"},
    {key:"是否启用", value:"enable"},
    {key:"报警邮箱", value:"alarmEmail"},
    {key:"报警手机号", value:"alarmCellPhone"},
    {key:"创建时间", value:"createTime"},
]
//中间件监控mysql查看
export const middleMysqlColumns =[
    {key:"mysql名称", value:"name"},
    {key:"调用表达式", value:"scheduleExpression"},
    // {key:"任务处理器", value:"jobHandler"},
    {key:"是否启用", value:"enable"},
    {key:"报警邮箱", value:"alarmEmail"},
    {key:"报警手机号", value:"alarmCellPhone"},
    {key:"创建时间", value:"createTime"},
]

// elasticsearch查看
export const elasticsearchColumns =[
    {key:"es名称", value:"name"},
    {key:"调用表达式", value:"scheduleExpression"},
    // {key:"任务处理器", value:"jobHandler"},
    {key:"是否启用", value:"enable"},
    {key:"报警邮箱", value:"alarmEmail"},
    {key:"报警手机号", value:"alarmCellPhone"},
    {key:"创建时间", value:"createTime"},
]

// mongodb查看
export const mongodbColumns =[
    {key:"mongodb名称", value:"name"},
    {key:"调用表达式", value:"scheduleExpression"},
    // {key:"任务处理器", value:"jobHandler"},
    {key:"是否启用", value:"enable"},
    {key:"报警邮箱", value:"alarmEmail"},
    {key:"报警手机号", value:"alarmCellPhone"},
    {key:"创建时间", value:"createTime"},
]

// rabbitmq查看
export const rabbitmqColumns =[
    {key:"rabbitmq名称", value:"name"},
    {key:"调用表达式", value:"scheduleExpression"},
    // {key:"任务处理器", value:"jobHandler"},
    {key:"是否启用", value:"enable"},
    {key:"报警邮箱", value:"alarmEmail"},
    {key:"报警手机号", value:"alarmCellPhone"},
    {key:"创建时间", value:"createTime"},
]

// redis查看
export const redisColumns =[
    {key:"redis名称", value:"name"},
    {key:"调用表达式", value:"scheduleExpression"},
    // {key:"任务处理器", value:"jobHandler"},
    {key:"是否启用", value:"enable"},
    {key:"报警邮箱", value:"alarmEmail"},
    {key:"报警手机号", value:"alarmCellPhone"},
    {key:"创建时间", value:"createTime"},
]