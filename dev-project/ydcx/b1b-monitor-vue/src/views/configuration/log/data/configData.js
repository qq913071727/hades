export const columns = [
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "服务名称", value: "serviceName" },
    { key: "日志名称", value: "name" },
    { key: "日志文件路径的正则表达式", value: "fileRegularExpression" },
    { key: "关键字列表", value: "keyWordList" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "系统名称", prop: "systemName", },
        { sort: null, label: "环境名称", prop: "environmentName", },
        { sort: null, label: "服务名称", prop: "serviceName", },
        { sort: null, label: "日志名称", prop: "name", },
        { sort: null, label: "日志文件路径的正则表达式", prop: "fileRegularExpression", },
        { sort: null, label: "关键字列表", prop: "keyWordList", },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 180,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

