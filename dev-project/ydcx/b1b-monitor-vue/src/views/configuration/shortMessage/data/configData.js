export const columns = [
    { key: "手机号", value: "cellPhone" },
    { key: "真实姓名", value: "realName" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        { sort: null, label: "手机号", prop: "cellPhone", width: 'auto' },
        { sort: null, label: "真实姓名", prop: "realName", width: 'auto' },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 250,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

