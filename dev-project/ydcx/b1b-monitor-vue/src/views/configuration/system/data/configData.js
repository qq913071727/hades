export const columns = [
    { key: "系统名称", value: "name" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "系统名称", prop: "name", width: 'auto' },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 250,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 0,
}

