export const columns = [
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "服务名称", value: "serviceName" },
    { key: "接口名称", value: "name" },

    { key: "url", value: "url" },
    { key: "方法", value: "methodName" },
    { key: "参数类型", value: "parameterTypeName" },
    { key: "参数", value: "parameter" },
    { key: "header中的参数", value: "header" },
    { key: "接口调用成功的标准", value: "responseSuccessStandardName" },
    { key: "判断接口调用成功的正则表达式", value: "responseSuccessRegularExpression" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "系统名称", prop: "systemName", },
        { sort: null, label: "环境名称", prop: "environmentName", },
        { sort: null, label: "服务名称", prop: "serviceName",  },
        { sort: null, label: "接口名称", prop: "name", },

        { sort: null, label: "url", prop: "url", width: 100 },
        { sort: null, label: "方法", prop: "methodName",  },
        { sort: null, label: "参数类型", prop: "parameterTypeName", },
        { sort: null, label: "参数", prop: "parameter", },
        { sort: null, label: "header中的参数", prop: "header", },
        { sort: null, label: "接口调用成功的标准", prop: "responseSuccessStandardName",  },
        { sort: null, label: "判断接口调用成功的正则表达式", prop: "responseSuccessRegularExpression",  },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 100,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

