export const columns = [
    { key: "redis名称", value: "name" },
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "数据库", value: "redisDatabase" },
    { key: "节点", value: "nodeName" },
    // { key: "密码", value: "password" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        { sort: null, label: "redis名称", prop: "name",width: 200 },
        { sort: null, label: "系统名称", prop: "systemName", width: 'auto' },
        { sort: null, label: "环境名称", prop: "environmentName", width: 'auto' },
        { sort: null, label: "数据库", prop: "redisDatabase", width: 120 },
        { sort: null, label: "节点", prop: "nodeName", width: 150 },
        // { sort: null, label: "密码", prop: "password", width: 'auto' },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 150,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 0,
}

