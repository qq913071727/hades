export const columns = [
    { key: "es名称", value: "name" },
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "ip", value: "ip" },
    { key: "端口", value: "port" },
    { key: "索引", value: "esIndex" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        { sort: null, label: "es名称", prop: "name", width: 120 },
        { sort: null, label: "系统名称", prop: "systemName", width: 'auto' },
        { sort: null, label: "环境名称", prop: "environmentName", width: 'auto' },
        { sort: null, label: "ip", prop: "ip", },
        { sort: null, label: "端口", prop: "port", width: 'auto' },
        { sort: null, label: "索引", prop: "esIndex", width: 'auto' },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 150,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

