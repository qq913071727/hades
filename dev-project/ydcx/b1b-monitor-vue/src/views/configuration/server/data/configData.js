export const columns = [
    { key: "服务器名称", value: "name" },
    { key: "操作系统", value: "systemOperationName" },
    { key: "IP", value: "ip" },
    { key: "ssh协议端口", value: "sshPort" },
    { key: "账号", value: "userName" },
    // { key: "密码", value: "password" },
    { key: "描述", value: "description" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "服务器名称", prop: "name",width:100,},
        { sort: null, label: "操作系统", prop: "systemOperationName", },
        { sort: null, label: "IP", prop: "ip",  },
        { sort: null, label: "ssh协议端口", prop: "sshPort",  },
        { sort: null, label: "账号", prop: "userName",  },
        // { sort: null, label: "密码", prop: "password", width: 'auto' },
        { sort: null, label: "描述", prop: "description",width:180 },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 130,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

