export const columns = [
    { key: "邮箱", value: "email" },
    { key: "真实姓名", value: "realName" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        // { sort: null, label: "序号", prop: "id", width: 80 },
        { sort: null, label: "邮箱", prop: "email", width: 'auto' },
        { sort: null, label: "真实姓名", prop: "realName", width: 'auto' },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 250,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

