export const columns = [
    { key: "名称", value: "name" },
    { key: "系统名称", value: "systemName" },
    { key: "环境名称", value: "environmentName" },
    { key: "url", value: "url" },
    { key: "用户名", value: "userName" },
    // { key: "密码", value: "password" },
]

export const tableconfig = {
    loading: false,
    isselection: false,
    ispagination: true,
    isfixed: true,
    tabledata: [],
    isindex: true,
    thead: [
        { sort: null, label: "名称", prop: "name", width: 120 },
        { sort: null, label: "系统名称", prop: "systemName", width: 'auto' },
        { sort: null, label: "环境名称", prop: "environmentName", width: 'auto' },
        { sort: null, label: "url", prop: "url", width: 250 },
        { sort: null, label: "用户名", prop: "userName", width: 'auto' },
        // { sort: null, label: "密码", prop: "password", width: 'auto' },
        {
            sort: null,
            label: "操作",
            prop: "operation",
            type: "slot",
            width: 130,
        },
    ],
    // checkedAll: false, //全选所有
    checkedPage: false, //全选本页
    currentpage: 1,
    pagesize: 10,
    total: 3,
}

