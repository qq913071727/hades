//package com.innovation.ic.im.end.gateway.config;
//
//import org.springframework.cache.annotation.CachingConfigurerSupport;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.serializer.RedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
///**
// * redis配置类
// */
//@Configuration
////@EnableCaching
//public class RedisConfig extends CachingConfigurerSupport {
//
//    @Bean
//    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
//        RedisSerializer stringSerializer = new StringRedisSerializer();
//        redisTemplate.setKeySerializer(stringSerializer);
//        redisTemplate.setStringSerializer(stringSerializer);
//        redisTemplate.setValueSerializer(stringSerializer);
//        redisTemplate.setHashKeySerializer(stringSerializer);
//        redisTemplate.setHashValueSerializer(stringSerializer);
//        return redisTemplate;
//    }
//
////    @Bean
////    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
//////        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
//////        template.setConnectionFactory(factory);
////////        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//////        ObjectMapper om = new ObjectMapper();
//////        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//////        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
////////        jackson2JsonRedisSerializer.setObjectMapper(om);
//////        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
//////        //key采用String的序列化方式
//////        template.setKeySerializer(stringRedisSerializer);
//////        //hash的key也采用String的序列化方式
//////        template.setHashKeySerializer(stringRedisSerializer);
//////        //value序列化方式采用jackson
//////        template.setValueSerializer(stringRedisSerializer);
//////        //hash的value序列化方式采用jackson
//////        template.setHashValueSerializer(stringRedisSerializer);
//////        template.afterPropertiesSet();
////
////        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
////        template.setConnectionFactory(factory);
////        RedisSerializer stringSerializer = new StringRedisSerializer();
////        template.setKeySerializer(stringSerializer);
////        template.setValueSerializer(stringSerializer);
////        template.setHashKeySerializer(stringSerializer);
////        template.setHashValueSerializer(stringSerializer);
////        return template;
////    }
//
//}