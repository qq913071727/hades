package com.innovation.ic.im.end.gateway;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = {"com.innovation.ic.im.end"})
@MapperScan("com.innovation.ic.im.end.base.mapper")
//@EnableZuulProxy
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {DruidDataSourceAutoConfigure.class})
public class ImEndGatewayApplication {

    private static final Logger log = LoggerFactory.getLogger(ImEndGatewayApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(ImEndGatewayApplication.class, args);
        log.info("im-end-gateway服务启动成功");
//        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
    }
}
