package com.innovation.ic.im.end.base.thread.web;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.im.end.base.handler.helper.HandlerHelper;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.thread.AbstractThread;

import java.util.List;

/**
 * 推送rabbitMq消息
 */
public class SendWithdrawRabbitMqMessageThread extends AbstractThread implements Runnable {

    private RetractionPojo retractionPojo;

    private String exchange;

    private List<String> refGroupAccountList;

    public SendWithdrawRabbitMqMessageThread() {
    }

    public SendWithdrawRabbitMqMessageThread(List<String> refGroupAccountList, RetractionPojo retractionPojo, String exchange) {
        this.refGroupAccountList = refGroupAccountList;
        this.retractionPojo = retractionPojo;
        this.exchange = exchange;
    }

    @Override
    public void run() {
        if (retractionPojo != null && refGroupAccountList != null && refGroupAccountList.size() > 0) {
            for (int i = 0; i < refGroupAccountList.size(); i++) {
                String username = refGroupAccountList.get(i);
                HandlerHelper.getRabbitMqHandler().sendRabbitMqMsg(username, JSON.toJSONString(retractionPojo), exchange);
            }
        }
    }
}