package com.innovation.ic.im.end.base.thread.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessageReceiver;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccount;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.enums.AvailableStatusEnum;
import com.innovation.ic.im.end.base.pojo.enums.ReadStatusEnum;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountGroupLastContactPojo;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import lombok.SneakyThrows;
import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.List;

/**
 * @desc   保存群组未读消息数据
 * @author linuo
 * @time   2022年10月17日15:31:36
 */
public class SaveGroupMessageReceiverThread extends AbstractThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Integer groupMessageId;

    private String fromUserAccount;

    private Long waitingLockTime;

    private String exchange;

    private List<String> onlineAccountList;

    public SaveGroupMessageReceiverThread(GroupMessage groupMessage, List<String> onlineAccountList, Integer groupMessageId, String fromUserAccount, CuratorFramework curatorFramework, String exchange, Long waitingLockTime) {
        this.groupMessage = groupMessage;
        this.onlineAccountList = onlineAccountList;
        this.groupMessageId = groupMessageId;
        this.fromUserAccount = fromUserAccount;
        this.curatorFramework = curatorFramework;
        this.exchange = exchange;
        this.waitingLockTime = waitingLockTime;
    }

    @SneakyThrows
    @Override
    public void run() {
        // 保存群组未读消息数据并推送rabbitMq消息
        ServiceResult<List<RefGroupAccount>> serviceResult = ServiceImplHelper.getRefGroupAccountService().findByGroupIdAndNotInList(groupMessage.getGroupId(), onlineAccountList);
        if (null != serviceResult && null != serviceResult.getResult()) {
            List<RefGroupAccount> refGroupAccountList = serviceResult.getResult();
            logger.info("需要给群组refGroupAccountList:[{}]中的用户记录未读消息", JSONObject.toJSONString(refGroupAccountList));
            for (RefGroupAccount refGroupAccount : refGroupAccountList) {
                GroupMessageReceiver groupMessageReceiver = new GroupMessageReceiver();
                groupMessageReceiver.setGroupId(groupMessage.getGroupId());
                groupMessageReceiver.setGroupMessageId(groupMessageId);
                groupMessageReceiver.setToUserAccount(refGroupAccount.getUsername());
                groupMessageReceiver.setToUserRealName(getUserRealNameByAccount(refGroupAccount.getUsername()));
                groupMessageReceiver.setRead(ReadStatusEnum.UN_READ.getCode());
                groupMessageReceiver.setToUserAvailable(AvailableStatusEnum.AVAILABLE.getCode());
                ServiceImplHelper.getGroupMessageReceiverService().saveGroupMessageReceiver(groupMessageReceiver);

                // 处理账号和群组的操作表数据
                ServiceImplHelper.getRefGroupAccountOperationService().handleRefGroupAccountOperationData(groupMessage.getGroupId(), refGroupAccount.getUsername(), curatorFramework, waitingLockTime);
            }

            // 推送rabbitMq消息
            // 查询当前默认群组中的用户账号
            ServiceResult<List<String>> result = ServiceImplHelper.getRefGroupAccountService().getGroupAccounts(groupMessage.getGroupId());
            if(result != null && result.getResult() != null){
                List<String> userNames = result.getResult();
                for (String userName : userNames) {
                    // 推送rabbitMq消息
                    ServiceResult<List<AccountGroupLastContactPojo>> lastContact = ServiceImplHelper.getUserGroupService().findLastContact(userName);
                    if (lastContact != null && lastContact.getResult() != null) {
                        JSONObject json = (JSONObject) JSONObject.parse(JSON.toJSONString(lastContact));

                        // 如果自己发的消息，并且推送给自己，说明没有新消息
                        if (fromUserAccount.equals(userName)) {
                            json.put(Constants.IF_HAVE_NEW_MSG_FIELD, 0);
                        } else {
                            json.put(Constants.IF_HAVE_NEW_MSG_FIELD, 1);
                        }

                        // 推送消息
                        logger.info("将消息推送给用户:[{}]的mq队列中,内容为:[{}]", userName, json.toJSONString());
                        try {
                            ServiceImplHelper.getRabbitMqManager().basicPublish(exchange, userName, null, json.toJSONString().getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}