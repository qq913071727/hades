package com.innovation.ic.im.end.base.thread.data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import lombok.SneakyThrows;

/**
 * @desc   发送rabbitMq消息到用户，更新个人账号信息
 * @author linuo
 * @time   2022年8月11日10:47:48
 */
public class SendRabbitMqMsgUpdateAccountDataThread extends AbstractThread implements Runnable{

    public SendRabbitMqMsgUpdateAccountDataThread() {
    }

    public SendRabbitMqMsgUpdateAccountDataThread(Account account, RabbitMqHandler rabbitMqHandler, RabbitMqParamConfig rabbitMqParamConfig) {
        this.rabbitMqHandler = rabbitMqHandler;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
        this.account = account;
    }

    @SneakyThrows
    @Override
    public void run() {
        Thread.sleep(3000);
        if(account != null){
            JSONObject json = (JSONObject) JSON.toJSON(account);
            rabbitMqHandler.sendRabbitMqMsg(account.getUsername(), json.toJSONString(), rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.ACCOUNT_UPDATE_EXCHANGE));
        }
    }
}