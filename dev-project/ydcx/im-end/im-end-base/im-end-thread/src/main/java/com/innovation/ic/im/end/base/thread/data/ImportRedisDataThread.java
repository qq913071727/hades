package com.innovation.ic.im.end.base.thread.data;

import com.innovation.ic.im.end.base.thread.AbstractThread;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @desc   执行脚本导入redis数据
 * @author linuo
 * @time   2023年4月24日15:21:45
 */
public class ImportRedisDataThread extends AbstractThread implements Runnable{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String[] scripts;

    public ImportRedisDataThread(String[] scripts) {
        this.scripts = scripts;
    }

    @SneakyThrows
    @Override
    public void run() {
        // 休眠3分钟，防止出现服务未注册到eureka导致调用接口失败的问题
        Thread.sleep(1000 * 60 * 3);
        if(scripts != null){
            logger.info("-----开始执行命令-----");
            for(String str : scripts){
                logger.info("执行脚本 -> {}", str);
                Thread.sleep(1000 * 2);
                try {
                    Process exec = Runtime.getRuntime().exec(str);
                    exec.waitFor();
                }catch (Exception e){
                    logger.error("执行脚本[{}]出现问题,原因:", str, e);
                }
            }
            logger.info("-----执行命令结束-----");
        }
    }
}