package com.innovation.ic.im.end.base.thread.web;

import com.innovation.ic.im.end.base.handler.helper.HandlerHelper;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import java.util.List;

/**
 * 一对一聊天环境下，推送给rabbitMq未读状态修改为已读状态的消息id
 */
public class SendRabbitMqReadMsgThread extends AbstractThread implements Runnable {
    // 接收消息的用户
    private String fromUserAccount;

    private List<String> msgIds;

    private String exchange;

    public SendRabbitMqReadMsgThread() {
    }

    public SendRabbitMqReadMsgThread(String fromUserAccount, String exchange, List<String> msgIds) {
        this.fromUserAccount = fromUserAccount;
        this.exchange = exchange;
        this.msgIds = msgIds;
    }

    @Override
    public void run() {
        if(msgIds != null && msgIds.size() > 0){
            for(String msgId : msgIds){
                HandlerHelper.getRabbitMqHandler().sendRabbitMqMsg(fromUserAccount, msgId, exchange);
            }
        }
    }
}