package com.innovation.ic.im.end.base.thread.web;

import com.innovation.ic.im.end.base.pojo.constant.UserLoginType;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * 点对点用户信息存取
 */
public class SaveUserLoginLogThread extends AbstractThread implements Runnable {
    private String username;
    private String groupId;
    private Integer type;
    private Integer userGroupId;
    private String ipAddr;

    public SaveUserLoginLogThread(String username, Integer type, String ipAddr) {
        this.username = username;
        this.type = type;
        this.ipAddr = ipAddr;
    }

    public SaveUserLoginLogThread(String username, String groupId, Integer type, String ipAddr) {
        this.username = username;
        this.groupId = groupId;
        this.type = type;
        this.ipAddr = ipAddr;
    }

    public SaveUserLoginLogThread(String username, Integer userGroupId, Integer type, String ipAddr) {
        this.username = username;
        this.userGroupId = userGroupId;
        this.type = type;
        this.ipAddr = ipAddr;
    }

    @Override
    public void run() {
        if (this.type == UserLoginType.POINT_TO_POINT) {
            ServiceImplHelper.getUserLoginLogService().saveUserData(this.username, this.type, this.ipAddr);
        } else if (this.type == UserLoginType.DEFAULT_GROUP) {
            ServiceImplHelper.getUserLoginLogService().saveDefaultUserData(this.username, this.groupId, this.type, this.ipAddr);
        } else {
            String userGroupId = this.userGroupId.toString();
            ServiceImplHelper.getUserLoginLogService().saveCustomUserData(this.username, userGroupId, this.type, this.ipAddr);
        }

    }
}
