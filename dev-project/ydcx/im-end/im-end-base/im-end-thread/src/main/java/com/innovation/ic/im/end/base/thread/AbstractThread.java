package com.innovation.ic.im.end.base.thread;

import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.handler.im_erp9.GroupMessageHandler;
import com.innovation.ic.im.end.base.handler.im_erp9.MessageHandler;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.handler.im_erp9.UserGroupMessageHandler;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9AdminsService;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9StructuresService;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.service.im_erp9.*;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.base.value.config.SystemConfig;
import com.innovation.ic.im.end.base.value.config.ZookeeperParamConfig;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupMessageVo;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import com.innovation.ic.im.end.base.vo.im_erp9.MessageVo;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupMessageVo;
import com.rabbitmq.client.Channel;
import org.apache.curator.framework.CuratorFramework;

/**
 * @desc   抽象Thread
 * @author linuo
 * @time   2022年10月18日14:04:38
 */
public abstract class AbstractThread{
    protected GroupRetractionVo groupRetractionVo;

    protected UserGroupMessageVo userGroupMessageVo;

    protected GroupMessageVo groupMessageVo;

    protected MessageVo messageVo;

    protected GroupMessageReceiverService groupMessageReceiverService;

    protected UserGroupMessageReceiverService userGroupMessageReceiverService;

    protected UserGroupService userGroupService;

    protected AccountService accountService;

    protected AccountRelationService accountRelationService;

    protected Erp9AdminsService erp9AdminsService;

    protected Erp9StructuresService erp9StructuresService;

    protected GroupService groupService;

    protected ChatPairService chatPairService;

    protected TempChatPairService tempChatPairService;

    protected TreeNodeService treeNodeService;

    protected GroupMessage groupMessage;

    protected UserGroupMessage userGroupMessage;

    protected Message message;

    protected Account account;

    protected UserGroupMessageHandler userGroupMessageHandler;

    protected GroupMessageHandler groupMessageHandler;

    protected MessageHandler messageHandler;

    protected RabbitMqHandler rabbitMqHandler;

    protected Channel channel;

    protected RabbitMqParamConfig rabbitMqParamConfig;

    protected ZookeeperParamConfig zookeeperParamConfig;

    protected ThreadPoolManager threadPoolManager;

    protected RedisManager redisManager;

    protected CuratorFramework curatorFramework;

    protected SystemConfig systemConfig;

    /**
     * 根据用户账号获取用户真实姓名
     * @param accountId 用户账号
     * @return 返回用户真实姓名
     */
    protected String getUserRealNameByAccount(String accountId){
        String userRealName = null;
        ServiceResult<Account> account = ServiceImplHelper.getAccountService().findByAccount(accountId);
        if(account.getResult() != null && !Strings.isNullOrEmpty(account.getResult().getRealName())){
            userRealName = account.getResult().getRealName();
        }
        return userRealName;
    }
}