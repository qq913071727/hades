package com.innovation.ic.im.end.base.thread.data;

import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.handler.im_erp9.UserGroupMessageHandler;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * 向表user_group_message中插入数据，并同步到redis中
 */
public class InsertUserGroupMessageThread extends AbstractThread implements Runnable{

    public InsertUserGroupMessageThread() {
    }

    public InsertUserGroupMessageThread(UserGroupMessageHandler userGroupMessageHandler, UserGroupMessage userGroupMessage) {
        this.userGroupMessageHandler = userGroupMessageHandler;
        this.userGroupMessage = userGroupMessage;
    }

    @Override
    public void run() {
        userGroupMessageHandler.handleInsertUserGroupMessage(userGroupMessage);
    }
}
