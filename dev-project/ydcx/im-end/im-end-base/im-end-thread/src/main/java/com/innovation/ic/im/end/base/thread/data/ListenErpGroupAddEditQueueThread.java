package com.innovation.ic.im.end.base.thread.data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqConstants;
import com.innovation.ic.im.end.base.service.im_erp9.AccountService;
import com.innovation.ic.im.end.base.service.im_erp9.GroupService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

/**
 * @desc   监听erp部门新增、修改默认群组队列
 * @author linuo
 * @time   2022年10月21日13:10:49
 */
public class ListenErpGroupAddEditQueueThread extends AbstractThread implements Runnable{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /** erp交换机 */
    private String exchange;

    public ListenErpGroupAddEditQueueThread(Channel channel, String exchange, GroupService groupService, AccountService accountService, RabbitMqHandler rabbitMqHandler, RabbitMqParamConfig rabbitMqParamConfig, ThreadPoolManager threadPoolManager) {
        this.channel = channel;
        this.exchange = exchange;
        this.rabbitMqHandler = rabbitMqHandler;
        this.groupService = groupService;
        this.accountService = accountService;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
        this.threadPoolManager = threadPoolManager;
    }

    @SneakyThrows
    @Override
    public void run() {
        String routingKey = RabbitMqConstants.ERP_GROUP_ADD_EDIT_QUEUE;
        String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                String bodyString = new String(body);
                logger.info("接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    JSONObject jsonObject = JSON.parseObject(bodyString);
                    if(jsonObject != null && !jsonObject.isEmpty()){
                        // 获取消息名称
                        String msgName = jsonObject.getString(RabbitMqConstants.MESSAGE_NAME_FIELD);
                        if(!msgName.equals(RabbitMqConstants.ERP_GROUP_ADD_EDIT_QUEUE)){
                            logger.info("mq消息中的方法为:[{}],与当前监听方法:[{}]不一致,不进行后续处理", msgName, RabbitMqConstants.ERP_GROUP_ADD_EDIT_QUEUE);
                            return;
                        }

                        // 获取消息体
                        JSONObject json = (JSONObject) jsonObject.get(RabbitMqConstants.MESSAGE_BODY_FIELD);
                        ServiceResult<List<String>> serviceResult = groupService.handleErpAddEditGroupInfoQueueData(json);
                        if(serviceResult.getResult() != null && serviceResult.getResult().size() > 0){
                            // 推送mq消息给前端更新组织机构数据
                            SendRabbitMqMsgUpdateTreeNodeThread sendRabbitMqMsgUpdateTreeNodeThread = new SendRabbitMqMsgUpdateTreeNodeThread(serviceResult.getResult(), rabbitMqHandler, rabbitMqParamConfig, accountService);
                            threadPoolManager.execute(sendRabbitMqMsgUpdateTreeNodeThread);
                        }
                    }
                    logger.info("erp部门新增、修改默认群组队列处理结束");
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}