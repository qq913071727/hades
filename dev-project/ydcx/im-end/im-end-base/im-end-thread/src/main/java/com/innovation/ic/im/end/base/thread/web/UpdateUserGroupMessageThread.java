package com.innovation.ic.im.end.base.thread.web;

import com.innovation.ic.im.end.base.handler.im_erp9.UserGroupMessageHandler;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.service.im_erp9.RefGroupAccountService;
import com.innovation.ic.im.end.base.service.im_erp9.RefUserGroupAccountService;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * @desc   表user_group_message中数据更新，同步到redis中
 * @author linuo
 * @time   2022年8月11日14:39:24
 */
public class UpdateUserGroupMessageThread extends AbstractThread implements Runnable{

    public UpdateUserGroupMessageThread() {
    }

    public UpdateUserGroupMessageThread(UserGroupMessageHandler userGroupMessageHandler, UserGroupMessage userGroupMessage) {
        this.userGroupMessageHandler = userGroupMessageHandler;
        this.userGroupMessage = userGroupMessage;
    }

    @Override
    public void run() {
        userGroupMessageHandler.handleUpdateUserGroupMessage(userGroupMessage);
    }

    public static class UpdateLastContactTimeInRefGroupAccountThread implements Runnable {

        private GroupMessage groupMessage;

        public UpdateLastContactTimeInRefGroupAccountThread() {
        }

        public UpdateLastContactTimeInRefGroupAccountThread(GroupMessage groupMessage) {
            this.groupMessage = groupMessage;
        }

        @Override
        public void run() {
            RefGroupAccountService refGroupAccountService = ServiceImplHelper.getRefGroupAccountService();
            refGroupAccountService.updateLastContactTimeByGroupId(groupMessage.getGroupId());
        }
    }

    public static class UpdateLastContactTimeInRefUserGroupAccountThread implements Runnable {

        private UserGroupMessage userGroupMessage;

        public UpdateLastContactTimeInRefUserGroupAccountThread() {
        }

        public UpdateLastContactTimeInRefUserGroupAccountThread(UserGroupMessage userGroupMessage) {
            this.userGroupMessage = userGroupMessage;
        }

        @Override
        public void run() {
            RefUserGroupAccountService refUSerGroupAccountService = ServiceImplHelper.getRefUserGroupAccountService();
            refUSerGroupAccountService.updateLastContactTimeByUserGroupId(userGroupMessage.getUserGroupId());
        }
    }
}