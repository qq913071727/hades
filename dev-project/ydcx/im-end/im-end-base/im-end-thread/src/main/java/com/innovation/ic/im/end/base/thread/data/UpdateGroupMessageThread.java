package com.innovation.ic.im.end.base.thread.data;

import com.innovation.ic.im.end.base.handler.im_erp9.GroupMessageHandler;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * @desc   表group_message中数据更新，同步到redis中
 * @author linuo
 * @time   2022年8月11日10:47:48
 */
public class UpdateGroupMessageThread extends AbstractThread implements Runnable{

    public UpdateGroupMessageThread() {
    }

    public UpdateGroupMessageThread(GroupMessageHandler groupMessageHandler, GroupMessage groupMessage) {
        this.groupMessageHandler = groupMessageHandler;
        this.groupMessage = groupMessage;
    }

    @Override
    public void run() {
        groupMessageHandler.handleUpdateGroupMessage(groupMessage);
    }
}
