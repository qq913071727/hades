package com.innovation.ic.im.end.base.thread.data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.service.im_erp9.AccountService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import lombok.SneakyThrows;
import java.util.List;

/**
 * @desc   发送rabbitMq消息，接收消息的人收到之后根据结果更新机构树内容
 * @author linuo
 * @time   2022年10月17日14:32:25
 */
public class SendRabbitMqMsgUpdateTreeNodeThread extends AbstractThread implements Runnable{

    private List<String> groupIdList;

    public SendRabbitMqMsgUpdateTreeNodeThread() {
    }

    public SendRabbitMqMsgUpdateTreeNodeThread(List<String> groupIdList, RabbitMqHandler rabbitMqHandler, RabbitMqParamConfig rabbitMqParamConfig, AccountService accountService) {
        this.groupIdList = groupIdList;
        this.rabbitMqHandler = rabbitMqHandler;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
        this.accountService = accountService;
    }

    @SneakyThrows
    @Override
    public void run() {
        Thread.sleep(3000);
        // 查询全部账号
        ServiceResult<List<String>> serviceResult = accountService.selectAllAccountUserName();
        List<String> userNameList = serviceResult.getResult();
        if(userNameList != null && userNameList.size() > 0 && groupIdList != null && groupIdList.size() > 0){
            // 推送mq消息
            for(String userName : userNameList){
                JSONArray jsonArray = (JSONArray) JSON.toJSON(groupIdList);
                rabbitMqHandler.sendRabbitMqMsg(userName, jsonArray.toJSONString(), rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.TREE_NODE_UPDATE_EXCHANGE));
            }
        }
    }
}