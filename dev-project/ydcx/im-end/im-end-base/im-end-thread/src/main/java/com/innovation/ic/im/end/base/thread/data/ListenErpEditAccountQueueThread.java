package com.innovation.ic.im.end.base.thread.data;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Structures;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqConstants;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9StructuresService;
import com.innovation.ic.im.end.base.service.im_erp9.AccountService;
import com.innovation.ic.im.end.base.service.im_erp9.GroupService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.rabbitmq.client.*;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc   监听erp修改账号队列(变更账号所属群组)
 * @author linuo
 * @time   2022年10月17日10:47:00
 */
public class ListenErpEditAccountQueueThread extends AbstractThread implements Runnable{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /** erp交换机 */
    private String exchange;

    public ListenErpEditAccountQueueThread(Channel channel, String exchange, AccountService accountService, Erp9StructuresService erp9StructuresService, GroupService groupService, RabbitMqHandler rabbitMqHandler, RabbitMqParamConfig rabbitMqParamConfig, ThreadPoolManager threadPoolManager) {
        this.channel = channel;
        this.exchange = exchange;
        this.accountService = accountService;
        this.erp9StructuresService = erp9StructuresService;
        this.groupService = groupService;
        this.rabbitMqHandler = rabbitMqHandler;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
        this.threadPoolManager = threadPoolManager;
    }

    @SneakyThrows
    @Override
    public void run() {
        String routingKey = RabbitMqConstants.ERP_EDIT_ACCOUNT_QUEUE;
        String queue = routingKey;
        channel.queueBind(queue, exchange, routingKey);
        channel.queueDeclare(queue, true, false, false, null);
        channel.exchangeDeclare(exchange, RabbitMqConstants.DIRECT_TYPE, true);
        Consumer callback = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                String bodyString = new String(body);
                logger.info("接收到的队列:[{}]的消息为:[{}]", queue, bodyString);
                try {
                    JSONObject jsonObject = JSON.parseObject(bodyString);
                    if(jsonObject != null && !jsonObject.isEmpty()){
                        // 获取消息名称
                        String msgName = jsonObject.getString(RabbitMqConstants.MESSAGE_NAME_FIELD);
                        if(!msgName.equals(RabbitMqConstants.ERP_EDIT_ACCOUNT_QUEUE)){
                            logger.info("mq消息中的方法为:[{}],与当前监听方法:[{}]不一致,不进行后续处理", msgName, RabbitMqConstants.ERP_EDIT_ACCOUNT_QUEUE);
                            return;
                        }

                        // 获取消息体
                        JSONObject json = (JSONObject) jsonObject.get(RabbitMqConstants.MESSAGE_BODY_FIELD);

                        // 组织架构id
                        String groupId = json.getString(RabbitMqConstants.STRUCTURE_ID_FIELD);

                        // 根据id查询erp中的组织机构信息
                        String groupName = null;

                        if(!Strings.isNullOrEmpty(groupId)){
                            ServiceResult<Erp9Structures> serviceResult = erp9StructuresService.selectErp9StructuresById(groupId);
                            if(serviceResult.getResult() != null){
                                groupName = serviceResult.getResult().getName();
                            }

                            // 处理默认群组数据
                            groupService.handleGroupData(groupId, groupName);
                        }

                        // 处理修改账号的mq数据
                        ServiceResult<Boolean> serviceResult = accountService.handleEditAccountRabbitMqData(json, groupName);
                        if(serviceResult.getResult()){
                            // 上次组织架构id
                            String oldGroupId = json.getString(RabbitMqConstants.OLD_STRUCTURE_ID_FIELD);

                            // 推送mq消息给前端更新组织机构数据
                            List<String> groupIdList = new ArrayList<>();
                            groupIdList.add(groupId);
                            groupIdList.add(oldGroupId);
                            SendRabbitMqMsgUpdateTreeNodeThread sendRabbitMqMsgUpdateTreeNodeThread = new SendRabbitMqMsgUpdateTreeNodeThread(groupIdList, rabbitMqHandler, rabbitMqParamConfig, accountService);
                            threadPoolManager.execute(sendRabbitMqMsgUpdateTreeNodeThread);
                        }
                    }
                    logger.info("erp删除账号队列处理结束");
                } catch (Exception e) {
                    logger.error("监听队列:[{}]时出现问题,原因:", queue, e);
                }
            }
        };
        channel.basicConsume(queue, true, callback);
    }
}