package com.innovation.ic.im.end.base.thread.data;

import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.handler.im_erp9.GroupMessageHandler;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * 向表group_message中插入数据，并同步到redis中
 */
public class InsertGroupMessageThread extends AbstractThread implements Runnable{

    public InsertGroupMessageThread() {
    }

    public InsertGroupMessageThread(GroupMessageHandler groupMessageHandler, GroupMessage groupMessage) {
        this.groupMessageHandler = groupMessageHandler;
        this.groupMessage = groupMessage;
    }

    @Override
    public void run() {
        groupMessageHandler.handleInsertGroupMessage(groupMessage);
    }
}
