package com.innovation.ic.im.end.base.thread.data;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.innovation.ic.im.end.base.pojo.im_erp9.TreeNodePojo;
import com.innovation.ic.im.end.base.service.im_erp9.TreeNodeService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import lombok.SneakyThrows;
import java.util.List;

/**
 * 组织结构向Redis放入数据
 */
public class TreeNodeIntoRedisThread extends AbstractThread implements Runnable {

    private String groupId;
    private List<Account> accountList;

    public TreeNodeIntoRedisThread() {
    }

    public TreeNodeIntoRedisThread(String groupId, List<Account> accountList, TreeNodeService treeNodeService, RedisManager redisManager) {
        this.groupId = groupId;
        this.accountList = accountList;
        this.treeNodeService = treeNodeService;
        this.redisManager = redisManager;
    }

    @SneakyThrows
    @Override
    public void run() {
        //遍历所有账号
        for (Account account : accountList) {
            Thread.sleep(500);
            String key = RedisStorage.TREE_NODE_PREFIX + groupId +
                    RedisStorage.UNDERLINE + account.getId() +
                    RedisStorage.UNDERLINE + account.getUsername();
            //查询数据
            ServiceResult<TreeNodePojo> serviceResult = treeNodeService.findById(groupId, account.getId(), account.getUsername());
            //更新缓存数据
            redisManager.set(key, JSON.toJSONString(serviceResult));
        }
    }
}