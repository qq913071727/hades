package com.innovation.ic.im.end.base.thread.data;

import com.innovation.ic.im.end.base.handler.im_erp9.MessageHandler;
import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * @desc   表message中数据更新，同步到redis中
 * @author linuo
 * @time   2022年8月11日09:34:22
 */
public class UpdateMessageThread extends AbstractThread implements Runnable{

    public UpdateMessageThread() {}

    public UpdateMessageThread(MessageHandler messageHandler, Message message) {
        this.messageHandler = messageHandler;
        this.message = message;
    }

    @Override
    public void run() {
        messageHandler.handleUpdateMessage(message);
    }
}
