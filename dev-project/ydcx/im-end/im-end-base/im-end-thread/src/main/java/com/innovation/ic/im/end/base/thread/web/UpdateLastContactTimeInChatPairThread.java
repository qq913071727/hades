package com.innovation.ic.im.end.base.thread.web;

import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.service.im_erp9.ChatPairService;
import com.innovation.ic.im.end.base.thread.AbstractThread;

public class UpdateLastContactTimeInChatPairThread extends AbstractThread implements Runnable {

    public UpdateLastContactTimeInChatPairThread() {
    }

    public UpdateLastContactTimeInChatPairThread(Message message) {
        this.message = message;
    }

    @Override
    public void run() {
        ChatPairService chatPairService = ServiceImplHelper.getChatPairService();
        chatPairService.updateLastContactTime(message.getFromUserAccount(), message.getToUserAccount());
    }
}
