package com.innovation.ic.im.end.base.thread.data;

import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.handler.im_erp9.MessageHandler;
import com.innovation.ic.im.end.base.thread.AbstractThread;

/**
 * 向表message中插入数据，并同步到redis中
 */
public class InsertMessageThread extends AbstractThread implements Runnable{

    public InsertMessageThread() {
    }

    public InsertMessageThread(MessageHandler messageHandler, Message message) {
        this.messageHandler = messageHandler;
        this.message = message;
    }

    @Override
    public void run() {
        messageHandler.handleInsertMessage(message);
    }
}
