/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : openfire

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 07/03/2022 14:30:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for of_chat_logs
-- ----------------------------
DROP TABLE IF EXISTS `of_chat_logs`;
CREATE TABLE `of_chat_logs`  (
  `MESSAGE_ID` int NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `SESSION_JID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户session jid名称',
  `SENDER` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息发送者',
  `RECEIVER` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受者',
  `CREATE_DATE` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息发送、创建时间',
  `LENGTH` int NULL DEFAULT NULL COMMENT '消息长度、大小',
  `CONTENT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息内容',
  `DETAIL` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息源报文',
  `STATE` int NULL DEFAULT NULL COMMENT '删除状态，1表示删除',
  PRIMARY KEY (`MESSAGE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'openfire聊天记录' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
