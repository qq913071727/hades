package com.innovation.ic.im.end.base.plugin.constant;

/**
 * 插件中使用的sql
 */
public class PluginSql {

    public static final String LOGS_COUNT = "SELECT count(1) FROM of_chat_logs";
    public static final String LOGS_LAST_MESSAGE_ID = "SELECT max(message_id) FROM of_chat_logs";
    public static final String LOGS_FIND_BY_ID = "SELECT message_id, session_jid, sender, receiver, create_date, length, content FROM of_chat_logs where message_id = ?";
    public static final String LOGS_REMOVE = "UPDATE of_chat_logs set state = 1 where message_id = ?";//"DELETE FROM of_chat_logs WHERE message_id = ?";
    public static final String LOGS_INSERT = "INSERT INTO of_chat_logs(message_id, session_jid, sender, receiver, create_date, length, content, detail, state) VALUES(?,?,?,?,?,?,?,?,?)";
    public static final String LOGS_QUERY = "SELECT message_id, session_jid, sender, receiver, create_date, length, content FROM of_chat_logs where state = 0";
    public static final String LOGS_SEARCH = "SELECT * FROM of_chat_logs where state = 0";
    public static final String LOGS_LAST_CONTACT = "SELECT distinct receiver FROM of_chat_logs where state = 0 and sender = ?";
    public static final String LOGS_ALL_CONTACT = "SELECT distinct session_jid FROM of_chat_logs where state = 0";
}
