package com.innovation.ic.im.end.base.plugin;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.innovation.ic.im.end.base.model.ChatLogs;
import com.innovation.ic.im.end.base.plugin.manager.DbChatLogsManager;
import org.jivesoftware.database.SequenceManager;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.openfire.user.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.*;

/**
 * 插件类，用于存储用户发送的消息
 */
public class MessageStoragePlugin implements PacketInterceptor, Plugin {

    private static final Logger log = LoggerFactory.getLogger(MessageStoragePlugin.class);

    private static PluginManager pluginManager;
    private static DbChatLogsManager logsManager;
    private InterceptorManager interceptorManager;

    public MessageStoragePlugin() {
        this.interceptorManager = InterceptorManager.getInstance();
        logsManager = DbChatLogsManager.getInstance();
    }

    @Override
    public void initializePlugin(PluginManager manager, File file) {
        interceptorManager.addInterceptor(this);
        MessageStoragePlugin.pluginManager = manager;
        log.info("初始化插件MessageStoragePlugin");
    }

    @Override
    public void destroyPlugin() {
        interceptorManager.removeInterceptor(this);
        log.info("销毁插件MessageStoragePlugin");
    }

    /**
     * 拦截消息核心方法，Packet就是拦截消息对象
     * @param packet
     * @param session
     * @param incoming
     * @param processed
     * @throws PacketRejectedException
     */
    @Override
    public void interceptPacket(Packet packet, Session session, boolean incoming, boolean processed) throws PacketRejectedException {
        log.info("调用插件MessageStoragePlugin大的interceptPacket方法");

        if (session != null) {
            debug(packet, incoming, processed, session);
        }

        JID recipient = packet.getTo();
        if (recipient != null) {
            String username = recipient.getNode();
            // 广播消息或是不存在/没注册的用户.
            if (username == null || !UserManager.getInstance().isRegisteredUser(recipient)) {
                return;
            } else if (!XMPPServer.getInstance().getServerInfo().getXMPPDomain().equals(recipient.getDomain())) {
                // 非当前openfire服务器信息
                return;
            } else if ("".equals(recipient.getResource())) {
            }
        }
        this.doAction(packet, incoming, processed, session);
    }

    /**
     * 执行保存/分析聊天记录动作
     * @param packet
     * @param incoming
     * @param processed
     * @param session
     */
    private void doAction(Packet packet, boolean incoming, boolean processed, Session session) {
        Packet copyPacket = packet.createCopy();
        if (packet instanceof Message) {
            Message message = (Message) copyPacket;

            // 一对一聊天，单人模式
            if (message.getType() == Message.Type.chat) {
                log.info("单人聊天信息：{}", message.toXML());
                debug("单人聊天信息：" + message.toXML());

                // 程序执行中；是否为结束或返回状态（是否是当前session用户发送消息）
                if (processed || !incoming) {
                    return;
                }
                logsManager.add(this.get(packet, incoming, session));

                // 群聊天，多人模式
            } else if (message.getType() ==  Message.Type.groupchat) {
                List<?> els = message.getElement().elements("x");
                if (els != null && !els.isEmpty()) {
                    log.info("群聊天信息：{}", message.toXML());
                    debug("群聊天信息：" + message.toXML());
                } else {
                    log.info("群系统信息：{}", message.toXML());
                    debug("群系统信息：" + message.toXML());
                }

                // 其他信息
            } else {
                log.info("其他信息：{}", message.toXML());
                debug("其他信息：" + message.toXML());
            }
        } else if (packet instanceof IQ) {
            IQ iq = (IQ) copyPacket;
            if (iq.getType() == IQ.Type.set && iq.getChildElement() != null && "session".equals(iq.getChildElement().getName())) {
                log.info("用户登录成功：{}", iq.toXML());
                debug("用户登录成功：" + iq.toXML());
            }
        } else if (packet instanceof Presence) {
            Presence presence = (Presence) copyPacket;
            if (presence.getType() == Presence.Type.unavailable) {
                log.info("用户退出服务器成功：{}", presence.toXML());
                debug("用户退出服务器成功：" + presence.toXML());
            }
        }
    }

    /**
     * 创建一个聊天记录实体对象，并设置相关数据
     * @param packet
     * @param incoming
     * @param session
     * @return
     */
    private ChatLogs get(Packet packet, boolean incoming, Session session) {
        Message message = (Message) packet;
        ChatLogs logs = new ChatLogs();

        JID jid = session.getAddress();
        if (incoming) {        // 发送者
            logs.setSender(jid.getNode());
            JID recipient = message.getTo();
            logs.setReceiver(recipient.getNode());
        }
        logs.setContent(message.getBody());
        logs.setCreateDate(new Timestamp(new Date().getTime()));
        logs.setDetail(message.toXML());
        logs.setLength(logs.getContent().length());
        logs.setState(0);
        logs.setSessionJid(jid.toString());
        // 生成主键id，利用序列生成器
        long messageID = SequenceManager.nextID(ChatLogs.ChatLogsConstants.CHAT_LOGS);
        logs.setMessageId(messageID);

        return logs;
    }

    /**
     * 调试信息
     * @param packet
     * @param incoming
     * @param processed
     * @param session
     */
    private void debug(Packet packet, boolean incoming, boolean processed, Session session) {
        String info = "[ packetID: " + packet.getID() + ", to: " + packet.getTo() + ", from: " + packet.getFrom() + ", incoming: " + incoming + ", processed: " + processed + " ]";

        long timed = System.currentTimeMillis();
        debug("################### start ###################" + timed);
        debug("id:" + session.getStreamID() + ", address: " + session.getAddress());
        debug("info: " + info);
        debug("xml: " + packet.toXML());
        debug("################### end #####################" + timed);

        log.info("id:" + session.getStreamID() + ", address: " + session.getAddress());
        log.info("info: {}", info);
        log.info("plugin Name: " + pluginManager.getName(this) + ", xml: " + packet.toXML());
    }

    private void debug(Object message) {
        if (true) {
            System.out.println(message);
        }
    }

}
