package com.innovation.ic.im.end.base.handler.helper;

import com.innovation.ic.im.end.base.handler.im_erp9.ImErp9AbstractHandler;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.handler.im_erp9.WebSocketHandler;
import org.springframework.context.ApplicationContext;

/**
 * Handler的帮助类
 */
public class HandlerHelper {

    /**
     * WebSocketHandler类
     */
    private static WebSocketHandler webSocketHandler;

    /**
     * RabbitMqHandler类
     */
    private static RabbitMqHandler rabbitMqHandler;

    /**
     * 返回具体的handler类
     *
     * @param clazz
     * @return
     */
    private static ImErp9AbstractHandler getHandler(Class clazz) {
        ApplicationContext applicationContext = ApplicationContextRegister.getApplicationContext();
        return (ImErp9AbstractHandler) applicationContext.getBean(clazz);
    }

    /**
     * 返回WebSocketHandler
     *
     * @return
     */
    public static WebSocketHandler getWebSocketHandler() {
        if (null != webSocketHandler) {
            return webSocketHandler;
        } else {
            return (WebSocketHandler) HandlerHelper.getHandler(WebSocketHandler.class);
        }
    }

    /**
     * 返回RabbitMqHandler
     *
     * @return
     */
    public static RabbitMqHandler getRabbitMqHandler() {
        if (null != rabbitMqHandler) {
            return rabbitMqHandler;
        } else {
            return (RabbitMqHandler) HandlerHelper.getHandler(RabbitMqHandler.class);
        }
    }

}
