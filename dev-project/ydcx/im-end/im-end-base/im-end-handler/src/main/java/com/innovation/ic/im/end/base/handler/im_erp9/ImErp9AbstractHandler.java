package com.innovation.ic.im.end.base.handler.im_erp9;

import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.im.end.base.mapper.im_erp9.ChatPairMapper;
import com.innovation.ic.im.end.base.value.config.RedisParamConfig;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ImErp9AbstractHandler {
    @Autowired
    protected RedisParamConfig redisParamConfig;

    @Autowired
    protected RedisManager redisManager;

    @Autowired
    protected ChatPairMapper chatPairMapper;

    @Autowired
    protected RabbitMqManager rabbitMqManager;
}