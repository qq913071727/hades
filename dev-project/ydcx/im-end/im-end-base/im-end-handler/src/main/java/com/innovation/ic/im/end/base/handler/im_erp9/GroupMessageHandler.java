package com.innovation.ic.im.end.base.handler.im_erp9;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Set;

@Component
public class GroupMessageHandler extends ImErp9AbstractHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 处理im-erp9数据库的group_message表的insert操作
     * @param groupMessage 需要更新的默认群组消息
     */
    public void handleInsertGroupMessage(GroupMessage groupMessage) {
        logger.info("处理im-erp9数据库的group_message表的insert操作，向redis插入GROUP_MESSAGE:开头的数据");

        String key = RedisStorage.GROUP_MESSAGE_PREFIX + groupMessage.getGroupId();
        String value = JSONObject.toJSONString(groupMessage);
        double score = groupMessage.getCreateTime().getTime();
        redisManager.zAdd(key, value, score);
//        redisManager.expire(key, redisParamConfig.getTimeout());
    }

    /**
     * 处理im-erp9数据库的group_message表的update操作
     * @param groupMessage 需要更新的默认群组消息
     */
    public void handleUpdateGroupMessage(GroupMessage groupMessage) {
        logger.info("处理im-erp9数据库的group_message表的update操作，更新redis中GROUP_MESSAGE:开头的数据");

        String key = RedisStorage.GROUP_MESSAGE_PREFIX + groupMessage.getGroupId();
        double score = groupMessage.getCreateTime().getTime();

        Set<String> stringSet = redisManager.zReverseRangeByScore(key, score, score);
        if(stringSet != null && stringSet.size() > 0){
            for(String groupMsg : stringSet){
                JSONObject jsonObject = JSONObject.parseObject(groupMsg);
                if(((Integer) jsonObject.get(Constants.ID)).intValue() == groupMessage.getId().intValue()){
                    // 删除redis中的值
                    redisManager.zRemove(key, groupMsg);
                    // 添加新的值
                    String value = JSONObject.toJSONString(groupMessage);
                    redisManager.zAdd(key, value, score);
                    break;
                }
            }
        }
    }
}