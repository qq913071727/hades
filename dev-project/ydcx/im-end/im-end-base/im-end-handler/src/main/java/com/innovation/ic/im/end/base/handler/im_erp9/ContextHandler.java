package com.innovation.ic.im.end.base.handler.im_erp9;

import com.innovation.ic.im.end.base.pojo.constant.ContextParameter;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.innovation.ic.im.end.base.pojo.global.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Set;

@Component
public class ContextHandler extends ImErp9AbstractHandler {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 向Context中添加clientId列表
     */
    public void putClientSet() {
        logger.info("向Context中添加clientId列表");

        Set<String> clientSet = redisManager.sMembers(RedisStorage.CLIENT);
        Context.put(ContextParameter.CLIENT_SET, clientSet);
    }
}