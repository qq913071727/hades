package com.innovation.ic.im.end.base.handler.im_erp9;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.*;

@Component
public class WebSocketHandler extends ImErp9AbstractHandler {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 将onlineMap对象存储到redis中
     * @param key 键
     * @param json 值
     */
    public void putOnlineMap(String key, JSONObject json) {
        String jsonStr = json.toJSONString();
        jsonStr = jsonStr.replaceAll("\\\\", "");
        logger.info("redis中存的key为[{}],value为[{}]", key, jsonStr);
        redisManager.sAdd(key, jsonStr);
    }

    /**
     * 根据键值从redis中取onlineMap数据
     * @param key 键
     */
    public Set<String> getOnlineMap(String key) {
        return redisManager.sMembers(key);
    }

    /**
     * 根据键值判断成员是否存在
     * @param key 键
     * @param member 成员信息
     */
    public Boolean judgeKeyIfHave(String key, Object member) {
        return redisManager.sIsMember(key, member);
    }

    /**
     * 根据键值从redis中删除onlineMap数据
     * @param key 键
     * @param value 要删除的值
     * @return
     */
    public Long delOnlineMapValue(String key, String value) {
        return redisManager.sRemove(key, value);
    }

    /**
     * 将onlineGroupMap、onlineUserGroupMap对象存储到redis中
     * @param key 键
     * @param json 值
     */
    public void putOnlineGroupMap(String key, JSONObject json) {
        String jsonStr = json.toJSONString();
        jsonStr = jsonStr.replaceAll("\\\\", "");
        logger.info("redis中存的key为[{}],value为[{}]", key, jsonStr);
        redisManager.sAdd(key, jsonStr);
    }

    /**
     * 根据key获取redis中值数量
     * @param key 键
     * @return 值的数量
     */
    public Long getRedisKeySize(String key) {
        return redisManager.sSize(key);
    }
}