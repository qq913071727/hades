package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.ChatPair;
import com.innovation.ic.im.end.base.model.im_erp9.TempChatPair;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface ChatPairMapper extends EasyBaseMapper<ChatPair> {

    /**
     * 删除chat_pair表中的所有数据
     */
    void truncateTable();

    /**
     * 根据fromUserAccount和toUserAccount，查找ChatPair对象
     *
     * @param fromUserAccount
     * @param toUserAccount
     * @return
     */
    ChatPair findByFromUserAccountAndToUserAccount(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount);

    /**
     * 查询设置了消息免打扰、有最近联系时间和内容不可见时间的数据
     * @return 返回查询结果
     */
    List<TempChatPair> getNoReminderLastContactInvisibleIsNotNullData();

    /**
     * 查找当前联系人所有最近联系人账号集合
     * @param account 账号
     * @return 返回当前账号的最近联系人账号集合
     */
    List<String> selectLastContactAccounts(@Param("account") String account);

    /**
     * 更新聊天对的最近联系时间
     * @param fromUserAccount 发送消息的用户账号
     * @param toUserAccount 接收消息的用户账号
     * @return 返回更新结果
     */
    int updateLastContactTime(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount);

    /**
     * 设置某个用户对某个用户消息免打扰
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount 接收消息的用户
     * @param status 状态。1表示设置为消息免打扰。0表示取消消息免打扰
     * @return 返回结果
     */
    int updateNoReminder(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount, @Param("status") Integer status);

    /**
     * 清空chatPair中的最近联系时间
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount 接收消息的用户
     * @return 返回结果
     */
    int deleteChat(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount);

    /**
     * 获取最近联系时间
     * @param account 用户1
     * @param username 用户2
     * @return 返回最近联系时间
     */
    Date getLastContactTime(@Param("account") String account, @Param("username") String username);

    /**
     * 更新某个用户与某个用户的聊天页面打开状态
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount 接收消息的用户
     * @param status 状态。0表示聊天窗口关闭，1表示聊天窗口打开
     * @return 返回结果
     */
    int updateChatPageStatus(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount, @Param("status") Integer status);

    /**
     * 将当前用户所有的聊天页面状态设置为关闭
     * @param username 用户名
     * @return 返回结果
     */
    int closeAccountChatPageStatus(@Param("username") String username);

    /**
     * 获取用户A与用户B的聊天页面打开状态
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount   接收消息的用户
     * @return 返回聊天页面打开状态(0 : 表示聊天窗口关闭 、 1 : 表示聊天窗口打开)
     */
    Integer getChatPageStatus(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount);

    /**
     * 删除聊天对信息
     * @param account 账号id
     * @return 返回删除结果
     */
    int deleteChatPairDataByAccount(@Param("account") String account);

    /**
     * 查询当前聊天对code最大值
     * @return 返回
     */
    Integer getChatPairMaxCode();

    /**
     * 删除历史供应商协同账号的聊天对数据
     * @param scAccountId 供应商协同账号
     * @return 返回删除结果
     */
    Integer deleteScChatPairData(@Param("scAccountId") String scAccountId);
}