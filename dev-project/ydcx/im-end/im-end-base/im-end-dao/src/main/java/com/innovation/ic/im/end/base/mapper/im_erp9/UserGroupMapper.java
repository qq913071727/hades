package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroup;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGroupMapper extends EasyBaseMapper<UserGroup> {

    /**
     * 插入UserGroup对象，返回id
     * @param userGroup
     * @return
     */
    Integer saveUserGroupReturnId(UserGroup userGroup);

    /**
     * 按照user_group_id对ref_user_group_account表分组统计，并将结果更新到user_group表的membership字段
     * @return
     */
    Boolean updateMembershipSelectRefUserGroupAccount();

    /**
     * 获取用户自定义群组。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量。显示是否消息免打扰。显示最后一条消息。
     * @param account
     * @return
     */
    List<GroupPojo> findUserGroupInfos(@Param("account") String account);

    /**
     * 通过id获取到自定义组信息
     * @param userGroupId
     * @return
     */
    UserGroup findByRealId(@Param("userGroupId")Integer userGroupId);
}
