package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.CustomerServiceScore;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;

@Repository
public interface CustomerServiceScoreMapper extends EasyBaseMapper<CustomerServiceScore> {
    /**
     * 查询上次评分时间
     * @param scAccountId 供应商协同账号id
     * @return 返回查询结果
     */
    Date queryLastScoreTime(@Param("scAccountId") String scAccountId);
}