package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.AccountRelation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AccountRelationMapper extends EasyBaseMapper<AccountRelation> {
    /**
     * 查询账号关系表数据
     * @return 返回查询结果
     */
    List<AccountRelation> getAccountRelationListData();

    /**
     * 查询与采购绑定的供应商协同账号
     * @param saleAccountId 采购账号id
     * @return 返回查询结果
     */
    List<String> selectScAccountIdsBySaleAccountId(@Param("saleAccountId") String saleAccountId);
}