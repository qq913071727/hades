package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccountOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.Map;

/**
 * @desc   RefGroupAccountOperationMapper类
 * @author linuo
 * @time   2022年6月8日16:12:10
 */
@Repository
public interface RefGroupAccountOperationMapper extends EasyBaseMapper<RefGroupAccountOperation> {

    /**
     * 根据默认群组id、账号id查询账号和群组的操作表数据
     * @param groupId 群组id
     * @param accountId 账号id
     * @param userName 用户名
     * @return 返回账号和群组的操作表数据
     */
    RefGroupAccountOperation selectDataByGroupAccountUsername(@Param("groupId") String groupId, @Param("accountId") String accountId, @Param("userName") String userName);

    /**
     * 取消置顶
     * @param refGroupAccountOperation 操作实体
     * @return 返回更新结果
     */
    int setToppingTimeNull(@Param("refGroupAccountOperation") RefGroupAccountOperation refGroupAccountOperation);

    /**
     * 根据默认群组id更新最近联系时间
     * @param groupId 默认群组id
     * @return 返回更新结果
     */
    int updateLastContactTimeByGroupId(@Param("groupId") String groupId);

    /**
     * 更新当前用户的群组最近联系时间
     * @param groupId 默认群组id
     * @param accountId 账号id
     * @param username 用户名
     * @return 返回更新结果
     */
    int updateLastContactTime(@Param("groupId") String groupId, @Param("accountId") String accountId, @Param("username") String username);

    /**
     * 清除当前用户对群组groupId的最近联系时间，设置当前时间前的聊天记录对username不可见
     * @param map 更新条件
     * @return 返回更新结果
     */
    int deleteGroupChat(@Param("map") Map<String, Object> map);

    /**
     * 获取与默认群组的最近联系时间
     * @param groupId 默认群组id
     * @param account 账号id
     * @return 返回最近联系时间
     */
    Date getLastContactTime(@Param("groupId") String groupId, @Param("account") String account);
}