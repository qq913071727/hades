package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.TempAccount;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @desc   TempAccount的mapper接口
 * @author linuo
 * @time   2022年7月5日16:36:32
 */
@Repository
public interface TempAccountMapper extends EasyBaseMapper<TempAccount> {

    /**
     * 删除temp_account表中的所有数据
     */
    void truncateTable();

    /**
     * 查询account表中登录状态和最近登录时间不为空的数据
     * @return 返回查询结果
     */
    List<TempAccount> queryAccountLoginAndTimeIsNotNull();

    /**
     * 备份account表
     */
    void backupAccount();

    /**
     * 比对account和temp_account表，获取在erp系统被删除的用户账号
     * @return 返回在erp系统被删除的用户账号
     */
    List<String> queryErpDeleteAccountDatas();
}