package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.TreeNode;
import org.springframework.stereotype.Repository;

@Repository
public interface TreeNodeMapper extends EasyBaseMapper<TreeNode> {

    /**
     * 删除表tree_node中的数据
     */
    void truncateTable();
}
