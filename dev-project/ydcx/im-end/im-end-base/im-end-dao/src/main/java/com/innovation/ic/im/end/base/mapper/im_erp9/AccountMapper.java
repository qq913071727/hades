package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountFindLastContactResultPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.ScAccountFindLastContactResultPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.SearchAccountDataPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public interface AccountMapper extends EasyBaseMapper<Account> {
    /**
     * 删除account表中的所有数据
     */
    void truncateTable();

    /**
     * 返回所有人员，按照字母顺序升序排列
     * @return
     */
    List<AccountPojo> findAllOrderByRealNameAsc();

    /**
     * 通过名字，进行模糊查询
     * @param integration 集成(1表示erp相关，2表示供应商协同相关)
     * @param realName
     * @return
     */
    List<SearchAccountDataPojo> findByRealNameLike(@Param("integration") Integer integration, @Param("realName") String realName);

    /**
     * 获取近期的联系人。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量
     * @param account
     * @return
     */
    List<AccountFindLastContactResultPojo> findLastContact(@Param("account") String account);

    /**
     * 根据自定义群组查询条件查询账号信息集合
     * @param map 查询条件
     * @return 返回账号信息集合
     */
    List<Account> selectAccountByUserGroupParam(@Param("map") Map<String, Object> map);

    /**
     * 根据默认群组查询条件查询账号信息集合
     * @param map 查询条件
     * @return 返回账号信息集合
     */
    List<Account> selectAccountByGroupParam(@Param("map") Map<String, Object> map);

    /**
     * 通过拼音，进行模糊查询
     * @param integration 集成(1表示erp相关，2表示供应商协同相关)
     * @param realName
     * @return
     */
    List<SearchAccountDataPojo> findByRealPinYinLike(@Param("integration") Integer integration, @Param("realName") String realName);

    /**
     * 通过dyjCode，进行模糊查询
     * @param integration 集成(1表示erp相关，2表示供应商协同相关)
     * @param realName
     * @return
     */
    List<SearchAccountDataPojo> findByRealDyjCodeLike(@Param("integration") Integer integration, @Param("realName") String realName);

    /**
     * 查询所有数据
     */
    List<Account> queryAll();

    /**
     * 查询所有在线用户id集合
     * @return 返回在线用户id集合
     */
    List<String> selectOnlineUserIds();

    /**
     * 修改Account表中的登录状态
     */
    void exit();

    /**
     * 查询不包含自己的其它账号
     * @param userName 用户名
     * @return 返回不包含自己的其它账号集合
     */
    List<String> selectAccountListNotHaveSelf(@Param("userName") String userName);

    /**
     * 查询全部账号
     * @return 返回全部账号
     */
    List<String> selectAllAccountUserName();

    /**
     * 查询系统账号
     * @return 返回查询结果
     */
    List<String> getSystemAccount();

    /**
     * 清空account表中erp导入的账号数据
     */
    void deleteErpAccountData();

    /**
     * 获取供应商协同账号的近期联系人
     * @param account 账号
     * @return 返回查询结果
     */
    List<ScAccountFindLastContactResultPojo> getScRecentContact(@Param("account") String account);

    /**
     * 获取与采购的最近联系信息
     * @param scAccountId 供应商协同账号id
     * @return 返回查询结果
     */
    ScAccountFindLastContactResultPojo selectScSaleLastContactInfo(@Param("scAccountId") String scAccountId);

    /**
     * 查询与系统账号的最近联系信息
     * @param username 当前用户名
     * @param systemAccount 系统账号名称
     * @return 返回查询结果
     */
    ScAccountFindLastContactResultPojo selectScSystemLastContactInfo(@Param("username") String username, @Param("systemAccount") String systemAccount);
}