package com.innovation.ic.im.end.base.mapper.erp9_sqlserver;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Structures;
import org.springframework.stereotype.Repository;

@Repository
public interface Erp9StructuresMapper extends EasyBaseMapper<Erp9Structures> {

    /**
     * 删除erp9_structures表中的所有数据
     */
    void truncateTable();


}
