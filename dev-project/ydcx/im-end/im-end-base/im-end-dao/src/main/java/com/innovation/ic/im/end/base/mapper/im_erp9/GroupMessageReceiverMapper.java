package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessageReceiver;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupMessageReceiverMapper extends BaseMapper<GroupMessageReceiver> {
    /**
     * 默认群组中本人接收到的消息设置为不可用
     * @param userName 用户名
     */
    int setToUserAvailable(@Param("userName") String userName);

    /**
     * 转移group_message_receiver表数据到his_group_message_receiver表中
     * @param outTime 超时时间
     * @return 返回转移结果
     */
    int transGroupMessageReceiverToHisGroupMessageReceiver(@Param("outTime") String outTime);

    /**
     * 删除转移成功的group_message_receiver表历史数据开始
     * @param outTime 超时时间
     * @return 返回转移结果
     */
    int deleteGroupMessageReceiverHistoryData(@Param("outTime") String outTime);
}