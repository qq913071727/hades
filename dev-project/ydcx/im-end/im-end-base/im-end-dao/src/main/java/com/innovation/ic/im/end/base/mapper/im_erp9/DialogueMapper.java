package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Dialogue;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DialogueMapper extends EasyBaseMapper<Dialogue> {

    /**
     * 获取组与id
     */
    List<Dialogue> findByAccountIdAndGroup();
}
