package com.innovation.ic.im.end.base.mapper.erp9_sqlserver;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9MapsStructure;
import org.springframework.stereotype.Repository;

@Repository
public interface Erp9MapsStructureMapper extends EasyBaseMapper<Erp9MapsStructure> {
}
