package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccountPrivilege;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RefGroupAccountPrivilegeMapper extends EasyBaseMapper<RefGroupAccountPrivilege> {
    /**
     * 删除ref_group_account_privilege表中所有数据
     * @return 返回删除结果
     */
    void truncateTable();

    /**
     * 根据账号id、账号查询有权限发送消息的群组id集合
     * @param accountId 账号id
     * @param username 账号
     * @return 返回有权限发送消息的群组id集合
     */
    List<String> selectGroupIdByAccountAndUsername(@Param("accountId") String accountId, @Param("username") String username);
}