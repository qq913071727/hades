package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupMessageMapper extends BaseMapper<GroupMessage> {
    /**
     * 将删除用户发送的默认消息数据设置为本人不可用
     * @param userName 用户名
     */
    int setFromUserAvailable(@Param("userName") String userName);

    /**
     * 转移group_message的历史数据到his_group_message表中
     * @param outTime 超时时间
     * @return 返回转移结果
     */
    int transGroupMessageDataToHisGroupMessage(@Param("outTime") String outTime);

    /**
     * 删除转移成功的group_message表历史数据
     * @param outTime 超时时间
     * @return 返回删除结果
     */
    int deleteGroupMessageHistoryData(@Param("outTime") String outTime);
}