package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccount;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public interface RefGroupAccountMapper extends EasyBaseMapper<RefGroupAccount> {

    /**
     * 删除ref_group_account表中的所有数据
     */
    void truncateTable();

    /**
     * 通过accountId 去ref_group_account表获取对应的 id
     * @param id
     * @return
     */
    List<RefGroupAccount> findByRealId(@Param("id") String id);

    /**
     * 根据条件查询默认群组信息
     * @param map 查询条件
     * @return 返回默认群组信息
     */
    GroupPojo getGroupDetailInfoByParam(@Param("map") Map<String, Object> map);

    /**
     * 查询当前默认群组中除自己之外的用户账号列表
     * @param groupId 默认群组id
     * @param userAccount 发送消息的用户账号
     * @return 返回除自己之外的用户账号列表
     */
    List<String> getGroupAccountsNotHaveSelf(@Param("groupId") String groupId, @Param("userAccount") String userAccount);

    /**
     * 查询当前默认群组中的用户账号
     * @param groupId 默认群组id
     * @return 返回默认群组中的用户账号列表
     */
    List<String> getGroupAccounts(@Param("groupId") String groupId);

    /**
     * 通过组id获取对应人信息
     * @param groupId 组id
     */
    List<RefGroupAccount> findUsername(@Param("groupId")String groupId,@Param("fromUserAccount")String fromUserAccount);

    /**
     * 查询与当前账号关联的默认群组id集合
     * @param accountId 账号id
     * @return 返回与当前账号关联的默认群组id集合
     */
    List<String> getGroupIdListByAdminId(@Param("accountId") String accountId);
}