package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessageReceiver;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupMessageReceiverMapper extends BaseMapper<UserGroupMessageReceiver> {

    /**
     * 把user_group_offline_message表数据存入his_user_group_offline_message历史表中
     * @param userGroupId 自定义群组id
     * @param username 用户账号
     * @return 返回除自己之外的用户账号
     */

    void copy(@Param("userGroupId") Integer userGroupId, @Param("username")String username);

    /**
     * 自定义群组中本人接收到的消息设置为不可用
     * @param userName 用户名
     * @return 返回结果
     */
    int setToUserAvailable(@Param("userName") String userName);

    /**
     * 转移user_group_message_receiver表数据到his_user_group_message_receiver表中
     * @param outTime 超时时间
     * @return 返回转移结果
     */
    int transUserGroupMessageReceiverToHisUserGroupMessageReceiver(@Param("outTime") String outTime);

    /**
     * 删除转移成功的user_group_message_receiver表历史数据
     * @param outTime 超时时间
     * @return 返回删除结果
     */
    int deleteUserGroupMessageReceiverHistoryData(@Param("outTime") String outTime);
}