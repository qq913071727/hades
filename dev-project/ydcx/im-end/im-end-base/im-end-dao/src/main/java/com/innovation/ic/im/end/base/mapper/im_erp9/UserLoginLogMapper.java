package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.UserLoginLog;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

    /**
     * 点对点
     * 添加user_login_log表数据
     *
     * @param username
     * @param type
     * @param date
     */
    void saveUserData(@Param("username") String username, @Param("type") Integer type, @Param("date") Date date, @Param("ipAddr") String ipAddr);

    /**
     * 默认
     * 添加user_login_log表数据
     *
     * @param username
     * @param type
     * @param date
     * @param groupId
     */
    void saveDefaultUserData(@Param("username") String username, @Param("type") Integer type, @Param("date") Date date, @Param("groupId") String groupId, @Param("ipAddr") String ipAddr);

    /**
     * 自定义
     * 添加user_login_log表数据
     *
     * @param username
     * @param type
     * @param date
     * @param userGroupId
     */
    void saveCustomUserData(@Param("username") String username, @Param("type") Integer type, @Param("date") Date date, @Param("userGroupId") String userGroupId, @Param("ipAddr") String ipAddr);
}
