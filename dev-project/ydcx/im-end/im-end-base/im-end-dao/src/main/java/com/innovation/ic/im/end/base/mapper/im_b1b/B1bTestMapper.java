package com.innovation.ic.im.end.base.mapper.im_b1b;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_b1b.B1bTest;
import com.innovation.ic.im.end.base.pojo.im_b1b.B1bTestPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface B1bTestMapper extends EasyBaseMapper<B1bTest> {

    /**
     * 删除b1b_admins表中的所有数据
     */
    void truncateTable();

    /**
     * 返回所有人员，按照字母顺序升序排列
     * @return
     */
    List<B1bTestPojo> findAllOrderByRealNameAsc();

}
