package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.RefUserGroupAccount;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface RefUserGroupAccountMapper extends EasyBaseMapper<RefUserGroupAccount> {

    /**
     * 根据条件查询自定义群组信息
     * @param map 查询条件
     * @return 返回自定义群组信息
     */
    GroupPojo getGroupDetailInfoByParam(@Param("map") Map<String, Object> map);

    /**
     * 通过accountId 去ref_user_group_account表获取对应的 id
     * @param id
     * @return
     */
    List<RefUserGroupAccount> findByRealId(@Param("id") String id);

    /**
     * 查询当前自定义群组中除自己之外的用户账号
     * @param userGroupId 自定义群组id
     * @param userName 用户账号
     * @return 返回除自己之外的用户账号
     */
    List<String> getUserGroupAccountsNotHaveSelf(@Param("userGroupId") Integer userGroupId, @Param("userName") String userName);

    /**
     * 查询自定义群组中的用户账号
     * @param userGroupId 自定义群组id
     * @return 返回自定义群组中的用户账号
     */
    List<String> getUserGroupAccounts(@Param("userGroupId") Integer userGroupId);

    /**
     * 先把ref_user_group_account表数据存入his_ref_user_group_account历史表中
     * @param userGroupId 自定义群组id
     * @param username 用户账号
     * @param accountId 账号id
     * @return 返回除自己之外的用户账号
     */
    Integer copyBy(@Param("newLastLoginTime")String newLastLoginTime,@Param("userGroupId")Integer userGroupId,@Param("accountId")String accountId, @Param("username")String username);

    /**
     * 根据用户自定义群组id更新最近联系时间
     * @param userGroupId 自定义群组id
     * @return 返回更新结果
     */
    int updateLastContactTimeByUserGroupId(@Param("userGroupId") Integer userGroupId);

    /**
     * 更新用户消息免打扰设置
     * @param userGroupId 自定义群组id
     * @param accountId 账号id
     * @param username 用户名
     * @param status 状态
     * @return 返回更新结果
     */
    int updateNoReminder(@Param("userGroupId") Integer userGroupId, @Param("accountId") String accountId, @Param("username") String username, @Param("status") Integer status);

    /**
     * 更新用户对自定义群组的内容不可见时间
     * @param userGroupId 自定义群组id
     * @param accountId 账号id
     * @param username 用户名
     * @return 返回更新结果
     */
    int updateContentInvisibleTime(@Param("userGroupId") String userGroupId, @Param("accountId") String accountId, @Param("username") String username);

    /**
     * 更新用户username自定义群组userGroupId的最近联系时间
     * @param userGroupId 自定义群组id
     * @param accountId 账号id
     * @param username 用户名
     * @return 返回更新结果
     */
    int updateLastContactTime(@Param("userGroupId") String userGroupId, @Param("accountId") String accountId, @Param("username") String username);

    /**
     * 通过GroupId 获取对应的人
     * @param userGroupId 群组id
     * @param fromUserAccount 发送消息的用户名
     * @return 返回删除结果
     */
    List<RefUserGroupAccount> findUsername(@Param("userGroupId")Integer userGroupId, @Param("fromUserAccount")String fromUserAccount);

    /**
     * 删除最近联系自定义群组并清除聊天记录
     * @param map 删除条件
     * @return 返回删除结果
     */
    int deleteUserGroupChat(@Param("map") Map<String, Object> map);

    /**
     * 查找包含当前用户的自定义群组id
     * @param userName 用户名
     * @return 返回查询结果
     */
    List<Integer> selectIncludeUserNameGroupIds(@Param("userName") String userName);

    /**
     * 将当前用户从自定义群组中删除
     * @param userGroupId 自定义群组id
     * @param userName 用户名
     * @return 返回删除结果
     */
    int deleteUserFromUserGroup(@Param("userGroupId") Integer userGroupId, @Param("userName") String userName);

    /**
     * 更新自定义群组成员数量
     * @param userGroupId 自定义群组id
     */
    void updateUserGroupMemberCount(@Param("userGroupId") Integer userGroupId);

    /**
     * 获取与自定义群组的最近联系时间
     * @param groupId 默认群组id
     * @param account 账号id
     * @return 返回最近联系时间
     */
    Date getLastContactTime(@Param("groupId") String groupId, @Param("account") String account);

    /**
     * 查询与当前账号关联的自定义群组id集合
     * @param accountId
     * @return
     */
    List<String> getUserGroupIdListAdminId(@Param("accountId")String accountId);
}