package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.TempChatPair;
import org.springframework.stereotype.Repository;

@Repository
public interface TempChatPairMapper extends EasyBaseMapper<TempChatPair> {
    /**
     * 清空表数据
     */
    void truncateTable();
}