package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Message;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MessageMapper extends BaseMapper<Message> {

    /**
     * 按照根据id模糊查询组信息
     * @param account
     * @return
     */
    List<Message> findRecent(@Param("from_user_account") String account);

    /**
     * 处理一对一聊天记录数据，将删除用户发送的一对一消息数据设置为本人不可用
     * @param userName 用户名
     * @return 返回处理数据数量
     */
    int setFromUserAvailable(@Param("userName") String userName);

    /**
     * 转移message的历史数据到his_message表中
     * @param outTime 过期时间
     * @retun 返回插入数据条数
     */
    int transMessageDataToHisMessage(@Param("outTime") String outTime);

    /**
     * 删除转移成功的message表历史数据
     * @param outTime 过期时间
     * @return 返回删除数据条数
     */
    int deleteMessageHistoryData(@Param("outTime") String outTime);

    /**
     * 查询未读消息id集合
     * @param fromUserAccount 发送消息的人
     * @param toUserAccount   接收消息的人
     * @return 返回查询结果
     */
    List<String> getUnreadMsgList(@Param("fromUserAccount") String fromUserAccount, @Param("toUserAccount") String toUserAccount);
}