package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupMessageMapper extends BaseMapper<UserGroupMessage> {

    /**
     * 把user_group_message表数据存入hisuser_group_message历史表中
     * @param userGroupId 自定义群组id
     * @param username 用户账号
     * @return 返回除自己之外的用户账号
     */
    void copy(@Param("userGroupId") Integer userGroupId, @Param("username") String username);

    /**
     * 将删除用户发送的自定义群组消息数据设置为本人不可用
     * @param userName 用户名
     * @return 返回结果
     */
    int setFromUserAvailable(@Param("userName") String userName);

    /**
     * 转移user_group_message表数据到his_user_group_message表中
     * @param outTime 超时时间
     * @return 返回转移结果
     */
    int transUserGroupMessageToHisUserGroupMessage(@Param("outTime") String outTime);

    /**
     * 删除转移成功的user_group_message表历史数据
     * @param outTime 超时时间
     * @return 返回删除结果
     */
    int deleteUserGroupMessageHistoryData(@Param("outTime") String outTime);
}