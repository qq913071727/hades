package com.innovation.ic.im.end.base.mapper.erp9_sqlserver;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Admins;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountAddPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountResultByStaffIdNotNullPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface Erp9AdminsMapper extends EasyBaseMapper<Erp9Admins> {

    /**
     * 根据部门id（structuresId），查找账号列表
     * @param structuresId
     * @return
     */
    List<Erp9Admins> findByStructuresId(@Param("structuresId") String structuresId);

    /**
     * 从erp9系统的数据库中获取Account对象列表，不包括部门
     * @return
     */
    List<Account> getAccountWithoutDepartment();

    /**
     * 从erp9系统的数据库中获取Account对象列表，包括部门
     * @return
     */
    List<Account> getAccountWithDepartment();

    /**
     * 查询员工ID不为空的所有数据
     * @return 查询结果
     */
    List<AccountResultByStaffIdNotNullPojo> getAccountByStaffIdIsNotNull();

    /**
     * 根据账号id查询所属部门
     * @param adminId 账号id
     * @return 返回所属部门信息
     */
    String getDepartmentNameByAdminId(@Param("adminId") String adminId);

    /**
     * 通过账号id集合批量获取账号信息
     * @param list 账号id集合
     * @return 返回查询结果
     */
    List<AccountAddPojo> getAccountInfosByAdminIdList(@Param("list") List<String> list);
}