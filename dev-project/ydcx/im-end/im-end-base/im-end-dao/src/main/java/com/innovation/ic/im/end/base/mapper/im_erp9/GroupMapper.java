package com.innovation.ic.im.end.base.mapper.im_erp9;

import com.innovation.ic.im.end.base.mapper.EasyBaseMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Group;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface GroupMapper extends EasyBaseMapper<Group> {

    /**
     * 删除group表中的所有数据
     */
    void truncateTable();

    /**
     * 插入Group对象，返回id
     * @param group
     * @return
     */
    Integer saveGroupReturnId(Group group);

    /**
     * 按照group_id对ref_group_account表分组统计，并将结果更新到group_表的membership字段
     * @return
     */
    Boolean updateMembershipSelectRefGroupAccount();

    /**
     * 获取近期联系的默认群组。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量。显示是否消息免打扰。显示最后一条消息。
     * @param account
     * @return
     */
    List<GroupPojo> findLastContact(@Param("account") String account);

    /**
     * 按照根据id模糊查询组信息
     * @param groupId
     * @return
     */
    Group findByRealId(@Param("groupId") String groupId);

    /**
     * 查询表中所有信息
     * @return
     */
    List<Group> findList();

    /**
     * 更新默认群组人数数据
     * @param groupId 默认群组id
     */
    int updateMembershipByGroupId(@Param("groupId") String groupId);

    /**
     * 添加默认群组
     * @param group 默认群组数据
     * @return 返回添加结果
     */
    int insertGroup(@Param("group") Group group);
}