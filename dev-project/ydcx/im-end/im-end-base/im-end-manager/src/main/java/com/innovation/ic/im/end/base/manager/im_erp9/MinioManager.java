//package com.innovation.ic.im.end.base.manager.im_erp9;
//
//import com.innovation.ic.b1b.framework.util.FileUtil;
//import io.minio.*;
//import io.minio.messages.DeleteError;
//import io.minio.messages.DeleteObject;
//import lombok.SneakyThrows;
//import org.apache.tomcat.util.http.fileupload.IOUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//import org.springframework.web.multipart.MultipartFile;
//import javax.servlet.http.HttpServletResponse;
//import java.io.*;
//import java.net.URLEncoder;
//import java.nio.charset.StandardCharsets;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.security.InvalidKeyException;
//import io.minio.errors.InvalidResponseException;
//import io.minio.errors.InsufficientDataException;
//import java.security.NoSuchAlgorithmException;
//import io.minio.errors.ServerException;
//import io.minio.errors.InternalException;
//import io.minio.errors.XmlParserException;
//import io.minio.errors.ErrorResponseException;
//
///**
// * minio处理类
// */
//@Component
//public class MinioManager extends ImErp9AbstractManager {
//    private static final Logger log = LoggerFactory.getLogger(MinioManager.class);
//
//    /**
//     * description: 判断bucket是否存在，不存在则创建
//     *
//     * @return: void
//     */
//    public void existBucket(String name) {
//        try {
//            boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(name).build());
//            if (!exists) {
//                minioClient.makeBucket(MakeBucketArgs.builder().bucket(name).build());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 创建存储bucket
//     *
//     * @param bucketName 存储bucket名称
//     * @return Boolean
//     */
//    public Boolean makeBucket(String bucketName) {
//        try {
//            minioClient.makeBucket(MakeBucketArgs.builder()
//                    .bucket(bucketName)
//                    .build());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 删除存储bucket
//     *
//     * @param bucketName 存储bucket名称
//     * @return Boolean
//     */
//    public Boolean removeBucket(String bucketName) {
//        try {
//            minioClient.removeBucket(RemoveBucketArgs.builder()
//                    .bucket(bucketName)
//                    .build());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * description: 上传文件
//     *
//     * @param multipartFile
//     * @return: java.lang.String
//     */
//    public List<String> upload(MultipartFile[] multipartFile) {
//        List<String> names = new ArrayList<>(multipartFile.length);
//        for (MultipartFile file : multipartFile) {
//            String fileName = file.getOriginalFilename();
////            String[] split = fileName.split("\\.");
////            if (split.length > 1) {
////                fileName = split[0] + "_" + System.currentTimeMillis() + "." + split[1];
////            } else {
////                fileName = fileName + System.currentTimeMillis();
////            }
//            InputStream in = null;
//            try {
//                in = file.getInputStream();
//                minioClient.putObject(PutObjectArgs.builder()
//                        .bucket(minioConfig.getBucketName())
//                        .object(fileName)
//                        .stream(in, in.available(), -1)
//                        .contentType(file.getContentType())
//                        .build()
//                );
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (in != null) {
//                    try {
//                        in.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//            names.add(fileName);
//        }
//        return names;
//    }
//
//    /**
//     * description: 下载文件
//     *
//     * @param fileName
//     * @return: org.springframework.http.ResponseEntity<byte                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               [                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ]>
//     */
//    public ResponseEntity<byte[]> download(String fileName) {
//        ResponseEntity<byte[]> responseEntity = null;
//        InputStream in = null;
//        ByteArrayOutputStream out = null;
//        try {
//            in = minioClient.getObject(GetObjectArgs.builder().bucket(minioConfig.getBucketName()).object(fileName).build());
//            out = new ByteArrayOutputStream();
//            IOUtils.copy(in, out);
//            //封装返回值
//            byte[] bytes = out.toByteArray();
//            HttpHeaders headers = new HttpHeaders();
//            try {
//                headers.add("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//            headers.setContentLength(bytes.length);
//            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//            headers.setAccessControlExposeHeaders(Arrays.asList("*"));
//            responseEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (in != null) {
//                    try {
//                        in.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (out != null) {
//                    out.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return responseEntity;
//    }
//
//    /**
//     * 查看文件对象
//     *
//     * @param bucketName 存储bucket名称
//     * @return 存储bucket内文件对象信息
//     */
////    public List<ObjectItem> listObjects(String bucketName) {
////        Iterable<Result<Item>> results = minioClient.listObjects(
////                ListObjectsArgs.builder().bucket(bucketName).build());
////        List<ObjectItem> objectItems = new ArrayList<>();
////        try {
////            for (Result<Item> result : results) {
////                Item item = result.get();
////                ObjectItem objectItem = new ObjectItem();
////                objectItem.setObjectName(item.objectName());
////                objectItem.setSize(item.size());
////                objectItems.add(objectItem);
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////            return null;
////        }
////        return objectItems;
////    }
//
//    /**
//     * 批量删除文件对象
//     *
//     * @param bucketName 存储bucket名称
//     * @param objects    对象名称集合
//     */
//    public Iterable<Result<DeleteError>> removeObjects(String bucketName, List<String> objects) {
//        List<DeleteObject> dos = objects.stream().map(e -> new DeleteObject(e)).collect(Collectors.toList());
//        Iterable<Result<DeleteError>> results = minioClient.removeObjects(RemoveObjectsArgs.builder().bucket(bucketName).objects(dos).build());
//        return results;
//    }
//
//    /**
//     * 下载文件
//     *
//     * @param filename : 文件名
//     * @param response :
//     * @return: void
//     * @date : 2020/8/17 0:34
//     */
//    @SneakyThrows(Exception.class)
//    public Boolean newDownload(String filename, HttpServletResponse response, Integer type) {
//        boolean result = true;
//        try {
//            StatObjectResponse object = minioClient.statObject(StatObjectArgs.builder().bucket(minioConfig.getBucketName()).object(filename).build());
//            response.setContentType(object.contentType());
//            response.setCharacterEncoding("UTF-8");
//            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
//            // InputStream is = minioClient.getObject(bucketName, fileName);
//            InputStream is = minioClient.getObject(GetObjectArgs.builder().bucket(minioConfig.getBucketName()).object(filename).build());
//            IOUtils.copy(is, response.getOutputStream());
//            is.close();
//        } catch (Exception e) {
//            log.error("文件下载出现问题,原因:", e);
//            result =false;
//        }
//        return result;
//    }
//
//    /**
//     * 拷贝文件
//     *
//     * @param bucketName    bucket名称
//     * @param objectName    文件名称
//     * @param srcBucketName 目标bucket名称
//     * @param srcObjectName 目标文件名称
//     */
//    public ObjectWriteResponse copyObject(String bucketName, String objectName, String srcBucketName, String srcObjectName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
//        return minioClient.copyObject(
//                CopyObjectArgs.builder()
//                        .source(CopySource.builder().bucket(bucketName).object(objectName).build())
//                        .bucket(srcBucketName)
//                        .object(srcObjectName)
//                        .build());
//    }
//
//    /**
//     * 删除文件
//     *
//     * @param bucketName bucket名称
//     * @param objectName 文件名称
//     */
//    public void removeObject(String bucketName, String objectName)
//            throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
//        minioClient
//                .removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
//    }
//
//    /**
//     * 下载图片
//     *
//     * @param fileName : 文件名
//     * @param response :
//     * @return: void
//     * @date : 2020/10/9 10:4
//     */
//    @SneakyThrows(Exception.class)
//    public Boolean downloadImg(String fileName, HttpServletResponse response) {
//        boolean result = true;
//        try {
////            String a = "D:" + "/" + fileName;
////            System.out.println(a);
////            byte[] bytes = FileUtil.getBytesByFile(a);
//            byte[] bytes = FileUtil.getBytesByFile(fileParamConfig.getFileUrl() + "/" + fileName);
//            int len = 0;
//            response.reset(); // 非常重要
//            // 纯下载方式
//            response.setContentType("application/json;charset=utf-8");
//            response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));
//            response.addHeader("Content-Length", "" + bytes.length);
//            OutputStream out = response.getOutputStream();
//            out.write(bytes);
//            out.flush();
//            out.close();
//        } catch (Exception e) {
//            result = false;
//            log.error("图片下载出现问题,原因:", e);
//        }
//        return result;
//    }
//}
//
//
