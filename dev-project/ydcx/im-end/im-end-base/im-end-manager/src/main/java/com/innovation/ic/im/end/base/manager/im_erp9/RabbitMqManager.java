//package com.innovation.ic.im.end.base.manager.im_erp9;
//
//import com.google.common.base.Strings;
//import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
//import com.rabbitmq.client.*;
//import org.springframework.stereotype.Component;
//import javax.annotation.PostConstruct;
//import javax.annotation.Resource;
//import java.io.IOException;
//import java.util.concurrent.TimeoutException;
//
///**
// * Rabbitmq的管理类
// */
//@Component
//public class RabbitMqManager extends ImErp9AbstractManager {
//    private static ConnectionFactory factory;
//    private static Connection connection;
//    private static Channel channel;
//
//    @Resource
//    private RabbitMqParamConfig rabbitMqParamConfig;
//
//    /** 默认交换机名称 */
//    private String DEFAULT_EXCHANGE = "im-end";
//
//    @PostConstruct
//    public void init() {
//        factory = new ConnectionFactory();
//        factory.setHost(rabbitMqParamConfig.getHost());
//        factory.setPort(rabbitMqParamConfig.getPort());
//        factory.setUsername(rabbitMqParamConfig.getUsername());
//        factory.setPassword(rabbitMqParamConfig.getPassword());
//        if(!Strings.isNullOrEmpty(rabbitMqParamConfig.getVirtualHost())){
//            factory.setVirtualHost(rabbitMqParamConfig.getVirtualHost());
//        }
//
//        try {
//            connection = factory.newConnection();
//            channel = connection.createChannel();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取channel
//     * @return 返回channel
//     */
//    public Channel getChannel(){
//        return channel;
//    }
//
//    /**
//     * 声明交换机
//     * @param exchange
//     * @param builtinExchangeType
//     * @param b
//     * @throws IOException
//     */
//    public void exchangeDeclare(String exchange, BuiltinExchangeType builtinExchangeType, boolean b) throws IOException {
//        channel.exchangeDeclare(exchange, builtinExchangeType, b);
//    }
//
//    /**
//     * 向指定交换机发送消息
//     * @param exchange 交换机名称
//     * @param queueName 队列名称
//     * @param basicProperties 基础配置信息，默认为null
//     * @param b 消息二进制数据
//     * @throws IOException 抛出IO异常
//     */
//    public void basicPublish(String exchange, String queueName, AMQP.BasicProperties basicProperties, byte[] b) throws IOException {
//        channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT, true);
//        channel.basicPublish(exchange, queueName, basicProperties, b);
//    }
//
//    /**
//     * 发送消息，通过默认交换机
//     * @param queueName
//     * @param message
//     * @throws IOException
//     */
//    public void basicPublish(String queueName, String message) throws IOException {
//        channel.exchangeDeclare(this.DEFAULT_EXCHANGE, BuiltinExchangeType.DIRECT, true);
//        channel.basicPublish(this.DEFAULT_EXCHANGE, queueName, null, message.getBytes());
//    }
//
//    /**
//     * 关闭连接
//     * @throws IOException
//     * @throws TimeoutException
//     */
//    public void close() throws IOException, TimeoutException {
//        channel.close();
//        connection.close();
//    }
//
//    //    public void setHost(String host) {
////        this.host = host;
////    }
////
////
////    public void setPort(Integer port) {
////        this.port = port;
////    }
////
////
////    public void setUsername(String username) {
////        this.username = username;
////    }
////
////
////    public void setPassword(String password) {
////        this.password = password;
////    }
////
////
////    public void setExchange(String exchange) {
////        this.exchange = exchange;
////    }
//
//    /**
//     * 动态处理队列，不存在时创建队列
//     * @param queneName 队列名称
//     * @return 返回rabbitmq中的队列名称
//     */
////    public void handleDynamicallyQuene(String queneName){
////        RabbitAdmin rabbitAdmin = new RabbitAdmin(rabbitTemplate);
//////        Queue springQueue = new Queue(queneName);
////        Properties queueProperties = rabbitAdmin.getQueueProperties(queneName);
////        if(queueProperties == null){
////            Queue springQueue = new Queue(queneName, true, false, false);
////            rabbitAdmin.declareQueue(springQueue);
////        }
////    }
////
////    public void send(String exchangeKey, String routingKey, Object message) {
////        CorrelationData correlationData = new CorrelationData(new Random().toString());
////        rabbitTemplate.convertAndSend(exchangeKey, routingKey, message, correlationData);
////    }
////
////    public void delayedSend(String exchangeKey, String routingKey, Object msg, final int xdelay) {
////        rabbitTemplate.convertAndSend(exchangeKey, routingKey, msg, message -> {
////            // 设置延迟时间
////            message.getMessageProperties().setDelay(xdelay);
////            return message;
////        });
////    }
////
////    public void convertAndSend(String routingKey, Object message){
////        rabbitTemplate.convertAndSend(routingKey, message);
////    }
//}