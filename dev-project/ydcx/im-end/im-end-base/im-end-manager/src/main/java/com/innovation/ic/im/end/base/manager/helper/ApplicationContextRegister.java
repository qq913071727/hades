//package com.innovation.ic.im.end.base.manager.helper;
//
//import org.springframework.beans.BeansException;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.stereotype.Component;
//
///**
// * 应用程序上下文
// */
//@Component("managerApplicationContextRegister")
//public class ApplicationContextRegister implements ApplicationContextAware {
//    private static ApplicationContext APPLICATION_CONTEXT;
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        APPLICATION_CONTEXT = applicationContext;
//    }
//
//    public static ApplicationContext getApplicationContext() {
//        return APPLICATION_CONTEXT;
//    }
//}
