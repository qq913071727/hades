//package com.innovation.ic.im.end.base.manager.helper;
//
//import com.innovation.ic.b1b.framework.manager.MinioManager;
//import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
//import com.innovation.ic.b1b.framework.manager.RedisManager;
//import com.innovation.ic.b1b.framework.manager.SftpChannelManager;
//import org.springframework.context.ApplicationContext;
//
///**
// * Manager的帮助类
// */
//public class ManagerHelper {
//
//    /**
//     * MinioManager类
//     */
//    private static MinioManager minioManager;
//
//    /**
//     * QueueMessageManager类
//     */
//    private static RabbitMqManager rabbitMqManager;
//
//    /**
//     * RedisManager类
//     */
//    private static RedisManager redisManager;
//
//    /**
//     * RedisManager类
//     */
//    private static SftpChannelManager sftpChannelManager;
//
//    /**
//     * 返回具体的manager类
//     *
//     * @param clazz
//     * @return
//     */
//    private static Object getManager(Class clazz) {
//        ApplicationContext applicationContext = ApplicationContextRegister.getApplicationContext();
//        return applicationContext.getBean(clazz);
//    }
//
//    /**
//     * 返回MinioManager
//     *
//     * @return
//     */
//    public static MinioManager getMinioManager() {
//        if (null != minioManager) {
//            return minioManager;
//        } else {
//            return (MinioManager) ManagerHelper.getManager(MinioManager.class);
//        }
//    }
//
//    /**
//     * 返回RabbitMqManager
//     *
//     * @return
//     */
//    public static RabbitMqManager getRabbitMqManager() {
//        if (null != rabbitMqManager) {
//            return rabbitMqManager;
//        } else {
//            return (RabbitMqManager) ManagerHelper.getManager(RabbitMqManager.class);
//        }
//    }
//
//    /**
//     * 返回QueueMessageManager
//     *
//     * @return
//     */
//    public static RedisManager getRedisManager() {
//        if (null != redisManager) {
//            return redisManager;
//        } else {
//            return (RedisManager) ManagerHelper.getManager(RedisManager.class);
//        }
//    }
//
//    /**
//     * 返回SftpChannalManager
//     *
//     * @return
//     */
//    public static SftpChannelManager getSftpChannalManager() {
//        if (null != sftpChannelManager) {
//            return sftpChannelManager;
//        } else {
//            return (SftpChannelManager) ManagerHelper.getManager(SftpChannelManager.class);
//        }
//    }
//}
