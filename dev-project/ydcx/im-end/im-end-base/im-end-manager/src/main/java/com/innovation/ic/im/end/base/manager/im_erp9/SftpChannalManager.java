//package com.innovation.ic.im.end.base.manager.im_erp9;
//
//import com.innovation.ic.im.end.base.value.config.SftpParamConfig;
//import com.jcraft.jsch.*;
//import org.springframework.stereotype.Component;
//import javax.annotation.PostConstruct;
//import javax.annotation.Resource;
//import java.util.Properties;
//
///**
// * @desc   SftpChannal的管理类
// * @author linuo
// * @time   2022年7月7日16:49:07
// */
//@Component
//public class SftpChannalManager extends ImErp9AbstractManager {
//    public static ChannelSftp channelSftp;
//
//    public static ChannelExec channelExec;
//
//    public static Session sshSession;
//
//    @Resource
//    private SftpParamConfig SftpParamConfig;
//
//    @PostConstruct
//    public void init() {
//        try {
//            JSch jsch = new JSch();
//            jsch.getSession(SftpParamConfig.getUsername(), SftpParamConfig.getHost(), SftpParamConfig.getPort());
//            sshSession = jsch.getSession(SftpParamConfig.getUsername(), SftpParamConfig.getHost(), SftpParamConfig.getPort());
//            sshSession.setPassword(SftpParamConfig.getPassword());
//            Properties sshConfig = new Properties();
//            sshConfig.put("StrictHostKeyChecking", "no");
//            sshSession.setConfig(sshConfig);
//            sshSession.connect();
//            Channel channel = sshSession.openChannel("sftp");
//            //channel.connect(SftpParamConfig.getTimeout());
//            channel.connect();
//            channelSftp = (ChannelSftp) channel;
//            channelExec = (ChannelExec) sshSession.openChannel("exec");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取sftp连接
//     * @return 返回sftp连接
//     */
//    public ChannelSftp getChannelSftp(){
//        return channelSftp;
//    }
//
//    /**
//     * 获取执行脚本连接
//     * @return 返回执行脚本连接
//     */
//    public ChannelExec getChannelExec(){
//        return channelExec;
//    }
//
//    /**
//     * 获取执行脚本连接
//     * @return 返回执行脚本连接
//     */
//    public Session getSession(){
//        return sshSession;
//    }
//}