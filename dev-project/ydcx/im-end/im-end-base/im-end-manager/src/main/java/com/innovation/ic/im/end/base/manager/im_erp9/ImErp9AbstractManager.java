//package com.innovation.ic.im.end.base.manager.im_erp9;
//
//import com.innovation.ic.im.end.base.value.config.FileParamConfig;
//import com.innovation.ic.im.end.base.value.config.MinioConfig;
//import io.minio.MinioClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//
//public abstract class ImErp9AbstractManager {
//
//    @Autowired
//    protected MinioClient minioClient;
//
//    @Autowired
//    protected RedisTemplate<String, String> redisTemplate;
//
//    @Autowired
//    protected FileParamConfig fileParamConfig;
//
//    @Autowired
//    protected MinioConfig minioConfig;
//}
