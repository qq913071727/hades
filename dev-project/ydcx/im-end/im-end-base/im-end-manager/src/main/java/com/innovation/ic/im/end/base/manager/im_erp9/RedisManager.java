//package com.innovation.ic.im.end.base.manager.im_erp9;
//
//import org.springframework.data.domain.Sort;
//import org.springframework.data.geo.Distance;
//import org.springframework.data.geo.GeoResults;
//import org.springframework.data.geo.Point;
//import org.springframework.data.redis.connection.BitFieldSubCommands;
//import org.springframework.data.redis.connection.RedisGeoCommands;
//import org.springframework.data.redis.core.*;
//import java.util.Collection;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
//import org.springframework.stereotype.Component;
//
///**
// * redis操作的具体实现类
// */
//@Component
//public class RedisManager extends ImErp9AbstractManager {
//    public void set(String key, String value, long time) {
//        redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
//    }
//
//    public void set(String key, String value) {
//        redisTemplate.opsForValue().set(key, value);
//    }
//
//    public Object get(String key) {
//        return redisTemplate.opsForValue().get(key);
//    }
//
//    /**
//     * 删除一条记录
//     * @param key
//     * @return
//     */
//    public Boolean del(String key) {
//        return redisTemplate.delete(key);
//    }
//
//    /**
//     * 批量删除
//     * @param keys
//     * @return
//     */
//    public Long del(List<String> keys) {
//        return redisTemplate.delete(keys);
//    }
//
//    /**
//     * 批量删除
//     * @param keys
//     * @return
//     */
//    public Long del(Set<String> keys) {
//        return redisTemplate.delete(keys);
//    }
//
//    /**
//     * 模糊查询，匹配key的前缀
//     * @param keyPrefix
//     * @return
//     */
//    public Set<String> keysPrefix(String keyPrefix){
//        return redisTemplate.keys(keyPrefix + "*");
//    }
//
//    /**
//     * 根据redis的key删除对应数据
//     * @param redisKey redisKey
//     * @return 返回删除结果
//     */
//    public Boolean delRedisDataByKeyPrefix(String redisKey){
//        Set<String> keySet = keysPrefix(redisKey);
//        Long del = del(keySet);
//        if(del > 0){
//            return Boolean.TRUE;
//        }else{
//            return Boolean.FALSE;
//        }
//    }
//
//    /**
//     * 获取新的id（全局统一id）
//     * @return 返回新的id
//     */
//    public synchronized Integer nextId(String key, long delta) {
//        return Math.toIntExact(incr(key, delta));
//    }
//
//    /**
//     * 设置过期时间
//     * @param key
//     * @param time
//     * @return
//     */
//    public Boolean expire(String key, long time) {
//        return redisTemplate.expire(key, time, TimeUnit.SECONDS);
//    }
//
//    public Long getExpire(String key) {
//        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
//    }
//
//    public Boolean hasKey(String key) {
//        return redisTemplate.hasKey(key);
//    }
//
//    public synchronized Long incr(String key, long delta) {
//        return redisTemplate.opsForValue().increment(key, delta);
//    }
//
//    public Long decr(String key, long delta) {
//        return redisTemplate.opsForValue().increment(key, -delta);
//    }
//
//    public Object hGet(String key, String hashKey) {
//        return redisTemplate.opsForHash().get(key, hashKey);
//    }
//
//    public Boolean hSet(String key, String hashKey, Object value, long time) {
//        redisTemplate.opsForHash().put(key, hashKey, value);
//        return expire(key, time);
//    }
//
//    public void hSet(String key, String hashKey, Object value) {
//        redisTemplate.opsForHash().put(key, hashKey, value);
//    }
//
//    public Map<Object, Object> hGetAll(String key) {
//        return redisTemplate.opsForHash().entries(key);
//    }
//
//    public Boolean hSetAll(String key, Map<String, Object> map, long time) {
//        redisTemplate.opsForHash().putAll(key, map);
//        return expire(key, time);
//    }
//
//    public void hSetAll(String key, Map<String, ?> map) {
//        redisTemplate.opsForHash().putAll(key, map);
//    }
//
//    public void hDel(String key, Object... hashKey) {
//        redisTemplate.opsForHash().delete(key, hashKey);
//    }
//
//    public Boolean hHasKey(String key, String hashKey) {
//        return redisTemplate.opsForHash().hasKey(key, hashKey);
//    }
//
//    public Long hIncr(String key, String hashKey, Long delta) {
//        return redisTemplate.opsForHash().increment(key, hashKey, delta);
//    }
//
//    public Long hDecr(String key, String hashKey, Long delta) {
//        return redisTemplate.opsForHash().increment(key, hashKey, -delta);
//    }
//
//    public Set<String> sMembers(String key) {
//        return redisTemplate.opsForSet().members(key);
//    }
//
//    public Long sAdd(String key, String... values) {
//        return redisTemplate.opsForSet().add(key, values);
//    }
//
//    public Long sAdd(String key, long time, String... values) {
//        Long count = redisTemplate.opsForSet().add(key, values);
//        expire(key, time);
//        return count;
//    }
//
//    public Boolean sIsMember(String key, Object value) {
//        return redisTemplate.opsForSet().isMember(key, value);
//    }
//
//    public Long sSize(String key) {
//        return redisTemplate.opsForSet().size(key);
//    }
//
//    public Long sRemove(String key, Object... values) {
//        return redisTemplate.opsForSet().remove(key, values);
//    }
//
//    public List<String> lRange(String key, long start, long end) {
//        return redisTemplate.opsForList().range(key, start, end);
//    }
//
//    public Long lSize(String key) {
//        return redisTemplate.opsForList().size(key);
//    }
//
//    public Object lIndex(String key, long index) {
//        return redisTemplate.opsForList().index(key, index);
//    }
//
//    public Long lPush(String key, String value) {
//        return redisTemplate.opsForList().rightPush(key, value);
//    }
//
//    public Long lPush(String key, String value, long time) {
//        Long index = redisTemplate.opsForList().rightPush(key, value);
//        expire(key, time);
//        return index;
//    }
//
//    public Long lPushAll(String key, String... values) {
//        return redisTemplate.opsForList().rightPushAll(key, values);
//    }
//
//    public Long lPushAll(String key, Long time, String... values) {
//        Long count = redisTemplate.opsForList().rightPushAll(key, values);
//        expire(key, time);
//        return count;
//    }
//
//    public Long lRemove(String key, long count, Object value) {
//        return redisTemplate.opsForList().remove(key, count, value);
//    }
//
//    public Boolean bitAdd(String key, int offset, boolean b) {
//        return redisTemplate.opsForValue().setBit(key, offset, b);
//    }
//
//    public Boolean bitGet(String key, int offset) {
//        return redisTemplate.opsForValue().getBit(key, offset);
//    }
//
//    public Long bitCount(String key) {
//        return redisTemplate.execute((RedisCallback<Long>) con -> con.bitCount(key.getBytes()));
//    }
//
//    public List<Long> bitField(String key, int limit, int offset) {
//        return redisTemplate.execute((RedisCallback<List<Long>>) con ->
//                con.bitField(key.getBytes(),
//                        BitFieldSubCommands.create().get(BitFieldSubCommands.BitFieldType.unsigned(limit)).valueAt(offset)));
//    }
//
//    public byte[] bitGetAll(String key) {
//        return redisTemplate.execute((RedisCallback<byte[]>) con -> con.get(key.getBytes()));
//    }
//
//    public Long geoAdd(String key, Double x, Double y, String name) {
//        return redisTemplate.opsForGeo().add(key, new Point(x, y), name);
//    }
//
//    public List<Point> geoGetPointList(String key, String... place) {
//        return redisTemplate.opsForGeo().position(key, place);
//    }
//
//    public Distance geoCalculationDistance(String key, String placeOne, String placeTow) {
//        return redisTemplate.opsForGeo()
//                .distance(key, placeOne, placeTow, RedisGeoCommands.DistanceUnit.KILOMETERS);
//    }
//
//    public GeoResults<RedisGeoCommands.GeoLocation<String>> geoNearByPlace(String key, String place, Distance distance, long limit, Sort.Direction sort) {
//        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().includeCoordinates();
//        // 判断排序方式
//        if (Sort.Direction.ASC == sort) {
//            args.sortAscending();
//        } else {
//            args.sortDescending();
//        }
//        args.limit(limit);
//        return redisTemplate.opsForGeo()
//                .radius(key, place, distance, args);
//    }
//
//    public List<String> geoGetHash(String key, String... place) {
//        return redisTemplate.opsForGeo()
//                .hash(key, place);
//    }
//
//    /**------------------zSet相关操作--------------------------------*/
//
//    /**
//     * 添加元素，有序集合是按照元素的score值由小到大排列
//     *
//     * @param key
//     * @param value
//     * @param score
//     * @return
//     */
//    public Boolean zAdd(String key, String value, double score) {
//        return redisTemplate.opsForZSet().add(key, value, score);
//    }
//
//    /**
//     * 添加元素
//     * @param key
//     * @param values
//     * @return
//     */
//    public Long zAdd(String key, Set<ZSetOperations.TypedTuple<String>> values) {
//        return redisTemplate.opsForZSet().add(key, values);
//    }
//
//    /**
//     * @param key
//     * @param values
//     * @return
//     */
//    public Long zRemove(String key, Object... values) {
//        return redisTemplate.opsForZSet().remove(key, values);
//    }
//
//    /**
//     * 增加元素的score值，并返回增加后的值
//     *
//     * @param key
//     * @param value
//     * @param delta
//     * @return
//     */
//    public Double zIncrementScore(String key, String value, double delta) {
//        return redisTemplate.opsForZSet().incrementScore(key, value, delta);
//    }
//
//    /**
//     * 返回元素在集合的排名,有序集合是按照元素的score值由小到大排列
//     *
//     * @param key
//     * @param value
//     * @return 0表示第一位
//     */
//    public Long zRank(String key, Object value) {
//        return redisTemplate.opsForZSet().rank(key, value);
//    }
//
//    /**
//     * 返回元素在集合的排名,按元素的score值由大到小排列
//     *
//     * @param key
//     * @param value
//     * @return
//     */
//    public Long zReverseRank(String key, Object value) {
//        return redisTemplate.opsForZSet().reverseRank(key, value);
//    }
//
//    /**
//     * 获取集合的元素, 从小到大排序
//     *
//     * @param key
//     * @param start 开始位置
//     * @param end   结束位置, -1查询所有
//     * @return
//     */
//    public Set<String> zRange(String key, long start, long end) {
//        return redisTemplate.opsForZSet().range(key, start, end);
//    }
//
//    /**
//     * 获取集合元素, 并且把score值也获取
//     *
//     * @param key
//     * @param start
//     * @param end
//     * @return
//     */
//    public Set<ZSetOperations.TypedTuple<String>> zRangeWithScores(String key, long start,
//                                                                   long end) {
//        return redisTemplate.opsForZSet().rangeWithScores(key, start, end);
//    }
//
//    /**
//     * 根据Score值查询集合元素
//     *
//     * @param key
//     * @param min 最小值
//     * @param max 最大值
//     * @return
//     */
//    public Set<String> zRangeByScore(String key, double min, double max) {
//        return redisTemplate.opsForZSet().rangeByScore(key, min, max);
//    }
//
//    /**
//     * 根据Score值查询集合元素, 从小到大排序
//     *
//     * @param key
//     * @param min 最小值
//     * @param max 最大值
//     * @return
//     */
//    public Set<ZSetOperations.TypedTuple<String>> zRangeByScoreWithScores(String key,
//                                                                          double min, double max) {
//        return redisTemplate.opsForZSet().rangeByScoreWithScores(key, min, max);
//    }
//
//    /**
//     * @param key
//     * @param min
//     * @param max
//     * @param start
//     * @param end
//     * @return
//     */
//    public Set<ZSetOperations.TypedTuple<String>> zRangeByScoreWithScores(String key,
//                                                                          double min, double max, long start, long end) {
//        return redisTemplate.opsForZSet().rangeByScoreWithScores(key, min, max,
//                start, end);
//    }
//
//    /**
//     * 获取集合的元素, 从大到小排序
//     *
//     * @param key
//     * @param start
//     * @param end
//     * @return
//     */
//    public Set<String> zReverseRange(String key, long start, long end) {
//        return redisTemplate.opsForZSet().reverseRange(key, start, end);
//    }
//
//    /**
//     * 获取集合的元素, 从大到小排序, 并返回score值
//     *
//     * @param key
//     * @param start
//     * @param end
//     * @return
//     */
//    public Set<ZSetOperations.TypedTuple<String>> zReverseRangeWithScores(String key,
//                                                                          long start, long end) {
//        return redisTemplate.opsForZSet().reverseRangeWithScores(key, start,
//                end);
//    }
//
//    /**
//     * 根据Score值查询集合元素, 从大到小排序
//     *
//     * @param key
//     * @param min
//     * @param max
//     * @return
//     */
//    public Set<String> zReverseRangeByScore(String key, double min,
//                                            double max) {
//        return redisTemplate.opsForZSet().reverseRangeByScore(key, min, max);
//    }
//
//    /**
//     * 根据Score值查询集合元素, 从大到小排序
//     *
//     * @param key
//     * @param min
//     * @param max
//     * @return
//     */
//    public Set<ZSetOperations.TypedTuple<String>> zReverseRangeByScoreWithScores(
//            String key, double min, double max) {
//        return redisTemplate.opsForZSet().reverseRangeByScoreWithScores(key,
//                min, max);
//    }
//
//    /**
//     * @param key redis中的key
//     * @param min score的最小值
//     * @param max score的最大值
//     * @param offset 指定位置
//     * @param count 获取元素的数量
//     * @return
//     */
//    public Set<String> zReverseRangeByScore(String key, double min, double max, long offset, long count) {
//        return redisTemplate.opsForZSet().reverseRangeByScore(key, min, max, offset, count);
//    }
//
//    /**
//     * 根据score值获取集合元素数量
//     *
//     * @param key
//     * @param min
//     * @param max
//     * @return
//     */
//    public Long zCount(String key, double min, double max) {
//        return redisTemplate.opsForZSet().count(key, min, max);
//    }
//
//    /**
//     * 获取集合大小
//     *
//     * @param key
//     * @return
//     */
//    public Long zSize(String key) {
//        return redisTemplate.opsForZSet().size(key);
//    }
//
//    /**
//     * 获取集合大小
//     *
//     * @param key
//     * @return
//     */
//    public Long zZCard(String key) {
//        return redisTemplate.opsForZSet().zCard(key);
//    }
//
//    /**
//     * 获取集合中value元素的score值
//     *
//     * @param key
//     * @param value
//     * @return
//     */
//    public Double zScore(String key, Object value) {
//        return redisTemplate.opsForZSet().score(key, value);
//    }
//
//    /**
//     * 移除指定索引位置的成员
//     *
//     * @param key
//     * @param start
//     * @param end
//     * @return
//     */
//    public Long zRemoveRange(String key, long start, long end) {
//        return redisTemplate.opsForZSet().removeRange(key, start, end);
//    }
//
//    /**
//     * 根据指定的score值的范围来移除成员
//     *
//     * @param key
//     * @param min
//     * @param max
//     * @return
//     */
//    public Long zRemoveRangeByScore(String key, double min, double max) {
//        return redisTemplate.opsForZSet().removeRangeByScore(key, min, max);
//    }
//
//    /**
//     * 获取key和otherKey的并集并存储在destKey中
//     *
//     * @param key
//     * @param otherKey
//     * @param destKey
//     * @return
//     */
//    public Long zUnionAndStore(String key, String otherKey, String destKey) {
//        return redisTemplate.opsForZSet().unionAndStore(key, otherKey, destKey);
//    }
//
//    /**
//     * @param key
//     * @param otherKeys
//     * @param destKey
//     * @return
//     */
//    public Long zUnionAndStore(String key, Collection<String> otherKeys,
//                               String destKey) {
//        return redisTemplate.opsForZSet()
//                .unionAndStore(key, otherKeys, destKey);
//    }
//
//    /**
//     * 交集
//     *
//     * @param key
//     * @param otherKey
//     * @param destKey
//     * @return
//     */
//    public Long zIntersectAndStore(String key, String otherKey,
//                                   String destKey) {
//        return redisTemplate.opsForZSet().intersectAndStore(key, otherKey,
//                destKey);
//    }
//
//    /**
//     * 交集
//     *
//     * @param key
//     * @param otherKeys
//     * @param destKey
//     * @return
//     */
//    public Long zIntersectAndStore(String key, Collection<String> otherKeys,
//                                   String destKey) {
//        return redisTemplate.opsForZSet().intersectAndStore(key, otherKeys,
//                destKey);
//    }
//
//    /**
//     * @param key
//     * @param options
//     * @return
//     */
//    public Cursor<ZSetOperations.TypedTuple<String>> zScan(String key, ScanOptions options) {
//        return redisTemplate.opsForZSet().scan(key, options);
//    }
//}