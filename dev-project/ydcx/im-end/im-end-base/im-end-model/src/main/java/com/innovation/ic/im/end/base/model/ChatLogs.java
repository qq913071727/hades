package com.innovation.ic.im.end.base.model;

import lombok.Data;
import org.jivesoftware.util.JiveConstants;

import java.sql.Timestamp;

/**
 * 聊天记录对象实体
 */
@Data
public class ChatLogs {

    private long messageId;
    private String sessionJid;
    private String sender;
    private String receiver;
    private Timestamp createDate;
    private String content;
    private String detail;
    private int length;
    private int state; // 1 表示删除

    public interface LogState {
        int show = 0;
        int remove = 1;
    }

    /**
     * 自增id序列管理器，类型变量
     */
    public class ChatLogsConstants extends JiveConstants {
        // 日志表id自增对应类型
        public static final int CHAT_LOGS = 52;
        // 用户在线统计id自增对应类型
        public static final int USER_ONLINE_STATE = 53;
    }

    public ChatLogs() {
    }

    public ChatLogs(String sessionJid, Timestamp createDate, String content, String detail, int length) {
        super();
        this.sessionJid = sessionJid;
        this.createDate = createDate;
        this.content = content;
        this.detail = detail;
        this.length = length;
    }

    public ChatLogs(long messageId, String sessionJid, Timestamp createDate, String content, String detail, int length, int state) {
        super();
        this.messageId = messageId;
        this.sessionJid = sessionJid;
        this.createDate = createDate;
        this.content = content;
        this.detail = detail;
        this.length = length;
        this.state = state;
    }
}
