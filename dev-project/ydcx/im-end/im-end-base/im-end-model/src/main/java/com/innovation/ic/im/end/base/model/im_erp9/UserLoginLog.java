package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginLog", description = "用户自定义群组消息")
@TableName("user_login_log")
public class UserLoginLog {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "登录类型。1表示点对点，2表示默认群组，3表示用户自定义群组", dataType = "Integer")
    @TableField(value = "type_")
    private Integer type;

    @ApiModelProperty(value = "账号", dataType = "Date")
    @TableField(value = "create_time")
    private Data createtime;

    @ApiModelProperty(value = "组id", dataType = "String")
    @TableField(value = "group_id")
    private String groupid;

    @ApiModelProperty(value = "地址", dataType = "String")
    @TableField(value = "remote_address")
    private String remoteaddress;
}
