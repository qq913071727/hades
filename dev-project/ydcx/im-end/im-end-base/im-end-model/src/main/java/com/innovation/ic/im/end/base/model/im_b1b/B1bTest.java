package com.innovation.ic.im.end.base.model.im_b1b;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 对应b1b系统的test表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "B1bTest", description = "对应b1b系统的test表")
@TableName("b1b_test")
public class B1bTest {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "Admins表的主键", dataType = "String")
    @TableField(value = "admins_id")
    private String adminsId;

    @ApiModelProperty(value = "员工ID", dataType = "String")
    @TableField(value = "staff_id")
    private String staffId;

    @ApiModelProperty(value = "登入名称", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    @TableField(value = "real_name")
    private String realName;

    @ApiModelProperty(value = "登入密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "默认使用：ID的数字部分。即，001", dataType = "String")
    @TableField(value = "sel_code")
    private String selCode;

    @ApiModelProperty(value = "角色ID", dataType = "String")
    @TableField(value = "role_id")
    private String roleId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "修改日期", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;

    @ApiModelProperty(value = "管理员（在系统中要时刻显示管理员的在职与离职状态）：900表示超级管理员、200表示正常（管理员）、500表示停用，901表示Npc", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "最终登入时间", dataType = "Date")
    @TableField(value = "last_login_date")
    private Date lastLoginDate;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "origin_id")
    private String originId;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "dyj_code")
    private String dyjCode;
}
