package com.innovation.ic.im.end.base.model.erp9_sqlserver;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 对应erp9系统的Structures表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Erp9Structures", description = "对应erp9系统的Structures表")
@TableName("Structures")
public class Erp9Structures {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.AUTO)
    private String id;

//    @ApiModelProperty(value = "Structures表的主键", dataType = "String")
//    @TableField(value = "structures_id")
//    private String structuresId;

    @ApiModelProperty(value = "父节点id", dataType = "String")
    @TableField(value = "FatherID")
    private String fatherId;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "Name")
    private String name;

    @ApiModelProperty(value = "树形目录同级节点排序序号", dataType = "Integer")
    @TableField(value = "OrderIndex")
    private Integer orderIndex;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "状态。200表示正常", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;

//    @ApiModelProperty(value = "erp9系统中，StructureNodes表的id", dataType = "String")
//    @TableField(value = "struc_node_id")
//    private String strucNodeId;

//    @ApiModelProperty(value = "erp9系统中，StructureNodes表的Type字段", dataType = "Integer")
//    @TableField(value = "type")
//    private Integer type;

//    @ApiModelProperty(value = "erp9系统中，StructureNodes表的Code字段", dataType = "String")
//    @TableField(value = "code")
//    private String code;

    @ApiModelProperty(value = "erp9系统中，StructureNodes表的Summary字段", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "erp9系统中，StructureNodes表的CreatorID字段", dataType = "String")
    @TableField(value = "CreatorID")
    private String creatorId;

}
