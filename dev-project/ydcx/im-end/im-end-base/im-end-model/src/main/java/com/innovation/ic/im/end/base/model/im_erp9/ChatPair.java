package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 聊天对
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ChatPair", description = "聊天对")
@TableName("chat_pair")
public class ChatPair {
    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    @TableField(value = "from_user_account")
    private String fromUserAccount;

    @ApiModelProperty(value = "接收消息的用户的账号", dataType = "String")
    @TableField(value = "to_user_account")
    private String toUserAccount;

    @ApiModelProperty(value = "每个聊天对的代码", dataType = "Integer")
    @TableField(value = "code_")
    private Integer code;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    @TableField(value = "topping_time", updateStrategy = FieldStrategy.IGNORED)
    private Date toppingTime;

    @ApiModelProperty(value = "最近联系时间", dataType = "Date")
    @TableField(value = "last_contact_time")
    private Date lastContactTime;

    @ApiModelProperty(value = "内容不可见时间", dataType = "Date")
    @TableField(value = "content_invisible_time")
    private Date contentInvisibleTime;

    @ApiModelProperty(value = "聊天页面打开状态(0:表示聊天窗口关闭、1:表示聊天窗口打开)", dataType = "Integer")
    @TableField(value = "chat_page_status")
    private Integer chatPageStatus;

    @ApiModelProperty(value = "是否消息免打扰。1表示是，0表示否", dataType = "Integer")
    @TableField(value = "no_reminder")
    private Integer noReminder;
}