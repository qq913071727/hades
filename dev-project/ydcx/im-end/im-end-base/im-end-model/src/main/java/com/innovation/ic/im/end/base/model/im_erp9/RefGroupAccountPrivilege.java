package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RefGroupAccountPrivilege", description = "账号对默认群组的权限表")
@TableName("ref_group_account_privilege")
public class RefGroupAccountPrivilege {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "account表的id", dataType = "String")
    @TableField(value = "account_id")
    private String accountId;

    @ApiModelProperty(value = "账号。account表的username字段", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "默认群组id", dataType = "String")
    @TableField(value = "group_id")
    private String groupId;
}