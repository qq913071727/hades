package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 话术
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Dialogue", description = "话术")
@TableName("dialogue")
public class Dialogue {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "erp9_admins表的id", dataType = "String")
    @TableField(value = "account_id")
    private String accountId;

    @ApiModelProperty(value = "分组", dataType = "String")
    @TableField(value = "group_")
    private String group;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    @TableField(value = "topping_time")
    private Date toppingTime;

    @ApiModelProperty(value = "内容", dataType = "String")
    @TableField(value = "content")
    private String content;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "update_time")
    private Date updateTime;
}
