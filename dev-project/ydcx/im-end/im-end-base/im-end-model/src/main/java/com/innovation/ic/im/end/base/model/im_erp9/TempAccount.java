package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   账号详细信息备份表
 * @author linuo
 * @time   2022年7月5日16:37:21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "TempAccount", description = "账号详细信息")
@TableName("temp_account")
public class TempAccount {
    @ApiModelProperty(value = "主键。erp9系统的Admins表的id字段", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "真实姓名。erp9系统的Admins表的RealName字段", dataType = "String")
    @TableField(value = "real_name")
    private String realName;

    @ApiModelProperty(value = "账号。erp9系统的Admins表的UserName字段", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "大赢家code。erp9系统的Admins表的DyjCode字段", dataType = "String")
    @TableField(value = "dyj_code")
    private String dyjCode;

    @ApiModelProperty(value = "汉语拼音", dataType = "String")
    @TableField(value = "pin_yin")
    private String pinYin;

    @ApiModelProperty(value = "汉语拼音首字母", dataType = "String")
    @TableField(value = "capital_initial")
    private String capitalInitial;

    @ApiModelProperty(value = "部门。erp9系统的Structures表的Name字段。如果部门重名，则在前面加上上一级部门名称，中间用-", dataType = "String")
    @TableField(value = "department_name")
    private String departmentName;

    @ApiModelProperty(value = "电话号码。erp9系统的Staffs表的Mobile字段", dataType = "String")
    @TableField(value = "mobile")
    private String mobile;

    @ApiModelProperty(value = "通过erp9登录的状态。1表示已登录，0表示未登录", dataType = "Integer")
    @TableField(value = "erp9_login")
    private Integer erp9Login;

    @ApiModelProperty(value = "通过erp9的最近登录时间", dataType = "Date")
    @TableField(value = "last_erp9_login_time")
    private Date lastErp9LoginTime;

    @ApiModelProperty(value = "通过芯聊客户端登录的状态。1表示已登录，0表示未登录", dataType = "Integer")
    @TableField(value = "xl_login")
    private Integer xlLogin;

    @ApiModelProperty(value = "通过芯聊客户端的最近登录时间", dataType = "Date")
    @TableField(value = "last_xl_login_time")
    private Date lastXlLoginTime;

    @ApiModelProperty(value = "角色(1: 超级管理员)", dataType = "Integer")
    @TableField(value = "role")
    private Integer role;
}