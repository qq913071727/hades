package com.innovation.ic.im.end.base.model.erp9_sqlserver;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 对应erp9系统的Admins表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Erp9Admins", description = "对应erp9系统的Admins表")
@TableName("Admins")
public class Erp9Admins {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.AUTO)
    private String id;

//    @ApiModelProperty(value = "Admins表的主键", dataType = "String")
//    @TableField(value = "admins_id")
//    private String adminsId;

    @ApiModelProperty(value = "员工ID", dataType = "String")
    @TableField(value = "StaffID")
    private String staffId;

    @ApiModelProperty(value = "登入名称", dataType = "String")
    @TableField(value = "UserName")
    private String username;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    @TableField(value = "RealName")
    private String realName;

    @ApiModelProperty(value = "登入密码", dataType = "String")
    @TableField(value = "Password")
    private String password;

    @ApiModelProperty(value = "默认使用：ID的数字部分。即，001", dataType = "String")
    @TableField(value = "SelCode")
    private String selCode;

    @ApiModelProperty(value = "角色ID", dataType = "String")
    @TableField(value = "RoleID")
    private String roleId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改日期", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "管理员（在系统中要时刻显示管理员的在职与离职状态）：900表示超级管理员、200表示正常（管理员）、500表示停用，901表示Npc", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;

    @ApiModelProperty(value = "最终登入时间", dataType = "Date")
    @TableField(value = "LastLoginDate")
    private Date lastLoginDate;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "OriginID")
    private String originId;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "DyjCode")
    private String dyjCode;
}
