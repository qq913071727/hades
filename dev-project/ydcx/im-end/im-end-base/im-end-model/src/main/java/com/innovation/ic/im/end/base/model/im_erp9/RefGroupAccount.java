package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RefGroupAccount", description = "群组和账号的关系表")
@TableName("ref_group_account")
public class RefGroupAccount {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "群组id", dataType = "String")
    @TableField(value = "group_id")
    private String groupId;

    @ApiModelProperty(value = "账号id", dataType = "String")
    @TableField(value = "account_id")
    private String accountId;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String username;
}