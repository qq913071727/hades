package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "GroupMessage", description = "群组消息")
@TableName("group_message")
public class GroupMessage {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "群组id", dataType = "String")
    @TableField(value = "group_id")
    private String groupId;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    @TableField(value = "from_user_account")
    private String fromUserAccount;

    @ApiModelProperty(value = "内容", dataType = "String")
    @TableField(value = "content")
    private String content;

    @ApiModelProperty(value = "消息类型。1表示文字，2表示文件，3表示图片，4表示系统消息", dataType = "Integer")
    @TableField(value = "type_")
    private Integer type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    @TableField(value = "file_path")
    private String filePath;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    @TableField(value = "from_user_real_name")
    private String fromUserRealName;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "消息撤回时间", dataType = "Date")
    @TableField(value = "retraction_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date retractionTime;

    @ApiModelProperty(value = "发送消息的是否可用。1表示可用，2表示不可用", dataType = "Integer")
    @TableField(value = "from_user_available")
    private Integer fromUserAvailable;

    @ApiModelProperty(value = "发送消息的人不可用的时间", dataType = "Date")
    @TableField(value = "from_user_unavailable_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date fromUserUnavailableTime;

    @ApiModelProperty(value = "撤回消息内容", dataType = "String")
    @TableField(value = "retraction_content")
    private String retractionContent;

    @ApiModelProperty(value = "撤回消息id", dataType = "Integer")
    @TableField(value = "retraction_msg_id")
    private Integer retractionMsgId;

    public GroupMessage(Integer id, String groupId, String fromUserAccount, String content, Integer type, String filePath, String fromUserRealName, Date createTime, Date retractionTime, Integer fromUserAvailable, Date fromUserUnavailableTime) {
        this.id = id;
        this.groupId = groupId;
        this.fromUserAccount = fromUserAccount;
        this.content = content;
        this.type = type;
        this.filePath = filePath;
        this.fromUserRealName = fromUserRealName;
        this.createTime = createTime;
        this.retractionTime = retractionTime;
        this.fromUserAvailable = fromUserAvailable;
        this.fromUserUnavailableTime = fromUserUnavailableTime;
    }

    public GroupMessage() {
    }
}