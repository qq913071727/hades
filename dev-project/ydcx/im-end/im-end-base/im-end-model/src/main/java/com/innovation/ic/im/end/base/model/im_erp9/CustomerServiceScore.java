package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   客服评分表
 * @author linuo
 * @time   2023年4月20日16:52:45
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CustomerServiceScore", description = "客服评分表")
@TableName("customer_service_score")
public class CustomerServiceScore {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "供应商协同账号id", dataType = "String")
    @TableField(value = "sc_account_id")
    private String scAccountId;

    @ApiModelProperty(value = "销售账号id", dataType = "String")
    @TableField(value = "sale_account_id")
    private String saleAccountId;

    @ApiModelProperty(value = "评分", dataType = "Integer")
    @TableField(value = "score")
    private Integer score;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}