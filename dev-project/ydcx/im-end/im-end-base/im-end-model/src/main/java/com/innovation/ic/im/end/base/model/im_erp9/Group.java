package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Group", description = "群组")
@TableName("group_")
public class Group {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @ApiModelProperty(value = "群组名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "群组成员数量", dataType = "Integer")
    @TableField(value = "membership")
    private Integer membership;

//    @ApiModelProperty(value = "类型。1表示默认生成的部门类型群组。2表示用户自己创建的群组", dataType = "Integer")
//    @TableField(value = "type_")
//    private Integer type;
}
