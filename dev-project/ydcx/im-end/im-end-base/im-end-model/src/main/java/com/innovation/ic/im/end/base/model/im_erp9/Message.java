package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Message", description = "消息")
@TableName("message")
public class Message {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    @TableField(value = "from_user_account")
    private String fromUserAccount;

    @ApiModelProperty(value = "接收消息的用户的账号", dataType = "String")
    @TableField(value = "to_user_account")
    private String toUserAccount;

    @ApiModelProperty(value = "内容", dataType = "String")
    @TableField(value = "content")
    private String content;

    @ApiModelProperty(value = "消息类型。1表示文字，2表示文件，3表示图片，4表示系统消息，5修改自定义群组名称的系统消息，6公告栏提醒，7二次比价任务提醒，8销售订单推送失败", dataType = "Integer")
    @TableField(value = "type_")
    private Integer type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    @TableField(value = "file_path")
    private String filePath;

    @ApiModelProperty(value = "消息是否已读。1表示未读，2表示已读", dataType = "Integer")
    @TableField(value = "read_")
    private Integer read;

    @ApiModelProperty(value = "读取消息的时间", dataType = "Date")
    @TableField(value = "read_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date readTime;

    @ApiModelProperty(value = "发送消息的人是否可用。1表示可用，2表示不可用", dataType = "Integer")
    @TableField(value = "from_user_available")
    private Integer fromUserAvailable;

    @ApiModelProperty(value = "发送消息的人不可用的时间", dataType = "Date")
    @TableField(value = "from_user_unavailable_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date fromUserUnavailableTime;

    @ApiModelProperty(value = "接收消息的人是否可用。1表示可用，2表示不可用", dataType = "Integer")
    @TableField(value = "to_user_available")
    private Integer toUserAvailable;

    @ApiModelProperty(value = "接收消息的人不可用的时间", dataType = "Date")
    @TableField(value = "to_user_unavailable_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date toUserUnavailableTime;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    @TableField(value = "from_user_real_name")
    private String fromUserRealName;

    @ApiModelProperty(value = "接收消息的用户真实姓名", dataType = "String")
    @TableField(value = "to_user_real_name")
    private String toUserRealName;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "消息撤回时间", dataType = "Date")
    @TableField(value = "retraction_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date retractionTime;

    @ApiModelProperty(value = "撤回消息内容", dataType = "String")
    @TableField(value = "retraction_content")
    private String retractionContent;

    @ApiModelProperty(value = "撤回消息id", dataType = "Integer")
    @TableField(value = "retraction_msg_id")
    private Integer retractionMsgId;

    public Message(Integer id, String fromUserAccount, String toUserAccount, String content, Integer type, String filePath, Integer read, Date readTime, Integer fromUserAvailable, Date fromUserUnavailableTime, Integer toUserAvailable, Date toUserUnavailableTime, String fromUserRealName, String toUserRealName, Date createTime, Date retractionTime) {
        this.id = id;
        this.fromUserAccount = fromUserAccount;
        this.toUserAccount = toUserAccount;
        this.content = content;
        this.type = type;
        this.filePath = filePath;
        this.read = read;
        this.readTime = readTime;
        this.fromUserAvailable = fromUserAvailable;
        this.fromUserUnavailableTime = fromUserUnavailableTime;
        this.toUserAvailable = toUserAvailable;
        this.toUserUnavailableTime = toUserUnavailableTime;
        this.fromUserRealName = fromUserRealName;
        this.toUserRealName = toUserRealName;
        this.createTime = createTime;
        this.retractionTime = retractionTime;
    }

    public Message() {
    }
}