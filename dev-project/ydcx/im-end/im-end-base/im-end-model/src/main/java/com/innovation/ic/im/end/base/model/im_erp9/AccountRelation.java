package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   账号关系表(记录供应商协同账号与销售账号的关联关系)
 * @author linuo
 * @time   2023年4月18日17:15:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountRelation", description = "账号关系表(记录供应商协同账号与销售账号的关联关系)")
@TableName("account_relation")
public class AccountRelation {
    @ApiModelProperty(value = "主键id", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "供应商协同账号id", dataType = "String")
    @TableField(value = "sc_account_id")
    private String scAccountId;

    @ApiModelProperty(value = "销售账号id", dataType = "String")
    @TableField(value = "sale_account_id")
    private String saleAccountId;
}