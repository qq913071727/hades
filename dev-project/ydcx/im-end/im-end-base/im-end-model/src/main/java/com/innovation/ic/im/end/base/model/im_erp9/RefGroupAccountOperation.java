package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RefGroupAccountOperation", description = "账号和群组的操作表")
@TableName("ref_group_account_operation")
public class RefGroupAccountOperation {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "群组id", dataType = "String")
    @TableField(value = "group_id")
    private String groupId;

    @ApiModelProperty(value = "账号id", dataType = "String")
    @TableField(value = "account_id")
    private String accountId;

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "是否消息免打扰。1表示是，null表示否", dataType = "Integer")
    @TableField(value = "no_reminder")
    private Integer noReminder;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    @TableField(value = "topping_time", updateStrategy = FieldStrategy.IGNORED)
    private Date toppingTime;

    @ApiModelProperty(value = "最近联系时间", dataType = "Date")
    @TableField(value = "last_contact_time")
    private Date lastContactTime;

    @ApiModelProperty(value = "内容不可见时间", dataType = "Date")
    @TableField(value = "content_invisible_time")
    private Date contentInvisibleTime;
}