package com.innovation.ic.im.end.base.model.erp9_sqlserver;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 对应erp9系统的MapsStructure表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Erp9MapsStructure", description = "对应erp9系统的MapsStructure表")
@TableName("MapsStructure")
public class Erp9MapsStructure {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.AUTO)
    private String id;

    @ApiModelProperty(value = "部门id", dataType = "String")
    @TableField(value = "StructureID")
    private String structureID;

    @ApiModelProperty(value = "人员id", dataType = "String")
    @TableField(value = "StaffID")
    private String staffID;

    @ApiModelProperty(value = "职务id", dataType = "String")
    @TableField(value = "PositionID")
    private String positionID;

    @ApiModelProperty(value = "是否时领导，0表示不是领导，1表示是领导", dataType = "Integer")
    @TableField(value = "IsLeader")
    private Integer IsLeader;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "OrderIndex")
    private Integer orderIndex;

}
