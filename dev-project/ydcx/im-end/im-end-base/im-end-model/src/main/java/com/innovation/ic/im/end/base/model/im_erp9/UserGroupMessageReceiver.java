package com.innovation.ic.im.end.base.model.im_erp9;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserGroupMessageReceiver", description = "用户自定义群组未读消息")
@TableName("user_group_message_receiver")
public class UserGroupMessageReceiver {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "自定义群组id", dataType = "Integer")
    @TableField(value = "user_group_id")
    private Integer userGroupId;

    @ApiModelProperty(value = "自定义群组消息表id", dataType = "Integer")
    @TableField(value = "user_group_message_id")
    private Integer userGroupMessageId;

    @ApiModelProperty(value = "还没有看到消息的用户的账号", dataType = "String")
    @TableField(value = "to_user_account")
    private String toUserAccount;

    @ApiModelProperty(value = "接收消息的用户真实姓名", dataType = "String")
    @TableField(value = "to_user_real_name")
    private String toUserRealName;

    @ApiModelProperty(value = "消息是否已读。1表示未读，2表示已读", dataType = "Integer")
    @TableField(value = "read_")
    private Integer read;

    @ApiModelProperty(value = "消息读取时间", dataType = "Date")
    @TableField(value = "read_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date readTime;

    @ApiModelProperty(value = "接收消息是否可用。1表示可用，2表示不可用", dataType = "Integer")
    @TableField(value = "to_user_available")
    private Integer toUserAvailable;

    @ApiModelProperty(value = "接收消息的人不可用的时间", dataType = "Date")
    @TableField(value = "to_user_unavailable_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date toUserUnavailableTime;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    public UserGroupMessageReceiver(Integer id, Integer userGroupId, Integer userGroupMessageId, String toUserAccount, String toUserRealName, Integer read, Date readTime, Integer toUserAvailable, Date toUserUnavailableTime) {
        this.id = id;
        this.userGroupId = userGroupId;
        this.userGroupMessageId = userGroupMessageId;
        this.toUserAccount = toUserAccount;
        this.toUserRealName = toUserRealName;
        this.read = read;
        this.readTime = readTime;
        this.toUserAvailable = toUserAvailable;
        this.toUserUnavailableTime = toUserUnavailableTime;
    }

    public UserGroupMessageReceiver() {
    }
}