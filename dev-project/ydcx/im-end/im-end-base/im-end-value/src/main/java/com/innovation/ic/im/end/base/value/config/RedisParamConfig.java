package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   redis配置类
 * @author linuo
 * @time   2022年7月15日17:13:27
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.redis")
public class RedisParamConfig {
    /** 连接超时时间 */
    private Integer timeout;
}