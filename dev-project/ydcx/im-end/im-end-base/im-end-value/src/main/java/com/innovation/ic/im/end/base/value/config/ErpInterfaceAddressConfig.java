package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   erp接口地址配置类
 * @author linuo
 * @time   2022年7月26日16:56:59
 */
@Data
@Component
@ConfigurationProperties(prefix = "erp")
public class ErpInterfaceAddressConfig {
    /** 获取用户信息 */
    private String getInfo;
}