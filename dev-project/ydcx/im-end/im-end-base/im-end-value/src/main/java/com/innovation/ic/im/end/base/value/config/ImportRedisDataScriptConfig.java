package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   导入redis数据脚本配置类
 * @author linuo
 * @time   2023年4月24日15:14:07
 */
@Data
@Component
@ConfigurationProperties(prefix = "importdata")
public class ImportRedisDataScriptConfig {
    String[] scripts;
}