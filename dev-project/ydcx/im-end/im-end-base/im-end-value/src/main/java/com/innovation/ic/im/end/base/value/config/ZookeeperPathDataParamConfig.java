package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   im-end-data的Zookeeper路径配置类
 * @author linuo
 * @time   2022年7月18日14:19:47
 */
@Data
@Component
@ConfigurationProperties(prefix = "zookeeper.path.im-end.im-end-data")
public class ZookeeperPathDataParamConfig {
    private String importAccountAndChatPair;

    private String importDefaultGroup;

    private String importTreeNode;

    private String importRedisData;

    private String changeLoginStatus;

    private String redisTiming;

    private String initImportRedisData;
}