package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   图片配置类
 * @author linuo
 * @time   2022年6月21日13:25:00
 */
@Data
@Component
@ConfigurationProperties(prefix = "picture")
public class PictureParamConfig {
    private String urlprefix;
}