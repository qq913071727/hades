package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * @desc   历史数据超时配置类
 * @author linuo
 * @time   2022年7月28日15:50:47
 */
@Data
@Component
@ConfigurationProperties(prefix = "history")
public class HistoryTimeoutConfig {
    /** 获取用户信息 */
    private Map<String, Integer> timeout;
}