package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   系统相关配置类
 * @author linuo
 * @time   2023年5月6日15:01:58
 */
@Data
@Component
@ConfigurationProperties(prefix = "system")
public class SystemConfig {
    /** 系统账号 */
    private String account;
}