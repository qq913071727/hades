package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * @desc   sftp脚本配置类
 * @author linuo
 * @time   2022年10月26日13:14:02
 */
@Data
@Component
@ConfigurationProperties(prefix = "sftp.script")
public class SftpScriptParamConfig {
    /** 导入redis数据 */
    private Map<String, String> importRedis;
}