package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 文件存入服务器
 */
@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinioConfig {
    /** minio连接地址 */
    private String endpoint;

    /** 用户名 */
    private String accessKey;

    /** 密码 */
    private String secretKey;

    /** 存放文件的文件夹名称 */
    private String bucketName;

    /** 存放历史文件的文件夹名称 */
    private String bucketHistoryName;
}