package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   Zookeeper配置类
 * @author linuo
 * @time   2022年6月20日20:58:57
 */
@Data
@Component
@ConfigurationProperties(prefix = "zookeeper")
public class ZookeeperParamConfig {
    private Long waitingLockTime;

    private Long initImportRedisDataWaitingLockTime;

    private String host;

    private Integer baseSleepTimeMs;

    private Integer maxEntries;

    private Integer sessionTimeoutMs;

    private Integer connectionTimeoutMs;
}