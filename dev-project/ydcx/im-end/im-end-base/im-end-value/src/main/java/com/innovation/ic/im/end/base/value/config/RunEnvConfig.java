package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   项目启动环境配置类
 * @author linuo
 * @time   2022年7月19日10:32:19
 */
@Data
@Component
@ConfigurationProperties(prefix = "run")
public class RunEnvConfig {
    /** 项目启动环境 */
    private String env;
}