package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   im-end-web的Zookeeper路径配置类
 * @author linuo
 * @time   2022年8月29日11:28:59
 */
@Data
@Component
@ConfigurationProperties(prefix = "zookeeper.path.im-end.im-end-web")
public class ZookeeperPathWebParamConfig {
    /** 一对一聊天上传文件 */
    private String endPointUpload;

    /**  默认群组聊天上传文件 */
    private String groupEndPointUpload;

    /** 自定义群组聊天上传文件 */
    private String userGroupEndPointUpload;
}