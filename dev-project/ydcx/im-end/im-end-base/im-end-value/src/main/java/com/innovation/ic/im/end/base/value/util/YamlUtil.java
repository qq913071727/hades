package com.innovation.ic.im.end.base.value.util;

import org.yaml.snakeyaml.Yaml;
import java.util.Map;

/**
 * yaml工具类
 */
public class YamlUtil {
    /**
     * 返回application.yml文件中的value
     * @param env 当前启动环境
     * @return 返回配置文件中的内容
     */
    public static Map<String, Object> getServerMap(String env) {
        Yaml yaml = new Yaml();
        return yaml.load(YamlUtil.class.getResourceAsStream("/" + "application-" + env + ".yml"));
    }
}