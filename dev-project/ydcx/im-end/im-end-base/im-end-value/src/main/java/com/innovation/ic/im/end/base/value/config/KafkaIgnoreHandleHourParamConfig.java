package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   kafka忽略处理时间参数配置类
 * @author linuo
 * @time   2022年10月26日10:57:04
 */
@Data
@Component
@ConfigurationProperties(prefix = "kafka.ignore")
public class KafkaIgnoreHandleHourParamConfig {
    /** 时间 */
    private int hour;
}