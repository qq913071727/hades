package com.innovation.ic.im.end.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   文件配置类
 * @author linuo
 * @time   2022年7月5日10:02:52
 */
@Data
@Component
@ConfigurationProperties(prefix = "file")
public class FileParamConfig {
    /** 文件地址 */
    private String fileUrl;

    /** 历史文件地址 */
    private String fileHistoryUrl;
}