package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   登录用户vo类
 * @author linuo
 * @time   2022年7月25日16:07:11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "LoginUserVo", description = "登录用户vo类")
public class LoginTypeVo {
    @ApiModelProperty(value = "用户名", dataType = "String")
    private String username;

    @ApiModelProperty(value = "真实姓名", dataType = "Integer")
    private Integer type_;

    public LoginTypeVo(String username, Integer type){
        this.username = username;
        this.type_ = type;
    }
}