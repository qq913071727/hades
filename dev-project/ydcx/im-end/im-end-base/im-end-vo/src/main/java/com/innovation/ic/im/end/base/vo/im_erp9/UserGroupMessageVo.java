package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.nio.ByteBuffer;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserGroupMessageVo", description = "客户端发来的用户自定义群组消息")
public class UserGroupMessageVo {
    @ApiModelProperty(value = "群组id", dataType = "Integer")
    public Integer userGroupId;

    @ApiModelProperty(value = "内容", dataType = "String")
    public String content;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    public String fromUserAccount;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    private String fromUserRealName;

    @ApiModelProperty(value = "接收消息的用户真实姓名", dataType = "String")
    private String toUserRealName;

    @ApiModelProperty(value = "消息类型。-1表示心跳，1表示文字，2表示文件，3表示图片", dataType = "Integer")
    public Integer type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    public String filePath;

    @ApiModelProperty(value = "每页的行数", dataType = "Integer")
    public Integer pageSize;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    public Integer pageNo;

    @ApiModelProperty(value = "自定义群组消息id", dataType = "String")
    private String userGroupMessageId;

    @ApiModelProperty(value = "前端生成的消息id", dataType = "String")
    public String vueId;

    /** 上传图片临时数据 */
    public ByteBuffer byteBuffer;

    public UserGroupMessageVo() {
    }

    public UserGroupMessageVo(Integer userGroupId, String content, String fromUserAccount,
                              Integer type, String filePath) {
        this.userGroupId = userGroupId;
        this.content = content;
        this.fromUserAccount = fromUserAccount;
        this.type = type;
        this.filePath = filePath;
    }

    public UserGroupMessageVo(Integer userGroupId, String content, String fromUserAccount, Integer type, String filePath, Integer pageSize, Integer pageNo) {
        this.userGroupId = userGroupId;
        this.content = content;
        this.fromUserAccount = fromUserAccount;
        this.type = type;
        this.filePath = filePath;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }
}
