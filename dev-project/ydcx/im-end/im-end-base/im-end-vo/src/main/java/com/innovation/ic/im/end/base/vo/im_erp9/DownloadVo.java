package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UploadVo", description = "下载vo类")
public class DownloadVo {

    @ApiModelProperty(value = "文件名", dataType = "String")
    private String filename;

    @ApiModelProperty(value = "文件类型", dataType = "Integer")
    private Integer type;
}
