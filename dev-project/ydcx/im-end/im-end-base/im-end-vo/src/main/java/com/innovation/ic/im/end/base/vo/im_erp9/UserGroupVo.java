package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserGroupVo", description = "用户自定义群组vo类")
public class UserGroupVo {

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "群组名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "群组成员数量", dataType = "Integer")
    private Integer membership;

    @ApiModelProperty(value = "群组新成员的账号id列表", dataType = "List")
    private List<String> newMemberAccountIdList;

    @ApiModelProperty(value = "修改自定义群组的用户", dataType = "String")
    private String updateUserGroupNameAccount;

    @ApiModelProperty(value = "修改自定义群组的用户真实姓名", dataType = "String")
    private String updateUserGroupNameRealName;
}