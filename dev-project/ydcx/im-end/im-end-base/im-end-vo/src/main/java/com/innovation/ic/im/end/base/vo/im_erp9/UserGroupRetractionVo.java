package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 自定义群组，撤回消息的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RetractionVo", description = "自定义群组，撤回消息的vo类")
public class UserGroupRetractionVo {

    @ApiModelProperty(value = "自定义群组消息id", dataType = "Integer")
    private Integer userGroupMessageId;

//    @ApiModelProperty(value = "自定义群组未读消息id列表", dataType = "List")
//    private List<Integer> userGroupOfflineMessageIdList;

    @ApiModelProperty(value = "撤回消息的时间", dataType = "Date")
    private Date retractionTime;
}
