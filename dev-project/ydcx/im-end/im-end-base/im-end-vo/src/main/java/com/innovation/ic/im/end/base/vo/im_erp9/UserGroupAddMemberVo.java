package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserGroupVo", description = "用户自定义群组添加成员vo类")
public class UserGroupAddMemberVo {
    @ApiModelProperty(value = "自定义群组id", dataType = "Integer")
    private Integer userGroupId;

    @ApiModelProperty(value = "自定义群组新成员的账号id列表", dataType = "List")
    private List<String> newMemberAccountIdList;
}