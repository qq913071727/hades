package com.innovation.ic.im.end.base.vo.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.Dialogue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DialogueVo", description = "话术的vo类")
public class DialogueVo {

    @ApiModelProperty(value = "dialogue表的id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "账号id", dataType = "String")
    private String accountId;

    @ApiModelProperty(value = "话术分组", dataType = "String")
    private String group;

    @ApiModelProperty(value = "内容", dataType = "String")
    private String content;

    /**
     * 将Dialogue对象转换为Dialogue对象。create如果为true，在createTime设置为new Date()。
     * @param create
     * @return
     */
    public Dialogue toDialogue(boolean create) {
        Dialogue dialogue = new Dialogue();
        if (null != this.getId()) {
            dialogue.setId(this.getId());
        }
        if (null != this.getAccountId()) {
            dialogue.setAccountId(this.getAccountId());
        }
        if (null != this.getContent()) {
            dialogue.setContent(this.getContent());
        }
        if (null != this.getGroup()) {
            dialogue.setGroup(this.getGroup());
        }
        if (create){
            dialogue.setCreateTime(new Date());
        }
        return dialogue;
    }

}
