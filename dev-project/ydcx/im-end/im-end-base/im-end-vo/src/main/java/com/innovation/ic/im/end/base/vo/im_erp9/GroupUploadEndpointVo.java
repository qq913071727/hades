package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "GroupUploadEndpointVo", description = "群组上传文件vo类")
public class GroupUploadEndpointVo {

    @ApiModelProperty(value = "群组id", dataType = "String")
    private String groupId;

    @ApiModelProperty(value = "发送文件的用户的账号", dataType = "String")
    private String username;

    @ApiModelProperty(value = "文件名", dataType = "String")
    private String filename;
}
