package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   客户端发来的群组消息
 * @author linuo
 * @time   2022年6月21日13:51:00
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SendGroupMessageVo", description = "默认群组聊天推送的消息")
public class SendGroupMessageVo {
    @ApiModelProperty(value = "群组id", dataType = "String")
    public String groupId;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    public String fromUserAccount;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    private String fromUserRealName;

    @ApiModelProperty(value = "接收消息的用户真实姓名", dataType = "String")
    private String toUserRealName;

    @ApiModelProperty(value = "内容", dataType = "String")
    public String content;

    @ApiModelProperty(value = "消息类型。-1表示心跳，1表示文字，2表示文件，3表示图片", dataType = "Integer")
    public Integer type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    public String filePath;

    @ApiModelProperty(value = "每页的行数", dataType = "Integer")
    public Integer pageSize;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    public Integer pageNo;

    @ApiModelProperty(value = "消息撤回时间", dataType = "Date")
    private Date retractionTime;

    @ApiModelProperty(value = "默认群组消息id", dataType = "String")
    private String groupMessageId;

    @ApiModelProperty(value = "前端生成的消息id", dataType = "String")
    public String vueId;
}