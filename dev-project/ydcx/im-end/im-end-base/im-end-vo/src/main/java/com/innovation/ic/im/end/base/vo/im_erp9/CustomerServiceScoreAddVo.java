package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   新增客服评分的vo类
 * @author linuo
 * @time   2023年4月20日17:09:36
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CustomerServiceScoreAddVo", description = "新增客服评分的vo类")
public class CustomerServiceScoreAddVo {
    @ApiModelProperty(value = "供应商协同账号id", dataType = "String")
    private String scAccountId;

    @ApiModelProperty(value = "销售账号id", dataType = "String")
    private String saleAccountId;

    @ApiModelProperty(value = "评分", dataType = "Integer")
    private Integer score;
}
