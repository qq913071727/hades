package com.innovation.ic.im.end.base.vo.im_erp9;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 默认群组，撤回消息的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RetractionVo", description = "默认群组，撤回消息的vo类")
public class GroupRetractionVo {

    @ApiModelProperty(value = "默认群组消息id", dataType = "String")
    private String groupMessageId;

//    @ApiModelProperty(value = "默认群组未读消息id列表", dataType = "List")
//    private List<String> groupOfflineMessageIdList;

    @ApiModelProperty(value = "撤回消息的时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date retractionTime;
}
