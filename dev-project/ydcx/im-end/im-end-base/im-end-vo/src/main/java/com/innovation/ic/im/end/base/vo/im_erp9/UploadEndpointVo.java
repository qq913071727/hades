package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UploadEndpointVo", description = "上传文件vo类")
public class UploadEndpointVo {

    @ApiModelProperty(value = "上传文件的用户的账号", dataType = "String")
    private String fromUserAccount;

    @ApiModelProperty(value = "接收文件的用户的账号", dataType = "String")
    private String toUserAccount;

    @ApiModelProperty(value = "文件名", dataType = "String")
    private String filename;

    public UploadEndpointVo() {
    }

    public UploadEndpointVo(String fromUserAccount, String toUserAccount, String filename) {
        this.fromUserAccount = fromUserAccount;
        this.toUserAccount = toUserAccount;
        this.filename = filename;
    }
}
