package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   模糊查询供应商协同账号信息接口的vo类
 * @author linuo
 * @time   2023年4月27日09:16:13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BlurSearchScAccountVo", description = "模糊查询供应商协同账号信息接口的vo类")
public class BlurSearchScAccountVo {
    @ApiModelProperty(value = "查询参数", dataType = "String")
    private String searchInfo;
}