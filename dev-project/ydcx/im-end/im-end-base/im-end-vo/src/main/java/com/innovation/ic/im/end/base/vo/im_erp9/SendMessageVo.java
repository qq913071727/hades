package com.innovation.ic.im.end.base.vo.im_erp9;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   客户端发来的消息
 * @author linuo
 * @time   2022年6月21日13:48:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SendMessageVo", description = "一对一聊天推送的消息")
public class SendMessageVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    public Integer id;

    @ApiModelProperty(value = "内容", dataType = "String")
    public String content;

    @ApiModelProperty(value = "消息类型。-1表示心跳，1表示文字，2表示文件，3表示图片", dataType = "Integer")
    public Integer type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    public String filePath;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    public String fromUserAccount;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    private String fromUserRealName;

    @ApiModelProperty(value = "接收消息的用户真实姓名", dataType = "String")
    private String toUserRealName;

    @ApiModelProperty(value = "接收消息的用户的账号", dataType = "String")
    public String toUserAccount;

    @ApiModelProperty(value = "每页的行数", dataType = "Integer")
    public Integer pageSize;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    public Integer pageNo;

    @ApiModelProperty(value = "消息撤回时间", dataType = "Date")
    public Date retractionTime;

    @ApiModelProperty(value = "前端生成的消息id", dataType = "String")
    public String vueId;

    @ApiModelProperty(value = "账号类别(1表示erp账号，2表示供应商协同账号)", dataType = "Integer")
    @TableField(value = "integration")
    private Integer integration;
}