package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.nio.ByteBuffer;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MessageVo", description = "客户端发来的消息")
public class MessageVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    public Integer id;

    @ApiModelProperty(value = "内容", dataType = "String")
    public String content;

    @ApiModelProperty(value = "消息类型。-1表示心跳，1表示文字，2表示文件，3表示图片，4表示系统消息", dataType = "Integer")
    public Integer type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    public String filePath;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    public String fromUserAccount;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    private String fromUserRealName;

    @ApiModelProperty(value = "接收消息的用户真实姓名", dataType = "String")
    private String toUserRealName;

    @ApiModelProperty(value = "接收消息的用户的账号", dataType = "String")
    public String toUserAccount;

    @ApiModelProperty(value = "每页的行数", dataType = "Integer")
    public Integer pageSize;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    public Integer pageNo;

    @ApiModelProperty(value = "消息撤回时间", dataType = "Date")
    public Date retractionTime;

    /** 上传图片临时数据 */
    public ByteBuffer byteBuffer;

    @ApiModelProperty(value = "前端生成的消息id", dataType = "String")
    public String vueId;

    public MessageVo() {
    }

    public MessageVo(String content, Integer type, String filePath, String fromUserAccount, String toUserAccount) {
        this.content = content;
        this.type = type;
        this.filePath = filePath;
        this.fromUserAccount = fromUserAccount;
        this.toUserAccount = toUserAccount;
    }

    public MessageVo(String content, Integer type, String filePath, String fromUserAccount, String toUserAccount, Integer pageSize, Integer pageNo) {
        this.content = content;
        this.type = type;
        this.filePath = filePath;
        this.fromUserAccount = fromUserAccount;
        this.toUserAccount = toUserAccount;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }
}
