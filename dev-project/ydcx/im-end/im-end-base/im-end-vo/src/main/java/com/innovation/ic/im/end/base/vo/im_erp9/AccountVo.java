package com.innovation.ic.im.end.base.vo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 账号的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountVo", description = "账号的vo类")
public class AccountVo {
    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;
}