package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 聊天类型
 */
public class ChatType {

    /**
     * user_login_type 为点对点
     */
    public static final Integer POINT_TO_POINT = 1;

    /**
     * user_login_type 为默认群组
     */
    public static final Integer DEFAULT_GROUP = 2;

    /**
     * user_login_type 用户自定义群组
     */
    public static final Integer CUSTOM_USER_GROUP = 3;
}
