package com.innovation.ic.im.end.base.pojo.im_erp9;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

/**
 * @desc   供应商协同账号或采购获取近期的联系人结果pojo类
 * @author linuo
 * @time   2023年5月4日14:06:13
 */
@Data
public class ScAccountFindLastContactResultPojo extends AccountPojo {
    @ApiModelProperty(value = "消息表内容", dataType = "String")
    private String messageContent;

    @ApiModelProperty(value = "消息表内容类型", dataType = "Integer")
    private Integer messageType;

    @ApiModelProperty(value = "消息表内容类时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date messageTime;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "Date")
    private String fromUserRealName;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "Date")
    private String fromUserAccount;

    @ApiModelProperty(value = "聊天类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "内容不可见时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date contentInvisibleTime;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date toppingTime;

    @ApiModelProperty(value = "通过erp9登录的状态。1表示已登录，0表示未登录", dataType = "Integer")
    private Integer erp9Login;

    @ApiModelProperty(value = "通过芯聊客户端登录的状态。1表示已登录，0表示未登录", dataType = "Integer")
    private Integer xlLogin;

    @ApiModelProperty(value = "是否消息免打扰。1表示是，0表示否", dataType = "Integer")
    private Integer noReminder;

    @ApiModelProperty(value = "最后一次联系时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastContactTime;

    @ApiModelProperty(value = "撤回消息内容", dataType = "String")
    private String retractionContent;

    @ApiModelProperty(value = "撤回消息id", dataType = "Integer")
    private Integer retractionMsgId;

    @ApiModelProperty(value = "最后一次评分时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastScoreTime;
}