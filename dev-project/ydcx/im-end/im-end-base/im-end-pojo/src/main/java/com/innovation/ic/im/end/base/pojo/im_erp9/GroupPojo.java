package com.innovation.ic.im.end.base.pojo.im_erp9;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.util.List;

/**
 * 群组和自定义群组的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "GroupPojo", description = "群组和自定义群组的pojo类")
public class GroupPojo implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "群组id", dataType = "String")
    private String groupId;

    @ApiModelProperty(value = "群组和自定义群组的名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "是否消息免打扰。1表示是，null表示否", dataType = "Integer")
    private Integer noReminder;

    @ApiModelProperty(value = "未读消息数量", dataType = "Integer")
    private Integer offlineMessageNumber;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date toppingTime;

    @ApiModelProperty(value = "最后联系时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastMessageTime;

    @ApiModelProperty(value = "最后消息类型", dataType = "Integer")
    private Integer lastMessageType;

    @ApiModelProperty(value = "最后的消息", dataType = "String")
    private String lastMessage;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "String")
    private String fromUserAccount;

    @ApiModelProperty(value = "发送消息的用户真实姓名", dataType = "String")
    private String fromUserRealName;

    @ApiModelProperty(value = "聊天类型(1一对一、2默认群组、3自定义群组)", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "成员列表", dataType = "List")
    private List<Account> accountList;

    @ApiModelProperty(value = "内容不可见时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date contentInvisibleTime;

    @ApiModelProperty(value = "最后一次联系时间", dataType = "Date")
    private Date lastContactTime;

    @ApiModelProperty(value = "撤回消息内容", dataType = "String")
    private String retractionContent;

    @ApiModelProperty(value = "撤回消息id", dataType = "Integer")
    private Integer retractionMsgId;

    @ApiModelProperty(value = "消息id", dataType = "String")
    private String msgId;
}