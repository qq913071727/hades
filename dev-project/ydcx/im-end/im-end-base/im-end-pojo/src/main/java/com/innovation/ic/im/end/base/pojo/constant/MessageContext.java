package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 向上或向下分页显示
 */
public class MessageContext {

    /**
     * 向下
     */
    public static final Integer DOWN = 1;

    /**
     * 向上
     */
    public static final Integer UPWARD = 2;
}
