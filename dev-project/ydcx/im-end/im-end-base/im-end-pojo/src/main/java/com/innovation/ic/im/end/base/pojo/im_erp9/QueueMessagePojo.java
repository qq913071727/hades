//package com.innovation.ic.im.end.base.pojo.im_erp9;
//
//import io.swagger.annotations.ApiModel;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//
//@Data
//@EqualsAndHashCode(callSuper = false)
//@ApiModel(value = "QueueMessage", description = "消息队列的消息")
//public class QueueMessagePojo implements java.io.Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//}
