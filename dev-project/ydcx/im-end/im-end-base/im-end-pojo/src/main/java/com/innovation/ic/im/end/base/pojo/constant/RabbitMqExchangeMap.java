package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   rabbitmq的交换机map
 * @author linuo
 * @time   2022年7月13日14:19:15
 */
public class RabbitMqExchangeMap {
    /** 最近联系人消息交换机名称 */
    public static final String CURRENT_EXCHANGE = "current";

    /** 撤回消息交换机名称 */
    public static final String RETRACTION_EXCHANGE = "retraction";

    /** 阅读状态修改消息交换机名称 */
    public static final String READ_STATE_UPDATE_MSG_EXCHANGE = "read";

    /** 供应商协同账号阅读状态修改消息交换机名称 */
    public static final String SC_READ_STATE_UPDATE_MSG_EXCHANGE = "scRead";

    /** erp更新账号数据交换机名称 */
    public static final String ERP_UPDATE_ACCOUNT_EXCHANGE = "erpUpdateAccount";

    /** 机构树更新交换机名称 */
    public static final String TREE_NODE_UPDATE_EXCHANGE = "treeNodeUpdate";

    /** 用户信息更新交换机名称 */
    public static final String ACCOUNT_UPDATE_EXCHANGE = "accountUpdate";
}