package com.innovation.ic.im.end.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   登录状态枚举
 * @author linuo
 * @time   2023年2月8日14:23:18
 */
public enum LoginStatusEnum {
    login(1,"在线"),
    logout(0,"退出");

    private Integer code;
    private String desc;

    LoginStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static LoginStatusEnum of(Integer code) {
        for (LoginStatusEnum c : LoginStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (LoginStatusEnum c : LoginStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (LoginStatusEnum e : LoginStatusEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}