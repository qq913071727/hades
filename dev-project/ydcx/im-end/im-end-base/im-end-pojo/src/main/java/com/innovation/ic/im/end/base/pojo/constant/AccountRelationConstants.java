package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   账号关系表常量类
 * @author linuo
 * @time   2023年4月19日16:15:29
 */
public class AccountRelationConstants {
    /** 供应商协同账号id字段 */
    public static final String SC_ACCOUNT_ID_FIELD = "sc_account_id";

    /** 销售账号id字段 */
    public static final String SALE_ACCOUNT_ID_FIELD = "sale_account_id";
}