package com.innovation.ic.im.end.base.pojo.constant;

public class DatabaseGlobal {

    /**
     * erp9系统（开发环境）
     */
    public static final String IM_ERP9 = "im-erp9";

    /**
     * erp9系统（测试环境）
     */
    public static final String IM_ERP9_TEST = "im-erp9-test";

    /**
     * erp9系统（生产环境）
     */
    public static final String IM_ERP9_PROD = "im-erp9-prod";

    /**
     * b1b系统
     */
    public static final String IM_B1B = "im-b1b";
//    public static final String B1B = "b1b";

    /**
     * erp9系统的sqlserver数据库
     */
    public static final String ERP9_SQLSERVER = "erp9-sqlserver";
//    public static final String LOCAL = "";
}
