package com.innovation.ic.im.end.base.pojo.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 撤回消息pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RetractionPojo", description = "撤回消息pojo类")
public class RetractionPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "类型，1表示一对一，2表示默认群组，3表示自定义群组", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "一对一消息", dataType = "Message")
    private Message message;

    @ApiModelProperty(value = "默认群组消息", dataType = "GroupMessage")
    private GroupMessage groupMessage;

    @ApiModelProperty(value = "自定义群组消息", dataType = "UserGroupMessage")
    private UserGroupMessage userGroupMessage;
}
