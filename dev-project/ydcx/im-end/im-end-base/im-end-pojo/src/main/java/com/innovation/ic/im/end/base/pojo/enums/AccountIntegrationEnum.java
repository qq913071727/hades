package com.innovation.ic.im.end.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   账号集成类型枚举类
 * @author linuo
 * @time   2023年4月18日16:54:50
 */
public enum AccountIntegrationEnum {
    ERP_CORRELATION(1,"erp相关"),
    SC_CORRELATION(2,"供应商协同相关");

    private Integer code;
    private String desc;

    AccountIntegrationEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AccountIntegrationEnum of(Integer code) {
        for (AccountIntegrationEnum c : AccountIntegrationEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (AccountIntegrationEnum c : AccountIntegrationEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (AccountIntegrationEnum e : AccountIntegrationEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}