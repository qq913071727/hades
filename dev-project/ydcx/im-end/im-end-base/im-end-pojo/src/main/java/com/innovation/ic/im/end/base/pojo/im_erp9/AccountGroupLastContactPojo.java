package com.innovation.ic.im.end.base.pojo.im_erp9;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.util.List;

/**
 * @desc   一对一、群组最近联系人的pojo类
 * @author linuo
 * @time   2022年6月27日15:16:05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountGroupLastContactPojo", description = "一对一、群组最近联系人的pojo类")
public class AccountGroupLastContactPojo implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键。erp9系统的Admins表的id字段", dataType = "String")
    private String id;

    @ApiModelProperty(value = "最后的消息", dataType = "String")
    private String lastMessage;

    @ApiModelProperty(value = "最后消息时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastMessageTime;

    @ApiModelProperty(value = "最后消息类型", dataType = "Integer")
    private Integer lastMessageType;

    @ApiModelProperty(value = "是否消息免打扰。1表示是，null表示否", dataType = "Integer")
    private Integer noReminder;

    @ApiModelProperty(value = "未读消息数量", dataType = "Integer")
    private Integer offlineMessageNumber;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "聊天类型(1一对一、2默认群组、3自定义群组)", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "账号。erp9系统的Admins表的UserName字段", dataType = "String")
    private String username;

    @ApiModelProperty(value = "发送消息的用户的账号", dataType = "Date")
    private String fromUserAccount;

    @ApiModelProperty(value = "群组和自定义群组的名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "默认群组id", dataType = "String")
    private String groupId;

    @ApiModelProperty(value = "自定义群组id", dataType = "String")
    private String userGroupId;

    @ApiModelProperty(value = "成员列表", dataType = "List")
    private List<Account> accountList;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date toppingTime;

    @ApiModelProperty(value = "通过erp9登录的状态。1表示已登录，0表示未登录", dataType = "Integer")
    private Integer erp9Login;

    @ApiModelProperty(value = "通过芯聊客户端登录的状态。1表示已登录，0表示未登录", dataType = "Integer")
    private Integer xlLogin;

    @ApiModelProperty(value = "撤回消息内容", dataType = "String")
    private String retractionContent;

    @ApiModelProperty(value = "撤回消息id", dataType = "Integer")
    private Integer retractionMsgId;

    @ApiModelProperty(value = "消息id", dataType = "String")
    private String msgId;
}