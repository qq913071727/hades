package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 消息类型
 */
public class MessageType {
    /**
     * 全部
     */
    public static final Integer ALL = 0;

    /**
     * 文本
     */
    public static final Integer TEXT = 1;

    /**
     * 文件
     */
    public static final Integer FILE = 2;

    /**
     * 系统消息
     */
    public static final Integer SYS_MSG = 4;

    /**
     * 修改自定义群组名称的系统消息
     */
    public static final Integer UPDATE_USER_GROUP_NAME_SYS_MSG = 5;

    /**
     * 公告栏提醒
     */
    public static final Integer BULLETIN_BOARD_REMINDER = 6;

    /**
     * 二次比价任务提醒
     */
    public static final Integer DOUBLE_PRICE_COMPARISON_TASK_NOTICE = 7;

    /**
     * 销售订单推送失败
     */
    public static final Integer SALES_ORDER_PUSH_FAIL = 8;

    /**
     * 图片
     */
    public static final Integer PICTURE = 3;

    /**
     * 消息类型 为未读
     */
    public static final Integer UNREAD = 1;

    /**
     * 消息类型 为已读
     */
    public static final Integer READ = 2;

    /**
     * 消息 为可用
     */
    public static final Integer MESSAGE_AVAILABLE = 1;
}
