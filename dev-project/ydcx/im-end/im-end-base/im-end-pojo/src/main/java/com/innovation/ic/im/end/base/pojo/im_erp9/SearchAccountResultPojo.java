package com.innovation.ic.im.end.base.pojo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * 搜索账号和账号所在群组的结果的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SearchAccountResultPojo", description = "搜索账号和账号所在群组的结果的pojo类")
public class SearchAccountResultPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账号对象列表", dataType = "List")
    private List<SearchAccountDataPojo> accountList;

    @ApiModelProperty(value = "群组对象列表", dataType = "List")
    private List<SearchGroupDataPojo> groupList;

    @ApiModelProperty(value = "自定义群组对象列表", dataType = "List")
    private List<SearchUserGroupDataPojo> userGroupList;
}