package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 上下文中的参数
 */
public class ContextParameter {

    /**
     * 客户端信息集合
     */
    public static final String CLIENT_SET = "CLIENT_SET";
}
