package com.innovation.ic.im.end.base.pojo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   搜索账号数据结果类
 * @author linuo
 * @time   2022年6月20日11:39:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SearchAccountDataPojo", description = "搜索账号数据结果类")
public class SearchAccountDataPojo {
    /** 主键 */
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    /** 真实姓名 */
    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

    /** 账号 */
    @ApiModelProperty(value = "账号", dataType = "String")
    private String username;

    /** 大赢家code */
    @ApiModelProperty(value = "大赢家code", dataType = "String")
    private String dyjCode;

    /** 汉语拼音 */
    @ApiModelProperty(value = "汉语拼音", dataType = "String")
    private String pinYin;

    /** 汉语拼音首字母 */
    @ApiModelProperty(value = "汉语拼音首字母", dataType = "String")
    private String capitalInitial;

    /** 部门 */
    @ApiModelProperty(value = "部门", dataType = "String")
    private String departmentName;

    /** 电话号码 */
    @ApiModelProperty(value = "电话号码", dataType = "String")
    private String mobile;

    /** 聊天类型 */
    @ApiModelProperty(value = "聊天类型", dataType = "Integer")
    private Integer type;
}