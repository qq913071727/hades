package com.innovation.ic.im.end.base.pojo.im_erp9;

import com.innovation.ic.im.end.base.pojo.constant.ChatType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   搜索默认群组数据结果类
 * @author linuo
 * @time   2022年6月20日11:39:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SearchGroupDataPojo", description = "搜索默认群组数据结果类")
public class SearchGroupDataPojo {

    /** 主键 */
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    /** 群组名称 */
    @ApiModelProperty(value = "群组名称", dataType = "String")
    private String name;

    /** 群组成员数量 */
    @ApiModelProperty(value = "群组成员数量", dataType = "Integer")
    private Integer membership;

    /** 聊天类型 */
    @ApiModelProperty(value = "聊天类型", dataType = "Integer")
    private final Integer type = ChatType.DEFAULT_GROUP;
}