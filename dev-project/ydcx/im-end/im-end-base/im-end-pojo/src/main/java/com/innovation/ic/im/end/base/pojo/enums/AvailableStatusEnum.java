package com.innovation.ic.im.end.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   可用状态
 * @author linuo
 * @time   2022年7月27日09:52:08
 */
public enum AvailableStatusEnum {
    AVAILABLE(1,"可用"),
    UN_AVAILABLE(2,"不可用");

    private Integer code;
    private String desc;

    AvailableStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AvailableStatusEnum of(Integer code) {
        for (AvailableStatusEnum c : AvailableStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(String code) {
        for (AvailableStatusEnum c : AvailableStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (AvailableStatusEnum e : AvailableStatusEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}