package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   日常使用的常量值
 * @author linuo
 * @time   2022年5月13日17:23:35
 */
public class Constants {
    /** id */
    public static final String ID = "id";

    /** 通讯协议变量参数 */
    public static final String SEC_WEBSOCKET_PROTOCOL = "Sec-WebSocket-Protocol";

    /** headerName */
    public static final String HEADER_NAME = "headerName";

    /** logger */
    public static final String LOGGER = "logger";

    /** 自定义群组id */
    public static final String USER_GROUP_ID = "userGroupId";

    /** 自定义群组id字段 */
    public static final String USER_GROUP_ID_FIELD = "user_group_id";

    /** 自定义群组消息表id字段 */
    public static final String USER_GROUP_MESSAGE_ID = "user_group_message_id";

    /** 默认群组id */
    public static final String GROUP_ID = "groupId";

    /** 默认群组id字段 */
    public static final String GROUP_ID_FIELD = "group_id";

    /**默认群组消息表id字段 */
    public static final String GROUP_MESSAGE_ID = "group_message_id";

    /** 账号id */
    public static final String ACCOUNT_ID = "accountId";

    /** 账号id字段 */
    public static final String ACCOUNT_ID_FIELD = "account_id";

    /** 用户名 */
    public static final String USER_NAME = "username";

    /** 发送消息的账号 */
    public static final String FROM_USER_ACCOUNT = "fromUserAccount";

    /** 发送消息的账号字段 */
    public static final String FROM_USER_ACCOUNT_FIELD = "from_user_account";

    /** 最近联系时间的账号字段 */
    public static final String LAST_CONTACT_TIME_FIELD = "last_contact_time";

    /** 接收消息的账号 */
    public static final String TO_USER_ACCOUNT = "toUserAccount";

    /** 接收消息的账号字段 */
    public static final String TO_USER_ACCOUNT_FIELD = "to_user_account";

    /** 是否消息免打扰字段 */
    public static final String NO_REMINDER_FIELD = "no_reminder";

    /** 在线人员map数据 */
    public static final String ONLINE_MAP = "onlineMap";

    /** 自定义群组 */
    public static final String USER_GROUP = "userGroup";

    /** 自定义群组成员 */
    public static final String REF_USER_GROUP_ACCOUNTS = "refUserGroupAccounts";

    /** 项目启动环境 */
    public static final String PROPERTY = "spring.profiles.active";

    /** 状态 */
    public static final String STATUS = "Status";

    /** 状态 */
    public static final String STATUS_ = "status";

    /** 非正常状态 */
    public static final String NOT_NORMAL_STATUS = "500";

    /** 客户端id */
    public static final String CLIENT_ID = "clientId";

    /** 消息是否已读id */
    public static final String READ = "read_";

    /** 一对一消息 */
    public static final String MESSAGE = "message";

    /** 默认群组消息 */
    public static final String GROUP_MESSAGE = "groupMessage";

    /** 群组消息接收方信息 */
    public static final String GROUP_MESSAGE_RECEIVER = "groupMessageReceiver";

    /** 自定义群组消息 */
    public static final String USER_GROUP_MESSAGE = "userGroupMessage";

    /** 群组消息接收方信息 */
    public static final String USER_GROUP_MESSAGE_RECEIVER = "userGroupMessageReceiver";

    /** 生产运行环境 */
    public static final String PROD_RUN_ENVIRONMENT = "prod";

    /** 关闭浏览器时websocket关闭状态码 */
    public static final int CLOSE_BROWSER_WS_CODE = 1001;

    /** 结果 */
    public static final String RESULT = "result";

    /** 是否有新消息字段 */
    public static String IF_HAVE_NEW_MSG_FIELD = "ifHaveNewMsg";
}