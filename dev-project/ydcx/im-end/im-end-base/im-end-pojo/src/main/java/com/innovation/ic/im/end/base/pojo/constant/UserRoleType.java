package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   用户角色类型
 * @author linuo
 * @time   2022年7月14日17:11:11
 */
public class UserRoleType {
    /** 管理员 */
    public static final Integer ADMIN = 1;
}
