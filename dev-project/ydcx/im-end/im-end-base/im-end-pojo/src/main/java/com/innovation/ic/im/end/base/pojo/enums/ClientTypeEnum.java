package com.innovation.ic.im.end.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   客户端信息
 * @author linuo
 * @time   2022年8月10日10:29:03
 */
public enum ClientTypeEnum {
    IM_END(1,"即时通信前端调用即时通信后端接口"),
    ERP9(2,"erp9前端调用即时通信后端接口"),
    CLIENT(3,"即时通信客户端（芯聊）调用即时通信后端接口");

    private Integer code;
    private String desc;

    ClientTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ClientTypeEnum of(Integer code) {
        for (ClientTypeEnum c : ClientTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (ClientTypeEnum c : ClientTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (ClientTypeEnum e : ClientTypeEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}