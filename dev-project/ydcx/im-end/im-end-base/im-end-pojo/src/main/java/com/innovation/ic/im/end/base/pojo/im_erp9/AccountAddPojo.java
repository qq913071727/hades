package com.innovation.ic.im.end.base.pojo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   账号添加的pojo类
 * @author linuo
 * @time   2022年10月12日17:06:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountAddPojo", description = "账号添加的pojo类")
public class AccountAddPojo implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "账号id", dataType = "String")
    private String adminId;

    @ApiModelProperty(value = "默认群组id", dataType = "String")
    private String groupId;

    @ApiModelProperty(value = "默认群组名称", dataType = "String")
    private String groupName;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "大赢家code", dataType = "String")
    private String dyjCode;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String mobile;

    @ApiModelProperty(value = "部门名称", dataType = "String")
    private String departmentName;
}
