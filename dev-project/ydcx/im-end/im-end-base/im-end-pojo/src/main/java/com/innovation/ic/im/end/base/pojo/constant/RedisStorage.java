package com.innovation.ic.im.end.base.pojo.constant;

/**
 * redis存储时使用的常量
 */
public class RedisStorage {

    /**
     * 存入redis 下划线特殊标识
     */
    public static final String UNDERLINE = "_";

    /**
     * message表的前缀
     */
    public static final String MESSAGE_PREFIX = "MESSAGE:";

    /**
     * group_message表的前缀
     */
    public static final String GROUP_MESSAGE_PREFIX = "GROUP_MESSAGE:";

    /**
     * user_group_message表的前缀
     */
    public static final String USER_GROUP_MESSAGE_PREFIX = "USER_GROUP_MESSAGE:";

    /**
     * 用户最近联系人的前缀
     */
    public static final String USER_GROUP_CURRENT_PREFIX = "api:v1:userGroup:current_";

    /**
     * 话术的前缀
     */
    public static final String DIALOGUE_FIND_BY_ACCOUNT_ID_AND_GROUP_PREFIX = "api:v1:dialogue:findByAccountIdAndGroup_";

    /**
     * client表
     */
    public static final String CLIENT = "CLIENT";

    /**
     * Endpoint类中的onlineMap对象
     */
    public static final String ONLINE_MAP = "ONLINE_MAP";

    /**
     * GroupEndpoint类中的onlineGroupMap对象
     */
    public static final String ONLINE_GROUP_MAP = "ONLINE_GROUP_MAP";

    /**
     * UserGroupEndpoint类中的onlineUserGroupMap对象
     */
    public static final String ONLINE_USER_GROUP_MAP = "ONLINE_USER_GROUP_MAP";

    /**
     * message和offline_message表的id在redis中key
     */
    public static final String MESSAGE_ID = "MESSAGE_ID";

    /**
     * message和offline_message表的id在redis中每次自增的数量
     */
    public static final Long MESSAGE_ID_INCREMENT = 1L;

    /**
     * 获取组织结构前缀
     */
    public static final String TREE_NODE_PREFIX = "api:v1:treeNode:get_";
    /**
     * 账户信息 adminsId数据前缀
     */
    public static final String ACCOUNT_BY_ADMINS_ID_PREFIX = "api:v1:account:detailByAdminsId_";
    /**
     * 账户信息 id数据前缀
     */
    public static final String ACCOUNT_BY_ACCOUNT_PREFIX = "api:v1:account:detailByAccount_";
    /**
     * 账户信息 所有
     */
    public static final String ACCOUNT_ALL = "api:v1:account:findAllOrderByRealNameAsc";
    /**
     * 根据群组Id获取默认群组中的人数前缀
     */
    public static final String REF_GROUP_ACCOUNT_BY_GROUP_ID_COUNT_PREFIX ="api:v1:refGroupAccount:count_";
    /**
     * 根据自定义群组Id获取自定义群组的人数
     */
    public static final String REF_USER_GROUP_ACCOUNT_BY_GROUP_ID_COUNT_PREFIX ="api:v1:refUserGroupAccount:count_";
    /**
     * 自定义群组的详细信息前缀
     */
    public static final String REF_USER_GROUP_ACCOUNT_DETAIL_PREFIX = "api:v1:refUserGroupAccount:detail_";
}
