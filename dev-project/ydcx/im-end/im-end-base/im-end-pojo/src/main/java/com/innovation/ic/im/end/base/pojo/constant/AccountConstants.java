package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   Account表常量类
 * @author linuo
 * @time   2022年10月10日16:24:23
 */
public class AccountConstants {
    /** 用户名字段 */
    public static final String USERNAME_FIELD = "username";

    /** 大赢家code */
    public static final String DYJ_CODE_FIELD = "dyj_code";
}