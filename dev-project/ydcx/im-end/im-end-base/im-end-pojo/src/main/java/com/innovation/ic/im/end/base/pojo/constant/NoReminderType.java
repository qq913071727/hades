package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 消息免打扰的类型
 */
public class NoReminderType {

    /**
     * 开启消息免打扰
     */
    public static final Integer YSE = 1;

    /**
     * 关闭消息免打扰
     */
    public static final Integer NO = 0;
}
