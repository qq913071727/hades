package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 置顶类型
 */
public class ToppingType {

    /**
     * 设置为置顶
     */
    public static final Integer YES = 1;

    /**
     * 取消置顶
     */
    public static final Integer NO = 0;
}
