package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 根据用户的真实姓名，查找用户和用户所在的组。模糊查询。判断参数是那种数据类型。
 */
public class ParameterType {
    /**
     * 参数为code
     */
    public static final String dyjCode ="[0-9]*";
    /**
     * 参数为字母
     */
    public static final String pinYin ="[a-zA-Z]+";
}
