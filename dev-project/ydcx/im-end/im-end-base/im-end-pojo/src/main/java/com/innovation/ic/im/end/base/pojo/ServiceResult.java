package com.innovation.ic.im.end.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * 调用服务层的返回对象
 * @param <T>
 */
@Data
@ApiModel(value = "ServiceResult<T>", description = "service返回值对象")
public class ServiceResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    /**
     * 查询成功
     */
    public static final String SELECT_SUCCESS = "查询成功";

    /**
     * 查询失败
     */
    public static final String SELECT_FAIL = "查询失败";

    /**
     * 操作成功
     */
    public static final String OPERATE_SUCCESS = "操作成功";

    /**
     * 操作失败
     */
    public static final String OPERATE_FAIL = "操作失败";

    /**
     * 更新成功
     */
    public static final String UPDATE_SUCCESS = "更新成功";

    /**
     * 更新失败
     */
    public static final String UPDATE_FAIL = "更新失败";

    /**
     * 插入成功
     */
    public static final String INSERT_SUCCESS = "插入成功";

    /**
     * 插入失败
     */
    public static final String INSERT_FAIL = "插入失败";

    /**
     * 删除成功
     */
    public static final String DELETE_SUCCESS = "删除成功";

    /**
     * 删除失败
     */
    public static final String DELETE_FAIL = "删除失败";

    /**
     * 同步成功
     */
    public static final String SYNC_SUCCESS = "同步成功";

    /**
     * 同步失败
     */
    public static final String SYNC_FAIL = "同步失败";

    /**
     * 超时
     */
    public static final String TIME_OUT = "接口调用超时,请5秒后重试";
}
