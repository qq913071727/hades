package com.innovation.ic.im.end.base.pojo.im_b1b;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * b1b统的人员
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "B1bTestPojo", description = "b1b系统的人员")
public class B1bTestPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "拼音", dataType = "String")
    private String pinYin;

    @ApiModelProperty(value = "首字母大写", dataType = "String")
    private String capitalInitial;
}
