package com.innovation.ic.im.end.base.pojo.constant;

/**
 * erp9系统中Structures表的Status字段的取值
 */
public class Erp9StructuresStatus {

    /**
     * 可用
     */
    public static final Integer AVAILABLE = 200;

    /**
     * 不可用
     */
    public static final Integer NOT_AVAILABLE = 400;
}
