package com.innovation.ic.im.end.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   聊天页面打开状态
 * @author linuo
 * @time   2022年8月16日11:10:33
 */
public enum ChatPageStatusEnum {
    CLOSE(0,"聊天窗口关闭"),
    OPEN(1,"聊天窗口打开");

    private Integer code;
    private String desc;

    ChatPageStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ChatPageStatusEnum of(Integer code) {
        for (ChatPageStatusEnum c : ChatPageStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(String code) {
        for (ChatPageStatusEnum c : ChatPageStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (ChatPageStatusEnum e : ChatPageStatusEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}