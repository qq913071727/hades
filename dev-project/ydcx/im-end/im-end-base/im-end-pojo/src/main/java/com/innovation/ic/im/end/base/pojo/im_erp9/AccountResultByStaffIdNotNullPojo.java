package com.innovation.ic.im.end.base.pojo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   查询erp系统员工id不为空的所有数据
 * @author linuo
 * @time   2022年5月18日13:18:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountResultByStaffIdNotNullPojo", description = "查询erp系统员工id不为空的所有数据")
public class AccountResultByStaffIdNotNullPojo {
    /** 账号 */
    @ApiModelProperty(value = "账号", dataType = "String")
    private String accountId;

    /** 用户名 */
    @ApiModelProperty(value = "用户名", dataType = "String")
    private String username;

    /** 机构id */
    @ApiModelProperty(value = "机构id", dataType = "String")
    private String groupId;
}