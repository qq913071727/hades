package com.innovation.ic.im.end.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @desc   erp账号状态枚举类
 * @author linuo
 * @time   2022年10月12日16:24:30
 */
public enum ErpAccountStatusEnum {
    ENABLE(200,"启用"),
    CLOSE(500,"禁用");

    private Integer code;
    private String desc;

    ErpAccountStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ErpAccountStatusEnum of(Integer code) {
        for (ErpAccountStatusEnum c : ErpAccountStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (ErpAccountStatusEnum c : ErpAccountStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static List<Map> enumToList = new ArrayList<Map>();

    static {
        for (ErpAccountStatusEnum e : ErpAccountStatusEnum.values()) {
            Map param = new HashMap();
            param.put("code", e.code);
            param.put("desc", e.desc);
            enumToList.add(param);
        }
    }

    public static List<Map> getEnumToList() {
        return enumToList;
    }
}