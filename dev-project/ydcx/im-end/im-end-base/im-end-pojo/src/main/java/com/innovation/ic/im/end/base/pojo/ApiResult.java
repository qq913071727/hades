package com.innovation.ic.im.end.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 调用API的返回对象
 * @param <T>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiResult<T>", description = "接口返回值对象")
public class ApiResult<T> implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Integer")
    private Integer code;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result = null;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    /**
     * 3001表示参数为空
     */
    @ApiModelProperty(value = "3001表示参数为空", dataType = "Integer")
    public static final Integer PARAMETER_EMPTY = 3001;

    /**
     * 3002表示token不合法或已经过期，需要重新登录
     */
    @ApiModelProperty(value = "3002表示token不合法或已经过期，需要重新登录", dataType = "Integer")
    public static final Integer TOKEN_ILLEGAL_EXPIRE = 3002;
}
