package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 消息的发送发和接收方
 */
public class FromTo {

    /**
     * 发送给所有用户
     */
    public static final String TO_ALL_USER = "TO_ALL_USER";

}
