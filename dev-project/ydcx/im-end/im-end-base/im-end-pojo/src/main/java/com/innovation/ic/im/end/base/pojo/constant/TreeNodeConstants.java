package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   treeNode表常量类
 * @author linuo
 * @time   2022年10月11日16:27:17
 */
public class TreeNodeConstants {
    /** 组织机构群组id */
    public static final String ORGANIZATION_GROUP_ID = "Struc00000";

    /** 子节点字段 */
    public static final String CHILDREN_FIELD = "children";

    /** 中括号 */
    public static final String BRACKETS = "[]";
}