package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   固定消息内容
 * @author linuo
 * @time   2023年2月8日11:25:16
 */
public class FixMessageContent {
    public static final String BULLETIN_BOARD_REMINDER_MSG = "公告栏提醒";

    public static final String DOUBLE_PRICE_COMPARISON_TASK_NOTICE_MSG = "二次比价任务提醒";

    public static final String SALES_ORDER_PUSH_FAIL_MSG = "销售订单推送失败";
}