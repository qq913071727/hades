package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 数据库操作类型
 */
public class DatabaseOperationType {

    /**
     * 增
     */
    public static final String INSERT = "INSERT";

    /**
     * 删
     */
    public static final String DELETE = "DELETE";

    /**
     * 改
     */
    public static final String UPDATE = "UPDATE";

}
