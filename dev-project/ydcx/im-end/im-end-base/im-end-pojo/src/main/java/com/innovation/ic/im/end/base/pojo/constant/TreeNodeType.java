package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 和erp9系统集成时，树形目录节点类型
 */
public class TreeNodeType {
    /**
     * 人员
     */
    public static final Integer STAFF = 1;

    /**
     * 部门
     */
    public static final Integer ORGANIZATION = 2;

    /**
     * 管理员节点id
     */
    public static final String ADMIN_NODE_ID = "Struc11111";

    /**
     * 管理员节点名称
     */
    public static final String ADMIN_NODE_NAME = "管理员";

    /**
     * 组织机构节点名称
     */
    public static final String ORGANIZATION_NODE_NAME = "组织机构";
}