package com.innovation.ic.im.end.base.pojo.constant;

/**
 * im-erp9数据库中表的名称
 */
public class ImErp9TableName {

    public static final String MESSAGE = "message";

    public static final String GROUP_MESSAGE = "group_message";

    public static final String USER_GROUP_MESSAGE = "user_group_message";

    public static final String CHAT_PAIR = "chat_pair";

    public static final String ACCOUNT = "account";

    public static final String REF_GROUP_ACCOUNT_OPERATION = "ref_group_account_operation";

    public static final String GROUP_MESSAGE_RECEIVER = "group_message_receiver";

    public static final String REF_GROUP_ACCOUNT = "ref_group_account";

    public static final String REF_USER_GROUP_ACCOUNT = "ref_user_group_account";

    public static final String USER_GROUP_MESSAGE_RECEIVER = "user_group_message_receiver";

    public static final String TREE_NODE = "tree_node";

    public static final String  DIALOGUE = "dialogue";
}
