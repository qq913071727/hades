package com.innovation.ic.im.end.base.pojo.constant;

/**
 * 群组类型
 */
public class GroupType {

    /**
     * 默认生成的部门群组
     */
    public static final Integer DEFAULT = 1;

    /**
     * 用户自定义的群组
     */
    public static final Integer USER_DEFINE = 2;
}
