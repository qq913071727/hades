package com.innovation.ic.im.end.base.pojo.im_erp9;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 账号的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AccountPojo", description = "账号的pojo类")
public class AccountPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;

    @ApiModelProperty(value = "拼音", dataType = "String")
    private String pinYin;

    @ApiModelProperty(value = "首字母大写", dataType = "String")
    private String capitalInitial;

    @ApiModelProperty(value = "主键。erp9系统的Admins表的id字段", dataType = "String")
    private String id;

    @ApiModelProperty(value = "账号。erp9系统的Admins表的UserName字段", dataType = "String")
    private String username;

    @ApiModelProperty(value = "未读消息数量", dataType = "Integer")
    private Integer offlineMessageNumber;

    @ApiModelProperty(value = "置顶时间", dataType = "Date")
    private Date toppingTime;

    @ApiModelProperty(value = "最后消息时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastMessageTime;

    @ApiModelProperty(value = "最后消息类型", dataType = "Integer")
    private Integer lastMessageType;

    @ApiModelProperty(value = "最后的消息", dataType = "String")
    private String lastMessage;

    @ApiModelProperty(value = "聊天类型(1一对一、2默认群组、3自定义群组)", dataType = "Integer")
    private Integer type;
}
