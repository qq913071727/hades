package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   登录入口类型
 * @author linuo
 * @time   2022年7月15日11:37:17
 */
public class OperateEntranceType {
    /** ERP9登录 */
    public static final Integer ERP9 = 1;

    /** 芯聊客户端登录 */
    public static final Integer XL_CLIENT = 2;
}
