package com.innovation.ic.im.end.base.pojo.constant;

/**
 * @desc   rabbitMq相关常量参数
 * @author linuo
 * @time   2022年10月8日17:31:00
 */
public class RabbitMqConstants {
    /** erp新增账号信息的队列 */
    public static final String ERP_ADD_ACCOUNT_QUEUE = "erm.to.im.setstrucstaff.add";

    /** erp删除账号信息的队列 */
    public static final String ERP_DELETE_ACCOUNT_QUEUE = "erm.to.im.setstrucstaff.del";

    /** erp修改账号信息的队列 */
    public static final String ERP_EDIT_ACCOUNT_QUEUE = "erm.to.im.setstrucstaff.edit";

    /** erp修改员工信息的队列 */
    public static final String ERP_EDIT_ACCOUNT_INFO_QUEUE = "erm.to.im.staff.edit";

    /** erp修改员工信息的队列 */
    public static final String ERP_EDIT_ADMIN_STATUS_QUEUE = "erm.to.im.admin.status";

    /** erp部门新增、修改队列 */
    public static final String ERP_GROUP_ADD_EDIT_QUEUE = "erm.to.im.setstructure";

    /** erp部门删除队列 */
    public static final String ERP_GROUP_DELETE_QUEUE = "erm.to.im.delstructure";

    /** erp新增、修改供应商协同账号队列 */
    public static final String ERP_SC_ACCOUNT_ADD_UPDATE_QUEUE = "erp.to.im.account.add.update";

    /** erp删除供应商协同账号队列 */
    public static final String ERP_SC_ACCOUNT_DELETE_QUEUE = "erp.to.im.account.delete";

    /** 队列类型 */
    public static final String DIRECT_TYPE = "direct";

    /** 账号id字段 */
    public static final String ADMIN_ID_FIELD = "AdminID";

    /** AdminId 数组字段 */
    public static final String ADMIN_IDS_FIELD = "Ids";

    /** 组织架构id字段 */
    public static final String STRUCTURE_ID_FIELD = "StructureID";

    /** 上次组织架构ID字段 */
    public static final String OLD_STRUCTURE_ID_FIELD = "OldStructureID";

    /** 真实姓名字段 */
    public static final String REAL_NAME_FIELD = "RealName";

    /** 大赢家code字段 */
    public static final String DYJ_CODE_FIELD = "DyjCode";

    /** 用户名字段 */
    public static final String USER_NAME_FIELD = "UserName";

    /** 手机号字段 */
    public static final String MOBILE_FIELD = "Mobile";

    /** 手机号字段 */
    public static final String PHONE_FIELD = "Phone";

    /** 状态字段(200 启用；500 禁用) */
    public static final String STATUS_FIELD = "Status";

    /** 消息名称字段 */
    public static final String MESSAGE_NAME_FIELD = "MessageName";

    /** 消息体字段 */
    public static final String MESSAGE_BODY_FIELD = "MessageBody";

    /** id字段 */
    public static final String ID_FIELD = "ID";

    /** 父级id字段 */
    public static final String FATHER_ID_FIELD = "FatherID";

    /** 名称字段 */
    public static final String NAME_FIELD = "Name";

    /** id */
    public static final String ID = "id";

    /** 真实姓名 */
    public static final String REAL_NAME = "realName";

    /** 用户名 */
    public static final String USER_NAME = "userName";

    /** 手机号 */
    public static final String MOBILE = "mobile";

    /** 供应商名称 */
    public static final String SUPPLIER_NAME = "supplierName";

    /** 供应商账号对应的采购账号id */
    public static final String PURCHASE_ACCOUNT_ID = "purchaseAccountId";
}