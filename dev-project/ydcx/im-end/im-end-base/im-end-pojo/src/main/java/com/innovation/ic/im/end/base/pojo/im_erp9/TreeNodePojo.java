package com.innovation.ic.im.end.base.pojo.im_erp9;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "TreeNode", description = "组织结构树形目录的节点")
public class TreeNodePojo {

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String label;

    @ApiModelProperty(value = "类型。1表示部门，2表示人员", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "子节点（json字符串）", dataType = "List")
    private List<TreeNodePojo> children;

    @ApiModelProperty(value = "发送消息权限", dataType = "Boolean")
    private Boolean sendMsgPermission = Boolean.FALSE;

}