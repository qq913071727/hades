package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.AccountMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.GroupMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.RefGroupAccountPrivilegeMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccountPrivilege;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.im_erp9.RefGroupAccountPrivilegeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * RefGroupAccountPrivilege的具体实现类
 */
@Service
@Transactional
public class RefGroupAccountPrivilegeServiceImpl extends ServiceImpl<RefGroupAccountPrivilegeMapper, RefGroupAccountPrivilege> implements RefGroupAccountPrivilegeService {
    @Resource
    private RefGroupAccountPrivilegeMapper refGroupAccountPrivilegeMapper;

    @Resource
    private AccountMapper accountMapper;

    @Resource
    private GroupMapper groupMapper;

    /**
     * 根据accountId和username查找RefGroupAccountPrivilege列表
     *
     * @param accountId
     * @param username
     * @return
     */
    @Override
    public ServiceResult<List<RefGroupAccountPrivilege>> findByAccountIdAndUsername(String accountId, String username) {
        ServiceResult<List<RefGroupAccountPrivilege>> serviceResult = new ServiceResult<List<RefGroupAccountPrivilege>>();

        QueryWrapper<RefGroupAccountPrivilege> wrapper = new QueryWrapper();
        wrapper.eq("account_id", accountId);
        wrapper.eq("username", username);
        List<RefGroupAccountPrivilege> refGroupAccountPrivilegeList = baseMapper.selectList(wrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(refGroupAccountPrivilegeList);

        return serviceResult;
    }

    /**
     * 插入账号对默认群组的权限表数据
     *
     * @param list 需要插入到数据库的结果
     * @return 返回插入数据条数
     */
    @Override
    public ServiceResult<Integer> insertRefGroupAccountPrivilegeList(List<RefGroupAccountPrivilege> list) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        Integer integer = refGroupAccountPrivilegeMapper.insertBatchSomeColumn(list);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(integer);
        return serviceResult;
    }

    /**
     * 删除ref_group_account_privilege表中所有数据
     *
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> truncateTable() {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        refGroupAccountPrivilegeMapper.truncateTable();

        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 赋予某用户高级权限
     *
     * @return
     */
    @Override
    public void updateGroupId(String accountId, String username, Integer role) {
        UpdateWrapper<Account> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",accountId);
        updateWrapper.eq("username",username);
        Account account = new Account();
        account.setRole(role);
        accountMapper.update(account,updateWrapper);
    }

    /**
     * 获取 群组全部数据
     * @return
     */
    @Override
    public List<RefGroupAccountPrivilege> refGroupAccountPrivilegeListAll() {
        QueryWrapper<RefGroupAccountPrivilege> wrapper = new QueryWrapper();
        return baseMapper.selectList(wrapper);
    }
}