package com.innovation.ic.im.end.base.service.erp9_sqlserver;

import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Structures;
import com.innovation.ic.im.end.base.pojo.ServiceResult;

import java.util.List;

/**
 * Erp9StructuresService的服务类接口
 */
public interface Erp9StructuresService {

    /**
     * 批量插入Erp9Structures对象
     * @param erp9StructuresList
     * @return
     */
    ServiceResult<Boolean> saveErp9StructuresList(List<Erp9Structures> erp9StructuresList);

    /**
     * 从erp9项目的数据库中查找所有的Structures表的数据
     * @return
     */
    ServiceResult<List<Erp9Structures>> findAll();

    /**
     * 根据status字段，从erp9项目的数据库中查找Structures表的数据
     * @param status
     * @return
     */
    ServiceResult<List<Erp9Structures>> findByStatus(Integer status);

    /**
     * 根据fatherId，查找Erp9Structures对象列表
     * @param fatherId
     * @return
     */
    ServiceResult<List<Erp9Structures>> findByFatherId(String fatherId, Integer status);

    /**
     * 根据id查询erp中的组织机构信息
     * @param structureId 组织机构id
     * @return 返回组织机构信息
     */
    ServiceResult<Erp9Structures> selectErp9StructuresById(String structureId);
}
