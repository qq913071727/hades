package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.UserLoginLogMapper;
import com.innovation.ic.im.end.base.model.im_erp9.UserLoginLog;
import com.innovation.ic.im.end.base.service.im_erp9.UserLoginLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
@Transactional
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements UserLoginLogService {

    @Resource
    UserLoginLogMapper userLoginLogMapper;


    /**
     * 保存点对点用户登录数据
     *
     * @param username
     * @return
     */
    @Override
    public void saveUserData(String username, Integer type, String ipAddr) {
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        userLoginLogMapper.saveUserData(username, type, date, ipAddr);
    }

    /**
     * 保存默认用户登录数据
     *
     * @param username
     * @param groupId
     * @return
     */
    @Override
    public void saveDefaultUserData(String username, String groupId, Integer type, String ipAddr) {
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        userLoginLogMapper.saveDefaultUserData(username, type, date, groupId, ipAddr);
    }

    /**
     * 保存自定义用户登录数据
     *
     * @param username
     * @param userGroupId
     * @return
     */
    @Override
    public void saveCustomUserData(String username, String userGroupId,Integer type, String ipAddr) {
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        userLoginLogMapper.saveCustomUserData(username, type, date, userGroupId, ipAddr);
    }
}
