package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.Client;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import java.util.List;

/**
 * Client的服务类接口
 */
public interface ClientService {
    /**
     * 查找client表中所有数据
     * @return
     */
    ServiceResult<List<Client>> findAll();

    /**
     * 根据id获取客户端类型
     * @param id 主键
     * @return 返回客户端类型
     */
    ServiceResult<Integer> getTypeById(String id);

    /**
     * 获取登录类型
     * @return 返回登录类型
     */
    ServiceResult<List<Integer>> getLoginTypeList();
}