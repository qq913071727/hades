package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.AccountRelation;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import java.util.List;

/**
 * @desc   账号关系表Service
 * @author linuo
 * @time   2023年4月18日17:17:20
 */
public interface AccountRelationService {
    /**
     * 保存账号关系表数据
     * @param accountRelation 账号关系表数据
     * @return 返回保存结果
     */
    ServiceResult<Integer> saveAccountRelation(AccountRelation accountRelation);

    /**
     * 查询账号关系表数据
     * @param scAccountId 供应商协同账号id
     * @param saleAccountId 销售账号id
     * @return 返回查询结果
     */
    ServiceResult<AccountRelation> selectDataByScSaleAccountId(String scAccountId, String saleAccountId);

    /**
     * 根据ScAccountId删除账号关系表数据
     * @param scAccountId 供应商协同账号id
     * @return 返回删除结果
     */
    ServiceResult<Integer> deleteByScAccountId(String scAccountId);

    /**
     * 查询账号关系表数据
     * @return 返回查询结果
     */
    ServiceResult<List<AccountRelation>> getAccountRelationListData();
}