package com.innovation.ic.im.end.base.service.erp9_sqlserver;

import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9MapsStructure;
import com.innovation.ic.im.end.base.pojo.ServiceResult;

import java.util.List;

/**
 * Erp9MapsStructureService的服务类接口
 */
public interface Erp9MapsStructureService {

    /**
     * 根据structureId查找Erp9MapsStructure对象列表
     * @param structureId
     * @return
     */
    ServiceResult<List<Erp9MapsStructure>> findByStructureID(String structureId);

}
