package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.RefUserGroupAccountMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.UserGroupMessageMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.UserGroupMessageReceiverMapper;
import com.innovation.ic.im.end.base.model.im_erp9.*;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.constant.MessageType;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.service.im_erp9.UserGroupMessageReceiverService;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * UserGroupOfflineMessage的具体实现类
 */
@Service
@Transactional
public class UserGroupMessageReceiverServiceImpl extends ServiceImpl<UserGroupMessageReceiverMapper, UserGroupMessageReceiver> implements UserGroupMessageReceiverService {
    private static final Logger log = LoggerFactory.getLogger(UserGroupMessageReceiverServiceImpl.class);

    @Resource
    private UserGroupMessageReceiverMapper userGroupMessageReceiverMapper;
    @Resource
    private UserGroupMessageMapper userGroupMessageMapper;
    @Resource
    private RefUserGroupAccountMapper refUserGroupAccountMapper;

    /**
     * 保存GroupMessage对象
     * @param userGroupMessageReceiver
     */
    @Override
    public ServiceResult<Boolean> saveUserGroupMessageReceiver(UserGroupMessageReceiver userGroupMessageReceiver) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        userGroupMessageReceiver.setCreateTime(new Date(System.currentTimeMillis()));
        userGroupMessageReceiverMapper.insert(userGroupMessageReceiver);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据userGroupId和toUserAccount查找UserGroupOfflineMessage对象列表，并且按照创建时间升序排列
     * @param userGroupId
     * @param toUserAccount
     * @return
     */
    @Override
    public ServiceResult<List<UserGroupMessageReceiver>> findByUserGroupIdAndToUserAccountOrderByCreateTimeAsc(Integer userGroupId, String toUserAccount) {
        ServiceResult<List<UserGroupMessageReceiver>> serviceResult = new ServiceResult<>();

        QueryWrapper<UserGroupMessageReceiver> wrapper = new QueryWrapper();
        wrapper.eq("user_group_id", userGroupId);
        wrapper.eq("to_user_account", toUserAccount);
        wrapper.orderByAsc("create_time");
        List<UserGroupMessageReceiver> userGroupOfflineMessageList = userGroupMessageReceiverMapper.selectList(wrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userGroupOfflineMessageList);

        return serviceResult;
    }

    /**
     * 删除UserGroupOfflineMessage对象
     * @param id
     * @return
     */
    @Override
    public ServiceResult<Boolean> deleteUserGroupOfflineMessage(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();

        userGroupMessageReceiverMapper.deleteById(id);

        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 保存UserGroupOfflineMessage类型对象
     * @param userGroupOfflineMessage
     * @return
     */
    @Override
    public ServiceResult<Boolean> saveUserGroupOfflineMessage(UserGroupMessageReceiver userGroupOfflineMessage) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        baseMapper.insert(userGroupOfflineMessage);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查询所有记录，按照创建时间降序排列
     *
     * @return
     */
    @Override
    public ServiceResult<List<UserGroupMessageReceiver>> findAllOrderByCreateTimeDesc() {
        ServiceResult<List<UserGroupMessageReceiver>> serviceResult = new ServiceResult<List<UserGroupMessageReceiver>>();
        QueryWrapper<UserGroupMessageReceiver> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");

        List<UserGroupMessageReceiver> userGroupOfflineMessageList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userGroupOfflineMessageList);
        return serviceResult;
    }

    /**
     * 通过userGroupId和toUserAccount 去修改消息状态
     */
    @Override
    public ServiceResult<Integer> restoreUserGroupMessageReceiver(Integer userGroupId, String toUserAccount) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        // 通过userGroupId和toUserAccount 去修改消息状态
        UpdateWrapper<UserGroupMessageReceiver> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.USER_GROUP_ID_FIELD, userGroupId);
        updateWrapper.eq(Constants.TO_USER_ACCOUNT_FIELD, toUserAccount);
        updateWrapper.eq(Constants.READ,MessageType.UNREAD);
        UserGroupMessageReceiver userGroupMessageReceiver = new UserGroupMessageReceiver();
        userGroupMessageReceiver.setReadTime(new Date());
        userGroupMessageReceiver.setRead(MessageType.READ);
        int update = userGroupMessageReceiverMapper.update(userGroupMessageReceiver, updateWrapper);
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(update);
        return serviceResult;
    }

    /**
     * 自定义群组，撤回消息
     *
     */
    @Override
    public RetractionPojo retractionUserGroupMessageReceiver(GroupRetractionVo groupRetractionVo){
        RetractionPojo retractionPojo = new RetractionPojo();
        //获取群组id
        String groupMessageId = groupRetractionVo.getGroupMessageId();
        //获取撤回时间
        Date retractionTime = groupRetractionVo.getRetractionTime();
        //修改user_group_message
        UpdateWrapper<UserGroupMessage> groupMessageUpdateWrapper = new UpdateWrapper<>();
        groupMessageUpdateWrapper.eq(Constants.ID,groupMessageId);
        UserGroupMessage userGroupMessage = new UserGroupMessage();
        userGroupMessage.setRetractionTime(retractionTime);
        userGroupMessageMapper.update(userGroupMessage,groupMessageUpdateWrapper);
        //获取当前修改的值 返回
        UserGroupMessage userGroupMessageData = userGroupMessageMapper.selectOne(groupMessageUpdateWrapper);
        retractionPojo.setType(3);
        retractionPojo.setUserGroupMessage(userGroupMessageData);
        return retractionPojo;
    }

    /**
     * 通过GroupId 获取对应的人
     * @param userGroupId 群组id
     * @param fromUserAccount 发送消息的用户名
     * @return 返回删除结果
     */
    @Override
    public List<String> findUsername(Integer userGroupId, String fromUserAccount){
        List<RefUserGroupAccount> refUserGroupAccountList = refUserGroupAccountMapper.findUsername(userGroupId, fromUserAccount);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < refUserGroupAccountList.size(); i++) {
            RefUserGroupAccount refUserGroupAccount = refUserGroupAccountList.get(i);
            list.add(i, refUserGroupAccount.getUsername());
        }
        return list;
    }

}