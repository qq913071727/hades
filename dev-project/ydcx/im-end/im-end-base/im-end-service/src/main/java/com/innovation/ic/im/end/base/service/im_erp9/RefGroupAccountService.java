package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccount;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import java.util.List;
import java.util.Map;

public interface RefGroupAccountService {

    /**
     * 删除ref_group_account表中的所有数据
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 插入RefGroupAccount对象
     *
     * @param refGroupAccount
     * @return
     */
    ServiceResult<Boolean> saveRefGroupAccount(RefGroupAccount refGroupAccount);

    /**
     * 根据groupId，查找username不在usernameList范围内的用户
     * @param groupId
     * @param usernameList
     * @return
     */
    ServiceResult<List<RefGroupAccount>> findByGroupIdAndNotInList(String groupId, List<String> usernameList);

    /**
     * 根据groupId，修改对应记录的last_contact_time字段
     *
     * @param groupId
     * @return
     */
    ServiceResult<Boolean> updateLastContactTimeByGroupId(String groupId);

    /**
     * 根据群组id，账号id,账号,状态,去设置置顶时间
     *
     * @param groupId
     * @param accountId
     * @param username
     * @param status
     * @return
     */
    ServiceResult<Boolean> updateRefGroupAccount(String groupId, String accountId, String username, Integer status);


    /**
     * 根据groupId,accountId,username去修改status状态
     *
     * @param groupId
     * @param accountId
     * @param username
     * @param status
     * @return
     */
    ServiceResult<Boolean> updateRefGroupAccountStatus(String groupId, String accountId, String username, Integer status);

    /**
     * 根据id去查找对应的数据
     *
     * @param id
     * @return
     */
    ServiceResult<List<RefGroupAccount>> findByRealId(String id);

    /**
     * 根据条件查询自定义群组信息
     * @param map 查询条件
     * @return 返回自定义群组信息
     */
    ServiceResult<GroupPojo> getGroupDetailInfoByParam(Map<String, Object> map);

    /**
     * 根据用户设置默认群组聊天内容不可见时间
     * @param map 更新条件
     * @return 返回更新结果
     */
    ServiceResult<Boolean> invisibleChatRecordSet(Map<String, Object> map);

    /**
     * 根据默认群组id获取群组人数
     * @param groupId 默认群组id
     * @return 返回群组人数
     */
    ServiceResult<Integer> getCountByGroupId(String groupId);

    /**
     * 查询当前默认群组中除自己之外的用户账号列表
     * @param groupId 默认群组id
     * @param userAccount 发送消息的用户账号
     * @return 返回除自己之外的用户账号列表
     */
    ServiceResult<List<String>> getGroupAccountsNotHaveSelf(String groupId, String userAccount);

    /**
     * 查询当前默认群组中的用户账号
     * @param groupId 默认群组id
     * @return 返回默认群组中的用户账号列表
     */
    ServiceResult<List<String>> getGroupAccounts(String groupId);

    /**
     * 更新用户username组groupId的最近联系时间
     * @param groupId 默认群组id
     * @param accountId 用户账号
     * @param username 用户名
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateLastContactTime(String groupId, String accountId, String username);

    /**
     * 删除最近联系群组并清除聊天记录
     * @param map 条件
     * @return 返回删除结果
     */
    ServiceResult<Boolean> deleteGroupChat(Map<String, Object> map);

    /**
     * 根据Group表,将ref_group_account表的数据统计数量导入redis
     * @return
     */
    ServiceResult<Boolean> importRefGroupAccountIntoRedis();
}