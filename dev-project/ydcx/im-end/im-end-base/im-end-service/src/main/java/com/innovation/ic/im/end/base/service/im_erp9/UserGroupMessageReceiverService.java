package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessageReceiver;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import java.util.List;

/**
 * UserGroupOfflineMessage的服务类接口
 */
public interface UserGroupMessageReceiverService {
    /**
     * 保存GroupMessage对象
     * @param userGroupMessageReceiver
     */
    ServiceResult<Boolean> saveUserGroupMessageReceiver(UserGroupMessageReceiver userGroupMessageReceiver);

    /**
     * 根据userGroupId和toUserAccount查找UserGroupOfflineMessage对象列表，并且按照创建时间升序排列
     * @param userGroupId
     * @param toUserAccount
     * @return
     */
    ServiceResult<List<UserGroupMessageReceiver>> findByUserGroupIdAndToUserAccountOrderByCreateTimeAsc(Integer userGroupId, String toUserAccount);

    /**
     * 删除UserGroupOfflineMessage对象
     * @param id
     * @return
     */
    ServiceResult<Boolean> deleteUserGroupOfflineMessage(Integer id);

    /**
     * 保存UserGroupOfflineMessage类型对象
     * @param userGroupOfflineMessage
     * @return
     */
    ServiceResult<Boolean> saveUserGroupOfflineMessage(UserGroupMessageReceiver userGroupOfflineMessage);

    /**
     * 查询所有记录，按照创建时间降序排列
     * @return
     */
    ServiceResult<List<UserGroupMessageReceiver>> findAllOrderByCreateTimeDesc();


    /**
     * 自定义群组，撤回消息
     *
     */
    RetractionPojo retractionUserGroupMessageReceiver(GroupRetractionVo groupRetractionVo);

    /**
     * 通过GroupId 获取对应的人
     * @param userGroupId 群组id
     * @param fromUserAccount 发送消息的用户名
     * @return 返回删除结果
     */
    List<String> findUsername(Integer userGroupId, String fromUserAccount);

    /**
     * 通过userGroupId和toUserAccount 去修改消息状态
     */
    ServiceResult<Integer> restoreUserGroupMessageReceiver(Integer userGroupId, String toUserAccount);
}