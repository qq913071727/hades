package com.innovation.ic.im.end.base.service.im_erp9;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.model.im_erp9.Group;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupService {

    /**
     * 删除group表中所有的数据
     *
     * @return
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 批量插入Group对象列表
     *
     * @param groupList
     * @return
     */
    ServiceResult<Boolean> saveGroupList(List<Group> groupList);

    /**
     * 插入Group对象，返回id
     *
     * @param group
     * @return
     */
    ServiceResult<String> saveGroupReturnId(Group group);

    /**
     * 按照group_id对ref_group_account表分组统计，并将结果更新到group_表的membership字段
     *
     * @return
     */
    ServiceResult<Boolean> updateMembershipSelectRefGroupAccount();

    /**
     * 根据id去查询组信息
     *
     * @param groupId
     * @return
     */
    ServiceResult<Group> findByRealId(String groupId);

    /**
     * 获取近期联系的默认群组和用户自定义群组。按置顶顺序降序排列，按最后聊天时间降序排列。
     * 显示未读消息数量。显示是否消息免打扰
     *
     * @param account
     * @return
     */
    ServiceResult<List<GroupPojo>> findLastContact(@Param("account") String account);

    /**
     * 根据群组id获取在线人员数量
     *
     * @param groupId 群组id
     * @return 在线人员数量
     */
    ServiceResult<Long> getUserNumberOnlineFromRedis(String groupId);

    /**
     * 处理默认群组数据
     *
     * @param groupId   默认群组id
     * @param groupName 群组名称
     */
    ServiceResult<Boolean> handleGroupData(String groupId, String groupName);

    /**
     * 处理erp新增、修改默认群组的mq队列消息
     *
     * @param json 数据
     * @return 返回处理结果
     */
    ServiceResult<List<String>> handleErpAddEditGroupInfoQueueData(JSONObject json);

    /**
     * 处理erp删除默认群组的mq队列消息
     *
     * @param groupId 删除的默认群组id
     * @return 返回处理结果
     */
    ServiceResult<List<String>> handleErpDeleteGroupInfoQueueData(String groupId);

    /**
     * 查询组织结构数据
     *
     * @return
     */
    List<Group> groupListAll();
}