package com.innovation.ic.im.end.base.service.im_erp9;

import org.apache.curator.framework.CuratorFramework;

/**
 * @desc   账号和群组的操作表service类
 * @author linuo
 * @time   2022年6月8日16:10:12
 */
public interface RefGroupAccountOperationService {

    /**
     * 处理账号和群组的操作表数据
     * @param groupId 用户组id
     * @param userName 用户名
     */
    void handleRefGroupAccountOperationData(String groupId, String userName, CuratorFramework curatorFramework, Long waitingLockTime) throws Exception;
}