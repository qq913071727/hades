package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.UserGroup;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountGroupLastContactPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.ScAccountFindLastContactResultPojo;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupVo;
import java.util.List;

public interface UserGroupService {

    /**
     * 添加UserGroup对象和RefUserGroupAccount对象，并返回新创建的群组的id
     *
     * @param userGroupName
     * @param newMemberAccountIdList
     * @return
     */
    ServiceResult<Integer> saveUserGroupAndRefUserGroupAccount(String userGroupName, List<String> newMemberAccountIdList);

    /**
     * 根据id去查询组信息
     * @param userGroupId
     * @return
     */
    ServiceResult <UserGroup> findByRealId(Integer userGroupId);

    /**
     * 根据自定义群组Id获取自定义群组中在线人数
     * @param userGroupId 自定义群组id
     * @return 在线人员数量
     */
    ServiceResult<Long> getUserNumberOnline(String userGroupId);

    /**
     * 从redis中获取最近一对一、群组的联系人数据。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量
     * @param account 账号
     * @return 返回近期的联系人数据
     */
    ServiceResult<List<AccountGroupLastContactPojo>> findRedisLastContact(String account);

    /**
     * 获取最近一对一、群组的联系人数据。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量
     * @param account 账号
     * @return 返回近期的联系人数据
     */
    ServiceResult<List<AccountGroupLastContactPojo>> findLastContact(String account);

    /**
     * 根据群组id修改用户自定义群组记录
     * @param userGroupVo
     * @return
     */
    ServiceResult<Integer> updateUserGroup(UserGroupVo userGroupVo);

    /**
     * 将用户最近联系人的数据导入redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importCurrentIntoRedis(List<String> accountList);

    /**
     * 导入最新的最近联系人数据
     * @param userName 用户名
     */
    ServiceResult<Boolean> insertUserCurrentDataToRedis(String userName);

    /**
     * 获取供应商协同账号的近期联系人
     * @param account 账号
     * @return 返回查询结果
     */
    ServiceResult<List<ScAccountFindLastContactResultPojo>> findScRecentContact(String account);

    /**
     * 从redis中获取供应商协同账号的近期联系人
     * @param account 账号
     * @return 返回查询结果
     */
    ServiceResult<List<ScAccountFindLastContactResultPojo>> findRedisScRecentContact(String account);
}