package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccountPrivilege;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import java.util.List;

/**
 * RefGroupAccountPrivilege的服务类接口
 */
public interface RefGroupAccountPrivilegeService {

    /**
     * 根据accountId和username查找RefGroupAccountPrivilege列表
     * @param accountId
     * @param username
     * @return
     */
    ServiceResult<List<RefGroupAccountPrivilege>> findByAccountIdAndUsername(String accountId, String username);

    /**
     * 插入账号对默认群组的权限表数据
     * @param list 需要插入到数据库的结果
     * @return 返回插入数据条数
     */
    ServiceResult<Integer> insertRefGroupAccountPrivilegeList(List<RefGroupAccountPrivilege> list);

    /**
     * 删除ref_group_account_privilege表中所有数据
     * @return 返回删除结果
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 赋予某用户高级权限
     * @return
     */
    void updateGroupId(String accountId, String username,Integer role);

    /**
     * 获取 群组全部数据
     * @return
     */
    List<RefGroupAccountPrivilege> refGroupAccountPrivilegeListAll();
}