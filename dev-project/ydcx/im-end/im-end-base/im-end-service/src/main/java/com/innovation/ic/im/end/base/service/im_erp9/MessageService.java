package com.innovation.ic.im.end.base.service.im_erp9;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.vo.im_erp9.MessageVo;
import java.util.List;

/**
 * Message的服务类接口
 */
public interface MessageService {

    /**
     * 保存Message对象
     *
     * @param message
     */
    ServiceResult<Integer> saveMessage(Message message);

    /**
     * 获取用户的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @param fromUserAccount
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<Page> page(String fromUserAccount, Integer pageSize, Integer pageNo);

    /**
     * 获取用户的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @param fromUserAccount
     * @param toUserAccount
     * @param pageSize
     * @param pageNo
     * @param type
     * @return
     */
    ServiceResult<List<Message>> redisPage(String fromUserAccount, String toUserAccount, Integer pageSize, Integer pageNo, Integer type, Integer timeout);

    /**
     * 根据参数type和content（模糊查询），获取用户的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @return
     */
    ServiceResult<List<Message>> redisLikePage(MessageVo messageVo, Integer timeout);

    /**
     * 根据参数fromUserAccount、toUserAccountinit、Row和direction，获取用户的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示
     *
     * @param fromUserAccount
     * @param toUserAccount
     * @param initRow
     * @param direction
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<Message>> redisContextPage(String fromUserAccount, String toUserAccount, Integer id, Integer initRow, Integer direction, Integer pageSize, Integer pageNo, Integer timeout);

    /**
     * 恢复未读消息，先在message表中通过fromUserAccount对toUserAccount修改消息状态，toUserAccount发送到消息服务器上。
     * @return
     */
    ServiceResult<Integer> restoreMessage(String fromUserAccount, String toUserAccount);

    /**
     * 撤回消息
     * @param id 账号id
     * @param dateTime 撤回时间
     */
    RetractionPojo retractionMessage(Integer id, Integer dateTime);

    /**
     * 查询未读消息id集合
     * @param fromUserAccount 发送消息的人
     * @param toUserAccount 接收消息的人
     * @return 返回查询结果
     */
    ServiceResult<List<String>> getUnreadMsgList(String fromUserAccount, String toUserAccount);

    /**
     * 将message表中的数据导入redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importMessageIntoRedis();
}