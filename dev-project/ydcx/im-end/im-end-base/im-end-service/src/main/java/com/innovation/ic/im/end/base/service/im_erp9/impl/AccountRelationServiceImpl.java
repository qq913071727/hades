package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.*;
import com.innovation.ic.im.end.base.model.im_erp9.*;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.AccountRelationConstants;
import com.innovation.ic.im.end.base.service.im_erp9.AccountRelationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   账号关系表Service实现类
 * @author linuo
 * @time   2023年4月18日17:24:31
 */
@Service
@Transactional
public class AccountRelationServiceImpl extends ServiceImpl<AccountRelationMapper, AccountRelation> implements AccountRelationService {
    private static final Logger log = LoggerFactory.getLogger(AccountRelationServiceImpl.class);

    @Resource
    private AccountRelationMapper accountRelationMapper;

    /**
     * 保存账号关系表数据
     * @param accountRelation 账号关系表数据
     * @return 返回保存结果
     */
    @Override
    public ServiceResult<Integer> saveAccountRelation(AccountRelation accountRelation) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setResult(accountRelationMapper.insert(accountRelation));
        return serviceResult;
    }

    /**
     * 查询账号关系表数据
     * @param scAccountId   供应商协同账号id
     * @param saleAccountId 销售账号id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<AccountRelation> selectDataByScSaleAccountId(String scAccountId, String saleAccountId) {
        AccountRelation result = null;
        QueryWrapper<AccountRelation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(AccountRelationConstants.SC_ACCOUNT_ID_FIELD, scAccountId);
        queryWrapper.eq(AccountRelationConstants.SALE_ACCOUNT_ID_FIELD, saleAccountId);
        List<AccountRelation> accountRelations = accountRelationMapper.selectList(queryWrapper);
        if(accountRelations != null && accountRelations.size() > 0){
            result = accountRelations.get(0);
        }
        ServiceResult<AccountRelation> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 根据ScAccountId删除账号关系表数据
     * @param scAccountId 供应商协同账号id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Integer> deleteByScAccountId(String scAccountId) {
        QueryWrapper<AccountRelation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(AccountRelationConstants.SC_ACCOUNT_ID_FIELD, scAccountId);
        int delete = accountRelationMapper.delete(queryWrapper);

        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(delete);
        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return serviceResult;
    }

    /**
     * 查询账号关系表数据
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<AccountRelation>> getAccountRelationListData() {
        List<AccountRelation> list = accountRelationMapper.getAccountRelationListData();

        ServiceResult<List<AccountRelation>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(list);
        return serviceResult;
    }
}