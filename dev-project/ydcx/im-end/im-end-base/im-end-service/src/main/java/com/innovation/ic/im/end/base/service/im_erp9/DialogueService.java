package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.Dialogue;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import java.util.List;

/**
 * Dialogue的服务类接口
 */
public interface DialogueService {

    /**
     * 添加9Dialogue对象
     *
     * @param dialogue
     * @return
     */
    ServiceResult<Boolean> saveDialogue(Dialogue dialogue);

    /**
     * 根据id，修改Dialogue对象
     *
     * @param dialogue
     * @return
     */
    ServiceResult<Boolean> updateDialogueById(Dialogue dialogue);

    /**
     * 根据id，删除Dialogue对象
     *
     * @param id
     * @return
     */
    ServiceResult<Boolean> deleteDialogueById(Integer id);

    /**
     * 根据id，置顶Dialogue对象
     *
     * @param id
     * @return
     */
    ServiceResult<Boolean> toppingDialogueById(Integer id);

    /**
     * 按照分组和用户id，返回话术。已置顶的按照置顶时间降序排列，没置顶的按照创建时间升序排列
     *
     * @param group
     * @param erp9AdminsId
     * @return
     */
    ServiceResult<List<Dialogue>> findByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc(String group, String erp9AdminsId);

    /**
     * 查询话术所有数据
     * @return 返回查询结果
     */
    ServiceResult<List<Dialogue>> findByAccountIdAndGroup();

    /**
     * 按照分组和用户id，返回话术。
     *
     * @param accountId
     * @param group
     * @return
     */
    ServiceResult<List<Dialogue>> findByAccountIdAndGroupOrderByCreateTimeAsc(String accountId, String group);

    /**
     * 从redis中获取数据按照分组和用户id，返回话术。已置顶的按照置顶时间降序排列，没置顶的按照创建时间升序排列
     * @param group 组
     * @param accountId id
     * @return
     */
    ServiceResult<List<Dialogue>> findRedisByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc(String group, String accountId);

    /**
     * 将dialogue表中的数据导入redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importDialogueIntoRedis();
}
