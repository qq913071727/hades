package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.ChatPair;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import java.util.List;
import java.util.Map;

/**
 * ChatPair的服务类接口
 */
public interface ChatPairService {
    /**
     * 删除chat_pair表中的所有数据
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 添加ChatPair对象
     *
     * @param chatPair
     * @return
     */
    ServiceResult<Boolean> saveChatPair(ChatPair chatPair);

    /**
     * 批量插入ChatPair对象
     * @param chatPairList
     * @return
     */
    ServiceResult<Boolean> saveChatPairList(List<ChatPair> chatPairList);

    /**
     * 根据fromUserAccount，查找ChatPair对象
     * @param fromUserAccount
     * @return
     */
    ServiceResult<ChatPair> findByFromUserAccountAndToUserAccount(String fromUserAccount);

    /**
     * 更新last_contact_time字段，两条记录都要更新
     * @param fromUserAccount 用户账号1
     * @param toUserAccount 用户账号2
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateAccountLastContactTime(String fromUserAccount, String toUserAccount);

    /**
     * 更新last_contact_time字段，两条记录都要更新
     * @param fromUserAccount
     * @param toUserAccount
     * @return
     */
    ServiceResult<Boolean> updateLastContactTime(String fromUserAccount, String toUserAccount);

    /**
     * 根据发送账号，接收账号,状态,去设置置顶时间
     *
     * @param fromUserAccount
     * @param toUserAccount
     * @param status
     * @return
     */
    ServiceResult<Boolean> updateCharPair(String fromUserAccount, String toUserAccount, Integer status);

    /**
     * 根据用户设置一对一聊天内容不可见时间
     * @param map 查询条件
     * @return 返回结果
     */
    ServiceResult<Boolean> invisibleChatRecordSet(Map<String, Object> map);

    /**
     * 备份chat_pair表中设置了置顶时间、最近联系时间、内容不可见时间的数据到temp_chat_pair表中
     * @return 返回备份结果
     */
    ServiceResult<Boolean> backupLastContactInvisibleIsNotNullData();

    /**
     * 将char_pair备份的数据更新到原表中
     * @return 返回更新结果
     */
    ServiceResult<Boolean> insertTempDataToChatpair();

    /**
     * 查询当前账号的最近联系人账号集合
     * @param account 账号
     * @return 返回当前账号的最近联系人账号集合
     */
    ServiceResult<List<String>> selectLastContactAccounts(String account);

    /**
     * 设置某个用户对某个用户消息免打扰
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount 接收消息的用户
     * @param status 状态。1表示设置为消息免打扰。0表示取消消息免打扰
     * @return 返回结果
     */
    ServiceResult<Boolean> noReminder(String fromUserAccount, String toUserAccount, Integer status);

    /**
     * 删除最近联系人并清除聊天记录
     * @param map 查询条件
     * @return 返回处理结果
     */
    ServiceResult<Boolean> deleteChat(Map<String, Object> map);

    /**
     * 更新某个用户与某个用户的聊天页面打开状态
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount 接收消息的用户
     * @param status 状态。0表示聊天窗口关闭，1表示聊天窗口打开
     * @return 返回结果
     */
    ServiceResult<Boolean> updateChatPageStatus(String fromUserAccount, String toUserAccount, Integer status);

    /**
     * 将当前用户所有的聊天页面状态设置为关闭
     * @param username 用户名
     * @return 返回结果
     */
    ServiceResult<Boolean> closeAccountChatPageStatus(String username);

    /**
     * 获取用户A与用户B的聊天页面打开状态
     * @param fromUserAccount 发送消息的用户
     * @param toUserAccount 接收消息的用户
     * @return 返回聊天页面打开状态(0:表示聊天窗口关闭、1:表示聊天窗口打开)
     */
    ServiceResult<Integer> getChatPageStatus(String fromUserAccount, String toUserAccount);

    /**
     * 删除历史供应商协同账号的聊天对数据
     * @param scAccountId 供应商协同账号
     * @return 返回删除结果
     */
    ServiceResult<Boolean> deleteScChatPairData(String scAccountId);

    /**
     * 获取最大的code
     * @return 返回查询结果
     */
    ServiceResult<Integer> getMaxCode();

    /**
     * 根据账号查询聊天对数据
     * @param fromUserAccount 发送消息的用户账号
     * @param toUserAccount 接收消息的用户账号
     * @return 返回查询结果
     */
    ServiceResult<List<ChatPair>> selectDataByAccount(String fromUserAccount, String toUserAccount);
}