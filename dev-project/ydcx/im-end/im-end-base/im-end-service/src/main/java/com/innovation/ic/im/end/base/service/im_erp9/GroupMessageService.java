package com.innovation.ic.im.end.base.service.im_erp9;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupMessageVo;
import java.util.List;

/**
 * GroupMessage的服务类接口
 */
public interface GroupMessageService {

    /**
     * 保存GroupMessage对象
     * @param groupMessage
     */
    ServiceResult<Integer> saveGroupMessage(GroupMessage groupMessage);

    /**
     * 获取用户的聊天记录，并按照创建时间降序排列，支持分页显示
     * @param fromUserAccount
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<Page> page(String fromUserAccount, Integer pageSize, Integer pageNo);

    /**
     * 根据参数type，获取某个默认群组的聊天记录，并按照创建时间降序排列，支持分页显示
     * @param groupId
     * @param pageSize
     * @param pageNo
     * @param type
     * @return
     */
    ServiceResult<List<GroupMessage>> redisPage(String groupId, String fromUserAccount, Integer pageSize, Integer pageNo, Integer type, Integer timeout);

    /**
     * 根据参数type和content（模糊查询），获取默认群组的聊天记录，并按照创建时间降序排列，支持分页显示
     * @return
     */
    ServiceResult<List<GroupMessage>> redisLikePage(GroupMessageVo groupMessageVo, Integer timeout);

    /**
     * 根据参数id、initRow和direction，获取默认群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示
     * @param id
     * @param fromUserAccount
     * @param initRow
     * @param direction
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<GroupMessage>> redisContextPage(String id, String fromUserAccount, Integer initRow, Integer direction, Integer pageSize, Integer pageNo, Integer timeout);

    /**
     * 将group_message表中的数据导入redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importGroupMessageIntoRedis();
}
