package com.innovation.ic.im.end.base.service.im_erp9;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountAddPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.SearchAccountDataPojo;
import com.innovation.ic.im.end.base.vo.im_erp9.AccountVo;
import com.innovation.ic.im.end.base.vo.im_erp9.BlurSearchScAccountVo;
import java.util.List;

/**
 * Account的服务类接口
 */
public interface AccountService {

    /**
     * 删除account表中的所有数据
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 返回所有人员，按照字母顺序升序排列
     * @return
     */
    ServiceResult<List<AccountPojo>> findAllOrderByRealNameAsc();

    /**
     * 批量插入Account对象
     * @param accountList
     * @return
     */
    ServiceResult<Boolean> saveAccountList(List<Account> accountList);

    /**
     * 根据id查找Account对象
     * @param id
     * @return
     */
    ServiceResult<Account> findById(String id);

    /**
     * 根据真实姓名/拼音/code去查找用户数据
     * @param accountVo 账号的vo类
     * @return 返回查询结果
     */
//    ServiceResult<List<Account>> findByRealNameLike(String realName);
    ServiceResult<List<SearchAccountDataPojo>> findByRealNameLike(AccountVo accountVo);

    /**
     * 获取近期的联系人。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量
     * @param account
     * @return
     */
    ServiceResult<List<AccountPojo>> findLastContact(String account);

    /**
     * 根据账号获取用户详细信息
     * @param accountId 账号
     * @return 返回用户详细信息
     */
    ServiceResult<Account> findByAccount(String accountId);

    /**
     * 根据账号记录登录状态
     * @param account 账号
     * @param entrance 登录入口。1表示erp9登录，2表示芯聊客户端登录
     * @return 返回登录结果
     */
    ServiceResult<Boolean> login(String account, Integer entrance);

    /**
     * 根据账号记录退出状态
     * @param account 账号
     * @param entrance 登录入口。1表示erp9登录，2表示芯聊客户端登录
     * @return 返回登录结果
     */
    ServiceResult<Boolean> logout(String account, Integer entrance);

    /**
    * 查询所有数据
    */
    ServiceResult<List<Account>> queryAll();

    /**
     * 修改Account表中的登录状态
     */
    void exit();

    /**
     * 保存账号数据
     * @param account 账号数据
     * @return 返回保存结果
     */
    ServiceResult<Integer> saveAccount(Account account);

    /**
     * 处理新增账号的mq数据
     * @param json 用户数据
     * @param groupName 群组名称
     * @param departmentName 用户所属部门名称
     */
    ServiceResult<Boolean> handleAddAccountRabbitMqData(JSONObject json, String groupName, String departmentName);

    /**
     * 处理删除账号的mq数据
     * @param json 用户数据
     */
    ServiceResult<Boolean> handleDeleteAccountRabbitMqData(JSONObject json);

    /**
     * 处理修改账号的mq数据
     * @param groupName 群组名称
     * @param json 用户数据
     */
    ServiceResult<Boolean> handleEditAccountRabbitMqData(JSONObject json, String groupName);

    /**
     * 处理修改账号基础信息的mq数据
     * @param json 用户数据
     */
    ServiceResult<Boolean> handleEditAccountInfoRabbitMqData(JSONObject json);

    /**
     * 处理修改账号状态的mq数据
     * @param json 用户数据
     * @param accountList 账号数据集合
     */
    ServiceResult<List<String>> handleEditAccountStatusRabbitMqData(JSONObject json, List<AccountAddPojo> accountList);

    /**
     * 查询全部账号
     * @return 返回查询结果
     */
    ServiceResult<List<String>> selectAllAccountUserName();

    /**
     * 将account表中的数据导入Redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importAccountIntoRedis();

    /**
     * 查询系统账号
     * @return 返回查询结果
     */
    ServiceResult<List<String>> getSystemAccount();

    /**
     * 设置系统账号登录状态为在线
     * @param accountList 系统账号
     * @return 返回结果
     */
    ServiceResult<Boolean> setSystemAccountLoginOnline(List<String> accountList);

    /**
     * 清空account表中erp导入的账号数据
     */
    void deleteErpAccountData();

    /**
     * 更新账号数据
     * @param account 账号数据
     * @return 返回更新结果
     */
    ServiceResult<Integer> updateAccountData(Account account);

    /**
     * 根据id删除账号表数据
     * @param id 账号id
     * @return 返回删除结果
     */
    ServiceResult<Integer> deleteById(String id);

    /**
     * 模糊查询供应商协同账号信息
     * @param blurSearchScAccountVo 模糊查询供应商协同账号信息接口的vo类
     * @return 返回查询结果
     */
    ServiceResult<List<SearchAccountDataPojo>> blurSearchScAccountInfo(BlurSearchScAccountVo blurSearchScAccountVo);
}