package com.innovation.ic.im.end.base.service.helper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.manager.MinioManager;
import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.SftpChannelManager;
import com.innovation.ic.im.end.base.service.im_erp9.*;
import com.innovation.ic.im.end.base.service.im_erp9.impl.*;
import org.springframework.context.ApplicationContext;

/**
 * ServiceImpl的帮助类
 */
public class ServiceImplHelper {

    /**
     * MessageService类
     */
    private static MessageService messageService;

    /**
     * MessageService类
     */
    private static AccountService accountService;

    /**
     * GroupMessageReceiverService类
     */
    private static GroupMessageReceiverService groupMessageReceiverService;

    /**
     * UserGroupMessageReceiverService类
     */
    private static UserGroupMessageReceiverService userGroupMessageReceiverService;

    /**
     * GroupMessageService类
     */
    private static GroupMessageService groupMessageService;

    /**
     * RefGroupAccountService类
     */
    private static RefGroupAccountService refGroupAccountService;

    /**
     * RefGroupAccountOperationService类
     */
    private static RefGroupAccountOperationService refGroupAccountOperationService;

    /**
     * UserGroupMessageService类
     */
    private static UserGroupMessageService userGroupMessageService;

    /**
     * UserGroupService类
     */
    private static UserGroupService userGroupService;

    /**
     * RefUserGroupAccountService类
     */
    private static RefUserGroupAccountService refUserGroupAccountService;

    /**
     * ChatPairService类
     */
    private static ChatPairService chatPairService;

    /**
     * UserLoginLogService类
     */
    private static UserLoginLogService userLoginLogService;

    /**
     * MinioManager类
     */
    private static MinioManager minioManager;

    /**
     * QueueMessageManager类
     */
    private static RabbitMqManager rabbitMqManager;

    /**
     * RedisManager类
     */
    private static RedisManager redisManager;

    /**
     * RedisManager类
     */
    private static SftpChannelManager sftpChannelManager;

    /**
     * 返回具体的manager类
     *
     * @param clazz
     * @return
     */
    private static Object getManager(Class clazz) {
        ApplicationContext applicationContext = com.innovation.ic.im.end.base.service.helper.ApplicationContextRegister.getApplicationContext();
        return applicationContext.getBean(clazz);
    }

    /**
     * 返回MinioManager
     *
     * @return
     */
    public static MinioManager getMinioManager() {
        if (null != minioManager) {
            return minioManager;
        } else {
            return (MinioManager) ServiceImplHelper.getManager(MinioManager.class);
        }
    }

    /**
     * 返回RabbitMqManager
     *
     * @return
     */
    public static RabbitMqManager getRabbitMqManager() {
        if (null != rabbitMqManager) {
            return rabbitMqManager;
        } else {
            return (RabbitMqManager) ServiceImplHelper.getManager(RabbitMqManager.class);
        }
    }

    /**
     * 返回QueueMessageManager
     *
     * @return
     */
    public static RedisManager getRedisManager() {
        if (null != redisManager) {
            return redisManager;
        } else {
            return (RedisManager) ServiceImplHelper.getManager(RedisManager.class);
        }
    }

    /**
     * 返回SftpChannalManager
     *
     * @return
     */
    public static SftpChannelManager getSftpChannalManager() {
        if (null != sftpChannelManager) {
            return sftpChannelManager;
        } else {
            return (SftpChannelManager) ServiceImplHelper.getManager(SftpChannelManager.class);
        }
    }

    /**
     * 返回具体的service实现类
     * @param clazz
     * @return
     */
    private static ServiceImpl getService(Class clazz) {
        ApplicationContext applicationContext = ApplicationContextRegister.getApplicationContext();
        return (ServiceImpl) applicationContext.getBean(clazz);
    }

    /**
     * 返回MessageService
     * @return
     */
    public static UserLoginLogService getUserLoginLogService() {
        if (null != userLoginLogService) {
            return userLoginLogService;
        } else {
            return (UserLoginLogService) ServiceImplHelper.getService(UserLoginLogServiceImpl.class);
        }
    }

    /**
     * 返回MessageService
     * @return
     */
    public static MessageService getMessageService() {
        if (null != messageService) {
            return messageService;
        } else {
            return (MessageService) ServiceImplHelper.getService(MessageServiceImpl.class);
        }
    }

    /**
     * 返回OfflineMessageService
     * @return
     */
    public static AccountService getAccountService() {
        if (null != accountService) {
            return accountService;
        } else {
            return (AccountService) ServiceImplHelper.getService(AccountServiceImpl.class);
        }
    }

    /**
     * 返回GroupMessageReceiverService
     * @return GroupMessageReceiverService
     */
    public static GroupMessageReceiverService getGroupMessageReceiverService() {
        if (null != groupMessageReceiverService) {
            return groupMessageReceiverService;
        } else {
            return (GroupMessageReceiverService) ServiceImplHelper.getService(GroupMessageReceiverServiceImpl.class);
        }
    }

    /**
     * 返回UserGroupMessageReceiverService
     * @return 返回UserGroupMessageReceiverService
     */
    public static UserGroupMessageReceiverService getUserGroupMessageReceiverService() {
        if (null != userGroupMessageReceiverService) {
            return userGroupMessageReceiverService;
        } else {
            return (UserGroupMessageReceiverService) ServiceImplHelper.getService(UserGroupMessageReceiverServiceImpl.class);
        }
    }

    /**
     * 返回GroupMessageService
     * @return
     */
    public static GroupMessageService getGroupMessageService() {
        if (null != groupMessageService) {
            return groupMessageService;
        } else {
            return (GroupMessageService) ServiceImplHelper.getService(GroupMessageServiceImpl.class);
        }
    }

    /**
     * 返回UserGroupMessageService
     * @return
     */
    public static UserGroupMessageService getUserGroupMessageService() {
        if (null != userGroupMessageService) {
            return userGroupMessageService;
        } else {
            return (UserGroupMessageService) ServiceImplHelper.getService(UserGroupMessageServiceImpl.class);
        }
    }

    /**
     * 返回UserGroupService
     * @return
     */
    public static UserGroupService getUserGroupService() {
        if (null != userGroupService) {
            return userGroupService;
        } else {
            return (UserGroupService) ServiceImplHelper.getService(UserGroupServiceImpl.class);
        }
    }

    /**
     * 返回RefGroupAccountService
     * @return
     */
    public static RefGroupAccountService getRefGroupAccountService() {
        if (null != refGroupAccountService) {
            return refGroupAccountService;
        } else {
            return (RefGroupAccountService) ServiceImplHelper.getService(RefGroupAccountServiceImpl.class);
        }
    }

    /**
     * 返回RefGroupAccountOperationService
     * @return
     */
    public static RefGroupAccountOperationService getRefGroupAccountOperationService() {
        if (null != refGroupAccountOperationService) {
            return refGroupAccountOperationService;
        } else {
            return (RefGroupAccountOperationService) ServiceImplHelper.getService(RefGroupAccountOperationService.class);
        }
    }

    /**
     * 返回RefUserGroupAccountService
     * @return
     */
    public static RefUserGroupAccountService getRefUserGroupAccountService() {
        if (null != refUserGroupAccountService) {
            return refUserGroupAccountService;
        } else {
            return (RefUserGroupAccountService) ServiceImplHelper.getService(RefUserGroupAccountServiceImpl.class);
        }
    }

    /**
     * 返回ChatPairService
     * @return
     */
    public static ChatPairService getChatPairService() {
        if (null != chatPairService) {
            return chatPairService;
        } else {
            return (ChatPairService) ServiceImplHelper.getService(ChatPairServiceImpl.class);
        }
    }
}