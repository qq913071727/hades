package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.CustomerServiceScoreMapper;
import com.innovation.ic.im.end.base.model.im_erp9.CustomerServiceScore;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.im_erp9.CustomerServiceScoreService;
import com.innovation.ic.im.end.base.vo.im_erp9.CustomerServiceScoreAddVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Date;

/**
 * @desc   客服评分表Service实现类
 * @author linuo
 * @time   2023年4月20日17:04:54
 */
@Service
@Transactional
public class CustomerServiceScoreServiceImpl extends ServiceImpl<CustomerServiceScoreMapper, CustomerServiceScore> implements CustomerServiceScoreService {
    private static final Logger log = LoggerFactory.getLogger(CustomerServiceScoreServiceImpl.class);

    @Resource
    private CustomerServiceScoreMapper customerServiceScoreMapper;

    /**
     * 新增客服评分
     * @param customerServiceScoreAddVo 新增客服评分的vo类
     * @return 返回评分结果
     */
    @Override
    public ServiceResult<Boolean> addScore(CustomerServiceScoreAddVo customerServiceScoreAddVo) {
        Boolean result = Boolean.FALSE;
        String message = ServiceResult.INSERT_FAIL;

        CustomerServiceScore customerServiceScore = new CustomerServiceScore();
        BeanUtils.copyProperties(customerServiceScoreAddVo, customerServiceScore);
        customerServiceScore.setCreateTime(new Date(System.currentTimeMillis()));
        int insert = customerServiceScoreMapper.insert(customerServiceScore);
        if(insert > 0){
            log.info("客服评分表数据添加成功");
            result = Boolean.TRUE;
            message = ServiceResult.INSERT_SUCCESS;
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(message);
        serviceResult.setResult(result);
        return serviceResult;
    }
}