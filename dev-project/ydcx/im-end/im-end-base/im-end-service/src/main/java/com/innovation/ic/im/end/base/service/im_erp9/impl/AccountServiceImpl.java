package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.mapper.im_erp9.*;
import com.innovation.ic.im.end.base.model.im_erp9.*;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.*;
import com.innovation.ic.im.end.base.pojo.enums.AccountIntegrationEnum;
import com.innovation.ic.im.end.base.pojo.enums.ErpAccountStatusEnum;
import com.innovation.ic.im.end.base.pojo.enums.LoginStatusEnum;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountAddPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountFindLastContactResultPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.SearchAccountDataPojo;
import com.innovation.ic.im.end.base.service.im_erp9.AccountService;
import com.innovation.ic.b1b.framework.util.HanziToPinyinUtils;
import com.innovation.ic.im.end.base.vo.im_erp9.AccountVo;
import com.innovation.ic.im.end.base.vo.im_erp9.BlurSearchScAccountVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Account的具体实现类
 */
@Service
@Transactional
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {
    private static final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Resource
    private AccountMapper accountMapper;

    @Resource
    private GroupMapper groupMapper;

    @Resource
    private TreeNodeMapper treeNodeMapper;

    @Resource
    private ChatPairMapper chatPairMapper;

    @Resource
    private RefGroupAccountPrivilegeMapper refGroupAccountPrivilegeMapper;

    @Resource
    private RefGroupAccountOperationMapper refGroupAccountOperationMapper;

    @Resource
    private RefGroupAccountMapper refGroupAccountMapper;

    @Resource
    private RefUserGroupAccountMapper refUserGroupAccountMapper;

    @Resource
    private UserGroupMapper userGroupMapper;

    @Resource
    private RedisManager redisManager;

    /**
     * 处理修改账号状态的mq数据
     * @param json 用户数据
     * @param accountList 账号数据集合
     */
    @Override
    public ServiceResult<List<String>> handleEditAccountStatusRabbitMqData(JSONObject json, List<AccountAddPojo> accountList) {
        // 有变化的默认群组id集合
        List<String> updateInfoGroupIdList = new ArrayList<>();

        if(json != null && !json.isEmpty()){
            // 状态(200 启用、500 禁用)
            Integer status = json.getInteger(RabbitMqConstants.STATUS_FIELD);

            // 启用
            if(status.equals(ErpAccountStatusEnum.ENABLE.getCode())){
                if(accountList != null && accountList.size() > 0){
                    for(AccountAddPojo accountAddPojo : accountList){
                        // 插入账号信息
                        Boolean insertAccountResult = insertAccountData(accountAddPojo.getAdminId(), accountAddPojo.getRealName(), accountAddPojo.getDyjCode(), accountAddPojo.getUserName(),
                                accountAddPojo.getMobile(), accountAddPojo.getDepartmentName());
                        if(insertAccountResult){
                            // 插入与其他人的聊天对数据
                            insertChatPairData(accountAddPojo.getUserName());

                            // 添加新的群组关联关系
                            addGroupRelationship(accountAddPojo.getGroupId(), accountAddPojo.getAdminId(), accountAddPojo.getUserName(), accountAddPojo.getRealName(), accountAddPojo.getGroupName());

                            // 添加与组织机构群组的关联关系
                            addGroupRelationship(TreeNodeConstants.ORGANIZATION_GROUP_ID, accountAddPojo.getAdminId(), accountAddPojo.getUserName(), accountAddPojo.getRealName(), accountAddPojo.getGroupName());

                            if(!Strings.isNullOrEmpty(accountAddPojo.getGroupId())){
                                updateInfoGroupIdList.add(accountAddPojo.getGroupId());
                            }
                        }
                    }
                }
                log.info("处理启用账号状态的mq数据结束");
            }

            // 禁用
            if(status.equals(ErpAccountStatusEnum.CLOSE.getCode())){
                // 账号id数组字段
                JSONArray jsonArray = (JSONArray) json.get(RabbitMqConstants.ADMIN_IDS_FIELD);
                List<String> ids = JSONObject.parseArray(jsonArray.toJSONString(),  String.class);

                for(String adminId : ids){
                    // 先查询账号是否存在,不存在时无需操作,存在时删除账号及其它关联数据
                    Account account = accountMapper.selectById(adminId);
                    if(account != null){
                        // 查询与当前账号关联的默认群组id集合
                        List<String> refGroupAccountList = refGroupAccountMapper.getGroupIdListByAdminId(adminId);
                        if(refGroupAccountList != null && refGroupAccountList.size() > 0){
                            updateInfoGroupIdList.addAll(refGroupAccountList);
                            for (String groupId : refGroupAccountList) {
                                // 删除群组关联关系
                                deleteGroupRelationship(groupId, adminId);

                                // 删除账号信息
                                int i = accountMapper.deleteById(adminId);
                                if(i > 0){
                                    log.info("用户账号已删除,用户id:[{}],用户名:[{}]", adminId, account.getUsername());
                                }
                            }
                        }
                        // 查询与当前账号关联的自定义群组id集合
                        List<String> refUserGroupAccountList = refUserGroupAccountMapper.getUserGroupIdListAdminId(adminId);
                        if(refUserGroupAccountList != null && refUserGroupAccountList.size() > 0){
                            updateInfoGroupIdList.addAll(refUserGroupAccountList);
                            // 删除群组关联关系
                            deleteUserGroupRelationship(adminId);
                        }
                    }
                }
                log.info("处理禁用账号状态的mq数据结束");
            }
        }

        ServiceResult<List<String>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        serviceResult.setResult(updateInfoGroupIdList);
        return serviceResult;
    }

    /**
     * 处理修改账号基础信息的mq数据
     * @param json 用户数据
     */
    @Override
    public ServiceResult<Boolean> handleEditAccountInfoRabbitMqData(JSONObject json) {
        if(json != null && !json.isEmpty()){
            // 账号id
            String adminId = json.getString(RabbitMqConstants.ADMIN_ID_FIELD);

            // 大赢家code
            String dyjCode = json.getString(RabbitMqConstants.DYJ_CODE_FIELD);

            // 真实姓名
            String realName = json.getString(RabbitMqConstants.REAL_NAME_FIELD);

            // 手机号
            String phone = json.getString(RabbitMqConstants.PHONE_FIELD);

            if(!Strings.isNullOrEmpty(adminId)){
                Boolean ifEdit = Boolean.FALSE;

                // 查询账号信息
                Account account = new Account();

                // 修改大赢家code
                if(!Strings.isNullOrEmpty(dyjCode)){
                    account.setDyjCode(dyjCode);
                    ifEdit = Boolean.TRUE;
                }

                // 修改真实姓名、拼音、拼音首字母
                if(!Strings.isNullOrEmpty(realName)){
                    account.setRealName(realName);
                    // 处理拼音及汉语拼音首字母字段数据
                    String pinyin = HanziToPinyinUtils.getPinyin(realName);
                    account.setPinYin(pinyin);
                    account.setCapitalInitial(pinyin.substring(0, 1).toUpperCase());
                    ifEdit = Boolean.TRUE;
                }

                // 修改手机号
                if(!Strings.isNullOrEmpty(phone)){
                    account.setMobile(phone);
                    ifEdit = Boolean.TRUE;
                }

                if(ifEdit){
                    UpdateWrapper<Account> updateWrapper = new UpdateWrapper<>();
                    updateWrapper.eq(Constants.ID, adminId);

                    int update = accountMapper.update(account, updateWrapper);
                    if(update > 0){
                        log.info("修改用户信息成功,用户id:[{}]", adminId);
                    }
                }
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        return serviceResult;
    }

    /**
     * 处理修改账号的mq数据
     * @param json 用户数据
     * @param groupName 群组名称
     */
    @Override
    public ServiceResult<Boolean> handleEditAccountRabbitMqData(JSONObject json, String groupName) {
        if(json != null && !json.isEmpty()){
            // 群组id
            String groupId = json.getString(RabbitMqConstants.STRUCTURE_ID_FIELD);

            // 上次组织架构id
            String oldGroupId = json.getString(RabbitMqConstants.OLD_STRUCTURE_ID_FIELD);

            // 账号id
            String adminId = json.getString(RabbitMqConstants.ADMIN_ID_FIELD);

            // 用户名
            String userName = json.getString(RabbitMqConstants.USER_NAME_FIELD);

            // 真实姓名
            String realName = json.getString(RabbitMqConstants.REAL_NAME_FIELD);

            // 删除原群组关联关系
            deleteGroupRelationship(oldGroupId, adminId);

            // 添加新的群组关联关系
            addGroupRelationship(groupId, adminId, userName, realName, groupName);
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        return serviceResult;
    }

    /**
     * 处理删除账号的mq数据
     * @param json 用户数据
     */
    @Override
    public ServiceResult<Boolean> handleDeleteAccountRabbitMqData(JSONObject json) {
        Boolean result = Boolean.FALSE;
        if(json != null && !json.isEmpty()){
            // 组织架构id
            String groupId = json.getString(RabbitMqConstants.STRUCTURE_ID_FIELD);

            // 账号id
            String adminId = json.getString(RabbitMqConstants.ADMIN_ID_FIELD);

            if(!Strings.isNullOrEmpty(groupId) && !Strings.isNullOrEmpty(adminId)){
                // 删除群组关联关系
                result = deleteGroupRelationship(groupId, adminId);
            }
            if (!Strings.isNullOrEmpty(adminId)){
                //删除自定义群组关联关系
                deleteUserGroupRelationship(adminId);
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        return serviceResult;
    }

    /**
     * 处理新增账号的mq数据
     * @param json 用户数据
     * @param groupName 群组名称
     * @param departmentName 用户所属部门名称
     */
    @Override
    public ServiceResult<Boolean> handleAddAccountRabbitMqData(JSONObject json, String groupName, String departmentName) {
        if(json != null && !json.isEmpty()){
            // 群组id
            String groupId = json.getString(RabbitMqConstants.STRUCTURE_ID_FIELD);

            // 账号id
            String adminId = json.getString(RabbitMqConstants.ADMIN_ID_FIELD);

            // 用户名
            String userName = json.getString(RabbitMqConstants.USER_NAME_FIELD);

            // 真实姓名
            String realName = json.getString(RabbitMqConstants.REAL_NAME_FIELD);

            // 大赢家code
            String dyjCode = json.getString(RabbitMqConstants.DYJ_CODE_FIELD);

            // 手机号
            String mobile = json.getString(RabbitMqConstants.MOBILE_FIELD);

            // 插入账号信息
            Boolean insertAccountResult = insertAccountData(adminId, realName, dyjCode, userName, mobile, departmentName);
            if(insertAccountResult){
                // 插入与其他人的聊天对数据
                insertChatPairData(userName);

                // 添加新的群组关联关系
                addGroupRelationship(groupId, adminId, userName, realName, groupName);
            }
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setResult(Boolean.TRUE);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.OPERATE_SUCCESS);
        return serviceResult;
    }

    /**
     * 保存账号数据
     * @param account 账号数据
     * @return 返回保存结果
     */
    @Override
    public ServiceResult<Integer> saveAccount(Account account) {
        int insert = accountMapper.insert(account);

        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setResult(insert);
        return serviceResult;
    }

    /**
     * 根据账号记录登录状态
     * @param account 账号
     * @return 返回登录结果
     */
    @Override
    public ServiceResult<Boolean> login(String account, Integer entrance) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        UpdateWrapper<Account> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.USER_NAME, account);

        Account updateData = new Account();

        log.info("调用接口【/api/v1/account/login/{}/{}】,更新登录状态", account, entrance);

        // erp9登录
        if(entrance == OperateEntranceType.ERP9.intValue()){
            updateData.setErp9Login(LoginStatusEnum.login.getCode());
            updateData.setLastErp9LoginTime(new Date(System.currentTimeMillis()));
        }else{
            // 芯聊客户端登录
            updateData.setXlLogin(LoginStatusEnum.login.getCode());
            updateData.setLastXlLoginTime(new Date(System.currentTimeMillis()));
        }

        int update = accountMapper.update(updateData, updateWrapper);
        if(update > 0){
            log.info("记录用户:[{}]登录状态及登录时间成功", account);
        }else{
            log.warn("记录用户:[{}]登录状态及登录时间失败", account);
        }

        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据账号记录退出状态
     * @param account 账号
     * @param entrance 登录入口。1表示erp9登录，2表示芯聊客户端登录
     * @return 返回登录结果
     */
    @Override
    public ServiceResult<Boolean> logout(String account, Integer entrance) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        UpdateWrapper<Account> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.USER_NAME, account);

        Account updateData = new Account();
        // erp9退出
        if(entrance == OperateEntranceType.ERP9.intValue()){
            updateData.setErp9Login(LoginStatusEnum.logout.getCode());
        }else{
            updateData.setXlLogin(LoginStatusEnum.logout.getCode());
        }

        int update = accountMapper.update(updateData, updateWrapper);
        if(update > 0){
            log.info("记录用户:[{}]退出状态成功", account);
        }else{
            log.warn("记录用户:[{}]退出状态失败", account);
        }

        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 删除account表中的所有数据
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Boolean> truncateTable() {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        accountMapper.truncateTable();

        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 返回所有人员，按照字母顺序升序排列
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<AccountPojo>> findAllOrderByRealNameAsc() {
        ServiceResult<List<AccountPojo>> serviceResult = new ServiceResult<List<AccountPojo>>();

        List<AccountPojo> accountPojoList = accountMapper.findAllOrderByRealNameAsc();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(accountPojoList);

        return serviceResult;
    }

    /**
     * 批量插入Account对象
     * @param accountList 账号list
     * @return 返回保存结果
     */
    @Override
    public ServiceResult<Boolean> saveAccountList(List<Account> accountList) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        // 给account数据补充汉语拼音及汉语拼音首字母
        log.info("-----给account数据补充汉语拼音及汉语拼音首字母开始-----");
        for(Account account : accountList){
            addAccountDataPinYinCapitalInitial(account);
            account.setIntegration(AccountIntegrationEnum.ERP_CORRELATION.getCode());
        }
        log.info("-----给account数据补充汉语拼音及汉语拼音首字母结束-----");

        Integer count = baseMapper.insertBatchSomeColumn(accountList);
        if(count > 0){
            log.info("插入account表数据成功,入库数据条数:[{}]", count);
        }

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据id查找Account对象
     * @param id
     * @return
     */
    @Override
    public ServiceResult<Account> findById(String id) {
        ServiceResult<Account> serviceResult = new ServiceResult<>();
        Account account = baseMapper.selectById(id);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(account);
        return serviceResult;
    }

    /**
     * 模糊查询供应商协同账号信息
     * @param blurSearchScAccountVo 模糊查询供应商协同账号信息接口的vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<SearchAccountDataPojo>> blurSearchScAccountInfo(BlurSearchScAccountVo blurSearchScAccountVo) {
        List<SearchAccountDataPojo> result;

        String searchInfo = blurSearchScAccountVo.getSearchInfo();
        // 查询参数为拼音
        if (searchInfo.matches(ParameterType.pinYin)){
            result = accountMapper.findByRealPinYinLike(AccountIntegrationEnum.SC_CORRELATION.getCode(), searchInfo);
        }else {
            // 查询参数为姓名
            result = accountMapper.findByRealNameLike(AccountIntegrationEnum.SC_CORRELATION.getCode(), searchInfo);
        }

        ServiceResult<List<SearchAccountDataPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 根据真实姓名/拼音/code去查找用户数据
     * @param accountVo 账号的vo类
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<SearchAccountDataPojo>> findByRealNameLike(AccountVo accountVo) {
        ServiceResult<List<SearchAccountDataPojo>> serviceResult = new ServiceResult<>();
        String realName = accountVo.getRealName();
        //如果传递参数为code
        if (realName.matches(ParameterType.dyjCode)){
            List<SearchAccountDataPojo> accountList = accountMapper.findByRealDyjCodeLike(AccountIntegrationEnum.ERP_CORRELATION.getCode(), realName);
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(accountList);
            return serviceResult;
        }else if (realName.matches(ParameterType.pinYin)){//如果传递参数为拼音
            List<SearchAccountDataPojo> accountList = accountMapper.findByRealPinYinLike(AccountIntegrationEnum.ERP_CORRELATION.getCode(), realName);
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(accountList);
            return serviceResult;
        }else {//如果传递参数为姓名
            List<SearchAccountDataPojo> accountList = accountMapper.findByRealNameLike(AccountIntegrationEnum.ERP_CORRELATION.getCode(), realName);
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(accountList);
            return serviceResult;
        }
    }

    /**
     * 获取近期的联系人。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量、最后一条消息
     * @param account 账号
     * @return 查询结果
     */
    @Override
    public ServiceResult<List<AccountPojo>> findLastContact(String account) {
        ServiceResult<List<AccountPojo>> serviceResult = new ServiceResult<>();

        List<AccountPojo> accountPojoList = new ArrayList<>();
        List<AccountFindLastContactResultPojo> lastContact = accountMapper.findLastContact(account);

        if (lastContact != null && !lastContact.isEmpty()){
            /*for (int i = 0; i < lastContact.size() ; i++) {
                AccountFindLastContactResultPojo accountFindLastContactResultPojo = lastContact.get(i);
                AccountPojo accountPojo = new AccountPojo();
                BeanUtils.copyProperties(accountFindLastContactResultPojo, accountPojo);
                // 消息表内容类时间
                Date messageTime = accountFindLastContactResultPojo.getMessageTime();
                // 判断消息表、未读消息表时间是否小于内容不可见时间
                if(accountFindLastContactResultPojo.getContentInvisibleTime() != null && messageTime != null && offlineMessageTime != null && accountFindLastContactResultPojo.getContentInvisibleTime().compareTo(messageTime) > 0 &&
                        accountFindLastContactResultPojo.getContentInvisibleTime().compareTo(offlineMessageTime) > 0){
                    accountPojoList.add(accountPojo);
                    continue;
                }

                if(messageTime != null && offlineMessageTime != null){
                    if(messageTime.compareTo(offlineMessageTime) > 0){
                        accountPojo.setLastMessageTime(messageTime);
                        accountPojo.setLastMessageType(accountFindLastContactResultPojo.getMessageType());
                        accountPojo.setLastMessage(accountFindLastContactResultPojo.getMessageContent());
                    }else{
                        accountPojo.setLastMessageTime(offlineMessageTime);
                        accountPojo.setLastMessageType(accountFindLastContactResultPojo.getOfflineMessageType());
                        accountPojo.setLastMessage(accountFindLastContactResultPojo.getOfflineMessageContent());
                    }
                }

                // 消息表时间不为空，未读消息表为空时返回消息表的信息
                if(messageTime != null && offlineMessageTime == null){
                    accountPojo.setLastMessageTime(messageTime);
                    accountPojo.setLastMessageType(accountFindLastContactResultPojo.getMessageType());
                    accountPojo.setLastMessage(accountFindLastContactResultPojo.getMessageContent());
                }

                // 消息表时间为空，未读消息表不为空时返回未读消息表的信息
                if(messageTime == null && offlineMessageTime != null){
                    accountPojo.setLastMessageTime(offlineMessageTime);
                    accountPojo.setLastMessageType(accountFindLastContactResultPojo.getOfflineMessageType());
                    accountPojo.setLastMessage(accountFindLastContactResultPojo.getOfflineMessageContent());
                }

                // 当内容不可见时间大于最近联系时间时，将最近联系时间信息置空
                if(accountPojo.getLastMessageTime() != null && accountFindLastContactResultPojo.getContentInvisibleTime() != null){
                    if(accountFindLastContactResultPojo.getContentInvisibleTime().compareTo(accountPojo.getLastMessageTime()) > 0){
                        accountPojo.setLastMessage(null);
                        accountPojo.setLastMessageTime(null);
                        accountPojo.setLastMessageType(null);
                    }
                }

                accountPojoList.add(accountPojo);
            }*/
        }

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(accountPojoList);
        return serviceResult;
    }

    /**
     * 根据账号获取用户详细信息
     * @param accountId 账号
     * @return 返回用户详细信息
     */
    @Override
    public ServiceResult<Account> findByAccount(String accountId) {
        ServiceResult<Account> serviceResult = new ServiceResult<Account>();
        QueryWrapper<Account> queryWrapper = new QueryWrapper<Account>();
        queryWrapper.eq("username", accountId);
        Account account = baseMapper.selectOne(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(account);
        return serviceResult;
    }

    /**
     * 查询所有数据
     */
    @Override
    public ServiceResult<List<Account>> queryAll(){
        ServiceResult<List<Account>> serviceResult = new ServiceResult<List<Account>>();

        List<Account> accountList = accountMapper.queryAll();

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(accountList);
        return serviceResult;
    }

    /**
     * 修改Account表中的登录状态
     */
    @Override
    public void exit(){
        accountMapper.exit();
    }

    /**
     * 查询全部账号
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<String>> selectAllAccountUserName() {
        // 查询全部账号
        List<String> userNameList = accountMapper.selectAllAccountUserName();

        ServiceResult<List<String>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(userNameList);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }

    /**
     * 将account表中的数据导入Redis
     * @return 返回导入结果
     */
    @Override
    public ServiceResult<Boolean> importAccountIntoRedis() {
        ServiceResult<Boolean> returnResult = new ServiceResult<>();
        //获取账户所有数据
        ServiceResult<List<Account>> listServiceResult = queryAll();
        List<Account> result = listServiceResult.getResult();
        if (result != null && result.size() > 0) {
            for (Account account : result) {
                //数据存入redis
                addAccountDataToRedis(account);
            }
        }
        //添加所有账户信息到redis
        ServiceResult<List<AccountPojo>> allOrderByRealNameAsc = findAllOrderByRealNameAsc();
        redisManager.set(RedisStorage.ACCOUNT_ALL,JSON.toJSONString(allOrderByRealNameAsc));

        returnResult.setMessage(ServiceResult.SYNC_SUCCESS);
        returnResult.setSuccess(Boolean.TRUE);
        returnResult.setResult(Boolean.TRUE);
        return returnResult;
    }

    /**
     * 查询系统账号
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<String>> getSystemAccount() {
        List<String> systemAccount = accountMapper.getSystemAccount();
        ServiceResult<List<String>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(systemAccount);
        return serviceResult;
    }

    /**
     * 设置系统账号登录状态为在线
     * @param accountList 系统账号
     * @return 返回结果
     */
    @Override
    public ServiceResult<Boolean> setSystemAccountLoginOnline(List<String> accountList) {
        String message = ServiceResult.OPERATE_FAIL;
        boolean result = Boolean.FALSE;

        UpdateWrapper<Account> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in(AccountConstants.USERNAME_FIELD, accountList);

        Account account = new Account();
        account.setXlLogin(LoginStatusEnum.login.getCode());
        account.setErp9Login(LoginStatusEnum.login.getCode());
        account.setLastXlLoginTime(new Date(System.currentTimeMillis()));
        account.setLastErp9LoginTime(new Date(System.currentTimeMillis()));

        int update = accountMapper.update(account, updateWrapper);
        if(update > 0){
            message = ServiceResult.OPERATE_SUCCESS;
            result = Boolean.TRUE;
        }

        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(message);
        serviceResult.setSuccess(result);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 清空account表中erp导入的账号数据
     */
    @Override
    public void deleteErpAccountData() {
        accountMapper.deleteErpAccountData();
    }

    /**
     * 更新账号数据
     * @param account 账号数据
     * @return 返回更新结果
     */
    @Override
    public ServiceResult<Integer> updateAccountData(Account account) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(accountMapper.updateById(account));
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return serviceResult;
    }

    /**
     * 根据id删除账号表数据
     * @param id 账号id
     * @return 返回删除结果
     */
    @Override
    public ServiceResult<Integer> deleteById(String id) {
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.ID, id);
        int delete = accountMapper.delete(queryWrapper);

        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        serviceResult.setResult(delete);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return serviceResult;
    }

    /**
     * 添加account数据到redis
     * @param account 账号
     */
    private void addAccountDataToRedis(Account account) {
        //以id为 添加到redis
        redisManager.set(RedisStorage.ACCOUNT_BY_ADMINS_ID_PREFIX + account.getId(), JSON.toJSONString(account));
        //以name为 添加到redis
        redisManager.set(RedisStorage.ACCOUNT_BY_ACCOUNT_PREFIX + account.getUsername(), JSON.toJSONString(account));
    }

    /**
     * 从tree_node表中删除当前账号
     * @param structureId 组织机构id
     * @param adminId 账号id
     * @return 返回删除结果
     */
    private Boolean deleteTreeNodeAccountByStructureId(String structureId, String adminId) {
        Boolean result = Boolean.FALSE;
        // 查询tree_node数据
        TreeNode treeNode = treeNodeMapper.selectById(structureId);
        if(treeNode != null){
            JSONArray newJsonArray = new JSONArray();

            String children = treeNode.getChildren();
            JSONArray jsonArray = (JSONArray) JSONArray.parse(children);
            if(jsonArray != null && !jsonArray.isEmpty()){
                for(Object obj : jsonArray){
                    JSONObject jsonObject = (JSONObject) obj;
                    String id = jsonObject.getString(Constants.ID);
                    if(!id.equals(adminId)){
                        newJsonArray.add(jsonObject);
                    }
                }
            }

            if(jsonArray != null && !jsonArray.isEmpty()){
                treeNode.setChildren(newJsonArray.toString());
                int i = treeNodeMapper.updateById(treeNode);
                if(i > 0){
                    result = Boolean.TRUE;
                }
            }
        }
        return result;
    }

    /**
     * 插入账号信息
     * @param adminId 账号id
     * @param realName 真实姓名
     * @param dyjCode 大赢家code
     * @param userName 账号id
     * @param mobile 手机号
     * @param departmentName 用户所属部门名称
     * @return 返回插入结果
     */
    private Boolean insertAccountData(String adminId, String realName, String dyjCode, String userName, String mobile, String departmentName) {
        Boolean result = Boolean.FALSE;

        // 判断账号是否存在
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.ID, adminId);
        queryWrapper.eq(AccountConstants.USERNAME_FIELD, userName);
        queryWrapper.eq(AccountConstants.DYJ_CODE_FIELD, dyjCode);
        Integer selectCount = accountMapper.selectCount(queryWrapper);
        if(selectCount == 0){
            Account account = new Account();
            account.setId(adminId);
            account.setRealName(realName);
            account.setUsername(userName);
            account.setDyjCode(dyjCode);

            // 处理拼音及汉语拼音首字母字段数据
            if(!Strings.isNullOrEmpty(realName)){
                String pinyin = HanziToPinyinUtils.getPinyin(realName);
                account.setPinYin(pinyin);
                account.setCapitalInitial(pinyin.substring(0, 1).toUpperCase());
            }

            // 用户所属部门
            if(!Strings.isNullOrEmpty(departmentName)){
                account.setDepartmentName(departmentName);
            }

            account.setMobile(mobile);
            account.setErp9Login(0);
            account.setXlLogin(0);
            int insert = accountMapper.insert(account);
            if(insert > 0){
                result = Boolean.TRUE;
                log.info("插入账号数据成功,用户id:[{}],用户名:[{}]", adminId, userName);
            }
        }else{
            log.info("账号数据已存在,用户id:[{}],用户名:[{}]", adminId, userName);
            result = Boolean.TRUE;
        }
        log.info("插入账号信息结束");
        return result;
    }

    /**
     * 更新机构树表数据
     * @param groupId 群组id
     * @param accountId 账号id
     * @param realName 真实姓名
     */
    private void updateTreeNodeData(String groupId, String accountId, String realName, String groupName) {
        if(Strings.isNullOrEmpty(groupId) || Strings.isNullOrEmpty(accountId) || Strings.isNullOrEmpty(realName) || Strings.isNullOrEmpty(groupName)){
            log.info("更新机构树表数据时,groupId、accountId、realName和groupName不能为空,当前数据不完整,跳过处理");
            return;
        }

        // 查询组织结构表数据是否存在
        TreeNode treeNode = treeNodeMapper.selectById(groupId);
        if(treeNode == null){
            // 插入组织结构表新数据
            treeNode = new TreeNode();
            treeNode.setId(groupId);
            if(!Strings.isNullOrEmpty(groupName)){
                treeNode.setLabel(groupName);
            }
            treeNode.setType(TreeNodeType.ORGANIZATION);
            treeNode.setChildren(createTreeNodeChildrenString(accountId, realName));
            int insert = treeNodeMapper.insert(treeNode);
            if(insert > 0){
                log.info("插入组织机构表数据成功,组织机构id:[{}]", groupId);
            }
        }else{
            String children = treeNode.getChildren();
            if(TreeNodeConstants.BRACKETS.equals(children)){
                treeNode.setChildren(createTreeNodeChildrenString(accountId, realName));
                int update = treeNodeMapper.updateById(treeNode);
                if(update > 0){
                    log.info("更新组织机构表数据成功,组织机构id:[{}]", groupId);
                }
            }else{
                List<TreeNode> treeNodeList = JSON.parseArray(children, TreeNode.class);
                if(treeNodeList != null && treeNodeList.size() > 0){
                    // 判断用户数据是否已存在
                    boolean ifExist = Boolean.FALSE;
                    for(TreeNode treeNode1 : treeNodeList){
                       if(treeNode1.getId().equals(accountId)){
                           ifExist = Boolean.TRUE;
                           break;
                       }
                    }

                    if(!ifExist){
                        TreeNode childTreeNode = new TreeNode();
                        childTreeNode.setId(accountId);
                        childTreeNode.setLabel(realName);
                        childTreeNode.setType(TreeNodeType.STAFF);
                        treeNodeList.add(childTreeNode);

                        treeNode.setChildren(JSONObject.toJSONString(treeNodeList));
                        int update = treeNodeMapper.updateById(treeNode);
                        if(update > 0){
                            log.info("更新组织机构表数据成功,组织机构id:[{}]", groupId);
                        }
                    }else{
                        log.info("组织机构表数据已包含用户信息,组织机构id:[{}],用户id:[{}],用户名:[{}]", groupId, accountId, realName);
                    }
                }
            }
        }

        log.info("更新机构树表数据结束");
    }

    /**
     * 生成机构树子数据字符串
     * @param adminId 账号id
     * @param realName 真实姓名
     * @return 返回结果
     */
    private String createTreeNodeChildrenString(String adminId, String realName){
        List<TreeNode> childList = new ArrayList<>();
        TreeNode childTreeNode = new TreeNode();
        childTreeNode.setId(adminId);
        childTreeNode.setLabel(realName);
        childTreeNode.setType(TreeNodeType.STAFF);
        childList.add(childTreeNode);
        return JSONObject.toJSONString(childList);
    }

    /**
     * 插入与其他人的聊天对数据
     * @param userName 用户名
     */
    private void insertChatPairData(String userName) {
        // 查询当前聊天对code最大值
        Integer code = chatPairMapper.getChatPairMaxCode();

        // 查询不包含自己的其它账号,插入聊天对数据
        List<String> accountList = accountMapper.selectAccountListNotHaveSelf(userName);
        if(accountList != null && accountList.size() > 0){
            List<ChatPair> list = new ArrayList<>();
            for(String account : accountList){
                // 判断聊天对是否存在
                QueryWrapper<ChatPair> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq(Constants.FROM_USER_ACCOUNT_FIELD, userName);
                queryWrapper.eq(Constants.TO_USER_ACCOUNT_FIELD, account);
                Integer selectCount = chatPairMapper.selectCount(queryWrapper);
                if(selectCount == 0){
                    code++;
                    ChatPair chatPairOne = new ChatPair();
                    chatPairOne.setFromUserAccount(userName);
                    chatPairOne.setToUserAccount(account);
                    chatPairOne.setCode(code);
                    list.add(chatPairOne);

                    queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq(Constants.FROM_USER_ACCOUNT_FIELD, account);
                    queryWrapper.eq(Constants.TO_USER_ACCOUNT_FIELD, userName);
                    Integer count = chatPairMapper.selectCount(queryWrapper);
                    if(count == 0){
                        ChatPair chatPairTwo = new ChatPair();
                        chatPairTwo.setFromUserAccount(account);
                        chatPairTwo.setToUserAccount(userName);
                        chatPairTwo.setCode(code);
                        list.add(chatPairTwo);
                    }
                }
            }

            if(list.size() > 0){
                chatPairMapper.insertBatchSomeColumn(list);
            }
        }
        log.info("插入与其他人的聊天对数据结束");
    }

    /**
     * 插入账号和群组的关系表数据
     * @param groupId 组织架构id
     * @param accountId 账号id
     * @param userName 用户名
     */
    private void insertRefGroupAccountData(String groupId, String accountId, String userName) {
        if(Strings.isNullOrEmpty(groupId) || Strings.isNullOrEmpty(accountId) || Strings.isNullOrEmpty(userName)){
            log.info("插入账号和群组的关系表数据时,groupId、accountId、userName不能为空,当前数据不完整,跳过处理");
            return;
        }

        // 判断账号和群组的关系表数据是否存在
        QueryWrapper<RefGroupAccount> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.ACCOUNT_ID_FIELD, accountId);
        queryWrapper.eq(Constants.GROUP_ID_FIELD, groupId);
        queryWrapper.eq(Constants.USER_NAME, userName);
        Integer selectCount = refGroupAccountMapper.selectCount(queryWrapper);
        if(selectCount == 0){
            RefGroupAccount refGroupAccount = new RefGroupAccount();
            refGroupAccount.setAccountId(accountId);
            refGroupAccount.setGroupId(groupId);
            refGroupAccount.setUsername(userName);
            int insert = refGroupAccountMapper.insert(refGroupAccount);
            if(insert > 0){
                log.info("插入账号和群组的关系表数据成功,id:[{}]", refGroupAccount.getId());
            }
        }else{
            log.info("账号和群组的关系表数据已存在,默认群组id:[{}],账号id:[{}],用户名:[{}]", groupId, accountId, userName);
        }

        log.info("插入账号和群组的关系表数据结束");
    }

    /**
     * 插入账号对默认群组的权限表数据
     * @param groupId 群组id
     * @param accountId 账号id
     * @param userName 用户名
     */
    private void insertRefGroupAccountPrivilegeData(String groupId, String accountId, String userName) {
        if(Strings.isNullOrEmpty(groupId) || Strings.isNullOrEmpty(accountId) || Strings.isNullOrEmpty(userName)){
            log.info("插入账号对默认群组的权限表数据时,groupId、accountId、userName不能为空,当前数据不完整,跳过处理");
            return;
        }

        // 判断账号对默认群组的权限表数据是否存在
        QueryWrapper<RefGroupAccountPrivilege> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.ACCOUNT_ID_FIELD, accountId);
        queryWrapper.eq(Constants.GROUP_ID_FIELD, groupId);
        queryWrapper.eq(Constants.USER_NAME, userName);
        Integer selectCount = refGroupAccountPrivilegeMapper.selectCount(queryWrapper);
        if(selectCount == 0){
            RefGroupAccountPrivilege refGroupAccountPrivilege = new RefGroupAccountPrivilege();
            refGroupAccountPrivilege.setAccountId(accountId);
            refGroupAccountPrivilege.setGroupId(groupId);
            refGroupAccountPrivilege.setUsername(userName);
            int insert = refGroupAccountPrivilegeMapper.insert(refGroupAccountPrivilege);
            if(insert > 0){
                log.info("插入账号对默认群组的权限表数据成功,id:[{}]", refGroupAccountPrivilege.getId());
            }
        }else{
            log.info("账号对默认群组的权限表数据已存在,默认群组id:[{}],账号id:[{}],用户名:[{}]", groupId, accountId, userName);
        }

        log.info("插入账号对默认群组的权限表数据结束");
    }

    /**
     * 插入默认群组操作表数据
     * @param groupId 群组id
     * @param accountId 账号id
     * @param userName 用户名
     */
    private void insertRefGroupAccountOperationData(String groupId, String accountId, String userName) {
        if(Strings.isNullOrEmpty(groupId) || Strings.isNullOrEmpty(accountId) || Strings.isNullOrEmpty(userName)){
            log.info("插入默认群组操作表数据时,groupId、accountId、userName不能为空,当前数据不完整,跳过处理");
            return;
        }

        // 查询账号和群组的操作表数据条数
        Integer count = getRefGroupAccountOperationCount(groupId, accountId, userName);
        if(count == 0){
            // 插入新数据
            RefGroupAccountOperation refGroupAccountOperation = new RefGroupAccountOperation();
            refGroupAccountOperation.setGroupId(groupId);
            refGroupAccountOperation.setAccountId(accountId);
            refGroupAccountOperation.setUsername(userName);
            refGroupAccountOperation.setNoReminder(0);
            refGroupAccountOperation.setLastContactTime(new Date(System.currentTimeMillis()));
            Integer confirmCount = getRefGroupAccountOperationCount(groupId, accountId, userName);
            if(confirmCount == 0){
                int insert = refGroupAccountOperationMapper.insert(refGroupAccountOperation);
                if(insert > 0){
                    log.info("插入默认群组操作表数据成功");
                }
            }
        }else{
            log.info("账号和群组的操作表数据已存在,默认群组id:[{}],账号id:[{}],用户名:[{}]", groupId, accountId, userName);
        }

        log.info("插入默认群组操作表数据结束");
    }

    /**
     * 查询账号和群组的操作表数据条数
     * @param groupId 默认群组id
     * @param accountId 账号id
     * @param userName 用户名
     * @return 返回账号和群组的操作表数据条数
     */
    private Integer getRefGroupAccountOperationCount(String groupId, String accountId, String userName){
        QueryWrapper<RefGroupAccountOperation> param = new QueryWrapper<>();
        param.eq(Constants.GROUP_ID_FIELD, groupId);
        param.eq(Constants.ACCOUNT_ID_FIELD, accountId);
        param.eq(Constants.USER_NAME, userName);
        return refGroupAccountOperationMapper.selectCount(param);
    }

    /**
     * 删除群组关联关系
     * @param groupId 群组id
     * @param adminId 账号id
     */
    private Boolean deleteGroupRelationship(String groupId, String adminId){
        // 从tree_node表中删除当前账号
        Boolean result = deleteTreeNodeAccountByStructureId(groupId, adminId);
        if(result){
            log.info("从tree_node表中删除当前账号成功,组织机构id:[{}],账号id:[{}]", groupId, adminId);
        }

        // 删除账号和群组的关系数据
        UpdateWrapper<RefGroupAccount> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.GROUP_ID_FIELD, groupId);
        updateWrapper.eq(Constants.ACCOUNT_ID_FIELD, adminId);
        int delete = refGroupAccountMapper.delete(updateWrapper);
        if(delete > 0){
            log.info("删除账号和群组的关系数据成功,群组id:[{}],账号id:[{}]", groupId, adminId);
        }

        // 删除账号和群组的操作表数据
        UpdateWrapper<RefGroupAccountOperation> wrapper = new UpdateWrapper<>();
        wrapper.eq(Constants.GROUP_ID_FIELD, groupId);
        wrapper.eq(Constants.ACCOUNT_ID_FIELD, adminId);
        int delete1 = refGroupAccountOperationMapper.delete(wrapper);
        if(delete1 > 0){
            log.info("删除账号和群组的操作表数据成功,群组id:[{}],账号id:[{}]", groupId, adminId);
        }

        // 删除账号对默认群组的权限表数据
        UpdateWrapper<RefGroupAccountPrivilege> param = new UpdateWrapper<>();
        param.eq(Constants.GROUP_ID_FIELD, groupId);
        param.eq(Constants.ACCOUNT_ID_FIELD, adminId);
        int delete2 = refGroupAccountPrivilegeMapper.delete(param);
        if(delete2 > 0){
            log.info("删除账号对默认群组的权限表数据成功,群组id:[{}],账号id:[{}]", groupId, adminId);
        }

        // 更新默认群组人数数据
        int i = groupMapper.updateMembershipByGroupId(groupId);
        if(i > 0){
            log.info("更新默认群组人数数据成功,群组id:[{}]", groupId);
        }
        return true;
    }

    /**
     * 删除自定义群组关联关系
     * @param adminId 账号id
     */
    public Boolean deleteUserGroupRelationship(String adminId){
        // 删除账号和群组的关系数据
        UpdateWrapper<RefUserGroupAccount> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.ACCOUNT_ID_FIELD, adminId);
        int delete = refUserGroupAccountMapper.delete(updateWrapper);
        if(delete > 0){
            log.info("删除账号和自定义群组的关系数据成功,账号id:[{}]", adminId);
        }
        // 更新自定义群组人数数据
        boolean i = userGroupMapper.updateMembershipSelectRefUserGroupAccount();
        if(i){
            log.info("更新自定义群组人数数据成功");
        }
        return true;
    }

    /**
     * 添加群组关联关系
     * @param groupId 群组id
     * @param accountId 账号id
     * @param userName 用户名
     * @param realName 真实姓名
     * @param groupName 群组名称
     */
    private void addGroupRelationship(String groupId, String accountId, String userName, String realName, String groupName) {
        // 插入账号和群组的关系表数据
        insertRefGroupAccountData(groupId, accountId, userName);

        // 插入账号对默认群组的权限表数据
        insertRefGroupAccountPrivilegeData(groupId, accountId, userName);

        // 插入默认群组操作表数据
        insertRefGroupAccountOperationData(groupId, accountId, userName);

        // 更新机构树表数据
        updateTreeNodeData(groupId, accountId, realName, groupName);

        // 更新默认群组人数数据
        if(!Strings.isNullOrEmpty(groupId)){
            int i = groupMapper.updateMembershipByGroupId(groupId);
            if(i > 0){
                log.info("更新默认群组人数数据成功,群组id:[{}]", groupId);
            }
        }
    }

    /**
     * 给account数据补充汉语拼音及汉语拼音首字母
     */
    private void addAccountDataPinYinCapitalInitial(Account account) {
        // 获取用户名的拼音信息
        String pinyin = HanziToPinyinUtils.getPinyin(account.getRealName());

        if(!StringUtils.isEmpty(pinyin)){
            // 补充拼音及首字母信息
            account.setPinYin(pinyin);
            account.setCapitalInitial(pinyin.substring(0, 1).toUpperCase());
        }
    }
}