package com.innovation.ic.im.end.base.service.erp9_sqlserver;

import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Admins;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountAddPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountResultByStaffIdNotNullPojo;
import java.util.List;

/**
 * Erp9Admins的服务类接口
 */
public interface Erp9AdminsService {

    /**
     * 批量插入Erp9Admins对象
     * @param erp9AdminsList
     * @return
     */
    ServiceResult<Boolean> saveErp9AdminsList(List<Erp9Admins> erp9AdminsList);

    /**
     * 根据部门id（structuresId），查找账号列表
     * @param structuresId
     * @return
     */
    ServiceResult<List<Erp9Admins>> findByStructuresId(String structuresId);

    /**
     * 查找staffId为空的数据
     * @return
     */
    ServiceResult<List<Erp9Admins>> findByStaffIdIsNull();

    /**
     * 从erp9系统的数据库中获取Account对象列表，不包括部门
     * @return
     */
    ServiceResult<List<Account>> getAccountWithoutDepartment();

    /**
     * 从erp9系统的数据库中获取Account对象列表，包括部门
     * @return
     */
    ServiceResult<List<Account>> getAccountWithDepartment();

    /**
     * 根据staffId，查找Erp9Admins对象列表
     * @param staffId
     * @return
     */
    ServiceResult<List<Erp9Admins>> findByStaffId(String staffId);

    /**
     * 查询员工ID不为空的所有数据
     */
    ServiceResult<List<AccountResultByStaffIdNotNullPojo>> getAccountByStaffIdIsNotNull();

    /**
     * 根据账号id查询所属部门
     * @param adminId 账号id
     * @return 返回所属部门名称
     */
    ServiceResult<String> getDepartmentNameByAdminId(String adminId);

    /**
     * 通过账号id集合批量获取账号信息
     * @param list 账号id集合
     * @return 返回查询结果
     */
    ServiceResult<List<AccountAddPojo>> getAccountInfosByAdminIdList(List<String> list);
}