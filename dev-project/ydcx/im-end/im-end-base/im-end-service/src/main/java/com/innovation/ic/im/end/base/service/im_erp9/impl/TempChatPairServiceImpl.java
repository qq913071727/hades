package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.TempChatPairMapper;
import com.innovation.ic.im.end.base.model.im_erp9.TempChatPair;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.service.im_erp9.TempChatPairService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * TempChatPair的具体实现类
 */
@Service
@Transactional
public class TempChatPairServiceImpl extends ServiceImpl<TempChatPairMapper, TempChatPair> implements TempChatPairService {
    private static final Logger log = LoggerFactory.getLogger(TempChatPairServiceImpl.class);

    @Resource
    private TempChatPairMapper tempChatPairMapper;

    /**
     * 根据发送消息账号、接收消息账号查询聊天对数据
     * @param fromUserAccount 发送消息账号
     * @param toUserAccount 接收消息账号
     * @return 返回聊天对数据
     */
    @Override
    public TempChatPair getTempChatPairData(String fromUserAccount, String toUserAccount) {
        QueryWrapper<TempChatPair> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.FROM_USER_ACCOUNT_FIELD, fromUserAccount);
        queryWrapper.eq(Constants.TO_USER_ACCOUNT_FIELD, toUserAccount);
        List<TempChatPair> list = tempChatPairMapper.selectList(queryWrapper);
        if(list != null && list.size() > 0){
            return list.get(0);
        }
        return null;
    }

    /**
     * 清空表数据
     */
    @Override
    public void truncateTable() {
        tempChatPairMapper.truncateTable();
    }
}