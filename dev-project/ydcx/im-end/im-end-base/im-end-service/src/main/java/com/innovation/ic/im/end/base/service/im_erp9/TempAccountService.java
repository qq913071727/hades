package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.pojo.ServiceResult;

/**
 * @desc   TempAccount的服务类接口
 * @author linuo
 * @time   2022年7月5日16:28:27
 */
public interface TempAccountService {

    /**
     * 删除temp_account表中的所有数据
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 备份account表中登录状态和最近登录时间不为空的数据
     */
    ServiceResult<Boolean> backupAccountLoginAndTimeIsNotNull();

    /**
     * 备份account表
     */
    ServiceResult<Boolean> backupAccount();

    /**
     * 将备份的TempAccount数据导入回原account表
     * @return 返回导入结果
     */
    ServiceResult<Boolean> insertTempAccountToAccount();
}