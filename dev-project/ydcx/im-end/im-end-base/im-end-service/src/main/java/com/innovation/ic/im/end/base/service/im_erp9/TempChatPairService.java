package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.TempChatPair;

/**
 * TempChatPair的服务类接口
 */
public interface TempChatPairService {
    /**
     * 根据发送消息账号、接收消息账号查询聊天对数据
     * @param fromUserAccount 发送消息账号
     * @param toUserAccount 接收消息账号
     * @return 返回聊天对数据
     */
    TempChatPair getTempChatPairData(String fromUserAccount, String toUserAccount);

    /**
     * 清空表数据
     */
    void truncateTable();
}