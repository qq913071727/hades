package com.innovation.ic.im.end.base.service.im_erp9;

public interface UserLoginLogService {

    /**
     * 保存点对点用户登录数据
     * @param username
     * @param type
     * @return
     */
    void saveUserData(String username,Integer type, String ipAddr);

    /**
     * 保存默认用户登录数据
     * @param username
     * @param groupId
     * @param type
     * @return
     */
    void saveDefaultUserData(String username, String groupId ,Integer type, String ipAddr);

    /**
     * 保存自定义用户登录数据
     * @param username
     * @param userGroupId
     * @return
     */
    void saveCustomUserData(String username, String userGroupId,Integer type, String ipAddr);
}
