package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.vo.im_erp9.CustomerServiceScoreAddVo;

/**
 * @desc   客服评分表service类
 * @author linuo
 * @time   2023年4月20日16:56:17
 */
public interface CustomerServiceScoreService {
    /**
     * 新增客服评分
     * @param customerServiceScoreAddVo 新增客服评分的vo类
     * @return 返回评分结果
     */
    ServiceResult<Boolean> addScore(CustomerServiceScoreAddVo customerServiceScoreAddVo);
}