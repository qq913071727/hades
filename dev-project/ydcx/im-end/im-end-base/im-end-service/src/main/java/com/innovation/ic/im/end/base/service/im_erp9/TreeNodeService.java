package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.TreeNode;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.TreeNodePojo;
import java.util.List;

/**
 * TreeNode的服务类接口
 */
public interface TreeNodeService {

    /**
     * 保存TreeNode对象列表
     * @param treeNodeList
     */
    ServiceResult<Boolean> saveTreeNodeList(List<TreeNode> treeNodeList);

    /**
     * 删除表tree_node中的数据
     * @return
     */
    ServiceResult<Boolean> truncateTable();

    /**
     * 根据id获取TreeNode对象
     * @param id 主键
     * @param accountId 账号id
     * @param username 账号
     * @return 返回TreeNode对象
     */
    ServiceResult<TreeNodePojo> findById(String id, String accountId, String username);
}