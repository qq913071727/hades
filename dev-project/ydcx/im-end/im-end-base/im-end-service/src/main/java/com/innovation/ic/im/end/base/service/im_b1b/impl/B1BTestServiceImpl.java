package com.innovation.ic.im.end.base.service.im_b1b.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_b1b.B1bTestMapper;
import com.innovation.ic.im.end.base.model.im_b1b.B1bTest;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_b1b.B1bTestPojo;
import com.innovation.ic.im.end.base.service.im_b1b.B1bTestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * B1bAdmins的具体实现类
 */
@Service
@Transactional
public class B1BTestServiceImpl extends ServiceImpl<B1bTestMapper, B1bTest> implements B1bTestService {

    @Resource
    private B1bTestMapper b1BTestMapper;

}
