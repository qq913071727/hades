package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.handler.im_erp9.ModelHandler;
import com.innovation.ic.im.end.base.mapper.im_erp9.AccountMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.RefGroupAccountPrivilegeMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.TreeNodeMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.model.im_erp9.TreeNode;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.UserRoleType;
import com.innovation.ic.im.end.base.pojo.im_erp9.TreeNodePojo;
import com.innovation.ic.im.end.base.service.im_erp9.TreeNodeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * TreeNode的具体实现类
 */
@Service
@Transactional
public class TreeNodeServiceImpl extends ServiceImpl<TreeNodeMapper, TreeNode> implements TreeNodeService {
    @Resource
    private TreeNodeMapper treeNodeMapper;

    @Resource
    private AccountMapper accountMapper;

    @Resource
    private RefGroupAccountPrivilegeMapper refGroupAccountPrivilegeMapper;

    @Resource
    private ModelHandler modelHandler;

    /**
     * 保存TreeNode对象列表
     * @param treeNodeList
     * @return
     */
    @Override
    public ServiceResult<Boolean> saveTreeNodeList(List<TreeNode> treeNodeList) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        baseMapper.insertBatchSomeColumn(treeNodeList);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 删除表tree_node中的数据
     * @return
     */
    @Override
    public ServiceResult<Boolean> truncateTable() {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        treeNodeMapper.truncateTable();

        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据id获取TreeNode对象
     * @param id 主键
     * @param accountId 账号id
     * @param username 账号
     * @return 返回TreeNode对象
     */
    @Override
    public ServiceResult<TreeNodePojo> findById(String id, String accountId, String username) {
        ServiceResult<TreeNodePojo> serviceResult = new ServiceResult<TreeNodePojo>();
        TreeNode treeNode = baseMapper.selectById(id);

        // 根据账号id、账号查询有权限发送消息的群组id集合
        List<String> sendMsgPermissionGroupIds = refGroupAccountPrivilegeMapper.selectGroupIdByAccountAndUsername(accountId, username);

        // 判断用户是否是管理员
        Boolean isAdmin = judgeAccountIsAdmin(accountId);

        // 处理查询机构结果，处理发送消息权限标记及是否在线标记
        TreeNodePojo treeNodePojo = modelHandler.toTreeNodePojo(treeNode, sendMsgPermissionGroupIds, isAdmin);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(treeNodePojo);
        return serviceResult;
    }

    /**
     * 判断用户是否是管理员
     * @param accountId 账号id
     * @return 返回结果
     */
    private Boolean judgeAccountIsAdmin(String accountId) {
        Boolean result = Boolean.FALSE;
        Account account = accountMapper.selectById(accountId);
        if(account != null &&  account.getRole() != null && account.getRole().intValue() == UserRoleType.ADMIN){
            result = Boolean.TRUE;
        }
        return result;
    }
}
