package com.innovation.ic.im.end.base.service.erp9_sqlserver.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.erp9_sqlserver.Erp9MapsStructureMapper;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9MapsStructure;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9MapsStructureService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Erp9MapsStructureService的具体实现类
 */
@Service
@Transactional
public class Erp9MapsStructureServiceImpl extends ServiceImpl<Erp9MapsStructureMapper, Erp9MapsStructure> implements Erp9MapsStructureService {

    @Resource
    private Erp9MapsStructureMapper erp9MapsStructuresMapper;

    /**
     * 根据structureId查找Erp9MapsStructure对象列表
     * @param structureId
     * @return
     */
    @Override
    public ServiceResult<List<Erp9MapsStructure>> findByStructureID(String structureId) {
        ServiceResult<List<Erp9MapsStructure>> serviceResult = new ServiceResult<List<Erp9MapsStructure>>();

        Erp9MapsStructure erp9MapsStructure = new Erp9MapsStructure();
        erp9MapsStructure.setStructureID(structureId);
        QueryWrapper<Erp9MapsStructure> queryWrapper = new QueryWrapper<>(erp9MapsStructure);
        List<Erp9MapsStructure> erp9MapsStructureList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9MapsStructureList);

        return serviceResult;
    }
}
