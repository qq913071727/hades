package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.ClientMapper;
import com.innovation.ic.im.end.base.model.im_erp9.Client;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.im_erp9.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Client的具体实现类
 */
@Service
@Transactional
public class ClientServiceImpl extends ServiceImpl<ClientMapper, Client> implements ClientService {

    @Autowired
    private ClientMapper clientMapper;

    /**
     * 查找client表中所有数据
     * @return
     */
    @Override
    public ServiceResult<List<Client>> findAll() {
        ServiceResult<List<Client>> serviceResult = new ServiceResult<List<Client>>();
        List<Client> clientList = clientMapper.selectList(null);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(clientList);
        return serviceResult;
    }

    /**
     * 根据id获取客户端类型
     * @param id 主键
     * @return 返回客户端类型
     */
    @Override
    public ServiceResult<Integer> getTypeById(String id) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();

        // 根据id获取客户端类型
        Integer type = clientMapper.getTypeById(id);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(type);
        return serviceResult;
    }

    /**
     * 获取登录类型
     * @return 返回登录类型
     */
    @Override
    public ServiceResult<List<Integer>> getLoginTypeList() {
        ServiceResult<List<Integer>> serviceResult = new ServiceResult<>();

        List<Integer> result = clientMapper.getLoginTypeList();

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }
}
