package com.innovation.ic.im.end.base.service.erp9_sqlserver.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.erp9_sqlserver.Erp9StructuresMapper;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Structures;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9StructuresService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * Erp9StructuresService的具体实现类
 */
@Service
@Transactional
public class Erp9StructuresServiceImpl extends ServiceImpl<Erp9StructuresMapper, Erp9Structures> implements Erp9StructuresService {
    @Resource
    private Erp9StructuresMapper erp9StructuresMapper;

    /**
     * 根据id查询erp中的组织机构信息
     * @param structureId 组织机构id
     * @return 返回组织机构信息
     */
    @Override
    public ServiceResult<Erp9Structures> selectErp9StructuresById(String structureId) {
        Erp9Structures erp9Structures = erp9StructuresMapper.selectById(structureId);
        ServiceResult<Erp9Structures> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(erp9Structures);
        return serviceResult;
    }

    /**
     * 批量插入Erp9Structures对象
     * @param erp9StructuresList
     * @return
     */
    @Override
    public ServiceResult<Boolean> saveErp9StructuresList(List<Erp9Structures> erp9StructuresList) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        baseMapper.insertBatchSomeColumn(erp9StructuresList);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 从erp9项目的数据库中查找所有的Structures表的数据
     * @return
     */
    @Override
    public ServiceResult<List<Erp9Structures>> findAll() {
        ServiceResult<List<Erp9Structures>> serviceResult = new ServiceResult<List<Erp9Structures>>();
        List<Erp9Structures> erp9StructuresList = baseMapper.selectList(null);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9StructuresList);
        return serviceResult;
    }

    /**
     * 根据status字段，从erp9项目的数据库中查找Structures表的数据
     * @param status
     * @return
     */
    @Override
    public ServiceResult<List<Erp9Structures>> findByStatus(Integer status) {
        ServiceResult<List<Erp9Structures>> serviceResult = new ServiceResult<List<Erp9Structures>>();
        QueryWrapper<Erp9Structures> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("Status", status);
        List<Erp9Structures> erp9StructuresList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9StructuresList);
        return serviceResult;
    }

    /**
     * 根据fatherId，查找Erp9Structures对象列表
     * @param fatherId
     * @return
     */
    @Override
    public ServiceResult<List<Erp9Structures>> findByFatherId(String fatherId, Integer status) {
        ServiceResult<List<Erp9Structures>> serviceResult = new ServiceResult<List<Erp9Structures>>();
        QueryWrapper<Erp9Structures> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("FatherID", fatherId);
        queryWrapper.eq("Status", status);
        List<Erp9Structures> erp9StructuresList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9StructuresList);
        return serviceResult;
    }
}