package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.GroupMessageReceiver;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import java.util.List;

/**
 * GroupOfflineMessage的服务类接口
 */
public interface GroupMessageReceiverService {
    /**
     * 保存GroupMessageReceivar对象
     * @param groupMessageReceiver 群组未读消息
     * @return 返回结果
     */
    ServiceResult<Boolean> saveGroupMessageReceiver(GroupMessageReceiver groupMessageReceiver);

    /**
     * 根据groupId和toUserAccount查找GroupOfflineMessage对象列表，并且按照创建时间升序排列
     * @param groupId
     * @param toUserAccount
     * @return
     */
    ServiceResult<List<GroupMessageReceiver>> findByGroupIdAndToUserAccountOrderByCreateTimeAsc(String groupId, String toUserAccount);

    /**
     * 删除GroupOfflineMessage对象
     * @param id
     * @return
     */
    ServiceResult<Boolean> deleteGroupOfflineMessage(Integer id);

    /**
     * 查询所有记录，按照创建时间降序排列
     * @return
     */
    ServiceResult<List<GroupMessageReceiver>> findAllOrderByCreateTimeDesc();

    /**
     * 默认群组，撤回消息
     * @param groupRetractionVo 撤回消息的vo类
     */
    RetractionPojo retractionGroupMessageReceiver(GroupRetractionVo groupRetractionVo);

    /**
     * 通过组id获取对应人信息
     * @param groupId 组id
     */
    List<String> findUsername(String groupId, String fromUserAccount);

    /**
     * 通过groupMessageId与toUserAccount 修改消息状态为已读
     */
    ServiceResult<Integer> restoreGroupMessageReceiver(String groupId, String toUserAccount);
}
