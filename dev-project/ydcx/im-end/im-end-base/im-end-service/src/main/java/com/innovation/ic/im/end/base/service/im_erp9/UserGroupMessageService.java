package com.innovation.ic.im.end.base.service.im_erp9;

import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupMessageVo;
import java.util.List;

/**
 * UserGroupMessage的服务类接口
 */
public interface UserGroupMessageService {

    /**
     * 保存UserGroupMessage对象
     *
     * @param userGroupMessage
     */
    ServiceResult<Integer> saveUserGroupMessage(UserGroupMessage userGroupMessage);

    /**
     * 根据参数type和content（模糊查询），获取用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @return
     */
    ServiceResult<List<UserGroupMessage>> redisLikePage(UserGroupMessageVo userGroupMessageVo, Integer timeout);

    /**
     * 根据参数type，获取某个用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @param userGroupId
     * @param pageSize
     * @param pageNo
     * @param type
     * @return
     */
    ServiceResult<List<UserGroupMessage>> redisPage(Integer userGroupId, String fromUserAccount, Integer pageSize, Integer pageNo, Integer type, Integer timeout);

    /**
     * 根据参数id、initRow和direction，获取默认群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示
     *
     * @param id
     * @param initRow
     * @param direction
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<UserGroupMessage>> redisContextPage(Integer id, String fromUserAccount, Integer initRow, Integer direction, Integer pageSize, Integer pageNo, Integer timeout);

    /**
     * 将user_group_message表中的数据导入redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importUserGroupMessageIntoRedis();
}