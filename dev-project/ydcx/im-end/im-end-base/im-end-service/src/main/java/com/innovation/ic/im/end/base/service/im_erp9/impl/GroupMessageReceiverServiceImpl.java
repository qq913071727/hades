package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.im_erp9.GroupMessageMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.GroupMessageReceiverMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.RefGroupAccountMapper;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessageReceiver;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccount;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.constant.MessageType;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.service.im_erp9.GroupMessageReceiverService;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.*;

/**
 * GroupOfflineMessage的具体实现类
 */
@Service
@Transactional
public class GroupMessageReceiverServiceImpl extends ServiceImpl<GroupMessageReceiverMapper, GroupMessageReceiver> implements GroupMessageReceiverService {
    private static final Logger log = LoggerFactory.getLogger(GroupMessageReceiverServiceImpl.class);

    @Resource
    private GroupMessageReceiverMapper groupMessageReceiverMapper;
    @Resource
    private GroupMessageMapper groupMessageMapper;
    @Resource
    private RefGroupAccountMapper refGroupAccountMapper;

    /**
     * 保存GroupMessageReceiver对象
     * @param groupMessageReceiver 群组未读消息
     * @return 返回结果
     */
    @Override
    public ServiceResult<Boolean> saveGroupMessageReceiver(GroupMessageReceiver groupMessageReceiver) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();

        groupMessageReceiver.setCreateTime(new Date(System.currentTimeMillis()));
        groupMessageReceiverMapper.insert(groupMessageReceiver);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据groupId和toUserAccount查找GroupOfflineMessage对象列表，并且按照创建时间升序排列
     *
     * @param groupId
     * @param toUserAccount
     * @return
     */
    @Override
    public ServiceResult<List<GroupMessageReceiver>> findByGroupIdAndToUserAccountOrderByCreateTimeAsc(String groupId, String toUserAccount) {
        ServiceResult<List<GroupMessageReceiver>> serviceResult = new ServiceResult<List<GroupMessageReceiver>>();

        QueryWrapper<GroupMessageReceiver> wrapper = new QueryWrapper<>();
        wrapper.eq("group_id", groupId);
        wrapper.eq("to_user_account", toUserAccount);
        wrapper.orderByAsc("create_time");
        List<GroupMessageReceiver> groupOfflineMessageList = groupMessageReceiverMapper.selectList(wrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(groupOfflineMessageList);

        return serviceResult;
    }

    /**
     * 删除GroupOfflineMessage对象
     *
     * @param id
     * @return
     */
    @Override
    public ServiceResult<Boolean> deleteGroupOfflineMessage(Integer id) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();

        groupMessageReceiverMapper.deleteById(id);

        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查询所有记录，按照创建时间降序排列
     *
     * @return
     */
    @Override
    public ServiceResult<List<GroupMessageReceiver>> findAllOrderByCreateTimeDesc() {
        ServiceResult<List<GroupMessageReceiver>> serviceResult = new ServiceResult<List<GroupMessageReceiver>>();
        QueryWrapper<GroupMessageReceiver> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");

        List<GroupMessageReceiver> groupOfflineMessageList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(groupOfflineMessageList);
        return serviceResult;
    }

    /**
     * 通过groupMessageId与toUserAccount 修改消息状态为已读
     */
    @Override
    public ServiceResult<Integer> restoreGroupMessageReceiver(String groupId, String toUserAccount) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        // 根据默认群组id和接收消息的账号修改消息状态
        UpdateWrapper<GroupMessageReceiver> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq(Constants.GROUP_ID_FIELD, groupId);
        updateWrapper.eq(Constants.TO_USER_ACCOUNT_FIELD, toUserAccount);
        updateWrapper.eq(Constants.READ,MessageType.UNREAD);
        GroupMessageReceiver groupMessageReceiver = new GroupMessageReceiver();
        groupMessageReceiver.setRead(MessageType.READ);
        groupMessageReceiver.setReadTime(new Date());
        int update = groupMessageReceiverMapper.update(groupMessageReceiver, updateWrapper);
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(update);
        return serviceResult;
    }

    /**
     * 默认群组，撤回消息
     *
     * @param groupRetractionVo 撤回消息的vo类
     */
    @Override
    public RetractionPojo retractionGroupMessageReceiver(GroupRetractionVo groupRetractionVo) {
        RetractionPojo retractionPojo = new RetractionPojo();
        //获取群组id
        String groupMessageId = groupRetractionVo.getGroupMessageId();
        //获取撤回时间
        Date retractionTime = groupRetractionVo.getRetractionTime();
        //修改group_message
        UpdateWrapper<GroupMessage> groupMessageUpdateWrapper = new UpdateWrapper<>();
        groupMessageUpdateWrapper.eq(Constants.ID, groupMessageId);
        GroupMessage groupMessage = new GroupMessage();
        groupMessage.setRetractionTime(retractionTime);
        groupMessageMapper.update(groupMessage, groupMessageUpdateWrapper);
        //获取当前修改的值 返回
        GroupMessage groupMessageData = groupMessageMapper.selectOne(groupMessageUpdateWrapper);
        retractionPojo.setType(2);
        retractionPojo.setGroupMessage(groupMessageData);
        return retractionPojo;
    }

    /**
     * 通过组id获取对应人信息
     *
     * @param groupId 组id
     */
    @Override
    public List<String> findUsername(String groupId, String fromUserAccount) {
        List<RefGroupAccount> refGroupAccountList = refGroupAccountMapper.findUsername(groupId, fromUserAccount);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < refGroupAccountList.size(); i++) {
            RefGroupAccount refGroupAccount = refGroupAccountList.get(i);
            list.add(i, refGroupAccount.getUsername());
        }
        return list;
    }
}