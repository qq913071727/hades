package com.innovation.ic.im.end.base.service.erp9_sqlserver.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.im.end.base.mapper.erp9_sqlserver.Erp9AdminsMapper;
import com.innovation.ic.im.end.base.model.erp9_sqlserver.Erp9Admins;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountAddPojo;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountResultByStaffIdNotNullPojo;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9AdminsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * Erp9Admins的具体实现类
 */
@Service
@Transactional
public class Erp9AdminsServiceImpl extends ServiceImpl<Erp9AdminsMapper, Erp9Admins> implements Erp9AdminsService {

    @Resource
    private Erp9AdminsMapper erp9AdminsMapper;

    /**
     * 批量插入Erp9Admins对象
     * @param erp9AdminsList
     * @return
     */
    @Override
    public ServiceResult<Boolean> saveErp9AdminsList(List<Erp9Admins> erp9AdminsList) {
        ServiceResult<Boolean> serviceResult = new ServiceResult<Boolean>();
        baseMapper.insertBatchSomeColumn(erp9AdminsList);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 根据部门id（structuresId），查找账号列表
     * @return
     */
    @Override
    public ServiceResult<List<Erp9Admins>> findByStructuresId(String structuresId) {
        ServiceResult<List<Erp9Admins>> serviceResult = new ServiceResult<List<Erp9Admins>>();
        List<Erp9Admins> erp9AdminsList = erp9AdminsMapper.findByStructuresId(structuresId);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9AdminsList);
        return serviceResult;
    }

    /**
     * 查找staffId为空的数据
     * @return
     */
    @Override
    public ServiceResult<List<Erp9Admins>> findByStaffIdIsNull() {
        ServiceResult<List<Erp9Admins>> serviceResult = new ServiceResult<List<Erp9Admins>>();
        QueryWrapper<Erp9Admins> erp9AdminsQueryWrapper = new QueryWrapper<Erp9Admins>();
        erp9AdminsQueryWrapper.isNull("StaffID");
        erp9AdminsQueryWrapper.ne(Constants.STATUS, Constants.NOT_NORMAL_STATUS);
        List<Erp9Admins> erp9AdminsList = baseMapper.selectList(erp9AdminsQueryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9AdminsList);
        return serviceResult;
    }

    /**
     * 从erp9系统的数据库中获取Account对象列表，不包括部门
     * @return
     */
    @Override
    public ServiceResult<List<Account>> getAccountWithoutDepartment() {
        ServiceResult<List<Account>> serviceResult = new ServiceResult<List<Account>>();
        List<Account> accountList = erp9AdminsMapper.getAccountWithoutDepartment();

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(accountList);
        return serviceResult;
    }

    /**
     * 从erp9系统的数据库中获取Account对象列表，包括部门
     * @return
     */
    @Override
    public ServiceResult<List<Account>> getAccountWithDepartment() {
        ServiceResult<List<Account>> serviceResult = new ServiceResult<List<Account>>();
        List<Account> accountList = erp9AdminsMapper.getAccountWithDepartment();

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(accountList);
        return serviceResult;
    }

    /**
     * 根据staffId，查找Erp9Admins对象列表
     * @param staffId
     * @return
     */
    @Override
    public ServiceResult<List<Erp9Admins>> findByStaffId(String staffId) {
        ServiceResult<List<Erp9Admins>> serviceResult = new ServiceResult<List<Erp9Admins>>();
        Erp9Admins erp9Admins = new Erp9Admins();
        erp9Admins.setStaffId(staffId);
        QueryWrapper<Erp9Admins> queryWrapper = new QueryWrapper<>(erp9Admins);
        queryWrapper.ne(Constants.STATUS, Constants.NOT_NORMAL_STATUS);
        List<Erp9Admins> erp9AdminsList = baseMapper.selectList(queryWrapper);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(erp9AdminsList);
        return serviceResult;
    }

    /**
     * 查询员工ID不为空的所有数据
     * @return 查询结果
     */
    @Override
    public ServiceResult<List<AccountResultByStaffIdNotNullPojo>> getAccountByStaffIdIsNotNull() {
        ServiceResult<List<AccountResultByStaffIdNotNullPojo>> serviceResult = new ServiceResult<>();
        List<AccountResultByStaffIdNotNullPojo> list = erp9AdminsMapper.getAccountByStaffIdIsNotNull();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(list);
        return serviceResult;
    }

    /**
     * 根据账号id查询所属部门
     * @param adminId 账号id
     * @return 返回所属部门名称
     */
    @Override
    public ServiceResult<String> getDepartmentNameByAdminId(String adminId) {
        String departmentName = erp9AdminsMapper.getDepartmentNameByAdminId(adminId);
        ServiceResult<String> stringServiceResult = new ServiceResult<>();
        stringServiceResult.setSuccess(Boolean.TRUE);
        stringServiceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        stringServiceResult.setResult(departmentName);
        return stringServiceResult;
    }

    /**
     * 通过账号id集合批量获取账号信息
     * @param list 账号id集合
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<AccountAddPojo>> getAccountInfosByAdminIdList(List<String> list) {
        List<AccountAddPojo> result = erp9AdminsMapper.getAccountInfosByAdminIdList(list);
        if(result != null && result.size() > 0){
            for(AccountAddPojo accountAddPojo : result){
                accountAddPojo.setDepartmentName(erp9AdminsMapper.getDepartmentNameByAdminId(accountAddPojo.getAdminId()));
            }
        }

        ServiceResult<List<AccountAddPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }
}
