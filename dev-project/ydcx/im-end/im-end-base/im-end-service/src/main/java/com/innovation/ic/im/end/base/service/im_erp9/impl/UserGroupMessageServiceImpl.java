package com.innovation.ic.im.end.base.service.im_erp9.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.handler.im_erp9.ModelHandler;
import com.innovation.ic.im.end.base.mapper.im_erp9.RefUserGroupAccountMapper;
import com.innovation.ic.im.end.base.mapper.im_erp9.UserGroupMessageMapper;
import com.innovation.ic.im.end.base.model.im_erp9.RefUserGroupAccount;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.constant.MessageContext;
import com.innovation.ic.im.end.base.pojo.constant.MessageType;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.innovation.ic.im.end.base.service.im_erp9.UserGroupMessageService;
import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.im.end.base.value.config.PictureParamConfig;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupMessageVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * UserGroupMessage的具体实现类
 */
@Service
@Transactional
public class UserGroupMessageServiceImpl extends ServiceImpl<UserGroupMessageMapper, UserGroupMessage> implements UserGroupMessageService {
    private static final Logger log = LoggerFactory.getLogger(UserGroupMessageServiceImpl.class);

    @Resource
    private PictureParamConfig pictureParamConfig;

    @Resource
    private UserGroupMessageMapper userGroupMessageMapper;

    @Resource
    private RefUserGroupAccountMapper refUserGroupAccountMapper;

    @Resource
    private RedisManager redisManager;

    @Resource
    private ModelHandler modelHandler;

    /**
     * 保存UserGroupMessage类型对象
     *
     * @param userGroupMessage
     * @return
     */
    @Override
    public ServiceResult<Integer> saveUserGroupMessage(UserGroupMessage userGroupMessage) {
        ServiceResult<Integer> serviceResult = new ServiceResult<>();
        baseMapper.insert(userGroupMessage);

        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userGroupMessage.getId());
        return serviceResult;
    }

    /**
     * 根据参数type，获取某个用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @param userGroupId
     * @param pageSize
     * @param pageNo
     * @param type
     * @return
     */
    @Override
    public ServiceResult<List<UserGroupMessage>> redisPage(Integer userGroupId, String fromUserAccount, Integer pageSize, Integer pageNo, Integer type, Integer timeout) {
        ServiceResult<List<UserGroupMessage>> serviceResult = new ServiceResult<>();
        int start = pageSize * (pageNo - 1);
        // 根据自定义群组id和账号id获取自定义群组账号信息
        RefUserGroupAccount refUserGroupAccount = getRefUserGroupAccountByUserGroupIdAccountId(userGroupId, fromUserAccount);
        if (refUserGroupAccount == null) {
            // 获取根据查询条件未获取到用户信息时的返回结果
            serviceResult = returnQueryFaultResult(serviceResult, userGroupId, fromUserAccount);
            return serviceResult;
        }

        // 根据请求参数从redis中获取需要的数据
        Set<String> stringSet = getRedisDataByParam(start, pageSize, userGroupId, refUserGroupAccount.getContentInvisibleTime(), refUserGroupAccount.getJoinGroupTime(), timeout);

        if (stringSet == null || stringSet.size() == 0) {
            List<UserGroupMessage> userGroupMessageList = new ArrayList<>();
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(userGroupMessageList);
            return serviceResult;
        }
        //如果传递的type 是0 直接返回全部数据
        if (type.equals(MessageType.ALL)) {
            List<UserGroupMessage> list = modelHandler.toUserGroupMessageList(stringSet, fromUserAccount);
            for (int i = 0; i < list.size(); i++) {
                UserGroupMessage userGroupMessage = list.get(i);
                if (userGroupMessage.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                    String substring = userGroupMessage.getFilePath().substring(userGroupMessage.getFilePath().lastIndexOf("/") + 1);
                    list.get(i).setFilePath(substring);
                }
            }
            // 处理list中的图片数据
            handListPictureData(list);
            //进行list集合排序（升序）
            list.sort(Comparator.comparing(UserGroupMessage::getCreateTime));
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(list);
            return serviceResult;
        }
        //序列化数据
        List<UserGroupMessage> userGroupMessageList = modelHandler.toUserGroupMessageList(stringSet, fromUserAccount);
        //创建一个空的集合 把属于文件(图片)的数据存入
        List<UserGroupMessage> newUserGroupMessageList = new ArrayList<UserGroupMessage>();
        for (int i = 0; i < userGroupMessageList.size(); i++) {
            Integer userGroupMessageType = userGroupMessageList.get(i).getType();
            //type为文件
            if (type.equals(MessageType.FILE) && userGroupMessageType.equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessageList.get(i).getFilePath())) {
                String substring = userGroupMessageList.get(i).getFilePath().substring(userGroupMessageList.get(i).getFilePath().lastIndexOf("/") + 1);
                userGroupMessageList.get(i).setFilePath(substring);
                newUserGroupMessageList.add(userGroupMessageList.get(i));
            }
            //type为图片
            if (type.equals(MessageType.PICTURE) && userGroupMessageType.equals(MessageType.PICTURE)) {
                UserGroupMessage userGroupMessage = userGroupMessageList.get(i);
                if (!Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                    userGroupMessage.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessage.getFilePath());
                }
                newUserGroupMessageList.add(userGroupMessage);
            }
        }
        //进行list集合排序（升序）
        newUserGroupMessageList.sort(Comparator.comparing(UserGroupMessage::getCreateTime));
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(newUserGroupMessageList);
        return serviceResult;
    }

    /**
     * 根据参数type和content（模糊查询），获取用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示
     *
     * @return
     */
    @Override
    public ServiceResult<List<UserGroupMessage>> redisLikePage(UserGroupMessageVo userGroupMessageVo, Integer timeout) {
        ServiceResult<List<UserGroupMessage>> serviceResult = new ServiceResult<>();
        int start = userGroupMessageVo.getPageSize() * (userGroupMessageVo.getPageNo() - 1);

        // 根据自定义群组id和账号id获取自定义群组账号信息
        RefUserGroupAccount refUserGroupAccount = getRefUserGroupAccountByUserGroupIdAccountId(userGroupMessageVo.getUserGroupId(), userGroupMessageVo.getFromUserAccount());
        if (refUserGroupAccount == null) {
            // 获取根据查询条件未获取到用户信息时的返回结果
            serviceResult = returnQueryFaultResult(serviceResult, userGroupMessageVo.getUserGroupId(), userGroupMessageVo.getFromUserAccount());
            return serviceResult;
        }

        // 根据请求参数从redis中获取需要的数据
        Set<String> stringSet = getRedisDataByParam(start, userGroupMessageVo.getPageSize(), userGroupMessageVo.getUserGroupId(), refUserGroupAccount.getContentInvisibleTime(), refUserGroupAccount.getJoinGroupTime(), timeout);

        if (stringSet == null || stringSet.size() == 0) {
            List<UserGroupMessage> userGroupMessageList = new ArrayList<>();
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(userGroupMessageList);
            return serviceResult;
        }
        //数据序列化
        List<UserGroupMessage> userGroupMessageList = modelHandler.toUserGroupMessageList(stringSet, userGroupMessageVo.getFromUserAccount());
        //创建一个空的集合进行存数据
        List<UserGroupMessage> newUserGroupMessagesList = new ArrayList<>();
        //传递参数是否带内容如果有则模糊查询，没有则不需要
        if (StringUtils.isEmpty(userGroupMessageVo.getContent())) {
            for (int i = 0; i < userGroupMessageList.size(); i++) {
                UserGroupMessage userGroupMessage = userGroupMessageList.get(i);

                // 撤回、系统的消息忽略
                if(userGroupMessage.getRetractionTime() != null || userGroupMessage.getType().equals(MessageType.SYS_MSG) || userGroupMessage.getType().equals(MessageType.UPDATE_USER_GROUP_NAME_SYS_MSG)){
                    continue;
                }

                //根据type不同返回不同的数据
                if (userGroupMessageVo.getType().equals(MessageType.ALL)) {
                    if (userGroupMessage.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                        String substring = userGroupMessage.getFilePath().substring(userGroupMessage.getFilePath().lastIndexOf("/") + 1);
                        userGroupMessage.setFilePath(substring);
                    }
                    if (userGroupMessage.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                        userGroupMessage.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessage.getFilePath());
                    }
                    newUserGroupMessagesList.add(userGroupMessage);
                }
                if (userGroupMessageVo.getType().equals(MessageType.FILE) && userGroupMessage.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                    String substring = userGroupMessage.getFilePath().substring(userGroupMessage.getFilePath().lastIndexOf("/") + 1);
                    userGroupMessage.setFilePath(substring);
                    newUserGroupMessagesList.add(userGroupMessage);
                }
                if (userGroupMessageVo.getType().equals(MessageType.PICTURE) && userGroupMessage.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                    userGroupMessage.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessage.getFilePath());
                    newUserGroupMessagesList.add(userGroupMessage);
                }
            }
            //进行list集合排序（升序）
            newUserGroupMessagesList.sort(Comparator.comparing(UserGroupMessage::getCreateTime));
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(newUserGroupMessagesList);
            return serviceResult;
        } else {
            //模糊不区分大小
            Pattern pattern = Pattern.compile(userGroupMessageVo.getContent(), Pattern.CASE_INSENSITIVE);
            for (int i = 0; i < userGroupMessageList.size(); i++) {
                UserGroupMessage userGroupMessage = userGroupMessageList.get(i);

                // 撤回、系统的消息忽略
                if(userGroupMessage.getRetractionTime() != null || userGroupMessage.getType().equals(MessageType.SYS_MSG) || userGroupMessage.getType().equals(MessageType.UPDATE_USER_GROUP_NAME_SYS_MSG)){
                    continue;
                }

                if (!Strings.isNullOrEmpty(userGroupMessage.getContent()) || !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                    Boolean result = Boolean.FALSE;

                    // 文字匹配聊天内容
                    if (!Strings.isNullOrEmpty(userGroupMessage.getContent())) {
                        Matcher contentMatcherContent = pattern.matcher(userGroupMessage.getContent());
                        result = contentMatcherContent.find();
                    }

                    // 图片、文件匹配文件名
                    if (!Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                        Matcher filepathMatcherContent = pattern.matcher(userGroupMessage.getFilePath());
                        result = filepathMatcherContent.find();
                    }

                    //进行根据参数type和content（模糊查询）
                    if (result) {
                        //根据type不同返回不同的数据
                        if (userGroupMessageVo.getType().equals(MessageType.ALL)) {
                            if (userGroupMessage.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                                String substring = userGroupMessage.getFilePath().substring(userGroupMessage.getFilePath().lastIndexOf("/") + 1);
                                userGroupMessage.setFilePath(substring);
                            }
                            if (userGroupMessage.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                                // 图片路径前要拼接url前缀
                                userGroupMessage.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessage.getFilePath());
                            }
                            newUserGroupMessagesList.add(userGroupMessage);
                            continue;
                        }
                        if (userGroupMessageVo.getType().equals(MessageType.FILE) && userGroupMessage.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                            String substring = userGroupMessage.getFilePath().substring(userGroupMessage.getFilePath().lastIndexOf("/") + 1);
                            userGroupMessage.setFilePath(substring);
                            newUserGroupMessagesList.add(userGroupMessage);
                        }
                    }
                }
            }
            //进行list集合排序（升序）
            newUserGroupMessagesList.sort(Comparator.comparing(UserGroupMessage::getCreateTime));
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(newUserGroupMessagesList);
            return serviceResult;
        }
    }

    /**
     * 根据参数id、initRow和direction，获取默认群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示
     *
     * @param id
     * @param initRow
     * @param direction
     * @param pageSize
     * @param pageNo
     * @return
     */
    @Override
    public ServiceResult<List<UserGroupMessage>> redisContextPage(Integer id, String fromUserAccount, Integer initRow, Integer direction, Integer pageSize, Integer pageNo, Integer timeout) {
        ServiceResult<List<UserGroupMessage>> serviceResult = new ServiceResult<>();
        int start = pageSize * (pageNo - 1);

        // 根据id获取用户自定义群组消息
        UserGroupMessage userGroupMessage = userGroupMessageMapper.selectById(id);

        // 根据自定义群组id和账号id获取自定义群组账号信息
        RefUserGroupAccount refUserGroupAccount = getRefUserGroupAccountByUserGroupIdAccountId(userGroupMessage.getUserGroupId(), fromUserAccount);
        if (refUserGroupAccount == null) {
            List<UserGroupMessage> userGroupMessageList = new ArrayList<>();
            serviceResult.setMessage(ServiceResult.SELECT_FAIL);
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage("根据用户账号:{" + fromUserAccount + "}查询用户数据失败,请重试");
            serviceResult.setResult(userGroupMessageList);
            return serviceResult;
        }

        // 根据请求参数从redis中获取需要的数据
        Set<String> stringSet = getRedisDataByParam(start, pageSize, userGroupMessage.getUserGroupId(), refUserGroupAccount.getContentInvisibleTime(), refUserGroupAccount.getJoinGroupTime(), timeout);

        if (stringSet == null || stringSet.size() == 0) {
            List<UserGroupMessage> userGroupMessageList = new ArrayList<UserGroupMessage>();
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(userGroupMessageList);
            return serviceResult;
        }
        //数据序列化
        List<UserGroupMessage> userGroupMessageList = modelHandler.toUserGroupMessageList(stringSet, fromUserAccount);
        //根据当前数据向下展示
        if (direction.equals(MessageContext.DOWN)) {
            List<UserGroupMessage> userGroupMessageDownList;
            if (userGroupMessageList.size() > initRow) {
                userGroupMessageDownList = userGroupMessageList.subList(initRow, userGroupMessageList.size());
                for (int i = 0; i < userGroupMessageDownList.size(); i++) {
                    UserGroupMessage userGroupMessageData = userGroupMessageDownList.get(i);
                    if (userGroupMessageData.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        String substring = userGroupMessageData.getFilePath().substring(userGroupMessageData.getFilePath().lastIndexOf("/") + 1);
                        userGroupMessageDownList.get(i).setFilePath(substring);
                    }
                    if (userGroupMessageData.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        // 图片路径前要拼接url前缀
                        userGroupMessageData.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessageData.getFilePath());
                    }
                    userGroupMessageDownList.set(i, userGroupMessageData);
                }
            } else {
                userGroupMessageDownList = userGroupMessageList;
                for (int i = 0; i < userGroupMessageDownList.size(); i++) {
                    UserGroupMessage userGroupMessageData = userGroupMessageDownList.get(i);
                    if (userGroupMessageData.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        String substring = userGroupMessageData.getFilePath().substring(userGroupMessageData.getFilePath().lastIndexOf("/") + 1);
                        userGroupMessageDownList.get(i).setFilePath(substring);
                    }
                    if (userGroupMessageData.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        // 图片路径前要拼接url前缀
                        userGroupMessageData.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessageData.getFilePath());
                    }
                    userGroupMessageDownList.set(i, userGroupMessageData);
                }
            }
            //进行list集合排序（升序）
            userGroupMessageDownList.sort(Comparator.comparing(UserGroupMessage::getCreateTime));
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(userGroupMessageDownList);
            return serviceResult;
        } else {
            //根据当前数据向上展示UPWARD
            List<UserGroupMessage> userGroupMessageUpwardList;
            if (userGroupMessageList.size() > initRow) {
                userGroupMessageUpwardList = userGroupMessageList.subList(0, initRow + 1);
                for (int i = 0; i < userGroupMessageUpwardList.size(); i++) {
                    UserGroupMessage userGroupMessageData = userGroupMessageUpwardList.get(i);
                    if (userGroupMessageData.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        String substring = userGroupMessageData.getFilePath().substring(userGroupMessageData.getFilePath().lastIndexOf("/") + 1);
                        userGroupMessageUpwardList.get(i).setFilePath(substring);
                    }
                    if (userGroupMessageData.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        // 图片路径前要拼接url前缀
                        userGroupMessageData.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessageData.getFilePath());
                    }
                    userGroupMessageUpwardList.set(i, userGroupMessageData);
                }
            } else {
                userGroupMessageUpwardList = userGroupMessageList;
                for (int i = 0; i < userGroupMessageUpwardList.size(); i++) {
                    UserGroupMessage userGroupMessageData = userGroupMessageUpwardList.get(i);
                    if (userGroupMessageData.getType().equals(MessageType.FILE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        String substring = userGroupMessageData.getFilePath().substring(userGroupMessageData.getFilePath().lastIndexOf("/") + 1);
                        userGroupMessageUpwardList.get(i).setFilePath(substring);
                    }
                    if (userGroupMessageData.getType().equals(MessageType.PICTURE) && !Strings.isNullOrEmpty(userGroupMessageData.getFilePath())) {
                        // 图片路径前要拼接url前缀
                        userGroupMessageData.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessageData.getFilePath());
                    }
                    userGroupMessageUpwardList.set(i, userGroupMessageData);
                }
            }
            //进行list集合排序（升序）
            userGroupMessageUpwardList.sort(Comparator.comparing(UserGroupMessage::getCreateTime));
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(userGroupMessageUpwardList);
            return serviceResult;
        }
    }

    /**
     * 将user_group_message表中的数据导入redis
     * @return 返回导入结果
     */
    @Override
    public ServiceResult<Boolean> importUserGroupMessageIntoRedis() {
        ServiceResult<Boolean> returnResult = new ServiceResult<>();

        List<UserGroupMessage> userGroupMessageList = findAllOrderByCreateTimeDesc();
        if (null != userGroupMessageList && userGroupMessageList.size() > 0) {
            log.info("开始导入user_group_message数据");
            for (int i = 0; i < userGroupMessageList.size(); i++) {
                UserGroupMessage userGroupMessage = userGroupMessageList.get(i);
                redisManager.zAdd(RedisStorage.USER_GROUP_MESSAGE_PREFIX + userGroupMessage.getUserGroupId(),
                        JSON.toJSONString(userGroupMessageList.get(i)),
                        userGroupMessage.getCreateTime().getTime());
//                redisManager.expire(RedisStorage.USER_GROUP_MESSAGE_PREFIX + userGroupMessage.getUserGroupId(), redisParamConfig.getTimeout());
            }
        }

        returnResult.setMessage(ServiceResult.SYNC_SUCCESS);
        returnResult.setSuccess(Boolean.TRUE);
        returnResult.setResult(Boolean.TRUE);
        return returnResult;
    }

    /**
     * 查询所有记录，按照创建时间降序排列
     * @return 返回查询结果
     */
    public List<UserGroupMessage> findAllOrderByCreateTimeDesc() {
        QueryWrapper<UserGroupMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        return baseMapper.selectList(queryWrapper);
    }

    /**
     * 处理list中的图片数据
     * @param list 结果list
     */
    private void handListPictureData(List<UserGroupMessage> list) {
        if (list != null && list.size() > 0) {
            for (UserGroupMessage userGroupMessage : list) {
                if (userGroupMessage.getType().intValue() == MessageType.PICTURE && !Strings.isNullOrEmpty(userGroupMessage.getFilePath())) {
                    userGroupMessage.setFilePath(pictureParamConfig.getUrlprefix() + userGroupMessage.getFilePath());
                }
            }
        }
    }

    /**
     * 根据自定义群组id和账号id获取自定义群组账号信息
     * @param userGroupId
     * @param fromUserAccount
     * @return 返回自定义群组账号信息
     */
    private RefUserGroupAccount getRefUserGroupAccountByUserGroupIdAccountId(Integer userGroupId, String fromUserAccount){
        QueryWrapper<RefUserGroupAccount> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(Constants.USER_GROUP_ID_FIELD, userGroupId);
        queryWrapper.eq(Constants.USER_NAME, fromUserAccount);
        return refUserGroupAccountMapper.selectOne(queryWrapper);
    }

    /**
     * 根据请求参数从redis中获取需要的数据
     * @param start 指定开始位置
     * @param pageSize 查询数据数量
     * @param userGroupId 用户自定义群组id
     * @param contentInvisibleTime 内容不可见时间
     * @param joinGroupTime 进群时间
     * @param timeout redis值超时时间
     * @return 返回查询结果
     */
    private Set<String> getRedisDataByParam(long start, long pageSize, Integer userGroupId, Date contentInvisibleTime, Date joinGroupTime, Integer timeout) {
        Set<String> stringSet;
        long startScore;
        long endScore = System.currentTimeMillis();

        // 获取指定时间前的时间
        Date leftDate = DateUtils.getLeftDateByTime(timeout);

        // 若用户设置了内容不可见时间，查询聊天记录时查询此时间及之后的数据，否则查询所有数据
        if(contentInvisibleTime != null || joinGroupTime != null){
            Date queryStartTime;
            if(contentInvisibleTime != null && joinGroupTime != null){
                if(contentInvisibleTime.compareTo(joinGroupTime) > 0){
                    queryStartTime = contentInvisibleTime;
                }else{
                    queryStartTime = joinGroupTime;
                }
            }else if(contentInvisibleTime == null && joinGroupTime != null){
                queryStartTime = joinGroupTime;
            }else{
                queryStartTime = contentInvisibleTime;
            }

            // 判断查询开始时间如果早于15天前的时间，则查询开始时间为15天前的时间
            if(queryStartTime.compareTo(leftDate) < 0){
                queryStartTime = leftDate;
            }
            startScore = queryStartTime.getTime();
        }else{
            startScore = leftDate.getTime();
        }

        stringSet = redisManager.zReverseRangeByScore(RedisStorage.USER_GROUP_MESSAGE_PREFIX + userGroupId, startScore, endScore, start, pageSize);
        return stringSet;
    }

    /**
     * 获取根据查询条件未获取到用户信息时的返回结果
     * @param serviceResult 结果
     * @param userGroupId 自定义群组id
     * @param fromUserId 发送消息的用户id
     * @return 返回结果
     */
    private ServiceResult<List<UserGroupMessage>> returnQueryFaultResult(ServiceResult<List<UserGroupMessage>> serviceResult, Integer userGroupId, String fromUserId){
        serviceResult.setMessage(ServiceResult.SELECT_FAIL);
        serviceResult.setSuccess(Boolean.FALSE);
        serviceResult.setMessage("根据自定义群组:{" + userGroupId + "},用户id:{" + fromUserId + "}查询用户数据失败,请重试");
        serviceResult.setResult(new ArrayList<>());
        return serviceResult;
    }
}