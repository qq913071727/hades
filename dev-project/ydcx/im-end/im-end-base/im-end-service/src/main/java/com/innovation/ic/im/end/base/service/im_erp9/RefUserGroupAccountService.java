package com.innovation.ic.im.end.base.service.im_erp9;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.model.im_erp9.RefUserGroupAccount;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupAddMemberVo;

import java.util.List;
import java.util.Map;

public interface RefUserGroupAccountService {

    /**
     * 根据userGroupId，查找username不在usernameList范围内的用户
     *
     * @param userGroupId
     * @param usernameList
     * @return
     */
    ServiceResult<List<RefUserGroupAccount>> findByUserGroupIdAndNotInList(Integer userGroupId, List<String> usernameList);

    /**
     * 保存RefUserGroupAccount对象
     *
     * @param refUserGroupAccount
     * @return
     */
    ServiceResult<Boolean> saveRefUserGroupAccount(RefUserGroupAccount refUserGroupAccount);

    /**
     * 设置某个用户对某个自定义群组是否消息免打扰
     *
     * @param userGroupId
     * @param accountId
     * @param username
     * @param status
     * @return
     */
    ServiceResult<Boolean> noReminder(Integer userGroupId, String accountId, String username, Integer status);

    /**
     * 将某个用户对某个自定义群组设置为置顶
     *
     * @param userGroupId
     * @param accountId
     * @param username
     * @param status
     * @return
     */
    ServiceResult<Boolean> topping(Integer userGroupId, String accountId, String username, Integer status);

    /**
     * 退出群聊。删除某个用户和某个自定义群组的关系，删除这个用户在这个自定义群组的未读消息
     *
     * @param userGroupId
     * @param accountId
     * @param username
     * @return
     */
    ServiceResult<Boolean> exit(Integer userGroupId, String accountId, String username);

    /**
     * 根据userGroupId，修改对应记录的last_contact_time字段
     *
     * @param userGroupId
     * @return
     */
    ServiceResult<Boolean> updateLastContactTimeByUserGroupId(Integer userGroupId);

    /**
     * 根据条件查询自定义群组信息
     *
     * @param map 查询条件
     * @return 返回自定义群组信息
     */
    ServiceResult<GroupPojo> getGroupDetailInfoByParam(Map<String, Object> map);

    /**
     * 根据用户设置自定义群组聊天内容不可见时间
     *
     * @param map 更新条件
     * @return 返回更新结果
     */
    ServiceResult<Boolean> invisibleChatRecordSet(Map<String, Object> map);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
    ServiceResult<List<RefUserGroupAccount>> findByRealId(String id);

    /**
     * 根据自定义群组id获取群组人数
     *
     * @param userGroupId 自定义群组id
     * @return 群组人数
     */
    ServiceResult<Integer> getCountByUserGroupId(String userGroupId);

    /**
     * 查询当前自定义群组中除自己之外的用户账号
     *
     * @param userGroupId 自定义群组id
     * @param userName    用户名
     * @return 返回除自己之外的用户账号
     */
    ServiceResult<List<String>> getUserGroupAccountsNotHaveSelf(Integer userGroupId, String userName);

    /**
     * 查询自定义群组中的用户账号
     *
     * @param userGroupId 自定义群组id
     * @return 返回自定义群组中的用户账号
     */
    ServiceResult<List<String>> getUserGroupAccounts(Integer userGroupId);

    /**
     * 根据自定义群组id获取自定义群组信息
     *
     * @param userGroupId 自定义群组id
     * @return 返回自定义群组信息
     */
    ServiceResult<JSONObject> getUserGroupInfo(Integer userGroupId);

    /**
     * 更新用户username自定义群组userGroupId的最近联系时间
     *
     * @param userGroupId 自定义群组id
     * @param accountId   用户账号
     * @param username    用户名
     * @return 返回更新结果
     */
    ServiceResult<Boolean> updateLastContactTime(String userGroupId, String accountId, String username);

    /**
     * 添加自定义群组成员
     *
     * @param userGroupAddMemberVo 用户自定义群组添加成员vo类
     * @return 返回添加结果
     */
    ServiceResult<Boolean> addRefUserGroupMember(UserGroupAddMemberVo userGroupAddMemberVo);

    /**
     * 删除最近联系自定义群组并清除聊天记录
     *
     * @param map 删除条件
     * @return 返回删除结果
     */
    ServiceResult<Boolean> deleteUserGroupChat(Map<String, Object> map);

    /**
     * 添加数据到redis
     * @param userGroupId 为null全量添加,userGroupId不为null更新当前自定义群组信息
     * @return
     */
    ServiceResult<Boolean> importRefUserGroupAccountIntoRedis(Integer userGroupId);
}