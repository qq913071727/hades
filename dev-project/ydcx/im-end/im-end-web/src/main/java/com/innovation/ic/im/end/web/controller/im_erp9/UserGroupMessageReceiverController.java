package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.service.im_erp9.UserGroupMessageReceiverService;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.innovation.ic.im.end.web.thread.HandleRetractionUserGroupMsgThread;
import com.innovation.ic.im.end.base.thread.web.SendRabbitMqMessageThread;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * 用户自定义群组未读消息API
 */
@Api(value = "用户自定义群组未读消息API", tags = "UserGroupOfflineMessageController")
@RestController
@RequestMapping("/api/v1/userGroupMessageReceiver")
@DefaultProperties(defaultFallback = "defaultFallback")
public class UserGroupMessageReceiverController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(UserGroupMessageReceiverController.class);

    @Resource
    private UserGroupMessageReceiverService userGroupMessageReceiverService;

    @Resource
    private RabbitMqParamConfig rabbitMqParamConfig;

    /**
     * 恢复用户自定义群组未读消息
     * @return 返回处理结果
     */
    @HystrixCommand
    @ApiOperation(value = "恢复用户自定义群组未读消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "自定义群组id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "toUserAccount", value = "用户账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/restore/{userGroupId}/{toUserAccount}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> restore(@PathVariable("userGroupId") Integer userGroupId, @PathVariable("toUserAccount") String toUserAccount, HttpServletRequest request, HttpServletResponse response) {
        if ( userGroupId == null || StringUtils.isEmpty(toUserAccount)) {
            String message = "调用接口【/api/v1/userGroupMessageReceiver/restore/{userGroupId}/{toUserAccount}时，" +
                    "参数userGroupId和toUserAccount不能为空";
            log.warn(message);
            ApiResult<Integer> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 通过userGroupId和toUserAccount 去修改消息状态
        ServiceResult<Integer> result = userGroupMessageReceiverService.restoreUserGroupMessageReceiver(userGroupId, toUserAccount);

        // 推送rabbitMq消息
        List<String> list = new ArrayList<>();
        list.add(toUserAccount);
        SendRabbitMqMessageThread sendRabbitMqMessageThread = new SendRabbitMqMessageThread(list, rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.CURRENT_EXCHANGE));
        threadPoolManager.execute(sendRabbitMqMessageThread);

        ApiResult<Integer> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        apiResult.setResult(result.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 自定义群组，撤回消息
     * @return 返回处理结果
     */
    @HystrixCommand
    @ApiOperation(value = "自定义群组，撤回消息")
    @ApiImplicitParam(name = "groupRetractionVo", value = "自定义群组，撤回消息的vo类", required = true, dataType = "GroupRetractionVo")
    @RequestMapping(value = "/retraction", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> retraction(@RequestBody GroupRetractionVo groupRetractionVo, HttpServletRequest request, HttpServletResponse response) {
        if (null == groupRetractionVo || null == groupRetractionVo.getGroupMessageId()
                || null == groupRetractionVo.getRetractionTime()) {
            String message = "调用接口【/api/v1/userGroupMessageReceiver/retraction时，" +
                    "参数groupMessageId和retractionTime不能为空";
            log.warn(message);
            ApiResult apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 创建线程处理撤回消息数据
        HandleRetractionUserGroupMsgThread handleRetractionUserGroupMsgThread = new HandleRetractionUserGroupMsgThread(userGroupMessageReceiverService, groupRetractionVo, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(handleRetractionUserGroupMsgThread);

        ApiResult apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}