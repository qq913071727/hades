package com.innovation.ic.im.end.web.thread;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.model.im_erp9.*;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.enums.AvailableStatusEnum;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.service.im_erp9.RefUserGroupAccountService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.vo.im_erp9.SendUserGroupMessageVo;
import com.innovation.ic.im.end.web.endpoint.UserGroupEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import javax.websocket.Session;
import java.util.*;

/**
 * @desc   保存自定义群组撤回消息对象内容
 * @author linuo
 * @time   2022年8月10日09:44:10
 */
public class SaveRetractionUserGroupMessageThread extends AbstractThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String exchange;

    private Map<String, Set<UserGroupEndpoint>> onlineUserGroupMap;

    public SaveRetractionUserGroupMessageThread(UserGroupMessage userGroupMessage, Map<String, Set<UserGroupEndpoint>> onlineUserGroupMap, String exchange, ThreadPoolManager threadPoolManager) {
        this.userGroupMessage = userGroupMessage;
        this.onlineUserGroupMap = onlineUserGroupMap;
        this.exchange = exchange;
        this.threadPoolManager = threadPoolManager;
    }

    public SaveRetractionUserGroupMessageThread() {}

    @Override
    public void run() {
        // 更新最近联系时间
        RefUserGroupAccountService refUserGroupAccountService = ServiceImplHelper.getRefUserGroupAccountService();
        refUserGroupAccountService.updateLastContactTimeByUserGroupId(userGroupMessage.getUserGroupId());

        // 保存自定义群组消息
        userGroupMessage.setFromUserAvailable(AvailableStatusEnum.AVAILABLE.getCode());
        ServiceResult<Integer> integerServiceResult = ServiceImplHelper.getUserGroupMessageService().saveUserGroupMessage(this.userGroupMessage);
        Integer userGroupMsgId = integerServiceResult.getResult();
        userGroupMessage.setId(userGroupMsgId);

        // 保存自定义群组未读消息
        SaveRetractionUserGroupMessageReceiverThread saveRetractionUserGroupMessageReceiverThread = new SaveRetractionUserGroupMessageReceiverThread(userGroupMessage, exchange, userGroupMsgId);
        threadPoolManager.execute(saveRetractionUserGroupMessageReceiverThread);

        // 向自定义群组的所有在线用户推送补充了消息id的数据
        Set<UserGroupEndpoint> userGroupEndpointSet = onlineUserGroupMap.get(userGroupMessage.getUserGroupId().toString());
        if(null != userGroupEndpointSet && userGroupEndpointSet.size() > 0){
            for (UserGroupEndpoint userGroupEndpoint : userGroupEndpointSet) {
                Session theSession = userGroupEndpoint.session;
                synchronized (theSession) {
                    if (theSession.isOpen()) {
                        SendUserGroupMessageVo sendUserGroupMessageVo = new SendUserGroupMessageVo();
                        BeanUtils.copyProperties(userGroupMessage, sendUserGroupMessageVo);
                        logger.info("sendUserGroupMessageVo=[{}]", sendUserGroupMessageVo);
                        theSession.getAsyncRemote().sendText(JSON.toJSONString(sendUserGroupMessageVo));
                    }
                }

                logger.info("用户【" + userGroupMessage.getFromUserAccount() + "】向自定义群组【"
                        + userGroupMessage.getUserGroupId() + "】中的用户【" + userGroupEndpoint.username
                        + "】发送系统消息【" + userGroupMessage.getContent() + "】");
            }
        }
    }
}