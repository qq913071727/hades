package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.model.im_erp9.RefGroupAccountPrivilege;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 账号对默认群组的权限API
 */
@Api(value = "账号对默认群组的权限API", tags = "RefGroupAccountPrivilegeController")
@RestController
@RequestMapping("/api/v1/refGroupAccountPrivilege")
@DefaultProperties(defaultFallback = "defaultFallback")
public class RefGroupAccountPrivilegeController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RefGroupAccountPrivilegeController.class);

    /**
     * 查找某个用户可以发送消息的默认群组的id列表
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "查找某个用户可以发送消息的默认群组的id列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/list/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> list(@PathVariable("accountId") String accountId,
                                                        @PathVariable("username") String username,
                                                        HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(accountId) || StringUtils.isEmpty(username)) {
            String message = "调用接口【/api/v1/refGroupAccountPrivilege/list/{accountId}/{username}时，" +
                    "参数accountId和username不能为空";
            log.warn(message);
            ApiResult<List<RefGroupAccountPrivilege>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<RefGroupAccountPrivilege>> serviceResult = refGroupAccountPrivilegeService.findByAccountIdAndUsername(accountId, username);

        ApiResult<List<RefGroupAccountPrivilege>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 赋予某用户高级权限
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "赋予某用户高级权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/senior/{accountId}/{username}/{role}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> senior(@PathVariable("accountId") String accountId,
                                                                          @PathVariable("username") String username,
                                                                            @PathVariable("role") Integer role,
                                                                          HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(accountId) || StringUtils.isEmpty(username) ||role == null) {
            String message = "调用接口【/api/v1/refGroupAccountPrivilege/senior/{accountId}/{username}/{role}时，" +
                    "参数accountId和username和role不能为空";
            log.warn(message);
            ApiResult<List<RefGroupAccountPrivilege>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        refGroupAccountPrivilegeService.updateGroupId(accountId, username, role);

        ApiResult<List<RefGroupAccountPrivilege>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}