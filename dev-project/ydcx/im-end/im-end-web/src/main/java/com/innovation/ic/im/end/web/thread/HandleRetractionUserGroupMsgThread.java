package com.innovation.ic.im.end.web.thread;

import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.pojo.constant.MessageType;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.service.im_erp9.UserGroupMessageReceiverService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.thread.web.SendWithdrawRabbitMqMessageThread;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import com.innovation.ic.im.end.web.endpoint.UserGroupEndpoint;
import lombok.SneakyThrows;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @desc   处理自定义群组撤回消息数据
 * @author linuo
 * @time   2022年10月18日10:30:58
 */
public class HandleRetractionUserGroupMsgThread extends AbstractThread implements Runnable {

    public HandleRetractionUserGroupMsgThread(UserGroupMessageReceiverService userGroupMessageReceiverService, GroupRetractionVo groupRetractionVo, RabbitMqParamConfig rabbitMqParamConfig, ThreadPoolManager threadPoolManager) {
        this.userGroupMessageReceiverService = userGroupMessageReceiverService;
        this.groupRetractionVo = groupRetractionVo;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
        this.threadPoolManager = threadPoolManager;
    }

    @SneakyThrows
    @Override
    public void run() {
        RetractionPojo retractionPojo = userGroupMessageReceiverService.retractionUserGroupMessageReceiver(groupRetractionVo);
        //通过GroupId 获取对应的人
        List<String> refGroupAccountList = userGroupMessageReceiverService.findUsername(retractionPojo.getUserGroupMessage().getUserGroupId(),retractionPojo.getUserGroupMessage().getFromUserAccount());
        SendWithdrawRabbitMqMessageThread sendWithdrawRabbitMqMessageThread = new SendWithdrawRabbitMqMessageThread(refGroupAccountList,retractionPojo, rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.RETRACTION_EXCHANGE));
        threadPoolManager.execute(sendWithdrawRabbitMqMessageThread);

        // 给自定义群组中接收消息的人发送通知消息
        saveRetractionUserGroupMsgAndSendToAccount(retractionPojo.getUserGroupMessage());
    }

    /**
     * 给自定义群组中接收消息的人发送通知消息
     * @param userGroupMessage 自定义群组消息
     */
    private void saveRetractionUserGroupMsgAndSendToAccount(UserGroupMessage userGroupMessage) {
        Map<String, Set<UserGroupEndpoint>> onlineUserGroupMap = UserGroupEndpoint.onlineUserGroupMap;

        UserGroupMessage ugm = new UserGroupMessage();
        ugm.setUserGroupId(userGroupMessage.getUserGroupId());
        ugm.setFromUserAccount(userGroupMessage.getFromUserAccount());
        ugm.setFromUserRealName(userGroupMessage.getFromUserRealName());
        if(!Strings.isNullOrEmpty(userGroupMessage.getFromUserRealName())){
            ugm.setContent(userGroupMessage.getFromUserRealName() + "撤回了一条消息");
        }else{
            ugm.setContent(userGroupMessage.getFromUserAccount() + "撤回了一条消息");
        }
        ugm.setType(MessageType.SYS_MSG);
        ugm.setRetractionContent(userGroupMessage.getContent());
        ugm.setCreateTime(userGroupMessage.getCreateTime());
        ugm.setRetractionMsgId(userGroupMessage.getId());

        SaveRetractionUserGroupMessageThread saveRetractionUserGroupMessageThread = new SaveRetractionUserGroupMessageThread(ugm, onlineUserGroupMap,
                rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.CURRENT_EXCHANGE), this.threadPoolManager);
        threadPoolManager.execute(saveRetractionUserGroupMessageThread);
    }
}