package com.innovation.ic.im.end.web.controller.im_b1b;

import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_b1b.B1bTestPojo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 人员API
 */
@Api(value = "人员API", tags = "B1bAdminsController")
@RestController
@RequestMapping("/api/v1/b1bAdmins")
public class B1bTestController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(B1bTestController.class);

    /**
     * 返回所有人员，按照字母顺序升序排列
     *
     * @return
     */
    @ApiOperation(value = "返回所有人员，按照字母顺序升序排列")
    @RequestMapping(value = "/findAllOrderByRealNameAsc", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<B1bTestPojo>>> findAllOrderByRealNameAsc() {
        ServiceResult<List<B1bTestPojo>> serviceResult = new ServiceResult<List<B1bTestPojo>>();

        ApiResult<List<B1bTestPojo>> apiResult = new ApiResult<List<B1bTestPojo>>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<List<B1bTestPojo>>>(apiResult, HttpStatus.OK);
    }


}
