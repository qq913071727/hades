package com.innovation.ic.im.end.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 解决跨域问题，session跨域问题
 */
@Configuration
@EnableWebMvc
public class CorsConfig implements WebMvcConfigurer {

    /**
     * 解决跨域问题，session跨域问题
     *
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                // 允许向该服务器提交请求的URI，解决跨域问题，*表示全部允许。这里尽量限制来源域，比如http://xxxx:8080，以降低安全风险
                .allowedOrigins("*")
                // 允许提交请求的方法，*表示全部允许，也可以单独设置GET、PUT等
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
//            .allowedMethods("*")
                // 允许访问的头信息，*表示全部
                .allowedHeaders("*")
                // 允许cookies跨域
                .allowCredentials(true)
                // 预检请求的缓存时间（秒），即在这个时间段里，对于相同的跨域请求不会再预检了
                .maxAge(3600);
    }
}