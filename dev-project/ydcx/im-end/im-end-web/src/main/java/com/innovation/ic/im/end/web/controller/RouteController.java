package com.innovation.ic.im.end.web.controller;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 路由controller
 */
@Api(value = "路由controller", tags = "RouteController")
@Controller
@RequestMapping("/page")
public class RouteController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(RouteController.class);

    @RequestMapping(value = "/test")
    public String test() {
        return "test";
    }

    @RequestMapping(value = "/talk")
    public String talk() {
        return "talk";
    }

    @RequestMapping(value = "/group")
    public String group() {
        return "group";
    }

    @RequestMapping(value = "/userGroup")
    public String userGroup() {
        return "userGroup";
    }
}
