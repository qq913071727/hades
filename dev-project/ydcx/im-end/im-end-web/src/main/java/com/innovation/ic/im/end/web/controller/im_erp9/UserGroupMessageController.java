package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.value.config.RedisParamConfig;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupMessageVo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户自定义群组消息API
 */
@Api(value = "用户自定义群组消息API", tags = "UserGroupMessageController")
@RestController
@RequestMapping("/api/v1/userGroupMessage")
@DefaultProperties(defaultFallback = "defaultFallback")
public class UserGroupMessageController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(UserGroupMessageController.class);

    @Resource
    private RedisParamConfig redisParamConfig;

    /**
     * 根据参数type，获取某个用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数type，获取某个用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "用户自定义群组id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "fromUserAccount", value = "用户id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "type", value = "0表示全部，2表示文件，3表示图片", required = true, dataType = "Integer")
    })
    @RequestMapping(value = "/pageByType/{userGroupId}/{fromUserAccount}/{pageSize}/{pageNo}/{type}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> pageByType(@PathVariable("userGroupId") Integer userGroupId,
                                                                        @PathVariable("fromUserAccount") String fromUserAccount,
                                                                        @PathVariable("pageSize") Integer pageSize,
                                                                        @PathVariable("pageNo") Integer pageNo,
                                                                        @PathVariable("type") Integer type,
                                                                        HttpServletRequest request, HttpServletResponse response) {
        if (null == userGroupId || StringUtils.isEmpty(fromUserAccount)
                || null == pageSize || null == pageNo || null == type) {
            String message = "调用接口【/api/v1/userGroupMessage/pageByType/{userGroupId}/{fromUserAccount}/{pageSize}/{pageNo}/{type}时，" +
                    "userGroupId、fromUserAccount、pageSize、pageNo、type不能为空";
            log.warn(message);
            ApiResult<List<UserGroupMessage>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<UserGroupMessage>> userGruopMessageListServiceResult = userGroupMessageService.redisPage(userGroupId, fromUserAccount, pageSize, pageNo, type, redisParamConfig.getTimeout());

        ApiResult<List<UserGroupMessage>> apiResult = new ApiResult<>();
        apiResult.setSuccess(userGruopMessageListServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(userGruopMessageListServiceResult.getResult());
        apiResult.setMessage(userGruopMessageListServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据参数type和content（模糊查询），获取用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数type和content（模糊查询），获取用户自定义群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupMessageVo", value = "UserGroupMessage的vo类", required = true, dataType = "UserGroupMessageVo"),
            @ApiImplicitParam(name = "userGroupId", value = "用户自定义群组id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "fromUserAccount", value = "用户账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "type", value = "0表示全部，2表示文件", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "content", value = "内容", required = true, dataType = "String")
    })
    @RequestMapping(value = "/searchAndPageByType", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> searchAndPageByType(@RequestBody UserGroupMessageVo userGroupMessageVo,
                                                                             HttpServletRequest request, HttpServletResponse response) {
        if (userGroupMessageVo.getUserGroupId() == null || StringUtils.isEmpty(userGroupMessageVo.getFromUserAccount())
                || userGroupMessageVo.getPageNo() == null || userGroupMessageVo.getPageSize() == null || userGroupMessageVo.getType() == null) {
            String message = "调用接口【/api/v1/userGroupMessage/searchAndPageByType时，userGroupId、fromUserAccount、pageNo、pageSize、type、content不能为空";
            log.warn(message);
            ApiResult<List<UserGroupMessage>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<ApiResult>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<UserGroupMessage>> userGroupMessageListServiceResult = userGroupMessageService.redisLikePage(userGroupMessageVo, redisParamConfig.getTimeout());

        ApiResult<List<UserGroupMessage>> apiResult = new ApiResult<>();
        apiResult.setSuccess(userGroupMessageListServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(userGroupMessageListServiceResult.getResult());
        apiResult.setMessage(userGroupMessageListServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据参数id、initRow和direction，获取用户自定义群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示，过滤掉不可见的记录
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数id、initRow和direction，获取用户自定义群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示，过滤掉不可见的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "user_group_message表的id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "fromUserAccount", value = "用户账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "initRow", value = "第一次显示时，当前记录上面的记录数或下面的记录数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "direction", value = "分页显示的方向。1表示向下，2表示向上", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", required = true, dataType = "Integer")

    })
    @RequestMapping(value = "/context/{id}/{fromUserAccount}/{initRow}/{direction}/{pageSize}/{pageNo}", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> context(@PathVariable("id") Integer id,
                                                                     @PathVariable("fromUserAccount") String fromUserAccount,
                                                                     @PathVariable("initRow") Integer initRow,
                                                                     @PathVariable("direction") Integer direction,
                                                                     @PathVariable("pageSize") Integer pageSize,
                                                                     @PathVariable("pageNo") Integer pageNo,
                                                                     HttpServletRequest request, HttpServletResponse response) {
        if (id == null || initRow == null || direction == null || pageSize == null || pageNo == null) {
            String message = "调用接口【/api/v1/userGroupMessage/context/{id}/{fromUserAccount}/{initRow}/{direction}/{pageSize}/{pageNo}时，" +
                    "id、fromUserAccount、initRow、direction、pageSize、pageNo不能为空";
            log.warn(message);
            ApiResult<List<UserGroupMessage>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<UserGroupMessage>> userGroupMessageListServiceResult = userGroupMessageService.redisContextPage(id, fromUserAccount, initRow, direction, pageSize, pageNo, redisParamConfig.getTimeout());

        ApiResult<List<UserGroupMessage>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(userGroupMessageListServiceResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}