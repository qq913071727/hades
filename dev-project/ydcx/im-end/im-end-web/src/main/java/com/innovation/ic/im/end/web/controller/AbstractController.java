package com.innovation.ic.im.end.web.controller;

import com.innovation.ic.b1b.framework.manager.MinioManager;
import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.im_erp9.*;
import com.innovation.ic.im.end.base.value.config.FileParamConfig;
import com.innovation.ic.im.end.base.value.config.MinioConfig;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.base.value.config.SftpParamConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 抽象controller
 */
public abstract class AbstractController {
    @Autowired
    protected SftpParamConfig sftpParamConfig;

    @Autowired
    protected FileParamConfig fileParamConfig;

    @Autowired
    protected MinioConfig minioConfig;

    @Autowired
    protected MinioManager minioManager;

    @Autowired
    protected DialogueService dialogueService;

    @Autowired
    protected TreeNodeService treeNodeService;

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected MessageService messageService;

    @Autowired
    protected GroupMessageService groupMessageService;

    @Autowired
    protected CustomerServiceScoreService customerServiceScoreService;

    @Autowired
    protected UserGroupService userGroupService;

    @Autowired
    protected RefGroupAccountService refGroupAccountService;

    @Autowired
    protected RefGroupAccountPrivilegeService refGroupAccountPrivilegeService;

    @Autowired
    protected RefUserGroupAccountService refUserGroupAccountService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected ChatPairService chatPairService;

    @Autowired
    protected UserGroupMessageService userGroupMessageService;

    @Autowired
    protected RabbitMqManager rabbitMqManager;

    @Autowired
    protected RedisManager redisManager;

    @Autowired
    protected ThreadPoolManager threadPoolManager;

    @Autowired
    protected RabbitMqParamConfig rabbitMqParamConfig;
    /**
     * hystrix默认处理方法
     * 其它应用调用超时时调用此方法进行异常信息抛出，防止出现长时间接口不返回结果的情况出现
     * @return 返回熔断默认结果
     */
    public ResponseEntity<ApiResult> defaultFallback(){
        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(Boolean.FALSE);
        apiResult.setCode(HttpStatus.REQUEST_TIMEOUT.value());
        apiResult.setResult(Boolean.FALSE);
        apiResult.setMessage(ServiceResult.TIME_OUT);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}