package com.innovation.ic.im.end.web.endpoint;

import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.MessageType;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.vo.im_erp9.MessageVo;
import com.innovation.ic.im.end.web.config.GetHttpSessionConfigurator;
import com.innovation.ic.im.end.web.thread.SaveMessageThread;
import com.innovation.ic.im.end.web.thread.SendSelfMessageThread;
import lombok.Data;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Data
@Component
@ServerEndpoint(value = "/ws/v1/upload/{fromUserAccount}/{toUserAccount}/{filename}/{type}", configurator = GetHttpSessionConfigurator.class)
public class UploadEndpoint extends AbstractEndpoint {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 建立连接
     *
     * @param fromUserAccount
     * @param toUserAccount
     * @param filename
     * @param session
     * @param config
     */
    @OnOpen
    public void onOpen(@PathParam("fromUserAccount") String fromUserAccount,
                       @PathParam("toUserAccount") String toUserAccount,
                       @PathParam("filename") String filename,
                       @PathParam("type") Integer type,
                       Session session, EndpointConfig config) {
        // 验证参数
        if (!StringUtils.validateParameter(fromUserAccount) || !StringUtils.validateParameter(toUserAccount)
                || !StringUtils.validateParameter(filename) || null == type) {
            logger.warn("参数fromUserAccount、toUserAccount、filename、type不能为空");
            return;
        } else {
            logger.info("session的id为【" + session.getId() + "】，用户【" + fromUserAccount + "】" +
                    "发送给用户【" + toUserAccount + "】文件【" + filename + "】，当前在线人数【" + onlineMap.size() + "】");
        }

        Integer type_ = null;
        if (MessageType.FILE.equals(type)) {
            type_ = MessageType.FILE;
        } else if (MessageType.PICTURE.equals(type)) {
            type_ = MessageType.PICTURE;
        } else {
            logger.warn("session的id为【" + session.getId() + "】，用户【" + fromUserAccount + "】" +
                    "发送给用户【" + toUserAccount + "】文件【" + filename + "】，当前在线人数【" + onlineMap.size() + "】，" +
                    "type参数错误【" + type_ + "】");
            return;
        }
        MessageVo messageVo = new MessageVo(null, type_, filename, fromUserAccount, toUserAccount);
        onlineUploadMap.put(session.getId(), messageVo);

        // 注意，此处不用处理未读消息，未读消息在Endpoint类中处理
    }

    /**
     * 报错
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

    /**
     * 连接关闭
     */
    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        onlineUploadMap.remove(session.getId());
        logger.info("session id为【" + session.getId() + "】的连接关闭了，关闭原因【" + closeReason.toString() + "】，当前在线人数" + onlineMap.size());
    }

    /**
     * 上传文件
     *
     * @param byteBuffer
     * @param last
     * @param session
     */
    @OnMessage
    public void processUpload(ByteBuffer byteBuffer, boolean last, Session session) {
        // 验证参数
        if (null == byteBuffer) {
            logger.warn("上传的内容不能为空");
            return;
        }

        try {
            MessageVo messageVo = onlineUploadMap.get(session.getId());

            if (!last) {
                ByteBuffer lsByteBuffer = messageVo.getByteBuffer();
                if (lsByteBuffer == null) {
                    messageVo.setByteBuffer(byteBuffer);
                } else {
                    ByteBuffer newByteBuffer = ByteBuffer.allocate(lsByteBuffer.limit() + byteBuffer.limit());
                    newByteBuffer.put(lsByteBuffer);
                    newByteBuffer.put(byteBuffer);
                    newByteBuffer.flip();
                    messageVo.setByteBuffer(newByteBuffer);
                }
                logger.info("last不为true,文件传递不完整,待后续操作");
                return;
            } else {
                if (messageVo.getByteBuffer() != null) {
                    logger.info("在onlineUploadMap中存有其它分片的文件数据,需要拼合为完整文件内容");
                    ByteBuffer newByteBuffer = ByteBuffer.allocate(messageVo.getByteBuffer().limit() + byteBuffer.limit());
                    newByteBuffer.put(messageVo.getByteBuffer());
                    newByteBuffer.put(byteBuffer);
                    newByteBuffer.flip();
                    byteBuffer = newByteBuffer;
                    logger.info("文件byteBuffer拼接完成");
                } else {
                    logger.info("在onlineUploadMap中不存在其它分片的文件数据,byteBuffer为完整内容");
                }
            }

            String filename = messageVo.getFilePath();
            if (!Strings.isNullOrEmpty(filename)) {
                logger.info("-------------接收图片或文件:[{}]---------------", filename);
            }

            Integer type = messageVo.getType();
            if (type.intValue() == MessageType.FILE.intValue()) {
                InputStream inputStream = new ByteArrayInputStream(byteBuffer.array());
                MultipartFile file = new MockMultipartFile(filename, filename, ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
                List<String> upload = ServiceImplHelper.getMinioManager().upload(minioConfig.getBucketName(), new MultipartFile[]{file});
                if (!messageVo.getFilePath().contains(bucketName + File.separator)) {
                    messageVo.setFilePath(bucketName + File.separator + upload.get(0));
                }
                inputStream.close();
            }

            if (type.intValue() == MessageType.PICTURE.intValue()) {
                //获取byteBuffer中有效大小
                int len = byteBuffer.limit() - byteBuffer.position();
                byte[] bytes = new byte[len];
                for (int i = 0; i < bytes.length; i++) {
                    bytes[i] = byteBuffer.get();
                }

                // 获取ChannelSftp连接
                InterProcessMutex lock = new InterProcessMutex(curatorFramework, zookeeperPathWebParamConfig.getEndPointUpload());
                if (lock.acquire(zookeeperParamConfig.getWaitingLockTime(), TimeUnit.SECONDS)) {
                    Thread.sleep(100);
                    // 上传文件
                    sftpChannelManager.upload(fileParamConfig.getFileUrl(), bytes, filename);
                    messageVo.setByteBuffer(null);
                    lock.release();
                } else {
                    lock.release();
                }
            }

            if (messageVo != null && Strings.isNullOrEmpty(messageVo.getFromUserRealName())) {
                // 根据用户名获取真实姓名
                ServiceResult<Account> result = accountService.findByAccount(messageVo.getFromUserAccount());
                messageVo.setFromUserRealName(result.getResult().getRealName());
            }

            Message message = modelHandler.toMessage(messageVo);

            // 获取登录类型
            List<Integer> loginTypeList = getLoginTypeList();

            // 获取账号类别
            Integer integration = getIntegration(messageVo.getToUserAccount());

            // 对自己和自己聊天的情况做特殊处理
            if (messageVo.getFromUserAccount().equals(messageVo.getToUserAccount())) {
                SendSelfMessageThread sendSelfMessageThread = new SendSelfMessageThread(message, onlineMap, messageVo,
                        rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.CURRENT_EXCHANGE), loginTypeList, pictureParamConfig.getUrlprefix(), integration);
                threadPoolManager.execute(sendSelfMessageThread);
            } else {
                // 记录消息
                SaveMessageThread saveMessageThread = new SaveMessageThread(message, onlineMap, messageVo,
                        pictureParamConfig.getUrlprefix(), rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.CURRENT_EXCHANGE), loginTypeList,
                        rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.READ_STATE_UPDATE_MSG_EXCHANGE), threadPoolManager, integration,
                        rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.SC_READ_STATE_UPDATE_MSG_EXCHANGE));
                threadPoolManager.execute(saveMessageThread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}