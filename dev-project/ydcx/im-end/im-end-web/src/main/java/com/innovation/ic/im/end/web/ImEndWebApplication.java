package com.innovation.ic.im.end.web;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.innovation.ic.im.end")
@MapperScan("com.innovation.ic.im.end.base.mapper")
@EnableSwagger2
//@EnableHystrix
public class ImEndWebApplication {
    private static final Logger log = LoggerFactory.getLogger(ImEndWebApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(ImEndWebApplication.class, args);
        log.info("im-end-web服务启动成功");
    }
}