package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.value.config.RedisParamConfig;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupMessageVo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 默认群组消息API
 */
@Api(value = "默认群组消息API", tags = "GroupMessageController")
@RestController
@RequestMapping("/api/v1/groupMessage")
@DefaultProperties(defaultFallback = "defaultFallback")
public class GroupMessageController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(GroupMessageController.class);

    @Resource
    private RedisParamConfig redisParamConfig;

    /**
     * 根据参数type，获取某个默认群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数type，获取某个默认群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", value = "群组id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "fromUserAccount", value = "用户账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "type", value = "0表示全部，2表示文件，3表示图片", required = true, dataType = "Integer")
    })
    @RequestMapping(value = "/pageByType/{groupId}/{fromUserAccount}/{pageSize}/{pageNo}/{type}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> pageByType(@PathVariable("groupId") String groupId,
                                                                    @PathVariable("fromUserAccount") String fromUserAccount,
                                                                    @PathVariable("pageSize") Integer pageSize,
                                                                    @PathVariable("pageNo") Integer pageNo,
                                                                    @PathVariable("type") Integer type,
                                                                    HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(groupId) || !StringUtils.validateParameter(fromUserAccount)
                || null == pageSize || null == pageNo || null == type) {
            String message = "调用接口【/api/v1/groupMessage/pageByType/{groupId}/{fromUserAccount}/{pageSize}/{pageNo}/{type}时，" +
                    "参数groupId、fromUserAccount、pageSize、pageNo、type不能为空";
            log.warn(message);
            ApiResult<List<GroupMessage>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<GroupMessage>> groupMessageListServiceResult = groupMessageService.redisPage(groupId, fromUserAccount, pageSize, pageNo, type, redisParamConfig.getTimeout());

        ApiResult<List<GroupMessage>> apiResult = new ApiResult<>();
        apiResult.setSuccess(groupMessageListServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(groupMessageListServiceResult.getResult());
        apiResult.setMessage(groupMessageListServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据参数type和content（模糊查询），获取默认群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数type和content（模糊查询），获取默认群组的聊天记录，并按照创建时间降序排列，支持分页显示，过滤掉不可见的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupMessageVo", value = "GroupMessage的vo类", required = true, dataType = "GroupMessageVo"),
            @ApiImplicitParam(name = "groupId", value = "群组id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "fromUserAccount", value = "用户账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "type", value = "0表示全部，2表示文件", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "content", value = "内容", required = true, dataType = "Integer")
    })
    @RequestMapping(value = "/searchAndPageByType", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> searchAndPageByType(@RequestBody GroupMessageVo groupMessageVo, HttpServletRequest request, HttpServletResponse response) {
        if (null == groupMessageVo
                || !StringUtils.validateParameter(groupMessageVo.getGroupId())
                || !StringUtils.validateParameter(groupMessageVo.getFromUserAccount())
                || groupMessageVo.getPageNo() == null
                || groupMessageVo.getPageSize() == null
                || groupMessageVo.getType() == null) {
            String message = "调用接口【/api/v1/groupMessage/searchAndPageByType时，参数groupId、fromUserAccount、pageNo、pageSize、type、content不能为空";
            log.warn(message);
            ApiResult<List<GroupMessage>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<GroupMessage>> groupMessageListServiceResult = groupMessageService.redisLikePage(groupMessageVo, redisParamConfig.getTimeout());

        ApiResult<List<GroupMessage>> apiResult = new ApiResult<>();
        apiResult.setSuccess(groupMessageListServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(groupMessageListServiceResult.getResult());
        apiResult.setMessage(groupMessageListServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据参数id、initRow和direction，获取默认群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示，过滤掉不可见的记录
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数id、initRow和direction，获取默认群组的某个聊天记录的上下文记录，并按照创建时间降序排列，分别支持向上和向下分页显示，过滤掉不可见的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "group_message表的id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "fromUserAccount", value = "用户账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "initRow", value = "第一次显示时，当前记录上面的记录数或下面的记录数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "direction", value = "分页显示的方向。1表示向下，2表示向上", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", required = true, dataType = "Integer")

    })
    @RequestMapping(value = "/context/{id}/{fromUserAccount}/{initRow}/{direction}/{pageSize}/{pageNo}", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> context(@PathVariable("id") String id,
                                                                 @PathVariable("fromUserAccount") String fromUserAccount,
                                                                 @PathVariable("initRow") Integer initRow,
                                                                 @PathVariable("direction") Integer direction,
                                                                 @PathVariable("pageSize") Integer pageSize,
                                                                 @PathVariable("pageNo") Integer pageNo,
                                                                 HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(id) || !StringUtils.validateParameter(fromUserAccount)
                || initRow == null || direction == null || pageSize == null || pageNo == null) {
            String message = "调用接口【/api/v1/groupMessage/context/{Id}/{fromUserAccount}/{initRow}/{direction}/{pageSize}/{pageNo}时，" +
                    "id、fromUserAccount、initRow、direction、pageSize、pageNo不能为空";
            log.warn(message);
            ApiResult<List<GroupMessage>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<GroupMessage>> groupMessageListServiceResult = groupMessageService.redisContextPage(id, fromUserAccount, initRow, direction, pageSize, pageNo, redisParamConfig.getTimeout());

        ApiResult<List<GroupMessage>> apiResult = new ApiResult<>();
        apiResult.setSuccess(groupMessageListServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(groupMessageListServiceResult.getResult());
        apiResult.setMessage(groupMessageListServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}