package com.innovation.ic.im.end.web.thread;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.im.end.base.model.im_erp9.RefUserGroupAccount;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessageReceiver;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.enums.AvailableStatusEnum;
import com.innovation.ic.im.end.base.pojo.enums.ReadStatusEnum;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountGroupLastContactPojo;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc   保存自定义群组撤回消息对象内容
 * @author linuo
 * @time   2022年10月18日10:17:27
 */
public class SaveRetractionUserGroupMessageReceiverThread extends AbstractThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String exchange;
    private Integer userGroupMsgId;

    public SaveRetractionUserGroupMessageReceiverThread(UserGroupMessage userGroupMessage, String exchange, Integer userGroupMsgId) {
        this.userGroupMessage = userGroupMessage;
        this.exchange = exchange;
        this.userGroupMsgId = userGroupMsgId;
    }

    @Override
    public void run() {
        List<String> list = new ArrayList<>();
        list.add(userGroupMessage.getFromUserAccount());
        ServiceResult<List<RefUserGroupAccount>> serviceResult = ServiceImplHelper.getRefUserGroupAccountService().findByUserGroupIdAndNotInList(userGroupMessage.getUserGroupId(), list);
        if (null != serviceResult && null != serviceResult.getResult()) {
            List<RefUserGroupAccount> refUserGroupAccountList = serviceResult.getResult();
            for (RefUserGroupAccount refUserGroupAccount : refUserGroupAccountList) {
                UserGroupMessageReceiver userGroupMessageReceiver = new UserGroupMessageReceiver();
                userGroupMessageReceiver.setUserGroupId(userGroupMessage.getUserGroupId());
                userGroupMessageReceiver.setUserGroupMessageId(userGroupMsgId);
                userGroupMessageReceiver.setToUserAccount(refUserGroupAccount.getUsername());
                userGroupMessageReceiver.setToUserRealName(getUserRealNameByAccount(refUserGroupAccount.getUsername()));
                userGroupMessageReceiver.setRead(ReadStatusEnum.READ.getCode());
                userGroupMessageReceiver.setToUserAvailable(AvailableStatusEnum.AVAILABLE.getCode());
                ServiceImplHelper.getUserGroupMessageReceiverService().saveUserGroupMessageReceiver(userGroupMessageReceiver);
            }

            // 推送rabbitMq消息
            // 查询当前自定义群组中的用户账号
            ServiceResult<List<String>> result = ServiceImplHelper.getRefUserGroupAccountService().getUserGroupAccounts(userGroupMessage.getUserGroupId());
            if (result != null && result.getResult() != null) {
                List<String> userNames = result.getResult();
                for (String userName : userNames) {
                    // 推送rabbitMq消息
                    ServiceResult<List<AccountGroupLastContactPojo>> lastContact = ServiceImplHelper.getUserGroupService().findLastContact(userName);
                    if (lastContact != null && lastContact.getResult() != null) {
                        String json = JSON.toJSONString(lastContact);
                        // 推送消息
                        logger.info("将消息推送给用户:[{}]的mq队列中,内容为:[{}]", userName, json);
                        try {
                            ServiceImplHelper.getRabbitMqManager().basicPublish(exchange, userName, null, json.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}