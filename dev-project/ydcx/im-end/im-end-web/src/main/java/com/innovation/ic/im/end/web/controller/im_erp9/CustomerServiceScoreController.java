package com.innovation.ic.im.end.web.controller.im_erp9;

import com.google.common.base.Strings;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.*;
import com.innovation.ic.im.end.base.vo.im_erp9.CustomerServiceScoreAddVo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @desc   客服评分API
 * @author linuo
 * @time   2023年4月20日17:19:40
 */
@Api(value = "客服评分API", tags = "CustomerServiceScoreController")
@RestController
@RequestMapping("/api/v1/customerServiceScore")
@DefaultProperties(defaultFallback = "defaultFallback")
public class CustomerServiceScoreController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(CustomerServiceScoreController.class);

    @HystrixCommand
    @ApiOperation(value = "客服评分")
    @RequestMapping(value = "/addScore", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ApiImplicitParam(name = "CustomerServiceScoreAddVo", value = "新增客服评分的vo类", required = true, dataType = "CustomerServiceScoreAddVo")
    @ResponseBody
    public ResponseEntity<ApiResult> searchByRealName(@RequestBody CustomerServiceScoreAddVo customerServiceScoreAddVo) {
        if (Strings.isNullOrEmpty(customerServiceScoreAddVo.getSaleAccountId()) || Strings.isNullOrEmpty(customerServiceScoreAddVo.getScAccountId()) || customerServiceScoreAddVo.getScore() == null) {
            String message = "调用接口【/api/v1/customerServiceScore/addScore时，参数scAccountId、saleAccountId和score不能为空";
            log.warn(message);
            ApiResult<SearchAccountResultPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 新增客服评分
        ServiceResult<Boolean> serviceResult = customerServiceScoreService.addScore(customerServiceScoreAddVo);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}