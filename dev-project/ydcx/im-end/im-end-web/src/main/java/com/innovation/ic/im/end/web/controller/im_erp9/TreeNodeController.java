package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.innovation.ic.im.end.base.pojo.im_erp9.TreeNodePojo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 组织机构树形目录API
 */
@Api(value = "组织机构树形目录API", tags = "TreeNodeController")
@RestController
@RequestMapping("/api/v1/treeNode")
@DefaultProperties(defaultFallback = "defaultFallback")
public class TreeNodeController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(TreeNodeController.class);

    /**
     * 获取组织结构树形目录某个节点，其中包括下一级子节点
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "获取组织结构树形目录某个节点，其中包括下一级子节点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organizationId", value = "组织机构id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/get/{organizationId}/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> get(@PathVariable("organizationId") String organizationId, @PathVariable("accountId") String accountId, @PathVariable("username") String username,
                                         HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(organizationId) || !StringUtils.validateParameter(accountId) || !StringUtils.validateParameter(username)) {
            String message = "调用接口【/api/v1/treeNode/get/{organizationId}/{accountId}/{username}时，参数organizationId、accountId和username不能为空";
            log.warn(message);
            ApiResult<TreeNodePojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //ServiceResult<TreeNodePojo> serviceResult = treeNodeService.findById(organizationId, accountId, username);
        // 更新从redis获取数据
        String serviceResult = (String) redisManager.get(RedisStorage.TREE_NODE_PREFIX + organizationId + RedisStorage.UNDERLINE + accountId + RedisStorage.UNDERLINE + username);
        ApiResult<TreeNodePojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        //解析redis数据
        if (StringUtils.isNotEmpty(serviceResult)) {
            JSONObject json = (JSONObject) JSONObject.parse(serviceResult);
            if (json != null && !json.isEmpty()) {
                JSONObject result = (JSONObject) json.get(Constants.RESULT);
                if (result != null && !result.isEmpty()) {
                    apiResult.setResult(JSONObject.parseObject(result.toJSONString(), TreeNodePojo.class));
                }
            }
        }
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}