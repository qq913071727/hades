package com.innovation.ic.im.end.web.config;

import com.innovation.ic.im.end.base.value.config.ZookeeperParamConfig;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 使用zookeeper做分布式锁，解决多个节点同时启动相同的定时任务的问题
 */
@Configuration
public class ZookeeperConfig {
    @Resource
    ZookeeperParamConfig zookeeperParamConfig;

    private static ZookeeperParamConfig config;

    @PostConstruct
    public void init() {
        config = zookeeperParamConfig;
    }

    @Bean(initMethod = "start")
    public CuratorFramework curatorFramework() {
        return CuratorFrameworkFactory.newClient(
                config.getHost(),
                config.getSessionTimeoutMs(),
                config.getConnectionTimeoutMs(),
                new ExponentialBackoffRetry(config.getBaseSleepTimeMs(), config.getMaxEntries()));
    }
}