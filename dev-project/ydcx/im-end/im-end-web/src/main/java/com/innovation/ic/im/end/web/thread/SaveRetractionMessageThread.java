package com.innovation.ic.im.end.web.thread;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.model.im_erp9.Message;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.service.im_erp9.ChatPairService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.thread.web.SendRabbitMqMessageThread;
import com.innovation.ic.im.end.web.endpoint.Endpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @desc   保存一对一撤回消息对象内容
 * @author linuo
 * @time   2022年8月9日14:40:09
 */
public class SaveRetractionMessageThread extends AbstractThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String exchange;

    private List<Integer> loginTypeList;

    private Map<String, Endpoint> onlineMap;

    public SaveRetractionMessageThread(Message message, Map<String, Endpoint> onlineMap, String exchange, List<Integer> loginTypeList, ThreadPoolManager threadPoolManager) {
        this.message = message;
        this.onlineMap = onlineMap;
        this.exchange = exchange;
        this.loginTypeList = loginTypeList;
        this.threadPoolManager = threadPoolManager;
    }

    public SaveRetractionMessageThread() {}

    @Override
    public void run() {
        // 更新最近联系时间
        ChatPairService chatPairService = ServiceImplHelper.getChatPairService();
        chatPairService.updateLastContactTime(message.getFromUserAccount(), message.getToUserAccount());

        logger.info("将用户【" + message.getToUserAccount() + "】的消息【" + message.getContent() + "】保存到消息表message中");

        // 保存一对一消息
        ServiceResult<Integer> result = ServiceImplHelper.getMessageService().saveMessage(this.message);
        if(result != null && result.getResult() != null){
            message.setId(result.getResult());
        }

/*        if(loginTypeList != null && loginTypeList.size() > 0){
            for (int i = 0; i < loginTypeList.size(); i++) {
                LoginTypeVo loginTypeVo = new LoginTypeVo(message.getToUserAccount(), loginTypeList.get(i));
                String usernameStr = JSONObject.toJSON(loginTypeVo).toString();

                // 给消息接收人推送消息
                if (null != onlineMap.get(usernameStr)
                        && null != onlineMap.get(usernameStr).session) {
                    Session toSession = onlineMap.get(usernameStr).session;
                    // 将消息发送给客户端B
                    synchronized (toSession) {
                        if (toSession.isOpen()) {
                            SendMessageVo sendMessageVo = new SendMessageVo();
                            BeanUtils.copyProperties(message, sendMessageVo);
                            logger.info("sendMessageVo=[{}]", sendMessageVo);
                            toSession.getAsyncRemote().sendText(JSON.toJSONString(sendMessageVo));
                        }
                    }
                    logger.info("向用户【" + message.getToUserAccount() + "】，发送了系统消息【" + message.getContent() + "】");
                }
            }
        }*/

        // 推送rabbitMq消息
        List<String> list = new ArrayList<>();
        list.add(message.getToUserAccount());
        list.add(message.getFromUserAccount());
        SendRabbitMqMessageThread sendRabbitMqMessageThread = new SendRabbitMqMessageThread(list, exchange);
        threadPoolManager.execute(sendRabbitMqMessageThread);
    }
}