package com.innovation.ic.im.end.web.controller.im_erp9;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import com.innovation.ic.im.end.base.thread.data.InsertUserCurrentDataToRedisThread;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupAddMemberVo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.jcraft.jsch.JSchException;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.minio.errors.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户自定义群组和账号的关系API
 */
@Api(value = "用户自定义群组和账号的关系API", tags = "RefUserGroupAccountController")
@RestController
@RequestMapping("/api/v1/refUserGroupAccount")
@DefaultProperties(defaultFallback = "defaultFallback")
public class RefUserGroupAccountController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RefUserGroupAccountController.class);

    /**
     * 添加用户自定义群组和账号的关系
     *
     * @return 返回添加结果
     */
    @HystrixCommand
    @ApiOperation(value = "添加用户自定义群组和账号的关系")
    @ApiImplicitParam(name = "userGroupAddMemberVo", value = "用户自定义群组添加成员vo类", required = true, dataType = "UserGroupAddMemberVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> add(@RequestBody UserGroupAddMemberVo userGroupAddMemberVo, HttpServletRequest request, HttpServletResponse response) {
        if (null == userGroupAddMemberVo || null == userGroupAddMemberVo.getNewMemberAccountIdList() || userGroupAddMemberVo.getNewMemberAccountIdList().size() == 0 || null == userGroupAddMemberVo.getUserGroupId()) {
            String message = "调用接口【/api/v1/refUserGroupAccount/add】时，参数newMemberAccountIdList不能为空";
            log.warn(message);
            ApiResult<JSONObject> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 添加自定义群组成员
        refUserGroupAccountService.addRefUserGroupMember(userGroupAddMemberVo);

        // 根据自定义群组id获取自定义群组信息
        ServiceResult<JSONObject> serviceResult = refUserGroupAccountService.getUserGroupInfo(userGroupAddMemberVo.getUserGroupId());

        ApiResult<JSONObject> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 设置某个用户对某个自定义群组消息免打扰
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "设置某个用户对某个自定义群组消息免打扰")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "群组id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "status", value = "状态。1表示设置为消息免打扰。0表示取消消息免打扰", required = true, dataType = "Integer")
    })
    @RequestMapping(value = "/noReminder/{userGroupId}/{accountId}/{username}/{status}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> noReminder(@PathVariable("userGroupId") Integer userGroupId,
                                                @PathVariable("accountId") String accountId,
                                                @PathVariable("username") String username,
                                                @PathVariable("status") Integer status,
                                                HttpServletRequest request, HttpServletResponse response) {
        if (null == userGroupId || !StringUtils.validateParameter(accountId)
                || !StringUtils.validateParameter(username) || null == status) {
            String message = "调用接口【/api/v1/refUserGroupAccount/noReminder/{userGroupId}/{accountId}/{username}/{status}时，参数userGroupId、accountId、username和status不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = refUserGroupAccountService.noReminder(userGroupId, accountId, username, status);
        if (serviceResult.getResult()) {
            // 将用户最近联系人的信息导入redis
            List<String> userNameList = new ArrayList<>();
            userNameList.add(username);
            InsertUserCurrentDataToRedisThread insertUserCurrentDataToRedisThread = new InsertUserCurrentDataToRedisThread(userNameList, redisManager, userGroupService, Boolean.TRUE, rabbitMqParamConfig);
            threadPoolManager.execute(insertUserCurrentDataToRedisThread);
        }

        ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 将某个用户对某个自定义群组设置为置顶
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "将某个用户对某个自定义群组设置为置顶")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "群组id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "status", value = "状态。1表示设置为置顶。0表示取消置顶", required = true, dataType = "Integer")
    })
    @RequestMapping(value = "/topping/{userGroupId}/{accountId}/{username}/{status}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> topping(@PathVariable("userGroupId") Integer userGroupId,
                                             @PathVariable("accountId") String accountId,
                                             @PathVariable("username") String username,
                                             @PathVariable("status") Integer status,
                                             HttpServletRequest request, HttpServletResponse response) {
        if (null == userGroupId || !StringUtils.validateParameter(accountId)
                || !StringUtils.validateParameter(username)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/topping/{userGroupId}/{accountId}/{username}/{status}时，参数userGroupId、accountId、username和status不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = refUserGroupAccountService.topping(userGroupId, accountId, username, status);
        if (serviceResult.getResult()) {
            // 将用户最近联系人的信息导入redis
            List<String> userNameList = new ArrayList<>();
            userNameList.add(username);
            InsertUserCurrentDataToRedisThread insertUserCurrentDataToRedisThread = new InsertUserCurrentDataToRedisThread(userNameList, redisManager, userGroupService, Boolean.TRUE, rabbitMqParamConfig);
            threadPoolManager.execute(insertUserCurrentDataToRedisThread);
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据用户自定义群组id、账号id和用户账号退出用户自定义群组。
     * 删除user_group_message、user_group_offline_message、ref_user_group_account表中对应的记录，
     * 删除文件服务器上对应的文件和图片服务器上对应的图片
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据用户自定义群组id、账号id和用户账号退出用户自定义群组。删除user_group_message、user_group_offline_message、ref_user_group_account表中对应的记录，删除文件服务器上对应的文件和图片服务器上对应的图片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "群组id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/exit/{userGroupId}/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> exit(@PathVariable("userGroupId") Integer userGroupId,
                                          @PathVariable("accountId") String accountId,
                                          @PathVariable("username") String username,
                                          HttpServletRequest request, HttpServletResponse response) throws IOException, ServerException, NoSuchAlgorithmException, InsufficientDataException, InvalidResponseException, InternalException, XmlParserException, ErrorResponseException, InvalidKeyException, JSchException {
        if (null == userGroupId || !StringUtils.validateParameter(accountId)
                || !StringUtils.validateParameter(username)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/exit/{userGroupId}/{accountId}/{username}时，参数userGroupId、accountId和username不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = refUserGroupAccountService.exit(userGroupId, accountId, username);
        if (serviceResult.getResult()) {
            // 将用户最近联系人的信息导入redis
            List<String> userNameList = new ArrayList<>();
            userNameList.add(username);
            InsertUserCurrentDataToRedisThread insertUserCurrentDataToRedisThread = new InsertUserCurrentDataToRedisThread(userNameList, redisManager, userGroupService, Boolean.TRUE, rabbitMqParamConfig);
            threadPoolManager.execute(insertUserCurrentDataToRedisThread);
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 返回某个自定义群组的详细信息，包括：主键，群组id，群组名称，是否消息免打扰，置顶时间，成员列表
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "返回某个自定义群组的详细信息，包括：主键，群组id，群组名称，是否消息免打扰，置顶时间，成员列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "自定义群组id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "accountId", value = "账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/detail/{userGroupId}/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> detail(@PathVariable("userGroupId") String userGroupId,
                                            @PathVariable("accountId") String accountId,
                                            @PathVariable("username") String username,
                                            HttpServletRequest request, HttpServletResponse response) throws ParseException {
        if (StringUtils.isEmpty(userGroupId) || StringUtils.isEmpty(accountId) || StringUtils.isEmpty(username)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/detail/{userGroupId}/{accountId}/{username}时，参数userGroupId、accountId、username不能为空";
            log.warn(message);
            ApiResult<GroupPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 查询自定义群组信息
        //Map<String, Object> map = new HashMap<>();
        //map.put(Constants.USER_GROUP_ID, userGroupId);
        //map.put(Constants.ACCOUNT_ID, accountId);
        //map.put(Constants.USER_NAME, username);
        //ServiceResult<GroupPojo> result = refUserGroupAccountService.getGroupDetailInfoByParam(map);
        //获取数据
        String detail = (String) redisManager.get(RedisStorage.REF_USER_GROUP_ACCOUNT_DETAIL_PREFIX + userGroupId +
                RedisStorage.UNDERLINE + accountId +
                RedisStorage.UNDERLINE + username);
        ApiResult<GroupPojo> apiResult = new ApiResult<GroupPojo>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        //解析数据
        JSONObject json = (JSONObject) JSONObject.parse(detail);
        if (json != null && !json.isEmpty()) {
            apiResult.setResult(JSONObject.parseObject(json.toJSONString(), GroupPojo.class));
        }
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户username在组userGroupId中某个时间之前的聊天记录，对username不可见
     *
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "用户username在自定义群组userGroupId中某个时间之前的聊天记录，对username不可见")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "自定义群组账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "accountId", value = "用户账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "用户账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/invisible/{userGroupId}/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> invisible(@PathVariable("userGroupId") String userGroupId,
                                               @PathVariable("accountId") String accountId,
                                               @PathVariable("username") String username,
                                               HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(userGroupId) || StringUtils.isEmpty(accountId)
                || StringUtils.isEmpty(username)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/invisible/{userGroupId}/{accountId}/{username}时，参数userGroupId、accountId、username不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 根据用户设置自定义群组聊天内容不可见时间
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.USER_GROUP_ID, userGroupId);
        map.put(Constants.ACCOUNT_ID, accountId);
        map.put(Constants.USER_NAME, username);
        ServiceResult<Boolean> result = refUserGroupAccountService.invisibleChatRecordSet(map);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        apiResult.setResult(result.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 将自定义群组groupId从用户username的最近联系人中删除，并且清除当前时间之前用户username可看到的群聊天记录
     *
     * @return 返回删除结果
     */
    @HystrixCommand
    @ApiOperation(value = "将自定义群组groupId从用户username的最近联系人中删除，并且清除当前时间之前用户username可看到的群聊天记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "自定义群组账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "accountId", value = "用户账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "用户账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/deleteUserGroupChat/{userGroupId}/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> deleteUserGroupChat(@PathVariable("userGroupId") String userGroupId,
                                                         @PathVariable("accountId") String accountId,
                                                         @PathVariable("username") String username,
                                                         HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(userGroupId) || StringUtils.isEmpty(accountId)
                || StringUtils.isEmpty(username)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/deleteUserGroupChat/{userGroupId}/{accountId}/{username}时，参数userGroupId、accountId、username不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 删除最近联系自定义群组并清除聊天记录
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.USER_GROUP_ID, userGroupId);
        map.put(Constants.ACCOUNT_ID, accountId);
        map.put(Constants.USER_NAME, username);
        ServiceResult<Boolean> result = refUserGroupAccountService.deleteUserGroupChat(map);
        if (result.getResult()) {
            // 将用户最近联系人的信息导入redis
            List<String> userNameList = new ArrayList<>();
            userNameList.add(username);
            InsertUserCurrentDataToRedisThread insertUserCurrentDataToRedisThread = new InsertUserCurrentDataToRedisThread(userNameList, redisManager, userGroupService, Boolean.TRUE, rabbitMqParamConfig);
            threadPoolManager.execute(insertUserCurrentDataToRedisThread);
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        apiResult.setResult(result.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据自定义群组Id获取自定义群组的人数
     *
     * @param userGroupId
     * @param request
     * @param response
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据自定义群组Id获取自定义群组中的人数")
    @ApiImplicitParam(name = "userGroupId", value = "自定义群组id", required = true, dataType = "String")
    @RequestMapping(value = "/count/{userGroupId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> count(@PathVariable("userGroupId") String userGroupId, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(userGroupId)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/count/{userGroupId}时，参数userGroupId不能为空";
            log.warn(message);
            ApiResult<Integer> apiResult = new ApiResult<Integer>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 根据自定义群组id获取群组人数
        //ServiceResult<Integer> result = refUserGroupAccountService.getCountByUserGroupId(userGroupId);
        String userGroupCount = (String) redisManager.get(RedisStorage.REF_USER_GROUP_ACCOUNT_BY_GROUP_ID_COUNT_PREFIX + userGroupId);
        ApiResult<Integer> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        if (StringUtils.isNotEmpty(userGroupCount)){
            apiResult.setResult(Integer.valueOf(userGroupCount));
        }else {
            apiResult.setResult(0);
        }
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 更新用户username自定义群组userGroupId的最近联系时间
     *
     * @return 返回更新结果
     */
    @HystrixCommand
    @ApiOperation(value = "更新用户username自定义群组userGroupId的最近联系时间")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userGroupId", value = "自定义群组账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "accountId", value = "用户账号id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "username", value = "用户账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/updateLastContactTime/{userGroupId}/{accountId}/{username}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> updateLastContactTime(@PathVariable("userGroupId") String userGroupId,
                                                           @PathVariable("accountId") String accountId,
                                                           @PathVariable("username") String username,
                                                           HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(userGroupId) || StringUtils.isEmpty(accountId) || StringUtils.isEmpty(username)) {
            String message = "调用接口【/api/v1/refUserGroupAccount/updateLastContactTime/{userGroupId}/{accountId}/{username}时，参数userGroupId、accountId、username不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 更新用户username自定义群组userGroupId的最近联系时间
        ServiceResult<Boolean> result = refUserGroupAccountService.updateLastContactTime(userGroupId, accountId, username);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(result.getMessage());
        apiResult.setResult(result.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}