package com.innovation.ic.im.end.web.config;

import com.innovation.ic.b1b.framework.manager.*;
import com.innovation.ic.im.end.base.value.config.MinioConfig;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.base.value.config.SftpParamConfig;
import com.innovation.ic.im.end.base.value.config.ThreadPoolConfig;
import io.minio.MinioClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

@Configuration
public class ManagerConfig {

    /****************************************** redis *********************************************/
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setStringSerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }

    @Bean
    public RedisManager redisManager() {
        return new RedisManager(redisTemplate);
    }

    /****************************************** minio *********************************************/
    @Resource
    private MinioConfig minioConfig;

    @Resource
    private MinioClient minioClient;

    /**
     * 注入minio 客户端
     *
     * @return
     */
    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(minioConfig.getEndpoint())
                .credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey())
                .build();
    }

    @Bean
    public MinioManager minioManager() {
        return new MinioManager(minioClient);
    }

    /****************************************** rabbitmq *********************************************/
    @Resource
    private RabbitMqParamConfig rabbitMqParamConfig;

    @Bean
    public RabbitMqManager rabbitMqManager() {
        return new RabbitMqManager(rabbitMqParamConfig.getHost(), rabbitMqParamConfig.getPort(),
                rabbitMqParamConfig.getUsername(), rabbitMqParamConfig.getPassword(),
                rabbitMqParamConfig.getVirtualHost());
    }

    /****************************************** sftp *********************************************/
    @Resource
    private SftpParamConfig sftpParamConfig;

    @Bean
    public SftpChannelManager sftpChannelManager() {
        return new SftpChannelManager(sftpParamConfig.getUsername(), sftpParamConfig.getPassword(),
                sftpParamConfig.getHost(), sftpParamConfig.getPort(), sftpParamConfig.getTimeout());
    }

    /**************************************** thread pool *******************************************/
    @Resource
    private ThreadPoolConfig threadPoolConfig;

    @Bean
    public ThreadPoolManager threadPoolManager() {
        return new ThreadPoolManager(threadPoolConfig.getCorePoolSize(),
                threadPoolConfig.getMaximumPoolSize(), threadPoolConfig.getKeepAliveTime(),
                threadPoolConfig.getQueueSize(), threadPoolConfig.getThreadNamePrefix());
    }
}
