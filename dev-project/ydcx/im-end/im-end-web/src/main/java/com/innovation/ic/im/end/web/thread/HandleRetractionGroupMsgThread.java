package com.innovation.ic.im.end.web.thread;

import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.model.im_erp9.GroupMessage;
import com.innovation.ic.im.end.base.pojo.constant.MessageType;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.pojo.enums.AvailableStatusEnum;
import com.innovation.ic.im.end.base.pojo.im_erp9.RetractionPojo;
import com.innovation.ic.im.end.base.service.im_erp9.GroupMessageReceiverService;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.thread.web.SendWithdrawRabbitMqMessageThread;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.base.value.config.ZookeeperParamConfig;
import com.innovation.ic.im.end.base.vo.im_erp9.GroupRetractionVo;
import com.innovation.ic.im.end.web.endpoint.GroupEndpoint;
import lombok.SneakyThrows;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author linuo
 * @desc 处理默认群组撤回消息数据
 * @time 2022年10月18日10:25:29
 */
public class HandleRetractionGroupMsgThread extends AbstractThread implements Runnable {


    public HandleRetractionGroupMsgThread(GroupMessageReceiverService groupMessageReceiverService, GroupRetractionVo groupRetractionVo, RabbitMqParamConfig rabbitMqParamConfig, ZookeeperParamConfig zookeeperParamConfig, ThreadPoolManager threadPoolManager) {
        this.groupMessageReceiverService = groupMessageReceiverService;
        this.groupRetractionVo = groupRetractionVo;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
        this.zookeeperParamConfig = zookeeperParamConfig;
        this.threadPoolManager = threadPoolManager;
    }

    @SneakyThrows
    @Override
    public void run() {
        RetractionPojo retractionPojo = groupMessageReceiverService.retractionGroupMessageReceiver(groupRetractionVo);
        //通过GroupId 获取对应的人
        List<String> refGroupAccountList = groupMessageReceiverService.findUsername(retractionPojo.getGroupMessage().getGroupId(), retractionPojo.getGroupMessage().getFromUserAccount());
        SendWithdrawRabbitMqMessageThread sendWithdrawRabbitMqMessageThread = new SendWithdrawRabbitMqMessageThread(refGroupAccountList, retractionPojo, rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.RETRACTION_EXCHANGE));
        threadPoolManager.execute(sendWithdrawRabbitMqMessageThread);

        // 给群组中接收消息的人发送通知消息
        saveRetractionGroupMsgAndSendToAccount(retractionPojo.getGroupMessage());
    }

    /**
     * 给群组中接收消息的人发送通知消息
     *
     * @param groupMessage 默认群组消息
     */
    private void saveRetractionGroupMsgAndSendToAccount(GroupMessage groupMessage) {
        Map<String, Set<GroupEndpoint>> onlineGroupMap = GroupEndpoint.onlineGroupMap;
        CuratorFramework curatorFramework = GroupEndpoint.curatorFramework;

        GroupMessage gm = new GroupMessage();
        gm.setGroupId(groupMessage.getGroupId());
        gm.setFromUserAccount(groupMessage.getFromUserAccount());
        gm.setFromUserRealName(groupMessage.getFromUserRealName());
        if (!Strings.isNullOrEmpty(groupMessage.getFromUserRealName())) {
            gm.setContent(groupMessage.getFromUserRealName() + "撤回了一条消息");
        } else {
            gm.setContent(groupMessage.getFromUserAccount() + "撤回了一条消息");
        }
        gm.setType(MessageType.SYS_MSG);
        gm.setFromUserAvailable(AvailableStatusEnum.AVAILABLE.getCode());
        gm.setRetractionContent(groupMessage.getContent());
        gm.setCreateTime(groupMessage.getCreateTime());
        gm.setRetractionMsgId(groupMessage.getId());

        // 锁等待时间
        Long waitingLockTime = zookeeperParamConfig.getWaitingLockTime();

        SaveRetractionGroupMessageThread saveRetractionGroupMessageThread = new SaveRetractionGroupMessageThread(gm, onlineGroupMap,
                rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.CURRENT_EXCHANGE), curatorFramework, waitingLockTime, this.threadPoolManager);
        threadPoolManager.execute(saveRetractionGroupMessageThread);
    }
}