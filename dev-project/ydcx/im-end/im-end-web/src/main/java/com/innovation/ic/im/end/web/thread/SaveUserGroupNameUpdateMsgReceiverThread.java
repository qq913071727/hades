package com.innovation.ic.im.end.web.thread;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.im.end.base.model.im_erp9.RefUserGroupAccount;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessage;
import com.innovation.ic.im.end.base.model.im_erp9.UserGroupMessageReceiver;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.enums.AvailableStatusEnum;
import com.innovation.ic.im.end.base.pojo.enums.ReadStatusEnum;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountGroupLastContactPojo;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @desc   保存自定义群组修改群名的未读消息
 * @author linuo
 * @time   2022年10月18日10:39:10
 */
public class SaveUserGroupNameUpdateMsgReceiverThread extends AbstractThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Integer userGroupMsgId;
    private String exchange;

    public SaveUserGroupNameUpdateMsgReceiverThread(Integer userGroupMsgId, UserGroupMessage userGroupMessage, String exchange) {
       this.userGroupMsgId = userGroupMsgId;
       this.userGroupMessage = userGroupMessage;
       this.exchange = exchange;
    }

    @Override
    public void run() {
        logger.info("保存自定义群组:[{}]未读消息", userGroupMsgId);
        ServiceResult<List<RefUserGroupAccount>> serviceResult = ServiceImplHelper.getRefUserGroupAccountService().findByUserGroupIdAndNotInList(userGroupMessage.getUserGroupId(), null);
        if (null != serviceResult && null != serviceResult.getResult()) {
            List<RefUserGroupAccount> refUserGroupAccountList = serviceResult.getResult();
            for (int i = 0; i < refUserGroupAccountList.size(); i++) {
                UserGroupMessageReceiver userGroupMessageReceiver = new UserGroupMessageReceiver();
                userGroupMessageReceiver.setUserGroupId(userGroupMessage.getUserGroupId());
                userGroupMessageReceiver.setUserGroupMessageId(userGroupMsgId);
                userGroupMessageReceiver.setToUserAccount(refUserGroupAccountList.get(i).getUsername());
                userGroupMessageReceiver.setToUserRealName(getUserRealNameByAccount(refUserGroupAccountList.get(i).getUsername()));
                userGroupMessageReceiver.setRead(ReadStatusEnum.READ.getCode());
                userGroupMessageReceiver.setReadTime(new Date(System.currentTimeMillis()));
                userGroupMessageReceiver.setToUserAvailable(AvailableStatusEnum.AVAILABLE.getCode());
                ServiceImplHelper.getUserGroupMessageReceiverService().saveUserGroupMessageReceiver(userGroupMessageReceiver);
            }

            // 推送rabbitMq消息
            // 查询当前自定义群组中除自己之外的用户账号
            ServiceResult<List<String>> result = ServiceImplHelper.getRefUserGroupAccountService().getUserGroupAccounts(userGroupMessage.getUserGroupId());
            if (result != null && result.getResult() != null) {
                List<String> userNames = result.getResult();
                for (int i = 0; i < userNames.size(); i++) {
                    String userName = userNames.get(i);
                    // 推送rabbitMq消息
                    ServiceResult<List<AccountGroupLastContactPojo>> lastContact = ServiceImplHelper.getUserGroupService().findLastContact(userName);
                    if (lastContact != null && lastContact.getResult() != null) {
                        String json = JSON.toJSONString(lastContact);
                        // 推送消息
                        logger.info("将消息推送给用户:[{}]的mq队列中,内容为:[{}]", userName, json);
                        try {
                            ServiceImplHelper.getRabbitMqManager().basicPublish(exchange, userName, null, json.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}