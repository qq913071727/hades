package com.innovation.ic.im.end.web.thread;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.im.end.base.model.im_erp9.*;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.pojo.enums.AvailableStatusEnum;
import com.innovation.ic.im.end.base.pojo.enums.ReadStatusEnum;
import com.innovation.ic.im.end.base.pojo.im_erp9.AccountGroupLastContactPojo;
import com.innovation.ic.im.end.base.service.helper.ServiceImplHelper;
import com.innovation.ic.im.end.base.thread.AbstractThread;
import com.innovation.ic.im.end.base.vo.im_erp9.UserGroupMessageVo;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.List;

/**
 * @desc   保存自定义群组未读消息数据
 * @author linuo
 * @time   2022年10月18日09:26:07
 */
public class SaveUserGroupMessageReceiverThread extends AbstractThread implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Integer userGroupMsgId;
    private String exchange;
    private List<String> onlineAccountList;

    public SaveUserGroupMessageReceiverThread(UserGroupMessage userGroupMessage, UserGroupMessageVo userGroupMessageVo, Integer userGroupMsgId, String exchange, List<String> onlineAccountList) {
        this.userGroupMessage = userGroupMessage;
        this.userGroupMessageVo = userGroupMessageVo;
        this.userGroupMsgId = userGroupMsgId;
        this.exchange = exchange;
        this.onlineAccountList = onlineAccountList;
    }

    @SneakyThrows
    @Override
    public void run() {
        ServiceResult<List<RefUserGroupAccount>> serviceResult = ServiceImplHelper.getRefUserGroupAccountService().findByUserGroupIdAndNotInList(userGroupMessage.getUserGroupId(), onlineAccountList);
        if (null != serviceResult && null != serviceResult.getResult()) {
            List<RefUserGroupAccount> refUserGroupAccountList = serviceResult.getResult();
            for (RefUserGroupAccount refUserGroupAccount : refUserGroupAccountList) {
                UserGroupMessageReceiver userGroupMessageReceiver = new UserGroupMessageReceiver();
                userGroupMessageReceiver.setUserGroupId(userGroupMessageVo.getUserGroupId());
                userGroupMessageReceiver.setUserGroupMessageId(userGroupMsgId);
                userGroupMessageReceiver.setToUserAccount(refUserGroupAccount.getUsername());
                userGroupMessageReceiver.setToUserRealName(getUserRealNameByAccount(refUserGroupAccount.getUsername()));
                userGroupMessageReceiver.setRead(ReadStatusEnum.UN_READ.getCode());
                userGroupMessageReceiver.setToUserAvailable(AvailableStatusEnum.AVAILABLE.getCode());
                ServiceImplHelper.getUserGroupMessageReceiverService().saveUserGroupMessageReceiver(userGroupMessageReceiver);
            }

            // 推送rabbitMq消息
            // 查询当前自定义群组中的用户账号
            ServiceResult<List<String>> result = ServiceImplHelper.getRefUserGroupAccountService().getUserGroupAccounts(userGroupMessage.getUserGroupId());
            if (result != null && result.getResult() != null) {
                List<String> userNames = result.getResult();
                for (String userName : userNames) {
                    // 推送rabbitMq消息
                    ServiceResult<List<AccountGroupLastContactPojo>> lastContact = ServiceImplHelper.getUserGroupService().findLastContact(userName);
                    if (lastContact != null && lastContact.getResult() != null) {
                        JSONObject json = (JSONObject) JSONObject.parse(JSON.toJSONString(lastContact));

                        // 如果自己发的消息，并且推送给自己，说明没有新消息
                        if (userGroupMessage.getFromUserAccount().equals(userName)) {
                            json.put(Constants.IF_HAVE_NEW_MSG_FIELD, 0);
                        } else {
                            json.put(Constants.IF_HAVE_NEW_MSG_FIELD, 1);
                        }

                        // 推送消息
                        logger.info("将消息推送给用户:[{}]的mq队列中,内容为:[{}]", userName, json.toJSONString());
                        try {
                            ServiceImplHelper.getRabbitMqManager().basicPublish(exchange, userName, null, json.toJSONString().getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}