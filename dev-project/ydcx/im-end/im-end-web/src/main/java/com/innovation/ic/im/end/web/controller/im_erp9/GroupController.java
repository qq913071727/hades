package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.im_erp9.GroupPojo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 群组API
 */
@Api(value = "群组API", tags = "GroupController")
@RestController
@RequestMapping("/api/v1/group")
@DefaultProperties(defaultFallback = "defaultFallback")
public class GroupController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(GroupController.class);

    /**
     * 这个接口不再用了
     * 获取有消息的群组和用户自定义群组。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量，显示消息免打扰
     * @return
     */
//    @HystrixCommand
//    @ApiOperation(value = "获取有消息的群组和用户自定义群组。按置顶顺序降序排列，按最后聊天时间降序排列。显示未读消息数量，显示消息免打扰")
//    @ApiImplicitParam(name = "account", value = "账号", required = true, dataType = "String")
//    @RequestMapping(value = "/current/{account}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult> current(@PathVariable("account") String account, HttpServletRequest request, HttpServletResponse response) {
//        if (StringUtils.isEmpty(account)) {
//            String message = "调用接口【/api/v1/group/current/{account}时，参数account不能为空";
//            log.warn(message);
//            ApiResult<List<GroupPojo>> apiResult = new ApiResult<>();
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        ServiceResult<List<GroupPojo>> serviceResult = groupService.findLastContact(account);
//
//        ApiResult<List<GroupPojo>> apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setResult(serviceResult.getResult());
//        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }

    /**
     * 根据群组Id获取默认群组中在线人数
     * @param groupId 群组id
     * @param request 请求体
     * @param response 返回体
     * @return 群组中在线人数
     */
    @HystrixCommand
    @ApiOperation(value = "根据群组Id获取默认群组中在线人数")
    @ApiImplicitParam(name = "groupId", value = "群组id", required = true, dataType = "String")
    @RequestMapping(value = "/userNumberOnline/{groupId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> userNumberOnline(@PathVariable("groupId") String groupId, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(groupId)) {
            String message = "调用接口【/api/v1/group/userNumberOnline/{groupId}时，参数groupId不能为空";
            log.warn(message);
            ApiResult<Long> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 根据群组id获取在线人员数量
        ServiceResult<Long> serviceResult = groupService.getUserNumberOnlineFromRedis(groupId);

        ApiResult<Long> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}