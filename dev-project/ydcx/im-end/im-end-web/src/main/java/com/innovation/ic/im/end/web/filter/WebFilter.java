package com.innovation.ic.im.end.web.filter;

import com.innovation.ic.b1b.framework.util.IpAddressUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@javax.servlet.annotation.WebFilter(filterName = "sessionFilter",urlPatterns = "/*")
@Order(1)
@Component
public class WebFilter implements Filter {


    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req= (HttpServletRequest) request;
        String ipAdrress = IpAddressUtil.getIpAddress(req);
        req.getSession().setAttribute("ip", ipAdrress);
//        logger.info("过滤器中显示的ip是"+ipAdrress);
        chain.doFilter(request,response);
    }
}