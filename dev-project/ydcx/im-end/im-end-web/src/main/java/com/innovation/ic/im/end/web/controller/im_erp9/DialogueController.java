package com.innovation.ic.im.end.web.controller.im_erp9;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.im.end.base.model.im_erp9.Dialogue;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.vo.im_erp9.DialogueVo;
import com.innovation.ic.im.end.web.controller.AbstractController;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 话术API
 */
@Api(value = "话术API", tags = "DialogueController")
@RestController
@RequestMapping("/api/v1/dialogue")
@DefaultProperties(defaultFallback = "defaultFallback")
public class DialogueController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(DialogueController.class);

    /**
     * 添加话术
     * @return 返回添加结果
     */
    @HystrixCommand
    @ApiOperation(value = "添加话术")
    @ApiImplicitParam(name = "dialogueVo", value = "话术的vo类", required = true, dataType = "DialogueVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> add(@RequestBody DialogueVo dialogueVo, HttpServletRequest request, HttpServletResponse response) {
        if (null == dialogueVo || !StringUtils.validateParameter(dialogueVo.getGroup())
                || !StringUtils.validateParameter(dialogueVo.getContent())
                || !StringUtils.validateParameter(dialogueVo.getAccountId())) {
            String message = "调用接口【/api/v1/dialogue/add时，参数accountId、group和content不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (dialogueVo.getContent().length()>200){
            String message = "调用接口【/api/v1/dialogue/add时，参数content字符不能超过200";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = dialogueService.saveDialogue(dialogueVo.toDialogue(true));

        ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 修改话术
     * @return 返回修改结果
     */
    @HystrixCommand
    @ApiOperation(value = "修改话术")
    @ApiImplicitParam(name = "dialogueVo", value = "话术的vo类", required = true, dataType = "DialogueVo")
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> update(@RequestBody DialogueVo dialogueVo, HttpServletRequest request, HttpServletResponse response) {
        if (null == dialogueVo || null == dialogueVo.getId()
                || !StringUtils.validateParameter(dialogueVo.getGroup())
                || !StringUtils.validateParameter(dialogueVo.getContent())) {
            String message = "调用接口【/api/v1/dialogue/update时，参数id、group、content不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (dialogueVo.getContent().length()>200){
            String message = "调用接口【/api/v1/dialogue/update时，参数content字符不能超过200";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = dialogueService.updateDialogueById(dialogueVo.toDialogue(false));

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除话术
     * @return 返回删除结果
     */
    @HystrixCommand
    @ApiOperation(value = "删除话术")
    @ApiImplicitParam(name = "id", value = "话术的id", required = true, dataType = "Integer")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> delete(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        if (null == id) {
            String message = "调用接口【/api/v1/dialogue/delete/{id}时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = dialogueService.deleteDialogueById(id);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 话术置顶
     * @return 返回置顶结果
     */
    @HystrixCommand
    @ApiOperation(value = "话术置顶")
    @ApiImplicitParam(name = "id", value = "话术的id", required = true, dataType = "Integer")
    @RequestMapping(value = "/topping/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> topping(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        if (null == id) {
            String message = "调用接口【/api/v1/dialogue/topping/{id}时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = dialogueService.toppingDialogueById(id);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 按照分组和用户id，返回话术。已置顶的按照置顶时间降序排列，没置顶的按照创建时间升序排列
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "按照分组和用户id，返回话术。已置顶的按照置顶时间降序排列，没置顶的按照创建时间升序排列")
    @ApiImplicitParam(name = "dialogueVo", value = "话术的vo类", required = true, dataType = "DialogueVo")
    @RequestMapping(value = "/findByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> findDialogueByAccountIdOrderByToppingTimeDescAndCreateTimeAsc(@RequestBody DialogueVo dialogueVo, HttpServletRequest request, HttpServletResponse response) {
        if (null == dialogueVo || !StringUtils.validateParameter(dialogueVo.getGroup())
                || !StringUtils.validateParameter(dialogueVo.getAccountId())) {
            String message = "调用接口【/api/v1/dialogue/findByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc时，参数group和erp9AdminsId不能为空";
            log.warn(message);
            ApiResult<List<Dialogue>> apiResult = new ApiResult<List<Dialogue>>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<List<Dialogue>> serviceResult = dialogueService.findRedisByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc(dialogueVo.getGroup(), dialogueVo.getAccountId());

//        ServiceResult<List<Dialogue>> serviceResult = dialogueService.findByAccountIdAndGroupOrderByToppingTimeDescAndCreateTimeAsc(dialogueVo.getGroup(), dialogueVo.getAccountId());

        ApiResult<List<Dialogue>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据参数account（模糊查询），获取用户的常用话术，并按照创建时间降序排列，支持分页显示，置顶的在最上边
     * @return
     */
    @HystrixCommand
    @ApiOperation(value = "根据参数account（模糊查询），获取用户的常用话术，并按照创建时间降序排列，支持分页显示，置顶的在最上边")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account", value = "用户账号", required = true, dataType = "String")
    })
    @RequestMapping(value = "/searchAndPageByType/{account}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> searchAndPageByType(@PathVariable("account") String account, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(account)) {
            String message = "调用接口【/api/v1/dialogue/searchAndPageByType/{account}，参数account不能为空";
            log.warn(message);
            ApiResult<List<Dialogue>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ApiResult<List<Dialogue>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}