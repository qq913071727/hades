/**
 * 客户端和服务端的通信对象
 */
class Message {
    constructor(content, fromUserAccount, toUserAccount, type) {
        /**
         * 消息内容
         */
        this.content = content;

        /**
         * 发送消息的用户的账号
         */
        this.fromUserAccount = fromUserAccount;

        /**
         * 接收消息的用户的账号
         */
        this.toUserAccount = toUserAccount;

        /**
         * 消息类型。1表示文字，2表示文件，3表示图片
         */
        this.type = type;
    }
}