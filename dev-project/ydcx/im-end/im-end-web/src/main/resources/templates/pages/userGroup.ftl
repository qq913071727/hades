<!DOCTYPE html>
<head>
    <title>聊天页面</title>
    <script type="text/javascript" src="/im-end-web/static/jslib/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="/im-end-web/static/jslib/sockjs.min.js"></script>
    <script type="text/javascript" src="/im-end-web/static/jslib/stomp.min.js"></script>
    <script type="text/javascript" src="/im-end-web/static/js/message.js"></script>
    <script type="text/javascript" src="/im-end-web/static/js/constant.js"></script>
    <script type="text/javascript" src="/im-end-web/static/js/userGroup.js"></script>
    <link href="/im-end-web/static/css/group.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" href="data:;base64,=">
</head>
<body>
<div id="hz-main">
    <div id="hz-message">
        <!-- 头部 -->
        <div>
            群组id：<input id="userGroupId" type="text" />
        </div>
        <div>
            用户名：<input id="username" type="text" />
            <button id="openConnection" type="button">连接</button>
        </div>
        <div style="margin-right: 10px">
            <button id="closeConnection" type="button">关闭连接</button>
        </div>
        <hr style="margin: 0px;"/>
        <!-- 主体 -->
        <div id="hz-message-body">
        </div>
        <!-- 功能条 -->
        <div id="">
            <input id="file" type="file" />
            <input id="upload" type="button" value="上传(点两次)" />
            <button>表情</button>
            <button>图片</button>
            <button id="videoBut">视频</button>
            <button id="sendMessage" style="float: right;">发送</button>
        </div>
        <!-- 输入框 -->
        <div contenteditable="true" id="hz-message-input">

        </div>
    </div>
    <div id="hz-group">
        登录用户：<span id="talks" text="${username!}">请登录</span>
        <br/>
        在线人数:<span id="onlineCount">0</span>
        <!-- 主体 -->
        <div id="hz-group-body">

        </div>
    </div>
</div>
</body>
</html>