﻿<!DOCTYPE html>
<html>
    <head>
        <title>聊天页面</title>
        <script type = "text/javascript" src="/im-end-web/static/jslib/jquery-3.6.0.min.js"></script>
        <script type = "text/javascript" src="/im-end-web/static/jslib/sockjs.min.js"></script>
        <script type = "text/javascript" src="/im-end-web/static/jslib/stomp.min.js"></script>
        <script type = "text/javascript" src="/im-end-web/static/js/message.js"></script>
        <script type = "text/javascript" src="/im-end-web/static/js/constant.js"></script>
        <script type = "text/javascript" src="/im-end-web/static/js/talk.js"></script>
        <link rel="icon" href="data:;base64,=">
    </head>

    <body>
        <div>
            用户名：<input id="username" type="text" />
            <button id="openConnection" type="button">连接</button>
        </div>
        <br />
        <div style="margin: auto">
            接收人：<input id="toUsername" type="text" /><br />
            <textarea id="textarea" rows="5" cols="30"></textarea>
            <button id="sendMessage" type="button">发送消息</button>
        </div>
        <br />
        <input id="file" type="file" />
        <input id="upload" type="button" value="上传(点两次)" />
        <br />
        <div style="margin-right: 10px">
            <button id="closeConnection" type="button">关闭连接</button>
        </div>
        <br />
        <div id="chat"></div>
    </body>
</html> 