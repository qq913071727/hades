package com.innovation.ic.im.end.eureka;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication(scanBasePackages = {"com.innovation.ic.im.end.eureka"})
@EnableEurekaServer
public class ImEndEurekaApplication {

    private static final Logger log = LoggerFactory.getLogger(ImEndEurekaApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(ImEndEurekaApplication.class, args);

        log.info("im-end-eureka服务启动成功");
    }
}
