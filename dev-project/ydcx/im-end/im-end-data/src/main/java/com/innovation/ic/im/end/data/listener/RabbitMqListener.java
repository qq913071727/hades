package com.innovation.ic.im.end.data.listener;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.handler.im_erp9.RabbitMqHandler;
import com.innovation.ic.im.end.base.pojo.constant.RabbitMqExchangeMap;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9AdminsService;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9StructuresService;
import com.innovation.ic.im.end.base.service.im_erp9.AccountRelationService;
import com.innovation.ic.im.end.base.service.im_erp9.AccountService;
import com.innovation.ic.im.end.base.service.im_erp9.ChatPairService;
import com.innovation.ic.im.end.base.service.im_erp9.GroupService;
import com.innovation.ic.im.end.base.thread.data.*;
import com.innovation.ic.im.end.base.value.config.RabbitMqParamConfig;
import com.innovation.ic.im.end.base.value.config.SystemConfig;
import com.rabbitmq.client.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * @desc   mq队列收到消息后，调用这个监听器的方法
 * @author linuo
 * @time   2022年10月8日17:29:50
 */
@Component
@RequiredArgsConstructor
public class RabbitMqListener implements ApplicationRunner {
    @Resource
    private RabbitMqHandler rabbitMqHandler;

    @Resource
    private RabbitMqParamConfig rabbitMqParamConfig;

    @Resource
    private AccountService accountService;

    @Resource
    private ChatPairService chatPairService;

    @Resource
    private AccountRelationService accountRelationService;

    @Resource
    private GroupService groupService;

    @Resource
    private Erp9StructuresService erp9StructuresService;

    @Resource
    private Erp9AdminsService erp9AdminsService;

    @Resource
    private ThreadPoolManager threadPoolManager;

    @Resource
    private SystemConfig systemConfig;

    @Override
    public void run(ApplicationArguments args) {
        Channel channel = rabbitMqHandler.getChannel();

        String exchange = rabbitMqParamConfig.getExchange().get(RabbitMqExchangeMap.ERP_UPDATE_ACCOUNT_EXCHANGE);

        // 监听erp删除账号队列(只删除账号与群组关联关系,不删除账号信息,删除账号的逻辑依然通过每日定时任务处理)
        ListenErpDeleteAccountQueueThread listenErpDeleteAccountQueueThread = new ListenErpDeleteAccountQueueThread(channel, exchange, accountService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpDeleteAccountQueueThread);

        // 监听erp新增账号队列
        ListenErpAddAccountQueueThread listenErpAddAccountQueueThread = new ListenErpAddAccountQueueThread(channel, exchange, erp9StructuresService, groupService, erp9AdminsService, accountService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpAddAccountQueueThread);

        // 监听erp修改账号队列(变更账号所属群组)
        ListenErpEditAccountQueueThread listenErpEditAccountQueueThread = new ListenErpEditAccountQueueThread(channel, exchange, accountService, erp9StructuresService, groupService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpEditAccountQueueThread);

        // 监听erp员工信息修改队列
        ListenErpEditAccountInfoQueueThread listenErpEditAccountInfoQueueThread = new ListenErpEditAccountInfoQueueThread(channel, exchange, accountService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpEditAccountInfoQueueThread);

        // 监听erp账号状态变更队列
        ListenErpAccountStatusEditQueueThread listenErpAccountStatusEditQueueThread = new ListenErpAccountStatusEditQueueThread(channel, exchange, accountService, erp9AdminsService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpAccountStatusEditQueueThread);

        // 监听erp部门新增、修改默认群组队列
        ListenErpGroupAddEditQueueThread listenErpGroupAddEditQueueThread = new ListenErpGroupAddEditQueueThread(channel, exchange, groupService, accountService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpGroupAddEditQueueThread);

        // 监听erp部门删除默认群组队列
        ListenErpGroupDeleteQueueThread listenErpGroupDeleteQueueThread = new ListenErpGroupDeleteQueueThread(channel, exchange, groupService, accountService, rabbitMqHandler, rabbitMqParamConfig, threadPoolManager);
        threadPoolManager.execute(listenErpGroupDeleteQueueThread);

        // 监听erp新增、修改供应商协同账号队列
        ListenErpScAccountAddUpdateQueueThread listenErpScAccountAddUpdateQueueThread = new ListenErpScAccountAddUpdateQueueThread(channel, exchange, accountService, accountRelationService, chatPairService, systemConfig);
        threadPoolManager.execute(listenErpScAccountAddUpdateQueueThread);

        // 监听erp删除供应商协同账号队列
        ListenErpScAccountDeleteQueueThread listenErpScAccountDeleteQueueThread = new ListenErpScAccountDeleteQueueThread(channel, exchange, accountService, accountRelationService);
        threadPoolManager.execute(listenErpScAccountDeleteQueueThread);
    }
}