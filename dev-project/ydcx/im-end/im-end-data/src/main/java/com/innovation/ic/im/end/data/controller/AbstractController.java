package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.service.im_erp9.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {
    @Autowired
    protected RedisManager redisManager;

    @Autowired
    protected ThreadPoolManager threadPoolManager;

    @Autowired
    protected MessageService messageService;

    @Autowired
    protected ChatPairService chatPairService;

    @Autowired
    protected GroupMessageService groupMessageService;

    @Autowired
    protected UserGroupMessageService userGroupMessageService;

    @Autowired
    protected ClientService clientService;

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected TreeNodeService treeNodeService;

    @Autowired
    protected UserGroupService userGroupService;

    @Autowired
    protected DialogueService dialogueService;

    @Autowired
    protected RefGroupAccountService refGroupAccountService;

    @Autowired
    protected RefUserGroupAccountService refUserGroupAccountService;

    /**
     * hystrix默认处理方法
     * 其它应用调用超时时调用此方法进行异常信息抛出，防止出现长时间接口不返回结果的情况出现
     * @return 返回熔断默认结果
     */
    public ResponseEntity<ApiResult> defaultFallback() {
        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(Boolean.FALSE);
        apiResult.setCode(HttpStatus.REQUEST_TIMEOUT.value());
        apiResult.setResult(Boolean.FALSE);
        apiResult.setMessage(ServiceResult.TIME_OUT);
        return new ResponseEntity<ApiResult>(apiResult, HttpStatus.OK);
    }
}