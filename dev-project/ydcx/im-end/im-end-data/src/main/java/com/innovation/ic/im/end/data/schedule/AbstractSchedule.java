package com.innovation.ic.im.end.data.schedule;

import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9AdminsService;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9MapsStructureService;
import com.innovation.ic.im.end.base.service.erp9_sqlserver.Erp9StructuresService;
import com.innovation.ic.im.end.base.service.im_erp9.*;
import com.innovation.ic.im.end.base.value.config.ZookeeperPathDataParamConfig;
import com.innovation.ic.im.end.base.value.config.ZookeeperParamConfig;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * 定时任务的抽象类
 */
public abstract class AbstractSchedule {
    @Autowired
    protected RedisManager redisManager;

    @Autowired
    protected Erp9StructuresService erp9StructuresService;

    @Autowired
    protected Erp9AdminsService erp9AdminsService;

    @Autowired
    protected TreeNodeService treeNodeService;

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected AccountRelationService accountRelationService;

    @Autowired
    protected TempAccountService tempAccountService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected Erp9MapsStructureService erp9MapsStructureService;

    @Autowired
    protected TempChatPairService tempChatPairService;

    @Autowired
    protected RefGroupAccountService refGroupAccountService;

    @Autowired
    protected ChatPairService chatPairService;

    @Autowired
    protected CuratorFramework curatorFramework;

    @Autowired
    protected RefGroupAccountPrivilegeService refGroupAccountPrivilegeService;

    @Autowired
    protected ZookeeperPathDataParamConfig zookeeperPathDataParamConfig;

    @Autowired
    protected ZookeeperParamConfig zookeeperParamConfig;

    @Autowired
    protected MessageService messageService;

    @Autowired
    protected StringRedisTemplate stringRedisTemplate;
}