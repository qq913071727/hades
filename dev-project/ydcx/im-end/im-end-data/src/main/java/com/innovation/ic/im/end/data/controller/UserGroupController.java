package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @desc   处理用户最近联系人数据controller
 * @author linuo
 * @time   2022年10月24日11:45:08
 */
@RestController
@RequestMapping("/api/v2/userGroup")
@DefaultProperties(defaultFallback = "defaultFallback")
public class UserGroupController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(UserGroupController.class);

    /**
     * 删除api:v1:userGroup:current_开头的数据，将用户最近联系人的数据导入redis
     * @return 返回处理结果
     */
    @RequestMapping(value = "/importCurrentIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> importCurrentIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("删除api:v1:userGroup:current_开头的数据，将用户最近联系人的数据导入redis");

        // 删除api:v1:userGroup:current_开头的数据
        Boolean deleteResult = redisManager.delRedisDataByKeyPrefix(RedisStorage.USER_GROUP_CURRENT_PREFIX + "*");
        if(deleteResult){
            log.info("删除api:v1:userGroup:current_开头的数据成功");
        }

        // 将message表中的数据导入redis
        log.info("将用户最近联系人的数据导入redis");

        // 查询全部账号数据
        ServiceResult<List<String>> accountResult = accountService.selectAllAccountUserName();
        List<String> accountList = accountResult.getResult();

        ServiceResult<Boolean> serviceResult = userGroupService.importCurrentIntoRedis(accountList);
        if (serviceResult.getResult()) {
            log.info("将用户最近联系人的数据导入redis完成");
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}