//package com.innovation.ic.im.end.data.conf;
//
//import com.innovation.ic.im.end.base.value.config.MinioConfig;
//import io.minio.MinioClient;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import javax.annotation.Resource;
//
///**
// * minio客户端配置类
// */
//@Configuration
//public class MinIoClientConfig {
//    @Resource
//    MinioConfig minioConfig;
//
//    /**
//     * 注入minio 客户端
//     *
//     * @return
//     */
//    @Bean
//    public MinioClient minioClient() {
//        return MinioClient.builder()
//                .endpoint(minioConfig.getEndpoint())
//                .credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey())
//                .build();
//    }
//}