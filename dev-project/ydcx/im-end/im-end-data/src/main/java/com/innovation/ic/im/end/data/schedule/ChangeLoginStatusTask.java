package com.innovation.ic.im.end.data.schedule;

import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.concurrent.TimeUnit;

/**
 * 修改超时的erp9登录状态
 */
@Configuration
@EnableScheduling
public class ChangeLoginStatusTask extends AbstractSchedule {
    @Scheduled(fixedRate = 10000)
    private void changeLoginStatus() {
        try {
            InterProcessMutex lock = new InterProcessMutex(curatorFramework, zookeeperPathDataParamConfig.getChangeLoginStatus());
            if (lock.acquire(zookeeperParamConfig.getWaitingLockTime(), TimeUnit.SECONDS)) {
                accountService.exit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}