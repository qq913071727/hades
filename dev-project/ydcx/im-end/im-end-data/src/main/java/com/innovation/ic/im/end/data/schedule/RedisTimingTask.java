package com.innovation.ic.im.end.data.schedule;

import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
public class RedisTimingTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(RedisTimingTask.class);

    /**
     * 查询redis中所有key 判断数据是否超过15天 超过删除
     */
    @Scheduled(cron = "0 20 2 * * ?")
//    @Scheduled(fixedRate = 60000000)
//    @Scheduled(cron = "0 10 14 * * ?")
    public void redisTiming() {
        try {
            InterProcessMutex lock = new InterProcessMutex(curatorFramework, zookeeperPathDataParamConfig.getRedisTiming());
            if (lock.acquire(zookeeperParamConfig.getWaitingLockTime(), TimeUnit.SECONDS)) {
                log.info("定时任务开启：查询redis中所有key 判断是否大于十五天 超过删除");
                //获取当前时间
                long date = System.currentTimeMillis();
                Date date1 = new Date(date);
                Calendar calc = Calendar.getInstance();
                calc.setTime(date1);
                calc.add(Calendar.DATE, -15);
                Date minDate = calc.getTime();
                long time = minDate.getTime();//结束时间
                Calendar newCalc = Calendar.getInstance();
                newCalc.setTime(minDate);
                newCalc.add(Calendar.DATE, -30);
                Date newMinDate = newCalc.getTime();
                long newTime = newMinDate.getTime();//开始时间
                // 处理redis中message数据 MESSAGE
                Set<String> messageKey = redisManager.keysPrefix(RedisStorage.MESSAGE_PREFIX);
                if (messageKey == null && messageKey.size() == 0) {
                    log.info("redis中:MESSAGE_PREFIX为空");
                } else {
                    Object[] keySetData = messageKey.toArray();
                    //遍历数组
                    for (int i = 0; i < keySetData.length; i++) {
                        Set<String> messageKeyData = redisManager.zReverseRangeByScore(keySetData[i].toString(), newTime, time);
                        if (messageKeyData != null && messageKeyData.size() != 0) {
                            for (String messageKeyDatum : messageKeyData) {
                                stringRedisTemplate.opsForZSet().remove(keySetData[i].toString(), messageKeyDatum);
                            }
                        }
                    }
                }
                // 处理redis中group_message数据 GROUP_MESSAGE_PREFIX
                Set<String> groupMessageKey = redisManager.keysPrefix(RedisStorage.GROUP_MESSAGE_PREFIX);
                if (groupMessageKey == null && groupMessageKey.size() == 0) {
                    log.info("redis中:GROUP_MESSAGE_PREFIX为空");
                } else {
                    Object[] keySetData = groupMessageKey.toArray();
                    //遍历数组
                    for (int i = 0; i < keySetData.length; i++) {
                        Set<String> groupMessageKeyData = redisManager.zReverseRangeByScore(keySetData[i].toString(), newTime, time);
                        if (groupMessageKeyData != null && groupMessageKeyData.size() != 0) {
                            for (String groupMessageKeyDatum : groupMessageKeyData) {
                                stringRedisTemplate.opsForZSet().remove(keySetData[i].toString(), groupMessageKeyDatum);
                            }
                        }
                    }
                }
                // 处理redis中user_group_message数据 USER_GROUP_MESSAGE_PREFIX
                Set<String> userGroupMessageKey = redisManager.keysPrefix(RedisStorage.USER_GROUP_MESSAGE_PREFIX);
                if (userGroupMessageKey == null && userGroupMessageKey.size() == 0) {
                    log.info("redis中:USER_GROUP_MESSAGE_PREFIX为空");
                } else {
                    Object[] keySetData = userGroupMessageKey.toArray();
                    //遍历数组
                    for (int i = 0; i < keySetData.length; i++) {
                        Set<String> userGroupMessageKeyData = redisManager.zReverseRangeByScore(keySetData[i].toString(), newTime, time);
                        if (userGroupMessageKeyData != null && userGroupMessageKeyData.size() != 0) {
                            for (String userGroupMessageKeyDatum : userGroupMessageKeyData) {
                                stringRedisTemplate.opsForZSet().remove(keySetData[i].toString(), userGroupMessageKeyDatum);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}