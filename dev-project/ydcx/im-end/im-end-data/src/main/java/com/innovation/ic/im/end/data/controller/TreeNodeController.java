package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.im.end.base.model.im_erp9.Account;
import com.innovation.ic.im.end.base.model.im_erp9.Group;
import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.innovation.ic.im.end.base.thread.data.TreeNodeIntoRedisThread;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @desc   根据Group表和Account表的数据，将treeNode表中的数据导入redis
 * @author zengqinglong
 * @time   2022年11月02日10:52:12
 */
@RestController
@RequestMapping("/api/v2/treeNode")
@DefaultProperties(defaultFallback = "defaultFallback")
public class TreeNodeController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(TreeNodeController.class);

    /**
     * 删除redis中删除redis中api:v1:treeNode:get_:开头的数据，根据Group表和Account表的数据，将treeNode表中的数据导入redis
     * @return 返回处理结果
     */
    @RequestMapping(value = "/importTreeNodeIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> importTreeNodeIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("删除redis中删除redis中api:v1:treeNode:get_:开头的数据，根据Group表和Account表的数据，将treeNode表中的数据导入redis");

        // 删除redis中删除redis中api:v1:treeNode:get_:开头的数据
        Boolean deleteResult = redisManager.delRedisDataByKeyPrefix(RedisStorage.TREE_NODE_PREFIX);
        if(deleteResult){
            log.info("删除redis中删除redis中api:v1:treeNode:get_:开头的数据成功");
        }

        //根据Group表和Account表的数据，将treeNode表中的数据导入redis
        log.info("根据Group表和Account表的数据，将treeNode表中的数据导入redis");
        long l = System.currentTimeMillis();
        List<Group> groups = groupService.groupListAll();
        ServiceResult<List<Account>> listServiceResult = accountService.queryAll();
        List<Account> result = listServiceResult.getResult();
        for (Group group : groups) {
            TreeNodeIntoRedisThread treeNodeIntoRedisThread = new TreeNodeIntoRedisThread(group.getId(), result, treeNodeService, redisManager);
            threadPoolManager.execute(treeNodeIntoRedisThread);
        }
        log.info("根据Group表和Account表的数据，将treeNode表中的数据导入redis完成,耗时:{}ms", System.currentTimeMillis() - l);
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}