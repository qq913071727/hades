package com.innovation.ic.im.end.data.schedule;

import com.innovation.ic.b1b.framework.util.DateUtils;
import com.innovation.ic.im.end.base.mapper.im_erp9.*;
import com.innovation.ic.im.end.base.pojo.constant.Constants;
import com.innovation.ic.im.end.base.value.config.HistoryTimeoutConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import javax.annotation.Resource;
import java.util.Date;

/**
 * @desc   转移历史数据定时任务
 * @author linuo
 * @time   2022年7月28日15:51:29
 */
@Configuration
@EnableScheduling
public class TransferHistoryDataTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(TransferHistoryDataTask.class);

    private static final String PATTERN = "yyyy-MM-dd";

    @Resource
    private HistoryTimeoutConfig historyTimeoutConfig;

    @Resource
    private MessageMapper messageMapper;

    @Resource
    private GroupMessageMapper groupMessageMapper;

    @Resource
    private UserGroupMessageMapper userGroupMessageMapper;

    @Resource
    private UserGroupMessageReceiverMapper userGroupMessageReceiverMapper;

    @Resource
    private GroupMessageReceiverMapper groupMessageReceiverMapper;

    /**
     * 转移消息中过期的数据到历史表中
     */
    @Scheduled(cron = "0 30 0 * * ?")
//    @Scheduled(fixedRate = 6000000)
//    @Scheduled(cron = "0 17 11 * * ?")
    private void transferHistoryData() {
        try {
            // 转移message的历史数据到his_message表中
            transMessageToHisMessage();

            // 转移group_message的历史数据到his_group_message表中
            transGroupMessageToHisGroupMessage();

            // 转移group_message_receiver表数据到his_group_message_receiver表中
            transGroupMessageReceiverToHisGroupMessageReceiver();

            // 转移user_group_message表数据到his_user_group_message表中
            transUserGroupMessageToHisUserGroupMessage();

            // 转移user_group_message_receiver表数据到his_user_group_message_receiver表中
            transUserGroupMessageReceiverToHisUserGroupMessageReceiver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 转移user_group_message_receiver表数据到his_user_group_message_receiver表中
     */
    private void transUserGroupMessageReceiverToHisUserGroupMessageReceiver() {
        log.info("-----转移user_group_message_receiver表数据到his_user_group_message_receiver表中开始-----");
        Integer userGroupMessageReceiverTimeoutDay = historyTimeoutConfig.getTimeout().get(Constants.USER_GROUP_MESSAGE_RECEIVER);
        // 获取过期时间
        Date leftDateByTime = DateUtils.getLeftDayTime(userGroupMessageReceiverTimeoutDay);
        String outTime = DateUtils.formatDate(leftDateByTime, PATTERN);
        int count = userGroupMessageReceiverMapper.transUserGroupMessageReceiverToHisUserGroupMessageReceiver(outTime);
        if(count > 0){
            log.info("转移user_group_message_receiver表数据到his_user_group_message_receiver表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的user_group_message_receiver表历史数据开始-----");
            int delCount = userGroupMessageReceiverMapper.deleteUserGroupMessageReceiverHistoryData(outTime);
            if(delCount > 0){
                log.info("删除转移成功的user_group_message_receiver表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的user_group_message_receiver表历史数据结束-----");
        }
        log.info("-----转移user_group_message_receiver表数据到his_user_group_message_receiver表中结束-----");
    }

    /**
     * 转移user_group_message表数据到his_user_group_message表中
     */
    private void transUserGroupMessageToHisUserGroupMessage() {
        log.info("-----转移user_group_message表数据到his_user_group_message表中开始-----");
        Integer userGroupMessageTimeoutDay = historyTimeoutConfig.getTimeout().get(Constants.USER_GROUP_MESSAGE);
        // 获取过期时间
        Date leftDateByTime = DateUtils.getLeftDayTime(userGroupMessageTimeoutDay);
        String outTime = DateUtils.formatDate(leftDateByTime, PATTERN);
        int count = userGroupMessageMapper.transUserGroupMessageToHisUserGroupMessage(outTime);
        if(count > 0){
            log.info("转移user_group_message表数据到his_user_group_message表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的user_group_message表历史数据开始-----");
            int delCount = userGroupMessageMapper.deleteUserGroupMessageHistoryData(outTime);
            if(delCount > 0){
                log.info("删除转移成功的user_group_message表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的user_group_message表历史数据结束-----");
        }
        log.info("-----转移user_group_message表数据到his_user_group_message表中结束-----");
    }

    /**
     * 转移group_message_receiver表数据到his_group_message_receiver表中
     */
    private void transGroupMessageReceiverToHisGroupMessageReceiver() {
        log.info("-----转移group_message_receiver表数据到his_group_message_receiver表中开始-----");
        Integer groupMessageReceiverTimeoutDay = historyTimeoutConfig.getTimeout().get(Constants.GROUP_MESSAGE_RECEIVER);
        // 获取过期时间
        Date leftDateByTime = DateUtils.getLeftDayTime(groupMessageReceiverTimeoutDay);
        String outTime = DateUtils.formatDate(leftDateByTime, PATTERN);
        int count = groupMessageReceiverMapper.transGroupMessageReceiverToHisGroupMessageReceiver(outTime);
        if(count > 0){
            log.info("转移group_message_receiver表数据到his_group_message_receiver表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的group_message_receiver表历史数据开始-----");
            int delCount = groupMessageReceiverMapper.deleteGroupMessageReceiverHistoryData(outTime);
            if(delCount > 0){
                log.info("删除转移成功的group_message_receiver表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的group_message_receiver表历史数据结束-----");

        }
        log.info("-----转移group_message_receiver表数据到his_group_message_receiver表中结束-----");
    }

    /**
     * 转移group_message的历史数据到his_group_message表中
     */
    private void transGroupMessageToHisGroupMessage() {
        log.info("-----转移group_message的历史数据到his_group_message表中开始-----");
        Integer groupMessageTimeoutDay = historyTimeoutConfig.getTimeout().get(Constants.GROUP_MESSAGE);
        // 获取过期时间
        Date leftDateByTime = DateUtils.getLeftDayTime(groupMessageTimeoutDay);
        String outTime = DateUtils.formatDate(leftDateByTime, PATTERN);
        int count = groupMessageMapper.transGroupMessageDataToHisGroupMessage(outTime);
        if(count > 0){
            log.info("转移group_message的历史数据到his_group_message表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的group_message表历史数据开始-----");
            int delCount = groupMessageMapper.deleteGroupMessageHistoryData(outTime);
            if(delCount > 0){
                log.info("删除转移成功的group_message表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的group_message表历史数据结束-----");
        }
        log.info("-----转移group_message的历史数据到his_group_message表中结束-----");
    }

    /**
     * 转移message的历史数据到his_message表中
     */
    private void transMessageToHisMessage() {
        log.info("-----转移message的历史数据到his_message表中开始-----");
        Integer messageTimeoutDay = historyTimeoutConfig.getTimeout().get(Constants.MESSAGE);
        // 获取过期时间
        Date leftDateByTime = DateUtils.getLeftDayTime(messageTimeoutDay);
        String outTime = DateUtils.formatDate(leftDateByTime, PATTERN);
        int count = messageMapper.transMessageDataToHisMessage(outTime);
        if(count > 0){
            log.info("转移message的历史数据到his_message表中成功,转移数据条数:[{}]", count);
            log.info("-----删除转移成功的message表历史数据开始-----");
            int delCount = messageMapper.deleteMessageHistoryData(outTime);
            if(delCount > 0){
                log.info("删除转移成功的message表历史数据成功,删除数据条数:[{}]", delCount);
            }
            log.info("-----删除转移成功的message表历史数据结束-----");
        }
        log.info("-----转移message的历史数据到his_message表中结束-----");
    }
}