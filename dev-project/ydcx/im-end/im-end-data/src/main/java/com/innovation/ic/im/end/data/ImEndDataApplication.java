package com.innovation.ic.im.end.data;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableHystrix
@SpringBootApplication(scanBasePackages = "com.innovation.ic.im.end")
@MapperScan("com.innovation.ic.im.end.base.mapper")
public class ImEndDataApplication {

    private static final Logger log = LoggerFactory.getLogger(ImEndDataApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(ImEndDataApplication.class, args);
        log.info("im-end-data服务启动成功");
    }

}
