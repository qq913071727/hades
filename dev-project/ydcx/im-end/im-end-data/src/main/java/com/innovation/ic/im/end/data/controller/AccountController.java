package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @desc   账户管理
 * @author zengqinglong
 * @time   2022/11/4 10:24
 **/
@RestController
@RequestMapping("/api/v2/account")
@DefaultProperties(defaultFallback = "defaultFallback")
public class AccountController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    /**
     * 删除redis中api:v1:account:detailByAdminsId:adminsId:get_ 和 api:v1:account:detailByAccount:account:get_开头的数据"
     * 和 Redis 中 api:v1:account:findAllOrderByRealNameAsc数据 ,根据Account表的数据,导入redis"
     * @return 返回处理结果
     */
    @RequestMapping(value = "/importAccountIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> importAccountIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("删除redis中api:v1:account:detailByAdminsId:adminsId:get_ 和 api:v1:account:detailByAccount:account:get_开头的数据" +
                " 和 Redis 中 api:v1:account:findAllOrderByRealNameAsc数据 ,根据Account表的数据,导入redis");
        // 删除redis中api:v1:account:detailByAdminsId:adminsId:get_ 开头的数据
        Boolean deleteResult = redisManager.delRedisDataByKeyPrefix(RedisStorage.ACCOUNT_BY_ADMINS_ID_PREFIX);
        if(deleteResult){
            log.info("删除redis中api:v1:account:detailByAdminsId:adminsId:get_ 和 api:v1:account:detailByAccount:account:get_开头的数据" +
                    " 和 Redis 中 api:v1:account:findAllOrderByRealNameAsc数据 ,根据Account表的数据,导入redis成功");
        }

        // 删除api:v1:account:detailByAccount:account:get_开头的数据
        Boolean deleteResult1 = redisManager.delRedisDataByKeyPrefix(RedisStorage.ACCOUNT_BY_ACCOUNT_PREFIX);
        if(deleteResult1){
            log.info("删除api:v1:account:detailByAccount:account:get_开头的数据成功");
        }

        //删除redis中api:v1:account:findAllOrderByRealNameAsc数据
        redisManager.del(RedisStorage.ACCOUNT_ALL);

        //根据Account表的数据，导入redis
        log.info("根据Account表的数据，导入redis");
        long l = System.currentTimeMillis();
        ServiceResult<Boolean> booleanServiceResult = accountService.importAccountIntoRedis();
        if(booleanServiceResult.getResult()){
            log.info("根据Account表的数据，导入redis完成,耗时:{}ms", System.currentTimeMillis() - l);
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}