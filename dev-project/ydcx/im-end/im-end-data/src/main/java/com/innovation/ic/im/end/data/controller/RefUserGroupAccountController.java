package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author zengqinglong
 * @desc 自定义群组账户关系管理
 * @Date 2022/11/10 10:16
 **/
@RestController
@RequestMapping("/api/v2/refUserGroupAccount")
@DefaultProperties(defaultFallback = "defaultFallback")
public class RefUserGroupAccountController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RefUserGroupAccountController.class);



    /**
     * 删除redis中删除redis中api:v1:refUserGroupAccount:count_:和api:v1:refUserGroupAccount:detail_开头的数据,将ref_user_group_account表的数据统计数量导入redis
     *
     * @return 返回处理结果
     */
    @HystrixCommand
    @RequestMapping(value = "/importRefUserGroupAccountIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> importRefUserGroupAccountIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("删除redis中删除redis中api:v1:refUserGroupAccount:count_:和api:v1:refUserGroupAccount:detail_开头的数据,将ref_user_group_account表的数据统计数量导入redis");

        //删除redis中删除redis中api:v1:refGroupAccount:count_:开头的数据
        Boolean deleteResult = redisManager.delRedisDataByKeyPrefix(RedisStorage.REF_GROUP_ACCOUNT_BY_GROUP_ID_COUNT_PREFIX);
        if (deleteResult) {
            log.info("删除redis中删除redis中api:v1:refGroupAccount:count_:开头的数据成功");
        }

        //根据Group表,将ref_group_account表的数据统计数量导入redis
        log.info("根据Group表,将ref_group_account表的数据统计数量导入redis");
        long l = System.currentTimeMillis();
        ServiceResult<Boolean> booleanServiceResult = refUserGroupAccountService.importRefUserGroupAccountIntoRedis(null);
        log.info("根据Group表,将ref_group_account表的数据统计数量导入redis完成,耗时:{}ms", System.currentTimeMillis() - l);
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
