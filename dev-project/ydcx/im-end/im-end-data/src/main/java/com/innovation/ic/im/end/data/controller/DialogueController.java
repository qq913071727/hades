package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author swq
 * @desc 导入dialogue表数据API
 * @time 2022年10月25日16:49:00
 */
@RestController
@RequestMapping("/api/v2/dialogue")
@DefaultProperties(defaultFallback = "defaultFallback")
public class DialogueController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(DialogueController.class);

    /**
     * 删除redis中DIALOGUE:开头的数据，将dialogue表中的数据导入redis
     *
     * @return 返回处理结果
     */
    @RequestMapping(value = "/importDialogueIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> importMessageIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("删除redis中DIALOGUE:开头的数据，将dialogue表中的数据导入redis");

        // 删除redis中DIALOGUE:开头的数据
        Boolean deleteResult = redisManager.delRedisDataByKeyPrefix(RedisStorage.DIALOGUE_FIND_BY_ACCOUNT_ID_AND_GROUP_PREFIX + "*");
        if(deleteResult){
            log.info("删除redis中DIALOGUE:开头的数据成功");
        }

        // 将dialogue表中的数据导入redis
        log.info("将dialogue表中的数据导入redis");
        ServiceResult<Boolean> serviceResult = dialogueService.importDialogueIntoRedis();
        if (serviceResult.getResult()) {
            log.info("将dialogue表中的数据导入redis完成");
        }

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
