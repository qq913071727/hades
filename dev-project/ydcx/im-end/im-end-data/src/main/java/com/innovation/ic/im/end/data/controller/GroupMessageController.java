package com.innovation.ic.im.end.data.controller;

import com.innovation.ic.im.end.base.pojo.ApiResult;
import com.innovation.ic.im.end.base.pojo.ServiceResult;
import com.innovation.ic.im.end.base.pojo.constant.RedisStorage;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @desc   导入groupMessage表数据API
 * @author linuo
 * @time   2022年10月24日16:50:32
 */
@RestController
@RequestMapping("/api/v2/groupMessage")
@DefaultProperties(defaultFallback = "defaultFallback")
public class GroupMessageController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(GroupMessageController.class);

    /**
     * 删除redis中GROUP_MESSAGE:开头的数据，将group_message表中的数据导入redis
     * @return 返回处理结果
     */
    @RequestMapping(value = "/importGroupMessageIntoRedis", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> importGroupMessageIntoRedis(HttpServletRequest request, HttpServletResponse response) {
        log.info("将group_message表中的数据导入redis");

        // 删除redis中GROUP_MESSAGE:开头的数据
        Boolean deleteResult = redisManager.delRedisDataByKeyPrefix(RedisStorage.GROUP_MESSAGE_PREFIX);
        if(deleteResult){
            log.info("删除redis中GROUP_MESSAGE:开头的数据成功");
        }

        // 将group_message表中的数据导入redis
        log.info("将group_message表中的数据导入redis");
        ServiceResult<Boolean> serviceResult = groupMessageService.importGroupMessageIntoRedis();
        if (serviceResult.getResult()) {
            log.info("将group_message表中的数据导入redis完成");
        }

        ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<ApiResult>(apiResult, HttpStatus.OK);
    }
}