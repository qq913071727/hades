echo 将MESSAGE:开头的数据导入到redis
curl http://127.0.0.1:14101/im-end-data/api/v2/message/importMessageIntoRedis -k

echo 将GROUP_MESSAGE:开头的数据导入到redis
curl http://127.0.0.1:14101/im-end-data/api/v2/groupMessage/importGroupMessageIntoRedis -k

echo 将USER_GROUP_MESSAGE:开头的数据导入到redis
curl https://127.0.0.1:14101/im-end-data/api/v2/userGroupMessage/importUserGroupMessageIntoRedis -k

echo 将key为CLIENT的数据导入到redis
curl https://127.0.0.1:14101/im-end-data/api/v2/client/importClientIntoRedis -k

echo 删除api:v1:userGroup:current_开头的数据,将用户最近联系人的数据导入redis
curl https://127.0.0.1:14101/im-end-data/api/v2/userGroup/importCurrentIntoRedis -k

echo 删除api:v1:dialogue:findByAccountIdAndGroup_开头的数据,将话术数据导入redis
curl https://127.0.0.1:14101/im-end-data/api/v2/dialogue/importDialogueIntoRedis -k

echo 删除redis中删除redis中api:v1:treeNode:get_:开头的数据,根据Group表和Account表的数据,将treeNode表中的数据导入redis
curl https://127.0.0.1:14101/im-end-data/api/v2/treeNode/importTreeNodeIntoRedis -k

echo 删除redis中api:v1:account:detailByAdminsId:adminsId:get_ 和 api:v1:account:detailByAccount:account:get_开头的数据 和 Redis 中 api:v1:account:findAllOrderByRealNameAsc数据 ,根据Account表的数据,导入redis
curl https://127.0.0.1:14101/im-end-data/api/v2/account/importAccountIntoRedis -k

echo 删除redis中删除redis中api:v1:refGroupAccount:count_:开头的数据,根据Group表,将ref_group_account表的数据统计数量导入redis
curl https://127.0.0.1:14101/im-end-data/api/v2/refGroupAccount/importRefGroupAccountIntoRedis -k

echo 删除redis中删除redis中api:v1:refUserGroupAccount:count_:和api:v1:refUserGroupAccount:detail_开头的数据,将ref_user_group_account表的数据统计数量导入redis
curl https://127.0.0.1:14101/im-end-data/api/v2/refUserGroupAccount/importRefUserGroupAccountIntoRedis -k