﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WinApp.Services
{
    public class Twinkle
    {

        private static Icon icon1 = new Icon(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "img", "favicon_small.ico"));
        private static Icon icon2 = new Icon(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "img", "small_kong.ico"));

        public Twinkle()
        {

        }

         int i = 0;
         Thread thread;
        public  void IsOpen(bool istrue)
        {

            try
            {
                thread = new Thread(delegate ()
                {
                    while (istrue)
                    {
                        //如果i=0则让任务栏图标变为透明的图标并且退出
                        if (i < 1)
                        {
                            SimHelper.NotifyIcon.Icon = icon1;
                            i++;

                        }
                        //如果i!=0,就让任务栏图标变为ico1,并将i置为0;
                        else
                        {
                            SimHelper.NotifyIcon.Icon = icon2;
                            i = 0;
                        }
                        Thread.Sleep(500);
                    }

                });
                thread.Start();

            }
            catch
            {

            }
            finally
            {
                if (!istrue)
                {
                    thread.Abort();
                }
            }


        }

        static Twinkle current;
        static object locker = new object();
        static public Twinkle Current
        {
            get
            {
                if (current == null)
                {
                    lock (locker)
                    {
                        if (current == null)
                        {
                            current = new Twinkle();
                        }
                    }
                }
                return current;
            }
        }
    }
}
