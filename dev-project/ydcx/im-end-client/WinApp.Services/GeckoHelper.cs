﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WinApp.Services
{
    public class GeckoHelper
    {
        /// <summary>
        /// 测试
        /// </summary>
        /// <param name="data"></param>
        [GeckoFuntion]
        static public void Logon(string account)
        {
            SimHelper.Account = account.Replace("\"", "");
            //Config.HomeUrl = url.Replace("\"", "");
        }

        static Gecko.GeckoWebBrowser firefox = SimHelper.Firefox;

        [GeckoFuntion]
        static public string GetLoginType()
        {
            return "1";
            //Config.HomeUrl = url.Replace("\"", "");
        }

        static string icon1File = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "img", "favicon_small.ico");
        static string icon2File = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "img", "small_kong.ico");

        private static Icon icon1 = new Icon(icon1File);
        private static Icon icon2 = new Icon(icon2File);

        [GeckoFuntion]
        static public void GetMessage(int number)
        {
            //Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + number);

            SimHelper.NumberInti(number);

            //2022.9.15新思路：不在焦点并且数字不是0的时候无条件闪烁，是0停止闪烁。大于0和-1的情况还闪烁
            //获取当前获得焦点的控件
            IntPtr handle = (IntPtr)GetFocus();


            Console.WriteLine($"GetMessage->GetFocus:{handle.ToInt32()}");

            if (number == int.MinValue)
            {
                //Console.WriteLine($"GetMessage->number:{number}");
                ////SimHelper.flashTaskBar(SimHelper.IntPtr, SimHelper.falshType.FLASHW_STOP);
                ////SimHelper.NotifyIcon.Icon = icon1;

                //if (SimHelper.Form != null)
                //{
                //    //SimHelper.Form.Hide();
                //    //SimHelper.Form.Show();
                //    //SimHelper.Form.Select();
                //    //SimHelper.Form.Focus();
                //    //SimHelper.Form.ShowInTaskbar = true;
                //    //SimHelper.Form.ShowInTaskbar = false;
                //}

                //return;
            }

            //if (handle.ToInt32() != 0)
            //{
            //    return;
            //}


            //if (handle.ToInt32() == -1)
            //{

            //}


            if (number == 0 || (handle.ToInt32() != 0 && number == -1))
            {
                SimHelper.flashTaskBar(SimHelper.IntPtr, SimHelper.falshType.FLASHW_STOP);
                SimHelper.NotifyIcon.Icon = icon1;
                SimHelper.Timer.Enabled = false;
                if (SimHelper.Form != null)
                {
                    //SimHelper.Form.Select();
                    //SimHelper.Form.Focus();
                }
            }
            //-1焦点在后闪烁 并且>0也闪烁
            else
            {
                SimHelper.Timer.Enabled = true;
                SimHelper.Timer.Interval = 500;
            }


        }

        [DllImport("user32.dll")]
        public static extern int GetFocus(); //获取当前获得焦点的控件

        static public void GetMessage1(int number)
        {


            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Console.WriteLine(number);
            SimHelper.NumberInti(number);
            //获取当前获得焦点的控件
            IntPtr handle = (IntPtr)GetFocus();
            if (number == 0 || number == -1)
            {
                SimHelper.Timer.Enabled = false;
                SimHelper.flashTaskBar(SimHelper.IntPtr, SimHelper.falshType.FLASHW_STOP);
                SimHelper.NotifyIcon.Icon = icon1;
            }
            else
            {
                SimHelper.Timer.Enabled = true;
                SimHelper.Timer.Interval = 500;
            }
        }

        /// <summary>
        /// 获得回调地址
        /// </summary>
        /// <param name="backUrl"></param>
        [GeckoFuntion]
        static public void GetBackUrl(string backUrl)
        {
            //Config.HomeUrl = backUrl;
            var url = backUrl.Replace("\"", "");
            SimHelper.Firefox.Navigate(url);
        }
    }
}
