﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinApp.Services
{
    /// <summary>
    /// 总配置类
    /// </summary>
    /// <remarks>
    /// winform的配置只能开发死在这里
    /// </remarks>
    public class Config
    {
#if DEBUG
        /// <summary> 
        /// 域名
        /// </summary>
        public const string SchemeName = "https";

        const string ApiUrlName = "out.bj.51db.com:12101";
        static public readonly string ApiUrl = $"{SchemeName}://{ApiUrlName}/im-end-web";

        public static string HomeUrl = $"http://erp9.b1b.com/#/login";
        //public static string HomeUrl = "http://localhost:8081/";


#elif TEST
        //对应着他们说的验证版
        /// <summary> 
        /// 域名
        /// </summary>
        public const string SchemeName = "https";

        const string ApiUrlName = "out.bj.51db.com:12201";
        /// <summary>
        /// 接口地址
        /// </summary>
       static public readonly string ApiUrl = $"{SchemeName}://{ApiUrlName}/im-end-web";
        /// <summary>
        /// 前端地址
        /// </summary>
        public static string HomeUrl = $"http://erp9val.ic360.cn/#/login";
#else
        /// <summary> 
        /// 域名
        /// </summary>
        public const string SchemeName = "https";

        const string ApiUrlName = "ydim.etime.net.cn:12301";
         /// <summary>
        /// 接口地址
        /// </summary>
        static public readonly string ApiUrl = $"{SchemeName}://{ApiUrlName}/im-end-web";
         /// <summary>
        /// 前端地址
        /// </summary>
        public static string HomeUrl = $"{SchemeName}://erp9.etime.net.cn";
#endif
    }
}
