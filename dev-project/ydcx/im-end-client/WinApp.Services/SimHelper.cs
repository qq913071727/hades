﻿using Gecko;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WinApp.Services
{
    public class SimHelper
    {
        /// <summary>
        /// 系统当前活跃的主窗体 的Firefox
        /// </summary>
        static public Gecko.GeckoWebBrowser Firefox { get; private set; }

        /// <summary>
        /// 账号
        /// </summary>
        static public string Account = "";

        static public IntPtr IntPtr { get; private set; }

        /// <summary>
        /// 窗体
        /// </summary>
        static public System.Windows.Forms.Form Form { get; private set; }
        static public System.Windows.Forms.NotifyIcon NotifyIcon { get; private set; }
        static public System.Windows.Forms.Timer Timer { get; private set; }

        static public int Number { get; private set; }
        static public System.Windows.Forms.Form HomePage { get; private set; }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sim">状态消息接口 对象</param>
        static public void Initialize(Gecko.GeckoWebBrowser firefox)
        {
            Firefox = firefox;
        }

        static public void NumberInti(int number)
        {
            Number = number;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sim">状态消息接口 对象</param>
        //static public void Initialize( System.Windows.Forms.Form homePage)
        //{
        //    HomePage = homePage;
        //}



        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sim">状态消息接口 对象</param>
        static public void Initialize(System.Windows.Forms.Timer timer, System.Windows.Forms.NotifyIcon notifyIcon)
        {
            Timer = timer;
            NotifyIcon = notifyIcon;
        }

        static public void Inti(IntPtr intPtr)
        {
            IntPtr = intPtr;
        }

        static public void Inti(System.Windows.Forms.Form form)
        {
            Form = form;
        }




        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        [DllImport("user32.dll")]
        public static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        public enum falshType : uint
        {
            FLASHW_STOP = 0,    //停止闪烁
            FALSHW_CAPTION = 1,  //只闪烁标题
            FLASHW_TRAY = 2,   //只闪烁任务栏
            FLASHW_ALL = 3,     //标题和任务栏同时闪烁
            FLASHW_PARAM1 = 4,
            FLASHW_PARAM2 = 12,
            /// <summary>
            /// 无条件闪烁任务栏直到发送停止标志或者窗口被激活，如果未激活，停止时高亮
            /// </summary>
            FLASHW_TIMER = FLASHW_TRAY | FLASHW_PARAM1,   //无条件闪烁任务栏直到发送停止标志或者窗口被激活，如果未激活，停止时高亮
            /// <summary>
            /// 未激活时闪烁任务栏直到发送停止标志或者窗体被激活，停止后高亮
            /// </summary>
            FLASHW_TIMERNOFG = FLASHW_TRAY | FLASHW_PARAM2  //未激活时闪烁任务栏直到发送停止标志或者窗体被激活，停止后高亮
        }

        public static bool flashTaskBar(IntPtr hWnd, falshType type)
        {
            FLASHWINFO fInfo = new FLASHWINFO();
            fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
            fInfo.hwnd = hWnd;//要闪烁的窗口的句柄，该窗口可以是打开的或最小化的
            fInfo.dwFlags = (uint)type;//闪烁的类型
            fInfo.uCount = UInt32.MaxValue;//闪烁窗口的次数
            fInfo.dwTimeout = 0; //窗口闪烁的频度，毫秒为单位；若该值为0，则为默认图标的闪烁频度
            return FlashWindowEx(ref fInfo);
        }

    }
}
