﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinApp.Services;

namespace WinApp
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
        }

        private Icon icon1 = new Icon(@"D:\im\3-项目代码\trunk\im-end-client\WinApp\img\favicon_small.ico");
        private Icon icon2 = new Icon(@"D:\im\3-项目代码\trunk\im-end-client\WinApp\img\small_kong.ico");
        //创建NotifyIcon对象
        NotifyIcon notifyIcon = new NotifyIcon();
        //创建托盘图标对象
        Icon ico = new Icon(@"D:\im\3-项目代码\trunk\im-end-client\WinApp\img\favicon_small.ico");
        //创建托盘菜单对象
        ContextMenu notifyContextMenu = new ContextMenu();

        private void TestForm_Load(object sender, EventArgs e)
        {
            this.notifyIcon1.Text = "这是托盘图标";
            this.notifyIcon1.Icon = icon1;
            this.timer1.Enabled = true;//将定时控件设为启用,默认为false;
            this.timer1.Interval = 500;
        }

        int i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //如果i=0则让任务栏图标变为透明的图标并且退出
            if (i < 1)
            {
                this.notifyIcon1.Icon = icon2;
                i++;
                return;
            }
            //如果i!=0,就让任务栏图标变为ico1,并将i置为0;
            else
                this.notifyIcon1.Icon = icon1;
            i = 0;
        }

        private void TestForm_SizeChanged(object sender, EventArgs e)
        {
            //判断是否选择的是最小化按钮
            if (WindowState == FormWindowState.Minimized)
            {
                //托盘显示图标等于托盘图标对象
                //注意notifyIcon1是控件的名字而不是对象的名字
                notifyIcon1.Icon = ico;
                //隐藏任务栏区图标
                this.ShowInTaskbar = false;
                //图标显示在托盘区
                notifyIcon.Visible = true;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        /// <summary>
        /// 还原窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            //判断是否已经最小化于托盘
            if (WindowState == FormWindowState.Minimized)
            {
                //还原窗体显示
                WindowState = FormWindowState.Normal;
                //激活窗体并给予它焦点
                this.Activate();
                //任务栏区显示图标
                this.ShowInTaskbar = true;
                //托盘区图标隐藏
                notifyIcon.Visible = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
