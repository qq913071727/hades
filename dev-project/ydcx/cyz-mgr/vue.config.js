'use strict';
const path = require('path');
const { name } = require('./package');
const timeStamp = new Date().getTime();

function resolve (dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  // 部署生产环境(production)和开发环境(development)下的URL。
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
  outputDir: 'dist',
  // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
  assetsDir: 'static',
  // 是否开启eslint保存检测，有效值：ture | false | 'error'
  lintOnSave: process.env.NODE_ENV === 'development',
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,
  devServer: {
    port: 8092,
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  configureWebpack: {
    // devtool: 'source-map',    //浏览器可调试代码
    output: {
      library: `${name}-[name]`,
      libraryTarget: 'umd', // 把微应用打包成 umd 库格式
      jsonpFunction: `webpackJsonp_${name}`,
      //缓存问题
      // filename: `js[name].${timeStamp}.js`,
      // chunkFilename: `js[name].${timeStamp}.js`,
    },
    // css: {
    //   //缓存问题
    //   extract: { // 打包后css文件名称添加时间戳
    //     filename: `css/[name].${timeStamp}.css`,
    //     chunkFilename: `css/[name].${timeStamp}.css`,
    //   }
    // }
  },
  chainWebpack (config) {
    config.plugins.delete('preload'); // TODO: need test
    config.plugins.delete('prefetch'); // TODO: need test

    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/assets/icons'))
      .end();
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]',
      })
      .end();
  },
};
