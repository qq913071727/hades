import { service } from './index'

  export function getEnumerate(params) {
    return service({
      url:  "/Product/BaseApi/GetEnumerate",
      method: "post",
      data: params
    });
  }
const commonUrl ='/Product/Category/';
/** 获取分类树 */
export function gettree() {
    return service({
      url: commonUrl + "Tree",
      method: "get",
    });
  }
/** 获取分类数数据*/
export function Details(id) {
  return service({
    url: commonUrl + "Details/" + id,
    method: "get",
  });
}
  /** 保存数据*/
export function Enter(data) {
  return service({
    url: commonUrl + "Save",
    method: "post",
    data: data,
  });
}
// 添加
export function categoryproplist(params,id) {
  return service({
    url: "/Product/Property/CategoryPropList?categoryid="+id,
    method: "post",
    data:params
  });
}
/** 获取分组信息 */
export function GetGroup() {
  return service({
    url: "/Product/Property/GetGroups",
    method: "get"
  });
}
/** 关联属性*/
export function RelationProp(id, ids) {
  var params = {
    mainid: id,
    relationids: ids
  }
  return service({
    url: commonUrl + "RelationProp",
    method: "post",
    data: params
  });
}
/** 启用 */
export function BootCategoryProperty(ids) {
  return service({
    url: commonUrl + "BootCategoryProperty",
    method: "post",
    data: ids
  });
}
/** 禁用 */
export function StopCategoryProperty(ids) {
  return service({
    url: commonUrl + "StopCategoryProperty",
    method: "post",
    data: ids
  });
}
export function SetIsRequired(id, check) {
  var data = {
    id: id,
    check: check
  }
  return service({
    url: commonUrl + "SetIsRequired",
    method: "post",
    data: data
  });
}
export function SetIsSearch(id, check) {
  var data = {
    id: id,
    check: check
  }
  return service({
    url: commonUrl + "SetIsSearch",
    method: "post",
    data: data
  });
}
/**禁用事件*/
export function Disable(ids) {
  var params = {
    ids: ids,
    status: 400
  }
  return service({
    url: commonUrl + "Disable",
    method: "post",
    data: params
  });
}
/** 删除属性 */
export function delCategoryProperty(ids) {
  return service({
    url: commonUrl + "DelCategoryProperty",
    method: "post",
    data: ids
  });
}
/** 获取分类树 */
export function getnormaltree() {
  return service({
    url: commonUrl + "NormalTree",
    method: "get",
  });
}