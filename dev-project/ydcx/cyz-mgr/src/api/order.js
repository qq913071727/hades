import { service } from '@/utils/request'

// 工单分类列表
export function queryList(pageNo,pageSize) {
    return service({
        url: '/api/v1/repairOrderType/queryList/'+pageNo + '/' + pageSize,
        method: 'get',
        // data: param,
    })
}
// 工单列表编辑
export function editInfro(param) {
    return service({
        url: '/api/v1/repairOrderType/info/' + param,
        method: 'get',
        // data: param,
    })
}
// 工单列表编辑选项
export function getFaeOption(param) {
    return service({
        url: '/api/v1/repairOrderType/getFaeList',
        method: 'get',
        data: param,
    })
}
// 工单列表编辑确定
export function editSumbit(param) {
    return service({
        url: '/api/v1/repairOrderType/edit',
        method: 'post',
        data: param,
    })
}
// 工单删除
export function deleteOrder(param) {
    return service({
        url: '/api/v1/repairOrderType/delete/' + param,
        method: 'get',
        // data: param,
    })
}
// 添加分类
export function addOrder(param) {
    return service({
        url: '/api/v1/repairOrderType/add',
        method: 'post',
        data: param,
    })
}
// 工单列表查询
export function searchList(param) {
    return service({
        url: '/api/v1/repairOrder/queryList',
        method: 'post',
        data: param,
    })
}
// 获取一级名称选项
export function firstTypeOption(param) {
    return service({
        url: '/api/v1/repairOrderType/getFirstTypeList',
        method: 'get',
        data: param,
    })
}
// 指派提交
export function assignOrder(orderId,headId) {
    return service({
        url: '/api/v1/repairOrder/assign/' + orderId + '/' + headId,
        method: 'get',
        // data: param,
    })
}
// 获取工单内容
export function repairOrderInfo(params) {
    return service({
        url: "/api/v1/repairOrder/info/" + params ,
        method: "get",
        
    })
}
// 工单回复
export function replyRepairOrder(data) {
    return service({
        url: "/api/v1/repairOrder/reply",
        method: "post",
        data
    })
}
// 删除工单
export function deleteRepairOrder(params) {
    return service({
        url: "/api/v1/repairOrderFile/delete/" + params ,
        method: "get",
        
    })
}
// 上传文件成功删除
export function deleteByFilePath(data) {
    return service({
        url: "/api/v1/repairOrderFile/deleteByFilePath" ,
        method: "post",
        data
    })
}
// 下载工单
export function downloadRepairOrder(params) {
    return service({
        url: "/api/v1/repairOrderFile/download/" + params ,
        method: "get",
        responseType: 'arraybuffer',
    })
}
// 图片上传

export function getImgList(data,x) {
    return service({
        url: "/api/v1/file/uploadImage?category=" + x,
        method: "post",
        data     
    })
}
// 视频上传
export function getVideoList(data,x) {
    return service({
        url: "/api/v1/file/uploadVideo?category=" + x,
        method: "post",
        data     
    })
}
// 上传附件
export function uploadProtocol(data) {
    return service({
        url: "/api/v1/file/uploadProtocol",
        method: "post",
        data,
    }) 
}
// 工单解决
export function orderSolve(data) {
    return service({
        url: "/api/v1/repairOrder/solve",
        method: "post",
        data,
    }) 
}
