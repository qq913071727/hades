import { service } from './index'
const commonUrl ='/Product/Replacement/';
/** 获取列表 */
export function getList(params) {
  return service({
    url: commonUrl + "List/",
    method: "post",
    data: params
  });
}
/** 保存数据 */
export function save(data) {
  return service({
    url: commonUrl + "Save",
    method: "post",
    data: data
  });
}
/**移除信息*/
export function remove(id) {
  return service({
    url: commonUrl + "Remove/" + id,
    method: "delete",
  });
}
/**移除信息*/
export function getOption(key) {
  return service({
    url: commonUrl + "GetPartNumberOption/" + key,
    method: "get",
  });
}
