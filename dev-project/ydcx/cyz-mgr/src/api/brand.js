import { service } from './index'

const commonUrl ='/Product/Brand/';
/** 获取列表 */
export function List(params) {
  return service({
    url: commonUrl + "List",
    method: "post",
    data: params,
  });
}
/** 获取明细*/
export function Details(id) {
  return service({
    url: commonUrl + "Details/" + id,
    method: "get",
  });
}
/** 保存数据*/
// export function Enter(Brand, cat) {
//   var data = {
//     Brand: Brand,
//     categories: cat
//   }
//   return request({
//     url: commonUrl + "Save",
//     method: "post",
//     data: data,
//   });
// }
/** 启用 */
export function Boot(ids) {
  return service({
    url: commonUrl + "Boot",
    method: "post",
    data:ids
  });
}
/** 禁用 */
export function Stop(ids) {
  return service({
    url: commonUrl + "Stop",
    method: "post",
    data:ids
  });
}
/** 保存数据 */
export function save(data) {
  return service({
    url:"/Product/Feedback/Save",
    method: "post",
    data:data
  });
}
/** 获取品牌信息作为项*/
// export function GetBrandOptions(params) {
//   var query={ key: params }
//   return request({
//     url: commonUrl + "GetBrandOptions",
//     method: "get",
//     params: query
//   });
// }
/** 获取品牌关联信息*/
// export function GetRelationBrand(BrandID) {
//   return request({
//     url: commonUrl + "GetRelationBrand/" + BrandID,
//     method: "get",
//   });
// }
/** 保存关联品牌*/
// export function SaveRelationBrand(data) {
//   return request({
//     url: commonUrl + "SaveRelationBrand",
//     method: "post",
//     data: data,
//   });
// }
/** 删除关联品牌*/
// export function RemoveRelationBrand(data) {
//   return request({
//     url: commonUrl + "RemoveRelationBrand",
//     method: "post",
//     data: data,
//   });
// }

/** 获取品牌代理信息*/
// export function GetBrandAgent(BrandID) {
//   return request({
//     url: commonUrl + "BrandAgent/" + BrandID,
//     method: "get",
//   });
// }
/** 获取品牌代理信息*/
// export function getadminOption(key,brandid) {
//   return request({
//     url: commonUrl + "GetAdminOptions/"+key+"/"+brandid,
//     method: "get",
//   });
// }
/** 获取品牌负责人*/
// export function getBrandPM(brandid) {
//   return request({
//     url: commonUrl + "GetBrandPM/" + brandid,
//     method: "get",
//   });
// }
/**新增品牌负责人*/
// export function saveBrandPM(data) {
//   return request({
//     url: commonUrl + "BindBrandPM",
//     method: "post",
//     data: data,
//   });
// }
/**移除品牌负责人*/
// export function removeBrandPM(id) {
//   return request({
//     url: commonUrl + "RemoveBrandPM/" + id,
//     method: "delete",
//   });
// }
/** 获取全部品牌明细*/
// export function getlist() {
//   return request({
//     url: commonUrl + "List",
//     method: "get",
//   });
// }
/** 调整品牌信息*/
// export function ajustment(Brand) {
//   return request({
//     url: commonUrl + "Ajustment",
//     method: "post",
//     data: Brand,
//   });
// }
