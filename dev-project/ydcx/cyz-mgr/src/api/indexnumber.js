import { service } from './index'
//首页销售api
//数据统计 (Auth)
export function GetStatsCount() {
    return service({
        url: 'rfqapi/api/SaleIndex/GetStatsCount',
        method: 'get',
        data: {},
    })
}
//获取热门型号 (Auth)
export function GetHotPartNumbers(params) {
    return service({
        url: 'rfqapi/api/SaleIndex/GetHotPartNumbers',
        method: 'get',
        data: params,
    })
}
//我的询价追踪 (Auth
export function GetMyInquirieFromClient(param) {
    return service({
        url: 'rfqapi/api/SaleIndex/GetMyInquirieFromClient/' + param,
        method: 'get',
        data: {},
    })
}

//获取询价成功率 (Auth)
export function GetMyInquiriesRate() {
    return service({
        url: 'rfqapi/api/SaleIndex/GetMyInquiriesRate',
        method: 'get',
        data: {},
    })
}

// 获取询价数据统计 (Auth)
export function GetInqStats(params) {
    return service({
        url: 'rfqapi/api/SaleIndex/GetInqStats',
        method: 'get',
        params: params,
    })
}


//首页采购api
// 数据统计 (Auth)
export function GetPurchaseIndex() {
    return service({
        url: 'rfqapi/api/PurchaseIndex/GetStatsCount',
        method: 'get',
        data: {},
    })
}
//获取报价成功率 (Auth
export function GetMyQuotesRate() {
    return service({
        url: 'rfqapi/api/PurchaseIndex/GetMyQuotesRate',
        method: 'get',
        data: {},
    })
}
// 公海认领 (Auth)
export function GetMyQuotesClaimRate() {
    return service({
        url: 'rfqapi/api/PurchaseIndex/GetMyQuotesClaimRate',
        method: 'get',
        data: {},
    })
}
// 按供应商统计采纳率（近一年） (Auth)
export function GetQuotesRateFromSupplier(param) {
    return service({
        url: 'rfqapi/api/PurchaseIndex/GetQuotesRateFromSupplier/' + param,
        method: 'get',
        data: {},
    })
}