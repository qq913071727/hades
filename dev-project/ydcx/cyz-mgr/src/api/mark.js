import { service } from './index'

const commonUrl = '/Product/PartNumber/';
/** 获取列表 */
export function List(params) {
  return service({
    url: commonUrl + "List",
    method: "post",
    data: params,
  });
}
/** 获取明细*/
export function Details(id) {
  return service({
    url: commonUrl + "Details/" + encodeURIComponent(id) ,
    method: "get",
  });
}
/** 获取分类树 */
// export function gettree() {
//   return request({
//     url: commonUrl + "Tree",
//     method: "get",
//   });
// }
/** 保存数据*/
// export function Enter(data) {

//   return request({
//     url: commonUrl + "Save",
//     method: "post",
//     data: data,
//   });
// }
/** 启用 */
export function Boot(ids) {
  return service({
    url: commonUrl + "Boot",
    method: "post",
    data: ids
  });
}
/** 禁用 */
export function Stop(ids) {
  return service({
    url: commonUrl + "Stop",
    method: "post",
    data: ids
  });
}
/** 获取属性 */
// export function getProp(catid, partNumber) {
//   return request({
//     url: commonUrl + "getpropGroup",
//     method: "get",
//     params: {
//       id: catid,
//       partNumber: partNumber

//     }
//   });
// }
/** 获取品牌为下拉项*/
// export function getbrand(params) {
//   var query={ brandName: params }
//   return request({
//     url: "api/ElementUI/BrandList",
//     method: "GET",
//     params: query
//   })
// }
/** 关联别名*/
// export function saveAlias(id, ids) {
//   var params = {
//     mainid: id,
//     relationids: ids
//   }
//   return request({
//     url: commonUrl + "SaveAlias",
//     method: "post",
//     data: params
//   });
// }
/** 获取别名*/
// export function getAlias(ID) {
//   return request({
//     url: commonUrl + "GetAlias/" + ID,
//     method: "get",
//   });
// }
/** 获取型号信息下拉信息*/
// export function getProductOption(PartNumber) {
//   return request({
//     url: commonUrl + "GetProductOption/" + PartNumber,
//     method: "get",
//   });
// }
/** 获取型号信息*/
// export function getProduct(id) {
//   return request({
//     url: commonUrl + "GetProduct/" + id,
//     method: "get",
//   });
// }
