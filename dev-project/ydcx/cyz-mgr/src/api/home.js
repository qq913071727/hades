import { service } from '@/utils/request'

// banner列表
export function allBanner(param) {
    return service({
        url: '/api/v1/banner/allBanner',
        method: 'post',
        data: param,
    })
}
// 删除轮播图
export function deleteBanner(param) {
    return service({
        url: '/api/v1/banner/deleteBanner',
        method: 'post',
        data: param,
    })
}
// 添加banner
export function addBanner(param) {
    return service({
        url: '/api/v1/banner/addBenner',
        method: 'post',
        data: param,
    })
}
// 启用更改
export function updateAvailable(param) {
    return service({
        url: '/api/v1/banner/updateAvailable',
        method: 'post',
        data: param,
    })
}
