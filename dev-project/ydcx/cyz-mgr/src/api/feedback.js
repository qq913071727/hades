import { service } from '@/utils/request'

//问题类型
export function allFeedbackType(param) {
    return service({
        url: '/api/v1/feedback/allFeedbackType',
        method: 'post',
        data: param,
    })
}
// 信息反馈展示数据
export function allFeedback(param) {
    return service({
        url: '/api/v1/feedback/allFeedback',
        method: 'post',
        data: param,
    })
}

// 资讯列表
export function informationList(param) {
    return service({
        url: '/api/v1/inquiry/page',
        method: 'post',
        data: param,
    })
}
