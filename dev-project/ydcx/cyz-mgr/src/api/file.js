import { service } from './index'

const commonUrl = '/FileUpload/';
/** 获取列表 */
// export function getPcFileByMainID(MianID) {
//   return request({
//     url: commonUrl + "GetPcFileByMainID/" + MianID,
//     method: "get",
//   });
// }
// 上传文件
export function getUpFileByMainID(MianID) {
  return service({
    url: commonUrl + "GetProductFile/" + MianID,
    method: "get",
  });
}
/** 删除文件*/
export function remove(id) {
  return service({
    url: commonUrl + "Remove/" + id,
    method: "delete",
  });
}
/** 文件下载*/
// export function downfile() {
//   return request({
//     url: commonUrl + "downfile",
//     method: "post",
//     responseType: "blob",// 表明返回服务器返回的数据类型
//   });
// }