import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
// import Layout from "@/layout";

/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta : {
		noCache: true                // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
		title: 'title'               // 设置该路由在侧边栏和面包屑中展示的名字
		icon: 'svg-name'             // 设置该路由的图标，对应路径src/assets/icons/svg
		breadcrumb: false            // 如果设置为false，则不会在breadcrumb面包屑中显示
		activeMenu: '/system/user'   // 当路由设置了该属性，则会高亮相对应的侧边栏。
	}
 */
let prefix = '';
if (window.__POWERED_BY_QIANKUN__) {
	prefix = '/subapp/cyz/cyz'; // /workbench为主应用的activeRule
}

//公共路由
export const routes = [
	 /** 首页*/
	{
		path: prefix + '/home/index',
		component: () => import('../views/home/index.vue'),
		meta:{isKeeplive:true},
	},
	{
		path: prefix + '/home/selection',
		component: () => import('../views/home/selection.vue'),
		meta:{isKeeplive:true},
	},
	{
		path: prefix + '/home/device',
		component: () => import('../views/home/device.vue'),
		meta:{isKeeplive:true},
	},
	 /** 我的任务*/
	{
		path: prefix + '/task/list',
		component: () => import('../views/task/list.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/task/editmodel',
		component: () => import('../views/task/editModel.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/task/editpack',
		component: () => import('../views/task/editPack.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/task/editbrand',
		component: () => import('../views/task/editBrand.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/task/editpackage',
		component: () => import('../views/task/editPackage.vue'),
		meta:{isalive:false},
	},
	// 信息反馈
	{
		path: prefix + '/feedback/index',
		component: () => import('../views/feedback/index.vue'),
		meta:{isalive:false},
	},
	// 资讯列表
	{
		path: prefix + '/information/index',
		component: () => import('../views/information/index.vue'),
		meta:{isalive:false},
	},
	// 工单管理
	{
		path: prefix + '/order/index',
		component: () => import('../views/order/index.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/order/list',
		component: () => import('../views/order/list.vue'),
		meta:{isalive:false},
	},
	/** 人工维护 */
	{
		path: prefix + '/defend/classifylist',
		component: () => import('../views/defend/classifylist.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/brandlist',
		component: () => import('../views/defend/brandlist.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/detail',
		component: () => import('../views/defend/detail.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/marklist',
		component: () => import('../views/defend/marklist.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/markdetail',
		component: () => import('../views/defend/markdetail.vue'),
		meta:{isalive:false},
	},
	
	{
		path: prefix + '/defend/packinglist',
		component: () => import('../views/defend/packinglist.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/package',
		component: () => import('../views/defend/package.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/add',
		component: () => import('../views/defend/addpackage.vue'),
		meta:{isalive:false},
	},
	{
		path: prefix + '/defend/pool',
		component: () => import('../views/defend/replacepool.vue'),
		meta:{isalive:false},
	},

];

export default routes;

// export default new Router({
//   mode: "hash",
//   scrollBehavior: () => ({ y: 0 }),
//   routes: routers,
// });
