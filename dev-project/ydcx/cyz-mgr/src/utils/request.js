import axios from 'axios'
import Cookies from 'vue-cookies'
import { Message } from 'element-ui'

// 修改统一配置baseUrl
// 由于每次打包需要更改api, 现增加变量如红框中的配置,
//     在使用process.env.VUE_APP_BASE_API的地方通过判断直接使用window.__GLOBALCONFIG.VUE_APP_BASE_API;
// 可不用每次更改baseurl去打包, 会直接读取plat基座的baseUrl,
//     子应用更改后可任意发布不同域名环境


var service = axios.create({
    baseURL: process.env.NODE_ENV == 'development' ? process.env.VUE_APP_CYZ_URL : window.__GLOBALCONFIG.VUE_APP_CYZ_URL,
    timeout: 50000,
    headers: { 'content-type': 'application/json;charset=utf-8', "Access-Control-Allow-Origin": "*" },
})



import CryptoJS from "crypto-js"
//请求解密
const key = CryptoJS.enc.Utf8.parse("824L248Z2222P04V")
    //十六位十六进制数作为密钥
    //解密方法

function decrypt(word) {
    var decrypt = CryptoJS.AES.decrypt(word, key, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    })
    return CryptoJS.enc.Utf8.stringify(decrypt).toString()
}


service.interceptors.request.use(
    (config) => {
        let token = Cookies.get('ydcx_Yahv.Erp')
        if (token) {
            config.headers['token'] = 'Bearer ' + token;
            config.headers['authorization'] = '10cbce37a0a14f9a9d2305baac613b51'
        }
        return config
    },
    (err) => {
        Message({
            showClose: true,
            msg: '请求出错',
            type: 'error',
        })
        return Promise.reject(err)
    }
)
let errorflag = null
service.interceptors.response.use(

    (response) => {
        if ((typeof response.data).toLowerCase() == "string" && response.headers['encrypt'] == '1') {
            response.data = JSON.parse(decrypt(response.data))
        }
        const data = response.data
        if (response.status == 200) {
            return data
        } else {
            return { data: null, msg: '请求出错' }
        }
    },
    //接口错误状态处理，也就是说无响应时的处理
    (error) => {
        if (error && error.response) {
            switch (error.response.status) {
                case 400:
                    error.msg = '请求错误(400)'
                    break
                case 401:
                    error.msg = '未授权，请重新登录(401)'
                    window.location.reload()
                    break
                case 403:
                    error.msg = '拒绝访问(403)'
                    break
                case 404:
                    error.msg = '请求出错(404)'
                    break
                case 408:
                    error.msg = '请求超时(408)'
                    break
                case 500:
                    error.msg = '服务器错误(500)'
                    break
                case 501:
                    error.msg = '服务未实现(501)'
                    break
                case 502:
                    error.msg = '网络错误(502)'
                    break
                case 503:
                    error.msg = '服务不可用(503)'
                    break
                case 504:
                    error.msg = '网络超时(504)'
                    break
                case 505:
                    error.msg = 'HTTP版本不受支持(505)'
                    break
                default:
                    error.msg = `连接出错(${error.message || ''})!`
            }
        } else {
            error.msg = '连接出错 ' + error.message || '连接出错!'
        }
        if (errorflag) {
            errorflag.close()
        }
        errorflag = Message({
            showClose: true,
            message: `${error.msg}`,
            type: 'error',
        })
        return Promise.resolve(error)
    }
)

export { service }