import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import routes from './router';
import actions from '../src/utils/actions'; //导入实例
import './assets/icons'; // icon
import './assets/styles/element-variables.scss';
import '@/assets/styles/ruoyi.scss'; // ruoyi css


import './assets/styles/common.scss';
import './utils/element'; //包含自定义全局组件   用到的控件  自定义指令



import {parseTime,resetForm,selectDictLabel} from "@/utils/index";

// 全局方法挂载
Vue.prototype.parseTime = parseTime;
Vue.prototype.resetForm = resetForm;
Vue.prototype.selectDictLabel = selectDictLabel;

Vue.prototype.msgSuccess = function (msg) {
    this.$message({
        showClose: true,
        message: msg,
        type: 'success',
    });
};

Vue.prototype.msgError = function (msg) {
    this.$message({
        showClose: true,
        message: msg,
        type: 'error',
    });
};

Vue.prototype.msgInfo = function (msg) {
    this.$message.info(msg);
};

// 全局组件挂载
Vue.config.productionTip = false;
Vue.use(ElementUI);

//基座
let router = null;
let instance = null;

// 生命周期
let routerarr = null;
export async function bootstrap (props) {
    //初始化子程序
}
export async function mount (props) {
    //获取基座数据
    actions.setActions(props); //注入actions实例
    render(props);
    actions.onGlobalStateChange((state, prev) => {
        console.log('--------------------获取基座数据执行-------------------', props);
        // store.dispatch('SetInPermission', props.data.permissions);
        // store.dispatch('SetInToken', props.data.token);
    }, true);

    //监听全局状态  该方法可以获取到基座的全局数据 例如token 路由，用户信息，权限
    // if (state.token) store.dispatch("SetInToken", state.token);
    // if (state.permissions) store.dispatch("SetInPermission", state.permissions);
}

function render (props = {}) {
    const { container } = props;

    router = new VueRouter({
        mode: 'hash',
        routes,
    });
    router.beforeEach((to, from, next) => {
        next();
    });
    instance = new Vue({
        router,
        render: (h) => h(App),
    }).$mount(container ? container.querySelector('#app') : '#app');
}

export async function unmount () {
    instance.$destroy();
    instance.$el.innerHTML = '';
    instance = null;
    router = null;
}

// webpack打包公共文件路径
/*eslint no-undef: 0*/
if (window.__POWERED_BY_QIANKUN__) {
    __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__;
}
// 独立运行
if (!window.__POWERED_BY_QIANKUN__) {
    render();
}
