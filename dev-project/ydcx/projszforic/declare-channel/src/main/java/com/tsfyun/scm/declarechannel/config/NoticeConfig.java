package com.tsfyun.scm.declarechannel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 *
 * @Date 2020/3/4 14:20
 * @Version V1.0
 */
@Configuration
public class NoticeConfig {

    @Bean("noticeProperties")
    public NoticeProperties noticeProperties(){
        return new NoticeProperties();
    }

}
