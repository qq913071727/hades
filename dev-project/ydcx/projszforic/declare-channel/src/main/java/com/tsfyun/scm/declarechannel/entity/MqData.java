package com.tsfyun.scm.declarechannel.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: MQ消息记录
 * @CreateDate: Created in 2020/6/29 10:59
 */
@Data
public class MqData implements Serializable {

    private String id;

    /**
     * 消息id
     */
    private String msgId;

    /**
     * 交换机
     */
    private String exchange;

    /**
     * 路由键
     */
    private String routingKey;

    /**
     * 请求头
     */
    private String headers;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 原因
     */
    private String reason;

    /**
     * 创建时间
     */
    private LocalDateTime dateCreated;

}
