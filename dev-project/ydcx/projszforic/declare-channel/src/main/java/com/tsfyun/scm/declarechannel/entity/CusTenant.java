package com.tsfyun.scm.declarechannel.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 统一编号对应租户
 * </p>
 *

 * @since 2020-05-13
 */
@Data
public class CusTenant implements Serializable {

     private static final long serialVersionUID=1L;

    /**
     * 统一编号
     */
     private String id;

    /**
     * 租户编号
     */

    private String tenant;

    /**=
     * 创建时间
     */
    private LocalDateTime dateCreated;

    /**=
     * 订单号
     */
    private String docNo;

    /**=
     * 来源
     */
    private String source;



}
