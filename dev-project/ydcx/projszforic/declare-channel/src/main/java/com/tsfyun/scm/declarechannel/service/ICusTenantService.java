package com.tsfyun.scm.declarechannel.service;


import com.tsfyun.scm.declarechannel.entity.CusReceipt;
import com.tsfyun.scm.declarechannel.entity.CusTenant;

import java.util.List;

/**
 * <p>
 * 统一编号对应租户 服务类
 * </p>
 *

 * @since 2020-05-13
 */
public interface ICusTenantService {
    //存在则修改
    void saveExistenceUpdate(CusTenant cusTenant);
    //根据ID查询
    CusTenant getId(String id);
}
