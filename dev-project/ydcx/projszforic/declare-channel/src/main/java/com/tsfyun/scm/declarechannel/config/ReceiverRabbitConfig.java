package com.tsfyun.scm.declarechannel.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 消费者配置rabbitMQ
 *
 *
 */
@Slf4j
@Component
@Configuration
public class ReceiverRabbitConfig {

    /**
     * rabbitmq配置信息
     */
    @Value("${tsfyun.rabbitmq.host}")
    private String host;
    @Value("${tsfyun.rabbitmq.username}")
    private String username;
    @Value("${tsfyun.rabbitmq.password}")
    private String password;
    @Value("${tsfyun.rabbitmq.port}")
    private Integer port;
    @Value("${tsfyun.rabbitmq.virtual-host}")
    private String virtualHost;

    @Value("${tsfyun.tenant}")
    private String tenantCode;

    /**
     * 交换机名称
     */
    public static final String NOTICE_EXCHANGE_NAME = "noticeExchange";

    /**
     * saas系统报关队列和路由键
     */
    public static final String DECLARE_QUEUEA_NAME = "declare_receive";

    public static final String NOTICE_ROUTING_KEY = "declare_receive";

    /**
     * 报关系统报关队列和路由键
     */
    public static final String CUSTOMS_DECLARE_QUEUEA_NAME = "customs_declare_receive";

    public static final String CUSTOMS_NOTICE_ROUTING_KEY = "customs_declare_receive";

    @Bean("declareConnectionFactory")
    public ConnectionFactory declareConnectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host,port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }

    @Bean("customContainerFactory")
    public SimpleRabbitListenerContainerFactory containerFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer,
                                                                 ConnectionFactory declareConnectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConcurrentConsumers(2);
        factory.setMaxConcurrentConsumers(10);
        configurer.configure(factory, declareConnectionFactory);
        return factory;
    }

    /**
     * 交换机
     * @return
     */
    /*
    @Bean("declareExchange")
    public TopicExchange declareExchange(){
        return new TopicExchange(NOTICE_EXCHANGE_NAME);
    }
     */

    @Bean("declareExchange")
    public CustomExchange declareExchange(){
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "topic");
        return new CustomExchange(NOTICE_EXCHANGE_NAME, "x-delayed-message",true, false,args);
    }

    /**
     * 队列
     * @return
     */
    @Bean("declare")
    public Queue declare() {
        return QueueBuilder.durable(DECLARE_QUEUEA_NAME).build();
    }

    /**
     * 绑定
     * @return
     */
    @Bean
    public Binding declareBingding(){
        return BindingBuilder.bind(declare()).to(declareExchange()).with(NOTICE_ROUTING_KEY).noargs();
    }


    /**
     * 以下为报关系统队列和绑定配置
     */
    /**
     * 队列
     * @return
     */
    @Bean("customsDeclare")
    public Queue customsDeclare() {
        return QueueBuilder.durable(CUSTOMS_DECLARE_QUEUEA_NAME).build();
    }

    /**
     * 绑定
     * @return
     */
    @Bean
    public Binding customsDeclareBingding(){
        return BindingBuilder.bind(customsDeclare()).to(declareExchange()).with(CUSTOMS_NOTICE_ROUTING_KEY).noargs();
    }




}
 