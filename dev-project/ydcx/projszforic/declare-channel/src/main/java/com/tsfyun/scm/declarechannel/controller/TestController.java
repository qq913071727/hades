package com.tsfyun.scm.declarechannel.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @since Created in 2020/4/13 12:02
 */
@RestController
@RequestMapping(value = "test")
public class TestController {


    @GetMapping("testScan")
    public String testScan(){

        return "success";
    }


}
