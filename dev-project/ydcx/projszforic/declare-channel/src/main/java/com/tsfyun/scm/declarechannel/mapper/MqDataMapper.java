package com.tsfyun.scm.declarechannel.mapper;

import com.tsfyun.scm.declarechannel.entity.MqData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @CreateDate: Created in 2020/6/29 11:02
 */
@Repository
public interface MqDataMapper {

    int insert(MqData mqData);

    MqData findByMsgId(@Param(value = "msgId") String msgId);

    void updateMsg(@Param(value = "msgId") String msgId,@Param(value = "reason") String reason);

}
