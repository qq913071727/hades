package com.tsfyun.scm.declarechannel.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @since Created in 2020/4/21 17:48
 */
@Component
@Configuration
public class TenantQueueConfig {

    public static final String NOTICE_QUEUEA_PREFIX = "declareNotice.";

    @Value("${tsfyun.tenant}")
    private String tenantCode;

    @Bean("tenantQueue")
    public TenantQueue tenantQueue() {
        return new TenantQueue(NOTICE_QUEUEA_PREFIX + tenantCode);
    };

}
