package com.tsfyun.scm.declarechannel.service.impl;

import com.tsfyun.scm.declarechannel.entity.MqData;
import com.tsfyun.scm.declarechannel.mapper.MqDataMapper;
import com.tsfyun.scm.declarechannel.service.IMqDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description:
 * @CreateDate: Created in 2020/6/29 11:15
 */
@Service
public class MqDataServiceImpl implements IMqDataService {

    @Autowired
    private MqDataMapper mqDataMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(MqData mqData) {
        mqDataMapper.insert(mqData);
    }

    @Override
    public MqData findByMsgId(String msgId) {
        return mqDataMapper.findByMsgId(msgId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateMsg(String msgId, String reason) {
        mqDataMapper.updateMsg(msgId,reason);
    }

}
