package com.tsfyun.scm.declarechannel.service;

public interface ICusConfigService {

    void startUpdateCookie();

    void receiver(String message);
}
