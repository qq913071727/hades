package com.tsfyun.scm.declarechannel.support.dingding;


import lombok.extern.slf4j.Slf4j;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2019/12/13 14:20
 */
@Slf4j
public class DingTalkNoticeUtil {

    public static void send2DingDingOtherException(String tenantCode, String tenantName, String title, String content, Exception e, List<String> dingdingUrl) {
        try {
            new Thread(() -> {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String crashTime = df.format(new Date());
                String text = "####本地报关程序处理失败业务预警\n" +
                        "> 租户编码： " + tenantCode + "\n\n" +
                        "> 租户名称： " + tenantName + "\n\n" +
                        "> 出错描述： " + content + "\n\n" +
                        "> 堆栈信息： \n" + (Objects.nonNull(e) ? e.getMessage() : "") + "\n\n" +
                        "> ###### 出错时间： " + crashTime + " \n";
                new ExceptionDingTalkMessage(new DingTalkMessageBuilder().markdownMessage(title, text).at(null),dingdingUrl).send();
            }).start();
        } catch (Exception oe) {
            log.error("发送至钉钉异常", oe);
        }
    }

}
