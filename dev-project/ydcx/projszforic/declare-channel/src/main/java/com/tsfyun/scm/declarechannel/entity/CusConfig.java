package com.tsfyun.scm.declarechannel.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CusConfig implements Serializable {

    private static final long serialVersionUID=1L;

    private String id;
    private String val;
    private LocalDateTime dateUpdated;
}
