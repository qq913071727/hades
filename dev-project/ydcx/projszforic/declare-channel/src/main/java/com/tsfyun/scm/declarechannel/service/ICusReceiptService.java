package com.tsfyun.scm.declarechannel.service;


import com.tsfyun.scm.declarechannel.entity.CusReceipt;

import java.util.List;

/**
 * <p>
 * 报关单回执 服务类
 * </p>
 *

 * @since 2020-05-13
 */
public interface ICusReceiptService  {

    List<CusReceipt> getByEntryId(String entryId);

}
