package com.tsfyun.scm.declarechannel.service.impl;

import cn.hutool.http.HttpRequest;
import com.tsfyun.scm.declarechannel.service.IBusinessService;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class IBusinessServiceImpl implements IBusinessService {

    @Override
    public void synchro(String name){
        try{
            String url = "https://www.baidu.com/s?wd="+URLEncoder.encode(name,"UTF-8");
            log.info(url);
            String response = HttpRequest.get(url).addHeaders(new HashMap<String, String>(){{
                put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                put("Host","www.baidu.com");
                put("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
            }}).execute().body();
            Document document = Jsoup.parse(response);
            Elements elements = document.select("a");
            for(Element element : elements){
                String href = element.attr("href");
                if(href.startsWith("https://xin.baidu.com/detail/compinfo")){
                    compinfo(href);
                    break;
                }
            }
        }catch (Exception e){

            log.error("同步工商信息失败：",e);
        }
    }

    public void compinfo(String url){
        try {
            System.out.println(HttpRequest.get("https://xin.baidu.com/").execute().getCookies());
//            log.info("详情页面：" + url);
//            String response = HttpRequest.get(url).addHeaders(new HashMap<String, String>(){{
//                put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
//                put("Host","xin.baidu.com");
//                put("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
//                put("Cookie","BDPPN=3a789918645e083b37602c01268e8913; log_guid=f00393206c6b663541919b08f34b8f8c; BAIDUID=2A15ED25B2ABF8174F29689C78BF58A9:FG=1; PSTM=1571065786; BIDUPSID=45F31A801B473162B8FC86295606C4E7; ZX_HISTORY=%5B%7B%22visittime%22%3A%222019-10-21+22%3A22%3A56%22%2C%22pid%22%3A%22xlTM-TogKuTwJPp2DlBn6f5OHWWRgc34vAmd%22%7D%2C%7B%22visittime%22%3A%222019-09-20+19%3A47%3A43%22%2C%22pid%22%3A%22xlTM-TogKuTwyySdtdsXxFZJOL7Ox%2A7jYQmd%22%7D%5D; H_WISE_SIDS=138596_132920_139203_140720_139110_128065_139397_138105_139148_120166_138470_140833_138878_137978_140173_131247_132552_137743_118880_118876_118849_118831_118802_138165_138883_140260_136430_140267_140592_138146_140121_140075_139173_139627_140113_136196_131862_137105_140590_139430_139694_138585_133847_140793_137735_140066_134256_131423_140350_140367_140838_138662_110085_140327_127969_140593_140235_139978_139409_127416_138313_139909_140274_138425_139733_139971_140682_139926_140596_139599; BDUSS=jd5NXhQZHBXVGxOODFZWFVCZ1VJUG93QVd1TDVFTzFNUHpGdG5nRTRGUVlPbkJlSVFBQUFBJCQAAAAAAAAAAAEAAABFuzMHd3d3emhhbmdtaXNzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABitSF4YrUheM; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; yjs_js_security_passport=32f190e45044aed5bc987be19ce8b2f21bcf2c42_1589698286_js; Hm_lvt_baca6fe3dceaf818f5f835b0ae97e4cc=1589698207,1589700786,1589701183,1589702308; Hm_lpvt_baca6fe3dceaf818f5f835b0ae97e4cc=1589702308");
////                put("Sec-Fetch-Dest","document");
////                put("Sec-Fetch-Mode","navigate");
////                put("Sec-Fetch-Site","none");
////                put("Sec-Fetch-User","?1");
////                put("Upgrade-Insecure-Requests","1");
//            }}).execute().body();
//            System.out.println(response);


        }catch (Exception e){

            log.error("工商详情获取失败：",e);
        }
    }


    @Override
    public void suggest(String name) {
        Map<String,Object> params = new HashMap<>();
        params.put("q",name);
        params.put("t","0");
        String response = HttpRequest.post("https://xin.baidu.com/index/suggest").addHeaders(new HashMap<String, String>() {{
            put("Accept","application/json, text/javascript, */*; q=0.01");
            put("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
            put("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
        }}).setConnectionTimeout(10000).setReadTimeout(12000).form(params).execute().body();
        System.out.println(response);


    }
}
