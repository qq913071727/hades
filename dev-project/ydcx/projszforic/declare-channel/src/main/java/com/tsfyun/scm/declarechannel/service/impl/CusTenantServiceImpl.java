package com.tsfyun.scm.declarechannel.service.impl;

import com.tsfyun.scm.declarechannel.entity.CusTenant;
import com.tsfyun.scm.declarechannel.mapper.CusTenantMapper;
import com.tsfyun.scm.declarechannel.service.ICusTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 统一编号对应租户 服务实现类
 * </p>
 *

 * @since 2020-05-13
 */
@Service
public class CusTenantServiceImpl  implements ICusTenantService {

    @Autowired
    private CusTenantMapper cusTenantMapper;

    @Override
    public void saveExistenceUpdate(CusTenant cusTenant) {
        cusTenantMapper.saveExistenceUpdate(cusTenant);
    }

    @Override
    public CusTenant getId(String id) {
        if(StringUtils.isEmpty(id)){return null;}
        return cusTenantMapper.getId(id);
    }
}
