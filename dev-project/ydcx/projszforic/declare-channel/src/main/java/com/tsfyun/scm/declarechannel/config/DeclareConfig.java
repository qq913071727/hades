package com.tsfyun.scm.declarechannel.config;

import java.util.LinkedHashMap;
import java.util.Map;

public class DeclareConfig {

    //报关单目录
    public static final String DECCUS_PATH = "D:/ImpPath/Deccus001/";
    //舱单目录
    public static final String RMFT_PATH = "D:/ImpPath/Rmft/";
    //报关单回执状态
//    L-海关入库
//    E-退单
//    J/G-审结
//    A/D/Z-删单
//    P/K/W/I/X-放行
//    R-结关
//    C-查验通知

    public static final Map<String,String> DECCUS_REC_STATUS_MAP = new LinkedHashMap<String,String>(){{
        put("0","单一窗口已暂存");//自定义
        put("3","");
        put("7","申报到海关预录入系统成功");
        put("9","");
        put("a","签证，已被检务收单，领取证单");
        put("A","海关放行前删除");
        put("b","已发检验检疫审核");
        put("B","担保放行");
        put("c","撤单");
        put("C","海关无纸验放查验通知书（放行）");
        put("d","检务受理");
        put("D","海关放行后删除");
        put("E","海关退单/不受理");
        put("F","放行交单");
        put("G","海关接单交单");
        put("H","海关挂起，需手工申报");
        put("I","海关无纸放行通知（放行）");
        put("J","通关无纸化审结");
        put("K","通关无纸化担保放行");
        put("L","海关入库成功");
        put("M","报关单重审");
        put("N","补传随附单证电子数据");
        put("O","");
        put("P","海关已放行");
        put("R","海关已结关");
        put("S","施检");
        put("T","需交税费");
        put("U","");
        put("W","海关无纸验放通知（审结）");
        put("X","海关准予进港回执（上海洋山保税港区专用）");
        put("Y","申报失败");
        put("Z","退回修改");
    }};
}
