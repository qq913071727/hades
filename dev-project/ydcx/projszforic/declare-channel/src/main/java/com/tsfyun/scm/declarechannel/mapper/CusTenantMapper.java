package com.tsfyun.scm.declarechannel.mapper;

import com.tsfyun.scm.declarechannel.entity.CusTenant;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 统一编号对应租户 Mapper 接口
 * </p>
 *

 * @since 2020-05-13
 */
@Repository
public interface CusTenantMapper {

    void saveExistenceUpdate(CusTenant cusTenant);

    CusTenant getId(@Param(value = "id")String id);
}
