package com.tsfyun.scm.declarechannel.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeclareBizSendDTO implements Serializable {
    //租户
    private String tenant;
    //类型
    private String type;//declare(报关单)manifest(舱单)
    //下载地址
    private String downUrl;
    //文件名称
    private String fileName;
}
