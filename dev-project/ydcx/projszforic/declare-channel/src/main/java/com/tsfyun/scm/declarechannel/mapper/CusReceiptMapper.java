package com.tsfyun.scm.declarechannel.mapper;

import com.tsfyun.scm.declarechannel.entity.CusReceipt;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 报关单回执 Mapper 接口
 * </p>
 *

 * @since 2020-05-13
 */
@Repository
public interface CusReceiptMapper {

    List<CusReceipt> findByEntryId(String entryId);

}
