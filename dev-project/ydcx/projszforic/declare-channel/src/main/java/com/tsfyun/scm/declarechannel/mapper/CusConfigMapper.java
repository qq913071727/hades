package com.tsfyun.scm.declarechannel.mapper;

import com.tsfyun.scm.declarechannel.entity.CusConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface CusConfigMapper {

    void saveExistenceUpdate(CusConfig cusConfig);
}
