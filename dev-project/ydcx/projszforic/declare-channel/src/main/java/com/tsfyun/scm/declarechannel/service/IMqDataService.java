package com.tsfyun.scm.declarechannel.service;

import com.tsfyun.scm.declarechannel.entity.MqData;

/**
 * @Description:
 * @CreateDate: Created in 2020/6/29 11:15
 */
public interface IMqDataService {

    void save(MqData mqData);

    MqData findByMsgId(String msgId);

    void updateMsg(String msgId,String reason);

}
