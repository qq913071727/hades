package com.tsfyun.scm.declarechannel.service;

import com.tsfyun.scm.declarechannel.dto.DeclareBizSendDTO;

public interface IDeclareService {

    void receiver(DeclareBizSendDTO dto);

    void scanDeclareRecFile();

    void scanManifestRecFile();
}
