package com.tsfyun.scm.declarechannel.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;


/**
 * <p>
 * 报关单回执
 * </p>
 *

 * @since 2020-05-13
 */
@Data
public class CusReceipt implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private String cusCiqNo;

    /**
     * 报关单编号
     */

    private String entryId;

    /**
     * 通知时间
     */

    private LocalDateTime noticeDate;

    /**
     * 处理结果
     */

    private String channel;

    /**
     * 回执说明
     */

    private String note;

    /**
     * 申报地海关
     */

    private String customMaster;

    /**
     * 进出口日期
     */

    private String ieDate;

    /**
     * 申报日期
     */

    private LocalDateTime ddate;


}
