package com.tsfyun.scm.declarechannel;


import com.tsfyun.scm.declarechannel.config.NoticeProperties;
import com.tsfyun.scm.declarechannel.service.IDeclareService;
import com.tsfyun.scm.declarechannel.task.TaskCheck;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


@SpringBootApplication
@ComponentScan("com.tsfyun")
@MapperScan(basePackages = "com.tsfyun.scm.declarechannel.mapper")
@EnableScheduling
public class DeclarechannelApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DeclarechannelApplication.class, args);

        NoticeProperties noticeProperties = ctx.getBean(NoticeProperties.class);

        IDeclareService declareService = ctx.getBean(IDeclareService.class);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                TaskCheck.checkTask(noticeProperties.getDingdingUrl());
            }
        }, 0,5000);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(new Date() + "开始扫描回执目录");
                declareService.scanDeclareRecFile();
                declareService.scanManifestRecFile();
                TaskCheck.LAST_EXECUTE_TIME = LocalDateTime.now();
            }
        }, 0,5000);

    }

}
