package com.tsfyun.scm.declarechannel.service.impl;

import com.tsfyun.scm.declarechannel.entity.CusReceipt;
import com.tsfyun.scm.declarechannel.mapper.CusReceiptMapper;
import com.tsfyun.scm.declarechannel.service.ICusReceiptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 报关单回执 服务实现类
 * </p>
 *

 * @since 2020-05-13
 */
@Service
public class CusReceiptServiceImpl implements ICusReceiptService {

    @Autowired
    private CusReceiptMapper cusReceiptMapper;

    @Override
    public List<CusReceipt> getByEntryId(String entryId) {
        return cusReceiptMapper.findByEntryId(entryId);
    }
}
