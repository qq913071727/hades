package com.tsfyun.scm.declarechannel.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: 辅助消息监听队列动态名称
 * @since Created in 2020/4/21 17:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TenantQueue {

    private String name;


}
