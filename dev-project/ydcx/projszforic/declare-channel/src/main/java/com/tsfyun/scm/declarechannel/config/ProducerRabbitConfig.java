package com.tsfyun.scm.declarechannel.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 配置rabbitMQ
 *
 *
 */
@Slf4j
@Component
@Configuration
public class ProducerRabbitConfig {

    //交换机
    public static final String NOTICE_EXCHANGE_NAME = "noticeExchange";
    //报关报文接收
    public static final String DECLARE_RECEIVE = "scm_declare_receive";
    //报关单回执（发送到供应链服务）
    public static final String DECLARE_RECEIPT = "scm_declare_receipt";
}
