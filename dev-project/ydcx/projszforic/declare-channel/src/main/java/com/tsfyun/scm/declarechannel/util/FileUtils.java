package com.tsfyun.scm.declarechannel.util;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {

    /**=
     * 复制文件
     * @param source
     * @param dest
     */
    public static void copyFileByNIO(File source, File dest) {
        if(dest != null && !dest.exists()){
            dest.getParentFile().mkdirs();
        }
        FileChannel inf = null;
        FileChannel out = null;
        FileInputStream inStream = null;
        FileOutputStream outStream = null;
        try {
            inStream = new FileInputStream(source);
            outStream = new FileOutputStream(dest);
            inf = inStream.getChannel();
            out = outStream.getChannel();
            inf.transferTo(0, inf.size(), out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(inStream);
            close(inf);
            close(outStream);
            close(out);
        }
    }
    /**
     * 关闭所有流
     * @param closeable
     */
    private static void close(Closeable closeable){
        if(closeable != null){
            try {
                closeable.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**=
     * 压缩文件
     * @param zipFilePath
     * @param zipFile
     * @throws IOException
     */
    public static void execute(String zipFilePath,String zipFile) throws IOException {
        File file = new File(zipFilePath);
        if(file.exists()&&file.isDirectory()){
            File[] files = file.listFiles();
            InputStream input = null;
            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(new File(zipFile)));
            for (int i = 0; i < files.length; ++i) {
                input = new FileInputStream(files[i]);
                zipOut.putNextEntry(new ZipEntry(files[i].getName()));
                int temp = 0;
                while ((temp = input.read()) != -1) {
                    zipOut.write(temp);
                }
                input.close();
            }
            zipOut.close();
        }else if(file.exists()&&file.isFile()){
            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(new File(zipFile)));
            InputStream input = new FileInputStream(file);
            zipOut.putNextEntry(new ZipEntry(file.getName()));
            int temp = 0;
            while ((temp = input.read()) != -1) {
                zipOut.write(temp);
            }
            input.close();
            zipOut.close();
        }
    }
    /**=
     * 删除目录及下所有文件
     * @param dir
     * @return
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }
}
