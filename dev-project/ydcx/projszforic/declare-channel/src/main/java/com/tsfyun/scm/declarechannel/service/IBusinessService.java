package com.tsfyun.scm.declarechannel.service;

import java.io.UnsupportedEncodingException;

public interface IBusinessService {

    void synchro(String name);
    void compinfo(String url);
    void suggest(String name);
}
