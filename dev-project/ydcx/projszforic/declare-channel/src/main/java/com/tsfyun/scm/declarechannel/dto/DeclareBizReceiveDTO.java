package com.tsfyun.scm.declarechannel.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeclareBizReceiveDTO implements Serializable {
    //租户
    private String tenant;
    //类型
    private String type;//declare(报关单)manifest(舱单)
    //单据编号
    private String billNo;
    //报文状态
    private String status;//0:成功，1：失败
    //节点
    private String node;
    //备注信息
    private String memo;
    //状态标识
    private String channel;
    //关检关联号 (数据中心统一编号)
    private String cusCiqNo;
    //报关单编号 (内网返回的报关单号)
    private String entryId;
    //进出口日期
    private String ieDate;
    //申报日期
    private String ddate;
    //回执通知时间
    private String noticeDate;
    //文件名称
    private String fileName;
    //转关单号
    private String transDclNo;
    //舱单操作码（9：原始/预配舱单传输 3：删除 5：修改）
    private String functionCode;
}
