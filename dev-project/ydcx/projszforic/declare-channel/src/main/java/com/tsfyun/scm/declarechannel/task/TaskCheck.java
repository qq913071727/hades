package com.tsfyun.scm.declarechannel.task;

import cn.hutool.core.util.StrUtil;
import com.tsfyun.scm.declarechannel.support.dingding.DingTalkNoticeUtil;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2021/4/25 10:53
 */
@Slf4j
public class TaskCheck {

    //定时器最后执行时间
    public static LocalDateTime LAST_EXECUTE_TIME = null;

    public static void checkTask (List<String> dingdingUrl) {
        if(Objects.isNull(LAST_EXECUTE_TIME)) {
            return;
        }
        final LocalDateTime lastExecuteTime = LAST_EXECUTE_TIME;
        LocalDateTime checkTime = LocalDateTime.now();
        //比较当前时间和定时器最后执行时间，如果超过10秒则触发警告
        long diffTime = Duration.between(lastExecuteTime, checkTime).toMillis();
        if(diffTime / 1000 >= 10) {
            String memo = StrUtil.format("定时器最后执行时间【{}】，当前检查时间【{}】，已经超过10秒未执行", DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss").format(lastExecuteTime),
                    DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss").format(checkTime));
            log.info(memo);
            DingTalkNoticeUtil.send2DingDingOtherException("","","报关渠道》定时器执行状态异常",memo,null,dingdingUrl);
        }
    }

}
