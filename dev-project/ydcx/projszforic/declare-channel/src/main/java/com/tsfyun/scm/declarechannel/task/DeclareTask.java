package com.tsfyun.scm.declarechannel.task;

import com.tsfyun.scm.declarechannel.service.ICusConfigService;
import com.tsfyun.scm.declarechannel.service.IDeclareService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Description:
 * @since Created in 2020/5/12 18:25
 */
@Lazy(value = false)
@Component
@Slf4j
public class DeclareTask {

    @Autowired
    private IDeclareService declareService;
    @Autowired
    private ICusConfigService cusConfigService;
    /**
     * 每5秒钟执行一次
     * @throws InterruptedException
     */
    //@Scheduled(cron="*/5 * * * * ?")
    public void startScanCus() throws InterruptedException {
        //log.info("开始扫描回执目录");
        declareService.scanDeclareRecFile();
        declareService.scanManifestRecFile();
        TaskCheck.LAST_EXECUTE_TIME = LocalDateTime.now();
    }

//    @Scheduled(cron="0 */20 * * * ?")
//    @Scheduled(initialDelay = 1000,fixedRate = 1500000)
//    public void startUpdateCookie() throws InterruptedException {
//        log.info("开始更新COOKIE");
//        cusConfigService.startUpdateCookie();
//    }

}
