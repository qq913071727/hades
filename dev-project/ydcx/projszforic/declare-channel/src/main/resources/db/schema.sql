

-- ----------------------------
-- Table structure for cus_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cus_config` (
  `id` varchar(20) NOT NULL,
  `val` varchar(500) NOT NULL,
  `date_updated` datetime(4) NOT NULL DEFAULT CURRENT_TIMESTAMP(4) COMMENT '修改时间',
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for cus_receipt
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cus_receipt` (
  `id` bigint(20) NOT NULL,
  `cus_ciq_no` varchar(18) NOT NULL,
  `entry_id` varchar(18) DEFAULT NULL COMMENT '报关单编号',
  `notice_date` datetime NOT NULL COMMENT '通知时间',
  `channel` varchar(1) NOT NULL COMMENT '处理结果',
  `note` varchar(255) DEFAULT NULL COMMENT '回执说明',
  `custom_master` varchar(4) DEFAULT '' COMMENT '申报地海关',
  `ie_date` varchar(10) DEFAULT NULL COMMENT '进出口日期',
  `ddate` datetime DEFAULT NULL COMMENT '申报日期',
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for cus_tenant
-- ----------------------------
CREATE TABLE IF NOT EXISTS `cus_tenant` (
  `id` varchar(18)  NOT NULL COMMENT '统一编号',
  `tenant` varchar(32)  NOT NULL COMMENT '租户编号',
  `date_created` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `doc_no` varchar(20) DEFAULT NULL COMMENT '单据编号',
  `source` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for mq_data
-- ----------------------------
CREATE TABLE IF NOT EXISTS `mq_data` (
  `id` varchar(32) NOT NULL,
  `msg_id` varchar(64)  DEFAULT NULL COMMENT '消息id',
  `exchange` varchar(64)  DEFAULT NULL COMMENT '交换机',
  `routing_key` varchar(64) DEFAULT NULL COMMENT '路由键',
  `headers` varchar(1000)  DEFAULT NULL COMMENT '消息头',
  `content` varchar(2000)  DEFAULT NULL COMMENT '消息内容体',
  `reason` varchar(500)  DEFAULT NULL COMMENT '原因',
  `date_created` datetime DEFAULT NULL COMMENT '消息创建时间',
  PRIMARY KEY (`id`)
);

-- 表结构变更全放在后面-----------------------
