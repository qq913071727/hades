package com.tsfyun.scm.service.impl.report;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.ExportExcelFileDTO;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.util.DateUtils;
import com.tsfyun.scm.dto.file.ExcelKeyValue;
import com.tsfyun.scm.dto.file.ExportExcelParamsDTO;
import com.tsfyun.scm.dto.view.ViewImpDifferenceMemberQTO;
import com.tsfyun.scm.dto.view.ViewOrderCostMemberQTO;
import com.tsfyun.scm.mapper.view.ViewImpDifferenceMemberMapper;
import com.tsfyun.scm.mapper.view.ViewOrderCostMemberMapper;
import com.tsfyun.scm.service.file.IExportFileService;
import com.tsfyun.scm.service.report.IImpReportService;
import com.tsfyun.scm.vo.report.MonthCostVO;
import com.tsfyun.scm.vo.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class ImpReportServiceImpl implements IImpReportService {

    @Autowired
    private ViewOrderCostMemberMapper viewOrderCostMemberMapper;
    @Autowired
    private OrikaBeanMapper beanMapper;
    @Autowired
    private IExportFileService exportService;
    @Autowired
    private ViewImpDifferenceMemberMapper viewImpDifferenceMemberMapper;


    @Override
    public List<ViewOrderCostMemberVO> orderReceivableList(ViewOrderCostMemberQTO qto) {
        Map<String,Object> params = beanMapper.map(qto,Map.class);
        return viewOrderCostMemberMapper.orderReceivableList(params);
    }

    @Override
    public PageInfo<ViewOrderCostMemberVO> orderReceivablePageList(ViewOrderCostMemberQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        Map<String,Object> params = beanMapper.map(qto,Map.class);
        List<ViewOrderCostMemberVO> dataList = viewOrderCostMemberMapper.orderReceivableList(params);
        return new PageInfo<>(dataList);
    }

    @Override
    public ViewOrderCostMemberTotalVO orderReceivableTotal(ViewOrderCostMemberQTO qto) {
        Map<String,Object> params = beanMapper.map(qto,Map.class);
        return viewOrderCostMemberMapper.orderReceivableTotal(params);
    }

    @Override
    public Long exportOrderReceivableExcel(ViewOrderCostMemberQTO qto) throws Exception {
        //获取需要导出的数据
        List<ViewOrderCostMemberVO> dataList = orderReceivableList(qto);
        ExportExcelParamsDTO params = new ExportExcelParamsDTO();
        params.setFileName(String.format("订单应收%s", DateUtils.fromatShortNoSign(new Date())));
        params.setSheetName("订单应收");
        params.setOperator(SecurityUtil.getCurrentPersonIdAndName());
        params.setDatas(dataList);
        List<ExcelKeyValue> files = new ArrayList<ExcelKeyValue>(){{
            add(new ExcelKeyValue("docNo","订单号"));
            add(new ExcelKeyValue("periodDate","订单日期"));
            add(new ExcelKeyValue("customerName","客户名称"));
            add(new ExcelKeyValue("salePersonName","销售人员"));
            add(new ExcelKeyValue("totalPrice","委托金额"));
            add(new ExcelKeyValue("currencyName","币制"));
            add(new ExcelKeyValue("quoteTypeName","报价类型"));
            add(new ExcelKeyValue("totalCost","应收金额"));
            add(new ExcelKeyValue("writeValue","已收金额"));
            add(new ExcelKeyValue("arrearsValue","欠款金额"));
            add(new ExcelKeyValue("periodDate","账期日期"));
            add(new ExcelKeyValue("overdueDaysStr","逾期天数"));
            add(new ExcelKeyValue("huokuan","货款"));
            add(new ExcelKeyValue("shouxufei","手续费"));
            add(new ExcelKeyValue("dailifei","代理费"));
            add(new ExcelKeyValue("zenzhishui","增值税"));
            add(new ExcelKeyValue("guanshui","关税"));
            add(new ExcelKeyValue("xiaofeishui","消费税"));
            add(new ExcelKeyValue("mian3c","免3C认证费"));
            add(new ExcelKeyValue("guoleisonhuofei","国内送货费"));
            add(new ExcelKeyValue("shangjianfei","商检服务费"));
        }};
        params.setFiles(files);
        params.setTotal(Arrays.asList("totalPrice","totalCost","writeValue","arrearsValue","huokuan","shouxufei","dailifei","zenzhishui","guanshui","xiaofeishui","mian3c","guoleisonhuofei","shangjianfei"));
        return exportService.exportExcel(params);
    }

    @Override
    public PageInfo<ViewImpDifferenceMemberVO> impDifferenceMemberPage(ViewImpDifferenceMemberQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        return new PageInfo<>(viewImpDifferenceMemberMapper.impDifferenceMemberList(qto));
    }

    @Override
    public BigDecimal getCustomerDifference(Long customerId) {
        return viewImpDifferenceMemberMapper.getCustomerDifference(customerId);
    }

    @Override
    public Long exportImpDifferenceMember(ViewImpDifferenceMemberQTO qto) throws Exception {
        List<ViewImpDifferenceMemberVO> list = viewImpDifferenceMemberMapper.impDifferenceMemberList(qto);
        ExportExcelParamsDTO params = new ExportExcelParamsDTO();
        params.setFileName(String.format("进口票差明细%s", DateUtils.fromatShortNoSign(new Date())));
        params.setSheetName("进口票差明细");
        params.setOperator(SecurityUtil.getCurrentPersonIdAndName());
        params.setDatas(list);
        List<ExcelKeyValue> files = new ArrayList<ExcelKeyValue>(){{
            add(new ExcelKeyValue("docNo","订单编号"));
            add(new ExcelKeyValue("orderDate","订单日期"));
            add(new ExcelKeyValue("customerName","客户名称"));
            add(new ExcelKeyValue("salesDocNo","采购合同号"));
            add(new ExcelKeyValue("accountValueCny","付汇金额(CNY)"));
            add(new ExcelKeyValue("goodsVal","销售合同货款金额"));
            add(new ExcelKeyValue("balance","票差金额"));
            add(new ExcelKeyValue("differenceVal","票差调整金额"));
            add(new ExcelKeyValue("totalPrice","销售合同总价"));
        }};
        params.setFiles(files);
        return exportService.exportExcel(params);
    }

    @Override
    public PageInfo<ViewImpDifferenceSummaryVO> impDifferenceSummaryPage(ViewImpDifferenceMemberQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        return new PageInfo<>(viewImpDifferenceMemberMapper.impDifferenceSummaryList(qto));
    }

    @Override
    public Long exportImpDifferenceSummary(ViewImpDifferenceMemberQTO qto) throws Exception {
        List<ViewImpDifferenceSummaryVO> list = viewImpDifferenceMemberMapper.impDifferenceSummaryList(qto);
        ExportExcelParamsDTO params = new ExportExcelParamsDTO();
        params.setFileName(String.format("进口票差汇总%s", DateUtils.fromatShortNoSign(new Date())));
        params.setSheetName("进口票差汇总");
        params.setOperator(SecurityUtil.getCurrentPersonIdAndName());
        params.setDatas(list);
        List<ExcelKeyValue> files = new ArrayList<ExcelKeyValue>(){{
            add(new ExcelKeyValue("customerName","客户名称"));
            add(new ExcelKeyValue("differenceVal","总票差金额"));
        }};
        params.setFiles(files);
        return exportService.exportExcel(params);
    }

    @Override
    public Map<String, BigDecimal> monthAgencyFee(String year) {
        List<MonthCostVO> list = viewOrderCostMemberMapper.findMonthCost(year);
        if(CollectionUtil.isNotEmpty(list)){
            return list.stream().collect(Collectors.toMap(MonthCostVO::getMonth,(e)->e.getCost()));
        }
        return new LinkedHashMap<>();
    }
}
