package com.tsfyun.scm.dto.risk;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.enums.domain.RiskApprovalStatusEnum;
import com.tsfyun.common.base.enums.risk.RiskApprovalDocTypeEnum;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @since Created in 2020/5/8 14:08
 */
@Data
public class RiskApprovalQTO extends PaginationDto implements Serializable {

    @EnumCheck(clazz = RiskApprovalDocTypeEnum.class,message = "审核类型参数错误")
    private String docType;

    private String docNo;

    private String customerName;

    @EnumCheck(clazz = RiskApprovalStatusEnum.class,message = "状态参数错误")
    private String statusId;

    private String submitter;

    private String approver;

    /**
     * 开始创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 结束创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
