package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.exp.CollectionSourceEnum;
import com.tsfyun.common.base.enums.finance.CostClassifyEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * <p>
 * 订单费用响应实体
 * </p>
 *
 *
 * @since 2020-04-21
 */
@Data
@ApiModel(value="expOrderCost响应对象", description="订单费用响应实体")
public class ExpOrderCostVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    private String customerName;

    @ApiModelProperty(value = "订单")
    private Long expOrderId;
    @ApiModelProperty(value = "订单号")
    private String expOrderNo;
    @ApiModelProperty(value = "客户单号")
    private String customerOrderNo;

    @ApiModelProperty(value = "科目")
    private String expenseSubjectId;

    @ApiModelProperty(value = "科目名称")
    private String expenseSubjectName;

    @ApiModelProperty(value = "费用分类")
    private String costClassify;

    @ApiModelProperty(value = "支付方式")
    private String collectionSource;
    private String collectionSourceDesc;

    @ApiModelProperty(value = "实际金额")
    private BigDecimal actualAmount;

    @ApiModelProperty(value = "应收金额")
    private BigDecimal receAmount;

    @ApiModelProperty(value = "已收金额")
    private BigDecimal acceAmount;

    @ApiModelProperty(value = "是否系统自动生成")
    private Boolean isAutomatic;

    @ApiModelProperty(value = "标记-(系统预留字段)")
    private String mark;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "是否需要在账期内冲销(优先核销)")
    private Boolean isFirstWriteOff;

    @ApiModelProperty(value = "是否锁定 (可以核销)")
    private Boolean isLock;

    @ApiModelProperty(value = "发生日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime happenDate;

    @ApiModelProperty(value = "账期日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime periodDate;

    @ApiModelProperty(value = "开始计算滞纳金日期")
    private LocalDateTime lateFeeDate;

    @ApiModelProperty(value = "逾期利率")
    private BigDecimal overdueRate;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "是否允许修改")
    private Boolean isAllowEdit;

    private String costClassifyDesc;

    /**
     * 应付金额
     */

    private BigDecimal payAmount;

    /**
     * 扩展字段1
     */

    private String extend1;

    private String payCurrencyCode;

    private String payCurrencyName;

    public String getCostClassifyDesc () {
        return Optional.ofNullable(CostClassifyEnum.of(costClassify)).map(CostClassifyEnum::getName).orElse("");
    }

    public String getCollectionSourceDesc () {
        return Optional.ofNullable(CollectionSourceEnum.of(collectionSource)).map(CollectionSourceEnum::getName).orElse("");
    }


}
