package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 商品项包装信息
 * @since Created in 2020/4/22 12:04
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Packaging", propOrder = {
        "quantityQuantity",
})
public class Packaging {

    @XmlElement(name = "QuantityQuantity")
    private QuantityQuantity quantityQuantity;

    public Packaging () {

    }

    public Packaging (QuantityQuantity quantityQuantity) {
        this.quantityQuantity = quantityQuantity;
    }


    public QuantityQuantity getQuantityQuantity() {
        return quantityQuantity;
    }

    public void setQuantityQuantity(QuantityQuantity quantityQuantity) {
        this.quantityQuantity = quantityQuantity;
    }
}
