package com.tsfyun.scm.service.materiel;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.materiel.MaterielLogQTO;
import com.tsfyun.scm.entity.materiel.Materiel;
import com.tsfyun.scm.entity.materiel.MaterielExp;
import com.tsfyun.scm.entity.materiel.MaterielLog;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.materiel.MaterielLogVO;

import java.util.List;

/**
 * <p>
 * 归类变更日志 服务类
 * </p>
 *
 *
 * @since 2020-03-26
 */
public interface IMaterielLogService extends IService<MaterielLog> {

    void recordMaterielLog(Materiel history,Materiel materiel);
    void recordMaterielLog(MaterielExp history, MaterielExp materiel);

    String comparativeChange(Materiel history,Materiel materiel);
    String comparativeChange(MaterielExp history,MaterielExp materiel);

    Result<List<MaterielLogVO>> pageList(MaterielLogQTO qto);

    void batchSave(List<MaterielLog> materielLogList);

    void updateMaterielId(String oldId,String newId);

    void removeByMaterielId(String materielId);
}
