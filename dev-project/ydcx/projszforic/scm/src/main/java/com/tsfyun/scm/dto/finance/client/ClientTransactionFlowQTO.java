package com.tsfyun.scm.dto.finance.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.enums.finance.TransactionCategoryEnum;
import com.tsfyun.common.base.enums.finance.TransactionTypeEnum;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: 客户端交易流水请求实体
 * @CreateDate: Created in 2020/11/9 16:00
 */
@Data
@ApiModel(value="TransactionFlow查询请求对象", description="交易流水查询请求实体")
public class ClientTransactionFlowQTO extends PaginationDto implements Serializable {

   @JsonIgnore
   private Long customerId;

   @EnumCheck(clazz = TransactionTypeEnum.class,message = "交易类型错误")
   private String transactionType;

   @EnumCheck(clazz = TransactionCategoryEnum.class,message = "交易类别错误")
   private String transactionCategory;

   private String transactionNo;

   private LocalDateTime dateCreatedStart;

   private LocalDateTime dateCreatedEnd;

   public void setDateCreatedEnd(LocalDateTime dateCreatedEnd) {
      if(dateCreatedEnd != null){
         this.dateCreatedEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateCreatedEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
      }
   }

}
