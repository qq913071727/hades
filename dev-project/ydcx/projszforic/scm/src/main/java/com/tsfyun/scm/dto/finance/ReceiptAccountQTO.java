package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 * 收款单请求实体
 * </p>
 *
 * @since 2020-04-15
 */
@Data
@ApiModel(value="ReceiptAccount查询请求对象", description="收款单查询请求实体")
public class ReceiptAccountQTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String customerName;

   private String docNo;

   private String statusId;

   private String receivingMode;

   /**
    * 收款开始时间
    */
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private LocalDateTime accountDateStart;

   /**
    * 收款结束时间
    */
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private LocalDateTime accountDateEnd;

   public void setAccountDateEnd(LocalDateTime accountDateEnd) {
      if(accountDateEnd != null){
         this.accountDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(accountDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
      }
   }


}
