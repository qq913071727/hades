package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**=
 * 保存归类
 */
@Data
public class ClassifyMaterielDTO implements Serializable {

    private static final long serialVersionUID=1L;

    @NotEmptyTrim(message = "请选择您要归类的物料")
    private String id;

    @NotEmptyTrim(message = "外文品牌不能为空")
    private String brand;

//    @NotEmptyTrim(message = "中文品牌不能为空")
    private String brandZh;

    @NotEmptyTrim(message = "物料名称不能为空")
    @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
    private String name;

    @NotEmptyTrim(message = "海关编码不能为空")
    @LengthTrim(min = 10,max = 10,message = "海关编码必须10位")
    private String hsCode;

    @LengthTrim(min = 3,max = 3,message = "CIQ编码必须3位")
    private String ciqNo;

    private Boolean isSmp;//战略物资
    private Boolean isCapp;//需要3C证书(当检验检疫为L时)
    private Boolean isCappNo;//需要3C目录鉴定
    @LengthTrim(max = 50,message = "3C证书编号最大长度不能超过50位")
    private String cappNo;//3C证书编号

    @LengthTrim(max = 255,message = "归类备注最大长度不能超过255位")
    private String memo;

    //申报要素值
    List<String> elementsVal;

}
