package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class ClaimedAmountPlusDTO implements Serializable {

    @NotNull(message = "ID不能为空")
    private Long id;


    @ApiModelProperty(value = "订单")
    @NotNull(message = "请至少勾选一条订单信息",groups = UpdateGroup.class)
    @Valid
    private List<ClaimedAmountOrderDTO> orders;
}
