package com.tsfyun.scm.vo.wms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description:
 * @since Created in 2020/4/24 13:42
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReceivingNotePlusDetailVO {

    /**
     * 入库单主单信息
     */
    private ReceivingNoteVO receivingNote;

    /**
     * 入库单明细
     */
    private List<ReceivingNoteMemberVO> members;

}
