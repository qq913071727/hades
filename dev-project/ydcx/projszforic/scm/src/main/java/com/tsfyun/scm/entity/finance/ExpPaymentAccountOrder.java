package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import tk.mybatis.mapper.annotation.KeySql;
import javax.persistence.Id;
import java.io.Serializable;
import lombok.Data;


/**
 * <p>
 * 付款订单明细
 * </p>
 *
 *
 * @since 2021-11-17
 */
@Data
public class ExpPaymentAccountOrder implements Serializable {

     private static final long serialVersionUID=1L;


    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 订单ID
     */

    private Long orderId;

    /**
     * 付款金额
     */

    private BigDecimal accountValue;

    /**
     * 结汇汇率
     */

    private BigDecimal customsRate;

    /**
     * 付款单主单ID
     */

    private Long paymentAccountId;


}
