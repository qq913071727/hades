package com.tsfyun.scm.entity.logistics;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 送货单
 * </p>
 *

 * @since 2020-05-25
 */
@Data
public class DeliveryWaybill implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    private Long id;

    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;

    /**
     * 送货单号
     */

    private String docNo;

    /**
     * 备注
     */

    private String memo;


    /**
     * 客户id
     */

    private Long customerId;

    /**=
     * 订单
     */
    private Long impOrderId;

    /**=
     * 订单编号
     */
    private String impOrderNo;

    /**
     * 国内收货方式
     */

    private String receivingMode;

    /**
     * 国内收货方式
     */

    private String receivingModeName;

    /**
     * 运单日期
     */

    private LocalDateTime transDate;

    /**
     * 发车时间
     */

    private LocalDateTime departureDate;

    /**
     * 签收时间
     */

    private LocalDateTime reachDate;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 单据类型-枚举
     */

    private String billType;

    /**
     * 仓库
     */

    private Long warehouseId;

    /**
     * 发货公司
     */

    private String shippingCompany;

    /**
     * 发货地址
     */

    private String shippingAddress;

    /**
     * 发货联系人
     */

    private String shippingLinkMan;

    /**
     * 发货联系人电话
     */

    private String shippingTel;

    /**
     * 快递单号
     */

    private String experssNo;

    /**
     * 封条号
     */

    private String sealNo;

    /**
     * 运输供应商
     */

    private Long transportSupplierId;

    /**
     * 运输工具
     */

    private Long conveyanceId;

    /**
     * 车牌1
     */

    private String conveyanceNo;

    /**
     * 车牌2
     */

    private String conveyanceNo2;

    /**
     * 司机信息
     */

    private String driverInfo;

}
