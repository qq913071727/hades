package com.tsfyun.scm.mapper.customer;

import com.tsfyun.scm.entity.customer.CustomerExpressQuote;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 客户快递模式代理费报价 Mapper 接口
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Repository
public interface CustomerExpressQuoteMapper extends Mapper<CustomerExpressQuote> {

    /**
     * 根据报价主单ID删除客户按重计费报价
     * @param agreementId
     * @return
     */
    Integer removeByAgreementId(@Param(value = "agreementId")Long agreementId);
}
