package com.tsfyun.scm.service.customer;

import com.tsfyun.scm.entity.customer.CustomerExpressQuote;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.CustomerExpressQuoteMemberVO;
import com.tsfyun.scm.vo.customer.CustomerExpressQuotePlusVO;
import com.tsfyun.scm.vo.customer.ExpressStandardQuoteMemberVO;

import java.util.List;

/**
 * <p>
 * 客户快递模式代理费报价 服务类
 * </p>
 *
 *
 * @since 2020-10-30
 */
public interface ICustomerExpressQuoteService extends IService<CustomerExpressQuote> {

    /**
     * 根据客户id和报价类型获取客户代理费报价信息
     * @param customerId
     * @param quoteType
     * @return
     */
    CustomerExpressQuote getExistsCustomerQuote(Long customerId,String quoteType);

    /**
     * 根据代理费标准报价创建客户代理费报价
     */
    void createCustomerExpressQuoteByStandard(Long customerId);

    /**
     * 根据客户和报价类型获取报价主表和明细信息
     * @param customerId
     * @param quoteType
     * @return
     */
    CustomerExpressQuotePlusVO getCustomerExpressQuote(Long customerId, String quoteType);

    /**
     * 根据报价类型获取标准报价信息（含明细）
     * @param quoteType
     * @return
     */
    CustomerExpressQuotePlusVO getStandardExpressQuote(String quoteType);

    /**
     * 根据客户和报价类型获取报价主表和明细信息
     * @param customerName
     * @param quoteType
     * @return
     */
    CustomerExpressQuotePlusVO getCustomerExpressQuoteByName(String customerName, String quoteType);

    /**
     * 根据基础物流报价保存客户报价
     * @param customerId
     * @param expressStandardQuoteMemberVOList
     * @return
     */
    CustomerExpressQuote saveByBaseExpressQuote(Long customerId,String quoteType,List<ExpressStandardQuoteMemberVO> expressStandardQuoteMemberVOList);

    /**
     * 根据报价主单ID删除客户按重计费报价
     * @param agreementId
     */
    void removeByAgreementId(Long agreementId);
}
