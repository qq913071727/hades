package com.tsfyun.scm.controller.order;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.scm.service.order.IPriceFluctuationHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 价格波动历史 前端控制器
 * </p>
 *
 *
 * @since 2020-04-17
 */
@RestController
@RequestMapping("/priceFluctuationHistory")
public class PriceFluctuationHistoryController extends BaseController {

    @Autowired
    private IPriceFluctuationHistoryService priceFluctuationHistoryService;

    @PostMapping(value = "list")
    public Result<List<PriceFluctuationHistory>> list(@RequestParam(value = "memberId")Long memberId){
        return success(priceFluctuationHistoryService.list(memberId));
    }
}

