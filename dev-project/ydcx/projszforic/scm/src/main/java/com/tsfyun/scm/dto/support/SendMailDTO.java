package com.tsfyun.scm.dto.support;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 发送邮件请求实体 (不带附件)
 * @CreateDate: Created in 2020/12/16 10:31
 */
@Data
public class SendMailDTO implements Serializable {

    /**
     * 发送邮件的id
     */
    private Long id;

    /**
     * 收件人邮箱
     */
    private List<String> receivers;
    /**
     * 邮箱标题
     */
    private String title;

    /**
     * 邮箱内容
     */
    private String content;

    /**
     * 文件id集合（邮件带附件）
     */
    private List<Long> fileIds;

    /**
     * 删除文件标志
     */
    private Boolean removeFileFlag;


}
