package com.tsfyun.scm.controller.finance;


import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.validator.ValidatorUtils;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.finance.*;
import com.tsfyun.scm.service.finance.IPurchaseContractService;
import com.tsfyun.scm.vo.finance.CreateDrawbackPlusVO;
import com.tsfyun.scm.vo.finance.ExpInvoiceNotificationVO;
import com.tsfyun.scm.vo.finance.PurchaseContractPlusVO;
import com.tsfyun.scm.vo.finance.PurchaseContractVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 采购合同 前端控制器
 * </p>
 *
 *
 * @since 2021-09-26
 */
@RestController
@RequestMapping("/purchaseContract")
public class PurchaseContractController extends BaseController {

    private final IPurchaseContractService purchaseContractService;

    public PurchaseContractController(IPurchaseContractService purchaseContractService) {
        this.purchaseContractService = purchaseContractService;
    }

    /**
     * 分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<PurchaseContractVO>> pageList(@ModelAttribute PurchaseContractQTO qto) {
        PageInfo<PurchaseContractVO> page = purchaseContractService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情信息
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<PurchaseContractPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(purchaseContractService.detailPlus(id,operation));
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping(value = "delete")
    public Result<Void> detail(@RequestParam(value = "id")Long id) {
        purchaseContractService.delete(id);
        return success();
    }

    /**
     * 根据订单ID生成采购合同
     * @param id
     * @return
     */
    @PostMapping(value = "createByOrderId")
    public Result<CreateContractPlusVO> createByOrderId(@RequestParam(value = "id")Long id){
        return success(purchaseContractService.createByOrderId(id));
    }

    /**
     * 重新计算费用
     * @return
     */
    @PostMapping(value = "recalculate")
    public Result<CreateContractPlusVO> recalculate(@ModelAttribute @Validated(UpdateGroup.class) PurchaseContractDTO dto){
        return success(purchaseContractService.recalculate(dto));
    }

    /**
     * 保存采购合同
     * @return
     */
    @PostMapping(value = "save")
    public Result<Void> save(@ModelAttribute @Validated(AddGroup.class) PurchaseContractDTO dto){
        purchaseContractService.save(dto);
        return success();
    }

    /**=
     * 采购合同确认
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirm")
    public Result<Void> confirm(@ModelAttribute @Valid TaskDTO dto){
        purchaseContractService.confirm(dto);
        return success();
    }

    /**=
     * 采购合同收票
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirmInvoice")
    public Result<Void> confirmInvoice(@ModelAttribute @Valid PurchaseInvoiceDTO dto){
        purchaseContractService.confirmInvoice(dto);
        return success();
    }

    /**
     * 根据采购合同初始化退税
     * @return
     */
    @GetMapping(value = "initDrawback")
    public Result<CreateDrawbackPlusVO> initDrawback(@RequestParam(value = "id")String id){
        return success(purchaseContractService.initDrawback(StringUtils.stringToLongs(id)));
    }

    /**
     * 获取开票通知信息
     * @param expOrderNo
     * @return
     */
    @GetMapping(value = "obtainExpInvoiceNotificationData")
    public Result<ExpInvoiceNotificationVO> obtainExpInvoiceNotificationData(@RequestParam(value = "expOrderNo")String expOrderNo) {
        return success(purchaseContractService.obtainExpInvoiceNotificationData(expOrderNo));
    }

    /**
     * 税局退税登记
     * @return
     */
    @PostMapping(value = "taxBureauRegister")
    public Result<Void> taxBureauRegister(@ModelAttribute @Validated TaxBureauDTO dto){
        purchaseContractService.taxBureauRegister(dto);
        return success();
    }

    /**
     * 根据采购合同发送邮件
     * @param id
     * @return
     */
    @PostMapping(value = "sendMail")
    public Result<Void> sendMail(@RequestParam(value = "id")Long id,@RequestParam(value = "receivers")String receivers){
        List<String> receiverList = Arrays.asList(receivers.replace("，",",").split(","));
        receiverList.stream().forEach(r-> {
            ValidatorUtils.isTrue(Validator.isEmail(r),()->new ServiceException(StrUtil.format("邮箱{}格式错误",r)));
        });
        purchaseContractService.sendMail(id,receiverList);
        return success();
    }

    /**
     * 导出PDF
     * @param id
     * @return
     */
    @GetMapping(value = "exportPdf")
    public Result<Long> exportPdf(@RequestParam(value = "id")Long id){
        return Result.success(purchaseContractService.contractPdf(id));
    }

    /**
     * 获取采购合同默认发送邮箱
     * @param customerName
     * @return
     */
    @PostMapping(value = "getDefaultMail")
    public Result<String> getDefaultMail(@RequestParam(value = "customerName")String customerName){
        return success(purchaseContractService.getDefaultMail(customerName));
    }

    /**
     * 导出送货单PDF
     * @param id
     * @return
     */
    @GetMapping(value = "exportDeliveryPdf")
    public Result<Long> exportDeliveryPdf(@RequestParam(value = "id")Long id){
        return Result.success(purchaseContractService.deliveryPdf(id));
    }

}

