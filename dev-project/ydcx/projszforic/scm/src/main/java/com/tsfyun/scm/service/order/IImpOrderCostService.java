package com.tsfyun.scm.service.order;

import com.tsfyun.common.base.dto.ExportFileDTO;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderCost;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.PaymentCostInfoVO;
import com.tsfyun.scm.vo.order.AdviceReceiptVO;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;
import com.tsfyun.scm.vo.order.OrderCostInfoVO;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单费用 服务类
 * </p>
 *
 *
 * @since 2020-04-21
 */
public interface IImpOrderCostService extends IService<ImpOrderCost> {
    /**=
     * 订单费用计算
     * @param impOrder
     */
    void calculationCost(ImpOrder impOrder);
    void calculationCost(ImpOrder impOrder, LocalDateTime orderDate);

    /**=
     * 根据订单Id删除系统自动生成的费用
     * @param orderId
     */
    void removeAutomaticOrderId(Long orderId);

    /**=
     * 订单费用信息(确认进口展示)
     * @param orderId
     * @return
     */
    OrderCostInfoVO orderCostInfo(Long orderId);
    OrderCostInfoVO orderCostInfo(ImpOrder impOrder);

    /**=
     * 付款单费用信息(确认付款单展示)
     * @param paymentId
     * @return
     */
    PaymentCostInfoVO paymentCostInfo(Long paymentId);

    /**=
     * 获取订单总费用(排除货款,手续费)
     * @param orderId
     * @return
     */
    BigDecimal orderCostTotal(Long orderId);
    /**=
     * 获取订单未锁定的费用
     * @param orderId
     * @return
     */
    List<ImpOrderCostVO> obtainOrderNotLockCosts(Long orderId);

    /**=
     * 获取订单所有费用明细
     * @param orderId
     * @return
     */
    List<ImpOrderCostVO> orderCostList(Long orderId);
    List<ImpOrderCost> findByOrderId(Long orderId);

    /**=
     * 获取订单应支付金额
     * @param orderId
     * @return
     */
    BigDecimal orderCostPayTotal(Long orderId);

    /**=
     * 客户逾期费用
     * @param customerId
     * @return
     */
    BigDecimal customerArrearsTotal(Long customerId);

    /**=
     * 客户应收未核销金额(排除货款)
     * @param customerId
     * @return
     */
    BigDecimal customerTaxTotal(Long customerId);

    /**=
     * 客户应收未核销金额(货款)
     * @param customerId
     * @return
     */
    BigDecimal customerGoodsTotal(Long customerId);

    /**=
     * 进口锁定费用
     * @param orderId
     */
    void impLockCost(Long orderId);

    /**
     * 解锁费用
     * @param costIds
     */
    void impUnLockCost(List<Long> costIds);

    /**
     * 根据订单获取可开服务费发票费用信息
     * @param orderId
     * @return
     */
    List<ImpOrderCost> getAgentOrderCosts(Long orderId);

    /**
     * 根据标记类型和科目查询费用
     * @param mark
     * @param expenseSubjectId
     * @return
     */
    List<ImpOrderCost> getByMarkAndExpenseSubject(String mark,String expenseSubjectId);

    /**=
     * 收款通知
     * @param orderId
     * @return
     */
    AdviceReceiptVO adviceReceipt(Long orderId);

    /**
     * 导出收款通知Excel
     * @param orderId
     * @return
     */
    Long exportAdviceReceiptExcel(Long orderId);

    /**=
     * 根据客户获取可核销的费用
     * @param customerId
     * @return
     */
    List<ImpOrderCostVO> canWriteOffList(Long customerId);

    /**=
     * 根据订单ID查询可核销的费用
     * @param orderId
     * @return
     */
    List<ImpOrderCostVO> canWriteOffByOrderId(Long orderId);


    /**
     * 根据订单id或者订单号获取所有的费用信息
     * @param orderId
     * @return
     */
    List<ImpOrderCostVO> getAllCosts(Long orderId,String impOrderNo);

    /**
     * 根据费用科目编码获取费用信息
     * @param expenseSubjectId
     * @return
     */
    List<ImpOrderCost> getByExpenseSubject(Long orderId,String expenseSubjectId);

    /**
     * 校验订单是否可以调整费用，并可以同时返回费用信息
     * @param impOrderNo
     * @param getCost
     */
    List<ImpOrderCostVO> checkGetAdjustCost(String impOrderNo,Boolean getCost);

    /**
     * 获取订单费用的信息
     * @param impOrderCostId
     * @return
     */
    ImpOrderCostVO getSimpleCostInfo(Long impOrderCostId);

    /**
     * 根据订单id生成收款通知书pdf
     * @param orderId
     * @return
     */
    Long createOrderCostPdf(Long orderId);

    /**
     * 根据订单id
     * @param orderId
     * @return
     */
    Long createOrderCostExcel(Long orderId);

}
