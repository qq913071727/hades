package com.tsfyun.scm.mapper.support;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.support.TaskNoticeContentQTO;
import com.tsfyun.scm.entity.support.TaskNoticeContent;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.support.TaskNoticeContentVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务通知内容 Mapper 接口
 * </p>
 *

 * @since 2020-04-05
 */
@Repository
public interface TaskNoticeContentMapper extends Mapper<TaskNoticeContent> {

    @DataScope(tableAlias = "tnc",customerTableAlias = "c")
    List<TaskNoticeContentVO> list(Map<String,Object> params);

    @DataScope(tableAlias = "tnc",customerTableAlias = "c")
    int count(Map<String,Object> params);
}
