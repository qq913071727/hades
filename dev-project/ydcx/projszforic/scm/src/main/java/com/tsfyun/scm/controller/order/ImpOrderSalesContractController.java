package com.tsfyun.scm.controller.order;

import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.order.IImpOrderSalesContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @Description:
 * @Project: 供应链saas平台
 * @CreateDate: Created in 2020/5/26 13:04
 */
@RestController
@RequestMapping(value = "impOrderSalesContract")
public class ImpOrderSalesContractController extends BaseController {

    @Autowired
    private IImpOrderSalesContractService impOrderSalesContractService;

    /**=
     * 自营订单生成销售合同
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "generateSalesContract")
    public Result<Void> generateSalesContract(@RequestParam(value = "id")Long id,@RequestParam(value = "adjustmentAmount") BigDecimal adjustmentAmount){
        impOrderSalesContractService.generateImpSalesContract(id,adjustmentAmount);
        return success();
    }

}
