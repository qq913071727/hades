package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.order.ExpOrderPrice;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.ExpOrderPriceVO;
import com.tsfyun.scm.vo.order.ImpOrderPriceVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Repository
public interface ExpOrderPriceMapper extends Mapper<ExpOrderPrice> {

    @DataScope(tableAlias = "a",customerTableAlias = "c",addCustomerNameQuery=true)
    List<ExpOrderPriceVO> list(Map<String,Object> params);

    Integer removeByOrderId(@Param(value = "orderId") Long orderId);
}
