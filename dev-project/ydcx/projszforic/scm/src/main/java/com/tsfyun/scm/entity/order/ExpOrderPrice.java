package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Data
public class ExpOrderPrice implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    private Long id;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 供应商
     */

    private Long supplierId;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 订单
     */

    private Long orderId;

    /**
     * 订单号
     */

    private String orderDocNo;

    /**
     * 审核时间
     */

    private Date reviewTime;

    /**
     * 审核人
     */

    private String reviewer;

    /**
     * 提交次数
     */

    private Integer submitCount;


}
