package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 变更信息
 * @since Created in 2020/4/23 12:07
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Amendment", propOrder = {
        "changeInformation",
        "changeReasonCode",
})
public class Amendment {

    @XmlElement(name = "ChangeInformation")
    private ChangeInformation changeInformation;

    @XmlElement(name = "ChangeReasonCode")
    private String changeReasonCode;


}
