package com.tsfyun.scm.vo.finance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/22 10:12
 */
@Data
public class ImpSalesContractMainVO implements Serializable {

    @ApiModelProperty(value = "合同日期")
    private LocalDateTime contractDate;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "客户单号")
    private String customerOrderNo;

    @ApiModelProperty(value = "购货单位名称")
    private String buyerName;

    @ApiModelProperty(value = "买方银行名称")
    private String buyerBankName;

    @ApiModelProperty(value = "买方纳税人识别号")
    private String buyerTaxpayerNo;

    @ApiModelProperty(value = "买方银行账号")
    private String buyerBankAccount;

    @ApiModelProperty(value = "买方银行地址")
    private String buyerBankAddress;

    @ApiModelProperty(value = "买方联系电话")
    private String buyerLinkPerson;

    @ApiModelProperty(value = "买方银行电话")
    private String buyerBankTel;

    @ApiModelProperty(value = "卖方名称")
    private String sellerName;

    @ApiModelProperty(value = "卖方银行名称")
    private String sellerBankName;

    @ApiModelProperty(value = "卖方银行账号")
    private String sellerBankAccount;

    @ApiModelProperty(value = "卖方银行地址")
    private String sellerBankAddress;

    @ApiModelProperty(value = "合同金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "卖方印章")
    private String sellerChapter;

}
