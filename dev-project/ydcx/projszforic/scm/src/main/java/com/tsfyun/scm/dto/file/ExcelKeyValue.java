package com.tsfyun.scm.dto.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelKeyValue implements Serializable {

    private String key;
    private String value;
}
