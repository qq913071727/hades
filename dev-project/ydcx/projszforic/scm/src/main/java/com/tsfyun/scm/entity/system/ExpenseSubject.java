package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Id;
import java.time.LocalDateTime;


/**
 * <p>
 * 费用会计科目
 * </p>
 *

 * @since 2020-03-16
 */
@Data
@Accessors(chain = true)
public class ExpenseSubject {

     private static final long serialVersionUID=1L;

     //主键人工赋值
    @Id
    private String id;

    /**
     * 科目名称
     */

    private String name;

    /**
     * 禁用状态
     */

    private Boolean disabled;

    /**
     * 锁定状态
     */

    private Boolean locking;


    /**
     * 备注信息
     */

    private String memo;


    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;


}
