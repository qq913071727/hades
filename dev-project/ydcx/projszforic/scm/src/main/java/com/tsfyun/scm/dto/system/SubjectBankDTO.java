package com.tsfyun.scm.dto.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-03-20
 */
@Data
@ApiModel(value="SubjectBank请求对象", description="请求实体")
public class SubjectBankDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   private Long subjectId;

   @NotEmptyTrim(message = "银行名称不能为空")
   @LengthTrim(max = 255,message = "银行名称最大长度不能超过255位")
   @ApiModelProperty(value = "银行名称")
   private String name;

   @NotEmptyTrim(message = "银行账号不能为空")
   @LengthTrim(max = 255,message = "银行账号最大长度不能超过255位")
   @ApiModelProperty(value = "银行账号")
   private String account;

   @LengthTrim(max = 255,message = "银行地址最大长度不能超过255位")
   @ApiModelProperty(value = "银行地址")
   private String address;

   @ApiModelProperty(value = "禁用标示")
   private Boolean disabled;

   @ApiModelProperty(value = "是否默认")
   private Boolean isDefault;

   @ApiModelProperty(value = "默认收款账户")
   private Boolean receivables;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @ApiModelProperty(value = "币制")
   private String currencyId;

   @NotEmptyTrim(message = "币制不能为空")
   @ApiModelProperty(value = "币制")
   private String currencyName;

}
