package com.tsfyun.scm.config.redis;

import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.util.StringUtils;

import java.time.Duration;

/**
 * @Description:
 * @since Created in 2019/11/12 10:06
 */
public class TsfRedisCacheManager extends RedisCacheManager {

    public TsfRedisCacheManager(RedisCacheWriter cacheWriter, RedisCacheConfiguration defaultCacheConfiguration) {
        super(cacheWriter, defaultCacheConfiguration);
    }

    @Override
    protected RedisCache createRedisCache(String name, RedisCacheConfiguration cacheConfig) {
        //缓存过期时间用|竖线隔开
        String[] array = name.split("\\|");
        //缓存名拼接租户编码区分
        name = array[0];
        if (array.length > 1) {
            long ttl = Long.parseLong(array[1]);
            cacheConfig = cacheConfig.entryTtl(Duration.ofSeconds(ttl));
            //缓存名称需要去掉|及其以后的数据
            name = name.substring(0,name.lastIndexOf("|"));
        }
        return super.createRedisCache(name, cacheConfig);
    }

}
