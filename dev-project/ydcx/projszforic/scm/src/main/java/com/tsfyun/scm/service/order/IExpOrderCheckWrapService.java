package com.tsfyun.scm.service.order;

import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.scm.entity.order.ExpOrderLogistics;
import com.tsfyun.scm.entity.order.ImpOrder;

import java.util.List;

public interface IExpOrderCheckWrapService {
    //校验赋值国内交货
    void checkWrapDomesticDeliveryLogistics(ExpOrderLogistics expOrderLogistics, ExpOrderLogisticsDTO logistiscs,Boolean isSubmit);
    //校验赋值境外交货
    void checkWrapOverseasDeliveryLogistics(ExpOrderLogistics expOrderLogistics, ExpOrderLogisticsDTO logistiscs,Boolean isSubmit);

}
