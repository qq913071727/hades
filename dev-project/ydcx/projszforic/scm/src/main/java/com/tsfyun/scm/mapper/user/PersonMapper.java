package com.tsfyun.scm.mapper.user;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.dto.user.InsidePersonDTO;
import com.tsfyun.scm.entity.user.Person;
import com.tsfyun.scm.vo.user.PersonInfoVO;
import com.tsfyun.scm.vo.user.SimplePersonInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonMapper extends Mapper<Person> {

    Person findByCode(String code);

    /**
     * 根据手机号码查询
     * @param phone
     * @return
     */
    //Person findByPhone(String phone);


    /**
     * 根据邮箱查询
     * @param mail
     * @return
     */
    Person findByMail(String mail);

    /**
     * 获取员工列表
     * @param dto
     * @return
     */
    List<PersonInfoVO> list(InsidePersonDTO dto);

    /**
     * 根据递归的部门id集合查询员工列表
     * @param dto
     * @param departmentIds
     * @return
     */
    List<PersonInfoVO> listWithDepartmentIds(@Param("dto") InsidePersonDTO dto, @Param("departmentIds") List<Long> departmentIds);

    /**
     * 根据菜单获取有该权限的员工信息
     * @param menuId
     * @return
     */
    List<SimplePersonInfo> getPersonByMenuId(@Param(value = "id")String menuId);

    /**
     * 根据角色获取有该角色的员工信息
     * @param roleId
     * @return
     */
    List<SimplePersonInfo> getPersonByRoleId(@Param(value = "roleId")String roleId);

    /**=
     * 统计员工数
     * @return
     */
    Integer countPersonNumber();

    /**
     * 逻辑删除人员
     * @param id
     * @return
     */
    Integer deletePerson(@Param(value = "id") Long id);

    /**
     * 修改密码（管理员修改）
     * @param id
     * @param newPassWord
     * @param updateBy
     * @return
     */
    int updatePwd(@Param("id")Long id,@Param("newPassWord")String newPassWord,@Param("updateBy")String updateBy);

    /**
     * 修改密码（自行修改）
     * @param personId
     * @param passWord
     * @param newPassWord
     * @param updateBy
     * @return
     */
    int modifyPwd(@Param("personId")Long personId,@Param("passWord")String passWord,@Param("newPassWord")String newPassWord,@Param("updateBy")String updateBy);

    /**
     * 取消关注
     * @param wxgzhId
     */
    void setNullWxgzh(@Param(value = "wxgzhId")String wxgzhId);
}
