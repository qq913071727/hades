package com.tsfyun.scm.entity.support;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 任务通知
 * </p>
 *

 * @since 2020-04-05
 */
@Data
public class TaskNotice {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 操作码
     */
    private String operationCode;

    /**
     * 任务通知内容id
     */

    private Long taskNoticeId;

    /**
     * 提交任务通知用户id
     */

    private Long senderId;

    /**
     * 提交任务通知用户名称
     */

    private String senderName;

    /**
     * 接收用户id
     */

    private Long receiverId;

    /**
     * 接收用户名称
     */

    private String receiverName;


}
