package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.customer.CustomerAbroadDeliveryInfoDTO;
import com.tsfyun.scm.dto.customer.client.ClientCustomerAbroadDeliveryInfoQTO;
import com.tsfyun.scm.entity.customer.CustomerAbroadDeliveryInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.CustomerAbroadDeliveryInfoVO;

import java.util.List;

/**
 * <p>
 * 客户境外送货地址 服务类
 * </p>
 *
 *
 * @since 2021-09-16
 */
public interface ICustomerAbroadDeliveryInfoService extends IService<CustomerAbroadDeliveryInfo> {

    /**
     * 新增
     * @param dto
     */
    void add(CustomerAbroadDeliveryInfoDTO dto, boolean handleDefault);

    /**
     * 修改
     * @param dto
     */
    void edit(CustomerAbroadDeliveryInfoDTO dto,boolean handleDefault);

    /**=
     * 批量保存
     */
    void saveList(Long customerId,List<CustomerAbroadDeliveryInfoDTO> customerDeliveryInfoDTOS);

    /**
     * 禁用启用
     * @param id
     */
    void activeDisable(Long id);

    /**
     * 设置默认
     * @param id
     */
    void setDefault(Long id);

    /**
     * 取消以前设置的默认境外收货地址
     * @param customerId
     */
    void handleOldDefault(Long customerId);

    /**
     * 获取客户境外收货地址
     * @param customerId
     * @return
     */
    List<CustomerAbroadDeliveryInfoVO> select(Long customerId);

    List<CustomerAbroadDeliveryInfoVO> effectiveList(String customerName);

    /**
     * 分页查询客户境外收货地址
     * @param dto
     * @return
     */
    PageInfo<CustomerAbroadDeliveryInfoVO> pageList(CustomerAbroadDeliveryInfoDTO dto);

    /**
     * 获取客户境外收货地址详细信息
     * @param id
     * @return
     */
    CustomerAbroadDeliveryInfoVO detail(Long id);

    /**
     * 根据客户id删除客户境外收货地址
     * @param customerId
     */
    void removeByCustomerId(Long customerId);

    /**
     * 根据客户id获取客户境外收货地址列表
     * @param customerId
     * @return
     */
    List<CustomerAbroadDeliveryInfo> list(Long customerId);


    /**
     * 根据主键id删除
     * @param id
     */
    void deleteById(Long id);

    /**
     * 根据客户id获取客户收货记录数
     * @param customerId
     * @return
     */
    int countByCustomerId(Long customerId);

    /**
     * 客户端分页查询客户境外收货地址
     * @param qto
     * @return
     */
    PageInfo<CustomerAbroadDeliveryInfo> clientPage(ClientCustomerAbroadDeliveryInfoQTO qto);

    /**
     * 客户端删除
     * @param id
     */
    void clientRemove(Long id);

    /**
     * 客户端新增
     * @param dto
     */
    void clientAdd(CustomerAbroadDeliveryInfoDTO dto);

    /**
     * 客户端修改
     * @param dto
     */
    void clientEdit(CustomerAbroadDeliveryInfoDTO dto);
}
