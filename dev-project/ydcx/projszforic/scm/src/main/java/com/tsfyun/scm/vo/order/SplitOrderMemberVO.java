package com.tsfyun.scm.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SplitOrderMemberVO implements Serializable {
    private Long orderId;
    private List<ImpOrderMemberVO> members;
}
