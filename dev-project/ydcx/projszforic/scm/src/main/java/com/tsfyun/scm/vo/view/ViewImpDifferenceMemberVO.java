package com.tsfyun.scm.vo.view;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * VIEW响应实体
 * </p>
 *
 *
 * @since 2021-10-21
 */
@Data
@ApiModel(value="ViewImpDifferenceMember响应对象", description="VIEW响应实体")
public class ViewImpDifferenceMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单日期-由后端写入")
    private Date orderDate;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "客户")
    private String customerName;

    @ApiModelProperty(value = "付汇金额(CNY)")
    private BigDecimal accountValueCny;

    @ApiModelProperty(value = "采购合同单号")
    private String salesDocNo;

    @ApiModelProperty(value = "销售合同货款金额")
    private BigDecimal goodsVal;

    @ApiModelProperty(value = "票差")
    private BigDecimal balance;

    @ApiModelProperty(value = "票差调整金额")
    private BigDecimal differenceVal;

    @ApiModelProperty(value = "销售合同总价")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "订单编号")
    private String docNo;


}
