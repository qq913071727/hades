package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.tsfyun.scm.entity.order.ExpOrderPriceMember;
import com.tsfyun.scm.entity.order.ImpOrderPriceMember;
import com.tsfyun.scm.mapper.order.ExpOrderPriceMemberMapper;
import com.tsfyun.scm.service.order.IExpOrderPriceMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.order.ExpOrderPriceMemberVO;
import com.tsfyun.scm.vo.order.ImpOrderPriceMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Service
public class ExpOrderPriceMemberServiceImpl extends ServiceImpl<ExpOrderPriceMember> implements IExpOrderPriceMemberService {

    @Autowired
    private ExpOrderPriceMemberMapper expOrderPriceMemberMapper;

    @Override
    public Map<String, Object> historicalRecord(Long orderMemberId) {
        return expOrderPriceMemberMapper.historicalRecord(orderMemberId);
    }

    @Override
    public List<ExpOrderPriceMemberVO> findByOrderPriceId(Long orderPriceId) {
        ExpOrderPriceMember query = new ExpOrderPriceMember();
        query.setOrderPriceId(orderPriceId);
        List<ExpOrderPriceMember> list = list(query);
        if(CollUtil.isNotEmpty(list)){
            list = list.stream().sorted(Comparator.comparing(ExpOrderPriceMember::getRowNo)).collect(Collectors.toList());
            return beanMapper.mapAsList(list, ExpOrderPriceMemberVO.class);
        }
        return Lists.newArrayList();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeByOrderId(Long orderId) {
        expOrderPriceMemberMapper.removeByOrderId(orderId);
    }
}
