package com.tsfyun.scm.dto.customs;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description:
 * @since Created in 2020/4/24 17:43
 */
@Data
public class DeclarationQTO  extends PaginationDto implements Serializable {

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 单据类型
     */
    private String billType;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 订单编号
     */
    private String orderDocNo;

    /**=
     * 合同编号
     */
    private String contrNo;

    /**
     * 六联单号
     */
    private String cusVoyageNo;

    /**
     * 报关单号
     */
    private String declarationNo;

    /**
     * 跨境运单号
     */
    private String transportNo;

    /**
     * 状态
     */
    private String statusId;

    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

}
