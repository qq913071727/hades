package com.tsfyun.scm.config;

import com.tsfyun.scm.config.properties.FileProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 *
 * @Date 2020/3/4 14:20
 * @Version V1.0
 */
@Configuration
public class FileConfig {

    @Bean
    @RefreshScope
    public FileProperties fileProperties(){
        return new FileProperties();
    }

}