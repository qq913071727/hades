package com.tsfyun.scm.vo.customer.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.CustomerStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/25 17:03
 */
@Data
public class ClientCustomerGradeVO implements Serializable {

    /**
     * 客户代码
     */
    @ApiModelProperty(value = "客户代码")
    private String code;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String name;

    /**
     * 上次登录时间
     */
    @ApiModelProperty(value = "上次登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime lastLoginTime;

    /**
     * 资料完善度评分
     */
    @ApiModelProperty(value = "资料完善度评分")
    private Integer perfectGrade;

    /**
     * 账户安全级别
     */
    @ApiModelProperty(value = "账户安全级别")
    private String accountSafeLevel;

    /**
     * 是否绑定微信
     */
    @ApiModelProperty(value = "是否绑定微信登录")
    private Boolean bindWeixin;

    /**
     * 是否绑定手机
     */
    @ApiModelProperty(value = "是否绑定手机")
    private Boolean bindPhone;

    /**
     * 是否绑定邮箱
     */
    @ApiModelProperty(value = "是否绑定邮箱")
    private Boolean bindMail;

    /**
     * 是否设置密码
     */
    @ApiModelProperty(value = "是否设置密码")
    private Boolean setPassword;

    /**
     * 状态编码
     */
    @ApiModelProperty(value = "状态编码")
    private String statusId;
    /**
     * 状态名称
     */
    @ApiModelProperty(value = "状态名称")
    private String statusName;
    public String getStatusName(){
        CustomerStatusEnum statusEnum = CustomerStatusEnum.of(getStatusId());
        return Objects.nonNull(statusEnum)?statusEnum.getName():"";
    }

    /**
     * 审核意见
     */
    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;
}
