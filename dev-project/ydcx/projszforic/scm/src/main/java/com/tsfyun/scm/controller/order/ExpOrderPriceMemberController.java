package com.tsfyun.scm.controller.order;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2021-09-14
 */
@RestController
@RequestMapping("/expOrderPriceMember")
public class ExpOrderPriceMemberController extends BaseController {

}

