package com.tsfyun.scm.dto.materiel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 归类变更日志请求实体
 * </p>
 *
 *
 * @since 2020-03-26
 */
@Data
@ApiModel(value="MaterielLog请求对象", description="归类变更日志请求实体")
public class MaterielLogDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @LengthTrim(max = 1000,message = "变更内容最大长度不能超过1000位")
   @ApiModelProperty(value = "变更内容")
   private String changeContent;

   @NotEmptyTrim(message = "物料ID不能为空")
   @LengthTrim(max = 32,message = "物料ID最大长度不能超过32位")
   @ApiModelProperty(value = "物料ID")
   private String materielId;


}
