package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.order.ExpOrderPricePlusVo;
import com.tsfyun.scm.dto.order.ExpOrderPriceQTO;
import com.tsfyun.scm.dto.order.ImpOrderPriceQTO;
import com.tsfyun.scm.service.order.IExpOrderPriceService;
import com.tsfyun.scm.service.order.IImpOrderPriceService;
import com.tsfyun.scm.vo.order.ExpOrderPriceVO;
import com.tsfyun.scm.vo.order.ImpOrderPricePlusVo;
import com.tsfyun.scm.vo.order.ImpOrderPriceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2021-09-14
 */
@RestController
@RequestMapping("/expOrderPrice")
public class ExpOrderPriceController extends BaseController {

    @Autowired
    private IExpOrderPriceService expOrderPriceService;

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ExpOrderPriceVO>> list(@ModelAttribute ExpOrderPriceQTO qto) {
        PageInfo<ExpOrderPriceVO> page = expOrderPriceService.list(qto);
        return success((int) page.getTotal(),page.getList());
    }

    @PostMapping(value = "detail")
    public Result<ExpOrderPricePlusVo> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation){
        return success(expOrderPriceService.detail(id,operation));
    }

    /**=
     * 审核
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "examine")
    public Result<Void> examine(@ModelAttribute @Valid TaskDTO dto){
        expOrderPriceService.examine(dto);
        return success();
    }
}

