package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 提（运）单数据
 * @since Created in 2020/4/22 11:23
 */
@Data
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Consignment", propOrder = {
        "grossVolumeMeasure",
        "totalPackageQuantity",
        "valueAmount",
        "borderTransportMeans",
        "consignee",
        "consignmentItem",
        "consignor",
        "freight",
        "governmentAgencyGoodsItem",
        "governmentProcedure",
        "notifyParty",
        "transitDeparture",
        "transitDestination",
        "transportContractDocument",
        "transportEquipment",
        "undgContact",
})
public class Consignment {

    //货物体积
    @XmlElement(name = "GrossVolumeMeasure")
    private GrossVolumeMeasure grossVolumeMeasure;

    //货物总件数
    @XmlElement(name = "TotalPackageQuantity")
    private TotalPackageQuantity totalPackageQuantity;

    //货物价值
    @XmlElement(name = "ValueAmount")
    private ValueAmount valueAmount;

    //提（运）单相关运输工具信息
    @XmlElement(name = "BorderTransportMeans")
    private BorderTransportMeans borderTransportMeans;

    //收货人信息
    @XmlElement(name = "Consignee")
    private Consignee consignee;

    //商品项信息
    @XmlElement(name = "ConsignmentItem")
     private List<ConsignmentItem> consignmentItem;

    //发货人信息
    @XmlElement(name = "Consignor")
    private Consignor consignor;

    //运费支付信息
    @XmlElement(name = "Freight")
    private Freight freight;

    //海关通关货物信息
    @XmlElement(name = "GovernmentAgencyGoodsItem")
    private GovernmentAgencyGoodsItem governmentAgencyGoodsItem;

    //海关货物通关代码信息
    @XmlElement(name = "GovernmentProcedure")
    private GovernmentProcedure governmentProcedure;

    //通知人信息
    @XmlElement(name = "NotifyParty")
    private NotifyParty notifyParty;

    //跨境启运地信息（无需填写）
    @XmlElement(name = "TransitDeparture")
    private TransitDeparture transitDeparture;

    //跨境指运地信息
    @XmlElement(name = "TransitDestination")
    private TransitDestination transitDestination;

    //运输合同信息
    @XmlElement(name = "TransportContractDocument")
    private TransportContractDocument transportContractDocument;

    //集装箱信息
    @XmlElement(name = "TransportEquipment")
    private List<TransportEquipment> transportEquipment;

    //危险品联系人信息
    @XmlElement(name = "UNDGContact")
    private UNDGContact undgContact;

    public List<ConsignmentItem> getConsignmentItem() {
        if (consignmentItem == null) {
            consignmentItem = new ArrayList<ConsignmentItem>();
        }
        return consignmentItem;
    }

    public List<TransportEquipment> getTransportEquipment() {
        if (transportEquipment == null) {
            transportEquipment = new ArrayList<TransportEquipment>();
        }
        return transportEquipment;
    }

}
