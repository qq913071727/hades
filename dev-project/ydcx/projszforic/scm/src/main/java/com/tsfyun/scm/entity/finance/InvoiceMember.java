package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 发票明细
 * </p>
 *

 * @since 2020-05-18
 */
@Data
public class InvoiceMember {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;


    /**
     * 行号
     */

    private Integer rowNo;

    /**
     * 发票id
     */

    private Long invoiceId;

    /**
     * 物料型号
     */

    private String goodsModel;

    /**
     * 物料名称
     */

    private String goodsName;

    /**
     * 物料品牌
     */

    private String goodsBrand;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 单位编码
     */

    private String unitCode;

    /**
     * 单位名称
     */

    private String unitName;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 税收分类编码
     */

    private String taxCode;

    /**=
     * 税收分类编码简称
     */
    private String taxCodeName;

    /**
     * 开票名称
     */

    private String invoiceName;

    /**
     * 首次开
     */
    private Boolean isFirst;
}
