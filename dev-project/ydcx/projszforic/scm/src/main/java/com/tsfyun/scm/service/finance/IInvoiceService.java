package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.ConfirmTaxCodeDTO;
import com.tsfyun.scm.dto.finance.InvoiceCompleteDTO;
import com.tsfyun.scm.dto.finance.InvoiceQTO;
import com.tsfyun.scm.dto.finance.client.ClientConfirmInvoiceDTO;
import com.tsfyun.scm.dto.finance.client.ClientInvoiceQTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.scm.entity.finance.ImpSalesContractMember;
import com.tsfyun.scm.entity.finance.Invoice;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderCost;
import com.tsfyun.scm.vo.finance.ImpSalesContractMemberVO;
import com.tsfyun.scm.vo.finance.InvoiceDetailPlusVO;
import com.tsfyun.scm.vo.finance.InvoiceVO;
import com.tsfyun.scm.vo.finance.client.ClientInvoiceListVO;
import com.tsfyun.scm.vo.order.ImpOrderInvoiceVO;

import java.util.List;

/**
 * <p>
 * 发票 服务类
 * </p>
 *

 * @since 2020-05-15
 */
public interface IInvoiceService extends IService<Invoice> {

    /**
     * 分页列表（带权限）
     * @param qto
     * @return
     */
    PageInfo<InvoiceVO> pageList(InvoiceQTO qto);

    /**
     * 导出开票资料
     * @param ids
     * @return
     */
    Long exportSpaceFlightExcel(List<Long> ids) throws Exception;

    /**
     * 根据订单号获取发票申请信息
     * @param orderNo
     * @return
     */
    Invoice getByOrderNo(String orderNo);


    void generateInvoice(ImpOrderInvoiceVO impOrderInvoiceVO, List<ImpOrderCost> orderCosts);

    /**
     * 发票明细
     * @param id
     * @param operation
     * @return
     */
    InvoiceDetailPlusVO detailPlus(Long id,String operation);

    /**
     * 商务确认
     * @param dto
     */
    void confirm(TaskDTO dto);


    /**
     * 根据销售合同号获取发票信息
     * @param contractNo
     * @return
     */
    Invoice getBySalesContractNo(String contractNo);

    /**
     * 根据销售合同生成发票申请
     * @param impSalesContract
     * @param impSalesContractMembers
     */
    void generateInvoiceFromSalesContract(ImpSalesContract impSalesContract, List<ImpSalesContractMemberVO> impSalesContractMembers);

    /**
     * 删除发票
     * @param id
     */
    void delete(Long id);

    /**
     * 财务确定开票完成
     * @param dto
     */
    void completeInvoice(InvoiceCompleteDTO dto);

    /**=
     * 根据订单做发票申请
     * @param id
     */
    void invoiceApply(Long id);

    /**=
     * 财务确定编码
     * @param dto
     */
    void financialTaxCode(ConfirmTaxCodeDTO dto);

    /**=
     * 商务开票确认
     * @param dto
     */
    void businessConfirm(TaskDTO dto);

    /**
     * 客户端分页列表
     * @param qto
     * @return
     */
    PageInfo<ClientInvoiceListVO> clientPageList(ClientInvoiceQTO qto);

    /**
     * 客户确定税收分类编码
     * @param dto
     */
    void clientConfrim(ClientConfirmInvoiceDTO dto);

    /**
     * 根据订单id获取发票信息
     * @param orderId
     * @return
     */
    ImpOrderInvoiceVO getInvoiceByOrderId(Long orderId);
}
