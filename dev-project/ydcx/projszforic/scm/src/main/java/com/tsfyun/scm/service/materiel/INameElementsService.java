package com.tsfyun.scm.service.materiel;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.materiel.NameElementsDTO;
import com.tsfyun.scm.dto.materiel.NameElementsQTO;
import com.tsfyun.scm.entity.materiel.NameElements;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.materiel.NameElementsPlusVO;
import com.tsfyun.scm.vo.materiel.NameElementsVO;

import java.util.List;

/**
 * <p>
 * 品名要素 服务类
 * </p>
 *

 * @since 2020-03-26
 */
public interface INameElementsService extends IService<NameElements> {


    //新增
    void add(NameElementsDTO dto);

    //修改
    void edit(NameElementsDTO dto);

    //分页列表
    PageInfo<NameElementsVO> pageList(NameElementsQTO dto);

    //批量删除
    void deleteBatch(List<String> ids);

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(String id,Boolean disabled);


    /**
     * 详情
     * @param id
     * @param isGetCustomsElements 是否查询申报要素信息
     * @return
     */
    NameElementsPlusVO detail(String id,boolean isGetCustomsElements);

}
