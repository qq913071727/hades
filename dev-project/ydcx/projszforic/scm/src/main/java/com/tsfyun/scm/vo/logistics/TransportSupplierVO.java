package com.tsfyun.scm.vo.logistics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="TransportSupplier响应对象", description="响应实体")
public class TransportSupplierVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private String address;

    @ApiModelProperty(value = "禁用启用")
    private Boolean disabled;

    @ApiModelProperty(value = "传真")
    private String fax;

    @ApiModelProperty(value = "联系人")
    private String linkPerson;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "供应商名称")
    private String name;

    @ApiModelProperty(value = "电话")
    private String tel;

    @JsonIgnore
    @ApiModelProperty(value = "创建人")
    private String createBy;

    private LocalDateTime dateCreated;


}
