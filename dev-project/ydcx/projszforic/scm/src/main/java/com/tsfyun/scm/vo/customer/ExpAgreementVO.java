package com.tsfyun.scm.vo.customer;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.domain.ExpAgreementStatusEnum;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 出口协议响应实体
 * </p>
 *
 *
 * @since 2021-09-10
 */
@Data
@ApiModel(value="ExpAgreement响应对象", description="出口协议响应实体")
public class ExpAgreementVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "协议编号")
    private String docNo;

    @ApiModelProperty(value = "业务类型(枚举)")
    private String businessType;

    @ApiModelProperty(value = "报关类型")
    private String declareType;
    private String declareTypeDesc;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "甲方")
    private String partyaName;

    @ApiModelProperty(value = "甲方法人")
    private String partyaLegalPerson;

    @ApiModelProperty(value = "甲方电话")
    private String partyaTel;

    @ApiModelProperty(value = "甲方传真")
    private String partyaFax;

    @ApiModelProperty(value = "甲方地址")
    private String partyaAddress;

    @ApiModelProperty(value = "乙方")
    private String partybName;

    @ApiModelProperty(value = "乙方法人")
    private String partybLegalPerson;

    @ApiModelProperty(value = "乙方电话")
    private String partybTel;

    @ApiModelProperty(value = "乙方传真")
    private String partybFax;

    @ApiModelProperty(value = "乙方地址")
    private String partybAddress;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private LocalDateTime dateCreated;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "签约日期")
    private Date signingDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "生效日期")
    private Date effectDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "失效日期")
    private Date invalidDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "实际失效日期 ")
    private Date actualInvalidDate;

    @ApiModelProperty(value = "自动延期/年")
    private Integer delayYear;

    @ApiModelProperty(value = "协议正本是否签回")
    private Boolean isSignBack;

    @ApiModelProperty(value = "签回确定人")
    private String signBackPerson;

    @ApiModelProperty(value = "报价描述")
    private String quoteDescribe;

    @ApiModelProperty(value = "代理费结算模式")
    private String settlementMode;

    @ApiModelProperty(value = "代理费%")
    private BigDecimal agencyFee;

    @ApiModelProperty(value = "最低收费")
    private BigDecimal minAgencyFee;


    @ApiModelProperty(value = "是否垫税")
    private Boolean isTaxAdvance;

    @ApiModelProperty(value = "甲方社会统一信用代码")
    private String partyaSocialNo;

    @ApiModelProperty(value = "乙方统一社会信用代码")
    private String partybSocialNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String createBy;

    public String getDeclareTypeDesc() {
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(declareType);
        return Optional.ofNullable(declareTypeEnum).map(DeclareTypeEnum::getName).orElse("");
    }

    public String getStatusName() {
        ExpAgreementStatusEnum expAgreementStatusEnum = ExpAgreementStatusEnum.of(statusId);
        return Optional.ofNullable(expAgreementStatusEnum).map(ExpAgreementStatusEnum::getName).orElse("");
    }

    public String getCreateBy() {
        if(StringUtils.isEmpty(createBy)) {
            return "";
        }
        if(createBy.contains("/")) {
            return createBy.split("/")[1];
        } else {
            return createBy;
        }
    }


}
