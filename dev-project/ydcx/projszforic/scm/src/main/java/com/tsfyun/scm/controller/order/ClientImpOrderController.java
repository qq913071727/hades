package com.tsfyun.scm.controller.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ClientImpOrderQTO;
import com.tsfyun.scm.dto.order.ImpOrderPlusDTO;
import com.tsfyun.scm.dto.order.ImpOrderQTO;
import com.tsfyun.scm.service.order.IImpOrderService;
import com.tsfyun.scm.vo.order.ClientImpOrderVO;
import com.tsfyun.scm.vo.order.ImpOrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 客户端进口订单
 */
@RestController
@RequestMapping("/client/impOrder")
@Api(tags = "订单")
public class ClientImpOrderController extends BaseController {

    @Autowired
    private IImpOrderService impOrderService;

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @ApiOperation("订单列表")
    @PostMapping(value = "list")
    public Result<List<ClientImpOrderVO>> list(@RequestBody ClientImpOrderQTO qto) {
        PageInfo<ClientImpOrderVO> page = impOrderService.clientList(qto);
        return success((int) page.getTotal(), page.getList());
    }

    /**
     * 新增订单
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    @ApiOperation("新增订单")
    Result<Long> addOrder(@RequestBody ImpOrderPlusDTO dto) {
        return success(impOrderService.clienAdd(dto));
    }

    /**
     * 修改订单
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "edit")
    @ApiOperation("编辑订单")
    Result<Void> editOrder(@RequestBody ImpOrderPlusDTO dto) {
        impOrderService.clienEdit(dto);
        return success();
    }

    /**
     * 删除订单
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @ApiOperation("删除订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "订单id", required = true, paramType = "path", dataType = "String")
    })
    public Result<Void> delete(@RequestParam("id")Long id){
        impOrderService.clientRemove(id);
        return success();
    }

    /**
     * 获取需要修改的订单数
     * @return
     */
    @ApiOperation(value = "待修改订单数")
    @GetMapping("editOrderSize")
    public Result<Integer> editOrderSize(){
        return success(impOrderService.editOrderSize());
    }

}
