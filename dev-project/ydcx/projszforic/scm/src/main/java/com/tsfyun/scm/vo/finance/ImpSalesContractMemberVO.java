package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 销售合同明细响应实体
 * </p>
 *

 * @since 2020-05-19
 */
@Data
@ApiModel(value="ImpSalesContractMember响应对象", description="销售合同明细响应实体")
public class ImpSalesContractMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "行号")
    private Integer rowNo;

    @ApiModelProperty(value = "销售合同id")
    private Long impSalesContractId;

    @ApiModelProperty(value = "物料型号")
    private String goodsModel;

    @ApiModelProperty(value = "物料名称")
    private String goodsName;

    @ApiModelProperty(value = "物料品牌")
    private String goodsBrand;

    @ApiModelProperty(value = "产地编码")
    private String countryId;

    @ApiModelProperty(value = "产地名称")
    private String countryName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "单位编码")
    private String unitCode;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "币制编码")
    private String currencyId;

    @ApiModelProperty(value = "币制名称")
    private String currencyName;


}
