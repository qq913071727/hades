package com.tsfyun.scm.mapper.customer;

import com.tsfyun.scm.entity.customer.ImpClause;
import com.tsfyun.scm.vo.customer.ImpClauseOrderVO;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 进口条款 Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Repository
public interface ImpClauseMapper extends Mapper<ImpClause> {

    /**
     * 根据订单id集合获取进口条款信息
     * @param orderIds
     * @return
     */
    List<ImpClauseOrderVO> findByImpOrderIds(@Param(value = "orderIds") List<Long> orderIds);

}
