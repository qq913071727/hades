package com.tsfyun.scm.service.customs;

import com.tsfyun.common.base.dto.DeclareBizReceiveDTO;

public interface IDeclarationReceiptService {
    //接收回执
    void receive(DeclareBizReceiveDTO dto);
}
