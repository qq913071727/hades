package com.tsfyun.scm.service.impl.materiel;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Snowflake;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.help.excel.ExcelUtil;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.support.DomainStatus;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.common.base.validator.ValidatorUtils;
import com.tsfyun.common.base.vo.ImpExcelVO;
import com.tsfyun.scm.client.CustomsCodeClient;
import com.tsfyun.scm.dto.materiel.ClassifyMaterielDTO;
import com.tsfyun.scm.dto.materiel.MaterielDTO;
import com.tsfyun.scm.dto.materiel.MaterielQTO;
import com.tsfyun.scm.dto.materiel.client.ClientMaterielQTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.materiel.Materiel;
import com.tsfyun.scm.entity.materiel.MaterielBrand;
import com.tsfyun.scm.entity.materiel.MaterielLog;
import com.tsfyun.scm.excel.ClassifyMaterielExcel;
import com.tsfyun.scm.excel.ClientMaterielExcel;
import com.tsfyun.scm.excel.MaterielExcel;
import com.tsfyun.scm.mapper.materiel.MaterielMapper;
import com.tsfyun.scm.service.base.ISystemCacheService;
import com.tsfyun.scm.service.customer.ICustomerService;
import com.tsfyun.scm.service.materiel.IMaterielBrandService;
import com.tsfyun.scm.service.materiel.IMaterielLogService;
import com.tsfyun.scm.service.materiel.IMaterielService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.order.IImpOrderMemberService;
import com.tsfyun.scm.stream.send.ScmProcessor;
import com.tsfyun.scm.system.vo.CustomsCodeVO;
import com.tsfyun.scm.system.vo.CustomsElementsVO;
import com.tsfyun.scm.system.vo.TariffCustomsCodeVO;
import com.tsfyun.scm.util.MaterielUtil;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.materiel.MaterielHSVO;
import com.tsfyun.scm.vo.materiel.MaterielVO;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import com.tsfyun.scm.vo.materiel.client.ClientMaterielVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2020-03-22
 */
@Service
@Slf4j
public class MaterielServiceImpl extends ServiceImpl<Materiel> implements IMaterielService {

    @Autowired
    private MaterielMapper materielMapper;
    @Autowired
    private ICustomerService customerService;
    @Autowired
    private CustomsCodeClient customsCodeClient;
    @Autowired
    private ScmProcessor saasProcessor;
    @Autowired
    private IMaterielLogService materielLogService;
    @Autowired
    private IMaterielBrandService materielBrandService;
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private IImpOrderMemberService impOrderMemberService;
    @Autowired
    private ISystemCacheService systemCacheService;

    @Override
    public Result<List<MaterielHSVO>> pageList(MaterielQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        Map<String,Object> params = beanMapper.map(qto,Map.class);
        List<MaterielVO> materielVOList = materielMapper.pageList(params);
        PageInfo page = new PageInfo<>(materielVOList);
        List<MaterielHSVO> hsvoList = formatMateriel(materielVOList);
        return Result.success((int)page.getTotal(),hsvoList);
    }

    @Override
    public List<SimpleMaterielVO> vague(MaterielQTO qto) {
        Map<String,Object> params = beanMapper.map(qto,Map.class);
        return materielMapper.vague(params);
    }

    @Override
    public List<MaterielHSVO> formatMateriel(List<MaterielVO> materielVOList){
        List<MaterielHSVO> hsvoList = Lists.newArrayList();
        if(CollectionUtil.isNotEmpty(materielVOList)){
            Map<String,CustomsCodeVO> hsCodeMap = new LinkedHashMap<>();
            materielVOList.stream().forEach(vo ->{
                CustomsCodeVO codeVO = null;
                if(StringUtils.isNotEmpty(vo.getHsCode())){
                    codeVO = hsCodeMap.get(vo.getHsCode());
                    if(codeVO==null){
                        Result<CustomsCodeVO> codeVOResult = customsCodeClient.detail(vo.getHsCode());
                        if(codeVOResult.isSuccess()) {
                            codeVO = codeVOResult.getData();
                            hsCodeMap.put(vo.getHsCode(),codeVO);
                        }
                    }
                }
                hsvoList.add(new MaterielHSVO(vo,codeVO));
            });
        }
        return hsvoList;
    }

    @Override
    public void analysisNewMateriel(List<String> ids) {
        if(CollectionUtil.isNotEmpty(ids)){
            List<Materiel> materielList = materielMapper.findInId(ids);
            materielList.stream().forEach(m->{
                analysisNewMateriel(m);
            });
        }
    }

    @Override
    public Map<String, Object> classifyDetail(List<String> ids) {
        List<Materiel> materielList = materielMapper.findInId(ids);
        TsfPreconditions.checkArgument(CollectionUtil.isNotEmpty(materielList),new ServiceException("未找到对应物料信息"));
        Materiel materiel = materielList.get(0);
        //验证状态是否可以执行操作
        DomainStatus.getInstance().check(DomainOprationEnum.MATERIEL_CLASSIFY, materiel.getStatusId());

        Map<String,Object> materielMap = beanMapper.map(materiel,Map.class);
        //中文品牌
        String brandZh = materielBrandService.findZhByNameEn(materiel.getBrand());
        materielMap.put("brandZh",brandZh);
        List<String> hsCodeList = Lists.newArrayList();
        if(materielList.size()>1){
            for(int i=1;i<materielList.size();i++){
                Materiel om = materielList.get(i);

                //验证状态是否可以执行操作
                DomainStatus.getInstance().check(DomainOprationEnum.MATERIEL_CLASSIFY, om.getStatusId());

                materielMap.put("id",StringUtils.removeSpecialSymbol(materielMap.get("id")).concat(",").concat(om.getId()));
                materielMap.put("model",StringUtils.removeSpecialSymbol(materielMap.get("model")).concat("|").concat(om.getModel()));
                materielMap.put("brand",StringUtils.removeSpecialSymbol(materielMap.get("brand")).concat("|").concat(om.getBrand()));
                String brandNameZh = materielBrandService.findZhByNameEn(om.getBrand());
                materielMap.put("brandZh",StringUtils.removeSpecialSymbol(materielMap.get("brandZh")).concat("|").concat(brandNameZh));
                if(StringUtils.isNotEmpty(om.getHsCode())&&!hsCodeList.contains(om.getHsCode())){
                    hsCodeList.add(om.getHsCode());
                }
            }
        }
        //构建申报要素
        List<CustomsElementsVO> elements = Lists.newArrayList();
        if(StringUtils.isNotEmpty(materiel.getHsCode())){
            if(!hsCodeList.contains(materiel.getHsCode())){
                hsCodeList.add(materiel.getHsCode());
            }
            //根据海关编码获取申报要素
            Result<List<CustomsElementsVO>> listResult = customsCodeClient.elementDetail(materiel.getHsCode());
            TsfPreconditions.checkArgument(listResult.isSuccess(),new ServiceException("获取申报要素失败，请稍后重试"));
            elements = listResult.getData();
            List<String> valElements = Arrays.asList(StringUtils.removeSpecialSymbol(materiel.getElements()).concat(" ").split("\\|"));
            for(int i=0;i<elements.size();i++){
                CustomsElementsVO ce = elements.get(i);
                String evalue = StringUtils.removeSpecialSymbol((valElements.size()>i)?valElements.get(i):"");
                if(MaterielUtil.ELEMENT_BRAND_MARK.equals(ce.getName())){
                    // 品牌
                    evalue = MaterielUtil.formatGoodsBrand(materiel.getBrand(),brandZh);
                }else if(MaterielUtil.ELEMENT_MODEL_MARK.equals(ce.getName())){
                    // 型号
                    evalue = MaterielUtil.formatGoodsModel(materiel.getModel());
                }
                ce.setVal(evalue);
            }
        }
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("materiel",materielMap);
        result.put("elements",elements);
        result.put("hsCodeNum",hsCodeList.size());
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveClassify(ClassifyMaterielDTO dto) {
        List<String> ids = Arrays.asList(dto.getId().split(","));
        List<Materiel> materielList = materielMapper.findInId(ids);
        TsfPreconditions.checkArgument(CollectionUtil.isNotEmpty(materielList),new ServiceException("未找到对应物料信息"));
        //根据海关编码获取申报要素
        Result<List<CustomsElementsVO>> listResult = customsCodeClient.elementDetail(dto.getHsCode());
        TsfPreconditions.checkArgument(listResult.isSuccess(),new ServiceException("获取申报要素失败，请稍后重试"));
        List<CustomsElementsVO> customsElements = listResult.getData();
        List<String> elementsVal = dto.getElementsVal();//填写的申报要素
        if(customsElements!=null&&customsElements.size()>0&&(elementsVal==null||customsElements.size()!=elementsVal.size())){
            TsfPreconditions.checkArgument(false,new ServiceException("申报要素错误，请刷新页面重试"));
        }
        List<String> declareSpecs = new ArrayList<>();
        for(int i=0;i<customsElements.size();i++){
            CustomsElementsVO ce = customsElements.get(i);
            String evalue = StringUtils.removeSpecialSymbol(elementsVal.get(i));
            if(MaterielUtil.ELEMENT_BRAND_MARK.equals(ce.getName())){
                // 品牌
                evalue = "X-X-X牌";
            }else if(MaterielUtil.ELEMENT_MODEL_MARK.equals(ce.getName())){
                // 型号
                evalue = "X-X-X型";
            }
            declareSpecs.add(evalue);
        }
        LocalDateTime now = LocalDateTime.now();
        String personName = SecurityUtil.getCurrentPersonName();
        // 品牌库
        Map<String, MaterielBrand> brandTypeMap = new LinkedHashMap<>();
        materielList.stream().forEach(ma ->{
            //验证状态是否可以执行操作
            DomainStatus.getInstance().check(DomainOprationEnum.MATERIEL_CLASSIFY, ma.getStatusId());

            //记录历史物料信息
            Materiel history = new Materiel();
            BeanUtils.copyProperties(ma,history);

            ma.setName(dto.getName());
            if(materielList.size()==1){
                ma.setBrand(dto.getBrand().toUpperCase());
            }
            ma.setHsCode(dto.getHsCode());
            ma.setCiqNo(dto.getCiqNo());
            ma.setIsSmp(Objects.equals(Boolean.TRUE,dto.getIsSmp())?true:false);
            ma.setIsCapp(Objects.equals(Boolean.TRUE,dto.getIsCapp())?true:false);
            ma.setIsCappNo(Objects.equals(Boolean.TRUE,dto.getIsCappNo())?true:false);
            ma.setCappNo(dto.getCappNo());

            String elements = String.join("|",declareSpecs).replace("X-X-X型",MaterielUtil.formatGoodsModel(ma.getModel()));
            if(materielList.size()==1){
                elements = elements.replace("X-X-X牌",MaterielUtil.formatGoodsBrand(ma.getBrand(),dto.getBrandZh()));
                MaterielBrand mb = new MaterielBrand();
                mb.setName(ma.getBrand());
                mb.setMemo(dto.getBrandZh());
                mb.setType(declareSpecs.get(0));
                brandTypeMap.put(ma.getBrand(),mb);
            }else{
                String brandZh = materielBrandService.findZhByNameEn(ma.getBrand());
//                if(StringUtils.isEmpty(brandZh)){
//                    throw new ServiceException(String.format("外文品牌：%s 未找到中文名称",ma.getBrand()));
//                }
                elements = elements.replace("X-X-X牌",MaterielUtil.formatGoodsBrand(ma.getBrand(),brandZh));
                MaterielBrand mb = new MaterielBrand();
                mb.setName(ma.getBrand());
                mb.setMemo(brandZh);
                mb.setType(declareSpecs.get(0));
                brandTypeMap.put(ma.getBrand(),mb);
            }
            ma.setElements(elements);
            ma.setMemo(dto.getMemo());
            ma.setStatusId(MaterielStatusEnum.CLASSED.getCode());
            ma.setClassifyPerson(personName);
            ma.setClassifyTime(now);
            //去除重复型号+品牌
            removeRepeat(ma);
            //保存归类
            super.updateById(ma);
            //记录数据变更
            materielLogService.recordMaterielLog(history,ma);
        });
        //同步其他产品归类
        synchroClassify(materielList.get(0),ids);

        //保存品牌类型新增或修改
        for(Map.Entry<String,MaterielBrand> map : brandTypeMap.entrySet()){
            materielBrandService.saveMaterielBrand(map.getValue());
        }
    }

    //归类同步
    @Transactional(rollbackFor = Exception.class)
    public void synchroClassify(Materiel dto,List<String> excludeIds){
        List<Materiel> materielList = materielMapper.findByModelAndBrand(dto.getModel(),dto.getBrand());
        materielList.stream().forEach(ma ->{
            if(!excludeIds.contains(ma.getId())){
                //记录历史物料信息
                Materiel history = new Materiel();
                BeanUtils.copyProperties(ma,history);
                ma.setName(dto.getName());
                ma.setHsCode(dto.getHsCode());
                ma.setCiqNo(dto.getCiqNo());
                ma.setIsSmp(Objects.equals(Boolean.TRUE,dto.getIsSmp())?true:false);
                ma.setElements(dto.getElements());
                ma.setMemo(dto.getMemo());
                ma.setStatusId(ma.getStatusId());
                ma.setClassifyPerson(ma.getClassifyPerson());
                ma.setClassifyTime(ma.getClassifyTime());
                //去除重复型号+品牌
                removeRepeat(ma);
                //保存归类
                super.updateById(ma);
                //记录数据变更
                materielLogService.recordMaterielLog(history,ma);
            }
        });
    }

    //去除重复物料
    @Transactional(rollbackFor = Exception.class)
    public void removeRepeat(Materiel materiel){
        List<Materiel> materielList = materielMapper.findByRepeat(materiel);
        if(CollUtil.isNotEmpty(materielList)){
            List<String> ids = Lists.newArrayList();
            materielList.stream().forEach(um ->{
                //修改进口订单明细
                impOrderMemberService.updateMemberMaterielId(um.getId(),materiel.getId());
                //修改归类日志
                materielLogService.updateMaterielId(um.getId(),materiel.getId());
                ids.add(um.getId());
            });
            //删除重复物料
            removeByIds(ids);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void analysisNewMateriel(Materiel materiel){
        //待匹配
        if(MaterielStatusEnum.WAIT_MATCH.getCode().equals(materiel.getStatusId())){
            //查询是否存在归类完成物料
            Materiel classed = materielMapper.findOneClassed(materiel.getModel(),materiel.getBrand());
            if(Objects.nonNull(classed)){
                materiel.setMate("标");
                materiel.setStatusId(classed.getStatusId());//归类完成
                materiel.setClassifyTime(classed.getClassifyTime());
                materiel.setClassifyPerson(classed.getClassifyPerson());
                materiel.setName(classed.getName());
                materiel.setHsCode(classed.getHsCode());
                materiel.setCiqNo(classed.getCiqNo());
                materiel.setElements(classed.getElements());
            }else{
                materiel.setMate("未");
                materiel.setStatusId(MaterielStatusEnum.WAIT_CLASSIFY.getCode());//待归类
            }
            super.updateById(materiel);
        }
    }

    //导入文件上限
    private static Integer uploadMaxmum = 10000;
    @Override
    public ImpExcelVO importMateriel(MultipartFile file) {
        ImpExcelVO excelVO = new ImpExcelVO();
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<MaterielExcel> dataList = null;
        try {
            dataList = ExcelUtil.readExcel(file, MaterielExcel.class, 1);
        } catch (Exception e) {
            log.error("读取导入的excel文件异常",e);
            throw new ServiceException("导入数据异常，请仔细检查您的导入文件是否符合模板要求");
        }
        if(CollectionUtils.isEmpty(dataList)) {
            throw new ServiceException("本次没有可以导入的数据");
        }
        TsfPreconditions.checkArgument(dataList.size() <= uploadMaxmum,new ServiceException(String.format("一次最多导入%d条物料信息",uploadMaxmum)));
        List<Materiel> materielList = Lists.newArrayList();
        Map<String,Long> customerMap = new LinkedHashMap<>();
        LocalDateTime now = LocalDateTime.now();
        String person = SecurityUtil.getCurrentPersonIdAndName();
        for(int i=0;i<dataList.size();i++){
            MaterielExcel materielExcel = dataList.get(i);
            int contentRowNo = i + 2;
            ValidatorUtils.validateEntity(materielExcel,contentRowNo);
            Materiel materiel = new Materiel();
            materiel.setMate("");
            if(Objects.isNull(customerMap.get(materielExcel.getCustomerName()))){
                Customer customer = customerService.findByName(materielExcel.getCustomerName());
                TsfPreconditions.checkArgument(Objects.nonNull(customer),new ServiceException(String.format("第%d行客户不存在",contentRowNo)));
                customerMap.put(materielExcel.getCustomerName(),customer.getId());
            }
            materiel.setCustomerId(customerMap.get(materielExcel.getCustomerName()));
            materiel.setModel(materielExcel.getModel().replace("型号","").replace("型",""));
            materiel.setBrand(materielExcel.getBrand().replace("品牌","").replace("牌","").toUpperCase());
            materiel.setName(materielExcel.getName());
            materiel.setSpec(materielExcel.getSpec());
            materiel.setIsSmp(Boolean.FALSE);
            materiel.setIsCapp(Boolean.FALSE);
            materiel.setIsCappNo(Boolean.FALSE);
            //待归类
            materiel.setStatusId(MaterielStatusEnum.WAIT_CLASSIFY.getCode());
            materiel.setSource("后台物料导入");
            materiel.setSourceMark("");
            materiel.setCreateBy(person);
            materiel.setUpdateBy(person);
            materiel.setDateCreated(now);
            materiel.setDateUpdated(now);
            materielList.add(materiel);
        }
        Integer successCount = materielMapper.batchSaveList(materielList);
        excelVO.setTotalCount(materielList.size());
        excelVO.setSuccessCount(successCount);
        stopwatch.stop();
        excelVO.setMessage(String.format("耗时：%d秒",stopwatch.elapsed(TimeUnit.SECONDS)));
        return excelVO;
    }

    // 已归类物料导入
    @Override
    public ImpExcelVO importClassifyMateriel(MultipartFile file) {
        ImpExcelVO excelVO = new ImpExcelVO();
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<ClassifyMaterielExcel> dataList = null;
        try {
            dataList = ExcelUtil.readExcel(file, ClassifyMaterielExcel.class, 1);
        } catch (Exception e) {
            log.error("读取导入的excel文件异常",e);
            throw new ServiceException("导入数据异常，请仔细检查您的导入文件是否符合模板要求");
        }
        if(CollectionUtils.isEmpty(dataList)) {
            throw new ServiceException("本次没有可以导入的数据");
        }
        TsfPreconditions.checkArgument(dataList.size() <= uploadMaxmum,new ServiceException(String.format("一次最多导入%d条物料信息",uploadMaxmum)));
        Long personId = SecurityUtil.getCurrentPersonId();
        String personName = SecurityUtil.getCurrentPersonName();
        for(int i=0;i<dataList.size();i++){
            ClassifyMaterielExcel materielExcel = dataList.get(i);
            int contentRowNo = i + 2;
            ValidatorUtils.validateEntity(materielExcel,contentRowNo);
            //根据海关编码获取申报要素
            Result<List<CustomsElementsVO>> listResult = customsCodeClient.elementDetail(materielExcel.getHsCode());
            TsfPreconditions.checkArgument(listResult.isSuccess(),new ServiceException("获取申报要素失败，请稍后重试"));
            //验证申报要素
            List<String> elementsVal = Arrays.asList(StringUtils.removeSpecialSymbol(materielExcel.getElements()).concat(" ").split("\\|"));
            List<CustomsElementsVO> customsElements = listResult.getData();
            if(customsElements!=null&&customsElements.size()>0&&(elementsVal==null||customsElements.size()!=elementsVal.size())){
                throw new ServiceException(String.format("第%d行%s", contentRowNo, "申报要素错误"));
            }
            List<String> declareSpecs = new ArrayList<>();
            for(int j=0;j<customsElements.size();j++){
                CustomsElementsVO ce = customsElements.get(j);
                String evalue = StringUtils.removeSpecialSymbol(elementsVal.get(j));
                if(MaterielUtil.ELEMENT_BRAND_MARK.equals(ce.getName())){
                    // 品牌
                    evalue = "X-X-X牌";
                }else if(MaterielUtil.ELEMENT_MODEL_MARK.equals(ce.getName())){
                    // 型号
                    evalue = "X-X-X型";
                }
                declareSpecs.add(evalue);
            }
            String elements = String.join("|",declareSpecs).replace("X-X-X型",MaterielUtil.formatGoodsModel(materielExcel.getModel()));
            String brandZh = materielBrandService.findZhByNameEn(materielExcel.getBrand());
            if(StringUtils.isEmpty(brandZh)){
                throw new ServiceException(String.format("第%d行外文品牌：%s 未找到中文名称",contentRowNo,materielExcel.getBrand()));
            }
            elements = elements.replace("X-X-X牌",MaterielUtil.formatGoodsBrand(materielExcel.getBrand(),brandZh));
            materielExcel.setElements(elements);
            materielExcel.setPersonId(personId);
            materielExcel.setPersonName(personName);
        }
        List<List<ClassifyMaterielExcel>> subList = ListUtils.partition(dataList,500);
        subList.stream().forEach(sub ->{
            saasProcessor.sendClassifyMateriel(sub);
        });
        excelVO.setTotalCount(dataList.size());
        excelVO.setSuccessCount(dataList.size());
        stopwatch.stop();
        excelVO.setMessage(String.format("耗时：%d秒",stopwatch.elapsed(TimeUnit.SECONDS)));
        return excelVO;
    }

    @Override
    public void saveImportClassifyMateriel(List<ClassifyMaterielExcel> classifyList){
        if(CollUtil.isEmpty(classifyList)){
            return;
        }
        List<Materiel> materielList = new ArrayList<>();
        List<MaterielLog> materielLogList = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        classifyList.stream().forEach(cl ->{
            List<Materiel> materiels = materielMapper.findByModelAndBrand(cl.getModel(),cl.getBrand());
            if(CollectionUtils.isEmpty(materiels)){
                materiels = new ArrayList<>();
                Materiel materiel = new Materiel();
                materiel.setCustomerId(Long.valueOf(0));//未知客户
                materiel.setSource("已归类物料导入");
                materiel.setMate("未");
                materiels.add(materiel);
            }
            materiels.stream().forEach(materiel ->{
                //记录历史物料信息
                Materiel history = new Materiel();
                BeanUtils.copyProperties(materiel,history);
                materiel.setHsCode(cl.getHsCode());
                materiel.setCiqNo(cl.getCipNo());
                materiel.setModel(cl.getModel());
                materiel.setBrand(cl.getBrand());
                materiel.setName(cl.getName());
                materiel.setElements(cl.getElements());
                materiel.setIsSmp(StringUtils.isNotEmpty(cl.getIsSmp()));
                materiel.setIsCapp(StringUtils.isNotEmpty(cl.getIsCapp()));
                materiel.setIsCappNo(StringUtils.isNotEmpty(cl.getIsCappNo()));
                materiel.setCappNo(StringUtils.removeSpecialSymbol(cl.getCappNo()));
                materiel.setMemo(cl.getMemo());
                materiel.setStatusId(MaterielStatusEnum.CLASSED.getCode());
                materiel.setClassifyPerson(cl.getPersonName());
                materiel.setClassifyTime(now);
                materiel.setCreateBy(StringUtils.removeSpecialSymbol(cl.getPersonId()).concat("/").concat(StringUtils.isEmpty(cl.getPersonName())?"":cl.getPersonName()));
                materiel.setDateCreated(now);
                materiel.setUpdateBy(materiel.getCreateBy());
                materiel.setDateUpdated(now);
                if(Objects.isNull(materiel.getId())){
                    materiel.setId(StringUtils.UUID());
                }else{
                    //修改记录日志
                    String change = materielLogService.comparativeChange(history,materiel);
                    if(StringUtils.isNotEmpty(change)){
                        MaterielLog log = new MaterielLog();
                        log.setId(snowflake.nextId());
                        log.setChangeContent(change);
                        log.setMaterielId(materiel.getId());
                        log.setCreateBy(materiel.getCreateBy());
                        log.setDateCreated(now);
                        log.setUpdateBy(log.getCreateBy());
                        log.setDateUpdated(now);
                        materielLogList.add(log);
                    }
                }
                materielList.add(materiel);
            });
        });
        materielMapper.batchSaveClassifyList(materielList);
        if(CollUtil.isNotEmpty(materielLogList)){
            materielLogService.batchSave(materielLogList);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeIdsNotPrompt(List<String> ids) {
        if(CollUtil.isEmpty(ids)){return;}
        ids.stream().forEach(id ->{
            try{
                TsfWeekendSqls sqls = TsfWeekendSqls.<Materiel>custom()
                        .andEqualTo(true,Materiel::getId,id)
                        .andNotEqualTo(true,Materiel::getStatusId,MaterielStatusEnum.CLASSED.getCode());
                materielMapper.deleteByExample(Example.builder(Materiel.class).where(sqls).build());
            }catch (Exception e){

            }
        });
    }

//    @Override
//    public Integer batchSaveList(List<Materiel> materielList) {
//        Integer successCount = materielMapper.batchSaveList(materielList);
//        //查询所有需要分析的物料信息
//        List<String> stringList = materielMapper.findWaitMacth();
//        List<List<String>> subList = ListUtils.partition(stringList,500);
//        subList.stream().forEach(sub ->{
//            materielMapper.updateWaitMacth(sub);
//            saasProcessor.sendMateriel(sub);
//        });
//        return successCount;
//    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MaterielVO obtainMaterialAndSave(MaterielDTO dto) {
        String key = dto.getBrand().concat((dto.getCustomerId()!=null)?dto.getCustomerId().toString():"").intern();
        synchronized (key){
            MaterielVO materielVO = materielMapper.findByKey(dto.getCustomerId(),dto.getModel(),dto.getBrand());
            if(Objects.isNull(materielVO)){
                //查询是否存在归类完成物料
                Materiel materiel = materielMapper.findOneClassed(dto.getModel(),dto.getBrand());
                if(materiel==null){
                    materiel = new Materiel();
                    materiel.setName(dto.getName());
                    materiel.setMate("未");
                    //待归类
                    materiel.setStatusId(MaterielStatusEnum.WAIT_CLASSIFY.getCode());
                    materiel.setIsSmp(Boolean.FALSE);
                }else{
                    materiel.setMate("标");
                }
                materiel.setCustomerId(dto.getCustomerId());
                materiel.setId(StringUtils.UUID());
                materiel.setModel(dto.getModel().toUpperCase());
                materiel.setBrand(dto.getBrand().toUpperCase());
                materiel.setSpec(dto.getSpec());
                materiel.setSource(dto.getSource());
                materiel.setSourceMark(dto.getSourceMark());
                try{
                    super.saveNonNull(materiel);
                }catch (DuplicateKeyException e) {
                    if(e.getMessage().contains("UK_materiel_customer_id_model_brand")) {
                        throw new ServiceException(String.format("客户已经存在型号[%s]品牌[%s]的物料",dto.getModel(),dto.getBrand()));
                    } else {
                        throw new ServiceException("物料保存失败，请检查数据是否填写正确");
                    }
                }
                materielVO = beanMapper.map(materiel,MaterielVO.class);
                materielVO.setIsAdd(Boolean.TRUE);
            }else{
                //物料存在
                materielVO.setIsAdd(Boolean.FALSE);
            }
            return materielVO;
        }
    }

    @Override
    public Result<List<ClientMaterielVO>> clientPage(ClientMaterielQTO qto) {
        qto.setCustomerId(SecurityUtil.getCurrentCustomerId());
        if(Objects.equals(qto.getHasTariff(),Boolean.TRUE)) {
            //如果勾选了包含最惠国关税
            log.info("开始远程调用获取包含最惠国税率的海关编码");
            List<TariffCustomsCodeVO> tariffCustomsCodeVOs = systemCacheService.tariffCustoms();
            log.info("结束远程调用获取包含最惠国税率的海关编码");
            List<String> tariffHsCodes = tariffCustomsCodeVOs.stream().map(TariffCustomsCodeVO::getId).collect(Collectors.toList());
            qto.setTariffHsCodes(tariffHsCodes);
        }
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        List<ClientMaterielVO> dataList = materielMapper.clientPage(qto.getKeyword(),qto.getStatusId(),qto.getCustomerId(),qto.getDateCreateStart(),qto.getDateCreateEnd(),qto.getHasTariff(),qto.getTariffHsCodes());
        PageInfo<ClientMaterielVO> materielPage = new PageInfo<>(dataList);
        List<ClientMaterielVO> resultList = Lists.newArrayListWithExpectedSize(materielPage.getList().size());
        //获取海关编码信息
        Map<String,CustomsCodeVO> customsCodeMap = Maps.newHashMap();
        materielPage.getList().stream().forEach(materiel -> {
            CustomsCodeVO customsCodeVO = null;
            if(StringUtils.isNotEmpty(materiel.getHsCode())) {
                customsCodeVO = customsCodeMap.get(materiel.getHsCode());
                if(Objects.isNull(customsCodeVO)) {
                    Result<CustomsCodeVO> codeVOResult = customsCodeClient.detail(materiel.getHsCode());
                    if(codeVOResult.isSuccess()) {
                        customsCodeVO = codeVOResult.getData();
                        customsCodeMap.put(materiel.getHsCode(),customsCodeVO);
                    }
                }
            }
            ClientMaterielVO clientMaterielVO = beanMapper.map(materiel,ClientMaterielVO.class);
            clientMaterielVO.setCustomsCode(customsCodeVO);
            resultList.add(clientMaterielVO);
        });
        //不返回海关编码
        resultList.stream().forEach(result->{
            if(Objects.nonNull(result.getCustomsCode())) {
                result.getCustomsCode().setId("");
            }
        });
        return Result.success((int)materielPage.getTotal(),resultList);
    }

    @Override
    public List<SimpleMaterielVO> clientVague(MaterielQTO qto) {
        return materielMapper.clientVague(SecurityUtil.getCurrentCustomerId(),qto.getModel(),qto.getBrand(),qto.getName());
    }

    @Override
    public List<String> searchByBrand(String brand) {
        Example example = Example.builder(Materiel.class).where(TsfWeekendSqls.<Materiel>custom()
                .andRightLike(true,Materiel::getBrand,brand)
                .andEqualTo(false,Materiel::getStatusId,"classed")).build();
        example.setOrderByClause("brand asc");
        example.selectProperties("brand").setDistinct(Boolean.TRUE);
        List<Materiel> materiels = materielMapper.selectByExampleAndRowBounds(example, new RowBounds(0, 10));
        return materiels.stream().map(Materiel::getBrand).collect(Collectors.toList());
    }

    @Override

    public ImpExcelVO clientImportMateriel(MultipartFile file) {
        ImpExcelVO excelVO = new ImpExcelVO();
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<ClientMaterielExcel> dataList = null;
        try {
            dataList = ExcelUtil.readExcel(file, ClientMaterielExcel.class, 1);
        } catch (Exception e) {
            log.error("读取导入的excel文件异常",e);
            throw new ServiceException("导入数据异常，请仔细检查您的导入文件是否符合模板要求");
        }
        if(CollectionUtils.isEmpty(dataList)) {
            throw new ServiceException("本次没有可以导入的数据");
        }
        TsfPreconditions.checkArgument(dataList.size() <= uploadMaxmum,new ServiceException(String.format("一次最多导入%d条物料信息",uploadMaxmum)));
        List<Materiel> materielList = Lists.newArrayList();
        LocalDateTime now = LocalDateTime.now();
        String person = SecurityUtil.getCurrentPersonIdAndName();
        Long customerId = SecurityUtil.getCurrentCustomerId();
        for(int i=0;i<dataList.size();i++){
            ClientMaterielExcel materielExcel = dataList.get(i);
            int contentRowNo = i + 2;
            ValidatorUtils.validateEntity(materielExcel,contentRowNo);
            Materiel materiel = new Materiel();
            materiel.setMate("");
            materiel.setCustomerId(customerId);
            materiel.setModel(materielExcel.getModel().replace("型号","").replace("型",""));
            materiel.setBrand(materielExcel.getBrand().replace("品牌","").replace("牌","").toUpperCase());
            materiel.setName(materielExcel.getName());
            materiel.setSpec(materielExcel.getSpec());
            materiel.setIsSmp(Boolean.FALSE);
            materiel.setIsCapp(Boolean.FALSE);
            materiel.setIsCappNo(Boolean.FALSE);
            //待归类
            materiel.setStatusId(MaterielStatusEnum.WAIT_CLASSIFY.getCode());
            materiel.setSource("客户端物料导入");
            materiel.setSourceMark("");
            materiel.setCreateBy(person);
            materiel.setUpdateBy(person);
            materiel.setDateCreated(now);
            materiel.setDateUpdated(now);
            materielList.add(materiel);
        }
        Integer successCount = materielMapper.batchSaveList(materielList);
        excelVO.setTotalCount(materielList.size());
        excelVO.setSuccessCount(successCount);
        stopwatch.stop();
        excelVO.setMessage(String.format("耗时：%d秒",stopwatch.elapsed(TimeUnit.SECONDS)));
        return excelVO;
    }


}
