package com.tsfyun.scm.controller.support;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.support.TaskNoticeContentQTO;
import com.tsfyun.scm.dto.support.TaskNoticeFocusDTO;
import com.tsfyun.scm.service.support.ITaskNoticeContentService;
import com.tsfyun.scm.service.support.ITaskNoticeFocusService;
import com.tsfyun.scm.vo.support.TaskNoticeContentVO;
import com.tsfyun.scm.vo.support.TaskNoticeFocusVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 任务通知 前端控制器
 * </p>
 *
 * @since 2020-04-05
 */
@RestController
@RequestMapping("/taskNotice")
public class TaskNoticeController extends BaseController {

    @Autowired
    private ITaskNoticeContentService taskNoticeContentService;

    @Autowired
    private ITaskNoticeFocusService taskNoticeFocusService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<TaskNoticeContentVO>> pageList(@ModelAttribute TaskNoticeContentQTO qto) {
        PageInfo<TaskNoticeContentVO> page = taskNoticeContentService.page(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情
     */
    @GetMapping("/detail")
    public Result<TaskNoticeContentVO> detail(@RequestParam(value = "id")Long id){
        return success(taskNoticeContentService.detail(id));
    }

    /**
     * 获取我的任务数量
     */
    @PostMapping("/count")
    public Result<Integer> detail(@ModelAttribute TaskNoticeContentQTO qto){
        return success(taskNoticeContentService.getMyTaskNoticeNumber(qto));
    }

    /**
     * 不再接收某操作码对应的任务通知
     * @param qto
     * @return
     */
    @PostMapping("/dislikeTaskNotice")
    public Result<Void> dislikeTaskNotice(@ModelAttribute @Valid TaskNoticeFocusDTO qto){
        taskNoticeFocusService.dislikeTaskNotice(qto);
        return success();
    }

    /**
     * 接收某操作码对应的任务通知
     * @param qto
     * @return
     */
    @PostMapping("/likeTaskNotice")
    public Result<Void> likeTaskNotice(@ModelAttribute @Valid TaskNoticeFocusDTO qto){
        taskNoticeFocusService.likeTaskNotice(qto);
        return success();
    }

    /**
     * 不再接收任务通知的数据
     * @return
     */
    @GetMapping("/dislikeTaskNoticeList")
    public Result<List<TaskNoticeFocusVO>> dislikeTaskNoticeList(){
        return success(taskNoticeFocusService.getMyDislikeTaskNotice( ));
    }

}

