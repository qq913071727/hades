package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 订单明细响应实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrderMember响应对象", description="订单明细响应实体")
public class ImpOrderMemberVO implements Serializable {

    private Long id;
    @ApiModelProperty(value = "型号")
    private String model;
    @ApiModelProperty(value = "品牌")
    private String brand;
    @ApiModelProperty(value = "物料报关品牌")
    private String decBrand;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "物料报关名称")
    private String decName;
    @ApiModelProperty(value = "规格参数")
    private String spec;
    @ApiModelProperty(value = "客户物料号")
    private String goodsCode;
    @ApiModelProperty(value = "产地")
    private String country;
    @ApiModelProperty(value = "产地")
    private String countryName;
    @ApiModelProperty(value = "单位")
    private String unitCode;
    @ApiModelProperty(value = "单位")
    private String unitName;
    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;
    @ApiModelProperty(value = "委托单价")
    private BigDecimal unitPrice;
    @ApiModelProperty(value = "委托总价")
    private BigDecimal totalPrice;
    @ApiModelProperty(value = "报关单价")
    private BigDecimal decUnitPrice;
    @ApiModelProperty(value = "报关总价")
    private BigDecimal decTotalPrice;
    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;
    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;
    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;
    @ApiModelProperty(value = "箱号")
    private String cartonNo;
    @ApiModelProperty(value = "行号")
    private Integer rowNo;
    @ApiModelProperty(value = "物料状态")
    private String materielStatusId;
    private String materielStatusName;
    public String getMaterielStatusName(){
        MaterielStatusEnum materielStatusEnum = MaterielStatusEnum.of(getMaterielStatusId());
        return Objects.nonNull(materielStatusEnum)?materielStatusEnum.getName():"";
    }

    @ApiModelProperty(value = "海关编码")
    private String hsCode;
    @ApiModelProperty(value = "监管条件")
    private String csc;
    @ApiModelProperty(value = "检验检疫监管条件")
    private String iaqr;
    @ApiModelProperty(value = "CIQ编码")
    private String ciqNo;
    @ApiModelProperty(value = "是否涉证")
    private BigDecimal isSmp;
    @ApiModelProperty(value = "需要3C证书")
    private BigDecimal isCapp;
    @ApiModelProperty(value = "需要3C目录鉴定")
    private BigDecimal isCappNo;
    @ApiModelProperty(value = "3C证书编号")
    private String capp_no;

    @ApiModelProperty(value = "申报要素")
    private String declareSpec;

    private String receivingNoteNo;//入库单号
    private BigDecimal receivingQuantity;//绑定入库数量

    private BigDecimal tariffRate;//关税率
    private BigDecimal addedTaxTate;//增值税率
    private BigDecimal tariffRateVal;//关税金额
    private BigDecimal addedTaxTateVal;//增值税金额


}
