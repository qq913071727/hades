package com.tsfyun.scm.mapper.system;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.dto.system.RoleSaveDTO;
import com.tsfyun.scm.entity.system.SysRole;
import com.tsfyun.scm.vo.system.RolePersonVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMapper extends Mapper<SysRole> {

    List<SysRole> findRolesByPersonId(Long personId);

    List<RolePersonVO> findRolesByPersonIdBatch(List<Long> list);

    /**
     * 修改角色信息
     * @param dto
     */
    void updateRole(@Param("dto") RoleSaveDTO dto, @Param("userIdAndName") String userIdAndName);

}
