package com.tsfyun.scm.vo.materiel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SimpleMaterielVO implements Serializable {

    private String id;
    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "规格参数")
    private String spec;
}
