package com.tsfyun.scm.util;

import cn.hutool.core.collection.CollectionUtil;
import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.common.base.security.SysRoleVO;
import com.tsfyun.scm.entity.system.SysRole;
import com.tsfyun.common.base.security.SecurityUtil;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class AuthUserUtil {

    /**
     * 只能包含字母+数字、字母（不能为纯数字）
     */
    public static Pattern codePattern = Pattern.compile("^(?![0-9]+$)[0-9A-Za-z]+$");

    /**=
     * 未知客户角色编码
     */
    private final static String UNKNOWNCUSTOMERS = "unknown_customers";

    /**
     * 判断是否为管理员
     * @param roles
     * @return
     */
    public static boolean checkIsAdmin(List<SysRoleVO> roles, LoginVO loginVO, String manageRoleCode, String managePersonCode) {
        if(CollectionUtil.isEmpty(roles)) {
            return false;
        }
        return roles.stream().filter(r-> Objects.equals(r.getId().toLowerCase(),manageRoleCode)).count() > 0
                || Objects.equals(loginVO.getLoginName(),managePersonCode);
    }

    /**=
     * 判断是否拥有未知客户角色
     * @param roles
     * @return
     */
    public static boolean checkIsUnknownCustomers(List<SysRole> roles){
        if(CollectionUtil.isEmpty(roles)) {
            return false;
        }
        return roles.stream().filter(r-> Objects.equals(r.getId().toLowerCase(),UNKNOWNCUSTOMERS)).count() > 0;
    }

    /**
     * 检查当前登录人员是否把自己置为离职
     * @param personId 待修改的人员id
     * @param incumbency 待修改人员修改后的离职状态
     * @return
     */
    public static boolean checkSelfSetIncumbency(Long personId,Boolean incumbency) {
        //自己不能把自己置为离职
        LoginVO loginVO = SecurityUtil.getCurrent();
        if(Objects.nonNull(loginVO) && Objects.equals(loginVO.getPersonId(),personId)
                && !Objects.equals(incumbency,Boolean.TRUE)) {
            //是用户本身
            return true;
        }
        return false;
    }

}
