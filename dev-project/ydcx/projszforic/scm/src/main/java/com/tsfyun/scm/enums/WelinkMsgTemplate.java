package com.tsfyun.scm.enums;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

public enum WelinkMsgTemplate implements OperationEnum {

    PROVE("SMS_239775105", "您的验证码为：{}，请勿泄露于他人！"),
    CHANGE_IMPORTANT("SMS_239775105", "您的验证码为：{}，请勿泄露于他人！"),
    //付汇审核退回修改
    PAY_BACK("SMS_205685290","尊敬的客户：您所提交的付汇申请{}未通过审核，请您登录系统查看审核不通过原因，按要求修改后重新提交。感谢您的配合！"),
    //付汇审核退回作废
    PAY_INVALID("SMS_205685291","尊敬的客户：您所提交的付汇申请{}已作废，请您登录系统查看作废原因，重新提交申请。感谢您的配合！"),
    //付汇完成
    PAY_COMPLETE("SMS_205685292","尊敬的客户：贵公司的订单（{}）已完成付汇。"),
    //境外发车
    WAYBILL_DEPARTURE("SMS_205685293","尊敬的客户：您的订单{}已从香港发车，等待通关。"),
    //到达深圳仓
    WAYBILL_ARRIVE("SMS_205685294","尊敬的客户：您的订单{}已到达国内，等待入深圳仓库。"),
    //客户付款财务已确认
    CUSTOMER_PAY("SMS_205685295","尊敬的客户：我司已收到贵公司一笔金额为{}元的款项。"),
    //订单退回修改
    ORDER_BACK("SMS_205685296","尊敬的客户：您所提交的订单{}未通过审核，请您登录系统查看审核不通过原因，按要求修改后重新提交。感谢您的配合！"),
    //客户审核通过
    CUSTOMER_APROVED("SMS_205685297","尊敬的客户：您所提交的资料已通过审核，请尽快登录我司官网签约好协议后即可下单。"),
    //客户审核未通过
    CUSTOMER_REJECT("SMS_205685298","尊敬的客户：您所提交的资料未通过审核，请您修改资料信息后等待审核。"),
    ;
    private String code;
    private String name;

    WelinkMsgTemplate(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WelinkMsgTemplate of(String code) {
        return Arrays.stream(WelinkMsgTemplate.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

