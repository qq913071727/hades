package com.tsfyun.scm.util;

import com.tsfyun.common.base.util.StringUtils;

import java.math.BigDecimal;

/**
 * 物料管理工具类
 * z.mark
 * 2020-01-05
 */
public class MaterielUtil {

    //格式化归类型号
    public static String formatGoodsModel(String goodsModel) {
        String model = goodsModel.indexOf("##") > 0 ? goodsModel.split("##")[0] : goodsModel;
        if("无".equals(model)){
            return "无型号";
        }
        return model.concat("型");
    }
    //格式化归类品牌
    public static String formatGoodsBrand(String nameEn,String nameZh){
        // return String.format("中文品牌：%s，外文品牌：%s", StringUtils.removeSpecialSymbol(nameZh), StringUtils.removeSpecialSymbol(nameEn));
        return formatGoodsBrand(nameEn);
    }
    //格式化归类品牌
    public static String formatGoodsBrand(String nameEn){
        return StringUtils.removeSpecialSymbol(nameEn).replace("品牌","").replace("牌","").concat("牌");
    }
    //申报要素品牌标志
    public static String ELEMENT_BRAND_MARK = "品牌（中文或外文名称）";
    //申报要素型号标志
    public static String ELEMENT_MODEL_MARK = "型号";

    /**
     * 根据数量分摊重量（偏差放到最后一项）
     * @param totalQuantity      总数量
     * @param quantity           本次出数量
     * @param totalWeight        总重量
     * @param outQuantity        已出数量
     * @param outWeight          已出重量
     * @return
     */
    public static BigDecimal calWeight(BigDecimal totalQuantity,BigDecimal quantity,BigDecimal totalWeight,BigDecimal outQuantity,BigDecimal outWeight) {
        BigDecimal rate = totalWeight.divide(totalQuantity, 20, BigDecimal.ROUND_HALF_UP);
        BigDecimal calWeight = quantity.multiply(rate).setScale(4, BigDecimal.ROUND_HALF_UP);
        BigDecimal surplusQuantity = totalQuantity.subtract(outQuantity.add(quantity).setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
        if (surplusQuantity.compareTo(BigDecimal.ZERO) <= 0) {
            //本次为最后一次出库，把差额放到最后一项
            calWeight = calWeight.add(totalWeight.subtract(outWeight.add(calWeight).setScale(4, BigDecimal.ROUND_HALF_UP))).setScale(4, BigDecimal.ROUND_HALF_UP);
        }
        return calWeight;
    }


}
