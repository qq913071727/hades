package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.order.CostChangeRecordExpDTO;
import com.tsfyun.scm.dto.order.CostChangeRecordExpQTO;
import com.tsfyun.scm.dto.order.ImportCostChangeRecordExpDTO;
import com.tsfyun.scm.entity.order.CostChangeRecordExp;
import com.tsfyun.scm.vo.order.CostChangeRecordExpVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 费用变更记录 服务类
 * </p>
 *

 * @since 2020-05-27
 */
public interface ICostChangeRecordExpService extends IService<CostChangeRecordExp> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<CostChangeRecordExpVO> pageList(CostChangeRecordExpQTO qto);

    /**
     * 根据订单删除
     * @param orderId
     */
    void removeByOrderId(Long orderId);

    /**
     * 调整订单费用
     * @param dto
     */
    void adjustOrderCost(CostChangeRecordExpDTO dto);

    /**
     * 导入订单费用 （自己的模板）
     * @param file
     */
    void importOrderCost(MultipartFile file);

    /**
     * 批量保存订单导入费用（自己的模板）
     * @param dataList
     */
    void batchHandleImportCost(List<ImportCostChangeRecordExpDTO> dataList);


    /**
     * 导入订单其他费用（客户提供的模板）
     * @param file
     */
    void importOrderOtherCost(MultipartFile file);

    /**
     * 批量保存订单导入其他费用（客户提供的模板）
     * @param dataList
     */
    void batchHandleImportOtherCost(List<ImportCostChangeRecordExpDTO> dataList);



}
