package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 代客户签署协议请求实体
 * @CreateDate: Created in 2021/9/16 16:46
 */
@Data
public class SubstituteAgreementDTO implements Serializable {

    //报价单主单ID
    @NotNull(message = "id不能为空")
    private Long agreementId;


}
