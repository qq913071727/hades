package com.tsfyun.scm.mapper.system;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.system.SysDepartment;
import org.springframework.stereotype.Repository;

@Repository
public interface SysDepartmentMapper extends Mapper<SysDepartment> {
}
