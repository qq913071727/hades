package com.tsfyun.scm.util;

import com.tsfyun.common.base.enums.AgreeModeEnum;
import com.tsfyun.scm.entity.customer.ImpQuote;

/**
 * @Description:
 * @since Created in 2020/4/8 15:39
 */
public class AgreementUtil {

    public static String formatMonthDescribe(Integer month){
        if(month==0){
            return "本月";
        }else if(month==1){
            return "次月";
        }
        return String.format("第%d个月后",month);
    }
    public static String formatMonthDayDescribe(Integer monthDay){
        if(monthDay==31){
            return "月末";
        }
        return String.format("%d号",monthDay);
    }
    public static String formatWeekDescribe(Integer week){
        if(week==0){
            return "本周";
        }else if(week==1){
            return "下周";
        }
        return String.format("第%d周后",week);
    }
    public static String formatWeekDayDescribe(Integer weekDay){
        switch (weekDay){
            case 1:return "星期一";
            case 2:return "星期二";
            case 3:return "星期三";
            case 4:return "星期四";
            case 5:return "星期五";
            case 6:return "星期六";
            case 7:return "星期日";
        }
        return "未知星期";
    }

    public static String getAgreementQuoteDesc(ImpQuote impQuote) {
        AgreeModeEnum agreeModeEnum = AgreeModeEnum.of(impQuote.getAgreeMode());
        StringBuffer quote = new StringBuffer("");
        quote.append(agreeModeEnum.getName()+" ");

        switch (agreeModeEnum){
            case IMPORT_DAY:
                quote.append(impQuote.getDay()+"天");
                break;
            case PAYMENT_DAY:
                quote.append(impQuote.getDay()+"天");
                break;
            case MONTH:
                quote.append(AgreementUtil.formatMonthDescribe(impQuote.getMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getMonthDay()));
                break;
            case HALF_MONTH:
                quote.append("1号到15号:" + AgreementUtil.formatMonthDescribe(impQuote.getFirstMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getFirstMonthDay()));
                quote.append(" 16号到月末:"+ AgreementUtil.formatMonthDescribe(impQuote.getLowerMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getLowerMonthDay()));
                break;
            case WEEK:
                quote.append(AgreementUtil.formatWeekDescribe(impQuote.getWeek()) + AgreementUtil.formatWeekDayDescribe(impQuote.getWeekDay()));
                break;
        }
        AgreeModeEnum taxAgreeModeEnum = AgreeModeEnum.of(impQuote.getTaxAgreeMode());
        StringBuffer taxQuote = new StringBuffer("");
        taxQuote.append(taxAgreeModeEnum.getName()+" ");
        switch (taxAgreeModeEnum){
            case IMPORT_DAY:
                taxQuote.append(impQuote.getTaxDay()+"天");
                break;
            case PAYMENT_DAY:
                taxQuote.append(impQuote.getTaxDay()+"天");
                break;
            case MONTH:
                taxQuote.append(AgreementUtil.formatMonthDescribe(impQuote.getTaxMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getTaxMonthDay()));
                break;
            case HALF_MONTH:
                taxQuote.append("1号到15号:" + AgreementUtil.formatMonthDescribe(impQuote.getTaxFirstMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getTaxFirstMonthDay()));
                taxQuote.append(" 16号到月末:"+ AgreementUtil.formatMonthDescribe(impQuote.getTaxLowerMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getTaxLowerMonthDay()));
                break;
            case WEEK:
                taxQuote.append(AgreementUtil.formatWeekDescribe(impQuote.getTaxWeek()) + AgreementUtil.formatWeekDayDescribe(impQuote.getTaxWeekDay()));
                break;
        }

        AgreeModeEnum goAgreeModeEnum = AgreeModeEnum.of(impQuote.getGoAgreeMode());
        StringBuffer goQuote = new StringBuffer("");
        goQuote.append(goAgreeModeEnum.getName()+" ");
        switch (goAgreeModeEnum){
            case IMPORT_DAY:
                goQuote.append(impQuote.getGoDay()+"天");
                break;
            case PAYMENT_DAY:
                goQuote.append(impQuote.getGoDay()+"天");
                break;
            case MONTH:
                goQuote.append(AgreementUtil.formatMonthDescribe(impQuote.getGoMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getGoMonthDay()));
                break;
            case HALF_MONTH:
                goQuote.append("1号到15号:" + AgreementUtil.formatMonthDescribe(impQuote.getGoFirstMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getGoFirstMonthDay()));
                goQuote.append(" 16号到月末:"+ AgreementUtil.formatMonthDescribe(impQuote.getGoLowerMonth()) + AgreementUtil.formatMonthDayDescribe(impQuote.getGoLowerMonthDay()));
                break;
            case WEEK:
                goQuote.append(AgreementUtil.formatWeekDescribe(impQuote.getGoWeek()) + AgreementUtil.formatWeekDayDescribe(impQuote.getGoWeekDay()));
                break;
        }
        return quote.toString();
    }

}
