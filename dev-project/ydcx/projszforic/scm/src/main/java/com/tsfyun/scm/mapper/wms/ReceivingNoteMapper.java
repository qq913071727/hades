package com.tsfyun.scm.mapper.wms;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.wms.client.ClientReceivingNoteQTO;
import com.tsfyun.scm.entity.wms.ReceivingNote;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.storage.SimpleReceivingNoteCartonVO;
import com.tsfyun.scm.vo.wms.ReceivingNoteListVO;
import com.tsfyun.scm.vo.wms.ReceivingNoteVO;
import com.tsfyun.scm.vo.wms.client.ClientReceivingNoteVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库单 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface ReceivingNoteMapper extends Mapper<ReceivingNote> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ReceivingNoteListVO> list(Map<String,Object> params);

    ReceivingNoteVO detail(@Param(value = "id")Long id);

    /**
     * 根据入库单号模糊搜索带出入库单号和总箱数
     * @param receivingNoteNo
     * @param customerId
     * @return
     */
    List<SimpleReceivingNoteCartonVO> selectByReceivingNoteNo(@Param(value = "receivingNoteNo") String receivingNoteNo, @Param(value = "customerId") Long customerId);

    /**=
     * 根据入库单获取已绑定的订单号
     * @param docNo
     * @return
     */
    String findReceivingNoteOrderNo(@Param(value = "docNo")String docNo);

    /**
     * 客户端境外入库列表
     * @param qto
     * @return
     */
    List<ClientReceivingNoteVO> clientList(ClientReceivingNoteQTO qto);
}
