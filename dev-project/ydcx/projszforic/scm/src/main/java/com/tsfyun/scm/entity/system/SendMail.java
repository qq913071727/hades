package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 邮件记录
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class SendMail extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 邮件内容类型
     */

    private String mailType;

    /**
     * 邮件标题
     */

    private String title;

    /**
     * 邮件内容
     */

    private String content;

    /**
     * 发件人邮箱号
     */

    private String fromUserMail;

    /**
     * 收件人邮箱号-多个用英文逗号隔开
     */

    private String toUsersMail;

    /**
     * 发送状态描述-如发送异常存储异常消息
     */

    private String message;

    /**
     * 备注
     */

    private String memo;

    /**
     * 发送状态
     */

    private Integer status;



}
