package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.InvoiceMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.InvoiceMemberExcel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 发票明细 Mapper 接口
 * </p>
 *

 * @since 2020-05-18
 */
@Repository
public interface InvoiceMemberMapper extends Mapper<InvoiceMember> {


    Integer batchUpdateTax(@Param(value = "memberList") List<InvoiceMember> memberList);

    List<InvoiceMemberExcel> findByInvoiceIds(@Param(value = "ids")List<Long> ids);
}
