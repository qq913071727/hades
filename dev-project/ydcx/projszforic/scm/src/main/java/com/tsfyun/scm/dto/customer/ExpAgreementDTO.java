package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.exp.ExpSettlementModeEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 出口协议请求实体
 * </p>
 *
 *
 * @since 2021-09-10
 */
@Data
@ApiModel(value="ExpAgreement请求对象", description="出口协议请求实体")
public class ExpAgreementDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "id不能为空",groups = UpdateGroup.class)
   private Long id;

   @ApiModelProperty(value = "保存或提交审核")
   private Boolean submitAudit;

   @NotEmptyTrim(message = "协议编号不能为空")
   @LengthTrim(max = 20,message = "协议编号最大长度不能超过20位")
   @ApiModelProperty(value = "协议编号")
   private String docNo;


   @NotEmptyTrim(message = "报关类型不能为空")
   @LengthTrim(max = 20,message = "报关类型最大长度不能超过20位")
   @ApiModelProperty(value = "报关类型")
   private String declareType;


   @NotEmptyTrim(message = "甲方不能为空")
   @LengthTrim(max = 200,message = "甲方最大长度不能超过200位")
   @ApiModelProperty(value = "甲方")
   private String partyaName;

   @LengthTrim(max = 18,message = "甲方社会统一信用代码最大长度不能超过18位")
   @ApiModelProperty(value = "甲方社会统一信用代码")
   private String partyaSocialNo;

   @LengthTrim(max = 50,message = "甲方法人最大长度不能超过50位")
   @ApiModelProperty(value = "甲方法人")
   private String partyaLegalPerson;

   @LengthTrim(max = 50,message = "甲方电话最大长度不能超过50位")
   @ApiModelProperty(value = "甲方电话")
   private String partyaTel;

   @LengthTrim(max = 50,message = "甲方传真最大长度不能超过50位")
   @ApiModelProperty(value = "甲方传真")
   private String partyaFax;

   @LengthTrim(max = 255,message = "甲方地址最大长度不能超过255位")
   @ApiModelProperty(value = "甲方地址")
   private String partyaAddress;

   @NotEmptyTrim(message = "乙方不能为空")
   @LengthTrim(max = 255,message = "乙方最大长度不能超过255位")
   @ApiModelProperty(value = "乙方")
   private String partybName;

   @LengthTrim(max = 18,message = "乙方统一社会信用代码最大长度不能超过18位")
   @ApiModelProperty(value = "乙方统一社会信用代码")
   private String partybSocialNo;

   @LengthTrim(max = 50,message = "乙方法人最大长度不能超过50位")
   @ApiModelProperty(value = "乙方法人")
   private String partybLegalPerson;

   @LengthTrim(max = 50,message = "乙方电话最大长度不能超过50位")
   @ApiModelProperty(value = "乙方电话")
   private String partybTel;

   @LengthTrim(max = 50,message = "乙方传真最大长度不能超过50位")
   @ApiModelProperty(value = "乙方传真")
   private String partybFax;

   @LengthTrim(max = 255,message = "乙方地址最大长度不能超过255位")
   @ApiModelProperty(value = "乙方地址")
   private String partybAddress;

   @ApiModelProperty(value = "签约日期")
   @NotNull(message = "签约日期不能为空")
   private Date signingDate;

   @ApiModelProperty(value = "生效日期")
   @NotNull(message = "生效日期不能为空")
   private Date effectDate;

   @ApiModelProperty(value = "失效日期")
   @NotNull(message = "失效日期不能为空")
   private Date invalidDate;

   @LengthTrim(max = 2,message = "自动延期/年最大长度不能超过2位")
   @ApiModelProperty(value = "自动延期/年")
   @NotNull(message = "自动延期/年不能为空")
   private Integer delayYear;


   @NotEmptyTrim(message = "代理费结算模式不能为空")
   @LengthTrim(max = 20,message = "代理费结算模式最大长度不能超过20位")
   @EnumCheck(clazz = ExpSettlementModeEnum.class,message = "代理费结算模式错误")
   @ApiModelProperty(value = "代理费结算模式")
   private String settlementMode;

   @NotNull(message = "代理费%不能为空")
   @Digits(integer = 6, fraction = 4, message = "代理费%整数位不能超过6位，小数位不能超过4")
   @DecimalMin(value = "0.0001",message = "代理费率必须大于0")
   @ApiModelProperty(value = "代理费%")
   private BigDecimal agencyFee;

   @NotNull(message = "最低收费不能为空")
   @Digits(integer = 6, fraction = 2, message = "最低收费整数位不能超过6位，小数位不能超过2")
   @DecimalMin(value = "0.00",message = "最低收费必须大于等于0")
   @ApiModelProperty(value = "最低收费")
   private BigDecimal minAgencyFee;

   @NotNull(message = "是否垫税不能为空")
   @ApiModelProperty(value = "是否垫税")
   private Boolean isTaxAdvance;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
