package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.PaymentAccount;
import com.tsfyun.scm.entity.finance.PaymentAccountMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.PaymentAccountMemberVO;
import com.tsfyun.scm.vo.finance.PaymentOrderCostVO;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * <p>
 * 付款单明细 服务类
 * </p>
 *

 * @since 2020-04-24
 */
public interface IPaymentAccountMemberService extends IService<PaymentAccountMember> {

    /**
     * 根据付款单主单id获取付款单明细数据
     * @param paymentAccountId
     * @return
     */
    List<PaymentAccountMemberVO> findByPaymentAccountId(@NonNull Long paymentAccountId);

    List<PaymentAccountMember> getByPaymentAccountId(@NonNull Long paymentAccountId);

    /**
     * 批量更新付汇单明细人民币金额数据
     * @param paymentAccountMembers
     */
    void updateAccountValueCny(List<PaymentAccountMemberVO> paymentAccountMembers);

    /**=
     * 根据订单号查询付汇单号
     * @param orderId
     * @return
     */
    List<String> findByOrderIdByPayNos(Long orderId);

    /**=
     * 根据订单ID查询订单对应付款费用信息
     * @param orderId
     * @return
     */
    List<PaymentOrderCostVO> findByOrderPaymentCostId(Long orderId);

}
