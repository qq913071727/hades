package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.ExpOrderLogistics;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ExpOrderLogisticsSimpleVO;
import com.tsfyun.scm.vo.order.ExpOrderLogisticsVO;

import java.util.List;

/**
 * <p>
 * 出口订单物流信息 服务类
 * </p>
 *
 *
 * @since 2021-09-13
 */
public interface IExpOrderLogisticsService extends IService<ExpOrderLogistics> {

    ExpOrderLogisticsVO findById(Long id);

    /**
     * 获取最近货至客户指定地点的收货地址信息
     * @return
     */
    List<ExpOrderLogisticsSimpleVO> getCharterCarDeliveryInfo();

    /**
     * 根据订单id获取
     * @param orderId
     * @return
     */
    ExpOrderLogistics findByOrderId(Long orderId);
}
