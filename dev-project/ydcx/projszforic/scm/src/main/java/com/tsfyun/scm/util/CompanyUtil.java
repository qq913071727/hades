package com.tsfyun.scm.util;

import cn.hutool.core.util.ReflectUtil;
import com.tsfyun.common.base.dto.FileQTO;
import com.tsfyun.common.base.util.SpringBeanUtils;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.user.Person;
import com.tsfyun.scm.service.file.IUploadFileService;
import com.tsfyun.scm.service.user.ILoginService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description: 公司工具方法
 * @CreateDate: Created in 2020/11/25 16:09
 */
public class CompanyUtil {

    /**
     * 内部类单例模式
     */
    private static class CompanyUtilInstance{
        private static final CompanyUtil instance = new CompanyUtil();
    }

    private CompanyUtil(){}

    public static CompanyUtil getInstance(){
        return CompanyUtilInstance.instance;
    }

    public int calCompanyGrade(Customer customer, Person person) {
        AtomicInteger grade = new AtomicInteger();
        //客户信息分数检查项（合计57分）
        Map<String,GradeItem> checkCustomerType = new HashMap<String,GradeItem>(){{
            put("name",new GradeItem(9,"1"));//公司名称
            put("socialNo",new GradeItem(5,"1"));//统一社会信用代码
            put("taxpayerNo",new GradeItem(5,"1"));//税务登记证
            put("legalPerson",new GradeItem(2,"1"));//法人
            put("tel",new GradeItem(2,"1"));//电话
            put("address",new GradeItem(2,"1"));//公司地址
            put("invoiceBankName",new GradeItem(5,"1"));//开票银行名称
            put("invoiceBankAccount",new GradeItem(5,"1"));//开票银行账号
            put("invoiceBankAddress",new GradeItem(5,"1"));//开票银行地址
            put("invoiceBankTel",new GradeItem(2,"1"));//开票电话
            put("linkPerson",new GradeItem(5,"1"));//收票联系人
            put("linkTel",new GradeItem(5,"1"));//收票联系人电话
            put("linkAddress",new GradeItem(5,"1"));//收票地址
        }};
        //用户信息分数检查项（合计27分）
        Map<String,GradeItem> checkPersonType = new HashMap<String,GradeItem>(){{
            put("name",new GradeItem(3,"2"));//用户名称(新用户开通不算)
            put("mail",new GradeItem(5,"1"));//邮箱
            put("head",new GradeItem(3,"1"));//头像
            put("passWord",new GradeItem(8,"1"));//是否设置密码
            put("wxgzhOpenid",new GradeItem(8,"1"));//是否绑定微信公众号登录
        }};
        //上传三证资料、上传开票资料（合计16分）
        Map<String,GradeItem> checkFileType = new HashMap<String,GradeItem>(){{
            put("cus_business",new GradeItem(8,"1"));//三证资料
            put("cus_invoice",new GradeItem(8,"1"));//开票资料
        }};
        checkCustomerType.forEach((k,v)->{
            String checkVal = StringUtils.null2EmptyWithTrim(ReflectUtil.getFieldValue(customer,k));
            switch (v.getVal()) {
                case "1":
                    if(StringUtils.isNotEmpty(checkVal)) {
                        grade.set(grade.get() + v.getGrade());
                    }
                    break;
                default:
                    break;
            }
        });
        ILoginService loginService = SpringBeanUtils.getBean(ILoginService.class);
        checkPersonType.forEach((k,v)->{
            String checkVal = StringUtils.null2EmptyWithTrim(ReflectUtil.getFieldValue(person,k));
            switch (v.getVal()) {
                case "1":
                    if(Objects.equals(k,"wxgzhOpenid")) {
                        Boolean bindWxLogin = loginService.checkUserBindWxLogin(person.getId());
                        if(bindWxLogin) {
                            grade.set(grade.get() + v.getGrade());
                        }
                    } else {
                        if(StringUtils.isNotEmpty(checkVal)) {
                            grade.set(grade.get() + v.getGrade());
                        }
                    }
                    break;
                case "2":
                    if(StringUtils.isNotEmpty(checkVal) && !checkVal.startsWith("新用户")) {
                        grade.set(grade.get() + v.getGrade());
                    }
                    break;
                default:
                    break;
            }
        });
        IUploadFileService uploadFileService = SpringBeanUtils.getBean(IUploadFileService.class);
        checkFileType.forEach((k,v)->{
            FileQTO fileQTO = new FileQTO();
            fileQTO.setBusinessType(k);
            fileQTO.setDocType("customer");
            fileQTO.setDocId(customer.getId().toString());
            int fileCount = uploadFileService.count(fileQTO);
            if(fileCount > 0) {
                grade.set(grade.get() + v.getGrade());
            }
        });
        return grade.get();
    }

    /**
     * 账户安全级别
     * @return
     */
    public String accountSafeLevel(Person person){
        Integer grade = 0;
        // 设置密码
        if(StringUtils.isNotEmpty(person.getPassWord())){
            grade++;
        }
        // 绑定微信公众号
        if(StringUtils.isNotEmpty(person.getWxgzhOpenid())){
            grade++;
        }
        // 设置邮箱
        if(StringUtils.isNotEmpty(person.getMail())){
            grade++;
        }
        // 设置手机号
        if(StringUtils.isNotEmpty(person.getPhone())){
            grade++;
        }
        List<String> level = Arrays.asList("低","低","中","中","高");
        return level.get(grade);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    class GradeItem {
        private Integer grade;
        /**
         * 校验值说明
         * 1-不为空
         * 2-系统设置的默认值，不以新用户开头
         */
        private String  val;
    }

}
