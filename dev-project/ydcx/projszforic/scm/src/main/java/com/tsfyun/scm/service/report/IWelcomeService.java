package com.tsfyun.scm.service.report;

import com.tsfyun.scm.vo.report.ImpExpAgencyFeeVO;
import com.tsfyun.scm.vo.report.ImpExpTotalDataVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface IWelcomeService {

    // 进出口数据统计
    ImpExpTotalDataVO impExpTotalData();

    // 月度进出口数据统计
    Map<String,ImpExpTotalDataVO> monthSummaryImpExpTotalData(String year);

    // 月度代理费
    List<ImpExpAgencyFeeVO> monthImpExpAgencyFee(String year);
}
