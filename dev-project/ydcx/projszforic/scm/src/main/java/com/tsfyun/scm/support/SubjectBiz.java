package com.tsfyun.scm.support;

import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.scm.entity.system.SubjectOverseas;
import com.tsfyun.scm.service.customer.ISupplierService;
import com.tsfyun.scm.service.system.ISubjectOverseasService;
import com.tsfyun.scm.vo.customer.BuyerSellerInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/19 10:15
 */
@RequiredArgsConstructor
@Component
public class SubjectBiz {

    private final ISubjectOverseasService subjectOverseasService;

    private final ISupplierService supplierService;

    /**
     * 获取外贸合同买方信息
     * @param billType
     * @param subjectOverseasId
     * @param subjectOverseasType
     * @return
     */
    public BuyerSellerInfo getBySubjectOverseasType(BillTypeEnum billType, Long subjectOverseasId, String subjectOverseasType) {
        BuyerSellerInfo buyerSellerInfo = new BuyerSellerInfo();
        if(Objects.equals(billType,BillTypeEnum.EXP)) { //出口
            if(Objects.equals(subjectOverseasType,"1")) { //主体境外公司
                SubjectOverseas subjectOverseas = subjectOverseasService.getById(subjectOverseasId);
                if(Objects.nonNull(subjectOverseas)) {
                    buyerSellerInfo.setName(subjectOverseas.getNameCn());
                    buyerSellerInfo.setAddress(subjectOverseas.getAddressCn());
                    buyerSellerInfo.setAddressEn(subjectOverseas.getAddress());
                    buyerSellerInfo.setFax(subjectOverseas.getFax());
                    buyerSellerInfo.setTel(subjectOverseas.getTel());
                    buyerSellerInfo.setChapter(subjectOverseas.getChapter());
                    buyerSellerInfo.setNameEn(subjectOverseas.getName());
                }
            } else if(Objects.equals(subjectOverseasType,"2")){ //客户境外公司
                Supplier supplier = supplierService.getById(subjectOverseasId);
                if(Objects.nonNull(supplier)) {
                    buyerSellerInfo.setName("");
                    buyerSellerInfo.setAddress("");
                    buyerSellerInfo.setAddressEn(supplier.getAddress());
                    buyerSellerInfo.setFax("");
                    buyerSellerInfo.setTel("");
                    buyerSellerInfo.setChapter(null);
                    buyerSellerInfo.setNameEn(supplier.getName());
                }
            }
        } else { //进口
            SubjectOverseas subjectOverseas = subjectOverseasService.getById(subjectOverseasId);
            if(Objects.nonNull(subjectOverseas)) {
                buyerSellerInfo.setName(subjectOverseas.getNameCn());
                buyerSellerInfo.setAddress(subjectOverseas.getAddressCn());
                buyerSellerInfo.setAddressEn(subjectOverseas.getAddress());
                buyerSellerInfo.setFax(subjectOverseas.getFax());
                buyerSellerInfo.setTel(subjectOverseas.getTel());
                buyerSellerInfo.setChapter(subjectOverseas.getChapter());
                buyerSellerInfo.setNameEn(subjectOverseas.getName());
            }
        }
        return buyerSellerInfo;
    }

}
