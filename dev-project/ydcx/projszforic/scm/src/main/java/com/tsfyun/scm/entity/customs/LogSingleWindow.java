package com.tsfyun.scm.entity.customs;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import lombok.experimental.Accessors;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 单一窗口日志
 * </p>
 *

 * @since 2020-05-07
 */
@Data
@Accessors(chain = true)
public class LogSingleWindow implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 单据id
     */

    private String domainId;

    /**
     * 单据类型
     */

    private String domainType;

    /**
     * 操作节点
     */

    private String node;

    /**
     * 备注
     */

    private String memo;

    /**
     * 操作人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime dateCreated;


}
