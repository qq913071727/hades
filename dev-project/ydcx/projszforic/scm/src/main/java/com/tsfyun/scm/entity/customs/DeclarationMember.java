package com.tsfyun.scm.entity.customs;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
public class DeclarationMember  {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    private Long declarationId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String declareSpec;

    /**
     * 产地
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 币制
     */

    private String currencyName;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;


    private Integer rowNo;

    /**
     * 海关编码
     */

    private String hsCode;

    private String ciqNo;

    /**
     * 单位1
     */

    private String unit1Code;

    /**
     * 单位1
     */

    private String unit1Name;

    /**
     * 数量1
     */

    private BigDecimal quantity1;

    /**
     * 单位2
     */

    private String unit2Code;

    /**
     * 单位2
     */

    private String unit2Name;

    /**
     * 数量2
     */

    private BigDecimal quantity2;

    /**
     * 最终目的国
     */

    private String destinationCountry;

    /**
     * 最终目的国
     */

    private String destinationCountryName;

    /**
     * 征免方式	
     */

    private String dutyMode;

    /**
     * 征免方式	
     */

    private String dutyModeName;

    /**
     * 境内目的地/境内货源地
     */

    private String districtCode;

    /**
     * 境内目的地/境内货源地
     */

    private String districtName;

    /**
     * 目的地代码/产地代码
     */

    private String ciqDestCode;

    /**
     * 目的地代码/产地代码
     */

    private String ciqDestName;


}
