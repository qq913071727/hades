package com.tsfyun.scm.entity.materiel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.tsfyun.common.base.extension.UUIDWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2021-03-08
 */
@Data
public class MaterielExp implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    private String id;


    /**
     * 客户
     */

    private Long customerId;

    /**
     * 海关编码
     */

    private String hsCode;

    /**
     * CIQ编码
     */

    private String ciqNo;

    /**
     * 物料编号
     */

    private String code;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 申报要素
     */

    private String elements;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 归类备注
     */

    private String memo;

    /**
     * 来源
     */

    private String source;

    /**
     * 来源标识
     */

    private String sourceMark;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 归类人员
     */

    private String classifyPerson;

    /**
     * 最后归类时间
     */

    private Date classifyTime;


    /**
     * 通用字段，后续可以利用Mybatisl拦截器统一处理
     *
     */

    /**
     * 创建人   创建人personId/创建人名称
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private LocalDateTime dateUpdated;


}
