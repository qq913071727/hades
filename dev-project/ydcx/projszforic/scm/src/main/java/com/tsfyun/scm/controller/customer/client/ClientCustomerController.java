package com.tsfyun.scm.controller.customer.client;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.vo.SimpleBusinessVO;
import com.tsfyun.scm.dto.customer.CompanyDTO;
import com.tsfyun.scm.dto.customer.CustomerInvoiceInfoDTO;
import com.tsfyun.scm.service.customer.ICustomerService;
import com.tsfyun.scm.service.finance.IReceiptAccountService;
import com.tsfyun.scm.vo.customer.BusinessLicenseVO;
import com.tsfyun.scm.vo.customer.CompanyVO;
import com.tsfyun.scm.vo.customer.client.CheckCompanyResultVO;
import com.tsfyun.scm.vo.customer.client.ClientCustomerGradeVO;
import com.tsfyun.scm.vo.customer.client.ClientFinanceInfoVO;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


@RestController
@RequestMapping(value = "company")
@Api(tags = "公司")
public class ClientCustomerController extends BaseController {

    @Autowired
    private ICustomerService customerService;
    @Autowired
    private IReceiptAccountService receiptAccountService;

    /**
     * 获取公司基本信息
     * @return
     */
    @ApiOperation(value = "获取公司基本信息")
    @GetMapping("info")
    public Result<CompanyVO> info() {
        return success(customerService.companyInfo(SecurityUtil.getCurrentCustomerId()));
    }

    /**
     * 营业执照识别
     * @return
     */
    @ApiOperation(value = "营业执照识别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", required = true, paramType = "path", dataType = "Long")
    })
    @GetMapping(value = "businessLicenseOcr/{id}")
    public Result<BusinessLicenseVO> businessLicenseOcr(@PathVariable(value = "id") Long id) {
        return success(customerService.businessLicenseOcr(id));
    }

    /**
     * 检查客户修改的公司名称
     * @param name
     * @return
     */
    @PostMapping("checkCompanyName")
    public Result<CheckCompanyResultVO> checkCompanyName(@RequestParam(value = "name")String name){
        return success(customerService.checkCompanyName(name));
    }

    /**
     * 认领公司信息
     * @return
     */
    @PostMapping("claimCompany")
    public Result<Long> claimCompany(@RequestParam(value = "ncustomerId")Long ncustomerId){
        return success(customerService.claimCompany(ncustomerId));
    }

    /**
     * 修改公司信息
     * @return
     */
    @ApiOperation(value = "修改公司信息")
    @PostMapping("update")
    public Result<Integer> update(@ApiParam(value = "公司信息", type = "Object",required = true) @RequestBody @Validated CompanyDTO dto){
        return success(customerService.updateCompany(dto));
    }

    /**
     * 修改公司开票信息
     * @return
     */
    @ApiOperation(value = "修改公司开票信息")
    @PostMapping("updateInvoice")
    public Result<Void> updateInvoice(@ApiParam(value = "公司开票信息", type = "Object",required = true) @RequestBody @Validated CustomerInvoiceInfoDTO dto){
        customerService.saveCustomerInvoiceInfo(dto);
        return success();
    }

    /**
     * 获取公司财务信息
     * @return
     */
    @ApiOperation(value = "获取公司财务信息")
    @PostMapping("getCompanyFinance")
    public Result<ClientFinanceInfoVO> getCompanyFinance(){
        return success(customerService.getCompanyFinance( ));
    }

    /**
     * 获取公司完善信息评分值等
     * @return
     */
    @ApiOperation(value = "获取公司完善信息评分值")
    @GetMapping("grade")
    public Result<ClientCustomerGradeVO> grade() {
        return success(customerService.companyPerfectShow());
    }

    /**
     * 模糊查询公司工商信息
     * @param name
     * @return
     */
    @ApiIgnore
    @GetMapping("vagueSimpleBusinessQuery")
    public Result<List<SimpleBusinessVO>> vagueSimpleBusinessQuery(@RequestParam(value = "name",required = false,defaultValue = "")String name){
        return success(customerService.vagueSimpleBusinessQuery(name));
    }
}
