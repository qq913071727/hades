package com.tsfyun.scm.dto.finance.client;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @Description: 发票查询请求实体
 * @CreateDate: Created in 2020/11/19 13:12
 */
@Data
public class ClientInvoiceQTO extends PaginationDto implements Serializable {

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 发票号
     */
    private String invoiceNo;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 客户端状态
     */
    private String clientStatusId;

    /**
     * 状态集合
     */
    private List<String> statusIds;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime applyDateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime applyDateEnd;



    public void setApplyDateEnd(LocalDateTime applyDateEnd) {
        if(applyDateEnd != null){
            this.applyDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(applyDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
