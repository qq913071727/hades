package com.tsfyun.scm.controller.risk;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.risk.RiskApprovalQTO;
import com.tsfyun.scm.service.risk.IRiskApprovalService;
import com.tsfyun.scm.vo.risk.RiskApprovalVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 风控审批 前端控制器
 * </p>
 *
 * @since 2020-05-08
 */
@RestController
@RequestMapping("/riskApproval")
public class RiskApprovalController extends BaseController {

    @Autowired
    private IRiskApprovalService riskApprovalService;

    /**
     * 分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<RiskApprovalVO>> pageList(@ModelAttribute @Validated RiskApprovalQTO qto) {
        PageInfo<RiskApprovalVO> page = riskApprovalService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情信息
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<RiskApprovalVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(riskApprovalService.detail(id,operation));
    }

    /**
     * 风控审批（含订单及付款单）
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "approval")
    public Result<Void> approval(@ModelAttribute @Validated TaskDTO dto) {
        riskApprovalService.approval(dto);
        return success();
    }
}

