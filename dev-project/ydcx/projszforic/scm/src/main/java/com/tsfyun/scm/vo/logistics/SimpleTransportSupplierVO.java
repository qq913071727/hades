package com.tsfyun.scm.vo.logistics;

import lombok.Data;

import java.io.Serializable;

@Data
public class SimpleTransportSupplierVO implements Serializable {

    private Long id;

    private String name;

}
