package com.tsfyun.scm.service.third;

import com.tsfyun.scm.dto.support.WXReplyMessageDTO;
import com.tsfyun.scm.dto.support.WxMenuDTO;
import com.tsfyun.scm.vo.support.WxFocusUserInfoVO;

import java.util.Map;

public interface IWeixinService {
    //获取微信access_token
    String access_token();
    //获取微信jsapi_ticket
    String jsapi_ticket(String access_token);
    //获取微信配置
    Map<String,Object> config(String url);
    //微信授权
    String authorize(String redirect_uri,String scope,String state);
    //网页授权access_token
    Map<String,Object> access_token(String code);

    //void tempNotice(WxNoticeDTO dto);

    //生成临时二维码
    String genTempQrcode(String code);


    /**
     * 获取微信公众号用户信息
     * @param fromUserName
     * @return
     */
    WxFocusUserInfoVO getWxFocusUserInfo(String fromUserName);

    /**
     * 发送客服消息
     * @param dto
     */
    void sendCustomsMessage(WXReplyMessageDTO dto);

    /**
     * 新增微信菜单
     * @param dto
     */
    void addMenu(WxMenuDTO dto);

    /**
     * 删除全部菜单
     */
    void deleteMenu();

    /**
     * 生成短链接
     * @return
     */
    String generateShortUrl(String longUrl);
}
