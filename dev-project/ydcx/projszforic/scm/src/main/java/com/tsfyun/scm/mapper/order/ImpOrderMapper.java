package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.order.ClientImpOrderQTO;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 进口订单 Mapper 接口
 * </p>
 *

 * @since 2020-04-08
 */
@Repository
public interface ImpOrderMapper extends Mapper<ImpOrder> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ImpOrderVO> list(Map<String,Object> params);

    //客户端列表查询
    List<ClientImpOrderVO> clientList(ClientImpOrderQTO qto);

    //订单详情
    ImpOrderDetailVO detail(Long id);

    //根据订单号模糊搜索并带出客户名称，返回20条
    List<ImpOrderSimpleVO> selectByOrderNo(@Param(value = "orderNo") String orderNo);

    /**
     * 根据订单id修改绑定的跨境运输单号
     * @param id
     * @param transportNo
     * @return
     */
    int updateTransportNo(@Param(value = "id")Long id,@Param(value = "transportNo")String transportNo);

    /**
     * 获取跨境运输单可以绑定的订单
     * @param isCharterCar
     * @return
     */
    List<BindOrderVO> listCanBind(@Param(value = "isCharterCar")Boolean isCharterCar);

    /**
     * 获取跨境运输单已经绑定的订单
     * @param transportNo
     * @return
     */
    List<BindOrderVO> listBinded(@Param(value = "transportNo")String transportNo);

    /**
     * 置空订单中的运输单号
     * @param id
     */
    void resetTransportNo(@Param(value = "id")Long id);

    /**
     * 设置订单发票号
     * @param id
     */
    void setInvoiceNo(@Param(value = "id")Long id,@Param(value = "orderNo")String orderNo,@Param(value = "invoiceNo")String invoiceNo);

    /**
     * 设置付款单号
     * @param id
     * @param paymentNo
     */
    void updateOrderPaymentNo(@Param(value = "id")Long id,@Param(value = "paymentNo")String paymentNo);

    /**=
     * 订单付汇情况
     * @param params
     * @return
     */
    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ImpOrderPaySituationVO> paySituationList(Map<String,Object> params);

    /**=
     * 订单开票列表
     * @param params
     * @return
     */
    @DataScope(tableAlias = "a",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ImpOrderInvoiceVO> orderInvoiceList(Map<String,Object> params);

    /**
     * 设置订单销售合同号
     * @param impOrderNo
     * @param salesContractNo
     */
    void updateSalesContractNo(@Param(value = "impOrderNo") String impOrderNo,@Param(value = "salesContractNo") String salesContractNo);


    //根据订单号模糊搜索可以操作的订单，返回20条
    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ImpOrderAdjustCostVO> selectAdjustOrder(Map<String,Object> params);

    /**
     * 获取客户对应状态的订单数
     * @param customerId
     * @param statusId
     * @return
     */
    Integer getCountInStatusId(@Param(value = "customerId") Long customerId,@Param(value = "statusId")String statusId);

    /**
     * 获取客户未付款订单数
     * @param customerId
     * @return
     */
    Long getMyWaitPayOrderNum(@Param(value = "customerId") Long customerId);

    /**
     * 单量统计
     * @param startDate
     * @param endDate
     * @return
     */
    Integer totalSingleQuantity(@Param(value = "startDate")Date startDate,@Param(value = "endDate") Date endDate);

    /**
     * 金额统计(USD)
     * @param startDate
     * @param endDate
     * @return
     */
    BigDecimal totalAmountMoney(@Param(value = "startDate")Date startDate, @Param(value = "endDate") Date endDate);


    /**
     * 统计订单金额
     * @param params
     * @return
     */
    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    BigDecimal summaryOrderAmount(Map<String,Object> params);

}
