package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.MaterielBrandTypeEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.entity.materiel.MaterielBrand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 物料品牌请求实体
 * </p>
 *

 * @since 2020-03-26
 */
@Data
@ApiModel(value="MaterielBrand请求对象", description="物料品牌请求实体")
public class MaterielBrandDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "数据id不能为空",groups = UpdateGroup.class)
   private String id;

   @NotEmptyTrim(message = "英文名称不能为空")
   @LengthTrim(max = 100,message = "英文名称最大长度不能超过100位")
   @ApiModelProperty(value = "英文名称")
   private String name;

   @NotEmptyTrim(message = "中文名称不能为空")
   @LengthTrim(max = 100,message = "中文名称最大长度不能超过100位")
   @ApiModelProperty(value = "中文名称")
   private String memo;

   @LengthTrim(max = 10,message = "品牌类型最大长度不能超过10位")
   @EnumCheck(clazz = MaterielBrandTypeEnum.class,message = "仓库类型错误")
   @ApiModelProperty(value = "品牌类型")
   private String type;

}
