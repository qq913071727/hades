package com.tsfyun.scm.controller.finance.client;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.finance.client.ClientConfirmInvoiceDTO;
import com.tsfyun.scm.dto.finance.client.ClientInvoiceQTO;
import com.tsfyun.scm.entity.finance.Invoice;
import com.tsfyun.scm.service.finance.IInvoiceService;
import com.tsfyun.scm.vo.finance.client.ClientInvoiceListVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/19 14:24
 */
@RestController
@RequestMapping("/client/invoice")
@Api(tags = "发票（客户端）")
public class ClientInvoiceController extends BaseController {

    @Autowired
    private IInvoiceService invoiceService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 查询客户发票分页信息
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    @ApiOperation("分页")
    Result<List<ClientInvoiceListVO>> page(@RequestBody @Validated ClientInvoiceQTO qto) {
        PageInfo<ClientInvoiceListVO> page = invoiceService.clientPageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 客户确定税收分类编码
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirm")
    public Result<Void> confirm(@RequestBody @Validated ClientConfirmInvoiceDTO dto){
        invoiceService.clientConfrim(dto);
        return success();
    }

}
