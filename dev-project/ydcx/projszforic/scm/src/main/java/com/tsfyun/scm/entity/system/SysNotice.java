package com.tsfyun.scm.entity.system;

import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-09-22
 */
@Data
public class SysNotice extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 公告类别
     */

    private String category;

    /**
     * 公告标题
     */

    private String title;

    /**
     * 公告描述
     */

    private String represent;

    /**
     * 公告内容
     */

    private String content;

    /**
     * banner图链接
     */

    private String banner;

    /**
     * 公告是否对外公开
     */

    private Boolean isOpen;
    /**
     * 登录允许查看
     */

    private Boolean isLoginSee;

    /**
     * 是否弹出显示
     */

    private Boolean isPopup;

    /**
     * 公告时间
     */

    private LocalDateTime publishTime;



}
