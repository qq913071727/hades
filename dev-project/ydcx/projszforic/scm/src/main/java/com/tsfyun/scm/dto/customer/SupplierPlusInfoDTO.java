package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.FileDTO;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class SupplierPlusInfoDTO implements Serializable {

    /**
     * 供应商信息
     */
    @NotNull(message = "请填写供应商信息")
    @Valid
    private SupplierDTO supplier;

    /**
     * 供应商银行信息
     */
    @Valid
    private List<SupplierBankDTO> banks;

    /**
     * 供应商提货信息
     */
    @Valid
    private List<SupplierTakeInfoDTO> takes;

    /**=
     * 文件信息
     */
    private FileDTO file;
}
