package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.dto.finance.SettlementAccountQTO;
import com.tsfyun.scm.entity.finance.SettlementAccount;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.SettlementAccountVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 结汇主单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Repository
public interface SettlementAccountMapper extends Mapper<SettlementAccount> {

    List<SettlementAccountVO> list(SettlementAccountQTO qto);

    SettlementAccount findByOverseasReceivingAccountId(Long accountId);

}
