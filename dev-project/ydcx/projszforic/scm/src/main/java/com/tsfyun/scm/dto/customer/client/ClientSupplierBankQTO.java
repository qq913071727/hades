package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/29 17:43
 */
@Data
public class ClientSupplierBankQTO extends PaginationDto implements Serializable {

    private Long supplierId;

    private Boolean disabled;

}
