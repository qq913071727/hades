package com.tsfyun.scm.vo.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomerLoginInfoVO implements Serializable {

    private Long customerId;// 客户ID
    private Integer phoneQty;// 是否注册手机
    private Integer wechatQty;// 是否注册微信

    private Boolean isPhone;// 是否注册手机
    private Boolean isWechat;// 是否注册微信

    private String phoneNo;// 手机号

    public Boolean getIsPhone(){
        return phoneQty > 0;
    }
    public Boolean getIsWechat(){
        return wechatQty > 0;
    }
}
