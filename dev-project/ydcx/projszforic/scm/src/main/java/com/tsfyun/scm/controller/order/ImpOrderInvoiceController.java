package com.tsfyun.scm.controller.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ImpOrderInvoiceQTO;
import com.tsfyun.scm.service.order.IImpOrderInvoiceService;
import com.tsfyun.scm.vo.order.ImpOrderInvoiceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/impOrderInvoice")
public class ImpOrderInvoiceController extends BaseController {

    @Autowired
    private IImpOrderInvoiceService impOrderInvoiceService;

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ImpOrderInvoiceVO>> list(@ModelAttribute ImpOrderInvoiceQTO qto) {
        PageInfo<ImpOrderInvoiceVO> page = impOrderInvoiceService.list(qto);
        return success((int) page.getTotal(),page.getList());
    }

    /**=
     * 根据IDS查询
     * @param ids
     * @return
     */
    @PostMapping(value = "idsList")
    public Result<List<ImpOrderInvoiceVO>> idsList(@RequestParam(value = "ids")String ids) {
        return success(impOrderInvoiceService.idsList(Arrays.asList(ids.split(","))));
    }
}
