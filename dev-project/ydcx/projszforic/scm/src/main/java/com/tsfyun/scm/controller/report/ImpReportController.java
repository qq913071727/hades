package com.tsfyun.scm.controller.report;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.view.ViewImpDifferenceMemberQTO;
import com.tsfyun.scm.dto.view.ViewOrderCostMemberQTO;
import com.tsfyun.scm.service.report.IImpReportService;
import com.tsfyun.scm.vo.view.ViewImpDifferenceMemberVO;
import com.tsfyun.scm.vo.view.ViewImpDifferenceSummaryVO;
import com.tsfyun.scm.vo.view.ViewOrderCostMemberTotalVO;
import com.tsfyun.scm.vo.view.ViewOrderCostMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 进口报表
 */
@RestController
@RequestMapping("/impReport")
public class ImpReportController extends BaseController {

    @Autowired
    private IImpReportService impReportService;

    /**
     * 订单应收报表
     * @return
     */
    @PostMapping(value = "orderReceivable")
    public Result<List<ViewOrderCostMemberVO>> orderReceivable(@ModelAttribute @Validated ViewOrderCostMemberQTO qto){
        PageInfo<ViewOrderCostMemberVO> pageInfo = impReportService.orderReceivablePageList(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }
    /**
     * 订单应收报表统计
     * @return
     */
    @PostMapping(value = "orderReceivableTotal")
    public Result<ViewOrderCostMemberTotalVO> orderReceivableTotal(@ModelAttribute @Validated ViewOrderCostMemberQTO qto){
        return success(impReportService.orderReceivableTotal(qto));
    }

    /**
     * 订单应收报表Excel导出
     * @param qto
     * @return
     */
    @PostMapping(value = "exportOrderReceivableExcel")
    public Result<Long> exportOrderReceivableExcel(@ModelAttribute @Validated ViewOrderCostMemberQTO qto) throws Exception {
        return success(impReportService.exportOrderReceivableExcel(qto));
    }

    /**
     * 客户票差明细
     * @return
     */
    @PostMapping(value = "impDifferenceMember")
    public Result<List<ViewImpDifferenceMemberVO>> impDifferenceMember(@ModelAttribute @Validated ViewImpDifferenceMemberQTO qto){
        PageInfo<ViewImpDifferenceMemberVO> pageInfo = impReportService.impDifferenceMemberPage(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }


    /**
     * 客户票差明细Excel导出
     * @param qto
     * @return
     */
    @PostMapping(value = "impDifferenceMemberExport")
    public Result<Long> impDifferenceMemberExport(@ModelAttribute @Validated ViewImpDifferenceMemberQTO qto) throws Exception {
        return success(impReportService.exportImpDifferenceMember(qto));
    }

    /**
     * 客户票差明细汇总
     * @return
     */
    @PostMapping(value = "impDifferenceMemberSummary")
    public Result<List<ViewImpDifferenceSummaryVO>> impDifferenceMemberSummary(@ModelAttribute @Validated ViewImpDifferenceMemberQTO qto){
        PageInfo<ViewImpDifferenceSummaryVO> pageInfo = impReportService.impDifferenceSummaryPage(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 客户票差明细Excel导出
     * @param qto
     * @return
     */
    @PostMapping(value = "impDifferenceSummaryExport")
    public Result<Long> impDifferenceSummaryExport(@ModelAttribute @Validated ViewImpDifferenceMemberQTO qto) throws Exception {
        return success(impReportService.exportImpDifferenceSummary(qto));
    }
}
