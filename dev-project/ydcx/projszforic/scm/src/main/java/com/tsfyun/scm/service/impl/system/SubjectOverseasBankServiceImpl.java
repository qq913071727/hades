package com.tsfyun.scm.service.impl.system;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.system.SubjectOverseasBankDTO;
import com.tsfyun.scm.entity.base.Currency;
import com.tsfyun.scm.entity.system.SubjectOverseasBank;
import com.tsfyun.scm.mapper.system.SubjectOverseasBankMapper;
import com.tsfyun.scm.service.base.ICurrencyService;
import com.tsfyun.scm.service.system.ISubjectOverseasBankService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.system.ISubjectOverseasService;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.system.SubjectOverseasBankVO;
import com.tsfyun.scm.vo.system.SubjectOverseasVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Service
public class SubjectOverseasBankServiceImpl extends ServiceImpl<SubjectOverseasBank> implements ISubjectOverseasBankService {

    @Autowired
    private ICurrencyService currencyService;

    @Autowired
    private Snowflake snowflake;

    @Autowired
    private SubjectOverseasBankMapper subjectOverseasBankMapper;

    @Autowired
    private ISubjectOverseasService subjectOverseasService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addBatch(Long overseasId, List<SubjectOverseasBankDTO> banks) {
        if(CollectionUtil.isEmpty(banks)) {
            return;
        }
        Boolean isDefault = false;
        for(int i=0;i<banks.size();i++){
            SubjectOverseasBankDTO bankDTO = banks.get(i);
            String currencyName = bankDTO.getCurrencyName();
            Currency currency = currencyService.findByName(currencyName);
            TsfPreconditions.checkArgument(Objects.nonNull(currency),new ServiceException(String.format("第%d条银行信息币制不存在",(i+1))));
            bankDTO.setCurrencyId(currency.getId());
            if(bankDTO.getIsDefault()){isDefault=true;}
        }
        if(isDefault==false){
            banks.get(0).setIsDefault(Boolean.TRUE);
        }
        List<SubjectOverseasBank> bankList = Lists.newArrayListWithExpectedSize(banks.size());
        banks.stream().forEach(b->{
            SubjectOverseasBank r = new SubjectOverseasBank();
            r.setId(snowflake.nextId());
            r.setName(StringUtils.removeSpecialSymbol(b.getName()));
            r.setAccount(StringUtils.removeSpecialSymbol(b.getAccount()));
            r.setSwiftCode(StringUtils.removeSpecialSymbol(b.getSwiftCode()));
            r.setCurrencyId(b.getCurrencyId());
            r.setCode(StringUtils.removeSpecialSymbol(b.getCode()));
            r.setAddress(StringUtils.removeSpecialSymbol(b.getAddress()));
            r.setDisabled(b.getDisabled());
            r.setIsDefault(b.getIsDefault());
            r.setSubjectOverseasId(overseasId);
            r.setDateCreated(LocalDateTime.now());
            r.setDateUpdated(r.getDateCreated());
            r.setCreateBy(SecurityUtil.getCurrentPersonIdAndName());
            r.setUpdateBy(r.getCreateBy());
            bankList.add(r);
        });
        super.savaBatch(bankList);
    }

    @Override
    public Integer countByOverseasId(Long overseasId) {
        SubjectOverseasBank bank = new SubjectOverseasBank();
        bank.setSubjectOverseasId(overseasId);
        return subjectOverseasBankMapper.selectCount(bank);
    }

    @Override
    public List<SubjectOverseasBankVO> listByOverseasId(Long overseasId) {
        return subjectOverseasBankMapper.listByOverseasId(overseasId);
    }

    @Override
    public void removeByOverseasId(Long overseasId) {
        subjectOverseasBankMapper.deleteByExample(Example.builder(SubjectOverseasBank.class).where(
                TsfWeekendSqls.<SubjectOverseasBank>custom().andEqualTo(false,SubjectOverseasBank::getSubjectOverseasId,overseasId)
        ).build());
    }

    @Override
    public List<SubjectOverseasBankVO> getDefaultSubjectOverseasBank() {
        SubjectOverseasVO subjectOverseasVO = subjectOverseasService.defOverseas();
        if(Objects.isNull(subjectOverseasVO)) {
            return null;
        }
        return listByOverseasId(subjectOverseasVO.getId());
    }

    @Override
    public SubjectOverseasBankVO detail(Long id) {
        SubjectOverseasBankVO subjectOverseasBankVO = beanMapper.map(super.getById(id),SubjectOverseasBankVO.class);
        Currency currency = currencyService.findById(subjectOverseasBankVO.getCurrencyId());
        subjectOverseasBankVO.setCurrencyName(currency.getName());
        return subjectOverseasBankVO;
    }
}
