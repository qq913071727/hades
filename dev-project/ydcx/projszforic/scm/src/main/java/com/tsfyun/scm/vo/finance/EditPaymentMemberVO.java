package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class EditPaymentMemberVO implements Serializable {

    private Long id;//订单ID
    @ApiModelProperty(value = "订单编号")
    private String docNo;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;
    private String supplierName;//客户供应商名称
    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "付汇申请金额")
    private BigDecimal applyPayVal;
    @ApiModelProperty(value = "付汇付出金额")
    private BigDecimal payVal;
    @ApiModelProperty(value = "本次申请金额")
    private BigDecimal thisApplyPayVal;
}
