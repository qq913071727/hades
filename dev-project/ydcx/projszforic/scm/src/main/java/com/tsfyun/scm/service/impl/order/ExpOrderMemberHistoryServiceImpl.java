package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.order.ExpOrderMemberHistory;
import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import com.tsfyun.scm.mapper.order.ExpOrderMemberHistoryMapper;
import com.tsfyun.scm.mapper.order.ImpOrderMemberHistoryMapper;
import com.tsfyun.scm.service.order.IExpOrderMemberHistoryService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Service
public class ExpOrderMemberHistoryServiceImpl extends ServiceImpl<ExpOrderMemberHistory> implements IExpOrderMemberHistoryService {
    @Autowired
    private ExpOrderMemberHistoryMapper expOrderMemberHistoryMapper;

    @Override
    public void removeByOrderId(Long orderId) {
        expOrderMemberHistoryMapper.removeByOrderId(orderId);
    }


    @Override
    public List<ExpOrderMemberHistory> getByOrderId(Long orderId) {
        TsfWeekendSqls wheres = TsfWeekendSqls.<ExpOrderMemberHistory>custom().andEqualTo(false,ExpOrderMemberHistory::getExpOrderId,orderId);
        List<ExpOrderMemberHistory> members = expOrderMemberHistoryMapper.selectByExample(Example.builder(ExpOrderMemberHistory.class).where(wheres).build());
        if(CollUtil.isNotEmpty(members)){
            //按行号升序
            return members.stream().sorted(Comparator.comparing(ExpOrderMemberHistory::getRowNo)).collect(Collectors.toList());
        }
        return members;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchUpdateSplitMembers(List<ExpOrderMemberHistory> expOrderMemberHistoryList) {
        if(Objects.nonNull(expOrderMemberHistoryList)){
            expOrderMemberHistoryMapper.batchUpdateSplitMembers(expOrderMemberHistoryList);
        }
    }
}
