//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import com.tsfyun.common.base.util.jaxb.CDataAdapter;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 报关单表体信息
 * 
 * <p>DecListItemType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="DecListItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodeTS">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="10"/>
 *               &lt;pattern value="[0-9]{10}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ContrItem" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="19"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclPrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DutyMode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Factor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GModel" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OriginCountry">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeCurr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclTotal">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GQty" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FirstQty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecondQty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GUnit" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;maxLength value="3"/>
 *               &lt;pattern value="[0-9]{3}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FirstUnit" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;maxLength value="3"/>
 *               &lt;pattern value="[0-9]{3}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SecondUnit" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *               &lt;pattern value="[0-9]{3}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UseTo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="2"/>
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WorkUsd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExgNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ExgVersion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DestinationCountry">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CiqCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclGoodsEname" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OrigPlaceCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Purpose" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ProdValidDt" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ProdQgp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsAttr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Stuff" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="400"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Uncode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DangName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="80"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DangPackType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DangPackSpec" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="24"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EngManEntCnm" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NoDangFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DestCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsSpec" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsModel" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsBrand" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ProduceDate" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ProdBatchNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DistrictCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CiqName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DecGoodsLimits" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecGoodsLimit" type="{http://www.chinaport.gov.cn/dec}DecGoodsLimitType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MnufctrRegNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MnufctrRegName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecListItemType", propOrder = {
    "classMark",
    "codeTS",
    "contrItem",
    "declPrice",
    "dutyMode",
    "factor",
    "gModel",
    "gName",
    "gNo",
    "originCountry",
    "tradeCurr",
    "declTotal",
    "gQty",
    "firstQty",
    "secondQty",
    "gUnit",
    "firstUnit",
    "secondUnit",
    "useTo",
    "workUsd",
    "exgNo",
    "exgVersion",
    "destinationCountry",
    "ciqCode",
    "declGoodsEname",
    "origPlaceCode",
    "purpose",
    "prodValidDt",
    "prodQgp",
    "goodsAttr",
    "stuff",
    "uncode",
    "dangName",
    "dangPackType",
    "dangPackSpec",
    "engManEntCnm",
    "noDangFlag",
    "destCode",
    "goodsSpec",
    "goodsModel",
    "goodsBrand",
    "produceDate",
    "prodBatchNo",
    "districtCode",
    "ciqName",
    "mnufctrRegNo",
    "mnufctrRegName"
})
public class DecListItemType {
    //归类标志，空值
    @XmlElement(name = "ClassMark")
    protected String classMark;
    //商品编号
    @XmlElement(name = "CodeTS", required = true)
    protected String codeTS;
    //备案序号
    @XmlElement(name = "ContrItem")
    protected String contrItem;
    //申报单价
    @XmlElement(name = "DeclPrice", required = true)
    protected String declPrice;
    //申报总价
    @XmlElement(name = "DeclTotal", required = true)
    protected String declTotal;
    //征减免税方式0-9
    @XmlElement(name = "DutyMode")
    protected String dutyMode;
    //货号
    @XmlElement(name = "ExgNo")
    protected String exgNo;
    //版本
    @XmlElement(name = "ExgVersion")
    protected String exgVersion;
    //申报计量单位与法定单位比例因子
    @XmlElement(name = "Factor")
    protected String factor;
    //法一单位
    @XmlElement(name = "FirstUnit")
    protected String firstUnit;
    //法一数量
    @XmlElement(name = "FirstQty")
    protected String firstQty;
    //成交计量单位
    @XmlElement(name = "GUnit")
    protected String gUnit;
    //商品规格/型号
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "GModel")
    protected String gModel;
    //商品名称
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "GName", required = true)
    protected String gName;
    //商品序号
    @XmlElement(name = "GNo", required = true)
    protected String gNo;
    //成交数量
    @XmlElement(name = "GQty", required = true)
    protected String gQty;
    //原产国
    @XmlElement(name = "OriginCountry", required = true)
    protected String originCountry;
    //法二单位
    @XmlElement(name = "SecondUnit")
    protected String secondUnit;
    //法二数量
    @XmlElement(name = "SecondQty")
    protected String secondQty;
    //成交币制
    @XmlElement(name = "TradeCurr")
    protected String tradeCurr;
    //用途/生产厂家
    @XmlElement(name = "UseTo")
    protected String useTo;
    //工缴费
    @XmlElement(name = "WorkUsd")
    protected String workUsd;
    //最终目的国
    @XmlElement(name = "DestinationCountry", required = true)
    protected String destinationCountry;
    //检验检疫编码
    @XmlElement(name = "CiqCode", required = true)
    protected String ciqCode;
    //商品名称英文
    @XmlElement(name = "DeclGoodsEname")
    protected String declGoodsEname;
    //原产地区代码
    @XmlElement(name = "OrigPlaceCode")
    protected String origPlaceCode;
    //用途代码
    @XmlElement(name = "Purpose")
    protected String purpose;
    //产品有效期
    @XmlElement(name = "ProdValidDt")
    protected String prodValidDt;
    //产品保质期
    @XmlElement(name = "ProdQgp")
    protected String prodQgp;
    //货物属性代码
    @XmlElement(name = "GoodsAttr")
    protected String goodsAttr;
    //成分/原料/组份
    @XmlElement(name = "Stuff")
    protected String stuff;
    //UN编码
    @XmlElement(name = "Uncode")
    protected String uncode;
    //危险货物名称
    @XmlElement(name = "DangName")
    protected String dangName;
    //危包类别
    @XmlElement(name = "DangPackType")
    protected String dangPackType;
    //危包规格
    @XmlElement(name = "DangPackSpec")
    protected String dangPackSpec;
    //境外生产企业名称
    @XmlElement(name = "EngManEntCnm")
    protected String engManEntCnm;
    //非危险化学品
    @XmlElement(name = "NoDangFlag")
    protected String noDangFlag;
    //目的地代码
    @XmlElement(name = "DestCode")
    protected String destCode;
    //检验检疫货物规格
    @XmlElement(name = "GoodsSpec")
    protected String goodsSpec;
    //货物型号
    @XmlElement(name = "GoodsModel")
    protected String goodsModel;
    //货物品牌
    @XmlElement(name = "GoodsBrand")
    protected String goodsBrand;
    //生产日期
    @XmlElement(name = "ProduceDate")
    protected String produceDate;
    //生产批号
    @XmlElement(name = "ProdBatchNo")
    protected String prodBatchNo;
    //进口-境内目的地，出口-境内货源地
    @XmlElement(name = "DistrictCode")
    protected String districtCode;
    //校验检疫名称
    @XmlElement(name = "CiqName")
    protected String ciqName;
    //生产单位注册号
    @XmlElement(name = "MnufctrRegNo")
    protected String mnufctrRegNo;
    //生产单位名称
    @XmlElement(name = "MnufctrRegName")
    protected String mnufctrRegName;

    public String getClassMark() {
        return classMark;
    }

    public void setClassMark(String classMark) {
        this.classMark = classMark;
    }

    public String getgUnit() {
        return gUnit;
    }

    public void setgUnit(String gUnit) {
        this.gUnit = gUnit;
    }

    public String getgModel() {
        return gModel;
    }

    public void setgModel(String gModel) {
        this.gModel = gModel;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getgNo() {
        return gNo;
    }

    public void setgNo(String gNo) {
        this.gNo = gNo;
    }

    public String getgQty() {
        return gQty;
    }

    public void setgQty(String gQty) {
        this.gQty = gQty;
    }

    /**
     * 获取codeTS属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeTS() {
        return codeTS;
    }

    /**
     * 设置codeTS属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeTS(String value) {
        this.codeTS = value;
    }

    /**
     * 获取contrItem属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrItem() {
        return contrItem;
    }

    /**
     * 设置contrItem属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrItem(String value) {
        this.contrItem = value;
    }

    /**
     * 获取declPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclPrice() {
        return declPrice;
    }

    /**
     * 设置declPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclPrice(String value) {
        this.declPrice = value;
    }

    /**
     * 获取dutyMode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyMode() {
        return dutyMode;
    }

    /**
     * 设置dutyMode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyMode(String value) {
        this.dutyMode = value;
    }

    /**
     * 获取factor属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactor() {
        return factor;
    }

    /**
     * 设置factor属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactor(String value) {
        this.factor = value;
    }

    /**
     * 获取gModel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGModel() {
        return gModel;
    }

    /**
     * 设置gModel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGModel(String value) {
        this.gModel = value;
    }

    /**
     * 获取gName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGName() {
        return gName;
    }

    /**
     * 设置gName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGName(String value) {
        this.gName = value;
    }

    /**
     * 获取gNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGNo() {
        return gNo;
    }

    /**
     * 设置gNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGNo(String value) {
        this.gNo = value;
    }

    /**
     * 获取originCountry属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCountry() {
        return originCountry;
    }

    /**
     * 设置originCountry属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCountry(String value) {
        this.originCountry = value;
    }

    /**
     * 获取tradeCurr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeCurr() {
        return tradeCurr;
    }

    /**
     * 设置tradeCurr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeCurr(String value) {
        this.tradeCurr = value;
    }

    /**
     * 获取declTotal属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclTotal() {
        return declTotal;
    }

    /**
     * 设置declTotal属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclTotal(String value) {
        this.declTotal = value;
    }

    /**
     * 获取gQty属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGQty() {
        return gQty;
    }

    /**
     * 设置gQty属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGQty(String value) {
        this.gQty = value;
    }

    /**
     * 获取firstQty属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstQty() {
        return firstQty;
    }

    /**
     * 设置firstQty属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstQty(String value) {
        this.firstQty = value;
    }

    /**
     * 获取secondQty属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondQty() {
        return secondQty;
    }

    /**
     * 设置secondQty属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondQty(String value) {
        this.secondQty = value;
    }

    /**
     * 获取gUnit属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGUnit() {
        return gUnit;
    }

    /**
     * 设置gUnit属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGUnit(String value) {
        this.gUnit = value;
    }

    /**
     * 获取firstUnit属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstUnit() {
        return firstUnit;
    }

    /**
     * 设置firstUnit属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstUnit(String value) {
        this.firstUnit = value;
    }

    /**
     * 获取secondUnit属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondUnit() {
        return secondUnit;
    }

    /**
     * 设置secondUnit属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondUnit(String value) {
        this.secondUnit = value;
    }

    /**
     * 获取useTo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseTo() {
        return useTo;
    }

    /**
     * 设置useTo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseTo(String value) {
        this.useTo = value;
    }

    /**
     * 获取workUsd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkUsd() {
        return workUsd;
    }

    /**
     * 设置workUsd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkUsd(String value) {
        this.workUsd = value;
    }

    /**
     * 获取exgNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExgNo() {
        return exgNo;
    }

    /**
     * 设置exgNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExgNo(String value) {
        this.exgNo = value;
    }

    /**
     * 获取exgVersion属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExgVersion() {
        return exgVersion;
    }

    /**
     * 设置exgVersion属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExgVersion(String value) {
        this.exgVersion = value;
    }

    /**
     * 获取destinationCountry属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * 设置destinationCountry属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

    /**
     * 获取ciqCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiqCode() {
        return ciqCode;
    }

    /**
     * 设置ciqCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiqCode(String value) {
        this.ciqCode = value;
    }

    /**
     * 获取declGoodsEname属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclGoodsEname() {
        return declGoodsEname;
    }

    /**
     * 设置declGoodsEname属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclGoodsEname(String value) {
        this.declGoodsEname = value;
    }

    /**
     * 获取origPlaceCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigPlaceCode() {
        return origPlaceCode;
    }

    /**
     * 设置origPlaceCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigPlaceCode(String value) {
        this.origPlaceCode = value;
    }

    /**
     * 获取purpose属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * 设置purpose属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurpose(String value) {
        this.purpose = value;
    }

    /**
     * 获取prodValidDt属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdValidDt() {
        return prodValidDt;
    }

    /**
     * 设置prodValidDt属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdValidDt(String value) {
        this.prodValidDt = value;
    }

    /**
     * 获取prodQgp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdQgp() {
        return prodQgp;
    }

    /**
     * 设置prodQgp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdQgp(String value) {
        this.prodQgp = value;
    }

    /**
     * 获取goodsAttr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsAttr() {
        return goodsAttr;
    }

    /**
     * 设置goodsAttr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsAttr(String value) {
        this.goodsAttr = value;
    }

    /**
     * 获取stuff属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStuff() {
        return stuff;
    }

    /**
     * 设置stuff属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStuff(String value) {
        this.stuff = value;
    }

    /**
     * 获取uncode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUncode() {
        return uncode;
    }

    /**
     * 设置uncode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUncode(String value) {
        this.uncode = value;
    }

    /**
     * 获取dangName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDangName() {
        return dangName;
    }

    /**
     * 设置dangName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDangName(String value) {
        this.dangName = value;
    }

    /**
     * 获取dangPackType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDangPackType() {
        return dangPackType;
    }

    /**
     * 设置dangPackType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDangPackType(String value) {
        this.dangPackType = value;
    }

    /**
     * 获取dangPackSpec属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDangPackSpec() {
        return dangPackSpec;
    }

    /**
     * 设置dangPackSpec属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDangPackSpec(String value) {
        this.dangPackSpec = value;
    }

    /**
     * 获取engManEntCnm属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngManEntCnm() {
        return engManEntCnm;
    }

    /**
     * 设置engManEntCnm属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngManEntCnm(String value) {
        this.engManEntCnm = value;
    }

    /**
     * 获取noDangFlag属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoDangFlag() {
        return noDangFlag;
    }

    /**
     * 设置noDangFlag属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoDangFlag(String value) {
        this.noDangFlag = value;
    }

    /**
     * 获取destCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestCode() {
        return destCode;
    }

    /**
     * 设置destCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestCode(String value) {
        this.destCode = value;
    }

    /**
     * 获取goodsSpec属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsSpec() {
        return goodsSpec;
    }

    /**
     * 设置goodsSpec属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsSpec(String value) {
        this.goodsSpec = value;
    }

    /**
     * 获取goodsModel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsModel() {
        return goodsModel;
    }

    /**
     * 设置goodsModel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsModel(String value) {
        this.goodsModel = value;
    }

    /**
     * 获取goodsBrand属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsBrand() {
        return goodsBrand;
    }

    /**
     * 设置goodsBrand属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsBrand(String value) {
        this.goodsBrand = value;
    }

    /**
     * 获取produceDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduceDate() {
        return produceDate;
    }

    /**
     * 设置produceDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduceDate(String value) {
        this.produceDate = value;
    }

    /**
     * 获取prodBatchNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdBatchNo() {
        return prodBatchNo;
    }

    /**
     * 设置prodBatchNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdBatchNo(String value) {
        this.prodBatchNo = value;
    }

    /**
     * 获取districtCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrictCode() {
        return districtCode;
    }

    /**
     * 设置districtCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrictCode(String value) {
        this.districtCode = value;
    }

    /**
     * 获取ciqName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiqName() {
        return ciqName;
    }

    /**
     * 设置ciqName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiqName(String value) {
        this.ciqName = value;
    }

    /**
     * 获取mnufctrRegNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMnufctrRegNo() {
        return mnufctrRegNo;
    }

    /**
     * 设置mnufctrRegNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMnufctrRegNo(String value) {
        this.mnufctrRegNo = value;
    }

    /**
     * 获取mnufctrRegName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMnufctrRegName() {
        return mnufctrRegName;
    }

    /**
     * 设置mnufctrRegName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMnufctrRegName(String value) {
        this.mnufctrRegName = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecGoodsLimit" type="{http://www.chinaport.gov.cn/dec}DecGoodsLimitType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decGoodsLimit"
    })
    public static class DecGoodsLimits {

        @XmlElement(name = "DecGoodsLimit")
        protected List<DecGoodsLimitType> decGoodsLimit;

        /**
         * Gets the value of the decGoodsLimit property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decGoodsLimit property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecGoodsLimit().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecGoodsLimitType }
         * 
         * 
         */
        public List<DecGoodsLimitType> getDecGoodsLimit() {
            if (decGoodsLimit == null) {
                decGoodsLimit = new ArrayList<DecGoodsLimitType>();
            }
            return this.decGoodsLimit;
        }

    }

}
