package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 退税打印付款申请单
 * @CreateDate: Created in 2021/11/26 11:35
 */
@Data
public class PrintDrawBackVO implements Serializable {

    /**
     * 收款单位
     */

    private String payeeName;


    /**
     * 要求付款时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date claimPaymentDate;

    /**
     * 申请付款时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date applyPaymentDate;

    /**
     * 付款金额
     */

    private BigDecimal accountValue;

    /**
     * 付款金额大写
     */

    private String accountValueCn;


    /**
     * 收款银行
     */

    private String payeeBank;

    /**
     * 收款银行账号
     */

    private String payeeBankNo;


    /**
     * 申请人
     */
    private String applyUser;

    private String remark;

    /**
     * 客户单号
     */
    private String clientNo;

}
