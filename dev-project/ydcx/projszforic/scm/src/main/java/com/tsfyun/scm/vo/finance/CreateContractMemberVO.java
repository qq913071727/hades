package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CreateContractMemberVO implements Serializable {

    private Integer rowNo;

    /**
     * 订单明细
     */
    private Long orderMemberId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 报关总价
     */
    private BigDecimal decTotalPrice;

    private BigDecimal settlementValue;// 结汇人民币金额

    private BigDecimal settlementValueCny;// 已结汇人民币金额

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 代理费
     */

    private BigDecimal agentFee;

    /**
     * 代垫费
     */

    private BigDecimal matFee;

    /**
     * 票差调整金额
     */

    private BigDecimal differenceVal;

    /**
     * 增值税率
     */
    private BigDecimal addedTaxRate;

    /**
     * 退税率
     */
    private BigDecimal taxRebateRate;


}
