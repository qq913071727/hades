package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.PriceFluctuationHistoryExp;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-17
 */
@Repository
public interface PriceFluctuationHistoryExpMapper extends Mapper<PriceFluctuationHistoryExp> {

}
