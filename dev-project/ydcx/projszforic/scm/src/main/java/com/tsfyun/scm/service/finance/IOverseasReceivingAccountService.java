package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.scm.dto.finance.ClaimedAmountPlusDTO;
import com.tsfyun.scm.dto.finance.ConfirmOverseasReceivingAccountDTO;
import com.tsfyun.scm.dto.finance.OverseasReceivingAccountDTO;
import com.tsfyun.scm.dto.finance.OverseasReceivingAccountQTO;
import com.tsfyun.scm.entity.finance.OverseasReceivingAccount;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.*;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 境外收款 服务类
 * </p>
 *
 *
 * @since 2021-10-08
 */
public interface IOverseasReceivingAccountService extends IService<OverseasReceivingAccount> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<OverseasReceivingAccountVO> pageList(OverseasReceivingAccountQTO qto);

    /**
     * 详情
     * @param id
     * @param operation
     * @return
     */
    OverseasReceivingAccountPlusVO detail(Long id, String operation);

    /**
     * 境外收款登记
     * @param dto
     */
    void add(OverseasReceivingAccountDTO dto);
    void edit(OverseasReceivingAccountDTO dto);

    /**
     * 确认
     * @param dto
     */
    void confirm(ConfirmOverseasReceivingAccountDTO dto);

    /**
     * 收款单认领
     * @param dto
     */
    void claim(ClaimedAmountPlusDTO dto);

    /**
     * 清空收款单认领数据
     * @param id
     */
    void clearClaimOrder(Long id, DomainOprationEnum oprationEnum);

    /**
     * 作废
     * @param dto
     */
    void toVoid(TaskDTO dto);

    /**
     * 删除收款单
     * @param id
     */
    void removeAllById(Long id);

    /**
     * 获取主体
     * @return
     */
    List<SubjectTypeVO> subjectSelect(String first);

    /**
     * 初始化结汇单
     * @param ids
     * @return
     */
    SettlementPlusVO initSettlement(List<Long> ids);

    /**
     * 初始化付款单
     * @param id
     * @return
     */
    CreatePaymentVO initPayment(Long id);

    /**
     * 根据收款单编号获取订单合并信息
     * @param accountNos
     * @return
     */
    List<OverseasReceivingOrderVO> overseasReceivings(List<String> accountNos);

    /**
     * 写入结汇信息
     * @param id
     * @param settlementValue
     */
    void writeSettlementValue(Long id,String settlementNo,BigDecimal settlementValue,Long settlementId,BigDecimal customsRate);

    /**
     * 取消结汇信息
     * @param id
     * @param settlementValue
     */
    void canWriteSettlementValue(Long id,BigDecimal settlementValue);

    /**
     * 查询订单对应的所有收款单是否都结汇完成
     * @param docNo
     * @return
     */
    Boolean findOrderNoIsSettlement(String docNo);

    /**
     * 根据
     * @param docNo
     * @return
     */
    OverseasReceivingAccount findByDocNo(String docNo);

    /**
     * 出口一票到底台账
     * @param orderId
     * @return
     */
    List<ExpOneVoteTheEndVO> oneVoteTheEndByOrderId(Long orderId);
}
