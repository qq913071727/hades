package com.tsfyun.scm.vo.order;

import com.tsfyun.scm.vo.finance.PaymentOrderCostVO;
import com.tsfyun.scm.vo.system.SubjectBankVO;
import com.tsfyun.scm.vo.system.SubjectSimpleVO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AdviceReceiptVO implements Serializable {

    private SubjectSimpleVO subject;//公司信息
    private ImpOrderDetailVO order;//订单信息
    private List<ImpOrderMemberVO> members; //产品明细
    private List<ImpOrderCostVO> costs;//费用
    private List<SubjectBankVO> banks;//银行信息
    List<PaymentOrderCostVO> payments;//付汇信息
}
