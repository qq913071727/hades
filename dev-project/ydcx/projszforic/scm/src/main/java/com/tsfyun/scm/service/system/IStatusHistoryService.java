package com.tsfyun.scm.service.system;

import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.scm.entity.system.StatusHistory;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.StatusHistoryVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-03
 */
public interface IStatusHistoryService<T> extends IService<StatusHistory> {

    void saveHistory(DomainOprationEnum opration, String domainId, String domainType, String nowStatusId, String nowStatusName);
    void saveHistory(DomainOprationEnum opration, String domainId,String domainType,String nowStatusId,String nowStatusName,String memo);
    void saveHistory(DomainOprationEnum opration, String domainId,String domainType,String nowStatusId,String nowStatusName,String memo,String operator);
    void saveHistory(DomainOprationEnum opration, String domainId,String domainType,String historyStatusId,String historyStatusName,String nowStatusId,String nowStatusName,String memo);
    void saveHistory(DomainOprationEnum opration, String domainId,String domainType,String historyStatusId,String historyStatusName,String nowStatusId,String nowStatusName,String memo,String operator);
    //根据单据删除
    void removeHistory(String domainId,String domainType);

    List<StatusHistoryVO> findByDomainId(String domainId);

    /**
     * 根据单据和状态获取最新的一条数据
     * @param domainId
     * @param domainType
     * @param nowStatusId
     * @return
     */
    StatusHistoryVO findRecentByDomainAndNowStatus(String domainId,String domainType,String nowStatusId);

    StatusHistoryVO findRecentByDomainAndHistoryStatus(String domainId,String domainType,String historyStatusId);

    String findInfoByDomainAndNowStatus(String domainId,String domainType,String nowStatusId);

    /**
     * 批量根据单据和状态获取最新的一条数据
     * @param domainIds
     * @param domainType
     * @param nowStatusId
     * @return
     */
    List<StatusHistoryVO> findRecentByDomainAndNowStatusBatch(List<String> domainIds,String domainType,String nowStatusId);

    /**
     * 根据单据ID和操作节点获取操作人
     * @param domainId
     * @param opration
     * @return
     */
    String obtainOperator(String domainId,DomainOprationEnum opration);
}
