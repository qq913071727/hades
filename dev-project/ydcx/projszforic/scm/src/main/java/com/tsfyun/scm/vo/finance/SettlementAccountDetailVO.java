package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/15 10:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettlementAccountDetailVO implements Serializable {

    // 结汇主单
    private SettlementAccountVO settlementAccount;
    // 收款单
    List<OverseasReceivingAccountVO> overseasReceivingAccountList;

}
