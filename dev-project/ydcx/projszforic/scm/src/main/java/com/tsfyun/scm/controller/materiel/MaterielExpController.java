package com.tsfyun.scm.controller.materiel;


import com.sun.org.apache.regexp.internal.RE;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.common.base.vo.ImpExcelVO;
import com.tsfyun.scm.dto.materiel.*;
import com.tsfyun.scm.service.materiel.IMaterielExpService;
import com.tsfyun.scm.vo.materiel.MaterielExpHSVO;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2021-03-08
 */
@RestController
@RequestMapping("/materielExp")
public class MaterielExpController extends BaseController {

    @Autowired
    private IMaterielExpService materielExpService;
    /**=
     * 查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<MaterielExpHSVO>> list(@ModelAttribute @Valid MaterielExpQTO qto){
        return materielExpService.pageList(qto);
    }

    /**=
     * 物料导入
     * @param file
     * @return
     */
    @PostMapping(value = "import")
    Result<ImpExcelVO> importMateriel(@RequestPart(value = "file") MultipartFile file){
        return success(materielExpService.importMateriel(file));
    }

    /**=
     * 归类物料导入
     * @param file
     * @return
     */
    @PostMapping(value = "importClassify")
    Result<ImpExcelVO> importClassify(@RequestPart(value = "file") MultipartFile file){
        return success(materielExpService.importClassifyMateriel(file));
    }

    /**=
     * 归类详情
     * @return
     */
    @PostMapping(value = "classifyDetail")
    Result<Map<String,Object>> classifyDetail(@RequestParam(name = "id")String id){
        return success(materielExpService.classifyDetail(Arrays.asList(id.split(","))));
    }

    /**=
     * 保存归类
     * @return
     */
    @PostMapping(value = "saveClassify")
    Result<Void> saveClassify(@ModelAttribute @Valid ClassifyMaterielExpDTO dto){
        materielExpService.saveClassify(dto);
        return success();
    }

    /**=
     * 模糊查询0
     * @param qto
     * @return
     */
    @PostMapping(value = "vague")
    Result<List<SimpleMaterielVO>> vague(@ModelAttribute @Valid ExpMaterielQTO qto){
        qto.setLimit(10);
        qto.setStatusId(MaterielStatusEnum.CLASSED.getCode());
        return success(materielExpService.vague(qto));
    }

    /**
     * 删除物流
     * @param id
     * @return
     */
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")String id){
        materielExpService.removeMateriel(id);
        return success();
    }
}

