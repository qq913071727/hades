package com.tsfyun.scm.dto.customs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 报关行请求实体
 * </p>
 *
 *
 * @since 2021-11-15
 */
@Data
@ApiModel(value="DeclarationBroker请求对象", description="报关行请求实体")
public class DeclarationBrokerDTO implements Serializable {

   private static final long serialVersionUID=1L;

    @NotNull(message = "id不能为空",groups = {UpdateGroup.class})
    private Long id;

   @NotEmptyTrim(message = "统一信用代码不能为空")
   @LengthTrim(min = 18,max = 18,message = "统一信用代码只能为18位")
   @ApiModelProperty(value = "统一信用代码")
   private String socialNo;

   @NotEmptyTrim(message = "注册海关编码不能为空")
   @LengthTrim(min = 10,max = 10,message = "注册海关编码最大长度只能为10位")
   @ApiModelProperty(value = "注册海关编码")
   private String customsNo;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @NotEmptyTrim(message = "名称不能为空")
   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;


}
