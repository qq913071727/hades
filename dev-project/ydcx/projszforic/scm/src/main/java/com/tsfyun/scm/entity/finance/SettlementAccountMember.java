package com.tsfyun.scm.entity.finance;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <p>
 * 结汇明细
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Data
public class SettlementAccountMember implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 结汇主单
     */

    private Long settlementId;

    /**
     * 收款单ID
     */

    private Long accountId;

    /**
     * 收款单号
     */

    private String accountNo;

    /**
     * 结汇金额（人民币）
     */
    private BigDecimal settlementValue;


}
