package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;

@Data
public class DepartmentVO implements Serializable {

    /**
     * 部门id
     */
    private Long id;

    /**
     * 部门编码
     */
    private String code;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 父级部门（保留字段，暂不使用）
     */
    private Long parentId;

    /**
     * 排序字段
     */
    private Integer sort;

}
