package com.tsfyun.scm.controller.order;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.order.IImpOrderCostService;
import com.tsfyun.scm.vo.finance.PaymentCostInfoVO;
import com.tsfyun.scm.vo.order.AdviceReceiptVO;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;
import com.tsfyun.scm.vo.order.OrderCostInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 订单费用 前端控制器
 * </p>
 *
 *
 * @since 2020-04-21
 */
@RestController
@RequestMapping("/impOrderCost")
public class ImpOrderCostController extends BaseController {

    @Autowired
    private IImpOrderCostService impOrderCostService;

    /**=
     * 订单费用信息(确认进口展示)
     */
    @PostMapping("orderCostInfo")
    public Result<OrderCostInfoVO> orderCostInfo(@RequestParam("id")Long id){
        return success(impOrderCostService.orderCostInfo(id));
    }

    /**=
     * 付款单费用信息(确认付款单展示)
     * @param id
     * @return
     */
    @PostMapping("paymentCostInfo")
    public Result<PaymentCostInfoVO> paymentCostInfo(@RequestParam("id")Long id){
        return success(impOrderCostService.paymentCostInfo(id));
    }

    /**=
     * 收款通知
     * @param id
     * @return
     */
    @PostMapping("adviceReceipt")
    public Result<AdviceReceiptVO> adviceReceipt(@RequestParam("id")Long id){
        return success(impOrderCostService.adviceReceipt(id));
    }

    /**
     * 导出收款通知Excel
     * @param id
     * @return
     */
    @GetMapping("exportAdviceReceiptExcel")
    public Result<Long> exportAdviceReceiptExcel(@RequestParam("id")Long id){
        return success(impOrderCostService.exportAdviceReceiptExcel(id));
    }


    /**=
     * 订单所有费用信息
     * @param orderId
     * @return
     */
    @GetMapping("orderCosts")
    public Result<List<ImpOrderCostVO>> getAllCostsByOrderNo(@RequestParam(value = "orderId",required = false)Long orderId,@RequestParam(value = "impOrderNo",required = false)String impOrderNo){
        return success(impOrderCostService.getAllCosts(orderId,impOrderNo));
    }

    /**
     * 校验订单是否可以调整费用并返回素有费用信息
     * @param impOrderNo
     * @return
     */
    @GetMapping("checkGetAdjustCost")
    public Result<List<ImpOrderCostVO>> checkGetAdjustCost(@RequestParam("impOrderNo")String impOrderNo,@RequestParam("getCost")Boolean getCost){
        return success(impOrderCostService.checkGetAdjustCost(impOrderNo,getCost));
    }

    /**
     * 获取具体某一笔订单费用的信息
     * @param id
     * @return
     */
    @GetMapping("getSimpleCostInfo")
    public Result<ImpOrderCostVO> getSimpleCostInfo(@RequestParam("id")Long id){
        return success(impOrderCostService.getSimpleCostInfo(id));
    }

    /**
     * 在线生成pdf
     * @param id
     * @return
     */
    @GetMapping(value = "pdf")
    public Result<Long> pdf(@RequestParam(value = "id")Long id){
        return Result.success(impOrderCostService.createOrderCostPdf(id));
    }

    /**
     * 在线生成excel
     * @param id
     * @return
     */
    @GetMapping(value = "excel")
    public Result<Long> excel(@RequestParam(value = "id")Long id){
        return Result.success(impOrderCostService.createOrderCostExcel(id));
    }

}

