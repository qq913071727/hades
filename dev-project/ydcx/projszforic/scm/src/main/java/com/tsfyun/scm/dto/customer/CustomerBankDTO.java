package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-03
 */
@Data
@ApiModel(value="CustomerBank请求对象", description="请求实体")
public class CustomerBankDTO extends PaginationDto implements Serializable {

     private static final long serialVersionUID=1L;

    //@NotNull(message = "请选择需要修改的数据",groups = UpdateGroup.class)
     private Long id;

     //@NotNull(message = "请选择客户",groups = {AddGroup.class,UpdateGroup.class})
    @ApiModelProperty(value = "客户ID")
    private Long customerId;

     private String customerName;

     @NotEmptyTrim(message = "银行名称不能为空")
     @LengthTrim(max = 50,message = "银行名称长度不能超过50位")
    @ApiModelProperty(value = "银行名称")
    private String name;

    @NotEmptyTrim(message = "银行账号不能为空")
    @LengthTrim(max = 50,message = "银行账号长度不能超过50位")
    @ApiModelProperty(value = "银行账号")
    private String account;

    //@NotEmptyTrim(message = "银行地址不能为空")
    @LengthTrim(max = 255,message = "银行地址长度不能超过255位")
    @ApiModelProperty(value = "银行地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;


    /**
     * 默认银行
     */
    private Boolean isDefault;


}
