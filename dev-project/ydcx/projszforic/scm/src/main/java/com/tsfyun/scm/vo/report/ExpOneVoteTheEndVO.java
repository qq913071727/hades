package com.tsfyun.scm.vo.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ExpOrderStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Data
public class ExpOneVoteTheEndVO implements Serializable {

    private Integer idx;//序号
    private Long orderId;//订单ID
    private String orderNo;//订单号
    private String clientOrderNo;//客户订单号
    private String orderStatus;//订单状态
    private String orderStatusDesc;//订单状态
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date orderDate;//订单日期
    private String customerId;//客户ID
    private String customerName;//客户名称
    private String currencyId;//订单币制
    private String currencyName;//订单币制
    private String quoteDescribe;//代理协议
    private BigDecimal agencyFee;//代理费率
    private BigDecimal minAgencyFee;//最低收费
    private BigDecimal agencyFeeCost;//应收代理费

    private BigDecimal purchaseValue;// 采购合同金额
    private BigDecimal actualPurchaseValue;// 应开发票金额
    private BigDecimal differenceVal;// 产生票差
    private BigDecimal adjustmentVal;// 调整票差
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date invoicingDate;// 开票日期
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date invoiceDate;// 收票日期
    private String invoiceNo;// 发票号码
    private BigDecimal invoiceVal;// 收票金额
    private BigDecimal vatInvVal;// 增票金额
    private BigDecimal taxInvVal;// 增票税额

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date taxDeclareTime;// 退税申报日期
    private BigDecimal taxDeclareAmount;// 退税申报金额
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date taxRefundTime;// 税局退税日期
    private BigDecimal taxRefundAmount;// 税局退税金额
    private Boolean isCorrespondence;// 是否函调
    private String replyStatus;// 是否回函
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date replyDate;// 回函日期

    private String name;//产品名称
    private String model;//产品型号
    private String brand;//产品品牌
    private String hsCode;//海关编码
    private String trr;//退税率
    private BigDecimal unitPrice;// 产品单价
    private BigDecimal quantity;// 数量
    private BigDecimal totalPrice;// 产品金额
    private Integer cartonNum;// 箱数
    private Integer plateNum;// 板数

    private BigDecimal noSettlementValue;// 订单未结汇金额

    private String agentName;// 申报单位
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date decDate;// 通关日期
    private String declarationNo;// 报关单号
    private String ciqEntyPortName;// 通关口岸
    private String cusVoyageNo;// 国际运单号
    private String cusTrafModeName;// 运输方式


    private String conveyanceType;// 订单车型
    private String conveyanceNo;// 大陆车牌
    private String conveyanceNo2;// 香港车牌
    private String transportSupplierName;// 承运公司
    private String shippingCompany;// 发货单位
    private String shippingAddress;// 发货地址
    private String shippingLinkPerson;// 发货联系人
    private String shippingLinkTel;// 发货联系电话
    private String deliveryCompany;// 收货单位
    private String deliveryAddress;// 收货地址
    private String deliveryLinkPerson;// 收货联系人
    private String deliveryLinkTel;// 收货联系电话

    private String receivingNo;// 境外收款单号
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date receivingDate;// 收款日期
    private BigDecimal amountReceived;// 到账金额
    private BigDecimal receivingFee;// 手续费
    private BigDecimal orderClaimValue;// 订单认领金额
    private String receivingCurrency;// 收款币制
    private String settlementNo;// 结汇单号

    private String paymentNo;// 付款单号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date paymentDate;// 付款时间
    private BigDecimal settlementRate;// 结汇汇率
    private BigDecimal paymentValue;// 已付客户结汇金额(CNY)

    private String drawbackNo;// 退税申请单号
    private BigDecimal actualDrawbackValue;// 应退税金额
    private BigDecimal alreadyDrawbackValue;// 已退税金额
    private BigDecimal unpaidDrawbackValue;// 未付退税金额


    private String isCorrespondenceDesc;

    public String getOrderStatusDesc() {
        ExpOrderStatusEnum expOrderStatusEnum = ExpOrderStatusEnum.of(orderStatus);
        return Optional.ofNullable(expOrderStatusEnum).map(ExpOrderStatusEnum::getName).orElse("");
    }

    public String getIsCorrespondenceDesc() {
        return Objects.equals(isCorrespondence,Boolean.TRUE) ? "是" : "否";
    }
}
