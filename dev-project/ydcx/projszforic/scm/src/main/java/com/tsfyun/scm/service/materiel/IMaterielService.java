package com.tsfyun.scm.service.materiel;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.ImpExcelVO;
import com.tsfyun.scm.dto.materiel.ClassifyMaterielDTO;
import com.tsfyun.scm.dto.materiel.MaterielDTO;
import com.tsfyun.scm.dto.materiel.MaterielQTO;
import com.tsfyun.scm.dto.materiel.client.ClientMaterielQTO;
import com.tsfyun.scm.entity.materiel.Materiel;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.excel.ClassifyMaterielExcel;
import com.tsfyun.scm.vo.materiel.MaterielHSVO;
import com.tsfyun.scm.vo.materiel.MaterielVO;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import com.tsfyun.scm.vo.materiel.client.ClientMaterielVO;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-03-22
 */
public interface IMaterielService extends IService<Materiel> {

    //物料信息包含海关编码信息
    Result<List<MaterielHSVO>> pageList(MaterielQTO qto);
    //模糊查询
    List<SimpleMaterielVO> vague(MaterielQTO qto);
    //物料导入
    ImpExcelVO importMateriel(MultipartFile file);
    //已归类物料导入
    ImpExcelVO importClassifyMateriel(MultipartFile file);
//    //批量保存物料
//    Integer batchSaveList(List<Materiel> materielList);

    List<MaterielHSVO> formatMateriel(List<MaterielVO> materielVOList);
    //保存物料，如果存在则直接返回
    MaterielVO obtainMaterialAndSave(MaterielDTO dto);
    //分析新物料数据
    void analysisNewMateriel(List<String> ids);
    //归类详情
    Map<String,Object> classifyDetail(List<String> ids);
    //保存归类
    void saveClassify(ClassifyMaterielDTO dto);
    //保存归类数据
    void saveImportClassifyMateriel(List<ClassifyMaterielExcel> classifyList);
    //根据id删除物料无法删除不提示
    void removeIdsNotPrompt(List<String> ids);

    /**
     * 客户端物料查询
     * @param qto
     * @return
     */
    Result<List<ClientMaterielVO>> clientPage(ClientMaterielQTO qto);


    //模糊查询
    List<SimpleMaterielVO> clientVague(MaterielQTO qto);

    //品牌模糊搜索，返回前10条
    List<String> searchByBrand(String brand);

    //客户端物料导入
    ImpExcelVO clientImportMateriel(MultipartFile file);

}
