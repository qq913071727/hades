package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 境外供应商银行
 * </p>
 *

 * @since 2020-03-12
 */
@Data
@Accessors(chain = true)
public class SupplierBank extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 供应商
     */
    private Long supplierId;

    /**=
     * 银行代码
     */
    private String code;

    /**
     * 银行名称
     */
    private String name;

    /**=
     * 币制
     */
    private String currencyId;

    /**
     * 银行账号
     */
    private String account;

    /**
     * SWIFT CODE
     */
    private String swiftCode;

    /**
     * 银行地址
     */
    private String address;

    /**
     * 禁用标示
     */
    private Boolean disabled;

    /**=
     * 是否默认
     */
    private Boolean isDefault;



}
