package com.tsfyun.scm.vo.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.tenant.TenantOpenVersionEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class SubjectVersionVO implements Serializable {

    private String code;//编码
    private String companyName;//企业名称
    private String domain;//域名
    private String openVersion;//版本
    private String openVersionDesc;//版本描述
    private Integer staffNumber;//员工数
    private Integer useNumber;//已使用
    private String permit;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime invalidTime;//失效时间

    public String getOpenVersionDesc(){
        TenantOpenVersionEnum versionEnum = TenantOpenVersionEnum.of(getOpenVersion());
        return Objects.nonNull(versionEnum)?versionEnum.getName():"";
    }
}
