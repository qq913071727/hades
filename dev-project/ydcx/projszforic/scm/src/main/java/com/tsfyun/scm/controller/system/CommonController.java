package com.tsfyun.scm.controller.system;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Maps;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.system.ISubjectService;
import com.tsfyun.scm.util.EnumUtils;
import com.tsfyun.scm.vo.system.SubjectVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  通用控制器，不登录拦截
 * </p>
 *
 * @since 2020-03-03
 */
@RestController
@RequestMapping("/common")
@Api(tags = "通用")
public class CommonController extends BaseController {

    @Autowired
    private ISubjectService subjectService;

    /**
     * 获取主体信息
     * @return
     */
    @ApiOperation(value = "获取主体信息")
    @GetMapping("subject")
    public Result<SubjectVO> subjectInfo() {
        return success(subjectService.findByCode());
    }

    /**
     * 获取枚举下拉框
     * @param type
     * @return
     */
    @ApiOperation(value = "获取字典下拉")
    @GetMapping("bizSelect")
    public Result<Map<String,List<Map<String,Object>>>> enumSelect(@RequestParam(value = "type")List<String> type) {
        Map<String,List<Map<String,Object>>> typeMaps = Maps.newHashMap();
        Map<String,List<Map<String,Object>>> enumMaps = EnumUtils.getInstance().getData();
        if(CollectionUtil.isNotEmpty(type)) {
            type.stream().forEach(r->{
                typeMaps.put(r,enumMaps.get(r));
            });
        }
        return success(typeMaps);
    }
}
