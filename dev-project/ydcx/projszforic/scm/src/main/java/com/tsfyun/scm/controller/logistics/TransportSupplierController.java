package com.tsfyun.scm.controller.logistics;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.logistics.TransportSupplierDTO;
import com.tsfyun.scm.dto.logistics.TransportSupplierPlusInfoDTO;
import com.tsfyun.scm.dto.logistics.TransportSupplierQTO;
import com.tsfyun.scm.entity.logistics.TransportSupplier;
import com.tsfyun.scm.service.logistics.ITransportSupplierService;
import com.tsfyun.scm.vo.logistics.SimpleTransportSupplierVO;
import com.tsfyun.scm.vo.logistics.TransportSupplierPlusVO;
import com.tsfyun.scm.vo.logistics.TransportSupplierVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  运输供应商前端控制器
 * </p>
 *
 * @since 2020-03-20
 */
@RestController
@RequestMapping("/transportSupplier")
public class TransportSupplierController extends BaseController {

    @Autowired
    private ITransportSupplierService transportSupplierService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 新增运输供应商信息（含运输工具信息）
     * @param dto
     * @return 返回运输供应商id
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Long> add(@RequestBody @Validated(AddGroup.class) TransportSupplierPlusInfoDTO dto) {
        return success(transportSupplierService.addPlus(dto));
    }

    /**
     * 修改运输供应商信息（含运输工具信息）
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated(UpdateGroup.class) TransportSupplierPlusInfoDTO dto) {
        transportSupplierService.editPlus(dto);
        return success();
    }


    /**
     * 运输供应商详情信息，返回运输供应商信息，运输工具信息
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    Result<TransportSupplierPlusVO> info(@RequestParam(value = "id")Long id) {
        return success(transportSupplierService.detailPlus(id));
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        transportSupplierService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 获取运输供应商下拉（过滤掉禁用的）
     * @return
     */
    @GetMapping("select")
    public Result<List<SimpleTransportSupplierVO>> select( ){
        return success(transportSupplierService.select( ));
    }

    /**
     * 简单新增运输供应商信息（不含运输工具信息）
     * @param dto
     * @return 返回运输供应商id
     */
    @PostMapping(value = "new")
    Result<Long> newly(@ModelAttribute @Validated(AddGroup.class) TransportSupplierDTO dto) {
        return success(transportSupplierService.simpleAdd(dto).getId());
    }

    /**
     * 简单修改运输供应商信息（不含运输工具信息）
     * @param dto
     * @return 返回运输供应商id
     */
    @PostMapping(value = "update")
    Result<Void> update(@ModelAttribute @Validated(UpdateGroup.class) TransportSupplierDTO dto) {
        transportSupplierService.simpleEdit(dto);
        return success();
    }

    /**
     * 分页查询运输供应商信息
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<TransportSupplierVO>> page(TransportSupplierQTO qto) {
        PageInfo<TransportSupplier> page = transportSupplierService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),TransportSupplierVO.class));
    }

    /**
     * 删除运输供应商信息（真删，如果存在运输工具信息则不允许删除）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        transportSupplierService.remove(id);
        return success();
    }


}

