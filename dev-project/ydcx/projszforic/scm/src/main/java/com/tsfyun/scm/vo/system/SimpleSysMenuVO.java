package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;

/**
 * 菜单响应实体
 */
@Data
public class SimpleSysMenuVO implements Serializable {

    /**
     * 菜单id
     */
    private String id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单排序
     */
    private int sort;

    /**=
     * 菜单类型
     */
    private Integer type;

    private String parentId;


}
