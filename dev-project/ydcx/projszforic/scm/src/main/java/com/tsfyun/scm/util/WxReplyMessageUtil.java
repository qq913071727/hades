package com.tsfyun.scm.util;
 
import com.alibaba.fastjson.JSONObject;
import com.tsfyun.scm.dto.support.WXReplyMessageDTO;
 
/**
 * 构造微信客服回复消息工具类
 * Created by zhangq on 2019/1/23.
 */
public class WxReplyMessageUtil {

    /**
     * 构造客服文本消息
     * @return 消息内容
     */
    public static JSONObject sendCustomTextMsg(WXReplyMessageDTO msg){
        JSONObject json =new JSONObject();
        String openid = msg.getToUserName();
        String context = msg.getContent();
        json.put("touser", openid);
        json.put("msgtype", "text");
        JSONObject text =new JSONObject();
        text.put("content", context);
        json.put("text", text);
        return json;
    }
 
    /**
     * 构造客服图片消息
     * @return 消息内容
     */
    public static JSONObject sendCustomImageMsg(WXReplyMessageDTO msg){
        JSONObject json =new JSONObject();
        String openid = msg.getToUserName();
        String media_id = msg.getMediaId();
        json.put("touser", openid);
        json.put("msgtype", "image");
        JSONObject media =new JSONObject();
        media.put("media_id", media_id);
        json.put("image", media);
        return json;
    }
 
 
    /**
     * 构造客服语音消息
     * @return 消息内容
     */
    public static JSONObject sendCustomVoiceMsg(WXReplyMessageDTO msg){
        JSONObject json =new JSONObject();
        String openid = msg.getToUserName();
        String media_id = msg.getMediaId();
        json.put("touser", openid);
        json.put("msgtype", "voice");
        JSONObject voice =new JSONObject();
        voice.put("media_id", media_id);
        json.put("voice", voice);
        return json;
    }
}