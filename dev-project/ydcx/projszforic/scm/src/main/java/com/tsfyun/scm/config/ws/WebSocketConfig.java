package com.tsfyun.scm.config.ws;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        //handler是webSocket的核心，配置入口，连接参数配置在路径中
        webSocketHandlerRegistry.addHandler(new WsHandler(), "/websocket").setAllowedOrigins("*").addInterceptors(new WebSocketInterceptor());
        webSocketHandlerRegistry.addHandler(new WsHandler(),"/sock-js").setAllowedOrigins("*").addInterceptors(new WebSocketInterceptor());
    }

}
