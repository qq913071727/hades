package com.tsfyun.scm.controller.customer;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.dto.customer.AgreemenPlusDTO;
import com.tsfyun.scm.dto.customer.AgreementQTO;
import com.tsfyun.scm.dto.customer.SubstituteAgreementDTO;
import com.tsfyun.scm.service.customer.IAgreementService;
import com.tsfyun.scm.vo.customer.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;
import javax.validation.Valid;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 协议报价单 前端控制器
 * </p>
 *
 *
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/agreement")
@Slf4j
public class AgreementController extends BaseController {

    private final IAgreementService agreementService;

    public AgreementController(IAgreementService agreementService) {
        this.agreementService = agreementService;
    }

    /**
     * 分页查询（带权限）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<AgreementVO>> pageList(@ModelAttribute @Valid  AgreementQTO qto) {
        PageInfo<AgreementVO> pageInfo = agreementService.list(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Void> add(@RequestBody @Validated AgreemenPlusDTO dto) {
        agreementService.addPlus(dto);
        return success();
    }

    @DuplicateSubmit
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated AgreemenPlusDTO dto) {
        agreementService.editPlus(dto);
        return success();
    }

    /**
     * 获取详情（含条款/报价）
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    public Result<AgreementDetailPlusVO> info(@RequestParam(value = "id")Long id,@RequestParam(value = "operation",required = false)String operation) {
        return success(agreementService.detailPlus(id,operation));
    }

    /**=
     * 删除
     * @return
     */
    @PostMapping(value = "delete")
    public Result<Void> delete(@RequestParam(value = "id")Long id){
        agreementService.delete(id);
        return success();
    }

    /**=
     * 审核
     * @return
     */
    @PostMapping(value = "examine")
    public Result<Void> examine(@ModelAttribute @Valid TaskDTO dto){
        agreementService.examine(dto);
        return success();
    }

    /**=
     * 原件签回
     * @param id
     * @return
     */
    @PostMapping(value = "originSignBack")
    public Result<Void> originSignBack(@RequestParam(value = "id")String id){
        List<Long> ids = StringUtils.stringToLongs(id);
        agreementService.originSignBack(ids);
        return success();
    }

    /**=
     * 进口下单根据客户获取有效协议报价
     * @param customerId
     * @return
     */
    @PostMapping(value = "impAgreement")
    public Result<List<ImpAgreementVO>> impAgreement(@RequestParam(value = "customerId")Long customerId){
        return success(agreementService.impAgreementCheck(customerId));
    }

    /**
     * 代客户确认
     * @param dto
     * @return
     */
    @PostMapping(value = "substituteCustomerSign")
    public Result<Void> substituteCustomerSign(@ModelAttribute @Validated SubstituteAgreementDTO dto){
        agreementService.substituteCustomerSign(dto);
        return success();
    }

    /**
     * 导出审批表
     * @param id
     */
    @GetMapping(value = "exportExcel")
    public void exportApprovalForm(@RequestParam(value = "id")Long id){
        HSSFWorkbook workbook;
        OutputStream outputStream = null;
        try {
            workbook = agreementService.exportApprovalForm(id);
            String contentType = "application/ms-excel;charset=UTF-8";
            response.setHeader("Content-disposition","attachment;filename=" + URLEncoder.encode( "进口客户协议审批表" + ".xls", "UTF-8"));
            response.setContentType(contentType);
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
        } catch (Exception e) {
            log.error("导出异常",e);
        } finally {
            if(Objects.nonNull(outputStream)) {
                IOUtils.closeQuietly(outputStream);
            }
        }
    }

    /**
     * 获取打印审批表数据
     * @param id
     * @return
     */
    @GetMapping(value = "getPrintApproval")
    public Result<AgreementApprovalVO> getPrintApproval(@RequestParam(value = "id")Long id) {
        return success(agreementService.getPrintApproval(id));
    }
}

