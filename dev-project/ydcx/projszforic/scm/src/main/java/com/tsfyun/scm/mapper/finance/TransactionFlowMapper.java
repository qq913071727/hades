package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.finance.client.ClientTransactionFlowQTO;
import com.tsfyun.scm.entity.finance.TransactionFlow;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.TransactionFlowVO;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 交易流水 Mapper 接口
 * </p>
 *

 * @since 2020-04-16
 */
@Repository
public interface TransactionFlowMapper extends Mapper<TransactionFlow> {

    /**
     * 根据客户id查询客户余额
     * @param customerId
     * @return
     */
    BigDecimal getBalance(Long customerId);

    /**
     * 查询列表
     * @param params
     * @return
     */
    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<TransactionFlowVO> list(Map<String,Object> params);

    /**
     * 客户端交易流水列表
     * @param qto
     * @return
     */
    List<TransactionFlowVO> clientList(ClientTransactionFlowQTO qto);

    /**
     * 客户端交易流水汇总
     * @param qto
     * @return
     */
    Map<String,BigDecimal> sumInOutcome(ClientTransactionFlowQTO qto);

}
