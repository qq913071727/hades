package com.tsfyun.scm.util;

import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;

import java.time.LocalDateTime;
import java.time.Period;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/25 09:43
 */
public class CheckUtil {

    /**
     * 校验时间范围
     * @param start
     * @param end
     */
    public static void checkTime(LocalDateTime start, LocalDateTime end) {
        //开始时间不能大于结束时间,并且时间范围不能超过1年
        if (start != null && end != null) {
            TsfPreconditions.checkArgument((start.isBefore(end) || start.equals(end))&&
                            Math.abs(Period.between(end.toLocalDate(), start.toLocalDate()).getYears()) <= 1,
                    new ServiceException("开始时间大于结束时间或者时间范围超过限制"));
        }
    }


}
