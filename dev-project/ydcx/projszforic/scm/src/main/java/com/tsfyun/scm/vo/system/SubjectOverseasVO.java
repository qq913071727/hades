package com.tsfyun.scm.vo.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Data
@ApiModel(value="SubjectOverseas响应对象", description="响应实体")
public class SubjectOverseasVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long Id;

    private Long subjectId;

    @ApiModelProperty(value = "英文名称")
    private String name;

    @ApiModelProperty(value = "中文名称")
    private String nameCn;

    @ApiModelProperty(value = "英文地址")
    private String address;

    @ApiModelProperty(value = "中文地址")
    private String addressCn;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "默认")
    private Boolean isDefault;

    @ApiModelProperty(value = "登记时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "传真")
    private String fax;

    @ApiModelProperty(value = "电话")
    private String tel;

    @ApiModelProperty(value = "电子章")
    private String chapter;

}
