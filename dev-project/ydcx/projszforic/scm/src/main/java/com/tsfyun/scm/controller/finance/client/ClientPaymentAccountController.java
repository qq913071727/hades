package com.tsfyun.scm.controller.finance.client;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.finance.ClientPaymentApplyQTO;
import com.tsfyun.scm.dto.finance.PaymentApplyDTO;
import com.tsfyun.scm.service.finance.IPaymentAccountService;
import com.tsfyun.scm.service.order.IImpOrderPaymentService;
import com.tsfyun.scm.vo.finance.EditPaymentPlusVO;
import com.tsfyun.scm.vo.finance.client.*;
import com.tsfyun.scm.vo.order.ApplyPaymentPlusVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 客户端付款单
 * @CreateDate: Created in 2020/11/3 10:24
 */
@RestController
@RequestMapping("/client/payment")
@Api(tags = "付汇（客户端）")
public class ClientPaymentAccountController extends BaseController {

    @Autowired
    private IPaymentAccountService paymentAccountService;

    @Autowired
    private IImpOrderPaymentService impOrderPaymentService;


    /**
     * 查询客户付汇分页信息
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    @ApiOperation("分页")
    Result<List<ClientPaymentListVO>> page(@ModelAttribute @Valid ClientPaymentApplyQTO qto) {
        PageInfo<ClientPaymentListVO> page = paymentAccountService.clientPageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 获取客户付汇状态数量
     * @return
     */
    @ApiOperation(value = "我的付汇单数")
    @PostMapping(value = "myPayStatistic")
    public Result<Map<String,Long>> myPayStatistic( ) {
        return success(paymentAccountService.getClientPaymentNum( ));
    }

    /**
     * 付汇详情
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    @ApiOperation("详情")
    Result<ClientPaymentDetailPlusVO> detail(@RequestParam(value = "id")Long id) {
        return success( paymentAccountService.clientPaymentInfo(id));
    }

    /**=
     * 初始化付款单修改数据
     * @param id
     * @return
     */
    @GetMapping(value = "initEdit")
    public Result<EditPaymentPlusVO> initEdit(@RequestParam(value = "id")Long id){
        return success(paymentAccountService.initEdit(id));
    }

    /**=
     * 客户端保存付汇修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    @DuplicateSubmit
    public Result<Void> edit(@RequestBody @Valid PaymentApplyDTO dto){
        paymentAccountService.clientEdit(dto);
        return success();
    }

    /**=
     * 客户端初始化订单付汇申请
     * @param id
     * @return
     */
    @PostMapping(value = "initApply")
    public Result<ApplyPaymentPlusVO> initApply(@RequestParam(value = "id")String id){
        List<Long> ids =  Arrays.asList(id.split(",")).stream().map(Long::valueOf).collect(Collectors.toList());
        return success(impOrderPaymentService.clientInitApply(ids));
    }

    /**=
     * 客户端新增付汇申请
     * @param dto
     * @return
     */
    @PostMapping(value = "apply")
    @DuplicateSubmit
    public Result<Void> apply(@RequestBody @Valid PaymentApplyDTO dto){
        paymentAccountService.clientApply(dto);
        return success();
    }

}
