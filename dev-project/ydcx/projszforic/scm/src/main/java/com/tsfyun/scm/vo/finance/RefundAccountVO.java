package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.RefundAccountStatusEnum;
import com.tsfyun.scm.util.ClientInfoStatus;
import com.tsfyun.scm.util.ClientStatusMappingUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 退款申请响应实体
 * </p>
 *
 *
 * @since 2020-11-27
 */
@Data
@ApiModel(value="RefundAccount响应对象", description="退款申请响应实体")
public class RefundAccountVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "收款账户名称")
    private String payeeAccountName;

    @ApiModelProperty(value = "收款银行名称")
    private String payeeBankName;

    @ApiModelProperty(value = "收款银行账号")
    private String payeeBankAccountNo;

    @ApiModelProperty(value = "收款银行地址")
    private String payeeBankAddress;

    @ApiModelProperty(value = "付款银行id")
    private String payerBankId;

    @ApiModelProperty(value = "付款银行名称")
    private String payerBankName;

    @ApiModelProperty(value = "付款银行账号")
    private String payerBankAccountNo;

    @ApiModelProperty(value = "申请退款原因")
    private String memo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "实际退款时间")
    private LocalDateTime accountDate;

    @ApiModelProperty(value = "审批原因")
    private String approvalInfo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateCreated;

    private String statusDesc;

    private String clientStatusId;

    private String clientStatusDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(RefundAccountStatusEnum.of(statusId)).map(RefundAccountStatusEnum::getName).orElse("");
    }

    public String getClientStatusId() {
        return ClientStatusMappingUtil.getClientRefundStatus(statusId);
    }

    public String getClientStatusDesc() {
        ClientInfoStatus clientInfoStatus = ClientStatusMappingUtil.REFUND_CLIENT_STATUS_MAPPING.get(getClientStatusId());
        return Optional.ofNullable(clientInfoStatus).map(ClientInfoStatus::getClientStatusDesc).orElse("");
    }


}
