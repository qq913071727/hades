package com.tsfyun.scm.vo.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientFileVO implements Serializable {

    private Long id;
    private String extension;
}
