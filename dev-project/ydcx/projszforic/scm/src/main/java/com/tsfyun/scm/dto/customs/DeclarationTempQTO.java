package com.tsfyun.scm.dto.customs;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:报关模板查询请求实体
 * @since Created in 2020/4/26 09:45
 */
@Data
public class DeclarationTempQTO extends PaginationDto implements Serializable {


    private String billType;

    private String tempName;

}
