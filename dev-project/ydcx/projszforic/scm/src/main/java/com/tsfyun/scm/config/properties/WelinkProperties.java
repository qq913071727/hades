package com.tsfyun.scm.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@RefreshScope
@Data
@Component
@ConfigurationProperties(prefix = WelinkProperties.PREFIX)
public class WelinkProperties {

    public static final String PREFIX = "welink.sms";
    private String sname;
    private String spwd;
    private String scorpid;
    private String sprdid;
    private String signName;

    private String domain = "http://cf.51welink.com/submitdata/Service.asmx/g_Submit";
}
