package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 内部付款申请单数据
 * @CreateDate: Created in 2021/11/5 14:16
 */
@Data
public class PrintExpPaymentAccountVO implements Serializable {

    /**
     * 收款单位
     */

    private String payeeName;

    /**
     * 要求付款时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date claimPaymentDate;


    /**
     * 付款金额
     */

    private BigDecimal accountValue;

    /**
     * 付款金额大写
     */

    private String accountValueCn;

    /**
     * 结汇金额（外币）
     */

    private BigDecimal settlementValue;

    /**
     * 结汇币制
     */

    private String currencyCode;

    /**
     * 结汇汇率
     */

    private BigDecimal settlementRate;

    /**
     * 收款银行
     */

    private String payeeBank;

    /**
     * 收款银行账号
     */

    private String payeeBankNo;


    /**
     * 备注 订单和结汇金额
     */
    private String remark;

    /**
     * 申请人
     */
    private String applyUser;

    /**
     * 部门复核人
     */
    private String reviewUser;

}
