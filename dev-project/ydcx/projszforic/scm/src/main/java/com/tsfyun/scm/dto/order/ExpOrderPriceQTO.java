package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;
@Data
public class ExpOrderPriceQTO extends PaginationDto implements Serializable {

    /**
     * 状态
     */
    private String statusId;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 客户名称
     */
    private String customerName;

}
