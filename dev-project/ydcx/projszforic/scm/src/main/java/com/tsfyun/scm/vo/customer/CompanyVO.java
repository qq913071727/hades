package com.tsfyun.scm.vo.customer;

import com.tsfyun.common.base.enums.domain.CustomerStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * 公司信息(客户端调用)
 */
@ApiModel(value = "公司信息")
@Data
public class CompanyVO implements Serializable {

    private Long id;
    /**
     * 客户代码
     */
    @ApiModelProperty(value = "客户代码")
    private String code;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String name;

    /**
     * 统一社会信用代码(18位)
     */
    @ApiModelProperty(value = "统一社会信用代码")
    private String socialNo;
    /**
     * 税务登记证
     */
    @ApiModelProperty(value = "税务登记证")
    private String taxpayerNo;
    /**
     * 海关注册编码
     */
    @ApiModelProperty(value = "海关注册编码")
    private String customsCode;
    /**
     * 公司法人
     */
    @ApiModelProperty(value = "公司法人")
    private String legalPerson;
    /**
     * 公司电话
     */
    @ApiModelProperty(value = "公司电话")
    private String tel;
    /**
     * 公司传真
     */
    @ApiModelProperty(value = "公司传真")
    private String fax;
    /**
     * 公司地址
     */
    @ApiModelProperty(value = "公司地址")
    private String address;
    /**
     * 状态编码
     */
    @ApiModelProperty(value = "状态编码")
    private String statusId;
    /**
     * 状态名称
     */
    @ApiModelProperty(value = "状态名称")
    private String statusName;
    public String getStatusName(){
        CustomerStatusEnum statusEnum = CustomerStatusEnum.of(getStatusId());
        return Objects.nonNull(statusEnum)?statusEnum.getName():"";
    }

    /**
     * 审核意见
     */
    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;
    /**
     * 开户银行
     */
    @ApiModelProperty(value = "开户银行")
    private String invoiceBankName;

    /**
     * 银行帐号
     */
    @ApiModelProperty(value = "银行帐号")
    private String invoiceBankAccount;

    /**
     * 开票地址
     */
    @ApiModelProperty(value = "开票地址")
    private String invoiceBankAddress;

    /**=
     * 开票要求备注
     */
    @ApiModelProperty(value = "开票要求备注")
    private String invoiceMemo;

    /**
     * 开票电话
     */
    @ApiModelProperty(value = "开票电话")
    private String invoiceBankTel;

    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人")
    private String linkPerson;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    private String linkTel;

    @ApiModelProperty(value = "联系省份")
    private String invoiceProvince;
    @ApiModelProperty(value = "联系市")
    private String invoiceCity;
    @ApiModelProperty(value = "联系区")
    private String invoiceArea;

    /**
     * 联系地址
     */
    @ApiModelProperty(value = "联系地址")
    private String linkAddress;
}
