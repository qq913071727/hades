package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.ExpConfirmBankDTO;
import com.tsfyun.scm.dto.finance.ExpPaymentAccountDTO;
import com.tsfyun.scm.dto.finance.ExpPaymentAccountQTO;
import com.tsfyun.scm.dto.finance.ExpPaymentCompletedDTO;
import com.tsfyun.scm.entity.finance.ExpPaymentAccount;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.ExpPaymentAccountPlusVO;
import com.tsfyun.scm.vo.finance.ExpPaymentAccountVO;
import com.tsfyun.scm.vo.finance.PrintExpPaymentAccountVO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;

import java.util.List;

/**
 * <p>
 * 出口付款单 服务类
 * </p>
 *
 *
 * @since 2021-10-18
 */
public interface IExpPaymentAccountService extends IService<ExpPaymentAccount> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<ExpPaymentAccountVO> pageList(ExpPaymentAccountQTO qto);

    /**
     * 详情
     * @param id
     * @param operation
     * @return
     */
    ExpPaymentAccountPlusVO detail(Long id, String operation);

    /**
     * 保存付款单
     * @param dto
     */
    void savePaymentAccount(ExpPaymentAccountDTO dto);

    /**
     * 删除付款单
     * @param id
     */
    void removePaymentAccount(Long id);

    /**
     * 付款单确认
     * @param dto
     */
    void confirm(TaskDTO dto);

    /**
     * 退回商务确认
     * @param dto
     */
    void backConfirm(TaskDTO dto);

    /**
     * 退回重新确定付款行
     * @param dto
     */
    void backConfirmBank(TaskDTO dto);

    /**
     * 确定付款银行
     * @param dto
     */
    void confirmBank(ExpConfirmBankDTO dto);

    /**
     * 确定付款完成
     * @param dto
     */
    void paymentCompleted(ExpPaymentCompletedDTO dto);

    /**
     * 获取打印付款申请单数据
     * @param id
     * @return
     */
    PrintExpPaymentAccountVO getPrintExpPaymentAccountData(Long id);

    /**
     * 出口一票到底台账
     * @param orderId
     * @return
     */
    List<ExpOneVoteTheEndVO> oneVoteTheEndByOrderId(Long orderId);
}
