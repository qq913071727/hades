package com.tsfyun.scm.dto.materiel.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @Description: 客户端物料查询请求实体
 * @CreateDate: Created in 2020/10/12 11:18
 */
@Data
public class ClientMaterielQTO extends PaginationDto {

    /**
     * 状态
     */

    private String statusId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 海关编码
     */

    private String hsCode;

    /**
     * 品牌、名称、型号、海关编码
     */
    private String keyword;

    /**
     * 客户
     */
    private Long customerId;

    /**
     * 是否有关税
     */
    private Boolean hasTariff;

    /**
     * 关税大于0的海关编码
     */
    @JsonIgnore
    @JSONField(serialize = false)
    private List<String> tariffHsCodes;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateCreateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateCreateEnd;

    public void setDateCreateEnd(LocalDateTime dateCreateEnd) {
        if(dateCreateEnd != null){
            this.dateCreateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateCreateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
