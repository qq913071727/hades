package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.ExpConfirmBankDTO;
import com.tsfyun.scm.dto.finance.ExpPaymentAccountDTO;
import com.tsfyun.scm.dto.finance.ExpPaymentAccountQTO;
import com.tsfyun.scm.dto.finance.ExpPaymentCompletedDTO;
import com.tsfyun.scm.service.finance.IExpPaymentAccountService;
import com.tsfyun.scm.vo.finance.ExpPaymentAccountPlusVO;
import com.tsfyun.scm.vo.finance.ExpPaymentAccountVO;
import com.tsfyun.scm.vo.finance.PrintExpPaymentAccountVO;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 出口付款单 前端控制器
 * </p>
 *
 *
 * @since 2021-10-18
 */
@RestController
@RequestMapping("/expPaymentAccount")
public class ExpPaymentAccountController extends BaseController {

    private final IExpPaymentAccountService expPaymentAccountService;

    public ExpPaymentAccountController(IExpPaymentAccountService expPaymentAccountService) {
        this.expPaymentAccountService = expPaymentAccountService;
    }

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ExpPaymentAccountVO>> pageList(@ModelAttribute @Valid ExpPaymentAccountQTO qto) {
        PageInfo<ExpPaymentAccountVO> page = expPaymentAccountService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<ExpPaymentAccountPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(expPaymentAccountService.detail(id,operation));
    }

    /**
     * 保存付款单
     * @return
     */
    @PostMapping(value = "save")
    @DuplicateSubmit
    public Result<Void> save(@RequestBody @Valid ExpPaymentAccountDTO dto){
        expPaymentAccountService.savePaymentAccount(dto);
        return success();
    }

    /**
     * 删除
     * @return
     */
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")Long id){
        expPaymentAccountService.removePaymentAccount(id);
        return success();
    }

    /**
     * 付款单确认
     * @return
     */
    @PostMapping(value = "confirm")
    @DuplicateSubmit
    public Result<Void> confirm(@ModelAttribute @Valid TaskDTO dto){
        expPaymentAccountService.confirm(dto);
        return success();
    }

    /**
     * 退回付款单确认
     * @return
     */
    @PostMapping(value = "backConfirm")
    @DuplicateSubmit
    public Result<Void> backConfirm(@ModelAttribute @Valid TaskDTO dto){
        expPaymentAccountService.backConfirm(dto);
        return success();
    }

    /**
     * 退回重新确定付款行
     * @return
     */
    @PostMapping(value = "backConfirmBank")
    @DuplicateSubmit
    public Result<Void> backConfirmBank(@ModelAttribute @Valid TaskDTO dto){
        expPaymentAccountService.backConfirmBank(dto);
        return success();
    }

    /**
     * 确定付款银行
     * @param dto
     * @return
     */
    @PostMapping(value = "confirmBank")
    @DuplicateSubmit
    public Result<Void> confirmBank(@ModelAttribute @Valid ExpConfirmBankDTO dto){
        expPaymentAccountService.confirmBank(dto);
        return success();
    }

    /**
     * 财务确定付款完成
     * @param dto
     * @return
     */
    @PostMapping(value = "paymentCompleted")
    @DuplicateSubmit
    public Result<Void> paymentCompleted(@ModelAttribute @Valid ExpPaymentCompletedDTO dto){
        expPaymentAccountService.paymentCompleted(dto);
        return success();
    }

    /**
     * 获取付款申请打印数据
     * @param id
     * @return
     */
    @GetMapping(value = "getPrintExpPaymentAccountData")
    public Result<PrintExpPaymentAccountVO> getPrintExpPaymentAccountData(@RequestParam(value = "id")Long id){
        return success(expPaymentAccountService.getPrintExpPaymentAccountData(id));
    }
}

