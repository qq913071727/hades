package com.tsfyun.scm.service.impl.customs;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.customs.LogSingleWindow;
import com.tsfyun.scm.mapper.customs.LogSingleWindowMapper;
import com.tsfyun.scm.service.customs.ILogSingleWindowService;
import com.tsfyun.common.base.extension.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 单一窗口日志 服务实现类
 * </p>
 *

 * @since 2020-05-07
 */
@Service
public class LogSingleWindowServiceImpl extends ServiceImpl<LogSingleWindow> implements ILogSingleWindowService {

    @Autowired
    private LogSingleWindowMapper logSingleWindowMapper;

    @Override
    public List<LogSingleWindow> singleWindowList(String domainId, String domainType) {
        LogSingleWindow query = new LogSingleWindow();
        query.setDomainId(domainId);
        query.setDomainType(domainType);
        List<LogSingleWindow> list = logSingleWindowMapper.select(query);
        if(CollUtil.isNotEmpty(list)){
            list = list.stream().sorted(Comparator.comparing(LogSingleWindow::getDateCreated).reversed()).collect(toList());
        }
        return list;
    }
}
