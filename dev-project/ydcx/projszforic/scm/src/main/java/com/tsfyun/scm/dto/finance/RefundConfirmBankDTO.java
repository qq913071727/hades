package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.TaskDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 退款申请确认付款行
 * @CreateDate: Created in 2020/11/27 16:01
 */
@Data
public class RefundConfirmBankDTO extends TaskDTO implements Serializable {

    //@NotNull(message = "请选择付款银行")
    @LengthTrim(max = 20,message = "付款银行id最大长度不能超过20位")
    @ApiModelProperty(value = "付款银行id")
    private Long payerBankId;

    //@NotEmptyTrim(message = "付款银行名称不能为空")
    /*
    @LengthTrim(max = 50,message = "付款银行名称最大长度不能超过50位")
    @ApiModelProperty(value = "付款银行名称")
    private String payerBankName;

    //@NotEmptyTrim(message = "付款银行账号不能为空")
    @LengthTrim(max = 50,message = "付款银行账号最大长度不能超过50位")
    @ApiModelProperty(value = "付款银行账号")
    private String payerBankAccountNo;
     */


}
