package com.tsfyun.scm.service.system;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IValidateCodeService {

    /**
     * 获取验证码
     * @return
     */
    Map getCode(HttpServletRequest request);

    /**
     * 移除验证码
     */
    void remove();

    /**
     * 校验验证码
     */
    void validate(HttpServletRequest request);
}
