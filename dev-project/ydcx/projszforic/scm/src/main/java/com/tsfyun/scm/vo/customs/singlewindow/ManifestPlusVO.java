package com.tsfyun.scm.vo.customs.singlewindow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/25 17:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManifestPlusVO implements Serializable {

    private ManifestVO manifestVO;

    private List<MftBillVO> mftBillVOList;

    private List<MtfGoodVO> mtfGoodVOList;

    private List<MftContainerVO> mftContainerVOList;
}
