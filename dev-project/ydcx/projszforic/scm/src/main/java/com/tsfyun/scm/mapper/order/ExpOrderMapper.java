package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.order.ClaimedAmountDTO;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.OverseasReceivingOrderVO;
import com.tsfyun.scm.vo.order.*;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出口订单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Repository
public interface ExpOrderMapper extends Mapper<ExpOrder> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ExpOrderListVO> list(Map<String,Object> params);

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ExpOneVoteTheEndVO> oneVoteTheEnd(Map<String,Object> params);

    ExpOrderVO detail(@Param(value = "id")Long id);

    /**
     * 获取跨境运输单已经绑定的订单
     * @param transportNo
     * @return
     */
    List<BindOrderVO> listBinded(@Param(value = "transportNo")String transportNo);

    /**
     * 置空订单中的运输单号
     * @param id
     */
    void resetTransportNo(@Param(value = "id")Long id);
    /**
     * 获取跨境运输单可以绑定的订单
     * @param isCharterCar
     * @return
     */
    List<BindOrderVO> listCanBind(@Param(value = "isCharterCar")Boolean isCharterCar);

    /**
     * 根据订单id修改绑定的跨境运输单号
     * @param id
     * @param transportNo
     * @return
     */
    int updateTransportNo(@Param(value = "id")Long id,@Param(value = "transportNo")String transportNo);

    //根据订单号模糊搜索可以操作的订单，返回20条
    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ExpOrderAdjustCostVO> selectAdjustOrder(Map<String,Object> params);

    /**
     * 根据采购合同修改订单
     * @param order
     */
    void updateByPurchaseContract(ExpOrder order);

    /**
     * 收款单可认领金额
     * @param dto
     * @return
     */
    List<OverseasReceivingOrderVO> collectionClaimedAmount(ClaimedAmountDTO dto);

    /**
     * 单量统计
     * @param startDate
     * @param endDate
     * @return
     */
    Integer totalSingleQuantity(@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate);

    /**
     * 金额统计(USD)
     * @param startDate
     * @param endDate
     * @return
     */
    BigDecimal totalAmountMoney(@Param(value = "startDate")Date startDate, @Param(value = "endDate") Date endDate);

    /**
     * 根据订单编号查询中港运输单据编号
     * @param orderNos
     * @return
     */
    List<OrderCrossBorderStatusVO> getTransportNoByOrderNo(@Param(value = "orderNos")List<String> orderNos);

    /**
     * 批量修改订单状态
     * @param statusId
     * @param orderNos
     * @return
     */
    int batchUpdateStatus(@Param(value = "statusId")String statusId,@Param(value = "orderNos")List<String> orderNos);

    /**
     * 统计订单金额
     * @param params
     * @return
     */
    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    BigDecimal summaryOrderAmount(Map<String,Object> params);

    List<String> findClientNoByDocNos(@Param(value = "docNo")List<String> docNo);
}
