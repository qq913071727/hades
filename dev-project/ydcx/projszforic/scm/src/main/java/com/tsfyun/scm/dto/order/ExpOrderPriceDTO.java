package com.tsfyun.scm.dto.order;

import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Data
@ApiModel(value="ExpOrderPrice请求对象", description="请求实体")
public class ExpOrderPriceDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @ApiModelProperty(value = "供应商")
   private Long supplierId;

   @NotEmptyTrim(message = "状态编码不能为空")
   @LengthTrim(max = 20,message = "状态编码最大长度不能超过20位")
   @ApiModelProperty(value = "状态编码")
   private String statusId;

   @NotNull(message = "订单不能为空")
   @ApiModelProperty(value = "订单")
   private Long orderId;

   @NotEmptyTrim(message = "订单号不能为空")
   @LengthTrim(max = 20,message = "订单号最大长度不能超过20位")
   @ApiModelProperty(value = "订单号")
   private String orderDocNo;

   @ApiModelProperty(value = "审核时间")
   private Date reviewTime;

   @LengthTrim(max = 20,message = "审核人最大长度不能超过20位")
   @ApiModelProperty(value = "审核人")
   private String reviewer;

   @NotNull(message = "提交次数不能为空")
   @LengthTrim(max = 11,message = "提交次数最大长度不能超过11位")
   @ApiModelProperty(value = "提交次数")
   private Integer submitCount;


}
