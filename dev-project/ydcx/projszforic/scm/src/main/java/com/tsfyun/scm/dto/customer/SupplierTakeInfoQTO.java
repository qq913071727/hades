package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * 供应商提货信息查询实体
 */
@Data
public class SupplierTakeInfoQTO extends PaginationDto implements Serializable {

    private String customerName;

    private String supplierName;

    private String companyName;

}
