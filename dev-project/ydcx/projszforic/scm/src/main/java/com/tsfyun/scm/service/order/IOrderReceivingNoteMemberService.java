package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.OrderReceivingNoteMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.OrderReceivingBindVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-04-29
 */
public interface IOrderReceivingNoteMemberService extends IService<OrderReceivingNoteMember> {

    /**=
     * 根据订单号查询绑定的入库单号
     * @param orderNo
     * @return
     */
    List<String> findReceivingNoByOrderNo(String orderNo);

    /**=
     * 根据订单号查询绑定信息
     * @param orderNo
     * @return
     */
    List<OrderReceivingNoteMember> findByOrderNo(String orderNo);

    /**=
     * 根据订单号 查询订单绑定入库单明细，生成出库单用
     * @param orderNos
     * @return
     */
    List<OrderReceivingBindVO> queryOrderReceivingBind(List<String> orderNos);

    /**=
     * 标识绑定明细是否已出库
     * @param orderNos
     * @return
     */
    Integer identificationIsIssued(List<String> orderNos,Boolean isOutStock);
}
