package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.ImpOrderPriceMember;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>
 * 订单审价明细 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Repository
public interface ImpOrderPriceMemberMapper extends Mapper<ImpOrderPriceMember> {

    Map<String, Object> historicalRecord(@Param(value = "orderMemberId") Long orderMemberId);

    Integer removeByOrderId(@Param(value = "orderId")Long orderId);
}
