package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 品名要素请求实体
 * </p>
 *

 * @since 2020-03-26
 */
@Data
@ApiModel(value="NameElements请求对象", description="品名要素请求实体")
public class NameElementsDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "数据id不能为空",groups = UpdateGroup.class)
   private String id;

   @NotEmptyTrim(message = "海关编码不能为空")
   @LengthTrim(min = 10,max = 10,message = "海关编码长度必须为10位")
   @ApiModelProperty(value = "海关编码")
   private String hsCode;

   /*
   @NotNull(message = "是否禁用不能为空")
   @ApiModelProperty(value = "是否禁用")
   private Boolean disabled;
    */

   @Size(max = 50,message = "要素项目不能超过50条")
   @ApiModelProperty(value = "要素项目")
   private List<String> elementsVal;

   @NotEmptyTrim(message = "物料名称不能为空")
   @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
   @ApiModelProperty(value = "物料名称")
   private String name;

   @NotEmptyTrim(message = "要素名称不能为空")
   @LengthTrim(max = 200,message = "要素名称最大长度不能超过200位")
   @ApiModelProperty(value = "要素名称")
   private String nameMemo;



}
