package com.tsfyun.scm.controller.wms;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.wms.client.ClientReceivingNoteQTO;
import com.tsfyun.scm.service.wms.IReceivingNoteService;
import com.tsfyun.scm.vo.wms.client.ClientReceivingNoteVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 入库单 前端控制器（客户端）
 * @CreateDate: Created in 2020/11/9 15:26
 */
@RestController
@RequestMapping("/client/receivingNote")
@Api(tags = "境外入库（客户端）")
public class ClientReceivingNoteController extends BaseController {

    @Autowired
    private IReceivingNoteService receivingNoteService;

    /**
     * 进口入库分页查询
     * @param qto
     * @return
     */
    @ApiOperation(value = "列表")
    @PostMapping(value = "list")
    public Result<List<ClientReceivingNoteVO>> pageList(@RequestBody ClientReceivingNoteQTO qto) {
        PageInfo<ClientReceivingNoteVO> page = receivingNoteService.clientList(qto);
        return success((int)page.getTotal(),page.getList());
    }

}
