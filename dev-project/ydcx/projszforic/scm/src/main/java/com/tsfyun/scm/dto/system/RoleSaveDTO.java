package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 保存角色
 */
@Data
public class RoleSaveDTO implements Serializable {

    //@NotEmpty(message = "角色编码不能为空")
    @NotEmptyTrim(message = "角色编码不能为空")
    private String id;

    @NotEmptyTrim(message = "原角色编码不能为空",groups = UpdateGroup.class)
    private String preId;

    //@NotEmpty(message = "角色名称不能为空")
    @NotEmptyTrim(message = "角色名称不能为空")
    private String name;

    /**
     * 角色状态
     */
    @NotNull(message = "角色状态不能为空")
    private Boolean disabled;

    /**
     * 备注
     */
    private String memo;

    //菜单编码
    private List<String> menuIds;


}
