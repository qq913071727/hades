package com.tsfyun.scm.controller.customer.client;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.client.ClientSupplierDTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierQTO;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.scm.service.customer.ISupplierService;
import com.tsfyun.scm.vo.customer.SimpleSupplierVO;
import com.tsfyun.scm.vo.customer.SupplierVO;
import com.tsfyun.scm.vo.customer.client.ClientSupplierVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 客户端供应商信息
 * @CreateDate: Created in 2020/9/28 09:50
 */
@RestController
@RequestMapping(value = "/client/supplier")
@Api(tags = "供应商（客户端）")
public class ClientSupplierController extends BaseController {

    @Autowired
    private ISupplierService supplierService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 简单新增供应商信息（不含供应商提货信息和供应商银行）
     * @param dto
     * @return 返回供应商id
     */
    @PostMapping(value = "add")
    @ApiOperation("新增")
    Result<Long> add(@ModelAttribute @Validated(AddGroup.class) ClientSupplierDTO dto) {
        return success(supplierService.clientAdd(dto));
    }

    /**
     * 删除供应商信息（真删，如果存在供应商提货和银行信息则不允许删除）
     */
    @PostMapping("/delete")
    @ApiOperation("删除")
    public Result<Void> delete(@RequestParam("id")Long id){
        supplierService.clientRemove(id);
        return success();
    }

    /**
     * 查询客户供应商分页信息
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    @ApiOperation("分页")
    Result<List<ClientSupplierVO>> page(@ModelAttribute @Valid ClientSupplierQTO qto) {
        PageInfo<Supplier> page = supplierService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),ClientSupplierVO.class));
    }

    /**
     * 简单修改供应商信息（不含供应商提货信息和供应商银行）
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    @ApiOperation("修改")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) ClientSupplierDTO dto) {
        supplierService.clientEdit(dto);
        return success( );
    }

    /**
     * 供应商详情
     * @param id
     * @return
     */
    @GetMapping("detail")
    public Result<SupplierVO> detail(@RequestParam(value = "id")Long id){
        return success(supplierService.detail(id));
    }

    /**
     * 模糊查询客户供应商
     * @param name
     * @return
     */
    @PostMapping("vague")
    public Result<List<SimpleSupplierVO>> clientVague(
            @RequestParam(value = "name",required = false)String name
    ){
        return success(supplierService.clientVague(name));
    }

}
