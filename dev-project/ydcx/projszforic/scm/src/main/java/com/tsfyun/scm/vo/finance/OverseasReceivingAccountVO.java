package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.OverseasReceivingAccountStatusEnum;
import com.tsfyun.common.base.enums.finance.ReceivingModeEnum;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.enums.UndertakingModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 境外收款响应实体
 * </p>
 *
 *
 * @since 2021-10-08
 */
@Data
@ApiModel(value="OverseasReceivingAccount响应对象", description="境外收款响应实体")
public class OverseasReceivingAccountVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;
    @ApiModelProperty(value = "订单号")
    private String orderNo;
    @ApiModelProperty(value = "认领的客户单号")
    private String clientNo;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "收款方式")
    private String receivingMode;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "收款主体")
    private Long overseasId;
    @ApiModelProperty(value = "收款主体")
    private String overseasName;

    @ApiModelProperty(value = "收款行")
    private String receivingBank;

    @ApiModelProperty(value = "收款行账号")
    private String receivingBankNo;

    @ApiModelProperty(value = "收款币制")
    private String currencyId;

    @ApiModelProperty(value = "收款币制")
    private String currencyName;

    @ApiModelProperty(value = "收款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "实际到账金额")
    private BigDecimal actualValue;

    /**
     * 手续费承担方式
     */
    private String undertakingMode;
    private String undertakingModeDesc;

    public String getUndertakingModeDesc(){
        return Optional.ofNullable(UndertakingModeEnum.of(undertakingMode)).map(UndertakingModeEnum::getName).orElse("");
    }

    @ApiModelProperty(value = "手续费")
    private BigDecimal fee;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "到账时间")
    private LocalDateTime accountDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "登记时间")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "付款人")
    private String payer;
    private String payerBank;
    private String payerBankNo;

    @ApiModelProperty(value = "可核销金额")
    private BigDecimal kwriteValue;

    @ApiModelProperty(value = "核销金额")
    private BigDecimal writeValue;

    @ApiModelProperty(value = "是否结汇")
    private Boolean isSettlement;

    /**
     * 是否付款
     */
    private Boolean isPayment;

    /**
     * 结汇金额
     */
    private BigDecimal settlementValue;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String statusDesc;

    private String receivingModeDesc;

    private String createBy;

    public String getStatusDesc () {
        return Optional.ofNullable(OverseasReceivingAccountStatusEnum.of(statusId)).map(OverseasReceivingAccountStatusEnum::getName).orElse("");
    }

    public String getReceivingModeDesc () {
        return Optional.ofNullable(ReceivingModeEnum.of(receivingMode)).map(ReceivingModeEnum::getName).orElse("");
    }

    public String getCreateBy() {
        return StringUtils.isNotEmpty(createBy) && createBy.contains("/") ? createBy.split("/")[1] : null;
    }

    public BigDecimal getKwriteValue(){
        BigDecimal value = getAccountValue();
        if(Objects.isNull(UndertakingModeEnum.of(getUndertakingMode()))){
            return BigDecimal.ZERO;
        }else if(Objects.equals(UndertakingModeEnum.CUSTOMER,UndertakingModeEnum.of(getUndertakingMode()))){
            value = value.subtract(getFee()).setScale(2,BigDecimal.ROUND_HALF_UP);
        }
        return value;
    }

    public BigDecimal getActualValue(){
        return getAccountValue().subtract(getFee()).setScale(2,BigDecimal.ROUND_HALF_UP);
    }

}
