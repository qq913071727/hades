package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/28 11:28
 */
@Data
public class ExpMaterielQTO extends PaginationDto implements Serializable {

    private String model;

    private String name;

    private String brand;

    private String spec;

    private String statusId;

    private String customerName;

}
