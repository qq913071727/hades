package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @since Created in 2020/4/28 09:51
 */
@Data
public class ImpOrderAdjustCostVO implements Serializable {

    /**
     * 订单id
     */
    private Long impOrderId;

    /**
     * 订单号
     */
    private String impOrderNo;

    /**
     * 客户名称
     */
    private String customerName;

}
