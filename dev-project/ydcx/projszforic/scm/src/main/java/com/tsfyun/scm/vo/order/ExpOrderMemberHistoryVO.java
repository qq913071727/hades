package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Data
@ApiModel(value="ExpOrderMemberHistory响应对象", description="响应实体")
public class ExpOrderMemberHistoryVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private Integer rowNo;

    @ApiModelProperty(value = "订单主单")
    private Long expOrderId;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "规格参数")
    private String spec;

    @ApiModelProperty(value = "客户物料号")
    private String goodsCode;

    @ApiModelProperty(value = "产地")
    private String country;

    @ApiModelProperty(value = "产地")
    private String countryName;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价(委托)")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "总价(委托)")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;

    @ApiModelProperty(value = "箱号")
    private String cartonNo;


}
