package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.ImpSalesContractMember;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 销售合同明细 Mapper 接口
 * </p>
 *

 * @since 2020-05-19
 */
@Repository
public interface ImpSalesContractMemberMapper extends Mapper<ImpSalesContractMember> {

    void deleteBySalesContractId(Long salesContractId);

}
