package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 绑定入库单辅助表
 * </p>
 *
 *
 * @since 2020-05-06
 */
@Data
public class OrderMemberReceivingNo implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 订单明细
     */

    private Long orderMemberId;

    /**
     * 入库单号
     */

    private String receivingNo;

    /**
     * 数量
     */

    private BigDecimal quantity;


}
