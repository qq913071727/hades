package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: 费用变更记录查询请求实体
 * @CreateDate: Created in 2020/5/27 09:34
 */
@Data
public class CostChangeRecordQTO extends PaginationDto implements Serializable {

    private String impOrderNo;

    private String customerName;

    private String operator;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
