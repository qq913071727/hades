package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditPaymentPlusVO implements Serializable {

    private EditPaymentVO payment;

    private List<EditPaymentMemberVO> members;
}
