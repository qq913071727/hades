package com.tsfyun.scm.vo.customs;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.ManifestStatusEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import com.tsfyun.common.base.enums.domain.DeclarationStatusEnum;
import com.tsfyun.common.base.enums.singlewindow.SingleWindowDeclarationStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description:
 * @Project:
 * @CreateDate: Created in 2020/5/11 12:05
 */
@Data
public class DeclarationListVO implements Serializable {

    /**
     * 报关单数据id
     */
    private Long id;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 订单编号
     */
    private String orderDocNo;

    /**
     * 合同号
     */
    private String contrNo;

    /**
     * 成交方式
     */
    private String transactionMode;

    /**
     * 成交方式名称
     */
    private String transactionModeDesc;

    /**
     * 状态编码
     */
    private String statusId;

    /**
     * 状态中文名称
     */
    private String statusDesc;

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 客户名称
     */
    private String customerName;


    /**
     * 报关单号
     */
    private String declarationNo;

    /**
     * 车牌号码
     */
    private String licensePlate;

    /**
     * 申报状态
     */
    private String declarationStatus;


    /**
     * 进出口日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime importDate;

    /**
     * 申报日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime decDate;

    /**
     * 提运单号
     */
    private String cusVoyageNo;

    private String customMasterName;//申报地海关
    private String iePortName;//进境关别

    private Integer packNo;//件数

    private String declarationStatusDesc;

    private Long manifestId;

    private String manifestStatusId;

    private String manifestStatusDesc;

    /**
     * 舱单单一窗口导入状态
     */
    private String manifestSwStatus;

    private String manifestSwStatusDesc;

    public String getstatusDesc() {
        return Optional.ofNullable(DeclarationStatusEnum.of(statusId)).map(DeclarationStatusEnum::getName).orElse("");
    }

    public String getTransactionModeDesc() {
        return Optional.ofNullable(TransactionModeEnum.of(transactionMode)).map(TransactionModeEnum::getName).orElse("");
    }

    public String getDeclarationStatusDesc() {
        return Optional.ofNullable(SingleWindowDeclarationStatusEnum.of(declarationStatus)).map(SingleWindowDeclarationStatusEnum::getName).orElse("");
    }

    public String getManifestSwStatusDesc() {
        return Optional.ofNullable(SingleWindowDeclarationStatusEnum.of(manifestSwStatus)).map(SingleWindowDeclarationStatusEnum::getName).orElse("");
    }

    public String getManifestStatusDesc(){
        return Optional.ofNullable(ManifestStatusEnum.of(manifestStatusId)).map(ManifestStatusEnum::getName).orElse("");
    }


}
