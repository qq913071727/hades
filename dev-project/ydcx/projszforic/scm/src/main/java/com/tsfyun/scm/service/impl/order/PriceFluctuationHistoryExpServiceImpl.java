package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.order.PriceFluctuationHistoryExp;
import com.tsfyun.scm.mapper.order.PriceFluctuationHistoryExpMapper;
import com.tsfyun.scm.service.order.IPriceFluctuationHistoryExpService;
import com.tsfyun.common.base.extension.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2021-09-17
 */
@Service
public class PriceFluctuationHistoryExpServiceImpl extends ServiceImpl<PriceFluctuationHistoryExp> implements IPriceFluctuationHistoryExpService {

    @Override
    public List<PriceFluctuationHistoryExp> list(Long memberId) {
        PriceFluctuationHistoryExp query = new PriceFluctuationHistoryExp();
        query.setOrderPriceMemberId(memberId);
        List<PriceFluctuationHistoryExp> list = list(query);
        if(CollUtil.isNotEmpty(list)){
            list = list.stream().sorted(Comparator.comparing(PriceFluctuationHistoryExp::getOrderDate).reversed()).collect(Collectors.toList());
        }
        return list;
    }
}
