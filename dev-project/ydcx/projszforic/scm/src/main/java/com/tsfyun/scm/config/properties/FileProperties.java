package com.tsfyun.scm.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.List;
import java.util.Map;

/**
 * @Description
 *
 * @Date 2020/3/4 14:08
 * @Version V1.0
 */
@RefreshScope
@ConfigurationProperties(prefix = "file")
@Data
public class FileProperties {

    private String serverUrl;

    private List<Map<String,Object>> businessType;

    private List<Map<String,Object>> docType;


}