package com.tsfyun.scm.mapper.support;

import com.tsfyun.scm.entity.support.TaskNotice;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 任务通知 Mapper 接口
 * </p>
 *

 * @since 2020-04-05
 */
@Repository
public interface TaskNoticeMapper extends Mapper<TaskNotice> {

    List<Long> findByPersonId(@Param(value = "tncIds") List<Long> tncIds);
}
