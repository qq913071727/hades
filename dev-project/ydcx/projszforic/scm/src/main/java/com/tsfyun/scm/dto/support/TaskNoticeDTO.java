package com.tsfyun.scm.dto.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 任务通知请求实体
 * </p>
 *

 * @since 2020-04-05
 */
@Data
@ApiModel(value="TaskNotice请求对象", description="任务通知请求实体")
@NoArgsConstructor
@AllArgsConstructor
public class TaskNoticeDTO implements Serializable {

   private static final long serialVersionUID=1L;

   /**
    * 操作码
    */
   private String operationCode;

   @NotNull(message = "任务通知内容id不能为空")
   @ApiModelProperty(value = "任务通知内容id")
   private Long taskNoticeId;

   @NotNull(message = "提交任务通知用户id不能为空")
   @ApiModelProperty(value = "提交任务通知用户id")
   private Long senderId;

   @LengthTrim(max = 40,message = "提交任务通知用户名称最大长度不能超过40位")
   @ApiModelProperty(value = "提交任务通知用户名称")
   private String senderName;

   @NotNull(message = "接收用户id不能为空")
   @ApiModelProperty(value = "接收用户id")
   private Long receiverId;

   @LengthTrim(max = 40,message = "接收用户名称最大长度不能超过40位")
   @ApiModelProperty(value = "接收用户名称")
   private String receiverName;


}
