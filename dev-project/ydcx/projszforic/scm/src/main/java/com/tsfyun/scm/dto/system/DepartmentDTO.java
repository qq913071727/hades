package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 部门请求实体
 */
@Data
public class DepartmentDTO implements Serializable {

    /**
     * 修改时不能为空
     */
    @NotNull(message = "部门id不能为空",groups = UpdateGroup.class)
    private Long id;

    @NotEmptyTrim(message = "部门编码不能为空")
    @LengthTrim(message = "部门编码长度不能超过20位",max = 20)
    private String code;

    @NotEmptyTrim(message = "部门名称不能为空")
    @LengthTrim(message = "部门名称长度不能超过50位",max = 50)
    private String name;

    /**
     * 排序码
     */
    private Integer sort;

    //一级部门传0
    @NotNull(message = "上级部门不能为空",groups = AddGroup.class)
    private Long parentId;

}
