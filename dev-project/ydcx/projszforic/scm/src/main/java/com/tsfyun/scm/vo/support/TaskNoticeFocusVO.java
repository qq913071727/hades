package com.tsfyun.scm.vo.support;

import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 用户关注任务通知响应实体
 * </p>
 *

 * @since 2020-04-09
 */
@Data
@ApiModel(value="TaskNoticeFocus响应对象", description="用户关注任务通知响应实体")
public class TaskNoticeFocusVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "操作码")
    private String operationCode;

    private String operationCodeDesc;

    public String getOperationCodeDesc() {
        if(StringUtils.isNotEmpty(operationCode)) {
            DomainOprationEnum domainOprationEnum = DomainOprationEnum.of(operationCode);
            return Optional.ofNullable(domainOprationEnum).isPresent() ? domainOprationEnum.getName() : null;
        }
        return null;
    }

}
