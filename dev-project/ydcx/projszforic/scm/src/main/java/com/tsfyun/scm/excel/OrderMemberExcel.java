package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;


import java.io.Serializable;

/**
 * @Description: 订单明细导入excel
 * @CreateDate: Created in 2020/5/20 09:29
 */
@Data
public class OrderMemberExcel extends BaseRowModel implements Serializable {


    @ExcelProperty(value = "型号", index = 0)
    private String model;

    @ExcelProperty(value = "品名", index = 1)
    private String name;

    @ExcelProperty(value = "品牌", index = 2)
    private String brand;

    @ExcelProperty(value = "单位", index = 3)
    private String unitName;

    @ExcelProperty(value = "数量", index = 4)
    private String quantity;

    @ExcelProperty(value = "总价", index = 5)
    private String totalPrice;

    @ExcelProperty(value = "箱数", index = 6)
    private String cartonNum;

    @ExcelProperty(value = "净重", index = 7)
    private String netWeight;

    @ExcelProperty(value = "毛重", index = 8)
    private String grossWeight;

    @ExcelProperty(value = "产地", index = 9)
    private String countryName;

    @ExcelProperty(value = "客户料号", index = 10)
    private String goodsCode;

    @ExcelProperty(value = "规格描述", index = 11)
    private String spec;

}
