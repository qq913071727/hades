package com.tsfyun.scm.service.impl.system;

import com.google.common.collect.Maps;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.security.config.StringRedisUtils;
import com.tsfyun.scm.service.system.IValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class ValidateCodeServiceImpl implements IValidateCodeService {

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    private StringRedisUtils stringRedisUtils;

    @Override
    public Map getCode(HttpServletRequest request) {
        String deviceId = request.getParameter("deviceId");
        TsfPreconditions.checkArgument(StringUtils.isNotEmpty(deviceId),new ServiceException("deviceId不能为空"));
        Map codeMap = Maps.newHashMap();
        String type = StringUtils.removeSpecialSymbol(request.getParameter("type"));
        if(StringUtils.isEmpty(type)) {
            type = "verificationCode";
        }
        Object redisVerifyCode = stringRedisUtils.get("IMG_VERIFY:" +  type + "-" +  deviceId);
        Object codeAlgorithmObj = stringRedisUtils.getToString("IMG_VERIFY_ALGORITHM:" + type + "-" + deviceId);
        Integer codeAlgorithm = Optional.ofNullable(codeAlgorithmObj).isPresent() ? Integer.valueOf(codeAlgorithmObj.toString()) : null;
        codeMap.put("code",redisVerifyCode);
        codeMap.put("codeAlgorithm",codeAlgorithm);
        return codeMap;
    }

    @Override
    public void remove() {
        String type = StringUtils.removeSpecialSymbol(request.getParameter("type"));
        if(StringUtils.isEmpty(type)) {
            type = "verificationCode";
        }
        String deviceId = request.getParameter("deviceId");
        stringRedisUtils.remove("IMG_VERIFY:" + type + "-" +  deviceId);
        stringRedisUtils.remove("IMG_VERIFY_ALGORITHM:" + type + "-" + deviceId);
    }

    @Override
    public void validate(HttpServletRequest request) {
        Map codeMap = this.getCode(request);
        String requestCode = request.getParameter("validateCode");
        if(StringUtils.isEmpty(requestCode)) {
            throw new AuthenticationException("验证码不能为空"){};
        }
        String saveCode = StringUtils.null2EmptyWithTrim(codeMap.get("code"));
        if(StringUtils.isEmpty(saveCode)) {
            throw new AuthenticationException("验证码不存在或已过期"){};
        }
        //此处算法不同，处理不一样
        Integer randomInt = (Integer)codeMap.get("codeAlgorithm");
        if(Objects.isNull(randomInt)) {
            throw new AuthenticationException("验证码不存在或已过期"){};
        }
        if(1 == randomInt) {
            int firstCode = Integer.valueOf(saveCode.substring(0,1));
            int sencondCode = Integer.valueOf(saveCode.substring(1,2));
            saveCode = (firstCode + sencondCode) + "";
        }
        if (!Objects.equals(saveCode, requestCode)) {
            throw new AuthenticationException ("验证码不正确"){};
        }
        this.remove();
    }
}
