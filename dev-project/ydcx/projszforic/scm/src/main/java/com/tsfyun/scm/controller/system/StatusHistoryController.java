package com.tsfyun.scm.controller.system;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.StatusHistoryDTO;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.scm.service.system.IStatusHistoryService;
import com.tsfyun.scm.vo.system.StatusHistoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-03-03
 */
@RestController
@RequestMapping("/statusHistory")
public class StatusHistoryController extends BaseController {

    @Autowired
    private IStatusHistoryService statusHistoryService;

    @PostMapping(value = "list")
    public Result<List<StatusHistoryVO>> list(@RequestParam(value = "docId")String docId){
        return success(statusHistoryService.findByDomainId(docId));
    }

    @GetMapping(value = "recent")
    public Result<StatusHistoryVO> recent(@RequestParam(value = "domainId")String domainId,@RequestParam(value = "domainType")String domainType,
                                          @RequestParam(value = "nowStatusId")String nowStatusId){
        return success(statusHistoryService.findRecentByDomainAndNowStatus(domainId,domainType,nowStatusId));
    }

    @PostMapping(value = "recentInfos")
    public Result<List<StatusHistoryVO>> recentInfos(@RequestParam(value = "domainIds")List<String> domainIds,@RequestParam(value = "domainType")String domainType,
                                          @RequestParam(value = "nowStatusId")String nowStatusId){
        return success(statusHistoryService.findRecentByDomainAndNowStatusBatch(domainIds,domainType,nowStatusId));
    }

    @PostMapping(value = "add")
    public Result<Void> add(@RequestBody @Validated StatusHistoryDTO dto){
        statusHistoryService.saveHistory(DomainOprationEnum.of(dto.getOperation()),dto.getDomainId(),dto.getDomainType(),dto.getHistoryStatusId(),dto.getHistoryStatusName(),dto.getNowStatusId(),dto.getNowStatusName(),dto.getMemo(),dto.getOperator());
        return success();
    }

}

