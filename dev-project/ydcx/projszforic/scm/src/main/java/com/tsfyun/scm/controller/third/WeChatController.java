package com.tsfyun.scm.controller.third;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.support.WxMenuDTO;
import com.tsfyun.scm.service.third.IWeixinService;
import com.tsfyun.scm.service.third.IWxMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/wx")
public class WeChatController extends BaseController {

    @Autowired
    private IWeixinService weixinService;
    @Autowired
    private IWxMessageService wxMessageService;

    //微信公众号验证
    @PostMapping(value = "signature")
    public String signature(HttpServletResponse response,
                          HttpServletRequest request){
        response.setContentType("text/html;charset=utf-8");
        return wxMessageService.handleEvent(request,response);
    }

    //生成临时二维码
    @PostMapping(value = "genTempQrcode")
    public Result<String> genTempQrcode(@RequestParam(value = "code")String code){
        return success(weixinService.genTempQrcode(code));
    }

    //微信公众号创建菜单
    @PostMapping(value = "addMenu")
    @PreAuthorize("hasRole('manager')")
    public Result<Void> addMenu(@RequestBody @Validated WxMenuDTO dto){
        weixinService.addMenu(dto);
        return success();
    }

    //微信公众号删除菜单
    @PostMapping(value = "deleteMenu")
    @PreAuthorize("hasRole('manager')")
    public Result<Void> deleteMenu( ){
        weixinService.deleteMenu( );
        return success();
    }

    //生成短链
    @PostMapping(value = "shortUrl")
    @PreAuthorize("hasRole('manager')")
    public Result<String> shortUrl(@RequestParam(value = "url")String url){
        return success(weixinService.generateShortUrl(url));
    }

}
