package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 货物毛重信息
 * @since Created in 2020/4/22 11:58
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GoodsMeasure", propOrder = {
        "grossMassMeasure",
})
public class GoodsMeasure {

    //货物毛重
    @XmlElement(name = "GrossMassMeasure")
    private GrossMassMeasure grossMassMeasure;

    public GoodsMeasure() {

    }

    public GoodsMeasure(GrossMassMeasure grossMassMeasure) {
        this.grossMassMeasure = grossMassMeasure;
    }

    public GrossMassMeasure getGrossMassMeasure() {
        return grossMassMeasure;
    }

    public void setGrossMassMeasure(GrossMassMeasure grossMassMeasure) {
        this.grossMassMeasure = grossMassMeasure;
    }

}
