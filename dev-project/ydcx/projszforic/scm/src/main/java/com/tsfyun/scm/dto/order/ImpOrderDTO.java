package com.tsfyun.scm.dto.order;


import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 进口订单请求实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrder请求对象", description="进口订单请求实体")
public class ImpOrderDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @ApiModelProperty(value = "订单id")
   private Long id;

   @ApiModelProperty(value = "订单编号")
   @NotEmptyTrim(message = "订单编号不能为空")
   @LengthTrim(max = 20,message = "订单编号最大长度不能超过20位")
   private String docNo;

   @ApiModelProperty(value = "客户单号")
   //@NotEmptyTrim(message = "客户单号不能为空")
   @LengthTrim(max = 20,message = "客户单号最大长度不能超过20位")
   private String clientNo;

   @ApiModelProperty(value = "客户")
   @NotEmptyTrim(message = "客户不能为空")
   private String customerName;

   @ApiModelProperty(value = "客户ID,客户ID存在优先取客户ID")
   private Long customerId;

   @ApiModelProperty(value = "业务类型")
   @NotEmptyTrim(message = "业务类型不能为空")
   @EnumCheck(clazz = BusinessTypeEnum.class,message = "业务类型错误")
   private String businessType;

   @ApiModelProperty(value = "境外供应商")
   @NotEmptyTrim(message = "境外供应商不能为空")
   private String supplierName;

   @ApiModelProperty(value = "币制")
   @NotEmptyTrim(message = "币制不能为空")
   private String currencyName;

   @ApiModelProperty(value = "协议报价")
   @NotNull(message = "请选择协议报价")
   private Long impQuoteId;

   @ApiModelProperty(value = "成交方式")
   @NotEmptyTrim(message = "成交方式不能为空")
   @EnumCheck(clazz = TransactionModeEnum.class,message = "成交方式错误")
   private String transactionMode;

   @ApiModelProperty(value = "外贸合同买方")
   @NotNull(message = "请选择外贸合同买方")
   private Long subjectId;

   @ApiModelProperty(value = "外贸合同卖方")
   @NotNull(message = "请选择外贸合同卖方")
   private Long subjectOverseasId;

   @ApiModelProperty(value = "订单备注")
   @LengthTrim(max = 500,message = "订单备注最大长度不能超过500位")
   private String memo;


}
