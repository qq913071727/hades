package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.client.RateClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:
 * @since Created in 2020/4/10 13:58
 */
@Component
@Slf4j
public class RateClientFallback implements FallbackFactory<RateClient> {


    @Override
    public RateClient create(Throwable throwable) {
        RateClient rateClient = new RateClient() {
            @Override
            public Result<BigDecimal> thisMonth(String currencyId) {
                log.error(String.format("远程调用获取币制【%s】失败", currencyId), throwable);
                return null;
            }
            public Result<BigDecimal> obtain(String currencyId, Date date) {
                log.error(String.format("远程调用获取币制【%s】失败", currencyId), throwable);
                return null;
            }

        };
        return rateClient;
    }
}
