package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 危险品信息
 * @CreateDate: Created in 2020/7/20 16:12
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Classification", propOrder = {
        "id",
})
@NoArgsConstructor
@AllArgsConstructor
public class Classification {

    //危险品编号
    @XmlElement(name = "ID")
    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
