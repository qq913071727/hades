package com.tsfyun.scm.service.impl.third;

import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.entity.third.LogSms;
import com.tsfyun.scm.mapper.third.LogSmsMapper;
import com.tsfyun.scm.service.third.ILogSmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2020-09-16
 */
@Service
public class LogSmsServiceImpl extends ServiceImpl<LogSms> implements ILogSmsService {

    @Autowired
    private LogSmsMapper logSmsMapper;

    @Override
    public Integer countOneMinute(String phoneNo, MessageNodeEnum messageNode) {
        return logSmsMapper.countOneMinute(phoneNo,messageNode.getCode());
    }

    @Override
    public Integer countByIpAndMinute(String ip, Integer minute) {
        return logSmsMapper.countByIpAndMinute(ip,minute);
    }

    @Override
    public String selectCodeNew(String phoneNo, MessageNodeEnum messageNode, Integer minute) {
        return logSmsMapper.selectCodeNew(phoneNo,messageNode.getCode(),minute);
    }
}
