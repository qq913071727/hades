package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 协议报价单查询请求实体
 * @since Created in 2020/4/1 10:09
 */
@Data
public class AgreementQTO extends PaginationDto implements Serializable {

    /**
     * 客户id
     */
    private String customerId;

    /**
     * 甲方名称
     */
    private String partyaName;

    /**
     * 状态
     */
    private String statusId;

    /**=
     * 业务类型
     */
    private String businessType;

    /**=
     * 报关类型
     */
    private String declareType;

    /**=
     * 协议编号
     */
    private String docNo;

    /**=
     * 协议是否签回
     */
    private Boolean isSignBack;

    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**=
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**=
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd){
        if(dateEnd!=null){
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }
}
