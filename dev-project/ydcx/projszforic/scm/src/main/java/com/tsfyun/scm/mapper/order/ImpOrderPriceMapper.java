package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.order.ImpOrderPrice;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.ImpOrderPriceVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单审价 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Repository
public interface ImpOrderPriceMapper extends Mapper<ImpOrderPrice> {

    @DataScope(tableAlias = "a",customerTableAlias = "c",addCustomerNameQuery=true)
    List<ImpOrderPriceVO> list(Map<String,Object> params);

    Integer removeByOrderId(@Param(value = "orderId") Long orderId);
}
