package com.tsfyun.scm.service.logistics;

import com.tsfyun.scm.entity.logistics.DeliveryWaybillMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillMemberVO;

import java.util.List;

/**
 * <p>
 * 送货单明细 服务类
 * </p>
 *

 * @since 2020-05-25
 */
public interface IDeliveryWaybillMemberService extends IService<DeliveryWaybillMember> {

    List<DeliveryWaybillMemberVO> findByDeliveryId(Long deliveryId);

    /**
     * 根据送货单主单删除明细数据
     * @param waybillId
     */
    void removeByDeliveryId(Long deliveryId);
}
