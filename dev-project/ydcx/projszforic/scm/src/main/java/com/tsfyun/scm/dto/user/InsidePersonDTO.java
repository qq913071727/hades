package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

@Data
public class InsidePersonDTO extends PaginationDto {

    //员工编码
    private String code;

    //员工名称
    private String name;

    //手机号码
    private String phone;

    //部门id
    private Long departmentId;

    //在职状态
    private Boolean incumbency;

    //职位类型
    private String position;

    //是否删除
    private Boolean isDelete;

    //员工渠道
    private String channel;

    //公司名称
    private String customerName;

}
