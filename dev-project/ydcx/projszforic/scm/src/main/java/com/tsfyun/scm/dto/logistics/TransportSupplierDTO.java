package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="TransportSupplier请求对象", description="请求实体")
public class TransportSupplierDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "数据id不能为空",groups = UpdateGroup.class)
   private Long id;

   private String address;

   @NotNull(message = "禁用启用不能为空")
   @ApiModelProperty(value = "禁用启用")
   private Boolean disabled;

   @LengthTrim(max = 50,message = "传真最大长度不能超过50位")
   @ApiModelProperty(value = "传真")
   private String fax;

   @LengthTrim(max = 50,message = "联系人最大长度不能超过50位")
   @ApiModelProperty(value = "联系人")
   private String linkPerson;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @NotEmptyTrim(message = "供应商名称不能为空")
   @LengthTrim(max = 200,message = "供应商名称最大长度不能超过200位")
   @ApiModelProperty(value = "供应商名称")
   private String name;

   @LengthTrim(max = 100,message = "电话最大长度不能超过100位")
   @ApiModelProperty(value = "电话")
   private String tel;



}
