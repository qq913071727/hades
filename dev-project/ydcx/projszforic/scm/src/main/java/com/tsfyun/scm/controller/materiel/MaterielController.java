package com.tsfyun.scm.controller.materiel;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.common.base.vo.ImpExcelVO;
import com.tsfyun.scm.dto.materiel.ClassifyMaterielDTO;
import com.tsfyun.scm.dto.materiel.MaterielQTO;
import com.tsfyun.scm.service.materiel.IMaterielService;
import com.tsfyun.scm.vo.materiel.MaterielHSVO;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  物料管理
 * </p>
 *
 *
 * @since 2020-03-22
 */
@RestController
@RequestMapping("/materiel")
public class MaterielController extends BaseController {

    @Autowired
    private IMaterielService materielService;

    /**=
     * 查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<MaterielHSVO>> list(@ModelAttribute @Valid MaterielQTO qto){
        return materielService.pageList(qto);
    }

    /**=
     * 模糊查询0
     * @param qto
     * @return
     */
    @PostMapping(value = "vague")
    Result<List<SimpleMaterielVO>> vague(@ModelAttribute @Valid MaterielQTO qto){
        qto.setLimit(10);
        qto.setStatusId(MaterielStatusEnum.CLASSED.getCode());
        return success(materielService.vague(qto));
    }

    /**=
     * 物料导入
     * @param file
     * @return
     */
    @PostMapping(value = "import")
    Result<ImpExcelVO> importMateriel(@RequestPart(value = "file") MultipartFile file){
        return success(materielService.importMateriel(file));
    }

    /**=
     * 归类物料导入
     * @param file
     * @return
     */
    @PostMapping(value = "importClassify")
    Result<ImpExcelVO> importClassify(@RequestPart(value = "file") MultipartFile file){
        return success(materielService.importClassifyMateriel(file));
    }

    /**=
     * 归类详情
     * @return
     */
    @PostMapping(value = "classifyDetail")
    Result<Map<String,Object>> classifyDetail(@RequestParam(name = "id")String id){
        return success(materielService.classifyDetail(Arrays.asList(id.split(","))));
    }

    /**=
     * 保存归类
     * @return
     */
    @PostMapping(value = "saveClassify")
    Result<Void> saveClassify(@ModelAttribute @Valid ClassifyMaterielDTO dto){
        materielService.saveClassify(dto);
        return success();
    }
}

