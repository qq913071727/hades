package com.tsfyun.scm.vo.logistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 国内物流响应实体
 * </p>
 *

 * @since 2020-03-25
 */
@Data
@ApiModel(value="DomesticLogistics响应对象", description="国内物流响应实体")
public class DomesticLogisticsVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "国内物流编码")
    private String code;

    @ApiModelProperty(value = "国内物流名称")
    private String name;

    @ApiModelProperty(value = "时效要求")
    @Transient
    private List<String> timeline;

    @ApiModelProperty(value = "时效要求-后台用逗号隔开")
    private String timeliness;

    @ApiModelProperty(value = "是否禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "是否锁定")
    private Boolean locking;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateUpdated;


}
