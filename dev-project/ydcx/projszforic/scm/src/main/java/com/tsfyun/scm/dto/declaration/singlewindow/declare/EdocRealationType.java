//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 电子随附单据关联关系
 * 
 * <p>Edoc_RealationType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="Edoc_RealationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EdocCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OpNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EdocCopId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="64"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EdocOwnerCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EdocOwnerName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EdocSize" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Edoc_RealationType", propOrder = {
    "edocId",
    "edocCode",
    "edocFomatType",
    "opNote",
    "edocCopId",
    "edocOwnerCode",
    "edocOwnerName",
    "edocSize"
})
public class EdocRealationType {
    //文件名、随附单据编号（文件名命名规则是：申报口岸+随附单据类别代码+IM+18位流水号+.pdf）
    @XmlElement(name = "EdocID", required = true)
    protected String edocId;
    //随附单据类型
    /*
    如：00000001:发票；
    00000002:装箱单；
    00000003:提/运单；
    00000004:合同；
    10000001：代理报关委托协议（电子）；
    10000002：减免税货物税款担保证明；
    10000003：减免税货物税款担保延期证明
    等等。
    具体代码详见：总署网站《随附单据代码参数表》。
     */
    @XmlElement(name = "EdocCode", required = true)
    protected String edocCode;
    //随附单据格式类型,S:结构化，US:非结构化（pdf文件填写US）
    @XmlElement(name = "EdocFomatType", required = true)
    protected String edocFomatType;
    //操作说明（重传原因）
    @XmlElement(name = "OpNote", required = true)
    protected String opNote;
    //随附单据文件企业名
    @XmlElement(name = "EdocCopId", required = true)
    protected String edocCopId;
    //所属单位改观编号
    @XmlElement(name = "EdocOwnerCode", required = true)
    protected String edocOwnerCode;
    //所属单位名称
    @XmlElement(name = "EdocOwnerName")
    protected String edocOwnerName;
    //随附单据文件大小
    @XmlElement(name = "EdocSize")
    protected String edocSize;

    public String getEdocFomatType() {
        return edocFomatType;
    }

    public void setEdocFomatType(String edocFomatType) {
        this.edocFomatType = edocFomatType;
    }

    public String getEdocId() {
        return edocId;
    }

    public void setEdocId(String edocId) {
        this.edocId = edocId;
    }

    /**
     * 获取edocCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdocCode() {
        return edocCode;
    }

    /**
     * 设置edocCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdocCode(String value) {
        this.edocCode = value;
    }

    /**
     * 获取opNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpNote() {
        return opNote;
    }

    /**
     * 设置opNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpNote(String value) {
        this.opNote = value;
    }

    /**
     * 获取edocCopId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdocCopId() {
        return edocCopId;
    }

    /**
     * 设置edocCopId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdocCopId(String value) {
        this.edocCopId = value;
    }

    /**
     * 获取edocOwnerCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdocOwnerCode() {
        return edocOwnerCode;
    }

    /**
     * 设置edocOwnerCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdocOwnerCode(String value) {
        this.edocOwnerCode = value;
    }

    /**
     * 获取edocOwnerName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdocOwnerName() {
        return edocOwnerName;
    }

    /**
     * 设置edocOwnerName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdocOwnerName(String value) {
        this.edocOwnerName = value;
    }

    /**
     * 获取edocSize属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdocSize() {
        return edocSize;
    }

    /**
     * 设置edocSize属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdocSize(String value) {
        this.edocSize = value;
    }

}
