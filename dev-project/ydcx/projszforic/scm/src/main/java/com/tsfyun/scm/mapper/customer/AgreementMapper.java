package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.customer.Agreement;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.AgreementVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 协议报价单 Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Repository
public interface AgreementMapper extends Mapper<Agreement> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<AgreementVO> list(Map<String,Object> params);

    List<Agreement> selectByIdsAndIsSignBack(@Param(value = "ids")List<Long> ids,@Param(value = "isSignBack")Boolean IsSignBack);

    /**
     * 统计客户未完成的报价（状态不等于（已确认，已作废））
     * @param customerId
     * @return
     */
    Integer countUndone(@Param(value = "customerId")Long customerId);

    /**
     * 根据客户和协议状态查询最新的一条报价单
     * @param customerId
     * @param statusId
     * @return
     */
    Agreement findOneByCustomerIdAndStatusId(@Param(value = "customerId")Long customerId,@Param(value = "statusId")String statusId);

    /**
     * 定时扫描处理出口报价失效
     */
    void invalidImpAgreement();

    /**
     * 获取客户最新的有效的协议报价
     * @param customerId
     * @return
     */
    Agreement obtainCustomerNewEffective(@Param(value = "customerId")Long customerId);
}
