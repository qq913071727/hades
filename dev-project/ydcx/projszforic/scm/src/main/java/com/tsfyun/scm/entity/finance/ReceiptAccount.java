package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 收款单
 * </p>
 *

 * @since 2020-04-15
 */
@Data
public class ReceiptAccount extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 收款日期
     */

    private LocalDateTime accountDate;

    /**
     * 收款行
     */

    private String receivingBank;

    /**
     * 收款金额
     */

    private BigDecimal accountValue;

    /**
     * 锁定金额
     */

    private BigDecimal lockValue;

    /**
     * 核销金额
     */

    private BigDecimal writeValue;

    /**
     * 币制编码
     */

    private String currencyId;

    /**
     * 币制名称
     */

    private String currencyName;

    /**
     * 客户id
     */

    private Long customerId;

    /**
     * 付款人
     */

    private String payer;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 收款方式
     */

    private String receivingMode;

    /**
     * 汇票编号
     */

    private String draftNo;

    /**
     * 备注
     */

    private String memo;


}
