package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.FileDTO;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-03
 */
@Data
@ApiModel(value="Customer请求对象", description="请求实体")
public class CustomerDTO implements Serializable {

    private static final long serialVersionUID=1L;

     //客户id
    @NotNull(message = "请选择客户",groups = {UpdateGroup.class})
    private Long id;

    @ApiModelProperty(value = "保存/审核")
    private Boolean isSubmit;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @LengthTrim(min = 3,max = 20,message = "客户代码只能为3-20位")
    @ApiModelProperty(value = "客户代码")
    private String code;

    @NotBlank(message = "客户名称不能为空")
    @LengthTrim(min = 1,max = 200)
    @ApiModelProperty(value = "客户名称")
    private String name;

    @LengthTrim(min = 1,max = 255)
    @ApiModelProperty(value = "英文名称")
    private String nameEn;

    @LengthTrim(min = 18,max = 18,message = "社会信用代码只能为18位")
    @ApiModelProperty(value = "社会信用代码(18位)")
    private String socialNo;

    @LengthTrim(min = 18,max = 18,message = "纳税人识别号只能为18位")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxpayerNo;

    @LengthTrim(min = 10,max = 10,message = "海关注册编码只能为10位")
    @ApiModelProperty(value = "海关注册编码")
    private String customsCode;

    @LengthTrim(min = 10,max = 10,message = "检验检疫编码只能为10位")
    @ApiModelProperty(value = "检验检疫编码")
    private String ciqNo;

    @LengthTrim(min = 2,max = 50,message = "公司法人只能为2-50位")
    @ApiModelProperty(value = "公司法人")
    private String legalPerson;

    @LengthTrim(min = 3,max = 50,message = "公司电话只能为3-50位")
    @ApiModelProperty(value = "公司电话")
    private String tel;

    @LengthTrim(min = 3,max = 50,message = "公司传真只能为3-50位")
    @ApiModelProperty(value = "公司传真")
    private String fax;

    @LengthTrim(min = 2,max = 255,message = "公司地址只能为2-255位")
    @ApiModelProperty(value = "公司地址")
    private String address;

    @LengthTrim(min = 2,max = 255,message = "公司地址英文只能为2-255位")
    @ApiModelProperty(value = "公司地址英文")
    private String addressEn;

    @ApiModelProperty(value = "销售员工")
    private Long salePersonId;

    @ApiModelProperty(value = "主要商务")
    private Long busPersonId;

    @ApiModelProperty(value = "辅助商务")
    private Long busSecPersonId;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @LengthTrim(min = 2,max = 50,message = "开户银行只能为2-50位")
    @ApiModelProperty(value = "开户银行")
    private String invoiceBankName;

    @LengthTrim(min = 2,max = 50,message = "银行帐号只能为2-50位")
    @ApiModelProperty(value = "银行帐号")
    private String invoiceBankAccount;

    @LengthTrim(min = 2,max = 255,message = "开票地址只能为2-255位")
    @ApiModelProperty(value = "开票地址")
    private String invoiceBankAddress;

    @LengthTrim(max = 50,message = "开票电话最大长度只能为50位")
    @ApiModelProperty(value = "开票电话")
    private String invoiceBankTel;

    @LengthTrim(max = 255,message = "开票要求最大长度只能为255位")
    @ApiModelProperty(value = "开票要求")
    private String invoiceMemo;

    @LengthTrim(min = 2,max = 50,message = "开票联系人只能为2-50位")
    @ApiModelProperty(value = "联系人")
    private String linkPerson;

    @LengthTrim(min = 3,max = 50,message = "开票联系电话能为3-50位")
    @ApiModelProperty(value = "联系电话")
    private String linkTel;

    @ApiModelProperty(value = "收票区域/省不能为空")
    private String invoiceProvince;
    @ApiModelProperty(value = "收票区域/市不能为空")
    private String invoiceCity;
    @ApiModelProperty(value = "收票区域/区不能为空")
    private String invoiceArea;
    /**
     * 收票详细地址
     */
    @ApiModelProperty(value = "收票详细地址")
    @LengthTrim(min = 2,max = 225,message = "收票详细地址只能为2-225位")
    private String linkAddress;

}
