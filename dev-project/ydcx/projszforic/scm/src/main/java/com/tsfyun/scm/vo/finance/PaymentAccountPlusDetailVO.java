package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 付款单详情
 * @since Created in 2020/4/24 10:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentAccountPlusDetailVO implements Serializable {

    /**
     * 付款单主单
     */
    private PaymentAccountVO paymentAccount;

    /**
     * 付款单明细
     */
    private List<PaymentAccountMemberVO> members;

}
