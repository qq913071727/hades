package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.InvoiceStatusEnum;
import com.tsfyun.common.base.enums.finance.InvoiceTaxTypeEnum;
import com.tsfyun.common.base.enums.finance.InvoiceTypeEnum;
import com.tsfyun.scm.util.ClientInfoStatus;
import com.tsfyun.scm.util.ClientStatusMappingUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 发票响应实体
 * </p>
 *

 * @since 2020-05-15
 */
@Data
@ApiModel(value="Invoice响应对象", description="发票响应实体")
public class InvoiceVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "结算类型")
    private String taxType;

    @ApiModelProperty(value = "申请日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime applyDate;

    @ApiModelProperty(value = "确认日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime confirmDate;

    @ApiModelProperty(value = "购货单位-买房")
    private Long buyerId;

    @ApiModelProperty(value = "购货单位名称")
    private String buyerName;

    @ApiModelProperty(value = "买方银行名称")
    private String buyerBankName;

    @ApiModelProperty(value = "买方纳税人识别号")
    private String buyerTaxpayerNo;

    @ApiModelProperty(value = "买方银行账号")
    private String buyerBankAccount;

    @ApiModelProperty(value = "买方银行地址")
    private String buyerBankAddress;

    @ApiModelProperty(value = "买方联系人")
    private String buyerLinkPerson;

    @ApiModelProperty(value = "买方联系电话")
    private String buyerBankTel;

    @ApiModelProperty(value = "卖方")
    private Long sellerId;

    @ApiModelProperty(value = "卖方银行名称")
    private String sellerBankName;

    @ApiModelProperty(value = "卖方银行账号")
    private String sellerBankAccount;

    @ApiModelProperty(value = "卖方银行地址")
    private String sellerBankAddress;

    @ApiModelProperty(value = "发票类型")
    private String invoiceType;

    @ApiModelProperty(value = "实际发票编号")
    private String invoiceNo;

    @ApiModelProperty(value = "实际开票日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime invoiceDate;

    @ApiModelProperty(value = "开票金额")
    private BigDecimal invoiceVal;

    @ApiModelProperty(value = "税点")
    private BigDecimal taxPoint;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "内贸合同号")
    private String saleContractNo;

    @ApiModelProperty(value = "快递单号")
    private String expressNo;

    @ApiModelProperty(value = "开票要求")
    private String requirement;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "客户开票确认意见")
    private String customerReason;

    /**
     * 商务人员名称
     */
    private String busPersonName;

    private String sellerName;

    private String statusDesc;

    private String taxTypeDesc;

    private String invoiceTypeDesc;

    public String getStatusDesc( ) {
        return Optional.ofNullable(InvoiceStatusEnum.of(statusId)).map(InvoiceStatusEnum::getName).orElse("");
    }

    public String getTaxTypeDesc( ) {
        return Optional.ofNullable(InvoiceTaxTypeEnum.of(taxType)).map(InvoiceTaxTypeEnum::getName).orElse("");
    }

    public String getInvoiceTypeDesc( ) {
        return Optional.ofNullable(InvoiceTypeEnum.of(invoiceType)).map(InvoiceTypeEnum::getName).orElse("");
    }

    private String clientStatusId;

    private String clientStatusDesc;

    public String getClientStatusId() {
        return ClientStatusMappingUtil.getClientInvoiceStatus(statusId);
    }

    public String getClientStatusDesc() {
        ClientInfoStatus clientInfoStatus = ClientStatusMappingUtil.INVOICE_CLENT_STATUS_MAPPING.get(getClientStatusId());
        return Optional.ofNullable(clientInfoStatus).map(ClientInfoStatus::getClientStatusDesc).orElse("");
    }
}
