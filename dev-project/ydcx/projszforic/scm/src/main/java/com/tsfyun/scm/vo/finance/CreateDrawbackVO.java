package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class CreateDrawbackVO implements Serializable {

    @ApiModelProperty(value = "客户ID")
    private Long customerId;
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    @ApiModelProperty(value = "收款方")
    private String payeeName;
    @ApiModelProperty(value = "收款银行")
    private String payeeBank;
    @ApiModelProperty(value = "收款行账号")
    private String payeeBankNo;
    @ApiModelProperty(value = "本次退税比例")
    private BigDecimal billie;
    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;
    @ApiModelProperty(value = "订单号")
    private String orderNo;
    private String clientNo;
    @ApiModelProperty(value = "采购合同号")
    private String purchaseNo;
    @ApiModelProperty(value = "要求付款时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date claimPaymentDate;
    @ApiModelProperty(value = "备注")
    private String memo;


}
