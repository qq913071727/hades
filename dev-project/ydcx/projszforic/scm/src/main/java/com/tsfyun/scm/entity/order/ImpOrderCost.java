package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 订单费用
 * </p>
 *
 *
 * @since 2020-04-21
 */
@Data
public class ImpOrderCost implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;
    /**
     * 订单
     */

    private Long impOrderId;

    /**
     * 科目
     */

    private String expenseSubjectId;

    /**
     * 费用分类
     */

    private String costClassify;

    /**
     * 实际金额
     */

    private BigDecimal actualAmount;

    /**
     * 应收金额
     */

    private BigDecimal receAmount;

    /**
     * 已收金额
     */

    private BigDecimal acceAmount;

    /**
     * 是否系统自动生成
     */

    private Boolean isAutomatic;

    /**
     * 标记-(系统预留字段)
     */

    private String mark;

    /**
     * 备注
     */

    private String memo;

    /**
     * 是否需要在账期内冲销(优先核销)
     */

    private Boolean isFirstWriteOff;

    /**
     * 是否锁定 (可以核销)(费用进入流水)
     */

    private Boolean isLock;

    /**
     * 发生日期
     */

    private LocalDateTime happenDate;

    /**
     * 账期日期
     */

    private LocalDateTime periodDate;

    /**
     * 开始计算滞纳金日期
     */

    private LocalDateTime lateFeeDate;

    /**
     * 逾期利率
     */

    private BigDecimal overdueRate;

    /**
     * 操作人
     */

    private String operator;

    /**
     * 是否允许修改
     */

    private Boolean isAllowEdit;

}
