package com.tsfyun.scm.dto.file;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Data
public class ExportExcelParamsDTO implements Serializable {
    private String fileName;//文件名称
    private String sheetName;//sheet名称
    private String operator;//操作人
    private List<ExcelKeyValue> files;//字段对应标题
    private List datas;//内容
    private List<String> total;//需要合计的数据
    private Boolean showIndex;//是否添加序号列
    public Boolean getShowIndex() {
        if(Objects.isNull(showIndex)) {
            return Boolean.TRUE;
        }
        return showIndex;
    }
}
