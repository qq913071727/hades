package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.service.order.IImpOrderService;
import com.tsfyun.scm.vo.order.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;


/**
 * <p>
 * 进口订单 前端控制器
 * </p>
 *
 * @since 2020-04-08
 */
@RestController
@RequestMapping("/impOrder")
public class ImpOrderController extends BaseController {

    @Autowired
    private IImpOrderService impOrderService;

    /**
     * 新增订单
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Long> addOrder(@RequestBody ImpOrderPlusDTO dto) {
        return success(impOrderService.add(dto));
    }

    /**
     * 修改订单
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "edit")
    Result<Void> editOrder(@RequestBody ImpOrderPlusDTO dto) {
        impOrderService.edit(dto);
        return success();
    }

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ImpOrderVO>> list(@ModelAttribute ImpOrderQTO qto) {
        PageInfo<ImpOrderVO> page = impOrderService.list(qto);
        return success((int) page.getTotal(),page.getList());
    }

    /**
     * 订单详情信息
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<ImpOrderPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(impOrderService.detailPlus(id,operation));
    }

    /**=
     * 初审审核(验货退回)
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "examine")
    public Result<Void> examine(@ModelAttribute @Valid TaskDTO dto){
        impOrderService.examine(dto);
        return success();
    }

    /**=
     * 确认进口
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirmImp")
    public Result<Void> confirmImp(@ModelAttribute @Valid TaskDTO dto){
        impOrderService.confirmImp(dto);
        return success();
    }

    /**=
     * 根据订单模糊查询(带出客户)
     * @param orderNo
     * @return
     */
    @PostMapping(value = "selectByOrderNo")
    public Result<List<ImpOrderSimpleVO>> selectByOrderNo(@RequestParam(value = "orderNo",required = false)String orderNo){
        return success(impOrderService.selectByOrderNo(orderNo));
    }

    /**=
     * 分摊净重
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "shareNetWeight")
    public Result<Void> shareNetWeight(@RequestParam(value = "id")Long id,@RequestParam(value = "netWeight") BigDecimal netWeight){
        impOrderService.shareNetWeight(id,netWeight);
        return success();
    }
    /**=
     * 分摊毛重
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "shareGrossWeight")
    public Result<Void> shareGrossWeight(@RequestParam(value = "id")Long id,@RequestParam(value = "grossWeight") BigDecimal grossWeight){
        impOrderService.shareGrossWeight(id,grossWeight);
        return success();
    }

    /**=
     * 批量填写入库单号
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "batchReceivingNo")
    public Result<Void> batchReceivingNo(@RequestParam(value = "id")Long id,@RequestParam(value = "receivingNo") String receivingNo){
        impOrderService.batchReceivingNo(id,receivingNo);
        return success();
    }

    /**=
     * 明细入库单绑定
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "saveBindMember")
    public Result<Void> saveBindMember(@RequestBody @Valid BindMemberDTO dto){
        impOrderService.saveBindMember(dto);
        return success();
    }

    /**
     * 获取需要拆分数量的明细
     * @return
     */
    @GetMapping(value = "obtainSplitMember")
    public Result<ImpOrderMemberVO> obtainSplitMember(@RequestParam(value = "id")Long id){
        return success(impOrderService.obtainSplitMember(id));
    }

    @DuplicateSubmit
    @PostMapping(value = "saveSplitMember")
    public Result<Void> saveSplitMember(@RequestBody @Valid OrderInspectionMemberDTO dto){
        impOrderService.saveSplitMember(dto);
        return success();
    }

    /**=
     * 保存验货明细修改
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "saveInspectionMemberEdit")
    public Result<Void> saveInspectionMemberEdit(@RequestBody @Valid OrderInspectionMemberDTO dto){
        impOrderService.saveInspectionMemberEdit(dto);
        return success();
    }
    /**
     * 辅助验货
     * @return
     */
    @PostMapping(value = "auxiliaryInspection",produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<InspectionPlusVO> auxiliaryInspection(
            @RequestPart(name = "file",required = true) MultipartFile file,
            @RequestParam(name = "orderId",required = true) Long orderId){
        return success(impOrderService.auxiliaryInspection(file,orderId));
    }

    /**=
     * 确认验货完成
     * @param id
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirmInspection")
    public Result<Void> confirmInspection(@RequestParam(value = "id")Long id){
        impOrderService.confirmInspection(id);
        return success();
    }

    /**=
     * 设置申报信息
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "setupDeclaration")
    public Result<Void> setupDeclaration(@ModelAttribute @Valid SetupDeclarationDTO dto){
        impOrderService.setupDeclaration(dto);
        return success();
    }

    /**
     * 下单根据模板导入订单明细
     * @param file
     * @return
     */
    @PostMapping(value = "importMember")
    public Result<List<ImportOrderMemberVO>> importMember(@RequestPart(value = "file") MultipartFile file){
        return success(impOrderService.readDataFromMemberImport(file));
    }

    /**
     * 初始化自助拆单
     * @return
     */
    @PostMapping(value = "initSelfHelpSplit")
    public Result<List<SplitOrderMemberVO>> initSelfHelpSplit(@RequestParam(value = "orderId")Long orderId, @RequestParam(value = "quantity")Integer quantity){
        return success(impOrderService.initSelfHelpSplit(orderId,quantity));
    }

    /**
     * 保存拆单
     * @return
     */
    @PostMapping(value = "saveSelfHelpSplit")
    public Result<Void> saveSelfHelpSplit(@RequestBody @Validated SelfHelpSplitDTO dto){
        impOrderService.saveSelfHelpSplit(dto);
        return success();
    }

    /**
     * 删除订单
     * @param id
     * @return
     */
    @PostMapping("/remove")
    @ApiOperation("删除订单")
    public Result<Void> remove(@RequestParam("id")Long id){
        impOrderService.remove(id);
        return success();
    }


    /**
     * 打印订单委托书
     * @param id
     * @return
     */
    @PostMapping("/getPrintEntrustData")
    @ApiOperation("打印订单委托书")
    public Result<ImpOrderEntrustVO> getPrintEntrustData(@RequestParam("id")Long id){
        return success(impOrderService.getPrintEntrustData(id));
    }

    /**
     * 导出订单委托书
     * @param id
     * @return
     */
    @PostMapping("/exportEntrustExcel")
    @ApiOperation("导出订单委托书")
    public Result<Long> exportEntrustExcel(@RequestParam("id")Long id){
        return success(impOrderService.exportEntrustExcel(id));
    }

    /**
     * 统计订单金额
     * @param qto
     * @return
     */
    @PostMapping("/summaryOrderAmount")
    @ApiOperation("统计订单金额")
    public Result<BigDecimal> summaryOrderAmount(@ModelAttribute ImpOrderQTO qto){
        return success(impOrderService.summaryOrderAmount(qto));
    }

}

