package com.tsfyun.scm.controller.wms;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.wms.DeliveryNoteQTO;
import com.tsfyun.scm.service.wms.IDeliveryNoteService;
import com.tsfyun.scm.vo.wms.DeliveryNotePlusDetailVO;
import com.tsfyun.scm.vo.wms.DeliveryNoteVO;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 出库单 前端控制器
 * </p>
 *
 *
 * @since 2020-04-24
 */
@RestController
@RequestMapping("/deliveryNote")
public class DeliveryNoteController extends BaseController {

    private final IDeliveryNoteService deliveryNoteService;

    public DeliveryNoteController(IDeliveryNoteService deliveryNoteService) {
        this.deliveryNoteService = deliveryNoteService;
    }

    /**
     * 进口出库分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<DeliveryNoteVO>> pageList(@ModelAttribute DeliveryNoteQTO qto) {
        PageInfo<DeliveryNoteVO> page = deliveryNoteService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }


    /**
     * 详情信息（含明细）
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<DeliveryNotePlusDetailVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(deliveryNoteService.detail(id,operation));
    }

}

