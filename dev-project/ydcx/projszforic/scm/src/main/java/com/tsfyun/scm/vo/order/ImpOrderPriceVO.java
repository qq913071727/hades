package com.tsfyun.scm.vo.order;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ImpOrderPriceStatusEnum;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import com.tsfyun.scm.entity.order.ImpOrderPrice;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 订单审价响应实体
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Data
@ApiModel(value="ImpOrderPrice响应对象", description="订单审价响应实体")
public class ImpOrderPriceVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "客户")
    private Long customerId;
    @ApiModelProperty(value = "客户")
    private String customerName;

    @ApiModelProperty(value = "供应商")
    private Long supplierId;
    @ApiModelProperty(value = "供应商")
    private String supplierName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;
    private String statusDesc;
    public String getStatusDesc(){
        ImpOrderPriceStatusEnum impOrderPriceStatusEnum = ImpOrderPriceStatusEnum.of(getStatusId());
        return Objects.nonNull(impOrderPriceStatusEnum)? impOrderPriceStatusEnum.getName():"";
    }

    @ApiModelProperty(value = "订单")
    private Long orderId;

    @ApiModelProperty(value = "订单号")
    private String orderDocNo;

    @ApiModelProperty(value = "审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime reviewTime;

    @ApiModelProperty(value = "提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "审核人")
    private String reviewer;

    @ApiModelProperty(value = "提交次数")
    private Integer submitCount;

    /**
     * 商务人员名称
     */
    private String busPersonName;


}
