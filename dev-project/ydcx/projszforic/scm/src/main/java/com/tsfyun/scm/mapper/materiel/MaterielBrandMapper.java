package com.tsfyun.scm.mapper.materiel;

import com.tsfyun.scm.entity.materiel.MaterielBrand;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 物料品牌 Mapper 接口
 * </p>
 *

 * @since 2020-03-26
 */
@Repository
public interface MaterielBrandMapper extends Mapper<MaterielBrand> {

    void insertOrUpdate(MaterielBrand mb);

}
