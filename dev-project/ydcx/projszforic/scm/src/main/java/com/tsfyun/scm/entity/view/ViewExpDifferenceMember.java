package com.tsfyun.scm.entity.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 出口票差明细
 * </p>
 *
 *
 * @since 2021-10-18
 */
@Data
public class ViewExpDifferenceMember implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private Long id;

    /**
     * 订单编号
     */

    private String docNo;

    /**
     * 订单日期-由后端写入
     */

    private Date orderDate;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 采购合同号
     */

    private String purchaseContractNo;

    /**
     * 采购合同总价
     */

    private BigDecimal purchaseCny;

    /**
     * 结汇人民币金额
     */

    private BigDecimal settleAccountCny;

    /**
     * 实际采购金额
     */

    private BigDecimal actualPurchaseCny;

    /**
     * 生成的票差金额
     */

    private BigDecimal differenceVal;

    /**
     * 票差调整金额
     */

    private BigDecimal adjustmentVal;


}
