package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/17 09:19
 */
@Data
public class ImportDeclarationNoExcel extends BaseRowModel implements Serializable {

    @NotEmptyTrim(message = "系统单号不能为空")
    @LengthTrim(max = 20,message = "系统单号长度不能超过20位")
    @ExcelProperty(value = "系统单号", index = 0)
    private String docNo;

    @NotEmptyTrim(message = "报关单号不能为空")
    @LengthTrim(max = 50,message = "报关单号长度不能超过50位")
    @ExcelProperty(value = "报关单号", index = 1)
    private String declarationNo;


}
