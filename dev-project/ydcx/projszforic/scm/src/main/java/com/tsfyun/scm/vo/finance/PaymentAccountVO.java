package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.PaymentAccountStatusEnum;
import com.tsfyun.common.base.enums.finance.CounterFeeTypeEnum;
import com.tsfyun.common.base.enums.finance.PaymentTermEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 付款单响应实体
 * </p>
 *

 * @since 2020-04-23
 */
@Data
@ApiModel(value="PaymentAccount响应对象", description="付款单响应实体")
public class PaymentAccountVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "付款提交时间")
    private LocalDateTime accountDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "实际付出时间")
    private LocalDateTime realAccountDate;

    @ApiModelProperty(value = "付款人")
    private String payer;

    @ApiModelProperty(value = "付款银行名称")
    private String payerBankName;

    @ApiModelProperty(value = "付款银行账号")
    private String payerBankAccountNo;

    @ApiModelProperty(value = "收款人")
    private Long payeeId;

    @ApiModelProperty(value = "收款人")
    private String payee;

    @ApiModelProperty(value = "收款行")
    private Long payeeBankId;

    @ApiModelProperty(value = "收款行名称")
    private String payeeBankName;

    @ApiModelProperty(value = "收款行账号")
    private String payeeBankAccountNo;

    @ApiModelProperty(value = "收款行地址")
    private String payeeBankAddress;

    @ApiModelProperty(value = "swift code")
    private String swiftCode;

    @ApiModelProperty(value = "bank code")
    private String ibankCode;

    @ApiModelProperty(value = "付款方式")
    private String paymentTerm;

    @ApiModelProperty(value = "付款方式天数")
    private Integer paymentTermDay;

    @ApiModelProperty(value = "到期日期")
    private LocalDateTime dueDate;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "结算汇率")
    private BigDecimal exchangeRate;

    @ApiModelProperty(value = "调整前汇率")
    private BigDecimal oldExchangeRate;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制名称")
    private String currencyName;

    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "付款人民币金额")
    private BigDecimal accountValueCny;

    @ApiModelProperty(value = "手续费承担方式")
    private String counterFeeType;
    private String counterFeeTypeDesc;

    @ApiModelProperty(value = "付款手续费金额")
    private BigDecimal bankFee;

    @ApiModelProperty(value = "实际付款金额")
    private BigDecimal actualAccountValue;

    @ApiModelProperty(value = "实际付款币制")
    private String actualCurrencyId;

    @ApiModelProperty(value = "实际付款币制")
    private String actualCurrencyName;

    @ApiModelProperty(value = "是否虚拟付汇")
    private Boolean fictitious;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String statusDesc;

    private String paymentTermDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(PaymentAccountStatusEnum.of(statusId)).map(PaymentAccountStatusEnum::getName).orElse("");
    }

    public String getPaymentTermDesc(){
        return Optional.ofNullable(PaymentTermEnum.of(paymentTerm)).map(PaymentTermEnum::getName).orElse("");
    }

    public String getCounterFeeTypeDesc(){
        return Optional.ofNullable(CounterFeeTypeEnum.of(counterFeeType)).map(CounterFeeTypeEnum::getName).orElse("");
    }

}
