package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 发票明细请求实体
 * </p>
 *
 * @since 2020-05-18
 */
@Data
@ApiModel(value="InvoiceMember请求对象", description="发票明细请求实体")
public class InvoiceMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "行号不能为空")
   @LengthTrim(max = 4,message = "行号最大长度不能超过4位")
   @ApiModelProperty(value = "行号")
   private Integer rowNo;

   @NotNull(message = "发票id不能为空")
   @ApiModelProperty(value = "发票id")
   private Long invoiceId;

   @LengthTrim(max = 100,message = "物料型号最大长度不能超过100位")
   @ApiModelProperty(value = "物料型号")
   private String goodsModel;

   @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
   @ApiModelProperty(value = "物料名称")
   private String goodsName;

   @LengthTrim(max = 100,message = "物料品牌最大长度不能超过100位")
   @ApiModelProperty(value = "物料品牌")
   private String goodsBrand;

   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @Digits(integer = 6, fraction = 4, message = "单价整数位不能超过6位，小数位不能超过4位")
   @ApiModelProperty(value = "单价")
   private BigDecimal unitPrice;

   @LengthTrim(max = 10,message = "单位编码最大长度不能超过10位")
   @ApiModelProperty(value = "单位编码")
   private String unitCode;

   @NotEmptyTrim(message = "单位名称不能为空")
   @LengthTrim(max = 50,message = "单位名称最大长度不能超过50位")
   @ApiModelProperty(value = "单位名称")
   private String unitName;

   @Digits(integer = 8, fraction = 2, message = "总价整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "总价")
   private BigDecimal totalPrice;

   @NotEmptyTrim(message = "税收分类编码不能为空")
   @LengthTrim(max = 20,message = "税收分类编码最大长度不能超过20位")
   @ApiModelProperty(value = "税收分类编码")
   private String taxCode;

   @NotEmptyTrim(message = "开票名称不能为空")
   @LengthTrim(max = 100,message = "开票名称最大长度不能超过100位")
   @ApiModelProperty(value = "开票名称")
   private String invoiceName;


}
