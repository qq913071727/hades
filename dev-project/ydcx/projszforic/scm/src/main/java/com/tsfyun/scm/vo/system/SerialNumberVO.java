package com.tsfyun.scm.vo.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-03-16
 */
@Data
@ApiModel(value="SerialNumber响应对象", description="响应实体")
public class SerialNumberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

     private String name;

    @ApiModelProperty(value = "前缀")
    private String prefix;

    @ApiModelProperty(value = "日期规则")
    private String timeRule;

    private String timeRuleName;

    @ApiModelProperty(value = "流水号长度")
    private Integer serialLength;

    @ApiModelProperty(value = "流水号重置方式")
    private String serialClear;
    private String serialClearName;

    @ApiModelProperty(value = "当前流水号大小")
    private Integer nowSerial;

    @ApiModelProperty(value = "结果展示")
    private String result;


}
