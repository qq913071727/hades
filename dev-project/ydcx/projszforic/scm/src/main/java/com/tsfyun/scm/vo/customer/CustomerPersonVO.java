package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @since Created in 2020/4/1 17:57
 */
@Data
public class CustomerPersonVO implements Serializable {

    private Long id;

    private String name;

    private Long salePersonId;

    private String salePersonName;

    private Long busPersonId;

    private String busPersonName;





}
