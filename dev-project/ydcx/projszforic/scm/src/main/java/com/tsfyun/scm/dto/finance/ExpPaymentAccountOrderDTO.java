package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 付款订单明细请求实体
 * </p>
 *
 *
 * @since 2021-11-17
 */
@Data
@ApiModel(value="ExpPaymentAccountOrder请求对象", description="付款订单明细请求实体")
public class ExpPaymentAccountOrderDTO implements Serializable {

   private static final long serialVersionUID=1L;

    @NotNull(message = "id不能为空",groups = {UpdateGroup.class})
    private Long id;

   @NotNull(message = "订单ID不能为空")
   @ApiModelProperty(value = "订单ID")
   private Long orderId;

   @NotNull(message = "付款金额不能为空")
   @Digits(integer = 17, fraction = 2, message = "付款金额整数位不能超过17位，小数位不能超过2位")
   @ApiModelProperty(value = "付款金额")
   private BigDecimal accountValue;

   @NotNull(message = "结汇汇率不能为空")
   @Digits(integer = 4, fraction = 6, message = "结汇汇率整数位不能超过4位，小数位不能超过6位")
   @ApiModelProperty(value = "结汇汇率")
   private BigDecimal customsRate;

   @NotNull(message = "付款单主单ID不能为空")
   @ApiModelProperty(value = "付款单主单ID")
   private Long paymentAccountId;


}
