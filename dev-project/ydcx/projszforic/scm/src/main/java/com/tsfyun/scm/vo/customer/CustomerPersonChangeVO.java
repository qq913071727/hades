package com.tsfyun.scm.vo.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description:
 * @since Created in 2020/4/1 17:28
 */
@Data
public class CustomerPersonChangeVO implements Serializable {

    @JsonIgnore
    private Integer personType;

    private Long oldPersonId;

    private String oldPersonName;

    private Long nowPersonId;

    private String nowPersonName;

    @JsonIgnore
    private String createBy;

    private String operator;

    private LocalDateTime dateCreated;

    private String memo;

}
