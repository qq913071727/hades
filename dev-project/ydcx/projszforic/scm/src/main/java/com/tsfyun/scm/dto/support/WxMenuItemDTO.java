package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.WxMenuEnum;
import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/31 16:24
 */
@Data
public class WxMenuItemDTO implements Serializable {

    /**
     * 菜单类型-WxMenuEnum
     * view-网页类型 click-点击类型 miniprogram-小程序类型
     */
    @NotEmptyTrim(message = "菜单类型不能为空")
    @EnumCheck(clazz = WxMenuEnum.class,message = "菜单类型错误")
    private String type;

    /**
     * 菜单名称（一级菜单不能超过4个汉字，不能超过7个汉字）
     */
    @NotEmptyTrim(message = "菜单名称不能为空")
    private String name;

    /**
     * click点击类型必传
     */
    private String key;

    /**
     * view和miniprogram必传
     */
    private String url;

    /**
     * miniprogram必传
     */
    private String appid;

    /**
     * miniprogram必传
     */
    private String pagepath;

    /**
     * 素材id
     */
    private String media_id;

    /**
     * 子菜单
     */
    @Size(max = 5,message = "最多只能添加5个二级菜单")
    private List<WxMenuItemDTO> sub_button;

}
