package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.ReceiptAccountDTO;
import com.tsfyun.scm.dto.finance.ReceiptAccountQTO;
import com.tsfyun.scm.entity.finance.ReceiptAccount;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.ReceiptAccountDetailPlusVO;
import com.tsfyun.scm.vo.finance.ReceiptAccountVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 收款单 服务类
 * </p>
 * @author z.mark
 * @since 2020-04-15
 */
public interface IReceiptAccountService extends IService<ReceiptAccount> {

    /**
     * 收款登记
     * @param dto
     * @return
     */
    Long register(ReceiptAccountDTO dto);

    /**
     * 分页
     * @param qto
     * @return
     */
    PageInfo<ReceiptAccountVO> pageList(ReceiptAccountQTO qto);

    /**
     * 详情
     * @param id
     * @return
     */
    ReceiptAccountVO detail(Long id);
    ReceiptAccountVO detail(Long id,String operation);

    /**=
     * 收款单确认
     * @param dto
     */
    void confirm(TaskDTO dto);

    /**=
     * 根据客户获取未核销金额
     * @param customerId
     * @return
     */
    BigDecimal obtainCustomerReceipts(Long customerId);

    /**=
     * 根据客户获取可核销收款单
     * @param customerId
     * @return
     */
    List<ReceiptAccount> canWriteOffList(Long customerId);

    /**
     * 根据收款单id获取收款单主单和明细信息
     * @param id
     * @return
     */
    ReceiptAccountDetailPlusVO plusDetail(Long id);

    /**
     * 导出收款单Excel
     * @param qto
     * @return
     */
    Long exportExcel(ReceiptAccountQTO qto);

    /**=
     * 取消核销金额
     * @param id
     * @param writeValue
     */
    void cancelWriteValue(Long id,BigDecimal writeValue);

    /**
     * 增加核销金额
     * @param id
     * @param writeValue
     */
    void writeValue(Long id,BigDecimal writeValue);

    /**
     * 删除收款单
     * @param id
     */
    void removeAllById(Long id);
}
