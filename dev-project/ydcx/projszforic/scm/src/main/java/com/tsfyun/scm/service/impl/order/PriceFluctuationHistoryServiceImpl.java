package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.scm.mapper.order.PriceFluctuationHistoryMapper;
import com.tsfyun.scm.service.order.IPriceFluctuationHistoryService;
import com.tsfyun.common.base.extension.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 价格波动历史 服务实现类
 * </p>
 *
 *
 * @since 2020-04-17
 */
@Service
public class PriceFluctuationHistoryServiceImpl extends ServiceImpl<PriceFluctuationHistory> implements IPriceFluctuationHistoryService {


    @Override
    public List<PriceFluctuationHistory> list(Long memberId) {
        PriceFluctuationHistory query = new PriceFluctuationHistory();
        query.setOrderPriceMemberId(memberId);
        List<PriceFluctuationHistory> list = list(query);
        if(CollUtil.isNotEmpty(list)){
            list = list.stream().sorted(Comparator.comparing(PriceFluctuationHistory::getOrderDate).reversed()).collect(Collectors.toList());
        }
        return list;
    }
}
