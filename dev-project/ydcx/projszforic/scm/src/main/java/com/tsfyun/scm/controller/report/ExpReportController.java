package com.tsfyun.scm.controller.report;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ExpOrderReceiveQTO;
import com.tsfyun.scm.dto.report.ExpOneVoteTheEndQTO;
import com.tsfyun.scm.dto.view.ViewExpDifferenceMemberQTO;
import com.tsfyun.scm.service.report.IExpReportService;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import com.tsfyun.scm.vo.view.ViewExpDifferenceMemberVO;
import com.tsfyun.scm.vo.view.ViewExpDifferenceSummaryVO;
import com.tsfyun.scm.vo.view.ViewExpOrderCostMemberTotalVO;
import com.tsfyun.scm.vo.view.ViewExpOrderCostMemberVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/18 13:40
 */
@RestController
@RequestMapping(value = "/expReport")
public class ExpReportController extends BaseController {

    private final IExpReportService expReportService;

    public ExpReportController(IExpReportService expReportService) {
        this.expReportService = expReportService;
    }

    /**
     * 客户票差明细报表
     * @return
     */
    @PostMapping(value = "expDifferenceMember")
    public Result<List<ViewExpDifferenceMemberVO>> expDifferenceMember(@ModelAttribute @Validated ViewExpDifferenceMemberQTO qto){
        PageInfo<ViewExpDifferenceMemberVO> pageInfo = expReportService.expDifferenceMemberPage(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 客户票差总额
     * @return
     */
    @PostMapping(value = "obtainCustomerExpDifferenceVal")
    public Result<BigDecimal> obtainCustomerExpDifferenceVal(@RequestParam(value = "customerId")Long customerId){
        return success(expReportService.obtainCustomerExpDifferenceVal(customerId));
    }

    /**
     * 客户票差明细报表导出Excel
     * @return
     */
    @PostMapping(value = "expDifferenceMemberExport")
    public Result<Long> expDifferenceMemberExport(@ModelAttribute @Validated ViewExpDifferenceMemberQTO qto) throws Exception {
        return success(expReportService.exportExpDifferenceMember(qto));
    }

    /**
     * 客户票差汇总报表
     * @return
     */
    @PostMapping(value = "expDifferenceSummary")
    public Result<List<ViewExpDifferenceSummaryVO>> expDifferenceSummary(@ModelAttribute @Validated ViewExpDifferenceMemberQTO qto){
        PageInfo<ViewExpDifferenceSummaryVO> pageInfo = expReportService.expDifferenceSummaryPage(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 客户票差汇总报表导出Excel
     * @return
     */
    @PostMapping(value = "expDifferenceSummaryExport")
    public Result<Long> expDifferenceSummaryExport(@ModelAttribute @Validated ViewExpDifferenceMemberQTO qto) throws Exception {
        return success(expReportService.exportExpDifferenceSummary(qto));
    }

    /**
     * 出口订单应收
     * @return
     */
    @PostMapping(value = "orderReceivable")
    public Result<List<ViewExpOrderCostMemberVO>> orderReceivableList(@ModelAttribute @Validated ExpOrderReceiveQTO qto){
        PageInfo<ViewExpOrderCostMemberVO> pageInfo = expReportService.orderReceivableList(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }


    /**
     * 出口订单应收报表
     * @return
     */
    @PostMapping(value = "orderReceivableTotal")
    public Result<ViewExpOrderCostMemberTotalVO> orderReceivableTotal(@ModelAttribute @Validated ExpOrderReceiveQTO qto){
        return success(expReportService.orderReceivableTotal(qto));
    }

    /**
     * 出口订单应收报表导出Excel
     * @return
     */
    @PostMapping(value = "orderReceivableExport")
    public Result<Long> orderReceivableExport(@ModelAttribute @Validated ExpOrderReceiveQTO qto) throws Exception {
        return success(expReportService.orderReceivableExport(qto));
    }

    /**
     * 出口一票到底台账
     * @return
     */
    @PostMapping(value = "oneVoteTheEnd")
    public Result<List<ExpOneVoteTheEndVO>> oneVoteTheEnd(@ModelAttribute @Validated ExpOneVoteTheEndQTO qto){
        return expReportService.oneVoteTheEnd(qto);
    }

    /**
     * 出口一票到底台账导出Excel
     * @return
     */
    @PostMapping(value = "oneVoteTheEndExport")
    public Result<Long> oneVoteTheEndExport(@ModelAttribute @Validated ExpOneVoteTheEndQTO qto) throws Exception {
        return success(expReportService.exportOneVoteTheEnd(qto));
    }
}
