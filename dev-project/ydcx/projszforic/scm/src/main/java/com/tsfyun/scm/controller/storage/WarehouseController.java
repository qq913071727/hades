package com.tsfyun.scm.controller.storage;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.storage.WarehouseDTO;
import com.tsfyun.scm.dto.storage.WarehouseQTO;
import com.tsfyun.scm.entity.storage.Warehouse;
import com.tsfyun.scm.service.storage.IWarehouseService;
import com.tsfyun.scm.vo.storage.WarehouseVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 仓库 前端控制器
 * </p>
 *
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/warehouse")
public class WarehouseController extends BaseController {

    @Autowired
    private IWarehouseService warehouseService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     *
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<WarehouseVO>> pageList(@ModelAttribute WarehouseQTO qto) {
        PageInfo<Warehouse> page = warehouseService.page(qto);
        return success((int) page.getTotal(), beanMapper.mapAsList(page.getList(), WarehouseVO.class));
    }

    /**
     * =
     * 根据仓库类型查询所有启用的仓库下拉
     *
     * @param dto
     * @return
     */
    @RequestMapping(value = "allList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<WarehouseVO>> allList(@ModelAttribute WarehouseQTO dto) {
        return success(warehouseService.allList(dto));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@ModelAttribute @Valid WarehouseDTO dto) {
        warehouseService.add(dto);
        return success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) WarehouseDTO dto) {
        warehouseService.edit(dto);
        return success();
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam(value = "id") Long id) {
        warehouseService.delete(id);
        return success();
    }

    /**
     * 启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id") Long id, @RequestParam("disabled") Boolean disabled) {
        warehouseService.updateDisabled(id, disabled);
        return success();
    }

    /**
     * 详情
     */
    @GetMapping("/detail")
    public Result<WarehouseVO> detail(@RequestParam(value = "id") Long id) {
        return success(warehouseService.detail(id));
    }

    /**
     * 获取所有国内自提仓库
     */
    @GetMapping("/selfWarehouses")
    public Result<List<WarehouseVO>> selfWarehouses() {
        return success(warehouseService.domesticSelfMentionWarehouses());
    }

    /**
     * 获取所有启用的仓库信息（登录不拦截）
     */
    @GetMapping("/all")
    @ApiOperation("获取所有启用的仓库")
    public Result<List<WarehouseVO>> all(WarehouseQTO qto) {
        return success(warehouseService.allEnableWarehouses(qto));
    }

}

