package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.util.Date;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 出口付款单请求实体
 * </p>
 *
 *
 * @since 2021-10-18
 */
@Data
@ApiModel(value="ExpPaymentAccount请求对象", description="出口付款单请求实体")
public class ExpPaymentAccountDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "收款单ID不能为空")
   @ApiModelProperty(value = "收款单ID")
   private Long overseasAccountId;

   @ApiModelProperty(value = "要求付款时间")
   @NotNull(message = "要求付款时间不能为空")
   private Date claimPaymentDate;

   @NotEmptyTrim(message = "收款方不能为空")
   @LengthTrim(max = 255,message = "收款方最大长度不能超过255位")
   @ApiModelProperty(value = "收款方")
   private String payeeName;

   @NotEmptyTrim(message = "收款银行不能为空")
   @LengthTrim(max = 100,message = "收款银行最大长度不能超过100位")
   @ApiModelProperty(value = "收款银行")
   private String payeeBank;

   @NotEmptyTrim(message = "收款银行账号不能为空")
   @LengthTrim(max = 50,message = "收款银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "收款银行账号")
   private String payeeBankNo;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;

}
