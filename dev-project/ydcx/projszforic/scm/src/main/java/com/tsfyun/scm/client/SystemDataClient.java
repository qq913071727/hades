package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.client.fallback.SystemDataClientFallback;
import com.tsfyun.scm.system.vo.CountryVO;
import com.tsfyun.scm.system.vo.TaxCodeBaseVO;
import com.tsfyun.scm.system.vo.UnitVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Component
@FeignClient(name = "scm-system",fallbackFactory = SystemDataClientFallback.class)
public interface SystemDataClient {

    /**
     * 获取国家配置数据
     * @return
     */
    @GetMapping(value = "/country/select")
    Result<List<CountryVO>> countrySelect();

    /**
     * 获取单位配置数据
     * @return
     */
    @GetMapping(value = "/unit/select")
    Result<List<UnitVO>> unitSelect();

    /**
     * 获取税收分类基础数据
     * @return
     */
    @GetMapping(value = "/taxCodeBase/select")
    Result<List<TaxCodeBaseVO>> taxSelect();


}
