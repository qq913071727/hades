package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AdjustBankFeeDTO implements Serializable {
    private Long id;

    @NotNull(message = "调整后手续费不能为空")
    @Digits(integer = 10, fraction = 2, message = "调整后手续费整数位不能超过10位，小数位不能超过6位")
    @ApiModelProperty(value = "调整后手续费")
    private BigDecimal adjustBankFee;

    @ApiModelProperty(value = "调整原因")
    @LengthTrim(max = 500,message = "调整原因最大长度不能超过500位")
    private String adjustInfo;
}
