package com.tsfyun.scm.controller.order;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 出口订单物流信息 前端控制器
 * </p>
 *
 *
 * @since 2021-09-13
 */
@RestController
@RequestMapping("/expOrderLogistics")
public class ExpOrderLogisticsController extends BaseController {

}

