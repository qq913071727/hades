package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 绑定入库单辅助表响应实体
 * </p>
 *
 *
 * @since 2020-05-06
 */
@Data
@ApiModel(value="OrderMemberReceivingNo响应对象", description="绑定入库单辅助表响应实体")
public class OrderMemberReceivingNoVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "订单明细")
    private Long orderMemberId;

    @ApiModelProperty(value = "入库单号")
    private String receivingNo;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;


}
