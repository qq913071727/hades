package com.tsfyun.scm.vo.wms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

/**=
 * 入库单验货信息
 */
@Data
public class InspectionNoteMemberVO implements Serializable {

    //统一主键生成策略
    private Long id;

    private Integer rowNo;

    /**
     * 入库单
     */

    private Long receivingNoteId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 客户物料号
     */

    private String goodsCode;

    /**
     * 产地
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;

    private String orderNo;//订单号

    private Boolean isLock;//绑单锁定

    @ApiModelProperty(value = "已绑定订单数量")
    private BigDecimal bindQuantity;

    private BigDecimal bindableQuantity;//可绑定数量
    public BigDecimal getBindableQuantity(){
        return getQuantity().subtract(getBindQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
    }
}
