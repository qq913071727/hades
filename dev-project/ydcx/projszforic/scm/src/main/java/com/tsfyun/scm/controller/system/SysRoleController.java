package com.tsfyun.scm.controller.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.system.RoleDTO;
import com.tsfyun.scm.dto.system.RoleSaveDTO;
import com.tsfyun.scm.dto.system.UserBindRoleDTO;
import com.tsfyun.scm.entity.system.SysRole;
import com.tsfyun.scm.service.system.ISysRoleService;
import com.tsfyun.scm.vo.system.SysRoleInfoVO;
import com.tsfyun.scm.vo.system.SysRoleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/role")
public class SysRoleController extends BaseController {

    @Autowired
    private ISysRoleService sysRoleService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<SysRoleVO>> pageList(@ModelAttribute RoleDTO dto) {
        PageInfo<SysRole> page = sysRoleService.pageList(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),SysRoleVO.class));
    }

    /**=
     * 查询所有角色列表
     * @param dto
     * @return
     */
    @PostMapping(value = "allList")
    public Result<List<SysRoleVO>> allList(@ModelAttribute RoleDTO dto) {
        return success(sysRoleService.list(dto));
    }
    /**=
     * 根据用户获取角色
     * @return
     */
    @PostMapping(value = "userRoles")
    public  Result<List<SysRoleVO>> userRoles(@RequestParam(value = "personId")Long personId){
        List<SysRoleVO> sysRoleVOList = new ArrayList<>();
        List<SysRole> sysRoleList = sysRoleService.getUserRoles(personId);
        if(!CollectionUtils.isEmpty(sysRoleList)){
            sysRoleVOList = beanMapper.mapAsList(sysRoleList,SysRoleVO.class);
        }
        return success(sysRoleVOList);
    }

    /**
     * 保存角色
     */
    @PostMapping("/add")
    public Result<Void> add(@ModelAttribute @Valid RoleSaveDTO role){
        sysRoleService.saveRole(role);
        return success();
    }

    /**
     * 修改角色
     */
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) RoleSaveDTO role){
        sysRoleService.updateRole(role);
        return success();
    }

    /**
     * 修改角色启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")String id,@RequestParam("disabled")Boolean disabled){
        sysRoleService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 删除角色（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam(value = "id") String id){
        sysRoleService.removeIds(Arrays.asList(id));
        return success();
    }

    /**
     * 角色下拉框
     * @return
     */
    @GetMapping("select")
    public Result<List<SysRoleVO>> select() {
        List<SysRole> list = sysRoleService.list();
        return success(CollectionUtils.isEmpty(list) ? null : beanMapper.mapAsList(list,SysRoleVO.class).stream().filter(r-> Objects.equals(r.getDisabled(),Boolean.FALSE.booleanValue())).collect(Collectors.toList()));
    }

    /**
     * 角色信息（含菜单）
     * @return
     */
    @GetMapping("detail")
    public Result<SysRoleInfoVO> detail(@RequestParam(value = "id")String id) {
        return success(sysRoleService.roleInfo(id));
    }

    /**
     * 用户绑定角色
     * @return
     */
    @PostMapping("bindRole")
    public Result<Void> bindRole(@ModelAttribute @Valid UserBindRoleDTO dto) {
        sysRoleService.userBindRole(dto);
        return success();
    }

}
