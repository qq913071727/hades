package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ApplyPaymentVO implements Serializable {

    private Long customerId;//客户ID
    private String customerName;//客户名称
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime accountDate;//申请时间
    private BigDecimal exchangeRate;//付汇汇率
    private BigDecimal accountValue;//付款金额
    private BigDecimal accountValueCny;//付款金额(人民币)
    private String currencyId;//币制
    private String currencyName;//币制
    private Long payeeId;//收款人
    private String payeeName;//收款人

    private Long payeeBankId;//收款银行
    private String payeeBankName;//收款银行
    private String payeeBankAccountNo;//收款账号
    private String swiftCode;//SWIFT CODE
    private String ibankCode;//IBANK CODE
    private String payeeBankAddress;//收款行地址
    //汇率时间
    private String goodsRateTime;
}
