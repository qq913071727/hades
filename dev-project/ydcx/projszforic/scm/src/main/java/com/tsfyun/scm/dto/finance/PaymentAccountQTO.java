package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.enums.finance.PaymentTermEnum;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: 付款单查询请求实体
 * @since Created in 2020/4/23 18:01
 */
@Data
public class PaymentAccountQTO  extends PaginationDto implements Serializable {

    private String customerName;

    private String docNo;

    private String statusId;

    @EnumCheck(clazz = PaymentTermEnum.class,message = "付款方式参数错误")
    private String paymentTerm;

    /**
     * 付款开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime accountDateStart;

    /**
     * 付款结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime accountDateEnd;

    public void setAccountDateEnd(LocalDateTime accountDateEnd) {
        if(accountDateEnd != null){
            this.accountDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(accountDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
