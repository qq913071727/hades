package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.client.fallback.RateClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 汇率远程调用
 * @since Created in 2020/4/10 13:57
 */
@Component
@FeignClient(name = "scm-system",fallbackFactory = RateClientFallback.class)
public interface RateClient {

    /**=
     * 根据币制获取本月海关汇率
     * @param currencyId
     * @return
     */
    @PostMapping("/customsRate/thisMonth")
    Result<BigDecimal> thisMonth(@RequestParam(value = "currencyId")String currencyId);

    /**=
     * 根据币制和日期获取汇率
     * @param currencyId
     * @param date
     * @return
     */
    @PostMapping("/customsRate/obtain")
    Result<BigDecimal> obtain(@RequestParam(value = "currencyId")String currencyId,@RequestParam(value = "date") Date date);
}
