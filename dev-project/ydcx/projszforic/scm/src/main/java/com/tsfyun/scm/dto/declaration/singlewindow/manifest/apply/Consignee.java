package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;



import com.tsfyun.common.base.util.jaxb.CDataAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @Description: 收件人信息
 * @since Created in 2020/4/22 11:28
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Consignee", propOrder = {
        "id",
        "name",
})
public class Consignee {

    //收货人代码
    @XmlElement(name = "ID")
    protected String id;

    //收货人名称
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "Name")
    protected String name;


    public Consignee( ) {

    }

    public Consignee(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
