package com.tsfyun.scm.dto.finance;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PaymentOrderDTO implements Serializable {

    @NotNull(message = "订单ID不能为空")
    private Long orderId;//订单ID
    private String orderNo;//订单号
    @NotNull(message = "订单申请金额不能为空")
    @Digits(integer = 10, fraction = 2, message = "订单申请金额整数位不能超过10位，小数位不能超过2位")
    @DecimalMin(value = "0",message = "订单申请金额不能小于零")
    private BigDecimal applyPayVal;//申请金额
}
