package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.BaseDataSimple;
import com.tsfyun.common.base.vo.DecDataVO;
import com.tsfyun.scm.client.BaseDataClient;
import com.tsfyun.scm.system.vo.BaseDataVO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class BaseDataClientFallback implements FallbackFactory<BaseDataClient> {

    @Override
    public BaseDataClient create(Throwable throwable) {
        return new BaseDataClient(){
            @Override
            public Result<List<BaseDataVO>> getByType(String type) {
                log.error(String.format("根据基础数据类型【%s】获取",type),throwable);
                return null;
            }

            @Override
            public Result<BigDecimal> obtainCustomsRate(String currencyId, Date date) {
                log.error("获取海关汇率失败",throwable);
                return null;
            }

            @Override
            public Result<BigDecimal> obtainBankChinaRate(String currencyId,String rateType, String date) {
                log.error("获取中行汇率失败",throwable);
                return null;
            }

            @Override
            public Result<Map<String,List<BaseDataSimple>>> decDatasMap() {
                log.error("获取海关基础数据失败",throwable);
                return null;
            }

        };
    }
}
