package com.tsfyun.scm.entity.risk;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 风控审批
 * </p>
 *

 * @since 2020-05-08
 */
@Data
public class RiskApproval extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 审核类型
     */

    private String docType;

    /**
     * 状态
     */
    private String statusId;

    /**
     * 单据id
     */

    private String docId;

    /**
     * 单据编号
     */

    private String docNo;

    /**
     * 客户id
     */

    private Long customerId;

    /**
     * 提交人
     */

    private String submitter;

    /**
     * 审批人
     */

    private String approver;

    /**
     * 审批意见
     */

    private String approvalInfo;

    /**
     * 审批原因
     */

    private String approvalReason;


}
