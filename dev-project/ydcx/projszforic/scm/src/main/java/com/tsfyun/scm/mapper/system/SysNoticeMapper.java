package com.tsfyun.scm.mapper.system;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.dto.system.SysNoticeQTO;
import com.tsfyun.scm.entity.system.SysNotice;
import com.tsfyun.scm.vo.system.SysNoticeVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-09-22
 */
@Repository
public interface SysNoticeMapper extends Mapper<SysNotice> {

    List<SysNoticeVO> list(SysNoticeQTO qto);

}
