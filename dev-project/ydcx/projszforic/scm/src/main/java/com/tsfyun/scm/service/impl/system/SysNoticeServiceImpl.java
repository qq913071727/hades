package com.tsfyun.scm.service.impl.system;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.FileDTO;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.common.base.util.TypeUtils;
import com.tsfyun.scm.dto.system.SysNoticeDTO;
import com.tsfyun.scm.dto.system.SysNoticeQTO;
import com.tsfyun.scm.entity.system.SysNotice;
import com.tsfyun.scm.mapper.system.SysNoticeMapper;
import com.tsfyun.scm.service.file.IUploadFileService;
import com.tsfyun.scm.service.system.ISysNoticeService;
import com.tsfyun.scm.vo.system.SimpleSysNoticeVO;
import com.tsfyun.scm.vo.system.SysNoticeVO;
import lombok.extern.slf4j.Slf4j;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 *  通知公告服务实现类
 * </p>
 *
 *
 * @since 2020-09-22
 */
@Service
@Slf4j
public class SysNoticeServiceImpl extends ServiceImpl<SysNotice> implements ISysNoticeService {

    @Autowired
    private SysNoticeMapper sysNoticeMapper;

    @Autowired
    private IUploadFileService uploadFileService;

    @Override
    public PageInfo<SysNoticeVO> pageList(SysNoticeQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        List<SysNoticeVO> list =  sysNoticeMapper.list(qto);
        return new PageInfo<>(list);
    }

    @Override
    public SysNoticeVO detail(Long id) {
        SysNotice sysNotice = super.getById(id);
        TsfPreconditions.checkNonNull(sysNotice,"该公告不存在或已被删除");
        return beanMapper.map(sysNotice,SysNoticeVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void remove(Long id) {
        SysNotice sysNotice = super.getById(id);
        TsfPreconditions.checkNonNull(sysNotice,"该公告不存在或已被删除");
        super.removeById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long add(SysNoticeDTO dto) {
        SysNotice sysNotice = beanMapper.map(dto,SysNotice.class);
        super.saveNonNull(sysNotice);
        FileDTO file = new FileDTO();
        file.setDocId(dto.getDocId());
        file.setDocType("notice");
        uploadFileService.relateFile(sysNotice.getId().toString(),file);
        return sysNotice.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SysNoticeDTO dto) {
        SysNotice sysNotice = super.getById(dto.getId());
        TsfPreconditions.checkNonNull(sysNotice,"该公告不存在或已被删除");
        sysNotice.setCategory(dto.getCategory());
        sysNotice.setBanner(dto.getBanner());
        sysNotice.setTitle(dto.getTitle());
        sysNotice.setRepresent(dto.getRepresent());
        sysNotice.setContent(dto.getContent());
        sysNotice.setIsOpen(dto.getIsOpen());
        sysNotice.setIsLoginSee(dto.getIsLoginSee());
        sysNotice.setIsPopup(dto.getIsPopup());
        sysNotice.setPublishTime(dto.getPublishTime());
        super.updateById(sysNotice);
    }

    @Override
    public List<SimpleSysNoticeVO> recentSysNotices(Integer num) {
        PageHelper.startPage(1, TypeUtils.castToInt(num,5));
        SysNoticeQTO query = new SysNoticeQTO();
        query.setIsOpen(Boolean.TRUE);
        List<SysNoticeVO> list =  sysNoticeMapper.list(query);
        return beanMapper.mapAsList(list,SimpleSysNoticeVO.class);
    }

}
