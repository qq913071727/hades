package com.tsfyun.scm.dto.wms;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeliveryNoteMember请求对象", description="请求实体")
public class DeliveryNoteMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long deliveryNoteId;

   private Long receivingNoteMemberId;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   @LengthTrim(max = 500,message = "规格参数最大长度不能超过500位")
   @ApiModelProperty(value = "规格参数")
   private String spec;

   @LengthTrim(max = 100,message = "客户物料号最大长度不能超过100位")
   @ApiModelProperty(value = "客户物料号")
   private String goodsCode;

   @NotEmptyTrim(message = "产地不能为空")
   @LengthTrim(max = 10,message = "产地最大长度不能超过10位")
   @ApiModelProperty(value = "产地")
   private String country;

   @NotEmptyTrim(message = "产地不能为空")
   @LengthTrim(max = 50,message = "产地最大长度不能超过50位")
   @ApiModelProperty(value = "产地")
   private String countryName;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitCode;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "净重不能为空")
   @Digits(integer = 6, fraction = 4, message = "净重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "净重")
   private BigDecimal netWeight;

   @NotNull(message = "毛重不能为空")
   @Digits(integer = 6, fraction = 4, message = "毛重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "毛重")
   private BigDecimal grossWeight;

   @NotNull(message = "箱数不能为空")
   @LengthTrim(max = 11,message = "箱数最大长度不能超过11位")
   @ApiModelProperty(value = "箱数")
   private Integer cartonNum;

   @LengthTrim(max = 20,message = "箱号最大长度不能超过20位")
   @ApiModelProperty(value = "箱号")
   private String cartonNo;


}
