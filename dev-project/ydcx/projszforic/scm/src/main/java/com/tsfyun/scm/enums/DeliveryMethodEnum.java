package com.tsfyun.scm.enums;

import java.util.Arrays;
import java.util.Objects;

public enum DeliveryMethodEnum {
    DIRECT_LIFTING("direct_lifting","港车直提"),
    RETURN_WARE("return_ware","提回仓库"),
    ;

    private String code;
    private String name;

    DeliveryMethodEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }


    public static DeliveryMethodEnum of(String code) {
        return Arrays.stream(DeliveryMethodEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
