package com.tsfyun.scm.service.impl.materiel;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.extension.OrderItem;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.dto.materiel.MaterielLogQTO;
import com.tsfyun.scm.entity.materiel.Materiel;
import com.tsfyun.scm.entity.materiel.MaterielExp;
import com.tsfyun.scm.entity.materiel.MaterielLog;
import com.tsfyun.scm.mapper.materiel.MaterielLogMapper;
import com.tsfyun.scm.service.materiel.IMaterielLogService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.materiel.MaterielLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 归类变更日志 服务实现类
 * </p>
 *
 *
 * @since 2020-03-26
 */
@Service
public class MaterielLogServiceImpl extends ServiceImpl<MaterielLog> implements IMaterielLogService {

    @Autowired
    private MaterielLogMapper materielLogMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void recordMaterielLog(Materiel history, Materiel materiel) {
        String change = comparativeChange(history,materiel);
        if(StringUtils.isNotEmpty(change)){
            MaterielLog log = new MaterielLog();
            log.setChangeContent(change);
            log.setMaterielId(materiel.getId());
            super.save(log);
        }
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void recordMaterielLog(MaterielExp history, MaterielExp materiel) {
        String change = comparativeChange(history,materiel);
        if(StringUtils.isNotEmpty(change)){
            MaterielLog log = new MaterielLog();
            log.setChangeContent(change);
            log.setMaterielId(materiel.getId());
            super.save(log);
        }
    }

    @Override
    public String comparativeChange(Materiel history, Materiel materiel) {
        StringBuffer sb = new StringBuffer("");
        if(!Objects.equals(history.getName(),materiel.getName())){
            sb.append(String.format("名称[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getName()),StringUtils.removeSpecialSymbol(materiel.getName())));
        }
        if(!Objects.equals(history.getModel(),materiel.getModel())){
            sb.append(String.format("型号[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getModel()),StringUtils.removeSpecialSymbol(materiel.getModel())));
        }
        if(!Objects.equals(history.getBrand(),materiel.getBrand())){
            sb.append(String.format("品牌[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getBrand()),StringUtils.removeSpecialSymbol(materiel.getBrand())));
        }
        if(!Objects.equals(history.getHsCode(),materiel.getHsCode())){
            sb.append(String.format("海关编码[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getHsCode()),StringUtils.removeSpecialSymbol(materiel.getHsCode())));
        }
        if(!Objects.equals(history.getCiqNo(),materiel.getCiqNo())){
            sb.append(String.format("CIQ编码[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getCiqNo()),StringUtils.removeSpecialSymbol(materiel.getCiqNo())));
        }
        if(!Objects.equals(history.getIsSmp(),materiel.getIsSmp())){
            sb.append(String.format("战略物资[%s]改[%s]；",history.getIsSmp(),materiel.getIsSmp()));
        }
        if(!Objects.equals(history.getIsCapp(),materiel.getIsCapp())){
            sb.append(String.format("需要3C证书[%s]改[%s]；",history.getIsCapp(),materiel.getIsCapp()));
        }
        if(!Objects.equals(history.getCappNo(),materiel.getCappNo())){
            sb.append(String.format("需要3C目录鉴定[%s]改[%s]；",history.getCappNo(),materiel.getCappNo()));
        }
        if(!Objects.equals(history.getElements(),materiel.getElements())){
            sb.append(String.format("申报要素[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getElements()),StringUtils.removeSpecialSymbol(materiel.getElements())));
        }
        return sb.toString();
    }

    @Override
    public String comparativeChange(MaterielExp history, MaterielExp materiel) {
        StringBuffer sb = new StringBuffer("");
        if(!Objects.equals(history.getName(),materiel.getName())){
            sb.append(String.format("名称[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getName()),StringUtils.removeSpecialSymbol(materiel.getName())));
        }
        if(!Objects.equals(history.getModel(),materiel.getModel())){
            sb.append(String.format("型号[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getModel()),StringUtils.removeSpecialSymbol(materiel.getModel())));
        }
        if(!Objects.equals(history.getBrand(),materiel.getBrand())){
            sb.append(String.format("品牌[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getBrand()),StringUtils.removeSpecialSymbol(materiel.getBrand())));
        }
        if(!Objects.equals(history.getHsCode(),materiel.getHsCode())){
            sb.append(String.format("海关编码[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getHsCode()),StringUtils.removeSpecialSymbol(materiel.getHsCode())));
        }
        if(!Objects.equals(history.getCiqNo(),materiel.getCiqNo())){
            sb.append(String.format("CIQ编码[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getCiqNo()),StringUtils.removeSpecialSymbol(materiel.getCiqNo())));
        }
        if(!Objects.equals(history.getElements(),materiel.getElements())){
            sb.append(String.format("申报要素[%s]改[%s]；",StringUtils.removeSpecialSymbol(history.getElements()),StringUtils.removeSpecialSymbol(materiel.getElements())));
        }
        return sb.toString();
    }

    @Override
    public Result<List<MaterielLogVO>> pageList(MaterielLogQTO qto) {
        TsfWeekendSqls sqls = TsfWeekendSqls.<MaterielLog>custom()
        .andEqualTo(true,MaterielLog::getMaterielId,qto.getMaterielId());
        PageInfo<MaterielLog> materielLogPageInfo = super.pageList(qto.getPage(),qto.getLimit(),sqls, Lists.newArrayList(OrderItem.desc("dateUpdated")));
        List<MaterielLogVO> materielLogVOList = beanMapper.mapAsList(materielLogPageInfo.getList(),MaterielLogVO.class);
        return Result.success((int)materielLogPageInfo.getTotal(),materielLogVOList);
    }

    @Override
    public void batchSave(List<MaterielLog> materielLogList) {
        materielLogMapper.batchSave(materielLogList);
    }

    @Override
    public void updateMaterielId(String oldId, String newId) {
        materielLogMapper.updateMaterielId(oldId,newId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeByMaterielId(String materielId) {
        materielLogMapper.removeByMaterielId(materielId);
    }
}
