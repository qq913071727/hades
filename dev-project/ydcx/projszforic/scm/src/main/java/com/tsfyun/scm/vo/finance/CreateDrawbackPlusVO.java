package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CreateDrawbackPlusVO implements Serializable {

    private CreateDrawbackVO drawback;

    private List<DrawbackOrderVO> orders;
}
