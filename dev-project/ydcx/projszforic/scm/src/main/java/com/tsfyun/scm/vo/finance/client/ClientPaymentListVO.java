package com.tsfyun.scm.vo.finance.client;

import com.tsfyun.common.base.enums.domain.PaymentAccountStatusEnum;
import com.tsfyun.common.base.enums.finance.CounterFeeTypeEnum;
import com.tsfyun.common.base.enums.finance.PaymentTermEnum;
import com.tsfyun.scm.util.ClientInfoStatus;
import com.tsfyun.scm.util.ClientStatusMappingUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description: 客户端付汇申请列表
 * @CreateDate: Created in 2020/11/3 10:38
 */
@Data
public class ClientPaymentListVO implements Serializable {

    private Long id;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 状态编码
     */
    private String statusId;

    /**
     * 付款申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime accountDate;

    /**
     * 收款人
     */
    private String payee;

    /**
     * 收款行名称
     */

    private String payeeBankName;

    /**
     * 收款行账号
     */

    private String payeeBankAccountNo;

    /**
     * swift code
     */

    private String swiftCode;

    /**
     * 付款方式
     */

    private String paymentTerm;

    private String paymentTermDesc;

    /**
     * 币制名称
     */

    private String currencyName;

    /**
     * 付款金额
     */

    private BigDecimal accountValue;

    /**
     * 结算汇率
     */

    private BigDecimal exchangeRate;

    /**
     * 付款人民币金额
     */

    private BigDecimal accountValueCny;

    /**
     * 付款手续费金额
     */

    private BigDecimal bankFee;

    private String statusDesc;

    @ApiModelProperty(value = "手续费承担方式")
    private String counterFeeType;
    private String counterFeeTypeDesc;

    private String clientStatusId;

    private String clientStatusDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(PaymentAccountStatusEnum.of(statusId)).map(PaymentAccountStatusEnum::getName).orElse("");
    }

    public String getClientStatusId() {
        return ClientStatusMappingUtil.getClientPayStatus(statusId);
    }

    public String getClientStatusDesc() {
        ClientInfoStatus clientInfoStatus = ClientStatusMappingUtil.PAY_CLIENT_STATUS_MAPPING.get(getClientStatusId());
        return Optional.ofNullable(clientInfoStatus).map(ClientInfoStatus::getClientStatusDesc).orElse("");
    }


    public String getPaymentTermDesc() {
        return Optional.ofNullable(PaymentTermEnum.of(paymentTerm)).map(PaymentTermEnum::getName).orElse("");
    }

    public String getCounterFeeTypeDesc(){
        return Optional.ofNullable(CounterFeeTypeEnum.of(counterFeeType)).map(CounterFeeTypeEnum::getName).orElse("");
    }

}
