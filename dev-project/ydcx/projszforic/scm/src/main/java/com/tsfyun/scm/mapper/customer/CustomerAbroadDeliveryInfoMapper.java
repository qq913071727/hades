package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.customer.CustomerAbroadDeliveryInfo;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.CustomerAbroadDeliveryInfoVO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 客户境外送货地址 Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-16
 */
@Repository
public interface CustomerAbroadDeliveryInfoMapper extends Mapper<CustomerAbroadDeliveryInfo> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
        //获取收货地址信息
    List<CustomerAbroadDeliveryInfoVO> list(Map<String,Object> params);

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = false)
    List<CustomerAbroadDeliveryInfoVO> effectiveList(Map<String,Object> params);
}
