package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.finance.WriteOffTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * <p>
 * 收款核销明细响应实体
 * </p>
 *

 * @since 2020-05-22
 */
@Data
@ApiModel(value="ReceiptAccountMember响应对象", description="收款核销明细响应实体")
public class ReceiptAccountMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "收款单主单")
    private Long receiptAccountId;

    @ApiModelProperty(value = "进口费用")
    private Long impOrderCostId;

    @ApiModelProperty(value = "核销单号")
    private String docNo;

    @ApiModelProperty(value = "费用科目ID")
    private String expenseSubjectId;

    @ApiModelProperty(value = "费用科目名称")
    private String expenseSubjectName;

    @ApiModelProperty(value = "核销金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "核销类型")
    private String writeOffType;

    @ApiModelProperty(value = "操作人")
    private String operation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateCreated;

    private String writeOffTypeDesc;

    public String getWriteOffTypeDesc() {
        return Optional.ofNullable(WriteOffTypeEnum.of(writeOffType)).map(WriteOffTypeEnum::getName).orElse("");
    }


}
