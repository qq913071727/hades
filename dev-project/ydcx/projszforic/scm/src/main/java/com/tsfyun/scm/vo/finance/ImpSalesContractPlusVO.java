package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 销售合同打印、导出响应实体
 * @CreateDate: Created in 2020/5/22 10:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpSalesContractPlusVO implements Serializable {

    private ImpSalesContractMainVO salesContract;

    private List<ImpSalesContractMemberVO> members;

}
