package com.tsfyun.scm.dto.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description: 微信回复消息内容
 * @CreateDate: Created in 2020/12/31 11:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WXReplyMessageDTO implements Serializable {

    //消息发送者  若是公众号接收消息 则为具体的关注者  若是公众号发送消息则为公共号自身
    public String fromUserName;
    //消息接收者
    public String toUserName;
    //消息生成时间
    public String createTime;
    //消息类型
    public String msgType;
    //素材ID 通过素材管理中的接口上传多媒体文件，得到的id。
    public String mediaId;
    //消息内容
    public String content;
    //图文标题
    public String title;
    //图文描述
    public String description;

    //快捷封装文本消息
    public WXReplyMessageDTO(String toUserName,String msgType,String content){
        this.toUserName = toUserName;
        this.msgType = msgType;
        this.content = content;
    }

}
