package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysRoleVO implements Serializable {

    private String id;

    private String name;

    private Boolean disabled;

    private Boolean locking;

    private String memo;


}
