package com.tsfyun.scm.dto.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Data
@ApiModel(value="SubjectOverseasBank请求对象", description="请求实体")
public class SubjectOverseasBankDTO implements Serializable {

   private static final long serialVersionUID=1L;


   @ApiModelProperty(value = "主体境外公司")
   private Long subjectOverseasId;

   @NotEmptyTrim(message = "银行名称不能为空")
   @LengthTrim(max = 64,message = "银行名称最大长度不能超过64位")
   @ApiModelProperty(value = "银行名称")
   private String name;

   @NotEmptyTrim(message = "银行账号不能为空")
   @LengthTrim(max = 50,message = "银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "银行账号")
   private String account;

   @LengthTrim(max = 50,message = "SWIFT CODE最大长度不能超过50位")
   @ApiModelProperty(value = "SWIFT CODE")
   private String swiftCode;

   @LengthTrim(max = 255,message = "银行地址最大长度不能超过255位")
   @ApiModelProperty(value = "银行地址")
   private String address;

   @NotNull(message = "禁用标示不能为空")
   @ApiModelProperty(value = "禁用标示")
   private Boolean disabled;

   @NotNull(message = "默认不能为空")
   @ApiModelProperty(value = "默认")
   private Boolean isDefault;

   @ApiModelProperty(value = "币制")
   private String currencyId;

   @NotEmptyTrim(message = "币制不能为空")
   @ApiModelProperty(value = "币制")
   private String currencyName;

   @LengthTrim(max = 20,message = "银行代码最大长度不能超过20位")
   @ApiModelProperty(value = "银行代码")
   private String code;


}
