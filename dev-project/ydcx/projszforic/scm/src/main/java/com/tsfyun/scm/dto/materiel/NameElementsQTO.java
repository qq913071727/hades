package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 品名要素请求实体
 * </p>
 *

 * @since 2020-03-26
 */
@Data
@ApiModel(value="NameElements查询请求对象", description="品名要素查询请求实体")
public class NameElementsQTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   @ApiModelProperty(value = "海关编码")
   private String hsCode;

   @ApiModelProperty(value = "是否禁用")
   private Boolean disabled;

   @ApiModelProperty(value = "物料名称")
   private String name;

   @ApiModelProperty(value = "要素名称")
   private String nameMemo;

}
