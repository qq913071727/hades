package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/6/1 15:39
 */
@Data
public class SubjectChangeVO implements Serializable {

    private String tenantCode;

    private List<ChangeVal> changeVal;

    private String changeContent;

    @Data
    public class ChangeVal {

        private String changeField;

        private String oldValue;

        private String nowValue;
    }

}
