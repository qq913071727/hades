package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-03
 */
@Data
public class StatusHistory implements Serializable{

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 单据ID
     */
    private String domainId;

    /**=
     * 操作编码
     */
    private String operationCode;

    /**
     * 单据类型
     */
    private String domainType;

    /**
     * 原单据状态
     */
    private String originalStatusId;

    /**
     * 原单据状态
     */
    private String originalStatusName;

    /**
     * 现单据状态
     */
    private String nowStatusId;

    /**
     * 现单据状态名称
     */
    private String nowStausName;

    /**
     * 创建人   创建人personId/创建人名称
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime dateCreated;

    /**
     * 备注
     */
    private String memo;


}
