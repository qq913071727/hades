package com.tsfyun.scm.service.storage;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.storage.WarehouseDTO;
import com.tsfyun.scm.dto.storage.WarehouseQTO;
import com.tsfyun.scm.entity.storage.Warehouse;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.storage.WarehouseVO;

import java.util.List;

/**
 * <p>
 * 仓库 服务类
 * </p>
 *

 * @since 2020-03-31
 */
public interface IWarehouseService extends IService<Warehouse> {

    //新增
    void add(WarehouseDTO dto);

    //修改
    void edit(WarehouseDTO dto);

    //分页列表
    PageInfo<Warehouse> page(WarehouseQTO qto);

    //删除
    void delete(Long id);

    //获取所有的指定类型的开启的仓库
    List<WarehouseVO> allList(WarehouseQTO dto);

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 详情
     * @param id
     * @return
     */
    WarehouseVO detail(Long id);

    /**
     * 获取所有的启用的境内自提仓库
     * @return
     */
    List<WarehouseVO> domesticSelfMentionWarehouses();

    /**
     * 根据编码获取仓库信息
     * @param code
     * @return
     */
    Warehouse getByCode(String code);

    /**
     * 获取所有可用的仓库
     * @return
     */
    List<WarehouseVO> allEnableWarehouses(WarehouseQTO qto);

}
