package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 客户端我的公司
 */
@Data
public class CompanyDTO implements Serializable {

    //客户id
    //@NotNull(message = "请选择客户")
    //private Long id;

    @NotBlank(message = "公司全称不能为空")
    @LengthTrim(min = 1,max = 200)
    @ApiModelProperty(value = "公司全称")
    private String name;

    @NotBlank(message = "社会信用代码不能为空")
    @LengthTrim(min = 18,max = 18,message = "社会信用代码只能为18位")
    @ApiModelProperty(value = "社会信用代码(18位)")
    private String socialNo;

    @NotBlank(message = "公司法人不能为空")
    @LengthTrim(min = 2,max = 50,message = "公司法人只能为2-50位")
    @ApiModelProperty(value = "公司法人")
    private String legalPerson;

    @LengthTrim(min = 10,max = 10,message = "海关注册编码只能为10位")
    @ApiModelProperty(value = "海关注册编码")
    private String customsCode;

    @LengthTrim(min = 3,max = 50,message = "公司电话只能为3-50位")
    @ApiModelProperty(value = "公司电话")
    private String tel;

    @LengthTrim(min = 3,max = 50,message = "公司传真只能为3-50位")
    @ApiModelProperty(value = "公司传真")
    private String fax;

    @NotBlank(message = "公司地址不能为空")
    @LengthTrim(min = 2,max = 255,message = "公司地址只能为2-255位")
    @ApiModelProperty(value = "公司地址")
    private String address;


}
