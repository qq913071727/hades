package com.tsfyun.scm.service.impl.order;

import com.tsfyun.scm.entity.order.ExpOrderLogistics;
import com.tsfyun.scm.mapper.order.ExpOrderLogisticsMapper;
import com.tsfyun.scm.service.order.IExpOrderLogisticsService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.order.ExpOrderLogisticsSimpleVO;
import com.tsfyun.scm.vo.order.ExpOrderLogisticsVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsSimpleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 出口订单物流信息 服务实现类
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Service
public class ExpOrderLogisticsServiceImpl extends ServiceImpl<ExpOrderLogistics> implements IExpOrderLogisticsService {

    @Autowired
    private ExpOrderLogisticsMapper expOrderLogisticsMapper;

    @Override
    public ExpOrderLogisticsVO findById(Long id) {
        return expOrderLogisticsMapper.findById(id);
    }

    @Override
    public List<ExpOrderLogisticsSimpleVO> getCharterCarDeliveryInfo() {
        LocalDateTime oneMonthBefore = LocalDateTime.now().plusMonths(-1);
        return expOrderLogisticsMapper.getCharterCarDeliveryInfo(oneMonthBefore);
    }

    @Override
    public ExpOrderLogistics findByOrderId(Long orderId) {
        return expOrderLogisticsMapper.selectOneByExample(Example.builder(ExpOrderLogistics.class).where(TsfWeekendSqls.<ExpOrderLogistics>custom().andEqualTo(false,ExpOrderLogistics::getExpOrderId,orderId)).build());
    }
}
