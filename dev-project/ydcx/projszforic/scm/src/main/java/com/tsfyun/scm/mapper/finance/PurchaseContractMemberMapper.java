package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.PurchaseContractMember;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 采购合同明细 Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Repository
public interface PurchaseContractMemberMapper extends Mapper<PurchaseContractMember> {

    List<PurchaseContractMember> findByPurchaseContractId(Long purchaseContractId);

}
