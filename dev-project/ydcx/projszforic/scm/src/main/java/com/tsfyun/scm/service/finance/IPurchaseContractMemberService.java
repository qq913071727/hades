package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.PurchaseContractMember;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 * 采购合同明细 服务类
 * </p>
 *
 *
 * @since 2021-09-26
 */
public interface IPurchaseContractMemberService extends IService<PurchaseContractMember> {

    /**
     * 根据采购合同id获取采购合同明细
     * @param purchaseContractId
     * @return
     */
    List<PurchaseContractMember> findByPurchaseContractId(Long purchaseContractId);

    /**
     * 根据合同ID删除明细
     * @param purchaseContractId
     */
    void removeByContractId(Long purchaseContractId);

}
