package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.FileDocIdDTO;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.UploadFileCntVO;
import com.tsfyun.common.base.vo.UploadFileVO;
import com.tsfyun.scm.client.UploadFileClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Component
@Slf4j
public class UploadFileClientFallback implements FallbackFactory<UploadFileClient> {
    @Override
    public UploadFileClient create(Throwable throwable) {
        return new UploadFileClient() {

            @Override
            public Result<Long> upload(String fileType, String tenantCode, String tenantName, String docId, String docType, String businessType, String memo, String uid, String uname, MultipartFile file) {
                log.error("调用文件服务服务上传文件异常",throwable);
                return null;
            }

            @Override
            public Result<List<UploadFileVO>> list(String docType, String docId) {
                log.error("调用文件服务服务获取文件异常",throwable);
                return null;
            }

            @Override
            public Result<Void> updateDocId(String docType, String docId, Long fileId, Boolean only) {
                log.error("调用文件服务服务关联文件异常",throwable);
                return null;
            }

            @Override
            public Result<Void> remove(List<Long> ids) {
                log.error("调用文件服务服务删除文件异常",throwable);
                return null;
            }

            @Override
            public Result<Void> batchUpdateDocId(@Valid List<FileDocIdDTO> list) {
                log.error("调用文件服务服务批量关联文件异常",throwable);
                return null;
            }

            @Override
            public Result<UploadFileCntVO> obtainFileSize(String docType, String docId) {
                log.error("调用文件服务服务获取文件数量异常",throwable);
                return null;
            }
        };
    }
}
