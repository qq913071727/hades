package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 快递模式标准报价计算代理费明细请求实体
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Data
@ApiModel(value="CustomerExpressQuoteMember请求对象", description="快递模式标准报价计算代理费明细请求实体")
public class CustomerExpressQuoteMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "报价单id不能为空")
   @ApiModelProperty(value = "报价单id")
   private Long quoteId;

   @NotNull(message = "开始重量不能为空")
   @Digits(integer = 8,fraction = 0,message = "开始重量最大长度不能超过8位")
   @ApiModelProperty(value = "开始重量")
   private BigDecimal startWeight;

   @Digits(integer = 8, fraction = 0,message = "结束重量最大长度不能超过8位")
   @ApiModelProperty(value = "结束重量-不含")
   private BigDecimal endWeight;

   @Digits(integer = 6, fraction = 2, message = "单价整数位不能超过6位，小数位不能超过2")
   @ApiModelProperty(value = "单价-KG")
   private BigDecimal price;


}
