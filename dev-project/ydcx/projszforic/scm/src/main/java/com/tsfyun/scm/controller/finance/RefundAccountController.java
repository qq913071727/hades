package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.RefundAccountDTO;
import com.tsfyun.scm.dto.finance.RefundAccountQTO;
import com.tsfyun.scm.dto.finance.RefundConfirmBankDTO;
import com.tsfyun.scm.service.finance.IRefundAccountService;
import com.tsfyun.scm.vo.finance.RefundAccountVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 退款申请 前端控制器
 */
@RestController
@RequestMapping("/refundAccount")
public class RefundAccountController extends BaseController {

    @Autowired
    private IRefundAccountService refundAccountService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<RefundAccountVO>> pageList(@ModelAttribute RefundAccountQTO qto) {
        PageInfo<RefundAccountVO> page = refundAccountService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 客户端退款申请
     * @param dto
     * @return
     */
    @PostMapping(value = "/clientApply")
    @DuplicateSubmit
    public Result<Void> clientApply(@RequestBody @Valid RefundAccountDTO dto) {
         refundAccountService.clientApply(dto);
        return success();
    }

    /**
     * 客户退款申请商务审核
     * @param dto
     * @return
     */
    @PostMapping(value = "/approve")
    @DuplicateSubmit
    public Result<Void> approve(@ModelAttribute @Valid TaskDTO dto) {
        refundAccountService.approve(dto);
        return success();
    }

    /**
     * 作废退款单
     * @param dto
     * @return
     */
    @PostMapping(value = "/toVoid")
    @DuplicateSubmit
    public Result<Void> toVoid(@ModelAttribute @Valid TaskDTO dto) {
        refundAccountService.toVoid(dto);
        return success();
    }

    /**
     * 删除退款单
     * @param id
     * @return
     */
    @PostMapping(value = "/remove")
    @DuplicateSubmit
    public Result<Void> remove(@RequestParam(value = "id")Long id) {
        refundAccountService.removeAllById(id);
        return success();
    }

    /**
     * 客户退款申请确定付款行
     * @param dto
     * @return
     */
    @PostMapping(value = "/confirmBank")
    @DuplicateSubmit
    public Result<Void> confirmBank(@ModelAttribute @Valid RefundConfirmBankDTO dto) {
        refundAccountService.confirmBank(dto);
        return success();
    }

    /**
     * 客户退款申请上传水单
     * @param dto
     * @return
     */
    @PostMapping(value = "/bankReceipt")
    @DuplicateSubmit
    public Result<Void> bankReceipt(@ModelAttribute @Valid TaskDTO dto) {
        refundAccountService.bankReceipt(dto);
        return success();
    }

    /**
     * 详情信息
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<RefundAccountVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(refundAccountService.detail(id,operation));
    }

    /**
     * 获取客户退款状态数量
     * @return
     */
    @ApiOperation(value = "我的退款单数")
    @PostMapping(value = "myRefundStatistic")
    public Result<Map<String,Long>> myRefundStatistic( ) {
        return success(refundAccountService.getClientRefundNum( ));
    }

}

