package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.entity.logistics.Conveyance;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface ConveyanceMapper extends Mapper<Conveyance> {

}
