package com.tsfyun.scm.entity.user;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * 登录表
 */
@Data
public class Login extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 员工ID
     */
    private Long personId;

    /**
     * 第三方登录需建立第三方关系表
     * (0：账号 1：手机号 2：邮箱 ，3：QQ快捷登录，4：微信快捷登录) 以此为准 zmark
     *
     * 与 LoginTypeEnum 中无对应关系
     */
    private Integer type;

    /**
     * 登录账号，与type对应
     */
    private String loginName;
}