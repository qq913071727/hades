package com.tsfyun.scm.controller.file;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.FileDocIdDTO;
import com.tsfyun.common.base.dto.FileQTO;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.util.FileUtil;
import com.tsfyun.common.base.vo.FileQtyVO;
import com.tsfyun.common.base.vo.UploadFileVO;
import com.tsfyun.scm.service.file.IUploadFileService;
import com.tsfyun.scm.vo.base.ClientFileVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/file")
public class FileController extends BaseController {

    @Autowired
    private IUploadFileService uploadFileService;


    /**
     *
     * @param fileType 文件类型file|image
     * @param docId      单据id
     * @param docType    单据类型
     * @param businessType 单据业务类型
     * @param memo   备注
     * @param file 文件
     * @return
     */
    @PostMapping(value = "/upload",produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<UploadFileVO> upload(@RequestParam(value = "fileType",required = false,defaultValue = "file") String fileType ,
                               @RequestParam(value = "docId") String docId,
                               @RequestParam(value = "docType") String docType,
                               @RequestParam(value = "businessType") String businessType,
                               @RequestParam(value = "memo",required = false) String memo,
                               @RequestParam(value = "only",required = false,defaultValue = "false") Boolean only,
                               @RequestPart(name = "file",required = true) MultipartFile file) {

        return success(uploadFileService.upload(FileUtil.wrapUploadFileDto(file,fileType,docId,docType,businessType,memo,only)));
    }

    /**
     * 删除文件（逻辑删）
     * @param ids
     * @return
     */
    @PostMapping(value = "/removes")
    public Result<Void> remove(@RequestParam(value = "ids") List<Long> ids) {
        uploadFileService.delete(ids);
        return success();
    }

    /**
     * 获取文件列表
     * @return
     */
    @PostMapping(value = "/fileList")
    public Result<List<UploadFileVO>> list(@ModelAttribute @Valid FileQTO fileQTO){
        return success(uploadFileService.list(fileQTO));
    }

    /**
     * 获取文件数量(分组统计)
     * @return
     */
    @PostMapping(value = "/fileSizeGroup")
    public Result<List<FileQtyVO>> fileSizeGroup(@ModelAttribute @Valid FileQTO fileQTO){
        return success(uploadFileService.countGroup(fileQTO));
    }

    /**
     *  获取文件列表
     * @return
     */
    @GetMapping("files")
    public Result<List<ClientFileVO>> files(
            @RequestParam(value = "docId") String docId,
            @RequestParam(value = "docType") String docType,
            @RequestParam(value = "businessType",required = false) String businessType
    ){
        return success(uploadFileService.findByFile(docId, docType, businessType));
    }

    /**
     * 关联文件单据id
     * @param fileDocIdDTOS
     * @return
     */
    @PostMapping(value = "/batchUpdateDocId")
    public Result<Void> batchUpdateDocId(@RequestBody @Valid List<FileDocIdDTO> fileDocIdDTOS){
        uploadFileService.batchUpdateDocId(fileDocIdDTOS);
        return Result.success();
    }

    /**
     * 获取文件路径
     * @param docId
     * @param docType
     * @param businessType
     * @return
     */
    @PostMapping(value = "/getFilePath")
    public Result<List<String>> getFilePath(@RequestParam(name = "docId",required = true)String docId, @RequestParam(name = "docType",required = false,defaultValue = "")String docType,
                                            @RequestParam(name = "businessType",required = false,defaultValue = "")String businessType){
        return Result.success(uploadFileService.getFilePath(docId,docType,businessType));
    }
}
