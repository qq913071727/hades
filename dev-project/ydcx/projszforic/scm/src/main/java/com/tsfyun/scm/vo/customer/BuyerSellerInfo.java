package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/19 10:01
 */
@Data
public class BuyerSellerInfo implements Serializable {

    /**
     * 名称
     */
    private String name;

    /**
     * 英文名称
     */
    private String nameEn;

    /**
     * 地址
     */
    private String address;

    /**
     * 英文地址
     */
    private String addressEn;

    /**
     * 传真
     */
    private String fax;

    /**
     * 电话
     */
    private String tel;

    /**
     * 签章
     */
    private String chapter;
}
