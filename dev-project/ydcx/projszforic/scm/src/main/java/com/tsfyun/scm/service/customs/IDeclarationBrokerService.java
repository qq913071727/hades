package com.tsfyun.scm.service.customs;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.customs.DeclarationBrokerDTO;
import com.tsfyun.scm.dto.customs.DeclarationBrokerQTO;
import com.tsfyun.scm.entity.customs.DeclarationBroker;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customs.DeclarationBrokerVO;

import java.util.List;

/**
 * <p>
 * 报关行 服务类
 * </p>
 *
 *
 * @since 2021-11-15
 */
public interface IDeclarationBrokerService extends IService<DeclarationBroker> {

    void add(DeclarationBrokerDTO dto);

    void edit(DeclarationBrokerDTO dto);

    void delete(Long id);

    void updateDisabled(Long id,Boolean disabled);

    DeclarationBrokerVO detail(Long id);

    PageInfo<DeclarationBroker> pageList(DeclarationBrokerQTO qto);

    List<DeclarationBrokerVO> select(String name);

}
