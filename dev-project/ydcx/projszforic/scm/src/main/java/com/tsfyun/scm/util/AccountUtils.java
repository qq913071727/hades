package com.tsfyun.scm.util;

import com.tsfyun.common.base.enums.AgreeModeEnum;
import com.tsfyun.scm.entity.customer.ImpQuote;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

/**=
 * 资金相关工具类
 */
public class AccountUtils {

    //代杂费账期
    public static LocalDateTime agencyFeesPeriod(LocalDateTime happenDateTime, ImpQuote impQuote){
        AgreeModeEnum agreeMode = AgreeModeEnum.of(impQuote.getAgreeMode());
        switch (agreeMode){
            case IMPORT_DAY://进口日
                return happenDateTime.plusDays(impQuote.getDay());
            case PAYMENT_DAY://付款日
                return happenDateTime.plusDays(impQuote.getDay());
            case MONTH://月结
                happenDateTime = happenDateTime.plusMonths(impQuote.getMonth());
                LocalDateTime lastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                if(lastDay.getDayOfMonth()>impQuote.getMonthDay()){
                    lastDay = lastDay.withDayOfMonth(impQuote.getMonthDay());
                }
                return lastDay;
            case HALF_MONTH://半月结
                if(happenDateTime.getDayOfMonth()<=15){//上半月
                    happenDateTime = happenDateTime.plusMonths(impQuote.getFirstMonth());
                    LocalDateTime firstLastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                    if(firstLastDay.getDayOfMonth()>impQuote.getFirstMonthDay()){
                        firstLastDay = firstLastDay.withDayOfMonth(impQuote.getFirstMonthDay());
                    }
                    return firstLastDay;
                }else{//下半月
                    happenDateTime = happenDateTime.plusMonths(impQuote.getLowerMonth());
                    LocalDateTime lowerLastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                    if(lowerLastDay.getDayOfMonth()>impQuote.getLowerMonthDay()){
                        lowerLastDay = lowerLastDay.withDayOfMonth(impQuote.getLowerMonthDay());
                    }
                    return lowerLastDay;
                }
            case WEEK://周结
                int day = (impQuote.getWeekDay() - happenDateTime.getDayOfWeek().getValue()) + impQuote.getWeek() * 7;
                return happenDateTime.plusDays(day);
        }
        return happenDateTime;
    }
    //税款账期
    public static LocalDateTime taxFeesPeriod(LocalDateTime happenDateTime, ImpQuote impQuote){
        AgreeModeEnum agreeMode = AgreeModeEnum.of(impQuote.getTaxAgreeMode());
        switch (agreeMode){
            case IMPORT_DAY://进口日
                return happenDateTime.plusDays(impQuote.getTaxDay());
            case PAYMENT_DAY://付款日
                return happenDateTime.plusDays(impQuote.getTaxDay());
            case MONTH://月结
                happenDateTime = happenDateTime.plusMonths(impQuote.getTaxMonth());
                LocalDateTime lastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                if(lastDay.getDayOfMonth()>impQuote.getTaxMonthDay()){
                    lastDay = lastDay.withDayOfMonth(impQuote.getTaxMonthDay());
                }
                return lastDay;
            case HALF_MONTH://半月结
                if(happenDateTime.getDayOfMonth()<=15){//上半月
                    happenDateTime = happenDateTime.plusMonths(impQuote.getTaxFirstMonth());
                    LocalDateTime firstLastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                    if(firstLastDay.getDayOfMonth()>impQuote.getTaxFirstMonthDay()){
                        firstLastDay = firstLastDay.withDayOfMonth(impQuote.getTaxFirstMonthDay());
                    }
                    return firstLastDay;
                }else{//下半月
                    happenDateTime = happenDateTime.plusMonths(impQuote.getTaxLowerMonth());
                    LocalDateTime lowerLastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                    if(lowerLastDay.getDayOfMonth()>impQuote.getTaxLowerMonthDay()){
                        lowerLastDay = lowerLastDay.withDayOfMonth(impQuote.getTaxLowerMonthDay());
                    }
                    return lowerLastDay;
                }
            case WEEK://周结
                int day = (impQuote.getTaxWeekDay() - happenDateTime.getDayOfWeek().getValue()) + impQuote.getTaxWeek() * 7;
                return happenDateTime.plusDays(day);
        }
        return happenDateTime;
    }
    //货款账期
    public static LocalDateTime goodFeesPeriod(LocalDateTime happenDateTime, ImpQuote impQuote){
        AgreeModeEnum agreeMode = AgreeModeEnum.of(impQuote.getGoAgreeMode());
        switch (agreeMode){
            case IMPORT_DAY://进口日
                return happenDateTime.plusDays(impQuote.getGoDay());
            case PAYMENT_DAY://付款日
                return happenDateTime.plusDays(impQuote.getGoDay());
            case MONTH://月结
                happenDateTime = happenDateTime.plusMonths(impQuote.getGoMonth());
                LocalDateTime lastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                if(lastDay.getDayOfMonth()>impQuote.getGoMonthDay()){
                    lastDay = lastDay.withDayOfMonth(impQuote.getGoMonthDay());
                }
                return lastDay;
            case HALF_MONTH://半月结
                if(happenDateTime.getDayOfMonth()<=15){//上半月
                    happenDateTime = happenDateTime.plusMonths(impQuote.getGoFirstMonth());
                    LocalDateTime firstLastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                    if(firstLastDay.getDayOfMonth()>impQuote.getGoFirstMonthDay()){
                        firstLastDay = firstLastDay.withDayOfMonth(impQuote.getGoFirstMonthDay());
                    }
                    return firstLastDay;
                }else{//下半月
                    happenDateTime = happenDateTime.plusMonths(impQuote.getGoLowerMonth());
                    LocalDateTime lowerLastDay = happenDateTime.with(TemporalAdjusters.lastDayOfMonth());
                    if(lowerLastDay.getDayOfMonth()>impQuote.getGoLowerMonthDay()){
                        lowerLastDay = lowerLastDay.withDayOfMonth(impQuote.getGoLowerMonthDay());
                    }
                    return lowerLastDay;
                }
            case WEEK://周结
                int day = (impQuote.getGoWeekDay() - happenDateTime.getDayOfWeek().getValue()) + impQuote.getGoWeek() * 7;
                return happenDateTime.plusDays(day);
        }
        return happenDateTime;
    }
}
