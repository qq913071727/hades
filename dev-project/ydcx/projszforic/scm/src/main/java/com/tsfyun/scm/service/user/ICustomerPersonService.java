package com.tsfyun.scm.service.user;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.user.CustomerPerson;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-09-15
 */
public interface ICustomerPersonService extends IService<CustomerPerson> {
    /**
     * 绑定人员客户关系
     * @param customerId
     * @param personId
     * @param isDefault
     */
    void bindCustomerPerson(Long customerId,Long personId,Boolean isDefault);

    /**
     * 绑定新客户
     * @param personId
     * @param customerId
     * @param newCustomerId
     */
    void newBindCustomerPerson(Long personId,Long customerId,Long newCustomerId);
}
