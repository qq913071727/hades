package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.enums.MsgContentTypeEnum;
import com.tsfyun.common.base.enums.domain.DomainTypeEnum;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

/**
 * 发送ws消息实体
 */
@Data
public class WsMessageDTO implements Serializable {

    /**
     * 是否广播消息，即所有的客户端均能收到，设置了isBroad，消息接收人无效
     */
    private Boolean isBroad;

    /**
     * 消息接收人id
     */
    private List<Long> userIds;

    /**
     * 单据类型
     */
    @LengthTrim(max = 64,message = "单据类型最大长度不能超过64位")
    @EnumCheck(clazz = DomainTypeEnum.class,message = "单据类型错误")
    private String documentType;

    /**
     * 单据id
     */
    @LengthTrim(max = 32,message = "单据id最大长度不能超过32位")
    private String documentId;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    @NotEmptyTrim(message = "消息内容不能为空")
    private String content;

    /**
     * 消息内容类型，如text-文本；image-图片；html-静态html
     */
    @EnumCheck(clazz = MsgContentTypeEnum.class,message = "消息内容类型错误")
    @Pattern(regexp = "text|image|html",message = "消息内容类型错误")
    private String contentType;

    /**
     * 消息跳转url(跳转带参数)
     */
    private String href;

    /**
     * 操作码
     */
    @EnumCheck(clazz = DomainOprationEnum.class,message = "操作码错误")
    private String operationCode;

    //提醒客户端是否需要拉取最新任务数量，True需要
    private Boolean noticeClient;

}
