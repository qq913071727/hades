package com.tsfyun.scm.support.pdf;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: pdf模板配置
 * @since Created in 2020/4/29 14:13
 */
@Data
public class PdfTemplateFile implements Serializable {

    /**
     * 模板文件名（含后缀，不含模板路径）
     */
    private String templateFileName;

    /**
     * 模板文件路径
     */
    private String templatePath;

    /**
     * 导出文件路径（包含文件名文件后缀）
     */
    private String destinationFileNamePath;

}
