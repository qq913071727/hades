package com.tsfyun.scm.vo.customs;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 获取所有的报关模板(已经按单据类型过滤)
 * @since Created in 2020/4/26 09:42
 */
@Data
public class DeclarationTempSimpleVO implements Serializable {

    private Long id;

    private String tempName;

    private Boolean isDefault;


}
