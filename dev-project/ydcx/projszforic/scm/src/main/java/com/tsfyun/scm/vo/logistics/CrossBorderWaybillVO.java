package com.tsfyun.scm.vo.logistics;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.CrossBorderWaybillStatusEnum;
import com.tsfyun.common.base.enums.logistics.CarTypeEnum;
import com.tsfyun.common.base.enums.logistics.TransportDestinationEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 跨境运输单响应实体
 * </p>
 *

 * @since 2020-04-17
 */
@Data
@ApiModel(value="CrossBorderWaybill响应对象", description="跨境运输单响应实体")
public class CrossBorderWaybillVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;
    @ApiModelProperty(value = "单据类型")
    private String billType;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "运输日期")
    private LocalDateTime transDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "发车日期")
    private LocalDateTime departureDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "到达日期")
    private LocalDateTime reachDate;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "运输业务类型")
    private String waybillBusinessType;

    @ApiModelProperty(value = "发货仓库id")
    private Long shippingWareId;

    @ApiModelProperty(value = "发货仓库名称")
    private String shippingWareName;

    @ApiModelProperty(value = "发货公司")
    private String shippingCompany;

    @ApiModelProperty(value = "发货地址")
    private String shippingAddress;

    @ApiModelProperty(value = "发货联系人")
    private String shippingLinkPerson;

    @ApiModelProperty(value = "发货联系电话")
    private String shippingLinkTel;

    @ApiModelProperty(value = "收货仓库id")
    private Long deliveryWareId;

    @ApiModelProperty(value = "收货仓库名称")
    private String deliveryWareName;

    @ApiModelProperty(value = "收货公司")
    private String deliveryCompany;

    @ApiModelProperty(value = "收货地址")
    private String deliveryAddress;

    @ApiModelProperty(value = "收货联系人")
    private String deliveryLinkPerson;

    @ApiModelProperty(value = "收货联系电话")
    private String deliveryLinkTel;

    @ApiModelProperty(value = "订车类型")
    private String conveyanceType;

    @ApiModelProperty(value = "运输供应商")
    private Long transportSupplierId;

    /**
     * 运输供应商名称
     */
    private String transportSupplierName;

    @ApiModelProperty(value = "运输工具")
    private Long conveyanceId;

    @ApiModelProperty(value = "车牌1")
    private String conveyanceNo;

    @ApiModelProperty(value = "车牌2")
    private String conveyanceNo2;

    @ApiModelProperty(value = "司机信息")
    private String driverInfo;

    @ApiModelProperty(value = "运单号")
    private String waybillNo;

    @ApiModelProperty(value = "封条号")
    private String sealNo;

    @ApiModelProperty(value = "集装箱号")
    private String containerNo;

    @ApiModelProperty(value = "送货目地")
    private String transportDestination;

    @ApiModelProperty(value = "通关口岸编号")
    private String customsCode;

    @ApiModelProperty(value = "通关口岸名称")
    private String customsName;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "订单编号")
    private String orderNos;

    private String statusDesc;

    private String transportDestinationDesc;

    private String conveyanceTypeDesc;

    public String getStatusDesc() {
        CrossBorderWaybillStatusEnum crossBorderWaybillStatusEnum = CrossBorderWaybillStatusEnum.of(statusId);
        return Objects.nonNull(crossBorderWaybillStatusEnum) ? crossBorderWaybillStatusEnum.getName() : null;
    }

    public String getTransportDestinationDesc() {
        TransportDestinationEnum transportDestinationEnum = TransportDestinationEnum.of(transportDestination);
        return Objects.nonNull(transportDestinationEnum) ? transportDestinationEnum.getName() : null;
    }

    public String getConveyanceTypeDesc() {
        CarTypeEnum carTypeEnum = CarTypeEnum.of(conveyanceType);
        return Objects.nonNull(carTypeEnum) ? carTypeEnum.getName() : null;
    }
}
