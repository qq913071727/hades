package com.tsfyun.scm.dto.finance;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class PaymentApplyDTO implements Serializable {

    @NotNull(message = "请填写付汇信息")
    @Valid
    private PaymentAccountDTO payment;

    @NotNull(message = "请选择订单信息")
    @Size(min = 1,max = 500,message = "请至少选择一条订单付汇")
    @Valid
    private List<PaymentOrderDTO> orders;
}
