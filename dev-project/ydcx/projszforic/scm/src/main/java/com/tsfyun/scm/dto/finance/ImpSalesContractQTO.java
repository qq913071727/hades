package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 * 销售合同请求实体
 * </p>
 *
 * @since 2020-05-19
 */
@Data
@ApiModel(value="ImpSalesContract查询请求对象", description="销售合同查询请求实体")
public class ImpSalesContractQTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   /**
    * 系统单号
    */
   private String docNo;

   /**
    * 发票申请号
    */
   private String invoiceNo;

   /**
    * 订单号
    */
   private String impOrderNo;

   private String customerName;

   /**
    * 开始合同日期
    */
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private LocalDateTime contractDateStart;

   /**
   结束合同日期
    */
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private LocalDateTime contractDateEnd;

   public void setContractDateEnd(LocalDateTime contractDateEnd) {
      if(contractDateEnd != null){
         this.contractDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(contractDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
      }
   }


}
