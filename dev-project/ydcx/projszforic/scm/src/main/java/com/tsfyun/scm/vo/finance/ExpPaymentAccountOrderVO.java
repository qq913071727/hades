package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 付款订单明细响应实体
 * </p>
 *
 *
 * @since 2021-11-17
 */
@Data
@ApiModel(value="ExpPaymentAccountOrder响应对象", description="付款订单明细响应实体")
public class ExpPaymentAccountOrderVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "结汇汇率")
    private BigDecimal customsRate;

    @ApiModelProperty(value = "付款单主单ID")
    private Long paymentAccountId;


}
