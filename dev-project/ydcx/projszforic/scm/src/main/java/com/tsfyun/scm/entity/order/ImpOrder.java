package com.tsfyun.scm.entity.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 进口订单
 * </p>
 *

 * @since 2020-04-08
 */
@Data
public class ImpOrder extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 订单编号
     */

    private String docNo;

    /**
     * 客户单号
     */

    private String clientNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 客户供应商
     */

    private Long supplierId;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 订单日期-由后端写入
     */

    private LocalDateTime orderDate;

    /**
     * 实际通关日期-由报关单反写
     */

    private LocalDateTime clearanceDate;

    /**=
     * 报关单号
     */
    private String declarationNo;

    /**
     * 生成报关单确定进口
     */

    private Boolean isImp;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 币制
     */

    private String currencyName;

    /**
     * 业务类型-枚举
     */

    private String businessType;

    /**=
     * 报关类型
     */
    private String declareType;

    /**
     * 报价
     */

    private Long impQuoteId;

    /**
     * 委托金额
     */

    private BigDecimal totalPrice;

    /**
     * 报关金额
     */

    private BigDecimal decTotalPrice;

    /**
     * 进口结算汇率
     */

    private BigDecimal impRate;

    /**=
     * 海关汇率
     */
    private BigDecimal customsRate;

    /**
     * 转美金汇率(海关汇率)
     */

    private BigDecimal usdRate;

    /**
     * 总箱数
     */

    private Integer totalCartonNum;

    /**
     * 明细条数
     */
    private Integer totalMember;

    /**
     * 总净重
     */

    private BigDecimal totalNetWeight;

    /**
     * 总毛重
     */

    private BigDecimal totalCrossWeight;

    /**
     * 已付汇金额
     */

    private BigDecimal payVal;

    /**
     * 成交方式
     */

    private String transactionMode;

    /**
     * 销售人员-计算提成用
     */

    private Long salePersonId;

    /**
     * 销售合同号
     */

    private String salesContractNo;

    /**
     * 发票申请号
     */

    private String invoiceNo;

    /**
     * 付汇申请号-多个逗号隔开
     */

    private String paymentNo;

    /**
     * 备注
     */

    private String memo;


    private Long subjectId;//外贸合同买方

    private Long subjectOverseasId;//外贸合同卖方

    /**
     * 跨境运输单号
     */
    private String transportNo;

    private String transCosts;//运费
    private String insuranceCosts;//保费
    private String miscCosts;//杂费
    private Long declarationTempId;//报关模板
}
