package com.tsfyun.scm.vo.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class ThreeLoginVO implements Serializable {

    private String openId;
    private String unionId;
    private String nickName;
    private String gender;
    private String avatarUrl;//头像
    private Long personId;//已登录用户绑定微信的人员id
    private String source;//(wxxcx:微信小程序 wxgzh:微信公众号)

}
