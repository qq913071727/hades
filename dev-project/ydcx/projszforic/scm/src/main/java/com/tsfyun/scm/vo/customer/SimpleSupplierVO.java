package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

@Data
public class SimpleSupplierVO implements Serializable {

    private Long id;

    private String name;

}
