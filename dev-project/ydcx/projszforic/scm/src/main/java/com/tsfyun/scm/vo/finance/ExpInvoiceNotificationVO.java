package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: 开票通知书
 * @CreateDate: Created in 2021/11/5 17:02
 */
@Data
public class ExpInvoiceNotificationVO implements Serializable {

    /**
     * 主体
     */
    private String subjectName;

    /**
     * 主体地址
     */
    private String subjectAddress;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 合同号
     */
    private String expOrderNo;

    /**
     * 主体税号
     */
    private String subjectSocialNo;

    /**
     * 主体开户行
     */
    private String subjectBankName;

    /**
     * 主体银行账号
     */
    private String subjectBankNo;

    /**
     * 申报日期
     */
    @JsonFormat(pattern = "yyyy/MM/dd")
    private LocalDateTime decDate;

    /**
     * 出口总价
     */
    private BigDecimal expAmount;

    /**
     * 含税总价
     */
    private BigDecimal purchaseContractAmount;

    /**
     * 结汇款
     */
    private BigDecimal settlementAmount;

    /**
     * 垫资费（代理费）
     */
    private BigDecimal agentAmount;

    /**
     * 杂费开支  在税款中需要扣的杂费，如单独支付不需要取数
     */
    private BigDecimal otherAmount;

    /**
     * 币制
     */
    private String currencyCode;

    /**
     * 明细
     */
    private List<ExpInvoiceNotificationMember> members;

    @Data
    public static class ExpInvoiceNotificationMember {
        /**
         * 品名
         */
        private String goodsName;

        /**
         * 品牌
         */
        private String goodsBrand;

        /**
         * 型号
         */
        private String goodsModel;

        /**
         * 数量
         */
        private BigDecimal quantity;

        /**
         * 单位
         */
        private String unitName;

        /**
         * 出口单价
         */
        private BigDecimal expUnitPrice;

        /**
         * 出口总价
         */
        private BigDecimal expTotalPrice;

        /**
         * 结汇款（人民币）
         */
        private BigDecimal settlementAccountCny;

        /**
         * 含税单价（人民币）
         */
        private BigDecimal purchaseContractUnitPrice;

        /**
         * 含税总金额（人民币）
         */
        private BigDecimal purchaseContractAmount;
    }
}
