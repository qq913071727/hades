package com.tsfyun.scm.service.base;

import com.tsfyun.scm.system.vo.*;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * @Description:
 * @since Created in 2020/4/8 13:51
 */
public interface ISystemCacheService {

    /**
     * 根据类型获取基础数据
     * @param type
     * @return
     */
    List<BaseDataVO> getBaseDataByTpe(@NonNull String type);

    /**
     * 根据类型和编码获取基础数据
     * @param type
     * @return
     */
    BaseDataVO getBaseDataByTpeAndCode(@NonNull String type,@NonNull String code);

    /**
     * 根据国家编码获取国家
     * @param id
     * @return
     */
    CountryVO getCountryById(@NonNull String id);

    /**
     * 根据国家编码获取国家
     * @param name
     * @return
     */
    CountryVO getCountryByName(@NonNull String name);

    /**
     * 根据单位编码获取信息
     * @param id
     * @return
     */
    UnitVO getUnitById(@NonNull String id);

    /**
     * 根据单位名称获取信息
     * @param name
     * @return
     */
    UnitVO getUnitByName(@NonNull String name);

    /**
     * 根据类型和报关编码获取基础数据
     * @param type
     * @return
     */
    BaseDataVO getBaseDataByTpeAndDecCode(@NonNull String type,@NonNull String decCode);

    /**
     * 根据海关编码获取数据
     * @param customsCode
     * @return
     */
    CustomsCodeVO getCustomsCodeInfo(@NonNull String customsCode);

    /**
     * 根据海关编码获取要素信息
     * @param customsCode
     * @return
     */
    List<CustomsElementsVO> getCustomsElementsByHsCode(@NonNull String customsCode);

    /**
     * 根据税收分类编码获取税收分类数据
     * @param code
     * @return
     */
    TaxCodeBaseVO getTaxCodeBaseByCode(@NonNull String code);

    /**
     * 获取最惠国税率大于0的海关编码数据
     * @return
     */
    List<TariffCustomsCodeVO> tariffCustoms();
}
