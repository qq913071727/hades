package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * @Description:
 * @since Created in 2020/4/22 14:40
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GrossMassMeasure {

    @XmlAttribute(name = "unitCode")
    private String unitCode;

    @XmlValue
    private String value;

    public GrossMassMeasure() {

    }

    public GrossMassMeasure(String unitCode, String value) {
        this.unitCode = unitCode;
        this.value = value;
    }

}
