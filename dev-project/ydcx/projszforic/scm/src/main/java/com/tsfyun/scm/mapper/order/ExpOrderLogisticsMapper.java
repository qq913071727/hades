package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.ExpOrderLogistics;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.ExpOrderLogisticsSimpleVO;
import com.tsfyun.scm.vo.order.ExpOrderLogisticsVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsSimpleVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 出口订单物流信息 Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Repository
public interface ExpOrderLogisticsMapper extends Mapper<ExpOrderLogistics> {

    public ExpOrderLogisticsVO findById(@Param(value = "id") Long id);

    List<ExpOrderLogisticsSimpleVO> getCharterCarDeliveryInfo(@Param(value = "orderDate") LocalDateTime orderDate);
}
