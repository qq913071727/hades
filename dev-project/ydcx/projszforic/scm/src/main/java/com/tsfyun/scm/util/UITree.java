package com.tsfyun.scm.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.tsfyun.scm.entity.system.SysMenu;
import lombok.Data;

/**
 * 构建菜单树
 */
@Data
public class UITree {

    private static Integer count = 0;

    private String id; //菜单id

    private String parentId;//父菜单id

    private String code;//菜单编码

    private String name;//菜单名称

    private Integer level;//菜单级别

    private boolean leaf; //是否子菜单

    private String url;//跳转url

    private int sort;//菜单排序

    private String remark;//备注

    private String icon;//菜单图标

    private int type ; //菜单类型 1-目录；2-菜单（跳转页面）；3-按钮

    /**
     * 按钮菜单函数
     * 此字段的本意是希望便利前端处理，需结合前端考量
     */
    private String action;

    private String operationCode;

    private Integer checkNum;

    //子菜单，临时非数据库字段
    @Transient
    private List<UITree> childs;

    public UITree() {

    }

    /**
     * 构建所有菜单树
     * @param menus
     */
    public static List<UITree> buildTree(List<SysMenu> menus){
        List<UITree> uiTreeList = new ArrayList<UITree>(menus.size());
        menus.stream().forEach(r -> {
              //根菜单（查询时已过滤）
            if(null == r.getParentId()) {
                UITree tree = new UITree();
                tree.setId(r.getId());
                tree.setName(r.getName());
                tree.setLeaf(null != tree.getParentId());
                tree.setUrl(r.getUrl());
                tree.setSort(r.getSort());
                tree.setType(r.getType());
                tree.setIcon(r.getIcon());
                tree.setParentId(r.getParentId());
                tree.setRemark(r.getRemark());
                tree.setAction(r.getAction());
                tree.setOperationCode(r.getOperationCode());
                tree.setCheckNum(r.getCheckNum());
                tree.setLevel(count);

                uiTreeList.add(buildTree(tree, menus));
                count = 0;
            }
        });
        return  uiTreeList;
    }

    /**
     * 构建单个菜单树
     * @param tree 根菜单
     * @param menus 所有菜单
     * @return
     */
    public static UITree buildTree(UITree tree,List<SysMenu> menus){
        count ++ ;
        List<UITree> sub = subMenu(tree,menus);
        for(UITree child:sub){
            UITree t = buildTree(child,menus);
            if(tree.childs != null){
                tree.childs.add(t);
            }else {
                tree.childs = new ArrayList<UITree>(){
                    {
                        add(t) ;
                    }
                };
            }
        }
        return  tree;
    }

    //获取菜单子菜单
    public static List<UITree> subMenu(UITree r,List<SysMenu> menus){
        List<UITree> sub = new ArrayList<>(menus.size());
        //获取菜单r的子菜单
        for(SysMenu menu : menus){
            if(r.getId().equals(menu.getParentId())){
                UITree tree = new UITree();
                tree.setId(menu.getId());
                tree.setName(menu.getName());
                tree.setParentId(menu.getParentId());
                tree.setLeaf(null != tree.getParentId());
                tree.setUrl(menu.getUrl());
                tree.setSort(menu.getSort());
                tree.setType(menu.getType());
                tree.setIcon(menu.getIcon());
                tree.setRemark(menu.getRemark());
                tree.setAction(menu.getAction());
                tree.setOperationCode(menu.getOperationCode());
                tree.setCheckNum(r.getCheckNum());
                tree.setLevel(count);

                sub.add(tree);
            }
        }
        return  sub;
    }

}
