package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.FileDTO;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 保存出口订单请求实体
 * @CreateDate: Created in 2021/1/26 16:26
 */
@ApiModel(value = "出口订单请求数据")
@Data
public class ExpOrderPlusDTO implements Serializable {

    /**
     * 订单主单
     */
    @NotNull(message = "请填写订单信息")
    @Valid
    @ApiModelProperty(value = "订单")
    private ExpOrderDTO order;

    /**
     * 订单明细
     */
    @ApiModelProperty(value = "订单明细")
    @NotNull(message = "请填写订单明细数据",groups = UpdateGroup.class)
    @Size(max = 50,message = "订单产品信息最多50条",groups = AddGroup.class)
    @Size(min = 1,max = 50,message = "订单产品信息只能为1-50条",groups = UpdateGroup.class)
    @Valid
    private List<ExpOrderMemberDTO> members;

    /**
     * 物流信息
     */
    @ApiModelProperty(value = "物流信息")
    @NotNull(message = "请填写订单物流数据",groups = UpdateGroup.class)
    @Valid
    private ExpOrderLogisticsDTO logistiscs;

    @ApiModelProperty(value = "临时保存或提交")
    private Boolean submitAudit;

    /**
     * 文件信息
     */
    @ApiModelProperty(value = "文件信息")
    @Valid
    private FileDTO file;

}
