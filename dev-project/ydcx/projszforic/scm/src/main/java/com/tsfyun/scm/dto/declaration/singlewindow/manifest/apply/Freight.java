package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 运费支付信息
 * @since Created in 2020/4/22 12:10
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Freight", propOrder = {
        "paymentMethodCode",
})
public class Freight {

    //运费支付方法代码，参考CN004，定义枚举PaymentMethodCodeEnum
    @XmlElement(name = "PaymentMethodCode")
    private String paymentMethodCode;

    public Freight() {

    }

    public Freight (String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }
}
