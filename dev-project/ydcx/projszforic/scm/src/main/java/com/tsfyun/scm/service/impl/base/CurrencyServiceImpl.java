package com.tsfyun.scm.service.impl.base;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.base.CurrencyDTO;
import com.tsfyun.scm.dto.base.CurrencyQTO;
import com.tsfyun.scm.entity.base.Currency;
import com.tsfyun.scm.mapper.base.CurrencyMapper;
import com.tsfyun.scm.service.base.ICurrencyService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.base.CurrencyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  币制服务实现类
 * </p>
 *

 * @since 2020-03-18
 */
@Service
public class CurrencyServiceImpl extends ServiceImpl<Currency> implements ICurrencyService {

    @Autowired
    private CurrencyMapper currencyMapper;

    @Override
    public List<CurrencyVO> select() {
        return currencyMapper.findAll();
    }

    @Override
    public Currency findById(String id) {
        if(StringUtils.isEmpty(id)){ return null; }

        return super.getById(id);
    }

    @Override
    public Currency findByName(String name) {
        if(StringUtils.isEmpty(name)){ return null; }

        Currency currency = new Currency();
        currency.setName(name);
        return currencyMapper.selectOne(currency);
    }

    @Override
    public PageInfo<Currency> page(CurrencyQTO dto) {
        PageHelper.startPage(dto.getPage(),dto.getLimit());
        List<Currency> currencies = currencyMapper.list(dto);
        return new PageInfo<>(currencies);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDisabled(String id, Boolean disabled) {
        Currency currency = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(currency),new ServiceException("币制信息不存在"));

        Currency update = new Currency();
        update.setDisabled(disabled);
        currencyMapper.updateByExampleSelective(update, Example.builder(Currency.class).where(
                TsfWeekendSqls.<Currency>custom().andEqualTo(false,Currency::getId, id)).build()
        );
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void adjustSort(String id, Integer sort) {
        //排序不能大于99
        TsfPreconditions.checkArgument(sort <= 99,new ServiceException("排序值不能大于99"));
        Currency currency = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(currency),new ServiceException("币制信息不存在"));

        Currency update = new Currency();
        update.setSort(sort);
        currencyMapper.updateByExampleSelective(update, Example.builder(Currency.class).where(
                TsfWeekendSqls.<Currency>custom().andEqualTo(false,Currency::getId, id)).build()
        );
    }
}
