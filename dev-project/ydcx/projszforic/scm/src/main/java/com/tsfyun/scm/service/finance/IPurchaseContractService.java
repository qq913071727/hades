package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.*;
import com.tsfyun.scm.entity.finance.PurchaseContract;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.CreateDrawbackPlusVO;
import com.tsfyun.scm.vo.finance.ExpInvoiceNotificationVO;
import com.tsfyun.scm.vo.finance.PurchaseContractPlusVO;
import com.tsfyun.scm.vo.finance.PurchaseContractVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 采购合同 服务类
 * </p>
 *
 *
 * @since 2021-09-26
 */
public interface IPurchaseContractService extends IService<PurchaseContract> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<PurchaseContractVO> pageList(PurchaseContractQTO qto);

    /**
     * 详情
     * @param id
     * @param operation
     * @return
     */
    PurchaseContractPlusVO detailPlus(Long id,String operation);

    /**
     * 详情
     * @param id
     * @param operation
     * @param queryCost
     * @return
     */
    PurchaseContractPlusVO detailPlus(Long id,String operation,Boolean queryCost);

    /**
     * 删除合同
     * @param id
     */
    void delete(Long id);

    /**
     * 根据订单ID生成采购合同
     * @param orderId
     * @return
     */
    CreateContractPlusVO createByOrderId(Long orderId);

    /**
     * 根据结汇金额计算实际采购总价
     * @param orderId
     * @param settlementValue
     * @return
     */
    BigDecimal calculationActualPurchaseValue(Long orderId,BigDecimal settlementValue);

    /**
     * 重新计算费用
     * @param dto
     * @return
     */
    CreateContractPlusVO recalculate(PurchaseContractDTO dto);

    /**
     * 保存合同
     * @param dto
     */
    void save(PurchaseContractDTO dto);

    /**
     * 采购合同确认
     * @param dto
     */
    void confirm(TaskDTO dto);

    /**
     * 合同收票
     * @param dto
     */
    void confirmInvoice(PurchaseInvoiceDTO dto);

    /**
     * 初始化退税
     * @param ids
     * @return
     */
    CreateDrawbackPlusVO initDrawback(List<Long> ids);
    CreateDrawbackPlusVO initDrawback(List<Long> ids,BigDecimal billie);

    /**
     * 根据单号查询采购合同
     * @param docNo
     * @return
     */
    PurchaseContract findByDocNo(String docNo);

    /**
     * 根据订单号查询采购合同
     * @param expOrderNo
     * @return
     */
    PurchaseContract findByExpOrderNo(String expOrderNo);

    /**
     * 获取开票通知数据
     * @param expOrderNo
     * @return
     */
    ExpInvoiceNotificationVO obtainExpInvoiceNotificationData(String expOrderNo);

    /**
     * 税局退税登记
     * @param dto
     */
    void taxBureauRegister(TaxBureauDTO dto);

    /**
     * 发送邮件给客户
     * @param id
     */
    void sendMail(Long id,List<String> receivers);

    /**
     * 生成合同PDF
     * @param id
     * @return
     */
    Long contractPdf(Long id);

    /**
     * 获取发送默认邮箱
     * @param customerName
     * @return
     */
    String getDefaultMail(String customerName);


    /**
     * 生成送货单PDF
     * @param id
     * @return
     */
    Long deliveryPdf(Long id);
}
