package com.tsfyun.scm.dto.wms;

import com.tsfyun.scm.entity.wms.ReceivingNoteMember;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReceivingMemberSaveDTO implements Serializable {

    //要新增修改的明细
    private List<ReceivingNoteMember> saveMembers;

    //要删除的明细
    private List<ReceivingNoteMember> deleteMembers;
}
