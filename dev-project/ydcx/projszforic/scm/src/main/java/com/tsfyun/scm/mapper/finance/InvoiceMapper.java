package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.finance.client.ClientInvoiceQTO;
import com.tsfyun.scm.entity.finance.Invoice;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.InvoiceVO;
import com.tsfyun.scm.vo.finance.client.ClientInvoiceListVO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 发票 Mapper 接口
 * </p>
 *

 * @since 2020-05-15
 */
@Repository
public interface InvoiceMapper extends Mapper<Invoice> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<InvoiceVO> list(Map<String,Object> params);

    /**
     * 客户端查询
     * @param qto
     * @return
     */
    List<ClientInvoiceListVO> clientPageList(ClientInvoiceQTO qto);
}
