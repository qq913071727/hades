package com.tsfyun.scm.vo.customer.client;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/10/10 09:54
 */
@Data
public class ClientSupplierTakeInfoVO implements Serializable {

    private Long id;

    @ApiModelProperty(value = "供应商")
    private Long supplierId;

    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(value = "提货公司")
    private String companyName;

    @ApiModelProperty(value = "联系人")
    private String linkPerson;

    @ApiModelProperty(value = "联系电话")
    private String linkTel;

    @ApiModelProperty(value = "省")
    private String provinceName;

    @ApiModelProperty(value = "市")
    private String cityName;

    @ApiModelProperty(value = "区")
    private String areaName;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "默认地址")
    private Boolean isDefault;

}
