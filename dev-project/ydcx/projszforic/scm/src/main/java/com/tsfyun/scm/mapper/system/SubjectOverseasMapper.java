package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.dto.system.SubjectOverseasQTO;
import com.tsfyun.scm.entity.system.SubjectOverseas;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.system.SubjectOverseasVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Repository
public interface SubjectOverseasMapper extends Mapper<SubjectOverseas> {

    List<SubjectOverseasVO> findList(SubjectOverseasQTO qto);
    SubjectOverseasVO defOverseas();
}
