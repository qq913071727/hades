package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.order.ExpOrderCostQTO;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.scm.entity.order.ExpOrderCost;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface IExpOrderCostService extends IService<ExpOrderCost> {
    /**=
     * 订单费用计算
     * @param expOrder
     */
    void calculationCost(ExpOrder expOrder);
    void calculationCost(ExpOrder expOrder, LocalDateTime orderDate);

    /**
     * 校验订单是否可以调整费用，并可以同时返回费用信息
     * @param expOrderNo
     * @param getCost
     */
    List<ExpOrderCostVO> checkGetAdjustCost(String expOrderNo, Boolean getCost);

    /**
     * 删除系统生成费用
     * @param orderId
     */
    void removeAutomaticOrderId(Long orderId);

    /**
     * 根据订单id或者订单号获取所有的费用信息
     * @param orderId
     * @return
     */
    List<ExpOrderCostVO> getAllCosts(Long orderId, String expOrderNo);

    /**
     * 根据订单ID和支付类型获取订单费用
     * @param orderId
     * @param collectionSources
     * @return
     */
    List<ExpOrderCostVO> findCostByOrderIdAndCollectionSource(Long orderId, List<String> collectionSources);

    /**
     * 根据费用科目编码获取费用信息
     * @param expenseSubjectId
     * @return
     */
    List<ExpOrderCost> getByExpenseSubject(Long orderId, String expenseSubjectId);

    /**
     * 获取订单费用的信息
     * @param expOrderCostId
     * @return
     */
    ExpOrderCostVO getSimpleCostInfo(Long expOrderCostId);

    /**
     * 根据订单ID删除费用
     * @param orderId
     */
    void removeByOrderId(Long orderId);

    /**
     * 根据订单查询可核销的费用
     * @param orderId
     * @return
     */
    List<ExpOrderCostVO> canWriteOffByOrderId(Long orderId);

    /**=
     * 根据客户获取可核销的费用
     * @param customerId
     * @return
     */
    List<ExpOrderCostVO> canWriteOffList(Long customerId);

    /**
     * 获取订单应收代垫费金额
     * @param orderId
     * @return
     */
    BigDecimal obtainOrderAdvancePayment(Long orderId, String collectionSource);

    /**
     * 订单采购合同收票完成
     * @param orderId
     */
    void purchaseInvoiceCompleted(Long orderId);

    /**=
     * 出口锁定费用
     * @param orderId
     */
    void expLockCost(Long orderId);

    /**=
     * 获取订单未锁定的费用
     * @param orderId
     * @return
     */
    List<ExpOrderCostVO> obtainOrderNotLockCosts(Long orderId);

    /**
     * 解锁费用
     * @param costIds
     */
    void expUnLockCost(List<Long> costIds);

    /**
     * 根据订单号和费用科目id获取费用信息
     * @param expOrderNo
     * @param expenseSubjectId
     * @return
     */
    ExpOrderCost findByExpOrderNoAndExpenseSubjectId(String expOrderNo,String expenseSubjectId);

    /**
     * 根据订单号批量获取费用信息
     * @param expOrderNos
     * @return
     */
    List<ExpOrderCost> findByExpOrderNoBatch(List<String> expOrderNos);

    /**
     * 分页查询
     * @param qto
     * @return
     */
    PageInfo<ExpOrderCostVO> pageList(ExpOrderCostQTO qto);
}
