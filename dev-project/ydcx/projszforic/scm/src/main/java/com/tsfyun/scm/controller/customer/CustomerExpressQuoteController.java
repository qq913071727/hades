package com.tsfyun.scm.controller.customer;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.customer.ICustomerExpressQuoteService;
import com.tsfyun.scm.vo.customer.CustomerExpressQuotePlusVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 客户快递模式代理费报价 前端控制器
 * </p>
 *
 *
 * @since 2020-10-30
 */
@RestController
@RequestMapping("/customerExpressQuote")
public class CustomerExpressQuoteController extends BaseController {

    @Autowired
    private ICustomerExpressQuoteService customerExpressQuoteService;

    /**
     * 获取客户代理费报价
     * @param quoteType
     * @return
     */
    @PostMapping(value = "getCustomerExpressQuote")
    Result<CustomerExpressQuotePlusVO> getCustomerExpressQuote(@RequestParam(value = "customerId",required = false)Long customerId,@RequestParam(value = "quoteType")String quoteType) {
        return success(customerExpressQuoteService.getCustomerExpressQuote(customerId,quoteType));
    }

    /**
     * 根据客户名称获取代理费报价
     * @param quoteType
     * @return
     */
    @PostMapping(value = "getCustomerExpressQuoteByName")
    Result<CustomerExpressQuotePlusVO> getCustomerExpressQuoteByName(@RequestParam(value = "customerName",required = false)String customerName,@RequestParam(value = "quoteType")String quoteType) {
        return success(customerExpressQuoteService.getCustomerExpressQuoteByName(customerName,quoteType));
    }

}

