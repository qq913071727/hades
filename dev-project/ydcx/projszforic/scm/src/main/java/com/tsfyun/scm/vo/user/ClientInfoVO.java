package com.tsfyun.scm.vo.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientInfoVO implements Serializable {
    /**
     * 员工id
     */
    private Long personId;

    /**
     * 员工名称，如昵称
     */
    private String personName;

    /**
     * 用户头像
     */
    private String userHead;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 客户名称
     */
    private String customerName;
    /**
     * 客户状态
     */
    private String status;
    /**
     * 客户状态
     */
    private String statusName;
}
