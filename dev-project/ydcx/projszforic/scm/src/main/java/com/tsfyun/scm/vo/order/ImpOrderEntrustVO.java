package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: 进口货物委托确认单
 * @CreateDate: Created in 2021/12/6 09:07
 */
@Data
public class ImpOrderEntrustVO implements Serializable {

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 主体企业名称
     */
    private String subjectName;

    /**
     * 供应商
     */
    private String supplierName;

    /**
     * 进出境关别
     */
    private String iePortName;


    /**
     * 境外交货方式
     */

    private String deliveryModeName;

    private String logisticsCompanyName;

    /**
     * 提货联系人
     */

    private String takeLinkPerson;

    /**
     * 提货联系人电话
     */

    private String takeLinkTel;

    /**
     * 提货地址
     */

    private String takeAddress;


    /**
     * 国内收货方式
     */

    private String receivingModeName;

    /**
     * 提货仓库名称
     */

    private String selfWareName;

    /**
     * 提货联系人电话
     */

    private String selfLinkPerson;

    /**
     * 提货人身份证号
     */

    private String selfPersonId;

    /**
     * 提货备注
     */

    private String selfMemo;
    /**
     * 备注
     */
    private String memo;


    /**
     * 币制
     */
    private String currencyName;


    /**
     * 物料明细
     */
    private List<InnerImpOrderMember> members;

    @Data
    public static class InnerImpOrderMember {

        /**
         * 品名
         */
        private String name;

        /**
         * 品牌
         */
        private String brand;

        /**
         * 型号
         */
        private String model;

        /**
         * 产地
         */

        private String countryName;

        /**
         * 数量
         */

        private BigDecimal quantity;

        /**
         * 单位
         */

        private String unitName;


        /**
         * 报关单价
         */

        private BigDecimal decUnitPrice;
        private BigDecimal unitPrice;

        /**
         * 报关总价
         */

        private BigDecimal decTotalPrice;
        private BigDecimal totalPrice;


        /**
         * 净重
         */

        private BigDecimal netWeight;

        /**
         * 毛重
         */

        private BigDecimal grossWeight;

        /**
         * 箱数
         */

        private Integer cartonNum;

    }


}
