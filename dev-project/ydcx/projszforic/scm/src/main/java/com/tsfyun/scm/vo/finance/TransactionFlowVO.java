package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.finance.TransactionCategoryEnum;
import com.tsfyun.common.base.enums.finance.TransactionTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 交易流水响应实体
 * </p>
 *

 * @since 2020-04-16
 */
@Data
@ApiModel(value="TransactionFlow响应对象", description="交易流水响应实体")
public class TransactionFlowVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "客户id")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "交易类型")
    private String transactionType;

    @ApiModelProperty(value = "交易类别")
    private String transactionCategory;

    @ApiModelProperty(value = "交易单号")
    private String transactionNo;

    @ApiModelProperty(value = "交易金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "交易时间")
    private LocalDateTime accountDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateCreated;

    private String transactionCategoryDesc;

    private String transactionTypeDesc;

    public String getTransactionCategoryDesc() {
        TransactionCategoryEnum transactionCategoryEnum = TransactionCategoryEnum.of(transactionCategory);
        return Objects.nonNull(transactionCategoryEnum) ? transactionCategoryEnum.getName() : null;
    }

    public String getTransactionTypeDesc() {
        TransactionTypeEnum transactionTypeEnum = TransactionTypeEnum.of(transactionType);
        return Objects.nonNull(transactionTypeEnum) ? transactionTypeEnum.getName() : null;
    }
}
