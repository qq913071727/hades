//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "urn:Declaration:datamodel:standard:CN:SCMBOT-TYPE-ID:1",
        xmlns = { @XmlNs(namespaceURI = "urn:Declaration:datamodel:standard:CN:SCMBOT-TYPE-ID:1", prefix = ""),
                  @XmlNs(namespaceURI = "http://www.w3.org/2001/XMLSchema", prefix = "xmlns:xsd"),
                  @XmlNs(namespaceURI = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xmlns:xsi"),
                  },
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)

package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlNs;