package com.tsfyun.scm.controller.report;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.report.IWelcomeService;
import com.tsfyun.scm.vo.report.ImpExpAgencyFeeVO;
import com.tsfyun.scm.vo.report.ImpExpTotalDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/welcome")
public class WelcomeController extends BaseController {

    @Autowired
    private IWelcomeService welcomeService;


    @GetMapping(value = "impExpTotalData")
    public Result<ImpExpTotalDataVO> impExpTotalData(){
        return success(welcomeService.impExpTotalData());
    }


    /**
     * 月度业务量报表
     * @param year
     * @return
     */
    @GetMapping(value = "monthImpExpTotalData")
    public Result<Map<String,ImpExpTotalDataVO>> monthImpExpTotalData(@RequestParam(value = "year",required = false)String year){
        return success(welcomeService.monthSummaryImpExpTotalData(year));
    }

    /**
     * 月度代理费
     * @param year
     * @return
     */
    @GetMapping(value = "monthImpExpAgencyFee")
    public Result<List<ImpExpAgencyFeeVO>> monthImpExpAgencyFee(@RequestParam(value = "year",required = false)String year){
        return success(welcomeService.monthImpExpAgencyFee(year));
    }

}
