package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.scm.dto.customer.AgreemenPlusDTO;
import com.tsfyun.scm.dto.customer.AgreementQTO;
import com.tsfyun.scm.dto.customer.SubstituteAgreementDTO;
import com.tsfyun.scm.dto.customer.client.ClientServiceAgreementSignDTO;
import com.tsfyun.scm.entity.customer.Agreement;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.file.UploadFile;
import com.tsfyun.scm.vo.customer.*;
import com.tsfyun.scm.vo.customer.client.ClientImpQuotePlusVO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 协议报价单 服务类
 * </p>
 *
 *
 * @since 2020-03-31
 */
public interface IAgreementService extends IService<Agreement> {

    /**
     * 分页查询协议报价单
     * @param qto
     * @return
     */
    PageInfo<AgreementVO> list(AgreementQTO qto);

    /**
     * 获取详情(包含报价信息)
     * @param id
     * @return
     */
    AgreementDetailPlusVO detailPlus(Long id);

    AgreementDetailPlusVO detailPlus(Long id,String operation);

    /**=
     * 添加协议
     * @param dto
     * @return
     */
    Long addPlus(AgreemenPlusDTO dto);

    /**=
     * 修改协议
     * @param dto
     * @return
     */
    void editPlus(AgreemenPlusDTO dto);

    /**=
     * 删除协议
     * @param id
     */
    void delete(Long id);

    /**=
     * 审核意见
     * @param dto
     */
    void examine(TaskDTO dto);

    /**=
     * 原件签回
     * @param ids
     */
    void originSignBack(List<Long> ids);

    /**
     * 获取客户有效协议报价单
     * @param customerId
     * @return
     */
    Agreement getEffectiveAgreement(Long customerId,@NonNull BusinessTypeEnum businessTypeEnum);


    /**=
     * 进口下单获取客户有效协议报价
     * @param customerId
     * @return
     */
    List<ImpAgreementVO> impAgreement(Long customerId);
    /**=
     * 进口下单获取客户有效协议报价(数据验证)
     * @param customerId
     * @return
     */
    List<ImpAgreementVO> impAgreementCheck(Long customerId);
    List<ImpAgreementVO> clientImpAgreementCheck();

    /**
     * 获取客户最新的有效的协议报价
     * @param customerId
     * @return
     */
    Agreement obtainCustomerNewEffective(Long customerId);

    /**=
     * 验证客户协议是否有效
     * @param id
     * @return
     */
    Agreement validationAgreement(Long id);

    /**
     * 初始化客户签约
     * @param customerId
     * @return
     */
    List<ClientImpQuotePlusVO> initSign(Long customerId);

    /**
     * 客户签约报价
     * @param businessTypeEnum
     * @param clientImpQuotePlusVOS
     */
    void agreeSignAgreement(ServiceAgreementVO serviceAgreement,BusinessTypeEnum businessTypeEnum, List<ClientImpQuotePlusVO> clientImpQuotePlusVOS);

    /**
     * 检查客户是否存在需要签约的协议
     * @return
     */
    Boolean existSignContract();

    /**
     * 代替客户确认进口报价
     * @param dto
     */
    void substituteCustomerSign(SubstituteAgreementDTO dto);

    /**
     * 客户端加载协议报价
     * @return
     */
    ServiceAgreementQuotePlusVo clientLoad();

    /**
     * 客户同意签署
     * @param dto
     */
    void agreeSign(ClientServiceAgreementSignDTO dto);

    /**
     * 定时扫描失效出口报价
     */
    void scheduleInvalidImpAgreement();

    /**
     * 导出审批表
     * @return
     */
    HSSFWorkbook exportApprovalForm(Long id);

    /**
     * 获取打印审批表数据(包含报价信息)
     * @param id
     * @return
     */
    AgreementApprovalVO getPrintApproval(Long id);
}
