package com.tsfyun.scm.mapper.support;

import com.tsfyun.scm.entity.support.TaskNoticeFocus;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 用户关注任务通知 Mapper 接口
 * </p>
 *

 * @since 2020-04-09
 */
@Repository
public interface TaskNoticeFocusMapper extends Mapper<TaskNoticeFocus> {

    void updateTaskNoticeFocus(TaskNoticeFocus taskNoticeFocus);

    List<TaskNoticeFocus> getDislikePersonByOperationCode(String operationCode);

    List<String> getDislikeOperationCodes(Long personId);

}
