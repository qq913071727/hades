package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.TaxCodeName;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 客户税收分类编码 Mapper 接口
 * </p>
 *

 * @since 2020-05-21
 */
@Repository
public interface TaxCodeNameMapper extends Mapper<TaxCodeName> {

    Integer batchInsert(@Param(value = "codeNameList") List<TaxCodeName> codeNameList);
}
