package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.annotation.LengthTrim;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class DeliveryConfirmDTO implements Serializable {

    @NotNull(message = "运单ID不能为空")
    private Long id;

    @LengthTrim(max = 50,message = "物流单号最大长度不能大于50")
    private String experssNo;
    //运输供应商
    private Long transportSupplierId;
    //运输工具
    private Long conveyanceId;
}
