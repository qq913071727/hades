package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.LoginTypeEnum;
import com.tsfyun.common.base.enums.SexTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户端更新人员基本信息
 */
@Data
public class ClientPersonInfoDTO implements Serializable {

    @ApiModelProperty(value = "姓名")
    @NotEmptyTrim(message = "姓名不能为空")
    @LengthTrim(min = 2,max = 20,message = "姓名只能为2-20位")
    private String name;

    @ApiModelProperty(value = "性别")
    @NotEmptyTrim(message = "性别不能为空")
    @EnumCheck(clazz = SexTypeEnum.class,message = "性别选择错误")
    private String gender;

    //邮箱
    @ApiModelProperty(value = "邮箱")
    private String mail;
}


