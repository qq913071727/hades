package com.tsfyun.scm.service.impl.customer;

import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.dto.customer.ExpressStandardQuoteMemberDTO;
import com.tsfyun.scm.entity.customer.ExpressStandardQuoteMember;
import com.tsfyun.scm.mapper.customer.ExpressStandardQuoteMemberMapper;
import com.tsfyun.scm.service.customer.IExpressStandardQuoteMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 快递模式标准报价计算代理费明细 服务实现类
 * </p>
 *
 *
 * @since 2020-10-26
 */
@Service
public class ExpressStandardQuoteMemberServiceImpl extends ServiceImpl<ExpressStandardQuoteMember> implements IExpressStandardQuoteMemberService {

    @Autowired
    private ExpressStandardQuoteMemberMapper expressStandardQuoteMemberMapper;

    @Resource
    private Snowflake snowflake;


    @Override
    public List<ExpressStandardQuoteMember> findByQuoteId(Long quoteId) {
        return expressStandardQuoteMemberMapper.selectByExample(Example.builder(ExpressStandardQuoteMember.class).where(TsfWeekendSqls.<ExpressStandardQuoteMember>custom().andEqualTo(false,ExpressStandardQuoteMember::getQuoteId,quoteId)).build());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addBatch(Long quoteId, List<ExpressStandardQuoteMemberDTO> memberDTOS) {
        List<ExpressStandardQuoteMember> members = Lists.newArrayListWithExpectedSize(memberDTOS.size());
        memberDTOS.stream().forEach(b->{
            ExpressStandardQuoteMember r = new ExpressStandardQuoteMember();
            r.setId(snowflake.nextId());
            r.setQuoteId(quoteId);
            r.setStartWeight(b.getStartWeight());
            r.setEndWeight(b.getEndWeight());
            r.setBasePrice(b.getBasePrice());
            r.setPrice(b.getPrice());
            members.add(r);
        });
        super.savaBatch(members);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeByQuoteId(Long quoteId) {
        expressStandardQuoteMemberMapper.deleteByExample(Example.builder(ExpressStandardQuoteMember.class).where(TsfWeekendSqls.<ExpressStandardQuoteMember>custom().andEqualTo(false,ExpressStandardQuoteMember::getQuoteId,quoteId)).build());
    }

}
