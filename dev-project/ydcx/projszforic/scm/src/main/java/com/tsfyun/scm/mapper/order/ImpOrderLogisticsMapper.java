package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.order.ImpOrderDomesticTakeQTO;
import com.tsfyun.scm.entity.order.ImpOrderLogistics;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.ImpOrderDomesticVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsSimpleVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单物流信息 Mapper 接口
 * </p>
 *

 * @since 2020-04-08
 */
@Repository
public interface ImpOrderLogisticsMapper extends Mapper<ImpOrderLogistics> {

    ImpOrderLogisticsVO findById(Long id);

    List<ImpOrderLogisticsSimpleVO> getCharterCarDeliveryInfo(@Param(value = "orderDate")LocalDateTime orderDate);

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ImpOrderDomesticVO> overseasTakeList(Map<String,Object> params);
}
