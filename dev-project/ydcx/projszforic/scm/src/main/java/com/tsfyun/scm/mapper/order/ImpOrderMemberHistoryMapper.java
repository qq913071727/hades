package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import com.tsfyun.common.base.extension.Mapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 原始订单明细 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Repository
public interface ImpOrderMemberHistoryMapper extends Mapper<ImpOrderMemberHistory> {

    void removeByOrderId(@Param(value = "orderId") Long orderId);

    Integer batchUpdateSplitMembers(@Param(value = "impOrderMemberHistoryList")List<ImpOrderMemberHistory> impOrderMemberHistoryList);
}
