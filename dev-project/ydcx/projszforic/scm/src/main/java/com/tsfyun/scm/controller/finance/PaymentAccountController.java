package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.*;
import com.tsfyun.scm.service.finance.IPaymentAccountService;
import com.tsfyun.scm.vo.finance.EditPaymentPlusVO;
import com.tsfyun.scm.vo.finance.PaymentAccountPlusDetailVO;
import com.tsfyun.scm.vo.finance.PaymentAccountVO;
import com.tsfyun.scm.vo.order.ApplyPaymentPlusVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 付款单 前端控制器
 * </p>
 *
 * @since 2020-04-23
 */
@RestController
@RequestMapping("/paymentAccount")
public class PaymentAccountController extends BaseController {

    @Autowired
    private IPaymentAccountService paymentAccountService;

    /**
     * 分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<PaymentAccountVO>> pageList(@ModelAttribute PaymentAccountQTO qto) {
        PageInfo<PaymentAccountVO> page = paymentAccountService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情信息（含明细）
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<PaymentAccountPlusDetailVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(paymentAccountService.detail(id,operation));
    }

    /**=
     * 初始化付款单修改数据
     * @param id
     * @return
     */
    @PostMapping(value = "initEdit")
    public Result<EditPaymentPlusVO> initEdit(@RequestParam(value = "id")Long id){
        return success(paymentAccountService.initEdit(id));
    }

    /**
     * 付汇申请
     * @return
     */
    @PostMapping(value = "apply")
    @DuplicateSubmit
    public Result<Void> apply(@RequestBody @Valid PaymentApplyDTO dto){
        paymentAccountService.apply(dto);
        return success();
    }

    /**=
     * 保存付汇修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    @DuplicateSubmit
    public Result<Void> edit(@RequestBody @Valid PaymentApplyDTO dto){
        paymentAccountService.edit(dto);
        return success();
    }

    /**=
     * 流程跳转
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "processJump")
    Result<Void> processJump(@ModelAttribute @Validated() TaskDTO dto){
        paymentAccountService.processJump(dto);
        return success();
    }

    /**=
     * 汇率调整
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "adjustRate")
    Result<Void> adjustRate(@ModelAttribute @Validated() AdjustRateDTO dto){
        paymentAccountService.adjustRate(dto);
        return success();
    }

    /**=
     * 手续费调整
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "adjustBankFee")
    Result<Void> adjustBankFee(@ModelAttribute @Validated() AdjustBankFeeDTO dto){
        paymentAccountService.adjustBankFee(dto);
        return success();
    }

    /**=
     * 确定付款行
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "saveConfirmBank")
    Result<Void> saveConfirmBank(@ModelAttribute @Validated() ConfirmBankDTO dto){
        paymentAccountService.saveConfirmBank(dto);
        return success();
    }
    /**=
     * 确定付汇完成
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "paymentCompleted")
    Result<Void> paymentCompleted(@ModelAttribute @Validated() PaymentCompletedDTO dto){
        paymentAccountService.paymentCompleted(dto);
        return success();
    }

    /**
     * 计算手续费
     * @return
     */
    @GetMapping(value = "calculationFee")
    Result<BigDecimal> calculationFee(
            @RequestParam(value = "orderId")Long orderId,
            @RequestParam(value = "paymentTerm")String paymentTerm,
            @RequestParam(value = "counterFeeType",required = false)String counterFeeType,
            @RequestParam(value = "swiftCode",required = false)String swiftCode
    ){
        return Result.success(paymentAccountService.calculationFee(orderId,paymentTerm,counterFeeType,swiftCode));
    }

    /**
     * 在线生成付汇委托书excel
     * @param id
     * @return
     */
    @GetMapping(value = "entrustExcel")
    public Result<Long> entrustExcel(@RequestParam(value = "id")Long id){
        return Result.success(paymentAccountService.createEntrustExcel(id));
    }

    /**
     * 删除付款单
     * @param id
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "remove")
    Result<Void> remove(@RequestParam(value = "id")Long id){
        paymentAccountService.removeAllById(id);
        return success();
    }
}

