package com.tsfyun.scm.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**=
 * 订单详情
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpOrderPlusVO implements Serializable {

    //订单主单详情
    private ImpOrderDetailVO order;

    //物流信息
    private ImpOrderLogisticsVO logistiscs;

    //产品明细
    List<ImpOrderMemberVO> members;
}
