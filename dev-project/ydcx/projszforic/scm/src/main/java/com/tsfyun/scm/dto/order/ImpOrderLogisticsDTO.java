package com.tsfyun.scm.dto.order;

import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.PaymentDeliveryEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 订单物流信息请求实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrderLogistics请求对象", description="订单物流信息请求实体")
public class ImpOrderLogisticsDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @ApiModelProperty(value = "订单物流id")
   private Long id;

   @ApiModelProperty(value = "订单id")
   private Long impOrderId;

   @NotEmptyTrim(message = "请选择境外交货方式",groups = UpdateGroup.class)
   @ApiModelProperty(value = "境外交货方式")
   private String deliveryMode;

   @LengthTrim(max = 500,message = "交货备注最大长度不能超过500位")
   @ApiModelProperty(value = "交货备注")
   private String deliveryModeMemo;

   @ApiModelProperty(value = "快递类型")
   private String inExpressType;

   @ApiModelProperty(value = "快递公司")
   private String hkExpress;

   @ApiModelProperty(value = "快递公司名称")
   private String hkExpressName;

   @ApiModelProperty(value = "快递单号")
   private String hkExpressNo;

   @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
   @ApiModelProperty(value = "要求提货时间")
   private LocalDateTime takeTime;

   @ApiModelProperty(value = "提货地址信息")
   private Long supplierTakeInfoId;

   @ApiModelProperty(value = "中港包车")
   private Boolean isCharterCar;

   @ApiModelProperty(value = "送货方式")
   private String receivingMode;

   @LengthTrim(max = 500,message = "国内配送-收货备注最大长度不能超过500位")
   @ApiModelProperty(value = "收货备注")
   private String receivingModeMemo;

   @ApiModelProperty(value = "物流公司")
   private String logisticsCompanyId;

   @LengthTrim(max = 30,message = "物流时效要求最大长度不能超过30位")
   @ApiModelProperty(value = "物流时效要求")
   private String prescription;

   @EnumCheck(clazz = PaymentDeliveryEnum.class,message = "国内配送-送货付款方式错误")
   @ApiModelProperty(value = "送货付款方式")
   private String paymentDelivery;

   @ApiModelProperty(value = "自提点")
   private Long selfWareId;

   @LengthTrim(max = 50,message = "提货联系人电话最大长度不能超过50位")
   @ApiModelProperty(value = "提货联系人电话")
   private String selfLinkPerson;

   @LengthTrim(max = 50,message = "提货人身份证号最大长度不能超过50位")
   @ApiModelProperty(value = "提货人身份证号")
   private String selfPersonId;

   @LengthTrim(max = 500,message = "提货备注最大长度不能超过500位")
   @ApiModelProperty(value = "提货备注")
   private String selfMemo;

   @ApiModelProperty(value = "收货信息")
   private Long customerDeliveryInfoId;
}
