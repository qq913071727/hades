package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.util.Date;

import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 采购合同请求实体
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Data
@ApiModel(value="PurchaseContract请求对象", description="采购合同请求实体")
public class PurchaseContractDTO implements Serializable {

   private static final long serialVersionUID=1L;
   @NotNull(message = "订单ID不能为空")
   @ApiModelProperty(value = "订单ID")
   private Long orderId;

   @NotEmptyTrim(message = "订单号不能为空")
   @ApiModelProperty(value = "订单号")
   private String orderNo;

   @NotEmptyTrim(message = "合同编号不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 20,message = "合同编号最大长度不能超过20位",groups = {AddGroup.class})
   @ApiModelProperty(value = "合同编号")
   private String docNo;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @ApiModelProperty(value = "签定日期")
   @NotNull(message = "签定日期不能为空",groups = {AddGroup.class})
   private Date signingDate;

   @NotEmptyTrim(message = "签定地点不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 20,message = "签定地点最大长度不能超过20位",groups = {AddGroup.class})
   @ApiModelProperty(value = "签定地点")
   private String signingPlace;

   @NotEmptyTrim(message = "买方单位不能为空")
   @LengthTrim(max = 200,message = "买方单位最大长度不能超过200位")
   @ApiModelProperty(value = "买方单位")
   private String buyerName;

   @NotEmptyTrim(message = "买方签约代表不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 20,message = "买方签约代表最大长度不能超过20位",groups = {AddGroup.class})
   @ApiModelProperty(value = "买方签约代表")
   private String buyerDeputy;

   @LengthTrim(max = 20,message = "买方电话最大长度不能超过20位",groups = {AddGroup.class})
   @ApiModelProperty(value = "买方电话")
   private String buyerTel;

   @NotEmptyTrim(message = "买方开户行不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 200,message = "买方开户行最大长度不能超过200位",groups = {AddGroup.class})
   @ApiModelProperty(value = "买方开户行")
   private String buyerBank;

   @NotEmptyTrim(message = "买方银行帐号不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 50,message = "买方银行帐号最大长度不能超过50位",groups = {AddGroup.class})
   @ApiModelProperty(value = "买方银行帐号")
   private String buyerBankNo;

   @LengthTrim(max = 255,message = "地址最大长度不能超过255位",groups = {AddGroup.class})
   @ApiModelProperty(value = "地址")
   private String buyerBankAddress;

   @NotEmptyTrim(message = "卖方单位不能为空")
   @LengthTrim(max = 200,message = "卖方单位最大长度不能超过200位")
   @ApiModelProperty(value = "卖方单位")
   private String sellerName;

   @NotEmptyTrim(message = "卖方签约代表不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 20,message = "卖方签约代表最大长度不能超过20位",groups = {AddGroup.class})
   @ApiModelProperty(value = "卖方签约代表")
   private String sellerDeputy;

   @LengthTrim(max = 20,message = "卖方电话最大长度不能超过20位",groups = {AddGroup.class})
   @ApiModelProperty(value = "卖方电话")
   private String sellerTel;

   @NotEmptyTrim(message = "卖方开户行不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 200,message = "卖方开户行最大长度不能超过200位",groups = {AddGroup.class})
   @ApiModelProperty(value = "卖方开户行")
   private String sellerBank;

   @NotEmptyTrim(message = "卖方帐号不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 50,message = "卖方帐号最大长度不能超过50位",groups = {AddGroup.class})
   @ApiModelProperty(value = "卖方帐号")
   private String sellerBankNo;

   @LengthTrim(max = 255,message = "卖方地址最大长度不能超过255位",groups = {AddGroup.class})
   @ApiModelProperty(value = "卖方地址")
   private String sellerBankAddress;

   @LengthTrim(max = 255,message = "结汇说明最大长度不能超过255位",groups = {AddGroup.class})
   @ApiModelProperty(value = "结汇说明")
   private String settleAccountInfo;

   @ApiModelProperty(value = "币制")
   @NotEmptyTrim(message = "币制不能为空",groups = {AddGroup.class})
   private String currencyId;

   @ApiModelProperty(value = "币制")
   @NotEmptyTrim(message = "币制不能为空",groups = {AddGroup.class})
   private String currencyName;

   @NotNull(message = "汇率不能为空")
   @Digits(integer = 4, fraction = 6, message = "汇率整数位不能超过4位，小数位不能超过6")
   @ApiModelProperty(value = "汇率")
   private BigDecimal exchangeRate;

   @NotNull(message = "票差调整金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "票差调整金额整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "票差调整金额")
   private BigDecimal differenceVal;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位",groups = {AddGroup.class})
   @ApiModelProperty(value = "备注")
   private String memo;

}
