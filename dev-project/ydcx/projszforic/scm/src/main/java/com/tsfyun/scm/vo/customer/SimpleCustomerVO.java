package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

@Data
public class SimpleCustomerVO implements Serializable {

    private Long id;

    private String code;

    private String name;

    private String socialNo;

    private String legalPerson;

    private String tel;

    private String fax;

    private String address;
}
