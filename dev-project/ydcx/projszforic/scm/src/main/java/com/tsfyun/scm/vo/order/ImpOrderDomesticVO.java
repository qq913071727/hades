package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description:
 * @CreateDate: Created in 2021/12/14 14:22
 */
@Data
public class ImpOrderDomesticVO implements Serializable {

    private Long id;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 订单号
     */
    private String orderNo;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;

    /**
     * 客户单号
     */
    private String customerOrderNo;

    /**
     * 是否提货
     */
    private Boolean overseasTakeFlag;


    /**
     * 提货公司
     */

    private String takeLinkCompany;

    /**
     * 提货联系人
     */

    private String takeLinkPerson;

    /**
     * 提货联系人电话
     */

    private String takeLinkTel;

    /**
     * 提货地址
     */

    private String takeAddress;

    /**
     * 提货时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime overseasTakeTime;

}
