package com.tsfyun.scm.entity.user;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * 员工角色
 */
@Data
public class PersonRole extends BaseEntity implements Serializable {

    private Long personId;

    private String roleId;

}