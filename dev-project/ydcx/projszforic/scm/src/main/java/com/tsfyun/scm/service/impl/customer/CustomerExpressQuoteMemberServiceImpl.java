package com.tsfyun.scm.service.impl.customer;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.tsfyun.scm.entity.customer.CustomerExpressQuoteMember;
import com.tsfyun.scm.entity.customer.ExpressStandardQuoteMember;
import com.tsfyun.scm.mapper.customer.CustomerExpressQuoteMemberMapper;
import com.tsfyun.scm.service.customer.ICustomerExpressQuoteMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.customer.IExpressStandardQuoteMemberService;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.customer.CustomerExpressQuoteMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.naming.ldap.PagedResultsControl;
import java.util.List;

/**
 * <p>
 * 快递模式标准报价计算代理费明细 服务实现类
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Service
public class CustomerExpressQuoteMemberServiceImpl extends ServiceImpl<CustomerExpressQuoteMember> implements ICustomerExpressQuoteMemberService {

    @Autowired
    private CustomerExpressQuoteMemberMapper customerExpressQuoteMemberMapper;

    @Autowired
    private IExpressStandardQuoteMemberService expressStandardQuoteMemberService;

    @Resource
    private Snowflake snowflake;

    @Override
    public List<CustomerExpressQuoteMember> findByQuoteId(Long customerQuoteId) {
        return customerExpressQuoteMemberMapper.selectByExample(Example.builder(CustomerExpressQuoteMember.class).where(TsfWeekendSqls.<CustomerExpressQuoteMember>custom().andEqualTo(false,CustomerExpressQuoteMember::getQuoteId,customerQuoteId)).build());
    }

    @Override
    public List<CustomerExpressQuoteMemberVO> findVoByQuoteId(Long customerQuoteId) {
        List<CustomerExpressQuoteMember> memberList = findByQuoteId(customerQuoteId);
        return beanMapper.mapAsList(memberList,CustomerExpressQuoteMemberVO.class);
    }

    @Override
    public void saveCustomerExpressQuoteMembers(Long customerQuoteId,Long quoteId) {
        List<ExpressStandardQuoteMember> expressStandardQuoteMembers = expressStandardQuoteMemberService.findByQuoteId(quoteId);
        List<CustomerExpressQuoteMember> customerExpressQuoteMembers = Lists.newArrayList();
        if(CollUtil.isNotEmpty(expressStandardQuoteMembers)) {
            expressStandardQuoteMembers.stream().forEach(k->{
                CustomerExpressQuoteMember customerExpressQuoteMember = beanMapper.map(k,CustomerExpressQuoteMember.class);
                customerExpressQuoteMember.setId(snowflake.nextId());
                customerExpressQuoteMember.setQuoteId(customerQuoteId);
                customerExpressQuoteMembers.add(customerExpressQuoteMember);
            });
        }
        super.savaBatch(customerExpressQuoteMembers);
    }

    @Override
    public void deleteAllCustomerExpressQuoteMembers(Long customerQuoteId) {
        customerExpressQuoteMemberMapper.deleteByExample(Example.builder(CustomerExpressQuoteMember.class).where(TsfWeekendSqls.<CustomerExpressQuoteMember>custom().andEqualTo(false,CustomerExpressQuoteMember::getQuoteId,customerQuoteId)).build());
    }
}
