package com.tsfyun.scm.service.impl.customer;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.common.base.enums.RateTypeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.customer.ImpClauseDTO;
import com.tsfyun.scm.entity.customer.ImpClause;
import com.tsfyun.scm.mapper.customer.ImpClauseMapper;
import com.tsfyun.scm.service.customer.IImpClauseService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.customer.ImpClauseOrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 进口条款 服务实现类
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Service
public class ImpClauseServiceImpl extends ServiceImpl<ImpClause> implements IImpClauseService {

    @Autowired
    private ImpClauseMapper impClauseMapper;

    @Override
    public ImpClause findByAgreementId(Long agreementId) {
        ImpClause condition = new ImpClause();
        condition.setAgreementId(agreementId);
        return super.getOne(condition);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long add(Long agreementId,ImpClauseDTO dto) {
        ImpClause impClause = beanMapper.map(dto,ImpClause.class);
        if(impClause.getIsPaidIn()){
            impClause.setTaxRate(RateTypeEnum.IMP_CUSTOMS.getCode());
            impClause.setTaxRateTime("");
        }
        RateTypeEnum taxRateTypeEnum = RateTypeEnum.of(impClause.getTaxRate());
        //报关日牌价卖出价
        if(RateTypeEnum.IMP_BANK_CHINA_SELL==taxRateTypeEnum && "".equals(impClause.getTaxRateTime())){
            throw new ServiceException("税款汇率时间不能为空");
        }
        //报关日海关汇率
        if(RateTypeEnum.IMP_CUSTOMS==taxRateTypeEnum){
            impClause.setTaxRateTime("");
        }
        impClause.setAgreementId(agreementId);
        impClause.setId(agreementId);
        super.saveNonNull(impClause);
        return impClause.getId();
    }

    @Override
    public void edit(Long agreementId, ImpClauseDTO dto) {
        ImpClause impClause = super.getById(agreementId);
        TsfPreconditions.checkArgument(Objects.nonNull(impClause),new ServiceException("协议约定不存在，请刷新页面重试"));
        impClause.setAgreementId(agreementId);
        impClause.setVoluntarilyTax(dto.getVoluntarilyTax());
        impClause.setGoodsQuota(dto.getGoodsQuota());
        impClause.setTaxQuota(dto.getTaxQuota());
        impClause.setQuotaExplain(dto.getQuotaExplain());
        impClause.setTransactionMode(dto.getTransactionMode());
        impClause.setGoodsRate(dto.getGoodsRate());
        impClause.setGoodsRateTime(dto.getGoodsRateTime());
        impClause.setIsPaidIn(dto.getIsPaidIn());
        impClause.setTaxRate(dto.getTaxRate());
        impClause.setTaxRateTime(dto.getTaxRateTime());
        impClause.setOverdueDays(dto.getOverdueDays());

        if(impClause.getIsPaidIn()){
            impClause.setTaxRate(RateTypeEnum.IMP_CUSTOMS.getCode());
            impClause.setTaxRateTime("");
        }
        RateTypeEnum taxRateTypeEnum = RateTypeEnum.of(impClause.getTaxRate());
        //报关日牌价卖出价
        if(RateTypeEnum.IMP_BANK_CHINA_SELL==taxRateTypeEnum && "".equals(impClause.getTaxRateTime())){
            throw new ServiceException("税款汇率时间不能为空");
        }
        //报关日海关汇率
        if(RateTypeEnum.IMP_CUSTOMS==taxRateTypeEnum){
            impClause.setTaxRateTime("");
        }
        super.updateById(impClause);
    }


    @Override
    public List<ImpClauseOrderVO> checkOrderIdsGoodsRateTime(List<Long> orderIds, Boolean checkError) {
        List<ImpClauseOrderVO> clauseOrderVOS = impClauseMapper.findByImpOrderIds(orderIds);
        //合并付汇验证汇率时间是否一致
        if(checkError && orderIds.size() > 1) {
            if(CollUtil.isNotEmpty(clauseOrderVOS)) {
                String firstGoodsRateTime = clauseOrderVOS.get(0).getGoodsRateTime();
                for(ImpClauseOrderVO impClauseOrderVO : clauseOrderVOS) {
                    if(!Objects.equals(firstGoodsRateTime,impClauseOrderVO.getGoodsRateTime())) {
                        throw new ServiceException(String.format("订单【%s】对应的汇率时间不一致，协议编号【%s】,无法合并付汇",impClauseOrderVO.getOrderNo(),impClauseOrderVO.getAgreementDocNo()));
                    }
                }
            }
        }
        return clauseOrderVOS;
    }
}
