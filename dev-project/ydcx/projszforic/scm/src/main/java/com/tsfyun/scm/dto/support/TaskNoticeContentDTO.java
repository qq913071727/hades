package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.domain.DomainTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.io.Serializable;
import java.util.Map;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 任务通知内容请求实体
 * </p>
 *

 * @since 2020-04-05
 */
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="TaskNoticeContent请求对象", description="任务通知内容请求实体")
public class TaskNoticeContentDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 64,message = "单据类型最大长度不能超过64位")
   @EnumCheck(clazz = DomainTypeEnum.class,message = "单据类型错误")
   @ApiModelProperty(value = "单据类型")
   private String documentType;

   /**
    * 暂未考虑到多个单据合并的情况
    */
   @NotEmptyTrim(message = "单据id不能为空")
   @LengthTrim(max = 32,message = "单据id最大长度不能超过32位")
   @ApiModelProperty(value = "单据id")
   private String documentId;

   @NotEmptyTrim(message = "操作码不能为空")
   @LengthTrim(max = 50,message = "操作码最大长度不能超过50位")
   @ApiModelProperty(value = "操作码")
   private String operationCode;

   @Deprecated
   @LengthTrim(max = 255,message = "请求参数最大长度不能超过255位")
   @ApiModelProperty(value = "请求参数")
   private String actionParams;

   @LengthTrim(max = 255,message = "任务内容最大长度不能超过255位")
   @ApiModelProperty(value = "任务内容")
   private String content;

   private Boolean isExeRemove;//执行后立即删除
   /**
    * 客户id
    */
   private Long customerId;

   /**
    * 客户名称
    */
   private String customerName;

   /**
    * 提交任务员工id，主要用于某些系统自动触发的场景，如果不传入系统将自动取当前登录人的信息
    */
   private Long senderId;

   /**
    * 提交任务员工名称
    */
   private String senderName;

   /**
    * 查询参数map
    */
   private Map<String,Object> queryParamsMap;

   /**
    * 业务参数map
    */
   private Map<String,Object> actionParamsMap;


}
