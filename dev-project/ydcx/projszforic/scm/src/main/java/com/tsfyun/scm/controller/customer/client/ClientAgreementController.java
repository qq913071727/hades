package com.tsfyun.scm.controller.customer.client;

import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.customer.client.ClientServiceAgreementSignDTO;
import com.tsfyun.scm.service.customer.IAgreementService;
import com.tsfyun.scm.vo.customer.ImpAgreementVO;
import com.tsfyun.scm.vo.customer.ServiceAgreementQuotePlusVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client/agreement")
public class ClientAgreementController extends BaseController {

    @Autowired
    private IAgreementService agreementService;

    /**=
     * 进口下单根据客户获取有效协议报价
     * @return
     */
    @GetMapping(value = "impAgreement")
    public Result<List<ImpAgreementVO>> impAgreement(){
        return success(agreementService.clientImpAgreementCheck());
    }

    /**
     * 检查客户是否存在需要签约的协议
     * @return
     */
    @GetMapping(value = "existSignContract")
    public Result<Boolean> existSignContract(){
        return success(agreementService.existSignContract());
    }

    /**
     * 客户端加载协议报价
     * @return
     */
    @GetMapping("load")
    public Result<ServiceAgreementQuotePlusVo> load(){
        return success(agreementService.clientLoad());
    }

    /**
     * 客户同意签署
     * @param dto
     * @return
     */
    @PostMapping("agreeSign")
    @DuplicateSubmit
    public Result<Void> agreeSign(@RequestBody @Validated ClientServiceAgreementSignDTO dto){
        agreementService.agreeSign(dto);
        return success();
    }
}
