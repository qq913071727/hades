package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ImpSalesContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 销售合同响应实体
 * </p>
 *

 * @since 2020-05-19
 */
@Data
@ApiModel(value="ImpSalesContract响应对象", description="销售合同响应实体")
public class ImpSalesContractVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    private String statusId;

    @ApiModelProperty(value = "发票编号")
    private String invoiceNo;

    @ApiModelProperty(value = "合同日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime contractDate;

    @ApiModelProperty(value = "订单编号")
    private String impOrderNo;

    @ApiModelProperty(value = "购货单位（客户）")
    private Long buyerId;

    @ApiModelProperty(value = "购货单位名称")
    private String buyerName;

    @ApiModelProperty(value = "买方银行名称")
    private String buyerBankName;

    @ApiModelProperty(value = "买方纳税人识别号")
    private String buyerTaxpayerNo;

    @ApiModelProperty(value = "买方银行账号")
    private String buyerBankAccount;

    @ApiModelProperty(value = "买方银行地址")
    private String buyerBankAddress;

    @ApiModelProperty(value = "买方联系电话")
    private String buyerLinkPerson;

    @ApiModelProperty(value = "买方银行电话")
    private String buyerBankTel;

    @ApiModelProperty(value = "卖方")
    private Long sellerId;

    @ApiModelProperty(value = "卖方名称")
    private String sellerName;

    @ApiModelProperty(value = "卖方银行名称")
    private String sellerBankName;

    @ApiModelProperty(value = "卖方银行账号")
    private String sellerBankAccount;

    @ApiModelProperty(value = "卖方银行地址")
    private String sellerBankAddress;

    @ApiModelProperty(value = "卖方印章")
    private String sellerChapter;

    @ApiModelProperty(value = "合同金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "货款金额")
    private BigDecimal goodsVal;

    @ApiModelProperty(value = "票差调整金额")
    private BigDecimal differenceVal;

    private String memo;

    /**
     * 商务人员
     */
    private String busPersonName;

    private String statusDesc;

    public String getStatusDesc( ) {
        return Optional.ofNullable(ImpSalesContractStatusEnum.of(statusId)).map(ImpSalesContractStatusEnum::getName).orElse("");
    }


}
