package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.finance.ImpSalesContractQTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.vo.finance.ImpSalesContractDetailPlusVO;
import com.tsfyun.scm.vo.finance.ImpSalesContractPlusVO;
import com.tsfyun.scm.vo.finance.ImpSalesContractVO;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 销售合同 服务类
 * </p>
 *

 * @since 2020-05-19
 */
public interface IImpSalesContractService extends IService<ImpSalesContract> {

    /**
     * 分页列表（带权限）
     * @param qto
     * @return
     */
    PageInfo<ImpSalesContractVO> pageList(ImpSalesContractQTO qto);

    /**
     * 明细
     * @param id
     * @param operation
     * @return
     */
    ImpSalesContractDetailPlusVO detail(Long id,String operation);

    /**
     * 删除销售合同
     * @param id
     */
    void delete(Long id);

    /**
     * 修改销售合同上的发票号（id和合同号二传一）
     * @param id
     * @param salesContractNo
     * @param invoiceNo
     */
    void setInvoiceNo(Long id,String salesContractNo,String invoiceNo);

    /**
     * 销售合同打印获取数据
     * @param id
     * @return
     */
    ImpSalesContractPlusVO getContractData(Long id);

    /**
     * 销售合同导出PDF
     * @param id
     * @return
     */
    Long contractPdf(Long id);

    /**
     * 根据订单生成销售合同
     * @param impOrder
     * @param orderCosts
     * @param customer
     * @param adjustAmount
     */
    void generateByImpOrder(ImpOrder impOrder, List<ImpOrderCostVO> orderCosts, Customer customer, BigDecimal adjustAmount);

    /**
     * 根据订单号获取销售合同
     * @param orderNo
     * @return
     */
    ImpSalesContract getByOrderNo(String orderNo);

    /**
     * 根据订单ID获取销售合同
     * @param orderId
     * @return
     */
    ImpSalesContract findByOrderId(Long orderId);

    /**
     * 批量下载合同
     * @param ids
     */
    List<File> batchDownloadZip(List<Long> ids);

}
