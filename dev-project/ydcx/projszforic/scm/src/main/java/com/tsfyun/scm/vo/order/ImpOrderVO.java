package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.annotation.LocalDateTimeFormat;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.QuoteTypeEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import com.tsfyun.common.base.help.excel.LocalDateTimeConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 进口订单响应实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrder响应对象", description="进口订单响应实体")
public class ImpOrderVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "订单编号")
    private String docNo;

    @ApiModelProperty(value = "客户单号")
    private String clientNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    /**
     * 客户名称
     */
    private String customerName;

    @ApiModelProperty(value = "客户供应商")
    private Long supplierId;

    /**
     * 客户供应商名称
     */
    private String supplierName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    /**
     * 状态描述
     */
    private String statusDesc;

    @ApiModelProperty(value = "订单日期-由后端写入")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @LocalDateTimeFormat(value = "yyyy-MM-dd")
    @ExcelProperty(value = "订单日期")
    private LocalDateTime orderDate;

    @ApiModelProperty(value = "实际通关日期-由报关单反写")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime clearanceDate;

    @ApiModelProperty(value = "生成报关单确定进口")
    private Boolean isImp;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "业务类型-枚举")
    private String businessType;

    /**=
     * 报关类型
     */
    private String declareType;

    private String declareTypeName;
    public String getDeclareTypeName(){
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(getDeclareType());
        return Objects.nonNull(declareTypeEnum)?declareTypeEnum.getName():"";
    }


    /**
     * 业务类型描述
     */
    private String businessTypeDesc;

    @ApiModelProperty(value = "报价")
    private Long impQuoteId;

    /**
     * 报价类型
     */
    private String quoteType;

    /**
     * 报价类型描述
     */
    private String quoteTypeDesc;

    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "报关金额")
    private BigDecimal decTotalPrice;

    @ApiModelProperty(value = "进口汇率")
    private BigDecimal impRate;

    @ApiModelProperty(value = "转美金汇率")
    private BigDecimal usdRate;

    @ApiModelProperty(value = "总箱数")
    private Integer totalCartonNum;

    @ApiModelProperty(value = "总净重")
    private BigDecimal totalNetWeight;

    @ApiModelProperty(value = "总毛重")
    private BigDecimal totalCrossWeight;

    @ApiModelProperty(value = "已付汇金额")
    private BigDecimal payVal;

    @ApiModelProperty(value = "成交方式")
    private String transactionMode;

    /**
     * 成交方式描述值
     */
    private String transactionModeDesc;

    @ApiModelProperty(value = "销售人员id")
    private Long salePersonId;

    /**
     * 销售人员名称
     */
    private String salePersonName;

    /**
     * 商务人员id
     */
    private Long busPersonId;

    /**
     * 商务人员名称
     */
    private String busPersonName;

    @ApiModelProperty(value = "销售合同号")
    private String salesContractNo;

    @ApiModelProperty(value = "发票申请号")
    private String invoiceNo;

    @ApiModelProperty(value = "付汇申请号-多个逗号隔开")
    private String paymentNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    public String getBusinessTypeDesc() {
        BusinessTypeEnum businessTypeEnum = BusinessTypeEnum.of(businessType);
        return Objects.nonNull(businessTypeEnum) ? businessTypeEnum.getName() : "";
    }

    public String getStatusDesc() {
        ImpOrderStatusEnum impOrderStatusEnum = ImpOrderStatusEnum.of(statusId);
        return Objects.nonNull(impOrderStatusEnum) ? impOrderStatusEnum.getName() : "";
    }

    public String getTransactionModeDesc() {
        TransactionModeEnum transactionModeEnum = TransactionModeEnum.of(transactionMode);
        return Objects.nonNull(transactionModeEnum) ? transactionModeEnum.getName() : "";
    }

    public String getQuoteTypeDesc() {
        QuoteTypeEnum quoteTypeEnum = QuoteTypeEnum.of(quoteType);
        return Objects.nonNull(quoteTypeEnum) ? quoteTypeEnum.getName() : "";
    }
}
