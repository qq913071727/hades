package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.customer.SupplierTakeInfoDTO;
import com.tsfyun.scm.dto.customer.SupplierTakeInfoQTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierTakeInfoQTO;
import com.tsfyun.scm.entity.customer.SupplierTakeInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.SupplierTakeInfoListVO;
import com.tsfyun.scm.vo.customer.SupplierTakeInfoVO;
import com.tsfyun.scm.vo.customer.client.ClientSupplierTakeInfoVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-12
 */
public interface ISupplierTakeInfoService extends IService<SupplierTakeInfo> {

    /**
     * 批量保存提货信息
     * @param supplierId
     * @param takes
     */
    void addBatch(Long supplierId, List<SupplierTakeInfoDTO> takes);

    /**
     * 根据供应商获取所有的提货信息
     * @param supplierId
     * @return
     */
    List<SupplierTakeInfo> list(Long supplierId);

    /**
     * 根据供应商id删除提货信息
     * @param supplierId
     */
    void removeBySupplierId(Long supplierId);

    /**
     * 根据id获取提货信息
     * @param id
     * @return
     */
    SupplierTakeInfoVO detail(Long id);

    /**
     * 根据供应商id获取供应商下拉
     * @param supplierId
     * @return
     */
    List<SupplierTakeInfoVO> select(Long supplierId);

    /**
     * 设置/取消默认
     * @param id
     */
    void setDefault(Long id);

    /**
     * 取消以前设置的默认收货地址
     * @param supplierId
     */
    void handleOldDefault(Long supplierId);

    /**
     * 简单新增供应商提货信息
     * @param dto
     * @return
     */
    Long add(SupplierTakeInfoDTO dto);
    Long clientAdd(SupplierTakeInfoDTO dto);

    /**
     * 简单修改供应商提货信息
     * @param dto
     */
    void edit(SupplierTakeInfoDTO dto);
    void clientEdit(SupplierTakeInfoDTO dto);

    /**=
     * 修改禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 删除供应商提货信息
     * @param id
     */
    void remove(Long id);

    /**
     * 根据供应商id获取供应商提货记录数
     * @param supplierId
     * @return
     */
    int countBySupplierId(Long supplierId);

    /**
     * 带权限根据客户名称或者供应商名称分页查询
     * @param qto
     * @return
     */
    PageInfo<SupplierTakeInfoListVO> pageList(SupplierTakeInfoQTO qto);


    List<SupplierTakeInfoListVO> effectiveList(SupplierTakeInfoQTO qto);

    /**
     * 客户端提货信息分页列表
     * @param qto
     * @return
     */
    PageInfo<ClientSupplierTakeInfoVO> clientPage(ClientSupplierTakeInfoQTO qto);

    /**
     * 客户端删除提货信息
     * @param id
     */
    void clientRemove(Long id);

}
