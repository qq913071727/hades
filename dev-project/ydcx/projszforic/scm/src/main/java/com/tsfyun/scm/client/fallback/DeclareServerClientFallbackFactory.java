package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.WriteOffOrderCostDTO;
import com.tsfyun.common.base.vo.declare.RecordEnterprisesVO;
import com.tsfyun.scm.client.DeclareServerClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class DeclareServerClientFallbackFactory implements FallbackFactory<DeclareServerClient> {

    @Override
    public DeclareServerClient create(Throwable throwable) {

        return new DeclareServerClient(){
            @Override
            public Result<Void> cancelwriteOffByCosts(List<WriteOffOrderCostDTO> costs) {
                log.error("取消费用核销异常：",throwable);
                return null;
            }

            @Override
            public Result<RecordEnterprisesVO> getEnterprise(Long customerId, String customsCode) {
                log.error("获取备案企业信息异常：",throwable);
                return null;
            }
        };
    };
}
