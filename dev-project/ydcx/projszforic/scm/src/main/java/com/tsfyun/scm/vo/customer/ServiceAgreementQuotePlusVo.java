package com.tsfyun.scm.vo.customer;

import com.tsfyun.scm.vo.customer.client.ClientImpQuotePlusVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 协议+报价
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceAgreementQuotePlusVo implements Serializable {

    /**
     * 框架协议
     */
    ServiceAgreementVO agreement;

    /**
     * 报价单明细
     */
    List<ClientImpQuotePlusVO> quoteList;
}
