package com.tsfyun.scm.vo.logistics;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.DeliveryWaybillStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 送货单响应实体
 * </p>
 *

 * @since 2020-05-25
 */
@Data
@ApiModel(value="DeliveryWaybill响应对象", description="送货单响应实体")
public class DeliveryWaybillVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "送货单号")
    private String docNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    private Long customerId;

    private String customerName;

    private Long impOrderId;

    private String impOrderNo;

    private String clientNo;//客户单号

    private String receivingMode;

    private String receivingModeName;

    @ApiModelProperty(value = "运单日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime transDate;

    @ApiModelProperty(value = "发车时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime departureDate;

    @ApiModelProperty(value = "签收时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime reachDate;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    private String statusName;

    @ApiModelProperty(value = "单据类型-枚举")
    private String billType;

    @ApiModelProperty(value = "仓库")
    private Long warehouseId;

    @ApiModelProperty(value = "发货公司")
    private String shippingCompany;

    @ApiModelProperty(value = "发货地址")
    private String shippingAddress;

    @ApiModelProperty(value = "发货联系人")
    private String shippingLinkMan;

    @ApiModelProperty(value = "发货联系人电话")
    private String shippingTel;

    @ApiModelProperty(value = "快递单号")
    private String experssNo;

    @ApiModelProperty(value = "封条号")
    private String sealNo;

    private String logisticsCompanyName;
    private String deliveryCompanyName;
    private String deliveryAddress;

    /**
     * 运输供应商
     */

    private Long transportSupplierId;

    private String transportSupplierName;

    /**
     * 运输工具
     */

    private Long conveyanceId;

    /**
     * 车牌1
     */

    private String conveyanceNo;

    /**
     * 车牌2
     */

    private String conveyanceNo2;

    /**
     * 司机信息
     */

    private String driverInfo;



    public String getStatusName(){
        DeliveryWaybillStatusEnum statusEnum = DeliveryWaybillStatusEnum.of(getStatusId());
        return Objects.nonNull(statusEnum)?statusEnum.getName():"";
    }
}
