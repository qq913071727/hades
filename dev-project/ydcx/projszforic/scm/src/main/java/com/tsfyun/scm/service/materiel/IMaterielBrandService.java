package com.tsfyun.scm.service.materiel;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.materiel.MaterielBrandDTO;
import com.tsfyun.scm.dto.materiel.MaterielBrandQTO;
import com.tsfyun.scm.entity.materiel.MaterielBrand;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.materiel.MaterielBrandVO;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * <p>
 * 物料品牌 服务类
 * </p>
 *

 * @since 2020-03-26
 */
public interface IMaterielBrandService extends IService<MaterielBrand> {

    //新增
    void add(MaterielBrandDTO dto);

    //修改
    void edit(MaterielBrandDTO dto);

    //分页列表
    PageInfo<MaterielBrand> pageList(MaterielBrandQTO dto);

    //删除
    void delete(String id);

    /**
     * 详情
     * @param id
     * @return
     */
    MaterielBrandVO detail(String id);

    /**
     * 根据品名精确查询品牌信息
     * @param name
     * @return
     */
    MaterielBrandVO findByName(@Nullable String name);

    /**
     * 批量根据品名精确查询品牌信息
     * @param names
     * @return
     */
    List<MaterielBrandVO> findByNames(@Nullable String... names);

    /**
     * 根据名称模糊搜索
     * @param name
     * @return
     */
    List<MaterielBrandVO> select(@Nullable String name);

    /**
     * 新增或修改物料品牌
     * @param mb
     */
    void saveMaterielBrand(MaterielBrand mb);

    /**
     * 根据英文名称查询中文名称
     * @param name
     * @return （不存在则返回空字符串）
     */
    String findZhByNameEn(String name);

}
