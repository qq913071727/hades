package com.tsfyun.scm.mapper.user;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.user.Login;
import com.tsfyun.scm.vo.user.CustomerLoginInfoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface LoginMapper extends Mapper<Login> {

    /**=
     * 根据登录名称获取登录账号
     * @param loginName
     * @return
     */
    Login selectByLoginName(String loginName);

    /**=
     * 根据员工和手机号查询登录账户
     * @param personId
     * @param phoneNo
     * @return
     */
    Login findByPersonIdAndPhoneNo(@Param(value = "personId") Long personId, @Param(value = "phoneNo") String phoneNo);

    /**=
     * 根据员工和账号类型查询账户
     * @param personId
     * @param type
     * @return
     */
    Login findByPersonIdAndType(@Param(value = "personId") Long personId, @Param(value = "type") Integer type);

    /**=
     * 根据员工ID获取所有登录方式
     * @param personId
     * @return
     */
    List<Login> findByPersonId(@Param(value = "personId") Long personId);

    /**
     * 获取所有客户登录信息
     * @return
     */
    List<CustomerLoginInfoVO> allCustomerLoginInfo();
}