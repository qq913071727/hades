package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 销售合同明细
 * </p>
 *

 * @since 2020-05-19
 */
@Data
public class ImpSalesContractMember  {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 行号
     */

    private Integer rowNo;

    /**
     * 销售合同id
     */

    private Long impSalesContractId;

    /**
     * 物料型号
     */

    private String goodsModel;

    /**
     * 物料名称
     */

    private String goodsName;

    /**
     * 物料品牌
     */

    private String goodsBrand;

    /**
     * 产地编码
     */

    private String countryId;

    /**
     * 产地名称
     */

    private String countryName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 单位编码
     */

    private String unitCode;

    /**
     * 单位名称
     */

    private String unitName;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 币制编码
     */

    private String currencyId;

    /**
     * 币制名称
     */

    private String currencyName;


}
