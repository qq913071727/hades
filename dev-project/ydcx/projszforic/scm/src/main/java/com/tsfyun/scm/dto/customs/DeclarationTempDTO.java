package com.tsfyun.scm.dto.customs;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 报关单模板请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeclarationTemp请求对象", description="报关单模板请求实体")
public class DeclarationTempDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "模板id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "模板名称不能为空")
   @LengthTrim(max = 100,message = "模板名称最大长度不能超过100位")
   @ApiModelProperty(value = "模板名称")
   private String tempName;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 10,message = "单据类型最大长度不能超过10位")
   @EnumCheck(clazz = BillTypeEnum.class,message = "单据类型错误")
   @ApiModelProperty(value = "单据类型-进出口")
   private String billType;

   //@NotNull(message = "禁用标示不能为空")
   @ApiModelProperty(value = "禁用标示")
   private Boolean disabled;

   //@NotNull(message = "是否默认不能为空")
   @ApiModelProperty(value = "是否默认")
   private Boolean isDefault;

   @LengthTrim(max = 10,message = "运输方式最大长度不能超过10位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafModeCode;

   @LengthTrim(max = 20,message = "运输方式最大长度不能超过20位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafModeName;

   @LengthTrim(max = 10,message = "监管方式最大长度不能超过10位")
   @ApiModelProperty(value = "监管方式")
   private String supvModeCdde;

   @LengthTrim(max = 20,message = "监管方式最大长度不能超过20位")
   @ApiModelProperty(value = "监管方式")
   private String supvModeCddeName;

   @LengthTrim(max = 10,message = "征免性质最大长度不能超过10位")
   @ApiModelProperty(value = "征免性质")
   private String cutModeCode;

   @LengthTrim(max = 20,message = "征免性质最大长度不能超过20位")
   @ApiModelProperty(value = "征免性质")
   private String cutModeName;

   @LengthTrim(max = 10,message = "启运国(地区)最大长度不能超过10位")
   @ApiModelProperty(value = "启运国(地区)")
   private String cusTradeCountryCode;

   @LengthTrim(max = 20,message = "启运国(地区)最大长度不能超过20位")
   @ApiModelProperty(value = "启运国(地区)")
   private String cusTradeCountryName;

   @LengthTrim(max = 10,message = "经停港最大长度不能超过10位")
   @ApiModelProperty(value = "经停港")
   private String distinatePortCode;

   @LengthTrim(max = 20,message = "经停港最大长度不能超过20位")
   @ApiModelProperty(value = "经停港")
   private String distinatePortName;

   @LengthTrim(max = 10,message = "申报地海关最大长度不能超过10位")
   @ApiModelProperty(value = "申报地海关")
   private String customMasterCode;

   @LengthTrim(max = 20,message = "申报地海关最大长度不能超过20位")
   @ApiModelProperty(value = "申报地海关")
   private String customMasterName;

   @LengthTrim(max = 10,message = "包装种类最大长度不能超过10位")
   @ApiModelProperty(value = "包装种类")
   private String wrapTypeCode;

   @LengthTrim(max = 20,message = "包装种类最大长度不能超过20位")
   @ApiModelProperty(value = "包装种类")
   private String wrapTypeName;

   @LengthTrim(max = 10,message = "贸易国别(地区)最大长度不能超过10位")
   @ApiModelProperty(value = "贸易国别(地区)")
   private String cusTradeNationCode;

   @LengthTrim(max = 20,message = "贸易国别(地区)最大长度不能超过20位")
   @ApiModelProperty(value = "贸易国别(地区)")
   private String cusTradeNationName;

   @LengthTrim(max = 10,message = "入境口岸最大长度不能超过10位")
   @ApiModelProperty(value = "入境口岸")
   private String ciqEntyPortCode;

   @LengthTrim(max = 20,message = "入境口岸最大长度不能超过20位")
   @ApiModelProperty(value = "入境口岸")
   private String ciqEntyPortName;

   @LengthTrim(max = 200,message = "货物存放地最大长度不能超过200位")
   @ApiModelProperty(value = "货物存放地")
   private String goodsPlace;

   @LengthTrim(max = 10,message = "启运港最大长度不能超过10位")
   @ApiModelProperty(value = "启运港")
   private String destPortCode;

   @LengthTrim(max = 20,message = "启运港最大长度不能超过20位")
   @ApiModelProperty(value = "启运港")
   private String destPortName;

   @LengthTrim(max = 5,message = "征免方式	最大长度不能超过5位")
   @ApiModelProperty(value = "征免方式")
   private String dutyMode;

   @LengthTrim(max = 10,message = "征免方式	最大长度不能超过10位")
   @ApiModelProperty(value = "征免方式")
   private String dutyModeName;

   @LengthTrim(max = 10,message = "境内目的地最大长度不能超过10位")
   @ApiModelProperty(value = "境内目的地")
   private String districtCode;

   @LengthTrim(max = 30,message = "境内目的地最大长度不能超过30位")
   @ApiModelProperty(value = "境内目的地")
   private String districtName;

   @LengthTrim(max = 10,message = "目的地代码最大长度不能超过10位")
   @ApiModelProperty(value = "目的地代码")
   private String ciqDestCode;

   @LengthTrim(max = 30,message = "目的地代码最大长度不能超过30位")
   @ApiModelProperty(value = "目的地代码")
   private String ciqDestName;

   @LengthTrim(max = 10,message = "进境关别最大长度不能超过10位")
   @ApiModelProperty(value = "进境关别")
   private String iePortCode;

   @LengthTrim(max = 20,message = "进境关别最大长度不能超过20位")
   @ApiModelProperty(value = "进境关别")
   private String iePortName;

   @LengthTrim(max = 400,message = "标记唛码最大长度不能超过400位")
   @ApiModelProperty(value = "标记唛码")
   private String markNo;

   @ApiModelProperty(value = "最终目的国")
   private String destinationCountry;

   @ApiModelProperty(value = "最终目的国")
   private String destinationCountryName;
}
