package com.tsfyun.scm.dto.finance;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class ExpPaymentCompletedDTO implements Serializable {

    @NotNull(message = "付款单id不能为空")
    private Long id;
    @NotNull(message = "实际付款时间不能为空")
    private Date actualPaymentDate;
}
