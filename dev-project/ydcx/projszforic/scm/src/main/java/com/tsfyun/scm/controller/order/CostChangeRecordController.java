package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.CostChangeRecordDTO;
import com.tsfyun.scm.dto.order.CostChangeRecordQTO;
import com.tsfyun.scm.service.order.ICostChangeRecordService;
import com.tsfyun.scm.service.order.IImpOrderService;
import com.tsfyun.scm.vo.order.CostChangeRecordVO;
import com.tsfyun.scm.vo.order.ImpOrderAdjustCostVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 费用变更记录 前端控制器
 * </p>
 *
 * @since 2020-05-27
 */
@RestController
@RequestMapping("/costChangeRecord")
public class CostChangeRecordController extends BaseController {

    @Autowired
    private ICostChangeRecordService costChangeRecordService;

    @Autowired
    private IImpOrderService impOrderService;

    /**
     * 分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<CostChangeRecordVO>> pageList(@ModelAttribute CostChangeRecordQTO qto) {
        PageInfo<CostChangeRecordVO> page = costChangeRecordService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 调整费用
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "adjust")
    Result<Void> adjust(@ModelAttribute @Validated CostChangeRecordDTO dto) {
        costChangeRecordService.adjust(dto);
        return success();
    }

    /**
     * 可以调整费用的订单下拉，订单模糊搜索（带权限）
     * @param impOrderNo
     * @return
     */
    @RequestMapping(value = "selectAdjustOrder")
    Result<List<ImpOrderAdjustCostVO>> selectAdjustOrder(@RequestParam(value = "impOrderNo",required = false)String impOrderNo) {
        return success(impOrderService.selectAdjustOrder(impOrderNo));
    }

}

