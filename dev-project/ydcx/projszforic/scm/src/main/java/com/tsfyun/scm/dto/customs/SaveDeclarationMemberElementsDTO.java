package com.tsfyun.scm.dto.customs;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 保存报关单明细申报要素信息
 * @since Created in 2020/5/6 10:33
 */
@Data
public class SaveDeclarationMemberElementsDTO implements Serializable {

    /**
     * 报关单明细id
     */
    @NotNull(message = "报关单明细id不能为空")
    private Long id;

    /**
     * 法一数量
     */
    @NotNull(message = "法一数量不能为空")
    @Digits(integer = 8, fraction = 4, message = "法一数量整数位不能超过8位，小数位不能超过4")
    @DecimalMin(value = "0.0001",message = "法一数量不能小于0.0001")
    private BigDecimal quantity1;

    /**
     * 法二数量
     */
    @Digits(integer = 8, fraction = 4, message = "法二数量2整数位不能超过8位，小数位不能超过4")
    private BigDecimal quantity2;

    @ApiModelProperty(value = "最终目的国")
    @NotEmptyTrim(message = "最终目的国不能为空")
    private String destinationCountry;
    @ApiModelProperty(value = "最终目的国")
    @NotEmptyTrim(message = "最终目的国不能为空")
    private String destinationCountryName;
    @ApiModelProperty(value = "征免方式")
    @NotEmptyTrim(message = "征免方式不能为空")
    private String dutyMode;
    @ApiModelProperty(value = "征免方式")
    @NotEmptyTrim(message = "征免方式不能为空")
    private String dutyModeName;
    @ApiModelProperty(value = "境内目的地")
    @NotEmptyTrim(message = "境内目的地不能为空")
    private String districtCode;
    @ApiModelProperty(value = "境内目的地")
    @NotEmptyTrim(message = "境内目的地不能为空")
    private String districtName;

    @NotEmptyTrim(message = "目的地代码不能为空")
    private String ciqDestCode;
    @ApiModelProperty(value = "目的地代码")
    private String ciqDestName;

    /**
     * 申报要素值
     */
    @NotNull(message = "请填写要素项")
    @Size(min = 1,message = "要素项目错误")
    List<String> elementsVal;



}
