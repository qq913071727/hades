package com.tsfyun.scm.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import tk.mybatis.mapper.annotation.KeySql;
import javax.persistence.Id;
import lombok.Data;


/**
 * <p>
 * 退税订单
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Data
public class DrawbackOrder implements Serializable {

     private static final long serialVersionUID=1L;


    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 采购合同
     */

    private Long purchaseId;

    /**
     * 订单
     */

    private Long orderId;

    /**
     * 代垫金额
     */

    private BigDecimal deductionValue;

    /**
     * 代理费金额
     */

    private BigDecimal agencyFee;

    /**
     * 退税金额
     */

    private BigDecimal drawbackValue;

    /**
     * 退税主单
     */

    private Long drawbackId;


    private Boolean isSettleAccount;// 是否结汇完成
    private BigDecimal settlementValue;// 结汇人民币金额
    private BigDecimal estimatedPayment;// 预估货款
    private BigDecimal actualDrawbackValue;// 应退税金额
    private BigDecimal alreadyDrawbackValue;// 已退税金额


}
