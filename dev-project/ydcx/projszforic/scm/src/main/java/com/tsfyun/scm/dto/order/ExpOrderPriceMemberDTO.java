package com.tsfyun.scm.dto.order;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Data
@ApiModel(value="ExpOrderPriceMember请求对象", description="请求实体")
public class ExpOrderPriceMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "波动率不能为空")
   @Digits(integer = 8, fraction = 2, message = "波动率整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "波动率")
   private BigDecimal fluctuations;

   @NotNull(message = "是否有报关记录不能为空")
   @ApiModelProperty(value = "是否有报关记录")
   private Boolean isImp;

   @NotNull(message = "是否新料不能为空")
   @ApiModelProperty(value = "是否新料")
   private Boolean isNewGoods;

   private Long orderMemberId;

   private Long orderPriceId;

   @LengthTrim(max = 255,message = "计算过程最大长度不能超过255位")
   @ApiModelProperty(value = "计算过程")
   private String processLogs;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   private String spec;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitCode;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "单价(委托)不能为空")
   @Digits(integer = 6, fraction = 4, message = "单价(委托)整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "单价(委托)")
   private BigDecimal unitPrice;

   @NotNull(message = "总价(委托)不能为空")
   @Digits(integer = 8, fraction = 2, message = "总价(委托)整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "总价(委托)")
   private BigDecimal totalPrice;

   @NotNull(message = "净重不能为空")
   @Digits(integer = 6, fraction = 4, message = "净重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "净重")
   private BigDecimal netWeight;

   @NotNull(message = "毛重不能为空")
   @Digits(integer = 6, fraction = 4, message = "毛重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "毛重")
   private BigDecimal grossWeight;

   @NotEmptyTrim(message = "币制不能为空")
   @LengthTrim(max = 10,message = "币制最大长度不能超过10位")
   @ApiModelProperty(value = "币制")
   private String currencyCode;

   @NotEmptyTrim(message = "币制不能为空")
   @LengthTrim(max = 10,message = "币制最大长度不能超过10位")
   @ApiModelProperty(value = "币制")
   private String currencyName;

   private Integer rowNo;


}
