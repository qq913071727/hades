package com.tsfyun.scm.vo.materiel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.scm.system.vo.CustomsCodeVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 品名要素响应实体
 * </p>
 *

 * @since 2020-03-26
 */
@Data
@ApiModel(value="NameElements响应对象", description="品名要素响应实体")
public class NameElementsVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    @ApiModelProperty(value = "是否禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "申报要素")
    private String elements;

    @ApiModelProperty(value = "物料名称")
    private String name;

    @ApiModelProperty(value = "要素名称")
    private String nameMemo;

    private List<String> elementsVal;

    private String createPerson;

    private String updatePerson;

    private LocalDateTime lastUpdated;

    //################以下数据为海关编码数据#################//
    private String hsCodeName;//海关编码名称
    private String conrate;//消费税率
    private String csc;//监管条件
    private BigDecimal exportRate;//出口税率
    private BigDecimal gtr;//进口普通国税率
    private String iaqr;//检验检疫条件
    private BigDecimal mfntr;//进口最惠国税率
    private String sunitCode;//法二单位编码
    private BigDecimal ter;//临时出口税率
    private BigDecimal tit;//进口暂定税率
    private String trr;//出口退税率
    private String unitCode;//法一单位编码
    private BigDecimal vatr;//增值税率
    private String sunitName;//法二单位名称
    private String unitName;//法一单位名称

    /**
     * 构建海关编码数据
     * @param customsCodeVO
     * @return
     */
    public NameElementsVO buildCustomsCode (CustomsCodeVO customsCodeVO) {
        Optional.ofNullable(customsCodeVO).ifPresent(customsCode->{
                    this.setHsCodeName(customsCode.getName());
                    this.setConrate(customsCode.getConrate());
                    this.setCsc(customsCode.getCsc());
                    this.setExportRate(customsCode.getExportRate());
                    this.setGtr(customsCode.getGtr());
                    this.setIaqr(customsCode.getIaqr());
                    this.setMfntr(customsCode.getMfntr());
                    this.setSunitCode(customsCode.getSunitCode());
                    this.setTer(customsCode.getTer());
                    this.setTit(customsCode.getTit());
                    this.setTrr(customsCode.getTrr());
                    this.setUnitCode(customsCode.getUnitCode());
                    this.setVatr(customsCode.getVatr());
                    this.setSunitName(customsCode.getSunitName());
                    this.setUnitName(customsCode.getUnitName());
                });
        return this;
    }


}
