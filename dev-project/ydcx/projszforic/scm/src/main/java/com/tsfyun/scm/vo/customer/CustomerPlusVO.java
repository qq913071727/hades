package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 客户大页面返回实体
 */
@Data
public class CustomerPlusVO implements Serializable {

    private CustomerVO customer;

    private List<CustomerDeliveryInfoVO> deliverys;

    private List<CustomerAbroadDeliveryInfoVO> abroadDeliverys;

    private List<CustomerBankVO> customerBanks;

}
