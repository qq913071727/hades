package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 采购合同明细响应实体
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Data
@ApiModel(value="PurchaseContractMember响应对象", description="采购合同明细响应实体")
public class PurchaseContractMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    private Integer rowNo;

    @ApiModelProperty(value = "采购合同主单")
    private Long purchaseContractId;

    @ApiModelProperty(value = "订单明细")
    private Long orderMemberId;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    @ApiModelProperty(value = "代理费")
    private BigDecimal agentFee;

    @ApiModelProperty(value = "代垫费")
    private BigDecimal matFee;

    @ApiModelProperty(value = "票差调整金额")
    private BigDecimal differenceVal;

    @ApiModelProperty(value = "增值税率")
    private BigDecimal addedTaxRate;

    @ApiModelProperty(value = "退税率")
    private BigDecimal taxRebateRate;


}
