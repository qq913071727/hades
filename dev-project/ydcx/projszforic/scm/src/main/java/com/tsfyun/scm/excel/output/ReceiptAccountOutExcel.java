package com.tsfyun.scm.excel.output;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.annotation.LocalDateTimeFormat;
import com.tsfyun.common.base.enums.domain.ReceiptAccountStatusEnum;
import com.tsfyun.common.base.enums.finance.ReceivingModeEnum;
import com.tsfyun.common.base.help.excel.LocalDateTimeConverter;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 收款单响应实体
 * </p>
 *

 * @since 2020-04-15
 */
@Data
@ApiModel(value="ReceiptAccount导出响应对象", description="收款单导出响应实体")
public class ReceiptAccountOutExcel implements Serializable {

    private static final long serialVersionUID=1L;

    @ExcelProperty(value = "状态")
    @ApiModelProperty(value = "状态")
    private String statusDesc;

    @ExcelProperty(value = "收款单号")
    @ApiModelProperty(value = "收款单号")
    private String docNo;

    @LocalDateTimeFormat(value = "yyyy-MM-dd HH:mm")
    @ExcelProperty(value = "收款时间",converter = LocalDateTimeConverter.class)
    @ApiModelProperty(value = "收款时间")
    private LocalDateTime accountDate;

    @ExcelProperty(value = "结算单位")
    @ApiModelProperty(value = "结算单位")
    private String customerName;

    @ExcelProperty(value = "币制")
    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ExcelProperty(value = "收款金额")
    @ApiModelProperty(value = "收款金额")
    private BigDecimal accountValue;

    @ExcelProperty(value = "核销金额")
    @ApiModelProperty(value = "核销金额")
    private BigDecimal writeValue;

    @ExcelProperty(value = "收款方式")
    @ApiModelProperty(value = "收款方式")
    private String receivingModeDesc;

    @ExcelProperty(value = "收款行")
    @ApiModelProperty(value = "收款行")
    private String receivingBank;

    @ExcelProperty(value = "制单人")
    @ApiModelProperty(value = "制单人")
    private String createBy;

    @ExcelIgnore
    @ApiModelProperty(value = "状态")
    private String statusId;

    @ExcelIgnore
    @ApiModelProperty(value = "收款方式")
    private String receivingMode;

    public String getReceivingModeDesc() {
        ReceivingModeEnum receivingModeEnum = ReceivingModeEnum.of(receivingMode);
        return Objects.nonNull(receivingModeEnum) ? receivingModeEnum.getName() : null;
    }

    public String getStatusDesc() {
        ReceiptAccountStatusEnum receiptAccountStatusEnum = ReceiptAccountStatusEnum.of(statusId);
        return Objects.nonNull(receiptAccountStatusEnum) ? receiptAccountStatusEnum.getName() : null;
    }


}
