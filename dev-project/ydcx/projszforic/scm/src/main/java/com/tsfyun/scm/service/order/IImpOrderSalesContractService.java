package com.tsfyun.scm.service.order;

import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/25 14:52
 */
public interface IImpOrderSalesContractService {

    /**=
     * 根据订单创建销售合同
     * @param orderId
     * @param adjustmentAmount
     */
    void generateImpSalesContract(Long orderId, BigDecimal adjustmentAmount);

}
