package com.tsfyun.scm.dto.logistics;

import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 香港送货单请求实体
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Data
@ApiModel(value="OverseasDeliveryNote请求对象", description="香港送货单请求实体")
public class OverseasDeliveryNoteDTO implements Serializable {

   private static final long serialVersionUID=1L;

    @NotNull(message = "id不能为空",groups = {UpdateGroup.class})
    private Long id;

   @NotEmptyTrim(message = "派送单号不能为空")
   @LengthTrim(max = 20,message = "派送单号最大长度不能超过20位")
   @ApiModelProperty(value = "派送单号")
   private String docNo;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @LengthTrim(max = 100,message = "发货单位最大长度不能超过100位")
   @ApiModelProperty(value = "发货单位")
   private String consignor;

   @LengthTrim(max = 255,message = "发货地址最大长度不能超过255位")
   @ApiModelProperty(value = "发货地址")
   private String consignorAddress;

   @LengthTrim(max = 64,message = "发货联系人最大长度不能超过64位")
   @ApiModelProperty(value = "发货联系人")
   private String consignorLinkMan;

   @LengthTrim(max = 64,message = "发货联系电话最大长度不能超过64位")
   @ApiModelProperty(value = "发货联系电话")
   private String consignorLinkTel;

   @NotNull(message = "请选择收货信息")
   private Long overseasTakeInfoId;

   @LengthTrim(max = 100,message = "收货单位最大长度不能超过100位")
   @ApiModelProperty(value = "收货单位")
   private String consignee;

   @LengthTrim(max = 255,message = "收货联系地址最大长度不能超过255位")
   @ApiModelProperty(value = "收货联系地址")
   private String consigneeAddress;

   @LengthTrim(max = 64,message = "收货联系人最大长度不能超过64位")
   @ApiModelProperty(value = "收货联系人")
   private String consigneeLinkMan;

   @LengthTrim(max = 64,message = "收货联系电话最大长度不能超过64位")
   @ApiModelProperty(value = "收货联系电话")
   private String consigneeLinkTel;

   @NotNull(message = "请选择派送日期")
   @ApiModelProperty(value = "派送日期")
   private Date deliveryDate;

   @ApiModelProperty(value = "入仓号")
   private String receivingNo;

   @ApiModelProperty(value = "收货仓库")
   private String receivingWarehouse;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String remark;


}
