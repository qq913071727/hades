package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 * 价格波动历史 服务类
 * </p>
 *
 *
 * @since 2020-04-17
 */
public interface IPriceFluctuationHistoryService extends IService<PriceFluctuationHistory> {

    List<PriceFluctuationHistory> list(Long memberId);
}
