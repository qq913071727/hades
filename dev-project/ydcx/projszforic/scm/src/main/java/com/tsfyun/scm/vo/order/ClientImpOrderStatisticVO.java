package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/2 14:15
 */
@Data
public class ClientImpOrderStatisticVO implements Serializable {

    /**
     * 待审价订单数
     */
    private Long waitPriceNum;

    /**
     * 待验货订单数
     */
    private Long waitInspection;

    /**
     * 待付款订单数
     */
    private Long waitPayNum;

    /**
     * 待申报订单数
     */
    private Long waitDeclareNum;

    /**
     * 待配送
     */
    private Long waitDeliveryNum;

}
