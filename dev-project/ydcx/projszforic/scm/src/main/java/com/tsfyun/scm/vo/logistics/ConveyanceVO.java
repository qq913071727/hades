package com.tsfyun.scm.vo.logistics;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="Conveyance响应对象", description="响应实体")
public class ConveyanceVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "车牌1")
    private String conveyanceNo;

    @ApiModelProperty(value = "车牌2")
    private String conveyanceNo2;

    @ApiModelProperty(value = "海关注册编号")
    private String customsRegNo;

    @ApiModelProperty(value = "禁用启用")
    private Boolean disabled;

    @ApiModelProperty(value = "司机")
    private String driver;

    @ApiModelProperty(value = "司机身份证号")
    private String driverNo;

    @ApiModelProperty(value = "司机电话")
    private String driverTel;

    @ApiModelProperty(value = "牌头")
    private String head;

    @ApiModelProperty(value = "企业电话")
    private String headFax;

    @ApiModelProperty(value = "企业代码")
    private String headNo;

    @ApiModelProperty(value = "企业电话")
    private String headTel;

    private Long transportSupplierId;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "默认")
    private Boolean isDefault;


}
