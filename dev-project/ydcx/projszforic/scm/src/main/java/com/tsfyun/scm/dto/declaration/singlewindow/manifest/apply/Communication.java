package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 通讯方式信息
 * @since Created in 2020/4/22 12:02
 */

/**
 * @Description: 通讯方式信息
 * @since Created in 2020/4/22 11:30
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Communication", propOrder = {
        "id",
        "typeId",
})
public class Communication {

    //通讯号码
    @XmlElement(name = "ID")
    private String id;

    //通讯方式类别代码
    @XmlElement(name = "TypeID")
    private String typeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
