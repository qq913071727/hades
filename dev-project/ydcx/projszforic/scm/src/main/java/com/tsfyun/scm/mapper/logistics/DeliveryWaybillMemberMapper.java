package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.entity.logistics.DeliveryWaybillMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 送货单明细 Mapper 接口
 * </p>
 *

 * @since 2020-05-25
 */
@Repository
public interface DeliveryWaybillMemberMapper extends Mapper<DeliveryWaybillMember> {

    List<DeliveryWaybillMemberVO> findByDeliveryId(@Param(value="deliveryId") Long deliveryId);
    //根据送货单主单ID删除明细
    Integer removeByDeliveryId(@Param(value="deliveryId") Long deliveryId);
}
