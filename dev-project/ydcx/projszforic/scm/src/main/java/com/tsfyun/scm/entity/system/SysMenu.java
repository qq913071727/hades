package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统菜单
 */
@Data
public class SysMenu implements Serializable {

    //统一主键生成策略
    @Id
    private String id;


    /**
     * 父菜单id，一级菜单为null
     */
    private String parentId;


    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 菜单图标（配置时可以从菜单图标库中挑选）
     */
    private String icon;

    /**
     * 菜单类型1-目录；2-菜单（跳转页面）；3-按钮
     */
    private Integer type;

    /**
     * 跳转url
     */
    private String url;


    /**
     * 按钮菜单函数
     * 前端可以采用js反射执行函数this[methodName]();
     */
    private String action;

    /**=
     * 操作编码
     */
    private String operationCode;

    /**=
     * 是否任务
     */
    private Boolean isTask;

    /**
     * 选择的记录数
     */
    private Integer checkNum;
}
