package com.tsfyun.scm.dto.view;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/18 12:36
 */
@Data
public class ViewExpDifferenceMemberQTO extends PaginationDto {

    /**
     * 订单编号
     */

    private String docNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 采购合同号
     */

    private String purchaseContractNo;

    /**=
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**=
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd){
        if(dateEnd!=null){
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }
}
