package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.enums.BizParamEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-04-15
 */
@Data
@ApiModel(value="SysParam请求对象", description="请求实体")
public class SysParamDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   /*
   @NotEmptyTrim(message = "原参数名不能为空",groups = UpdateGroup.class)
   @EnumCheck(clazz = BizParamEnum.class,message = "参数名错误")
   private String preId;
    */

   @NotEmptyTrim(message = "参数名不能为空")
   @LengthTrim(max = 64,message = "参数名最大长度不能超过64位")
   @EnumCheck(clazz = BizParamEnum.class,message = "参数名错误")
   @ApiModelProperty(value = "参数名")
   private String id;

   @NotEmptyTrim(message = "参数值不能为空")
   @LengthTrim(max = 64,message = "参数值最大长度不能超过64位")
   @ApiModelProperty(value = "参数值")
   private String val;

   @LengthTrim(max = 100,message = "备注最大长度不能超过100位")
   @ApiModelProperty(value = "备注")
   private String remark;

   @ApiModelProperty(value = "是否禁用-1-是;2-否")
   private Boolean disabled;


}
