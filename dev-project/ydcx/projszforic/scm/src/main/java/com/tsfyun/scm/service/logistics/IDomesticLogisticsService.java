package com.tsfyun.scm.service.logistics;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.logistics.DomesticLogisticsDTO;
import com.tsfyun.scm.entity.logistics.DomesticLogistics;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.DomesticLogisticsVO;
import com.tsfyun.scm.vo.logistics.SimpleDomesticLogisticsVO;

import java.util.List;

/**
 * <p>
 * 国内物流 服务类
 * </p>
 *

 * @since 2020-03-25
 */
public interface IDomesticLogisticsService extends IService<DomesticLogistics> {

    //新增
    void add(DomesticLogisticsDTO dto);

    //修改
    void edit(DomesticLogisticsDTO dto);

    //分页列表
    PageInfo<DomesticLogisticsVO> pageList(DomesticLogisticsDTO dto);

    //删除
    void delete(Long id);

    //获取所有的启用的国内物流
    List<SimpleDomesticLogisticsVO> select( );

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 详情
     * @param id
     * @return
     */
    DomesticLogisticsVO detail(Long id);


}
