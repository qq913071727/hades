package com.tsfyun.scm.mapper.file;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.file.ExportFile;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-04
 */
public interface ExportFileMapper extends Mapper<ExportFile> {

}
