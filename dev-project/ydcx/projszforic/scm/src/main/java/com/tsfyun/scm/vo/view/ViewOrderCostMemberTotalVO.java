package com.tsfyun.scm.vo.view;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ViewOrderCostMemberTotalVO implements Serializable {

    private Integer totalSingle = 0;//总单数
    private BigDecimal totalPrice;//总订单金额
    private BigDecimal totalReceAmount = BigDecimal.ZERO;//总应收
    private BigDecimal totalAcceAmount = BigDecimal.ZERO;//总已收
    private BigDecimal totalArrears = BigDecimal.ZERO;//总欠款
    private BigDecimal totalOverdue = BigDecimal.ZERO;//总逾期

    public BigDecimal getTotalArrears(){
        return totalReceAmount.subtract(totalAcceAmount);
    }
}
