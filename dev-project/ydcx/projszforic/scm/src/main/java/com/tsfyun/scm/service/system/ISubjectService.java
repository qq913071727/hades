package com.tsfyun.scm.service.system;

import com.tsfyun.scm.dto.system.SubjectDTO;
import com.tsfyun.scm.entity.system.Subject;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.SubjectSimpleVO;
import com.tsfyun.scm.vo.system.SubjectVO;
import com.tsfyun.scm.vo.system.SubjectVersionVO;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-03
 */
public interface ISubjectService extends IService<Subject> {

    SubjectVO findByCode();

    void update(SubjectDTO dto);

    SubjectSimpleVO getSimpleById(Long id);

    SubjectVersionVO version();

}
