package com.tsfyun.scm.dto.base;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-18
 */
@Data
@ApiModel(value="Currency请求对象", description="请求实体")
public class CurrencyDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "数据id不能为空",groups = UpdateGroup.class)
   private String id;

   @NotNull(message = "禁用状态不能为空")
   @ApiModelProperty(value = "禁用状态")
   private Boolean disabled;

   @NotEmptyTrim(message = "中国银行币制索引不能为空")
   @LengthTrim(max = 10,message = "中国银行币制索引最大长度不能超过10位")
   @ApiModelProperty(value = "中国银行币制索引")
   private String markCode;

   @NotEmptyTrim(message = "币制名称不能为空")
   @LengthTrim(max = 20,message = "币制名称最大长度不能超过20位")
   @ApiModelProperty(value = "币制名称")
   private String name;


}
