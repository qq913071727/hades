package com.tsfyun.scm.config.freemarker;

import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * freemarker配置类
 */
@Slf4j
@Configuration
public class FreeMarkerConfig {

    @Autowired
    protected FreeMarkerConfigurer configurer;

    @PostConstruct
    public void setSharedVariable() {
        configurer.setTemplateLoaderPath("classpath:/templates/");
        freemarker.template.Configuration config = null;
        try {
            config = configurer.createConfiguration();
        } catch (IOException | TemplateException e) {
            log.error("模板初始化异常");
        }
        config.setDefaultEncoding("UTF-8");
        config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        config.setLogTemplateExceptions(false);
        config.setNumberFormat("#");
        config.setDateFormat("yyyy-MM-dd");
        config.setDateTimeFormat("yyyy-MM-dd HH:mm:ss");
        configurer.setConfiguration(config);
    }
}
