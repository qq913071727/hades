package com.tsfyun.scm.controller.customs;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.customs.IDeclarationPdfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/declarationPdf")
public class DeclarationPdfController {

    @Autowired
    private IDeclarationPdfService declarationPdfService;

    @GetMapping(value = "contract")
    public Result<Long> contract(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.contract(id,autoChapter));
    }

    @GetMapping(value = "packing")
    public Result<Long> packing(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.packing(id,autoChapter));
    }

    @GetMapping(value = "invoice")
    public Result<Long> invoice(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.invoice(id,autoChapter));
    }

    @GetMapping(value = "delivery")
    public Result<Long> delivery(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.delivery(id,autoChapter));
    }

    @GetMapping(value = "overseasContract")
    public Result<Long> overseasContract(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.overseasContract(id,autoChapter));
    }

    @GetMapping(value = "overseasPacking")
    public Result<Long> overseasPacking(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.overseasPacking(id,autoChapter));
    }

    @GetMapping(value = "overseasInvoice")
    public Result<Long> overseasInvoice(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.overseasInvoice(id,autoChapter));
    }

    @GetMapping(value = "overseasDelivery")
    public Result<Long> overseasDelivery(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.overseasDelivery(id,autoChapter));
    }

    @GetMapping(value = "merge")
    public Result<Long> merge(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.merge(id,autoChapter));
    }

    @GetMapping(value = "mergeOverseas")
    public Result<Long> mergeOverseas(@RequestParam(value = "id")Long id,@RequestParam(value = "autoChapter",defaultValue = "true",required = false)Boolean autoChapter){
        return Result.success(declarationPdfService.mergeOverseas(id,autoChapter));
    }

}
