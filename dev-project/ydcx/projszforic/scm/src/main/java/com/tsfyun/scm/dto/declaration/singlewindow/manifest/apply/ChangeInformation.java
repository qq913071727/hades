package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import com.tsfyun.common.base.util.jaxb.CDataAdapter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @Description:
 * @since Created in 2020/4/23 12:07
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeInformation", propOrder = {
        "reason",
        "contact",
        "phone",
})
public class ChangeInformation {

    //变更原因描述
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "Reason")
    private String reason;

    //变更申请联系人姓名
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "Contact")
    private String contact;

    //变更申请联系人电话
    @XmlElement(name = "Phone")
    private String phone;

}
