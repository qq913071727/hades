package com.tsfyun.scm.mapper.base;

import com.tsfyun.scm.dto.base.CurrencyQTO;
import com.tsfyun.scm.entity.base.Currency;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.base.CurrencyVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  币制Mapper 接口
 * </p>
 *

 * @since 2020-03-18
 */
@Repository
public interface CurrencyMapper extends Mapper<Currency> {

    List<CurrencyVO> findAll();

    List<Currency> list(CurrencyQTO qto);
}
