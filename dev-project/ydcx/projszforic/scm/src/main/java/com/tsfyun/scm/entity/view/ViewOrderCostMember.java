package com.tsfyun.scm.entity.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 订单费用明细
 * </p>
 *
 *
 * @since 2021-05-28
 */
@Data
public class ViewOrderCostMember implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    private Long id;

    /**
     * 账期日期
     */

    private Date periodDate;

    /**
     * 货款应收
     */
    private BigDecimal huokuan;

    /**
     * 手续费
     */
    private BigDecimal shouxufei;

    /**
     * 代理费
     */
    private BigDecimal dailifei;

    /**
     * 增值税
     */
    private BigDecimal zenzhishui;

    /**
     * 关税
     */
    private BigDecimal guanshui;

    /**
     * 消费税
     */
    private BigDecimal xiaofeishui;

    /**
     * 免3C认证费
     */
    private BigDecimal mian3c;

    /**
     * 国内送货费
     */
    private BigDecimal guoleisonhuofei;

    /**
     * 商检服务费
     */
    private BigDecimal shangjianfei;

    /**
     * 总应收
     */
    private BigDecimal totalCost;

    /**
     * 总已收
     */
    private BigDecimal writeValue;

    /**
     * 订单编号
     */

    private String docNo;

    /**
     * 订单日期
     */
    private Date orderDate;

    /**
     * 委托金额
     */
    private BigDecimal totalPrice;

    /**
     * 报关金额
     */
    private BigDecimal decTotalPrice;

    /**
     * 币制
     */
    private String currencyName;

    /**
     * 报价类型 
     */
    private String quoteType;

    /**
     * 客户
     */
    private Long customerId;

    /**
     * 销售人员
     */
    private String salePersonName;

    /**
     * 逾期天数
     */
    private Integer overdueDays;


}
