package com.tsfyun.scm.mapper.materiel;

import com.tsfyun.scm.entity.materiel.MaterielLog;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 归类变更日志 Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-26
 */
@Repository
public interface MaterielLogMapper extends Mapper<MaterielLog> {

    Integer batchSave(@Param(value = "materielLogList") List<MaterielLog> materielLogList);

    Integer updateMaterielId(@Param(value = "oldId")String oldId, @Param(value = "newId")String newId);

    void removeByMaterielId(@Param(value = "materielId")String materielId);
}
