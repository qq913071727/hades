package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.*;

/**
 * @Description:
 * @since Created in 2020/4/22 14:43
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "head",
        "declaration",
})
@XmlRootElement(name = "Manifest")
public class Manifest {

    @XmlElement(name = "Head", required = true)
    private Head head;

    @XmlElement(name = "Declaration", required = true)
    private Declaration declaration;


    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }

    public Declaration getDeclaration() {
        return declaration;
    }

    public void setDeclaration(Declaration declaration) {
        this.declaration = declaration;
    }
}
