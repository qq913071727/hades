package com.tsfyun.scm.vo.logistics;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.OverseasDeliveryNoteStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 香港送货单响应实体
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Data
@ApiModel(value="OverseasDeliveryNote响应对象", description="香港送货单响应实体")
public class OverseasDeliveryNoteVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "订单编号")
    private String expOrderNos;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "客户")
    private String customerName;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "发货单位")
    private String consignor;

    @ApiModelProperty(value = "发货地址")
    private String consignorAddress;

    @ApiModelProperty(value = "发货联系人")
    private String consignorLinkMan;

    @ApiModelProperty(value = "发货联系电话")
    private String consignorLinkTel;

    private Long consigneeId;

    @ApiModelProperty(value = "收货单位")
    private String consignee;

    @ApiModelProperty(value = "收货联系地址")
    private String consigneeAddress;

    @ApiModelProperty(value = "收货联系人")
    private String consigneeLinkMan;

    @ApiModelProperty(value = "收货联系电话")
    private String consigneeLinkTel;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "送货日期")
    private Date deliveryDate;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "收货仓库")
    private String receivingWarehouse;

    @ApiModelProperty(value = "入仓号")
    private String receivingNo;

    private String statusDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(OverseasDeliveryNoteStatusEnum.of(statusId)).map(OverseasDeliveryNoteStatusEnum::getName).orElse("");
    }


}
