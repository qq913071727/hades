package com.tsfyun.scm.dto.customs;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;



/**
 * <p>
 * 报关行请求实体
 * </p>
 *
 *
 * @since 2021-11-15
 */
@Data
public class DeclarationBrokerQTO extends PaginationDto {

   private static final long serialVersionUID=1L;

   private String name;


}
