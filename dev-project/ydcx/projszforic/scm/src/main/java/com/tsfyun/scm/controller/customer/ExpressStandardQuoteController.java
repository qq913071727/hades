package com.tsfyun.scm.controller.customer;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.ExpressStandardQTO;
import com.tsfyun.scm.dto.customer.ExpressStandardQuotePlusDTO;
import com.tsfyun.scm.entity.customer.ExpressStandardQuote;
import com.tsfyun.scm.service.customer.IExpressStandardQuoteService;
import com.tsfyun.scm.vo.customer.ExpressStandardQuoteMemberVO;
import com.tsfyun.scm.vo.customer.ExpressStandardQuotePlusVO;
import com.tsfyun.scm.vo.customer.ExpressStandardQuoteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 快递模式标准报价代理费 前端控制器
 * </p>
 *
 *
 * @since 2020-10-26
 */
@RestController
@RequestMapping("/expressStandardQuote")
public class ExpressStandardQuoteController extends BaseController {

    private final IExpressStandardQuoteService expressStandardQuoteService;

    public ExpressStandardQuoteController(IExpressStandardQuoteService expressStandardQuoteService) {
        this.expressStandardQuoteService = expressStandardQuoteService;
    }

    @Autowired
    private OrikaBeanMapper beanMapper;

    @PostMapping(value = "page")
    Result<List<ExpressStandardQuoteVO>> page(ExpressStandardQTO qto) {
        PageInfo<ExpressStandardQuote> page = expressStandardQuoteService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),ExpressStandardQuoteVO.class));
    }

    /**
     * 根据业务类型获取当前有效的
     * @param quoteType
     * @return
     */
    @GetMapping(value = "currentEffective")
    public Result<List<ExpressStandardQuoteMemberVO>> currentEffective(@RequestParam(value = "quoteType")String quoteType){
        return success(expressStandardQuoteService.getCurrentEffective(quoteType));
    }

    /**
     * 启用/禁用
     */
    @PreAuthorize("hasRole('manager')")
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")String id, @RequestParam("disabled")Boolean disabled){
        expressStandardQuoteService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 新增
     * @param dto
     * @return 返回供应商id
     */
    @DuplicateSubmit
    @PreAuthorize("hasRole('manager')")
    @PostMapping(value = "add")
    Result<Void> add(@RequestBody @Validated(AddGroup.class) ExpressStandardQuotePlusDTO dto) {
        expressStandardQuoteService.addPlus(dto);
        return success( );
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PreAuthorize("hasRole('manager')")
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated(UpdateGroup.class) ExpressStandardQuotePlusDTO dto) {
        expressStandardQuoteService.editPlus(dto);
        return success();
    }


    /**
     * 详情信息
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    Result<ExpressStandardQuotePlusVO> info(@RequestParam(value = "id")Long id) {
        return success(expressStandardQuoteService.detail(id));
    }

    @PreAuthorize("hasRole('manager')")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        expressStandardQuoteService.delete(id);
        return success();
    }

}

