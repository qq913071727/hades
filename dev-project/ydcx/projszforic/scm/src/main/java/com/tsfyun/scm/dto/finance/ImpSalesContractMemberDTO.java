package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 销售合同明细请求实体
 * </p>
 *
 * @since 2020-05-19
 */
@Data
@ApiModel(value="ImpSalesContractMember请求对象", description="销售合同明细请求实体")
public class ImpSalesContractMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "行号不能为空")
   @LengthTrim(max = 4,message = "行号最大长度不能超过4位")
   @ApiModelProperty(value = "行号")
   private Integer rowNo;

   @NotNull(message = "销售合同id不能为空")
   @ApiModelProperty(value = "销售合同id")
   private Long impSalesContractId;

   @LengthTrim(max = 100,message = "物料型号最大长度不能超过100位")
   @ApiModelProperty(value = "物料型号")
   private String goodsModel;

   @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
   @ApiModelProperty(value = "物料名称")
   private String goodsName;

   @LengthTrim(max = 100,message = "物料品牌最大长度不能超过100位")
   @ApiModelProperty(value = "物料品牌")
   private String goodsBrand;

   @NotEmptyTrim(message = "产地编码不能为空")
   @LengthTrim(max = 20,message = "产地编码最大长度不能超过20位")
   @ApiModelProperty(value = "产地编码")
   private String countryId;

   @NotEmptyTrim(message = "产地名称不能为空")
   @LengthTrim(max = 20,message = "产地名称最大长度不能超过20位")
   @ApiModelProperty(value = "产地名称")
   private String countryName;

   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @Digits(integer = 6, fraction = 4, message = "单价整数位不能超过6位，小数位不能超过4位")
   @ApiModelProperty(value = "单价")
   private BigDecimal unitPrice;

   @LengthTrim(max = 10,message = "单位编码最大长度不能超过10位")
   @ApiModelProperty(value = "单位编码")
   private String unitCode;

   @NotEmptyTrim(message = "单位名称不能为空")
   @LengthTrim(max = 50,message = "单位名称最大长度不能超过50位")
   @ApiModelProperty(value = "单位名称")
   private String unitName;

   @Digits(integer = 8, fraction = 2, message = "总价整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "总价")
   private BigDecimal totalPrice;

   @NotEmptyTrim(message = "币制编码不能为空")
   @LengthTrim(max = 20,message = "币制编码最大长度不能超过20位")
   @ApiModelProperty(value = "币制编码")
   private String currencyId;

   @NotEmptyTrim(message = "币制名称不能为空")
   @LengthTrim(max = 20,message = "币制名称最大长度不能超过20位")
   @ApiModelProperty(value = "币制名称")
   private String currencyName;


}
