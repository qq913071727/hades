package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class InvoiceMemberExcel implements Serializable {

    private String docNo;
    private String orderNo;
    private String invoiceName;
    private String buyerName;
    private String unitName;
    private String goodsModel;
    private BigDecimal quantity;
    private BigDecimal unitPrice;
    private BigDecimal totalPrice;
    private String memo;
    private String version;
    private String taxCodeName;
    private String taxCode;
    private String favouredPolicy;

    public String getVersion(){
        return "27.0";
    }
    public String getFavouredPolicy(){
        return "0";
    }
}
