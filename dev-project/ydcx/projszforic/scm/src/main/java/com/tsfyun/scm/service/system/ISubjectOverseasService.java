package com.tsfyun.scm.service.system;

import com.tsfyun.scm.dto.system.SubjectOverseasDTO;
import com.tsfyun.scm.dto.system.SubjectOverseasPlusDTO;
import com.tsfyun.scm.dto.system.SubjectOverseasQTO;
import com.tsfyun.scm.entity.system.SubjectOverseas;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.SubjectOverseasPlusVO;
import com.tsfyun.scm.vo.system.SubjectOverseasVO;

import java.util.List;

/**
 * <p>
 *  主体境外公司
 * </p>
 *
 *
 * @since 2020-03-19
 */
public interface ISubjectOverseasService extends IService<SubjectOverseas> {

    //根据条件查询所有境外公司(不分页)
    List<SubjectOverseasVO> findList(SubjectOverseasQTO qto);
    //添加主体境外公司
    Long add(SubjectOverseasDTO dto);
    //修改主体境外公司
    void edit(SubjectOverseasDTO dto);
    //添加主体境外公司（包括银行）
    Long addPlus(SubjectOverseasPlusDTO dto);
    //修改主体境外公司（包括银行）
    void editPlus(SubjectOverseasPlusDTO dto);
    //修改禁用启用
    void updateDisabled(Long id,Boolean disabled);
    //删除境外公司
    void remove(Long id);
    //详情包含银行信息
    SubjectOverseasPlusVO detailPlus(Long id);
    //默认境外付款公司
    SubjectOverseasVO defOverseas();
}
