package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;


import com.tsfyun.common.base.util.jaxb.CDataAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @Description: 发货人信息
 * @since Created in 2020/4/22 11:30
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Consignor", propOrder = {
        "id",
        "name",
})
public class Consignor {

    //发货人代码
    @XmlElement(name = "ID")
    protected String id;

    //发货人名称
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "Name")
    protected String name;


    public Consignor () {

    }

    public Consignor (String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
