package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.vo.CustomerSimpleVO;
import com.tsfyun.common.base.vo.SimpleBusinessVO;
import com.tsfyun.scm.dto.customer.*;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.*;
import com.tsfyun.scm.vo.customer.client.CheckCompanyResultVO;
import com.tsfyun.scm.vo.customer.client.ClientCustomerGradeVO;
import com.tsfyun.scm.vo.customer.client.ClientFinanceInfoVO;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-03
 */
public interface ICustomerService extends IService<Customer> {


    /**
     * 分页查询客户信息，仅返回简单信息
     * @param qto
     * @return
     */
    PageInfo<Customer> selectPageList(CustomerQTO qto);

    /**
     * 分页查询客户信息
     * @param qto
     * @return
     */
    PageInfo<CustomerVO>  pageList(CustomerQTO qto);

    /**
     * 获取客户详细信息
     * @param id
     * @return
     */
    CustomerVO detail(Long id);

    /**
     * 客户信息大页面新增
     * @param dto
     * @return
     */
    Long addPlus(CustomerPlusInfoDTO dto);

    /**
     * 客户信息大页面修改
     * @param dto
     */
    void editPlus(CustomerPlusInfoDTO dto);

    /**
     * 客户信息大页面详情
     * @param id
     * @return
     */
    CustomerPlusVO detailPlus(Long id);

    /**=
     * 修改禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 删除客户id
     * @param id
     */
    void remove(Long id);

    /**=
     * 根据客户名称查询客户
     * @param name
     * @return
     */
    Customer findByName(String name);

    /**
     * 根据客户名称查询客户带权限
     * @param name
     * @return
     */
    Customer findByNameWithRight(String name);

    /**
     * 批量导入客户
     * @param file
     */
    void importData(MultipartFile file);

    /**
     * 调整客户商务或者销售
     * @param dto
     */
    void changeCustomerSaleOrBus(ChangeCustomerPersonDTO dto);

    /**
     * 根据客户id获取客户人员信息
     * @param id
     * @return
     */
    CustomerPersonVO getCustomerPerson(Long id);

    /**
     * 修改客户下单日期
     * @param customerId
     * @param orderDate
     */
    void changeCustomerOrderDate(Long customerId, LocalDateTime orderDate);

    /**
     * 保存一个空客户对象（用户客户端注册）
     * @return
     */
    Customer saveEmptyInfo();

    /**
     * 根据ID获取公司基本信息（客户端调用）
     * @param id
     * @return
     */
    CompanyVO companyInfo(Long id);

    /**
     * 识别营业执照
     * @param id
     */
    BusinessLicenseVO businessLicenseOcr(Long id);

    /**
     * 客户端修改我的公司
     * @param dto
     * @return 0:单纯修改，1：审核通过，2：需要人工审核
     */
    Integer updateCompany(CompanyDTO dto);

    /**
     * 刷新客户端公司缓存数据
     * @param id
     */
    CompanyVO refreshCacheCompany(Long id);
    CompanyVO refreshCacheCompany(Customer customer);
    /**
     * 缓存获取公司信息
     * @param id
     * @return
     */
    CompanyVO getCacheCompany(Long id);

    /**
     * 保存公司开票信息
     * @return
     */
    void saveCustomerInvoiceInfo(CustomerInvoiceInfoDTO dto);

    /**
     * 核准客户
     * @param dto
     */
    void approvingCustomer(TaskDTO dto);

    /**
     * 获取客户财务信息
     * @return
     */
    ClientFinanceInfoVO getCompanyFinance();

    /**
     * 客户资料完善度计算
     * @return
     */
    ClientCustomerGradeVO companyPerfectShow();

    /**
     * 模糊查询公司简洁工商信息
     * @param name
     * @return
     */
    List<SimpleBusinessVO> vagueSimpleBusinessQuery(String name);

    /**
     * 根据客户ID集合获取客户简要信息(优先REDIS缓存读取)
     * @param customerIds
     * @return
     */
    Map<Long, CustomerSimpleVO> obtainCustomerSimples(Set<Long> customerIds);

    /**
     * 根据客户ID获取客户简要信息
     * @param customerId
     * @return
     */
    CustomerSimpleVO findByCustomerId(Long customerId);

    /**
     * 获取所有的客户（含禁用，状态为已审核）
     * @return
     */
    List<CustomerSimpleVO> allAuditCustomer();

    /**
     * 客户端维护客户资料检查客户名称
     * @param name
     * @return
     */
    CheckCompanyResultVO checkCompanyName(String name);

    /**
     * 客户认领公司
     * @param ncustomerId
     * @return
     */
    Long claimCompany(Long ncustomerId);
}
