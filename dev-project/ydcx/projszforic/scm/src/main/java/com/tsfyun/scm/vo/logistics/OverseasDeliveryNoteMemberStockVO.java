package com.tsfyun.scm.vo.logistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.util.TypeUtils;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/10 14:29
 */
@Data
public class OverseasDeliveryNoteMemberStockVO implements Serializable {

    private Long id;

    private String expOrderNo;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;

    private Long customerId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱号
     */

    private String cartonNo;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 已派送数量
     */
    private BigDecimal outQuantity;

    /**
     * 库存数量
     */
    private BigDecimal stockQuantity;

    /**
     * 已派净重
     */

    private BigDecimal outNetWeight;

    /**
     * 已派毛重
     */

    private BigDecimal outGrossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 已派送箱数
     */
    private Integer outCartonNum;

    /**
     * 库存箱数
     */
    private Integer stockCartonNum;

    /**
     * 计算库存数量
     * @return
     */
    public BigDecimal getStockQuantity(){
        return quantity.subtract(outQuantity).setScale(2,BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 计算库存箱数
     * @return
     */
    public Integer getStockCartonNum(){
        return TypeUtils.castToInt(cartonNum,0) - TypeUtils.castToInt(outCartonNum,0);
    }


}
