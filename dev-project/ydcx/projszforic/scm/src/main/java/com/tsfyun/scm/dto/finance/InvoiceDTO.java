package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 发票请求实体
 * </p>
 *
 * @since 2020-05-15
 */
@Data
@ApiModel(value="Invoice请求对象", description="发票请求实体")
public class InvoiceDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "系统单号不能为空")
   @LengthTrim(max = 20,message = "系统单号最大长度不能超过20位")
   @ApiModelProperty(value = "系统单号")
   private String docNo;

   @NotEmptyTrim(message = "状态不能为空")
   @LengthTrim(max = 20,message = "状态最大长度不能超过20位")
   @ApiModelProperty(value = "状态")
   private String statusId;

   @NotEmptyTrim(message = "税单类型不能为空")
   @LengthTrim(max = 20,message = "税单类型最大长度不能超过20位")
   @ApiModelProperty(value = "税单类型")
   private String taxType;

   @NotNull(message = "申请日期不能为空")
   @ApiModelProperty(value = "申请日期")
   private LocalDateTime applyDate;

   @ApiModelProperty(value = "确认日期")
   private LocalDateTime confirmDate;

   @NotNull(message = "购货单位不能为空")
   @ApiModelProperty(value = "购货单位-买房")
   private Long buyerId;

   @NotEmptyTrim(message = "购货单位名称不能为空")
   @LengthTrim(max = 200,message = "购货单位名称最大长度不能超过200位")
   @ApiModelProperty(value = "购货单位名称")
   private String buyerName;

   @LengthTrim(max = 50,message = "买方银行名称最大长度不能超过50位")
   @ApiModelProperty(value = "买方银行名称")
   private String buyerBankName;

   @LengthTrim(max = 18,message = "买方纳税人识别号最大长度不能超过18位")
   @ApiModelProperty(value = "买方纳税人识别号")
   private String buyerTaxpayerNo;

   @LengthTrim(max = 50,message = "买方银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "买方银行账号")
   private String buyerBankAccount;

   @LengthTrim(max = 255,message = "买方银行地址最大长度不能超过255位")
   @ApiModelProperty(value = "买方银行地址")
   private String buyerBankAddress;

   @LengthTrim(max = 50,message = "买方联系人最大长度不能超过50位")
   @ApiModelProperty(value = "买方联系人")
   private String buyerLinkPerson;

   @LengthTrim(max = 50,message = "买方联系电话最大长度不能超过50位")
   @ApiModelProperty(value = "买方联系电话")
   private String buyerBankTel;

   @NotNull(message = "卖方不能为空")
   @ApiModelProperty(value = "卖方")
   private Long sellerId;

   @LengthTrim(max = 64,message = "卖方银行名称最大长度不能超过64位")
   @ApiModelProperty(value = "卖方银行名称")
   private String sellerBankName;

   @LengthTrim(max = 50,message = "卖方银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "卖方银行账号")
   private String sellerBankAccount;

   @NotEmptyTrim(message = "发票类型不能为空")
   @LengthTrim(max = 20,message = "发票类型最大长度不能超过20位")
   @ApiModelProperty(value = "发票类型")
   private String invoiceType;

   @LengthTrim(max = 20,message = "实际发票编号最大长度不能超过20位")
   @ApiModelProperty(value = "实际发票编号")
   private String invoiceNo;

   @ApiModelProperty(value = "实际开票日期")
   private LocalDateTime invoiceDate;

   @NotNull(message = "开票金额不能为空")
   @Digits(integer = 17, fraction = 2, message = "开票金额整数位不能超过17位，小数位不能超过2位")
   @ApiModelProperty(value = "开票金额")
   private BigDecimal invoiceVal;

   @NotNull(message = "税点不能为空")
   @Digits(integer = 4, message = "税点整数位最大长度不能超过4位，小数位不能超过2位", fraction = 2)
   @ApiModelProperty(value = "税点")
   private BigDecimal taxPoint;

   @LengthTrim(max = 20,message = "订单号最大长度不能超过20位")
   @ApiModelProperty(value = "订单号")
   private String orderNo;

   @LengthTrim(max = 20,message = "内贸合同号最大长度不能超过20位")
   @ApiModelProperty(value = "内贸合同号")
   private String saleContractNo;

   @LengthTrim(max = 32,message = "快递单号最大长度不能超过32位")
   @ApiModelProperty(value = "快递单号")
   private String expressNo;

   @LengthTrim(max = 255,message = "开票要求最大长度不能超过255位")
   @ApiModelProperty(value = "开票要求")
   private String requirement;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
