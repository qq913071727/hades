package com.tsfyun.scm.controller.logistics;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.logistics.*;
import com.tsfyun.scm.service.logistics.ICrossBorderWaybillService;
import com.tsfyun.scm.service.order.IImpOrderLogisticsService;
import com.tsfyun.scm.vo.logistics.CrossBorderWaybillVO;
import com.tsfyun.scm.vo.order.BindOrderVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsSimpleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 跨境运输单进口 前端控制器
 * </p>
 *
 *
 * @since 2020-04-17
 */
@RestController
@RequestMapping("/crossBorderWaybill")
public class CrossBorderWaybillController extends BaseController {

    @Autowired
    private ICrossBorderWaybillService crossBorderWaybillService;

    @Autowired
    private IImpOrderLogisticsService impOrderLogisticsService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<CrossBorderWaybillVO>> list(CrossBorderWaybillQTO qto) {
        PageInfo<CrossBorderWaybillVO> page = crossBorderWaybillService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<CrossBorderWaybillVO> detail(@RequestParam(value = "id")Long id,@RequestParam(value = "operation",required = false)String operation) {
        return success(crossBorderWaybillService.detail(id,operation));
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        crossBorderWaybillService.delete(id);
        return success();
    }

    /**
     * 新增
     * @param dto
     * @return 返回id
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Long> add(@ModelAttribute @Validated(AddGroup.class) CrossBorderWaybillDTO dto) {
        return success(crossBorderWaybillService.add(dto));
    }

    /**
     * 修改
     * @param dto
     * @return 返回id
     */
    @DuplicateSubmit
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) CrossBorderWaybillDTO dto) {
        crossBorderWaybillService.edit(dto);
        return success();
    }

    /**
     * 确认跨境运输单
     * @param dto
     * @return 返回id
     */
    @DuplicateSubmit
    @PostMapping(value = "confirm")
    Result<Void> confirm(@ModelAttribute @Validated() TaskDTO dto) {
        crossBorderWaybillService.comfirm(dto);
        return success();
    }

    /**
     * 跨境运输单绑定订单
     * @param dto
     * @return
     */
    @PostMapping(value = "bindOrder")
    Result<Void> bind(@ModelAttribute @Validated() CrossBorderWaybillBindOrderDTO dto) {
        crossBorderWaybillService.bindOrder(dto);
        return success();
    }

    /**
     * 跨境运输单解绑订单
     * @param id
     * @param orderId
     * @return
     */
    @PostMapping(value = "unbindOrder")
    Result<Void> unbind(@RequestParam(value = "id")Long id,@RequestParam(value = "orderId")Long orderId) {
        crossBorderWaybillService.unbindOrder(id,orderId);
        return success();
    }

    /**
     * 跨境运输单可绑定订单列表
     * @param id
     * @return
     */
    @GetMapping(value = "canBind")
    Result<List<BindOrderVO>> canBind(@RequestParam(value = "id")Long id) {
        return success(crossBorderWaybillService.canBindList(id));
    }

    /**
     * 跨境运输单已绑定订单列表
     * @param id
     * @return
     */
    @GetMapping(value = "binded")
    Result<List<BindOrderVO>> binded(@RequestParam(value = "id")Long id) {
        return success(crossBorderWaybillService.bindedList(id));
    }

    /**
     * 跨境运输单绑单完成
     * @param dto
     */
    @DuplicateSubmit
    @PostMapping(value = "bindComplete")
    Result<Void> bind(@ModelAttribute @Validated() TaskDTO dto) {
        crossBorderWaybillService.bindComplete(dto);
        return success();
    }

    /**=
     * 退回修改
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "backUpdate")
    Result<Void> backUpdate(TaskDTO dto){
        crossBorderWaybillService.backUpdate(dto);
        return success();
    }

    /**
     * 跨境运输单复核
     * @param dto
     */
    @DuplicateSubmit
    @PostMapping(value = "review")
    Result<Void> review(@ModelAttribute @Validated() TaskDTO dto) {
        crossBorderWaybillService.review(dto);
        return success();
    }

    /**=
     * 流程跳转
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "processJump")
    Result<Void> processJump(@ModelAttribute @Validated() TaskDTO dto){
        crossBorderWaybillService.processJump(dto);
        return success();
    }

    /**
     * 跨境运输单发车
     * @param dto
     */
    @DuplicateSubmit
    @PostMapping(value = "depart")
    Result<Void> depart(@ModelAttribute @Validated( ) CrossBorderWaybillDepartDTO dto) {
        crossBorderWaybillService.depart(dto);
        return success();
    }

    /**
     * 跨境运输单签收
     * @param dto
     */
    @DuplicateSubmit
    @PostMapping(value = "sign")
    Result<Void> sign(@ModelAttribute @Validated() CrossBorderWaybillSignDTO dto) {
        crossBorderWaybillService.sign(dto);
        return success();
    }

    /**
     * 获取最近一个月的货至客户指定地点的收货地址信息
     * @return
     */
    @GetMapping(value = "getRecentDeliveryInfo")
    Result<List<ImpOrderLogisticsSimpleVO>> getRecentDeliveryInfo() {
        return success(impOrderLogisticsService.getCharterCarDeliveryInfo());
    }

}

