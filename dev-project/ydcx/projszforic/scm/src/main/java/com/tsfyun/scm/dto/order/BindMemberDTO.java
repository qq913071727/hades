package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class BindMemberDTO implements Serializable {

    @NotNull(message = "订单明细ID不能为空")
    @ApiModelProperty(value = "订单明细ID")
    private Long memberId;

    @NotNull(message = "请至少填写一个入库单号")
    @Valid
    private List<ReceivingNoDTO> receivingNos;

}
