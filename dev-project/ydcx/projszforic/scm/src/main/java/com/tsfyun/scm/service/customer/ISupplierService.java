package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.customer.SupplierDTO;
import com.tsfyun.scm.dto.customer.SupplierPlusInfoDTO;
import com.tsfyun.scm.dto.customer.SupplierQTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierDTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierQTO;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.scm.vo.customer.SimpleSupplierVO;
import com.tsfyun.scm.vo.customer.SupplierPlusVO;
import com.tsfyun.scm.vo.customer.SupplierVO;
import com.tsfyun.scm.vo.customer.client.ClientSupplierVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-12
 */
public interface ISupplierService extends IService<Supplier> {

    /**
     * 供应商信息大页面新增
     * @param dto
     * @return
     */
    Long addPlus(SupplierPlusInfoDTO dto);

    /**
     * 供应商信息大页面修改
     * @param dto
     */
    void editPlus(SupplierPlusInfoDTO dto);

    /**
     * 供应商信息大页面详情
     * @param id
     * @return
     */
    SupplierPlusVO detailPlus(Long id);
    SupplierVO detail(Long id);

    /**
     * 修改禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 根据客户获取供应商下拉
     * @param customerId
     * @return
     */
    List<SimpleSupplierVO> select(Long customerId);

    /**=
     * 根据客户查询供应商
     * @param customerName
     * @param name
     * @return
     */
    List<SimpleSupplierVO> vague(String customerName,String name);

    List<SimpleSupplierVO> clientVague(String name);

    /**
     * 简单新增供应商信息
     * @param dto
     * @return
     */
    Supplier simpleAdd(SupplierDTO  dto);
    /**=
     * 根据客户名称和供应商查询供应商
     * @param customerName
     * @param supplierName
     * @return
     */
    Supplier findByCustomerNameAndSupplierName(String customerName,String supplierName);

    /**
     * 根据客户ID和供应商名称查询
     * @param customerId
     * @param supplierName
     * @return
     */
    Supplier findByCustomerIdAndSupplierName(Long customerId,String supplierName);

    /**
     * 简单修改供应商信息
     * @param dto
     */
    void simpleEdit(SupplierDTO dto);

    /**
     * 带权限根据客户名称或者供应商名称分页查询
     * @param qto
     * @return
     */
    PageInfo<SupplierVO> pageList(SupplierQTO qto);

    /**
     * 删除供应商
     * @param id
     */
    void remove(Long id);

    /**
     * 根据客户id获取供应商记录数
     * @param customerId
     * @return
     */
    int countByCustomerId(Long customerId);

    /**
     * 客户端新增供应商
     * @param dto
     * @return
     */
    Long clientAdd(ClientSupplierDTO dto);

    /**
     * 客户端删除供应商
     * @param id
     */
    void clientRemove(Long id);

    /**
     * 客户端供应商列表
     * @param qto
     * @return
     */
    PageInfo<Supplier> pageList(ClientSupplierQTO qto);

    /**
     * 根据id获取供应商
     * @param id
     * @return
     */
    SupplierVO get(Long id);

    /**
     * 客户端修改供应商
     * @param dto
     * @return
     */
    void clientEdit(ClientSupplierDTO dto);

}
