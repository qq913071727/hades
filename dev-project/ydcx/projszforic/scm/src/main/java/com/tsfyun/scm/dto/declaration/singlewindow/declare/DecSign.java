package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 报关单签名
 * @since Created in 2020/4/21 12:22
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecSign", propOrder = {
        "operType",
        "icCode",
        "copCode",
        "operName",
        "sign",
        "signDate",
        "clientSeqNo",
        "certificate",
        "domainId",
        "billSeqNo",
        "hostId",
        "note",
})
public class DecSign {
    //操作类型
    /*
    A:报关单上载
    B：报关单、转关单上载
    C:报关单申报
    D：报关单、转关单申报
    E：电子手册报关单上载（此种操作类型的报关单上载时不作非空和逻辑校验）
    G：报关单暂存（转关提前报关单暂存）
     */
    @XmlElement(name = "OperType")
    protected String operType;

    //操作员IC卡号
    @XmlElement(name = "ICCode")
    protected String icCode;

    //操作企业组织机构代码，海关编码
    @XmlElement(name = "CopCode")
    protected String copCode;

    //操作员姓名
    @XmlElement(name = "OperName")
    protected String operName;

    //数字签名信息
    @XmlElement(name = "Sign")
    protected String sign;

    //签名日期
    @XmlElement(name = "SignDate")
    protected String signDate;

    //客户端报关单编号
    @XmlElement(name = "ClientSeqNo")
    protected String clientSeqNo;

    //操作员的证书号
    @XmlElement(name = "Certificate")
    protected String certificate;

    //签收人分类1-录入人2-申报人
    @XmlElement(name = "DomainId")
    protected String domainId;

    //对应清单统一编号
    @XmlElement(name = "BillSeqNo")
    protected String billSeqNo;

    //客户端邮箱的HostId
    @XmlElement(name = "HostId")
    protected String hostId;

    //备注
    @XmlElement(name = "Note")
    protected String note;


    public void setOperType(String operType) {
        this.operType = operType;
    }

    public void setIcCode(String icCode) {
        this.icCode = icCode;
    }

    public void setCopCode(String copCode) {
        this.copCode = copCode;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public void setClientSeqNo(String clientSeqNo) {
        this.clientSeqNo = clientSeqNo;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public void setBillSeqNo(String billSeqNo) {
        this.billSeqNo = billSeqNo;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
