package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.util.StringUtils;
import lombok.Data;

import java.io.Serializable;

/**=
 * 归类物料信息导入
 */
@Data
public class ClassifyMaterielExcel extends BaseRowModel implements Serializable {


    @NotEmptyTrim(message = "物料型号不能为空")
    @LengthTrim(max = 100,message = "物料型号最大长度不能超过100位")
    @ExcelProperty(value = "物料型号", index = 0)
    private String model;

    @NotEmptyTrim(message = "物料品牌不能为空")
    @LengthTrim(max = 100,message = "物料品牌最大长度不能超过100位")
    @ExcelProperty(value = "物料品牌", index = 1)
    private String brand;

    @NotEmptyTrim(message = "物料名称不能为空")
    @ExcelProperty(value = "物料名称", index = 2)
    @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
    private String name;

    @NotEmptyTrim(message = "海关编码不能为空")
    @ExcelProperty(value = "海关编码", index = 3)
    @LengthTrim(min=10,max = 10,message = "海关编码必须10位")
    private String hsCode;

    @NotEmptyTrim(message = "申报要素不能为空")
    @ExcelProperty(value = "申报要素", index = 4)
    @LengthTrim(max = 255,message = "申报要素最大长度不能超过255位")
    private String elements;

    @ExcelProperty(value = "CIQ编码", index = 5)
    @LengthTrim(min=3,max = 3,message = "CIQ编码必须3位")
    private String cipNo;

    @ExcelProperty(value = "战略物资", index = 6)
    private String isSmp;

    @ExcelProperty(value = "需要3C证书", index = 7)
    private String isCapp;

    @ExcelProperty(value = "需要3C目录鉴定", index = 8)
    private String isCappNo;

    @ExcelProperty(value = "3C证书编号", index = 9)
    private String cappNo;

    @ExcelProperty(value = "归类备注", index = 10)
    @LengthTrim(max = 255,message = "归类备注最大长度不能超过255位")
    private String memo;

    private Long personId;//操作人ID
    private String personName;//操作人名称


    public void setModel(String model){
        this.model = StringUtils.removeSpecialSymbol(model).toUpperCase();
    }
    public void setBrand(String brand){
        this.brand = StringUtils.removeSpecialSymbol(brand).toUpperCase();
    }
    public void setName(String name){
        this.name = StringUtils.removeSpecialSymbol(name);
    }
    public void setHsCode(String hsCode){
        this.hsCode = StringUtils.removeSpecialSymbol(hsCode);
    }
    public void setElements(String elements){
        this.elements = StringUtils.removeSpecialSymbol(elements);
    }
    public void setMemo(String memo){
        this.memo = StringUtils.removeSpecialSymbol(memo);
    }

}
