package com.tsfyun.scm.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class LogLoginDTO implements Serializable {


    /**
     * 员工ID
     */
    private Long personId;

    /**
     * 员工名称
     */
    private String personName;

    /**
     * 登陆设备
     */
    private String device;

    /**
     * 登陆IP
     */
    private String ip;

    /**
     * 登陆地址
     */
    private String address;

    /**
     * 登陆方式
     */
    private Integer loginType;


    /**
     * 浏览器信息
     */
    private String userAgent;



}
