package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @CreateDate: Created in 2021/12/14 14:12
 */
@Data
public class ImpOrderDomesticTakeQTO extends PaginationDto {

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 客户单号
     */
    private String customerOrderNo;

    /**
     * 是否提货
     */
    private Boolean overseasTakeFlag;

    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
