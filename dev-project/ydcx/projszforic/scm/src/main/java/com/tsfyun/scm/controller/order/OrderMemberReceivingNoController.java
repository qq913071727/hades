package com.tsfyun.scm.controller.order;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import com.tsfyun.scm.service.order.IOrderMemberReceivingNoService;
import com.tsfyun.scm.vo.order.OrderMemberReceivingNoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 绑定入库单辅助表 前端控制器
 * </p>
 *
 *
 * @since 2020-05-06
 */
@RestController
@RequestMapping("/orderMemberReceivingNo")
public class OrderMemberReceivingNoController extends BaseController {

    @Autowired
    private IOrderMemberReceivingNoService orderMemberReceivingNoService;

    @PostMapping(value = "item")
    public Result<List<OrderMemberReceivingNoVO>> item(@RequestParam(value = "orderMemberId")Long orderMemberId){
        return success(orderMemberReceivingNoService.getByMemberId(orderMemberId));
    }
}

