package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AuxiliaryInspectionVO implements Serializable {

    private String rowNo;//箱号
    private String model;//型号
    private String brand;//品牌
    private BigDecimal quantity;//数量
    private Integer carton;//箱数
    private BigDecimal netWeight;//净重
    private BigDecimal grossWeight;//毛重
    private String country;//产地
}
