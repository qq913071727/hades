package com.tsfyun.scm.controller.customer;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.customer.ICustomerPersonChangeService;
import com.tsfyun.scm.vo.customer.CustomerPersonChangeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @since Created in 2020/4/1 17:45
 */
@RestController
@RequestMapping(value = "/customerPersonChange")
public class CustomerPersonChangeController extends BaseController {

    @Autowired
    private ICustomerPersonChangeService customerPersonChangeService;

    /**
     * 获取客户商务/销售变更历史
     * @param personType
     * @param customerId
     * @return
     */
    @GetMapping(value = "history")
    Result<List<CustomerPersonChangeVO>> history(@RequestParam(value = "personType")Integer personType, @RequestParam(value = "customerId")Long customerId) {
        return success(customerPersonChangeService.records(personType,customerId));
    }

}
