package com.tsfyun.scm.service.customer;

import com.tsfyun.scm.dto.customer.ImpQuoteDTO;
import com.tsfyun.scm.entity.customer.ImpQuote;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 * 进口报价 服务类
 * </p>
 *
 *
 * @since 2020-04-01
 */
public interface IImpQuoteService extends IService<ImpQuote> {

    /**
     * 根据协议id获取报价
     * @param agreementId
     * @return
     */
    List<ImpQuote> findByAgreementId(Long agreementId);

    /**=
     * 构建协议报价描述
     * @param impQuotes
     * @return
     */
    String quoteDescribe(List<ImpQuote> impQuotes);
    /**=
     * 构建协议报价描述
     * @param impQuote
     * @return
     */
    String quoteDescribe(ImpQuote impQuote);

    /**=
     * 保存或修改协议报价
     * @param impQuotes
     */
    void saveImpQuotes(Long agreementId,Long customerId,List<ImpQuoteDTO> impQuotes);

    /**=
     * 根据协议删除报价
     * @param agreementId
     */
    void deleteByAgreementId(Long agreementId);

    /**
     * 初始化基础协议报价
     * @return
     */
    ImpQuote initBaseQuote();

}
