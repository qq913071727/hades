package com.tsfyun.scm.vo.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Data
@ApiModel(value="SubjectOverseasBank响应对象", description="响应实体")
public class SubjectOverseasBankVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "主体境外公司")
    private Long subjectOverseasId;

    @ApiModelProperty(value = "银行名称")
    private String name;

    @ApiModelProperty(value = "银行账号")
    private String account;

    @ApiModelProperty(value = "SWIFT CODE")
    private String swiftCode;

    @ApiModelProperty(value = "银行地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "默认")
    private Boolean isDefault;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "银行代码")
    private String code;


}
