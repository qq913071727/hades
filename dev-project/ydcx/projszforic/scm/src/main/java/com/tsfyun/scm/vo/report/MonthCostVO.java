package com.tsfyun.scm.vo.report;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MonthCostVO implements Serializable {

    private String month;// 月份
    private BigDecimal cost;// 应收费用
}
