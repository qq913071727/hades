package com.tsfyun.scm.entity.logistics;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class Conveyance extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 车牌1
     */

    private String conveyanceNo;

    /**
     * 车牌2
     */

    private String conveyanceNo2;

    /**
     * 海关注册编号
     */

    private String customsRegNo;

    /**
     * 禁用启用
     */

    private Boolean disabled;

    /**
     * 司机
     */

    private String driver;

    /**
     * 司机身份证号
     */

    private String driverNo;

    /**
     * 司机电话
     */

    private String driverTel;

    /**
     * 牌头
     */

    private String head;

    /**
     * 企业电话
     */

    private String headFax;

    /**
     * 企业代码
     */

    private String headNo;

    /**
     * 企业电话
     */

    private String headTel;


    private Long transportSupplierId;


    /**
     * 默认
     */

    private Boolean isDefault;


}
