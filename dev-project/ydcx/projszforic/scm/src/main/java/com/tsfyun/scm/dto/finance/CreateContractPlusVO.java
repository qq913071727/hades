package com.tsfyun.scm.dto.finance;

import com.tsfyun.scm.vo.finance.CreateContractMemberVO;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CreateContractPlusVO implements Serializable {

    /**
     * 合同主单
     */
    private CreateContractMainVO main;

    /**
     * 合同明细
     */
    private List<CreateContractMemberVO> members;

    /**
     * 订单费用
     */
    List<ExpOrderCostVO> orderCosts;
}
