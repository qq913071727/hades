package com.tsfyun.scm.dto.logistics;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-20
 */
@Data
@ApiModel(value="Conveyance请求对象", description="请求实体")
public class ConveyanceDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "车牌1不能为空")
   @LengthTrim(max = 20,message = "车牌1最大长度不能超过20位")
   @ApiModelProperty(value = "车牌1")
   private String conveyanceNo;

   @LengthTrim(max = 20,message = "车牌2最大长度不能超过20位")
   @ApiModelProperty(value = "车牌2")
   private String conveyanceNo2;

   @LengthTrim(max = 50,message = "海关注册编号最大长度不能超过50位")
   @ApiModelProperty(value = "海关注册编号")
   private String customsRegNo;

   @NotNull(message = "禁用启用不能为空")
   @ApiModelProperty(value = "禁用启用")
   private Boolean disabled;

   @LengthTrim(max = 50,message = "司机最大长度不能超过50位")
   @ApiModelProperty(value = "司机")
   private String driver;

   @LengthTrim(max = 50,message = "司机身份证号最大长度不能超过50位")
   @ApiModelProperty(value = "司机身份证号")
   private String driverNo;

   @LengthTrim(max = 50,message = "司机电话最大长度不能超过50位")
   @ApiModelProperty(value = "司机电话")
   private String driverTel;

   @LengthTrim(max = 50,message = "牌头最大长度不能超过50位")
   @ApiModelProperty(value = "牌头")
   private String head;

   @LengthTrim(max = 50,message = "企业电话最大长度不能超过50位")
   @ApiModelProperty(value = "企业电话")
   private String headFax;

   @LengthTrim(max = 50,message = "企业代码最大长度不能超过50位")
   @ApiModelProperty(value = "企业代码")
   private String headNo;

   @LengthTrim(max = 50,message = "企业电话最大长度不能超过50位")
   @ApiModelProperty(value = "企业电话")
   private String headTel;

   private Long transportSupplierId;

   @NotNull(message = "默认不能为空")
   @ApiModelProperty(value = "默认")
   private Boolean isDefault;


}
