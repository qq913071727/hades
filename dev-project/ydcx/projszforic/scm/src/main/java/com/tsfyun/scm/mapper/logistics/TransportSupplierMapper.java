package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.entity.logistics.TransportSupplier;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface TransportSupplierMapper extends Mapper<TransportSupplier> {

}
