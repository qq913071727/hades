package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.client.SystemDataClient;
import com.tsfyun.scm.system.vo.CountryVO;
import com.tsfyun.scm.system.vo.TaxCodeBaseVO;
import com.tsfyun.scm.system.vo.UnitVO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class SystemDataClientFallback implements FallbackFactory<SystemDataClient> {

    @Override
    public SystemDataClient create(Throwable throwable) {
        return new SystemDataClient(){
            @Override
            public Result<List<CountryVO>> countrySelect() {
                log.error("获取国家配置数据异常",throwable);
                return null;
            }

            @Override
            public Result<List<UnitVO>> unitSelect() {
                log.error("获取单位配置数据异常",throwable);
                return null;
            }

            @Override
            public Result<List<TaxCodeBaseVO>> taxSelect() {
                log.error("获取税收分类基础数据异常",throwable);
                return null;
            }
        };
    }
}
