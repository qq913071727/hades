package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteBindQTO;
import com.tsfyun.scm.entity.logistics.OverseasDeliveryNoteMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNoteMemberStockVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Repository
public interface OverseasDeliveryNoteMemberMapper extends Mapper<OverseasDeliveryNoteMember> {

    /**
     * 查询可以派送的订单明细数据
     * @param qto
     * @return
     */
    List<OverseasDeliveryNoteMemberStockVO> queryStockOrderMembers(OverseasDeliveryNoteBindQTO qto);

    /**
     * 更新派送数量
     * @param id
     * @param oldQuantity
     * @param quantity
     * @param totalPrice
     * @param grossWeight
     * @param netWeight
     * @return
     */
    int updateQuantity(@Param(value = "id") Long id,@Param(value = "oldQuantity") BigDecimal oldQuantity,
                       @Param(value = "quantity") BigDecimal quantity,@Param(value = "totalPrice") BigDecimal totalPrice,
                       @Param(value = "cartonNum") Integer cartonNum,
                       @Param(value = "grossWeight") BigDecimal grossWeight,
                       @Param(value = "netWeight") BigDecimal netWeight);

}
