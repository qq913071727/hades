package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.ExpOrderPriceMember;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Repository
public interface ExpOrderPriceMemberMapper extends Mapper<ExpOrderPriceMember> {

    Map<String, Object> historicalRecord(@Param(value = "orderMemberId") Long orderMemberId);

    Integer removeByOrderId(@Param(value = "orderId")Long orderId);
}
