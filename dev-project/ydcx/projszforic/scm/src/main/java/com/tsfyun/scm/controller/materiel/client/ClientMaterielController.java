package com.tsfyun.scm.controller.materiel.client;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.ImpExcelVO;
import com.tsfyun.scm.dto.materiel.MaterielQTO;
import com.tsfyun.scm.dto.materiel.client.ClientMaterielQTO;
import com.tsfyun.scm.service.materiel.IMaterielService;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import com.tsfyun.scm.vo.materiel.client.ClientMaterielVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/10/12 11:27
 */
@RestController
@RequestMapping(value = "/client/materiel")
@Api(tags = "我的产品（客户端）")
public class ClientMaterielController extends BaseController {

    @Autowired
    private IMaterielService materielService;

    /**
     * 分页列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    @ApiOperation("分页列表")
    Result<List<ClientMaterielVO>> list(ClientMaterielQTO qto){
        return materielService.clientPage(qto);
    }

    /**=
     * 模糊查询
     * @param qto
     * @return
     */
    @PostMapping(value = "vague")
    @ApiOperation("根据型号、品牌、品名搜索")
    Result<List<SimpleMaterielVO>> vague(@ModelAttribute MaterielQTO qto){
        return success(materielService.clientVague(qto));
    }

    /**=
     * 品牌模糊查询
     * @param brand
     * @return
     */
    @PostMapping(value = "vagueByBrand")
    @ApiOperation("根据品牌、模糊搜索")
    Result<List<String>> vagueByBrand(@RequestParam(value = "brand",required = false)String brand){
        return success(materielService.searchByBrand(brand));
    }


    /**=
     * 客户端物料导入
     * @param file
     * @return
     */
    @PostMapping(value = "import")
    Result<ImpExcelVO> importMateriel(@RequestPart(value = "file") MultipartFile file){
        return success(materielService.clientImportMateriel(file));
    }

}
