package com.tsfyun.scm.service.impl.customer;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Snowflake;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.extension.OrderItem;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.customer.CustomerDeliveryInfoDTO;
import com.tsfyun.scm.dto.customer.client.ClientCustomerDeliveryInfoQTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.customer.CustomerDeliveryInfo;
import com.tsfyun.scm.mapper.customer.CustomerDeliveryInfoMapper;
import com.tsfyun.scm.service.customer.ICustomerDeliveryInfoService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.customer.ICustomerService;
import com.tsfyun.scm.util.PermissionUtil;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.customer.CustomerDeliveryInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-03
 */
@Service
public class CustomerDeliveryInfoServiceImpl extends ServiceImpl<CustomerDeliveryInfo> implements ICustomerDeliveryInfoService {

    @Autowired
    private ICustomerService customerService;

    @Autowired
    private CustomerDeliveryInfoMapper customerDeliveryInfoMapper;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private Snowflake snowflake;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(CustomerDeliveryInfoDTO dto,boolean handleDefault) {
        TsfPreconditions.checkArgument(Objects.nonNull(dto.getCustomerId()),new ServiceException("请选择客户"));
        Customer customer = customerService.getById(dto.getCustomerId());
        TsfPreconditions.checkArgument(Objects.nonNull(customer),new ServiceException("您选择的客户信息不存在"));
        CustomerDeliveryInfo customerDeliveryInfo = beanMapper.map(dto,CustomerDeliveryInfo.class);
        if(handleDefault) {
            //默认地址信息处理
            if (Objects.equals(Boolean.TRUE, customerDeliveryInfo.getIsDefault())) {
                //需要设置为默认地址，需要取消之前设置的默认地址
                handleOldDefault(customer.getId());
            }
        }
        super.saveNonNull(customerDeliveryInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CustomerDeliveryInfoDTO dto,boolean handleDefault) {
        TsfPreconditions.checkArgument(Objects.nonNull(dto.getCustomerId()),new ServiceException("请选择客户"));
        CustomerDeliveryInfo oldCustomerDeliveryInfo = super.getById(dto.getId());
        TsfPreconditions.checkArgument(Objects.nonNull(oldCustomerDeliveryInfo),new ServiceException("您选择的数据不存在"));
        Customer customer = customerService.getById(dto.getCustomerId());
        TsfPreconditions.checkArgument(Objects.nonNull(customer),new ServiceException("您选择的客户信息不存在"));
        CustomerDeliveryInfo customerDeliveryInfo = beanMapper.map(dto,CustomerDeliveryInfo.class);
        customerDeliveryInfo.setDateCreated(oldCustomerDeliveryInfo.getDateCreated());
        customerDeliveryInfo.setCreateBy(oldCustomerDeliveryInfo.getCreateBy());
        customerDeliveryInfo.setDisabled(Objects.equals(dto.getDisabled(),Boolean.TRUE));
        customerDeliveryInfo.setIsDefault(Objects.equals(dto.getIsDefault(),Boolean.TRUE));
        if(handleDefault) {
            //如果由非默认变为默认则需要处理之前设置的默认的
            if (!oldCustomerDeliveryInfo.getIsDefault() && customerDeliveryInfo.getIsDefault()) {
                handleOldDefault(customer.getId());
            }
            super.updateById(customerDeliveryInfo);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveList(Long customerId, List<CustomerDeliveryInfoDTO> dtos) {
        if(CollectionUtil.isEmpty(dtos)) {
            return;
        }
        //设置第一个启用的为默认
        Boolean isDefault = false;
        for(int i=0;i<dtos.size();i++){
            CustomerDeliveryInfoDTO dto = dtos.get(i);
            if(dto.getIsDefault() && dto.getDisabled() != true){
                isDefault=true;
            }
        }
        if(isDefault==false){
            for(int i=0;i<dtos.size();i++) {
                CustomerDeliveryInfoDTO dto = dtos.get(i);
                if (dto.getDisabled() != true) {
                    dtos.get(i).setIsDefault(Boolean.TRUE);
                }
            }
        }
        //删除客户地址信息
        removeByCustomerId(customerId);
        List<CustomerDeliveryInfo> customerDeliveryInfoList = Lists.newArrayList();
        dtos.stream().forEach(delivery ->{
            CustomerDeliveryInfo customerDeliveryInfo = new CustomerDeliveryInfo();
            customerDeliveryInfo.setId(snowflake.nextId());
            customerDeliveryInfo.setCustomerId(customerId);

            customerDeliveryInfo.setCompanyName(StringUtils.removeSpecialSymbol(delivery.getCompanyName()));
            customerDeliveryInfo.setLinkPerson(StringUtils.removeSpecialSymbol(delivery.getLinkPerson()));
            customerDeliveryInfo.setLinkTel(StringUtils.removeSpecialSymbol(delivery.getLinkTel()));
            customerDeliveryInfo.setProvinceName(StringUtils.removeSpecialSymbol(delivery.getProvinceName()));
            customerDeliveryInfo.setCityName(StringUtils.removeSpecialSymbol(delivery.getCityName()));
            customerDeliveryInfo.setAreaName(StringUtils.removeSpecialSymbol(delivery.getAreaName()));
            customerDeliveryInfo.setAddress(StringUtils.removeSpecialSymbol(delivery.getAddress()));
            customerDeliveryInfo.setDisabled(delivery.getDisabled());
            customerDeliveryInfo.setIsDefault(delivery.getIsDefault());
            //customerDeliveryInfo.setDateCreated(LocalDateTime.now());
            //customerDeliveryInfo.setCreateBy(SecurityUtil.getCurrentPersonIdAndName());
            //customerDeliveryInfo.setDateUpdated(customerDeliveryInfo.getDateCreated());
            //customerDeliveryInfo.setUpdateBy(customerDeliveryInfo.getCreateBy());
            customerDeliveryInfoList.add(customerDeliveryInfo);
        });
        super.savaBatch(customerDeliveryInfoList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void activeDisable(Long id) {
        CustomerDeliveryInfo customerDeliveryInfo = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(customerDeliveryInfo),new ServiceException("客户收货地址信息不存在"));
        customerDeliveryInfo.setDisabled(!customerDeliveryInfo.getDisabled());
        customerDeliveryInfo.setIsDefault(Boolean.FALSE);
        super.updateById(customerDeliveryInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void setDefault(Long id) {
        CustomerDeliveryInfo  customerDeliveryInfo = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(customerDeliveryInfo),new ServiceException("客户收货地址信息不存在"));
        Boolean currentDefault = customerDeliveryInfo.getIsDefault();
        if(Objects.equals(Boolean.TRUE,currentDefault)) {
            //以前是默认，则现在无需操作
            return;
        } else {
            //以前是非默认，现在置为默认，则需要取消以前的默认如果存在的话
            handleOldDefault(customerDeliveryInfo.getCustomerId());
            customerDeliveryInfo.setIsDefault(Boolean.TRUE);
            customerDeliveryInfo.setDisabled(Boolean.FALSE);
            super.updateById(customerDeliveryInfo);
        }
    }

    /**
     * 取消以前设置的默认地址
     * @param customerId
     */
    @Override
    public void handleOldDefault(Long customerId) {
        CustomerDeliveryInfo update = new CustomerDeliveryInfo();
        update.setIsDefault(Boolean.FALSE);
        customerDeliveryInfoMapper.updateByExampleSelective(update, Example.builder(CustomerDeliveryInfo.class).where(WeekendSqls.<CustomerDeliveryInfo>custom()
                .andEqualTo(CustomerDeliveryInfo::getCustomerId, customerId)
                .andEqualTo(CustomerDeliveryInfo::getIsDefault,Boolean.TRUE)).build());
    }

    @Override
    public List<CustomerDeliveryInfoVO> select(Long customerId) {
        CustomerDeliveryInfo condition = new CustomerDeliveryInfo();
        condition.setCustomerId(customerId);
        condition.setDisabled(Boolean.FALSE);
        List<CustomerDeliveryInfo> list = super.list(condition);
        //按是否默认排序，然后按最后修改日期排序
        if(CollectionUtil.isNotEmpty(list)) {
            //先按是否默认排序然后按修改时间排序
            list = list.stream().sorted(Comparator.comparing(CustomerDeliveryInfo::getIsDefault).thenComparing(CustomerDeliveryInfo::getDateUpdated).reversed())
                .collect(Collectors.toList());
            return beanMapper.mapAsList(list,CustomerDeliveryInfoVO.class);
        }
        return null;
    }

    @Override
    public List<CustomerDeliveryInfoVO> effectiveList(String customerName) {
        if(StringUtils.isNotEmpty(customerName)){
            Map<String,Object> params = new LinkedHashMap<String,Object>(){{
                put("customerName",customerName);
            }};
            return customerDeliveryInfoMapper.effectiveList(params);

        }
        return Lists.newArrayList();
    }

    @Override
    public PageInfo<CustomerDeliveryInfoVO> pageList(CustomerDeliveryInfoDTO dto) {
        PageHelper.startPage(dto.getPage(),dto.getLimit());
        Map<String,Object> params = beanMapper.map(dto,Map.class);
        return new PageInfo<>(customerDeliveryInfoMapper.list(params));
    }

    @Override
    public CustomerDeliveryInfoVO detail(Long id) {
        CustomerDeliveryInfo customerDeliveryInfo = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(customerDeliveryInfo),new ServiceException("客户收货地址信息不存在"));
        return beanMapper.map(customerDeliveryInfo,CustomerDeliveryInfoVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeByCustomerId(Long customerId) {
        /*
        CustomerDeliveryInfo condition = new CustomerDeliveryInfo();
        condition.setCustomerId(customerId);
        super.remove(condition);
         */
        customerDeliveryInfoMapper.deleteByExample(Example.builder(CustomerDeliveryInfo.class).where(TsfWeekendSqls.<CustomerDeliveryInfo>custom().andEqualTo(false,CustomerDeliveryInfo::getCustomerId,customerId)).build());
    }

    @Override
    public List<CustomerDeliveryInfo> list(Long customerId) {
        CustomerDeliveryInfo condition = new CustomerDeliveryInfo();
        condition.setCustomerId(customerId);
        return super.list(condition);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteById(Long id) {
        CustomerDeliveryInfo customerDeliveryInfo = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(customerDeliveryInfo),new ServiceException("客户收货地址信息不存在"));
        super.removeById(id);
    }

    @Override
    public int countByCustomerId(Long customerId) {
        return customerDeliveryInfoMapper.selectCountByExample(Example.builder(CustomerDeliveryInfo.class).where(TsfWeekendSqls.<CustomerDeliveryInfo>custom().andEqualTo(true,CustomerDeliveryInfo::getCustomerId,customerId)).build());
    }

    @Override
    public PageInfo<CustomerDeliveryInfo> clientPage(ClientCustomerDeliveryInfoQTO qto) {
        TsfWeekendSqls sqls = TsfWeekendSqls.<CustomerDeliveryInfo>custom()
                .andEqualTo(false,CustomerDeliveryInfo::getCustomerId,SecurityUtil.getCurrentCustomerId())
                .andEqualTo(true,CustomerDeliveryInfo::getDisabled,qto.getDisabled())
                .andEqualTo(true,CustomerDeliveryInfo::getIsDefault,qto.getIsDefault());
        return super.pageList(qto.getPage(),qto.getLimit(),sqls,Lists.newArrayList(OrderItem.desc("isDefault"),OrderItem.asc("companyName")));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void clientRemove(Long id) {
        CustomerDeliveryInfo customerDeliveryInfo = super.getById(id);
        Optional.ofNullable(customerDeliveryInfo).orElseThrow(()->new ServiceException("客户收货信息不存在，请确认是否已经被删除"));
        PermissionUtil.checkClientCustomerPermission(customerDeliveryInfo);
        super.removeById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void clientAdd(CustomerDeliveryInfoDTO dto) {
        CustomerDeliveryInfo customerDeliveryInfo = beanMapper.map(dto,CustomerDeliveryInfo.class);
        customerDeliveryInfo.setDisabled(Boolean.FALSE);
        Long customerId = SecurityUtil.getCurrentCustomerId();
        customerDeliveryInfo.setCustomerId(customerId);
        //默认地址信息处理
        if (Objects.equals(Boolean.TRUE, customerDeliveryInfo.getIsDefault())) {
            //需要设置为默认地址，需要取消之前设置的默认地址
            handleOldDefault(SecurityUtil.getCurrentCustomerId());
        }
        super.saveNonNull(customerDeliveryInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void clientEdit(CustomerDeliveryInfoDTO dto) {
        CustomerDeliveryInfo oldCustomerDeliveryInfo = super.getById(dto.getId());
        TsfPreconditions.checkArgument(Objects.nonNull(oldCustomerDeliveryInfo),new ServiceException("您选择的数据不存在"));
        CustomerDeliveryInfo customerDeliveryInfo = beanMapper.map(dto,CustomerDeliveryInfo.class);
        Long customerId = SecurityUtil.getCurrentCustomerId();
        customerDeliveryInfo.setCustomerId(customerId);
        customerDeliveryInfo.setDateCreated(oldCustomerDeliveryInfo.getDateCreated());
        customerDeliveryInfo.setCreateBy(oldCustomerDeliveryInfo.getCreateBy());
        customerDeliveryInfo.setDisabled(Objects.equals(dto.getDisabled(),Boolean.TRUE));
        customerDeliveryInfo.setIsDefault(Objects.equals(dto.getIsDefault(),Boolean.TRUE));
        //如果由非默认变为默认则需要处理之前设置的默认的
        if (!oldCustomerDeliveryInfo.getIsDefault() && customerDeliveryInfo.getIsDefault()) {
            customerDeliveryInfo.setDisabled(Boolean.FALSE);
            handleOldDefault(customerId);
        }
        super.updateById(customerDeliveryInfo);
    }
}
