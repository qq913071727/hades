package com.tsfyun.scm.service.order;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.enums.domain.DomainTypeEnum;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.support.DomainStatus;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.finance.ClaimedAmountOrderDTO;
import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.dto.support.TaskNoticeContentDTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.finance.OverseasReceivingOrder;
import com.tsfyun.scm.entity.order.*;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ExpOrderMemberVO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 出口订单明细 服务类
 * </p>
 *
 *
 * @since 2021-01-26
 */
public interface IExpOrderMemberService extends IService<ExpOrderMember> {

    /**
     * 校验赋值订单明细
     * @param order
     * @param paramMembers
     * @param submitAudit
     * @return
     */
    ExpOrderMemberSaveDTO checkWrapOrderMember(ExpOrder order, List<ExpOrderMemberDTO> paramMembers, Boolean submitAudit);


    /**=
     * 保存，修改，删除订单明细
     * @param expOrder
     * @param expOrderMemberSaveDTO
     */
    Integer saveMembers(ExpOrder expOrder, ExpOrderMemberSaveDTO expOrderMemberSaveDTO, Boolean isSubmit);

    /**
     * 根据订单获取订单明细
     * @param expOrderId
     * @return
     */
    List<ExpOrderMember> getByExpOrderId(Long expOrderId);

    /**
     * 根据订单获取订单物料明细
     * @param expOrderId
     * @return
     */
    List<ExpOrderMemberVO> findOrderMaterielMember(Long expOrderId);

    /**
     * 根据订单id删除明细信息
     * @param orderId
     */
    void removeByExpOrderId(Long orderId);

    /**
     * 根据订单id获取订单明细数据
     * @param expOrderId
     * @return
     */
    List<ExpOrderMember> getByOrderId(Long expOrderId);

    /**=
     * 根据时间和物料
     * @param dateTime
     * @return
     */
    List<PriceFluctuationHistoryExp> findExpHistory(LocalDateTime dateTime,String model,String brand,String name,String spec);

    /**=
     * 根据物料ID修改物料ID
     * @param oldId
     * @param newId
     */
    void updateMemberMaterielId(String oldId,String newId);

    /**=
     * 更新赋值数据
     * @param expOrderMemberList
     */
    void batchUpdatePartData(List<ExpOrderMember> expOrderMemberList);

    /**=
     * 验货保存修改明细
     */
    Integer saveInspectionMembers(ExpOrder expOrder,List<ExpOrderMember> expOrderMembers);

    /**
     * 境外收款认领
     * @param dto
     */
    void overseasAccountClaimed(ClaimedAmountOrderDTO dto);

    /**
     * 取消明细认领
     * @param receivingOrder
     */
    void clearClaimOrder(OverseasReceivingOrder receivingOrder);

    /**
     * 一票到底根据订单获取产品明细
     * @param orderId
     * @return
     */
    List<ExpOneVoteTheEndVO> oneVoteTheEndByOrderId(Long orderId);

    /**
     * 获取订单总数量
     * @param orderNo
     * @return
     */
    BigDecimal getOrderQuantity(String orderNo);
}
