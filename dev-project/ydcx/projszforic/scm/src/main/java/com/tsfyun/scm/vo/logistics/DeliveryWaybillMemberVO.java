package com.tsfyun.scm.vo.logistics;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 送货单明细响应实体
 * </p>
 *

 * @since 2020-05-25
 */
@Data
@ApiModel(value="DeliveryWaybillMember响应对象", description="送货单明细响应实体")
public class DeliveryWaybillMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;
    /**
     * 型号
     */
    private String model;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 名称
     */
    private String name;
    /**
     * 客户物料号
     */
    private String goodsCode;
    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;
    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;


}
