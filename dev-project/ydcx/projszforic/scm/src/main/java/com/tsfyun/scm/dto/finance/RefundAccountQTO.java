package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @Description: 退款申请查询请求实体
 * @CreateDate: Created in 2020/11/27 09:59
 */
@Data
public class RefundAccountQTO extends PaginationDto implements Serializable {

    private Long customerId;

    private String customerName;

    private String statusId;

    private String docNo;

    private String clientStatusId;


    /**
     * 状态集合
     */
    private List<String> statusIds;

    private LocalDateTime dateCreatedStart;

    private LocalDateTime dateCreatedEnd;

    public void setDateCreatedEnd(LocalDateTime dateCreatedEnd) {
        if(dateCreatedEnd != null){
            this.dateCreatedEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateCreatedEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
