package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class ClientImpOrderVO implements Serializable {

    @ApiModelProperty(value = "订单ID")
    private Long id;
    @ApiModelProperty(value = "订单编号")
    private String docNo;//订单编号
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;//供应商
    @ApiModelProperty(value = "状态编码")
    private String statusId;//状态
    @ApiModelProperty(value = "状态名称")
    private String statusDesc;//状态
    @ApiModelProperty(value = "订单日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;//订单日期
    @ApiModelProperty(value = "币制名称")
    private String currencyName;//币制
    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;//委托金额
    @ApiModelProperty(value = "报关金额")
    private BigDecimal decTotalPrice;//报关金额
    @ApiModelProperty(value = "付汇金额")
    private BigDecimal payVal;//付汇金额
    @ApiModelProperty(value = "明细条数")
    private Integer totalMember;//明细条数
    @ApiModelProperty(value = "关税金额")
    private BigDecimal tariffRateVal;//关税金额
    @ApiModelProperty(value = "订单应收费用")
    private BigDecimal receAmount;//订单应收费用
    @ApiModelProperty(value = "付汇情况",notes = "1-未申请付汇，2-部分申请付汇，3-付汇完成")
    private Integer paySituation;//付汇情况（1-未申请付汇，2-部分申请付汇，3-付汇完成）
    @ApiModelProperty(value = "是否开票完成")
    private Boolean isInvoiceComplete;//开票完成
    @ApiModelProperty(value = "已申请付汇金额")
    private BigDecimal applyPayVal;//已申请付汇金额
    //前1条订单明细
    @ApiModelProperty(value = "前1条订单明细")
    private ImpOrderMemberSimpleVO member;
    //前1条订单明细组合数据
    @JsonIgnore
    private String memDesc;

    //物流信息
    @ApiModelProperty(value = "国内收货方式")
    private String receivingMode;// 国内收货方式
    @ApiModelProperty(value = "国内收货方式")
    private String receivingModeName;// 国内收货方式
    @ApiModelProperty(value = "物流公司")
    private String logisticsCompanyName;// 物流公司
    @ApiModelProperty(value = "收货公司")
    private String deliveryCompanyName;// 收货公司
    @ApiModelProperty(value = "收货联系人")
    private String deliveryLinkPerson;// 收货联系人
    @ApiModelProperty(value = "收货联系人电话")
    private String deliveryLinkTel;// 收货联系人电话
    @ApiModelProperty(value = "收货地址")
    private String deliveryAddress;// 收货地址

    @ApiModelProperty(value = "提货仓库名称")
    private String selfWareName;//提货仓库名称
    @ApiModelProperty(value = "提货联系人电话")
    private String selfLinkPerson;//提货联系人电话
    @ApiModelProperty(value = "提货人身份证号")
    private String selfPersonId;//提货人身份证号

    public String getStatusDesc() {
        ImpOrderStatusEnum impOrderStatusEnum = ImpOrderStatusEnum.of(statusId);
        return Objects.nonNull(impOrderStatusEnum) ? impOrderStatusEnum.getName() : "";
    }
}
