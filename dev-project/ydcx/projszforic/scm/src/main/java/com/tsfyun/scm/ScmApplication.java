package com.tsfyun.scm;
import com.google.common.base.Stopwatch;
import com.tsfyun.scm.log.annotation.EnableLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.web.context.request.RequestContextListener;
import tk.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

@SpringBootApplication()
@ComponentScan(value={"com.tsfyun"})
@MapperScan(basePackages = "com.tsfyun.*.mapper")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.tsfyun.scm.client")
@EnableCircuitBreaker
@ServletComponentScan("com.tsfyun.scm.filter")
@EnableLog
@EnableAspectJAutoProxy(exposeProxy = true)
@Slf4j
public class ScmApplication {

    public static void main(String[] args) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        ConfigurableApplicationContext run = SpringApplication.run(ScmApplication.class, args);
        Environment env = run.getEnvironment();
        String host = "127.0.0.1";
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {}
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 '{}' 运行成功!\n\t" +
                        "Swagger文档: \t\thttp://{}:{}{}{}/doc.html\n\t" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                host,
                env.getProperty("server.port"),
                env.getProperty("server.servlet.context-path", ""),
                env.getProperty("spring.mvc.servlet.path", "")
        );
        log.info("本次启动系统耗时{}秒",stopwatch.elapsed(TimeUnit.SECONDS));
    }

    /**
     * 暴露request对象，防止多线程等其他情况获取不到
     * @return
     */
    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }

    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
        messageBundle.setBasename("message/message");
        messageBundle.setDefaultEncoding("UTF-8");
        return messageBundle;
    }

}
