package com.tsfyun.scm.dto.user;


import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;

@Data
public class SGTLoginDTO implements Serializable {

    /**
     * 海关代码
     */
    @NotEmptyTrim(message = "海关代码不能为空")
    private String customsCode;

    /**
     * 登录账号
     */
    @NotEmptyTrim(message = "登录账号不能为空")
    private String userName;

    /**
     * 密码
     */
    @NotEmptyTrim(message = "登录密码不能为空")
    private String passWord;
}
