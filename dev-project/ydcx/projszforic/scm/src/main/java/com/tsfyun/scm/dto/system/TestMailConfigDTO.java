package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description:
 * @since Created in 2020/3/30 18:23
 */
@Data
public class TestMailConfigDTO implements Serializable {

    @NotEmptyTrim(message = "邮箱服务器不能为空")
    @LengthTrim(max = 100,message = "邮箱服务器最大长度不能超过100位")
    @ApiModelProperty(value = "邮箱服务器")
    private String mailHost;

    @Email(message = "邮箱格式错误")
    @NotEmptyTrim(message = "邮箱用户名不能为空")
    @LengthTrim(max = 100,message = "邮箱用户名最大长度不能超过100位")
    @ApiModelProperty(value = "邮箱用户名")
    private String mailUser;

    @NotEmptyTrim(message = "邮箱密码不能为空")
    @LengthTrim(max = 150,message = "邮箱密码最大长度不能超过150位")
    @ApiModelProperty(value = "邮箱密码")
    private String mailPassword;

    @NotNull(message = "邮箱端口不能为空")
    @LengthTrim(max = 4,message = "邮箱端口最大长度不能超过4位")
    private Integer mailPort;

}
