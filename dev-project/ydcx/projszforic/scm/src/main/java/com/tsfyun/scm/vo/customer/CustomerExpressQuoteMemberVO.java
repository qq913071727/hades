package com.tsfyun.scm.vo.customer;

import com.tsfyun.scm.util.ExpressQuoteUtil;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/24 18:42
 */
@Data
public class CustomerExpressQuoteMemberVO implements Serializable {


    /**
     * 开始重量
     */

    private BigDecimal startWeight;

    /**
     * 结束重量
     */

    private BigDecimal endWeight;

    /**
     * 单价-KG
     */

    private BigDecimal price;

    /**
     * 基准价
     */
    private BigDecimal basePrice;

    /**
     * 结束重量快速计算费用
     */

    private BigDecimal fastAmount;

}
