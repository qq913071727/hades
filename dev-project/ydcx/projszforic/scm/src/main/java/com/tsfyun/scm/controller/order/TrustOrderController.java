package com.tsfyun.scm.controller.order;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.order.ITrustOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trustOrder")
public class TrustOrderController extends BaseController {

    @Autowired
    private ITrustOrderService trustOrderService;

    /**
     * 初始化订单 生成订单号
     * @return
     */
    @PostMapping("init")
    public Result<String> init(){
        return success(trustOrderService.generateDocNo());
    }
}
