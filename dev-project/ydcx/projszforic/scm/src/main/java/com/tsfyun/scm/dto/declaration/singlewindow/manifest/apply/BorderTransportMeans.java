package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @Description: 运输工具数据
 * @since Created in 2020/4/22 11:11
 */
@Data
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BorderTransportMeans", propOrder = {
        "typeCode",
        "itinerary",
})
public class BorderTransportMeans {

    //运输方式，参考CN012
    @XmlElement(name = "TypeCode")
    protected String typeCode;

    @XmlElement(name = "Itinerary")
    protected List<Itinerary> itinerary;


}
