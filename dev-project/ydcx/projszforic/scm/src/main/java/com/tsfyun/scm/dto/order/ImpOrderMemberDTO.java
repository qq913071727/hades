package com.tsfyun.scm.dto.order;

import java.math.BigDecimal;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.*;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 订单明细请求实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrderMember请求对象", description="订单明细请求实体")
public class ImpOrderMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @ApiModelProperty(value = "订单明细id")
   private Long id;

   @NotEmptyTrim(message = "型号不能为空",groups = UpdateGroup.class)
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空",groups = UpdateGroup.class)
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   @LengthTrim(max = 500,message = "规格参数最大长度不能超过500位")
   @ApiModelProperty(value = "规格参数")
   private String spec;

   @LengthTrim(max = 100,message = "客户物料号最大长度不能超过100位")
   @ApiModelProperty(value = "客户物料号")
   private String goodsCode;

   @NotEmptyTrim(message = "产地不能为空",groups = UpdateGroup.class)
   @LengthTrim(max = 10,message = "产地最大长度不能超过10位")
   @ApiModelProperty(value = "产地")
   private String countryName;

   @NotEmptyTrim(message = "单位不能为空",groups = UpdateGroup.class)
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "数量不能为空",groups = UpdateGroup.class)
   @Digits(integer = 10, fraction = 2, message = "数量整数位不能超过10位，小数位不能超过2位")
   @DecimalMin(value = "0.01",message = "数量必须大于0")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "总价不能为空",groups = UpdateGroup.class)
   @Digits(integer = 10, fraction = 2, message = "总价整数位不能超过10位，小数位不能超过2位")
   @DecimalMin(value = "0.01",message = "总价必须大于0")
   @ApiModelProperty(value = "总价")
   private BigDecimal totalPrice;

   @NotNull(message = "净重不能为空",groups = UpdateGroup.class)
   @Digits(integer = 10, fraction = 4, message = "净重整数位不能超过10位，小数位不能超过4位")
   @DecimalMin(value = "0",message = "净重必须大于等于0")
   @ApiModelProperty(value = "净重")
   private BigDecimal netWeight;

   @NotNull(message = "毛重不能为空",groups = UpdateGroup.class)
   @Digits(integer = 10, fraction = 4, message = "毛重整数位不能超过10位，小数位不能超过4位")
   @DecimalMin(value = "0",message = "毛重必须大于等于0")
   @ApiModelProperty(value = "毛重")
   private BigDecimal grossWeight;

   @NotNull(message = "箱数不能为空",groups = UpdateGroup.class)
   @LengthTrim(max = 6,message = "箱数最大长度不能超过6位")
   @Min(value = 0,message = "箱数必须大于等于0")
   @ApiModelProperty(value = "箱数")
   private Integer cartonNum;

   @ApiModelProperty(value = "箱号")
   @LengthTrim(max = 200,message = "箱号最大长度不能超过200位")
   private String cartonNo;
}
