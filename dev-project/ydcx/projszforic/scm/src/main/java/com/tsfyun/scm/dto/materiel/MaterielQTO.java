package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**=
 * 物料查询
 */
@Data
public class MaterielQTO extends PaginationDto {


    private String statusId;//状态

    private String customerName;//客户名称

    private String model;//型号

    private String brand;//品牌

    private String name;//名称

    private String hsCode;//海关编号

    private String classifyPerson;//归类人员

    private String decElements;//申报要素

    private Boolean isSmp;//是否涉证
    private Boolean isCapp;//需要3C证书(当检验检疫为L时)
    private Boolean isCappNo;//需要3C目录鉴定


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateCreatedStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateCreatedEnd;

    public void setDateCreatedEnd(Date dateCreatedEnd){
        if(dateCreatedEnd!=null){
            this.dateCreatedEnd = DateUtils.parseLong(DateUtils.format(dateCreatedEnd,"yyyy-MM-dd 23:59:59"));
        }
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date classifyTimeStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date classifyTimeEnd;

    public void setClassifyTimeEnd(Date classifyTimeEnd){
        if(classifyTimeEnd!=null){
            this.classifyTimeEnd = DateUtils.parseLong(DateUtils.format(classifyTimeEnd,"yyyy-MM-dd 23:59:59"));
        }
    }
}
