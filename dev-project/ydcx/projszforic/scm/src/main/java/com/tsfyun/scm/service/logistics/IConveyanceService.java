package com.tsfyun.scm.service.logistics;

import com.tsfyun.scm.dto.logistics.ConveyanceDTO;
import com.tsfyun.scm.dto.logistics.ConveyanceQTO;
import com.tsfyun.scm.entity.logistics.Conveyance;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.SimpleConveyanceVO;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * <p>
 *  运输工具服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface IConveyanceService extends IService<Conveyance> {

    /**
     * 根据运输供应商获取所有的车辆信息
     * @param transportSupplierId
     * @return
     */
    List<Conveyance> list(@NonNull Long transportSupplierId);

    /**
     * 批量保存
     * @param transportSupplierId
     * @param conveyances
     */
    void addBatch(Long transportSupplierId, List<ConveyanceDTO> conveyances);

    /**
     * 根据供应商id删除
     * @param transportSupplierId
     */
    void removeByTransportSupplierId(@NonNull Long transportSupplierId);

    /**
     * 根据运输供应商id获取记录数
     * @param transportSupplierId
     * @return
     */
    int countByTransportSupplierId(@NonNull Long transportSupplierId);

    /**
     * 查询运输工具下拉
     * @param qto
     * @return
     */
    List<SimpleConveyanceVO> select(ConveyanceQTO qto);

}
