package com.tsfyun.scm.vo.system;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SubjectSimpleVO implements Serializable {

    private Long id;
    @ApiModelProperty(value = "公司名称")
    private String name;
    @ApiModelProperty(value = "公司名称(英文)")
    private String nameEn;
    @ApiModelProperty(value = "公司电话")
    private String tel;
    @ApiModelProperty(value = "公司传真")
    private String fax;
    @ApiModelProperty(value = "公司地址")
    private String address;
    @ApiModelProperty(value = "公司地址英文")
    private String addressEn;
}
