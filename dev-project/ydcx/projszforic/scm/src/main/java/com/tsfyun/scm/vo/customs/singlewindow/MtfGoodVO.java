package com.tsfyun.scm.vo.customs.singlewindow;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *
 * @author min.zhang
 * @since 2020-07-15
 */
@Data
@ApiModel(value="MtfGood响应对象", description="响应实体")
public class MtfGoodVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private Long mftId;

    private String billNo;

    @ApiModelProperty(value = "序号")
    private Integer listNo;

    @ApiModelProperty(value = "商品项序号")
    private Integer goodsSeqNo;

    @ApiModelProperty(value = "商品项件数")
    private Integer goodsPackNum;

    @ApiModelProperty(value = "包装种类")
    private String goodsPackTypeCode;

    @ApiModelProperty(value = "包装种类")
    private String goodsPackType;

    @ApiModelProperty(value = "商品项毛重(KG)")
    private BigDecimal goodsGrossWt;

    @ApiModelProperty(value = "商品项简要描述")
    private String goodsBriefDesc;

    @ApiModelProperty(value = "危险品编号")
    private String undgNo;

    @ApiModelProperty(value = "危险品编号")
    private String undgNoName;

    @ApiModelProperty(value = "商品HS编码")
    private String hsCode;

    @ApiModelProperty(value = "商品项描述补充信息")
    private String goodsDetailDesc;


}
