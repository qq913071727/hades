package com.tsfyun.scm.mapper.storage;

import com.tsfyun.scm.entity.storage.Warehouse;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 仓库 Mapper 接口
 * </p>
 *

 * @since 2020-03-31
 */
@Repository
public interface WarehouseMapper extends Mapper<Warehouse> {

}
