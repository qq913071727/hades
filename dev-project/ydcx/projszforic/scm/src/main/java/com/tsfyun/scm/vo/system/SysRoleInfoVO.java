package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysRoleInfoVO implements Serializable {

    private String id;

    private String name;

    private Boolean disabled;

    /**
     * 备注
     */
    private String memo;

    List<SimpleSysMenuVO> menus;



}
