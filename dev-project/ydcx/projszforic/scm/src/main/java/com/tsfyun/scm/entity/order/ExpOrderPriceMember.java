package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2021-09-14
 */
@Data
public class ExpOrderPriceMember implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 波动率
     */

    private BigDecimal fluctuations;

    /**
     * 是否有报关记录
     */

    private Boolean isImp;

    /**
     * 是否新料
     */

    private Boolean isNewGoods;


    private Long orderMemberId;


    private Long orderPriceId;

    /**
     * 计算过程
     */

    private String processLogs;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;


    private String spec;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价(委托)
     */

    private BigDecimal unitPrice;

    /**
     * 总价(委托)
     */

    private BigDecimal totalPrice;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 币制
     */

    private String currencyCode;

    /**
     * 币制
     */

    private String currencyName;


    private Integer rowNo;


}
