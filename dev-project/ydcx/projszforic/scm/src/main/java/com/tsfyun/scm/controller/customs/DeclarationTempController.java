package com.tsfyun.scm.controller.customs;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customs.DeclarationTempDTO;
import com.tsfyun.scm.dto.customs.DeclarationTempQTO;
import com.tsfyun.scm.entity.customs.DeclarationTemp;
import com.tsfyun.scm.service.customs.IDeclarationTempService;
import com.tsfyun.scm.vo.customs.DeclarationTempSimpleVO;
import com.tsfyun.scm.vo.customs.DeclarationTempVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 报关单模板 前端控制器
 * </p>
 *
 *
 * @since 2020-04-24
 */
@RestController
@RequestMapping("/declarationTemp")
public class DeclarationTempController extends BaseController {

    @Autowired
    private IDeclarationTempService declarationTempService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 新增
     * @param dto
     * @return 返回运id
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) DeclarationTempDTO dto) {
        declarationTempService.add(dto);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) DeclarationTempDTO dto) {
        declarationTempService.edit(dto);
        return success();
    }


    /**
     * 详情
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<DeclarationTempVO> detail(@RequestParam(value = "id")Long id) {
        return success(declarationTempService.detail(id));
    }

    /**=
     * 进口默认模板
     * @return
     */
    @GetMapping(value = "impDefault")
    Result<DeclarationTempVO> impDefault() {
        DeclarationTempVO declarationTempVO = null;
        DeclarationTemp declarationTemp = declarationTempService.obtanDefault(BillTypeEnum.IMP);
        if(Objects.nonNull(declarationTemp)){
            declarationTempVO = beanMapper.map(declarationTemp,DeclarationTempVO.class);
        }
        return success(declarationTempVO);
    }

    /**=
     * 出口默认模板
     * @return
     */
    @GetMapping(value = "expDefault")
    Result<DeclarationTempVO> expDefault() {
        DeclarationTempVO declarationTempVO = null;
        DeclarationTemp declarationTemp = declarationTempService.obtanDefault(BillTypeEnum.EXP);
        if(Objects.nonNull(declarationTemp)){
            declarationTempVO = beanMapper.map(declarationTemp,DeclarationTempVO.class);
        }
        return success(declarationTempVO);
    }

    @GetMapping(value = "expDefaultList")
    Result<List<DeclarationTempSimpleVO>> expDefaultList() {
        return success(declarationTempService.obtanDefaultList(BillTypeEnum.EXP));
    }

    @GetMapping(value = "impDefaultList")
    Result<List<DeclarationTempSimpleVO>> impDefaultList() {
        return success(declarationTempService.obtanDefaultList(BillTypeEnum.IMP));
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        declarationTempService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 获取报关单模板下拉（过滤掉禁用的，固定返回前20条，按是否默认，创建时间排序）
     * @return
     */
    @PostMapping("select")
    public Result<List<DeclarationTempSimpleVO>> select(@RequestParam("billType")String billType,@RequestParam(value = "tempName",required = false)String tempName){
        return success(declarationTempService.selectByBillType(billType,tempName));
    }

    /**
     * 分页查询报关单模板
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<DeclarationTempVO>> page(DeclarationTempQTO qto) {
        PageInfo<DeclarationTemp> page = declarationTempService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),DeclarationTempVO.class));
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        declarationTempService.removeById(id);
        return success();
    }

    /**
     * 设置默认
     * @param id
     * @return
     */
    @PostMapping(value = "setDefault")
    Result<Void> setDefault(@RequestParam(value = "id")Long id,@RequestParam("isDefault")Boolean isDefault) {
        declarationTempService.setDefault(id,isDefault);
        return success();
    }


}

