package com.tsfyun.scm.util;

import cn.hutool.core.util.StrUtil;
import com.tsfyun.common.base.enums.WxResponseMessageEventEnum;
import com.tsfyun.scm.dto.support.WxMessageDTO;
import com.tsfyun.scm.vo.support.TextMessageResp;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @Description: 微信公众消息内容填充
 * @CreateDate: Created in 2021/1/11 13:31
 */
@Slf4j
public class WxMessageContentUtil {

    /**
     * 企业介绍填充
     * @return
     */
    public static String wrapIntroduce(){
        StringBuffer sendContentSb = new StringBuffer("");
        sendContentSb.append("xxxx.com是深圳市xxxx有限公司运营的数智化供应链服务平台。我们基于领先于行业的技术实力和运营管理能力，为用户提供进出口行业领先的SAAS产品，并提供安全可靠、高效、低成本的进出口&供应链服务。");
        sendContentSb.append("\n\n");
        sendContentSb.append("xxxx基于大数据及机器人技术和创新性的运营管理架构，引领供应链行业的数智化革新。");
        sendContentSb.append("\n\n");
        sendContentSb.append("xxxx链接一切，让进出口更简单。");
        return sendContentSb.toString();
    }

    /**
     * 联系我们填充
     * @return
     */
    public static String wrapLink() {
        StringBuffer sendContentSb = new StringBuffer("");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n\n");

        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n\n");

        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");
        sendContentSb.append("\n");
        sendContentSb.append("xxxx");

        return sendContentSb.toString();
    }

    public static String wrapFocus(WxMessageDTO weixinMessageDTO,String wxccxAppid) {
        String msgHref = StrUtil.format("<a href='https://www.xxxx.com' data-miniprogram-appid={} data-miniprogram-path='pages/order/add'>点我</a>",wxccxAppid);
        StringBuffer wxContentSb = new StringBuffer("");
        wxContentSb.append("感谢您关注xxxx，我们将竭诚为您服务！");
        wxContentSb.append("下单" + msgHref);
        TextMessageResp textMessage = new TextMessageResp();
        textMessage.setContent(wxContentSb.toString());
        textMessage.setCreateTime(new Date().getTime());
        textMessage.setFromUserName(weixinMessageDTO.getToUserName());
        textMessage.setToUserName(weixinMessageDTO.getFromUserName());
        textMessage.setMsgType(WxResponseMessageEventEnum.TEXT.getCode());
        String respMessage = WxMessageUtil.textMessageToXml(textMessage);
        log.info("响应给微信平台的内容为【{}】",respMessage);
        return respMessage;
    }

}
