package com.tsfyun.scm.entity.user;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


import java.io.Serializable;

/**
 * person
 * @author 
 */
@Data
public class Person  extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 用户 唯一编号，非登录账号
     */
    private String code;

    /**
     * 员工名称
     */
    private String name;


    /**
     * 邮箱
     */
    private String mail;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 头像
     */
    private String head;


    /**
     * 部门id
     */
    private Long departmentId;

    /**
     * 职位
     */
    private String position;

    /**
     * 在职状态
     */
    private Boolean incumbency;

    /**
     * 性别，见枚举SexTypeEnum
     */
    private String gender;

    /**
     * 备注
     */
    private String memo;

    /**
     * 是否需要强制修改密码
     */
    private Boolean isChangedPwd;

    /**
     * 是否已删除
     */
    private Boolean isDelete;

    /**
     * 登录密码
     */
    private String passWord;
    /**
     * 管理端
     */
    private Boolean isManager;
    /**=
     * 客户端
     */
    private Boolean isClient;
    /**
     * 销售端
     */
    private Boolean isSale;
    /**
     * 客户
     */
    private Long customerId;
    /**
     * 微信小程序
     */
    private String wxxcxOpenid;
    /**
     * 微信公众号
     */
    private String wxgzhOpenid;
}