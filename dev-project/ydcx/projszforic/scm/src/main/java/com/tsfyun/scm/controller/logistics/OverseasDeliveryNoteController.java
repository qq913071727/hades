package com.tsfyun.scm.controller.logistics;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteBindOrderMemberDTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteBindQTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteDTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteQTO;
import com.tsfyun.scm.service.logistics.IOverseasDeliveryNoteMemberService;
import com.tsfyun.scm.service.logistics.IOverseasDeliveryNoteService;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNoteMemberStockVO;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNotePlusVO;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNoteVO;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 香港送货单 前端控制器
 * </p>
 *
 *
 * @since 2021-11-09
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/overseasDeliveryNote")
public class OverseasDeliveryNoteController extends BaseController {

    private final IOverseasDeliveryNoteService overseasDeliveryNoteService;

    private final IOverseasDeliveryNoteMemberService overseasDeliveryNoteMemberService;

    /**
     * 出库分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<OverseasDeliveryNoteVO>> pageList(@ModelAttribute OverseasDeliveryNoteQTO qto) {
        PageInfo<OverseasDeliveryNoteVO> page = overseasDeliveryNoteService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<OverseasDeliveryNotePlusVO> detail(@RequestParam(value = "id")Long id) {
        return success(overseasDeliveryNoteService.detail(id));
    }


    /**
     * 选择订单明细派送
     * @param qto
     * @return
     */
    @PostMapping(value = "listBindMembers")
    public Result<List<OverseasDeliveryNoteMemberStockVO>> listBindMembers(@ModelAttribute OverseasDeliveryNoteBindQTO qto) {
        PageInfo<OverseasDeliveryNoteMemberStockVO> pageInfo = overseasDeliveryNoteMemberService.pageList(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 保存境外送货单主单
     * @param dto
     * @return
     */
    @PostMapping(value = "saveMain")
    public Result<Long> saveMain(@RequestBody @Validated OverseasDeliveryNoteDTO dto) {
        return success(overseasDeliveryNoteService.saveMain(dto));
    }

    /**
     * 境外派送单绑定订单明细
     * @param dto
     * @return
     */
    @PostMapping(value = "bindOrderMembers")
    public Result<Void> bindOrderMembers(@RequestBody @Validated OverseasDeliveryNoteBindOrderMemberDTO dto) {
        overseasDeliveryNoteMemberService.bind(dto);
        return success( );
    }

    /**
     * 确认
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirm")
    public Result<Void> confirm(@ModelAttribute @Validated TaskDTO dto) {
        overseasDeliveryNoteService.confirm(dto);
        return success( );
    }

    /**
     * 发车派送
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "delivery")
    public Result<Void> delivery(@ModelAttribute @Validated TaskDTO dto) {
        overseasDeliveryNoteService.delivery(dto);
        return success( );
    }

    /**
     * 派送完成
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "reached")
    public Result<Void> reached(@ModelAttribute @Validated TaskDTO dto) {
        overseasDeliveryNoteService.reached(dto);
        return success( );
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")Long id) {
        overseasDeliveryNoteService.delete(id);
        return success( );
    }

    /**
     * 删除明细集合
     * @param ids
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "removeIds")
    public Result<Void> removeIds(@RequestParam(value = "overseasDeliveryId") Long overseasDeliveryId,@RequestParam(value = "ids")List<Long> ids) {
        overseasDeliveryNoteMemberService.deleteByIds(overseasDeliveryId,ids);
        return success( );
    }

}

