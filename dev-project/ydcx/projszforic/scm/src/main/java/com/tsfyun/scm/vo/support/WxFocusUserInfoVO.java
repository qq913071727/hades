package com.tsfyun.scm.vo.support;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 微信公众号关注用户信息
 * @CreateDate: Created in 2020/12/29 15:23
 */
@Data
public class WxFocusUserInfoVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 *序号
	 */
	private String id;
	/**
	 *openid
	 */
	private String openid;
	/**
	 *昵称
	 */
	private String nickname;
	/**
	 *过滤后昵称
	 */
	private String nicknameTxt;
	/**
	 *备注名称
	 */
	private String bzname;
	/**
	 *用户头像
	 */
	private String headimgurl;
	/**
	 *性别
	 */
	private String sex;
	/**
	 *是否关注:'0':未关注；'1':关注
	 */
	private String subscribe;
	/**
	 *关注时间
	 */
	private String subscribeTime;
	/**
	 *用户关注渠道来源
	 */
	private String subscribeScene;
	/**
	 *手机号
	 */
	private String mobile;
	/**
	 *绑定状态：'0':未绑定；'1':已绑定
	 */
	private String bindStatus;
	/**
	 *绑定时间
	 */
	private Date bindTime;

	/**
	 *省份
	 */
	private String province;
	/**
	 *城市
	 */
	private String city;
	/**
	 *地区
	 */
	private String country;
	/**
	 *二维码扫码场景
	 */
	private String qrScene;
	/**
	 *二维码扫码常见描述
	 */
	private String qrSceneStr;
	/**
	 *用户所在分组id
	 */
	private String groupid;
	/**
	 *用户的语言，简体中文为zh_CN
	 */
	private String language;
	/**
	 *unionid
	 */
	private String unionid;

	/**
	 *公众号原始id
	 */
	private String jwid;

	/**
	 *创建时间
	 */
	private Date createTime;

}

