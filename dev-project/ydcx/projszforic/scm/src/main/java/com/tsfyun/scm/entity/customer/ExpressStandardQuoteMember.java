package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 快递标准报价代理费明细
 * @CreateDate: Created in 2020/10/22 16:33
 */
@Data
public class ExpressStandardQuoteMember implements Serializable {

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 快递标准报价代理费
     */
    private Long quoteId;

    /**
     * 开始重量
     */
    private BigDecimal startWeight;

    /**
     * 结束重量（不含）
     */
    private BigDecimal endWeight;

    /**
     * 基准价
     */
    private BigDecimal basePrice;

    /**
     * 单价/KG
     */
    private BigDecimal price;



}
