package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.FileDTO;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 保存订单前端映射实体
 * @since Created in 2020/4/8 14:32
 */
@ApiModel(value = "订单请求数据")
@Data
public class ImpOrderPlusDTO implements Serializable {

    /**
     * 订单主单
     */
    @NotNull(message = "请填写订单信息")
    @Valid
    @ApiModelProperty(value = "订单")
    private ImpOrderDTO order;

    /**
     * 订单明细
     */
    @ApiModelProperty(value = "订单明细")
    @NotNull(message = "请填写订单明细数据",groups = UpdateGroup.class)
    @Size(max = 1000,message = "订单产品信息最多1000条",groups = AddGroup.class)
    @Size(min = 1,max = 1000,message = "订单产品信息只能为1-1000条",groups = UpdateGroup.class)
    @Valid
    private List<ImpOrderMemberDTO> members;

    /**
     * 物流信息
     */
    @ApiModelProperty(value = "物流信息")
    @NotNull(message = "请填写订单物流数据",groups = UpdateGroup.class)
    @Valid
    private ImpOrderLogisticsDTO logistiscs;

    @ApiModelProperty(value = "临时保存或提交")
    private Boolean submitAudit;

    /**
     * 文件信息
     */
    @ApiModelProperty(value = "文件信息")
    @Valid
    private FileDTO file;

}
