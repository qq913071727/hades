package com.tsfyun.scm.controller.order;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ImpOrderPaySituationQTO;
import com.tsfyun.scm.service.order.IImpOrderPaymentService;
import com.tsfyun.scm.service.order.IImpOrderService;
import com.tsfyun.scm.vo.order.ApplyPaymentPlusVO;
import com.tsfyun.scm.vo.order.ImpOrderPaySituationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**=
 * 订单付汇情况
 */
@RestController
@RequestMapping("/impOrderPayment")
public class ImpOrderPaymentController extends BaseController {


    @Autowired
    private IImpOrderPaymentService impOrderPaymentService;

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ImpOrderPaySituationVO>> list(@ModelAttribute ImpOrderPaySituationQTO qto) {
        PageInfo<ImpOrderPaySituationVO> page = impOrderPaymentService.list(qto);
        return success((int) page.getTotal(),page.getList());
    }

    /**=
     * 初始化付汇申请
     * @param id
     * @return
     */
    @PostMapping(value = "initApply")
    public Result<ApplyPaymentPlusVO> initApply(@RequestParam(value = "id")String id){
        List<Long> ids =  Arrays.asList(id.split(",")).stream().map(Long::valueOf).collect(Collectors.toList());
        return success(impOrderPaymentService.initApply(ids));
    }
}
