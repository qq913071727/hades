package com.tsfyun.scm.vo.customs.singlewindow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 仓单集装箱响应实体
 * </p>
 *
 * @author min.zhang
 * @since 2020-07-15
 */
@Data
@ApiModel(value="MftContainer响应对象", description="仓单集装箱响应实体")
public class MftContainerVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    private String billNo;

    @ApiModelProperty(value = "序号")
    private Integer contaSeqNo;

    @ApiModelProperty(value = "集装箱(器)编号")
    private String contaId;

    @ApiModelProperty(value = "尺寸和类型")
    private String contaSizeTypeCode;

    @ApiModelProperty(value = "尺寸和类型")
    private String contaSizeType;

    @ApiModelProperty(value = "来源代码")
    private String contaSuppIdCode;

    @ApiModelProperty(value = "来源代码")
    private String contaSuppId;

    @ApiModelProperty(value = "重箱或空箱标识")
    private String contaLoadedTypeCode;

    @ApiModelProperty(value = "重箱或空箱标识")
    private String contaLoadedType;


}
