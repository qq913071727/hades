package com.tsfyun.scm.service.order;

import com.tsfyun.scm.dto.order.ImpOrderDTO;
import com.tsfyun.scm.dto.order.ImpOrderLogisticsDTO;
import com.tsfyun.scm.dto.order.ImpOrderMemberDTO;
import com.tsfyun.scm.dto.order.ImpOrderMemberSaveDTO;
import com.tsfyun.scm.entity.base.Currency;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.customer.ImpQuote;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderLogistics;
import com.tsfyun.scm.entity.order.ImpOrderMember;

import java.util.List;
import java.util.Map;

public interface IImpOrderCheckWrapService {


    /**
     * 校验订单币制
     * @param currencyName
     */
    Currency checkWrapCurrency(String currencyName);

    /**
     * 校验订单境外物流
     * @param impOrderLogistics
     * @param paramLogisticsDTO
     */
    void checkWrapOverseasLogistics(ImpOrderLogistics impOrderLogistics, ImpOrderLogisticsDTO paramLogisticsDTO,Boolean submitAudit);

    /**
     * 校验订单国内物流
     * @param impOrderLogistics
     * @param paramLogisticsDTO
     */
    void checkWrapDomesticLogistics(ImpOrderLogistics impOrderLogistics, ImpOrderLogisticsDTO paramLogisticsDTO,Boolean submitAudit);

    /**
     * 校验赋值订单明细
     * @param order
     * @param paramMembers
     * @param submitAudit
     * @return
     */
    ImpOrderMemberSaveDTO checkWrapOrderMember(ImpOrder order, List<ImpOrderMemberDTO> paramMembers, Boolean submitAudit);

    /**
     * 订单主单参数赋值
     * @param impOrder
     * @param orderMembers
     * @param paramOrderDTO
     */
    void wrapOrderValue(ImpOrder impOrder, List<ImpOrderMember> orderMembers,ImpOrderDTO paramOrderDTO,Boolean submitAudit);

}
