package com.tsfyun.scm.dto.wms;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @since Created in 2020/4/24 11:43
 */
@Data
public class ReceivingNoteQTO extends PaginationDto implements Serializable {

    private String customerName;

    private String docNo;

    private String orderNo;

    private String statusId;

    private Long warehouseId;

    //入库方式
    private String deliveryMode;

    private String hkExpressNo;

    private Long supplierId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime receivingDateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime receivingDateEnd;

    public void setReceivingDateEnd(LocalDateTime accountDateEnd) {
        if(accountDateEnd != null){
            this.receivingDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(accountDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }


}
