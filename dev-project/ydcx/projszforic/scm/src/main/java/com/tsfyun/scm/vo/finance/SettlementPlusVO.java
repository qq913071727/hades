package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SettlementPlusVO implements Serializable {

    // 结汇主单
    private SettlementAccountVO settlementAccount;
    // 收款单
    List<OverseasReceivingAccountVO> overseasReceivingAccountList;
}
