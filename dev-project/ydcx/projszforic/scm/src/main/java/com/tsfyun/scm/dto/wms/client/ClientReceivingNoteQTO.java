package com.tsfyun.scm.dto.wms.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @Description: 客户端境外入库查询请求实体
 * @CreateDate: Created in 2020/11/9 15:14
 */
@Data
public class ClientReceivingNoteQTO extends PaginationDto {

    @JsonIgnore
    private Long customerId;

    private String docNo;

    private String orderNo;

    //入库方式
    private String deliveryMode;

    private String hkExpressNo;

    private String keyword;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime receivingDateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime receivingDateEnd;

    public void setReceivingDateEnd(LocalDateTime accountDateEnd) {
        if(accountDateEnd != null){
            this.receivingDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(accountDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
