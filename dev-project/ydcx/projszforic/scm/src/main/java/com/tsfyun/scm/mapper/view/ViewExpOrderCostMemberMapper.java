package com.tsfyun.scm.mapper.view;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.view.ViewExpOrderCostMember;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import com.tsfyun.scm.vo.report.MonthCostVO;
import com.tsfyun.scm.vo.view.ViewExpOrderCostMemberTotalVO;
import com.tsfyun.scm.vo.view.ViewExpOrderCostMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/20 11:00
 */
@Repository
public interface ViewExpOrderCostMemberMapper extends Mapper<ViewExpOrderCostMember> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ViewExpOrderCostMemberVO> orderReceivableList(Map<String,Object> params);

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    ViewExpOrderCostMemberTotalVO orderReceivableTotal(Map<String,Object> params);

    List<MonthCostVO> findMonthCost(@Param(value = "year") String year);
}
