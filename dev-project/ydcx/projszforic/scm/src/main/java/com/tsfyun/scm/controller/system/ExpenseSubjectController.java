package com.tsfyun.scm.controller.system;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.common.base.vo.ExpenseSubjectVO;
import com.tsfyun.scm.dto.system.ExpenseSubjectDTO;
import com.tsfyun.scm.entity.system.ExpenseSubject;
import com.tsfyun.scm.service.system.IExpenseSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  费用会计科目前端控制器
 * </p>
 *
 * @since 2020-03-16
 */
@RestController
@RequestMapping("/expenseSubject")
public class ExpenseSubjectController extends BaseController {

    @Autowired
    private IExpenseSubjectService expenseSubjectService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ExpenseSubjectVO>> pageList(@ModelAttribute ExpenseSubjectDTO dto) {
        PageInfo<ExpenseSubject> page = expenseSubjectService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),ExpenseSubjectVO.class));
    }

    /**=
     * 查询所有数据（传查询参数可以当作下拉框）
     * @param dto
     * @return
     */
    @RequestMapping(value = "allList")
    public Result<List<ExpenseSubjectVO>> allList(@ModelAttribute ExpenseSubjectDTO dto) {
        return success(expenseSubjectService.allList(dto));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@ModelAttribute @Valid ExpenseSubjectDTO dto){
        expenseSubjectService.add(dto);
        return success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) ExpenseSubjectDTO dto){
        expenseSubjectService.edit(dto);
        return success();
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam(value = "id") String id){
        expenseSubjectService.delete(id);
        return success();
    }

    /**
     * 启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")String id,@RequestParam("disabled")Boolean disabled){
        expenseSubjectService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 详情
     */
    @GetMapping("/detail")
    public Result<ExpenseSubjectVO> detail(@RequestParam(value = "id")String id){
        return success(expenseSubjectService.detail(id));
    }

}

