package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 导入出口订单明细
 * @CreateDate: Created in 2021/10/27 09:46
 */
@Data
public class ExpOrderMemberExcel extends BaseRowModel implements Serializable {

    @ExcelProperty(value = "型号", index = 0)
    private String model;

    @ExcelProperty(value = "品名", index = 1)
    private String name;

    @ExcelProperty(value = "品牌", index = 2)
    private String brand;

    @ExcelProperty(value = "物料编号", index = 3)
    private String goodsCode;

    @ExcelProperty(value = "单位", index = 4)
    private String unitName;

    @ExcelProperty(value = "数量", index = 5)
    private String quantity;

    @ExcelProperty(value = "总价", index = 6)
    private String totalPrice;

    @ExcelProperty(value = "箱数", index = 7)
    private String cartonNum;

    @ExcelProperty(value = "净重", index = 8)
    private String netWeight;

    @ExcelProperty(value = "毛重", index = 9)
    private String grossWeight;

    @ExcelProperty(value = "规格描述", index = 10)
    private String spec;
}
