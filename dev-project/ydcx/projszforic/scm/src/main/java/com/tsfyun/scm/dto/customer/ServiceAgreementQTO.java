package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/8 09:39
 */
@Data
public class ServiceAgreementQTO extends PaginationDto implements Serializable {

    private Long customerId;

    private String partyaName;

    private String docNo;

    private Boolean isSignBack;

    private Boolean isEffective;

    /**
     * 日期查询类型
     */
    private String queryDate;

    /**=
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**=
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd){
        if(dateEnd!=null){
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }

}
