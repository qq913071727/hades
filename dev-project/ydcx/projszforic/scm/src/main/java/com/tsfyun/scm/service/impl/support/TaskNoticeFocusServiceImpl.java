package com.tsfyun.scm.service.impl.support;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.support.DomainStatus;
import com.tsfyun.common.base.support.OperationInfo;
import com.tsfyun.scm.dto.support.TaskNoticeFocusDTO;
import com.tsfyun.scm.entity.support.TaskNoticeFocus;
import com.tsfyun.scm.mapper.support.TaskNoticeFocusMapper;
import com.tsfyun.scm.service.support.ITaskNoticeFocusService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.support.ITaskNoticeService;
import com.tsfyun.scm.vo.support.TaskNoticeFocusVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <p>
 * 用户关注任务通知 服务实现类
 * </p>
 *

 * @since 2020-04-09
 */
@Service
public class TaskNoticeFocusServiceImpl extends ServiceImpl<TaskNoticeFocus> implements ITaskNoticeFocusService {

    @Autowired
    private TaskNoticeFocusMapper taskNoticeFocusMapper;

    @Autowired
    private ITaskNoticeService taskNoticeService;

    @Autowired
    private Snowflake snowflake;

    private static final Map<String, OperationInfo> operationMap = DomainStatus.getInstance().getOperationMap();

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void dislikeTaskNotice(TaskNoticeFocusDTO dto) {
        if(CollUtil.isNotEmpty(operationMap)) {
            OperationInfo operationInfo = operationMap.get(dto.getOperationCode());
            Optional.ofNullable(operationInfo).orElseThrow(()->new ServiceException("操作码错误"));
        }
        TaskNoticeFocus update = new TaskNoticeFocus();
        update.setId(snowflake.nextId());
        update.setPersonId(SecurityUtil.getCurrentPersonId());
        update.setPersonName(SecurityUtil.getCurrentPersonName());
        update.setNoReception(Boolean.TRUE);
        update.setOperationCode(dto.getOperationCode());
        taskNoticeFocusMapper.updateTaskNoticeFocus(update);

        //把我的某操作码的任务通知删掉
        taskNoticeService.deleteByOperationCodeAndPersonId(dto.getOperationCode(),SecurityUtil.getCurrentPersonId());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void likeTaskNotice(TaskNoticeFocusDTO dto) {
        if(CollUtil.isNotEmpty(operationMap)) {
            OperationInfo operationInfo = operationMap.get(dto.getOperationCode());
            Optional.ofNullable(operationInfo).orElseThrow(()->new ServiceException("操作码错误"));
        }
        //直接删掉
        TaskNoticeFocus update = new TaskNoticeFocus();
        update.setPersonId(SecurityUtil.getCurrentPersonId());
        update.setOperationCode(dto.getOperationCode());
        taskNoticeFocusMapper.delete(update);
    }

    @Override
    public List<TaskNoticeFocusVO> getMyDislikeTaskNotice() {
        TaskNoticeFocus condition = new TaskNoticeFocus();
        condition.setPersonId(SecurityUtil.getCurrentPersonId());
        condition.setNoReception(Boolean.TRUE);
        List<TaskNoticeFocus> list = super.list(condition);
        List<TaskNoticeFocusVO> datas = Lists.newArrayListWithExpectedSize(list.size());
        list.stream().forEach(k->{
            TaskNoticeFocusVO taskNoticeFocusVO =  new  TaskNoticeFocusVO();
            taskNoticeFocusVO.setOperationCode(k.getOperationCode());
            datas.add(taskNoticeFocusVO);
        });
        return datas;
    }

    @Override
    public List<TaskNoticeFocus> getDislikePersonByOperationCode(String operationCode) {
        TaskNoticeFocus condition = new TaskNoticeFocus();
        condition.setOperationCode(operationCode);
        condition.setNoReception(Boolean.TRUE);
        List<TaskNoticeFocus> list = super.list(condition);
        return list;
    }

    @Override
    public List<String> getDislikeOperationCodes(Long personId) {
        return taskNoticeFocusMapper.getDislikeOperationCodes(personId);
    }

}
