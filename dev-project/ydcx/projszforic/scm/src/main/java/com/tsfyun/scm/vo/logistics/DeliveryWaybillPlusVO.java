package com.tsfyun.scm.vo.logistics;

import com.tsfyun.scm.vo.order.ImpOrderLogisticsVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryWaybillPlusVO implements Serializable {

    private DeliveryWaybillVO delivery;

    private ImpOrderLogisticsVO logistics;

    private List<DeliveryWaybillMemberVO> members;
}
