package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;


/**
 * <p>
 * 进口订单详情响应实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrder详情响应对象", description="进口订单详情响应实体")
public class ImpOrderDetailVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "订单编号")
    private String docNo;
    @ApiModelProperty(value = "客户单号")
    private String clientNo;
    @ApiModelProperty(value = "客户")
    private Long customerId;
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    @ApiModelProperty(value = "客户供应商")
    private Long supplierId;
    @ApiModelProperty(value = "客户供应商名称")
    private String supplierName;
    @ApiModelProperty(value = "状态编码")
    private String statusId;
    @ApiModelProperty(value = "业务类型")
    private String businessType;
    @ApiModelProperty(value = "币制")
    private String currencyId;
    private String currencyName;
    @ApiModelProperty(value = "协议报价")
    private Long impQuoteId;
    @ApiModelProperty(value = "成交方式")
    private String transactionMode;
    @ApiModelProperty(value = "报关类型")
    private String declareType;
    @ApiModelProperty(value = "备注")
    private String memo;
    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;
    @ApiModelProperty(value = "外贸合同买方")
    private Long subjectId;
    private String subjectName;
    @ApiModelProperty(value = "外贸合同卖方")
    private Long subjectOverseasId;
    private String subjectOverseasName;
    @ApiModelProperty(value = "进口结算汇率")
    private BigDecimal impRate;
    private String transCosts;//运费
    private String insuranceCosts;//保费
    private String miscCosts;//杂费
    private Long declarationTempId;//报关模板

    private BigDecimal goodsRate;//货款汇率

    @ApiModelProperty(value = "订单日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime orderDate;

    @ApiModelProperty(value = "业务类型")
    private String businessTypeName;
    public String getBusinessTypeName(){
        BusinessTypeEnum businessTypeEnum = BusinessTypeEnum.of(getBusinessType());
        return Objects.nonNull(businessTypeEnum)?businessTypeEnum.getName():"";
    }
    @ApiModelProperty(value = "报关方式")
    private String declareTypeName;
    public String getDeclareTypeName(){
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(getDeclareType());
        return Objects.nonNull(declareTypeEnum)?declareTypeEnum.getName():"";
    }
    @ApiModelProperty(value = "成交方式")
    private String transactionModeName;
    public String getTransactionModeName(){
        TransactionModeEnum transactionModeEnum = TransactionModeEnum.of(getTransactionMode());
        return Objects.nonNull(transactionModeEnum)?transactionModeEnum.getName():"";
    }
    @ApiModelProperty(value = "协议报价描述")
    private String impQuoteDesc;
}
