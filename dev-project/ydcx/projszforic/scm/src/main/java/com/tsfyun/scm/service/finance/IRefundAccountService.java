package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.RefundAccountDTO;
import com.tsfyun.scm.dto.finance.RefundAccountQTO;
import com.tsfyun.scm.dto.finance.RefundConfirmBankDTO;
import com.tsfyun.scm.entity.finance.RefundAccount;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.RefundAccountVO;

import java.util.Map;

/**
 * <p>
 * 退款申请 服务类
 * </p>
 *
 *
 * @since 2020-11-27
 */
public interface IRefundAccountService extends IService<RefundAccount> {

    /**
     * 分页列表（兼容管理端和客户端）
     * @param qto
     * @return
     */
    PageInfo<RefundAccountVO> pageList(RefundAccountQTO qto);

    /**
     * 申请退款
     * @param dto
     */
    void apply(RefundAccountDTO dto);

    /**
     * 客户端申请
     * @param dto
     */
    void clientApply(RefundAccountDTO dto);

    /**
     * 管理端申请
     * @param dto
     */
    void manageApply(RefundAccountDTO dto);

    /**
     * 退款申请商务审核
     * @param taskDTO
     */
    void approve(TaskDTO taskDTO);

    /**
     * 退款申请确认付款行
     * @param dto
     */
    void confirmBank(RefundConfirmBankDTO dto);

    /**
     * 确认上传水单
     * @param taskDTO
     */
    void bankReceipt(TaskDTO taskDTO);

    /**
     * 退款单详情
     * @param id
     * @param operation
     * @return
     */
    RefundAccountVO detail(Long id,String operation);

    /**
     * 状态集合对应的数量
     * @return
     */
    Map<String,Long> getClientRefundNum();

    /**
     * 作废退款单
     * @param dto
     */
    void toVoid(TaskDTO dto);

    /**
     * 删除退款单以及附属数据
     * @param id
     */
    void removeAllById(Long id);

}
