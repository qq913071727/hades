package com.tsfyun.scm.mapper.view;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.dto.view.ViewOrderCostMemberQTO;
import com.tsfyun.scm.entity.view.ViewOrderCostMember;
import com.tsfyun.scm.vo.report.MonthCostVO;
import com.tsfyun.scm.vo.view.ViewOrderCostMemberTotalVO;
import com.tsfyun.scm.vo.view.ViewOrderCostMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 *
 * @since 2021-05-28
 */
@Repository
public interface ViewOrderCostMemberMapper extends Mapper<ViewOrderCostMember> {

    @DataScope(tableAlias = "a",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ViewOrderCostMemberVO> orderReceivableList(Map<String,Object> params);
    @DataScope(tableAlias = "a",customerTableAlias = "c",addCustomerNameQuery = true)
    ViewOrderCostMemberTotalVO orderReceivableTotal(Map<String,Object> params);

    List<MonthCostVO> findMonthCost(@Param(value = "year") String year);
}
