package com.tsfyun.scm.entity.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 出口订单
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Data
public class ExpOrder extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 订单编号
     */

    private String docNo;

    /**
     * 客户单号
     */

    private String clientNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 协议ID
     */
    private Long agreementId;

    /**
     * 境外采购商
     */

    private Long supplierId;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 订单日期-由后端写入
     */

    private LocalDateTime orderDate;

    /**
     * 实际通关日期-由报关单反写
     */

    private Date clearanceDate;

    /**
     * 生成报关单确定出口
     */

    private Boolean isExp;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 币制
     */

    private String currencyName;

    /**
     * 运输方式代码
     */

    private String cusTrafMode;

    /**
     * 运输方式名称
     */

    private String cusTrafModeName;

    /**
     * 成交方式
     */

    private String transactionMode;

    /**
     * 运抵国(地区)编码
     */

    private String cusTradeCountry;

    /**
     * 运抵国(地区)名称
     */

    private String cusTradeCountryName;

    /**
     * 贸易国别(地区)
     */
    private String cusTradeNationCode;
    private String cusTradeNationCodeName;

    /**
     * 最终目的国(地区)
     */
    private String destinationCountry;
    private String destinationCountryName;
    /**
     * 指运港代码
     */

    private String distinatePort;

    /**
     * 指运港名称
     */

    private String distinatePortName;

    /**
     * 境内货源地代码
     */

    private String districtCode;

    /**
     * 境内货源地名称
     */

    private String districtCodeName;

    /**
     * 报关类型
     */
    private String declareType;

    /**
     * 备注
     */

    private String memo;

    /**
     * 委托金额
     */

    private BigDecimal totalPrice;

    /**
     * 报关金额
     */

    private BigDecimal decTotalPrice;

    /**=
     * 海关汇率
     */
    private BigDecimal customsRate;

    /**
     * 转美金汇率(海关汇率)
     */

    private BigDecimal usdRate;

    /**
     * 总箱数
     */

    private Integer totalCartonNum;

    /**
     * 明细条数
     */
    private Integer totalMember;

    /**
     * 总净重
     */

    private BigDecimal totalNetWeight;

    /**
     * 总毛重
     */

    private BigDecimal totalCrossWeight;

    /**
     * 销售人员-计算提成用
     */

    private Long salePersonId;


    private Long subjectId;//外贸合同卖方

    private String subjectOverseasType;//外贸合同买方类型

    private Long subjectOverseasId;//外贸合同买方

    private String subjectOverseasName;//外贸合同买方
    /**=
     * 报关单号
     */
    private String declarationNo;
    /**
     * 跨境运输单号
     */
    private String transportNo;
    private Long declarationTempId;//报关模板
    private String transCosts;//运费
    private String insuranceCosts;//保费
    private String miscCosts;//杂费

    private String purchaseContractNo;//采购合同号
    private BigDecimal purchaseCny;// 采购合同总价
    private BigDecimal adjustmentVal;//票差调整金额
    private BigDecimal actualPurchaseCny;// 实际采购合同总价
    private BigDecimal differenceVal;// 生成的票差金额
    private BigDecimal amountCollected;// 境外收款认领金额
    private Boolean isSettleAccount;// 是否结汇完成
    private BigDecimal settleAccountCny;// 结汇人民币金额
    private BigDecimal settleAccountUsd;// 结汇外币金额

    private String drawbackNo;// 退税申请单号
    private BigDecimal actualDrawbackValue;// 应退税金额
    private BigDecimal alreadyDrawbackValue;// 已退税金额

}
