package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.lang.Snowflake;
import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.scm.entity.finance.ImpSalesContractMember;
import com.tsfyun.scm.mapper.finance.ImpSalesContractMemberMapper;
import com.tsfyun.scm.service.finance.IImpSalesContractMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.finance.ImpSalesContractMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 销售合同明细 服务实现类
 * </p>
 *

 * @since 2020-05-19
 */
@Service
public class ImpSalesContractMemberServiceImpl extends ServiceImpl<ImpSalesContractMember> implements IImpSalesContractMemberService {

    @Resource
    private Snowflake snowflake;

    @Autowired
    private ImpSalesContractMemberMapper impSalesContractMemberMapper;

    @Override
    public List<ImpSalesContractMemberVO> getMembersBySalesContractId(Long impSalesContractId) {
        ImpSalesContractMember condition = new ImpSalesContractMember();
        condition.setImpSalesContractId(impSalesContractId);
        return beanMapper.mapAsList(super.list(condition),ImpSalesContractMemberVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteBySalesContractId(Long impSalesContractId) {
        impSalesContractMemberMapper.deleteBySalesContractId(impSalesContractId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveMembers(List<ImpSalesContractMember> members, ImpSalesContract impSalesContract) {
        members.stream().forEach(member->{
            member.setId(snowflake.nextId());
            member.setImpSalesContractId(impSalesContract.getId());
        });
        super.savaBatch(members);
    }
}
