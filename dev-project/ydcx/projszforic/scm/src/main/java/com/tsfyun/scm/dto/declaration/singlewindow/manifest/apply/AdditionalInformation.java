package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 补充信息数据
 * @since Created in 2020/4/22 11:20
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalInformation", propOrder = {
        "content",
})
public class AdditionalInformation {

    @XmlElement(name = "Content")
    protected String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
