package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.customer.CustomerBankDTO;
import com.tsfyun.scm.entity.customer.CustomerBank;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.CustomerBankVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-03
 */
public interface ICustomerBankService extends IService<CustomerBank> {


    /**
     * 新增
     * @param dto
     */
    void add(CustomerBankDTO dto);

    /**
     * 修改
     * @param dto
     */
    void edit(CustomerBankDTO dto);


    /**
     * 禁用启用
     * @param id
     */
    void activeDisable(Long id);

    /**
     * 设置默认
     * @param id
     */
    void setDefault(Long id);

    /**
     * 取消以前设置的默认客户银行
     * @param customerId
     */
    void handleOldDefault(Long customerId);

    /**
     * 获取客户银行信息
     * @param customerId
     * @return
     */
    List<CustomerBankVO> select(Long customerId);

    /**
     * 分页查询客户银行信息
     * @param dto
     * @return
     */
    PageInfo<CustomerBankVO> pageList(CustomerBankDTO dto);

    /**
     * 获取客户银行详细信息
     * @param id
     * @return
     */
    CustomerBankVO detail(Long id);

    /**=
     * 批量保存
     */
    void saveList(Long customerId,List<CustomerBankDTO> customerDeliveryInfoDTOS);

    /**
     * 根据客户删除
     * @param customerId
     */
    void removeByCustomerId(Long customerId);


    /**
     * 根据客户id获取客户银行列表
     * @param customerId
     * @return
     */
    List<CustomerBank> list(Long customerId);

}
