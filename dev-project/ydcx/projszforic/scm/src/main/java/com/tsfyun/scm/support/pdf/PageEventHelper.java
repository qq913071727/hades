package com.tsfyun.scm.support.pdf;


import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @Description:
 * @since Created in 2020/4/29 15:41
 */
public class PageEventHelper extends PdfPageEventHelper {

    public PageEventHelper() {

    }

    public void onOpenDocument(PdfWriter writer, Document document) {
        super.onOpenDocument(writer,document);
    }

    /**
     *
     * 关闭每页的时候，可以做页眉页脚处理或者其他处理
     *
     */
    public void onEndPage(PdfWriter writer, Document document) {
        super.onEndPage(writer,document);
    }

    /**
     * 关闭文档时，可以做页眉页脚处理或者其他处理
     * @param writer
     * @param document
     */
    public void onCloseDocument(PdfWriter writer, Document document) {
        super.onCloseDocument(writer,document);
    }



}
