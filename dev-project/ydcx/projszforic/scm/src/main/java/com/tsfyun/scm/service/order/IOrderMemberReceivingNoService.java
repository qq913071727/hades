package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.OrderMemberReceivingNo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.OrderMemberReceivingNoVO;

import java.util.List;

/**
 * <p>
 * 绑定入库单辅助表 服务类
 * </p>
 *
 *
 * @since 2020-05-06
 */
public interface IOrderMemberReceivingNoService extends IService<OrderMemberReceivingNo> {

    List<OrderMemberReceivingNoVO> getByMemberId(Long orderMemberId);

    /**=
     * 根据订单明细删除
     * @param memberId
     */
    void removeByOrderMemberId(Long memberId);
    /**=
     * 根据订单明细删除
     * @param memberIds
     */
    void removeByOrderMemberIds(List<Long> memberIds);

    /**=
     * 根据订单查询预绑定信息
     * @param orderId
     * @return
     */
    List<OrderMemberReceivingNo> findByOrderId(Long orderId);
}
