package com.tsfyun.scm.vo.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class SimplePersonInfo implements Serializable {

    /**
     * 员工id
     */
    private Long id;

    /**
     * 员工编码
     */
    private String code;

    /**
     * 员工名称
     */
    private String name;

}
