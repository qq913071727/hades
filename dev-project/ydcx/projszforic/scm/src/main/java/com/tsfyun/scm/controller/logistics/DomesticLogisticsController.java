package com.tsfyun.scm.controller.logistics;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.logistics.DomesticLogisticsDTO;
import com.tsfyun.scm.service.logistics.IDomesticLogisticsService;
import com.tsfyun.scm.vo.logistics.DomesticLogisticsVO;
import com.tsfyun.scm.vo.logistics.SimpleDomesticLogisticsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 国内物流 前端控制器
 * </p>
 *
 * @since 2020-03-25
 */
@RestController
@RequestMapping("/domesticLogistics")
public class DomesticLogisticsController extends BaseController {

    @Autowired
    private IDomesticLogisticsService domesticLogisticsService;

    /**
     * 新增
     * @param dto
     */
    @PostMapping(value = "add")
    Result<Void> add(@RequestBody @Validated(AddGroup.class) DomesticLogisticsDTO dto) {
        domesticLogisticsService.add(dto);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody  @Validated(UpdateGroup.class) DomesticLogisticsDTO dto) {
        domesticLogisticsService.edit(dto);
        return success();
    }


    /**
     * 详情信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<DomesticLogisticsVO> detail(@RequestParam(value = "id")Long id) {
        return success(domesticLogisticsService.detail(id));
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        domesticLogisticsService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 获取下拉（过滤掉禁用的）
     * @return
     */
    @GetMapping("select")
    public Result<List<SimpleDomesticLogisticsVO>> select( ){
        return success(domesticLogisticsService.select( ));
    }


    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<DomesticLogisticsVO>> list(DomesticLogisticsDTO qto) {
        PageInfo<DomesticLogisticsVO> page = domesticLogisticsService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        domesticLogisticsService.delete(id);
        return success();
    }

}

