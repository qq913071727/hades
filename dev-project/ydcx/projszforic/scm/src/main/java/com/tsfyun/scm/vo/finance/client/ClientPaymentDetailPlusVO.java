package com.tsfyun.scm.vo.finance.client;

import com.tsfyun.scm.vo.finance.PaymentAccountMemberVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/4 11:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientPaymentDetailPlusVO implements Serializable {

    /**
     * 付款单主单
     */
    private ClientPaymentDetailVO payment;

    /**
     * 付款单明细
     */
    private List<PaymentAccountMemberVO> members;

}
