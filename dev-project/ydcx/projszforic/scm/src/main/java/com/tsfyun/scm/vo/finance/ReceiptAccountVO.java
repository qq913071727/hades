package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ReceiptAccountStatusEnum;
import com.tsfyun.common.base.enums.finance.ReceivingModeEnum;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 收款单响应实体
 * </p>
 *

 * @since 2020-04-15
 */
@Data
@ApiModel(value="ReceiptAccount响应对象", description="收款单响应实体")
public class ReceiptAccountVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
    @ApiModelProperty(value = "收款时间")
    private LocalDateTime accountDate;

    @ApiModelProperty(value = "收款行")
    private String receivingBank;

    @ApiModelProperty(value = "收款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "锁定金额")
    private BigDecimal lockValue;

    @ApiModelProperty(value = "核销金额")
    private BigDecimal writeValue;

    @ApiModelProperty(value = "币制编码")
    private String currencyId;

    @ApiModelProperty(value = "币制名称")
    private String currencyName;

    @ApiModelProperty(value = "客户id")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "付款人")
    private String payer;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "收款方式")
    private String receivingMode;

    @ApiModelProperty(value = "汇票编号")
    private String draftNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String statusDesc;

    private String receivingModeDesc;

    private String createBy;

    public String getReceivingModeDesc() {
        ReceivingModeEnum receivingModeEnum = ReceivingModeEnum.of(receivingMode);
        return Objects.nonNull(receivingModeEnum) ? receivingModeEnum.getName() : null;
    }

    public String getStatusDesc() {
        ReceiptAccountStatusEnum receiptAccountStatusEnum = ReceiptAccountStatusEnum.of(statusId);
        return Objects.nonNull(receiptAccountStatusEnum) ? receiptAccountStatusEnum.getName() : null;
    }

    public String getCreateBy() {
        return StringUtils.isNotEmpty(createBy) && createBy.contains("/") ? createBy.split("/")[1] : null;
    }

}
