package com.tsfyun.scm.vo.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/20 11:20
 */
@Data
public class ViewExpOrderCostMemberVO implements Serializable {

    private Long id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 委托金额
     */
    private BigDecimal totalPrice;

    /**
     * 订单日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date orderDate;

    /**
     * 账期日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date periodDate;

    /**
     * 客户id
     */
    private Long customerId;

    private String customerName;

    /**
     * 应收金额
     */
    private BigDecimal receAmount;

    /**
     * 已收金额
     */
    private BigDecimal acceAmount;

    /**
     * 币制
     */
    private String currencyName;

    /**
     * 销售
     */
    private String salePersonName;

}
