package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/11 14:47
 */
@Data
public class OverseasDeliveryNoteBindOrderMemberDTO implements Serializable {

    @NotNull(message = "境外派送单id不能为空")
    private Long overseasDeliveryNoteId;

    @Valid
    @NotNull(message = "请选择派送订单明细数据")
    @Size(min = 1,message = "请至少选择一条派送订单明细数据")
    private List<BindOrderMemberDTO> members;

    @Data
    @NoArgsConstructor
    public static class BindOrderMemberDTO {

        @NotNull(message = "订单明细ID不能为空")
        @ApiModelProperty(value = "订单明细ID")
        private Long id;

        @NotNull(message = "派送数量不能为空",groups = UpdateGroup.class)
        @Digits(integer = 10, fraction = 2, message = "派送数量整数位不能超过10位，小数位不能超过2位")
        @DecimalMin(value = "0.01",message = "派送数量必须大于0")
        @ApiModelProperty(value = "派送数量")
        private BigDecimal quantity;

        @Min(value = 0,message = "件数不能小于0")
        @ApiModelProperty(value = "件数")
        private Integer cartonNum;

    }


}
