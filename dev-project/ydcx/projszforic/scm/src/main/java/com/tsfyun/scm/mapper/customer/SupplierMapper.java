package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.SimpleSupplierVO;
import com.tsfyun.scm.vo.customer.SupplierVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-12
 */
@Repository
public interface SupplierMapper extends Mapper<Supplier> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<SupplierVO> list(Map<String,Object> params);

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = false)
    List<SimpleSupplierVO> vague(Map<String,Object> params);

    List<SimpleSupplierVO> clientVague(@Param(value = "customerId")Long customerId,@Param(value = "name")String name);

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = false)
    Supplier findByCustomerNameAndSupplierName(Map<String,Object> params);

    Supplier findByCustomerIdAndSupplierName(@Param(value = "customerId")Long customerId,@Param(value = "name")String name);

}
