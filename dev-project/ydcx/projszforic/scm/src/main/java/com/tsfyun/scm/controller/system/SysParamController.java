package com.tsfyun.scm.controller.system;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.system.SysParamDTO;
import com.tsfyun.scm.entity.system.SysParam;
import com.tsfyun.scm.service.system.ISysParamService;
import com.tsfyun.scm.vo.system.SysParamVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  业务参数前端控制器
 * </p>
 *
 * @since 2020-04-15
 */
@RestController
@RequestMapping("/sysParam")
public class SysParamController extends BaseController {

    @Autowired
    private ISysParamService sysParamService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<SysParamVO>> pageList(@ModelAttribute SysParamDTO dto) {
        PageInfo<SysParam> page = sysParamService.pageList(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),SysParamVO.class));
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) SysParamDTO dto){
        sysParamService.update(dto);
        return success();
    }

    /**
     * 启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")String id, @RequestParam("disabled")Boolean disabled){
        sysParamService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 详细信息
     * @return
     */
    @GetMapping("detail")
    public Result<SysParamVO> detail(@RequestParam(value = "id")String id) {
        return success(sysParamService.getById(id));
    }


}

