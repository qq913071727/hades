package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.WriteOffOrderCostDTO;
import com.tsfyun.common.base.vo.declare.RecordEnterprisesVO;
import com.tsfyun.scm.client.fallback.DeclareServerClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "declare-manage",fallbackFactory = DeclareServerClientFallbackFactory.class)
@Component
public interface DeclareServerClient {

    /**
     * 取消费用核销
     * @param costs
     * @return
     */
    @PostMapping("trustOrderCost/cancelwriteOffByCosts")
    Result<Void> cancelwriteOffByCosts(@RequestBody @Validated List<WriteOffOrderCostDTO> costs);

    /**
     * 根据客户ID和企业海关编码查询企业备案信息
     * @param customerId
     * @param customsCode
     * @return
     */
    @PostMapping(value = "recordEnterprises/getEnterprise")
    Result<RecordEnterprisesVO> getEnterprise(@RequestParam(value = "customerId")Long customerId,
                                                     @RequestParam(value = "customsCode")String customsCode);
}
