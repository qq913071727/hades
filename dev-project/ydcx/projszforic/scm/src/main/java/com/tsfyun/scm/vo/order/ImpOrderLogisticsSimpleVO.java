package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @Description: 客户最近订单收货地址信息
 * @CreateDate: Created in 2020/5/20 19:59
 */
@Data
public class ImpOrderLogisticsSimpleVO implements Serializable {

    /**
     * 收货公司
     */

    private String deliveryCompanyName;

    /**
     * 收货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 收货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 收货地址
     */

    private String deliveryAddress;

}
