package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/28 16:34
 */
@Data
public class ImportExpOrderCostExcel extends BaseRowModel implements Serializable {

    @NotEmptyTrim(message = "订单号不能为空")
    @LengthTrim(max = 20,message = "订单号最大长度不能超过20位")
    @ExcelProperty(value = "订单号", index = 0)
    private String expOrderNo;

    @NotEmptyTrim(message = "费用科目不能为空")
    @ExcelProperty(value = "费用科目", index = 1)
    private String expenseSubjectName;

    @NotEmptyTrim(message = "应收金额不能为空")
    @ExcelProperty(value = "应收金额", index = 2)
    @Digits(integer = 8, fraction = 2, message = "应收金额整数位不能超过8位，小数位不能超过2位")
    private String costAmount;

    @Pattern(regexp = "退税款中扣除|客户单独支付",message = "支付方式错误")
    @NotEmptyTrim(message = "请选择支付方式")
    @ExcelProperty(value = "支付方式", index = 3)
    private String collectionSource;

    @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
    @ExcelProperty(value = "备注", index = 4)
    private String memo;

}
