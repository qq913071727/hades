package com.tsfyun.scm.controller.core;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.CheckTaskDTO;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.service.common.ICommonService;
import com.tsfyun.scm.service.support.ITaskNoticeContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@RequestMapping("/task")
public class TaskController extends BaseController {

    @Autowired
    private ICommonService commonService;
    @Autowired
    private ITaskNoticeContentService taskNoticeContentService;

    /**
     * 登记单据状态变更
     * @param dto
     * @return
     */
    @PostMapping(value = "execute")
    public Result<Void> execute(@ModelAttribute @Valid TaskDTO dto){
        commonService.changeDocumentStatus(dto);
        return success();
    }

    /**
     * 校验单据是否能进行此操作
     * @param dto
     * @return
     */
    @PostMapping(value = "check")
    public Result<Void> check(@ModelAttribute @Valid CheckTaskDTO dto){
        commonService.checkDocumentStatus(dto);
        return success();
    }

    /**
     * 删除任务
     * @param dto
     * @return
     */
    @PostMapping(value = "deleteTaskNotice")
    public Result<Void> deleteTaskNotice(@ModelAttribute @Valid CheckTaskDTO dto){
        try{
            taskNoticeContentService.deleteTaskNotice(dto.getDocumentClass(),dto.getDocumentId(),dto.getOperation());
        }catch (Exception e){

        }
        return success();
    }

}
