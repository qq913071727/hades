package com.tsfyun.scm.dto.logistics;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 送货单明细请求实体
 * </p>
 *

 * @since 2020-05-25
 */
@Data
@ApiModel(value="DeliveryWaybillMember请求对象", description="送货单明细请求实体")
public class DeliveryWaybillMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long deliveryNoteId;

   private Long orderMemberId;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;


}
