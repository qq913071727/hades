package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class ClientImpOrderQTO extends PaginationDto {

    //客户ID
    @ApiModelProperty(value = "客户ID")
    private Long customerId;

    //订单状态
    @ApiModelProperty(value = "订单状态")
    private String orderStatusId;

    //开票状态
    @ApiModelProperty(value = "开票状态")
    private String invoiceStatusId;

    //付汇状态
    @ApiModelProperty(value = "付汇状态")
    private String paymentStatusId;

    //订单号
    @ApiModelProperty(value = "订单号")
    private String orderNo;

    //国内送货方式
    @ApiModelProperty(value = "国内送货方式")
    private String receivingMode;

    //关键字搜索
    @ApiModelProperty(value = "关键字搜索（订单号/供应商名称/商品信息）")
    private String keyword;

    /**
     * 下单开始时间
     */
    @ApiModelProperty(value = "下单开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDateStart;

    /**
     * 下单结束时间
     */
    @ApiModelProperty(value = "下单结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDateEnd;

    public void setOrderDateEnd(LocalDateTime orderDateEnd) {
        if(orderDateEnd != null){
            this.orderDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(orderDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
