package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 退款申请请求实体
 * </p>
 *
 *
 * @since 2020-11-27
 */
@Data
@ApiModel(value="RefundAccount请求对象", description="退款申请请求实体")
public class RefundAccountDTO implements Serializable {

   private static final long serialVersionUID=1L;

   /**
    * 客户（以防管理端也可以触发）
    */
   private Long customerId;

   @NotNull(message = "申请金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "申请金额整数位不能超过8位，小数位不能超过2位")
   @DecimalMin(value = "0.01",message = "申请金额不能小于等于0")
   @ApiModelProperty(value = "申请金额")
   private BigDecimal accountValue;

   @NotEmptyTrim(message = "请填写收款银行名称")
   @LengthTrim(max = 50,message = "收款银行名称最大长度不能超过50位")
   @ApiModelProperty(value = "收款银行名称")
   private String payeeBankName;

   @NotEmptyTrim(message = "请填写收款银行账号")
   @LengthTrim(max = 50,message = "收款银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "收款银行账号")
   private String payeeBankAccountNo;

   @NotEmptyTrim(message = "请填写收款银行地址")
   @LengthTrim(max = 255,message = "收款银行地址最大长度不能超过255位")
   @ApiModelProperty(value = "收款银行地址")
   private String payeeBankAddress;

   @NotEmptyTrim(message = "请填写申请退款原因")
   @LengthTrim(max = 255,message = "申请退款原因最大长度不能超过255位")
   @ApiModelProperty(value = "申请退款原因")
   private String memo;


}
