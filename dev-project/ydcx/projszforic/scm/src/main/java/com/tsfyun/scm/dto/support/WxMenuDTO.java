package com.tsfyun.scm.dto.support;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 微信菜单
 * @CreateDate: Created in 2020/12/31 16:31
 */
@Data
public class WxMenuDTO implements Serializable {

    /**
     * 一级菜单
     */
    @Valid
    @Size(min = 1,max = 3,message = "最少添加一个一级菜单，最多只能添加三个一级菜单")
    private List<WxMenuItemDTO> button;
}
