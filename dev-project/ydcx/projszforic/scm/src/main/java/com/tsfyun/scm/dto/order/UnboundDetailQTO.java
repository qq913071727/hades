package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

/**=
 * 查询未绑入库的订单明细
 */
@Data
public class UnboundDetailQTO extends PaginationDto {

    private Long customerId;//客户ID
    private String orderNo;//订单号
    private Long memberId;//订单明细ID
}
