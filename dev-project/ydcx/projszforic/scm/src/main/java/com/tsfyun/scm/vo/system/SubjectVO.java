package com.tsfyun.scm.vo.system;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.common.base.annotation.LengthTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-03
 */
@Data
@ApiModel(value="Subject响应对象", description="响应实体")
public class SubjectVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "公司编码")
    private String code;

    @ApiModelProperty(value = "公司名称")
    private String name;

    @ApiModelProperty(value = "公司名称(英文)")
    private String nameEn;

    @ApiModelProperty(value = "统一社会信用代码(18位)")
    private String socialNo;

    @ApiModelProperty(value = "税务登记证")
    private String taxpayerNo;

    @ApiModelProperty(value = "海关注册编码")
    private String customsCode;

    @ApiModelProperty(value = "检验检疫编码")
    private String ciqNo;

    @ApiModelProperty(value = "公司法人")
    private String legalPerson;

    @ApiModelProperty(value = "公司电话")
    private String tel;

    @ApiModelProperty(value = "公司传真")
    private String fax;

    @ApiModelProperty(value = "企业邮箱")
    private String mail;

    @ApiModelProperty(value = "公司联系地址")
    private String address;

    @ApiModelProperty(value = "公司联系地址英文")
    private String addressEn;

    @ApiModelProperty(value = "公司注册地址")
    private String regAddress;

    @ApiModelProperty(value = "浏览器标题")
    private String browserTitle;

    @ApiModelProperty(value = "企业logo")
    private String logo;

    @ApiModelProperty(value = "登录页面背景")
    private String loginBg;

    @ApiModelProperty(value = "电子章")
    private String chapter;

    @JsonIgnore
    private String createBy;

    /**
     * 创建时间
     */
    @JsonIgnore
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @JsonIgnore
    private String updateBy;

    /**
     * 修改时间
     */
    @JsonIgnore
    private LocalDateTime dateUpdated;

    //租户版本
    private String tenantOpenVersion;
}
