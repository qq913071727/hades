package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.order.ImpOrderPriceQTO;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderPrice;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ImpOrderPricePlusVo;
import com.tsfyun.scm.vo.order.ImpOrderPriceVO;

import java.util.List;

/**
 * <p>
 * 订单审价 服务类
 * </p>
 *
 *
 * @since 2020-04-16
 */
public interface IImpOrderPriceService extends IService<ImpOrderPrice> {

    //计算订单审价
    void calculatingPrice(ImpOrder order);
    void calculatingPrice(ImpOrder order,Boolean isForce);

    //订单分页查询
    PageInfo<ImpOrderPriceVO> list(ImpOrderPriceQTO qto);

    ImpOrderPricePlusVo detail(Long id);
    ImpOrderPricePlusVo detail(Long id,String operation);

    //审价
    void examine(TaskDTO dto);

    /**
     * 根据订单删除审价
     * @param orderId
     */
    void removeByOrderId(Long orderId);
}
