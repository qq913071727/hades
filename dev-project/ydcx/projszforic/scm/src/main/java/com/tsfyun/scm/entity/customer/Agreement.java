package com.tsfyun.scm.entity.customer;

import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * <p>
 * 协议报价单
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Data
public class Agreement extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 协议编号
     */

    private String docNo;

    /**
     * 业务类型
     */

    private String businessType;

    /**=
     * 报关类型
     */
    private String declareType;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 甲方
     */

    private String partyaName;

    /**
     * 甲方社会统一信用代码
     */

    private String partyaSocialNo;

    /**
     * 甲方法人
     */

    private String partyaLegalPerson;

    /**
     * 甲方电话
     */

    private String partyaTel;
    private String partyaAccountNo;
    private String partyaLinkPerson;
    private String partyaMail;

    /**
     * 甲方传真
     */

    private String partyaFax;

    /**
     * 甲方地址
     */

    private String partyaAddress;
    // 注册地址
    private String partybRegAddress;

    /**
     * 乙方
     */

    private String partybName;

    /**
     * 乙方统一社会信用代码
     */

    private String partybSocialNo;

    /**
     * 乙方法人
     */

    private String partybLegalPerson;

    /**
     * 乙方电话
     */

    private String partybTel;

    /**
     * 乙方传真
     */

    private String partybFax;

    /**
     * 乙方地址
     */

    private String partybAddress;

    /**
     * 签约日期
     */

    private LocalDateTime signingDate;

    /**
     * 生效日期
     */

    private LocalDateTime effectDate;

    /**
     * 失效日期
     */

    private LocalDateTime invalidDate;

    /**
     * 实际失效日期 
     */

    private LocalDateTime actualInvalidDate;

    /**
     * 自动延期/年
     */

    private Integer delayYear;

    /**
     * 协议正本是否签回
     */

    private Boolean isSignBack;

    /**
     * 签回确定人
     */

    private String signBackPerson;

    /**
     * 备注
     */

    private String memo;

    /**
     * 主要进口产品
     */
    private String mainProduct;

    /**
     * 报价描述
     */

    private String quoteDescribe;

    @ApiModelProperty(value = "版本")
    private String version;

    private Long afileId;// 框架协议文件

    private Long qfileId;// 报价单文件

}
