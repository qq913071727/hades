package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

@Data
public class RoleDTO extends PaginationDto implements Serializable {

    /**
     * 角色编码
     */
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**=
     * 禁用状态
     */
    private Boolean disabled;

}
