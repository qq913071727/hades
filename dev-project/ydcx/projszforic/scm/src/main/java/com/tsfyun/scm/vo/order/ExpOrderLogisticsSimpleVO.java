package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

@Data
public class ExpOrderLogisticsSimpleVO implements Serializable {

    /**
     * 收货公司
     */

    private String deliveryCompanyName;

    /**
     * 收货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 收货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 收货地址
     */

    private String deliveryAddress;

}
