package com.tsfyun.scm.dto.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class CreateContractMainVO implements Serializable {

    @ApiModelProperty(value = "客户ID")
    private Long customerId;
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    @ApiModelProperty(value = "订单ID")
    private Long orderId;
    @ApiModelProperty(value = "订单编号")
    private String orderNo;
    @ApiModelProperty(value = "签定日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signingDate;
    @ApiModelProperty(value = "签定地点")
    private String signingPlace;
    @ApiModelProperty(value = "买方")
    private String buyerName;
    @ApiModelProperty(value = "买方代表")
    private String buyerDeputy;
    @ApiModelProperty(value = "买方电话")
    private String buyerTel;
    @ApiModelProperty(value = "买方开户行")
    private String buyerBank;
    @ApiModelProperty(value = "银行帐号")
    private String buyerBankNo;
    @ApiModelProperty(value = "银行地址")
    private String buyerBankAddress;
    @ApiModelProperty(value = "卖方")
    private String sellerName;
    @ApiModelProperty(value = "卖方代表")
    private String sellerDeputy;
    @ApiModelProperty(value = "卖方电话")
    private String sellerTel;
    @ApiModelProperty(value = "卖方开户行")
    private String sellerBank;
    @ApiModelProperty(value = "银行帐号")
    private String sellerBankNo;
    @ApiModelProperty(value = "银行地址")
    private String sellerBankAddress;
    @ApiModelProperty(value = "是否结汇")
    private Boolean isSettleAccount;
    @ApiModelProperty(value = "结汇人民币金额")
    private BigDecimal settleAccountCny;
    @ApiModelProperty(value = "结汇说明")
    private String settleAccountInfo;
    @ApiModelProperty(value = "币制")
    private String currencyId;
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "汇率")
    private BigDecimal exchangeRate;
    @ApiModelProperty(value = "客户总票差")
    private BigDecimal totalDifferenceVal;
    @ApiModelProperty(value = "本次调整票差")
    private BigDecimal differenceVal;
    @ApiModelProperty(value = "代理费")
    private BigDecimal agentFee;
    @ApiModelProperty(value = "代垫费")
    private BigDecimal matFee;
    @ApiModelProperty(value = "采购总价")
    private BigDecimal totalPrice;

}
