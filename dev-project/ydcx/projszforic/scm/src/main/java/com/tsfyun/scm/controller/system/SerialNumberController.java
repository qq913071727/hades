package com.tsfyun.scm.controller.system;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.SerialNumberTypeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.system.SerialNumberDTO;
import com.tsfyun.scm.service.system.ISerialNumberService;
import com.tsfyun.scm.vo.system.SerialNumberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2020-03-16
 */
@RestController
@RequestMapping("/serialNumber")
public class SerialNumberController extends BaseController {

    @Autowired
    private ISerialNumberService serialNumberService;
    //规则列表
    @PostMapping(value = "list")
    public Result<List<SerialNumberVO>> list(){
        List<SerialNumberVO> serialNumberVOList = serialNumberService.allList();
        return success(serialNumberVOList);
    }
    //规则详情
    @GetMapping(value = "detail")
    public Result<SerialNumberVO> list(@RequestParam(name = "id")String id){
        return success(serialNumberService.detail(id));
    }
    //规则修改
    @PostMapping(value = "edit")
    public Result<Void> edit(@ModelAttribute @Valid SerialNumberDTO serialNumberDTO){
        serialNumberService.edit(serialNumberDTO);
        return success();
    }
    //生成单据编号
    @PostMapping(value = "generateDocNo")
    public Result<String> generateDocNo(@RequestParam(name = "type")String type){
        SerialNumberTypeEnum serialNumberTypeEnum = SerialNumberTypeEnum.of(type);
        TsfPreconditions.checkArgument(Objects.nonNull(serialNumberTypeEnum),new ServiceException("单据类型不存在"));
        return success(serialNumberService.generateDocNo(serialNumberTypeEnum));
    }

    //生成单据编号（内部使用）
    @PostMapping(value = "innerGenerateDocNo")
    public Result<String> innerGenerateDocNo(@RequestParam(name = "type")String type){
        SerialNumberTypeEnum serialNumberTypeEnum = SerialNumberTypeEnum.of(type);
        TsfPreconditions.checkArgument(Objects.nonNull(serialNumberTypeEnum),new ServiceException("单据类型不存在"));
        return success(serialNumberService.generateDocNo(serialNumberTypeEnum));
    }

}

