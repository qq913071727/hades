package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-12
 */
@Data
@ApiModel(value="SupplierTakeInfo请求对象", description="请求实体")
public class SupplierTakeInfoDTO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "供应商")
    private Long supplierId;

    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @NotEmptyTrim(message = "提货公司不能为空")
    @LengthTrim(max = 200,message = "提货公司最大长度不能超过200位")
    @ApiModelProperty(value = "提货公司")
    private String companyName;

    @NotEmptyTrim(message = "提货联系人不能为空")
    @LengthTrim(max = 50,message = "提货联系人最大长度不能超过50位")
    @ApiModelProperty(value = "联系人")
    private String linkPerson;

    @NotEmptyTrim(message = "提货联系电话不能为空")
    @LengthTrim(max = 50,message = "提货联系电话最大长度不能超过50位")
    @ApiModelProperty(value = "联系电话")
    private String linkTel;

    @NotEmptyTrim(message = "提货的省份不能为空")
    @LengthTrim(max = 50,message = "提货省份名称最大长度不能超过50位")
    @ApiModelProperty(value = "省")
    private String provinceName;

    @NotEmptyTrim(message = "提货的市不能为空")
    @LengthTrim(max = 50,message = "提货市名称最大长度不能超过50位")
    @ApiModelProperty(value = "市")
    private String cityName;

    @NotEmptyTrim(message = "提货的区不能为空")
    @LengthTrim(max = 50,message = "提货区名称最大长度不能超过50位")
    @ApiModelProperty(value = "区")
    private String areaName;

    @LengthTrim(max = 255,message = "提货详细地址名称最大长度不能超过50位")
    @ApiModelProperty(value = "详细地址")
    private String address;

    @NotNull(message = "提货禁用标识不能为空")
    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @NotNull(message = "提货是否默认不能为空")
    @ApiModelProperty(value = "默认地址")
    private Boolean isDefault;


}
