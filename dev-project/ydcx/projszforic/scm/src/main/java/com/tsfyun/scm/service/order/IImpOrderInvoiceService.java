package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.order.ImpOrderInvoiceQTO;
import com.tsfyun.scm.vo.order.ImpOrderInvoiceVO;

import java.util.List;

public interface IImpOrderInvoiceService {

    PageInfo<ImpOrderInvoiceVO> list(ImpOrderInvoiceQTO qto);

    List<ImpOrderInvoiceVO> idsList(List<String> ids);
}
