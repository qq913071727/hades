package com.tsfyun.scm.vo.logistics;

import lombok.Data;

import java.io.Serializable;

/**
 * 简单运输工具下拉
 */
@Data
public class SimpleConveyanceVO implements Serializable {

    private Long id;

    private String conveyanceNo;

    private String conveyanceNo2;

    private String driver;

    private String driverTel;

}
