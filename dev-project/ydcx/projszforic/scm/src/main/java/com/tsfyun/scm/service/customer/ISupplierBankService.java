package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.customer.SupplierBankDTO;
import com.tsfyun.scm.dto.customer.SupplierBankQTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierBankQTO;
import com.tsfyun.scm.entity.customer.SupplierBank;
import com.tsfyun.scm.vo.customer.SupplierBankVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-12
 */
public interface ISupplierBankService extends IService<SupplierBank> {

    /**
     * 批量保存供应商银行信息
     * @param supplierId
     * @param banks
     */
    public void addBatch(Long supplierId, List<SupplierBankDTO> banks);

    /**
     * 根据供应商id获取所有的供应商银行信息
     * @param supplierId
     * @return
     */
    List<SupplierBank> list(Long supplierId);

    /**
     * 根据供应商id获取所有的供应商银行信息
     * @param supplierId
     * @return
     */
    List<SupplierBankVO> listBySupplierId(Long supplierId);

    /**
     * 根据供应商id删除银行信息
     * @param supplierId
     */
    void removeBySupplierId(Long supplierId);

    /**
     * 根据供应商获取供应商银行下拉
     * @param supplierId
     * @return
     */
    List<SupplierBankVO> select(Long supplierId);

    /**=
     * 模糊查询
     * @param customerId
     * @param supplierName
     * @param bankName
     * @return
     */
    List<SupplierBankVO> vague(Long customerId,String supplierName,String bankName);
    SupplierBankVO getSupplierBankInfo(Long customerId,String supplierName,String bankName);

    /**=
     * 精确查询
     * @param customerId
     * @param supplierName
     * @param bankName
     * @return
     */
    SupplierBankVO accurate(Long customerId,String supplierName,String bankName);



    /**
     * 根据id获取供应商银行详情信息
     * @param id
     * @return
     */
    SupplierBankVO detail(Long id);

    /**=
     * 修改禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 简单新增供应商银行信息
     * @param dto
     * @return
     */
    Long add(SupplierBankDTO dto);
    Long clientAdd(SupplierBankDTO dto);

    /**
     * 简单修改供应商银行信息
     * @param dto
     */
    void edit(SupplierBankDTO dto);
    void clientEdit(SupplierBankDTO dto);

    /**
     * 删除供应商
     * @param id
     */
    void remove(Long id);

    /**
     * 根据供应商id获取供应商银行记录数
     * @param supplierId
     * @return
     */
    int countBySupplierId(Long supplierId);

    /**
     * 带权限根据客户名称或者供应商名称分页查询
     * @param qto
     * @return
     */
    PageInfo<SupplierBankVO> pageList(SupplierBankQTO qto);

    /**=
     * 供应商默认银行
     * @param supplierId
     * @return
     */
    SupplierBank defSupplierBank(Long supplierId);

    /**
     * 客户端供应商银行
     * @param qto
     * @return
     */
    PageInfo<SupplierBank> clientPage(ClientSupplierBankQTO qto);

    /**
     * 客户端删除
     * @param id
     */
    void clientRemove(Long id);

    /**
     * 设置默认
     * @param id
     */
    void setDefault(Long id);


}
