package com.tsfyun.scm.service.user;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.user.LogLoginDTO;
import com.tsfyun.scm.entity.user.LogLogin;

public interface ILogLoginService extends IService<LogLogin> {


    /**
     * 根据员工信息哦记录登录日志
     * @param logLogin
     */
    void save(LogLoginDTO logLogin);

    /**
     * 获取用户最近登录信息
     * @return
     */
    LogLogin personLastLogin(Long personId,String device);

}
