package com.tsfyun.scm.mapper.customs;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.customs.WaybillBindOrderUpdateDecDTO;
import com.tsfyun.scm.entity.customs.Declaration;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customs.ContractPlusVO;
import com.tsfyun.scm.vo.customs.ContractVO;
import com.tsfyun.scm.vo.customs.DeclarationListVO;
import com.tsfyun.scm.vo.customs.DeclarationVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 报关单 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface DeclarationMapper extends Mapper<Declaration> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<DeclarationListVO> list(Map<String,Object> params);

    DeclarationVO detail(@Param(value = "id") Long id);

    ContractVO contractData(@Param(value = "id") Long id);

    int updateByWaybill(WaybillBindOrderUpdateDecDTO dto);

    void resetByCrossBorderWaybill(@Param(value = "id") Long id);

    Declaration findManifestById(@Param(value = "manifestId") Long manifestId);

    void updateManifestReceiptData(@Param(value = "manifestId") Long manifestId,@Param(value = "manifestStatusId") String manifestStatusId,
                                   @Param(value = "manifestSwStatus") String manifestSwStatus);

    void updateDeclarationNo(List<Declaration> declarations);
}
