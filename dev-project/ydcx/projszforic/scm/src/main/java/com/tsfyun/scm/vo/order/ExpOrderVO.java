package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.domain.ExpOrderStatusEnum;
import com.tsfyun.common.base.enums.exp.ExpTransactionModeEnum;
import com.tsfyun.common.base.enums.exp.SettlementTypeEnum;
import com.tsfyun.common.base.enums.exp.TaxRefundEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 出口订单响应实体
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Data
@ApiModel(value="ExpOrder响应对象", description="出口订单响应实体")
public class ExpOrderVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "订单编号")
    private String docNo;

    @ApiModelProperty(value = "客户单号")
    private String clientNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "协议")
    private Long agreementId;
    @ApiModelProperty(value = "协议描述")
    private String agreementDesc;

    private String customerName;

    @ApiModelProperty(value = "境外收货方")
    private Long supplierId;

    private String supplierName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单日期-由后端写入")
    private Date orderDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "实际通关日期-由报关单反写")
    private Date clearanceDate;

    @ApiModelProperty(value = "生成报关单确定出口")
    private Boolean isExp;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "出境关别代码")
    private String iePort;

    @ApiModelProperty(value = "出境关别名称")
    private String iePortName;

    @ApiModelProperty(value = "运输方式代码")
    private String cusTrafMode;

    @ApiModelProperty(value = "运输方式名称")
    private String cusTrafModeName;

    @ApiModelProperty(value = "成交方式")
    private String transactionMode;

    @ApiModelProperty(value = "运抵国(地区)编码")
    private String cusTradeCountry;

    @ApiModelProperty(value = "运抵国(地区)名称")
    private String cusTradeCountryName;

    @ApiModelProperty(value = "指运港代码")
    private String distinatePort;

    @ApiModelProperty(value = "指运港名称")
    private String distinatePortName;

    @ApiModelProperty(value = "境内货源地代码")
    private String districtCode;

    @ApiModelProperty(value = "境内货源地名称")
    private String districtCodeName;

    @ApiModelProperty(value = "贸易国(地区)")
    private String cusTradeNationCode;
    @ApiModelProperty(value = "贸易国(地区)")
    private String cusTradeNationCodeName;

    @ApiModelProperty(value = "最终目的国")
    private String destinationCountry;
    @ApiModelProperty(value = "最终目的国")
    private String destinationCountryName;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "报关金额")
    private BigDecimal decTotalPrice;

    @ApiModelProperty(value = "总箱数")
    private Integer totalCartonNum;

    @ApiModelProperty(value = "明细条数")
    private Integer totalMember;

    @ApiModelProperty(value = "总净重")
    private BigDecimal totalNetWeight;

    @ApiModelProperty(value = "总毛重")
    private BigDecimal totalCrossWeight;

    @ApiModelProperty(value = "采购合同号")
    private String purchaseContractNo;

    @ApiModelProperty(value = "外贸合同卖方")
    private Long subjectId;
    private String subjectName;

    @ApiModelProperty(value = "外贸合买同方")
    private String subjectOverseasType;
    private Long subjectOverseasId;
    private String subjectOverseasName;

    private Long declarationTempId;//报关模板
    private String transCosts;//运费
    private String insuranceCosts;//保费
    private String miscCosts;//杂费

    private String declareType;

    private String statusDesc;

    private String settlementTypeDesc;

    private String taxRefundDesc;

    private String transactionModeDesc;

    private String declareTypeDesc;

    public String getStatusDesc() {
        ExpOrderStatusEnum expOrderStatusEnum = ExpOrderStatusEnum.of(statusId);
        return Optional.ofNullable(expOrderStatusEnum).map(ExpOrderStatusEnum::getName).orElse("");
    }


    public String getTransactionModeDesc(){
        ExpTransactionModeEnum expTransactionModeEnum = ExpTransactionModeEnum.of(transactionMode);
        return Optional.ofNullable(expTransactionModeEnum).map(ExpTransactionModeEnum::getName).orElse("");
    }

    public String getDeclareTypeDesc(){
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(declareType);
        return Optional.ofNullable(declareTypeEnum).map(DeclareTypeEnum::getName).orElse("");
    }

}
