//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DecHead" type="{http://www.chinaport.gov.cn/dec}DecHeadType"/>
 *         &lt;element name="DecLists">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecList" type="{http://www.chinaport.gov.cn/dec}DecListItemType" maxOccurs="50" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecContainers">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Container" type="{http://www.chinaport.gov.cn/dec}DecContainerType" maxOccurs="500" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecLicenseDocus" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LicenseDocu" type="{http://www.chinaport.gov.cn/dec}DecLicenseType" maxOccurs="7" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecRequestCerts" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecRequestCert" type="{http://www.chinaport.gov.cn/dec}DecRequestCertType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecOtherPacks" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecOtherPack" type="{http://www.chinaport.gov.cn/dec}DecOtherPackType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecCopLimits" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecCopLimit" type="{http://www.chinaport.gov.cn/dec}DecCopLimitType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecUsers" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecUser" type="{http://www.chinaport.gov.cn/dec}DecUserType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecMarkLobs" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecMarkLob" type="{http://www.chinaport.gov.cn/dec}DecMarkLobType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecFreeTxt" type="{http://www.chinaport.gov.cn/dec}DecFreeTxtType" minOccurs="0"/>
 *         &lt;element name="EdocRealation" type="{http://www.chinaport.gov.cn/dec}Edoc_RealationType" maxOccurs="10" minOccurs="0"/>
 *         &lt;element name="EcoRelation" type="{http://www.chinaport.gov.cn/dec}ECO_RealationType" maxOccurs="20" minOccurs="0"/>
 *         &lt;element name="DecCopPromises" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DecCopPromise" type="{http://www.chinaport.gov.cn/dec}DecCopPromiseType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DecRoyaltyFee" type="{http://www.chinaport.gov.cn/dec}DecRoyaltyFeeType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}float" fixed="4.6" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "decHead",
    "decLists",
    "decContainers",
    "decLicenseDocus",
    "decRequestCerts",
    "decOtherPacks",
    "decCopLimits",
    "decUsers",
    "decMarkLobs",
    "decFreeTxt",
    "edocRealation",
    "ecoRelation",
    "decCopPromises",
    "decRoyaltyFee",
    "decSign"
})
@XmlRootElement(name = "DecMessage")
public class DecMessage {

    @XmlElement(name = "DecHead", required = true)
    protected DecHeadType decHead;
    @XmlElement(name = "DecLists", required = true)
    protected DecLists decLists;
    @XmlElement(name = "DecContainers", required = true)
    protected DecContainers decContainers;
    @XmlElement(name = "DecLicenseDocus")
    protected DecLicenseDocus decLicenseDocus;
    @XmlElement(name = "DecRequestCerts")
    protected DecRequestCerts decRequestCerts;
    @XmlElement(name = "DecOtherPacks")
    protected DecOtherPacks decOtherPacks;
    @XmlElement(name = "DecCopLimits")
    protected DecCopLimits decCopLimits;
    @XmlElement(name = "DecUsers")
    protected DecUsers decUsers;
    @XmlElement(name = "DecMarkLobs")
    protected DecMarkLobs decMarkLobs;
    @XmlElement(name = "DecFreeTxt")
    protected DecFreeTxtType decFreeTxt;
    @XmlElement(name = "EdocRealation")
    protected List<EdocRealationType> edocRealation;
    @XmlElement(name = "EcoRelation")
    protected List<ECORealationType> ecoRelation;
    @XmlElement(name = "DecCopPromises")
    protected DecCopPromises decCopPromises;
    @XmlElement(name = "DecRoyaltyFee")
    protected DecRoyaltyFeeType decRoyaltyFee;
    @XmlAttribute(name = "Version")
    protected Float version;
    @XmlElement(name = "DecSign")
    protected DecSign decSign;

    /**
     * 获取decHead属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecHeadType }
     *     
     */
    public DecHeadType getDecHead() {
        return decHead;
    }

    /**
     * 设置decHead属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecHeadType }
     *     
     */
    public void setDecHead(DecHeadType value) {
        this.decHead = value;
    }

    /**
     * 获取decLists属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecLists }
     *     
     */
    public DecLists getDecLists() {
        return decLists;
    }

    /**
     * 设置decLists属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecLists }
     *     
     */
    public void setDecLists(DecLists value) {
        this.decLists = value;
    }

    /**
     * 获取decContainers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecContainers }
     *     
     */
    public DecContainers getDecContainers() {
        return decContainers;
    }

    /**
     * 设置decContainers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecContainers }
     *     
     */
    public void setDecContainers(DecContainers value) {
        this.decContainers = value;
    }

    /**
     * 获取decLicenseDocus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecLicenseDocus }
     *     
     */
    public DecLicenseDocus getDecLicenseDocus() {
        return decLicenseDocus;
    }

    /**
     * 设置decLicenseDocus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecLicenseDocus }
     *     
     */
    public void setDecLicenseDocus(DecLicenseDocus value) {
        this.decLicenseDocus = value;
    }

    /**
     * 获取decRequestCerts属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecRequestCerts }
     *     
     */
    public DecRequestCerts getDecRequestCerts() {
        return decRequestCerts;
    }

    /**
     * 设置decRequestCerts属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecRequestCerts }
     *     
     */
    public void setDecRequestCerts(DecRequestCerts value) {
        this.decRequestCerts = value;
    }

    /**
     * 获取decOtherPacks属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecOtherPacks }
     *     
     */
    public DecOtherPacks getDecOtherPacks() {
        return decOtherPacks;
    }

    /**
     * 设置decOtherPacks属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecOtherPacks }
     *     
     */
    public void setDecOtherPacks(DecOtherPacks value) {
        this.decOtherPacks = value;
    }

    /**
     * 获取decCopLimits属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecCopLimits }
     *     
     */
    public DecCopLimits getDecCopLimits() {
        return decCopLimits;
    }

    /**
     * 设置decCopLimits属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecCopLimits }
     *     
     */
    public void setDecCopLimits(DecCopLimits value) {
        this.decCopLimits = value;
    }

    /**
     * 获取decUsers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecUsers }
     *     
     */
    public DecUsers getDecUsers() {
        return decUsers;
    }

    /**
     * 设置decUsers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecUsers }
     *     
     */
    public void setDecUsers(DecUsers value) {
        this.decUsers = value;
    }

    /**
     * 获取decMarkLobs属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecMarkLobs }
     *     
     */
    public DecMarkLobs getDecMarkLobs() {
        return decMarkLobs;
    }

    /**
     * 设置decMarkLobs属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecMarkLobs }
     *     
     */
    public void setDecMarkLobs(DecMarkLobs value) {
        this.decMarkLobs = value;
    }

    /**
     * 获取decFreeTxt属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecFreeTxtType }
     *     
     */
    public DecFreeTxtType getDecFreeTxt() {
        return decFreeTxt;
    }


    public DecSign getDecSign() {
        return decSign;
    }

    public void setDecSign(DecSign decSign) {
        this.decSign = decSign;
    }

    /**
     * 设置decFreeTxt属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecFreeTxtType }
     *     
     */
    public void setDecFreeTxt(DecFreeTxtType value) {
        this.decFreeTxt = value;
    }

    /**
     * Gets the value of the edocRealation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the edocRealation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEdocRealation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EdocRealationType }
     * 
     * 
     */
    public List<EdocRealationType> getEdocRealation() {
        if (edocRealation == null) {
            edocRealation = new ArrayList<EdocRealationType>();
        }
        return this.edocRealation;
    }

    /**
     * Gets the value of the ecoRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ecoRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEcoRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ECORealationType }
     * 
     * 
     */
    public List<ECORealationType> getEcoRelation() {
        if (ecoRelation == null) {
            ecoRelation = new ArrayList<ECORealationType>();
        }
        return this.ecoRelation;
    }

    /**
     * 获取decCopPromises属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecCopPromises }
     *     
     */
    public DecCopPromises getDecCopPromises() {
        return decCopPromises;
    }

    /**
     * 设置decCopPromises属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecCopPromises }
     *     
     */
    public void setDecCopPromises(DecCopPromises value) {
        this.decCopPromises = value;
    }

    /**
     * 获取decRoyaltyFee属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DecRoyaltyFeeType }
     *     
     */
    public DecRoyaltyFeeType getDecRoyaltyFee() {
        return decRoyaltyFee;
    }

    /**
     * 设置decRoyaltyFee属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DecRoyaltyFeeType }
     *     
     */
    public void setDecRoyaltyFee(DecRoyaltyFeeType value) {
        this.decRoyaltyFee = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public float getVersion() {
        if (version == null) {
            return  4.6F;
        } else {
            return version;
        }
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setVersion(Float value) {
        this.version = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Container" type="{http://www.chinaport.gov.cn/dec}DecContainerType" maxOccurs="500" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "container"
    })
    public static class DecContainers {

        @XmlElement(name = "Container")
        protected List<DecContainerType> container;

        /**
         * Gets the value of the container property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the container property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContainer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecContainerType }
         * 
         * 
         */
        public List<DecContainerType> getContainer() {
            if (container == null) {
                container = new ArrayList<DecContainerType>();
            }
            return this.container;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecCopLimit" type="{http://www.chinaport.gov.cn/dec}DecCopLimitType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decCopLimit"
    })
    public static class DecCopLimits {

        @XmlElement(name = "DecCopLimit")
        protected List<DecCopLimitType> decCopLimit;

        /**
         * Gets the value of the decCopLimit property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decCopLimit property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecCopLimit().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecCopLimitType }
         * 
         * 
         */
        public List<DecCopLimitType> getDecCopLimit() {
            if (decCopLimit == null) {
                decCopLimit = new ArrayList<DecCopLimitType>();
            }
            return this.decCopLimit;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecCopPromise" type="{http://www.chinaport.gov.cn/dec}DecCopPromiseType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decCopPromise"
    })
    public static class DecCopPromises {

        @XmlElement(name = "DecCopPromise")
        protected List<DecCopPromiseType> decCopPromise;

        /**
         * Gets the value of the decCopPromise property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decCopPromise property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecCopPromise().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecCopPromiseType }
         * 
         * 
         */
        public List<DecCopPromiseType> getDecCopPromise() {
            if (decCopPromise == null) {
                decCopPromise = new ArrayList<DecCopPromiseType>();
            }
            return this.decCopPromise;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LicenseDocu" type="{http://www.chinaport.gov.cn/dec}DecLicenseType" maxOccurs="7" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "licenseDocu"
    })
    public static class DecLicenseDocus {

        @XmlElement(name = "LicenseDocu")
        protected List<DecLicenseType> licenseDocu;

        /**
         * Gets the value of the licenseDocu property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the licenseDocu property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLicenseDocu().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecLicenseType }
         * 
         * 
         */
        public List<DecLicenseType> getLicenseDocu() {
            if (licenseDocu == null) {
                licenseDocu = new ArrayList<DecLicenseType>();
            }
            return this.licenseDocu;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecList" type="{http://www.chinaport.gov.cn/dec}DecListItemType" maxOccurs="50" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decList"
    })
    public static class DecLists {

        @XmlElement(name = "DecList")
        protected List<DecListItemType> decList;

        /**
         * Gets the value of the decList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecListItemType }
         * 
         * 
         */
        public List<DecListItemType> getDecList() {
            if (decList == null) {
                decList = new ArrayList<DecListItemType>();
            }
            return this.decList;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecMarkLob" type="{http://www.chinaport.gov.cn/dec}DecMarkLobType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decMarkLob"
    })
    public static class DecMarkLobs {

        @XmlElement(name = "DecMarkLob")
        protected List<DecMarkLobType> decMarkLob;

        /**
         * Gets the value of the decMarkLob property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decMarkLob property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecMarkLob().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecMarkLobType }
         * 
         * 
         */
        public List<DecMarkLobType> getDecMarkLob() {
            if (decMarkLob == null) {
                decMarkLob = new ArrayList<DecMarkLobType>();
            }
            return this.decMarkLob;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecOtherPack" type="{http://www.chinaport.gov.cn/dec}DecOtherPackType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decOtherPack"
    })
    public static class DecOtherPacks {

        @XmlElement(name = "DecOtherPack")
        protected List<DecOtherPackType> decOtherPack;

        /**
         * Gets the value of the decOtherPack property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decOtherPack property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecOtherPack().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecOtherPackType }
         * 
         * 
         */
        public List<DecOtherPackType> getDecOtherPack() {
            if (decOtherPack == null) {
                decOtherPack = new ArrayList<DecOtherPackType>();
            }
            return this.decOtherPack;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecRequestCert" type="{http://www.chinaport.gov.cn/dec}DecRequestCertType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decRequestCert"
    })
    public static class DecRequestCerts {

        @XmlElement(name = "DecRequestCert")
        protected List<DecRequestCertType> decRequestCert;

        /**
         * Gets the value of the decRequestCert property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decRequestCert property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecRequestCert().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecRequestCertType }
         * 
         * 
         */
        public List<DecRequestCertType> getDecRequestCert() {
            if (decRequestCert == null) {
                decRequestCert = new ArrayList<DecRequestCertType>();
            }
            return this.decRequestCert;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DecUser" type="{http://www.chinaport.gov.cn/dec}DecUserType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "decUser"
    })
    public static class DecUsers {

        @XmlElement(name = "DecUser")
        protected List<DecUserType> decUser;

        /**
         * Gets the value of the decUser property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the decUser property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDecUser().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DecUserType }
         * 
         * 
         */
        public List<DecUserType> getDecUser() {
            if (decUser == null) {
                decUser = new ArrayList<DecUserType>();
            }
            return this.decUser;
        }

    }

}
