package com.tsfyun.scm.entity.materiel;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 *  物料表
 * </p>
 *
 *
 * @since 2020-03-22
 */
@Data
public class Materiel implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 海关编码
     */

    private String hsCode;

    /**
     * ciq编码
     */

    private String ciqNo;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 申报要素
     */

    private String elements;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 归类备注
     */

    private String memo;

    /**
     * 来源
     */

    private String source;

    /**
     * 来源标识
     */

    private String sourceMark;

    /**
     * 匹配方式(待，未，标，智)
     */

    private String mate;

    /**
     * 状态编码
     */

    private String statusId;


    /**
     * 归类人员
     */

    private String classifyPerson;

    /**
     * 最后归类时间
     */

    private LocalDateTime classifyTime;

    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;


    private Boolean isSmp;//是否涉证(战略物资许可证)
    private Boolean isCapp;//需要3C证书(当检验检疫为L时)
    private Boolean isCappNo;//需要3C目录鉴定
    private String cappNo;//3C证书编号
}
