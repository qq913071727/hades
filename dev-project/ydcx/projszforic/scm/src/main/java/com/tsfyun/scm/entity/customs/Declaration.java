package com.tsfyun.scm.entity.customs;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 报关单
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
public class Declaration extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 申报单位组织机构代码
     */

    private String agentScc;

    /**
     * 申报单位海关编号
     */

    private String agentCode;

    /**
     * 申报单位名称
     */

    private String agentName;

    private String ownerScc;//(进口)消费使用单位/(出口)生产销售单位(18位社会信用代码)
    private String ownerCode;//(进口)消费使用单位/(出口)生产销售单位(10位海关代码)
    private String ownerName;//(进口)消费使用单位/(出口)生产销售单位(名称)

    /**
     * 提运单号（仓单）
     */

    private String billNo;

    /**
     * 单据类型-进出口
     */

    private String billType;

    /**
     * 订单号
     */

    private Long orderId;

    /**
     * 订单编号
     */

    private String orderDocNo;

    /**
     * 航次号-六联单号
     */

    private String cusVoyageNo;

    /**
     * 集装箱号
     */

    private String containerNo;

    /**
     * 合同号
     */

    private String contrNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     *  申报日期
     */

    private LocalDateTime decDate;

    /**
     * 报关单号
     */

    private String declarationNo;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 申报地海关
     */

    private String customMasterCode;

    /**
     * 申报地海关
     */

    private String customMasterName;

    /**
     * 货物存放地
     */

    private String goodsPlace;

    /**
     * 启运港
     */

    private String destPortCode;

    /**
     * 启运港
     */

    private String destPortName;

    /**
     * 境内目的地
     */

    private String districtCode;

    /**
     * 境内目的地
     */

    private String districtName;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 入境口岸
     */

    private String ciqEntyPortCode;

    /**
     * 入境口岸
     */

    private String ciqEntyPortName;

    /**
     * 进境关别
     */

    private String iePortCode;

    /**
     * 进境关别
     */

    private String iePortName;

    /**
     * 进出口日期
     */

    private LocalDateTime importDate;

    /**
     * 保费
     */

    private String insuranceCosts;

    /**
     * 许可证号
     */

    private String licenseNo;

    /**
     * 车牌
     */

    private String licensePlate;

    /**
     * 备注
     */

    private String memo;

    /**
     * 杂费
     */

    private String miscCosts;

    /**
     * 包装种类
     */

    private String wrapTypeCode;

    /**
     * 包装种类
     */

    private String wrapTypeName;

    /**
     * 备案号
     */

    private String recordNo;

    /**
     * 统一编号(单一窗口)
     */

    private String cusCiqNo;

    /**
     * 标记唛码
     */

    private String markNo;

    /**
     * 经停港/指运港
     */

    private String distinatePortCode;

    /**
     * 经停港/指运港
     */

    private String distinatePortName;

    /**
     * 征免性质
     */

    private String cutModeCode;

    /**
     * 征免性质
     */

    private String cutModeName;

    /**
     * 件数
     */

    private Integer packNo;

    /**
     * 净重(KG)
     */

    private BigDecimal netWt;

    /**
     * 毛重(KG)
     */

    private BigDecimal grossWt;

    /**
     * 贸易国别(地区)	
     */

    private String cusTradeNationCode;

    /**
     * 贸易国别(地区)	
     */

    private String cusTradeNationName;

    /**
     * 运费
     */

    private String transCosts;

    /**
     * 启运国(地区)/运抵国(地区)
     */

    private String cusTradeCountryCode;

    /**
     * 启运国(地区)/运抵国(地区)
     */

    private String cusTradeCountryName;

    /**
     * 运输方式
     */

    private String cusTrafModeCode;

    /**
     * 运输方式
     */

    private String cusTrafModeName;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 币制
     */

    private String currencyName;

    /**
     * 境内收发货人
     */

    private Long subjectId;

    /**
     * 境外收发货人
     */

    private String subjectOverseasType;

    /**
     * 境外收发货人
     */

    private Long subjectOverseasId;

    /**
     * 境外收发货人
     */
    private String subjectOverseasName;

    /**
     * 监管方式
     */

    private String supvModeCdde;

    /**
     * 监管方式
     */

    private String supvModeCddeName;

    /**
     * 成交方式
     */

    private String transactionMode;

    /**
     * 单一窗口导入状态
     */
    private String declarationStatus;

    /**
     * 跨境运输单号
     */
    private String transportNo;

    /**
     * 舱单id
     */
    private Long manifestId;

    /**
     * 舱单状态
     */
    private String manifestStatusId;

    /**
     * 舱单单一窗口导入状态
     */
    private String manifestSwStatus;
}
