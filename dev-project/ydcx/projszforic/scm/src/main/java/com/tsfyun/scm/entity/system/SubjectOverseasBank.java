package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Data
public class SubjectOverseasBank extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 主体境外公司
     */

    private Long subjectOverseasId;

    /**
     * 银行名称
     */

    private String name;

    /**
     * 银行账号
     */

    private String account;

    /**
     * SWIFT CODE
     */

    private String swiftCode;

    /**
     * 银行地址
     */

    private String address;

    /**
     * 禁用标示
     */

    private Boolean disabled;

    /**
     * 创建人
     */

    private String createBy;

    /**
     * 默认
     */

    private Boolean isDefault;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 银行代码
     */

    private String code;


}
