package com.tsfyun.scm.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 结汇主单
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Data
public class SettlementAccount extends BaseEntity {

     private static final long serialVersionUID=1L;


    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 收款单号
     */

    private String accountNo;

    /**
     * 订单编号
     */

    private String orderNo;
    private String clientNo;

    /**
     * 结汇时间
     */
    private LocalDateTime settlementDate;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 结汇主体
     */
    private String subjectType;

    /**
     * 结汇主体
     */

    private Long subjectId;

    /**
     * 结汇主体
     */

    private String subjectName;

    /**
     * 结汇银行
     */

    private String subjectBank;

    /**
     * 结汇银行账号
     */

    private String subjectBankNo;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 币制
     */

    private String currencyName;

    /**
     * 原币金额
     */

    private BigDecimal accountValue;

    /**
     * 结汇汇率
     */

    private BigDecimal customsRate;

    /**
     * 本币金额
     */

    private BigDecimal settlementValue;

    /**
     * 备注
     */

    private String memo;


}
