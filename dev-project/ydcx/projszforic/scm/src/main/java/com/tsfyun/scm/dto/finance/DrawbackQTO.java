package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import com.tsfyun.scm.enums.SubjectTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
public class DrawbackQTO extends PaginationDto {

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 采购合同号
     */
    private String purchaseNo;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 状态
     */
    private String statusId;

    /**
     * 收款方
     */
    private String payeeName;

    /**
     * 付款主体
     */
    @EnumCheck(clazz = SubjectTypeEnum.class,message = "付款主体类型错误")
    private String subjectType;

    @Pattern(regexp = "claim_payment_date|actual_payment_date|date_created",message = "查询日期类型错误")
    private String queryDate;

    /**
     * 要求付款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**
     * 实际付款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd) {
        if(dateEnd != null) {
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }
}

