package com.tsfyun.scm.dto.logistics;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 跨境运输单绑定订单
 * @since Created in 2020/5/13 15:36
 */
@Data
public class CrossBorderWaybillBindOrderDTO implements Serializable {

    /**
     * 跨境运输单id
     */
    @NotNull(message = "请选择操作的跨境运输单")
    private Long id;
    /**
     * 订单id
     */
    @NotNull(message = "请选择需要绑定的订单")
    private Long orderId;

}
