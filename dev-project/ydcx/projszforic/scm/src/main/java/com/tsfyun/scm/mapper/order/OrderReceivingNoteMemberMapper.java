package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.OrderReceivingNoteMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.OrderReceivingBindVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-29
 */
@Repository
public interface OrderReceivingNoteMemberMapper extends Mapper<OrderReceivingNoteMember> {

    List<OrderReceivingNoteMember> findByOrderNo(@Param(value = "orderNo") String orderNo);

    List<String> findReceivingNoByOrderNo(@Param(value = "orderNo")String orderNo);

    //据订单号 查询订单绑定入库单明细，生成出库单用
    List<OrderReceivingBindVO> queryOrderReceivingBind(@Param(value = "orderNos")List<String> orderNos);
    //标识绑定明细是否已经出库
    Integer identificationIsIssued(@Param(value = "orderNos")List<String> orderNos,@Param(value = "isOutStock")Boolean isOutStock);
}
