package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户导入excel实体映射类
 */
@Data
public class CustomerExcel extends BaseRowModel implements Serializable {

    @ExcelProperty(value = "客户编号", index = 0)
    private String code;

    @NotEmptyTrim(message = "公司名称不能为空")
    @ExcelProperty(value = "公司名称", index = 1)
    private String name;

    /*
    @ExcelProperty(value = "销售人员", index = 2)
    private String salePersonName;

    @ExcelProperty(value = "商务人员", index = 3)
    private String busPersonName;
     */

    @ExcelProperty(value = "公司英文名称", index = 2)
    private String nameEn;

    @ExcelProperty(value = "18位社会信用代码", index = 3)
    private String socialNo;

    @ExcelProperty(value = "纳税人识别号", index = 4)
    private String taxpayerNo;

    @ExcelProperty(value = "海关注册编码", index = 5)
    private String customsCode;

    @ExcelProperty(value = "公司法人", index = 6)
    private String legalPerson;

    @ExcelProperty(value = "公司电话", index = 7)
    private String tel;

    @ExcelProperty(value = "公司传真", index = 8)
    private String fax;

    @ExcelProperty(value = "公司详细地址", index = 9)
    private String address;

    @ExcelProperty(value = "公司英文地址", index = 10)
    private String addressEn;

    @ExcelProperty(value = "联系人姓名", index = 11)
    private String linkPerson;

    @ExcelProperty(value = "联系电话", index = 12)
    private String linkTel;

    @ExcelProperty(value = "联系地址", index = 13)
    private String linkAddress;

    @ExcelProperty(value = "开票开户银行", index = 14)
    private String invoiceBankName;

    @ExcelProperty(value = "开票银行账号", index = 15)
    private String invoiceBankAccount;

    @ExcelProperty(value = "开票电话", index = 16)
    private String invoiceBankTel;

    @ExcelProperty(value = "开票地址", index = 17)
    private String invoiceBankAddress;

    @ExcelProperty(value = "开票要求", index = 18)
    private String invoiceMemo;





}
