package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 * 跨境运输单请求实体
 * </p>
 *

 * @since 2020-04-17
 */
@Data
@ApiModel(value="CrossBorderWaybill查询请求对象", description="跨境运输单查询请求实体")
public class CrossBorderWaybillQTO  extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   /**
    * 系统运单号
    */
   private String docNo;

   /**
    * 订单编号
    */
   private String orderNo;

   /**
    * 单据类型
    */
   @NotEmptyTrim(message = "单据类型不能为空")
   @Pattern(regexp = "imp|exp",message = "单据类型错误")
   private String billType;

   private LocalDateTime transDateStart;

   private LocalDateTime transDateEnd;

   private String statusId;

   private String waybillNo;

   private String conveyanceNo;


   public void setTransDateEnd(LocalDateTime transDateEnd) {
      if(transDateEnd != null){
         this.transDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(transDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
      }
   }
}
