package com.tsfyun.scm.vo.finance.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.PaymentAccountStatusEnum;
import com.tsfyun.common.base.enums.finance.CounterFeeTypeEnum;
import com.tsfyun.common.base.enums.finance.PaymentTermEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description: 客户端付汇详情
 * @CreateDate: Created in 2020/11/3 14:45
 */
@Data
public class ClientPaymentDetailVO implements Serializable {

    private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "付款提交时间")
    private LocalDateTime accountDate;

    @ApiModelProperty(value = "收款人")
    private Long payeeId;

    @ApiModelProperty(value = "收款人")
    private String payee;

    @ApiModelProperty(value = "收款行")
    private Long payeeBankId;

    @ApiModelProperty(value = "收款行名称")
    private String payeeBankName;

    @ApiModelProperty(value = "收款行账号")
    private String payeeBankAccountNo;

    @ApiModelProperty(value = "收款行地址")
    private String payeeBankAddress;

    @ApiModelProperty(value = "swift code")
    private String swiftCode;

    @ApiModelProperty(value = "bank code")
    private String ibankCode;

    @ApiModelProperty(value = "付款方式")
    private String paymentTerm;

    @ApiModelProperty(value = "结算汇率")
    private BigDecimal exchangeRate;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "币制名称")
    private String currencyName;

    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "付款人民币金额")
    private BigDecimal accountValueCny;

    @ApiModelProperty(value = "手续费承担方式")
    private String counterFeeType;
    private String counterFeeTypeDesc;

    @ApiModelProperty(value = "付款手续费金额")
    private BigDecimal bankFee;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String statusDesc;

    private String paymentTermDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(PaymentAccountStatusEnum.of(statusId)).map(PaymentAccountStatusEnum::getName).orElse("");
    }

    public String getPaymentTermDesc(){
        return Optional.ofNullable(PaymentTermEnum.of(paymentTerm)).map(PaymentTermEnum::getName).orElse("");
    }

    public String getCounterFeeTypeDesc(){
        return Optional.ofNullable(CounterFeeTypeEnum.of(counterFeeType)).map(CounterFeeTypeEnum::getName).orElse("");
    }

}
