package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 财务确定开票完成
 * @Project: 供应链saas平台
 * @CreateDate: Created in 2020/5/22 16:44
 */
@Data
public class InvoiceCompleteDTO implements Serializable {

    @NotNull(message = "发票id不能为空")
    private Long id;

    @LengthTrim(max = 255,message = "发票号长度不能超过255")
    private String invoiceNo;

    @NotEmptyTrim(message = "状态不能为空")
    private String statusId;

}
