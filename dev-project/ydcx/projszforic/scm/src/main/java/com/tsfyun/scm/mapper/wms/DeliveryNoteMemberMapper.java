package com.tsfyun.scm.mapper.wms;

import com.tsfyun.scm.entity.wms.DeliveryNoteMember;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface DeliveryNoteMemberMapper extends Mapper<DeliveryNoteMember> {

}
