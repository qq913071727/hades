package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/22 17:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReceiptAccountDetailPlusVO implements Serializable {

    private ReceiptAccountVO receiptAccount;

    private List<ReceiptAccountMemberVO> members;

}
