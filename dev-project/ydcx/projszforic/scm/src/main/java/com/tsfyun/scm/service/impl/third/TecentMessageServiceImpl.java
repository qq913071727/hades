package com.tsfyun.scm.service.impl.third;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ArrayUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.Feature;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.common.base.support.OperationEnum;
import com.tsfyun.common.base.support.TencentMessageOperation;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.config.properties.TencentCloudProperties;
import com.tsfyun.scm.entity.third.LogSms;
import com.tsfyun.scm.service.third.AbstractMessageService;
import com.tsfyun.scm.service.third.ILogSmsService;
import com.tsfyun.scm.service.third.IMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/15 16:00
 */
@Slf4j
@Service(value = "tencentMessage")
public class TecentMessageServiceImpl extends AbstractMessageService implements IMessageService {

    @Autowired
    private ILogSmsService logSmsService;

    @Autowired
    private TencentCloudProperties tencentCloudProperties;

    @Transactional(rollbackFor = Exception.class)
    public void sendVerificationCode(LogSms logSms) {
        String message = "";
        if(Objects.equals(isSend,Boolean.TRUE)) {
            OperationEnum tencentMessageTemplate = TencentMessageOperation.getInstance().getMessageTemplate().get(MessageNodeEnum.of(logSms.getNode()));
            if(Objects.isNull(tencentMessageTemplate)) {
                log.warn("腾讯云短信平台【{}】未找到短信节点【{}】的配置，不发送短信平台",tencentMessageTemplate.getName(),MessageNodeEnum.of(logSms.getNode()).getName());
                return;
            }
            message = send(tencentMessageTemplate.getCode(),logSms.getPhoneNo(), logSms.getCode());
        } else {
            message = "腾讯云短信发送未开启";
        }
        logSms.setBackMessage(message);
        logSmsService.updateById(logSms);
    }

    /**
     * 发送短信对接短信平台
     * @param templateCode      短信模板编码
     * @param phoneNo          手机号码
     * @param templateParam    模板参数List集合
     * @return
     */
    public String send(String templateCode,String phoneNo,String templateParam){
        try {
            /* 必要步骤：
             * 实例化一个认证对象，入参需要传入腾讯云账户密钥对secretId，secretKey。
             * 这里采用的是从环境变量读取的方式，需要在环境变量中先设置这两个值。
             * 你也可以直接在代码中写死密钥对，但是小心不要将代码复制、上传或者分享给他人，
             * 以免泄露密钥对危及你的财产安全。
             * CAM密匙查询: https://console.cloud.tencent.com/cam/capi*/
            Credential cred = new Credential(tencentCloudProperties.getSecretId(), tencentCloudProperties.getSecretKey());

            // 实例化一个http选项，可选，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            // 设置代理
            // httpProfile.setProxyHost("真实代理ip");
            // httpProfile.setProxyPort(真实代理端口);
            /* SDK默认使用POST方法。
             * 如果你一定要使用GET方法，可以在这里设置。GET方法无法处理一些较大的请求 */
            httpProfile.setReqMethod("POST");
            /* SDK有默认的超时时间，非必要请不要进行调整
             * 如有需要请在代码中查阅以获取最新的默认值 */
            httpProfile.setConnTimeout(600);
            /* SDK会自动指定域名。通常是不需要特地指定域名的，但是如果你访问的是金融区的服务
             * 则必须手动指定域名，例如sms的上海金融区域名： sms.ap-shanghai-fsi.tencentcloudapi.com */
            httpProfile.setEndpoint(tencentCloudProperties.getDomain());

            /* 非必要步骤:
             * 实例化一个客户端配置对象，可以指定超时时间等配置 */
            ClientProfile clientProfile = new ClientProfile();
            /* SDK默认用TC3-HMAC-SHA256进行签名
             * 非必要请不要修改这个字段 */
            clientProfile.setSignMethod("HmacSHA256");
            clientProfile.setHttpProfile(httpProfile);
            /* 实例化要请求产品(以sms为例)的client对象
             * 第二个参数是地域信息，可以直接填写字符串ap-guangzhou，或者引用预设的常量 */
            SmsClient client = new SmsClient(cred, "ap-guangzhou",clientProfile);
            /* 实例化一个请求对象，根据调用的接口和实际情况，可以进一步设置请求参数
             * 你可以直接查询SDK源码确定接口有哪些属性可以设置
             * 属性可能是基本类型，也可能引用了另一个数据结构
             * 推荐使用IDE进行开发，可以方便的跳转查阅各个接口和数据结构的文档说明 */
            SendSmsRequest req = new SendSmsRequest();

            /* 填充请求参数,这里request对象的成员变量即对应接口的入参
             * 你可以通过官网接口文档或跳转到request对象的定义处查看请求参数的定义
             * 基本类型的设置:
             * 帮助链接：
             * 短信控制台: https://console.cloud.tencent.com/smsv2
             * sms helper: https://cloud.tencent.com/document/product/382/3773 */

            /* 短信应用ID: 短信SdkAppId在 [短信控制台] 添加应用后生成的实际SdkAppId，示例如1400006666 */
            String sdkAppId = tencentCloudProperties.getSdkAppId();
            req.setSmsSdkAppId(sdkAppId);

            /* 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录 [短信控制台] 查看 */
            req.setSignName(tencentCloudProperties.getSignName());

            /* 国际/港澳台短信 SenderId: 国内短信填空，默认未开通，如需开通请联系 [sms helper] */
            String senderid = "";
            req.setSenderId(senderid);

            /* 用户的 session 内容: 可以携带用户侧 ID 等上下文信息，server 会原样返回 */
            String sessionContext = "";
            req.setSessionContext(sessionContext);

            /* 短信号码扩展号: 默认未开通，如需开通请联系 [sms helper] */
            String extendCode = "";
            req.setExtendCode(extendCode);

            /* 模板 ID: 必须填写已审核通过的模板 ID。模板ID可登录 [短信控制台] 查看 */
            req.setTemplateId(templateCode);

            /* 下发手机号码，采用 E.164 标准，+[国家或地区码][手机号]
             * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号 */
            String[] phoneNumberSet = {phoneNo};
            req.setPhoneNumberSet(phoneNumberSet);

            /* 模板参数: 若无模板参数，则设置为空 */
            //传过来的参数都是按照字符|#|分隔的
            String[] templateParamSet = new String[]{};
            //如果是json字符串，则把参数值取出来
            if(Objects.nonNull(templateParam)) {
                if(StringUtils.isJSONValid(templateParam)) {
                    LinkedHashMap<String, String> paramMap = JSON.parseObject(templateParam,LinkedHashMap.class, Feature.OrderedField);
                    List<String> paramValue = Lists.newArrayList();
                    paramMap.forEach((k,v)->{
                        paramValue.add(v);
                    });
                    templateParamSet = paramValue.toArray(new String[paramValue.size()]);
                } else if (templateParam.contains(ShortMessagePlatformEnum.PARAM_SPLIT)) {
                    templateParamSet = Splitter.on(ShortMessagePlatformEnum.PARAM_SPLIT).trimResults().splitToList(templateParam).toArray(templateParamSet);
                } else {
                    templateParamSet = new String[]{templateParam};
                }
            }
            req.setTemplateParamSet(templateParamSet);
            /* 通过 client 对象调用 SendSms 方法发起请求。注意请求方法名与请求对象是对应的
             * 返回的 res 是一个 SendSmsResponse 类的实例，与请求对象对应 */
            SendSmsResponse res = client.SendSms(req);

            // 输出json格式的字符串回包
            String responseData = SendSmsResponse.toJsonString(res);
            // 也可以取出单个值，你可以通过官网接口文档或跳转到response对象的定义处查看返回字段的定义
            Console.log("腾讯云短信响应内容【{}】",responseData);
            return responseData;
        } catch (Exception e) {
            log.error("短信发送失败：",e);
        }
        return "发送失败";
    }

    @Override
    public ShortMessagePlatformEnum getPlatform() {
        return ShortMessagePlatformEnum.TECENT;
    }
}
