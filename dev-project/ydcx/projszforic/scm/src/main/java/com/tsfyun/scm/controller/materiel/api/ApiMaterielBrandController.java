package com.tsfyun.scm.controller.materiel.api;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.materiel.MaterielBrandQTO;
import com.tsfyun.scm.entity.materiel.MaterielBrand;
import com.tsfyun.scm.service.materiel.IMaterielBrandService;
import com.tsfyun.scm.vo.materiel.MaterielBrandVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/5 10:16
 */
@RefreshScope
@RestController
@RequestMapping(value = "/api/brand")
public class ApiMaterielBrandController extends BaseController {

    @Autowired
    private IMaterielBrandService materielBrandService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Value("${scmbot.secret:qsxedc321}")
    private String secret;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<MaterielBrandVO>> list(@ModelAttribute MaterielBrandQTO qto, @RequestHeader(value = "secret")String requestSecret) {
        TsfPreconditions.checkArgument(Objects.equals(secret,requestSecret),new ServiceException("非法请求"));
        PageInfo<MaterielBrand> page = materielBrandService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),MaterielBrandVO.class));
    }

    /**
     * 根据品牌名称精确查询
     * @param name
     * @param requestSecret
     * @return
     */
    @PostMapping(value = "findByName")
    Result<MaterielBrandVO> findByName(@RequestParam(value = "name")String name, @RequestHeader(value = "secret")String requestSecret) {
        TsfPreconditions.checkArgument(Objects.equals(secret,requestSecret),new ServiceException("非法请求"));
        return success(materielBrandService.findByName(name));
    }

}
