package com.tsfyun.scm.mapper.customs;

import com.tsfyun.scm.entity.customs.DeclarationTemp;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 报关单模板 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface DeclarationTempMapper extends Mapper<DeclarationTemp> {

}
