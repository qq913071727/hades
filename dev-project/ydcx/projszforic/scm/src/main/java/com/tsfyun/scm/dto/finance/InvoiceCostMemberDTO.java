package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 发票或内贸合同费用明细请求实体
 * </p>
 *
 * @since 2020-05-18
 */
@Data
@ApiModel(value="InvoiceCostMember请求对象", description="发票或内贸合同费用明细请求实体")
public class InvoiceCostMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "单据id不能为空")
   @LengthTrim(max = 20,message = "单据id最大长度不能超过20位")
   @ApiModelProperty(value = "单据id")
   private String docId;

   @NotEmptyTrim(message = "单据编号不能为空")
   @LengthTrim(max = 20,message = "单据编号最大长度不能超过20位")
   @ApiModelProperty(value = "单据编号")
   private String docNo;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 10,message = "单据类型最大长度不能超过10位")
   @ApiModelProperty(value = "单据类型")
   private String docType;

   @NotEmptyTrim(message = "费用科目编码不能为空")
   @LengthTrim(max = 20,message = "费用科目编码最大长度不能超过20位")
   @ApiModelProperty(value = "费用科目编码")
   private String expenseSubjectCode;

   @LengthTrim(max = 50,message = "费用科目名称最大长度不能超过50位")
   @ApiModelProperty(value = "费用科目名称")
   private String expenseSubjectName;

   @Digits(integer = 8, fraction = 2, message = "费用整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "费用")
   private BigDecimal costMoney;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
