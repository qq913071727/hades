package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2021-09-17
 */
@Data
@ApiModel(value="PriceFluctuationHistoryExp响应对象", description="响应实体")
public class PriceFluctuationHistoryExpVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    private String currencyName;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "名称")
    private String name;

    private String spec;

    @ApiModelProperty(value = "进口日期")
    private Date orderDate;

    @ApiModelProperty(value = "订单号")
    private String orderDocNo;

    @ApiModelProperty(value = "订单审价明细")
    private Long orderPriceMemberId;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "美金单价")
    private BigDecimal usdTotalPrice;

    @ApiModelProperty(value = "美金总价")
    private BigDecimal usdUnitPrice;


}
