package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: 公告查询实体
 * @CreateDate: Created in 2020/9/22 15:07
 */
@Data
public class SysNoticeQTO extends PaginationDto implements Serializable {

    private String category;

    private String title;

    private String content;

    private Boolean isOpen;

    private Boolean isLoginSee;

    private Boolean isPopup;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime publishStartTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime publishEndTime;

    public void setPublishEndTime(LocalDateTime publishEndTime) {
        if(publishEndTime != null){
            this.publishEndTime = LocalDateTime.parse(LocalDateTimeUtils.formatTime(publishEndTime,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }



}
