package com.tsfyun.scm.service.logistics;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.logistics.TransportSupplierDTO;
import com.tsfyun.scm.dto.logistics.TransportSupplierPlusInfoDTO;
import com.tsfyun.scm.dto.logistics.TransportSupplierQTO;
import com.tsfyun.scm.entity.logistics.TransportSupplier;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.SimpleTransportSupplierVO;
import com.tsfyun.scm.vo.logistics.TransportSupplierPlusVO;

import java.util.List;

/**
 * <p>
 *  运输供应商服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface ITransportSupplierService extends IService<TransportSupplier> {

    /**
     * 大页面新增
     * @param dto
     * @return
     */
    Long addPlus(TransportSupplierPlusInfoDTO dto);

    /**
     * 大页面修改
     * @param dto
     */
    void editPlus(TransportSupplierPlusInfoDTO dto);

    /**
     * 大页面详情
     * @param id
     * @return
     */
    TransportSupplierPlusVO detailPlus(Long id);

    /**
     * 修改禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);


    /**
     * 简单新增运输供应商信息（不含车辆信息）
     * @param dto
     * @return
     */
    TransportSupplier simpleAdd(TransportSupplierDTO dto);

    /**
     * 简单修改运输供应商信息（不含车辆信息）
     * @param dto
     */
    void simpleEdit(TransportSupplierDTO dto);

    /**
     * 分页查询
     * @param qto
     * @return
     */
    PageInfo<TransportSupplier> pageList(TransportSupplierQTO qto);

    /**
     * 删除供应商
     * @param id
     */
    void remove(Long id);

    /**
     * 下拉
     * @return
     */
    List<SimpleTransportSupplierVO> select();

}
