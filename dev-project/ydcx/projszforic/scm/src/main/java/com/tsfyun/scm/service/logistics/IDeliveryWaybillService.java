package com.tsfyun.scm.service.logistics;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.logistics.*;
import com.tsfyun.scm.entity.logistics.CrossBorderWaybill;
import com.tsfyun.scm.entity.logistics.DeliveryWaybill;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.vo.logistics.CrossBorderWaybillVO;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillPlusVO;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillVO;

import java.util.List;

/**
 * <p>
 * 送货单 服务类
 * </p>
 *

 * @since 2020-05-25
 */
public interface IDeliveryWaybillService extends IService<DeliveryWaybill> {

    /**=
     * 根据进口订单生产国内送货单
     * @param impOrder
     */
    void createByImpOrder(ImpOrder impOrder);

    /**=
     * 分页查询
     * @param qto
     * @return
     */
    PageInfo<DeliveryWaybillVO> pageList(DeliveryWaybillQTO qto);

    /**=
     * 报关单申报成功驱动国内送货单
     * @param orderId
     */
    void confirmByOrder(Long orderId);

    /**=
     * 港车发车驱动国内送货单
     * @param crossBorderWaybill
     * @param orderId
     */
    void driveByCrossBorderWaybill(CrossBorderWaybill crossBorderWaybill,Long orderId);

    /**=
     * 港车签收驱动国内送货单
     * @param crossBorderWaybill
     * @param orderId
     */
    void signByCrossBorderWaybill(CrossBorderWaybill crossBorderWaybill,Long orderId);

    /**=
     * 根据订单ID查询
     * @param orderId
     * @return
     */
    List<DeliveryWaybill> findByOrderId(Long orderId);

    /**=
     * 详情
     * @param id
     * @return
     */
    DeliveryWaybillPlusVO detail(Long id,String operation);

    /**=
     * 确定送货单
     * @param dto
     */
    void confirm(DeliveryConfirmDTO dto);

    /**=
     * 发车确认
     * @param dto
     */
    void confirmDeparture(DeliveryDepartureDTO dto);

    /**=
     * 签收
     * @param dto
     */
    void confirmSign(DeliverySignDTO dto);

    /**
     * 根据ID删除送货单所有信息
     * @param id
     */
    void removeAllById(Long id);

    /**
     * 根据订单删除国内送货单信息
     * @param orderId
     */
    void removeByOrderId(Long orderId);
}
