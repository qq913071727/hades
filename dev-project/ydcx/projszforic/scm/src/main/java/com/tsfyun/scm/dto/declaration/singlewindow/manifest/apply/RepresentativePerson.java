package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description:舱单传输人数据
 * @since Created in 2020/4/22 11:18
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepresentativePerson", propOrder = {
        "name",
})
public class RepresentativePerson {

    //舱单传输人名称
    @XmlElement(name = "Name")
    protected String name;

    public RepresentativePerson () {

    }

    public RepresentativePerson(String name) {
        this.name = name;
    }

}
