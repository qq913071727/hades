package com.tsfyun.scm.controller.base;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.base.CurrencyQTO;
import com.tsfyun.scm.entity.base.Currency;
import com.tsfyun.scm.service.base.ICurrencyService;
import com.tsfyun.scm.vo.base.CurrencyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  币制前端控制器
 * </p>
 *

 * @since 2020-03-18
 */
@RestController
@RequestMapping("/currency")
@Api(tags = "币制")
public class CurrencyController extends BaseController {

    @Autowired
    private ICurrencyService currencyService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 获取所有启用币制（所有的币制，启用的，不分页）
     * @return
     */
    @ApiOperation(value = "获取所有启用币制")
    @GetMapping(value = "select")
    Result<List<CurrencyVO>> select( ) {
        return success(currencyService.select());
    }

    /**
     * 启用/禁用
     */
    @ApiIgnore
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")String id, @RequestParam("disabled")Boolean disabled){
        currencyService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 分页查询
     * @param dto
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "list")
    public Result<List<CurrencyVO>> pageList(@ModelAttribute @Valid CurrencyQTO dto) {
        PageInfo<Currency> page = currencyService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),CurrencyVO.class));
    }

    /**
     * 调整排序
     */
    @ApiIgnore
    @PostMapping("/adjustSort")
    public Result<Void> adjustSort(@RequestParam("id")String id, @RequestParam("sort")Integer sort){
        currencyService.adjustSort(id,sort);
        return success();
    }

}

