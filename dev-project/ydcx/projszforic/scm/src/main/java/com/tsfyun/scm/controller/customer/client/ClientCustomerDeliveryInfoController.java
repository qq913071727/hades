package com.tsfyun.scm.controller.customer.client;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.CustomerDeliveryInfoDTO;
import com.tsfyun.scm.dto.customer.client.ClientCustomerDeliveryInfoQTO;
import com.tsfyun.scm.entity.customer.CustomerDeliveryInfo;
import com.tsfyun.scm.service.customer.ICustomerDeliveryInfoService;
import com.tsfyun.scm.vo.customer.CustomerDeliveryInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/10/10 15:06
 */
@RestController
@RequestMapping(value = "/client/customerDeliveryInfo")
@Api(tags = "客户提货信息（客户端）")
public class ClientCustomerDeliveryInfoController extends BaseController {

    @Autowired
    private ICustomerDeliveryInfoService customerDeliveryInfoService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    @ApiOperation("分页")
    Result<List<CustomerDeliveryInfoVO>> page(@ModelAttribute @Valid ClientCustomerDeliveryInfoQTO qto) {
        PageInfo<CustomerDeliveryInfo> page = customerDeliveryInfoService.clientPage(qto);
        List<CustomerDeliveryInfoVO> supplierBankVOS = beanMapper.mapAsList(page.getList(),CustomerDeliveryInfoVO.class);
        return success((int)page.getTotal(),supplierBankVOS);
    }

    /**
     * 客户有效的收货地址
     * @return
     */
    @GetMapping(value = "effectiveList")
    Result<List<CustomerDeliveryInfoVO>> effectiveList() {
        return success(customerDeliveryInfoService.select(SecurityUtil.getCurrentCustomerId()));
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation("删除")
    public Result<Void> delete(@RequestParam("id")Long id){
        customerDeliveryInfoService.clientRemove(id);
        return success();
    }

    /**
     * 新增
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) CustomerDeliveryInfoDTO dto) {
        customerDeliveryInfoService.clientAdd(dto);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    @ApiOperation("修改")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) CustomerDeliveryInfoDTO dto) {
        customerDeliveryInfoService.clientEdit(dto);
        return success( );
    }

}
