package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 订单审价
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Data
public class ImpOrderPrice implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    private Long id;
    /**
     * 客户
     */

    private Long customerId;

    /**
     * 供应商
     */

    private Long supplierId;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 订单
     */

    private Long orderId;

    /**
     * 订单号
     */

    private String orderDocNo;

    /**
     * 审核时间
     */

    private LocalDateTime reviewTime;

    /**
     * 审核人
     */

    private String reviewer;

    /**
     * 提交次数
     */

    private Integer submitCount;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;
}
