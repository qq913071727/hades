package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PaymentCompletedDTO implements Serializable {

    @NotNull(message = "付款单id不能为空")
    private Long id;

    @NotNull(message = "实付金额不能为空")
    @Digits(integer = 10, fraction = 2, message = "实付金额整数位不能超过10位，小数位不能超过2位")
    @DecimalMin(value = "0.01",message = "实付金额不能小于0")
    @ApiModelProperty(value = "收款金额")
    private BigDecimal actualAccountValue;

    @NotEmptyTrim(message = "实付币制不能为空")
    private String actualCurrencyName;
}
