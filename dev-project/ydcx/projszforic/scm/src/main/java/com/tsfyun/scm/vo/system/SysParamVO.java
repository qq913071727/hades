package com.tsfyun.scm.vo.system;

import com.tsfyun.common.base.enums.BizParamEnum;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-04-15
 */
@Data
@ApiModel(value="SysParam响应对象", description="响应实体")
public class SysParamVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "参数名")
    private String id;

    @ApiModelProperty(value = "参数值")
    private String val;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否锁定-1-是;0-否-")
    private Boolean locking;

    @ApiModelProperty(value = "是否禁用-1-是;2-否")
    private Boolean disabled;

    private String idDesc;

    public String getRemark() {
        BizParamEnum bizParamEnum = BizParamEnum.of(id);
        return StringUtils.isEmpty(remark) ? (Objects.nonNull(bizParamEnum) ? bizParamEnum.getName() : "") : remark;
    }

    public String getIdDesc() {
        BizParamEnum bizParamEnum = BizParamEnum.of(id);
        return Objects.nonNull(bizParamEnum) ? bizParamEnum.getName() : "";
    }


}
