package com.tsfyun.scm.service.common;

import com.tsfyun.common.base.dto.CheckTaskDTO;
import com.tsfyun.common.base.dto.TaskDTO;
import org.springframework.lang.NonNull;

import java.io.Serializable;

/**
 * @Description: 公共服务接口类
 * @since Created in 2020/4/3 09:51
 */
public interface ICommonService {

    /**=
     * 保存单据状态并记录历史，为了减少后续的查询，返回修改前的对象
     * @param dto
     */
    <T> T changeDocumentStatus(TaskDTO dto);

    /**
     * 校验某单据是否可以进行某操作
     * @param dto
     */
    <T> void checkDocumentStatus(CheckTaskDTO dto);

    /**
     * 校验某单据是否可以进行某操作并处理任务
     * @param dto
     */
    <T> void checkDocumentStatus(CheckTaskDTO dto,Boolean handleTaskNotice);

}
