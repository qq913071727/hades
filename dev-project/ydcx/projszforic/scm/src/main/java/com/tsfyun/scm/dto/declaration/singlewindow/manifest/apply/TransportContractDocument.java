package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 运输合同信息
 * @since Created in 2020/4/22 12:16
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransportContractDocument", propOrder = {
        "id",
        "amendment"
})
public class TransportContractDocument {

    //提（运）单号
    @XmlElement(name = "ID")
    private String id;

    @XmlElement(name = "Amendment")
    private Amendment amendment;

    public TransportContractDocument() {

    }

    public TransportContractDocument(String id) {
        this.id = id;
    }

    public TransportContractDocument(String id,Amendment amendment) {
        this.id = id;
        this.amendment = amendment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Amendment getAmendment() {
        return amendment;
    }

    public void setAmendment(Amendment amendment) {
        this.amendment = amendment;
    }
}
