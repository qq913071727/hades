package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.FeedbackQuestionEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;


/**
 * <p>
 * 意见反馈请求实体
 * </p>
 *
 *
 * @since 2020-09-23
 */
@Data
@ApiModel(value="Feedback请求对象", description="意见反馈请求实体")
public class FeedbackDTO implements Serializable {

   private static final long serialVersionUID=1L;

   //意见反馈不能修改

   @NotEmptyTrim(message = "问题或建议类型不能为空")
   @LengthTrim(max = 16,message = "问题或建议类型最大长度不能超过16位")
   @EnumCheck(clazz = FeedbackQuestionEnum.class,message = "问题或建议类型错误")
   @ApiModelProperty(value = "问题或建议类型")
   private String questionType;

   @NotEmptyTrim(message = "问题或建议不能为空")
   @LengthTrim(max = 500,message = "问题或建议最大长度不能超过500位")
   @ApiModelProperty(value = "问题或建议")
   private String memo;

   @ApiModelProperty(value = "图片")
   private String picId;

   @LengthTrim(max = 64,message = "联系方式最大长度不能超过64位")
   @ApiModelProperty(value = "联系方式")
   private String contact;


}
