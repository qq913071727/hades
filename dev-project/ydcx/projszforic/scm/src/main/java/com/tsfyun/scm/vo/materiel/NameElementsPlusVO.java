package com.tsfyun.scm.vo.materiel;

import com.tsfyun.scm.system.vo.CustomsElementsVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @since Created in 2020/3/26 17:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NameElementsPlusVO implements Serializable {

    //品名要素
    private NameElementsVO nameElements;

    //申报要素项目
    private List<String> elementValues;

    //申报要素信息
    private List<CustomsElementsVO> customsElements;

}
