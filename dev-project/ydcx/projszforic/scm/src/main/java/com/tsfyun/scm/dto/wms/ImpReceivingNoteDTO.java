package com.tsfyun.scm.dto.wms;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel(value="进口入库登记", description="进口入库登记")
public class ImpReceivingNoteDTO implements Serializable {

    @NotEmptyTrim(message = "入库单号不能为空")
    @LengthTrim(max = 20,message = "入库单号最大长度不能超过20位")
    @ApiModelProperty(value = "入库单号")
    private String docNo;

    @NotNull(message = "入库时间不能为空")
    @ApiModelProperty(value = "入库时间")
    private LocalDateTime receivingDate;

    @NotNull(message = "仓库不能为空")
    @ApiModelProperty(value = "仓库")
    private Long warehouseId;

    @NotEmptyTrim(message = "交货方式不能为空")
    @LengthTrim(max = 20,message = "交货方式最大长度不能超过20位")
    @ApiModelProperty(value = "交货方式")
    private String deliveryMode;

    @ApiModelProperty(value = "快递类型")
    private String inExpressType;

    @ApiModelProperty(value = "快递公司")
    private String hkExpress;

    @LengthTrim(max = 100,message = "快递单号最大长度不能超过100位")
    @ApiModelProperty(value = "快递单号")
    private String hkExpressNo;

    @LengthTrim(max = 20,message = "订单编号最大长度不能超过20位")
    @ApiModelProperty(value = "订单编号")
    private String regOrderNo;

    @NotEmptyTrim(message = "客户名称不能为空")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @NotNull(message = "总箱数不能为空")
    @LengthTrim(max = 11,message = "总箱数最大长度不能超过11位")
    @ApiModelProperty(value = "总箱数")
    @Min(value = 1,message = "箱数必须大于等于1")
    private Integer totalCartonNum;

    @ApiModelProperty(value = "境外供应商")
    private String supplierName;

    @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
    @ApiModelProperty(value = "备注")
    private String memo;
}
