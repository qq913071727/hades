package com.tsfyun.scm.service.report;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.view.ViewImpDifferenceMemberQTO;
import com.tsfyun.scm.dto.view.ViewOrderCostMemberQTO;
import com.tsfyun.scm.vo.view.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface IImpReportService {

    /**
     * 订单应收报表
     * @param qto
     * @return
     */
    List<ViewOrderCostMemberVO> orderReceivableList(ViewOrderCostMemberQTO qto);
    PageInfo<ViewOrderCostMemberVO> orderReceivablePageList(ViewOrderCostMemberQTO qto);
    ViewOrderCostMemberTotalVO orderReceivableTotal(ViewOrderCostMemberQTO qto);
    Long exportOrderReceivableExcel(ViewOrderCostMemberQTO qto) throws Exception;

    /**
     * 客户票差明细列表
     * @param qto
     * @return
     */
    PageInfo<ViewImpDifferenceMemberVO> impDifferenceMemberPage(ViewImpDifferenceMemberQTO qto);

    /**
     * 获取客户票差
     * @param customerId
     * @return
     */
    BigDecimal getCustomerDifference(Long customerId);

    /**
     * 导出客户票差明细
     * @param qto
     * @return
     */
    Long exportImpDifferenceMember(ViewImpDifferenceMemberQTO qto) throws Exception;


    /**
     * 客户票差汇总列表
     * @param qto
     * @return
     */
    PageInfo<ViewImpDifferenceSummaryVO> impDifferenceSummaryPage(ViewImpDifferenceMemberQTO qto);

    /**
     * 导出客户票差汇总
     * @param qto
     * @return
     */
    Long exportImpDifferenceSummary(ViewImpDifferenceMemberQTO qto) throws Exception;

    /**
     * 月度代理费
     * @param year
     * @return
     */
    Map<String,BigDecimal> monthAgencyFee(String year);
}
