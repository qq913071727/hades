package com.tsfyun.scm.dto.order;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SelfHelpSplitMemberDTO implements Serializable {

    private List<Long> ids;
}
