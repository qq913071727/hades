package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-12
 */
@Data
@Accessors(chain = true)
public class SupplierTakeInfo extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 供应商
     */
    private Long supplierId;

    /**
     * 提货公司
     */
    private String companyName;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系电话
     */
    private String linkTel;

    /**
     * 省
     */
    private String provinceName;

    /**
     * 市
     */
    private String cityName;

    /**
     * 区
     */
    private String areaName;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 禁用标示
     */
    private Boolean disabled;

    /**
     * 默认地址
     */
    private Boolean isDefault;


}
