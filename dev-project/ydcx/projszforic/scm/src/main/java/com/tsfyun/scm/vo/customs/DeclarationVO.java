package com.tsfyun.scm.vo.customs;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import com.tsfyun.common.base.enums.domain.DeclarationStatusEnum;
import com.tsfyun.common.base.enums.singlewindow.DyckTransModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 报关单响应实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="Declaration响应对象", description="报关单响应实体")
public class DeclarationVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "申报单位组织机构代码")
    private String agentScc;

    @ApiModelProperty(value = "申报单位海关编号")
    private String agentCode;

    @ApiModelProperty(value = "申报单位名称")
    private String agentName;

    @ApiModelProperty(value = "境内收发货人")
    private Long subjectId;
    private String rcvgdTradeName;//境内收发货人
    private String rcvgdTradeScc;//18位社会信用代码
    private String rcvgdTradeCode;//10位海关代码

    @ApiModelProperty(value = "境外收发货人")
    private Long subjectOverseasId;
    private String consignorName;//境外收发货人
    private String consignorCode;//境外收发货人代码

    private String ownerScc;//消费使用单位(18位社会信用代码)
    private String ownerCode;//消费使用单位(10位海关代码)
    private String ownerName;//消费使用单位(名称)

    @ApiModelProperty(value = "提运单号")
    private String billNo;

    @ApiModelProperty(value = "单据类型-进出口")
    private String billType;
    private String billTypeName;
    public String getBillTypeName(){
        BillTypeEnum billTypeEnum = BillTypeEnum.of(getBillType());
        return Objects.nonNull(billTypeEnum)?billTypeEnum.getName():"";
    }


    @ApiModelProperty(value = "订单号")
    private Long orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderDocNo;

    @ApiModelProperty(value = "航次号-六联单号")
    private String cusVoyageNo;

    @ApiModelProperty(value = "集装箱号")
    private String containerNo;

    @ApiModelProperty(value = "合同号")
    private String contrNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;
    @ApiModelProperty(value = "客户")
    private String customerName;

    @ApiModelProperty(value = " 申报日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime decDate;

    @ApiModelProperty(value = "报关单号")
    private String declarationNo;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "申报地海关")
    private String customMasterCode;

    @ApiModelProperty(value = "申报地海关")
    private String customMasterName;

    @ApiModelProperty(value = "货物存放地")
    private String goodsPlace;

    @ApiModelProperty(value = "启运港")
    private String destPortCode;

    @ApiModelProperty(value = "启运港")
    private String destPortName;

    @ApiModelProperty(value = "境内目的地")
    private String districtCode;

    @ApiModelProperty(value = "境内目的地")
    private String districtName;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "入境口岸")
    private String ciqEntyPortCode;

    @ApiModelProperty(value = "入境口岸")
    private String ciqEntyPortName;

    @ApiModelProperty(value = "进境关别")
    private String iePortCode;

    @ApiModelProperty(value = "进境关别")
    private String iePortName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "进出口日期")
    private LocalDateTime importDate;

    @ApiModelProperty(value = "保费")
    private String insuranceCosts;

    @ApiModelProperty(value = "许可证号")
    private String licenseNo;

    @ApiModelProperty(value = "车牌")
    private String licensePlate;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "杂费")
    private String miscCosts;

    @ApiModelProperty(value = "包装种类")
    private String wrapTypeCode;

    @ApiModelProperty(value = "包装种类")
    private String wrapTypeName;

    @ApiModelProperty(value = "备案号")
    private String recordNo;

    @ApiModelProperty(value = "统一编号")
    private String cusCiqNo;

    @ApiModelProperty(value = "标记唛码")
    private String markNo;

    @ApiModelProperty(value = "经停港")
    private String distinatePortCode;

    @ApiModelProperty(value = "经停港")
    private String distinatePortName;

    @ApiModelProperty(value = "征免性质")
    private String cutModeCode;

    @ApiModelProperty(value = "征免性质名称")
    private String cutModeName;

    @ApiModelProperty(value = "件数")
    private Integer packNo;

    @ApiModelProperty(value = "净重(KG)")
    private BigDecimal netWt;

    @ApiModelProperty(value = "毛重(KG)")
    private BigDecimal grossWt;

    @ApiModelProperty(value = "贸易国别(地区)")
    private String cusTradeNationCode;

    @ApiModelProperty(value = "贸易国别(地区)")
    private String cusTradeNationName;

    @ApiModelProperty(value = "运费")
    private String transCosts;

    @ApiModelProperty(value = "启运国(地区)")
    private String cusTradeCountryCode;

    @ApiModelProperty(value = "启运国(地区)")
    private String cusTradeCountryName;

    @ApiModelProperty(value = "运输方式")
    private String cusTrafModeCode;

    @ApiModelProperty(value = "运输方式名称")
    private String cusTrafModeName;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制名称")
    private String currencyName;

    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "监管方式")
    private String supvModeCdde;

    @ApiModelProperty(value = "监管方式")
    private String supvModeCddeName;

    @ApiModelProperty(value = "成交方式")
    private String transactionMode;

    @ApiModelProperty(value = "外贸合同买方")
    private String subjectOverseasType;

    /**
     * 舱单id
     */
    private Long manifestId;

    /**
     * 状态描述
     */
    private String statusDesc;

    private String dyckTransactionMode;

    /**
     * 成交方式描述
     */
    private String transactionModeDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(DeclarationStatusEnum.of(statusId)).map(DeclarationStatusEnum::getName).orElse("");
    }

    public String getTransactionModeDesc() {
        return Optional.ofNullable(TransactionModeEnum.of(transactionMode)).map(TransactionModeEnum::getName).orElse("");
    }

    public String getDyckTransactionMode() {
        return Optional.ofNullable(DyckTransModeEnum.ofScode(transactionMode)).map(DyckTransModeEnum::getCode).orElse("");
    }





}
