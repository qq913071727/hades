package com.tsfyun.scm.controller.user;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.ShortMessageDTO;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.RequestUtils;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.user.*;
import com.tsfyun.scm.dto.user.client.ClientChangePassWordDTO;
import com.tsfyun.scm.dto.user.client.ClientSetPassWordDTO;
import com.tsfyun.scm.dto.user.client.ClientUpdatePassWordDTO;
import com.tsfyun.scm.entity.user.Login;
import com.tsfyun.scm.service.third.IShortMessageService;
import com.tsfyun.scm.service.user.ILoginService;
import com.tsfyun.scm.service.user.IPersonService;
import com.tsfyun.scm.vo.support.WeixinFocusScanVO;
import com.tsfyun.scm.vo.user.ClientInfoVO;
import com.tsfyun.scm.vo.user.LoginInfoVO;
import com.tsfyun.scm.vo.user.SGTLoginVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping(value = "/auth")
public class LoginController extends BaseController {

    @Autowired
    private ILoginService loginService;

    @Autowired
    private IShortMessageService shortMessageService;

    @Autowired
    private IPersonService personService;

    /**
     * 登录
     * @param dto
     * @return
     */
    @PostMapping(value = "/login")
    public Result<LoginInfoVO> login(@RequestBody @Validated LoginDTO dto) {
        return success(loginService.login(dto));
    }

    /**
     * 深关通数据交换登录
     * @param dto
     * @return
     */
    @PostMapping(value = "/sgtLogin")
    public Result<SGTLoginVO> sgtLogin(@ModelAttribute @Validated SGTLoginDTO dto){
        return success(loginService.sgtLogin(dto));
    }

    /**
     * 微信小程序授权
     * @return
     */
    @PostMapping(value = "/wxxcxAuthorize")
    public Result<LoginInfoVO> wxxcxAuthorize(@RequestBody @Validated WxxcxLoginDTO dto){
        return success(loginService.wxxcxAuthorize(dto));
    }

    /**
     * 微信小程序绑定手机号
     * @return
     */
    @PostMapping(value = "/wxxcxAuthorizeByPhone")
    public Result<LoginInfoVO> wxxcxAuthorizeByPhone(@RequestBody @Validated WxxcxLoginDTO dto){
        return success(loginService.wxxcxAuthorizeByPhone(dto));
    }

    /**
     * 网页版客户端绑定手机号
     * @return
     */
    @PostMapping(value = "/checkLogin")
    public Result<LoginInfoVO> checkLogin(@RequestBody @Validated WxgzhLoginDTO dto){
        return success(loginService.wxGzhCheckLogin(dto));
    }

    /**
     * 发送通用短信验证码
     * @param dto
     * @return
     */
    @PostMapping("sendVerificationCode")
    public Result<Void> sendVerificationCode(@RequestBody @Validated ShortMessageDTO dto){
        String ip = RequestUtils.getIp();
        if(StringUtils.isEmpty(ip)){ throw new ServiceException("非法请求");}
        shortMessageService.sendVerificationCode(dto, MessageNodeEnum.COMMON_PROVE,ip);
        return success();
    }

    /**
     * 发送找回密码短信验证码
     * @param dto
     * @return
     */
    @PostMapping("sendRetrievePasswordCode")
    public Result<Void> sendRetrievePasswordCode(@RequestBody @Validated ShortMessageDTO dto){
        String ip = RequestUtils.getIp();
        if(StringUtils.isEmpty(ip)){ throw new ServiceException("非法请求");}
        Login login = loginService.findByLoginName(dto.getPhoneNo());
        TsfPreconditions.checkNonNull(login,"当前手机号未注册，您可以直接注册登录");
        shortMessageService.sendVerificationCode(dto, MessageNodeEnum.COMMON_PROVE,ip);
        return success();
    }

    @ApiOperation("客户端修改密码（不需要登录）")
    @PostMapping(value = "/retrievePassword")
    public Result<Void> retrievePassword(@ModelAttribute @Validated ClientUpdatePassWordDTO dto){
        loginService.retrievePassword(dto);
        return success();
    }

    /**
     * 登出
     * @return
     */
    @PostMapping(value = "/logout")
    public Result<Void> logout(){
        loginService.logout();
        return success();
    }

    /**
     * 修改密码（如果存在多种登录类型，所有类型的密码保持一致，用户主动修改）
     * @return
     */
    @PostMapping(value = "/modifyPassWord")
    public Result<Void> modifyPassWord(@ModelAttribute @Validated(UpdateGroup.class) UpdatePassWordDTO dto){
        personService.updatePassWord(dto);
        return success();
    }

    /**
     * 设置密码（首次设置密码，密码过期，如果存在多种登录类型，所有类型的密码保持一致）
     * @return
     */
    @PostMapping(value = "/setPassWord")
    public Result<Void> setPassWord(@ModelAttribute @Valid SetPassWordDTO dto){
        personService.setPassword(dto);
        return success();
    }

    /**
     * 管理端修改密码
     * @return
     */
    @PostMapping(value = "/resetPassWord")
    public Result<Void> updatePassWordForManager(@ModelAttribute @Validated() UpdatePassWordDTO dto){
        loginService.updatePassWordForManager(dto);
        return success();
    }

    /**
     * 获取登录客户基本信息
     * @return
     */
    @GetMapping("clientInfo")
    public Result<ClientInfoVO> clientInfo(){
        return Result.success(loginService.clientInfo());
    }

    /**
     * 客户端用旧密码确认身份修改密码
     * @return
     */
    @PostMapping(value = "/clientModifyPassWord")
    public Result<Void> clientModifyPassWord(@RequestBody @Validated() ClientChangePassWordDTO dto){
        loginService.clientModifyPassWord(dto);
        return success();
    }

    /**
     * 客户端设置登录密码
     * @return
     */
    @PostMapping(value = "/clientSetPassWord")
    public Result<Void> clientSetPassWord(@RequestBody @Validated() ClientSetPassWordDTO dto){
        loginService.clientSetPassWord(dto);
        return success();
    }

    /**
     * 微信小程序端同步用户资料信息
     * @return
     */
    @PostMapping(value = "/syncUserDataByWx")
    public Result<Void> syncUserDataByWx(@RequestBody @Validated WxxcxLoginDTO dto){
        loginService.syncWxHeadAndName(dto);
        return success();
    }

    /**
     * 判断用户是否已经绑定或关注了微信公众号登录
     * @return
     */
    @PostMapping(value = "/checkUserBindWxUnion")
    public Result<String> checkUserBindWxUnion( ){
        return success(loginService.checkUserBindFocusWxUnion( ));
    }

    /**
     * 网页版客户端已登录用户绑定微信公众号
     * @return
     */
    @PostMapping(value = "/checkBind")
    public Result<WeixinFocusScanVO> checkBind(@RequestParam(value = "code")String code){
        return success(loginService.checkBindFocusWxgzh(code));
    }

    /**
     * 生成用户绑定微信/换绑微信事件key
     * @return
     */
    @PostMapping(value = "/generateWxLoginKey")
    public Result<String> generateWxLoginKey(@RequestParam(value = "operateType")String operateType){
        return success(loginService.generateWxLoginKey(operateType));
    }

    /**
     * 网页版客户端已登录用户绑定、换绑微信公众号
     * @return
     */
    @PostMapping(value = "/checkBindWxgzh")
    public Result<WeixinFocusScanVO> checkBindWxgzh(@RequestParam(value = "operateType")String operateType,@RequestParam(value = "code")String code){
        return success(loginService.checkBindWxgzh(operateType,code));
    }

}
