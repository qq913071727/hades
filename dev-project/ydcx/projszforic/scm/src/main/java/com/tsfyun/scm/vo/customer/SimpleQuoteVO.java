package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 简易报价信息
 * @since Created in 2020/4/8 14:48
 */
@Data
public class SimpleQuoteVO implements Serializable {

    private Long id;

    private String memo;
}
