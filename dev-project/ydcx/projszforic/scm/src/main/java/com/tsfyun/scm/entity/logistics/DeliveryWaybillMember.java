package com.tsfyun.scm.entity.logistics;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 送货单明细
 * </p>
 *

 * @since 2020-05-25
 */
@Data
public class DeliveryWaybillMember implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    private Long id;

    private Long deliveryNoteId;


    private Long orderMemberId;

    /**
     * 数量
     */

    private BigDecimal quantity;


}
