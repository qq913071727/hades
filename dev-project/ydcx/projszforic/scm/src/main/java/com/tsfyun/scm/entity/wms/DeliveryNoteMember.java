package com.tsfyun.scm.entity.wms;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
public class DeliveryNoteMember implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    private Long deliveryNoteId;


    private Long receivingNoteMemberId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 客户物料号
     */

    private String goodsCode;

    /**
     * 产地
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;


}
