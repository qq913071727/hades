package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 费用会计科目请求实体
 * </p>
 *

 * @since 2020-03-16
 */
@Data
@ApiModel(value="ExpenseSubject请求对象", description="请求实体")
public class ExpenseSubjectDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "科目编码不能为空",groups = UpdateGroup.class)
   @LengthTrim(max = 20,message = "科目编码最大长度不能超过20位")
   @ApiModelProperty(value = "科目编码")
   private String id;

   @NotEmptyTrim(message = "原科目编码不能为空",groups = UpdateGroup.class)
   private String preId;

   @NotEmptyTrim(message = "科目名称不能为空")
   @LengthTrim(max = 50,message = "科目名称最大长度不能超过50位")
   @ApiModelProperty(value = "科目名称")
   private String name;

   @NotNull(message = "禁用状态不能为空")
   @ApiModelProperty(value = "禁用状态")
   private Boolean disabled;

   @ApiModelProperty(value = "查询参数以XX开头")
   private String startsWith;

   /*
   @NotNull(message = "锁定状态不能为空")
   @ApiModelProperty(value = "锁定状态")
   private Boolean locking;
    */

   @LengthTrim(max = 50,message = "创建人最大长度不能超过50位")
   @ApiModelProperty(value = "创建人")
   private String createBy;

   @LengthTrim(max = 500,message = "备注信息最大长度不能超过500位")
   @ApiModelProperty(value = "备注信息")
   private String memo;


}
