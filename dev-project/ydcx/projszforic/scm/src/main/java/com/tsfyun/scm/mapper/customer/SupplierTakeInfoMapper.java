package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.customer.client.ClientSupplierTakeInfoQTO;
import com.tsfyun.scm.entity.customer.SupplierTakeInfo;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.SupplierTakeInfoListVO;
import com.tsfyun.scm.vo.customer.client.ClientSupplierTakeInfoVO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-12
 */
@Repository
public interface SupplierTakeInfoMapper extends Mapper<SupplierTakeInfo> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<SupplierTakeInfoListVO> list(Map<String,Object> params);

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = false)
    List<SupplierTakeInfoListVO> effectiveList(Map<String,Object> params);

    List<ClientSupplierTakeInfoVO> clientPage(ClientSupplierTakeInfoQTO qto);
}
