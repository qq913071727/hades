package com.tsfyun.scm.controller.customer;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 进口条款 前端控制器
 * </p>
 *
 *
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/impClause")
public class ImpClauseController extends BaseController {

}

