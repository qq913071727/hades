package com.tsfyun.scm.service.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.system.ExpenseSubjectDTO;
import com.tsfyun.scm.entity.system.ExpenseSubject;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.common.base.vo.ExpenseSubjectVO;

import java.util.List;

/**
 * <p>
 *  费用会计科目服务类
 * </p>
 *

 * @since 2020-03-16
 */
public interface IExpenseSubjectService extends IService<ExpenseSubject> {

    //新增
    void add(ExpenseSubjectDTO dto);

    //修改
    void edit(ExpenseSubjectDTO dto);

    //分页列表
    PageInfo<ExpenseSubject> page(ExpenseSubjectDTO dto);

    //删除
    void delete(String id);

    //获取所有的费用科目
    List<ExpenseSubjectVO> allList(ExpenseSubjectDTO dto);

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(String id,Boolean disabled);

    /**
     * 详情
     * @param id
     * @return
     */
    ExpenseSubjectVO detail(String id);

    /**
     * 所以启用的费用科目信息
     * @return
     */
    List<ExpenseSubject> allActive();

    /**
     * 清除缓存
     */
    void clear();
}
