package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tsfyun.common.base.enums.finance.InvoiceDocTypeEnum;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TypeUtils;
import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.scm.entity.finance.Invoice;
import com.tsfyun.scm.entity.finance.InvoiceCostMember;
import com.tsfyun.scm.entity.finance.InvoiceMember;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderCost;
import com.tsfyun.scm.entity.system.ExpenseSubject;
import com.tsfyun.scm.mapper.finance.InvoiceCostMemberMapper;
import com.tsfyun.scm.service.finance.IInvoiceCostMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.order.IImpOrderCostService;
import com.tsfyun.scm.service.system.IExpenseSubjectService;
import com.tsfyun.scm.vo.finance.InvoiceCostMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 发票或内贸合同费用明细 服务实现类
 * </p>
 *

 * @since 2020-05-18
 */
@Service
public class InvoiceCostMemberServiceImpl extends ServiceImpl<InvoiceCostMember> implements IInvoiceCostMemberService {

    @Resource
    private Snowflake snowflake;

    @Autowired
    private IExpenseSubjectService expenseSubjectService;

    @Autowired
    private InvoiceCostMemberMapper invoiceCostMemberMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAgentMember(Invoice invoice, ImpOrder impOrder, List<ImpOrderCost> orderCosts) {
        //赋值保存发票费用信息
        List<InvoiceCostMember> invoiceCostMemberList = Lists.newArrayListWithExpectedSize(orderCosts.size());
        Map<String,String> expenseSubjectMap = Maps.newHashMap();
        orderCosts.stream().forEach(impOrderCost -> {
            InvoiceCostMember invoiceCostMember = new InvoiceCostMember();
            invoiceCostMember.setId(snowflake.nextId());
            invoiceCostMember.setCostMoney(impOrderCost.getReceAmount());
            invoiceCostMember.setDocType(InvoiceDocTypeEnum.INVOICE.getCode());
            invoiceCostMember.setDocId(invoice.getId().toString());
            invoiceCostMember.setDocNo(invoice.getDocNo());
            invoiceCostMember.setOrderNo(impOrder.getDocNo());
            invoiceCostMember.setExpenseSubjectId(impOrderCost.getExpenseSubjectId());
            String expenseSubjectName = expenseSubjectMap.get(impOrderCost.getExpenseSubjectId());
            if(StringUtils.isEmpty(expenseSubjectName)) {
                ExpenseSubject expenseSubject = expenseSubjectService.getById(impOrderCost.getExpenseSubjectId());
                expenseSubjectName = Objects.nonNull(expenseSubject) ? expenseSubject.getName() : null;
                expenseSubjectMap.put(impOrderCost.getExpenseSubjectId(),expenseSubjectName);
            }
            invoiceCostMember.setExpenseSubjectName(expenseSubjectName);
            invoiceCostMember.setDateCreated(LocalDateTime.now());
            invoiceCostMemberList.add(invoiceCostMember);
        });
        super.savaBatch(invoiceCostMemberList);
    }

    @Override
    public List<InvoiceCostMemberVO> getMembersByOrderNo(String orderNo) {
        InvoiceCostMember invoiceCostMember = new InvoiceCostMember();
        invoiceCostMember.setOrderNo(orderNo);
        List<InvoiceCostMember> members = super.list(invoiceCostMember);
        return beanMapper.mapAsList(members, InvoiceCostMemberVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteByDocId(String docId) {
        invoiceCostMemberMapper.deleteByDocId(docId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveMembers(List<InvoiceCostMember> members, ImpSalesContract impSalesContract) {
        //Map<String,String> expenseSubjectMap = Maps.newHashMap();
        members.stream().forEach(member->{
            member.setId(snowflake.nextId());
            member.setDocType(InvoiceDocTypeEnum.CONTRACT.getCode());
            member.setDocId(impSalesContract.getId().toString());
            member.setDocNo(impSalesContract.getDocNo());
            member.setOrderNo(impSalesContract.getImpOrderNo());
            /*
            String expenseSubjectName = expenseSubjectMap.get(member.getExpenseSubjectId());
            if(StringUtils.isEmpty(expenseSubjectName)) {
                ExpenseSubject expenseSubject = expenseSubjectService.getById(member.getExpenseSubjectId());
                expenseSubjectName = Objects.nonNull(expenseSubject) ? expenseSubject.getName() : null;
                expenseSubjectMap.put(member.getExpenseSubjectId(),expenseSubjectName);
            }
            member.setExpenseSubjectName(expenseSubjectName);
             */
            member.setDateCreated(LocalDateTime.now());
        });
        super.savaBatch(members);
    }
}
