package com.tsfyun.scm.controller.customs;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customs.DeclarationBrokerDTO;
import com.tsfyun.scm.dto.customs.DeclarationBrokerQTO;
import com.tsfyun.scm.entity.customs.DeclarationBroker;
import com.tsfyun.scm.service.customs.IDeclarationBrokerService;
import com.tsfyun.scm.vo.customs.DeclarationBrokerVO;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 报关行 前端控制器
 * </p>
 *
 *
 * @since 2021-11-15
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/declarationBroker")
public class DeclarationBrokerController extends BaseController {

    private final IDeclarationBrokerService declarationBrokerService;

    private final OrikaBeanMapper beanMapper;

    /**
     * 新增
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) DeclarationBrokerDTO dto) {
        declarationBrokerService.add(dto);
        return success( );
    }

    /**
     * 修改供应商信息（含供应商提货信息和供应商银行）
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) DeclarationBrokerDTO dto) {
        declarationBrokerService.edit(dto);
        return success();
    }


    /**
     * 详情
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<DeclarationBrokerVO> info(@RequestParam(value = "id")Long id) {
        return success(declarationBrokerService.detail(id));
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        declarationBrokerService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 分页
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<DeclarationBrokerVO>> page(DeclarationBrokerQTO qto) {
        PageInfo<DeclarationBroker> page = declarationBrokerService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),DeclarationBrokerVO.class));
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        declarationBrokerService.delete(id);
        return success();
    }

    /**
     * 下拉
     * @param name
     * @return
     */
    @PostMapping(value = "select")
    Result<List<DeclarationBrokerVO>> select(@RequestParam(value = "name",required = false)String name) {
        return success(declarationBrokerService.select(name));
    }

}

