package com.tsfyun.scm.vo.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.domain.AgreementStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @Description:
 * @since Created in 2020/4/1 11:52
 */
@Data
public class AgreementDetailVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "协议编号")
    private String docNo;

    @ApiModelProperty(value = "业务类型")
    private String businessType;

    @ApiModelProperty(value = "报关类型")
    private String declareType;

    @ApiModelProperty(value = "客户ID")
    private Long customerId;

    @ApiModelProperty(value = "甲方")
    private String partyaName;

    @ApiModelProperty(value = "甲方统一社会信用代码")
    private String partyaSocialNo;

    @ApiModelProperty(value = "甲方法人")
    private String partyaLegalPerson;

    @ApiModelProperty(value = "甲方电话")
    private String partyaTel;

    @ApiModelProperty(value = "甲方传真")
    private String partyaFax;

    @ApiModelProperty(value = "甲方地址")
    private String partyaAddress;

    @ApiModelProperty(value = "乙方")
    private String partybName;

    @ApiModelProperty(value = "乙方统一社会信用代码")
    private String partybSocialNo;

    @ApiModelProperty(value = "乙方法人")
    private String partybLegalPerson;

    @ApiModelProperty(value = "乙方电话")
    private String partybTel;

    @ApiModelProperty(value = "乙方传真")
    private String partybFax;

    @ApiModelProperty(value = "乙方地址")
    private String partybAddress;

    @ApiModelProperty(value = "签约日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime signingDate;

    @ApiModelProperty(value = "生效日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime effectDate;

    @ApiModelProperty(value = "失效日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime invalidDate;

    @ApiModelProperty(value = "实际失效日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime actualInvalidDate;

    @ApiModelProperty(value = "自动延期/年")
    private Integer delayYear;

    @ApiModelProperty(value = "主要进口产品")
    private String mainProduct;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String subjectName;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 客户地址
     */
    private String customerAddress;

    /**
     * 销售人员
     */
    private String salePersonName;

    private String businessTypeDesc;

    public String getBusinessTypeDesc( ) {
        return Optional.ofNullable(BusinessTypeEnum.of(businessType)).map(BusinessTypeEnum::getName).orElse("");
    }

}
