package com.tsfyun.scm.service.third;

import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.scm.entity.third.LogSms;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/15 16:48
 */
public interface IMessageService {

    ShortMessagePlatformEnum getPlatform();

    void sendVerificationCode(LogSms logSms);

}
