package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.entity.system.SendMail;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface SendMailMapper extends Mapper<SendMail> {

}
