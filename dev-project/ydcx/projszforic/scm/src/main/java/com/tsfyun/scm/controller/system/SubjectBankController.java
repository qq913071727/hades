package com.tsfyun.scm.controller.system;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.system.SubjectBankDTO;
import com.tsfyun.scm.dto.system.SubjectBankQTO;
import com.tsfyun.scm.service.system.ISubjectBankService;
import com.tsfyun.scm.vo.system.SubjectBankVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2020-03-20
 */
@RestController
@RequestMapping("/subjectBank")
public class SubjectBankController extends BaseController {

    @Autowired
    private ISubjectBankService subjectBankService;

    @PostMapping(value = "list")
    Result<List<SubjectBankVO>> pageList(@ModelAttribute @Valid SubjectBankQTO qto){
        PageInfo<SubjectBankVO> page = subjectBankService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }


    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Valid SubjectBankDTO dto){
        subjectBankService.add(dto);
        return success();
    }

    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Valid SubjectBankDTO dto){
        subjectBankService.edit(dto);
        return success();
    }


    @GetMapping(value = "detail")
    Result<SubjectBankVO> detail(@RequestParam(value = "id")Long id){
        return success(subjectBankService.detail(id));
    }


    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        subjectBankService.remove(id);
        return success();
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        subjectBankService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 获取启用的收款企业银行
     * @return
     */
    @GetMapping(value = "selectEnableBanks")
    Result<List<SubjectBankVO>> selectEnableBanks(){
        return success(subjectBankService.selectEnableBanks());
    }
}

