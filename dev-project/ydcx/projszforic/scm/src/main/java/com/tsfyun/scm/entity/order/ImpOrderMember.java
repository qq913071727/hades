package com.tsfyun.scm.entity.order;

import java.math.BigDecimal;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 订单明细
 * </p>
 *

 * @since 2020-04-08
 */
@Data
public class ImpOrderMember {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 订单主单
     */

    private Long impOrderId;

    /**
     * 物料ID
     */

    private String materialId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 客户物料号
     */

    private String goodsCode;

    /**
     * 产地
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价(委托)
     */

    private BigDecimal unitPrice;

    /**
     * 总价(委托)
     */

    private BigDecimal totalPrice;

    /**
     * 报关单价
     */

    private BigDecimal decUnitPrice;

    /**
     * 报关总价
     */

    private BigDecimal decTotalPrice;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;

    /**
     * 关税率(包含加征)
     */

    private BigDecimal tariffRate;

    /**
     * 加征关税
     */

    private BigDecimal levyVal;

    /**
     * 增值税率
     */

    private BigDecimal addedTaxTate;

    /**
     * 消费税率
     */

    private String exciseTax;

    private BigDecimal dutyPaidVal;//完税价格
    /**
     * 关税金额
     */

    private BigDecimal tariffRateVal;

    /**
     * 增值税金额
     */

    private BigDecimal addedTaxTateVal;

    /**
     * 消费税金额
     */

    private BigDecimal exciseTaxVal;


    private BigDecimal rdutyPaidVal;//(应缴)完税价格
    private BigDecimal rtariffRateVal;//(应缴)关税
    private BigDecimal raddedTaxTateVal;//(应缴)增值税
    private BigDecimal rexciseTaxVal;//(应缴)消费税

    /**
     * 行号
     */
    private Integer rowNo;


    private String receivingNoteNo;//入库单号
    private BigDecimal receivingQuantity;//绑定入库数量
}
