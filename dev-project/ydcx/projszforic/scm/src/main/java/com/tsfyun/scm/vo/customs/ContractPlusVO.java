package com.tsfyun.scm.vo.customs;

import lombok.Data;
import java.io.Serializable;
import java.util.List;

@Data
public class ContractPlusVO implements Serializable {

    private ContractVO main;

    private List<ContractMemberVO> members;
}
