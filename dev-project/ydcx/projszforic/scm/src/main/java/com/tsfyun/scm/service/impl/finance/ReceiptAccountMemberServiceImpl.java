package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.finance.ReceiptAccountMember;
import com.tsfyun.scm.mapper.finance.ReceiptAccountMemberMapper;
import com.tsfyun.scm.service.finance.IReceiptAccountMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.finance.ReceiptAccountMemberVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 收款核销明细 服务实现类
 * </p>
 *

 * @since 2020-05-22
 */
@Service
public class ReceiptAccountMemberServiceImpl extends ServiceImpl<ReceiptAccountMember> implements IReceiptAccountMemberService {

    @Override
    public List<ReceiptAccountMember> getByReceiptAccountId(Long receiptAccountId) {
        ReceiptAccountMember condition = new ReceiptAccountMember();
        condition.setReceiptAccountId(receiptAccountId);
        List<ReceiptAccountMember> list = super.list(condition);
        //按创建时间降序排
        if(CollUtil.isNotEmpty(list)) {
            list = list.stream().sorted(Comparator.comparing(ReceiptAccountMember::getDateCreated).reversed()).collect(Collectors.toList());
        }
        return list;
    }

    @Override
    public List<ReceiptAccountMemberVO> getVoByReceiptAccountId(Long receiptAccountId) {
        List<ReceiptAccountMember> list = getByReceiptAccountId(receiptAccountId);
        return beanMapper.mapAsList(list,ReceiptAccountMemberVO.class);
    }

    @Override
    public List<ReceiptAccountMember> getByImpOrderCostId(Long costId) {
        if(Objects.isNull(costId)){
            return new ArrayList<>();
        }
        ReceiptAccountMember condition = new ReceiptAccountMember();
        condition.setImpOrderCostId(costId);
        return super.list(condition);
    }

    @Override
    public List<ReceiptAccountMember> getByExpOrderCostId(Long costId) {
        if(Objects.isNull(costId)){
            return new ArrayList<>();
        }
        ReceiptAccountMember condition = new ReceiptAccountMember();
        condition.setExpOrderCostId(costId);
        return super.list(condition);
    }

    @Override
    public List<ReceiptAccountMember> getByTrustOrderCostId(Long costId) {
        if(Objects.isNull(costId)){
            return new ArrayList<>();
        }
        ReceiptAccountMember condition = new ReceiptAccountMember();
        condition.setTrustOrderCostId(costId);
        return super.list(condition);
    }

    @Override
    public List<ReceiptAccountMember> getByRefundAccountId(Long refundAccountId) {
        if(Objects.isNull(refundAccountId)){
            return new ArrayList<>();
        }
        ReceiptAccountMember condition = new ReceiptAccountMember();
        condition.setRefundAccountId(refundAccountId);
        return super.list(condition);
    }
}
