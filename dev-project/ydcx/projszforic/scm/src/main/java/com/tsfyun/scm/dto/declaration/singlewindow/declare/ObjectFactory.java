//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tsfyun.scm.dto.declaration.singlewindow package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tsfyun.scm.dto.declaration.singlewindow
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DecMessage }
     * 
     */
    public DecMessage createDecMessage() {
        return new DecMessage();
    }

    /**
     * Create an instance of {@link DecListItemType }
     * 
     */
    public DecListItemType createDecListItemType() {
        return new DecListItemType();
    }

    /**
     * Create an instance of {@link DecHeadType }
     * 
     */
    public DecHeadType createDecHeadType() {
        return new DecHeadType();
    }

    /**
     * Create an instance of {@link DecMessage.DecLists }
     * 
     */
    public DecMessage.DecLists createDecMessageDecLists() {
        return new DecMessage.DecLists();
    }

    /**
     * Create an instance of {@link DecMessage.DecContainers }
     * 
     */
    public DecMessage.DecContainers createDecMessageDecContainers() {
        return new DecMessage.DecContainers();
    }

    /**
     * Create an instance of {@link DecMessage.DecLicenseDocus }
     * 
     */
    public DecMessage.DecLicenseDocus createDecMessageDecLicenseDocus() {
        return new DecMessage.DecLicenseDocus();
    }

    /**
     * Create an instance of {@link DecMessage.DecRequestCerts }
     * 
     */
    public DecMessage.DecRequestCerts createDecMessageDecRequestCerts() {
        return new DecMessage.DecRequestCerts();
    }

    /**
     * Create an instance of {@link DecMessage.DecOtherPacks }
     * 
     */
    public DecMessage.DecOtherPacks createDecMessageDecOtherPacks() {
        return new DecMessage.DecOtherPacks();
    }

    /**
     * Create an instance of {@link DecMessage.DecCopLimits }
     * 
     */
    public DecMessage.DecCopLimits createDecMessageDecCopLimits() {
        return new DecMessage.DecCopLimits();
    }

    /**
     * Create an instance of {@link DecMessage.DecUsers }
     * 
     */
    public DecMessage.DecUsers createDecMessageDecUsers() {
        return new DecMessage.DecUsers();
    }

    /**
     * Create an instance of {@link DecMessage.DecMarkLobs }
     * 
     */
    public DecMessage.DecMarkLobs createDecMessageDecMarkLobs() {
        return new DecMessage.DecMarkLobs();
    }

    /**
     * Create an instance of {@link DecFreeTxtType }
     * 
     */
    public DecFreeTxtType createDecFreeTxtType() {
        return new DecFreeTxtType();
    }

    /**
     * Create an instance of {@link EdocRealationType }
     * 
     */
    public EdocRealationType createEdocRealationType() {
        return new EdocRealationType();
    }

    /**
     * Create an instance of {@link ECORealationType }
     * 
     */
    public ECORealationType createECORealationType() {
        return new ECORealationType();
    }

    /**
     * Create an instance of {@link DecMessage.DecCopPromises }
     * 
     */
    public DecMessage.DecCopPromises createDecMessageDecCopPromises() {
        return new DecMessage.DecCopPromises();
    }

    /**
     * Create an instance of {@link DecRoyaltyFeeType }
     * 
     */
    public DecRoyaltyFeeType createDecRoyaltyFeeType() {
        return new DecRoyaltyFeeType();
    }

    /**
     * Create an instance of {@link DecSupplementSignType }
     * 
     */
    public DecSupplementSignType createDecSupplementSignType() {
        return new DecSupplementSignType();
    }

    /**
     * Create an instance of {@link DecRequestCertType }
     * 
     */
    public DecRequestCertType createDecRequestCertType() {
        return new DecRequestCertType();
    }

    /**
     * Create an instance of {@link DecCopPromiseType }
     * 
     */
    public DecCopPromiseType createDecCopPromiseType() {
        return new DecCopPromiseType();
    }

    /**
     * Create an instance of {@link DecContainerType }
     * 
     */
    public DecContainerType createDecContainerType() {
        return new DecContainerType();
    }

    /**
     * Create an instance of {@link DecSupplementListType }
     * 
     */
    public DecSupplementListType createDecSupplementListType() {
        return new DecSupplementListType();
    }

    /**
     * Create an instance of {@link DecGoodsLimitVinType }
     * 
     */
    public DecGoodsLimitVinType createDecGoodsLimitVinType() {
        return new DecGoodsLimitVinType();
    }

    /**
     * Create an instance of {@link DecUserType }
     * 
     */
    public DecUserType createDecUserType() {
        return new DecUserType();
    }

    /**
     * Create an instance of {@link DecOtherPackType }
     * 
     */
    public DecOtherPackType createDecOtherPackType() {
        return new DecOtherPackType();
    }

    /**
     * Create an instance of {@link DecMarkLobType }
     * 
     */
    public DecMarkLobType createDecMarkLobType() {
        return new DecMarkLobType();
    }

    /**
     * Create an instance of {@link DecLicenseType }
     * 
     */
    public DecLicenseType createDecLicenseType() {
        return new DecLicenseType();
    }

    /**
     * Create an instance of {@link DecCopLimitType }
     * 
     */
    public DecCopLimitType createDecCopLimitType() {
        return new DecCopLimitType();
    }

    /**
     * Create an instance of {@link DecGoodsLimitType }
     * 
     */
    public DecGoodsLimitType createDecGoodsLimitType() {
        return new DecGoodsLimitType();
    }

    /**
     * Create an instance of {@link DecSDResponseType }
     * 
     */
    public DecSDResponseType createDecSDResponseType() {
        return new DecSDResponseType();
    }

    /**
     * Create an instance of {@link DecListItemType.DecGoodsLimits }
     * 
     */
    public DecListItemType.DecGoodsLimits createDecListItemTypeDecGoodsLimits() {
        return new DecListItemType.DecGoodsLimits();
    }

}
