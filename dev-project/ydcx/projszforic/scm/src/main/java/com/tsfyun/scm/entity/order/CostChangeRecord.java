package com.tsfyun.scm.entity.order;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 费用变更记录
 * </p>
 *

 * @since 2020-05-27
 */
@Data
public class CostChangeRecord extends BaseEntity {

     private static final long serialVersionUID=1L;

     private Long impOrderId;

    /**
     * 订单号
     */

    private String impOrderNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 费用科目编码
     */

    private String expenseSubjectId;

    /**
     * 费用科目名称
     */

    private String expenseSubjectName;

    /**
     * 原始金额
     */

    private BigDecimal originCostAmount;

    /**
     * 变更后金额
     */

    private BigDecimal costAmount;

    /**
     * 备注
     */

    private String memo;


}
