package com.tsfyun.scm.config.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Description: 微信公众号配置
 * @CreateDate: Created in 2020/12/23 12:04
 */
@RefreshScope
@Data
@Component
public class WxGzhProperties {

    @Value("${weChat.wxgzhAppid}")
    private String wxgzhAppid;

    @Value("${weChat.wxgzhSecret}")
    private String wxgzhSecret;

    @Value("${weChat.timeout:4000}")
    private Integer timeout;

}
