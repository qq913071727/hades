package com.tsfyun.scm.dto.customs;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 报关单请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="Declaration确认请求对象", description="报关单确认请求实体")
public class ConfirmDeclarationDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "报关单id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "单据类型不能为空")
   private String billType;

   @NotEmptyTrim(message = "申报单位名称不能为空")
   @LengthTrim(max = 100,message = "申报单位名称最大长度不能超过100位")
   private String agentName;
   @NotEmptyTrim(message = "申报单位18位社会信用代码不能为空")
   @LengthTrim(min = 18,max = 18,message = "申报单位18位社会信用代码必须18位")
   private String agentScc;
   @NotEmptyTrim(message = "申报单位10位海关代码不能为空")
   @LengthTrim(max = 10,message = "申报单位10位海关代码最大长度不能超过10位")
   private String agentCode;

   @NotEmptyTrim(message = "合同协议号不能为空")
   @LengthTrim(max = 20,message = "合同协议号最大长度不能超过20位")
   private String contrNo;

   @NotEmptyTrim(message = "申报地海关不能为空")
   @LengthTrim(max = 10,message = "申报地海关最大长度不能超过10位")
   @ApiModelProperty(value = "申报地海关")
   private String customMasterCode;
   @NotEmptyTrim(message = "申报地海关不能为空")
   @LengthTrim(max = 20,message = "申报地海关最大长度不能超过20位")
   @ApiModelProperty(value = "申报地海关")
   private String customMasterName;

   @NotEmptyTrim(message = "进境关别不能为空")
   @LengthTrim(max = 10,message = "进境关别最大长度不能超过10位")
   @ApiModelProperty(value = "进境关别")
   private String iePortCode;
   @NotEmptyTrim(message = "进境关别不能为空")
   @LengthTrim(max = 20,message = "进境关别最大长度不能超过20位")
   @ApiModelProperty(value = "进境关别")
   private String iePortName;

   @NotEmptyTrim(message = "入境口岸不能为空")
   @LengthTrim(max = 10,message = "入境口岸最大长度不能超过10位")
   @ApiModelProperty(value = "入境口岸")
   private String ciqEntyPortCode;
   @NotEmptyTrim(message = "入境口岸不能为空")
   @LengthTrim(max = 20,message = "入境口岸最大长度不能超过20位")
   @ApiModelProperty(value = "入境口岸")
   private String ciqEntyPortName;

   @NotEmptyTrim(message = "启运国(地区)不能为空")
   @LengthTrim(max = 10,message = "启运国(地区)最大长度不能超过10位")
   @ApiModelProperty(value = "启运国(地区)")
   private String cusTradeCountryCode;
   @NotEmptyTrim(message = "启运国(地区)不能为空")
   @LengthTrim(max = 20,message = "启运国(地区)最大长度不能超过20位")
   @ApiModelProperty(value = "启运国(地区)")
   private String cusTradeCountryName;

   @NotEmptyTrim(message = "贸易国别(地区)不能为空")
   @LengthTrim(max = 10,message = "贸易国别(地区)	最大长度不能超过10位")
   @ApiModelProperty(value = "贸易国别(地区)")
   private String cusTradeNationCode;
   @NotEmptyTrim(message = "贸易国别(地区)不能为空")
   @LengthTrim(max = 20,message = "贸易国别(地区)	最大长度不能超过20位")
   @ApiModelProperty(value = "贸易国别(地区)")
   private String cusTradeNationName;

   @LengthTrim(max = 10,message = "启运港最大长度不能超过10位")
   @ApiModelProperty(value = "启运港")
   private String destPortCode;
   @LengthTrim(max = 20,message = "启运港最大长度不能超过20位")
   @ApiModelProperty(value = "启运港")
   private String destPortName;

   @NotEmptyTrim(message = "经停港不能为空")
   @LengthTrim(max = 10,message = "经停港最大长度不能超过10位")
   @ApiModelProperty(value = "经停港")
   private String distinatePortCode;
   @NotEmptyTrim(message = "经停港不能为空")
   @LengthTrim(max = 20,message = "经停港最大长度不能超过20位")
   @ApiModelProperty(value = "经停港")
   private String distinatePortName;

   @NotEmptyTrim(message = "运输方式不能为空")
   @LengthTrim(max = 10,message = "运输方式最大长度不能超过10位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafModeCode;
   @NotEmptyTrim(message = "运输方式不能为空")
   @LengthTrim(max = 20,message = "运输方式最大长度不能超过20位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafModeName;

   @NotEmptyTrim(message = "监管方式不能为空")
   @LengthTrim(max = 10,message = "监管方式最大长度不能超过10位")
   @ApiModelProperty(value = "监管方式")
   private String supvModeCdde;
   @NotEmptyTrim(message = "监管方式不能为空")
   @LengthTrim(max = 20,message = "监管方式最大长度不能超过20位")
   @ApiModelProperty(value = "监管方式")
   private String supvModeCddeName;

   @NotEmptyTrim(message = "征免性质不能为空")
   @LengthTrim(max = 10,message = "征免性质最大长度不能超过10位")
   @ApiModelProperty(value = "征免性质")
   private String cutModeCode;
   @NotEmptyTrim(message = "征免性质不能为空")
   @LengthTrim(max = 20,message = "征免性质最大长度不能超过20位")
   @ApiModelProperty(value = "征免性质")
   private String cutModeName;

   @NotEmptyTrim(message = "包装种类不能为空")
   @LengthTrim(max = 10,message = "包装种类最大长度不能超过10位")
   @ApiModelProperty(value = "包装种类")
   private String wrapTypeCode;
   @NotEmptyTrim(message = "包装种类不能为空")
   @LengthTrim(max = 20,message = "包装种类最大长度不能超过20位")
   @ApiModelProperty(value = "包装种类")
   private String wrapTypeName;

   @LengthTrim(max = 50,message = "备案号最大长度不能超过50位")
   private String recordNo;
   @LengthTrim(max = 50,message = "许可证号最大长度不能超过50位")
   private String licenseNo;
   @LengthTrim(max = 32,message = "航次号(六联单号)最大长度不能超过32位")
   private String cusVoyageNo;
   @LengthTrim(max = 50,message = "集装箱号最大长度不能超过50位")
   private String containerNo;
   @LengthTrim(max = 20,message = "提运单号最大长度不能超过20位")
   private String billNo;

   @NotEmptyTrim(message = "货物存放地不能为空")
   @LengthTrim(max = 200,message = "货物存放地最大长度不能超过200位")
   @ApiModelProperty(value = "货物存放地")
   private String goodsPlace;

   @NotEmptyTrim(message = "标记唛码不能为空")
   @LengthTrim(max = 400,message = "标记唛码最大长度不能超过400位")
   @ApiModelProperty(value = "标记唛码")
   private String markNo;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   private String memo;
}
