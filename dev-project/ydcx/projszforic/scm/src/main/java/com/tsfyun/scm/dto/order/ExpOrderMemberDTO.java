package com.tsfyun.scm.dto.order;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 出口订单明细请求实体
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Data
@ApiModel(value="ExpOrderMember请求对象", description="出口订单明细请求实体")
public class ExpOrderMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @ApiModelProperty(value = "订单明细id")
   private Long id;

   private Integer rowNo;

   @LengthTrim(max = 32,message = "物料ID最大长度不能超过32位")
   @ApiModelProperty(value = "物料ID")
   private String materialId;

   @NotEmptyTrim(message = "产品名称不能为空")
   @LengthTrim(max = 100,message = "产品名称最大长度不能超过100位")
   @ApiModelProperty(value = "产品名称")
   private String name;

   @NotEmptyTrim(message = "产品型号不能为空")
   @LengthTrim(max = 100,message = "产品型号最大长度不能超过100位")
   @ApiModelProperty(value = "产品型号")
   private String model;

   @NotEmptyTrim(message = "产品品牌不能为空")
   @LengthTrim(max = 100,message = "产品品牌最大长度不能超过100位")
   @ApiModelProperty(value = "产品品牌")
   private String brand;

   @LengthTrim(max = 100,message = "物料编号最大长度不能超过100位")
   @ApiModelProperty(value = "物料编号")
   private String goodsCode;

   @NotEmptyTrim(message = "产品规格描述不能为空")
   @LengthTrim(max = 500,message = "产品规格描述最大长度不能超过500位")
   @ApiModelProperty(value = "产品规格描述")
   private String spec;

   @ApiModelProperty(value = "单位")
   private String unitCode;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "总价不能为空")
   @Digits(integer = 8, fraction = 2, message = "总价整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "总价")
   private BigDecimal totalPrice;

   @NotNull(message = "净重不能为空")
   @Digits(integer = 6, fraction = 4, message = "净重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "净重")
   private BigDecimal netWeight;

   @NotNull(message = "毛重不能为空")
   @Digits(integer = 6, fraction = 4, message = "毛重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "毛重")
   private BigDecimal grossWeight;

   @NotNull(message = "箱数不能为空")
   @LengthTrim(max = 6,message = "箱数最大长度不能超过6位")
   @ApiModelProperty(value = "箱数")
   private Integer cartonNum;

   @LengthTrim(max = 200,message = "箱号最大长度不能超过200位")
   @ApiModelProperty(value = "箱号")
   private String cartonNo;

}
