package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.scm.enums.SubjectTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 结汇主单请求实体
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Data
@ApiModel(value="SettlementAccount请求对象", description="结汇主单请求实体")
public class SettlementAccountDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "收款单ID不能为空")
   private String accountId;

   @NotNull(message = "结汇时间不能为空")
   private LocalDateTime settlementDate;

   @NotEmptyTrim(message = "结汇主体类型不能为空")
   @EnumCheck(clazz = SubjectTypeEnum.class,message = "结汇主体类型错误")
   @ApiModelProperty(value = "结汇主体类型")
   private String subjectType;

   @NotNull(message = "结汇主体不能为空")
   @ApiModelProperty(value = "结汇主体")
   private Long subjectId;

   @NotNull(message = "结汇银行不能为空")
   private Long subjectBank;

   @NotNull(message = "结汇汇率不能为空")
   @Digits(integer = 4, fraction = 6, message = "结汇汇率整数位不能超过4位，小数位不能超过6")
   @DecimalMin(value = "0.000001",message = "结汇汇率必须大于0")
   @ApiModelProperty(value = "结汇汇率")
   private BigDecimal customsRate;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
