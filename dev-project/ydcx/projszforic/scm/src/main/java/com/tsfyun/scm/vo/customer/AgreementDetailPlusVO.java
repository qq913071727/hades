package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**=
 * 协议详情
 */
@Data
public class AgreementDetailPlusVO implements Serializable {

    private AgreementDetailVO agreement;

    /**
     * 进口条款
     */
    private ImpClauseVO impClause;

    /**
     * 进口报价信息
     */
    private List<ImpQuoteVO> impQuotes;

}
