package com.tsfyun.scm.service.support;


/**
 * @Description: PDF生成接口服务类
 * @since Created in 2020/4/28 10:29
 */
public interface IPdfService {

    /**
     * 打印入库单标签
     * @param id
     * @return
     */
    Long printReceivingNoteLabel(Long id);

}
