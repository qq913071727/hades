package com.tsfyun.scm.controller.order;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 出口订单明细 前端控制器
 * </p>
 *
 *
 * @since 2021-01-26
 */
@RestController
@RequestMapping("/expOrderMember")
public class ExpOrderMemberController extends BaseController {

}

