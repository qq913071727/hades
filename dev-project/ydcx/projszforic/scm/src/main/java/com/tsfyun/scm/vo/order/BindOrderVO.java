package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.customs.TrafModeCodeEnum;
import com.tsfyun.common.base.enums.domain.DeclarationStatusEnum;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import com.tsfyun.scm.enums.DeliveryDestinationEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description: 跨境运输单可绑定订单响应实体
 * @since Created in 2020/5/13 18:15
 */
@Data
public class BindOrderVO implements Serializable {

    /**
     * 订单id
     */
    private Long id;

    /**
     * 订单编号
     */
    private String docNo;

    /**
     * 订单状态
     */
    private String statusId;

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 订单日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;

    /**
     * 总件数
     */
    private Integer packNo;

    /**
     * 报关单id
     */
    private Long declareId;

    /**
     * 系统报关单号
     */
    private String declarationNo;

    /**=
     * 外贸合同号
     */
    private String contrNo;
    /**
     * 包车
     */
    private Boolean isCharterCar;
    /**=
     * (进口)国内送货方式
     */
    private String receivingMode;
    private String receivingModeName;
    /**
     * (出口)香港交货方式
     */
    private String deliveryDestination;
    private String deliveryDestinationDesc;
    /**
     * 收货公司
     */

    private String deliveryCompanyName;

    /**
     * 收货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 收货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 收货地址
     */

    private String deliveryAddress;


    /**
     * 报关单状态
     */
    private String declareStatusId;


    /**
     * 报关单状态描述
     */
    private String declareStatusDesc;

    /**
     * 订单状态描述
     */
    private String statusDesc;

    /**
     * 进境关别
     */

    private String iePortCode;

    /**
     * 进境关别
     */

    private String iePortName;


    public String getStatusDesc() {
        return Optional.ofNullable(ImpOrderStatusEnum.of(statusId)).map(ImpOrderStatusEnum::getName).orElse("");
    }

    public String getDeclareStatusDesc() {
        return Optional.ofNullable(DeclarationStatusEnum.of(declareStatusId)).map(DeclarationStatusEnum::getName).orElse("");
    }

    public String getDeliveryDestinationDesc(){
        return Optional.ofNullable(DeliveryDestinationEnum.of(deliveryDestination)).map(DeliveryDestinationEnum::getName).orElse("");
    }
}
