package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 员工导入excel实体映射类
 */
@Data
public class PersonExcel extends BaseRowModel implements Serializable {

    @NotEmptyTrim(message = "员工编号不能为空")
    @ExcelProperty(value = "员工编号", index = 0)
    private String code;

    @NotEmptyTrim(message = "员工姓名不能为空")
    @ExcelProperty(value = "员工姓名", index = 1)
    private String name;

    @ExcelProperty(value = "性别", index = 2)
    private String genderName;

    @NotEmptyTrim(message = "手机号码不能为空")
    @Pattern(regexp = "^1\\d{10}$",message = "手机号码格式不正确")
    @ExcelProperty(value = "手机号码", index = 3)
    private String phone;

    @NotEmptyTrim(message = "邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    @ExcelProperty(value = "邮箱", index = 4)
    private String mail;

    @NotEmptyTrim(message = "职位不能为空")
    @ExcelProperty(value = "职位", index = 5)
    private String positionName;

    @NotEmptyTrim(message = "部门不能为空")
    @ExcelProperty(value = "部门", index = 6)
    private String departmentName;

    @ExcelProperty(value = "备注", index = 7)
    private String memo;


}
