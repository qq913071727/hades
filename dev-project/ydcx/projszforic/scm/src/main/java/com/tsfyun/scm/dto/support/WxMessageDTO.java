package com.tsfyun.scm.dto.support;

import lombok.Data;

/**
 * @Description: 微信公众号响应内容
 * @CreateDate: Created in 2020/12/29 15:08
 */
@Data
public class WxMessageDTO {
	/**
	 * 发送方帐号（open_id）
	 */
	private String fromUserName;
	/**
	 * 公众帐号(JWID)
	 */
	private String toUserName;
	/**
	 * 消息类型
	 */
	private String msgType;
	/**
	 * 消息id
	 */
	private String msgId;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 多媒体ID
	 */
	private String mediaId;
	/**
	 * 菜单Key
	 */
	private String key;
	/**
	 * Event
	 */
	private String Event;
	/**
	 * 捷微公众账号ID(全局的数据权限ID)
	 */
	private String sysAccountId;
	/**
	 * 默认返回的文本消息内容
	 */
	private String respContent;
	/**
	 * 模板ID
	 */
	private String templateId;

	
}
