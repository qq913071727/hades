package com.tsfyun.scm.job;

import com.tsfyun.scm.service.customer.IAgreementService;
import com.tsfyun.scm.service.customer.IExpAgreementService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description: 协议定时器
 * @CreateDate: Created in 2021/9/16 09:56
 */
@JobHandler(value="agreementJobHandler")
@Component
@Slf4j
public class AgreementJobHandler extends IJobHandler {
    @Autowired
    private IAgreementService agreementService;
    @Autowired
    private IExpAgreementService expAgreementService;
    @Override
    public ReturnT<String> execute(String s) throws Exception {
        //失效进口服务协议状态
        log.info("定时处理进口服务协议失效");
        agreementService.scheduleInvalidImpAgreement();

        //失效出口报价协议状态
        log.info("定时处理出口协议失效");
        expAgreementService.scheduleInvalidExpAgreement();
        return ReturnT.SUCCESS;
    }

}
