package com.tsfyun.scm.service.impl.wms;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.wms.DeliveryNoteMember;
import com.tsfyun.scm.mapper.wms.DeliveryNoteMemberMapper;
import com.tsfyun.scm.service.wms.IDeliveryNoteMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.wms.DeliveryNoteMemberVO;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Service
public class DeliveryNoteMemberServiceImpl extends ServiceImpl<DeliveryNoteMember> implements IDeliveryNoteMemberService {

    @Override
    public List<DeliveryNoteMemberVO> findByDeliveryNoteId(Long deliveryNoteId) {
        DeliveryNoteMember condition = new DeliveryNoteMember();
        condition.setDeliveryNoteId(deliveryNoteId);
        List<DeliveryNoteMember> list = super.list(condition);
        if(CollUtil.isNotEmpty(list)) {
            return beanMapper.mapAsList(list,DeliveryNoteMemberVO.class).stream().sorted(Comparator.comparing(DeliveryNoteMemberVO::getModel)).collect(toList());
        }
        return null;
    }

    @Override
    public DeliveryNoteMember findByReceivingNoteMemberId(Long receivingNoteMemberId) {
        if(Objects.isNull(receivingNoteMemberId)){
            return null;
        }
        DeliveryNoteMember query = new DeliveryNoteMember();
        query.setReceivingNoteMemberId(receivingNoteMemberId);
        return super.getOne(query);
    }
}
