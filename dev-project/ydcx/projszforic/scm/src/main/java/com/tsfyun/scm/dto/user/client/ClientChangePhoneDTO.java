package com.tsfyun.scm.dto.user.client;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Description: 修改手机号码
 * @CreateDate: Created in 2020/9/25 15:18
 */
@Data
public class ClientChangePhoneDTO implements Serializable {

    @ApiModelProperty(value = "手机号码")
    @NotEmptyTrim(message = "手机号码不能为空")
    @Pattern(regexp = "^[1][0-9]{10}$",message = "手机号格式错误")
    private String phoneNo;

    @ApiModelProperty(value = "短信验证码")
    @NotEmptyTrim(message = "短信验证码不能为空")
    @LengthTrim(min = 4,max = 4,message = "请输入4位短信验证码")
    private String validateCode;


}
