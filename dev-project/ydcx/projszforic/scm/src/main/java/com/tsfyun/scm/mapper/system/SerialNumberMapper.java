package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.entity.system.SerialNumber;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-16
 */
@Repository
public interface SerialNumberMapper extends Mapper<SerialNumber> {

}
