package com.tsfyun.scm.service.system;

import com.tsfyun.scm.dto.system.MailConfigDTO;
import com.tsfyun.scm.dto.system.TestMailConfigDTO;
import com.tsfyun.scm.entity.system.MailConfig;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.MailConfigVO;

import java.util.List;

/**
 * <p>
 *  邮件配置接口类
 * </p>
 *

 * @since 2020-03-19
 */
public interface IMailConfigService extends IService<MailConfig> {

    /**
     * 获取可用的邮件配置数据，后续加缓存
     * @return
     */
    List<MailConfig> openList();

    //新增
    void add(MailConfigDTO dto);

    //修改
    void edit(MailConfigDTO dto);


    //删除
    void delete(Long id);

    //获取所有的邮箱配置（含禁用的）
    List<MailConfigVO> allList( );

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 详情
     * @param id
     * @return
     */
    MailConfigVO detail(Long id);

    /**
     * 设置/取消默认
     * @param id
     */
    void setDefault(Long id,Boolean isDefault);

    /**
     * 测试连接
     * @param dto
     * @return
     */
    Boolean checkConnect(TestMailConfigDTO dto);

}
