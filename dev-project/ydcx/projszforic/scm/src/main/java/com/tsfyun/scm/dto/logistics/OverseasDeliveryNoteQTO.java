package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/9 17:11
 */
@Data
public class OverseasDeliveryNoteQTO extends PaginationDto {

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 出库单号
     */

    private String docNo;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 订单编号
     */

    private String orderNo;


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
