package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

@Data
public class TransportSupplierQTO extends PaginationDto implements Serializable {

    private String name;

    private String linkPerson;

    private Boolean disabled;

}
