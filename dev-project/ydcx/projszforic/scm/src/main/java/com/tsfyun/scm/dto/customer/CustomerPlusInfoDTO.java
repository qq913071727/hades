package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.FileDTO;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 客户信息大页面请求参数
 */
@Data
public class CustomerPlusInfoDTO implements Serializable {

    /**
     * 客户信息
     */
    @NotNull(message = "请填写客户信息")
    @Valid
    private CustomerDTO customer;

    /**
     * 客户收货地址
     */
    @Valid
    private List<CustomerDeliveryInfoDTO> deliverys;

    /**
     * 客户境外收货信息
     */
    @Valid
    private List<CustomerAbroadDeliveryInfoDTO> abroadDeliverys;

    /**
     * 客户银行信息
     */
    @Valid
    private List<CustomerBankDTO> banks;

    /**=
     * 文件信息
     */
    private FileDTO file;

}
