package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 价格波动历史 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-17
 */
@Repository
public interface PriceFluctuationHistoryMapper extends Mapper<PriceFluctuationHistory> {

}
