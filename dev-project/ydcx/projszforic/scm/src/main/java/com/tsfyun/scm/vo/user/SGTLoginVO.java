package com.tsfyun.scm.vo.user;

import lombok.Data;

import java.io.Serializable;
@Data
public class SGTLoginVO implements Serializable {

    private String personName;// 人员名称
    private Long customerId;// 客户ID
    private String customerName;// 客户名称
    private String enterpriseName;// 备案企业名称
}
