package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;

@Data
public class AuxiliaryInspectionExcel extends BaseRowModel implements Serializable {
    @ExcelProperty(value = "箱号", index = 0)
    private String rowNo;
    @ExcelProperty(value = "型号", index = 1)
    private String model;
    @ExcelProperty(value = "品牌", index = 2)
    private String brand;
    @ExcelProperty(value = "数量", index = 3)
    private String quantity;
    @ExcelProperty(value = "箱数", index = 4)
    private String carton;
    @ExcelProperty(value = "净重", index = 5)
    private String netWeight;
    @ExcelProperty(value = "毛重", index = 6)
    private String grossWeight;
    @ExcelProperty(value = "产地", index = 7)
    private String country;
}
