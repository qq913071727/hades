package com.tsfyun.scm.service.impl.logistics;

import com.tsfyun.scm.entity.logistics.DeliveryWaybillMember;
import com.tsfyun.scm.mapper.logistics.DeliveryWaybillMemberMapper;
import com.tsfyun.scm.service.logistics.IDeliveryWaybillMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 送货单明细 服务实现类
 * </p>
 *

 * @since 2020-05-25
 */
@Service
public class DeliveryWaybillMemberServiceImpl extends ServiceImpl<DeliveryWaybillMember> implements IDeliveryWaybillMemberService {

    @Autowired
    private DeliveryWaybillMemberMapper deliveryWaybillMemberMapper;

    @Override
    public List<DeliveryWaybillMemberVO> findByDeliveryId(Long deliveryId) {
        return deliveryWaybillMemberMapper.findByDeliveryId(deliveryId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeByDeliveryId(Long deliveryId) {
        deliveryWaybillMemberMapper.removeByDeliveryId(deliveryId);
    }
}
