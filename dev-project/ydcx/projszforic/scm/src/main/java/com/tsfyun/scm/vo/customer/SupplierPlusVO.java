package com.tsfyun.scm.vo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupplierPlusVO implements Serializable {

    //供应商信息
    private SupplierVO supplier;

    //供应商提货信息
    private List<SupplierTakeInfoVO> takes;

    //供应商银行信息
    private List<SupplierBankVO> banks;

    public SupplierPlusVO(SupplierVO supplier) {
        this.supplier = supplier;
    }

}
