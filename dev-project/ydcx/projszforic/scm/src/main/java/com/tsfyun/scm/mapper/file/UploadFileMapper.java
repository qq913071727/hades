package com.tsfyun.scm.mapper.file;

import com.tsfyun.common.base.dto.FileDocIdDTO;
import com.tsfyun.common.base.dto.FileQTO;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.common.base.vo.FileQtyVO;
import com.tsfyun.scm.entity.file.UploadFile;
import com.tsfyun.scm.vo.base.ClientFileVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-04
 */
@Repository
public interface UploadFileMapper extends Mapper<UploadFile> {

    void deletes(List<Long> id);

    Integer count(FileQTO fileQTO);

    List<FileQtyVO> countGroup(FileQTO fileQTO);

    List<ClientFileVO> findByFile(@Param(value = "docId") String docId,@Param(value = "docType") String docType,@Param(value = "businessType") String businessType);
    //失效其它文件
    Integer invalidOthers(@Param(value = "id") Long id,@Param(value = "docId") String docId,@Param(value = "docType") String docType,@Param(value = "businessType") String businessType);

    Integer deleteByDocId(@Param(value = "docId")String docId);

    //修改文件ID
    Integer updateFileDocId(FileDocIdDTO fileDocIdDTO);

    List<String> getFilePath(@Param(value = "docId") String docId,@Param(value = "docType") String docType,@Param(value = "businessType") String businessType);
}
