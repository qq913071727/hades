package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TransactionFlowDTO;
import com.tsfyun.scm.dto.finance.TransactionFlowQTO;
import com.tsfyun.scm.dto.finance.client.ClientTransactionFlowQTO;
import com.tsfyun.scm.entity.finance.TransactionFlow;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.CustomerBalanceVO;
import com.tsfyun.scm.vo.finance.TransactionFlowVO;
import com.tsfyun.scm.vo.finance.client.ClientTransactionListVO;
import java.math.BigDecimal;

/**
 * <p>
 * 交易流水 服务类
 * </p>
 *

 * @since 2020-04-16
 */
public interface ITransactionFlowService extends IService<TransactionFlow> {

    /**
     * 登记交易流水
     * @param param
     */
    void recordTransactionFlow(TransactionFlowDTO param);

    /**
     * 获取客户余额
     * @param customerId
     * @return
     */
    BigDecimal getCustomerBalance(Long customerId);

    /**
     * 分页
     * @return
     */
    PageInfo<TransactionFlowVO> pageList(TransactionFlowQTO qto);

    /**
     * 分页
     * @return
     */
    Result<ClientTransactionListVO> clientPageList(ClientTransactionFlowQTO qto);

    /**
     * 获取客户余额、收入、支出
     * @param customerId
     * @return
     */
    CustomerBalanceVO getCustomerInoutBalance(Long customerId);


}
