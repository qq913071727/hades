package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.order.ImpOrderDomesticTakeQTO;
import com.tsfyun.scm.entity.order.ImpOrderLogistics;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ImpOrderDomesticVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsSimpleVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsVO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单物流信息 服务类
 * </p>
 *

 * @since 2020-04-08
 */
public interface IImpOrderLogisticsService extends IService<ImpOrderLogistics> {

    /**
     * 根据订单id获取订单物流信息
     * @param id
     * @return
     */
    ImpOrderLogisticsVO findById(Long id);

    /**
     * 获取最近货至客户指定地点的收货地址信息
     * @return
     */
    List<ImpOrderLogisticsSimpleVO> getCharterCarDeliveryInfo();

    /**
     * 根据订单id删除物流信息
     * @param orderId
     */
    void removeByImpOrderId(Long orderId);

    /**
     * 根据订单id获取物流信息
     * @param orderId
     * @return
     */
    ImpOrderLogistics findByOrderId(Long orderId);

    /**
     * 国内自提分页列表
     * @param qto
     * @return
     */
    PageInfo<ImpOrderDomesticVO> pageList(ImpOrderDomesticTakeQTO qto);

    /**
     * 提货完成确认
     * @param id
     */
    void takeConfirm(Long id, LocalDateTime takeTime);

}
