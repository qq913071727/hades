package com.tsfyun.scm.dto.customs;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeclarationMember请求对象", description="请求实体")
public class DeclarationMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long declarationId;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   @LengthTrim(max = 255,message = "规格参数最大长度不能超过255位")
   @ApiModelProperty(value = "规格参数")
   private String declareSpec;

   @NotEmptyTrim(message = "产地不能为空")
   @LengthTrim(max = 10,message = "产地最大长度不能超过10位")
   @ApiModelProperty(value = "产地")
   private String country;

   @NotEmptyTrim(message = "产地不能为空")
   @LengthTrim(max = 50,message = "产地最大长度不能超过50位")
   @ApiModelProperty(value = "产地")
   private String countryName;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitCode;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "单价(委托)不能为空")
   @Digits(integer = 6, fraction = 4, message = "单价(委托)整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "单价(委托)")
   private BigDecimal unitPrice;

   @NotNull(message = "总价(委托)不能为空")
   @Digits(integer = 8, fraction = 2, message = "总价(委托)整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "总价(委托)")
   private BigDecimal totalPrice;

   @NotNull(message = "净重不能为空")
   @Digits(integer = 6, fraction = 4, message = "净重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "净重")
   private BigDecimal netWeight;

   @NotNull(message = "毛重不能为空")
   @Digits(integer = 6, fraction = 4, message = "毛重整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "毛重")
   private BigDecimal grossWeight;

   @NotNull(message = "箱数不能为空")
   @LengthTrim(max = 11,message = "箱数最大长度不能超过11位")
   @ApiModelProperty(value = "箱数")
   private Integer cartonNum;

   private Integer rowNo;

   @NotEmptyTrim(message = "海关编码不能为空")
   @LengthTrim(max = 10,message = "海关编码最大长度不能超过10位")
   @ApiModelProperty(value = "海关编码")
   private String hsCode;

   @NotEmptyTrim(message = "单位1不能为空")
   @LengthTrim(max = 10,message = "单位1最大长度不能超过10位")
   @ApiModelProperty(value = "单位1")
   private String unit1Code;

   @NotEmptyTrim(message = "单位1不能为空")
   @LengthTrim(max = 10,message = "单位1最大长度不能超过10位")
   @ApiModelProperty(value = "单位1")
   private String unit1Name;

   @NotNull(message = "数量1不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量1整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量1")
   private BigDecimal quantity1;

   @LengthTrim(max = 10,message = "单位2最大长度不能超过10位")
   @ApiModelProperty(value = "单位2")
   private String unit2Code;

   @LengthTrim(max = 10,message = "单位2最大长度不能超过10位")
   @ApiModelProperty(value = "单位2")
   private String unit2Name;

   @Digits(integer = 8, fraction = 2, message = "数量2整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量2")
   private BigDecimal quantity2;

   @NotEmptyTrim(message = "最终目的国不能为空")
   @LengthTrim(max = 10,message = "最终目的国最大长度不能超过10位")
   @ApiModelProperty(value = "最终目的国")
   private String destinationCountry;

   @NotEmptyTrim(message = "最终目的国不能为空")
   @LengthTrim(max = 50,message = "最终目的国最大长度不能超过50位")
   @ApiModelProperty(value = "最终目的国")
   private String destinationCountryName;

   @NotEmptyTrim(message = "征免方式	不能为空")
   @LengthTrim(max = 5,message = "征免方式	最大长度不能超过5位")
   @ApiModelProperty(value = "征免方式	")
   private String dutyMode;

   @NotEmptyTrim(message = "征免方式	不能为空")
   @LengthTrim(max = 10,message = "征免方式	最大长度不能超过10位")
   @ApiModelProperty(value = "征免方式	")
   private String dutyModeName;

   @NotEmptyTrim(message = "境内目的地不能为空")
   @LengthTrim(max = 10,message = "境内目的地最大长度不能超过10位")
   @ApiModelProperty(value = "境内目的地")
   private String districtCode;

   @NotEmptyTrim(message = "境内目的地不能为空")
   @LengthTrim(max = 30,message = "境内目的地最大长度不能超过30位")
   @ApiModelProperty(value = "境内目的地")
   private String districtName;

   @NotEmptyTrim(message = "目的地代码不能为空")
   @LengthTrim(max = 10,message = "目的地代码最大长度不能超过10位")
   @ApiModelProperty(value = "目的地代码")
   private String ciqDestCode;

   @NotEmptyTrim(message = "目的地代码不能为空")
   @LengthTrim(max = 30,message = "目的地代码最大长度不能超过30位")
   @ApiModelProperty(value = "目的地代码")
   private String ciqDestName;


}
