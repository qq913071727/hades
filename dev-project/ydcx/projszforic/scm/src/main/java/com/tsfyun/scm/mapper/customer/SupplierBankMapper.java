package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.customer.SupplierBank;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.SupplierBankVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-12
 */
@Repository
public interface SupplierBankMapper extends Mapper<SupplierBank> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<SupplierBankVO> list(Map<String,Object> params);

    List<SupplierBankVO> listBySupplierId(@Param(value = "supplierId")Long supplierId);

    SupplierBank defSupplierBank(@Param(value = "supplierId")Long supplierId);

    List<SupplierBankVO> vague(@Param(value = "customerId")Long customerId,@Param(value = "supplierName")String supplierName,@Param(value = "bankName")String bankName);
    SupplierBankVO getSupplierBankInfo(@Param(value = "customerId")Long customerId,@Param(value = "supplierName")String supplierName,@Param(value = "bankName")String bankName);
    SupplierBankVO accurate(@Param(value = "customerId")Long customerId,@Param(value = "supplierName")String supplierName,@Param(value = "bankName")String bankName);

}
