package com.tsfyun.scm.service.logistics;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteBindOrderMemberDTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteBindQTO;
import com.tsfyun.scm.entity.logistics.OverseasDeliveryNoteMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNoteMemberStockVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2021-11-09
 */
public interface IOverseasDeliveryNoteMemberService extends IService<OverseasDeliveryNoteMember> {

    /**
     * 根据主单id查询明细
     * @param overseasDeliveryNoteId
     * @return
     */
    List<OverseasDeliveryNoteMember> findByOverseasDeliveryNoteId(Long overseasDeliveryNoteId);

    /**
     * 境外派送单选择订单明细
     * @param qto
     * @return
     */
    PageInfo<OverseasDeliveryNoteMemberStockVO> pageList(OverseasDeliveryNoteBindQTO qto);

    /**
     * 境外派送绑定订单明细
     * @param dto
     */
    void bind(OverseasDeliveryNoteBindOrderMemberDTO dto);

    /**
     * 根据主单id删除
     * @param overseasDeliveryNoteId
     */
    void deleteByOverseasDeliveryNoteId(Long overseasDeliveryNoteId);

    /**
     * 根据明细id集合删除
     * @param ids
     */
    void deleteByIds(Long overseasDeliveryId,List<Long> ids);

}
