package com.tsfyun.scm.vo.logistics;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @since Created in 2020/3/26 13:33
 */
@Data
public class SimpleDomesticLogisticsVO implements Serializable {

    private Long id;

    private String code;

    private String name;

    private List<String> timeline;

}
