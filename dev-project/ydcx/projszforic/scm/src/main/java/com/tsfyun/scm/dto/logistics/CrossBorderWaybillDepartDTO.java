package com.tsfyun.scm.dto.logistics;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 跨境运输单发车
 * @since Created in 2020/5/14 16:31
 */
@Data
public class CrossBorderWaybillDepartDTO implements Serializable {

    /**
     * 跨境运输单id
     */
    private Long id;

    /**
     * 发车时间
     */
    @NotNull(message = "请选择发车时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime departureDate;

    /**
     * 封条号
     */
    private String sealNo;

}
