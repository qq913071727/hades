package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/18 17:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpPaymentAccountPlusVO implements Serializable {

    private ExpPaymentAccountVO expPaymentAccount;

//    private OverseasReceivingAccountVO overseasReceivingAccount;

}
