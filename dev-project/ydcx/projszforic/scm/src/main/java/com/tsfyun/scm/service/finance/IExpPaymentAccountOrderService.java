package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.ExpPaymentAccountOrder;
import com.tsfyun.common.base.extension.IService;

/**
 * <p>
 * 付款订单明细 服务类
 * </p>
 *
 *
 * @since 2021-11-17
 */
public interface IExpPaymentAccountOrderService extends IService<ExpPaymentAccountOrder> {

    void removeByPaymentId(Long paymentId);
}
