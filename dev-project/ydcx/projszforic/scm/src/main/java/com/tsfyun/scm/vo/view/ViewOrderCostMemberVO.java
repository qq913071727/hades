package com.tsfyun.scm.vo.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.QuoteTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 订单费用明细
 */
@Data
public class ViewOrderCostMemberVO implements Serializable {
    private Long id;

    /**
     * 账期日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date periodDate;

    /**
     * 货款应收
     */
    private BigDecimal huokuan;

    /**
     * 手续费
     */
    private BigDecimal shouxufei;

    /**
     * 代理费
     */
    private BigDecimal dailifei;

    /**
     * 增值税
     */
    private BigDecimal zenzhishui;

    /**
     * 关税
     */
    private BigDecimal guanshui;

    /**
     * 消费税
     */
    private BigDecimal xiaofeishui;

    /**
     * 免3C认证费
     */
    private BigDecimal mian3c;

    /**
     * 国内送货费
     */
    private BigDecimal guoleisonhuofei;

    /**
     * 商检服务费
     */
    private BigDecimal shangjianfei;

    /**
     * 总应收
     */
    private BigDecimal totalCost;

    /**
     * 总已收
     */
    private BigDecimal writeValue;

    /**
     * 欠款金额
     */
    private BigDecimal arrearsValue;

    public BigDecimal getArrearsValue(){
        return getTotalCost().subtract(getWriteValue()).setScale(2,BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 订单编号
     */

    private String docNo;

    /**
     * 客户订单号
     */
    private String customerOrderNo;
    /**
     * 订单日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date orderDate;

    /**
     * 委托金额
     */
    private BigDecimal totalPrice;

    /**
     * 报关金额
     */
    private BigDecimal decTotalPrice;

    /**
     * 币制
     */
    private String currencyName;

    /**
     * 报价类型
     */
    private String quoteType;

    private String quoteTypeName;

    public String getQuoteTypeName(){
        QuoteTypeEnum quoteTypeEnum = QuoteTypeEnum.of(quoteType);
        return Objects.nonNull(quoteTypeEnum)?quoteTypeEnum.getName():"";
    }

    /**
     * 客户
     */
    private Long customerId;

    private String customerName;

    /**
     * 销售人员
     */
    private String salePersonName;

    /**
     * 逾期天数
     */
    private Integer overdueDays;

    private String overdueDaysStr;

    public String getOverdueDaysStr(){
        return overdueDays >=0?overdueDays.toString():"";
    }
}
