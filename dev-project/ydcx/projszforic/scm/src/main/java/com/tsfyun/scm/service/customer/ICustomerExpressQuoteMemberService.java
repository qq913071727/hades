package com.tsfyun.scm.service.customer;

import com.tsfyun.scm.entity.customer.CustomerExpressQuoteMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.CustomerExpressQuoteMemberVO;

import java.util.List;

/**
 * <p>
 * 快递模式标准报价计算代理费明细 服务类
 * </p>
 *
 *
 * @since 2020-10-30
 */
public interface ICustomerExpressQuoteMemberService extends IService<CustomerExpressQuoteMember> {

    /**
     * 根据客户报价id获取客户报价明细信息
     * @param customerQuoteId
     * @return
     */
    List<CustomerExpressQuoteMember> findByQuoteId(Long customerQuoteId);
    /**
     * 根据客户报价id获取客户报价明细信息
     * @param customerQuoteId
     * @return
     */
    List<CustomerExpressQuoteMemberVO> findVoByQuoteId(Long customerQuoteId);

    /**
     * 保存客户报价id
     * @param customerQuoteId
     * @param quoteId
     */
    void saveCustomerExpressQuoteMembers(Long customerQuoteId,Long quoteId);

    /**
     * 删除客户报价明细
     * @param customerQuoteId
     */
    void deleteAllCustomerExpressQuoteMembers(Long customerQuoteId);

}
