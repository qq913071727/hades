package com.tsfyun.scm.controller.system;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.system.SysNoticeDTO;
import com.tsfyun.scm.dto.system.SysNoticeQTO;
import com.tsfyun.scm.service.system.ISysNoticeService;
import com.tsfyun.scm.vo.system.SimpleSysNoticeVO;
import com.tsfyun.scm.vo.system.SysNoticeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  公告前端控制器
 * </p>
 *
 *
 * @since 2020-09-22
 */
@RestController
@RequestMapping("/notice")
@Api(tags = "公告")
public class SysNoticeController extends BaseController {

    @Autowired
    private ISysNoticeService sysNoticeService;

    @ApiOperation("客户端获取公告列表")
    @PostMapping(value = "list")
    public Result<List<SysNoticeVO>> list(@ModelAttribute SysNoticeQTO qto){
        qto.setIsOpen(Boolean.TRUE);
        qto.setIsLoginSee(Boolean.FALSE);
        PageInfo<SysNoticeVO> page = sysNoticeService.pageList(qto);
        //不返回内容
        page.getList().stream().forEach(data->{
            data.setContent("");
        });
        return success((int)page.getTotal(),page.getList());
    }

    @ApiOperation("登录后客户端获取公告列表")
    @PostMapping(value = "loginList")
    public Result<List<SysNoticeVO>> loginList(@ModelAttribute SysNoticeQTO qto){
        qto.setIsOpen(Boolean.TRUE);
        PageInfo<SysNoticeVO> page = sysNoticeService.pageList(qto);
        //不返回内容
        page.getList().stream().forEach(data->{
            data.setContent("");
        });
        return success((int)page.getTotal(),page.getList());
    }

    @ApiOperation("客户端获取最近几条公告列表，默认5条")
    @PostMapping(value = "recent")
    public Result<List<SimpleSysNoticeVO>> recent(@RequestParam(value = "num",required = false)Integer num){
        return success(sysNoticeService.recentSysNotices(num));
    }

    @ApiOperation("管理端获取公告列表")
    @PostMapping(value = "admin/list")
    @PreAuthorize("hasRole('manager')")
    public Result<List<SysNoticeVO>> adminList(@ModelAttribute SysNoticeQTO qto){
        PageInfo<SysNoticeVO> page = sysNoticeService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    @ApiOperation("获取公告内容")
    @GetMapping(value = "info")
    Result<SysNoticeVO> info(@RequestParam(value = "id")Long id) {
        return success(sysNoticeService.detail(id));
    }

    @ApiOperation("新增公告（管理端才允许操作）")
    @PostMapping(value = "add")
    @PreAuthorize("hasRole('manager')")
    public Result<Long> add(@RequestBody @Validated SysNoticeDTO dto){
        return success(sysNoticeService.add(dto));
    }

    @ApiOperation("修改公告（管理端才允许操作）")
    @PostMapping(value = "edit")
    @PreAuthorize("hasRole('manager')")
    public Result<Void> edit(@RequestBody @Validated(UpdateGroup.class) SysNoticeDTO dto){
        sysNoticeService.edit(dto);
        return success( );
    }

    @ApiOperation("删除公告（管理端才允许操作）")
    @PostMapping(value = "remove")
    @PreAuthorize("hasRole('manager')")
    public Result<Void> remove(@RequestParam(value = "id")Long id){
        sysNoticeService.remove(id);
        return success( );
    }


}

