package com.tsfyun.scm.controller.order;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.entity.order.ExpOrderMemberHistory;
import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import com.tsfyun.scm.service.order.IExpOrderMemberHistoryService;
import com.tsfyun.scm.service.order.IImpOrderMemberHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2021-09-13
 */
@RestController
@RequestMapping("/expOrderMemberHistory")
public class ExpOrderMemberHistoryController extends BaseController {
    @Autowired
    private IExpOrderMemberHistoryService expOrderMemberHistoryService;

    /**=
     * 根据订单ID获取明细历史记录
     * @param orderId
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ExpOrderMemberHistory>> list(@RequestParam(value = "orderId")Long orderId){
        return success(expOrderMemberHistoryService.getByOrderId(orderId));
    }
}

