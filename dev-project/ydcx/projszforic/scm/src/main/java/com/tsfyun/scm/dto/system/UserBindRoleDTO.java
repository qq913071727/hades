package com.tsfyun.scm.dto.system;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 用户绑定角色集合
 */
@Data
public class UserBindRoleDTO implements Serializable {

    @NotNull(message = "员工不能为空")
    private Long personId;

    private List<String> roleIds;

}
