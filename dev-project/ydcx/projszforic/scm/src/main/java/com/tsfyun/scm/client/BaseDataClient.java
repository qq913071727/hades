package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.BaseDataSimple;
import com.tsfyun.common.base.vo.DecDataVO;
import com.tsfyun.scm.client.fallback.BaseDataClientFallback;
import com.tsfyun.scm.system.vo.BaseDataVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@FeignClient(name = "scm-system",fallbackFactory = BaseDataClientFallback.class)
public interface BaseDataClient {

    /**
     * 根据类型获取基础配置数据
     * @param type
     * @return
     */
    @GetMapping(value = "/baseData/getByType")
    Result<List<BaseDataVO>> getByType(@RequestParam(value = "type")String type);

    /**=
     * 获取海关汇率
     * @param currencyId
     * @param date
     * @return
     */
    @PostMapping("/customsRate/obtain")
    Result<BigDecimal> obtainCustomsRate(@RequestParam(value = "currencyId")String currencyId, @RequestParam(value = "date") Date date);

    /**=
     * 获取中行汇率
     * @param currencyId
     * @param rateType
     * @param date
     * @return
     */
    @PostMapping("/bankChinaRate/obtain")
    Result<BigDecimal> obtainBankChinaRate(@RequestParam(value = "currencyId")String currencyId,@RequestParam(value = "rateType")String rateType,@RequestParam(value = "date") String date);

    /**
     * 获取海关基础配置数据
     * @return
     */
    @PostMapping(value = "/baseData/decDatasMap")
    Result<Map<String,List<BaseDataSimple>>> decDatasMap( );
}
