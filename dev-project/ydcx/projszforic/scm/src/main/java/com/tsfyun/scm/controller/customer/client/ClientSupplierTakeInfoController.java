package com.tsfyun.scm.controller.customer.client;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.SupplierTakeInfoDTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierTakeInfoQTO;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.scm.entity.customer.SupplierTakeInfo;
import com.tsfyun.scm.service.customer.ISupplierService;
import com.tsfyun.scm.service.customer.ISupplierTakeInfoService;
import com.tsfyun.scm.vo.customer.client.ClientSupplierTakeInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 客户端供应商提货信息
 * @CreateDate: Created in 2020/10/10 09:51
 */
@RestController
@RequestMapping(value = "/client/supplierTakeInfo")
@Api(tags = "供应商提货信息（客户端）")
public class ClientSupplierTakeInfoController extends BaseController {

    @Autowired
    private ISupplierTakeInfoService supplierTakeInfoService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private ISupplierService supplierService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    @ApiOperation("分页")
    Result<List<ClientSupplierTakeInfoVO>> page(@ModelAttribute @Valid ClientSupplierTakeInfoQTO qto) {
        PageInfo<ClientSupplierTakeInfoVO> page = supplierTakeInfoService.clientPage(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation("删除")
    public Result<Void> delete(@RequestParam("id")Long id){
        supplierTakeInfoService.clientRemove(id);
        return success();
    }

    /**
     * 设置默认
     */
    @PostMapping("/setDefault")
    @ApiOperation("设置默认")
    public Result<Void> setDefault(@RequestParam("id")Long id){
        supplierTakeInfoService.setDefault(id);
        return success();
    }

    /**
     * 新增供应商提货信息
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Long> add(@ModelAttribute @Validated(AddGroup.class) SupplierTakeInfoDTO dto) {
        return success(supplierTakeInfoService.clientAdd(dto));
    }
    /**
     * 修改供应商提货信息
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) SupplierTakeInfoDTO dto) {
        supplierTakeInfoService.clientEdit(dto);
        return success();
    }

}
