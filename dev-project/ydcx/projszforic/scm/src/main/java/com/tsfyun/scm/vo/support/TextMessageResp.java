package com.tsfyun.scm.vo.support;


import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 微信文本响应信息
 * @CreateDate: Created in 2020/12/29 16:22
 */
@Data
public class TextMessageResp implements Serializable {

    private String Content;
    //接收方帐号（收到的OpenID）
    private String ToUserName;
    //开发者微信号
    private String FromUserName;
    //消息创建时间 （整型）
    private long CreateTime;
    //消息类型（text/music/news）
    private String MsgType;

}
