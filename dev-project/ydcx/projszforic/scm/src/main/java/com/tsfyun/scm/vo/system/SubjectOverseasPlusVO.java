package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SubjectOverseasPlusVO implements Serializable {

    private SubjectOverseasVO overseas;

    private List<SubjectOverseasBankVO> banks;
}
