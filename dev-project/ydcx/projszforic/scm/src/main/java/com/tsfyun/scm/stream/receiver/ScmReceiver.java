package com.tsfyun.scm.stream.receiver;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tsfyun.common.base.dto.CostWriteOffDTO;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.dto.support.SendMailDTO;
import com.tsfyun.scm.dto.support.SendSmsDTO;
import com.tsfyun.scm.dto.support.SendWxDTO;
import com.tsfyun.scm.excel.ClassifyMaterielExcel;
import com.tsfyun.scm.excel.ClassifyMaterielExpExcel;
import com.tsfyun.scm.service.finance.IWriteOffService;
import com.tsfyun.scm.service.impl.third.ShortMessageServiceImpl;
import com.tsfyun.scm.service.materiel.IMaterielExpService;
import com.tsfyun.scm.service.materiel.IMaterielService;
import com.tsfyun.scm.service.system.IMailService;
import com.tsfyun.scm.service.third.AbstractMessageService;
import com.tsfyun.scm.service.third.IWxMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2019/12/16 18:55
 */
@EnableBinding(value = {Sink.class})
@Component
@Slf4j
public class ScmReceiver {

    @Autowired
    private IMaterielService materielService;
    @Autowired
    private IMaterielExpService materielExpService;
    @Autowired
    private IWriteOffService writeOffService;
    @Autowired
    private IMailService mailService;
    @Autowired
    private IWxMessageService wxMessageService;

    /**=
     * 接收物料导入数据分析
     * @param payload
     */
    @StreamListener(value = Sink.IN_SCM_MATERIEL)
    public void receiverMateriel(@Payload Message<List<String>> payload) throws Exception {
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        List<String> ids = payload.getPayload();
        log.info("收到物料导入消息>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, JSON.toJSONString(payload.getPayload()));
        materielService.analysisNewMateriel(ids);
    }

    /**=
     * 接收归类物料数据导入
     */
    @StreamListener(value = Sink.IN_SCM_CLASSIFY_MATERIEL)
    public void receiverClassifyMateriel(@Payload Message<List<LinkedHashMap<String,Object>>> payload){
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        log.info("收到进口物料归类导入消息>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, JSON.toJSONString(payload.getPayload()));
        List<LinkedHashMap<String,Object>> dataList = payload.getPayload();
        if(CollUtil.isNotEmpty(dataList)) {
            List<ClassifyMaterielExcel> classifyList = JSONObject.parseArray(JSON.toJSONString(dataList),ClassifyMaterielExcel.class);
            materielService.saveImportClassifyMateriel(classifyList);
        }
    }

    /**=
     * 接收归类物料数据导入
     */
    @StreamListener(value = Sink.IN_SCM_CLASSIFY_MATERIEL_EXP)
    public void receiverClassifyMaterielExp(@Payload Message<List<LinkedHashMap<String,Object>>> payload){
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        log.info("收到出口物料归类导入消息>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, JSON.toJSONString(payload.getPayload()));
        List<LinkedHashMap<String,Object>> dataList = payload.getPayload();
        if(CollUtil.isNotEmpty(dataList)) {
            List<ClassifyMaterielExpExcel> classifyList = JSONObject.parseArray(JSON.toJSONString(dataList),ClassifyMaterielExpExcel.class);
            materielExpService.saveImportClassifyMateriel(classifyList);
        }
    }

    /**=
     * 接收费用核销
     */
    @StreamListener(value = Sink.IN_SCM_COST_WRITE_OFF)
    public void receiverCostWriteOff(@Payload Message<byte[]> payload){
        String msgId = "";
        Object msgIdObj = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID);
        if(Objects.isNull(msgIdObj)) {
            msgId = StringUtils.null2EmptyWithTrim(payload.getHeaders().get("id"));
        } else {
            msgId = StringUtils.null2EmptyWithTrim(msgId);
        }
        String messageContent = new String(payload.getPayload());
        log.info("收到接收费用核销消息>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, messageContent);
        CostWriteOffDTO dto = JSONObject.parseObject(messageContent,CostWriteOffDTO.class);
        writeOffService.autoWriteOff(dto);
    }

    /**=
     * 接收邮箱
     */
    @StreamListener(value = Sink.IN_SCM_MAIL)
    public void receiverMail(@Payload Message<SendMailDTO> payload){
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        log.info("收到发送邮件>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, JSON.toJSONString(payload.getPayload()));
        SendMailDTO dto = payload.getPayload();
        mailService.consumeMailByMQ(dto);
    }

    /**=
     * 接收短信
     */
    @StreamListener(value = Sink.IN_SCM_SMS)
    public void receiverSms(@Payload Message<SendSmsDTO> payload){
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        log.info("收到发送短信>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, JSON.toJSONString(payload.getPayload()));
        SendSmsDTO dto = payload.getPayload();
        String msgPlatform = dto.getMsgPlatform();
        ShortMessagePlatformEnum shortMessagePlatformEnum = ShortMessagePlatformEnum.of(msgPlatform);
        if(Objects.nonNull(shortMessagePlatformEnum)) {
            AbstractMessageService messageService = (AbstractMessageService)ShortMessageServiceImpl.messageServiceMap.get(shortMessagePlatformEnum);
            messageService.consumeNoticeMessageWithMQ(dto);
        }
    }

    /**=
     * 接收微信通知
     */
    @StreamListener(value = Sink.IN_SCM_WX)
    public void receiverWx(@Payload Message<SendWxDTO> payload){
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        log.info("收到发送微信通知>>>>>>>>>>>>>>，消息id:{}，消息内容:{}",msgId, JSON.toJSONString(payload.getPayload()));
        SendWxDTO dto = payload.getPayload();
        wxMessageService.send2Wx(dto);
    }

}
