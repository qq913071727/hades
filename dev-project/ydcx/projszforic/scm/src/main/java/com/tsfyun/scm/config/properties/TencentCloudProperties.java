package com.tsfyun.scm.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Description: 腾讯云
 * @CreateDate: Created in 2021/9/15 14:23
 */
@RefreshScope
@Data
@Component
@ConfigurationProperties(prefix = TencentCloudProperties.PREFIX)
public class TencentCloudProperties {

    public static final String PREFIX = "tencent.sms";

    private String sdkAppId;

    private String secretId;

    private String secretKey;

    private String signName;

    private String domain = "sms.tencentcloudapi.com";

}
