package com.tsfyun.scm.controller.system;


import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.system.SubjectOverseasPlusDTO;
import com.tsfyun.scm.dto.system.SubjectOverseasQTO;
import com.tsfyun.scm.service.system.ISubjectOverseasService;
import com.tsfyun.scm.vo.system.SubjectOverseasPlusVO;
import com.tsfyun.scm.vo.system.SubjectOverseasVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  主体境外公司
 * </p>
 *
 *
 * @since 2020-03-19
 */
@RestController
@RequestMapping("/subjectOverseas")
public class SubjectOverseasController extends BaseController {

    @Autowired
    private ISubjectOverseasService subjectOverseasService;

    /**=
     * 查询主体境外公司
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<SubjectOverseasVO>> list(@ModelAttribute SubjectOverseasQTO qto) {
        return success(subjectOverseasService.findList(qto));
    }

    /**=
     * 下拉
     * @param qto
     * @return
     */
    @GetMapping(value = "select")
    Result<List<SubjectOverseasVO>> select(@ModelAttribute SubjectOverseasQTO qto) {
        qto.setDisabled(Boolean.FALSE);
        return success(subjectOverseasService.findList(qto));
    }

    /**
     * 新增主体境外公司（含银行信息）
     * @param dto
     * @return 返回供应商id
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Long> add(@RequestBody @Validated SubjectOverseasPlusDTO dto) {
        return success(subjectOverseasService.addPlus(dto));
    }

    /**
     * 修改主体境外公司（含银行信息）
     * @param dto
     * @return 返回供应商id
     */
    @DuplicateSubmit
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated SubjectOverseasPlusDTO dto) {
        subjectOverseasService.editPlus(dto);
        return success();
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        subjectOverseasService.updateDisabled(id,disabled);
        return success();
    }

    /**=
     * 删除境外公司
     * @param id
     * @return
     */
    @PostMapping(value = "delete")
    Result<Void> delete(@RequestParam(value = "id")Long id) {
        subjectOverseasService.remove(id);
        return success();
    }

    /**=
     * 详情包含银行信息
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    Result<SubjectOverseasPlusVO> info(@RequestParam(value = "id")Long id){
        return success(subjectOverseasService.detailPlus(id));
    }

}

