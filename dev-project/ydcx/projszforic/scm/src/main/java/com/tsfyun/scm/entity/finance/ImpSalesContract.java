package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 销售合同
 * </p>
 *

 * @since 2020-05-19
 */
@Data
public class ImpSalesContract extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;


    private String statusId;

    /**
     * 发票编号
     */

    private String invoiceNo;

    /**
     * 合同日期
     */

    private LocalDateTime contractDate;

    /**
     * 订单编号
     */

    private String impOrderNo;

    /**
     * 订单ID
     */
    private Long impOrderId;

    /**
     * 购货单位（客户）
     */

    private Long buyerId;

    /**
     * 购货单位名称
     */

    private String buyerName;

    /**
     * 买方银行名称
     */

    private String buyerBankName;

    /**
     * 买方纳税人识别号
     */

    private String buyerTaxpayerNo;

    /**
     * 买方银行账号
     */

    private String buyerBankAccount;

    /**
     * 买方银行地址
     */

    private String buyerBankAddress;

    /**
     * 买方联系电话
     */

    private String buyerLinkPerson;

    /**
     * 买方银行电话
     */

    private String buyerBankTel;

    /**
     * 卖方
     */

    private Long sellerId;

    /**
     * 卖方银行名称
     */

    private String sellerBankName;

    /**
     * 卖方银行账号
     */

    private String sellerBankAccount;

    /**
     * 卖方银行地址
     */

    private String sellerBankAddress;

    /**
     * 合同金额
     */

    private BigDecimal totalPrice;

    /**
     * 货款金额
     */

    private BigDecimal goodsVal;

    /**
     * 票差调整金额
     */

    private BigDecimal differenceVal;


    private String memo;


}
