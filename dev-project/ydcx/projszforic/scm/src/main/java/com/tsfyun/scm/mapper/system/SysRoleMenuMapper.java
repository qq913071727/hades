package com.tsfyun.scm.mapper.system;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.system.SysRoleMenu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMenuMapper extends Mapper<SysRoleMenu> {

    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatchByRoleIds(List<String> roleIds);

}
