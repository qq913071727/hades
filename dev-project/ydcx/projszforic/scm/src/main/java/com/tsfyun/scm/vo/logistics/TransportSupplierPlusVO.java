package com.tsfyun.scm.vo.logistics;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransportSupplierPlusVO implements Serializable {

    /**
     * 运输供应商信息
     */
    private TransportSupplierVO transportSupplier;

    /**
     * 车辆信息
     */
    private List<ConveyanceVO> conveyances;

}
