package com.tsfyun.scm.config.ws;

public interface WsConstant {

    /**
     * 消息通知广播
     */
    String WS_TOPIC = "notice_notenant";

    /**
     * 用户下线广播
     */
    String USER_OFFLINE_TOPIC = "user_offline";

}
