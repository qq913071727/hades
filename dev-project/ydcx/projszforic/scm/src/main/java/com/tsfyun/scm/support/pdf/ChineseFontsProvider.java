package com.tsfyun.scm.support.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @Description: 处理中文不显示和乱码问题
 * @since Created in 2020/4/29 15:38
 */
@Slf4j
class ChineseFontsProvider extends XMLWorkerFontProvider {

    @Override
    public Font getFont(final String fontname, String encoding, float size, final int style) {
        try {
            BaseFont bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            return new Font(bfChinese, size, style);
        } catch (DocumentException | IOException e) {
            log.error("获取字体异常",e);
        }
        return super.getFont(fontname, encoding, size, style);
    }
}
