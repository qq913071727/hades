package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

@Data
public class ClientCustomerAbroadDeliveryInfoQTO extends PaginationDto {

    private Boolean disabled;
    private Boolean isDefault;

}
