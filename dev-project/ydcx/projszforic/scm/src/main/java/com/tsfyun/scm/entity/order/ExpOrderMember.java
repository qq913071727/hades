package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 出口订单明细
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Data
public class ExpOrderMember implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    private Integer rowNo;

    /**
     * 订单主单
     */

    private Long expOrderId;

    /**
     * 物料ID
     */

    private String materialId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 客户物料号
     */

    private String goodsCode;

    /**
     * 原产国(地区)
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价(委托)
     */

    private BigDecimal unitPrice;

    /**
     * 总价(委托)
     */

    private BigDecimal totalPrice;

    /**
     * 报关单价
     */

    private BigDecimal decUnitPrice;

    /**
     * 报关总价
     */

    private BigDecimal decTotalPrice;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;

    private BigDecimal amountCollected;// 境外收款认领金额
}
