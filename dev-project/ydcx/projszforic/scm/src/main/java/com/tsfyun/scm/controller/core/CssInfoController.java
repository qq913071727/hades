package com.tsfyun.scm.controller.core;

import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.CssInfoQTO;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.util.CssInfoUtil;
import com.tsfyun.scm.vo.other.CssInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;


@RestController
@RequestMapping("/css")
public class CssInfoController extends BaseController {

    @Autowired
    private OrikaBeanMapper beanMapper;

    @PostMapping(value = "search")
    public Result<CssInfoVO> search(@RequestBody @Valid CssInfoQTO qto){
        return success(CssInfoUtil.search(beanMapper.map(qto, Map.class)));
    }
}
