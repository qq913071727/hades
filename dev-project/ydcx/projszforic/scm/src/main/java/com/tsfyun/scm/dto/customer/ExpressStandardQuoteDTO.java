package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.QuoteTypeEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 快递模式标准报价代理费请求实体
 * </p>
 *
 *
 * @since 2020-10-26
 */
@Data
@ApiModel(value="ExpressStandardQuote请求对象", description="快递模式标准报价代理费请求实体")
public class ExpressStandardQuoteDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "请选择需要修改的数据",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "报价名称不能为空")
   @LengthTrim(max = 32,message = "报价名称最大长度不能超过32位")
   @ApiModelProperty(value = "报价名称")
   private String name;

   @EnumCheck(clazz = QuoteTypeEnum.class,message = "报价类型错误")
   @NotEmptyTrim(message = "报价类型不能为空")
   private String quoteType;

   @NotNull(message = "报价开始时间不能为空")
   @ApiModelProperty(value = "报价开始时间")
   private LocalDateTime quoteStartTime;

   @NotNull(message = "报价失效时间不能为空")
   @ApiModelProperty(value = "报价结束时间")
   private LocalDateTime quoteEndTime;

   @ApiModelProperty(value = "封顶标识")
   private Boolean capped;

   @Digits(integer = 6, fraction = 2, message = "封顶费用整数位不能超过6位，小数位不能超过2")
   @DecimalMin(value = "0.01",message = "封顶费用不能小于等于0")
   @ApiModelProperty(value = "封顶费用")
   private BigDecimal cappedFee;

   @NotNull(message = "报价禁用标识不能为空")
   @ApiModelProperty(value = "禁用标识")
   private Boolean disabled;


}
