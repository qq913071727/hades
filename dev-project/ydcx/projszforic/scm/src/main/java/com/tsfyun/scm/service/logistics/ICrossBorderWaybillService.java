package com.tsfyun.scm.service.logistics;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.logistics.*;
import com.tsfyun.scm.entity.logistics.CrossBorderWaybill;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.CrossBorderWaybillVO;
import com.tsfyun.scm.vo.order.BindOrderVO;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 跨境运输单 服务类
 * </p>
 *

 * @since 2020-04-17
 */
public interface ICrossBorderWaybillService extends IService<CrossBorderWaybill> {

    /**
     * 新增
     * @param dto
     * @return
     */
    Long add(CrossBorderWaybillDTO dto);

    /**
     * 修改
     * @param dto
     */
    void edit(CrossBorderWaybillDTO dto);

    /**
     * 分页
     * @param qto
     * @return
     */
    PageInfo<CrossBorderWaybillVO> pageList(CrossBorderWaybillQTO qto);

    /**
     * 详情
     * @param id
     * @param operation
     * @return
     */
    CrossBorderWaybillVO detail(Long id,String operation);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 确认运单
     * @param dto
     */
    void comfirm(TaskDTO dto);

    /**
     * 跨境运输单绑定订单
     * @param dto
     */
    void bindOrder(CrossBorderWaybillBindOrderDTO dto);

    /**
     * 跨境运输单解绑订单
     * @param id
     * @param orderId
     */
    void unbindOrder(Long id,Long orderId);
    void unbindOrder(CrossBorderWaybill crossBorderWaybill,Long orderId);

    /**
     * 获取运输单可以绑定的订单数据
     * @param id
     * @return
     */
    List<BindOrderVO> canBindList(Long id);

    /**
     * 获取运输单可以绑定的订单数据
     * @param id
     * @return
     */
    List<BindOrderVO> bindedList(Long id);
    List<BindOrderVO> bindedList(CrossBorderWaybill crossBorderWaybill);

    /**
     * 绑单完成
     * @param taskDTO
     */
    void bindComplete(TaskDTO taskDTO);

    /**=
     * 退回修改
     * @param dto
     */
    void backUpdate(TaskDTO dto);

    /**
     * 复核
     * @param dto
     */
    void review(TaskDTO dto);

    /**=
     * 状态改变
     * @param dto
     */
    void processJump(TaskDTO dto);

    /**
     * 发车
     * @param dto
     */
    void depart(CrossBorderWaybillDepartDTO dto);

    /**
     * 签收
     * @param dto
     */
    void sign(CrossBorderWaybillSignDTO dto);

    /**=
     * 根据订单ID查询中港运单(进口)
     * @param orderId
     * @return
     */
    CrossBorderWaybill findByImpOrderId(Long orderId);
    /**=
     * 根据订单ID查询中港运单(出口)
     * @param orderId
     * @return
     */
    CrossBorderWaybill findByExpOrderId(Long orderId);

    /**
     * 统计进口车次
     * @param startDate
     * @param endDate
     * @return
     */
    Integer impTrainNumber(Date startDate, Date endDate);

    /**
     * 统计出口车次
     * @param startDate
     * @param endDate
     * @return
     */
    Integer expTrainNumber(Date startDate, Date endDate);
}
