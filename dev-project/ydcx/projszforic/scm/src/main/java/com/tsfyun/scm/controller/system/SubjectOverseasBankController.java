package com.tsfyun.scm.controller.system;


import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.system.ISubjectOverseasBankService;
import com.tsfyun.scm.vo.system.SubjectOverseasBankVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2020-03-19
 */
@RestController
@RequestMapping("/subjectOverseasBank")
public class SubjectOverseasBankController extends BaseController {

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private ISubjectOverseasBankService subjectOverseasBankService;

    @PostMapping(value = "selectByOverseasId")
    public Result<List<SubjectOverseasBankVO>> selectByOverseasId(@RequestParam(value = "overseasId")Long overseasId){
       return success(subjectOverseasBankService.listByOverseasId(overseasId));
    }

    @GetMapping(value = "getDefaultSubjectOverseasBank")
    public Result<List<SubjectOverseasBankVO>> getDefaultSubjectOverseasBank( ){
        return success(subjectOverseasBankService.getDefaultSubjectOverseasBank( ));
    }

    @GetMapping(value = "detail")
    public Result<SubjectOverseasBankVO> detail(@RequestParam(value = "id")Long id){
        return success(subjectOverseasBankService.detail(id));
    }

}

