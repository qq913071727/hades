package com.tsfyun.scm.constant;

/**
 * @Description: 微信常量数据
 * @CreateDate: Created in 2021/1/5 11:27
 */
public interface WeiXinConstant {

    //#####################以下为微信一些开放接口URL##########################
    //微信access_token
    String access_token_url = "https://api.weixin.qq.com/cgi-bin/token";
    //微信jsapi_ticket
    String jsapi_ticket_url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
    //网页授权
    String authorize_url = "https://open.weixin.qq.com/connect/oauth2/authorize";
    //网页授权access_token
    String authorize_access_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token";
    //小程序获取openID
    String jscode2session = "https://api.weixin.qq.com/sns/jscode2session";
    //微信生成二维码
    String WEIXIN_QRCODE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=ACCESS_TOKEN";
    //微信公众号获取用户信息
    String WEIXIN_USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
    //微信公众号客服回复消息
    String WEIXIN_SEND_MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
    //微信公众号创建菜单
    String WEIXIN_CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
    //微信公众号获取菜单
    String WEIXIN_GET_MENU_URL = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN";
    //微信公众号删除菜单
    String WEIXIN_DELETE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";
    //微信公众号模板消息
    String WEIXIN_TEMPLATE_MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send";
    //长链接转短链接
    String WEIXIN_SHORT_LINK_URL = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token=ACCESS_TOKEN";

}
