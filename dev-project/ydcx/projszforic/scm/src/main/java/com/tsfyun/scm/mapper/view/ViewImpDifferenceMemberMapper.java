package com.tsfyun.scm.mapper.view;

import com.tsfyun.scm.dto.view.ViewImpDifferenceMemberQTO;
import com.tsfyun.scm.entity.view.ViewImpDifferenceMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.view.ViewImpDifferenceMemberVO;
import com.tsfyun.scm.vo.view.ViewImpDifferenceSummaryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-21
 */
@Repository
public interface ViewImpDifferenceMemberMapper extends Mapper<ViewImpDifferenceMember> {

    /**
     * 客户票差明细
     * @param qto
     * @return
     */
    List<ViewImpDifferenceMemberVO> impDifferenceMemberList(ViewImpDifferenceMemberQTO qto);

    /**
     * 获取客户票差
     * @param customerId
     * @return
     */
    BigDecimal getCustomerDifference(@Param(value = "customerId")Long customerId);

    /**
     * 客户票差汇总
     * @param qto
     * @return
     */
    List<ViewImpDifferenceSummaryVO> impDifferenceSummaryList(ViewImpDifferenceMemberQTO qto);

}
