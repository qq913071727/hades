package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class InspectionPlusVO implements Serializable {

    //产品明细
    List<ImpOrderMemberVO> members;
    //辅助验货明细
    List<AuxiliaryInspectionVO> auxiliaryInspections;
}
