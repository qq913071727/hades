package com.tsfyun.scm.config.mock;

import feign.Client;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.cloud.openfeign.ribbon.CachingSpringLoadBalancerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * @Description: 便于本地开发测试，有时候连的是同一个注册中心不方便feign的开发测试，可以设置mock.enabled = true，mock.url = IP:端口
 * sleuth和MockLoadBalancerFeignClient不能共存,请设置spring.sleuth.feign.enabled=false
 * @CreateDate: Created in 2021/12/10 11:16
 */
@Profile(value = {"dev","test"})
@ConditionalOnProperty(prefix = MockAutoConfiguration.PREFIX,name = "enabled",havingValue = "true")
@Configuration
public class MockAutoConfiguration {

    public static final String PREFIX = "mock";

    @Bean
    public Client feignClient(CachingSpringLoadBalancerFactory cachingFactory, SpringClientFactory clientFactory) {
        return new MockLoadBalancerFeignClient(new Client.Default(null, null),cachingFactory,clientFactory);
    }

}
