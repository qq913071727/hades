package com.tsfyun.scm.dto.order;

import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2021-09-17
 */
@Data
@ApiModel(value="PriceFluctuationHistoryExp请求对象", description="请求实体")
public class PriceFluctuationHistoryExpDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "币制不能为空")
   @LengthTrim(max = 10,message = "币制最大长度不能超过10位")
   @ApiModelProperty(value = "币制")
   private String currencyId;

   private String currencyName;

   @NotEmptyTrim(message = "客户名称不能为空")
   @LengthTrim(max = 255,message = "客户名称最大长度不能超过255位")
   @ApiModelProperty(value = "客户名称")
   private String customerName;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   private String spec;

   @ApiModelProperty(value = "进口日期")
   private Date orderDate;

   @NotEmptyTrim(message = "订单号不能为空")
   @LengthTrim(max = 20,message = "订单号最大长度不能超过20位")
   @ApiModelProperty(value = "订单号")
   private String orderDocNo;

   @NotNull(message = "订单审价明细不能为空")
   @ApiModelProperty(value = "订单审价明细")
   private Long orderPriceMemberId;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "总价不能为空")
   @Digits(integer = 8, fraction = 2, message = "总价整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "总价")
   private BigDecimal totalPrice;

   @NotEmptyTrim(message = "单位名称不能为空")
   @LengthTrim(max = 10,message = "单位名称最大长度不能超过10位")
   @ApiModelProperty(value = "单位名称")
   private String unitName;

   @NotNull(message = "单价不能为空")
   @Digits(integer = 6, fraction = 4, message = "单价整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "单价")
   private BigDecimal unitPrice;

   @NotNull(message = "美金单价不能为空")
   @Digits(integer = 8, fraction = 2, message = "美金单价整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "美金单价")
   private BigDecimal usdTotalPrice;

   @NotNull(message = "美金总价不能为空")
   @Digits(integer = 6, fraction = 4, message = "美金总价整数位不能超过6位，小数位不能超过4")
   @ApiModelProperty(value = "美金总价")
   private BigDecimal usdUnitPrice;


}
