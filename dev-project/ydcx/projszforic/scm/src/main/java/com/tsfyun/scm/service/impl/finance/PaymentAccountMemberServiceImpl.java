package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.finance.PaymentAccountMember;
import com.tsfyun.scm.mapper.finance.PaymentAccountMemberMapper;
import com.tsfyun.scm.service.finance.IPaymentAccountMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.finance.PaymentAccountMemberVO;
import com.tsfyun.scm.vo.finance.PaymentOrderCostVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * <p>
 * 付款单明细 服务实现类
 * </p>
 *

 * @since 2020-04-24
 */
@Service
public class PaymentAccountMemberServiceImpl extends ServiceImpl<PaymentAccountMember> implements IPaymentAccountMemberService {

    @Autowired
    private PaymentAccountMemberMapper paymentAccountMemberMapper;

    @Override
    public List<PaymentAccountMemberVO> findByPaymentAccountId(@NonNull Long paymentAccountId) {
        return paymentAccountMemberMapper.findByPaymentAccountId(paymentAccountId);
    }

    @Override
    public List<PaymentAccountMember> getByPaymentAccountId(Long paymentAccountId) {
        PaymentAccountMember query = new PaymentAccountMember();
        query.setPaymentAccountId(paymentAccountId);
        return paymentAccountMemberMapper.select(query);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAccountValueCny(List<PaymentAccountMemberVO> paymentAccountMembers) {
        paymentAccountMemberMapper.updateAccountValueCny(paymentAccountMembers);
    }

    @Override
    public List<String> findByOrderIdByPayNos(Long orderId) {
        return paymentAccountMemberMapper.findByOrderIdByPayNos(orderId);
    }

    @Override
    public List<PaymentOrderCostVO> findByOrderPaymentCostId(Long orderId) {
        return paymentAccountMemberMapper.findByOrderPaymentCostId(orderId);
    }
}
