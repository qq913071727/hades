package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.ExpOrderMemberHistory;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2021-09-13
 */
public interface IExpOrderMemberHistoryService extends IService<ExpOrderMemberHistory> {

    //根据订单ID删除明细
    void removeByOrderId(Long orderId);

    List<ExpOrderMemberHistory> getByOrderId(Long orderId);

    // 拆单批量修改
    void batchUpdateSplitMembers(List<ExpOrderMemberHistory> expOrderMemberHistoryList);
}
