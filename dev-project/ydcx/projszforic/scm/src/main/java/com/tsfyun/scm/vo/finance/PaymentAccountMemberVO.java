package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 付款单明细响应实体
 * </p>
 *

 * @since 2020-04-24
 */
@Data
@ApiModel(value="PaymentAccountMember响应对象", description="付款单明细响应实体")
public class PaymentAccountMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private Long paymentAccountId;

    private BigDecimal accountValue;

    private BigDecimal accountValueCny;

    private Long impOrderId;

    private String impOrderNo;
    private Long impQuoteId;//报价
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;//订单日期
    private BigDecimal totalPrice;//订单金额
    private String supplierName;//供应商名称
    private String statusId;//订单状态
    private String statusDesc;//状态描述
    @JsonIgnore
    private LocalDateTime dateCreated;

    public String getStatusDesc() {
        ImpOrderStatusEnum impOrderStatusEnum = ImpOrderStatusEnum.of(statusId);
        return Objects.nonNull(impOrderStatusEnum) ? impOrderStatusEnum.getName() : "";
    }


}
