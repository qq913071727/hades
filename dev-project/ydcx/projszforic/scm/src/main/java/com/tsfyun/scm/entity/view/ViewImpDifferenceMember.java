package com.tsfyun.scm.entity.view;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import tk.mybatis.mapper.annotation.KeySql;
import java.time.LocalDateTime;
import javax.persistence.Id;
import java.io.Serializable;
import lombok.Data;


/**
 * <p>
 * VIEW
 * </p>
 *
 *
 * @since 2021-10-21
 */
@Data
public class ViewImpDifferenceMember implements Serializable {

     private static final long serialVersionUID=1L;


    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 订单日期-由后端写入
     */

    private LocalDateTime orderDate;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 付汇金额(CNY)
     */
    private BigDecimal accountValueCny;

    /**
     * 销售合同单号
     */

    private String salesDocNo;

    /**
     * 销售合同货款金额
     */

    private BigDecimal goodsVal;

    /**
     * 票差
     */

    private BigDecimal balance;

    /**
     * 票差调整金额
     */

    private BigDecimal differenceVal;

    /**
     * 销售合同总价
     */

    private BigDecimal totalPrice;

    /**
     * 订单编号
     */

    private String docNo;


}
