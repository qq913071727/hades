package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.ImpOrderPriceMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ImpOrderPriceMemberVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单审价明细 服务类
 * </p>
 *
 *
 * @since 2020-04-16
 */
public interface IImpOrderPriceMemberService extends IService<ImpOrderPriceMember> {
    //根据订单ID查询历史审价记录
    Map<String,Object> historicalRecord(Long orderMemberId);

    List<ImpOrderPriceMemberVO> findByOrderPriceId(Long orderPriceId);

    void removeByOrderId(Long orderId);
}
