//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 许可证
 * 
 * <p>DecGoodsLimitType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="DecGoodsLimitType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GoodsNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="9"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LicTypeCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LicenceNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="40"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LicWrtofDetailNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LicWrtofQty" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DecGoodsLimitVin" type="{http://www.chinaport.gov.cn/dec}DecGoodsLimitVinType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LicWrtofQtyUnit" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecGoodsLimitType", propOrder = {
    "goodsNo",
    "licTypeCode",
    "licenceNo",
    "licWrtofDetailNo",
    "licWrtofQty",
    "decGoodsLimitVin",
    "licWrtofQtyUnit"
})
public class DecGoodsLimitType {

    @XmlElement(name = "GoodsNo", required = true)
    protected String goodsNo;
    @XmlElement(name = "LicTypeCode", required = true)
    protected String licTypeCode;
    @XmlElement(name = "LicenceNo")
    protected String licenceNo;
    @XmlElement(name = "LicWrtofDetailNo")
    protected String licWrtofDetailNo;
    @XmlElement(name = "LicWrtofQty")
    protected String licWrtofQty;
    @XmlElement(name = "DecGoodsLimitVin")
    protected List<DecGoodsLimitVinType> decGoodsLimitVin;
    @XmlElement(name = "LicWrtofQtyUnit")
    protected String licWrtofQtyUnit;

    /**
     * 获取goodsNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsNo() {
        return goodsNo;
    }

    /**
     * 设置goodsNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsNo(String value) {
        this.goodsNo = value;
    }

    /**
     * 获取licTypeCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicTypeCode() {
        return licTypeCode;
    }

    /**
     * 设置licTypeCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicTypeCode(String value) {
        this.licTypeCode = value;
    }

    /**
     * 获取licenceNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenceNo() {
        return licenceNo;
    }

    /**
     * 设置licenceNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenceNo(String value) {
        this.licenceNo = value;
    }

    /**
     * 获取licWrtofDetailNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicWrtofDetailNo() {
        return licWrtofDetailNo;
    }

    /**
     * 设置licWrtofDetailNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicWrtofDetailNo(String value) {
        this.licWrtofDetailNo = value;
    }

    /**
     * 获取licWrtofQty属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicWrtofQty() {
        return licWrtofQty;
    }

    /**
     * 设置licWrtofQty属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicWrtofQty(String value) {
        this.licWrtofQty = value;
    }

    /**
     * Gets the value of the decGoodsLimitVin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the decGoodsLimitVin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDecGoodsLimitVin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DecGoodsLimitVinType }
     * 
     * 
     */
    public List<DecGoodsLimitVinType> getDecGoodsLimitVin() {
        if (decGoodsLimitVin == null) {
            decGoodsLimitVin = new ArrayList<DecGoodsLimitVinType>();
        }
        return this.decGoodsLimitVin;
    }

    /**
     * 获取licWrtofQtyUnit属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicWrtofQtyUnit() {
        return licWrtofQtyUnit;
    }

    /**
     * 设置licWrtofQtyUnit属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicWrtofQtyUnit(String value) {
        this.licWrtofQtyUnit = value;
    }

}
