package com.tsfyun.scm.vo.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-18
 */
@Data
@ApiModel(value="Currency响应对象", description="响应实体")
public class CurrencyVO implements Serializable {

     private static final long serialVersionUID=1L;


    private String id;

    @ApiModelProperty(value = "禁用状态")
    private Boolean disabled;

    @ApiModelProperty(value = "中国银行币制索引")
    private String markCode;

    @ApiModelProperty(value = "币制名称")
    private String name;

    private Integer sort;


}
