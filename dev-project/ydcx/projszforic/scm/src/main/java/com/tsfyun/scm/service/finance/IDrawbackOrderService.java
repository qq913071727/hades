package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.DrawbackOrder;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.DrawbackOrderVO;

import java.util.List;

/**
 * <p>
 * 退税订单 服务类
 * </p>
 *
 *
 * @since 2021-10-20
 */
public interface IDrawbackOrderService extends IService<DrawbackOrder> {

    List<DrawbackOrderVO>  findOrderByDrawbackId(Long drawbackId);

    List<DrawbackOrder> findByDrawbackId(Long drawbackId);
}
