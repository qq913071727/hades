package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 采购合同
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Data
public class PurchaseContract extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 合同编号
     */

    private String docNo;

    /**
     * 订单ID
     */

    private Long orderId;

    /**
     * 订单号
     */

    private String orderNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 签定地点
     */

    private String signingPlace;

    /**
     * 签定日期
     */

    private Date signingDate;

    /**
     * 开票日期
     */

    private Date invoicingDate;

    /**
     * 收票日期
     */

    private Date invoiceDate;

    /**
     * 发票号码
     */

    private String invoiceNo;

    /**
     * 买方单位
     */

    private String buyerName;

    /**
     * 签约代表
     */

    private String buyerDeputy;

    /**
     * 电话
     */

    private String buyerTel;

    /**
     * 开户行
     */

    private String buyerBank;

    /**
     * 帐号
     */

    private String buyerBankNo;

    /**
     * 地址
     */

    private String buyerBankAddress;

    /**
     * 卖方单位
     */

    private String sellerName;

    /**
     * 签约代表
     */

    private String sellerDeputy;

    /**
     * 电话
     */

    private String sellerTel;

    /**
     * 开户行
     */

    private String sellerBank;

    /**
     * 帐号
     */

    private String sellerBankNo;

    /**
     * 地址
     */

    private String sellerBankAddress;

    /**
     * 代理费
     */

    private BigDecimal agentFee;

    /**
     * 代垫费
     */

    private BigDecimal matFee;

    /**
     * 票差调整金额
     */

    private BigDecimal differenceVal;

    /**
     * 采购合同金额
     */

    private BigDecimal totalPrice;

    /**
     * 收票金额
     */

    private BigDecimal invoiceVal;

    /**
     * 是否结汇
     */

    private Boolean isSettleAccount;

    /**
     * 结汇说明
     */
    private String settleAccountInfo;

    /**
     * 订单币制
     */

    private String currencyId;

    /**
     * 订单币制
     */

    private String currencyName;

    /**
     * 汇率
     */

    private BigDecimal exchangeRate;

    /**
     * 备注
     */

    private String memo;

    /**
     * 是否函调
     */

    private Boolean isCorrespondence;

    /**
     * 是否回函
     */

    private String replyStatus;

    /**
     * 回函日期
     */

    private LocalDateTime replyDate;

    /**
     * 退税申报日期
     */

    private LocalDateTime taxDeclareTime;

    /**
     * 退税申报金额
     */

    private BigDecimal taxDeclareAmount;

    /**
     * 税局退税日期
     */

    private LocalDateTime taxRefundTime;

    /**
     * 税局退税金额
     */

    private BigDecimal taxRefundAmount;

    /**
     * 税局退税备注
     */

    private String taxRefundRemark;
}
