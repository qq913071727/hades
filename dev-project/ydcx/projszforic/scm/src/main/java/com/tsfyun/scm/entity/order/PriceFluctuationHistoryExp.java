package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2021-09-17
 */
@Data
public class PriceFluctuationHistoryExp implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 币制
     */

    private String currencyId;


    private String currencyName;

    /**
     * 客户名称
     */

    private String customerName;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 型号
     */

    private String model;

    /**
     * 名称
     */

    private String name;


    private String spec;

    /**
     * 进口日期
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date orderDate;

    /**
     * 订单号
     */

    private String orderDocNo;

    /**
     * 订单审价明细
     */

    private Long orderPriceMemberId;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 单位名称
     */

    private String unitName;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 美金单价
     */

    private BigDecimal usdTotalPrice;

    /**
     * 美金总价
     */

    private BigDecimal usdUnitPrice;


    private LocalDateTime dateCreated;

}
