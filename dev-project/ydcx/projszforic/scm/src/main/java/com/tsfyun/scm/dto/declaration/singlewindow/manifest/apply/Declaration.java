package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @since Created in 2020/4/22 11:08
 */
@Data
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Declaration", propOrder = {
        "declarationOfficeID",
        "id",
        "additionalInformation",
        "agent",
        "borderTransportMeans",
        "carrier",
        "consignment",
        "loadingLocation",
        "representativePerson",
        "unloadingLocation",
})
public class Declaration {

    //进出境口岸海关代码
    @XmlElement(name = "DeclarationOfficeID")
    protected String declarationOfficeID;

    @XmlElement(name = "ID")
    protected String id;

    //备注
    @XmlElement(name = "AdditionalInformation")
    protected AdditionalInformation additionalInformation;

    //运输工具代理企业数据（可以不用赋值）
    @XmlElement(name = "Agent")
    protected Agent agent;

    //运输工具数据
    @XmlElement(name = "BorderTransportMeans")
    protected BorderTransportMeans borderTransportMeans;

    //承运人数据
    @XmlElement(name = "Carrier")
    protected Carrier carrier;

    //提运（单）数据
    @XmlElement(name = "Consignment")
    protected List<Consignment> consignment;

    //装货地数据
    @XmlElement(name = "LoadingLocation")
    private LoadingLocation loadingLocation;

    //舱单传输人数据
    @XmlElement(name = "RepresentativePerson")
    private RepresentativePerson representativePerson;

    //卸货地数据
    @XmlElement(name = "UnloadingLocation")
    private UnloadingLocation unloadingLocation;

    public List<Consignment> getConsignment() {
        if(null == consignment) {
            return new ArrayList<Consignment>();
        }
        return consignment;
    }

}
