package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/20 10:16
 */
@Data
public class ExpOrderReceiveQTO extends PaginationDto implements Serializable {

    private Long customerId;// 客户ID

    private String orderNo;//订单编号

    private String salePersonName;// 销售人员

    @Pattern(regexp = "order_date|period_date",message = "查询日期类型错误")
    private String queryDate;

    /**=
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**=
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd){
        if(dateEnd!=null){
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }


}
