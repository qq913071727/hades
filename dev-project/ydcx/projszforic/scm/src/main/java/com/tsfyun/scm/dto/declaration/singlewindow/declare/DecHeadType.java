//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import com.tsfyun.common.base.util.jaxb.CDataAdapter;

import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 报关单表头信息
 * 
 * <p>DecHeadType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="DecHeadType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SeqNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *               &lt;minLength value="18"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IEFlag">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *               &lt;pattern value="[IE]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AgentCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AgentName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ApprNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BillNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ContrNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CustomMaster">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="4"/>
 *               &lt;pattern value="[0-9]{4}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CutMode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *               &lt;pattern value="[0-9]{3}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DistinatePort">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="6"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FeeCurr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FeeMark" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *               &lt;pattern value="[1-3]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FeeRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GrossWet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IEDate" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="8"/>
 *               &lt;pattern value="yyyyMMdd"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IEPort" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InsurCurr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InsurMark" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *               &lt;pattern value="[13]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InsurRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicenseNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *               &lt;pattern value="[0-9]{2}[-].{2}[-].+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ManualNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *               &lt;pattern value="[A-Z][0-9A-Z]{11}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NetWt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NoteS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OtherCurr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OtherMark" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *               &lt;pattern value="[1-3]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OtherRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnerCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="2"/>
 *               &lt;maxLength value="18"/>
 *               &lt;pattern value="[0-9]{10}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OwnerName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PackNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TradeCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeCountry">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="3"/>
 *               &lt;maxLength value="3"/>
 *               &lt;pattern value=""/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeMode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="4"/>
 *               &lt;maxLength value="4"/>
 *               &lt;pattern value="[0-9]{4}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TrafMode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *               &lt;pattern value="[0-9A-Z]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TrafName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TransMode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *               &lt;pattern value="[1-6]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WrapType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EntryId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PreEntryId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="9"/>
 *               &lt;maxLength value="9"/>
 *               &lt;pattern value="[0-9]{9}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Risk" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CopName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CopCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="9"/>
 *               &lt;maxLength value="9"/>
 *               &lt;pattern value=".{9}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EntryType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TypistNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *               &lt;pattern value="[0-9]{13}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InputerName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PartenerID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ChkSurety" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BillType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CopCodeScc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OwnerCodeScc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AgentCodeScc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeCoScc" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PromiseItmes">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeAreaCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MarkNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="400"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DespPortCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EntyPortCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsPlace">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BLNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InspOrgCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SpecDeclFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PurpOrgCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DespDate" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CmplDschrgDt" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CorrelationReasonFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VsaOrgCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OrigBoxFlag" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclareName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NoOtherPack">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OrgCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OverseasConsignorCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OverseasConsignorCname" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OverseasConsignorEname" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OverseasConsignorAddr" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OverseasConsigneeCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OverseasConsigneeEname" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="400"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DomesticConsigneeEname" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="400"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CorrelationNo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="500"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TradeCiqCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OwnerCiqCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclCiqCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecHeadType", propOrder = {
    "seqNo",
    "ieFlag",
    "agentCode",
    "agentName",
    "apprNo",
    "billNo",
    "contrNo",
    "customMaster",
    "cutMode",
    "distinatePort",
    "feeCurr",
    "feeMark",
    "feeRate",
    "grossWet",
    "ieDate",
    "iePort",
    "insurCurr",
    "insurMark",
    "insurRate",
    "licenseNo",
    "manualNo",
    "netWt",
    "noteS",
    "otherCurr",
    "otherMark",
    "otherRate",
    "ownerCode",
    "ownerName",
    "packNo",
    "tradeCode",
    "tradeCountry",
    "tradeMode",
    "tradeName",
    "trafMode",
    "trafName",
    "transMode",
    "wrapType",
    "entryId",
    "preEntryId",
    "risk",
    "copName",
    "copCode",
    "entryType",
    "typistNo",
    "inputerName",
    "partenerID",
    "chkSurety",
    "billType",
    "copCodeScc",
    "ownerCodeScc",
    "agentCodeScc",
    "tradeCoScc",
    "promiseItmes",
    "tradeAreaCode",
    "markNo",
    "despPortCode",
    "entyPortCode",
    "goodsPlace",
    "blNo",
    "inspOrgCode",
    "specDeclFlag",
    "purpOrgCode",
    "despDate",
    "cmplDschrgDt",
    "correlationReasonFlag",
    "vsaOrgCode",
    "origBoxFlag",
    "declareName",
    "noOtherPack",
    "orgCode",
    "overseasConsignorCode",
    "overseasConsignorCname",
    "overseasConsignorEname",
    "overseasConsignorAddr",
    "overseasConsigneeCode",
    "overseasConsigneeEname",
    "domesticConsigneeEname",
    "correlationNo",
    "tradeCiqCode",
    "ownerCiqCode",
    "declCiqCode",
    "declTrnRel",
    "ediId",
    "tgdNo",
    "type",
    "checkFlow",
    "taxAaminMark",
    "ediRemark",
    "ediRemark2",
    "dataSource",
     "pDate"
})
public class DecHeadType {

    //申报单位代码
    @XmlElement(name = "AgentCode", required = true)
    protected String agentCode;
    //申报单位名称
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "AgentName", required = true)
    protected String agentName;
    //批准文号，实填“外汇核销单号”
    @XmlElement(name = "ApprNo")
    protected String apprNo;
    //提单号
    @XmlElement(name = "BillNo")
    protected String billNo;
    //合同号
    @XmlElement(name = "ContrNo")
    protected String contrNo;
    //录入单位代码（海关编码）
    @XmlElement(name = "CopCode")
    protected String copCode;
    //录入单位名称
    @XmlElement(name = "CopName", required = true)
    protected String copName;
    //主管海关（申报地海关）
    @XmlElement(name = "CustomMaster", required = true)
    protected String customMaster;
    //征免性质
    @XmlElement(name = "CutMode")
    protected String cutMode;
    //数据来源（预留字段）
    @XmlElement(name = "DataSource")
    protected String dataSource;
    //报关/转关关系标志 0-一般报关单，1-转关提前报关单
    @XmlElement(name = "DeclTrnRel")
    protected String declTrnRel;
    //经停港/指运港
    @NotEmpty(message = "经停港/指运港不能为空")
    @XmlElement(name = "DistinatePort", required = true)
    protected String distinatePort;
    //报关标志 1-普通报关；3-北方转关提前；5-南方转关提前；6-普通报关，运输工具名称以‘◎’
    @XmlElement(name = "EdiId", required = true)
    protected String ediId;
    //海关编号
    @XmlElement(name = "EntryId")
    protected String entryId;
    //报关单类型 0-普通报关单，L-为带报关单清单的报关单，W-无纸报关类，D-既是清单又是无纸报关的情况，M-无纸化通关
    @XmlElement(name = "EntryType", required = true)
    protected String entryType;
    //运费币制
    @XmlElement(name = "FeeCurr")
    protected String feeCurr;
    //运费标记
    @XmlElement(name = "FeeMark")
    protected String feeMark;
    //运费/率
    @XmlElement(name = "FeeRate")
    protected String feeRate;
    //毛重
    @NotEmpty(message = "毛重不能为空")
    @XmlElement(name = "GrossWet", required = true)
    protected String grossWet;
    //进出口日期，格式如yyyyMMdd
    @XmlElement(name = "IEDate")
    protected String ieDate;
    //进出口标志，I-进口，E-出口
    @XmlElement(name = "IEFlag", required = true)
    protected String ieFlag;
    //进出口岸，需要代码转换
    @XmlElement(name = "IEPort")
    protected String iePort;
    //录入员名称
    @XmlElement(name = "InputerName")
    protected String inputerName;
    //保险费币制
    @XmlElement(name = "InsurCurr")
    protected String insurCurr;
    //保险费标记
    @XmlElement(name = "InsurMark")
    protected String insurMark;
    //保险费/率
    @XmlElement(name = "InsurRate")
    protected String insurRate;
    //许可证编号
    @XmlElement(name = "LicenseNo")
    protected String licenseNo;
    //备案号
    @XmlElement(name = "ManualNo")
    protected String manualNo;
    //净重
    @XmlElement(name = "NetWt", required = true)
    protected String netWt;
    //备注
    @XmlElement(name = "NoteS")
    protected String noteS;
    //杂费币制
    @XmlElement(name = "OtherCurr")
    protected String otherCurr;
    //杂费标志
    @XmlElement(name = "OtherMark")
    protected String otherMark;
    //杂费/率
    @XmlElement(name = "OtherRate")
    protected String otherRate;
    //消费使用/生产销售单位代码，10位或9位或者NO，单位海关编码
    @XmlElement(name = "OwnerCode")
    protected String ownerCode;
    //消费使用/生产销售单位名称
    @XmlElement(name = "OwnerName", required = true)
    protected String ownerName;
    //件数
    @NotEmpty(message = "件数不能为空")
    @XmlElement(name = "PackNo", required = true)
    protected String packNo;
    //申报人标识,申报人姓名
    @XmlElement(name = "PartenerID")
    protected String partenerID;
    //打印日期，预录入日期，格式如yyyyMMdd
    @XmlElement(name = "PDate")
    protected String pDate;
    //预录入编号
    @XmlElement(name = "PreEntryId")
    protected String preEntryId;
    //风险评估参数
    @XmlElement(name = "Risk")
    protected String risk;
    //数据中心统一编号，唯一标识一票报关单。首次导入传空值，由系统自动生成并返回客户端
    @XmlElement(name = "SeqNo")
    protected String seqNo;
    //关联单据号，预留字段
    @XmlElement(name = "TgdNo")
    protected String tgdNo;
    //进口-启运国；出口-运抵国
    @NotEmpty(message = "启运国/运抵国不能为空")
    @XmlElement(name = "TradeCountry", required = true)
    protected String tradeCountry;
    //监管方式
    @XmlElement(name = "TradeMode", required = true)
    protected String tradeMode;
    //境内收发货人编号，海关编号
    @NotEmpty(message = "境内收发货人编号不能为空")
    @XmlElement(name = "TradeCode", required = true)
    protected String tradeCode;
    //运输方式代码
    @NotEmpty(message = "运输方式代码不能为空")
    @XmlElement(name = "TrafMode", required = true)
    protected String trafMode;
    //运输工具代码及名称
    @NotEmpty(message = "运输工具代码及名称不能为空")
    @XmlElement(name = "TrafName")
    protected String trafName;
    //境内收发货人名称
    @NotEmpty(message = "境内收发货人名称不能为空")
    @XmlElement(name = "TradeName", required = true)
    protected String tradeName;
    //成交方式
    @XmlElement(name = "TransMode")
    protected String transMode;
    //单据类型
    /*
    前两位：一般报关单填空值；
    ML：保税区进出境备案清单
    SD: “属地申报，口岸验放”报关单
    LY:两单一审备案清单；
    CL:汇总征税报关单；
    SS:”属地申报，属地验放”报关单
    ZB：自主报税
    SW：税单无纸化
    SZ：水运中转普通报关单
    SM：水运中转保税区进出境备案清单
    SL：水运中转两单一审备案清单
    Z:自报自缴
    MF:公路舱单跨境快速通关报关单
    MK: 公路舱单跨境快速通关备案清单
    ZY:自报自缴，两单一审备案清单
    ZC:自报自缴，汇总征税报关单
    ZW:自报自缴，税单无纸化
    ZF:自报自缴，公路舱单跨境快速通关
    ZX：自报自缴，多项式联运
    MT:多项式联运。
    EX：低值快速货物报关单；
    EC：低值快速货物汇总征税报关单；
    EZ：低值快速货物自报自缴报关单；
    ES：低值快速货物汇总征税、自报自缴报关单。

    第三位：
    0/空：整合申报一次录入；
    1：概要申报；
    2：完整申报;
    3：两步申报一次录入。

    第四位：涉证标记0-否，1-是，概要申报必传。
    第五位：涉检标记0-否，1-是，概要申报必传。
    第六位：涉税标记0-否，1-是，概要申报必传。
     */
    @XmlElement(name = "Type")
    private String type;
    //录入员IC卡号
    @XmlElement(name = "TypistNo")
    protected String typistNo;
    //包装种类
    @NotEmpty(message = "包装种类不能为空")
    @XmlElement(name = "WrapType", required = true)
    protected String wrapType;
    //担保验放标志
    @XmlElement(name = "ChkSurety")
    protected String chkSurety;
    //备案清单类型
    @XmlElement(name = "BillType")
    protected String billType;
    //录入单位统一编码（统一信用代码）
    @XmlElement(name = "CopCodeScc")
    protected String copCodeScc;
    //收发货人统一编码（统一信用代码）
    @XmlElement(name = "TradeCoScc")
    protected String tradeCoScc;
    //申报代码统一编码（统一信用代码）
    @XmlElement(name = "AgentCodeScc")
    protected String agentCodeScc;
    //消费使用/生产销售单位统一编码
    @XmlElement(name = "OwnerCodeScc")
    protected String ownerCodeScc;
    //承诺事项
    @XmlElement(name = "PromiseItmes", required = true)
    protected String promiseItmes;
    //贸易国别
    @NotEmpty(message = "贸易国别不能为空")
    @XmlElement(name = "TradeAreaCode", required = true)
    protected String tradeAreaCode;
    //查验分流 1-查验分流 0 -不是查验分流
    @XmlElement(name = "CheckFlow")
    protected String checkFlow;
    //税收征管标记 0-无1-有
    @XmlElement(name = "TaxAaminMark")
    protected String taxAaminMark;
    //标记唛码
    @XmlElement(name = "MarkNo")
    protected String markNo;
    //启运港代码
    @NotEmpty(message = "启运港代码不能为空")
    @XmlElement(name = "DespPortCode", required = true)
    protected String despPortCode;
    //入境口岸代码
    @NotEmpty(message = "入境口岸代码不能为空")
    @XmlElement(name = "EntyPortCode", required = true)
    protected String entyPortCode;
    //存放地点
    @NotEmpty(message = "货物存放地点不能为空")
    @XmlElement(name = "GoodsPlace", required = true)
    protected String goodsPlace;
    //B/L号
    @XmlElement(name = "BLNo")
    protected String blNo;
    //口岸检验检疫机关代码
    @XmlElement(name = "InspOrgCode")
    protected String inspOrgCode;
    //特种业务标识
    /*特种业务标识：0未勾选，1选中。
    第一位：“国际赛事”；
    第二位：“特殊进出军工物资”；
    第三位:“国际援助物资”；
    第四位：“国际会议”；
    第五位：“直通放行”；
    第六位：“外交礼遇”；
    第七位：“转关“
     */
    @XmlElement(name = "SpecDeclFlag")
    protected String specDeclFlag;
    //目的地检验检疫机关
    @XmlElement(name = "PurpOrgCode")
    protected String purpOrgCode;
    //启运日期
    @XmlElement(name = "DespDate")
    protected String despDate;
    //卸毕日期
    @XmlElement(name = "CmplDschrgDt")
    protected String cmplDschrgDt;
    //关联理由
    @XmlElement(name = "CorrelationReasonFlag")
    protected String correlationReasonFlag;
    //领证机关
    @XmlElement(name = "VsaOrgCode")
    protected String vsaOrgCode;
    //原集装箱标识
    @XmlElement(name = "OrigBoxFlag")
    protected String origBoxFlag;
    //申报人姓名
    @XmlElement(name = "DeclareName", required = true)
    protected String declareName;
    //无其他包装 0-未选，有其他包装 1-选中，无其他包装
    @XmlElement(name = "NoOtherPack", required = true)
    protected String noOtherPack;
    //校验检疫受理机关
    @XmlElement(name = "OrgCode")
    protected String orgCode;
    //境外发货人代码
    @XmlElement(name = "OverseasConsignorCode")
    protected String overseasConsignorCode;
    //境外发货人名称
    @XmlElement(name = "OverseasConsignorCname")
    protected String overseasConsignorCname;
    //境外发货人外文名称
    @XmlElement(name = "OverseasConsignorEname")
    protected String overseasConsignorEname;
    //境外发货人地址
    @XmlElement(name = "OverseasConsignorAddr")
    protected String overseasConsignorAddr;
    //境外收货人编码
    @XmlElement(name = "OverseasConsigneeCode")
    protected String overseasConsigneeCode;
    //境外收发货人名称（外文)
    @XmlElement(name = "OverseasConsigneeEname")
    protected String overseasConsigneeEname;
    //境内收发货人名称（外文）
    @XmlElement(name = "DomesticConsigneeEname")
    protected String domesticConsigneeEname;
    //关联号码
    @XmlElement(name = "CorrelationNo")
    protected String correlationNo;
    //EDI申报备注
    @XmlElement(name = "EdiRemark")
    protected String ediRemark;
    //EDI申报备注2
    @XmlElement(name = "EdiRemark2")
    protected String ediRemark2;
    //境内收发货人检验检疫编码
    @XmlElement(name = "TradeCiqCode")
    protected String tradeCiqCode;
    //消费使用/生产销售单位检验检疫编码
    @XmlElement(name = "OwnerCiqCode")
    protected String ownerCiqCode;
    //申报单位检验检疫编码
    @XmlElement(name = "DeclCiqCode")
    protected String declCiqCode;

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getIeDate() {
        return ieDate;
    }

    public void setIeDate(String ieDate) {
        this.ieDate = ieDate;
    }

    public String getIeFlag() {
        return ieFlag;
    }

    public void setIeFlag(String ieFlag) {
        this.ieFlag = ieFlag;
    }

    public String getIePort() {
        return iePort;
    }

    public void setIePort(String iePort) {
        this.iePort = iePort;
    }

    public String getpDate() {
        return pDate;
    }

    public void setpDate(String pDate) {
        this.pDate = pDate;
    }

    public String getTgdNo() {
        return tgdNo;
    }

    public void setTgdNo(String tgdNo) {
        this.tgdNo = tgdNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCheckFlow() {
        return checkFlow;
    }

    public void setCheckFlow(String checkFlow) {
        this.checkFlow = checkFlow;
    }

    public String getTaxAaminMark() {
        return taxAaminMark;
    }

    public void setTaxAaminMark(String taxAaminMark) {
        this.taxAaminMark = taxAaminMark;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getEdiRemark() {
        return ediRemark;
    }

    public void setEdiRemark(String ediRemark) {
        this.ediRemark = ediRemark;
    }

    public String getEdiRemark2() {
        return ediRemark2;
    }

    public void setEdiRemark2(String ediRemark2) {
        this.ediRemark2 = ediRemark2;
    }

    public String getEdiId() {
        return ediId;
    }

    public void setEdiId(String ediId) {
        this.ediId = ediId;
    }

    public String getDeclTrnRel() {
        return declTrnRel;
    }

    public void setDeclTrnRel(String declTrnRel) {
        this.declTrnRel = declTrnRel;
    }

    /**
     * 获取seqNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * 设置seqNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeqNo(String value) {
        this.seqNo = value;
    }

    /**
     * 获取ieFlag属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIEFlag() {
        return ieFlag;
    }

    /**
     * 设置ieFlag属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIEFlag(String value) {
        this.ieFlag = value;
    }

    /**
     * 获取agentCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCode() {
        return agentCode;
    }

    /**
     * 设置agentCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCode(String value) {
        this.agentCode = value;
    }

    /**
     * 获取agentName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * 设置agentName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentName(String value) {
        this.agentName = value;
    }

    /**
     * 获取apprNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprNo() {
        return apprNo;
    }

    /**
     * 设置apprNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprNo(String value) {
        this.apprNo = value;
    }

    /**
     * 获取billNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * 设置billNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillNo(String value) {
        this.billNo = value;
    }

    /**
     * 获取contrNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrNo() {
        return contrNo;
    }

    /**
     * 设置contrNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrNo(String value) {
        this.contrNo = value;
    }

    /**
     * 获取customMaster属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomMaster() {
        return customMaster;
    }

    /**
     * 设置customMaster属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomMaster(String value) {
        this.customMaster = value;
    }

    /**
     * 获取cutMode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCutMode() {
        return cutMode;
    }

    /**
     * 设置cutMode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCutMode(String value) {
        this.cutMode = value;
    }

    /**
     * 获取distinatePort属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistinatePort() {
        return distinatePort;
    }

    /**
     * 设置distinatePort属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistinatePort(String value) {
        this.distinatePort = value;
    }

    /**
     * 获取feeCurr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeCurr() {
        return feeCurr;
    }

    /**
     * 设置feeCurr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeCurr(String value) {
        this.feeCurr = value;
    }

    /**
     * 获取feeMark属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeMark() {
        return feeMark;
    }

    /**
     * 设置feeMark属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeMark(String value) {
        this.feeMark = value;
    }

    /**
     * 获取feeRate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeRate() {
        return feeRate;
    }

    /**
     * 设置feeRate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeRate(String value) {
        this.feeRate = value;
    }

    /**
     * 获取grossWet属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossWet() {
        return grossWet;
    }

    /**
     * 设置grossWet属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossWet(String value) {
        this.grossWet = value;
    }

    /**
     * 获取ieDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIEDate() {
        return ieDate;
    }

    /**
     * 设置ieDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIEDate(String value) {
        this.ieDate = value;
    }

    /**
     * 获取iePort属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIEPort() {
        return iePort;
    }

    /**
     * 设置iePort属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIEPort(String value) {
        this.iePort = value;
    }

    /**
     * 获取insurCurr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurCurr() {
        return insurCurr;
    }

    /**
     * 设置insurCurr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurCurr(String value) {
        this.insurCurr = value;
    }

    /**
     * 获取insurMark属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurMark() {
        return insurMark;
    }

    /**
     * 设置insurMark属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurMark(String value) {
        this.insurMark = value;
    }

    /**
     * 获取insurRate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurRate() {
        return insurRate;
    }

    /**
     * 设置insurRate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurRate(String value) {
        this.insurRate = value;
    }

    /**
     * 获取licenseNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNo() {
        return licenseNo;
    }

    /**
     * 设置licenseNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNo(String value) {
        this.licenseNo = value;
    }

    /**
     * 获取manualNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualNo() {
        return manualNo;
    }

    /**
     * 设置manualNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualNo(String value) {
        this.manualNo = value;
    }

    /**
     * 获取netWt属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetWt() {
        return netWt;
    }

    /**
     * 设置netWt属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetWt(String value) {
        this.netWt = value;
    }

    /**
     * 获取noteS属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoteS() {
        return noteS;
    }

    /**
     * 设置noteS属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoteS(String value) {
        this.noteS = value;
    }

    /**
     * 获取otherCurr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherCurr() {
        return otherCurr;
    }

    /**
     * 设置otherCurr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherCurr(String value) {
        this.otherCurr = value;
    }

    /**
     * 获取otherMark属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherMark() {
        return otherMark;
    }

    /**
     * 设置otherMark属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherMark(String value) {
        this.otherMark = value;
    }

    /**
     * 获取otherRate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherRate() {
        return otherRate;
    }

    /**
     * 设置otherRate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherRate(String value) {
        this.otherRate = value;
    }

    /**
     * 获取ownerCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerCode() {
        return ownerCode;
    }

    /**
     * 设置ownerCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerCode(String value) {
        this.ownerCode = value;
    }

    /**
     * 获取ownerName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * 设置ownerName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerName(String value) {
        this.ownerName = value;
    }

    /**
     * 获取packNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackNo() {
        return packNo;
    }

    /**
     * 设置packNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackNo(String value) {
        this.packNo = value;
    }

    /**
     * 获取tradeCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeCode() {
        return tradeCode;
    }

    /**
     * 设置tradeCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeCode(String value) {
        this.tradeCode = value;
    }

    /**
     * 获取tradeCountry属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeCountry() {
        return tradeCountry;
    }

    /**
     * 设置tradeCountry属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeCountry(String value) {
        this.tradeCountry = value;
    }

    /**
     * 获取tradeMode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeMode() {
        return tradeMode;
    }

    /**
     * 设置tradeMode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeMode(String value) {
        this.tradeMode = value;
    }

    /**
     * 获取tradeName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeName() {
        return tradeName;
    }

    /**
     * 设置tradeName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeName(String value) {
        this.tradeName = value;
    }

    /**
     * 获取trafMode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrafMode() {
        return trafMode;
    }

    /**
     * 设置trafMode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrafMode(String value) {
        this.trafMode = value;
    }

    /**
     * 获取trafName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrafName() {
        return trafName;
    }

    /**
     * 设置trafName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrafName(String value) {
        this.trafName = value;
    }

    /**
     * 获取transMode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransMode() {
        return transMode;
    }

    /**
     * 设置transMode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransMode(String value) {
        this.transMode = value;
    }

    /**
     * 获取wrapType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrapType() {
        return wrapType;
    }

    /**
     * 设置wrapType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrapType(String value) {
        this.wrapType = value;
    }

    /**
     * 获取entryId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryId() {
        return entryId;
    }

    /**
     * 设置entryId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryId(String value) {
        this.entryId = value;
    }

    /**
     * 获取preEntryId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreEntryId() {
        return preEntryId;
    }

    /**
     * 设置preEntryId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreEntryId(String value) {
        this.preEntryId = value;
    }

    /**
     * 获取risk属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRisk() {
        return risk;
    }

    /**
     * 设置risk属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRisk(String value) {
        this.risk = value;
    }

    /**
     * 获取copName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopName() {
        return copName;
    }

    /**
     * 设置copName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopName(String value) {
        this.copName = value;
    }

    /**
     * 获取copCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopCode() {
        return copCode;
    }

    /**
     * 设置copCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopCode(String value) {
        this.copCode = value;
    }

    /**
     * 获取entryType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryType() {
        return entryType;
    }

    /**
     * 设置entryType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryType(String value) {
        this.entryType = value;
    }

    /**
     * 获取typistNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypistNo() {
        return typistNo;
    }

    /**
     * 设置typistNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypistNo(String value) {
        this.typistNo = value;
    }

    /**
     * 获取inputerName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInputerName() {
        return inputerName;
    }

    /**
     * 设置inputerName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInputerName(String value) {
        this.inputerName = value;
    }

    /**
     * 获取partenerID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartenerID() {
        return partenerID;
    }

    /**
     * 设置partenerID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartenerID(String value) {
        this.partenerID = value;
    }

    /**
     * 获取chkSurety属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChkSurety() {
        return chkSurety;
    }

    /**
     * 设置chkSurety属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChkSurety(String value) {
        this.chkSurety = value;
    }

    /**
     * 获取billType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillType() {
        return billType;
    }

    /**
     * 设置billType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillType(String value) {
        this.billType = value;
    }

    /**
     * 获取copCodeScc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopCodeScc() {
        return copCodeScc;
    }

    /**
     * 设置copCodeScc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopCodeScc(String value) {
        this.copCodeScc = value;
    }

    /**
     * 获取ownerCodeScc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerCodeScc() {
        return ownerCodeScc;
    }

    /**
     * 设置ownerCodeScc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerCodeScc(String value) {
        this.ownerCodeScc = value;
    }

    /**
     * 获取agentCodeScc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCodeScc() {
        return agentCodeScc;
    }

    /**
     * 设置agentCodeScc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCodeScc(String value) {
        this.agentCodeScc = value;
    }

    /**
     * 获取tradeCoScc属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeCoScc() {
        return tradeCoScc;
    }

    /**
     * 设置tradeCoScc属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeCoScc(String value) {
        this.tradeCoScc = value;
    }

    /**
     * 获取promiseItmes属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromiseItmes() {
        return promiseItmes;
    }

    /**
     * 设置promiseItmes属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromiseItmes(String value) {
        this.promiseItmes = value;
    }

    /**
     * 获取tradeAreaCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeAreaCode() {
        return tradeAreaCode;
    }

    /**
     * 设置tradeAreaCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeAreaCode(String value) {
        this.tradeAreaCode = value;
    }

    /**
     * 获取markNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarkNo() {
        return markNo;
    }

    /**
     * 设置markNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarkNo(String value) {
        this.markNo = value;
    }

    /**
     * 获取despPortCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDespPortCode() {
        return despPortCode;
    }

    /**
     * 设置despPortCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDespPortCode(String value) {
        this.despPortCode = value;
    }

    /**
     * 获取entyPortCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntyPortCode() {
        return entyPortCode;
    }

    /**
     * 设置entyPortCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntyPortCode(String value) {
        this.entyPortCode = value;
    }

    /**
     * 获取goodsPlace属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsPlace() {
        return goodsPlace;
    }

    /**
     * 设置goodsPlace属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsPlace(String value) {
        this.goodsPlace = value;
    }

    /**
     * 获取blNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLNo() {
        return blNo;
    }

    /**
     * 设置blNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLNo(String value) {
        this.blNo = value;
    }

    /**
     * 获取inspOrgCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspOrgCode() {
        return inspOrgCode;
    }

    /**
     * 设置inspOrgCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspOrgCode(String value) {
        this.inspOrgCode = value;
    }

    /**
     * 获取specDeclFlag属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecDeclFlag() {
        return specDeclFlag;
    }

    /**
     * 设置specDeclFlag属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecDeclFlag(String value) {
        this.specDeclFlag = value;
    }

    /**
     * 获取purpOrgCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurpOrgCode() {
        return purpOrgCode;
    }

    /**
     * 设置purpOrgCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurpOrgCode(String value) {
        this.purpOrgCode = value;
    }

    /**
     * 获取despDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDespDate() {
        return despDate;
    }

    /**
     * 设置despDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDespDate(String value) {
        this.despDate = value;
    }

    /**
     * 获取cmplDschrgDt属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmplDschrgDt() {
        return cmplDschrgDt;
    }

    /**
     * 设置cmplDschrgDt属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmplDschrgDt(String value) {
        this.cmplDschrgDt = value;
    }

    /**
     * 获取correlationReasonFlag属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationReasonFlag() {
        return correlationReasonFlag;
    }

    /**
     * 设置correlationReasonFlag属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationReasonFlag(String value) {
        this.correlationReasonFlag = value;
    }

    /**
     * 获取vsaOrgCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVsaOrgCode() {
        return vsaOrgCode;
    }

    /**
     * 设置vsaOrgCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVsaOrgCode(String value) {
        this.vsaOrgCode = value;
    }

    /**
     * 获取origBoxFlag属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigBoxFlag() {
        return origBoxFlag;
    }

    /**
     * 设置origBoxFlag属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigBoxFlag(String value) {
        this.origBoxFlag = value;
    }

    /**
     * 获取declareName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclareName() {
        return declareName;
    }

    /**
     * 设置declareName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclareName(String value) {
        this.declareName = value;
    }

    /**
     * 获取noOtherPack属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoOtherPack() {
        return noOtherPack;
    }

    /**
     * 设置noOtherPack属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoOtherPack(String value) {
        this.noOtherPack = value;
    }

    /**
     * 获取orgCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgCode() {
        return orgCode;
    }

    /**
     * 设置orgCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgCode(String value) {
        this.orgCode = value;
    }

    /**
     * 获取overseasConsignorCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasConsignorCode() {
        return overseasConsignorCode;
    }

    /**
     * 设置overseasConsignorCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasConsignorCode(String value) {
        this.overseasConsignorCode = value;
    }

    /**
     * 获取overseasConsignorCname属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasConsignorCname() {
        return overseasConsignorCname;
    }

    /**
     * 设置overseasConsignorCname属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasConsignorCname(String value) {
        this.overseasConsignorCname = value;
    }

    /**
     * 获取overseasConsignorEname属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasConsignorEname() {
        return overseasConsignorEname;
    }

    /**
     * 设置overseasConsignorEname属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasConsignorEname(String value) {
        this.overseasConsignorEname = value;
    }

    /**
     * 获取overseasConsignorAddr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasConsignorAddr() {
        return overseasConsignorAddr;
    }

    /**
     * 设置overseasConsignorAddr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasConsignorAddr(String value) {
        this.overseasConsignorAddr = value;
    }

    /**
     * 获取overseasConsigneeCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasConsigneeCode() {
        return overseasConsigneeCode;
    }

    /**
     * 设置overseasConsigneeCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasConsigneeCode(String value) {
        this.overseasConsigneeCode = value;
    }

    /**
     * 获取overseasConsigneeEname属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasConsigneeEname() {
        return overseasConsigneeEname;
    }

    /**
     * 设置overseasConsigneeEname属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasConsigneeEname(String value) {
        this.overseasConsigneeEname = value;
    }

    /**
     * 获取domesticConsigneeEname属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomesticConsigneeEname() {
        return domesticConsigneeEname;
    }

    /**
     * 设置domesticConsigneeEname属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomesticConsigneeEname(String value) {
        this.domesticConsigneeEname = value;
    }

    /**
     * 获取correlationNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationNo() {
        return correlationNo;
    }

    /**
     * 设置correlationNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationNo(String value) {
        this.correlationNo = value;
    }

    /**
     * 获取tradeCiqCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeCiqCode() {
        return tradeCiqCode;
    }

    /**
     * 设置tradeCiqCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeCiqCode(String value) {
        this.tradeCiqCode = value;
    }

    /**
     * 获取ownerCiqCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerCiqCode() {
        return ownerCiqCode;
    }

    /**
     * 设置ownerCiqCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerCiqCode(String value) {
        this.ownerCiqCode = value;
    }

    /**
     * 获取declCiqCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclCiqCode() {
        return declCiqCode;
    }

    /**
     * 设置declCiqCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclCiqCode(String value) {
        this.declCiqCode = value;
    }

}
