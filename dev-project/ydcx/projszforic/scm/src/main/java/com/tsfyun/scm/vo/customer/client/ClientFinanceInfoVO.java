package com.tsfyun.scm.vo.customer.client;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 客户财务信息
 * @CreateDate: Created in 2020/11/2 09:50
 */
@Data
public class ClientFinanceInfoVO implements Serializable {

    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    /**
     * 优惠券金额
     */
    @ApiModelProperty(value = "优惠券金额")
    private BigDecimal couponNumber;

    /**
     * 税款额度
     */
    @ApiModelProperty(value = "税款额度")
    private BigDecimal taxAmount;

    /**
     * 货款额度
     */
    @ApiModelProperty(value = "货款额度")
    private BigDecimal goodsAmount;

    /**
     * 逾期金额
     */
    @ApiModelProperty(value = "逾期金额")
    private BigDecimal totalArrearsVal;

}
