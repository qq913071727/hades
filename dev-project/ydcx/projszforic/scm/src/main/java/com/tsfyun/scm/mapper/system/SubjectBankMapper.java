package com.tsfyun.scm.mapper.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.system.SubjectBankQTO;
import com.tsfyun.scm.entity.system.SubjectBank;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.system.SubjectBankVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-20
 */
@Repository
public interface SubjectBankMapper extends Mapper<SubjectBank> {

    List<SubjectBankVO> pageList(SubjectBankQTO qto);
}
