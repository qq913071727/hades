package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.SettlementAccountMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.OverseasReceivingAccountVO;
import com.tsfyun.scm.vo.finance.SettlementAccountMemberVO;

import java.util.List;

/**
 * <p>
 * 结汇明细 服务类
 * </p>
 *
 *
 * @since 2021-10-14
 */
public interface ISettlementAccountMemberService extends IService<SettlementAccountMember> {


    List<OverseasReceivingAccountVO> findOverseasReceivingAccount(Long settlementAccountId);

    List<SettlementAccountMember> findByAccountId(Long settlementAccountId);
}
