package com.tsfyun.scm.mapper.view;

import com.tsfyun.scm.dto.view.ViewExpDifferenceMemberQTO;
import com.tsfyun.scm.entity.view.ViewExpDifferenceMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.view.ViewExpDifferenceMemberVO;
import com.tsfyun.scm.vo.view.ViewExpDifferenceSummaryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * VIEW Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-18
 */
@Repository
public interface ViewExpDifferenceMemberMapper extends Mapper<ViewExpDifferenceMember> {

    /**
     * 客户票差明细
     * @param qto
     * @return
     */
    List<ViewExpDifferenceMemberVO> expDifferenceMemberList(ViewExpDifferenceMemberQTO qto);

    /**
     * 获取客户票差总额
     * @param customerId
     * @return
     */
    BigDecimal getDifferenceVal(@Param(value = "customerId")Long customerId);


    /**
     * 客户票差汇总
     * @param qto
     * @return
     */
    List<ViewExpDifferenceSummaryVO> expDifferenceSummaryList(ViewExpDifferenceMemberQTO qto);

}
