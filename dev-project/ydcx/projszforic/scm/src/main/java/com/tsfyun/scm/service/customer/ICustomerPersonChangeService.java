package com.tsfyun.scm.service.customer;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.customer.CustomerPersonChange;
import com.tsfyun.scm.entity.user.Person;
import com.tsfyun.scm.vo.customer.CustomerPersonChangeVO;

import java.util.List;

/**
 * <p>
 *  客户人员变更记录
 * </p>
 *

 * @since 2020-03-12
 */
public interface ICustomerPersonChangeService extends IService<CustomerPersonChange> {
    //记录销售变更
    void saveSaleChange(Long customerId,Person oldPerson,Person nowPerson,String memo);
    //记录商务变更
    void saveBusChange(Long customerId,Person oldPerson,Person nowPerson,String memo);

    /**
     * 获取客户商务/销售变更记录
     * @return
     */
    List<CustomerPersonChangeVO> records(Integer personType,Long customerId);

}
