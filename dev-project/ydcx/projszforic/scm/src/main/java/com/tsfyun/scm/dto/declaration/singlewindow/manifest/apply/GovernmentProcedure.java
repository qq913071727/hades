package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 海关通关货物信息
 * @since Created in 2020/4/22 12:14
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GovernmentProcedure", propOrder = {
        "currentCode",
})
public class GovernmentProcedure {

    //海关货物通关代码
    @XmlElement(name = "CurrentCode")
    private String currentCode;

    public GovernmentProcedure() {

    }

    public GovernmentProcedure(String currentCode) {
        this.currentCode = currentCode;
    }

    public String getCurrentCode() {
        return currentCode;
    }

    public void setCurrentCode(String currentCode) {
        this.currentCode = currentCode;
    }
}
