package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ImpOrderDomesticTakeQTO;
import com.tsfyun.scm.service.order.IImpOrderLogisticsService;
import com.tsfyun.scm.vo.order.ImpOrderDomesticVO;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单物流信息 前端控制器
 * </p>
 *
 * @since 2020-04-08
 */
@AllArgsConstructor
@RestController
@RequestMapping("/impOrderLogistics")
public class ImpOrderLogisticsController extends BaseController {

    private final IImpOrderLogisticsService impOrderLogisticsService;

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ImpOrderDomesticVO>> list(@ModelAttribute @Validated ImpOrderDomesticTakeQTO qto) {
        PageInfo<ImpOrderDomesticVO> page = impOrderLogisticsService.pageList(qto);
        return success((int) page.getTotal(),page.getList());
    }

    /**
     * 提货完成
     * @param id
     * @return
     */
    @PostMapping(value = "takeConfirm")
    public Result<Void> takeConfirm(@RequestParam(value = "id")Long id,
                                    @RequestParam(value = "takeTime",required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime takeTime) {
        impOrderLogisticsService.takeConfirm(id,takeTime);
        return success( );
    }



}

