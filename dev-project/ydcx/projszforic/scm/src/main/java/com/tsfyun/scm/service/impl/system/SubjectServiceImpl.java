package com.tsfyun.scm.service.impl.system;

import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.enums.ResultCodeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.system.SubjectDTO;
import com.tsfyun.scm.entity.system.Subject;
import com.tsfyun.scm.service.system.ISubjectService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.user.IPersonService;
import com.tsfyun.scm.vo.system.SubjectSimpleVO;
import com.tsfyun.scm.vo.system.SubjectVO;
import com.tsfyun.scm.vo.system.SubjectVersionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-03
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<Subject> implements ISubjectService {

    @Autowired
    private OrikaBeanMapper beanMapper;
    @Autowired
    private IPersonService personService;

    @Override
    public SubjectVO findByCode() {
        Subject condition = new Subject();
        Subject subject= super.getOne(condition);
        TsfPreconditions.checkArgument(Objects.nonNull(subject),new ServiceException(ResultCodeEnum.DATA_NOT_FOUND));
        SubjectVO subjectVO = beanMapper.map(subject,SubjectVO.class);
        subjectVO.setTenantOpenVersion("");
        return subjectVO;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(SubjectDTO dto) {
        Subject condition = new Subject();
        Subject update = super.getOne(condition);
        update.setName(dto.getName());
        update.setNameEn(dto.getNameEn());
        update.setSocialNo(dto.getSocialNo());
        update.setTaxpayerNo(dto.getTaxpayerNo());
        update.setLegalPerson(dto.getLegalPerson());
        update.setCustomsCode(dto.getCustomsCode());
        update.setCiqNo(dto.getCiqNo());
        update.setTel(dto.getTel());
        update.setFax(dto.getFax());
        update.setMail(dto.getMail());
        update.setAddress(dto.getAddress());
        update.setAddressEn(dto.getAddressEn());
        update.setRegAddress(dto.getRegAddress());
        update.setChapter(StringUtils.removeSpecialSymbol(dto.getChapter()));
        update.setBrowserTitle(dto.getBrowserTitle());
        update.setLogo(dto.getLogo());
        update.setLoginBg(dto.getLoginBg());
        update.setUpdateBy(SecurityUtil.getCurrentPersonIdAndName());
        update.setDateUpdated(LocalDateTime.now());

        super.updateById(update);
    }

    @Override
    public SubjectSimpleVO getSimpleById(Long id) {
        Subject subject = super.getById(id);
        if(Objects.nonNull(subject)){
            return beanMapper.map(subject,SubjectSimpleVO.class);
        }
        return null;
    }

    @Override
    public SubjectVersionVO version() {
        Subject condition = new Subject();
        Subject subject = super.getOne(condition);
        SubjectVersionVO svvo = new SubjectVersionVO();
        svvo.setCode("");
        svvo.setCompanyName(subject.getName());
        svvo.setDomain("");
        svvo.setOpenVersion("");
        svvo.setInvalidTime(null);
        svvo.setStaffNumber(999999999);
        svvo.setUseNumber(personService.countPersonNumber());
        svvo.setPermit("");
        return svvo;
    }
}
