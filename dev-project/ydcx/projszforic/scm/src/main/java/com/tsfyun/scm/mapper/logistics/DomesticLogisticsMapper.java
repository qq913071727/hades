package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.dto.logistics.DomesticLogisticsDTO;
import com.tsfyun.scm.entity.logistics.DomesticLogistics;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.logistics.DomesticLogisticsVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 国内物流 Mapper 接口
 * </p>
 *

 * @since 2020-03-25
 */
@Repository
public interface DomesticLogisticsMapper extends Mapper<DomesticLogistics> {

    List<DomesticLogisticsVO> list(DomesticLogisticsDTO dto);

}
