package com.tsfyun.scm.controller.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.SupplierBankDTO;
import com.tsfyun.scm.dto.customer.SupplierBankQTO;
import com.tsfyun.scm.service.customer.ISupplierBankService;
import com.tsfyun.scm.vo.customer.SupplierBankVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 供应商银行前端控制器
 */
@RestController
@RequestMapping(value = "supplierBank")
public class SupplierBankController extends BaseController {

    @Autowired
    private ISupplierBankService supplierBankService;

    /**
     * 获取供应商银行下拉
     * @param supplierId
     * @return
     */
    @GetMapping("select")
    public Result<List<SupplierBankVO>> select(@RequestParam("supplierId")Long supplierId){
        return success(supplierBankService.select(supplierId));
    }
    @PostMapping("vague")
    public Result<List<SupplierBankVO>> vague(@RequestParam(value = "customerId")Long customerId,@RequestParam(value = "supplierName",required = false)String supplierName,@RequestParam(value = "bankName",required = false)String bankName){
        return success(supplierBankService.vague(customerId,supplierName,bankName));
    }

    /**
     * 根据id获取供应商银行信息
     * @param id
     * @return
     */
    @GetMapping("detail")
    public Result<SupplierBankVO> detail(@RequestParam("id")Long id){
        return success(supplierBankService.detail(id));
    }


    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        supplierBankService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 新增供应商银行信息
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Long> add(@ModelAttribute @Validated(AddGroup.class) SupplierBankDTO dto) {
        return success(supplierBankService.add(dto));
    }

    /**
     * 修改供应商银行信息
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) SupplierBankDTO dto) {
        supplierBankService.edit(dto);
        return success();
    }

    /**
     * 删除供应商银行信息（真删，不做数据关联校验）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        supplierBankService.remove(id);
        return success();
    }

    /**
     * 带权限分页查询供应商银行信息（查询条件customerName-客户名称;supplierName-供应商名称;name-银行名称)
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<SupplierBankVO>> page(SupplierBankQTO qto) {
        PageInfo<SupplierBankVO> page = supplierBankService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

}
