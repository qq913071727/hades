package com.tsfyun.scm.controller.materiel;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.materiel.NameElementsDTO;
import com.tsfyun.scm.dto.materiel.NameElementsQTO;
import com.tsfyun.scm.service.materiel.INameElementsService;
import com.tsfyun.scm.vo.materiel.NameElementsPlusVO;
import com.tsfyun.scm.vo.materiel.NameElementsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 品名要素 前端控制器
 * </p>
 *
 * @since 2020-03-26
 */
@RestController
@RequestMapping("/nameElements")
public class NameElementsController extends BaseController {

    @Autowired
    private INameElementsService nameElementsService;

    /**
     * 新增
     * @param dto
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) NameElementsDTO dto) {
        nameElementsService.add(dto);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) NameElementsDTO dto) {
        nameElementsService.edit(dto);
        return success();
    }


    /**
     * 详情信息(含申报要素信息）
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<NameElementsPlusVO> detail(@RequestParam(value = "id")String id) {
        return success(nameElementsService.detail(id,true));
    }

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<NameElementsVO>> list(NameElementsQTO qto) {
        PageInfo<NameElementsVO> page = nameElementsService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 批量删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") List<String> ids){
        nameElementsService.deleteBatch(ids);
        return success();
    }

    /**
     * 启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")String id,@RequestParam("disabled")Boolean disabled){
        nameElementsService.updateDisabled(id,disabled);
        return success();
    }

}

