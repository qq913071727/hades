package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

@Data
public class SupplierBankVO implements Serializable {

    private Long id;

    /**
     * 供应商
     */
    private Long supplierId;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 银行名称
     */
    private String name;

    /**=
     * 银行代码
     */
    private String code;

    /**
     * 银行账号
     */
    private String account;

    /**=
     * 币制
     */
    private String currencyName;

    /**=
     * 币制
     */
    private String currencyId;

    /**
     * SWIFT CODE
     */
    private String swiftCode;

    /**
     * 银行地址
     */
    private String address;

    /**
     * 禁用标示
     */
    private Boolean disabled;

    /**
     * 对应的客户
     */
    private String customerName;

    /**
     * 默认
     */
    private Boolean isDefault;


}
