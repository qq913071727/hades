package com.tsfyun.scm.service.order;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.excel.OrderMemberExcel;
import com.tsfyun.scm.vo.order.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 进口订单 服务类
 * </p>
 *

 * @since 2020-04-08
 */
public interface IImpOrderService extends IService<ImpOrder> {
    /**
     * 客户端新增订单
     * @param orderDto
     * @return
     */
    Long clienAdd(ImpOrderPlusDTO orderDto);

    /**
     * 客户端修改订单
     * @param orderDto
     */
    void clienEdit(ImpOrderPlusDTO orderDto);
    /**
     * 新增订单
     * @param orderDto
     * @return
     */
    Long add(ImpOrderPlusDTO orderDto);

    /**
     * 修改订单
     * @param orderDto
     */
    void edit(ImpOrderPlusDTO orderDto);

    /**
     * 分页查询订单列表
     * @param qto
     * @return
     */
    PageInfo<ImpOrderVO> list(ImpOrderQTO qto);

    /**
     * 客户端列表查询
     * @param qto
     * @return
     */
    PageInfo<ClientImpOrderVO> clientList(ClientImpOrderQTO qto);

    /**
     * 订单详情包含明细
     * @param id
     * @return
     */
    ImpOrderPlusVO detailPlus(Long id);
    ImpOrderPlusVO detailPlus(Long id, String operation);
    ImpOrderDetailVO detail(Long id);
    ImpOrderDetailVO detail(Long id,String operation);

    /**=
     * 订单审价改变状态
     * @param dto
     */
    void orderPricing(TaskDTO dto);

    /**=
     * 订单审核
     * @param dto
     */
    void examine(TaskDTO dto);

    /**=
     * 确认进口
     * @param dto
     */
    void confirmImp(TaskDTO dto);

    /**
     * 确认进口后退回验货
     * @param id
     * @param info
     */
    void backInspection(Long id,String info);

    /**=
     * 订单报关完成（报关单触发）
     * @param orderId
     */
    void declarationCompleted(Long orderId);

    /**
     * 根据订单号模糊搜索并带出客户名称
     * @param orderNo
     * @return
     */
    List<ImpOrderSimpleVO> selectByOrderNo(String orderNo);

    /**=
     * 根据订单号查询订单
     * @param docNo
     * @return
     */
    ImpOrder findByDocNo(String docNo);

    /**=
     * 分摊净重
     * @param id
     * @param netWeight
     */
    void shareNetWeight(Long id, BigDecimal netWeight);

    /**=
     * 分摊毛重
     * @param id
     * @param grossWeight
     */
    void shareGrossWeight(Long id, BigDecimal grossWeight);

    /**=
     * 批量填写入库单号
     * @param id
     * @param receivingNo
     */
    void batchReceivingNo(Long id,String receivingNo);

    /**=
     * 明细绑定入库单号
     * @param dto
     */
    void saveBindMember(BindMemberDTO dto);

    ImpOrderMemberVO obtainSplitMember(Long id);

    void saveSplitMember(OrderInspectionMemberDTO dto);

    /**=
     * 保存验货明细修改
     * @param dto
     */
    void saveInspectionMemberEdit(OrderInspectionMemberDTO dto);

    /**
     * 辅助验货
     * @param file
     * @param orderId
     */
    InspectionPlusVO auxiliaryInspection(MultipartFile file,Long orderId);

    /**=
     * 确认验货完成
     * @param id
     */
    void confirmInspection(Long id);

    /**
     * 根据订单初始化拆单
     * @param orderId
     * @param quantity
     * @return
     */
    List<SplitOrderMemberVO> initSelfHelpSplit(Long orderId, Integer quantity);

    /**
     * 保存拆单
     * @param dto
     */
    void saveSelfHelpSplit(SelfHelpSplitDTO dto);

    /**=
     * 解除订单绑定的入库单
     * @param impOrder
     */
    void relieveReceivingNoteBind(ImpOrder impOrder);

    /**=
     * 订单完成风控审核
     * @param orderId
     * @param isPass
     * @param memo
     */
    void completeRisk(Long orderId,Boolean isPass,String memo);

    /**
     * 订单更改跨境运输单号
     * @param orderId
     * @param transportNo
     */
    int updateTransportNo(Long orderId,String transportNo);

    /**
     * 获取跨境运输单可以绑定的订单数据
     * @param isCharterCar
     * @return
     */
    List<BindOrderVO> waybillCanBindOrderList(Boolean isCharterCar);

    /**
     * 获取跨境运输单已经绑定的订单数据
     * @param transportNo
     * @return
     */
    List<BindOrderVO> waybillBindedOrderList(String transportNo);

    /**
     * 根据跨境运输单号获取绑单的订单数
     * @param transportNo
     * @return
     */
    int getWaybillBindOrderNumber(String transportNo);

    /**
     * 重置订单上的运输单号
     * @param id
     */
    void resetTransportNo(Long id);

    /**
     * 设置订单申报信息
     * @param dto
     */
    void setupDeclaration(SetupDeclarationDTO dto);


    /**
     * 设置订单上的发票号 （订单id和订单编号二传一）
     * @param id
     * @param orderNo
     * @param invoiceNo
     */
    void setInvoiceNo(Long id,String orderNo,String invoiceNo);

    /**
     * 订单明细导入解析
     * @param file
     * @return
     */
     List<ImportOrderMemberVO> readDataFromMemberImport(MultipartFile file);

    /**=
     * 更新订单的付款单号
     * @param id
     * @param paymentNo
     */
     void updateOrderPaymentNo(Long id,String paymentNo);

    /**
     * 更新订单上的销售合同号
     * @param impOrderNo
     * @param salesContractNo
     */
     void updateSalesContractNo(String impOrderNo,String salesContractNo);

    /**=
     * 跨境运单签收同步订单 已通关
     * @param bindedOrders
     */
     void impOrderClearance(List<BindOrderVO> bindedOrders);

    /**
     * 获取可以调整费用的订单下拉
     * @param orderNo
     * @return
     */
     List<ImpOrderAdjustCostVO> selectAdjustOrder(String orderNo);

    /**
     * 删除订单
     * @param id
     */
     void clientRemove(Long id);
     void remove(Long id);

    /**
     * 获取需要修改的订单数
     * @return
     */
    Integer editOrderSize();

    /**
     * 单量统计
     * @param startDate
     * @param endDate
     * @return
     */
    Integer totalSingleQuantity(Date startDate, Date endDate);

    /**
     * 金额统计(USD)
     * @param startDate
     * @param endDate
     * @return
     */
    BigDecimal totalAmountMoney(Date startDate, Date endDate);

    /**
     * 获取打印委托书数据
     * @param id
     * @return
     */
    ImpOrderEntrustVO getPrintEntrustData(Long id);

    /**
     * 导出进口委托书Excel
     * @param id
     * @return
     */
    Long exportEntrustExcel(Long id);

    /**
     * 订单金额统计
     * @param qto
     * @return
     */
    BigDecimal summaryOrderAmount(ImpOrderQTO qto);
}
