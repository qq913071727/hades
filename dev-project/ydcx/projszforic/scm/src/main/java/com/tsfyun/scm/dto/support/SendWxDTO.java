package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.dto.WxMiniProgram;
import com.tsfyun.common.base.dto.WxNoticeAttr;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Description: 发送微信通知
 * @CreateDate: Created in 2020/12/23 09:52
 */
@Data
public class SendWxDTO implements Serializable {

    /**
     * 接收人（必填）
     */
    private String touser;

    /**
     * 微信通知模板id（必填）
     */
    private String template_id;

    /**
     * 模板跳转链接，跳转链接和微信小程序都不传，则不会跳转；如果都传则优先跳转小程序，若客户端不支持跳转小程序则跳转到url
     */
    private String url;

    /**
     * 跳转至微信小程序参数
     */
    private WxMiniProgram miniprogram;

    /**
     * 模板数据（必填）
     */
    private LinkedHashMap<String, WxNoticeAttr> data;


}
