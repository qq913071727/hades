package com.tsfyun.scm.dto.base;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-18
 */
@Data
@ApiModel(value="Currency查询请求对象", description="请求实体")
public class CurrencyQTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private Boolean disabled;

   private String keyword;


}
