package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/8 14:10
 */
@Data
public class ExpressStandardQTO extends PaginationDto {

    private String quoteType;

    private Boolean disabled;

}
