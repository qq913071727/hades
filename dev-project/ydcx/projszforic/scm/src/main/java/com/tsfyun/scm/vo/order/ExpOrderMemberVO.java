package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;

import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 出口订单明细响应实体
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Data
@ApiModel(value="ExpOrderMember响应对象", description="出口订单明细响应实体")
public class ExpOrderMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private Integer rowNo;

    @ApiModelProperty(value = "订单主单")
    private Long expOrderId;

    @ApiModelProperty(value = "物料ID")
    private String materialId;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;
    @ApiModelProperty(value = "物料报关品牌")
    private String decBrand;

    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "物料报关名称")
    private String decName;

    @ApiModelProperty(value = "规格参数")
    private String spec;
    private String decSpec;

    @ApiModelProperty(value = "申报要素")
    private String declareSpec;

    @ApiModelProperty(value = "客户物料号")
    private String goodsCode;

    @ApiModelProperty(value = "产地")
    private String country;

    @ApiModelProperty(value = "产地")
    private String countryName;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价(委托)")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "总价(委托)")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "报关单价")
    private BigDecimal decUnitPrice;

    @ApiModelProperty(value = "报关总价")
    private BigDecimal decTotalPrice;

    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;

    @ApiModelProperty(value = "箱号")
    private String cartonNo;

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    @ApiModelProperty(value = "物料状态")
    private String materielStatusId;
    private String materielStatusName;
    public String getMaterielStatusName(){
        MaterielStatusEnum materielStatusEnum = MaterielStatusEnum.of(getMaterielStatusId());
        return Objects.nonNull(materielStatusEnum)?materielStatusEnum.getName():"";
    }

}
