package com.tsfyun.scm.dto.materiel;

import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2021-03-08
 */
@Data
@ApiModel(value="MaterielExp请求对象", description="请求实体")
public class MaterielExpDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @LengthTrim(max = 10,message = "海关编码最大长度不能超过10位")
   @ApiModelProperty(value = "海关编码")
   private String hsCode;

   @LengthTrim(max = 3,message = "CIQ编码最大长度不能超过3位")
   @ApiModelProperty(value = "CIQ编码")
   private String ciqNo;

   @LengthTrim(max = 100,message = "物料编号最大长度不能超过100位")
   @ApiModelProperty(value = "物料编号")
   private String code;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过80位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过80位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 100,message = "名称最大长度不能超过80位")
   @ApiModelProperty(value = "名称")
   private String name;

   @LengthTrim(max = 255,message = "申报要素最大长度不能超过255位")
   @ApiModelProperty(value = "申报要素")
   private String elements;

   @LengthTrim(max = 500,message = "规格参数最大长度不能超过500位")
   @ApiModelProperty(value = "规格参数")
   private String spec;

   @LengthTrim(max = 255,message = "归类备注最大长度不能超过255位")
   @ApiModelProperty(value = "归类备注")
   private String memo;

   @LengthTrim(max = 50,message = "来源最大长度不能超过50位")
   @ApiModelProperty(value = "来源")
   private String source;

   @LengthTrim(max = 32,message = "来源标识最大长度不能超过32位")
   @ApiModelProperty(value = "来源标识")
   private String sourceMark;

   @NotEmptyTrim(message = "状态编码不能为空")
   @LengthTrim(max = 20,message = "状态编码最大长度不能超过20位")
   @ApiModelProperty(value = "状态编码")
   private String statusId;

   @LengthTrim(max = 20,message = "归类人员最大长度不能超过20位")
   @ApiModelProperty(value = "归类人员")
   private String classifyPerson;

   @ApiModelProperty(value = "最后归类时间")
   private Date classifyTime;


}
