package com.tsfyun.scm.vo.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImpExpAgencyFeeVO implements Serializable {

    private String month;
    private BigDecimal impCost;
    private BigDecimal expCost;
}
