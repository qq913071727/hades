package com.tsfyun.scm.controller.logistics;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.logistics.ConveyanceQTO;
import com.tsfyun.scm.service.logistics.IConveyanceService;
import com.tsfyun.scm.vo.logistics.SimpleConveyanceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-03-20
 */
@RestController
@RequestMapping("/conveyance")
public class ConveyanceController extends BaseController {

    @Autowired
    private IConveyanceService conveyanceService;

    /**
     * 获取运输工具下拉（过滤掉禁用的）
     * @return
     */
    @GetMapping("select")
    public Result<List<SimpleConveyanceVO>> select(ConveyanceQTO qto){
        return success(conveyanceService.select(qto));
    }


}

