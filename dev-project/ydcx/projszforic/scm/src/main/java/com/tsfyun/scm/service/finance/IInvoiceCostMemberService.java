package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.scm.entity.finance.Invoice;
import com.tsfyun.scm.entity.finance.InvoiceCostMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderCost;
import com.tsfyun.scm.vo.finance.InvoiceCostMemberVO;

import java.util.List;

/**
 * <p>
 * 发票或内贸合同费用明细 服务类
 * </p>
 *

 * @since 2020-05-18
 */
public interface IInvoiceCostMemberService extends IService<InvoiceCostMember> {
    /**
     * 代理类发票明细信息保存（根据订单费用信息）
     */
    void saveAgentMember(Invoice invoice, ImpOrder impOrder, List<ImpOrderCost> orderCosts);

    /**
     * 根据订单号获取费用明细信息
     * @param orderNo
     * @return
     */
    List<InvoiceCostMemberVO> getMembersByOrderNo(String orderNo);

    /**
     * 根据单据id删除费用明细信息
     * @param docId
     */
    void deleteByDocId(String docId);

    /**
     * 保存明细
     * @param members
     * @param impSalesContract
     */
    void saveMembers(List<InvoiceCostMember> members, ImpSalesContract impSalesContract);

}
