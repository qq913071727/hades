package com.tsfyun.scm.controller.customs;


import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.customs.SaveDeclarationMemberElementsDTO;
import com.tsfyun.scm.service.customs.IDeclarationMemberService;
import com.tsfyun.scm.vo.customs.DeclarationMemberCustomsElementsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2020-04-24
 */
@RestController
@RequestMapping("/declarationMember")
public class DeclarationMemberController extends BaseController {

    @Autowired
    private IDeclarationMemberService declarationMemberService;


    /**
     * 报关单明细申报要素
     * @param id
     * @return
     */
    @PostMapping(value = "obtainDeclarationElements")
    public Result<DeclarationMemberCustomsElementsVO> obtainDeclarationElements(@RequestParam("id")Long id) {
        return success(declarationMemberService.getMemberElements(id));
    }

    /**
     * 报关单明细保存申报要素
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "saveMemberElements")
    public Result<Void> saveMemberElements(@ModelAttribute @Validated() SaveDeclarationMemberElementsDTO dto) {
        declarationMemberService.saveMemberElements(dto);
        return success();
    }

}

