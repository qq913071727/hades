package com.tsfyun.scm.vo.customs.singlewindow;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 舱单主单
 * @CreateDate: Created in 2020/12/25 17:41
 */
@Data
public class ManifestVO implements Serializable {

    private Long id;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 仓单号
     */

    private String mtfNo;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 客户ID
     */

    private Long customerId;

    /**
     * 单证类型
     */

    private String typeId;

    /**
     * 单证类型
     */

    private String typeName;

    /**
     * 报关单ID
     */

    private Long decId;

    /**
     * 报关单系统单号
     */

    private String decDocNo;

    /**
     * 货物运输批次号
     */

    private String voyageNo;

    /**
     * 运输方式代码
     */

    private String trafModeCode;

    /**
     * 运输方式代码
     */

    private String trafMode;

    /**
     * 进出境口岸海关代码
     */

    private String customsCode;

    /**
     * 进出境口岸海关代码
     */

    private String customsCodeName;

    /**
     * 承运人代码
     */

    private String carrierCode;

    /**
     * 运输工具代理企业代码
     */

    private String transAgentCode;


    private String loadingLocationCode;

    /**
     * 货物装载时间
     */

    private LocalDateTime loadingDate;

    /**
     * 传输企业备案关区
     */

    private String customMaster;

    /**
     * 传输企业备案关区
     */

    private String customMasterName;

    /**
     * 企业代码
     */

    private String unitCode;

    /**
     * 舱单传输人名称
     */

    private String msgRepName;

    /**
     * 备注
     */

    private String memo;

    //进口原始舱单（到达卸货地日期）
    private LocalDateTime arrivalDate;

    /**
     * 舱单来源
     */
    private String source;

}
