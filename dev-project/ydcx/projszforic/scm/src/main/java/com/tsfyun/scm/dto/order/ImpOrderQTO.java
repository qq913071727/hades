package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 订单查询请求实体
 */
@Data
public class ImpOrderQTO extends PaginationDto implements Serializable {

    /**
     * 销售人员
     */
    private Long salePersonId;

    /**
     * 商务人员
     */
    private Long businessPersonId;

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 客户单号
     */
    private String clientNo;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 状态
     */
    private String statusId;

    /**=
     * 业务类型
     */
    private String businessType;

    /**=
     * 报价类型
     */
    private String quoteType;

    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**
     * 下单开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 下单结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
