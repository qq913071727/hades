package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/25 09:41
 */
@Data
public class CustomerExpressQuoteVO implements Serializable {

    /**
     * 报价类型
     */

    private String quoteType;


    /**
     * 报价开始时间
     */

    private LocalDateTime quoteStartTime;

    /**
     * 报价结束时间
     */

    private LocalDateTime quoteEndTime;

    /**
     * 封顶标识
     */

    private Boolean capped;

    /**
     * 封顶费用
     */

    private BigDecimal cappedFee;

    /**
     * 禁用标识
     */

    private Boolean disabled;

}
