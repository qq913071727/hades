package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.finance.CounterFeeTypeEnum;
import com.tsfyun.common.base.enums.finance.PaymentTermEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 付款单请求实体
 * </p>
 *
 * @since 2020-04-23
 */
@Data
@ApiModel(value="PaymentAccount请求对象", description="付款单请求实体")
public class PaymentAccountDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   @NotEmptyTrim(message = "收款人不能为空")
   @ApiModelProperty(value = "收款人不能为空")
   private String payeeName;

   @NotEmptyTrim(message = "收款银行不能为空")
   @ApiModelProperty(value = "收款银行不能为空")
   private String payeeBankName;

   @ApiModelProperty(value = "付款方式")
   @NotEmptyTrim(message = "付款方式不能为空")
   @EnumCheck(clazz = PaymentTermEnum.class,message = "付款方式错误")
   private String paymentTerm;

   @ApiModelProperty(value = "手续费承担方式")
   @NotEmptyTrim(message = "手续费承担方式不能为空")
   @EnumCheck(clazz = CounterFeeTypeEnum.class,message = "手续费承担方式错误")
   private String counterFeeType;

   @ApiModelProperty(value = "付款要求")
   @LengthTrim(max = 500,message = "付款要求最大长度不能超过500位")
   private String memo;
}
