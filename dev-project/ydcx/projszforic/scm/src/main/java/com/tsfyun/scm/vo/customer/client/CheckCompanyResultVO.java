package com.tsfyun.scm.vo.customer.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckCompanyResultVO implements Serializable {

    private Integer state;// 0：未占用 1：已占用可绑定 2：已占用不可绑定
    private Long ncustomerId;// 可绑定的客户ID
}
