package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 装货地数据
 * @since Created in 2020/4/22 11:13
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadingLocation", propOrder = {
        "id",
        "loadingDateTime"
})
@Data
public class LoadingLocation {

    //装货地代码
    @XmlElement(name = "ID")
    protected String id;

    //货物装载运输工具时间，格式如20190212123000
    @XmlElement(name = "LoadingDateTime")
    protected String loadingDateTime;

    public LoadingLocation() {

    }

    public LoadingLocation (String id) {
        this.id = id;
    }


    public LoadingLocation(String id, String loadingDateTime) {
        this.id = id;
        this.loadingDateTime = loadingDateTime;
    }
}
