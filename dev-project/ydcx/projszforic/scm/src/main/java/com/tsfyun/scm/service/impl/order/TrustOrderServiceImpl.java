package com.tsfyun.scm.service.impl.order;

import com.tsfyun.common.base.enums.SerialNumberTypeEnum;
import com.tsfyun.common.base.enums.domain.CustomerStatusEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.service.customer.ICustomerService;
import com.tsfyun.scm.service.order.ITrustOrderService;
import com.tsfyun.scm.service.system.ISerialNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class TrustOrderServiceImpl implements ITrustOrderService {

    @Autowired
    private ISerialNumberService serialNumberService;
    @Autowired
    private ICustomerService customerService;

    @Override
    public String generateDocNo() {
        //验证客户状态
        LoginVO loginVO = SecurityUtil.getCurrent();
        Customer customer = customerService.getById(loginVO.getCustomerId());
        if(!Objects.equals(CustomerStatusEnum.TYPE_2,CustomerStatusEnum.of(customer.getStatusId()))){
            throw new ServiceException("对不起您的公司资料还未审核通过");
        }
        return serialNumberService.generateDocNo(SerialNumberTypeEnum.TRUST_ORDER);
    }
}
