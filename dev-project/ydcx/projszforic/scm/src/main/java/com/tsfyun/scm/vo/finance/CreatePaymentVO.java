package com.tsfyun.scm.vo.finance;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CreatePaymentVO implements Serializable {
    @ApiModelProperty(value = "收款单ID")
    private Long overseasAccountId;
    @ApiModelProperty(value = "客户")
    private Long customerId;
    @ApiModelProperty(value = "客户")
    private String customerName;
    @ApiModelProperty(value = "收款方")
    private String payeeName;
    @ApiModelProperty(value = "收款银行")
    private String payeeBank;
    @ApiModelProperty(value = "收款银行账号")
    private String payeeBankName;
    @ApiModelProperty(value = "原币金额")
    private BigDecimal originalAccountValue;
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;
    @ApiModelProperty(value = "订单号")
    private String orderNo;
    @ApiModelProperty(value = "客户单号")
    private String clientNo;
    @ApiModelProperty(value = "收款单号")
    private String accountNo;
}
