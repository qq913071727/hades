package com.tsfyun.scm.dto.logistics;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 送货单请求实体
 * </p>
 *

 * @since 2020-05-25
 */
@Data
@ApiModel(value="DeliveryWaybill请求对象", description="送货单请求实体")
public class DeliveryWaybillDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "送货单号不能为空")
   @LengthTrim(max = 20,message = "送货单号最大长度不能超过20位")
   @ApiModelProperty(value = "送货单号")
   private String docNo;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;

   private Long customerId;

   @NotNull(message = "运单日期不能为空")
   @ApiModelProperty(value = "运单日期")
   private LocalDateTime transDate;

   @ApiModelProperty(value = "发车时间")
   private LocalDateTime departureDate;

   @ApiModelProperty(value = "签收时间")
   private LocalDateTime reachDate;

   @NotEmptyTrim(message = "状态编码不能为空")
   @LengthTrim(max = 20,message = "状态编码最大长度不能超过20位")
   @ApiModelProperty(value = "状态编码")
   private String statusId;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 10,message = "单据类型最大长度不能超过10位")
   @ApiModelProperty(value = "单据类型-枚举")
   private String billType;

   @NotNull(message = "仓库不能为空")
   @ApiModelProperty(value = "仓库")
   private Long warehouseId;

   @LengthTrim(max = 255,message = "收货公司最大长度不能超过255位")
   @ApiModelProperty(value = "收货公司")
   private String shippingCompany;

   @LengthTrim(max = 300,message = "收货地址最大长度不能超过300位")
   @ApiModelProperty(value = "收货地址")
   private String shippingAddress;

   @LengthTrim(max = 50,message = "收货联系人最大长度不能超过50位")
   @ApiModelProperty(value = "收货联系人")
   private String shippingLinkMan;

   @LengthTrim(max = 50,message = "收货联系人电话最大长度不能超过50位")
   @ApiModelProperty(value = "收货联系人电话")
   private String shippingTel;

   @LengthTrim(max = 50,message = "快递单号最大长度不能超过50位")
   @ApiModelProperty(value = "快递单号")
   private String experssNo;

   @LengthTrim(max = 50,message = "封条号最大长度不能超过50位")
   @ApiModelProperty(value = "封条号")
   private String sealNo;


}
