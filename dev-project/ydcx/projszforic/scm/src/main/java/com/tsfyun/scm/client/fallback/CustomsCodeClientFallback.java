package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.client.CustomsCodeClient;
import com.tsfyun.scm.system.vo.CustomsCodeVO;
import com.tsfyun.scm.system.vo.CustomsElementsVO;
import com.tsfyun.scm.system.vo.TariffCustomsCodeVO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@Slf4j
public class CustomsCodeClientFallback implements FallbackFactory<CustomsCodeClient> {


    @Override
    public CustomsCodeClient create(Throwable throwable) {
        return new CustomsCodeClient(){

            @Override
            public Result<CustomsCodeVO> detail(@RequestParam(value = "id")String id){
                log.error("调用海关编码异常",throwable);
                return Result.error("调用海关编码异常,请稍后重试");
            }

            @Override
            public Result<List<CustomsElementsVO>> elementDetail(@RequestParam(value = "hsCode")String hsCode){
                log.error("根据海关编码获取申报要素失败",throwable);
                return Result.error("调用海关编码申报要素异常,请稍后重试");
            }

            @Override
            public Result<List<TariffCustomsCodeVO>> tariffCustoms() {
                log.error("根据最惠国关税率大于0的海关编码",throwable);
                return null;
            }
        };
    }
}
