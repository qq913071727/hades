package com.tsfyun.scm.vo.materiel.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.scm.system.vo.CustomsCodeVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description:
 * @CreateDate: Created in 2020/10/12 11:31
 */
@Data
public class ClientMaterielVO implements Serializable {

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    @ApiModelProperty(value = "CIQ编码")
    private String ciqNo;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "申报要素")
    private String elements;

    @ApiModelProperty(value = "规格参数")
    private String spec;

    @ApiModelProperty(value = "是否涉证")
    private Boolean isSmp;

    @ApiModelProperty(value = "需要3C证书")
    private Boolean isCapp;

    @ApiModelProperty(value = "需要3C目录鉴定")
    private Boolean isCappNo;

    @ApiModelProperty(value = "3C证书编号")
    private String cappNo;

    @ApiModelProperty(value = "归类备注")
    private String memo;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "状态名称")
    private String statusName;

    @ApiModelProperty(value = "提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "最后归类时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime classifyTime;

    private CustomsCodeVO customsCode;

    public String getStatusName() {
        MaterielStatusEnum materielStatusEnum = MaterielStatusEnum.of(statusId);
        return Optional.ofNullable(materielStatusEnum).map(MaterielStatusEnum::getName).orElse("");
    }

}
