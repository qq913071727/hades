package com.tsfyun.scm.controller.customer;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.CustomerBankDTO;
import com.tsfyun.scm.service.customer.ICustomerBankService;
import com.tsfyun.scm.vo.customer.CustomerBankVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  客户银行前端控制器
 * </p>
 *
 * @since 2020-03-10
 */
@RestController
@RequestMapping("/customerBank")
public class CustomerBankController extends BaseController {

    @Autowired
    private ICustomerBankService customerBankService;

    /**
     * 新增
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) CustomerBankDTO dto) {
        customerBankService.add(dto);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) CustomerBankDTO dto) {
        customerBankService.edit(dto);
        return success();
    }

    /**
     * 激活禁用
     * @param id
     * @return
     */
    @PostMapping(value = "activeDisable")
    Result<Void> activeDisable(@RequestParam(value = "id")Long id) {
        customerBankService.activeDisable(id);
        return success();
    }

    /**
     * 设置默认
     * @param id
     * @return
     */
    @PostMapping(value = "setDefault")
    Result<Void> setDefault(@RequestParam(value = "id")Long id) {
        customerBankService.setDefault(id);
        return success();
    }

    /**
     * 获取客户银行（所有的客户银行，启用的，不分页）
     * @param customerId
     * @return
     */
    @GetMapping(value = "select")
    Result<List<CustomerBankVO>> select(@RequestParam(value = "customerId")Long customerId) {
        return success(customerBankService.select(customerId));
    }

    /**
     * 分页查询客户银行信息（查询条件customerName-客户名称;name-银行名称;)
     * @param dto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<CustomerBankVO>> page(@ModelAttribute CustomerBankDTO dto) {
        PageInfo<CustomerBankVO> page = customerBankService.pageList(dto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 获取客户银行详细信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<CustomerBankVO> detail(@RequestParam(value = "id")Long id) {
        return success(customerBankService.detail(id));
    }


}

