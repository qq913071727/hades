package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.order.CostChangeRecord;
import com.tsfyun.scm.entity.order.CostChangeRecordExp;
import com.tsfyun.scm.vo.order.CostChangeRecordExpVO;
import com.tsfyun.scm.vo.order.CostChangeRecordVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 费用变更记录 Mapper 接口
 * </p>
 *

 * @since 2020-05-27
 */
@Repository
public interface CostChangeRecordExpMapper extends Mapper<CostChangeRecordExp> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<CostChangeRecordExpVO> list(Map<String,Object> params);

    Integer removeByOrderId(@Param(value = "orderId") Long orderId);
}
