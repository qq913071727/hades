package com.tsfyun.scm.controller.support;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.support.FeedbackDTO;
import com.tsfyun.scm.dto.support.FeedbackQTO;
import com.tsfyun.scm.entity.support.Feedback;
import com.tsfyun.scm.service.support.IFeedbackService;
import com.tsfyun.scm.vo.support.FeedbackVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 意见反馈 前端控制器
 * </p>
 *
 *
 * @since 2020-09-23
 */
@RestController
@RequestMapping("/feedback")
@Api(tags = "意见反馈")
public class FeedbackController extends BaseController {

    @Autowired
    private IFeedbackService feedbackService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 新增
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    @ApiOperation("提交反馈")
    Result<Void> addOrder(@ModelAttribute @Validated FeedbackDTO dto) {
        feedbackService.add(dto);
        return success( );
    }

    @ApiOperation("管理端获取意见反馈列表")
    @PostMapping(value = "admin/list")
    @PreAuthorize("hasRole('manager')")
    public Result<List<FeedbackVO>> adminList(@ModelAttribute FeedbackQTO qto){
        PageInfo<Feedback> page = feedbackService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),FeedbackVO.class));
    }

}

