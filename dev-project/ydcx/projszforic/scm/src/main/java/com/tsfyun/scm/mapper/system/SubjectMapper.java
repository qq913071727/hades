package com.tsfyun.scm.mapper.system;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.system.Subject;

public interface SubjectMapper extends Mapper<Subject> {

}
