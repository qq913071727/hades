package com.tsfyun.scm.service.report;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ExpOrderReceiveQTO;
import com.tsfyun.scm.dto.report.ExpOneVoteTheEndQTO;
import com.tsfyun.scm.dto.view.ViewExpDifferenceMemberQTO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import com.tsfyun.scm.vo.view.ViewExpDifferenceMemberVO;
import com.tsfyun.scm.vo.view.ViewExpDifferenceSummaryVO;
import com.tsfyun.scm.vo.view.ViewExpOrderCostMemberTotalVO;
import com.tsfyun.scm.vo.view.ViewExpOrderCostMemberVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/18 12:33
 */
public interface IExpReportService {

    /**
     * 客户票差明细列表
     * @param qto
     * @return
     */
    PageInfo<ViewExpDifferenceMemberVO> expDifferenceMemberPage(ViewExpDifferenceMemberQTO qto);

    /**
     * 获取客户出口票差总额
     * @param customerId
     * @return
     */
    BigDecimal obtainCustomerExpDifferenceVal(Long customerId);


    /**
     * 导出客户票差明细报表
     * @param qto
     * @return
     */
    Long exportExpDifferenceMember(ViewExpDifferenceMemberQTO qto) throws Exception;


    /**
     * 客户票差汇总列表
     * @param qto
     * @return
     */
    PageInfo<ViewExpDifferenceSummaryVO> expDifferenceSummaryPage(ViewExpDifferenceMemberQTO qto);

    /**
     * 导出客户票差汇总报表
     * @param qto
     * @return
     */
    Long exportExpDifferenceSummary(ViewExpDifferenceMemberQTO qto) throws Exception;

    /**
     * 出口应收
     * @param qto
     * @return
     */
    PageInfo<ViewExpOrderCostMemberVO> orderReceivableList(ExpOrderReceiveQTO qto);

    /**
     * 出口应收统计
     * @param qto
     * @return
     */
    ViewExpOrderCostMemberTotalVO orderReceivableTotal(ExpOrderReceiveQTO qto);

    /**
     * 出口应收导出
      * @param qto
     * @return
     */
    Long orderReceivableExport(ExpOrderReceiveQTO qto) throws Exception;

    /**
     * 月度代理费
     * @param year
     * @return
     */
    Map<String,BigDecimal> monthAgencyFee(String year);

    /**
     * 出口一票到底台账
     * @param qto
     * @return
     */
    Result<List<ExpOneVoteTheEndVO>> oneVoteTheEnd(ExpOneVoteTheEndQTO qto);

    /**
     * 导出出口一票到底台账
     * @param qto
     * @return
     */
    Long exportOneVoteTheEnd(ExpOneVoteTheEndQTO qto) throws Exception;

}
