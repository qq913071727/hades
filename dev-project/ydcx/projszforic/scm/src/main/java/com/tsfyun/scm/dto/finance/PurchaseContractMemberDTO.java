package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 采购合同明细请求实体
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Data
@ApiModel(value="PurchaseContractMember请求对象", description="采购合同明细请求实体")
public class PurchaseContractMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Integer rowNo;

   @NotNull(message = "采购合同主单不能为空")
   @ApiModelProperty(value = "采购合同主单")
   private Long purchaseContractId;

   @NotNull(message = "订单明细不能为空")
   @ApiModelProperty(value = "订单明细")
   private Long orderMemberId;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @NotEmptyTrim(message = "名称不能为空")
   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitCode;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "数量整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "数量")
   private BigDecimal quantity;

   @NotNull(message = "单价(委托)不能为空")
   @Digits(integer = 8, fraction = 8, message = "单价(委托)整数位不能超过8位，小数位不能超过8")
   @ApiModelProperty(value = "单价(委托)")
   private BigDecimal unitPrice;

   @NotNull(message = "总价(委托)不能为空")
   @Digits(integer = 8, fraction = 2, message = "总价(委托)整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "总价(委托)")
   private BigDecimal totalPrice;

   @NotNull(message = "代理费不能为空")
   @Digits(integer = 8, fraction = 2, message = "代理费整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "代理费")
   private BigDecimal agentFee;

   @NotNull(message = "代垫费不能为空")
   @Digits(integer = 8, fraction = 2, message = "代垫费整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "代垫费")
   private BigDecimal matFee;

   @NotNull(message = "票差调整金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "票差调整金额整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "票差调整金额")
   private BigDecimal differenceVal;


}
