package com.tsfyun.scm.vo.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 意见反馈响应实体
 * </p>
 *
 *
 * @since 2020-09-23
 */
@Data
@ApiModel(value="Feedback响应对象", description="意见反馈响应实体")
public class FeedbackVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "问题或建议类型")
    private String questionType;

    @ApiModelProperty(value = "问题或建议")
    private String memo;

    @ApiModelProperty(value = "图片")
    private String pics;

    @ApiModelProperty(value = "联系方式")
    private String contact;


}
