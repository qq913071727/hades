package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.ExpOrderPriceMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ExpOrderPriceMemberVO;
import com.tsfyun.scm.vo.order.ImpOrderPriceMemberVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2021-09-14
 */
public interface IExpOrderPriceMemberService extends IService<ExpOrderPriceMember> {
    //根据订单ID查询历史审价记录
    Map<String,Object> historicalRecord(Long orderMemberId);

    List<ExpOrderPriceMemberVO> findByOrderPriceId(Long orderPriceId);

    void removeByOrderId(Long orderId);
}
