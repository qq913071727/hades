package com.tsfyun.scm.entity.materiel;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 归类变更日志
 * </p>
 *
 *
 * @since 2020-03-26
 */
@Data
public class MaterielLog extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 变更内容
     */

    private String changeContent;

    /**
     * 物料ID
     */

    private String materielId;


}
