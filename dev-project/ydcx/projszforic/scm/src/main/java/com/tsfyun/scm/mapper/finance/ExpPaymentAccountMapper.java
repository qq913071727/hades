package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.finance.ExpPaymentAccountQTO;
import com.tsfyun.scm.entity.finance.ExpPaymentAccount;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.ExpPaymentAccountVO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出口付款单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-18
 */
@Repository
public interface ExpPaymentAccountMapper extends Mapper<ExpPaymentAccount> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ExpPaymentAccountVO> list(Map<String,Object> params);

    List<ExpOneVoteTheEndVO> oneVoteTheEndByOrderId(@Param(value = "orderId") Long orderId);

}
