package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class ImpOrderInvoiceVO implements Serializable {
    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "订单编号")
    private String docNo;
    @ApiModelProperty(value = "客户")
    private Long customerId;
    @ApiModelProperty(value = "名称")
    private String customerName;
    @ApiModelProperty(value = "状态")
    private String statusId;
    @ApiModelProperty(value = "订单日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;
    @ApiModelProperty(value = "业务类型-枚举")
    private String businessType;
    @ApiModelProperty(value = "报关类型-枚举")
    private String declareType;
    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "销售合同Id")
    private Long scId;
    @ApiModelProperty(value = "销售合同")
    private String scDocNo;
    @ApiModelProperty(value = "销售合同金额")
    private BigDecimal scTotalPrice;
    @ApiModelProperty(value = "发票申请ID")
    private Long invId;
    @ApiModelProperty(value = "发票申请")
    private String invDocNo;
    @ApiModelProperty(value = "发票金额")
    private BigDecimal invoiceVal;
    @ApiModelProperty(value = "是否确认进口")
    private Boolean isImp;
    @ApiModelProperty(value = "客户总票差")
    private BigDecimal totalDifferenceVal;

    /**
     * 开户银行
     */
    private String invoiceBankName;

    /**
     * 银行帐号
     */
    private String invoiceBankAccount;

    /**
     * 纳税人识别号
     */
    private String taxpayerNo;

    /**=
     * 开票要求备注
     */
    private String invoiceMemo;

    /**
     * 开票电话
     */
    private String invoiceBankTel;

    /**
     * 开票地址
     */
    private String invoiceBankAddress;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系电话
     */
    private String linkTel;

    /**
     * 联系地址
     */
    private String linkAddress;


    /**
     * 业务类型描述
     */
    private String businessTypeDesc;
    public String getBusinessTypeDesc() {
        BusinessTypeEnum businessTypeEnum = BusinessTypeEnum.of(businessType);
        return Objects.nonNull(businessTypeEnum) ? businessTypeEnum.getName() : "";
    }
    /**=
     * 报关类型
     */
    private String declareTypeName;
    public String getDeclareTypeName(){
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(getDeclareType());
        return Objects.nonNull(declareTypeEnum)?declareTypeEnum.getName():"";
    }
    /**
     * 状态描述
     */
    private String statusDesc;
    public String getStatusDesc() {
        ImpOrderStatusEnum impOrderStatusEnum = ImpOrderStatusEnum.of(statusId);
        return Objects.nonNull(impOrderStatusEnum) ? impOrderStatusEnum.getName() : "";
    }
}
