package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.QichachaCompanyVO;
import com.tsfyun.common.base.vo.SimpleBusinessVO;
import com.tsfyun.scm.client.fallback.ToolClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/30 16:58
 */
@Component
@FeignClient(name = "scm-tool",fallbackFactory = ToolClientFallback.class)
public interface ToolClient {

    /**=
     * 根据公司名称获取公司详细信息
     * @param companyName
     * @return
     */
    @PostMapping("/qichacha/detailByCompanyName")
    Result<QichachaCompanyVO> detailByCompanyName(@RequestParam(value = "companyName")String companyName);

    /**
     * 根据公司名称模糊查询
     * @param name
     * @return
     */
    @GetMapping(value = "/simpleBusiness/vagueQuery")
    Result<List<SimpleBusinessVO>> vagueQuery(@RequestParam(value = "name",required = false)String name);

}
