package com.tsfyun.scm.vo.view;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/22 10:55
 */
@Data
public class ViewImpDifferenceSummaryVO implements Serializable {


    /**
     * 客户
     */

    private Long customerId;

    /**
     * 客户名称
     */

    private String customerName;


    /**
     * 票差总额
     */

    private BigDecimal differenceVal;

}
