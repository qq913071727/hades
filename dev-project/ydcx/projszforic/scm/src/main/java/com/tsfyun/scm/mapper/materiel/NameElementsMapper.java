package com.tsfyun.scm.mapper.materiel;

import com.tsfyun.scm.dto.materiel.NameElementsQTO;
import com.tsfyun.scm.entity.materiel.NameElements;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.materiel.NameElementsVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 品名要素 Mapper 接口
 * </p>
 *

 * @since 2020-03-26
 */
@Repository
public interface NameElementsMapper extends Mapper<NameElements> {

    List<NameElementsVO> list(NameElementsQTO qto);

}
