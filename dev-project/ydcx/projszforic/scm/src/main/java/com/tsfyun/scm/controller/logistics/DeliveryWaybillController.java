package com.tsfyun.scm.controller.logistics;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.logistics.*;
import com.tsfyun.scm.service.logistics.IDeliveryWaybillService;
import com.tsfyun.scm.vo.logistics.CrossBorderWaybillVO;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillPlusVO;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * 国内送货单
 */
@RestController
@RequestMapping("/deliveryWaybill")
public class DeliveryWaybillController extends BaseController {

    @Autowired
    private IDeliveryWaybillService deliveryWaybillService;
    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<DeliveryWaybillVO>> list(DeliveryWaybillQTO qto) {
        PageInfo<DeliveryWaybillVO> page = deliveryWaybillService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**=
     * 详情包含明细
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    Result<DeliveryWaybillPlusVO> detail(@RequestParam("id")Long id,@RequestParam(value = "operation",required = false)String operation){
        return success(deliveryWaybillService.detail(id,operation));
    }

    /**=
     * 送货单确认
     * @param dto
     * @return
     */
    @PostMapping(value = "confirm")
    @DuplicateSubmit
    Result<Void> confirm(@ModelAttribute @Valid DeliveryConfirmDTO dto){
        deliveryWaybillService.confirm(dto);
        return success();
    }

    /**=
     * 发车确认
     * @param dto
     * @return
     */
    @PostMapping(value = "confirmDeparture")
    @DuplicateSubmit
    Result<Void> confirmDeparture(@ModelAttribute @Valid DeliveryDepartureDTO dto){
        deliveryWaybillService.confirmDeparture(dto);
        return success();
    }

    /**=
     * 签收确认
     * @param dto
     * @return
     */
    @PostMapping(value = "confirmSign")
    @DuplicateSubmit
    Result<Void> confirmSign(@ModelAttribute @Valid DeliverySignDTO dto){
        deliveryWaybillService.confirmSign(dto);
        return success();
    }

    /**
     * 删除国内送货单
     * @param id
     * @return
     */
    @PostMapping(value = "remove")
    @DuplicateSubmit
    public Result<Void> remove(@RequestParam(value = "id")Long id){
        deliveryWaybillService.removeAllById(id);
        return success();
    }

}

