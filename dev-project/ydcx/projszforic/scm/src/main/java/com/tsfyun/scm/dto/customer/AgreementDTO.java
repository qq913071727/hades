package com.tsfyun.scm.dto.customer;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 协议报价单请求实体
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Data
@ApiModel(value="Agreement请求对象", description="协议报价单请求实体")
public class AgreementDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   @ApiModelProperty(value = "保存或提交审核")
   private Boolean submitAudit;

   @NotEmptyTrim(message = "协议编号不能为空")
   @LengthTrim(max = 20,message = "协议编号最大长度不能超过20位")
   @ApiModelProperty(value = "协议编号")
   private String docNo;

   @NotEmptyTrim(message = "业务类型不能为空")
   @EnumCheck(clazz = BusinessTypeEnum.class,message = "业务类型错误")
   @ApiModelProperty(value = "业务类型")
   private String businessType;

   @NotEmptyTrim(message = "报关类型不能为空")
   @EnumCheck(clazz = DeclareTypeEnum.class,message = "报关类型错误")
   @ApiModelProperty(value = "报关类型")
   private String declareType;

   @NotEmptyTrim(message = "甲方不能为空")
   @ApiModelProperty(value = "甲方")
   private String partyaName;


   @LengthTrim(max = 18,message = "甲方社会统一信用代码最大长度不能超过18位")
   @ApiModelProperty(value = "甲方社会统一信用代码")
   private String partyaSocialNo;

   @LengthTrim(max = 50,message = "甲方法人最大长度不能超过50位")
   @ApiModelProperty(value = "甲方法人")
   private String partyaLegalPerson;

   @LengthTrim(max = 50,message = "甲方电话最大长度不能超过50位")
   @ApiModelProperty(value = "甲方电话")
   private String partyaTel;

   @LengthTrim(max = 50,message = "甲方传真最大长度不能超过50位")
   @ApiModelProperty(value = "甲方传真")
   private String partyaFax;

   @LengthTrim(max = 255,message = "甲方地址最大长度不能超过255位")
   @ApiModelProperty(value = "甲方地址")
   private String partyaAddress;

   @NotNull(message = "签约日期不能为空")
   @ApiModelProperty(value = "签约日期")
   private LocalDateTime signingDate;

   @NotNull(message = "生效日期不能为空")
   @ApiModelProperty(value = "生效日期")
   private LocalDateTime effectDate;

   @NotNull(message = "失效日期不能为空")
   @ApiModelProperty(value = "失效日期")
   private LocalDateTime invalidDate;

   @NotNull(message = "自动延期/年不能为空")
   @ApiModelProperty(value = "自动延期/年")
   @Max(value = 99,message = "自动延期/年不能超过99")
   private Integer delayYear;

   @LengthTrim(max = 255,message = "主要进口产品最大长度不能超过255位")
   @ApiModelProperty(value = "主要进口产品")
   private String mainProduct;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;

}
