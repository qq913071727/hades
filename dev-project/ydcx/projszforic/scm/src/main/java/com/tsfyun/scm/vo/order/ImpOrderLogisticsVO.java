package com.tsfyun.scm.vo.order;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.InExpressTypeEnum;
import com.tsfyun.common.base.enums.PaymentDeliveryEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 订单物流信息响应实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="ImpOrderLogistics响应对象", description="订单物流信息响应实体")
public class ImpOrderLogisticsVO implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    private Long id;

    private Long impOrderId;

    /**
     * 境外交货方式
     */

    private String deliveryMode;

    /**
     * 境外交货方式
     */

    private String deliveryModeName;

    /**
     * 交货备注
     */

    private String deliveryModeMemo;

    /**
     * 快递类型
     */

    private String inExpressType;

    private String inExpressTypeName;
    public String getInExpressTypeName(){
        InExpressTypeEnum inExpressTypeEnum = InExpressTypeEnum.of(getInExpressType());
        return Objects.nonNull(inExpressTypeEnum)?inExpressTypeEnum.getName():"";
    }

    /**
     * 快递公司
     */

    private String hkExpress;


    private String hkExpressName;


    private String hkExpressNo;

    /**
     * 要求提货时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime takeTime;

    /**=
     * 供应商提货信息ID
     */
    private Long supplierTakeInfoId;

    /**
     * 提货公司
     */

    private String takeLinkCompany;

    /**
     * 提货联系人
     */

    private String takeLinkPerson;

    /**
     * 提货联系人电话
     */

    private String takeLinkTel;

    /**
     * 提货地址
     */

    private String takeAddress;

    /**
     * 中港包车
     */

    private Boolean isCharterCar;

    /**
     * 国内收货方式
     */

    private String receivingMode;

    /**
     * 国内收货方式
     */

    private String receivingModeName;

    /**
     * 收货备注
     */

    private String receivingModeMemo;

    /**
     * 物流公司
     */

    private Long logisticsCompanyId;

    /**
     * 物流公司
     */

    private String logisticsCompanyName;

    /**
     * 物流时效要求
     */

    private String prescription;


    /**
     * 送货付款方式
     */

    private String paymentDelivery;
    private String paymentDeliveryName;
    public String getPaymentDeliveryName(){
        PaymentDeliveryEnum paymentDeliveryEnum = PaymentDeliveryEnum.of(getPaymentDelivery());
        return Objects.nonNull(paymentDeliveryEnum)?paymentDeliveryEnum.getName():"";
    }

    /**
     * 提货仓库
     */

    private Long selfWareId;

    /**
     * 提货仓库名称
     */

    private String selfWareName;

    /**
     * 提货联系人电话
     */

    private String selfLinkPerson;

    /**
     * 提货人身份证号
     */

    private String selfPersonId;

    /**
     * 提货备注
     */

    private String selfMemo;

    /**
     * 收货信息
     */
    private Long customerDeliveryInfoId;

    /**
     * 收货公司
     */

    private String deliveryCompanyName;

    /**
     * 收货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 收货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 收货地址
     */

    private String deliveryAddress;
}
