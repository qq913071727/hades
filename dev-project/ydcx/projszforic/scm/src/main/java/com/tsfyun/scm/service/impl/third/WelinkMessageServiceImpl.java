package com.tsfyun.scm.service.impl.third;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.common.base.support.OperationEnum;
import com.tsfyun.scm.base.support.AliyunMsgOperation;
import com.tsfyun.scm.base.support.WelinkMsgOperation;
import com.tsfyun.scm.config.properties.AliyunProperties;
import com.tsfyun.scm.config.properties.WelinkProperties;
import com.tsfyun.scm.entity.third.LogSms;
import com.tsfyun.scm.service.third.AbstractMessageService;
import com.tsfyun.scm.service.third.ILogSmsService;
import com.tsfyun.scm.service.third.IMessageService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

@RefreshScope
@Service(value = "welinkMessage")
@Slf4j
public class WelinkMessageServiceImpl extends AbstractMessageService implements IMessageService {

    @Resource
    private WelinkProperties welinkProperties;

    @Autowired
    private ILogSmsService logSmsService;

    @Override
    public String send(String templateCode, String phoneNo, String templateParam) {
        Map<String,Object> params = new LinkedHashMap<String,Object>();
        params.put("sname",welinkProperties.getSname());
        params.put("spwd",welinkProperties.getSpwd());
        params.put("scorpid",welinkProperties.getScorpid());
        params.put("sprdid",welinkProperties.getSprdid());
        params.put("sdst",phoneNo);
        params.put("smsg",StrUtil.format("【{}】{}",welinkProperties.getSignName(),templateParam));
        log.info(JSON.toJSONString(params));
        String response = HttpUtil.get(welinkProperties.getDomain(),params);
        log.info(response);
        return "success";
    }

    @Override
    public ShortMessagePlatformEnum getPlatform() {
        return ShortMessagePlatformEnum.WELINK;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendVerificationCode(LogSms logSms) {
        String message = "";
        if(Objects.equals(isSend,Boolean.TRUE)) {
            OperationEnum welinkMessageTemplate = WelinkMsgOperation.getInstance().getMessageTemplate().get(MessageNodeEnum.of(logSms.getNode()));
            if(Objects.isNull(welinkMessageTemplate)) {
                log.warn("微网通联短信平台【{}】未找到短信节点【{}】的配置，不发送短信平台",welinkMessageTemplate.getName(),MessageNodeEnum.of(logSms.getNode()).getName());
                return;
            }
            message = send(welinkMessageTemplate.getCode(),logSms.getPhoneNo(), logSms.getContent());
        } else {
            message = "微网通联短信发送未开启";
        }
        logSms.setBackMessage(message);
        logSmsService.updateById(logSms);
    }
}
