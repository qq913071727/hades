package com.tsfyun.scm.mapper.risk;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.risk.RiskApproval;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.risk.RiskApprovalVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 风控审批 Mapper 接口
 * </p>
 *

 * @since 2020-05-08
 */
@Repository
public interface RiskApprovalMapper extends Mapper<RiskApproval> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<RiskApprovalVO> list(Map<String,Object> params);

    RiskApprovalVO detail(@Param(value = "id") Long id);

    RiskApproval lastApproval(@Param(value = "docType")String docType,@Param(value = "docId") String docId);

    Integer removeByDoc(@Param(value = "docType")String docType,@Param(value = "docId") String docId);
}
