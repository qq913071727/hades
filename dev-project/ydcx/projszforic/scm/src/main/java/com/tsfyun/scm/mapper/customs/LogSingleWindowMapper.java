package com.tsfyun.scm.mapper.customs;

import com.tsfyun.scm.entity.customs.LogSingleWindow;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 单一窗口日志 Mapper 接口
 * </p>
 *

 * @since 2020-05-07
 */
@Repository
public interface LogSingleWindowMapper extends Mapper<LogSingleWindow> {

}
