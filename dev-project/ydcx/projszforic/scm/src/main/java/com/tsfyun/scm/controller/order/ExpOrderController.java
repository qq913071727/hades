package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.service.order.IExpOrderService;
import com.tsfyun.scm.vo.finance.OverseasReceivingOrderVO;
import com.tsfyun.scm.vo.order.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 出口订单 前端控制器
 * </p>
 *
 *
 * @since 2021-01-26
 */
@RestController
@RequestMapping("/expOrder")
public class ExpOrderController extends BaseController {

    @Autowired
    private IExpOrderService expOrderService;

    /**
     * 新增订单
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Long> addOrder(@RequestBody ExpOrderPlusDTO dto) {
        return success(expOrderService.add(dto));
    }

    /**
     * 修改订单
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "edit")
    Result<Void> editOrder(@RequestBody ExpOrderPlusDTO dto) {
        expOrderService.edit(dto);
        return success( );
    }

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ExpOrderListVO>> list(@ModelAttribute ExpOrderQTO qto) {
        PageInfo<ExpOrderListVO> page = expOrderService.list(qto);
        return success((int) page.getTotal(),page.getList());
    }

    /**
     * 订单详情信息
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<ExpOrderPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(expOrderService.detailPlus(id,operation));
    }

    /**
     * 删除订单
     * @param id
     * @return
     */
    @PostMapping("/remove")
    @ApiOperation("删除订单")
    public Result<Void> remove(@RequestParam("id")Long id){
        expOrderService.removeOrder(id);
        return success();
    }

    /**
     * 审核
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "examine")
    public Result<Void> examine(@ModelAttribute @Valid TaskDTO dto){
        expOrderService.examine(dto);
        return success();
    }

    /**=
     * 分摊净重
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "shareNetWeight")
    public Result<Void> shareNetWeight(@RequestParam(value = "id")Long id,@RequestParam(value = "netWeight") BigDecimal netWeight){
        expOrderService.shareNetWeight(id,netWeight);
        return success();
    }
    /**=
     * 分摊毛重
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "shareGrossWeight")
    public Result<Void> shareGrossWeight(@RequestParam(value = "id")Long id,@RequestParam(value = "grossWeight") BigDecimal grossWeight){
        expOrderService.shareGrossWeight(id,grossWeight);
        return success();
    }

    /**=
     * 保存验货明细修改
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "saveInspectionMemberEdit")
    public Result<Void> saveInspectionMemberEdit(@RequestBody @Valid ExpOrderInspectionMemberDTO dto){
        expOrderService.saveInspectionMemberEdit(dto);
        return success();
    }

    /**=
     * 确认验货完成
     * @param id
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirmInspection")
    public Result<Void> confirmInspection(@RequestParam(value = "id")Long id){
        expOrderService.confirmInspection(id);
        return success();
    }

    /**=
     * 确认出口
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirmExp")
    public Result<Void> confirmExp(@ModelAttribute @Valid TaskDTO dto){
        expOrderService.confirmExp(dto);
        return success();
    }

    /**=
     * 设置申报信息
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "setupDeclaration")
    public Result<Void> setupDeclaration(@ModelAttribute @Valid SetupDeclarationDTO dto){
        expOrderService.setupDeclaration(dto);
        return success();
    }

    /**
     * 获取境外收款可认领订单
     * @return
     */
    @PostMapping(value = "collectionClaimedAmount")
    public Result<List<OverseasReceivingOrderVO>> collectionClaimedAmount(@ModelAttribute @Valid ClaimedAmountDTO dto){
        return success(expOrderService.collectionClaimedAmount(dto));
    }

    /**
     * 下单根据模板导入订单明细
     * @param file
     * @return
     */
    @PostMapping(value = "importMember")
    public Result<List<ImportExpOrderMemberVO>> importMember(@RequestPart(value = "file") MultipartFile file){
        return success(expOrderService.readDataFromMemberImport(file));
    }

    /**
     * 打印订单委托书
     * @param id
     * @return
     */
    @PostMapping("/getPrintEntrustData")
    @ApiOperation("打印订单委托书")
    public Result<ExpOrderEntrustVO> getPrintEntrustData(@RequestParam("id")Long id){
        return success(expOrderService.getPrintEntrustData(id));
    }

    /**
     * 统计订单金额
     * @param qto
     * @return
     */
    @PostMapping("/summaryOrderAmount")
    @ApiOperation("统计订单金额")
    public Result<BigDecimal> summaryOrderAmount(@ModelAttribute ExpOrderQTO qto){
        return success(expOrderService.summaryOrderAmount(qto));
    }

}

