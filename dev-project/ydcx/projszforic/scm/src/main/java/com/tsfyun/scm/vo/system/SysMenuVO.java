package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;

/**
 * 菜单响应实体
 */
@Data
public class SysMenuVO implements Serializable {

    /**
     * 菜单id
     */
    private String id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单排序
     */
    private int sort;

    /**
     * 备注
     */
    private String remark;


    /**
     * 菜单图标（配置时可以从菜单图标库中挑选）
     */
    private String icon;

    /**
     * 菜单类型1-目录；2-菜单（跳转页面）；3-按钮
     */
    private int type;

    /**
     * 操作码
     */
    private String operationCode;

    private String action;

    /**=
     * 可操作的状态编码
     */
    private String operationState;

    /**=
     * 是否任务
     */
    private Boolean isTask;

    /**
     * 操作链接
     */
    private String url;

    /**
     * 选择的记录数
     */
    private Integer checkNum;


}
