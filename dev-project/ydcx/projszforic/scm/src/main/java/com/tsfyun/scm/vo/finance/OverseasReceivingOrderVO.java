package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ExpOrderStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Data
public class OverseasReceivingOrderVO implements Serializable {

    private Long id;
    /**
     * 出口订单ID
     */

    private Long expOrderId;

    /**
     * 出口订单号
     */

    private String orderNo;

    /**
     * 客户单号
     */

    private String clientNo;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 订单日期-由后端写入
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;

    /**
     * 认领金额
     */

    private BigDecimal accountValue;

    private BigDecimal settlementValue;// 分摊到订单的结汇人民币金额

    private BigDecimal settlementValue1;// 分摊到订单的结汇人民币金额(详情展示怕影响计算)

    /**
     * 报关金额
     */

    private BigDecimal decTotalPrice;

    /**
     * 币制
     */

    private String currencyId;
    private String currencyName;

    private BigDecimal unclaimedValue;//可认领金额(查下订单有)

    private String statusDesc;

    public String getStatusDesc() {
        ExpOrderStatusEnum expOrderStatusEnum = ExpOrderStatusEnum.of(statusId);
        return Optional.ofNullable(expOrderStatusEnum).map(ExpOrderStatusEnum::getName).orElse("");
    }

    public BigDecimal getUnclaimedValue(){
        return getDecTotalPrice().subtract(getAccountValue()).setScale(2,BigDecimal.ROUND_HALF_UP);
    }
}
