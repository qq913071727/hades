package com.tsfyun.scm.vo.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 营业执照数据
 */
@ApiModel(value = "识别到的公司信息")
@Data
public class BusinessLicenseVO implements Serializable {
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String name;

    /**
     * 统一社会信用代码(18位)
     */
    @ApiModelProperty(value = "统一社会信用代码")
    private String socialNo;
    /**
     * 公司法人
     */
    @ApiModelProperty(value = "公司法人")
    private String legalPerson;
    /**
     * 公司地址
     */
    @ApiModelProperty(value = "公司地址")
    private String address;
}
