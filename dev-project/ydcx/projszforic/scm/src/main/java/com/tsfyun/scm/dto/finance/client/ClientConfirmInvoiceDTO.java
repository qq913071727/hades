package com.tsfyun.scm.dto.finance.client;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ClientConfirmInvoiceDTO implements Serializable {

    @NotNull(message = "发票ID不能为空")
    private Long id;

    @NotNull(message = "是否确定编码")
    private Boolean confirm;

    @NotEmptyTrim(message = "原因不能为空")
    @LengthTrim(max = 500,message = "原因最大长度不能超过500位")
    private String reason;
}
