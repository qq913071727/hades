package com.tsfyun.scm.vo.finance.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.InvoiceStatusEnum;
import com.tsfyun.common.base.enums.finance.InvoiceTaxTypeEnum;
import com.tsfyun.common.base.enums.finance.InvoiceTypeEnum;
import com.tsfyun.scm.util.ClientInfoStatus;
import com.tsfyun.scm.util.ClientStatusMappingUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description: 客户发票列表响应
 * @CreateDate: Created in 2020/11/19 13:15
 */
@Data
public class ClientInvoiceListVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    /**
     * 状态编码
     */
    private String statusId;

    private String clientStatusId;

    private String clientStatusDesc;

    @ApiModelProperty(value = "结算类型")
    private String taxType;

    @ApiModelProperty(value = "申请日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime applyDate;

    @ApiModelProperty(value = "发票类型")
    private String invoiceType;

    @ApiModelProperty(value = "实际发票编号")
    private String invoiceNo;

    @ApiModelProperty(value = "实际开票日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime invoiceDate;

    @ApiModelProperty(value = "开票金额")
    private BigDecimal invoiceVal;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "快递单号")
    private String expressNo;

    @ApiModelProperty(value = "购方单位名称")
    private String buyerName;

    @ApiModelProperty(value = "购方银行")
    private String buyerBankName;

    @ApiModelProperty(value = "银行账号")
    private String buyerBankAccount;

    private String taxTypeDesc;

    private String invoiceTypeDesc;


    public String getTaxTypeDesc( ) {
        return Optional.ofNullable(InvoiceTaxTypeEnum.of(taxType)).map(InvoiceTaxTypeEnum::getName).orElse("");
    }

    public String getInvoiceTypeDesc( ) {
        return Optional.ofNullable(InvoiceTypeEnum.of(invoiceType)).map(InvoiceTypeEnum::getName).orElse("");
    }

    public String getClientStatusId() {
        return ClientStatusMappingUtil.getClientInvoiceStatus(statusId);
    }

    public String getClientStatusDesc() {
        ClientInfoStatus clientInfoStatus = ClientStatusMappingUtil.INVOICE_CLENT_STATUS_MAPPING.get(getClientStatusId());
        return Optional.ofNullable(clientInfoStatus).map(ClientInfoStatus::getClientStatusDesc).orElse("");
    }

}
