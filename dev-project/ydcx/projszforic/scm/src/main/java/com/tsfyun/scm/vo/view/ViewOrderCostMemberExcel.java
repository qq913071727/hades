package com.tsfyun.scm.vo.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ViewOrderCostMemberExcel implements Serializable {

    private String docNo;// 订单号
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date orderDate;// 订单日期
    private String customerName;// 客户名称
    private String salePersonName;// 销售人员
    private BigDecimal totalPrice;// 委托金额
    private String currencyName;// 币制
    private String quoteTypeName;// 报价类型
}
