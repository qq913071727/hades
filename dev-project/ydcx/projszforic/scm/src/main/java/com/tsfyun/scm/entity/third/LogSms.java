package com.tsfyun.scm.entity.third;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-09-16
 */
@Data
public class LogSms implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     @KeySql(genId = IdWorker.class)
     private Long id;
    /**
     * 接口消息
     */

    private String backMessage;

    /**
     * 验证码
     */

    private String code;

    /**
     * 短信内容
     */

    private String content;

    /**
     * IP地址
     */

    private String ip;

    /**
     * 发送节点
     */

    private String node;

    /**
     * 手机号
     */

    private String phoneNo;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;
}
