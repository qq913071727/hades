package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 危险品联系人
 * @since Created in 2020/4/22 13:52
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UNDGContact", propOrder = {
        "name",
        "communication"
})
public class UNDGContact {

    //危险品联系人姓名
    @XmlElement(name = "Name")
    private String name;

    //危险品联系人通讯方式信息
    @XmlElement(name = "Communication")
    private Communication communication;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Communication getCommunication() {
        return communication;
    }

    public void setCommunication(Communication communication) {
        this.communication = communication;
    }
}
