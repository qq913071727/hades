package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.order.ExpOrderCost;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单费用 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-21
 */
@Repository
public interface ExpOrderCostMapper extends Mapper<ExpOrderCost> {

    // 获取订单所有费用
    List<ExpOrderCostVO> getAllOrderCosts(@Param(value = "orderId") Long orderId, @Param(value = "expOrderNo") String expOrderNo);
    // 根据订单ID和支付类型获取订单费用
    List<ExpOrderCostVO> findCostByOrderIdAndCollectionSource(@Param(value = "orderId") Long orderId, @Param(value = "collectionSources") List<String> collectionSources);

    //非系统生成的费用科目
    List<ExpOrderCost> findByNonAutomaticZfList(@Param(value = "orderId") Long orderId);

    //根据订单查询可核销的费用
    List<ExpOrderCostVO> canWriteOffByOrderId(@Param(value = "orderId")Long orderId);

    //获取客户可核销的费用
    List<ExpOrderCostVO> canWriteOffList(@Param(value = "customerId")Long customerId);

    // 获取订单应收代垫费金额
    BigDecimal obtainOrderAdvancePayment(@Param(value = "orderId")Long orderId,@Param(value = "collectionSource") String collectionSource);

    // 订单费用收款完成
    void updateCollectionCompletedByOrder(@Param(value = "orderId")Long orderId,@Param(value = "collectionSource") String collectionSource);

    // 出口费用锁定
    Integer expLockCost(@Param(value = "orderId") Long orderId);
    // 出口费用解锁
    Integer expUnLockCost(@Param(value = "costIds") List<Long> costIds);

    List<ExpOrderCostVO> obtainOrderNotLockCosts(@Param(value = "orderId") Long orderId);
    //根据订单号和费用科目查找费用
    ExpOrderCost findByExpOrderNoAndExpenseSubjectId(@Param(value = "expOrderNo") String expOrderNo,@Param(value = "expenseSubjectId") String expenseSubjectId);
    //根据订单号批量查找费用
    List<ExpOrderCost> findByExpOrderNoBatch(@Param(value = "expOrderNos") List<String> expOrderNos);
    //获取出口订单费用列表
    @DataScope(tableAlias = "c",customerTableAlias = "s")
    List<ExpOrderCostVO> list(Map<String,Object> params);
}
