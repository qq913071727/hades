package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.finance.RefundAccountQTO;
import com.tsfyun.scm.entity.finance.RefundAccount;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.RefundAccountVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 退款申请 Mapper 接口
 * </p>
 *
 *
 * @since 2020-11-27
 */
@Repository
public interface RefundAccountMapper extends Mapper<RefundAccount> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<RefundAccountVO> list(Map<String,Object> params);

    /**
     * 根据客户和状态集合统计数量
     * @param statusIds
     * @return
     */
    Long countByCustomerAndStatusIds(@Param(value = "customerId")Long customerId, @Param(value = "statusIds") List<String> statusIds);

}
