package com.tsfyun.scm.vo.customer;

import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**=
 * 进口下单获取客户有效协议报价
 */
@Data
public class ImpAgreementVO implements Serializable {

    //业务类型
    private String businessType;
    //业务类型名称
    private String businessTypeName;
    //报关类型
    private String declareType;
    //报关类型名称
    private String declareTypeName;
    //成交方式
    private String transactionMode;
    //报价
    private List<SimpleQuoteVO> quotes;

    public String getBusinessTypeName() {
        BusinessTypeEnum businessTypeEnum = BusinessTypeEnum.of(businessType);
        return Objects.nonNull(businessTypeEnum) ? businessTypeEnum.getName() : null;
    }
    public String getDeclareTypeName() {
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(declareType);
        return Objects.nonNull(declareTypeEnum) ? declareTypeEnum.getName() : null;
    }
}
