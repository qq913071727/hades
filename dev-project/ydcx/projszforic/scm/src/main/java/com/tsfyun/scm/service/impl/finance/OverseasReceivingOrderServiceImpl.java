package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.entity.finance.OverseasReceivingAccount;
import com.tsfyun.scm.entity.finance.OverseasReceivingOrder;
import com.tsfyun.scm.mapper.finance.OverseasReceivingOrderMapper;
import com.tsfyun.scm.service.finance.IOverseasReceivingOrderService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.order.IExpOrderService;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.finance.OverseasReceivingOrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 境外收款认领订单 服务实现类
 * </p>
 *
 *
 * @since 2021-10-11
 */
@Service
public class OverseasReceivingOrderServiceImpl extends ServiceImpl<OverseasReceivingOrder> implements IOverseasReceivingOrderService {

    @Autowired
    private OverseasReceivingOrderMapper overseasReceivingOrderMapper;
    @Autowired
    private IExpOrderService expOrderService;

    @Override
    public List<OverseasReceivingOrderVO> findByAccountId(Long accountId) {
        return overseasReceivingOrderMapper.findByAccountId(accountId);
    }

    @Override
    public List<OverseasReceivingOrder> findOverseasReceivingOrderByAccountId(Long accountId) {
        return overseasReceivingOrderMapper.findOverseasReceivingOrderByAccountId(accountId);
    }

    @Override
    public OverseasReceivingOrder findByAccountIdAndExpOrderId(Long accountId, Long orderId) {
        return overseasReceivingOrderMapper.selectOneByExample(Example.builder(OverseasReceivingOrder.class).where(TsfWeekendSqls.<OverseasReceivingOrder>custom()
                .andEqualTo(false,OverseasReceivingOrder::getOverseasReceivingId,accountId)
                .andEqualTo(false,OverseasReceivingOrder::getExpOrderId,orderId)).build());
    }

    @Override
    public List<String> findOrderNo(Long accountId) {
        return overseasReceivingOrderMapper.findOrderNo(accountId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void clearClaimOrder(OverseasReceivingAccount account) {
        OverseasReceivingOrder query = new OverseasReceivingOrder();
        query.setOverseasReceivingId(account.getId());
        BigDecimal clearValue = BigDecimal.ZERO;
        List<OverseasReceivingOrder> receivingOrderList = super.list(query);
        for(OverseasReceivingOrder receivingOrder : receivingOrderList){
            // 清空合同数据
            expOrderService.clearClaimOrder(receivingOrder);
            clearValue = clearValue.add(receivingOrder.getAccountValue()).setScale(2,BigDecimal.ROUND_HALF_UP);
            super.removeById(receivingOrder.getId());
        }
        if(account.getWriteValue().compareTo(clearValue)!=0){
            throw new ServiceException("收款单主单和明细认领金额不一致");
        }
    }

    @Override
    public List<OverseasReceivingOrderVO> overseasReceivings(List<String> accountNos) {
        return overseasReceivingOrderMapper.overseasReceivings(accountNos);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void writeSettlementValue(Long id, BigDecimal settlementValue) {
        overseasReceivingOrderMapper.writeSettlementValue(id,settlementValue);
    }
}
