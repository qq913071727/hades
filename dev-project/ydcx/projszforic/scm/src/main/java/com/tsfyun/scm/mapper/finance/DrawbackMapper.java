package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.finance.Drawback;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.DrawbackVO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 退税单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Repository
public interface DrawbackMapper extends Mapper<Drawback> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<DrawbackVO> list(Map<String,Object> params);
}
