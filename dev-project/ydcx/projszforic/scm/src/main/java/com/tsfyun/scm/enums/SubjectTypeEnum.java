package com.tsfyun.scm.enums;

import java.util.Arrays;
import java.util.Objects;

public enum SubjectTypeEnum{
    ABROAD("abroad","境外公司"),
    MAINLAND("mainland","大陆公司"),
    ;

    private String code;
    private String name;

    SubjectTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }


    public static SubjectTypeEnum of(String code) {
        return Arrays.stream(SubjectTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

