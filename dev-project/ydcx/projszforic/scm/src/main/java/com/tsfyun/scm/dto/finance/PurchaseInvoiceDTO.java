package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.validator.group.AddGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class PurchaseInvoiceDTO implements Serializable {

    @NotNull(message = "合同ID不能为空")
    @ApiModelProperty(value = "合同ID")
    private Long id;

    @ApiModelProperty(value = "开票日期")
    @NotNull(message = "开票日期不能为空")
    private Date invoicingDate;

    @ApiModelProperty(value = "收票日期")
    @NotNull(message = "收票日期不能为空")
    private Date invoiceDate;

    @NotNull(message = "收票金额不能为空")
    @Digits(integer = 8, fraction = 2, message = "收票金额整数位不能超过8位，小数位不能超过2")
    @DecimalMin(value = "0.01",message = "收票金额必须大于0.00")
    @ApiModelProperty(value = "收票金额")
    private BigDecimal invoiceVal;

    @LengthTrim(max = 200,message = "发票号码最大长度不能超过200位")
    @ApiModelProperty(value = "发票号码")
    private String invoiceNo;
}
