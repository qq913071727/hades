package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * @Description:
 * @since Created in 2020/4/22 14:39
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ValueAmount {

    @XmlAttribute(name = "currencyID")
    private String currencyID;

    @XmlValue
    private String value;	//变量名随意

    public ValueAmount() {

    }

    public ValueAmount(String currencyID, String value) {
        this.currencyID = currencyID;
        this.value = value;
    }

}
