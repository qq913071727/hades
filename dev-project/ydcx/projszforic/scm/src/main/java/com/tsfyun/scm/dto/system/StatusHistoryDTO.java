package com.tsfyun.scm.dto.system;

import lombok.Data;

import java.io.Serializable;

/**
 * 单据历史请求实体
 */
@Data
public class StatusHistoryDTO implements Serializable {

    /**
     * 单据ID
     */
    private Long domainId;

    /**
     * 单据类型
     */
    private String domainType;

    /**
     * 原单据状态
     */
    private String originalStatusId;

    /**
     * 原单据状态
     */
    private String originalStatusName;

    /**
     * 现单据状态
     */
    private String nowStatusId;

    /**
     * 现单据状态名称
     */
    private String nowStausName;

    /**
     * 备注
     */
    private String memo;

}
