package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.DrawbackOrder;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.DrawbackOrderVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 退税订单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Repository
public interface DrawbackOrderMapper extends Mapper<DrawbackOrder> {

    List<DrawbackOrderVO> findOrderByDrawbackId(@Param(value = "drawbackId") Long drawbackId);
    List<DrawbackOrder> findByDrawbackId(@Param(value = "drawbackId")Long drawbackId);
}
