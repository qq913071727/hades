package com.tsfyun.scm.vo.order;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.scm.enums.DeliveryDestinationEnum;
import com.tsfyun.scm.enums.DeliveryMethodEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 出口订单物流信息响应实体
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Data
@ApiModel(value="ExpOrderLogistics响应对象", description="出口订单物流信息响应实体")
public class ExpOrderLogisticsVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long expOrderId;

    @ApiModelProperty(value = "国内交货方式")
    private String deliveryMode;

    @ApiModelProperty(value = "国内交货方式")
    private String deliveryModeName;

    @ApiModelProperty(value = "交货备注")
    private String deliveryModeMemo;

    @ApiModelProperty(value = "国内交货快递单号")
    private String deliveryExpressNo;

    @ApiModelProperty(value = "国内提货方式")
    private String deliveryMethod;
    private String deliveryMethodDesc;

    @ApiModelProperty(value = "要求提货时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date takeTime;

    @ApiModelProperty(value = "国内提货信息")
    private Long customerDeliveryInfoId;

    @ApiModelProperty(value = "提货公司")
    private String deliveryCompanyName;

    @ApiModelProperty(value = "提货联系人")
    private String deliveryLinkPerson;

    @ApiModelProperty(value = "提货联系人电话")
    private String deliveryLinkTel;

    @ApiModelProperty(value = "提货地址")
    private String deliveryAddress;

    @ApiModelProperty(value = "中港包车")
    private Boolean isCharterCar;

    @ApiModelProperty(value = "送货目的地")
    private String deliveryDestination;
    private String deliveryDestinationDesc;

    @ApiModelProperty(value = "境外送货信息ID")
    private Long supplierTakeInfoId;

    @ApiModelProperty(value = "收货公司")
    private String takeLinkCompany;

    @ApiModelProperty(value = "收货联系人")
    private String takeLinkPerson;

    @ApiModelProperty(value = "收货联系人电话")
    private String takeLinkTel;

    @ApiModelProperty(value = "收货地址")
    private String takeAddress;

    @ApiModelProperty(value = "收货备注")
    private String receivingModeMemo;

    public String getDeliveryMethodDesc(){
        DeliveryMethodEnum deliveryMethodEnum = DeliveryMethodEnum.of(getDeliveryMethod());
        return Objects.nonNull(deliveryMethodEnum)?deliveryMethodEnum.getName():"";
    }
    public String getDeliveryDestinationDesc(){
        DeliveryDestinationEnum deliveryDestinationEnum = DeliveryDestinationEnum.of(getDeliveryDestination());
        return Objects.nonNull(deliveryDestinationEnum)?deliveryDestinationEnum.getName():"";
    }
}
