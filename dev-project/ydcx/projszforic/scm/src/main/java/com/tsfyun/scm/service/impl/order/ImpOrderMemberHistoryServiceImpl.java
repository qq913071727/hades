package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import com.tsfyun.scm.mapper.order.ImpOrderMemberHistoryMapper;
import com.tsfyun.scm.service.order.IImpOrderMemberHistoryService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 原始订单明细 服务实现类
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Service
public class ImpOrderMemberHistoryServiceImpl extends ServiceImpl<ImpOrderMemberHistory> implements IImpOrderMemberHistoryService {

    @Autowired
    private ImpOrderMemberHistoryMapper impOrderMemberHistoryMapper;

    @Override
    public void removeByOrderId(Long orderId) {
        impOrderMemberHistoryMapper.removeByOrderId(orderId);
    }


    @Override
    public List<ImpOrderMemberHistory> getByOrderId(Long orderId) {
        TsfWeekendSqls wheres = TsfWeekendSqls.<ImpOrderMemberHistory>custom().andEqualTo(false,ImpOrderMemberHistory::getImpOrderId,orderId);
        List<ImpOrderMemberHistory> members = impOrderMemberHistoryMapper.selectByExample(Example.builder(ImpOrderMemberHistory.class).where(wheres).build());
        if(CollUtil.isNotEmpty(members)){
            //按行号升序
            return members.stream().sorted(Comparator.comparing(ImpOrderMemberHistory::getRowNo)).collect(Collectors.toList());
        }
        return members;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchUpdateSplitMembers(List<ImpOrderMemberHistory> impOrderMemberHistoryList) {
        if(Objects.nonNull(impOrderMemberHistoryList)){
            impOrderMemberHistoryMapper.batchUpdateSplitMembers(impOrderMemberHistoryList);
        }
    }
}
