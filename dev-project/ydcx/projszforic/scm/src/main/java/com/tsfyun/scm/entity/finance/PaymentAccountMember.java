package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 付款单明细
 * </p>
 *

 * @since 2020-04-24
 */
@Data
public class PaymentAccountMember  {

     private static final long serialVersionUID=1L;

     @Id
     @KeySql(genId = IdWorker.class)
     private Long id;

    private Long paymentAccountId;


    private BigDecimal accountValue;


    private BigDecimal accountValueCny;


    private Long impOrderId;


    private String impOrderNo;

    //创建时间
    private LocalDateTime dateCreated;


}
