package com.tsfyun.scm.service.support;

import com.tsfyun.scm.dto.support.TaskNoticeFocusDTO;
import com.tsfyun.scm.entity.support.TaskNoticeFocus;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.support.TaskNoticeFocusVO;

import java.util.List;

/**
 * <p>
 * 用户关注任务通知 服务类
 * </p>
 *

 * @since 2020-04-09
 */
public interface ITaskNoticeFocusService extends IService<TaskNoticeFocus> {

    /**
     * 用户不接收某操作码的任务通知
     * @param dto
     */
    void dislikeTaskNotice(TaskNoticeFocusDTO dto);

    /**
     * 用户接收操作码的任务通知
     * @param dto
     */
    void likeTaskNotice(TaskNoticeFocusDTO dto);

    /**
     * 获取用户不接收任务通知的数据集合
     * @return
     */
    List<TaskNoticeFocusVO> getMyDislikeTaskNotice();

    /**
     * 根据操作码获取不接收任务通知的用户
     * @param operationCode
     * @return
     */
    List<TaskNoticeFocus> getDislikePersonByOperationCode(String operationCode);

    /**
     * 获取某用户不接受任务通知的操作码
     * @param personId
     * @return
     */
    List<String> getDislikeOperationCodes(Long personId);

}
