package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 发票或内贸合同费用明细响应实体
 * </p>
 *

 * @since 2020-05-18
 */
@Data
@ApiModel(value="InvoiceCostMember响应对象", description="发票或内贸合同费用明细响应实体")
public class InvoiceCostMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "单据id")
    private String docId;

    @ApiModelProperty(value = "单据编号")
    private String docNo;

    @ApiModelProperty(value = "单据类型")
    private String docType;

    @ApiModelProperty(value = "费用科目编码")
    private String expenseSubjectId;

    @ApiModelProperty(value = "费用科目名称")
    private String expenseSubjectName;

    @ApiModelProperty(value = "费用")
    private BigDecimal costMoney;

    @ApiModelProperty(value = "备注")
    private String memo;


    /**
     * 订单编号
     */
    private String orderNo;


}
