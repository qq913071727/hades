package com.tsfyun.scm.service.impl.finance;

import com.tsfyun.scm.entity.finance.DrawbackOrder;
import com.tsfyun.scm.mapper.finance.DrawbackOrderMapper;
import com.tsfyun.scm.service.finance.IDrawbackOrderService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.finance.DrawbackOrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 退税订单 服务实现类
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Service
public class DrawbackOrderServiceImpl extends ServiceImpl<DrawbackOrder> implements IDrawbackOrderService {

    @Autowired
    private DrawbackOrderMapper drawbackOrderMapper;

    @Override
    public List<DrawbackOrderVO> findOrderByDrawbackId(Long drawbackId) {
        return drawbackOrderMapper.findOrderByDrawbackId(drawbackId);
    }

    @Override
    public List<DrawbackOrder> findByDrawbackId(Long drawbackId) {
        return drawbackOrderMapper.findByDrawbackId(drawbackId);
    }
}
