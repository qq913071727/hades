package com.tsfyun.scm.controller.finance;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.WriteOffOrderCostDTO;
import com.tsfyun.common.base.dto.WriteOffOrderCostPlusDTO;
import com.tsfyun.scm.service.finance.IWriteOffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 核销（仅供内部服务调用）
 */
@RestController
@RequestMapping("/writeOff")
public class WriteOffController extends BaseController {

    @Autowired
    private IWriteOffService writeOffService;

    /**
     * 代理报关订单
     * 核销订单指定费用
     * @param dto
     * @return
     */
    @PostMapping("writeOffTrustOrderCosts")
    public Result<List<WriteOffOrderCostDTO>> writeOffTrustOrderCosts(@RequestBody @Validated WriteOffOrderCostPlusDTO dto){
        return success(writeOffService.writeOffTrustOrderCosts(dto));
    }

    /**
     * 代理报关订单
     * 取消订单费用核销
     * @return
     */
    @PostMapping("cancelWriteOffByTrustOrderCost")
    public Result<Void> cancelWriteOffByTrustOrderCost(@RequestBody @Validated WriteOffOrderCostDTO dto){
        writeOffService.cancelWriteOffByTrustOrderCost(dto);
        return success();
    }
}
