package com.tsfyun.scm.vo.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-12
 */
@Data
@ApiModel(value="Supplier响应对象", description="响应实体")
public class SupplierVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 客户名称
     */
    private String customerName;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;


}
