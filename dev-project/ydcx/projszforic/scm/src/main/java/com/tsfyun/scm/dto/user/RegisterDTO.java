package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**=
 * 用户注册
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDTO implements Serializable {

    @NotEmptyTrim(message = "手机号不能为空")
    @Pattern(regexp = "^[1][0-9]{10}$",message = "手机号格式错误")
    private String phoneNo;
}
