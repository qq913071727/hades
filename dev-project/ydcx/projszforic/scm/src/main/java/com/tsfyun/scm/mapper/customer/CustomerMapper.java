package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.common.base.vo.CustomerSimpleVO;
import com.tsfyun.scm.dto.customer.CustomerDTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.CustomerVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-03
 */
@Repository
public interface CustomerMapper extends Mapper<Customer> {

    @DataScope(relateCustomer = false)
    List<Customer> simpleSelect(Map<String,Object> params);

    @DataScope(relateCustomer = false)
    List<Customer> list(Map<String,Object> params);

    /**
     * 客户列表带用户信息
     * @param params
     * @return
     */
    @DataScope(customerTableAlias = "c",relateCustomer = false)
    List<CustomerVO> customerAndUserList(Map<String,Object> params);

    Customer findByName(@Param(value = "name") String name);

    @DataScope(relateCustomer = false)
    Customer getByNameWithRight(Map<String,Object> params);

    void updateStatus(@Param(value = "id")Long id,@Param(value = "statusId")String statusId);

    CustomerSimpleVO findByCustomerId(@Param(value = "customerId")Long customerId);

}
