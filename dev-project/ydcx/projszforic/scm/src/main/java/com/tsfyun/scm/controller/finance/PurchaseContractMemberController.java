package com.tsfyun.scm.controller.finance;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 采购合同明细 前端控制器
 * </p>
 *
 *
 * @since 2021-09-26
 */
@RestController
@RequestMapping("/purchaseContractMember")
public class PurchaseContractMemberController extends BaseController {

}

