package com.tsfyun.scm.vo.customer;

import java.math.BigDecimal;

import com.tsfyun.common.base.enums.TransactionModeEnum;
import com.tsfyun.common.base.enums.VoluntarilyTaxEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 进口条款响应实体
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Data
@ApiModel(value="ImpClause响应对象", description="进口条款响应实体")
public class ImpClauseVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "协议ID")
    private Long agreementId;

    @ApiModelProperty(value = "缴税方式")
    private String voluntarilyTax;

    @ApiModelProperty(value = "货款额度")
    private BigDecimal goodsQuota;

    @ApiModelProperty(value = "税款额度")
    private BigDecimal taxQuota;

    @ApiModelProperty(value = "额度说明")
    private String quotaExplain;

    @ApiModelProperty(value = "成交方式")
    private String transactionMode;

    /**=
     * 货款汇率类型
     */
    private String goodsRate;

    /**=
     * 货款汇率时间
     */
    private String goodsRateTime;

    /**=
     * 税款以实缴定应收
     */
    Boolean isPaidIn;

    /**=
     * 税款汇率
     */
    private String taxRate;

    /**=
     * 税款汇率时间
     */
    private String taxRateTime;

    /**
     * 最大超期天数
     */
    private Integer overdueDays;

    /**
     * 缴税方式说明
     */
    private String voluntarilyTaxDesc;

    /**
     * 成交方式说明
     */
    private String transactionModeDesc;


    public String getVoluntarilyTaxDesc() {
        VoluntarilyTaxEnum voluntarilyTaxEnum = VoluntarilyTaxEnum.of(voluntarilyTax);
        return Objects.nonNull(voluntarilyTaxEnum) ? voluntarilyTaxEnum.getName() : null;
    }

    public String getTransactionModeDesc() {
        TransactionModeEnum transactionModeEnum = TransactionModeEnum.of(transactionMode);
        return Objects.nonNull(transactionModeEnum) ? transactionModeEnum.getName() : null;
    }
}
