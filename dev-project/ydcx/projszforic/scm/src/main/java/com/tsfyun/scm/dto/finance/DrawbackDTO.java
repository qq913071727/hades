package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.time.LocalTime;

import com.tsfyun.common.base.validator.group.AddGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

import com.tsfyun.common.base.validator.group.UpdateGroup;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 退税单请求实体
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Data
@ApiModel(value="Drawback请求对象", description="退税单请求实体")
public class DrawbackDTO implements Serializable {
   @NotEmptyTrim(message = "采购合同ID不能为空")
   private String purchaseId;

   @ApiModelProperty(value = "要求付款时间")
   @NotNull(message = "要求付款时间不能为空",groups = {AddGroup.class})
   private Date claimPaymentDate;

   @NotEmptyTrim(message = "收款方不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 255,message = "收款方最大长度不能超过255位",groups = {AddGroup.class})
   @ApiModelProperty(value = "收款方")
   private String payeeName;

   @NotEmptyTrim(message = "收款银行不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 100,message = "收款银行最大长度不能超过100位",groups = {AddGroup.class})
   @ApiModelProperty(value = "收款银行")
   private String payeeBank;

   @NotEmptyTrim(message = "收款银行账号不能为空",groups = {AddGroup.class})
   @LengthTrim(max = 50,message = "收款银行账号最大长度不能超过50位",groups = {AddGroup.class})
   @ApiModelProperty(value = "收款银行账号")
   private String payeeBankNo;

   @NotNull(message = "本次退税比例不能为空")
   @Digits(integer = 3, message = "本次退税比例整数位最大长度不能超过3位，小数位不能超过2位", fraction = 2)
   @ApiModelProperty(value = "本次退税比例")
   private BigDecimal billie;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位",groups = {AddGroup.class})
   @ApiModelProperty(value = "备注")
   private String memo;


}
