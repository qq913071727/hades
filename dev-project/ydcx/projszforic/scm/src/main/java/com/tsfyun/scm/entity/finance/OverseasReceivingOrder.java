package com.tsfyun.scm.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 境外收款认领订单
 * </p>
 *
 *
 * @since 2021-10-11
 */
@Data
public class OverseasReceivingOrder implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    private Long overseasReceivingId;

    /**
     * 出口订单ID
     */

    private Long expOrderId;

    /**
     * 出口订单号
     */

    private String orderNo;

    /**
     * 认领金额
     */

    private BigDecimal accountValue;

    /**
     * 结汇人民币金额
     */
    private BigDecimal settlementValue;
}
