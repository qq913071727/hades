package com.tsfyun.scm.dto.order;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class SelfHelpSplitDTO implements Serializable {
    @NotNull(message = "订单ID不能为空")
    private Long orderId;
    @NotNull(message = "拆单明细不能为空")
    private List<SelfHelpSplitMemberDTO> members;
}
