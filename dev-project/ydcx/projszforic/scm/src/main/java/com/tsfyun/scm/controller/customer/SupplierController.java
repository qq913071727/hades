package com.tsfyun.scm.controller.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.SupplierDTO;
import com.tsfyun.scm.dto.customer.SupplierPlusInfoDTO;
import com.tsfyun.scm.dto.customer.SupplierQTO;
import com.tsfyun.scm.service.customer.ISupplierService;
import com.tsfyun.scm.vo.customer.SimpleSupplierVO;
import com.tsfyun.scm.vo.customer.SupplierPlusVO;
import com.tsfyun.scm.vo.customer.SupplierVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "supplier")
public class SupplierController extends BaseController {

    @Autowired
    private ISupplierService supplierService;

    /**
     * 新增供应商信息（含供应商提货信息和供应商银行）
     * @param dto
     * @return 返回供应商id
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Long> add(@RequestBody @Validated(AddGroup.class) SupplierPlusInfoDTO dto) {
        return success(supplierService.addPlus(dto));
    }

    /**
     * 修改供应商信息（含供应商提货信息和供应商银行）
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated(UpdateGroup.class) SupplierPlusInfoDTO dto) {
        supplierService.editPlus(dto);
        return success();
    }


    /**
     * 供应商详情信息，返回供应商信息，供应商银行信息，供应商提货信息
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    Result<SupplierPlusVO> info(@RequestParam(value = "id")Long id) {
        return success(supplierService.detailPlus(id));
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        supplierService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 获取客户的供应商下拉
     * @param customerId
     * @return
     */
    @GetMapping("select")
    public Result<List<SimpleSupplierVO>> select(@RequestParam("customerId")Long customerId){
        return success(supplierService.select(customerId));
    }

    /**
     * 客户端获取客户的供应商下拉（不通过前端参数获取）
     * @return
     */
    @GetMapping("client/select")
    public Result<List<SimpleSupplierVO>> clientSelect( ){
        return success(supplierService.select(SecurityUtil.getCurrentCustomerId()));
    }

    /**=
     * 根据客户模糊查询供应商
     * @param customerName
     * @param name
     * @return
     */
    @PostMapping("vague")
    public Result<List<SimpleSupplierVO>> vague(
            @RequestParam(value = "customerName",required = false)String customerName,
            @RequestParam(value = "name",required = false)String name
    ){
        return success(supplierService.vague(customerName,name));
    }

    /**
     * 简单新增供应商信息（不含供应商提货信息和供应商银行）
     * @param dto
     * @return 返回供应商id
     */
    @PostMapping(value = "new")
    Result<Long> newly(@ModelAttribute @Validated(AddGroup.class) SupplierDTO dto) {
        return success(supplierService.simpleAdd(dto).getId());
    }

    /**
     * 简单修改供应商信息（不含供应商提货信息和供应商银行）
     * @param dto
     * @return 返回供应商id
     */
    @PostMapping(value = "update")
    Result<Void> update(@ModelAttribute @Validated(UpdateGroup.class) SupplierDTO dto) {
        supplierService.simpleEdit(dto);
        return success();
    }

    /**
     * 带权限分页查询客户供应商信息（查询条件customerName-客户名称;name-供应商名称)
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<SupplierVO>> page(SupplierQTO qto) {
        PageInfo<SupplierVO> page = supplierService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 删除供应商信息（真删，如果存在供应商提货和银行信息则不允许删除）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        supplierService.remove(id);
        return success();
    }

    /**
     * 供应商详情信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<SupplierVO> detail(@RequestParam(value = "id")Long id) {
        return success(supplierService.get(id));
    }

}
