package com.tsfyun.scm.entity.customer;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 快递模式标准报价计算代理费明细
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Data
public class CustomerExpressQuoteMember implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 报价单id
     */

    private Long quoteId;

    /**
     * 开始重量
     */

    private BigDecimal startWeight;

    /**
     * 结束重量
     */

    private BigDecimal endWeight;

    /**
     * 基准价
     */
    private BigDecimal basePrice;

    /**
     * 单价-KG
     */

    private BigDecimal price;


}
