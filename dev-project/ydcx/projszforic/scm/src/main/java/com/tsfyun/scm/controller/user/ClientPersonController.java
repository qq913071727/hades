package com.tsfyun.scm.controller.user;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.scm.dto.user.ClientPersonInfoDTO;
import com.tsfyun.scm.dto.user.client.ClientChangePhoneCodeDTO;
import com.tsfyun.scm.dto.user.client.ClientChangePhoneDTO;
import com.tsfyun.scm.service.user.IPersonService;
import com.tsfyun.scm.vo.user.ClientPersonInfoVO;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/client/person")
@Api(tags = "人员（客户端）")
public class ClientPersonController extends BaseController {

    @Autowired
    private IPersonService personService;

    @ApiOperation("用户上传头像")
    @PostMapping(value = "/uploadHead",produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<LoginVO> uploadHead(@ApiParam(value = "头像文件") @RequestPart(name = "file",required = true) MultipartFile file){
        personService.uploadHead(file);
        return success(SecurityUtil.getCurrent());
    }

    @ApiOperation("登录人员基本信息")
    @GetMapping(value = "/info")
    public Result<ClientPersonInfoVO> info(){
        return success(personService.loginPersonInfo());
    }

    @ApiOperation("更新客户端缓存数据")
    @GetMapping(value = "/refresh")
    public Result<LoginVO> refresh(){
        return success(SecurityUtil.getCurrent());
    }

    @ApiOperation("更新登录人员基本信息")
    @PostMapping("update")
    public Result<Void> update(@RequestBody @Validated ClientPersonInfoDTO dto){
        personService.updateLoginPersonInfo(dto);
        return success();
    }

    @ApiOperation("修改手机号码（发送解绑手机验证码）")
    @PostMapping("changePhoneSendCode")
    public Result<Void> changePhoneSendCode(){
        personService.changePhoneSendCode();
        return success();
    }

    @ApiOperation("修改手机号码（解绑验证短信验证码）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "validateCode", value = "验证码", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("changePhoneValidate")
    public Result<Void> changePhoneValidate(@RequestParam(value = "validateCode",required = true)String validateCode){
        personService.changePhoneValidate(validateCode);
        return success();
    }

    @ApiOperation("修改手机号码（绑定发送验证码）")
    @PostMapping("changePhoneBindSendCode")
    public Result<Void> changePhoneBindSendCode(@ModelAttribute @Validated ClientChangePhoneCodeDTO dto){
        personService.changePhoneBindSendCode(dto);
        return success();
    }

    @ApiOperation("修改手机号码（绑定验证短信验证码）")
    @PostMapping("changePhone")
    public Result<Void> changePhone(@ModelAttribute @Validated ClientChangePhoneDTO dto){
        personService.changePhone(dto);
        return success();
    }

    @ApiOperation("修改邮箱")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mail", value = "邮箱", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("changeMail")
    public Result<Void> changeMail(@RequestParam(value = "mail")String mail){
        personService.changeMail(mail);
        return success();
    }

    @ApiOperation("修改性别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gender", value = "性别", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("changeGender")
    public Result<Void> changeGender(@RequestParam(value = "gender")String gender){
        personService.changeGender(gender);
        return success();
    }

    @ApiOperation("修改昵称")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nick", value = "昵称", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("changeNick")
    public Result<LoginVO> changeNick(@RequestParam(value = "nick")String nick){
        personService.changeNick(nick);
        return success(SecurityUtil.getCurrent());
    }

}
