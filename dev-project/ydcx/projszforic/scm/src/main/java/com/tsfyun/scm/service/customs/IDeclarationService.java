package com.tsfyun.scm.service.customs;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.customs.ConfirmDeclarationDTO;
import com.tsfyun.scm.dto.customs.DeclarationDTO;
import com.tsfyun.scm.dto.customs.DeclarationQTO;
import com.tsfyun.scm.dto.customs.WaybillBindOrderUpdateDecDTO;
import com.tsfyun.scm.entity.customs.Declaration;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.customs.LogSingleWindow;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.vo.customs.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 报关单 服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IDeclarationService extends IService<Declaration> {

    /**=
     * 根据进口订单生成报关单
     * @param order
     * @return
     */
    Declaration createByImpOrder(ImpOrder order);
    /**=
     * 根据出口订单生成报关单
     * @param order
     * @return
     */
    Declaration createByExpOrder(ExpOrder order);

    /**=
     * 根据单号查询
     * @param docNo
     * @return
     */
    Declaration findByDocNo(String docNo);

    /**
     * 根据订单ID查下报关单
     * @param orderId
     * @return
     */
    Declaration findByOrderId(Long orderId);

    /**
     * 进口报关单分页列表
     * @param qto
     * @return
     */
    PageInfo<DeclarationListVO> pageList(DeclarationQTO qto);

    /**=
     * 发送单一窗口，用户选择待发送列表
     * @param ids
     * @return
     */
    List<DeclarationListVO> waitSendList(List<String> ids);

    /**
     * 确认报关单（修改报关单）
     * @param dto
     */
    void confirm(ConfirmDeclarationDTO dto);

    /**
     * 退回验货
     * @param id
     * @param info
     */
    void backInspection(Long id,String info);

    /**
     * 报关单明细信息(支持操作页面获取详情数据状态验证)
     * @param id
     * @return
     */
    DeclarationPlusDetailVO detail(Long id,String operation);

    /**=
     * 报关单明细
     * @param id
     * @return
     */
    List<DeclarationMemberVO> memberDetail(Long id);

    /**=
     * 报关单主单详情
     * @param id
     * @param operation
     * @return
     */
    DeclarationVO mainDetail(Long id, String operation);

    /**
     * 确认申报
     * @param dto
     */
    void confirmDeclare(TaskDTO dto);

    /**=
     * 申报完成
     * @param dto
     */
    void declarationCompleted(TaskDTO dto);

    /**=
     * 构建合同数据
     * @param id
     * @return
     */
    ContractPlusVO contractData(Long id);

    /**
     * 根据运输单绑定订单
     * @param dto
     */
    int updateByWaybill(WaybillBindOrderUpdateDecDTO dto);

    /**
     * 根据订单获取报关单
     * @param orderId
     * @return
     */
    Declaration getByOrderId(Long orderId);


    /**=
     * 导入报文到单一窗口
     * @param id
     */
    void importSinglewindow(Long id);
    void importSinglewindow(Declaration declaration);

    /**
     * 根据报关单id重置运输单的数据
     * @param id
     */
    void resetByCrossBorderWaybill(Long id);

    /**
     * 报关生成公路舱单-进出口通用
     * @param id
     */
    void generateManifest(Long id);

    /**
     * 删除公路舱单-进出口通用
     * @param id
     */
    void removeManifest(Long id);

    /**
     * 根据舱单id获取舱单数据
     * @param manifestId
     * @return
     */
    Declaration findManifestById(Long manifestId);

    /**
     * 更新舱单状态
     * @param manifestId
     * @param manifestStatusId
     */
    void updateManifestReceiptData(Long manifestId,String manifestStatusId,String manifestSwStatus);

    /**
     * 推送至报关行（不走openApi，适应于内部进出口发送到本系统代理报关模块）
     * @param id
     */
    void sendToScmbot(Long id);

    /**
     * 导入报关单号
     * @param file
     */
    void importExcel(MultipartFile file);

    /**
     * 发送至报关行
     * @param id
     */
    void sendToScmbotDeclare(Long id);

    /**
     * 导出报关单那草单
     * @param id
     * @return
     */
    Map<String,Object> exportDraft(Long id,List<String> notShowFields);

    /**=
     * 构建境外合同数据
     * @param id
     * @return
     */
    ContractPlusVO overseasContractData(Long id);

    /**=
     * 申报完成
     * @param docNo
     */
    void declarationCompletedCallBack(String docNo);

}
