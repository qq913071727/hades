package com.tsfyun.scm.dto.finance;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class ConfirmTaxCodeDTO implements Serializable {

    @NotNull(message = "发票ID不能为空")
    private Long id;

    @NotNull(message = "发票明细不存在")
    @Size(min = 1,message = "发票明细不存在")
    @Valid
    private List<TaxCodeDTO> members;
}
