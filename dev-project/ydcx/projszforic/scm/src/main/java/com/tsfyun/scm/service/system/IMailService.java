package com.tsfyun.scm.service.system;

import com.tsfyun.common.base.enums.MailContentTypeEnum;
import com.tsfyun.common.base.enums.MailTemplateTypeEnum;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.support.SendMailDTO;
import com.tsfyun.scm.entity.system.SendMail;
import org.springframework.lang.NonNull;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/16 14:08
 */
public interface IMailService extends IService<SendMail> {

    /**
     * 发送简单邮件，邮件内容直接传入（不通过消息队列发送，直接异步发送）发送邮件比较费时间，建议走MQ
     * @param receivers 收件人
     * @param title     主题
     * @param content   内容
     * @param mailContentTypeEnum  邮件内容标识
     * @return 返回boolean值由业务方决定是否需要抛出异常，true表示发送成功,false表示发送失败
     */
    boolean sendSimple(List<String> receivers, String title, String content, @NonNull MailContentTypeEnum mailContentTypeEnum);

    /**
     * 发送简单邮件，根据模板（不通过消息队列发送，直接异步发送）发送邮件比较费时间，建议走MQ
     * @param receivers 收件人
     * @param title     主题
     * @param mailContentTypeEnum  邮件内容标识
     * @param mailTemplateTypeEnum 邮件模板标识（不需要模板渲染不要传）
     * @param data  模板内容
     * @return 返回boolean值由业务方决定是否需要抛出异常，true表示发送成功,false表示发送失败
     */
    boolean sendSimpleByTemplate(List<String> receivers, String title, @NonNull MailContentTypeEnum mailContentTypeEnum, @NonNull MailTemplateTypeEnum mailTemplateTypeEnum, Map<String,Object> data);

    /**
     * 发送简单邮件，邮件内容直接传入（通过MQ投递消息发送邮件）发送邮件比较费时间，建议走MQ
     * @param receivers
     * @param title
     * @param content
     * @param mailContentTypeEnum
     * @param removeFileFlag
     */
    void sendSimpleWithMQ(List<String> receivers, String title, String content, @NonNull MailContentTypeEnum mailContentTypeEnum,List<Long> fileIds,Boolean removeFileFlag);

    /**
     * 发送简单邮件，邮件内容根据模板（通过消息队列发送，比支持发送附件）发送邮件比较费时间，建议走MQ
     * @param receivers 收件人
     * @param title     主题
     * @param mailContentTypeEnum  邮件内容标识
     * @return
     */
    void sendSimpleWithMQByTemplate(List<String> receivers, String title, @NonNull MailContentTypeEnum mailContentTypeEnum, @NonNull MailTemplateTypeEnum mailTemplateTypeEnum,Map<String,Object> data);

    /**
     * 发送带附件邮件，邮件内容根据模板（通过消息队列发送，比支持发送附件）发送邮件比较费时间，建议走MQ
     * @param receivers 收件人
     * @param title     主题
     * @param mailContentTypeEnum  邮件内容标识
     * @return
     */
    void sendSimpleWithMQByTemplate(List<String> receivers, String title, @NonNull MailContentTypeEnum mailContentTypeEnum, @NonNull MailTemplateTypeEnum mailTemplateTypeEnum,Map<String,Object> data,List<Long> fileIds,Boolean removeFileFlag);


    /**
     * 消费邮件消息
     * @param dto
     */
    void consumeMailByMQ(SendMailDTO dto);

    /**
     * 发给多个收件人，带附件，内容直接传入（不通过消息队列发送，因为消息队列无法接收到文件，即使传递url地址过去也存在不确定性）
     * @param receivers 收件人
     * @param title   主题
     * @param content   内容
     * @param mailContentTypeEnum 邮件内容标识
     * @param attaches 附件
     * @return 返回boolean值由业务方决定是否需要抛出异常，true表示发送成功,false表示发送失败
     * @throws Exception
     */
    boolean sendAttach(List<String> receivers, String title, String content, @NonNull MailContentTypeEnum mailContentTypeEnum,File... attaches);


    /**
     * 发给多个收件人，带附件，内容直接传入（不通过消息队列发送，因为消息队列无法接收到文件，即使传递url地址过去也存在不确定性）
     * @param receivers 收件人
     * @param title   主题
     * @param mailContentTypeEnum 邮件内容标识
     * @param mailTemplateTypeEnum 邮件模板标识
     * @param data 模板内容数据
     * @param attaches 附件
     * @return 返回boolean值由业务方决定是否需要抛出异常，true表示发送成功,false表示发送失败
     * @throws Exception
     */
    boolean sendAttachByTemplate(List<String> receivers, String title, @NonNull MailContentTypeEnum mailContentTypeEnum,MailTemplateTypeEnum mailTemplateTypeEnum,Map<String,Object> data,File... attaches);



}
