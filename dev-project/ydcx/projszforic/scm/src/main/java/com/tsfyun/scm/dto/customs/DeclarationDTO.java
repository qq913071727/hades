package com.tsfyun.scm.dto.customs;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 报关单请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="Declaration请求对象", description="报关单请求实体")
public class DeclarationDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "报关单id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "申报单位组织机构代码不能为空")
   @LengthTrim(max = 20,message = "申报单位组织机构代码最大长度不能超过20位")
   @ApiModelProperty(value = "申报单位组织机构代码")
   private String agentScc;

   @NotEmptyTrim(message = "申报单位海关编号不能为空")
   @LengthTrim(max = 10,message = "申报单位海关编号最大长度不能超过10位")
   @ApiModelProperty(value = "申报单位海关编号")
   private String agentCode;

   @NotEmptyTrim(message = "申报单位名称不能为空")
   @LengthTrim(max = 100,message = "申报单位名称最大长度不能超过100位")
   @ApiModelProperty(value = "申报单位名称")
   private String agentName;

   @LengthTrim(max = 20,message = "提运单号最大长度不能超过20位")
   @ApiModelProperty(value = "提运单号")
   private String billNo;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 10,message = "单据类型最大长度不能超过10位")
   @ApiModelProperty(value = "单据类型-进出口")
   private String billType;

   @NotNull(message = "订单号不能为空")
   @ApiModelProperty(value = "订单号")
   private Long orderId;

   @NotEmptyTrim(message = "订单编号不能为空")
   @LengthTrim(max = 20,message = "订单编号最大长度不能超过20位")
   @ApiModelProperty(value = "订单编号")
   private String orderDocNo;

   @LengthTrim(max = 32,message = "航次号最大长度不能超过32位")
   @ApiModelProperty(value = "航次号-六联单号")
   private String cusVoyageNo;

   @LengthTrim(max = 50,message = "集装箱号最大长度不能超过50位")
   @ApiModelProperty(value = "集装箱号")
   private String containerNo;

   @LengthTrim(max = 20,message = "合同号最大长度不能超过20位")
   @ApiModelProperty(value = "合同号")
   private String contrNo;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @NotNull(message = " 申报日期不能为空")
   @ApiModelProperty(value = " 申报日期")
   private LocalDateTime decDate;

   @LengthTrim(max = 50,message = "报关单号最大长度不能超过50位")
   @ApiModelProperty(value = "报关单号")
   private String declarationNo;

   @NotEmptyTrim(message = "状态不能为空")
   @LengthTrim(max = 20,message = "状态最大长度不能超过20位")
   @ApiModelProperty(value = "状态")
   private String statusId;

   @LengthTrim(max = 10,message = "申报地海关最大长度不能超过10位")
   @ApiModelProperty(value = "申报地海关")
   private String customMasterCode;

   @LengthTrim(max = 20,message = "申报地海关最大长度不能超过20位")
   @ApiModelProperty(value = "申报地海关")
   private String customMasterName;

   @LengthTrim(max = 200,message = "货物存放地最大长度不能超过200位")
   @ApiModelProperty(value = "货物存放地")
   private String goodsPlace;

   @LengthTrim(max = 10,message = "启运港最大长度不能超过10位")
   @ApiModelProperty(value = "启运港")
   private String destPortCode;

   @LengthTrim(max = 20,message = "启运港最大长度不能超过20位")
   @ApiModelProperty(value = "启运港")
   private String destPortName;

   @LengthTrim(max = 10,message = "境内目的地最大长度不能超过10位")
   @ApiModelProperty(value = "境内目的地")
   private String districtCode;

   @LengthTrim(max = 20,message = "境内目的地最大长度不能超过20位")
   @ApiModelProperty(value = "境内目的地")
   private String districtName;

   @NotEmptyTrim(message = "系统单号不能为空")
   @LengthTrim(max = 20,message = "系统单号最大长度不能超过20位")
   @ApiModelProperty(value = "系统单号")
   private String docNo;

   @LengthTrim(max = 10,message = "入境口岸最大长度不能超过10位")
   @ApiModelProperty(value = "入境口岸")
   private String ciqEntyPortCode;

   @LengthTrim(max = 20,message = "入境口岸最大长度不能超过20位")
   @ApiModelProperty(value = "入境口岸")
   private String ciqEntyPortName;

   @LengthTrim(max = 10,message = "进境关别最大长度不能超过10位")
   @ApiModelProperty(value = "进境关别")
   private String iePortCode;

   @LengthTrim(max = 20,message = "进境关别最大长度不能超过20位")
   @ApiModelProperty(value = "进境关别")
   private String iePortName;

   @NotNull(message = "进出口日期不能为空")
   @ApiModelProperty(value = "进出口日期")
   private LocalDateTime importDate;

   @LengthTrim(max = 30,message = "保费最大长度不能超过30位")
   @ApiModelProperty(value = "保费")
   private String insuranceCosts;

   @LengthTrim(max = 50,message = "许可证号最大长度不能超过50位")
   @ApiModelProperty(value = "许可证号")
   private String licenseNo;

   @LengthTrim(max = 50,message = "车牌最大长度不能超过50位")
   @ApiModelProperty(value = "车牌")
   private String licensePlate;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @LengthTrim(max = 30,message = "杂费最大长度不能超过30位")
   @ApiModelProperty(value = "杂费")
   private String miscCosts;

   @LengthTrim(max = 10,message = "包装种类最大长度不能超过10位")
   @ApiModelProperty(value = "包装种类")
   private String wrapTypeCode;

   @LengthTrim(max = 20,message = "包装种类最大长度不能超过20位")
   @ApiModelProperty(value = "包装种类")
   private String wrapTypeName;

   @LengthTrim(max = 50,message = "备案号最大长度不能超过50位")
   @ApiModelProperty(value = "备案号")
   private String recordNo;

   @LengthTrim(max = 20,message = "统一编号最大长度不能超过20位")
   @ApiModelProperty(value = "统一编号")
   private String cusCiqNo;

   @LengthTrim(max = 400,message = "标记唛码最大长度不能超过400位")
   @ApiModelProperty(value = "标记唛码")
   private String markNo;

   @LengthTrim(max = 10,message = "经停港最大长度不能超过10位")
   @ApiModelProperty(value = "经停港")
   private String distinatePortCode;

   @LengthTrim(max = 20,message = "经停港最大长度不能超过20位")
   @ApiModelProperty(value = "经停港")
   private String distinatePortName;

   @LengthTrim(max = 10,message = "征免性质最大长度不能超过10位")
   @ApiModelProperty(value = "征免性质")
   private String cutModeCode;

   @LengthTrim(max = 20,message = "征免性质最大长度不能超过20位")
   @ApiModelProperty(value = "征免性质")
   private String cutModeName;

   @NotNull(message = "件数不能为空")
   @LengthTrim(max = 11,message = "件数最大长度不能超过11位")
   @ApiModelProperty(value = "件数")
   private Integer packNo;

   @NotNull(message = "净重(KG)不能为空")
   @Digits(integer = 17, fraction = 2, message = "净重(KG)整数位不能超过17位，小数位不能超过2")
   @ApiModelProperty(value = "净重(KG)")
   private BigDecimal netWt;

   @NotNull(message = "毛重(KG)不能为空")
   @Digits(integer = 17, fraction = 2, message = "毛重(KG)整数位不能超过17位，小数位不能超过2")
   @ApiModelProperty(value = "毛重(KG)")
   private BigDecimal grossWt;

   @LengthTrim(max = 10,message = "贸易国别(地区)	最大长度不能超过10位")
   @ApiModelProperty(value = "贸易国别(地区)	")
   private String cusTradeNationCode;

   @LengthTrim(max = 20,message = "贸易国别(地区)	最大长度不能超过20位")
   @ApiModelProperty(value = "贸易国别(地区)	")
   private String cusTradeNationName;

   @LengthTrim(max = 30,message = "运费最大长度不能超过30位")
   @ApiModelProperty(value = "运费")
   private String transCosts;

   @LengthTrim(max = 10,message = "启运国(地区)最大长度不能超过10位")
   @ApiModelProperty(value = "启运国(地区)")
   private String cusTradeCountryCode;

   @LengthTrim(max = 20,message = "启运国(地区)最大长度不能超过20位")
   @ApiModelProperty(value = "启运国(地区)")
   private String cusTradeCountryName;

   @LengthTrim(max = 10,message = "运输方式最大长度不能超过10位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafModeCode;

   @LengthTrim(max = 20,message = "运输方式最大长度不能超过20位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafModeName;

   @LengthTrim(max = 10,message = "币制最大长度不能超过10位")
   @ApiModelProperty(value = "币制")
   private String currencyId;

   @LengthTrim(max = 10,message = "币制最大长度不能超过10位")
   @ApiModelProperty(value = "币制")
   private String currencyName;

   @NotNull(message = "委托金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "委托金额整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "委托金额")
   private BigDecimal totalPrice;

   @NotNull(message = "境内收发货人不能为空")
   @ApiModelProperty(value = "境内收发货人")
   private Long subjectId;

   @NotNull(message = "境外收发货人不能为空")
   @ApiModelProperty(value = "境外收发货人")
   private Long subjectOverseasId;

   @LengthTrim(max = 10,message = "监管方式最大长度不能超过10位")
   @ApiModelProperty(value = "监管方式")
   private String supvModeCdde;

   @LengthTrim(max = 20,message = "监管方式最大长度不能超过20位")
   @ApiModelProperty(value = "监管方式")
   private String supvModeCddeName;

   @NotEmptyTrim(message = "成交方式不能为空")
   @LengthTrim(max = 20,message = "成交方式最大长度不能超过20位")
   @ApiModelProperty(value = "成交方式")
   private String transactionMode;


}
