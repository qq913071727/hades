package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/10 14:45
 */
@Data
public class OverseasDeliveryNoteBindQTO extends PaginationDto {

    private Long id;

    /**
     * 有无库存
     */
    private Boolean isStock;
    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 型号
     */
    private String model;

    /**
     * 名称
     */
    private String name;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 产品
     */
    private String product;

    /**
     * 订单开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDateStart;

    /**
     * 订单结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDateEnd;

    public void setOrderDateEnd(LocalDateTime orderDateEnd) {
        if(orderDateEnd != null){
            this.orderDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(orderDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
