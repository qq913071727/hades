package com.tsfyun.scm.dto.finance;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ConfirmBankDTO implements Serializable {

    @NotNull(message = "付款单id不能为空")
    private Long id;
    @NotNull(message = "付款方不能为空")
    private Long payerId;
    @NotNull(message = "付款银行不能为空")
    private Long payerBankId;
}
