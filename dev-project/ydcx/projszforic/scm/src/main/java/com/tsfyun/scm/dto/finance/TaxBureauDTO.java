package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.LengthTrim;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description: 税局退税数据录入
 * @CreateDate: Created in 2021/11/2 11:47
 */
@Data
public class TaxBureauDTO implements Serializable {

    @NotNull(message = "id不能为空")
    private Long id;

    @NotNull(message = "请选择是否函调")
    private Boolean isCorrespondence;
    // 是否回函
    private String replyStatus;
    // 回函日期
    private LocalDateTime replyDate;
    // 退税申报日期
    private LocalDateTime taxDeclareTime;
    /**
     * 退税申报金额
     */
    @Digits(integer = 17, fraction = 2, message = "退税申报金额整数位不能超过17位，小数位不能超过2位")
    @DecimalMin(value = "0.01",message = "退税申报金额不能小于0")
    private BigDecimal taxDeclareAmount;

    // 税局退税日期
    private LocalDateTime taxRefundTime;

    /**
     * 税局退税金额
     */
    @Digits(integer = 17, fraction = 2, message = "税局退税金额整数位不能超过17位，小数位不能超过2位")
    @DecimalMin(value = "0.01",message = "税局退税金额不能小于0")
    private BigDecimal taxRefundAmount;

    /**
     * 税局退税备注
     */
    @LengthTrim(message = "税局退税备注长度不能超过255")
    private String taxRefundRemark;

}
