package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 付款单
 * </p>
 *

 * @since 2020-04-23
 */
@Data
public class PaymentAccount {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    private Long id;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 付款提交时间
     */

    private LocalDateTime accountDate;

    /**
     * 实际付汇时间
     */
    private LocalDateTime realAccountDate;

    /**
     * 付款人
     */

    private String payer;

    /**
     * 付款银行名称
     */

    private String payerBankName;

    /**
     * 付款银行账号
     */

    private String payerBankAccountNo;

    /**
     * 收款人
     */

    private Long payeeId;

    /**
     * 收款人
     */

    private String payee;

    /**
     * 收款行
     */

    private Long payeeBankId;

    /**
     * 收款行名称
     */

    private String payeeBankName;

    /**
     * 收款行账号
     */

    private String payeeBankAccountNo;

    /**
     * 收款行地址
     */

    private String payeeBankAddress;

    /**
     * swift code
     */

    private String swiftCode;

    /**
     * bank code
     */

    private String ibankCode;

    /**
     * 付款方式
     */

    private String paymentTerm;

    /**
     * 付款方式天数
     */

    private Integer paymentTermDay;

    /**
     * 到期日期
     */

    private LocalDateTime dueDate;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 结算汇率
     */

    private BigDecimal exchangeRate;

    /**
     * 调整前汇率
     */

    private BigDecimal oldExchangeRate;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 币制
     */

    private String currencyId;

    /**
     * 币制名称
     */

    private String currencyName;

    /**
     * 付款金额
     */

    private BigDecimal accountValue;

    /**
     * 付款人民币金额
     */

    private BigDecimal accountValueCny;

    /**
     * 手续费承担方式
     */

    private String counterFeeType;

    /**
     * 付款手续费金额
     */

    private BigDecimal bankFee;

    /**
     * 实际付款金额
     */

    private BigDecimal actualAccountValue;

    /**
     * 实际付款币制
     */

    private String actualCurrencyId;

    /**
     * 实际付款币制
     */

    private String actualCurrencyName;

    /**
     * 是否虚拟付汇
     */

    private Boolean fictitious;

    /**
     * 备注
     */

    private String memo;

    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;


}
