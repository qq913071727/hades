package com.tsfyun.scm.vo.wms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description: 出库单详情
 * @since Created in 2020/4/24 14:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryNotePlusDetailVO {

    /**
     * 出库单主单
     */
    private DeliveryNoteVO deliveryNote;

    /**
     * 出库单明细
     */
    private List<DeliveryNoteMemberVO> members;


}
