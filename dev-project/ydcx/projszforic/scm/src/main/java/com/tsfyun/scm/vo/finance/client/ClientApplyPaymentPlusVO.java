package com.tsfyun.scm.vo.finance.client;

import com.tsfyun.scm.vo.order.ApplyPaymentVO;
import com.tsfyun.scm.vo.order.ImpOrderPaySituationVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description: 客户端单个订单付汇申请
 * @CreateDate: Created in 2020/11/5 09:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientApplyPaymentPlusVO implements Serializable {

    private ApplyPaymentVO payment;

    private ImpOrderPaySituationVO order;

}
