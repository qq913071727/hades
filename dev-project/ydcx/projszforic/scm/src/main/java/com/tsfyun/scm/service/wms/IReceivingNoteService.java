package com.tsfyun.scm.service.wms;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.wms.ImpReceivingNoteDTO;
import com.tsfyun.scm.dto.wms.InspectionDTO;
import com.tsfyun.scm.dto.wms.ReceivingNoteQTO;
import com.tsfyun.scm.dto.wms.client.ClientReceivingNoteQTO;
import com.tsfyun.scm.entity.wms.ReceivingNote;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.storage.SimpleReceivingNoteCartonVO;
import com.tsfyun.scm.vo.wms.*;
import com.tsfyun.scm.vo.wms.client.ClientReceivingNoteVO;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * <p>
 * 入库单 服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IReceivingNoteService extends IService<ReceivingNote> {

    /**
     * 进口入库单分页列表
     * @param qto
     * @return
     */
    PageInfo<ReceivingNoteListVO> pageList(ReceivingNoteQTO qto);

    /**=
     * 更新入库单已绑定的订单号
     * @param docNo
     */
    void updateReceivingNoteOrderNo(String docNo);
    void updateReceivingNoteOrderNo(ReceivingNote receivingNote);


    /**
     * 入库单详情（含明细）
     * @param id
     * @param operation
     * @return
     */
    ReceivingNotePlusDetailVO detail(Long id,@Nullable String operation);

    /**=
     * 入库主单详情
     * @param id
     * @param operation
     * @return
     */
    ReceivingNoteVO mainDetail(Long id,@Nullable String operation);

    /**=
     * 入库单验货详情
     * @param id
     * @return
     */
    InspectionNoteDetailVO inspectionDetail(Long id);

    /**=
     * 进口入库登记
     * @param dto
     * @return
     */
    Long impRegister(ImpReceivingNoteDTO dto);

    /**=
     * 保存验货制单
     * @param dto
     */
    void saveInspection(InspectionDTO dto);


    /**
     * 根据入库单号模糊搜索和客户搜索入库单号信息
     * @param receivingNoteNo
     * @return
     */
    List<SimpleReceivingNoteCartonVO> selectByReceivingNoteNo(String receivingNoteNo, Long customerId);

    /**=
     * 根据入库单号查询订单
     * @param docNo
     * @return
     */
    ReceivingNote findByDocNo(String docNo);

    /**
     * 客户端获取入库列表
     * @param qto
     * @return
     */
    PageInfo<ClientReceivingNoteVO> clientList(ClientReceivingNoteQTO qto);
}
