package com.tsfyun.scm.entity.wms;

import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 出库单
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
public class DeliveryNote extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 单号
     */

    private String docNo;

    /**
     * 单据类型-枚举
     */

    private String billType;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 入库时间
     */

    private LocalDateTime deliveryDate;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 仓库
     */

    private Long warehouseId;

    /**
     * 备注
     */

    private String memo;

    /**
     * 制单人
     */
    private String documentMaker;

    /**
     * 是否境外
     */
    private Boolean isAbroad;

    /**
     * 订单号
     */
    private String orderNo;
}
