package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.logistics.DeliveryWaybill;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.logistics.DeliveryWaybillVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 送货单 Mapper 接口
 * </p>
 *

 * @since 2020-05-25
 */
@Repository
public interface DeliveryWaybillMapper extends Mapper<DeliveryWaybill> {

    @DataScope(tableAlias = "a",customerTableAlias = "c",addCustomerNameQuery = true)
    List<DeliveryWaybillVO> list(Map<String,Object> params);

    DeliveryWaybillVO detail(@Param(value="id")Long id);

}
