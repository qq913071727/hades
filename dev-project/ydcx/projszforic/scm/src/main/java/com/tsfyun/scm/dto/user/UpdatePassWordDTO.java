package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import java.io.Serializable;

/**
 * 修改密码
 */
@Data
public class UpdatePassWordDTO implements Serializable {

    @NotEmptyTrim(message = "原密码不能为空",groups = UpdateGroup.class)
    private String passWord;

    @NotEmptyTrim(message = "新密码不能为空")
    @LengthTrim(min = 6,max = 20,message = "新密码长度只能为6-20位")
    private String newPassWord;

    /**
     * 修改密码的员工id
     */
    private Long personId;

}
