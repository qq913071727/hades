package com.tsfyun.scm.service.impl.customs;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.customs.DeclarationBrokerDTO;
import com.tsfyun.scm.dto.customs.DeclarationBrokerQTO;
import com.tsfyun.scm.entity.customs.DeclarationBroker;
import com.tsfyun.scm.mapper.customs.DeclarationBrokerMapper;
import com.tsfyun.scm.service.customs.IDeclarationBrokerService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.customs.DeclarationBrokerVO;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.WeekendSqls;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 报关行 服务实现类
 * </p>
 *
 *
 * @since 2021-11-15
 */
@RequiredArgsConstructor
@Service
public class DeclarationBrokerServiceImpl extends ServiceImpl<DeclarationBroker> implements IDeclarationBrokerService {

    private final DeclarationBrokerMapper declarationBrokerMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(DeclarationBrokerDTO dto) {
        DeclarationBroker declarationBroker = new DeclarationBroker();
        declarationBroker.setName(dto.getName());
        declarationBroker.setCustomsNo(dto.getCustomsNo());
        declarationBroker.setSocialNo(dto.getSocialNo());
        declarationBroker.setMemo(dto.getMemo());
        declarationBroker.setDisabled(Boolean.FALSE);
        try {
            super.saveNonNull(declarationBroker);
        } catch (DuplicateKeyException e) {
            if(StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().contains("UK_declaration_broker_name")) {
                throw new ServiceException("该名称已经存在");
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(DeclarationBrokerDTO dto) {
        DeclarationBroker declarationBroker = super.getById(dto.getId());
        declarationBroker.setName(dto.getName());
        declarationBroker.setCustomsNo(dto.getCustomsNo());
        declarationBroker.setSocialNo(dto.getSocialNo());
        declarationBroker.setMemo(dto.getMemo());
        try {
            super.updateById(declarationBroker);
        } catch (DuplicateKeyException e) {
            if(StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().contains("UK_declaration_broker_name")) {
                throw new ServiceException("该名称已经存在");
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long id) {
        DeclarationBroker declarationBroker = super.getById(id);
        Optional.ofNullable(declarationBroker).orElseThrow(()->new ServiceException("数据不存在"));
        super.removeById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDisabled(Long id,Boolean disabled) {
        DeclarationBroker declarationBroker = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(declarationBroker),new ServiceException("数据不存在"));
        DeclarationBroker update = new DeclarationBroker();
        update.setDisabled(disabled);
        declarationBrokerMapper.updateByExampleSelective(update, Example.builder(DeclarationBroker.class).where(
                WeekendSqls.<DeclarationBroker>custom().andEqualTo(DeclarationBroker::getId,id)
        ).build());
    }

    @Override
    public DeclarationBrokerVO detail(Long id) {
        DeclarationBroker declarationBroker = super.getById(id);
        Optional.ofNullable(declarationBroker).orElseThrow(()->new ServiceException("数据不存在"));
        return beanMapper.map(declarationBroker,DeclarationBrokerVO.class);
    }

    @Override
    public PageInfo<DeclarationBroker> pageList(DeclarationBrokerQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        Example example = Example.builder(DeclarationBroker.class).where(TsfWeekendSqls.<DeclarationBroker>custom()
                .andLike(true,DeclarationBroker::getName,qto.getName())).build( );
        example.setOrderByClause("name asc");
        List<DeclarationBroker> list = declarationBrokerMapper.selectByExample(example);
        return new PageInfo<>(list);
    }

    @Override
    public List<DeclarationBrokerVO> select(String name) {
        Example example = Example.builder(DeclarationBroker.class).where(TsfWeekendSqls.<DeclarationBroker>custom()
                .andLike(true,DeclarationBroker::getName,name)
                .andEqualTo(false,DeclarationBroker::getDisabled,Boolean.FALSE)).build( );
        example.selectProperties("name","socialNo","customsNo");
        example.setOrderByClause("name asc");
        List<DeclarationBroker> list = declarationBrokerMapper.selectByExample(example);
        return beanMapper.mapAsList(list,DeclarationBrokerVO.class);
    }
}
