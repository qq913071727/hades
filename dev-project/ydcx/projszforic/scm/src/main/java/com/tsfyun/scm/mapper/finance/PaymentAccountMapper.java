package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.finance.ClientPaymentApplyQTO;
import com.tsfyun.scm.entity.finance.PaymentAccount;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.PaymentAccountVO;
import com.tsfyun.scm.vo.finance.client.ClientPaymentListVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 付款单 Mapper 接口
 * </p>
 *

 * @since 2020-04-23
 */
@Repository
public interface PaymentAccountMapper extends Mapper<PaymentAccount> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<PaymentAccountVO> list(Map<String,Object> params);

    /**
     * 风控审批修改付汇单数据
     * @param updatePaymentAccount
     */
    void updateForRisk(PaymentAccount updatePaymentAccount);

    /**
     * 根据订单和状态获取对应的付汇金额
     * @param orderId
     * @param statusId
     * @return
     */
    Map<String,Object> getPaymentAmount(@Param(value = "orderId")Long orderId,@Param(value = "statusId")String statusId);

    /**
     * 客户端付汇申请列表
     * @param qto
     * @return
     */
    List<ClientPaymentListVO> clientList(ClientPaymentApplyQTO qto);

    /**
     * 根据客户和状态集合统计数量
     * @param statusIds
     * @return
     */
    Long countByCustomerAndStatusIds(@Param(value = "customerId")Long customerId,@Param(value = "statusIds") List<String> statusIds);

    /**
     * 根据进口订单获取付汇汇率
     * @param impOrderNo
     * @return
     */
    List<BigDecimal> getExchangeRateByImpOrderNo(@Param(value = "impOrderNo")String impOrderNo);



}
