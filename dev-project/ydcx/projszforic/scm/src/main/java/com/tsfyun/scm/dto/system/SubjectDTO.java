package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-03
 */
@Data
@ApiModel(value="Subject请求对象", description="请求实体")
public class SubjectDTO implements Serializable {

     private static final long serialVersionUID=1L;

    @NotEmptyTrim(message = "公司名称不能为空")
    @LengthTrim(min = 1,max = 200)
    @ApiModelProperty(value = "公司名称")
    private String name;

    @ApiModelProperty(value = "公司名称(英文)")
    @LengthTrim(min = 1,max = 255)
    private String nameEn;

    @ApiModelProperty(value = "统一社会信用代码(18位)")
    @LengthTrim(min = 18,max = 18,message = "统一社会信用代码只能为18位")
    private String socialNo;

    @ApiModelProperty(value = "税务登记证")
    @Length(min = 18,max = 18,message = "税务登记证只能为18位")
    private String taxpayerNo;

    @ApiModelProperty(value = "海关注册编码")
    @LengthTrim(min = 10,max = 10,message = "海关注册编码只能为10位")
    private String customsCode;

    @ApiModelProperty(value = "检验检疫编码")
    @LengthTrim(min = 10,max = 10,message = "检验检疫编码只能为10位")
    private String ciqNo;

    @ApiModelProperty(value = "公司法人")
    @LengthTrim(min = 2,max = 50,message = "公司法人只能为2-50位")
    private String legalPerson;

    @LengthTrim(min = 3,max = 50,message = "公司电话只能为3-50位")
    @ApiModelProperty(value = "公司电话")
    private String tel;

    @LengthTrim(min = 3,max = 50,message = "公司传真只能为3-50位")
    @ApiModelProperty(value = "公司传真")
    private String fax;

    @Email(message = "企业邮箱格式不正确")
    private String mail;

    @LengthTrim(min = 2,max = 255,message = "公司地址只能为2-255位")
    @ApiModelProperty(value = "公司联系地址")
    private String address;

    @LengthTrim(min = 2,max = 255,message = "公司地址英文只能为2-255位")
    @ApiModelProperty(value = "公司联系地址英文")
    private String addressEn;

    @LengthTrim(min = 2,max = 255,message = "公司地址英文只能为2-255位")
    @ApiModelProperty(value = "公司注册地址")
    private String regAddress;

    @ApiModelProperty(value = "电子章")
    private String chapter;

    @NotEmptyTrim(message = "浏览器标题不能为空")
    @LengthTrim(min = 1,max = 255)
    @ApiModelProperty(value = "浏览器标题")
    private String browserTitle;

    @NotEmptyTrim(message = "企业logo不能为空")
    @ApiModelProperty(value = "企业logo")
    private String logo;

    @NotEmptyTrim(message = "登录页面背景不能为空")
    @ApiModelProperty(value = "登录页面背景")
    private String loginBg;


}
