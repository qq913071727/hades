package com.tsfyun.scm.service.customs;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.scm.dto.customs.DeclarationTempDTO;
import com.tsfyun.scm.dto.customs.DeclarationTempQTO;
import com.tsfyun.scm.entity.customs.DeclarationTemp;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customs.DeclarationTempSimpleVO;
import com.tsfyun.scm.vo.customs.DeclarationTempVO;

import java.util.List;

/**
 * <p>
 * 报关单模板 服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IDeclarationTempService extends IService<DeclarationTemp> {

    /**
     * 新增
     * @param dto
     */
    void add(DeclarationTempDTO dto);

    /**
     * 修改
     * @param dto
     */
    void edit(DeclarationTempDTO dto);

    /**
     * 禁用，取消禁用
     * @param id
     * @param disabled
     */
    void updateDisabled(Long id,Boolean disabled);

    /**
     * 详情
     * @param id
     * @return
     */
    DeclarationTempVO detail(Long id);

    /**
     * 根据单据类型和模板名称获取报关模板下拉
     * @param billType
     * @param tempName
     * @return
     */
    List<DeclarationTempSimpleVO> selectByBillType(String billType,String tempName);

    /**
     * 分页查询报关模板
     * @param qto
     * @return
     */
    PageInfo<DeclarationTemp> pageList(DeclarationTempQTO qto);

    /**
     * 删除模板
     * @param id
     */
    void removeById(Long id);

    /**
     * 设置/取消默认
     * @param id
     */
    void setDefault(Long id,Boolean isDefault);

    /**=
     * 获取默认模板
     * @param billType
     * @return
     */
    DeclarationTemp obtanDefault(BillTypeEnum billType);
    List<DeclarationTempSimpleVO> obtanDefaultList(BillTypeEnum billType);
}
