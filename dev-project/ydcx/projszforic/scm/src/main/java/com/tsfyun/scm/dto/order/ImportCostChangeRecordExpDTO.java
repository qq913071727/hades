package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 导入订单费用变更记录请求实体
 * </p>
 *

 * @since 2020-05-27
 */
@Data
@ApiModel(value="ImportCostChangeRecordExpDTO请求对象", description="导入订单费用变更记录请求实体")
public class ImportCostChangeRecordExpDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long expOrderCostId;

   @ApiModelProperty(value = "订单号")
   private String expOrderNo;

   @ApiModelProperty(value = "费用科目名称")
   private String expenseSubjectId;

   @NotEmptyTrim(message = "请选择支付方式")
   private String collectionSource;

   @ApiModelProperty(value = "应收金额")
   private BigDecimal costAmount;

   @ApiModelProperty(value = "已付金额")
   private BigDecimal acceAmount;

   @ApiModelProperty(value = "应付金额")
   private BigDecimal payAmount;

   @ApiModelProperty(value = "应付币制")
   private String payCurrencyCode;

   @ApiModelProperty(value = "供应商")
   private String extend1;

   @ApiModelProperty(value = "费用日期")
   private LocalDateTime happenDate;

   @ApiModelProperty(value = "备注")
   private String memo;


}
