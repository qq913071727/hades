package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.ReceiptAccountMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.ReceiptAccountMemberVO;

import java.util.List;

/**
 * 收款单
 */
public interface IReceiptAccountMemberService extends IService<ReceiptAccountMember> {

    /**
     * 根据收款单主单id获取明细id
     * @param receiptAccountId
     * @return
     */
    List<ReceiptAccountMemberVO> getVoByReceiptAccountId(Long receiptAccountId);

    /**
     * 根据收款单主单id获取明细id
     * @param receiptAccountId
     * @return
     */
    List<ReceiptAccountMember> getByReceiptAccountId(Long receiptAccountId);

    /**=
     * 根据进口订单费用查询核销明细
     * @param costId
     * @return
     */
    List<ReceiptAccountMember> getByImpOrderCostId(Long costId);

    /**=
     * 根据出口订单费用查询核销明细
     * @param costId
     * @return
     */
    List<ReceiptAccountMember> getByExpOrderCostId(Long costId);

    /**=
     * 根据报关订单费用查询核销明细
     * @param costId
     * @return
     */
    List<ReceiptAccountMember> getByTrustOrderCostId(Long costId);

    /**
     * 根据退款单ID查询核销明细
     * @param refundAccountId
     * @return
     */
    List<ReceiptAccountMember> getByRefundAccountId(Long refundAccountId);



}
