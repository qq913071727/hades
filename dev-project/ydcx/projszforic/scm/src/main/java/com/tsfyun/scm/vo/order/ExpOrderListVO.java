package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.domain.ExpOrderStatusEnum;
import com.tsfyun.common.base.enums.exp.ExpTransactionModeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Data
public class ExpOrderListVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "订单编号")
    private String docNo;

    @ApiModelProperty(value = "客户单号")
    private String clientNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "境外收货方")
    private Long supplierId;

    private String supplierName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单日期-由后端写入")
    private Date orderDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "实际通关日期-由报关单反写")
    private Date clearanceDate;

    @ApiModelProperty(value = "生成报关单确定出口")
    private Boolean isExp;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "运输方式代码")
    private String cusTrafMode;

    @ApiModelProperty(value = "运输方式名称")
    private String cusTrafModeName;

    @ApiModelProperty(value = "成交方式")
    private String transactionMode;


    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "报关金额")
    private BigDecimal decTotalPrice;

    @ApiModelProperty(value = "销售人员id")
    private Long salePersonId;

    @ApiModelProperty(value = "采购合同")
    private String purchaseContractNo;

    /**
     * 销售人员名称
     */
    private String salePersonName;

    /**
     * 商务人员id
     */
    private Long busPersonId;

    /**
     * 商务人员名称
     */
    private String busPersonName;

    private Boolean isSettleAccount;// 是否结汇完成

    /**
     * 指运港
     */
    private String distinatePortName;

    private String declareType;

    private String statusDesc;

    private String transactionModeDesc;

    private String declareTypeDesc;

    public String getStatusDesc() {
        ExpOrderStatusEnum expOrderStatusEnum = ExpOrderStatusEnum.of(statusId);
        return Optional.ofNullable(expOrderStatusEnum).map(ExpOrderStatusEnum::getName).orElse("");
    }


    public String getTransactionModeDesc(){
        ExpTransactionModeEnum expTransactionModeEnum = ExpTransactionModeEnum.of(transactionMode);
        return Optional.ofNullable(expTransactionModeEnum).map(ExpTransactionModeEnum::getName).orElse("");
    }

    public String getDeclareTypeDesc(){
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(declareType);
        return Optional.ofNullable(declareTypeEnum).map(DeclareTypeEnum::getName).orElse("");
    }

}
