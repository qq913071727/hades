package com.tsfyun.scm.controller.materiel;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.materiel.MaterielLogDTO;
import com.tsfyun.scm.dto.materiel.MaterielLogQTO;
import com.tsfyun.scm.dto.materiel.MaterielQTO;
import com.tsfyun.scm.service.materiel.IMaterielLogService;
import com.tsfyun.scm.vo.materiel.MaterielHSVO;
import com.tsfyun.scm.vo.materiel.MaterielLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 归类变更日志 前端控制器
 * </p>
 *
 *
 * @since 2020-03-26
 */
@RestController
@RequestMapping("/materielLog")
public class MaterielLogController extends BaseController {

    @Autowired
    private IMaterielLogService materielLogService;

    /**=
     * 查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<MaterielLogVO>> list(@ModelAttribute @Valid MaterielLogQTO qto){
        return materielLogService.pageList(qto);
    }
}

