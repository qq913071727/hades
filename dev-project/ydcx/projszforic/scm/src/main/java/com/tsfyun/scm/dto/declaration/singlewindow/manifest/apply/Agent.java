package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 运输工具代理企业数据
 * @since Created in 2020/4/22 11:13
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agent", propOrder = {
        "id",
})
public class Agent {

    //运输工具代理企业代码
    @XmlElement(name = "ID")
    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
