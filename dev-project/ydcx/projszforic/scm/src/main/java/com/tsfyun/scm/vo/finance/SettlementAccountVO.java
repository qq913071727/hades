package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.SettlementAccountStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * <p>
 * 结汇主单响应实体
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Data
@ApiModel(value="SettlementAccount响应对象", description="结汇主单响应实体")
public class SettlementAccountVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "收款单号")
    private String accountNo;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;
    private String clientNo;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @ApiModelProperty(value = "结汇主体")
    private Long subjectId;

    @ApiModelProperty(value = "结汇主体")
    private String subjectName;

    @ApiModelProperty(value = "结汇时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime settlementDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "登记时间")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "结汇银行")
    private String subjectBank;

    @ApiModelProperty(value = "结汇银行账号")
    private String subjectBankNo;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "原币金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "结汇汇率")
    private BigDecimal customsRate;

    @ApiModelProperty(value = "本币金额")
    private BigDecimal settlementValue;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String statusDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(SettlementAccountStatusEnum.of(statusId)).map(SettlementAccountStatusEnum::getName).orElse("");
    }


}
