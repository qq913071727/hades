package com.tsfyun.scm.controller.order;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 订单明细 前端控制器
 * </p>
 *
 * @since 2020-04-08
 */
@RestController
@RequestMapping("/impOrderMember")
public class ImpOrderMemberController extends BaseController {

}

