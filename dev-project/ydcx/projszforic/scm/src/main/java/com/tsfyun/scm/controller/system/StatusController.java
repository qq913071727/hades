package com.tsfyun.scm.controller.system;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-03-03
 */
@RestController
@RequestMapping("/status")
public class StatusController extends BaseController {

}

