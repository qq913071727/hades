package com.tsfyun.scm.vo.customer.client;

import com.tsfyun.common.base.enums.AgencyFeeModeEnum;
import com.tsfyun.common.base.enums.QuoteTypeEnum;
import com.tsfyun.common.base.enums.TaxTypeEnum;
import com.tsfyun.scm.vo.customer.ExpressStandardQuoteMemberVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 进口报价响应实体
 * </p>
 *
 *
 * @since 2020-04-01
 */
@Data
@ApiModel(value="ImpQuote响应对象", description="进口报价响应实体")
public class ClientImpQuotePlusVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "协议ID")
    private Long agreementId;
    //协议报价PDF文件ID
    private Long fileId;

    @ApiModelProperty(value = "协议状态")
    private String agreementStatus;

    @ApiModelProperty(value = "税单类型")
    private String agreementBusinessType;

    @ApiModelProperty(value = "报价类型 ")
    private String quoteType;

    @ApiModelProperty(value = "约定方式")
    private String agreeMode;

    @ApiModelProperty(value = "天数")
    private Integer day;

    @ApiModelProperty(value = "月数")
    private Integer month;

    @ApiModelProperty(value = "几号")
    private Integer monthDay;

    private Integer firstMonth;

    private Integer firstMonthDay;

    private Integer lowerMonth;

    private Integer lowerMonthDay;

    private Integer week;

    private Integer weekDay;

    //税款账期
    private String taxAgreeMode;
    private Integer taxDay;
    private Integer taxMonth;
    private Integer taxMonthDay;
    private Integer taxFirstMonth;
    private Integer taxFirstMonthDay;
    private Integer taxLowerMonth;
    private Integer taxLowerMonthDay;
    private Integer taxWeek;
    private Integer taxWeekDay;

    //货款账期
    private String goAgreeMode;
    private Integer goDay;
    private Integer goMonth;
    private Integer goMonthDay;
    private Integer goFirstMonth;
    private Integer goFirstMonthDay;
    private Integer goLowerMonth;
    private Integer goLowerMonthDay;
    private Integer goWeek;
    private Integer goWeekDay;


    @ApiModelProperty(value = "成交方式")
    private String transactionMode;

    @ApiModelProperty(value = "税前,税后")
    private String taxType;
    private String taxTypeDesc;

    public String getTaxTypeDesc(){
        TaxTypeEnum taxTypeEnum = TaxTypeEnum.of(taxType);
        return Objects.nonNull(taxTypeEnum)?taxTypeEnum.getName():"";
    }

    @ApiModelProperty(value = "是否含税")
    private Boolean taxIncluded;

    @ApiModelProperty(value = "按单收费")
    private BigDecimal basePrice;

    @ApiModelProperty(value = "收费比例")
    private BigDecimal serviceRate;

    @ApiModelProperty(value = "最低收费")
    private BigDecimal minCost;

    @ApiModelProperty(value = "宽限天数")
    private Integer graceDay;

    @ApiModelProperty(value = "逾期利率")
    private BigDecimal overdueRate;

    private String quoteTypeDesc;

    @ApiModelProperty(value = "计费模式")
    private String agencyFeeMode;

    private String agencyFeeModeDesc;

    @ApiModelProperty(value = "按重计费明细")
    private List<ExpressStandardQuoteMemberVO> expressQuoteList;

    public String getQuoteTypeDesc() {
        QuoteTypeEnum  quoteTypeEnum = QuoteTypeEnum.of(quoteType);
        return Objects.nonNull(quoteTypeEnum) ? quoteTypeEnum.getName() : "";
    }

    public String getAgencyFeeModeDesc(){
        AgencyFeeModeEnum agencyFeeModeEnum = AgencyFeeModeEnum.of(agencyFeeMode);
        return Objects.nonNull(agencyFeeModeEnum) ? agencyFeeModeEnum.getName() : "";
    }
}
