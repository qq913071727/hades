package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.dto.system.SysParamDTO;
import com.tsfyun.scm.entity.system.SysParam;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-04-15
 */
@Repository
public interface SysParamMapper extends Mapper<SysParam> {

    void update(@Param("dto") SysParamDTO dto, @Param("userIdAndName") String userIdAndName);

    List<SysParam> getByIds(@Param("ids") List<String> ids);

}
