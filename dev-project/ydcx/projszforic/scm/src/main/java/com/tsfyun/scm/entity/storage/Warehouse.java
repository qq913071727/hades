package com.tsfyun.scm.entity.storage;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 仓库
 * </p>
 *

 * @since 2020-03-31
 */
@Data
public class Warehouse extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 仓库编码
     */

    private String code;

    /**
     * 仓库中文名称
     */

    private String name;

    /**
     * 仓库英文名称
     */

    private String nameEn;

    /**
     * 公司名称
     */

    private String companyName;

    /**
     * 仓库中文地址
     */

    private String address;

    /**
     * 仓库英文地址
     */

    private String addressEn;


    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系电话
     */
    private String linkTel;

    /**
     * 仓库类型
     */

    private String warehouseType;

    /**
     * 是否禁用
     */

    private Boolean disabled;

    /**
     * 是否开启国内自提
     */

    private Boolean isSelfMention;


    private String memo;


}
