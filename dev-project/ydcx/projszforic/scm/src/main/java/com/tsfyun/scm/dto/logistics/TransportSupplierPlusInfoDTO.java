package com.tsfyun.scm.dto.logistics;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class TransportSupplierPlusInfoDTO implements Serializable {

    /**
     * 运输供应商信息
     */
    @NotNull(message = "请填写运输供应商信息")
    @Valid
    private TransportSupplierDTO transportSupplier;

    /**
     * 运输供应商车辆信息
     */
    @Valid
    private List<ConveyanceDTO> conveyances;


}
