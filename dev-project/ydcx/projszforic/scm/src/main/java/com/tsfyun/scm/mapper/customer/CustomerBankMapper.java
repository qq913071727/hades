package com.tsfyun.scm.mapper.customer;

import com.tsfyun.scm.dto.customer.CustomerBankDTO;
import com.tsfyun.scm.entity.customer.CustomerBank;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.CustomerBankVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-03
 */
@Repository
public interface CustomerBankMapper extends Mapper<CustomerBank> {

    //获取客户银行信息
    List<CustomerBankVO> list(CustomerBankDTO dto);

}
