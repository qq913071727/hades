package com.tsfyun.scm.dto.user.client;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: PC客户端设置登录密码
 * @CreateDate: Created in 2020/11/26 13:04
 */
@Data
public class ClientSetPassWordDTO implements Serializable {

    /**
     * 密码
     */
    @NotEmptyTrim(message = "密码不能为空")
    @LengthTrim(min = 6,max = 18,message = "密码长度只能为6-18位")
    private String newPassword;

    /**
     * 确认密码
     */
    @NotEmptyTrim(message = "确认密码不能为空")
    @LengthTrim(min = 6,max = 18,message = "确认密码长度只能为6-18位")
    private String confirmPassword;
}
