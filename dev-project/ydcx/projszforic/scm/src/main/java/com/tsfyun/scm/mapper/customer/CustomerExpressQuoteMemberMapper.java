package com.tsfyun.scm.mapper.customer;

import com.tsfyun.scm.entity.customer.CustomerExpressQuoteMember;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 快递模式标准报价计算代理费明细 Mapper 接口
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Repository
public interface CustomerExpressQuoteMemberMapper extends Mapper<CustomerExpressQuoteMember> {

}
