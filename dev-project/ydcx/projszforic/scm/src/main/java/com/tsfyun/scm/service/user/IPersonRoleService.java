package com.tsfyun.scm.service.user;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.user.PersonRole;

import java.util.List;

public interface IPersonRoleService extends IService<PersonRole> {

    /**
     * 根据员工id获取角色
     * @param personId
     * @return
     */
    List<PersonRole> getPersonRoles(Long personId);

    /**
     * 员工绑定角色
     * @param personId
     * @param roleIdList
     */
    void saveOrUpdate(Long personId,List<String> roleIdList);

    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatchByRoleIds(List<String> roleIds);

}
