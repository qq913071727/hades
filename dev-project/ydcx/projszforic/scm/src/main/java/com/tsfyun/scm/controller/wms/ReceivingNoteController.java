package com.tsfyun.scm.controller.wms;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.wms.ImpReceivingNoteDTO;
import com.tsfyun.scm.dto.wms.InspectionDTO;
import com.tsfyun.scm.dto.wms.ReceivingNoteQTO;
import com.tsfyun.scm.service.support.IPdfService;
import com.tsfyun.scm.service.wms.IReceivingNoteMemberService;
import com.tsfyun.scm.service.wms.IReceivingNoteService;
import com.tsfyun.scm.vo.order.ImpOrderSimpleVO;
import com.tsfyun.scm.vo.storage.SimpleReceivingNoteCartonVO;
import com.tsfyun.scm.vo.wms.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 入库单 前端控制器
 * </p>
 *
 *
 * @since 2020-04-24
 */
@RestController
@RequestMapping("/receivingNote")
public class ReceivingNoteController extends BaseController {

    @Autowired
    private IReceivingNoteService receivingNoteService;

    @Autowired
    private IPdfService pdfService;


    /**
     * 进口入库分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ReceivingNoteListVO>> pageList(@ModelAttribute ReceivingNoteQTO qto) {
        PageInfo<ReceivingNoteListVO> page = receivingNoteService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }


    /**
     * 详情信息（含明细）
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<ReceivingNotePlusDetailVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(receivingNoteService.detail(id,operation));
    }

    /**=
     * 入库主单详情
     * @param id
     * @param operation
     * @return
     */
    @PostMapping(value = "mainDetail")
    public Result<ReceivingNoteVO> mainDetail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation){
        return success(receivingNoteService.mainDetail(id,operation));
    }

    /**=
     * 入库单验货详情
     * @return
     */
    @PostMapping(value = "inspectionDetail")
    public Result<InspectionNoteDetailVO> inspectionDetail(@RequestParam(value = "id")Long id){
        return success(receivingNoteService.inspectionDetail(id));
    }

    /**=
     * 进口入库登记
     * @return
     */
    @PostMapping(value = "impRegister")
    @DuplicateSubmit
    public Result<Long> impRegister(@RequestBody @Valid ImpReceivingNoteDTO dto){
        return success(receivingNoteService.impRegister(dto));
    }

    /**=
     * 保存验货
     * @param dto
     * @return
     */
    @PostMapping(value = "saveInspection")
    @DuplicateSubmit
    public Result<Void> saveInspection(@RequestBody @Valid InspectionDTO dto){
        receivingNoteService.saveInspection(dto);
        return success();
    }

    /**=
     * 标签打印
     * @param id
     * @return
     */
    @PostMapping(value = "printLable")
    public Result<Long> printLable(@RequestParam(value = "id")Long id){
        return success(pdfService.printReceivingNoteLabel(id));
    }

    /**=
     * 根据入库单号模糊查询和客户精确搜索
     * @param receivingNoteNo
     * @param customerId
     * @return
     */
    @PostMapping(value = "selectByReceivingNoteNo")
    public Result<List<SimpleReceivingNoteCartonVO>> selectByReceivingNoteNo(@RequestParam(value = "receivingNoteNo",required = false) String receivingNoteNo, @RequestParam(value = "customerId")Long customerId){
        return success(receivingNoteService.selectByReceivingNoteNo(receivingNoteNo,customerId));
    }

}

