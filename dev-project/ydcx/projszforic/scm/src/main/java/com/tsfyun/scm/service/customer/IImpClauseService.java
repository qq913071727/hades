package com.tsfyun.scm.service.customer;

import com.tsfyun.scm.dto.customer.ImpClauseDTO;
import com.tsfyun.scm.entity.customer.ImpClause;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.ImpClauseOrderVO;

import java.util.List;

/**
 * <p>
 * 进口条款 服务类
 * </p>
 *
 *
 * @since 2020-03-31
 */
public interface IImpClauseService extends IService<ImpClause> {

    /**
     * 根据协议id获取条款
     * @param agreementId
     * @return
     */
    ImpClause findByAgreementId(Long agreementId);

    /**=
     * 保存进口条款
     * @param agreementId
     * @param dto
     * @return
     */
    Long add(Long agreementId,ImpClauseDTO dto);

    /**=
     * 修改进口条款
     * @param agreementId
     * @param dto
     * @return
     */
    void edit(Long agreementId,ImpClauseDTO dto);

    /**
     * 根据订单id集合获取进口条款信息
     * @param orderIds
     * @param checkError
     * @return
     */
    List<ImpClauseOrderVO> checkOrderIdsGoodsRateTime(List<Long> orderIds,Boolean checkError);

}
