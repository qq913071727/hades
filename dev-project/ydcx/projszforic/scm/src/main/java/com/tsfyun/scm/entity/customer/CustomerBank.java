package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

/**
 * <p>
 * 客户银行
 * </p>
 *

 * @since 2020-03-03
 */
@Data
public class CustomerBank extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 银行名称
     */
    private String name;

    /**
     * 银行账号
     */
    private String account;

    /**
     * 银行地址
     */
    private String address;

    /**
     * 禁用标示
     */
    private Boolean disabled;

    /**
     * 默认银行
     */
    private Boolean isDefault;


}
