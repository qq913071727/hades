package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 订单费用响应实体
 * </p>
 *
 *
 * @since 2020-04-21
 */
@Data
@ApiModel(value="ImpOrderCost响应对象", description="订单费用响应实体")
public class ImpOrderCostVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "订单")
    private Long impOrderId;
    @ApiModelProperty(value = "订单号")
    private String impOrderNo;

    @ApiModelProperty(value = "科目")
    private String expenseSubjectId;

    @ApiModelProperty(value = "科目名称")
    private String expenseSubjectName;

    @ApiModelProperty(value = "费用分类")
    private String costClassify;

    @ApiModelProperty(value = "实际金额")
    private BigDecimal actualAmount;

    @ApiModelProperty(value = "应收金额")
    private BigDecimal receAmount;

    @ApiModelProperty(value = "已收金额")
    private BigDecimal acceAmount;

    @ApiModelProperty(value = "是否系统自动生成")
    private Boolean isAutomatic;

    @ApiModelProperty(value = "标记-(系统预留字段)")
    private String mark;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "是否需要在账期内冲销(优先核销)")
    private Boolean isFirstWriteOff;

    @ApiModelProperty(value = "是否锁定 (可以核销)")
    private Boolean isLock;

    @ApiModelProperty(value = "发生日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime happenDate;

    @ApiModelProperty(value = "账期日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime periodDate;

    @ApiModelProperty(value = "开始计算滞纳金日期")
    private LocalDateTime lateFeeDate;

    @ApiModelProperty(value = "逾期利率")
    private BigDecimal overdueRate;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "是否允许修改")
    private Boolean isAllowEdit;


}
