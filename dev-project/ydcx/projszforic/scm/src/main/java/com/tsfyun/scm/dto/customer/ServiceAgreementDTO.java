package com.tsfyun.scm.dto.customer;

import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 服务协议请求实体
 * </p>
 *
 *
 * @since 2020-12-07
 */
@Data
@ApiModel(value="ServiceAgreement请求对象", description="服务协议请求实体")
public class ServiceAgreementDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "协议编号不能为空")
   @LengthTrim(max = 20,message = "协议编号最大长度不能超过20位")
   @ApiModelProperty(value = "协议编号")
   private String docNo;

   @NotNull(message = "是否有效不能为空")
   @ApiModelProperty(value = "是否有效")
   private Boolean isEffective;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @LengthTrim(max = 200,message = "甲方名称最大长度不能超过200位")
   @ApiModelProperty(value = "甲方名称")
   private String partyaName;

   @LengthTrim(max = 50,message = "甲方法人最大长度不能超过50位")
   @ApiModelProperty(value = "甲方法人")
   private String partyaLegalPerson;

   @LengthTrim(max = 50,message = "甲方电话最大长度不能超过50位")
   @ApiModelProperty(value = "甲方电话")
   private String partyaTel;

   @LengthTrim(max = 50,message = "甲方邮箱最大长度不能超过50位")
   @ApiModelProperty(value = "甲方邮箱")
   private String partyaMail;

   @LengthTrim(max = 255,message = "甲方地址最大长度不能超过255位")
   @ApiModelProperty(value = "甲方地址")
   private String partyaAddress;

   @LengthTrim(max = 50,message = "甲方授权联系人最大长度不能超过50位")
   @ApiModelProperty(value = "甲方授权联系人")
   private String partyaLinkPerson;

   @LengthTrim(max = 18,message = "甲方社会统一信用代码最大长度不能超过18位")
   @ApiModelProperty(value = "甲方社会统一信用代码")
   private String partyaSocialNo;

   @LengthTrim(max = 20,message = "甲方账号最大长度不能超过20位")
   @ApiModelProperty(value = "甲方账号")
   private String partyaAccountNo;

   @NotEmptyTrim(message = "乙方不能为空")
   @LengthTrim(max = 50,message = "乙方最大长度不能超过50位")
   @ApiModelProperty(value = "乙方")
   private String partybName;

   @LengthTrim(max = 50,message = "乙方法人最大长度不能超过50位")
   @ApiModelProperty(value = "乙方法人")
   private String partybLegalPerson;

   @LengthTrim(max = 50,message = "乙方电话最大长度不能超过50位")
   @ApiModelProperty(value = "乙方电话")
   private String partybTel;

   @LengthTrim(max = 50,message = "乙方传真最大长度不能超过50位")
   @ApiModelProperty(value = "乙方传真")
   private String partybFax;

   @LengthTrim(max = 255,message = "乙方联系地址最大长度不能超过255位")
   @ApiModelProperty(value = "乙方联系地址")
   private String partybAddress;

   @LengthTrim(max = 18,message = "乙方统一社会信用代码最大长度不能超过18位")
   @ApiModelProperty(value = "乙方统一社会信用代码")
   private String partybSocialNo;

   @ApiModelProperty(value = "签订日期")
   private Date signingDate;

   private Date effectDate;

   @ApiModelProperty(value = "失效日期")
   private Date invalidDate;

   @ApiModelProperty(value = "实际失效日期")
   private Date actualInvalidDate;

   @LengthTrim(max = 2,message = "自动延期/年最大长度不能超过2位")
   @ApiModelProperty(value = "自动延期/年")
   private Integer delayYear;

   @ApiModelProperty(value = "协议正本是否签回")
   private Boolean isSignBack;

   @LengthTrim(max = 50,message = "signBackPerson最大长度不能超过50位")
   @ApiModelProperty(value = "signBackPerson")
   private String signBackPerson;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @LengthTrim(max = 5,message = "版本最大长度不能超过5位")
   @ApiModelProperty(value = "版本")
   private String version;


}
