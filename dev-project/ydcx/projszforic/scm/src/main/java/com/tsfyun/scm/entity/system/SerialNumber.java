package com.tsfyun.scm.entity.system;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 *
 * </p>
 *
 *
 * @since 2020-03-16
 */
@Data
public class SerialNumber implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    private String id;

    /**
     * 前缀
     */

    private String prefix;

    /**=
     * 日期规则
     */
    private String timeRule;

    /**
     * 流水号长度
     */
    private Integer serialLength;

    /**
     * 流水号重置方式
     */

    private String serialClear;

    /**
     * 当前流水号大小
     */
    private Integer nowSerial;

    /**
     * 修改人    修改人personId/修改人名称
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private LocalDateTime dateUpdated;

    /**
     * 流水号更新时间
     */
    private LocalDateTime dateSerial;

}
