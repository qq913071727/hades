package com.tsfyun.scm.dto.order;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClaimedAmountDTO implements Serializable {

   private Long customerId;
   private String currencyId;
}
