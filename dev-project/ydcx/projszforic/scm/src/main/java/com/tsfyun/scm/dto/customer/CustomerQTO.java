package com.tsfyun.scm.dto.customer;


import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;


@Data
@ApiModel(value="Customer请求对象", description="请求实体")
public class CustomerQTO extends PaginationDto {

    @ApiModelProperty(value = "客户代码")
    private String code;

    @ApiModelProperty(value = "客户名称")
    private String name;

    @ApiModelProperty(value = "查询空客户:null查询所有，true:查询为空的，false:不为空的")
    private Boolean queryNullName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "销售员工")
    private Long salePersonId;

    @ApiModelProperty(value = "商务员工")
    private Long busPersonId;

    @ApiModelProperty(value = "启用/禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "开始创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateCreatedStart;

    @ApiModelProperty(value = "结束创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateCreatedEnd;

    public void setDateCreatedEnd(Date dateCreatedEnd){
        if(dateCreatedEnd!=null){
            this.dateCreatedEnd = DateUtils.parseLong(DateUtils.format(dateCreatedEnd,"yyyy-MM-dd 23:59:59"));
        }
    }
}
