package com.tsfyun.scm.vo.storage;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/24 09:33
 */
@Data
public class SimpleReceivingNoteCartonVO implements Serializable {

    /**
     * 入库单号
     */
    private String receivingNoteNo;

    /**
     * 总箱数
     */
    private Integer totalCartonNum;

}
