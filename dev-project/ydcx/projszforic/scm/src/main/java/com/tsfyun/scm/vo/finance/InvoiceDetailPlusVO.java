package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 发票详情信息
 * @CreateDate: Created in 2020/5/19 10:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDetailPlusVO implements Serializable {

    /**
     * 发票主单
     */
    private InvoiceVO invoice;

    /**
     * 发票明细
     */
    private List<InvoiceMemberVO> members;

    /**
     * 发票费用明细
     */
    private List<InvoiceCostMemberVO> costMembers;

}
