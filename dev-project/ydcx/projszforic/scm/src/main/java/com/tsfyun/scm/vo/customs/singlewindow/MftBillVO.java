package com.tsfyun.scm.vo.customs.singlewindow;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 仓单提运单信息响应实体
 * </p>
 *
 * @author min.zhang
 * @since 2020-07-15
 */
@Data
@ApiModel(value="MftBill响应对象", description="仓单提运单信息响应实体")
public class MftBillVO implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "序号")
    private Integer listNo;

    @ApiModelProperty(value = "提(运)单号")
    private String billNo;

    @ApiModelProperty(value = "运输条款")
    private String conditionCode;

    @ApiModelProperty(value = "运输条款")
    private String conditionCodeName;

    @ApiModelProperty(value = "运费支付方法")
    private String paymentTypeCode;

    @ApiModelProperty(value = "运费支付方法")
    private String paymentType;

    @ApiModelProperty(value = "海关货物通关代码")
    private String goodsCustomsStatCode;

    @ApiModelProperty(value = "海关货物通关代码")
    private String goodsCustomsStat;

    @ApiModelProperty(value = "跨境启运地/指运地代码")
    private String transLocationIdCode;

    @ApiModelProperty(value = "跨境启运地/指运地名称")
    private String transLocationIdName;

    @ApiModelProperty(value = "货物总件数")
    private Integer packNum;

    @ApiModelProperty(value = "包装种类")
    private String packTypeCode;

    @ApiModelProperty(value = "包装种类")
    private String packType;

    @ApiModelProperty(value = "货物体积(M3)")
    private Integer volume;

    @ApiModelProperty(value = "货物总毛重(KG)")
    private BigDecimal grossWt;

    @ApiModelProperty(value = "货物价值")
    private BigDecimal goodsValue;

    @ApiModelProperty(value = "金额类型")
    private String currencyTypeCode;

    @ApiModelProperty(value = "金额类型")
    private String currencyType;

    @ApiModelProperty(value = "拼箱人代码")
    private String consolidatorId;

    @ApiModelProperty(value = "收货人名称")
    private String consigneeName;

    @ApiModelProperty(value = "发货人名称")
    private String consignorName;


}
