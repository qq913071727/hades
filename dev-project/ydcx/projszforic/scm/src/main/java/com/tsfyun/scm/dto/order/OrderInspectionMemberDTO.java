package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class OrderInspectionMemberDTO implements Serializable {

    @NotNull(message = "订单ID不能为空")
    @ApiModelProperty(value = "订单ID")
    private Long orderId;
    /**
     * 订单明细
     */
    @NotNull(message = "请填写订单明细数据")
    @Size(min = 1,max = 999,message = "订单明细数据只能为1-999条")
    @Valid
    private List<ImpOrderMemberDTO> members;

}
