package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.ReceiptAccountMember;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 收款核销明细 Mapper 接口
 * </p>
 *

 * @since 2020-05-22
 */
@Repository
public interface ReceiptAccountMemberMapper extends Mapper<ReceiptAccountMember> {

}
