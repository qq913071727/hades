package com.tsfyun.scm.service.materiel;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.ImpExcelVO;
import com.tsfyun.scm.dto.materiel.*;
import com.tsfyun.scm.entity.materiel.MaterielExp;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.excel.ClassifyMaterielExpExcel;
import com.tsfyun.scm.vo.materiel.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2021-03-08
 */
public interface IMaterielExpService extends IService<MaterielExp> {

    //物料信息包含海关编码信息
    Result<List<MaterielExpHSVO>> pageList(MaterielExpQTO qto);
    List<MaterielExpHSVO> formatMateriel(List<MaterielExpVO> materielVOList);
    //物料导入
    ImpExcelVO importMateriel(MultipartFile file);
    //已归类物料导入
    ImpExcelVO importClassifyMateriel(MultipartFile file);
    //根据id删除物料无法删除不提示
    void removeIdsNotPrompt(List<String> ids);
    //保存物料，如果存在则直接返回
    MaterielExpVO obtainMaterialAndSave(MaterielExpDTO dto);
    //归类详情
    Map<String,Object> classifyDetail(List<String> ids);
    //保存物料归类
    void saveClassify(ClassifyMaterielExpDTO dto);

    void saveImportClassifyMateriel(List<ClassifyMaterielExpExcel> classifyList);
    //模糊搜索
    List<SimpleMaterielVO> vague(ExpMaterielQTO qto);
    // 删除物料
    void removeMateriel(String id);
}
