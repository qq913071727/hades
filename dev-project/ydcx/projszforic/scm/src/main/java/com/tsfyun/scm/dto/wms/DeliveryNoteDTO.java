package com.tsfyun.scm.dto.wms;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 出库单请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeliveryNote请求对象", description="出库单请求实体")
public class DeliveryNoteDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "单号不能为空")
   @LengthTrim(max = 20,message = "单号最大长度不能超过20位")
   @ApiModelProperty(value = "单号")
   private String docNo;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 10,message = "单据类型最大长度不能超过10位")
   @ApiModelProperty(value = "单据类型-枚举")
   private String billType;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @NotNull(message = "入库时间不能为空")
   @ApiModelProperty(value = "入库时间")
   private LocalDateTime deliveryDate;

   @NotEmptyTrim(message = "状态编码不能为空")
   @LengthTrim(max = 20,message = "状态编码最大长度不能超过20位")
   @ApiModelProperty(value = "状态编码")
   private String statusId;

   @NotNull(message = "仓库不能为空")
   @ApiModelProperty(value = "仓库")
   private Long warehouseId;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
