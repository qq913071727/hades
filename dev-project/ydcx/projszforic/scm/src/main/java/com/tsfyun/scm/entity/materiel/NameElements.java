package com.tsfyun.scm.entity.materiel;

import java.time.LocalDateTime;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 品名要素
 * </p>
 *

 * @since 2020-03-26
 */
@Data
public class NameElements {

     private static final long serialVersionUID=1L;

     @Id
    private String id;

    private String createPerson;

    private String hsCode;


    private Boolean disabled;


    private String elements;


    private String name;

    /**
     * 修改时间
     */
    private LocalDateTime lastUpdated;

    @InsertFill
    private LocalDateTime dateCreated;


    private String nameMemo;


    private String updatePerson;


}
