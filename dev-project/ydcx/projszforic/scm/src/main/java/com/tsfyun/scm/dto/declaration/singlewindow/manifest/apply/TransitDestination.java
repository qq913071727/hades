package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 跨境指运地信息
 * @since Created in 2020/4/22 12:44
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransitDestination", propOrder = {
        "id"
})
public class TransitDestination {

    /**
     * 4位关区代码
     */
    @XmlElement(name = "ID")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
