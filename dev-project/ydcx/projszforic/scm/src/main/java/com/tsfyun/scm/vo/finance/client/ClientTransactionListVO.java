package com.tsfyun.scm.vo.finance.client;

import com.tsfyun.scm.vo.finance.TransactionFlowVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 交易明细列表
 * @CreateDate: Created in 2020/12/25 10:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientTransactionListVO  implements Serializable {

    private List<TransactionFlowVO> transactions;

    private BigDecimal incomeAmount;

    private BigDecimal outcomeAmount;

}
