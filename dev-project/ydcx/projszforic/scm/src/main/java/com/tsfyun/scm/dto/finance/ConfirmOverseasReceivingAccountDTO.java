package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.scm.enums.UndertakingModeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ConfirmOverseasReceivingAccountDTO implements Serializable {

    @NotNull(message = "单据ID不能为空")
    private Long id;
    @NotNull(message = "到账时间不能为空")
    private LocalDateTime accountDate;
    @NotEmptyTrim(message = "请选择手续费承担方式")
    @EnumCheck(clazz = UndertakingModeEnum.class,message = "手续费承担方式错误")
    private String undertakingMode;
    @Digits(integer = 6, fraction = 2, message = "手续费整数位不能超过6位，小数位不能超过2位")
    @DecimalMin(value = "0.00",message = "手续费必须大于等于0")
    @ApiModelProperty(value = "手续费")
    private BigDecimal fee;

}
