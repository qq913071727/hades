package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.CostChangeRecordExpDTO;
import com.tsfyun.scm.dto.order.CostChangeRecordExpQTO;
import com.tsfyun.scm.service.order.ICostChangeRecordExpService;
import com.tsfyun.scm.service.order.IExpOrderService;
import com.tsfyun.scm.vo.order.CostChangeRecordExpVO;
import com.tsfyun.scm.vo.order.ExpOrderAdjustCostVO;
import com.tsfyun.scm.vo.order.ImpOrderAdjustCostVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 费用变更记录 前端控制器
 * </p>
 *
 * @since 2020-05-27
 */
@RestController
@RequestMapping("/costChangeRecordExp")
public class CostChangeRecordExpController extends BaseController {

    @Autowired
    private ICostChangeRecordExpService costChangeRecordExpService;

    @Autowired
    private IExpOrderService expOrderService;

    /**
     * 分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<CostChangeRecordExpVO>> pageList(@ModelAttribute CostChangeRecordExpQTO qto) {
        PageInfo<CostChangeRecordExpVO> page = costChangeRecordExpService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 调整费用
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "adjust")
    Result<Void> adjust(@ModelAttribute @Validated CostChangeRecordExpDTO dto) {
        costChangeRecordExpService.adjustOrderCost(dto);
        return success();
    }

    /**
     * 可以调整费用的订单下拉，订单模糊搜索（带权限）
     * @param expOrderNo
     * @return
     */
    @RequestMapping(value = "selectAdjustOrder")
    Result<List<ExpOrderAdjustCostVO>> selectAdjustOrder(@RequestParam(value = "expOrderNo",required = false)String expOrderNo) {
        return success(expOrderService.selectAdjustOrder(expOrderNo));
    }

    /**=
     * 订单费用导入
     * @param file
     * @return
     */
    @PostMapping(value = "importCost")
    Result<Void> importCost(@RequestPart(value = "file") MultipartFile file){
        costChangeRecordExpService.importOrderCost(file);
        return success( );
    }

    /**=
     * 订单其他费用导入
     * @param file
     * @return
     */
    @PostMapping(value = "importOtherCost")
    Result<Void> importOtherCost(@RequestPart(value = "file") MultipartFile file){
        costChangeRecordExpService.importOrderOtherCost(file);
        return success( );
    }

}

