package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.dto.logistics.CrossBorderWaybillQTO;
import com.tsfyun.scm.entity.logistics.CrossBorderWaybill;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.logistics.CrossBorderWaybillVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 跨境运输单 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-17
 */
@Repository
public interface CrossBorderWaybillMapper extends Mapper<CrossBorderWaybill> {

    List<CrossBorderWaybillVO> list(CrossBorderWaybillQTO qto);

    CrossBorderWaybillVO detail(@Param(value = "id") Long id);

    CrossBorderWaybill findByImpOrderId(@Param(value = "orderId")Long orderId);
    CrossBorderWaybill findByExpOrderId(@Param(value = "orderId")Long orderId);

    /**
     * 统计车次
     * @param billType
     * @param startDate
     * @param endDate
     * @return
     */
    Integer totalTrainNumber(@Param(value = "billType")String billType, @Param(value = "startDate") Date startDate, @Param(value = "endDate")Date endDate);
}
