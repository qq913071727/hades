package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.FileDTO;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/10 15:07
 */
@Data
public class ExpAgreementPlusDTO implements Serializable {

    @NotNull(message = "请填写协议基础信息")
    @Valid
    private ExpAgreementDTO agreement;

    private FileDTO file;

}
