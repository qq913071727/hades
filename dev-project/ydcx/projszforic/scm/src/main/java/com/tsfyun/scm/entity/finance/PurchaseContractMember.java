package com.tsfyun.scm.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 采购合同明细
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Data
public class PurchaseContractMember implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    private Integer rowNo;

    /**
     * 采购合同主单
     */

    private Long purchaseContractId;

    /**
     * 订单明细
     */

    private Long orderMemberId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 代理费
     */

    private BigDecimal agentFee;

    /**
     * 代垫费
     */

    private BigDecimal matFee;

    /**
     * 票差调整金额
     */

    private BigDecimal differenceVal;

    /**
     * 增值税率
     */
    private BigDecimal addedTaxRate;

    /**
     * 退税率
     */
    private BigDecimal taxRebateRate;
}
