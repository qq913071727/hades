package com.tsfyun.scm.entity.logistics;

import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 出口香港送货单
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Data
public class OverseasDeliveryNote extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 订单编号
     */

    private String expOrderNos;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 发货单位
     */

    private String consignor;

    /**
     * 发货地址
     */

    private String consignorAddress;

    /**
     * 发货联系人
     */

    private String consignorLinkMan;

    /**
     * 发货联系电话
     */

    private String consignorLinkTel;

    /**
     * 收货信息id
     */

    private Long consigneeId;

    /**
     * 收货单位
     */

    private String consignee;

    /**
     * 收货联系地址
     */

    private String consigneeAddress;

    /**
     * 收货联系人
     */

    private String consigneeLinkMan;

    /**
     * 收货联系电话
     */

    private String consigneeLinkTel;

    /**
     * 送货日期
     */

    private Date deliveryDate;

    /**
     * 入仓号
     */

    private String receivingNo;

    /**
     * 收货仓库
     */

    private String receivingWarehouse;

    /**
     * 备注
     */

    private String remark;


}
