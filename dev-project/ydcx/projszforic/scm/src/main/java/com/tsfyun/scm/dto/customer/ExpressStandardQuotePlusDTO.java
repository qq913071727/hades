package com.tsfyun.scm.dto.customer;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/8 15:11
 */
@Data
public class ExpressStandardQuotePlusDTO implements Serializable {

    @NotNull(message = "请填写报价信息")
    private ExpressStandardQuoteDTO quote;

    @Size(min = 2,message = "请至少填写2条报价明细信息")
    private List<ExpressStandardQuoteMemberDTO> members;

}
