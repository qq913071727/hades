package com.tsfyun.scm.vo.customs;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

@Data
public class ContractVO implements Serializable {

    @ApiModelProperty(value = "合同号")
    private String contrNo;
    @ApiModelProperty(value = "单据类型")
    private String billType;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "进出口日期")
    private LocalDateTime importDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "合同日期")
    private LocalDateTime contractDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "有效期")
    private LocalDateTime validityDate;
    public LocalDateTime getValidityDate(){
        return getContractDate().plusYears(1);
    }


    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "装运期限")
    private LocalDateTime shipmentDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "发票日期")
    private LocalDateTime invoiceDate;

    @ApiModelProperty(value = "买方")
    private String buyerName;
    private String buyerNameEn;
    private String buyerAddress;
    private String buyerAddressEn;
    private String buyerTel;
    private String buyerFax;
    private String buyerChapter;

    @ApiModelProperty(value = "卖方")
    private String sellerName;
    private String sellerNameEn;
    private String sellerAddress;
    private String sellerAddressEn;
    private String sellerTel;
    private String sellerFax;
    private String sellerChapter;

    @ApiModelProperty(value = "消费使用单位")
    private String ownerScc;//消费使用单位(18位社会信用代码)
    private String ownerCode;//消费使用单位(10位海关代码)
    private String ownerName;//消费使用单位(名称)

    @ApiModelProperty(value = "成交方式")
    private String transactionMode;
    private String transactionModeDesc;
    public String getTransactionModeDesc() {
        return Optional.ofNullable(TransactionModeEnum.of(transactionMode)).map(TransactionModeEnum::getName).orElse("");
    }

    @ApiModelProperty(value = "标记唛码")
    private String markNo;

    @ApiModelProperty(value = "货物存放地")
    private String goodsPlace;

    @ApiModelProperty(value = "运抵国(地区)")
    private String cusTradeCountryName;

    @ApiModelProperty(value = "境外客户")
    private String supplierName;

    @ApiModelProperty(value = "境外客户地址")
    private String supplierAddress;

    /**
     * 境外收发货人
     */

    private String subjectOverseasType;

    /**
     * 境外收发货人
     */

    private Long subjectOverseasId;

    /**
     * 境外收发货人
     */
    private String subjectOverseasName;

    /**
     * 件数
     */

    private Integer packNo;
}
