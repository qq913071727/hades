package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-03-20
 */
@Data
public class SubjectBank extends BaseEntity {

     private static final long serialVersionUID=1L;


    private Long subjectId;

    /**
     * 银行名称
     */

    private String name;

    /**
     * 银行账号
     */

    private String account;

    /**
     * 银行地址
     */

    private String address;

    /**
     * 禁用标示
     */

    private Boolean disabled;

    /**
     * 创建人
     */

    private String createBy;

    /**
     * 是否默认
     */

    private Boolean isDefault;

    /**
     * 默认收款账户
     */

    private Boolean receivables;

    /**
     * 备注
     */

    private String memo;

    /**
     * 币制
     */

    private String currencyId;


}
