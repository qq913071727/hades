package com.tsfyun.scm.controller.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.util.ResultUtil;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.common.base.vo.CustomerSimpleVO;
import com.tsfyun.common.base.vo.QichachaCompanyVO;
import com.tsfyun.scm.client.ToolClient;
import com.tsfyun.scm.dto.customer.ChangeCustomerPersonDTO;
import com.tsfyun.scm.dto.customer.CustomerPlusInfoDTO;
import com.tsfyun.scm.dto.customer.CustomerQTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.service.customer.ICustomerService;
import com.tsfyun.scm.vo.customer.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(value = "customer")
@Api(tags = "客户")
public class CustomerController extends BaseController {

    @Autowired
    private ICustomerService customerService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Resource
    private ToolClient toolClient;

    /**
     * 根据编码，拼音，名称分页获取客户下拉框
     * @param qto
     * @return
     */
    @ApiOperation(value = "根据编码，拼音，名称分页获取客户下拉框")
    @PostMapping(value = "select")
    public Result<List<SimpleCustomerVO>> positionList(@ModelAttribute CustomerQTO qto) {
        qto.setQueryNullName(Boolean.FALSE);
        PageInfo<Customer> page = customerService.selectPageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(), SimpleCustomerVO.class));
    }

    /**
     * 分页查询客户列表(后端用)
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    @PreAuthorize("hasRole('manager')")
    public Result<List<CustomerVO>> pageList(@ModelAttribute CustomerQTO dto) {
//        dto.setQueryNullName(Boolean.FALSE);
        PageInfo<CustomerVO> pageInfo = customerService.pageList(dto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 客户详情基本信息，返回商务和销售名称
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<CustomerVO> detail(@RequestParam(value = "id")Long id) {
        return success(customerService.detail(id));
    }

    /**
     * 新增客户信息（含客户地址）
     * @param dto
     * @return 返回客户id
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    @ApiOperation(value = "新增")
    Result<Long> add(@RequestBody @Validated(AddGroup.class) CustomerPlusInfoDTO dto) {
        return success(customerService.addPlus(dto));
    }

    /**
     * 修改客户信息（含客户地址）
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated(UpdateGroup.class) CustomerPlusInfoDTO dto) {
        customerService.editPlus(dto);
        return success();
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        customerService.updateDisabled(id,disabled);
        return success();
    }



    /**
     * 客户信息详情（含客户地址）
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    Result<CustomerPlusVO> info(@RequestParam(value = "id")Long id) {
        return success(customerService.detailPlus(id));
    }


    /**
     *  删除客户信息（真删，如果存在供应商信息或客户收货信息则不允许删除，其他业务数据关联待后续补充）
     * @param id
     * @return
     */
    @PostMapping(value = "delete")
    Result<Void> delete(@RequestParam(value = "id")Long id) {
        customerService.remove(id);
        return success();
    }

    /**
     * 导入客户信息
     * @param file
     * @return
     */
    @PostMapping(value = "importData")
    public Result importData(@RequestPart(value = "file") MultipartFile file){
        customerService.importData(file);
        return Result.success();
    }

    /**
     * 变更销售人员
     * @param dto
     * @return
     */
    @PostMapping(value = "changeSale")
    public Result<Void> changeSale(@ModelAttribute @Validated(AddGroup.class) ChangeCustomerPersonDTO dto) {
        customerService.changeCustomerSaleOrBus(dto);
        return success();
    }

    /**
     * 变更商务人员
     * @param dto
     * @return
     */
    @PostMapping(value = "changeBus")
    public Result<Void> changeBus(@ModelAttribute @Validated(UpdateGroup.class) ChangeCustomerPersonDTO dto) {
        customerService.changeCustomerSaleOrBus(dto);
        return success();
    }

    /**
     * 客户销售/商务信息
     * @param id
     * @return
     */
    @GetMapping(value = "getCustomerPerson")
    Result<CustomerPersonVO> getCustomerPerson(@RequestParam(value = "id")Long id) {
        return success(customerService.getCustomerPerson(id));
    }

    /**
     * 核准客户
     * @param dto
     * @return
     */
    @PostMapping(value = "approving")
    @ApiOperation(value = "核准")
    Result<Void> approving(@ModelAttribute TaskDTO dto) {
        customerService.approvingCustomer(dto);
        return success( );
    }

    /**
     * 根据公司名称获取公司工商信息
     * @param companyName
     * @return
     */
    @PostMapping(value = "businessByCompanyName")
    @ApiOperation(value = "根据公司名称获取公司工商信息")
    Result<QichachaCompanyVO> businessByCompanyName(@RequestParam(value = "companyName")String companyName) {
        Result<QichachaCompanyVO> qichachaCompanyVOResult = toolClient.detailByCompanyName(companyName);
        ResultUtil.checkRemoteResult(qichachaCompanyVOResult,"获取公司工商信息失败");
        return qichachaCompanyVOResult;
    }

    /**
     * 根据客户ID集合获取客户简要信息（提供给其他服务调用）
     * @param customerIds
     * @return
     */
    @PostMapping(value = "obtainCustomerSimples")
    public Result<Map<Long, CustomerSimpleVO>> obtainCustomerSimples(@RequestBody Set<Long> customerIds){
        return success(customerService.obtainCustomerSimples(customerIds));
    }

    /**
     * 获取所有启用的客户
     * @return
     */
    @PostMapping(value = "allAuditCustomer")
    @ApiOperation(value = "获取所有启用的客户")
    Result<List<CustomerSimpleVO>> allAuditCustomer( ) {
        return success(customerService.allAuditCustomer( ));
    }

}
