package com.tsfyun.scm.service.impl.system;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.entity.system.StatusHistory;
import com.tsfyun.scm.mapper.system.StatusHistoryMapper;
import com.tsfyun.scm.service.system.IStatusHistoryService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.system.StatusHistoryVO;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-03
 */
@Service
public class StatusHistoryServiceImpl extends ServiceImpl<StatusHistory> implements IStatusHistoryService<T> {

    @Autowired
    private StatusHistoryMapper statusHistoryMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveHistory(DomainOprationEnum opration, String domainId, String domainType, String nowStatusId, String nowStatusName) {
        saveHistory(opration,domainId,domainType,"","",nowStatusId,nowStatusName,"","");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveHistory(DomainOprationEnum opration, String domainId, String domainType, String nowStatusId, String nowStatusName, String memo) {
        saveHistory(opration,domainId,domainType,"","",nowStatusId,nowStatusName,memo,"");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveHistory(DomainOprationEnum opration, String domainId, String domainType, String nowStatusId, String nowStatusName, String memo,String operator) {
        saveHistory(opration,domainId,domainType,"","",nowStatusId,nowStatusName,memo,operator);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveHistory(DomainOprationEnum opration, String domainId,String domainType,String historyStatusId,String historyStatusName,String nowStatusId,String nowStatusName,String memo){
        saveHistory(opration,domainId,domainType,historyStatusId,historyStatusName,nowStatusId,nowStatusName,memo,"");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveHistory(DomainOprationEnum opration, String domainId, String domainType, String historyStatusId, String historyStatusName, String nowStatusId, String nowStatusName, String memo,String operator) {
        StatusHistory history = new StatusHistory();
        history.setOperationCode(opration.getCode());
        history.setDomainId(domainId);
        history.setDomainType(domainType);
        history.setOriginalStatusId(historyStatusId);
        history.setOriginalStatusName(historyStatusName);
        history.setNowStatusId(nowStatusId);
        history.setNowStausName(nowStatusName);
        history.setMemo(memo);
        history.setDateCreated(LocalDateTime.now());
        history.setCreateBy(StringUtils.isEmpty(operator)? SecurityUtil.getCurrentPersonName():operator);
        super.save(history);
    }

    @Override
    public List<StatusHistoryVO> findByDomainId(String domainId) {
        List<StatusHistoryVO> historyVOList = Lists.newArrayList();
        List<StatusHistory> historyList =  statusHistoryMapper.selectByExample(Example.builder(StatusHistory.class).where(
                TsfWeekendSqls.<StatusHistory>custom().andEqualTo(false,StatusHistory::getDomainId,domainId)
        ).build());
        if(CollectionUtil.isNotEmpty(historyList)) {
            historyList = historyList.stream().sorted(Comparator.comparing(StatusHistory::getDateCreated).reversed()).collect(Collectors.toList());
            historyVOList = beanMapper.mapAsList(historyList,StatusHistoryVO.class);
        }
        return historyVOList;
    }

    @Override
    public StatusHistoryVO findRecentByDomainAndNowStatus(String domainId, String domainType, String nowStatusId) {
         return statusHistoryMapper.findRecentByDomainAndNowStatusOne(domainId,domainType,nowStatusId);
    }

    @Override
    public StatusHistoryVO findRecentByDomainAndHistoryStatus(String domainId, String domainType, String historyStatusId) {
        return statusHistoryMapper.findRecentByDomainAndHistoryStatus(domainId,domainType,historyStatusId);
    }

    @Override
    public String findInfoByDomainAndNowStatus(String domainId, String domainType, String nowStatusId) {
        StatusHistoryVO statusHistoryVO = findRecentByDomainAndNowStatus(domainId,domainType,nowStatusId);
        return Objects.nonNull(statusHistoryVO)?statusHistoryVO.getMemo():"";
    }

    @Override
    public List<StatusHistoryVO> findRecentByDomainAndNowStatusBatch(List<String> domainIds, String domainType, String nowStatusId) {
        return statusHistoryMapper.findRecentByDomainAndNowStatusOneBatch(domainIds,domainType,nowStatusId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeHistory(String domainId, String domainType) {
        statusHistoryMapper.removeHistory(domainId,domainType);
    }

    @Override
    public String obtainOperator(String domainId, DomainOprationEnum opration) {
        List<StatusHistory> list = statusHistoryMapper.findByOneDomainIdAndOpration(domainId,opration.getCode());
        if(CollectionUtil.isNotEmpty(list)){
            return list.get(0).getCreateBy();
        }
        return "";
    }
}
