package com.tsfyun.scm.stream.receiver;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

/**
 * @Description: 消息输入通道定义
 * @CreateDate: Created in 2019/12/16 15:43
 */
@Component
public interface Sink {

    String IN_SCM_MATERIEL = "scm-materiel-input";
    @Input(IN_SCM_MATERIEL)
    SubscribableChannel receiverMateriel();


    String IN_SCM_CLASSIFY_MATERIEL = "scm-classify-materiel-input";
    @Input(IN_SCM_CLASSIFY_MATERIEL)
    SubscribableChannel receiverClassifyMateriel();

    String IN_SCM_CLASSIFY_MATERIEL_EXP = "scm-classify-materiel-exp-input";
    @Input(IN_SCM_CLASSIFY_MATERIEL_EXP)
    SubscribableChannel receiverClassifyMaterielExp();

    String IN_SCM_COST_WRITE_OFF = "scm-cost-write-off-input";
    @Input(IN_SCM_COST_WRITE_OFF)
    SubscribableChannel receiverCostWriteOff();

    String IN_SCM_MAIL = "scm-mail-input";
    @Input(IN_SCM_MAIL)
    SubscribableChannel receiverMail();

    String IN_SCM_SMS = "scm-sms-input";
    @Input(IN_SCM_SMS)
    SubscribableChannel receiverSms();

    String IN_SCM_WX = "scm-wx-input";
    @Input(IN_SCM_WX)
    SubscribableChannel receiverWx();

}
