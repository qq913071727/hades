package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.AgreeModeEnum;
import com.tsfyun.common.base.enums.QuoteTypeEnum;
import com.tsfyun.common.base.enums.TaxTypeEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 进口报价请求实体
 * </p>
 *
 *
 * @since 2020-04-01
 */
@Data
@ApiModel(value="ImpQuote请求对象", description="进口报价请求实体")
public class ImpQuoteDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   private Long agreementId;

   @NotEmptyTrim(message = "报价类型不能为空")
   @EnumCheck(clazz = QuoteTypeEnum.class,message = "报价类型错误")
   @ApiModelProperty(value = "报价类型")
   private String quoteType;

   @NotEmptyTrim(message = "代杂费账期方式不能为空")
   @EnumCheck(clazz = AgreeModeEnum.class,message = "代杂费账期方式错误")
   @ApiModelProperty(value = "代杂费账期方式")
   private String agreeMode;

   @NotNull(message = "代杂费账期天数不能为空")
   @LengthTrim(max = 6,message = "代杂费账期天数最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费账期天数")
   private Integer day;

   @NotNull(message = "代杂费月结月份不能为空")
   @LengthTrim(max = 6,message = "代杂费月数最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费月结月份")
   private Integer month;

   @NotNull(message = "代杂费月结几号不能为空")
   @LengthTrim(max = 6,message = "代杂费月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费月结几号")
   private Integer monthDay;

   @NotNull(message = "代杂费半月结1号到15号月份不能为空")
   @LengthTrim(max = 6,message = "代杂费半月结1号到15号月份最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费半月结1号到15号月份")
   private Integer firstMonth;

   @NotNull(message = "代杂费半月结1号到15号月结几号不能为空")
   @LengthTrim(max = 6,message = "代杂费半月结1号到15号月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费半月结1号到15号月结几号")
   private Integer firstMonthDay;

   @NotNull(message = "代杂费半月结16号到月末月份不能为空")
   @LengthTrim(max = 6,message = "半月结16号到月末月份最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费半月结16号到月末月份")
   private Integer lowerMonth;

   @NotNull(message = "代杂费半月结16号到月末月结几号不能为空")
   @LengthTrim(max = 6,message = "代杂费半月结16号到月末月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费半月结16号到月末月结几号")
   private Integer lowerMonthDay;

   @NotNull(message = "代杂费周结周数不能为空")
   @LengthTrim(max = 6,message = "代杂费周结周数最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费周结周数")
   private Integer week;

   @NotNull(message = "代杂费周结星期数不能为空")
   @LengthTrim(max = 6,message = "代杂费周结星期数最大长度不能超过6位")
   @ApiModelProperty(value = "代杂费周结星期数")
   private Integer weekDay;

   @NotEmptyTrim(message = "税款账期方式不能为空")
   @EnumCheck(clazz = AgreeModeEnum.class,message = "税款账期方式错误")
   @ApiModelProperty(value = "税款账期方式")
   private String taxAgreeMode;

   @NotNull(message = "税款账期天数不能为空")
   @LengthTrim(max = 6,message = "税款账期天数最大长度不能超过6位")
   @ApiModelProperty(value = "税款账期天数")
   private Integer taxDay;

   @NotNull(message = "税款月结月份不能为空")
   @LengthTrim(max = 6,message = "税款月数最大长度不能超过6位")
   @ApiModelProperty(value = "税款月结月份")
   private Integer taxMonth;

   @NotNull(message = "税款月结几号不能为空")
   @LengthTrim(max = 6,message = "税款月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "税款月结几号")
   private Integer taxMonthDay;

   @NotNull(message = "税款半月结1号到15号月份不能为空")
   @LengthTrim(max = 6,message = "税款半月结1号到15号月份最大长度不能超过6位")
   @ApiModelProperty(value = "税款半月结1号到15号月份")
   private Integer taxFirstMonth;

   @NotNull(message = "税款半月结1号到15号月结几号不能为空")
   @LengthTrim(max = 6,message = "税款半月结1号到15号月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "税款半月结1号到15号月结几号")
   private Integer taxFirstMonthDay;

   @NotNull(message = "税款半月结16号到月末月份不能为空")
   @LengthTrim(max = 6,message = "半月结16号到月末月份最大长度不能超过6位")
   @ApiModelProperty(value = "税款半月结16号到月末月份")
   private Integer taxLowerMonth;

   @NotNull(message = "税款半月结16号到月末月结几号不能为空")
   @LengthTrim(max = 6,message = "税款半月结16号到月末月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "税款半月结16号到月末月结几号")
   private Integer taxLowerMonthDay;

   @NotNull(message = "税款周结周数不能为空")
   @LengthTrim(max = 6,message = "税款周结周数最大长度不能超过6位")
   @ApiModelProperty(value = "税款周结周数")
   private Integer taxWeek;

   @NotNull(message = "税款周结星期数不能为空")
   @LengthTrim(max = 6,message = "税款周结星期数最大长度不能超过6位")
   @ApiModelProperty(value = "税款周结星期数")
   private Integer taxWeekDay;

   @NotEmptyTrim(message = "货款账期方式不能为空")
   @EnumCheck(clazz = AgreeModeEnum.class,message = "货款账期方式错误")
   @ApiModelProperty(value = "货款账期方式")
   private String goAgreeMode;

   @NotNull(message = "货款账期天数不能为空")
   @LengthTrim(max = 6,message = "货款账期天数最大长度不能超过6位")
   @ApiModelProperty(value = "货款账期天数")
   private Integer goDay;

   @NotNull(message = "货款月结月份不能为空")
   @LengthTrim(max = 6,message = "货款月数最大长度不能超过6位")
   @ApiModelProperty(value = "货款月结月份")
   private Integer goMonth;

   @NotNull(message = "货款月结几号不能为空")
   @LengthTrim(max = 6,message = "货款月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "货款月结几号")
   private Integer goMonthDay;

   @NotNull(message = "货款半月结1号到15号月份不能为空")
   @LengthTrim(max = 6,message = "货款半月结1号到15号月份最大长度不能超过6位")
   @ApiModelProperty(value = "货款半月结1号到15号月份")
   private Integer goFirstMonth;

   @NotNull(message = "货款半月结1号到15号月结几号不能为空")
   @LengthTrim(max = 6,message = "货款半月结1号到15号月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "货款半月结1号到15号月结几号")
   private Integer goFirstMonthDay;

   @NotNull(message = "货款半月结16号到月末月份不能为空")
   @LengthTrim(max = 6,message = "半月结16号到月末月份最大长度不能超过6位")
   @ApiModelProperty(value = "货款半月结16号到月末月份")
   private Integer goLowerMonth;

   @NotNull(message = "货款半月结16号到月末月结几号不能为空")
   @LengthTrim(max = 6,message = "货款半月结16号到月末月结几号最大长度不能超过6位")
   @ApiModelProperty(value = "货款半月结16号到月末月结几号")
   private Integer goLowerMonthDay;

   @NotNull(message = "货款周结周数不能为空")
   @LengthTrim(max = 6,message = "货款周结周数最大长度不能超过6位")
   @ApiModelProperty(value = "货款周结周数")
   private Integer goWeek;

   @NotNull(message = "货款周结星期数不能为空")
   @LengthTrim(max = 6,message = "货款周结星期数最大长度不能超过6位")
   @ApiModelProperty(value = "货款周结星期数")
   private Integer goWeekDay;

   @NotEmptyTrim(message = "税前,税后不能为空")
   @EnumCheck(clazz = TaxTypeEnum.class,message = "税前,税后错误")
   @ApiModelProperty(value = "税前,税后")
   private String taxType;

   @NotNull(message = "是否含税不能为空")
   @ApiModelProperty(value = "是否含税")
   private Boolean taxIncluded;

   @NotNull(message = "按单收费不能为空")
   @Digits(integer = 10, fraction = 2, message = "按单收费整数位不能超过10位，小数位不能超过2位")
   @ApiModelProperty(value = "按单收费")
   private BigDecimal basePrice;

   @NotNull(message = "收费比例不能为空")
   @Digits(integer = 10, fraction = 2, message = "收费比例整数位不能超过10位，小数位不能超过2位")
   @ApiModelProperty(value = "收费比例")
   private BigDecimal serviceRate;

   @NotNull(message = "最低收费不能为空")
   @Digits(integer = 10, fraction = 2, message = "最低收费整数位不能超过10位，小数位不能超过2位")
   @ApiModelProperty(value = "最低收费")
   private BigDecimal minCost;

   @NotNull(message = "宽限天数不能为空")
   @LengthTrim(max = 6,message = "宽限天数最大长度不能超过6位")
   @ApiModelProperty(value = "宽限天数")
   private Integer graceDay;

   @NotNull(message = "逾期利率不能为空")
   @Digits(integer = 10, fraction = 2, message = "逾期利率整数位不能超过10位，小数位不能超过2")
   @ApiModelProperty(value = "逾期利率")
   private BigDecimal overdueRate;


   private Boolean taxFit;//税款账期是否和代杂费一致
   private Boolean goodFit;//货款账期是否和代杂费一致

   @NotEmpty(message = "请选择代理费计算模式")
   private String agencyFeeMode;

}
