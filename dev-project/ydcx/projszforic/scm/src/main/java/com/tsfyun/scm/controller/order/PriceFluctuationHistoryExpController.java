package com.tsfyun.scm.controller.order;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.entity.order.PriceFluctuationHistoryExp;
import com.tsfyun.scm.service.order.IPriceFluctuationHistoryExpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 *
 * @since 2021-09-17
 */
@RestController
@RequestMapping("/priceFluctuationHistoryExp")
public class PriceFluctuationHistoryExpController extends BaseController {

    @Autowired
    private IPriceFluctuationHistoryExpService priceFluctuationHistoryExpService;

    @PostMapping(value = "list")
    public Result<List<PriceFluctuationHistoryExp>> list(@RequestParam(value = "memberId")Long memberId){
        return success(priceFluctuationHistoryExpService.list(memberId));
    }
}

