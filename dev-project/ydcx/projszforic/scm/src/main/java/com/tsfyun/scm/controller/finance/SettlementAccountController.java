package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.finance.SettlementAccountDTO;
import com.tsfyun.scm.dto.finance.SettlementAccountQTO;
import com.tsfyun.scm.service.finance.ISettlementAccountService;
import com.tsfyun.scm.vo.finance.SettlementAccountDetailVO;
import com.tsfyun.scm.vo.finance.SettlementAccountVO;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 结汇主单 前端控制器
 * </p>
 *
 *
 * @since 2021-10-14
 */
@RestController
@RequestMapping("/settlementAccount")
public class SettlementAccountController extends BaseController {

    private final ISettlementAccountService settlementAccountService;

    public SettlementAccountController(ISettlementAccountService settlementAccountService) {
        this.settlementAccountService = settlementAccountService;
    }

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<SettlementAccountVO>> pageList(@ModelAttribute @Valid SettlementAccountQTO qto) {
        PageInfo<SettlementAccountVO> page = settlementAccountService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情查询
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<SettlementAccountDetailVO> detail(@RequestParam(value = "id")Long id,@RequestParam(value = "operation",required = false)String operation) {
        return success(settlementAccountService.detail(id,operation));
    }

    /**
     * 保存结汇
     * @return
     */
    @PostMapping(value = "save")
    @DuplicateSubmit
    public Result<Void> save(@RequestBody @Valid SettlementAccountDTO dto){
        settlementAccountService.saveSettlement(dto);
        return success();
    }

    /**
     * 删除结汇
     * @param id
     * @return
     */
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")Long id){
        settlementAccountService.removeSettlement(id);
        return success();
    }

}

