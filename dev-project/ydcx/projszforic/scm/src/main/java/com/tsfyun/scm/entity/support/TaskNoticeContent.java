package com.tsfyun.scm.entity.support;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 任务通知内容
 * </p>
 *

 * @since 2020-04-05
 */
@Data
public class TaskNoticeContent extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 单据类型
     */

    private String documentType;

    /**
     * 单据id
     */

    private String documentId;

    /**
     * 操作码
     */

    private String operationCode;

    /**
     * 查询参数
     */

    private String queryParams;

    /**
     * 请求参数
     */

    private String actionParams;

    /**
     * 任务内容
     */

    private String content;

    /**
     * 客户id
     */
    private Long customerId;

    /**=
     * 执行后立即删除
     */
    private Boolean isExeRemove;
}
