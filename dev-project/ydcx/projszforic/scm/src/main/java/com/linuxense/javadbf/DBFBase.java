package com.linuxense.javadbf;

public abstract class DBFBase {
    protected String characterSetName = "GBK";
    protected final int END_OF_DATA = 26;

    public DBFBase() {
    }

    public String getCharactersetName() {
        return this.characterSetName;
    }

    public void setCharactersetName(String characterSetName) {
        this.characterSetName = characterSetName;
    }
}
