package com.tsfyun.scm.vo.customer;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 快递模式标准报价计算代理费明细响应实体
 * </p>
 *
 *
 * @since 2020-10-26
 */
@Data
@ApiModel(value="ExpressStandardQuoteMember响应对象", description="快递模式标准报价计算代理费明细响应实体")
public class ExpressStandardQuoteMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;
    /**
     * 报价单id
     */

    private Long quoteId;

    /**
     * 开始重量
     */

    private BigDecimal startWeight;

    /**
     * 结束重量
     */

    private BigDecimal endWeight;

    /**
     * 基准价
     */
    private BigDecimal basePrice;

    /**
     * 单价-KG
     */

    private BigDecimal price;

    /**
     * 封顶标识
     */

    private Boolean capped;

    /**
     * 封顶费用
     */

    private BigDecimal cappedFee;


}
