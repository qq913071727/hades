package com.tsfyun.scm.dto.logistics;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Data
@ApiModel(value="OverseasDeliveryNoteMember请求对象", description="请求实体")
public class OverseasDeliveryNoteMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

    @NotNull(message = "id不能为空",groups = {UpdateGroup.class})
    private Long id;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "名称不能为空")
   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   @LengthTrim(max = 500,message = "规格参数最大长度不能超过500位")
   @ApiModelProperty(value = "规格参数")
   private String spec;

   @LengthTrim(max = 100,message = "批次号最大长度不能超过100位")
   @ApiModelProperty(value = "批次号")
   private String batchNo;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitCode;

   @NotEmptyTrim(message = "单位不能为空")
   @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
   @ApiModelProperty(value = "单位")
   private String unitName;

   @NotNull(message = "出库数量不能为空")
   @Digits(integer = 8, fraction = 2, message = "出库数量整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "出库数量")
   private BigDecimal quantity;

   @NotNull(message = "净重不能为空")
   @Digits(integer = 6, fraction = 4, message = "净重整数位不能超过6位，小数位不能超过4位")
   @ApiModelProperty(value = "净重")
   private BigDecimal netWeight;

   @NotNull(message = "毛重不能为空")
   @Digits(integer = 6, fraction = 4, message = "毛重整数位不能超过6位，小数位不能超过4位")
   @ApiModelProperty(value = "毛重")
   private BigDecimal grossWeight;

   @NotNull(message = "箱数不能为空")
   @LengthTrim(max = 6,message = "箱数最大长度不能超过6位")
   @ApiModelProperty(value = "箱数")
   private Integer cartonNum;

   @LengthTrim(max = 200,message = "箱号最大长度不能超过200位")
   @ApiModelProperty(value = "箱号")
   private String cartonNo;

   @NotNull(message = "单价不能为空")
   @Digits(integer = 6, fraction = 4, message = "单价整数位不能超过6位，小数位不能超过4位")
   @ApiModelProperty(value = "单价")
   private BigDecimal unitPrice;

   @NotNull(message = "总价不能为空")
   @Digits(integer = 8, fraction = 2, message = "总价整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "总价")
   private BigDecimal totalPrice;

   @NotNull(message = "境外送货主单id不能为空")
   @ApiModelProperty(value = "境外送货主单id")
   private Long deliveryNoteId;


}
