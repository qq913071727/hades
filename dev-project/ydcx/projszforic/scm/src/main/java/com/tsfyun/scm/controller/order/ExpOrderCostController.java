package com.tsfyun.scm.controller.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.order.ExpOrderCostQTO;
import com.tsfyun.scm.service.order.IExpOrderCostService;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/expOrderCost")
public class ExpOrderCostController extends BaseController {

    @Autowired
    private IExpOrderCostService expOrderCostService;

    /**
     * 校验订单是否可以调整费用并返回原有费用信息
     * @param expOrderNo
     * @return
     */
    @GetMapping("checkGetAdjustCost")
    public Result<List<ExpOrderCostVO>> checkGetAdjustCost(@RequestParam("expOrderNo")String expOrderNo, @RequestParam("getCost")Boolean getCost){
        return success(expOrderCostService.checkGetAdjustCost(expOrderNo,getCost));
    }

    /**
     * 获取具体某一笔订单费用的信息
     * @param id
     * @return
     */
    @GetMapping("getSimpleCostInfo")
    public Result<ExpOrderCostVO> getSimpleCostInfo(@RequestParam("id")Long id){
        return success(expOrderCostService.getSimpleCostInfo(id));
    }

    /**
     * 出口订单费用列表
     * @param qto
     * @return
     */
    @PostMapping("list")
    public Result<List<ExpOrderCostVO>> pageList(@ModelAttribute ExpOrderCostQTO qto){
        PageInfo<ExpOrderCostVO> pageInfo = expOrderCostService.pageList(qto);
        return success((int) pageInfo.getTotal(),pageInfo.getList());
    }

}
