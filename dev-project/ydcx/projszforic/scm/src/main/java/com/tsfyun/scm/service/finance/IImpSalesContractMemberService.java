package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.scm.entity.finance.ImpSalesContractMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.ImpSalesContractMemberVO;

import java.util.List;

/**
 * <p>
 * 销售合同明细 服务类
 * </p>
 *

 * @since 2020-05-19
 */
public interface IImpSalesContractMemberService extends IService<ImpSalesContractMember> {

    /**
     * 根据销售合同id查询销售合同明细列表
     * @param impSalesContractId
     * @return
     */
    List<ImpSalesContractMemberVO> getMembersBySalesContractId(Long impSalesContractId);

    /**
     * 根据销售合同删除合同明细
     * @param impSalesContractId
     */
    void deleteBySalesContractId(Long impSalesContractId);

    void saveMembers(List<ImpSalesContractMember> members, ImpSalesContract impSalesContract);

}
