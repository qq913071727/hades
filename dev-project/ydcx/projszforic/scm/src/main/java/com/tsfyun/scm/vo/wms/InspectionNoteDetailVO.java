package com.tsfyun.scm.vo.wms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**=
 * 入库单验货详情
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InspectionNoteDetailVO implements Serializable {


    private ReceivingNoteVO receivingNote;

    private List<InspectionNoteMemberVO> members;
}
