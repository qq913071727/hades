package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TransactionFlowDTO;
import com.tsfyun.scm.dto.finance.TransactionFlowQTO;
import com.tsfyun.scm.service.finance.ITransactionFlowService;
import com.tsfyun.scm.vo.finance.TransactionFlowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 交易流水 前端控制器
 * </p>
 *
 * @since 2020-04-16
 */
@RestController
@RequestMapping("/transactionFlow")
public class TransactionFlowController extends BaseController {

    @Autowired
    private ITransactionFlowService transactionFlowService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<TransactionFlowVO>> pageList(@ModelAttribute @Valid TransactionFlowQTO qto) {
        PageInfo<TransactionFlowVO> page = transactionFlowService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 写入交易流水给其他服务调用
     * @return
     */
    @PostMapping(value = "recordTransactionFlow")
    public Result<Void> recordTransactionFlow(@RequestBody @Validated TransactionFlowDTO dto){
        transactionFlowService.recordTransactionFlow(dto);
        return success();
    }
}

