package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.FileDTO;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**=
 * 协议
 */
@Data
public class AgreemenPlusDTO implements Serializable {

    @NotNull(message = "请填写协议基础信息")
    @Valid
    AgreementDTO agreement;

    @Valid
    ImpClauseDTO impClause;

    @Valid
    List<ImpQuoteDTO> impQuotes;

    private FileDTO file;
}
