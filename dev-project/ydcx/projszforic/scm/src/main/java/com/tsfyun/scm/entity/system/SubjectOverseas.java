package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Data
public class SubjectOverseas extends BaseEntity {

     private static final long serialVersionUID=1L;


    private Long subjectId;

    /**
     * 英文名称
     */

    private String name;

    /**
     * 中文名称
     */

    private String nameCn;

    /**
     * 英文地址
     */

    private String address;

    /**
     * 中文地址
     */

    private String addressCn;

    /**
     * 禁用标示
     */

    private Boolean disabled;

    /**
     * 默认
     */

    private Boolean isDefault;

    /**
     * 创建人
     */

    private String createBy;

    /**
     * 传真
     */

    private String fax;

    /**
     * 电话
     */

    private String tel;

    /**=
     * 电子章
     */
    private String chapter;
}
