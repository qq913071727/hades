package com.tsfyun.scm.service.impl.support;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Snowflake;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tsfyun.common.base.enums.MsgContentTypeEnum;
import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.scm.config.ws.WsBiz;
import com.tsfyun.scm.dto.support.TaskNoticeDTO;
import com.tsfyun.scm.dto.support.WsMessageDTO;
import com.tsfyun.scm.entity.support.TaskNotice;
import com.tsfyun.scm.entity.support.TaskNoticeContent;
import com.tsfyun.scm.mapper.support.TaskNoticeMapper;
import com.tsfyun.scm.service.support.ITaskNoticeService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 任务通知 服务实现类
 * </p>
 *

 * @since 2020-04-05
 */
@Service
public class TaskNoticeServiceImpl extends ServiceImpl<TaskNotice> implements ITaskNoticeService {

    @Autowired
    private TaskNoticeMapper taskNoticeMapper;

    @Autowired
    private Snowflake snowflake;

    @Resource
    private WsBiz wsBiz;

    @Override
    public void deleteByTaskNoticeIdBatch(List<Long> taskNoticeIds) {
        if(CollUtil.isNotEmpty(taskNoticeIds)) {
            taskNoticeMapper.deleteByExample(Example.builder(TaskNotice.class).where(TsfWeekendSqls.<TaskNotice>custom()
                    .andIn(false, TaskNotice::getTaskNoticeId, taskNoticeIds)).build());
        }
    }

    @Override
    public void addBatch(TaskNoticeContent taskNoticeContent, LoginVO loginVO, List<TaskNoticeDTO> datas) {
        if(CollUtil.isNotEmpty(datas)) {
            Set<Long> userIdSets = Sets.newHashSet();
            List<TaskNotice> list = Lists.newArrayListWithExpectedSize(datas.size());
            datas.stream().forEach(r->{
                if(!userIdSets.contains(r.getReceiverId())){
                    TaskNotice taskNotice = new TaskNotice();
                    taskNotice.setId(snowflake.nextId());
                    taskNotice.setOperationCode(r.getOperationCode());
                    taskNotice.setTaskNoticeId(taskNoticeContent.getId());
                    taskNotice.setReceiverId(r.getReceiverId());
                    taskNotice.setReceiverName(r.getReceiverName());
                    taskNotice.setSenderId(Objects.isNull(r.getSenderId()) ? loginVO.getPersonId() : r.getSenderId());
                    taskNotice.setSenderName(Objects.isNull(r.getSenderId()) ? loginVO.getPersonName() : r.getSenderName());
                    userIdSets.add(r.getReceiverId());
                    list.add(taskNotice);
                }
            });
            super.savaBatch(list);

            //发送消息通知客户端
            WsMessageDTO wsMessageDTO = beanMapper.map(taskNoticeContent,WsMessageDTO.class);
            wsMessageDTO.setUserIds(new ArrayList<>(userIdSets));
            wsMessageDTO.setContentType(MsgContentTypeEnum.TEXT.getCode());
            wsBiz.send2Client(wsMessageDTO);
        }
    }

    @Override
    public List<Long> findByPersonId(List<Long> tncIds) {
        return taskNoticeMapper.findByPersonId(tncIds);
    }

    @Override
    public void deleteByOperationCodeAndPersonId(String operationCode, Long personId) {
        TsfWeekendSqls wheres = TsfWeekendSqls.<TaskNotice>custom().andEqualTo(false,TaskNotice::getOperationCode,operationCode)
                .andEqualTo(false,TaskNotice::getReceiverId,personId);
        taskNoticeMapper.deleteByExample(Example.builder(TaskNotice.class).where(wheres).build());
    }
}
