package com.tsfyun.scm.util;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.common.base.enums.BaseDataTypeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.SpringBeanUtils;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.common.base.vo.BaseDataSimple;
import com.tsfyun.scm.client.BaseDataClient;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 海关操作工具类
 * @since Created in 2020/4/26 16:57
 */
public class CustomsUtil {

    /**
     * 校验海关基础数据
     * @param baseDataTypeEnum
     * @param decDataMap
     * @param val
     * @param configName
     * @return
     */
    public static BaseDataSimple checkDeclaraBaseData(BaseDataTypeEnum baseDataTypeEnum, Map<String, List<BaseDataSimple>> decDataMap,String val,String configName) {
        List<BaseDataSimple> baseDataSimples = decDataMap.get(baseDataTypeEnum.getCode());
        TsfPreconditions.checkArgument(CollUtil.isNotEmpty(baseDataSimples),new ServiceException(String.format("%s数据未配置",baseDataTypeEnum.getName())));
        Map<String,BaseDataSimple> baseDataSimpleMap = baseDataSimples.stream().collect(Collectors.toMap(item->item.getDecCode(), item->item));
        TsfPreconditions.checkArgument(baseDataSimpleMap.containsKey(val),new ServiceException(String.format("%s不存在", StringUtils.isNotEmpty(configName) ?  configName : baseDataTypeEnum.getName())));
        return baseDataSimpleMap.get(val);
    }

    /**
     * 校验海关基础数据
     * @param baseDataTypeEnum
     * @param decDataMap
     * @param val
     * @return
     */
    public static BaseDataSimple checkDeclaraBaseData(BaseDataTypeEnum baseDataTypeEnum, Map<String, List<BaseDataSimple>> decDataMap,String val) {
        return checkDeclaraBaseData(baseDataTypeEnum,decDataMap,val,null);
    }

}
