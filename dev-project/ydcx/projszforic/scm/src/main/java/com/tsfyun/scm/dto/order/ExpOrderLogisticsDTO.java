package com.tsfyun.scm.dto.order;

import java.util.Date;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.scm.enums.DeliveryDestinationEnum;
import com.tsfyun.scm.enums.DeliveryMethodEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 出口订单物流信息请求实体
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Data
@ApiModel(value="ExpOrderLogistics请求对象", description="出口订单物流信息请求实体")
public class ExpOrderLogisticsDTO implements Serializable {

   private static final long serialVersionUID=1L;


   @ApiModelProperty(value = "订单物流id")
   private Long id;

   @ApiModelProperty(value = "订单id")
   private Long expOrderId;

   @LengthTrim(max = 30,message = "国内交货方式最大长度不能超过30位")
   @ApiModelProperty(value = "国内交货方式")
   @NotEmptyTrim(message = "请选择国内交货方式")
   private String deliveryMode;

   @LengthTrim(max = 100,message = "国内交货快递单号最大长度不能超过100位")
   @ApiModelProperty(value = "国内交货快递单号")
   private String deliveryExpressNo;

   @LengthTrim(max = 20,message = "国内提货方式最大长度不能超过20位")
   @ApiModelProperty(value = "国内提货方式")
   @EnumCheck(clazz = DeliveryMethodEnum.class,message = "国内提货方式类型错误")
   private String deliveryMethod;

   @ApiModelProperty(value = "要求提货时间")
   private Date takeTime;

   @ApiModelProperty(value = "国内提货信息")
   private Long customerDeliveryInfoId;

   @LengthTrim(max = 200,message = "提货公司最大长度不能超过200位")
   @ApiModelProperty(value = "提货公司")
   private String deliveryCompanyName;

   @LengthTrim(max = 50,message = "提货联系人最大长度不能超过50位")
   @ApiModelProperty(value = "提货联系人")
   private String deliveryLinkPerson;

   @LengthTrim(max = 50,message = "提货联系人电话最大长度不能超过50位")
   @ApiModelProperty(value = "提货联系人电话")
   private String deliveryLinkTel;

   @LengthTrim(max = 255,message = "提货地址最大长度不能超过255位")
   @ApiModelProperty(value = "提货地址")
   private String deliveryAddress;

   @LengthTrim(max = 500,message = "交货备注最大长度不能超过500位")
   @ApiModelProperty(value = "交货备注")
   private String deliveryModeMemo;

   @ApiModelProperty(value = "中港包车")
   @NotNull(message = "请选择是否包车")
   private Boolean isCharterCar;

   @LengthTrim(max = 20,message = "送货目的地最大长度不能超过20位")
   @ApiModelProperty(value = "送货目的地")
   @EnumCheck(clazz = DeliveryDestinationEnum.class,message = "送货目的地类型错误")
   private String deliveryDestination;

   @ApiModelProperty(value = "境外送货信息ID")
   private Long supplierTakeInfoId;

   @LengthTrim(max = 255,message = "收货公司最大长度不能超过255位")
   @ApiModelProperty(value = "收货公司")
   private String takeLinkCompany;

   @LengthTrim(max = 50,message = "收货联系人最大长度不能超过50位")
   @ApiModelProperty(value = "收货联系人")
   private String takeLinkPerson;

   @LengthTrim(max = 50,message = "收货联系人电话最大长度不能超过50位")
   @ApiModelProperty(value = "收货联系人电话")
   private String takeLinkTel;

   @LengthTrim(max = 255,message = "收货地址最大长度不能超过255位")
   @ApiModelProperty(value = "收货地址")
   private String takeAddress;

   @LengthTrim(max = 500,message = "收货备注最大长度不能超过500位")
   @ApiModelProperty(value = "收货备注")
   private String receivingModeMemo;

}
