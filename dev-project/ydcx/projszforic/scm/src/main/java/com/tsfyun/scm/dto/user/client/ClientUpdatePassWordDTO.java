package com.tsfyun.scm.dto.user.client;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 客户端修改密码
 */
@Data
public class ClientUpdatePassWordDTO implements Serializable {

    @NotEmptyTrim(message = "手机号码不能为空")
    @Pattern(regexp = "^[1][0-9]{10}$",message = "手机号格式错误")
    private String phoneNo;

    @NotEmptyTrim(message = "短信验证码不能为空")
    @LengthTrim(min = 4,max = 4,message = "请输入4位短信验证码")
    private String validateCode;

    @NotEmptyTrim(message = "新密码不能为空")
    private String password;

}
