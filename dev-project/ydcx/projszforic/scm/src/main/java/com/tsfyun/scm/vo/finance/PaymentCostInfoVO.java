package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PaymentCostInfoVO implements Serializable {

    //收款未核销金额
    private BigDecimal receivablesVal;
    //付款单人民币金额
    private BigDecimal accountValueCny;
    //本次付汇应支付金额
    private BigDecimal paymentAmountCny;
    //逾期金额
    private BigDecimal totalArrearsVal;
    //当前客户可用货款额度
    private BigDecimal goodOccupyVal;

    //是否通过
    private Boolean isAdopt;
    //原因
    private String reason;
    //风控是否通过
    private Boolean fkAdopt;
    //风控意见
    private String riskControl;
}
