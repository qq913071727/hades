package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class DeliveryDepartureDTO implements Serializable {

    @NotNull(message = "运单ID不能为空")
    private Long id;

    @NotNull(message = "发车时间不能为空")
    private LocalDateTime departureDate;

    @LengthTrim(max = 50,message = "物流单号最大长度不能大于50")
    private String experssNo;

    @LengthTrim(max = 50,message = "封条号最大长度不能大于50")
    private String sealNo;
}
