package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.finance.ImpSalesContractQTO;
import com.tsfyun.scm.entity.finance.ImpSalesContract;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.ImpSalesContractVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 销售合同 Mapper 接口
 * </p>
 *

 * @since 2020-05-19
 */
@Repository
public interface ImpSalesContractMapper extends Mapper<ImpSalesContract> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ImpSalesContractVO> list(Map<String,Object> params);

    void updateInvoiceNo(@Param(value = "id")Long id,@Param(value = "salesContractNo")String salesContractNo,@Param(value = "invoiceNo")String invoiceNo);

    ImpSalesContractVO selectById(@Param(value = "id")Long id);

}
