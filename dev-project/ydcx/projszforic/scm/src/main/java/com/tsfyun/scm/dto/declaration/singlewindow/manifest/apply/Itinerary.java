package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 承运人数据
 * @since Created in 2020/4/22 11:13
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Itinerary", propOrder = {
        "routingCountryCode",
})
public class Itinerary {

    //承运人代码
    @XmlElement(name = "RoutingCountryCode")
    protected String routingCountryCode;

    public Itinerary()  {

    }

    public Itinerary(String routingCountryCode) {
        this.routingCountryCode = routingCountryCode;
    }


}
