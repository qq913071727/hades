package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 发票明细响应实体
 * </p>
 *

 * @since 2020-05-18
 */
@Data
@ApiModel(value="InvoiceMember响应对象", description="发票明细响应实体")
public class InvoiceMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "行号")
    private Integer rowNo;

    @ApiModelProperty(value = "发票id")
    private Long invoiceId;

    @ApiModelProperty(value = "物料型号")
    private String goodsModel;

    @ApiModelProperty(value = "物料名称")
    private String goodsName;

    @ApiModelProperty(value = "物料品牌")
    private String goodsBrand;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "单位编码")
    private String unitCode;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "税收分类编码")
    private String taxCode;

    @ApiModelProperty(value = "开票名称")
    private String invoiceName;

    /**
     * 税收分类名称
     */
    private String taxCodeName;

    /**
     * 首次开
     */
    private Boolean isFirst;


}
