package com.tsfyun.scm.service.impl.wms;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.tsfyun.scm.dto.wms.InspectionMemberDTO;
import com.tsfyun.scm.dto.wms.ReceivingMemberSaveDTO;
import com.tsfyun.scm.entity.wms.ReceivingNote;
import com.tsfyun.scm.entity.wms.ReceivingNoteMember;
import com.tsfyun.scm.mapper.wms.ReceivingNoteMemberMapper;
import com.tsfyun.scm.service.wms.IReceivingNoteMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.wms.InspectionNoteDetailVO;
import com.tsfyun.scm.vo.wms.InspectionNoteMemberVO;
import com.tsfyun.scm.vo.wms.ReceivingNoteMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 入库单明细 服务实现类
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Service
public class ReceivingNoteMemberServiceImpl extends ServiceImpl<ReceivingNoteMember> implements IReceivingNoteMemberService {

    @Autowired
    private ReceivingNoteMemberMapper receivingNoteMemberMapper;

    @Override
    public List<ReceivingNoteMemberVO> findByReceivingNoteId(@NonNull Long receivingNoteId) {
        ReceivingNoteMember condition = new ReceivingNoteMember();
        condition.setReceivingNoteId(receivingNoteId);
        List<ReceivingNoteMember> list = super.list(condition);
        if(CollUtil.isNotEmpty(list)) {
            //按型号升序排序
            return beanMapper.mapAsList(list,ReceivingNoteMemberVO.class).stream().sorted(Comparator.comparing(ReceivingNoteMemberVO::getModel)).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<InspectionNoteMemberVO> inspectionMembers(Long id) {
        return receivingNoteMemberMapper.inspectionMembers(id);
    }

    @Override
    public ReceivingMemberSaveDTO checkWrapReceivingMember(ReceivingNote receivingNote, List<InspectionMemberDTO> members) {
        List<ReceivingNoteMember> saveMemberList = Lists.newArrayList();
        //订单原始验货明细数据

        return null;
    }
}
