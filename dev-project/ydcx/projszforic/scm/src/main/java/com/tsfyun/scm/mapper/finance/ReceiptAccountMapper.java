package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.finance.ReceiptAccount;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.ReceiptAccountVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 收款单 Mapper 接口
 * </p>
 *

 * @since 2020-04-15
 */
@Repository
public interface ReceiptAccountMapper extends Mapper<ReceiptAccount> {


    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    List<ReceiptAccountVO> list(Map<String,Object> params);

    ReceiptAccountVO detail(@Param(value = "id")Long id);

    BigDecimal obtainCustomerReceipts(@Param(value = "customerId")Long customerId);
    //根据客户获取可核销收款单
    List<ReceiptAccount> canWriteOffList(@Param(value = "customerId")Long customerId);

    Integer cancelWriteValue(@Param(value = "id")Long id,@Param(value = "writeValue")BigDecimal writeValue);

    Integer writeValue(@Param(value = "id")Long id,@Param(value = "writeValue")BigDecimal writeValue);

}
