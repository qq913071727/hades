package com.tsfyun.scm.service.wms;

import com.tsfyun.scm.entity.wms.DeliveryNoteMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.wms.DeliveryNoteMemberVO;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IDeliveryNoteMemberService extends IService<DeliveryNoteMember> {

    /**
     * 根据出库单获取出库单详情
     * @param deliveryNoteId
     * @return
     */
    List<DeliveryNoteMemberVO> findByDeliveryNoteId(@NonNull Long deliveryNoteId);

    /**
     * 根据入库单明细查询出库单
     * @param receivingNoteMemberId
     * @return
     */
    DeliveryNoteMember findByReceivingNoteMemberId(Long receivingNoteMemberId);

}
