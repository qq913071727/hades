package com.tsfyun.scm.vo.materiel;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.enums.domain.MaterielStatusEnum;
import com.tsfyun.scm.entity.materiel.Materiel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 响应实体
 * </p>
 *
 * @since 2020-03-22
 */
@Data
@ApiModel(value="Materiel响应对象", description="响应实体")
public class MaterielVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "客户")
    private String customerName;

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    @ApiModelProperty(value = "CIQ编码")
    private String ciqNo;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "申报要素")
    private String elements;

    @ApiModelProperty(value = "规格参数")
    private String spec;

    @ApiModelProperty(value = "是否涉证")
    private Boolean isSmp;

    @ApiModelProperty(value = "需要3C证书")
    private Boolean isCapp;
    @ApiModelProperty(value = "需要3C目录鉴定")
    private Boolean isCappNo;
    @ApiModelProperty(value = "3C证书编号")
    private String cappNo;

    @ApiModelProperty(value = "归类备注")
    private String memo;

    @ApiModelProperty(value = "来源")
    private String source;

    @ApiModelProperty(value = "来源标识")
    private String sourceMark;

    @ApiModelProperty(value = "匹配方式(待，未，标，智)")
    private String mate;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "状态名称")
    private String statusName;

    @ApiModelProperty(value = "是否新增")
    private Boolean isAdd;

    @ApiModelProperty(value = "商务人员")
    private String busPersonName;

    //赋值状态名称
    private void setStatusId(String statusId){
        MaterielStatusEnum status = MaterielStatusEnum.of(statusId);
        if(Objects.nonNull(status)){
            this.statusId = statusId;
            this.statusName = status.getName();
        }
    }

    @ApiModelProperty(value = "归类人员")
    private String classifyPerson;

    @ApiModelProperty(value = "提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "最后归类时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime classifyTime;

    public MaterielVO(){}

    public MaterielVO(MaterielVO materiel){
        this.id = materiel.id;
        this.customerId = materiel.customerId;
        this.customerName = materiel.customerName;
        this.hsCode = materiel.hsCode;
        this.ciqNo = materiel.ciqNo;
        this.model = materiel.model;
        this.brand = materiel.brand;
        this.name = materiel.name;
        this.elements = materiel.elements;
        this.spec = materiel.spec;
        this.isSmp = materiel.isSmp;
        this.isCapp = materiel.isCapp;
        this.isCappNo = materiel.isCappNo;
        this.cappNo = materiel.cappNo;
        this.memo = materiel.memo;
        this.source = materiel.source;
        this.sourceMark = materiel.sourceMark;
        this.mate = materiel.mate;
        this.statusId = materiel.statusId;
        this.statusName = materiel.statusName;
        this.isAdd = materiel.isAdd;
        this.classifyPerson = materiel.classifyPerson;
        this.busPersonName = materiel.busPersonName;
        this.dateCreated = materiel.dateCreated;
        this.classifyTime = materiel.classifyTime;
    }
}
