package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.finance.SettlementAccountDTO;
import com.tsfyun.scm.dto.finance.SettlementAccountQTO;
import com.tsfyun.scm.entity.finance.SettlementAccount;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.SettlementAccountDetailVO;
import com.tsfyun.scm.vo.finance.SettlementAccountVO;


/**
 * <p>
 * 结汇主单 服务类
 * </p>
 *
 *
 * @since 2021-10-14
 */
public interface ISettlementAccountService extends IService<SettlementAccount> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<SettlementAccountVO> pageList(SettlementAccountQTO qto);

    /**
     * 详情
     * @param id
     * @param operation
     * @return
     */
    SettlementAccountDetailVO detail(Long id,String operation);

    /**
     * 保存结汇
     * @param dto
     */
    void saveSettlement(SettlementAccountDTO dto);

    /**
     * 删除结汇
     * @param id
     */
    void removeSettlement(Long id);

    /**
     * 根据收款单获取对应的结汇单
     * @param accountId
     * @return
     */
    SettlementAccount findByOverseasReceivingAccountId(Long accountId);

}
