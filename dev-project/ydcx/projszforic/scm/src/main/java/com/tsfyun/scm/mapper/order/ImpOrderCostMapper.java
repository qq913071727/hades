package com.tsfyun.scm.mapper.order;

import com.tsfyun.common.base.util.LocalDateTimeUtils;
import com.tsfyun.scm.entity.order.ImpOrderCost;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.order.ImpOrderCostVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单费用 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-21
 */
@Repository
public interface ImpOrderCostMapper extends Mapper<ImpOrderCost> {

    BigDecimal orderCostTotal(@Param(value = "orderId") Long orderId);

    List<ImpOrderCostVO> obtainOrderNotLockCosts(@Param(value = "orderId") Long orderId);

    List<ImpOrderCostVO> orderCostList(@Param(value = "orderId") Long orderId);

    BigDecimal orderCostPayTotal(@Param(value = "orderId") Long orderId);

    BigDecimal customerArrearsTotal(@Param(value = "customerId")Long customerId);

    BigDecimal customerTaxTotal(@Param(value = "customerId")Long customerId);

    BigDecimal customerGoodsTotal(@Param(value = "customerId")Long customerId);

    Integer impLockCost(@Param(value = "orderId") Long orderId);
    Integer impUnLockCost(@Param(value = "costIds") List<Long> costIds);
    //获取客户可核销的费用
    List<ImpOrderCostVO> canWriteOffList(@Param(value = "customerId")Long customerId);
    //根据订单查询可核销的费用
    List<ImpOrderCostVO> canWriteOffByOrderId(@Param(value = "orderId")Long orderId);
    //获取订单所有费用
    List<ImpOrderCostVO> getAllOrderCosts(@Param(value = "orderId") Long orderId,@Param(value = "impOrderNo") String impOrderNo);
    //非系统生成的费用科目
    List<ImpOrderCost> findByNonAutomaticZfList(@Param(value = "orderId") Long orderId);
    //批量修改账期
    Integer batchUpdatePeriodByIds(@Param(value = "ids")List<Long> ids,@Param(value = "happenDate")LocalDateTime happenDate,@Param(value = "periodDate")LocalDateTime periodDate,@Param(value = "lateFeeDate")LocalDateTime lateFeeDate,@Param(value = "overdueRate")BigDecimal overdueRate);
}
