package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 集装箱信息
 * @since Created in 2020/4/22 12:17
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransportEquipment", propOrder = {
        "id",
        "characteristicCode",
        "fullnessCode",
        "supplierPartyTypeCode",
})
public class TransportEquipment {

    //集装箱（器）编号
    //由大写字母或数字构成，只允许出现一次“-”且不能作为开头和结尾
    @XmlElement(name = "ID")
    private String id;

    //集装箱尺寸和类型，参照CN007
    /*
    代码最大长度：4位，由大写字母与数字组成
    <!--该代码前2位为集装箱（器）尺寸代码，后2位为箱型代码。-->
	<!--第1位：用数字或拉丁字母表示箱长。-->
	<!--第2位：用数字或拉丁字母表示箱宽和箱高。-->
	<!--第3位：用拉丁字母表示箱型。-->
	<!--第4位：用数字表示该型箱的特征。-->
     */
    @XmlElement(name = "CharacteristicCode")
    private String characteristicCode;

    //集装箱重箱或空箱标识代码
    /**
     * 1位数字组成，参考CN016
     */
    @XmlElement(name = "FullnessCode")
    private String fullnessCode;

    //集装箱（器）来源代码
    /**
     * 参考CN015
     */
    @XmlElement(name = "SupplierPartyTypeCode")
    private String supplierPartyTypeCode;

    public TransportEquipment () {

    }

    public TransportEquipment (String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCharacteristicCode() {
        return characteristicCode;
    }

    public void setCharacteristicCode(String characteristicCode) {
        this.characteristicCode = characteristicCode;
    }

    public String getFullnessCode() {
        return fullnessCode;
    }

    public void setFullnessCode(String fullnessCode) {
        this.fullnessCode = fullnessCode;
    }

    public String getSupplierPartyTypeCode() {
        return supplierPartyTypeCode;
    }

    public void setSupplierPartyTypeCode(String supplierPartyTypeCode) {
        this.supplierPartyTypeCode = supplierPartyTypeCode;
    }
}
