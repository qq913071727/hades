package com.tsfyun.scm.base.support;

import com.google.common.collect.ImmutableMap;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.common.base.support.MessageOperation;
import com.tsfyun.common.base.support.OperationEnum;
import com.tsfyun.scm.enums.WelinkMsgTemplate;

import java.util.Map;

public class WelinkMsgOperation implements MessageOperation {

    /**
     * 微网通联模板和操作映射
     */
    private static Map<MessageNodeEnum, OperationEnum> operationMap = ImmutableMap.<MessageNodeEnum, OperationEnum>builder()

            //通用认证
            .put(MessageNodeEnum.COMMON_PROVE, WelinkMsgTemplate.PROVE)
            //注册登录
            .put(MessageNodeEnum.REGISTER_LOGIN,WelinkMsgTemplate.PROVE)
            //找回密码
            .put(MessageNodeEnum.RETRIEVE_PASSWORD,WelinkMsgTemplate.PROVE)
            //解绑手机
            .put(MessageNodeEnum.UNBIND_PHONE,WelinkMsgTemplate.CHANGE_IMPORTANT)
            //绑定手机
            .put(MessageNodeEnum.BIND_PHONE,WelinkMsgTemplate.CHANGE_IMPORTANT)

            //付汇审核退回修改
            .put(MessageNodeEnum.PAY_BACK,WelinkMsgTemplate.PAY_BACK)
            //付汇审核退回作废
            .put(MessageNodeEnum.PAY_INVALID,WelinkMsgTemplate.PAY_INVALID)
            //付汇完成
            .put(MessageNodeEnum.PAY_COMPLETE,WelinkMsgTemplate.PAY_COMPLETE)

            //境外发车
            .put(MessageNodeEnum.WAYBILL_DEPARTURE,WelinkMsgTemplate.WAYBILL_DEPARTURE)

            //到达深圳仓
            .put(MessageNodeEnum.WAYBILL_ARRIVE,WelinkMsgTemplate.WAYBILL_ARRIVE)

            //客户付款财务已确认
            .put(MessageNodeEnum.CUSTOMER_PAY,WelinkMsgTemplate.CUSTOMER_PAY)

            //订单退回修改
            .put(MessageNodeEnum.ORDER_BACK,WelinkMsgTemplate.ORDER_BACK)

            //客户审核通过
            .put(MessageNodeEnum.CUSTOMER_APROVED,WelinkMsgTemplate.CUSTOMER_APROVED)

            //客户审核未通过待完善资料
            .put(MessageNodeEnum.CUSTOMER_REJECT,WelinkMsgTemplate.CUSTOMER_REJECT)

            .build();

    @Override
    public ShortMessagePlatformEnum getPlatform() {
        return ShortMessagePlatformEnum.ALIYUN;
    }

    @Override
    public Map<MessageNodeEnum, OperationEnum> getMessageTemplate() {
        return operationMap;
    }

    /**
     * 内部类单例模式
     */
    private static class WelinkMsgOperationInstance{
        private static final WelinkMsgOperation instance = new WelinkMsgOperation();
    }

    private WelinkMsgOperation(){}

    public static WelinkMsgOperation getInstance(){
        return WelinkMsgOperation.WelinkMsgOperationInstance.instance;
    }


}

