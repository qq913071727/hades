package com.tsfyun.scm.mapper.third;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.third.LogSms;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-09-16
 */
@Repository
public interface LogSmsMapper extends Mapper<LogSms> {

    Integer countOneMinute(@Param(value = "phoneNo") String phoneNo,@Param(value = "messageNode") String messageNode);

    Integer countByIpAndMinute(@Param(value = "ip") String ip,@Param(value = "minute") Integer minute);

    String selectCodeNew(@Param(value = "phoneNo") String phoneNo,@Param(value = "messageNode") String messageNode,@Param(value = "minute") Integer minute);
}
