package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

/**
 * @Description: 采购合同
 * @CreateDate: Created in 2021/9/27 16:44
 */
@Data
public class PurchaseContractQTO extends PaginationDto {

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 订单号
     */

    private String orderNo;

    /**
     * 发票号
     */
    private String invoiceNo;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 退税状态
     */
    private String drawbackStatus;

    /**
     * 客户
     */

    private String customerName;


    /**=
     * 日期查询类型
     */
    @Pattern(regexp = "signing_date|invoice_date|date_created",message = "日期查询类型错误")
    private String queryDate;

    /**
     * 下单开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 下单结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

}
