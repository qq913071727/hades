package com.tsfyun.scm.entity.logistics;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class TransportSupplier extends BaseEntity {

     private static final long serialVersionUID=1L;


    private String address;

    /**
     * 禁用启用
     */

    private Boolean disabled;

    /**
     * 传真
     */

    private String fax;

    /**
     * 联系人
     */

    private String linkPerson;

    /**
     * 备注
     */

    private String memo;

    /**
     * 供应商名称
     */

    private String name;

    /**
     * 电话
     */

    private String tel;



}
