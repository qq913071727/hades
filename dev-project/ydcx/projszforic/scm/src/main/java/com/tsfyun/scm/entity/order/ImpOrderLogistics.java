package com.tsfyun.scm.entity.order;

import java.time.LocalDateTime;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 订单物流信息
 * </p>
 *

 * @since 2020-04-08
 */
@Data
public class ImpOrderLogistics {

     private static final long serialVersionUID=1L;

     @Id
     private Long id;

    private Long impOrderId;

    /**
     * 境外交货方式
     */

    private String deliveryMode;

    /**
     * 境外交货方式
     */

    private String deliveryModeName;

    /**
     * 交货备注
     */

    private String deliveryModeMemo;

    /**
     * 快递类型
     */

    private String inExpressType;

    /**
     * 快递公司
     */

    private String hkExpress;


    private String hkExpressName;


    private String hkExpressNo;

    /**
     * 要求提货时间
     */

    private LocalDateTime takeTime;

    /**=
     * 供应商提货信息ID
     */
    private Long supplierTakeInfoId;

    /**
     * 提货公司
     */

    private String takeLinkCompany;

    /**
     * 提货联系人
     */

    private String takeLinkPerson;

    /**
     * 提货联系人电话
     */

    private String takeLinkTel;

    /**
     * 提货地址
     */

    private String takeAddress;

    /**
     * 中港包车
     */

    private Boolean isCharterCar;

    /**
     * 国内收货方式
     */

    private String receivingMode;

    /**
     * 国内收货方式
     */

    private String receivingModeName;

    /**
     * 收货备注
     */

    private String receivingModeMemo;

    /**
     * 物流公司
     */

    private Long logisticsCompanyId;

    /**
     * 物流公司
     */

    private String logisticsCompanyName;

    /**
     * 物流时效要求
     */

    private String prescription;


    /**
     * 送货付款方式
     */

    private String paymentDelivery;

    /**
     * 提货仓库
     */

    private Long selfWareId;

    /**
     * 提货仓库名称
     */

    private String selfWareName;

    /**
     * 提货联系人电话
     */

    private String selfLinkPerson;

    /**
     * 提货人身份证号
     */

    private String selfPersonId;

    /**
     * 提货备注
     */

    private String selfMemo;

    /**
     * 收货信息
     */
    private Long customerDeliveryInfoId;

    /**
     * 收货公司
     */

    private String deliveryCompanyName;

    /**
     * 收货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 收货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 收货地址
     */

    private String deliveryAddress;

    /**
     * 是否完成国内提货
     */
    private Boolean overseasTakeFlag;

    /**
     * 国内提货时间
     */
    private LocalDateTime overseasTakeTime;


}
