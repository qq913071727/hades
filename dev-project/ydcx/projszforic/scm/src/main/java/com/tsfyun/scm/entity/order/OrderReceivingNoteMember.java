package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-04-29
 */
@Data
public class OrderReceivingNoteMember implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**=
     * 订单号
     */
    private String orderNo;

    /**
     * 订单明细
     */

    private Long orderMemberId;

    /**=
     * 入库单号
     */
    private String receivingNo;

    /**
     * 入库明细
     */

    private Long receivingNoteMemberId;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**=
     * 锁定（订单确定绑单完成）
     */
    private Boolean isLock;

    /**
     * 是否已出库
     */

    private Boolean isOutStock;


}
