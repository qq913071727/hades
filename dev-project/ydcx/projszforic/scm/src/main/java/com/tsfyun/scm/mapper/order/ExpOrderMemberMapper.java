package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.ExpOrderMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.order.PriceFluctuationHistoryExp;
import com.tsfyun.scm.vo.order.ExpOrderMemberVO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 出口订单明细 Mapper 接口
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Repository
public interface ExpOrderMemberMapper extends Mapper<ExpOrderMember> {

    Integer savaAndUpdateBatch(@Param(value = "expOrderMemberList") List<ExpOrderMember> expOrderMemberList);

    List<ExpOrderMemberVO> findOrderMaterielMember(@Param(value = "expOrderId")Long expOrderId);

    List<PriceFluctuationHistoryExp> findExpHistory(@Param(value = "dateTime")LocalDateTime dateTime,@Param(value = "model") String model,@Param(value = "brand") String brand,@Param(value = "name") String name,@Param(value = "spec") String spec);

    //根据物料ID修改物料ID
    Integer updateMemberMaterielId(@Param(value = "oldId")String oldId, @Param(value = "newId")String newId);

    List<ExpOneVoteTheEndVO> oneVoteTheEndByOrderId(@Param(value = "orderId")Long orderId);

    //获取订单总数量
    BigDecimal getOrderQuantity(@Param(value = "orderNo") String orderNo);
}
