package com.tsfyun.scm.service.wms;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.wms.DeliveryNoteQTO;
import com.tsfyun.scm.entity.wms.DeliveryNote;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.wms.DeliveryNotePlusDetailVO;
import com.tsfyun.scm.vo.wms.DeliveryNoteVO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 出库单 服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IDeliveryNoteService extends IService<DeliveryNote> {

    /**
     * 进口出库分页列表
     * @param qto
     * @return
     */
    PageInfo<DeliveryNoteVO> pageList(DeliveryNoteQTO qto);

    /**
     * 出库单详情（支持操作页面获取详情状态验证）
     * @param id
     * @param operation
     * @return
     */
    DeliveryNotePlusDetailVO detail(Long id,String operation);

    /**=
     * 根据订单号生成境外出库单
     * @param orderNo
     * @param deliveryDate
     */
    void overseasDeliveryOrder(List<String> orderNo, LocalDateTime deliveryDate);

    /**
     * 根据入库单明细删除出库单
     * @param receivingNoteMemberIds
     */
    void removeByReceivingNoteMember(List<Long> receivingNoteMemberIds);
}
