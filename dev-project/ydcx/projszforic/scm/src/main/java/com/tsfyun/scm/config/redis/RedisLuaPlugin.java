package com.tsfyun.scm.config.redis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

@Configuration
public class RedisLuaPlugin {

    /**
     * Method Description
     * 登出脚本
     * @return null
     *
     * @date 2019/12/2 9:43
     */
    @Bean(name = "logoutRedisScript")
    public DefaultRedisScript logoutRedisScript() {
        DefaultRedisScript rateLimitRedisScript = new DefaultRedisScript();
        rateLimitRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("/META-INF/scripts/logout.lua")));
        rateLimitRedisScript.setResultType(Long.class);
        return rateLimitRedisScript;
    }


}
