package com.tsfyun.scm.vo.materiel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 归类变更日志响应实体
 * </p>
 *
 *
 * @since 2020-03-26
 */
@Data
@ApiModel(value="MaterielLog响应对象", description="归类变更日志响应实体")
public class MaterielLogVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "变更内容")
    private String changeContent;

    @ApiModelProperty(value = "物料ID")
    private String materielId;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateUpdated;

    @ApiModelProperty(value = "修改人")
    private String updateBy;

    public void setUpdateBy(String updateBy){
        if(StringUtils.isNotEmpty(updateBy)){
            this.updateBy = updateBy;
            if(updateBy.indexOf("/")!=-1){
                this.updateBy = updateBy.substring(updateBy.indexOf("/")+1);
            }
        }
    }

}
