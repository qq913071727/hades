package com.tsfyun.scm.base.support;

import com.google.common.collect.ImmutableMap;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.common.base.support.MessageOperation;
import com.tsfyun.common.base.support.OperationEnum;
import com.tsfyun.scm.enums.AliYunMsgTemplate;

import java.util.Map;

public class AliyunMsgOperation implements MessageOperation {

    /**
     * 阿里云模板和操作映射
     */
    private static Map<MessageNodeEnum, OperationEnum> operationMap = ImmutableMap.<MessageNodeEnum, OperationEnum>builder()

            //通用认证
            .put(MessageNodeEnum.COMMON_PROVE, AliYunMsgTemplate.PROVE)
            //注册登录
            .put(MessageNodeEnum.REGISTER_LOGIN,AliYunMsgTemplate.PROVE)
            //找回密码
            .put(MessageNodeEnum.RETRIEVE_PASSWORD,AliYunMsgTemplate.PROVE)
            //解绑手机
            .put(MessageNodeEnum.UNBIND_PHONE,AliYunMsgTemplate.CHANGE_IMPORTANT)
            //绑定手机
            .put(MessageNodeEnum.BIND_PHONE,AliYunMsgTemplate.CHANGE_IMPORTANT)

            //付汇审核退回修改
            .put(MessageNodeEnum.PAY_BACK,AliYunMsgTemplate.PAY_BACK)
            //付汇审核退回作废
            .put(MessageNodeEnum.PAY_INVALID,AliYunMsgTemplate.PAY_INVALID)
            //付汇完成
            .put(MessageNodeEnum.PAY_COMPLETE,AliYunMsgTemplate.PAY_COMPLETE)

            //境外发车
            .put(MessageNodeEnum.WAYBILL_DEPARTURE,AliYunMsgTemplate.WAYBILL_DEPARTURE)

            //到达深圳仓
            .put(MessageNodeEnum.WAYBILL_ARRIVE,AliYunMsgTemplate.WAYBILL_ARRIVE)

            //客户付款财务已确认
            .put(MessageNodeEnum.CUSTOMER_PAY,AliYunMsgTemplate.CUSTOMER_PAY)

            //订单退回修改
            .put(MessageNodeEnum.ORDER_BACK,AliYunMsgTemplate.ORDER_BACK)

            //客户审核通过
            .put(MessageNodeEnum.CUSTOMER_APROVED,AliYunMsgTemplate.CUSTOMER_APROVED)

            //客户审核未通过待完善资料
            .put(MessageNodeEnum.CUSTOMER_REJECT,AliYunMsgTemplate.CUSTOMER_REJECT)

            .build();

    @Override
    public ShortMessagePlatformEnum getPlatform() {
        return ShortMessagePlatformEnum.ALIYUN;
    }

    @Override
    public Map<MessageNodeEnum, OperationEnum> getMessageTemplate() {
        return operationMap;
    }

    /**
     * 内部类单例模式
     */
    private static class AliyunMsgOperationInstance{
        private static final AliyunMsgOperation instance = new AliyunMsgOperation();
    }

    private AliyunMsgOperation(){}

    public static AliyunMsgOperation getInstance(){
        return AliyunMsgOperation.AliyunMsgOperationInstance.instance;
    }


}

