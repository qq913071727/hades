package com.tsfyun.scm.entity.logistics;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 国内物流
 * </p>
 *

 * @since 2020-03-25
 */
@Data
public class DomesticLogistics extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 国内物流编码
     */

    private String code;

    /**
     * 国内物流名称
     */

    private String name;

    /**
     * 时效要求-后台用逗号隔开
     */

    private String timeliness;

    /**
     * 是否禁用
     */

    private Boolean disabled;

    /**
     * 是否锁定
     */
    private Boolean locking;


}
