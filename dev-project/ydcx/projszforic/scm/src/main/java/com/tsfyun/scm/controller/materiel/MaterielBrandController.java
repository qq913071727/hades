package com.tsfyun.scm.controller.materiel;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.materiel.MaterielBrandDTO;
import com.tsfyun.scm.dto.materiel.MaterielBrandQTO;
import com.tsfyun.scm.entity.materiel.MaterielBrand;
import com.tsfyun.scm.service.materiel.IMaterielBrandService;
import com.tsfyun.scm.vo.materiel.MaterielBrandVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 物料品牌 前端控制器
 * </p>
 *
 * @since 2020-03-26
 */
@RestController
@RequestMapping("/materielBrand")
public class MaterielBrandController extends BaseController {

    @Autowired
    private IMaterielBrandService materielBrandService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 新增
     * @param dto
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) MaterielBrandDTO dto) {
        materielBrandService.add(dto);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) MaterielBrandDTO dto) {
        materielBrandService.edit(dto);
        return success();
    }


    /**
     * 详情信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<MaterielBrandVO> detail(@RequestParam(value = "id")String id) {
        return success(materielBrandService.detail(id));
    }


    /**
     * 根据品名模糊搜索获取下拉
     * @return
     */
    @PostMapping("select")
    public Result<List<MaterielBrandVO>> select(@RequestParam(value = "name")String name){
        return success(materielBrandService.select(name));
    }

    /**
     * 根据品名精确获取数据
     * @return
     */
    @PostMapping("findByName")
    public Result<MaterielBrandVO> findByName(@RequestParam(value = "name",required = false)String name){
        return success(materielBrandService.findByName(name));
    }

    /**
     * 批量根据品名精确获取数据
     * @return
     */
    @PostMapping("findByNames")
    public Result<List<MaterielBrandVO>> findByNames(@RequestParam(value = "names",required = false)String... names){
        return success(materielBrandService.findByNames(names));
    }


    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    Result<List<MaterielBrandVO>> list(MaterielBrandQTO qto) {
        PageInfo<MaterielBrand> page = materielBrandService.pageList(qto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),MaterielBrandVO.class));
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")String id){
        materielBrandService.delete(id);
        return success();
    }

}

