package com.tsfyun.scm.service.customer;

import com.tsfyun.scm.dto.customer.ExpressStandardQuoteMemberDTO;
import com.tsfyun.scm.entity.customer.ExpressStandardQuoteMember;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 * 快递模式标准报价计算代理费明细 服务类
 * </p>
 *
 *
 * @since 2020-10-26
 */
public interface IExpressStandardQuoteMemberService extends IService<ExpressStandardQuoteMember> {

    List<ExpressStandardQuoteMember> findByQuoteId(Long quoteId);

    /**
     * 批量新增
     * @param quoteId
     * @param memberDTOS
     */
    void addBatch(Long quoteId, List<ExpressStandardQuoteMemberDTO> memberDTOS);

    /**
     * 根据代理费报价删除明细
     * @param quoteId
     */
    void removeByQuoteId(Long quoteId);

}
