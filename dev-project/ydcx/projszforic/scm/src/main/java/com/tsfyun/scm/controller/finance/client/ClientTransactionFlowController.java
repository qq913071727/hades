package com.tsfyun.scm.controller.finance.client;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.scm.dto.finance.client.ClientTransactionFlowQTO;
import com.tsfyun.scm.service.finance.ITransactionFlowService;
import com.tsfyun.scm.vo.finance.CustomerBalanceVO;
import com.tsfyun.scm.vo.finance.client.ClientTransactionListVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @Description: 客户端交易流水记录
 * @CreateDate: Created in 2020/11/9 15:57
 */
@Api(tags = "交易流水（客户端）")
@RestController
@RequestMapping("/client/transactionFlow")
public class ClientTransactionFlowController extends BaseController {

    @Autowired
    private ITransactionFlowService transactionFlowService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<ClientTransactionListVO> pageList(@RequestBody ClientTransactionFlowQTO qto) {
        return transactionFlowService.clientPageList(qto);
    }

    /**
     * 客户当前余额
     * @return
     */
    @GetMapping(value = "outBalance")
    public Result<BigDecimal> outBalance(){
        return success(transactionFlowService.getCustomerBalance(SecurityUtil.getCurrentCustomerId()));
    }

    /**
     * 客户余额情况
     * @return
     */
    @GetMapping(value = "customerBalanceInfo")
    public Result<CustomerBalanceVO> customerBalanceInfo(){
        return success(transactionFlowService.getCustomerInoutBalance(SecurityUtil.getCurrentCustomerId()));
    }

}
