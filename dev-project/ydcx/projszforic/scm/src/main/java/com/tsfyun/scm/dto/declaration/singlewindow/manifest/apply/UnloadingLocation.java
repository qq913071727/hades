package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 卸货地数据
 * @since Created in 2020/4/22 11:13
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnloadingLocation", propOrder = {
        "id",
        "arrivalDateTime"
})
@Data
public class UnloadingLocation {

    //卸货地代码
    @XmlElement(name = "ID")
    protected String id;

    //到达卸货地日期，格式如20190212
    @XmlElement(name = "ArrivalDateTime")
    protected String arrivalDateTime;

    public UnloadingLocation () {

    }

    public UnloadingLocation(String id) {
        this.id = id;
    }
    public UnloadingLocation(String id,String arrivalDateTime) {
        this.id = id;
        this.arrivalDateTime = arrivalDateTime;
    }

}
