package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *

 * @since 2020-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Subject extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 公司编码
     */
    private String code;

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司名称(英文)
     */
    private String nameEn;

    /**
     * 统一社会信用代码(18位)
     */
    private String socialNo;

    /**
     * 税务登记证
     */
    private String taxpayerNo;

    /**
     * 海关注册编码
     */
    private String customsCode;

    /**
     * 检验检疫编码
     */
    private String ciqNo;

    /**
     * 公司法人
     */
    private String legalPerson;

    /**
     * 公司电话
     */
    private String tel;

    /**
     * 公司传真
     */
    private String fax;

    /**
     * 公司邮箱
     */
    private String mail;

    /**
     * 公司地址
     */
    private String address;

    /**
     * 公司地址英文
     */
    private String addressEn;

    /**
     * 公司注册地址
     */
    private String regAddress;

    /**=
     * 浏览器标题
     */
    private String browserTitle;

    /**=
     * 企业logo
     */
    private String logo;

    /**=
     * 电子章
     */
    private String chapter;

    /**=
     * 登录页面背景
     */
    private String loginBg;

    /**
     * 创建人
     */
    private String createBy;


}
