package com.tsfyun.scm.dto.wms;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 入库单请求实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="ReceivingNote请求对象", description="入库单请求实体")
public class ReceivingNoteDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "入库单号不能为空")
   @LengthTrim(max = 20,message = "入库单号最大长度不能超过20位")
   @ApiModelProperty(value = "入库单号")
   private String docNo;

   @NotEmptyTrim(message = "单据类型不能为空")
   @LengthTrim(max = 10,message = "单据类型最大长度不能超过10位")
   @ApiModelProperty(value = "单据类型-枚举")
   private String billType;

   @NotNull(message = "客户不能为空")
   @ApiModelProperty(value = "客户")
   private Long customerId;

   @NotNull(message = "入库时间不能为空")
   @ApiModelProperty(value = "入库时间")
   private LocalDateTime receivingDate;

   @NotEmptyTrim(message = "交货方式不能为空")
   @LengthTrim(max = 20,message = "交货方式最大长度不能超过20位")
   @ApiModelProperty(value = "交货方式")
   private String deliveryMode;

   @NotEmptyTrim(message = "状态编码不能为空")
   @LengthTrim(max = 20,message = "状态编码最大长度不能超过20位")
   @ApiModelProperty(value = "状态编码")
   private String statusId;

   @NotNull(message = "仓库不能为空")
   @ApiModelProperty(value = "仓库")
   private Long warehouseId;

   @NotNull(message = "供应商不能为空")
   @ApiModelProperty(value = "供应商")
   private Long supplierId;

   @NotNull(message = "总箱数不能为空")
   @LengthTrim(max = 11,message = "总箱数最大长度不能超过11位")
   @ApiModelProperty(value = "总箱数")
   private Integer totalCartonNum;

   @LengthTrim(max = 20,message = "快递类型最大长度不能超过20位")
   @ApiModelProperty(value = "快递类型")
   private String inExpressType;

   @LengthTrim(max = 30,message = "快递公司最大长度不能超过30位")
   @ApiModelProperty(value = "快递公司")
   private String hkExpress;

   @LengthTrim(max = 50,message = "快递公司最大长度不能超过50位")
   @ApiModelProperty(value = "快递公司")
   private String hkExpressName;

   @LengthTrim(max = 100,message = "快递单号最大长度不能超过100位")
   @ApiModelProperty(value = "快递单号")
   private String hkExpressNo;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @LengthTrim(max = 500,message = "订单号最大长度不能超过500位")
   @ApiModelProperty(value = "订单号")
   private String orderNo;


}
