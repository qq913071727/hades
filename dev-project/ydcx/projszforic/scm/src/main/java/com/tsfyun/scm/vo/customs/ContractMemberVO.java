package com.tsfyun.scm.vo.customs;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ContractMemberVO implements Serializable {

    @ApiModelProperty(value = "型号")
    private String model;
    @ApiModelProperty(value = "品牌")
    private String brand;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "产地")
    private String countryName;
    @ApiModelProperty(value = "单位")
    private String unitName;
    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;
    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;
    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;
    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;
    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;
}
