package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.domain.ImpOrderStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class ImpOrderPaySituationVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "订单编号")
    private String docNo;
    @ApiModelProperty(value = "客户订单编号")
    private String clientNo;
    @ApiModelProperty(value = "客户")
    private Long customerId;
    @ApiModelProperty(value = "名称")
    private String customerName;
    private Long supplierId;//客户供应商
    private String supplierName;//客户供应商名称
    @ApiModelProperty(value = "状态")
    private String statusId;
    @ApiModelProperty(value = "订单日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;
    @ApiModelProperty(value = "业务类型-枚举")
    private String businessType;
    @ApiModelProperty(value = "报关类型-枚举")
    private String declareType;
    @ApiModelProperty(value = "委托金额")
    private BigDecimal totalPrice;
    @ApiModelProperty(value = "币制")
    private String currencyId;
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "付汇申请金额")
    private BigDecimal applyPayVal;
    @ApiModelProperty(value = "付汇付出金额")
    private BigDecimal payVal;
    @ApiModelProperty(value = "付汇单号")
    private String paymentNo;
    @ApiModelProperty(value = "超期天数")
    private Integer overdueDay;
    /**
     * 业务类型描述
     */
    private String businessTypeDesc;
    public String getBusinessTypeDesc() {
        BusinessTypeEnum businessTypeEnum = BusinessTypeEnum.of(businessType);
        return Objects.nonNull(businessTypeEnum) ? businessTypeEnum.getName() : "";
    }
    /**=
     * 报关类型
     */
    private String declareTypeName;
    public String getDeclareTypeName(){
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(getDeclareType());
        return Objects.nonNull(declareTypeEnum)?declareTypeEnum.getName():"";
    }
    /**
     * 状态描述
     */
    private String statusDesc;
    public String getStatusDesc() {
        ImpOrderStatusEnum impOrderStatusEnum = ImpOrderStatusEnum.of(statusId);
        return Objects.nonNull(impOrderStatusEnum) ? impOrderStatusEnum.getName() : "";
    }

}
