package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.order.ExpOrderPricePlusVo;
import com.tsfyun.scm.dto.order.ExpOrderPriceQTO;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.scm.entity.order.ExpOrderPrice;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.ExpOrderPriceVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2021-09-14
 */
public interface IExpOrderPriceService extends IService<ExpOrderPrice> {
    //计算订单审价
    void calculatingPrice(ExpOrder order);
    void calculatingPrice(ExpOrder order,Boolean isForce);

    //订单分页查询
    PageInfo<ExpOrderPriceVO> list(ExpOrderPriceQTO qto);

    ExpOrderPricePlusVo detail(Long id);
    ExpOrderPricePlusVo detail(Long id,String operation);

    //审价
    void examine(TaskDTO dto);

    /**
     * 根据订单删除审价
     * @param orderId
     */
    void removeByOrderId(Long orderId);
}
