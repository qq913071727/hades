package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.entity.system.SubjectOverseasBank;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.system.SubjectOverseasBankVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Repository
public interface SubjectOverseasBankMapper extends Mapper<SubjectOverseasBank> {

    List<SubjectOverseasBankVO> listByOverseasId(Long overseasId);
}
