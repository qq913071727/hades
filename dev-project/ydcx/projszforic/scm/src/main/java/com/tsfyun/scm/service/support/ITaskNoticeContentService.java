package com.tsfyun.scm.service.support;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.support.TaskNoticeContentDTO;
import com.tsfyun.scm.dto.support.TaskNoticeContentQTO;
import com.tsfyun.scm.entity.support.TaskNoticeContent;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.support.TaskNoticeContentVO;

import java.io.Serializable;

/**
 * <p>
 * 任务通知内容 服务类
 * </p>
 *

 * @since 2020-04-05
 */
public interface ITaskNoticeContentService extends IService<TaskNoticeContent> {

    /**
     * 保存任务通知
     * @param dto
     */
    void add(TaskNoticeContentDTO dto);

    /**
     * 任务通知详情
     * @param id
     * @return
     */
    TaskNoticeContentVO detail(Long id);

    /**
     * 查询我的任务通知分页列表
     * @param qto
     * @return
     */
    PageInfo<TaskNoticeContentVO> page(TaskNoticeContentQTO qto);

    /**
     * 根据单据类型，单据id，操作码删除任务通知（主要用于某任务被执行完，包含从任务通知点击进来的或者直接从列表页操作）
     * @param documentType  单据类型
     * @param documentId    单据id
     * @param operationCode 操作码
     */
    void deleteTaskNotice(String documentType, Serializable documentId, String operationCode);

    /**
     * 获取登录人的任务通知数量
     * @return
     */
    int getMyTaskNoticeNumber(TaskNoticeContentQTO qto);

}
