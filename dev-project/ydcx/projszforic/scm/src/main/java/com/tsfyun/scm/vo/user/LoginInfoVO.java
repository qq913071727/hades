package com.tsfyun.scm.vo.user;

import com.tsfyun.common.base.security.LoginVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginInfoVO implements Serializable {

    /**
     * 登录token
     */
    private String token;

    /**
     * 登录员工信息
     */
    private LoginVO loginInfo;

    /**
     * 是否需要修改密码
     */
    private Boolean requireChangedPwd;

    /**
     * 访问id
     */
    private String accessId;

    /**
     * 绑定第三方账号凭证ID
     */
    private String bindAccessId;

    /**
     * 是否登录成功
     */
    private Boolean success;

    public LoginInfoVO (String token,LoginVO loginInfo) {
        this.token = token;
        this.loginInfo = loginInfo;
        this.success = Boolean.TRUE;
    }

}
