package com.tsfyun.scm.vo.finance;

import com.tsfyun.scm.entity.finance.PurchaseContractMember;
import com.tsfyun.scm.vo.order.ExpOrderCostVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/29 14:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseContractPlusVO implements Serializable {

    /**
     * 采购合同主单
     */
    private PurchaseContractVO purchaseContract;

    /**
     * 采购合同明细
     */
    private List<PurchaseContractMemberVO> members;

    /**
     * 采购合同费用信息
     */
    List<ExpOrderCostVO> costMembers;

    public PurchaseContractPlusVO (PurchaseContractVO purchaseContract,List<PurchaseContractMemberVO> members) {
        this.purchaseContract = purchaseContract;
        this.members = members;
    }

}
