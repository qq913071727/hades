package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.tsfyun.scm.entity.order.ImpOrderPriceMember;
import com.tsfyun.scm.mapper.order.ImpOrderPriceMemberMapper;
import com.tsfyun.scm.service.order.IImpOrderPriceMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.order.ImpOrderPriceMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单审价明细 服务实现类
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Service
public class ImpOrderPriceMemberServiceImpl extends ServiceImpl<ImpOrderPriceMember> implements IImpOrderPriceMemberService {

    @Autowired
    private ImpOrderPriceMemberMapper impOrderPriceMemberMapper;

    @Override
    public Map<String, Object> historicalRecord(Long orderMemberId) {
        return impOrderPriceMemberMapper.historicalRecord(orderMemberId);
    }

    @Override
    public List<ImpOrderPriceMemberVO> findByOrderPriceId(Long orderPriceId) {
        ImpOrderPriceMember query = new ImpOrderPriceMember();
        query.setOrderPriceId(orderPriceId);
        List<ImpOrderPriceMember> list = list(query);
        if(CollUtil.isNotEmpty(list)){
            list = list.stream().sorted(Comparator.comparing(ImpOrderPriceMember::getRowNo)).collect(Collectors.toList());
            return beanMapper.mapAsList(list,ImpOrderPriceMemberVO.class);
        }
        return Lists.newArrayList();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeByOrderId(Long orderId) {
        impOrderPriceMemberMapper.removeByOrderId(orderId);
    }
}
