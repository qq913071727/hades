package com.tsfyun.scm.dto.wms;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class InspectionMemberDTO implements Serializable {
    private static final long serialVersionUID=1L;

    private Long id;

    @NotEmptyTrim(message = "型号不能为空",groups = UpdateGroup.class)
    @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
    @ApiModelProperty(value = "型号")
    private String model;

    @NotEmptyTrim(message = "品牌不能为空",groups = UpdateGroup.class)
    @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
    @ApiModelProperty(value = "品牌")
    private String brand;

    @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
    @ApiModelProperty(value = "名称")
    private String name;

    @LengthTrim(max = 100,message = "客户物料号最大长度不能超过100位")
    @ApiModelProperty(value = "客户物料号")
    private String goodsCode;

    @NotEmptyTrim(message = "产地不能为空",groups = UpdateGroup.class)
    @LengthTrim(max = 10,message = "产地最大长度不能超过10位")
    @ApiModelProperty(value = "产地")
    private String countryName;

    @NotEmptyTrim(message = "单位不能为空",groups = UpdateGroup.class)
    @LengthTrim(max = 10,message = "单位最大长度不能超过10位")
    @ApiModelProperty(value = "单位")
    private String unitName;

    @NotNull(message = "数量不能为空",groups = UpdateGroup.class)
    @Digits(integer = 10, fraction = 2, message = "数量整数位不能超过10位，小数位不能超过2位")
    @DecimalMin(value = "0.01",message = "数量必须大于0")
    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @NotNull(message = "净重不能为空",groups = UpdateGroup.class)
    @Digits(integer = 10, fraction = 4, message = "净重整数位不能超过10位，小数位不能超过4位")
    @DecimalMin(value = "0",message = "数量必须大于等于0")
    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @NotNull(message = "毛重不能为空",groups = UpdateGroup.class)
    @Digits(integer = 10, fraction = 4, message = "毛重整数位不能超过10位，小数位不能超过4位")
    @DecimalMin(value = "0",message = "数量必须大于等于0")
    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    @NotNull(message = "箱数不能为空",groups = UpdateGroup.class)
    @LengthTrim(max = 11,message = "箱数最大长度不能超过11位")
    @Min(value = 0,message = "箱数必须大于等于0")
    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;

    @LengthTrim(max = 20,message = "箱号最大长度不能超过20位")
    @ApiModelProperty(value = "箱号")
    private String cartonNo;
}
