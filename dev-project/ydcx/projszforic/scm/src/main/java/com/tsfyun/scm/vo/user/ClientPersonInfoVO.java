package com.tsfyun.scm.vo.user;

import com.tsfyun.common.base.enums.SexTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * 客户端人员修改页面基本信息
 */
@Data
public class ClientPersonInfoVO implements Serializable {

    @ApiModelProperty(value = "用户id")
    private Long id;
    @ApiModelProperty(value = "用户编码")
    private String code;
    @ApiModelProperty(value = "用户昵称")
    private String name;
    @ApiModelProperty(value = "邮箱")
    private String mail;
    @ApiModelProperty(value = "头像")
    private String head;
    @ApiModelProperty(value = "性别编码")
    private String gender;
    @ApiModelProperty(value = "性别描述")
    private String genderDesc;
    @ApiModelProperty(value = "手机号码")
    private String phone;
    //是否设置了登录密码
    @ApiModelProperty(value = "是否设置登录密码")
    private Boolean setPassword;
    @ApiModelProperty(value = "是否绑定了微信登录")
    private Boolean bindWx;

    public String getGenderDesc(){
        SexTypeEnum sexTypeEnum = SexTypeEnum.of(getGender());
        return Objects.nonNull(sexTypeEnum)?sexTypeEnum.getName():"";
    }
}
