package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.customer.ExpressStandardQTO;
import com.tsfyun.scm.dto.customer.ExpressStandardQuotePlusDTO;
import com.tsfyun.scm.entity.customer.ExpressStandardQuote;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.ExpressStandardQuoteMemberVO;
import com.tsfyun.scm.vo.customer.ExpressStandardQuotePlusVO;

import java.util.List;

/**
 * <p>
 * 快递模式标准报价代理费 服务类
 * </p>
 *
 *
 * @since 2020-10-26
 */
public interface IExpressStandardQuoteService extends IService<ExpressStandardQuote> {

    /**
     * 根据报价类型获取一个有效的标准报价
     * @param quoteType
     * @return
     */
    ExpressStandardQuote getExistsQuote(String quoteType);

    /**
     * 根据报价类型获取有效的标准报价明细
     * @param quoteType
     * @return
     */
    List<ExpressStandardQuoteMemberVO> getCurrentEffective(String quoteType);

    /**
     * 获取所有的有效的标准报价
     * @return
     */
    List<ExpressStandardQuote> getAllEffectiveStandardQuote();

    /**
     * 分页查询代理费报价信息
     * @param qto
     * @return
     */
    PageInfo<ExpressStandardQuote> pageList(ExpressStandardQTO qto);

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(String id,Boolean disabled);

    /**
     * 新增报价信息
     * @param dto
     */
    void addPlus(ExpressStandardQuotePlusDTO dto);

    /**
     * 修改报价信息
     * @param dto
     */
    void editPlus(ExpressStandardQuotePlusDTO dto);

    /**
     * 删除报价信息
     * @param id
     */
    void delete(Long id);

    /**
     * 报价详情
     * @param id
     * @return
     */
    ExpressStandardQuotePlusVO detail(Long id);

}
