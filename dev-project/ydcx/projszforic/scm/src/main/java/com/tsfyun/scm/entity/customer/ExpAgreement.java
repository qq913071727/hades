package com.tsfyun.scm.entity.customer;

import java.math.BigDecimal;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 出口协议
 * </p>
 *
 *
 * @since 2021-09-10
 */
@Data
public class ExpAgreement extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 协议编号
     */

    private String docNo;

    /**
     * 业务类型(枚举)
     */

    private String businessType;

    /**
     * 报关类型
     */

    private String declareType;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 甲方
     */

    private String partyaName;

    /**
     * 甲方法人
     */

    private String partyaLegalPerson;

    /**
     * 甲方电话
     */

    private String partyaTel;

    /**
     * 甲方传真
     */

    private String partyaFax;

    /**
     * 甲方地址
     */

    private String partyaAddress;

    /**
     * 乙方
     */

    private String partybName;

    /**
     * 乙方法人
     */

    private String partybLegalPerson;

    /**
     * 乙方电话
     */

    private String partybTel;

    /**
     * 乙方传真
     */

    private String partybFax;

    /**
     * 乙方地址
     */

    private String partybAddress;

    /**
     * 签约日期
     */

    private Date signingDate;

    /**
     * 生效日期
     */

    private Date effectDate;

    /**
     * 失效日期
     */

    private Date invalidDate;

    /**
     * 实际失效日期 
     */

    private Date actualInvalidDate;

    /**
     * 自动延期/年
     */

    private Integer delayYear;

    /**
     * 协议正本是否签回
     */

    private Boolean isSignBack;

    /**
     * 签回确定人
     */

    private String signBackPerson;

    /**
     * 报价描述
     */

    private String quoteDescribe;

    /**
     * 代理费结算模式
     */

    private String settlementMode;

    /**
     * 代理费%（数据库中存储的数据不除以100）
     */

    private BigDecimal agencyFee;

    /**
     * 最低收费
     */
    private BigDecimal minAgencyFee;

    /**
     * 是否垫税
     */

    private Boolean isTaxAdvance;

    /**
     * 甲方社会统一信用代码
     */

    private String partyaSocialNo;

    /**
     * 乙方统一社会信用代码
     */

    private String partybSocialNo;

    /**
     * 备注
     */

    private String memo;


}
