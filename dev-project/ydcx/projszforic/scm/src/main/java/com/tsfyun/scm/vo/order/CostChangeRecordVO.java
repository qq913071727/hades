package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;

import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 费用变更记录响应实体
 * </p>
 *

 * @since 2020-05-27
 */
@Data
@ApiModel(value="CostChangeRecord响应对象", description="费用变更记录响应实体")
public class CostChangeRecordVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "订单号")
    private String impOrderNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "费用科目编码")
    private String expenseSubjectId;

    @ApiModelProperty(value = "费用科目名称")
    private String expenseSubjectName;

    @ApiModelProperty(value = "原始金额")
    private BigDecimal originCostAmount;

    @ApiModelProperty(value = "变更后金额")
    private BigDecimal costAmount;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "操作时间")
    private LocalDateTime dateUpdated;

    /**
     * 操作人
     */
    private String updateBy;

    public String getUpdateBy() {
        return StringUtils.isNotEmpty(updateBy) && updateBy.contains("/") ? updateBy.split("/")[1] : null;
    }


}
