package com.tsfyun.scm.entity.support;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 用户关注任务通知
 * </p>
 *

 * @since 2020-04-09
 */
@Data
public class TaskNoticeFocus implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;


    /**
     * 员工id
     */

    private Long personId;

    /**
     * 员工名称
     */

    private String personName;

    /**
     * 操作码
     */

    private String operationCode;

    /**
     * 无需接收通知-1(不接收)
     */

    private Boolean noReception;


}
