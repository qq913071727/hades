package com.tsfyun.scm.util;

import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.scm.dto.user.LoginDTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.user.Login;
import com.tsfyun.scm.entity.user.Person;

import java.time.LocalDateTime;
import java.util.Objects;

public class LoginUtil {

    public static LoginVO buildLogin(Login login, LoginDTO loginDto, Person person, Customer customer) {
        LoginVO loginVO = new LoginVO();
        loginVO.setPersonId(person.getId());
        loginVO.setEmail(person.getMail());
        loginVO.setPhone(person.getPhone());
        loginVO.setPersonName(person.getName());
        loginVO.setUserHead(person.getHead());
        loginVO.setLoginDate(LocalDateTime.now());
        loginVO.setLoginType(loginDto.getLoginType());
        loginVO.setLoginId(login.getId());
        loginVO.setLoginName(login.getLoginName());
        loginVO.setLoginPassWord(person.getPassWord());
        loginVO.setPosition(person.getPosition());
        loginVO.setIncumbency(person.getIncumbency());
        if(Objects.nonNull(customer)){
            loginVO.setCustomerId(customer.getId());
            loginVO.setCustomerName(customer.getName());
        }
        return loginVO;
    }

}
