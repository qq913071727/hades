package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OrderReceivingBindVO implements Serializable {

    private Long id;//订单明细绑定表ID
    private String orderNo;//订单号
    private Long customerId;//客户ID
    private Long receivingNoteId;//入库单ID
    private String receivingNoteDocNo;//入库单编号
    private Long warehouseId;//入库仓库ID
    private Long receivingNoteMemberId;//入库单明细ID
    private Integer rowNo;//序号
    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 客户物料号
     */

    private String goodsCode;

    /**
     * 产地
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;
}
