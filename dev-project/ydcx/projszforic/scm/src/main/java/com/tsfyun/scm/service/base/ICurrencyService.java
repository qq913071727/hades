package com.tsfyun.scm.service.base;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.base.CurrencyQTO;
import com.tsfyun.scm.entity.base.Currency;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.base.CurrencyVO;

import java.util.List;

/**
 * <p>
 *  币制服务类
 * </p>
 *

 * @since 2020-03-18
 */
public interface ICurrencyService extends IService<Currency> {

    /**
     * 获取所有的启用的币制度=下拉框
     * @return
     */
    List<CurrencyVO> select();


    Currency findById(String id);

    Currency findByName(String name);

    //分页列表
    PageInfo<Currency> page(CurrencyQTO dto);

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(String id,Boolean disabled);

    /**
     * 调整排序
     * @param id
     * @param sort
     */
    void adjustSort(String id,Integer sort);
}
