package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/5 14:55
 */
@Data
public class ImpClauseOrderVO implements Serializable {

    private Long id;

    private Long orderId;

    private String orderNo;

    private Long agreementId;

    /**
     * 协议编号
     */
    private String agreementDocNo;

    /**
     * 缴税方式(枚举)
     */

    private String voluntarilyTax;

    /**
     * 货款额度
     */

    private BigDecimal goodsQuota;

    /**
     * 税款额度
     */

    private BigDecimal taxQuota;

    /**
     * 额度说明
     */

    private String quotaExplain;

    /**
     * 成交方式
     */
    private String transactionMode;

    /**=
     * 货款汇率类型
     */
    private String goodsRate;

    /**=
     * 货款汇率时间
     */
    private String goodsRateTime;

    /**
     * 最大超期天数
     */
    private Integer overdueDays;

    /**=
     * 税款以实缴定应收
     */
    Boolean isPaidIn;

    /**=
     * 税款汇率
     */
    private String taxRate;

    /**=
     * 税款汇率时间
     */
    private String taxRateTime;

}
