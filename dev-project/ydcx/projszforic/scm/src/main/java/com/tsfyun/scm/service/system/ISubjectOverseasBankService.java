package com.tsfyun.scm.service.system;

import com.tsfyun.scm.dto.system.SubjectOverseasBankDTO;
import com.tsfyun.scm.entity.system.SubjectOverseasBank;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.SubjectOverseasBankVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-03-19
 */
public interface ISubjectOverseasBankService extends IService<SubjectOverseasBank> {

    /**
     * 批量保存主体境外银行信息
     * @param overseasId
     * @param banks
     */
    public void addBatch(Long overseasId, List<SubjectOverseasBankDTO> banks);

    /**=
     * 统计境外公司下银行条数
     * @param overseasId
     * @return
     */
    Integer countByOverseasId(Long overseasId);

    /**=
     * 根据境外公司获取银行信息
     * @param overseasId
     * @return
     */
    List<SubjectOverseasBankVO> listByOverseasId(Long overseasId);

    /**=
     * 根据境外公司删除银行信息
     * @param overseasId
     */
    void removeByOverseasId(Long overseasId);

    /**
     * 获取默认境外公司对应的银行
     * @return
     */
    List<SubjectOverseasBankVO> getDefaultSubjectOverseasBank();

    /**
     * 详情
     * @param id
     * @return
     */
    SubjectOverseasBankVO detail(Long id);
}
