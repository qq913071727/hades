package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.PaymentAccountMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.PaymentAccountMemberVO;
import com.tsfyun.scm.vo.finance.PaymentOrderCostVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 付款单明细 Mapper 接口
 * </p>
 *

 * @since 2020-04-24
 */
@Repository
public interface PaymentAccountMemberMapper extends Mapper<PaymentAccountMember> {

    /**
     * 批量更新付汇单明细人民币金额数据
     * @param paymentAccountMembers
     */
    void updateAccountValueCny(List<PaymentAccountMemberVO> paymentAccountMembers);


    List<PaymentAccountMemberVO> findByPaymentAccountId(@Param(value = "paymentAccountId")Long paymentAccountId);

    List<String> findByOrderIdByPayNos(@Param(value = "orderId")Long orderId);

    List<PaymentOrderCostVO> findByOrderPaymentCostId(@Param(value = "orderId")Long orderId);
}
