package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import javax.validation.constraints.Digits;

/**
 * <p>
 * 快递模式标准报价计算代理费明细请求实体
 * </p>
 *
 *
 * @since 2020-10-26
 */
@Data
@ApiModel(value="ExpressStandardQuoteMember请求对象", description="快递模式标准报价计算代理费明细请求实体")
public class ExpressStandardQuoteMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   @ApiModelProperty(value = "报价单id")
   private Long quoteId;

   @NotNull(message = "开始重量不能为空")
   @Digits(integer = 8, fraction = 0, message = "开始重量最大长度不能超过8位且为正整数")
   @DecimalMin(value = "0",message = "开始重量不能小于0")
   @ApiModelProperty(value = "开始重量")
   private BigDecimal startWeight;

   @NotNull(message = "结束重量不能为空")
   @Digits(integer = 8, fraction = 0,message = "结束重量最大长度不能超过8位且为正整数")
   @DecimalMin(value = "0",message = "结束重量不能小于0")
   @ApiModelProperty(value = "结束重量")
   private BigDecimal endWeight;

   @Digits(integer = 8, fraction = 0,message = "基准价最大长度不能超过8位")
   @DecimalMin(value = "0.00",message = "基准价不能小于0")
   @ApiModelProperty(value = "基准价")
   private BigDecimal basePrice;

   @Digits(integer = 6, fraction = 2, message = "单价整数位不能超过6位，小数位不能超过2")
   @DecimalMin(value = "0.01",message = "单价不能小于等于0")
   @ApiModelProperty(value = "单价-KG")
   private BigDecimal price;


}
