package com.tsfyun.scm.entity.customs;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 报关单模板
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
public class DeclarationTemp extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 模板名称
     */

    private String tempName;

    /**
     * 单据类型-进出口
     */

    private String billType;

    /**
     * 禁用标示
     */

    private Boolean disabled;

    /**
     * 是否默认
     */

    private Boolean isDefault;

    /**
     * 运输方式
     */

    private String cusTrafModeCode;

    /**
     * 运输方式
     */

    private String cusTrafModeName;

    /**
     * 监管方式
     */

    private String supvModeCdde;

    /**
     * 监管方式
     */

    private String supvModeCddeName;

    /**
     * 征免性质
     */

    private String cutModeCode;

    /**
     * 征免性质
     */

    private String cutModeName;

    /**
     * 启运/运抵国(地区)
     */

    private String cusTradeCountryCode;

    /**
     * 启运/运抵国(地区)
     */

    private String cusTradeCountryName;

    /**
     * 经停港/指运港
     */

    private String distinatePortCode;

    /**
     * 经停港/指运港
     */

    private String distinatePortName;

    /**
     * 申报地海关
     */

    private String customMasterCode;

    /**
     * 申报地海关
     */

    private String customMasterName;

    /**
     * 包装种类
     */

    private String wrapTypeCode;

    /**
     * 包装种类
     */

    private String wrapTypeName;

    /**
     * 贸易国别(地区)	
     */

    private String cusTradeNationCode;

    /**
     * 贸易国别(地区)	
     */

    private String cusTradeNationName;

    /**
     * 入/离境口岸
     */

    private String ciqEntyPortCode;

    /**
     * 入/离境口岸
     */

    private String ciqEntyPortName;

    /**
     * 货物存放地
     */

    private String goodsPlace;

    /**
     * 启运港(进口)
     */

    private String destPortCode;

    /**
     * 启运港(进口)
     */

    private String destPortName;

    /**
     * 最终目的国
     */

    private String destinationCountry;

    /**
     * 最终目的国
     */

    private String destinationCountryName;

    /**
     * 征免方式	
     */

    private String dutyMode;

    /**
     * 征免方式	
     */

    private String dutyModeName;

    /**
     * 境内目的地/境内货源地
     */

    private String districtCode;

    /**
     * 境内目的地/境内货源地
     */

    private String districtName;

    /**
     * 目的地代码/产地代码
     */

    private String ciqDestCode;

    /**
     * 目的地代码/产地代码
     */

    private String ciqDestName;

    /**
     * 进/出境关别
     */

    private String iePortCode;

    /**
     * 进/出境关别
     */

    private String iePortName;

    /**
     * 标记唛码
     */

    private String markNo;


}
