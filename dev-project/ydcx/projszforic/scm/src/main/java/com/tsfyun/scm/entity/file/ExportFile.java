package com.tsfyun.scm.entity.file;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-04
 */
@Data
public class ExportFile extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 下载一次则删除
     */
    private Boolean once;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 文件存放路径
     */
    private String path;

    /**
     * 下载次数
     */
    private Integer times;


}
