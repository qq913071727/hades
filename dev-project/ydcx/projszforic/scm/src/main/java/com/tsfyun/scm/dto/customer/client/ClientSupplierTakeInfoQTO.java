package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 供应商提货信息查询请求实体
 * @CreateDate: Created in 2020/10/10 09:37
 */
@Data
public class ClientSupplierTakeInfoQTO extends PaginationDto implements Serializable {

    private Long supplierId;

    private String supplierName;

    private Boolean disabled;

    private Boolean isDefault;

}
