package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.InvoiceCostMember;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 发票或内贸合同费用明细 Mapper 接口
 * </p>
 *

 * @since 2020-05-18
 */
@Repository
public interface InvoiceCostMemberMapper extends Mapper<InvoiceCostMember> {

    void deleteByDocId(String docId);

}
