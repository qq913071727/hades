package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.ExpPaymentAccountOrder;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 付款订单明细 Mapper 接口
 * </p>
 *
 *
 * @since 2021-11-17
 */
@Repository
public interface ExpPaymentAccountOrderMapper extends Mapper<ExpPaymentAccountOrder> {

}
