package com.tsfyun.scm.service.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.system.SysNoticeDTO;
import com.tsfyun.scm.dto.system.SysNoticeQTO;
import com.tsfyun.scm.entity.system.SysNotice;
import com.tsfyun.scm.vo.system.SimpleSysNoticeVO;
import com.tsfyun.scm.vo.system.SysNoticeVO;

import java.util.List;

/**
 * <p>
 *  通知公告服务类
 * </p>
 *
 *
 * @since 2020-09-22
 */
public interface ISysNoticeService extends IService<SysNotice> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<SysNoticeVO> pageList(SysNoticeQTO qto);

    /**
     * 详情
     * @param id
     * @return
     */
    SysNoticeVO detail(Long id);

    /**
     * 删除
     * @param id
     */
    void remove(Long id);

    /**
     * 新增
     * @param dto
     */
    Long add(SysNoticeDTO dto);

    /**
     * 修改
     * @param dto
     */
    void edit(SysNoticeDTO dto);


    /**
     * 返回最近几条公告
     * @return
     */
    List<SimpleSysNoticeVO> recentSysNotices(Integer num);

}
