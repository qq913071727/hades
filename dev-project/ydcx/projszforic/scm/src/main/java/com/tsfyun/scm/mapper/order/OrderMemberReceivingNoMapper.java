package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.OrderMemberReceivingNo;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 绑定入库单辅助表 Mapper 接口
 * </p>
 *
 *
 * @since 2020-05-06
 */
@Repository
public interface OrderMemberReceivingNoMapper extends Mapper<OrderMemberReceivingNo> {

    void removeByOrderMemberIds(@Param(value = "memberIds") List<Long> memberIds);

    List<OrderMemberReceivingNo> findByOrderId(@Param(value = "orderId")Long orderId);
}
