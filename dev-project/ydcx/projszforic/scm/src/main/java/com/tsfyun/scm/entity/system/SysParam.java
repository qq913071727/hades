package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-04-15
 */
@Data
public class SysParam implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 参数值
     */

    private String val;

    /**
     * 备注
     */

    private String remark;

    /**
     * 是否锁定-1-是;0-否-
     */

    private Boolean locking;

    /**
     * 是否禁用-1-是;2-否
     */

    private Boolean disabled;


    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;


}
