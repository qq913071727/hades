package com.tsfyun.scm.dto.order;

import com.tsfyun.scm.entity.order.ImpOrderMember;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @since Created in 2020/4/10 15:12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpOrderMemberSaveDTO implements Serializable {

    /**
     * 需要新增/修改的订单明细
     */
    private List<ImpOrderMember> saveOrderMembers;

    /**
     * 需要删除的订单明细
     */
    private List<ImpOrderMember> deleteOrderMembers;



}
