package com.tsfyun.scm.mapper.customs;

import com.tsfyun.scm.entity.customs.DeclarationBroker;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 报关行 Mapper 接口
 * </p>
 *
 *
 * @since 2021-11-15
 */
@Repository
public interface DeclarationBrokerMapper extends Mapper<DeclarationBroker> {

}
