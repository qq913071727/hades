package com.tsfyun.scm.util;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tsfyun.common.base.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/10 18:27
 */
public class ClientStatusMappingUtil {


    /**
     * 客户端付汇状态和后台付汇状态映射
     */
    public static final Map<String, ClientInfoStatus> PAY_CLIENT_STATUS_MAPPING = new HashMap<String,ClientInfoStatus>(){{
        put("waitEdit", new ClientInfoStatus("waitEdit","待修改",Lists.newArrayList("waitEdit")));
        put("waitExamine", new ClientInfoStatus("waitExamine","待审核",Lists.newArrayList("waitSWExamine","waitFKExamine")));
        put("waitPay", new ClientInfoStatus("waitPay","待付汇",Lists.newArrayList("confirmBank","waitPay")));
        put("payed", new ClientInfoStatus("payed","已付汇",Lists.newArrayList("completed")));
        put("toVoid", new ClientInfoStatus("toVoid","已作废",Lists.newArrayList("toVoid")));
    }};

    /**
     * 客户端退款状态和后台退款状态映射
     */
    public static final Map<String, ClientInfoStatus> REFUND_CLIENT_STATUS_MAPPING = new HashMap<String, ClientInfoStatus>(){{
        put("waitExamine", new ClientInfoStatus("waitExamine","待审核",Lists.newArrayList("waitExamine")));
        put("waitRefund", new ClientInfoStatus("waitRefund","待退款",Lists.newArrayList("confirmBank","waitPay")));
        put("refunded", new ClientInfoStatus("refunded","已退款",Lists.newArrayList("completed")));
        put("toVoid",  new ClientInfoStatus("toVoid","已作废",Lists.newArrayList("notApprove")));
    }};

    /**
     * 客户端发票状态和后台退款状态映射
     */
    public static final Map<String, ClientInfoStatus> INVOICE_CLENT_STATUS_MAPPING = new HashMap<String, ClientInfoStatus>(){{
        put("invoiceing", new ClientInfoStatus("invoiceing","开票中",Lists.newArrayList("waitConfirm","waitFinanceTax","waitFinanceConfirm")));
        put("waitConfirm", new ClientInfoStatus("waitConfirm","待确认",Lists.newArrayList("waitCustomerTax")));
        put("invoiced", new ClientInfoStatus("invoiced","已开票",Lists.newArrayList("invoiced")));
    }};

    /**
     * 后端付汇状态映射前端状态
     */
    public static final Map<String,String> PAY_BACK_STATUS_MAPPING = Maps.newHashMap();

    /**
     * 后端退款状态映射前端状态
     */
    public static final Map<String,String> REFUND_BACK_STATUS_MAPPING = Maps.newHashMap();

    /**
     * 后端开票状态映射前端状态
     */
    public static final Map<String,String> INVOICE_BACK_STATUS_MAPPING = Maps.newHashMap();

    /**
     * 根据付汇后端状态映射前端状态
     * @param statusId
     * @return
     */
    public static String getClientPayStatus(String statusId) {
        if(StringUtils.isEmpty(statusId)) {
            return "";
        }
        if(CollUtil.isNotEmpty(PAY_BACK_STATUS_MAPPING)) {
            return PAY_BACK_STATUS_MAPPING.get(statusId);
        }
        PAY_CLIENT_STATUS_MAPPING.entrySet().forEach(k->{
            k.getValue().getBackStatusId().stream().forEach(r->{
                PAY_BACK_STATUS_MAPPING.put(r,k.getKey());
            });
        });
        return PAY_BACK_STATUS_MAPPING.get(statusId);
    }

    /**
     * 根据退款后端状态映射前端状态
     * @param statusId
     * @return
     */
    public static String getClientRefundStatus(String statusId) {
        if(StringUtils.isEmpty(statusId)) {
            return "";
        }
        if(CollUtil.isNotEmpty(REFUND_BACK_STATUS_MAPPING)) {
            return REFUND_BACK_STATUS_MAPPING.get(statusId);
        }
        REFUND_CLIENT_STATUS_MAPPING.entrySet().forEach(k->{
            k.getValue().getBackStatusId().stream().forEach(r->{
                REFUND_BACK_STATUS_MAPPING.put(r,k.getKey());
            });
        });
        return REFUND_BACK_STATUS_MAPPING.get(statusId);
    }

    /**
     * 根据开票后端状态映射前端状态
     * @param statusId
     * @return
     */
    public static String getClientInvoiceStatus(String statusId) {
        if(StringUtils.isEmpty(statusId)) {
            return "";
        }
        if(CollUtil.isNotEmpty(INVOICE_BACK_STATUS_MAPPING)) {
            return INVOICE_BACK_STATUS_MAPPING.get(statusId);
        }
        INVOICE_CLENT_STATUS_MAPPING.entrySet().forEach(k->{
            k.getValue().getBackStatusId().stream().forEach(r->{
                INVOICE_BACK_STATUS_MAPPING.put(r,k.getKey());
            });
        });
        return INVOICE_BACK_STATUS_MAPPING.get(statusId);
    }


}
