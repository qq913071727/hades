package com.tsfyun.scm.service.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.system.RoleDTO;
import com.tsfyun.scm.dto.system.RoleSaveDTO;
import com.tsfyun.scm.dto.system.UserBindRoleDTO;
import com.tsfyun.scm.entity.system.SysRole;
import com.tsfyun.scm.vo.system.RolePersonVO;
import com.tsfyun.scm.vo.system.SysRoleInfoVO;
import com.tsfyun.scm.vo.system.SysRoleVO;

import java.util.List;
import java.util.Map;

public interface ISysRoleService extends IService<SysRole> {

    /**
     * 获取用户角色
     * @param personId
     * @return
     */
    List<SysRole> getUserRoles(Long personId);

    /**
     * 角色列表
     * @param roleDTO
     * @return
     */
    PageInfo<SysRole> pageList(RoleDTO roleDTO);

    /**
     * 角色列表
     * @param roleDTO
     * @return
     */
    List<SysRoleVO> list(RoleDTO roleDTO);

    /**
     * 新增角色
     * @param dto
     */
    void saveRole(RoleSaveDTO dto);

    /**
     * 修改角色
     * @param dto
     */
    void updateRole(RoleSaveDTO dto);

    /**=
     * 修改角色禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(String id,Boolean disabled);

    /**
     * 删除角色
     * @param ids
     */
    void removeIds(List<String> ids);

    /**
     * 角色信息
     * @param id
     * @return
     */
    SysRoleInfoVO roleInfo(String id);

    /**
     * 用户绑定角色
     * @param dto
     */
    void userBindRole(UserBindRoleDTO dto);

    /**
     * 批量根据员工id获取用户角色
     * @param personIds
     * @return
     */
    List<RolePersonVO> getUserRolesBatch(List<Long> personIds);

}
