package com.tsfyun.scm.entity.materiel;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 物料品牌
 * </p>
 *

 * @since 2020-03-26
 */
@Data
public class MaterielBrand implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 英文名称
     */

    private String name;

    /**
     * 中文名称
     */

    private String memo;

    /**
     * 品牌类型
     */

    private String type;

    /**
     * 品牌LOGO
     */

    private String logo;


}
