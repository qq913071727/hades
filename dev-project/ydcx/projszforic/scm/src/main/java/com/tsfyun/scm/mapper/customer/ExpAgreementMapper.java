package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.customer.ExpAgreement;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.ExpAgreementVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出口协议 Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-07
 */
@Repository
public interface ExpAgreementMapper extends Mapper<ExpAgreement> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<ExpAgreementVO> list(Map<String,Object> params);

    /**
     * 查询是否存在未完成的出口协议
     * @param customerId
     * @return
     */
    Integer countUndone(Long customerId);

    /**
     * 查询出口协议正本签回情况
     * @param ids
     * @param IsSignBack
     * @return
     */
    List<ExpAgreement> selectByIdsAndIsSignBack(@Param(value = "ids")List<Long> ids, @Param(value = "isSignBack")Boolean IsSignBack);

    /**
     * 定时扫描处理出口报价失效
     */
    void invalidExpAgreement();

}
