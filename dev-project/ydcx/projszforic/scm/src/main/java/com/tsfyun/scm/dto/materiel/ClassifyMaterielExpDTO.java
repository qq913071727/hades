package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class ClassifyMaterielExpDTO implements Serializable {

    private static final long serialVersionUID=1L;

    @NotEmptyTrim(message = "请选择您要归类的物料")
    private String id;

    @NotEmptyTrim(message = "物料品牌不能为空")
    private String brand;

    @NotEmptyTrim(message = "物料名称不能为空")
    @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
    private String name;

    @NotEmptyTrim(message = "海关编码不能为空")
    @LengthTrim(min = 10,max = 10,message = "海关编码必须10位")
    private String hsCode;

    @NotEmptyTrim(message = "规格描述不能为空")
    @LengthTrim(max = 255,message = "规格描述最大长度不能超过255位")
    private String spec;

    @LengthTrim(max = 255,message = "归类备注最大长度不能超过255位")
    private String memo;

    //申报要素值
    List<String> elementsVal;

}

