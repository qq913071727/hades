package com.tsfyun.scm.controller.order;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.order.ImpOrderPriceQTO;
import com.tsfyun.scm.service.order.IImpOrderPriceService;
import com.tsfyun.scm.vo.order.ImpOrderPricePlusVo;
import com.tsfyun.scm.vo.order.ImpOrderPriceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 订单审价 前端控制器
 * </p>
 *
 *
 * @since 2020-04-16
 */
@RestController
@RequestMapping("/impOrderPrice")
public class ImpOrderPriceController extends BaseController {

    @Autowired
    private IImpOrderPriceService impOrderPriceService;

    /**
     * 分页查询列表
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ImpOrderPriceVO>> list(@ModelAttribute ImpOrderPriceQTO qto) {
        PageInfo<ImpOrderPriceVO> page = impOrderPriceService.list(qto);
        return success((int) page.getTotal(),page.getList());
    }

    @PostMapping(value = "detail")
    public Result<ImpOrderPricePlusVo> detail(@RequestParam(value = "id")Long id,@RequestParam(value = "operation",required = false)String operation){
        return success(impOrderPriceService.detail(id,operation));
    }

    /**=
     * 审核
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "examine")
    public Result<Void> examine(@ModelAttribute @Valid TaskDTO dto){
        impOrderPriceService.examine(dto);
        return success();
    }
}

