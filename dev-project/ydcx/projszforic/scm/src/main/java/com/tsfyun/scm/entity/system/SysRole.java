package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 角色
 */
@Data
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * id手工生成
     */
    @Id
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 是否禁用
     */
    private Boolean disabled;

    /**
     * 是否锁定
     */
    private Boolean locking;

    /**=
     * 备注
     */
    private String memo;

    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;


}