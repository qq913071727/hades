package com.tsfyun.scm.dto.view;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class ViewOrderCostMemberQTO extends PaginationDto {

    private String docNo;// 订单号
    private Long customerId;// 客户ID
    private String salePersonName;// 销售人员
    private String quoteType;// 报价类型
    private String queryDate;//日期查询类别
    private String arrears;//欠款情况(欠款：true 未欠款：false)
    private String overdue;//逾期情况(逾期：overdue1 已到期：overdue2 未逾期：overdue3)

    /**=
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**=
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd){
        if(dateEnd!=null){
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }
}
