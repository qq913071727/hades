package com.tsfyun.scm.entity.order;

import java.io.Serializable;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 出口订单物流信息
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Data
public class ExpOrderLogistics implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    private Long id;

    private Long expOrderId;

    /**
     * 国内交货方式
     */

    private String deliveryMode;

    /**
     * 国内交货方式
     */

    private String deliveryModeName;

    /**
     * 交货备注
     */

    private String deliveryModeMemo;

    /**
     * 国内交货快递单号
     */

    private String deliveryExpressNo;

    /**
     * 国内提货方式
     */

    private String deliveryMethod;

    /**
     * 要求提货时间
     */

    private Date takeTime;

    /**
     * 国内提货信息
     */

    private Long customerDeliveryInfoId;

    /**
     * 提货公司
     */

    private String deliveryCompanyName;

    /**
     * 提货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 提货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 提货地址
     */

    private String deliveryAddress;

    /**
     * 中港包车
     */

    private Boolean isCharterCar;

    /**
     * 送货目的地
     */

    private String deliveryDestination;

    /**
     * 境外送货信息ID
     */

    private Long supplierTakeInfoId;

    /**
     * 收货公司
     */

    private String takeLinkCompany;

    /**
     * 收货联系人
     */

    private String takeLinkPerson;

    /**
     * 收货联系人电话
     */

    private String takeLinkTel;

    /**
     * 收货地址
     */

    private String takeAddress;

    /**
     * 收货备注
     */

    private String receivingModeMemo;


}
