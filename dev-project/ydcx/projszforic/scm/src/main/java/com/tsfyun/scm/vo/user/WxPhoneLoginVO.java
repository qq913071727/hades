package com.tsfyun.scm.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 微信手机号码登录响应
 * @CreateDate: Created in 2020/12/28 17:17
 */
@Data
public class WxPhoneLoginVO implements Serializable {

    private String phoneNumber;//有区号手机号

    private String purePhoneNumber;//无区号手机号

    private String countryCode;//国家编码

}


