package com.tsfyun.scm.service.user;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.scm.dto.user.*;
import com.tsfyun.scm.dto.user.client.ClientChangePhoneCodeDTO;
import com.tsfyun.scm.dto.user.client.ClientChangePhoneDTO;
import com.tsfyun.scm.dto.user.client.ClientUpdatePassWordDTO;
import com.tsfyun.scm.entity.user.Person;
import com.tsfyun.scm.vo.user.*;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IPersonService extends IService<Person> {

    Person findByCode(String code);

    /**
     * 客户端用户注册
     *
     * @param dto
     * @return
     */
    void clientRegister(RegisterDTO dto);

    /**
     * 根据员工id获取员工信息
     *
     * @param id
     * @return
     */
    Person getById(Long id);

    /**
     * 新增员工，会生成登录帐号
     *
     * @param dto
     */
    void addInnerPerson(InsidePersonSaveDTO dto);

    /**
     * 修改员工，如果填写的话可以修改密码
     *
     * @param dto
     */
    void editInnerPerson(InsidePersonSaveDTO dto);

    /**
     * 员工详情
     *
     * @param id
     * @return
     */
    PersonInfoVO detailPerson(Long id);

    /**
     * 分页
     *
     * @param dto
     * @param findRole
     * @return
     */
    PageInfo<PersonInfoVO> pageList(InsidePersonDTO dto, Boolean findRole);

    /**
     * 登录员工查看员工信息
     *
     * @return
     */
    LoginVO info();

    /**
     * 删除员工信息
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 统计某部门下面的员工数量
     *
     * @param departmentId
     * @return
     */
    int countByDepartmentId(Long departmentId);

    /**
     * 是否修改密码标志位
     *
     * @param id
     * @param flag
     */
    void updatePwdFlag(Long id, Boolean flag);

    /**
     * 修改在职离职状态
     *
     * @param id
     * @param incumbency
     */
    void updateIncumbency(Long id, Boolean incumbency);

    /**
     * 员工类型下拉分页
     *
     * @param dto
     * @return
     */
    PageInfo<Person> positionList(InsidePersonDTO dto);

    /**
     * 根据id集合查询简单信息
     *
     * @param ids
     * @return
     */
    Map<Long, SimplePersonInfo> findByIds(Set<Long> ids);

    /**
     * 员工导入excel
     *
     * @param file
     */
    void importExcel(MultipartFile file);

    /**
     * 根据菜单id获取绑定了该权限的员工信息
     *
     * @param menuId
     * @return
     */
    List<SimplePersonInfo> getPersonByMenuId(@NonNull String menuId);

    /**
     * 根据角色获取该角色下面的用户
     *
     * @param roleId
     * @return
     */
    List<SimplePersonInfo> getPersonByRoleId(@NonNull String roleId);

    /**
     * 修改员工需要重置密码标识
     * @param password
     * @param id
     */
    void updatePersonPwdChangeFlag(String password,Long id);

    /**
     * =
     * 统计员工数
     *
     * @return
     */
    Integer countPersonNumber();

    /**
     * 校验员工是否被删除
     *
     * @param id
     */
    Integer checkPersonIsDelete(Long id);

    /**
     * 用户上传头像
     *
     * @param file
     * @return
     */
    String uploadHead(MultipartFile file);

    /**
     * 登录人员基本信息(客户端)
     *
     * @return
     */
    ClientPersonInfoVO loginPersonInfo();

    /**
     * 更新登录人员基本信息(客户端)
     */
    void updateLoginPersonInfo(ClientPersonInfoDTO dto);

    /**
     * 修改手机号码-解绑发送短信验证码
     */
    void changePhoneSendCode();

    /**
     * 修改手机号码-解绑验证短信验证码
     *
     * @param validateCode
     */
    void changePhoneValidate(String validateCode);

    /**
     * 修改手机号码-绑定新手机发送短信验证码
     * @param dto
     */
    void changePhoneBindSendCode(ClientChangePhoneCodeDTO dto);

    /**
     * 修改手机号码-绑定
     * @param dto
     */
    void changePhone(ClientChangePhoneDTO dto);

    /**
     * 修改邮箱
     * @param mail
     */
    void changeMail(String mail);

    /**
     * 修改性别
     * @param sex
     */
    void changeGender(String sex);

    /**
     * 修改昵称
     * @param nick
     */
    void changeNick(String nick);

    /**
     * 用户主动修改密码
     * @param dto
     */
    void updatePassWord(UpdatePassWordDTO dto);

    /**
     * 管理员设置密码
     * @param dto
     */
    void setPassword(SetPassWordDTO dto);

    /**
     * 根据客户id获取手机号码、邮箱、微信公众号
     * @param customerId
     * @return
     */
    Person getPhoneByCustomerId(Long customerId);

    /**
     * 批量根据客户id获取手机号码
     * @param customerIds
     * @return
     */
    Map<Long,Person> getPhoneByCustomerIdBatch(List<Long> customerIds);

    /**
     * 人员取消微信公众号关注
     * @param wxgzhId
     */
    void unsubscribeWxgzh(String wxgzhId);

    /**
     * 同步人员资料通过微信
     * @param vo
     */
    void updatePersonByWeixin(ThreeLoginVO vo);

    /**
     * 通过人员更新微信公众号
     * @param personId
     */
    void subscribeWxgzh(Long personId,String wxgzhId);

    /**
     * 修改人员客户
     * @param personId
     * @param customerId
     */
    void updatePersonCustomerId(Long personId,Long customerId);
}