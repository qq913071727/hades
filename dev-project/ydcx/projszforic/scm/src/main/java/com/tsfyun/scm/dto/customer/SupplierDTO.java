package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 请求实体
 * </p>
 *
 * @since 2020-03-12
 */
@Data
@ApiModel(value="Supplier请求对象", description="请求实体")
public class SupplierDTO implements Serializable {

     private static final long serialVersionUID=1L;

    @NotNull(message = "供应商不能为空",groups = {UpdateGroup.class})
    private Long id;

    @NotEmptyTrim(message = "请选择所属客户")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @NotEmptyTrim(message = "供应商名称不能为空")
    @LengthTrim(max = 100,message = "供应商名称最大长度为100")
    @ApiModelProperty(value = "名称")
    private String name;

    @LengthTrim(max = 255,message = "供应商地址不能为空")
    @ApiModelProperty(value = "地址")
    private String address;

}
