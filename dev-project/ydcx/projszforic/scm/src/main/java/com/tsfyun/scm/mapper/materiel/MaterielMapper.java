package com.tsfyun.scm.mapper.materiel;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.materiel.MaterielQTO;
import com.tsfyun.scm.entity.materiel.Materiel;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.materiel.MaterielVO;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import com.tsfyun.scm.vo.materiel.client.ClientMaterielVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-03-22
 */
@Repository
public interface MaterielMapper extends Mapper<Materiel> {

    @DataScope(tableAlias = "a",customerTableAlias = "b",addCustomerNameQuery = true)
    List<MaterielVO> pageList(Map<String,Object> qto);

    MaterielVO findByKey(@Param(value = "customerId") Long customerId,@Param(value = "model") String model,@Param(value = "brand") String brand);
    //查询不等于自己的物料集合（归类防止修改了品牌导致物料重复）
    List<Materiel> findByRepeat(Materiel materiel);
    //查询一条已归类产品信息
    Materiel findOneClassed(@Param(value = "model")String model,@Param(value = "brand")String brand);
    //批量保存
    Integer batchSaveList(@Param(value = "materielList")List<Materiel> materielList);
    //批量保存归类
    Integer batchSaveClassifyList(@Param(value = "materielList")List<Materiel> materielList);
    //查询需要分析的产品
    List<String> findWaitMacth();
    //修改需要分析的产品标志
    Integer updateWaitMacth(@Param(value = "ids")List<String> ids);
    //根据id集合查询
    List<Materiel> findInId(@Param(value = "ids")List<String> ids);
    //根据型号品牌查询
    List<Materiel> findByModelAndBrand(@Param(value = "model")String model,@Param(value = "brand")String brand);
    //模糊查询
    @DataScope(tableAlias = "a",customerTableAlias = "b",addCustomerNameQuery = false)
    List<SimpleMaterielVO> vague(Map<String,Object> params);

    //客户端模糊搜索
    List<SimpleMaterielVO> clientVague(@Param(value = "customerId") Long customerId,@Param(value = "model") String model,@Param(value = "brand") String brand,
                                       @Param(value = "name") String name);

    //客户端分页查询
    List<ClientMaterielVO> clientPage(@Param(value = "keyword")String keyword,@Param(value = "statusId")String statusId
            ,@Param(value = "customerId")Long customerId
            ,@Param(value = "dateCreateStart") LocalDateTime dateCreateStart
            ,@Param(value = "dateCreateEnd") LocalDateTime dateCreateEnd
            ,@Param(value = "hasTariff") Boolean hasTariff
            ,@Param(value = "tariffHsCodes")List<String> tariffHsCodes);
}
