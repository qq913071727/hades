package com.tsfyun.scm.vo.materiel;

import com.tsfyun.common.base.enums.MaterielBrandTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 物料品牌响应实体
 * </p>
 *

 * @since 2020-03-26
 */
@Data
@ApiModel(value="MaterielBrand响应对象", description="物料品牌响应实体")
public class MaterielBrandVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "品牌名称")
    private String name;

    @ApiModelProperty(value = "品牌描述")
    private String memo;

    @ApiModelProperty(value = "品牌类型")
    private String type;

    @ApiModelProperty(value = "品牌类型描述")
    private String typeDesc;

    /**
     * 品牌类型描述转义
     * @return
     */
    public String getTypeDesc() {
        MaterielBrandTypeEnum materielBrandTypeEnum = MaterielBrandTypeEnum.of(this.getType());
        return Objects.nonNull(materielBrandTypeEnum) ? materielBrandTypeEnum.getName() : null;
    }

}
