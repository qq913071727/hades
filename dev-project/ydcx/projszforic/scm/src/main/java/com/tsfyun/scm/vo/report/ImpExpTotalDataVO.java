package com.tsfyun.scm.vo.report;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ImpExpTotalDataVO implements Serializable {

    private Integer monthImpSingleQuantity;// 本月进口单量
    private Integer monthExpSingleQuantity;// 本月出口单量
    private Integer dayImpSingleQuantity;// 当日进口单量
    private Integer dayExpSingleQuantity;// 当日出口单量

    private BigDecimal monthImpAmountMoney;// 本月进口金额
    private BigDecimal monthExpAmountMoney;// 本月出口金额
    private BigDecimal dayImpAmountMoney;// 当日进口金额
    private BigDecimal dayExpAmountMoney;// 当日出口金额

    private Integer monthImpTrainNumber;// 本月进口车次
    private Integer monthExpTrainNumber;// 本月出口车次
    private Integer dayImpTrainNumber;// 当日进口车次
    private Integer dayExpTrainNumber;// 当日出口车次
}
