package com.tsfyun.scm.service.third;

import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.third.LogSms;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-09-16
 */
public interface ILogSmsService extends IService<LogSms> {
    /**=
     * 统计一内时候有发送
     * @param phoneNo
     * @param messageNode
     * @return
     */
    Integer countOneMinute(String phoneNo, MessageNodeEnum messageNode);

    /**=
     * 统计IP发送条数
     * @param ip
     * @param minute
     * @return
     */
    Integer countByIpAndMinute(String ip,Integer minute);

    /**
     * 指定分钟内最新验证码
     * @param phoneNo
     * @param messageNode
     * @param minute
     * @return
     */
    String selectCodeNew(String phoneNo, MessageNodeEnum messageNode,Integer minute);
}
