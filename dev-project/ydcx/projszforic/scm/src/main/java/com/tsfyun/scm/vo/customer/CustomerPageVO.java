package com.tsfyun.scm.vo.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.entity.customer.Customer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CustomerPageVO implements Serializable {

    @JsonIgnore
    private PageInfo<CustomerVO> pageInfo;

    private int total;

    private List<CustomerVO> list;


}
