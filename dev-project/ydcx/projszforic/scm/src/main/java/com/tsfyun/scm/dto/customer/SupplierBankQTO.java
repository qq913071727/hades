package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * 供应商查询实体
 */
@Data
public class SupplierBankQTO extends PaginationDto implements Serializable {

    private String customerName;

    private String supplierId;

    private String supplierName;

    private String name;


}
