package com.tsfyun.scm.mapper.customer;


import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.customer.CustomerPersonChange;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-12
 */
public interface CustomerPersonChangeMapper extends Mapper<CustomerPersonChange> {

}
