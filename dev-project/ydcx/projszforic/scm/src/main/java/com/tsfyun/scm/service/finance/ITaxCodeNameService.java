package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.TaxCodeName;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 * 客户税收分类编码 服务类
 * </p>
 *

 * @since 2020-05-21
 */
public interface ITaxCodeNameService extends IService<TaxCodeName> {

    /**
     * 根据客户id和物料名称获取客户分类编码数据
     * @param customerId
     * @param goodsName
     * @return
     */
    TaxCodeName getCustomerTaxCodeName(Long customerId, String goodsName);

    /**
     * 优先查询客户税收分类编码，未查到则查询默认税收分类编码
     * @param customerId
     * @param goodsName
     * @return
     */
    TaxCodeName defaultTaxCodeName(Long customerId, String goodsName);


    Integer batchInsert(List<TaxCodeName> codeNameList);
}
