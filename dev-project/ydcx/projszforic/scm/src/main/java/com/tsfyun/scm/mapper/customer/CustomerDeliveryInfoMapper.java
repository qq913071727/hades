package com.tsfyun.scm.mapper.customer;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.dto.customer.CustomerDeliveryInfoDTO;
import com.tsfyun.scm.entity.customer.CustomerDeliveryInfo;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customer.CustomerDeliveryInfoVO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-03
 */
@Repository
public interface CustomerDeliveryInfoMapper extends Mapper<CustomerDeliveryInfo> {

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = true)
    //获取收货地址信息
    List<CustomerDeliveryInfoVO> list(Map<String,Object> params);

    @DataScope(tableAlias = "d",customerTableAlias = "c",addCustomerNameQuery = false)
    List<CustomerDeliveryInfoVO> effectiveList(Map<String,Object> params);

}
