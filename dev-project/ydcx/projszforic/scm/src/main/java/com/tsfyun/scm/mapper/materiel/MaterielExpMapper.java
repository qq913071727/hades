package com.tsfyun.scm.mapper.materiel;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.materiel.Materiel;
import com.tsfyun.scm.entity.materiel.MaterielExp;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.materiel.MaterielExpVO;
import com.tsfyun.scm.vo.materiel.MaterielVO;
import com.tsfyun.scm.vo.materiel.SimpleMaterielVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2021-03-08
 */
@Repository
public interface MaterielExpMapper extends Mapper<MaterielExp> {

    @DataScope(tableAlias = "a",customerTableAlias = "b",addCustomerNameQuery = true)
    List<MaterielExpVO> pageList(Map<String,Object> qto);
    //批量保存
    Integer batchSaveList(@Param(value = "materielList")List<MaterielExp> materielList);
    //批量保存归类
    Integer batchSaveClassifyList(@Param(value = "materielList")List<MaterielExp> materielList);
    //根据产品唯一性查询物料
    MaterielExpVO findByKey(@Param(value = "customerId") Long customerId,@Param(value = "model") String model,@Param(value = "brand") String brand,@Param(value = "name") String name,@Param(value = "spec") String spec);

    List<MaterielExp> findByMateriel(@Param(value = "model") String model,@Param(value = "brand") String brand,@Param(value = "name") String name,@Param(value = "spec") String spec);
    //根据id集合查询
    List<MaterielExp> findInId(@Param(value = "ids")List<String> ids);
    //查询不等于自己的物料集合（归类防止修改了品牌导致物料重复）
    List<MaterielExp> findByRepeat(MaterielExp materiel);
    //模糊搜索物料
    @DataScope(tableAlias = "a",customerTableAlias = "b",addCustomerNameQuery = false)
    List<SimpleMaterielVO> vague(Map<String,Object> params);
}
