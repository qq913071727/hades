package com.tsfyun.scm.dto.support;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 发送短信
 * @CreateDate: Created in 2020/12/22 09:52
 */
@Data
public class SendSmsDTO implements Serializable {

    /**
     * 短信平台
     */
    private String msgPlatform;

    /**
     * 发送短信的id
     */
    private Long id;

    /**
     * 发送短信的手机号码
     */
    private String phoneNo;

    /**
     * 短信模板编码
     */
    private String templateCode;

    /**
     * 发送短信参数  json字符串，不同的平台需要转换成对应接受的参数
     */
    private String paramJson;

}
