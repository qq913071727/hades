package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 邮件配置信息
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class MailConfig extends BaseEntity {

     private static final long serialVersionUID=1L;


    /**
     * 邮箱服务器
     */

    private String mailHost;

    /**
     * 邮箱端口
     */
    private Integer mailPort;

    /**
     * 邮箱用户名
     */

    private String mailUser;

    /**
     * 邮箱密码
     */

    private String mailPassword;

    /**
     * 失败重试次数
     */
    private Integer retryTimes;

    /**
     * 是否默认
     */
    private Boolean isDefault;

    /**
     * 是否禁用
     */
    private Boolean disabled;


}
