package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.NoTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.LoginRoleTypeEnum;
import com.tsfyun.common.base.enums.LoginTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 登录请求信息（非第三方登录）
 */
@Data
public class LoginDTO implements Serializable {

    /**
     * 登录类型
     */
    @NotNull(message = "登录类型不能为空")
    @EnumCheck(clazz = LoginTypeEnum.class,message = "登录类型错误")
    private Integer loginType;

    /**=
     * 角色类型
     */
    @NotEmptyTrim(message = "角色类型不能为空")
    @EnumCheck(clazz = LoginRoleTypeEnum.class,message = "角色类型错误")
    private String roleType;

    /**
     * 登录账号
     */
    @NotEmptyTrim(message = "登录账号不能为空")
    private String userName;

    /**
     * 密码
     */
    @NoTrim
    private String passWord;

    /**
     * 手机验证码
     */
    private String messageCode;

    /**
     * 登录验证码
     */
    private String validateCode;

    /**
     * 地理位置
     */
    private String local;

    /**
     * 请求id
     */
    private String deviceId;

    /**
     * 绑定第三方账号
     */
    private String bindAccessId;

    /**
     * 滑块验证码信息
     */
    String captchaVerification;
}
