package com.tsfyun.scm.base.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OperationMaintenance implements Serializable {

    private String currency;//币制
    private BigDecimal val;//值
    private Integer type;//类型(1：率，2：单价 3：总价)
}
