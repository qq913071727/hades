package com.tsfyun.scm.dto.materiel;

import java.time.LocalDateTime;

import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-03-22
 */
@Data
@ApiModel(value="Materiel请求对象", description="请求实体")
public class MaterielDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @ApiModelProperty(value = "客户")
   private Long customerId;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 100,message = "型号最大长度不能超过100位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 100,message = "名称最大长度不能超过100位")
   @ApiModelProperty(value = "名称")
   private String name;

   @LengthTrim(max = 500,message = "规格参数最大长度不能超过500位")
   @ApiModelProperty(value = "规格参数")
   private String spec;

   @LengthTrim(max = 50,message = "来源最大长度不能超过50位")
   @ApiModelProperty(value = "来源")
   private String source;

   @LengthTrim(max = 32,message = "来源标识最大长度不能超过32位")
   @ApiModelProperty(value = "来源标识")
   private String sourceMark;
}
