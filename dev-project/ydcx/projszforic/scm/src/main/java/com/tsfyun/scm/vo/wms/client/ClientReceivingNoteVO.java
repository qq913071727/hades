package com.tsfyun.scm.vo.wms.client;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ReceivingNoteStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @Description: 客户端境外入库响应
 * @CreateDate: Created in 2020/11/9 15:11
 */
@Data
public class ClientReceivingNoteVO implements Serializable {

    private Long id;

    private String docNo;

    private String statusId;

    private String statusDesc;

    private String supplierName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime receivingDate;

    /**
     * 入库方式
     */
    private String deliveryMode;

    private String deliveryModeName;

    private String hkExpressName;

    private String hkExpressNo;

    private String warehouseName;

    private String orderNo;

    private String regOrderNo;

    private String totalCartonNum;

    private String memo;

    public String getStatusDesc() {
        return Optional.ofNullable(ReceivingNoteStatusEnum.of(statusId)).map(ReceivingNoteStatusEnum::getName).orElse("");
    }

}
