package com.tsfyun.scm.mapper.customer;

import com.tsfyun.scm.entity.customer.ExpressStandardQuote;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 快递模式标准报价代理费 Mapper 接口
 * </p>
 *
 *
 * @since 2020-10-26
 */
@Repository
public interface ExpressStandardQuoteMapper extends Mapper<ExpressStandardQuote> {

}
