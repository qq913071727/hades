package com.tsfyun.scm.dto.order;

import com.tsfyun.scm.entity.order.ExpOrderMember;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/26 17:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpOrderMemberSaveDTO implements Serializable {

    /**
     * 需要新增/修改的订单明细
     */
    private List<ExpOrderMember> saveOrderMembers;

    /**
     * 需要删除的订单明细
     */
    private List<ExpOrderMember> deleteOrderMembers;

}
