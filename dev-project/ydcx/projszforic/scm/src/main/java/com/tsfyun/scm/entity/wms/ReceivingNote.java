package com.tsfyun.scm.entity.wms;

import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 入库单
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
public class ReceivingNote extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 入库单号
     */

    private String docNo;

    /**
     * 单据类型-枚举
     */

    private String billType;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 入库时间
     */

    private LocalDateTime receivingDate;

    /**
     * 交货方式
     */

    private String deliveryMode;

    /**
     * 交货方式名称
     */
    private String deliveryModeName;

    /**
     * 状态编码
     */

    private String statusId;

    /**
     * 仓库
     */

    private Long warehouseId;

    /**
     * 供应商
     */

    private Long supplierId;

    /**
     * 总箱数
     */

    private Integer totalCartonNum;

    /**
     * 快递类型
     */

    private String inExpressType;

    /**
     * 快递公司
     */

    private String hkExpress;

    /**
     * 快递公司
     */

    private String hkExpressName;

    /**
     * 快递单号
     */

    private String hkExpressNo;

    /**
     * 备注
     */

    private String memo;

    /**
     * 订单号
     */

    private String orderNo;

    /**
     * 入库登记订单号
     */

    private String regOrderNo;
}
