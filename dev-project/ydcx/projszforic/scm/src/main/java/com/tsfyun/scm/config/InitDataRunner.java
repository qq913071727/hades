package com.tsfyun.scm.config;

import com.tsfyun.common.base.util.EntityUtil;
import com.tsfyun.scm.base.DataCalling;
import com.tsfyun.scm.client.BaseDataClient;
import com.tsfyun.scm.client.CustomsCodeClient;
import com.tsfyun.scm.client.DeclareServerClient;
import com.tsfyun.scm.service.system.ISysParamService;
import com.tsfyun.scm.util.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**=
 * 初始化数据
 */
@Component
public class InitDataRunner implements CommandLineRunner {

    @Autowired
    private BaseDataClient baseDataClient;
    @Autowired
    private CustomsCodeClient customsCodeClient;
    @Autowired
    private ISysParamService sysParamService;
    @Autowired
    private DeclareServerClient declareServerClient;

    @Override
    public void run(String... args) throws Exception {
        //系统启动初始化一些静态数据
        //实体状态
        EntityUtil.getInstance().getStatusData();
        //实体数据
        EntityUtil.getInstance().getData();
        //枚举（下拉等等）
        EnumUtils.getInstance().getData();

        DataCalling.getInstance().setBaseDataClient(baseDataClient);
        DataCalling.getInstance().setCustomsCodeClient(customsCodeClient);
        DataCalling.getInstance().setSysParamService(sysParamService);
        DataCalling.getInstance().setDeclareServerClient(declareServerClient);
    }

}
