package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

/**
 * @Description: 客户收货地址查询请求实体
 * @CreateDate: Created in 2020/10/10 15:04
 */
@Data
public class ClientCustomerDeliveryInfoQTO extends PaginationDto {

    private Boolean disabled;
    private Boolean isDefault;

}
