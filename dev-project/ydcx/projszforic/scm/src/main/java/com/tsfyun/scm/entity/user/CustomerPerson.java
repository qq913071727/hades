package com.tsfyun.scm.entity.user;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 *
 * @since 2020-09-15
 */
@Data
public class CustomerPerson implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 人员
     */

    private Long personId;

    /**
     * 时候默认
     */

    private Boolean isDefault;


}
