package com.tsfyun.scm.service.support;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.support.FeedbackDTO;
import com.tsfyun.scm.dto.support.FeedbackQTO;
import com.tsfyun.scm.entity.support.Feedback;
import com.tsfyun.scm.vo.support.FeedbackVO;

/**
 * <p>
 * 意见反馈 服务类
 * </p>
 *
 *
 * @since 2020-09-23
 */
public interface IFeedbackService extends IService<Feedback> {

    /**
     * 新增
     * @param dto
     */
    void add(FeedbackDTO dto);

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<Feedback> pageList(FeedbackQTO qto);

    /**
     * 详情
     * @param id
     * @return
     */
    FeedbackVO detail(Long id);

    /**
     * 删除
     * @param id
     */
    void remove(Long id);

}
