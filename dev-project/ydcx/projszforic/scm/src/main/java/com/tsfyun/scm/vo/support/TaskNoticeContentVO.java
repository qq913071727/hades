package com.tsfyun.scm.vo.support;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 任务通知内容响应实体
 * </p>
 *

 * @since 2020-04-05
 */
@Data
@ApiModel(value="TaskNoticeContent响应对象", description="任务通知内容响应实体")
public class TaskNoticeContentVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "单据类型")
    private String documentType;

    @ApiModelProperty(value = "单据id")
    private String documentId;

    @ApiModelProperty(value = "操作码")
    private String operationCode;

    private String operationName;

    @ApiModelProperty(value = "查询参数")
    private String queryParams;

    @ApiModelProperty(value = "请求参数")
    private String actionParams;

    @ApiModelProperty(value = "任务标题")
    private String title;

    @ApiModelProperty(value = "任务内容")
    private String content;

    private Boolean isExeRemove;//执行后立即删除

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateCreated;

    public String getOperationName(){
        DomainOprationEnum oprationEnum = DomainOprationEnum.of(this.operationCode);
        return Objects.nonNull(oprationEnum)?oprationEnum.getName():"";
    }

}
