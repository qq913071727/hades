package com.tsfyun.scm.stream.send;


import com.tsfyun.common.base.dto.CostWriteOffDTO;
import com.tsfyun.scm.dto.support.SendMailDTO;
import com.tsfyun.scm.dto.support.SendSmsDTO;
import com.tsfyun.scm.dto.support.SendWxDTO;
import com.tsfyun.scm.excel.ClassifyMaterielExcel;
import com.tsfyun.scm.excel.ClassifyMaterielExpExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @since Created in 2019/12/16 15:42
 */
@Component
@Slf4j
@EnableBinding(value = {OutputInterface.class})
public class ScmProcessor {

    @Autowired
    private OutputInterface outputInterface;

    public void saasDemo(String msg) {
        Message message = MessageBuilder.withPayload(msg).build();
        outputInterface.saasDemo().send(message);
    }
    //物料基本信息导入
    public void sendMateriel(List<String> ids){
        Message message = MessageBuilder.withPayload(ids).build();
        outputInterface.sendMateriel().send(message);
    }

    //已归类数据导入(进口)
    public void sendClassifyMateriel(List<ClassifyMaterielExcel> classifyList){
        Message message = MessageBuilder.withPayload(classifyList).build();
        outputInterface.sendClassifyMateriel().send(message);
    }

    //已归类数据导入(出口)
    public void sendClassifyMaterielExp(List<ClassifyMaterielExpExcel> classifyList){
        Message message = MessageBuilder.withPayload(classifyList).build();
        outputInterface.sendClassifyMaterielExp().send(message);
    }

    //费用核销
    public void sendCostWriteOff(CostWriteOffDTO dto){
        Message message = MessageBuilder.withPayload(dto)
                .setHeader("x-delay","15000").build();
        outputInterface.sendCostWriteOff().send(message);
    }

    //发送邮件
    public void sendMail(SendMailDTO dto){
        Message message = MessageBuilder.withPayload(dto)
                .setHeader("x-delay","15000").build();
        outputInterface.sendMail().send(message);
    }

    //发送短信
    public void sendSms(SendSmsDTO dto){
        Message message = MessageBuilder.withPayload(dto)
                .setHeader("x-delay","15000").build();
        outputInterface.sendSms().send(message);
    }

    //发送微信通知
    public void sendWx(SendWxDTO dto){
        Message message = MessageBuilder.withPayload(dto).build();
        outputInterface.sendWx().send(message);
    }

}
