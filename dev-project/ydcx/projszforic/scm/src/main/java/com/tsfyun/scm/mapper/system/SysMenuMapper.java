package com.tsfyun.scm.mapper.system;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.system.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysMenuMapper extends Mapper<SysMenu> {

    /**
     * 根据用户id获取菜单
     * @param menuTypes
     * @param personId
     * @param parentId
     * @return
     */
    List<SysMenu> findByPersonIdAndTypeAndParentId(@Param("menuTypes") List menuTypes,@Param("personId") Long personId,@Param("parentId") String parentId);

    /**
     * 获取所有的菜单
     * @param menuTypes
     * @return
     */
    List<SysMenu> getAllMenu(@Param("menuTypes") List menuTypes);

    /**
     * 根据角色id获取菜单
     * @param menuTypes
     * @param menuTypes
     * @param roleId
     * @return
     */
    List<SysMenu> findByRoleIdAndType(@Param("menuTypes") List menuTypes,@Param("roleId") String roleId);
}
