package com.tsfyun.scm.controller.customer;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.SupplierTakeInfoDTO;
import com.tsfyun.scm.dto.customer.SupplierTakeInfoQTO;
import com.tsfyun.scm.service.customer.ISupplierTakeInfoService;
import com.tsfyun.scm.vo.customer.SupplierTakeInfoListVO;
import com.tsfyun.scm.vo.customer.SupplierTakeInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;
import java.util.List;

/**
 * <p>
 *  供应商提货信息前端控制器
 * </p>
 *
 * @since 2020-03-12
 */
@RestController
@RequestMapping("/supplierTakeInfo")
public class SupplierTakeInfoController extends BaseController {

    @Autowired
    private ISupplierTakeInfoService supplierTakeInfoService;

    /**
     * 获取供应商提货地址下拉
     * @param supplierId
     * @return
     */
    @GetMapping("select")
    public Result<List<SupplierTakeInfoVO>> select(@RequestParam("supplierId")Long supplierId){
        return success(supplierTakeInfoService.select(supplierId));
    }

    /**
     * 根据id获取供应商详情信息
     * @param id
     * @return
     */
    @GetMapping("detail")
    public Result<SupplierTakeInfoVO> detail(@RequestParam("id")Long id){
        return success(supplierTakeInfoService.detail(id));
    }

    /**
     * 设置默认
     * @param id
     * @return
     */
    @PostMapping(value = "setDefault")
    Result<Void> setDefault(@RequestParam(value = "id")Long id) {
        supplierTakeInfoService.setDefault(id);
        return success();
    }


    /**
     * 新增供应商提货信息
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Long> add(@ModelAttribute @Validated(AddGroup.class) SupplierTakeInfoDTO dto) {
        return success(supplierTakeInfoService.add(dto));
    }

    /**
     * 修改供应商提货信息
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) SupplierTakeInfoDTO dto) {
        supplierTakeInfoService.edit(dto);
        return success();
    }

    /**
     * 修改启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        supplierTakeInfoService.updateDisabled(id,disabled);
        return success();
    }


    /**
     * 删除供应商提货信息（真删，不做数据关联校验）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        supplierTakeInfoService.remove(id);
        return success();
    }

    /**
     * 根据客户和供应商名称查询有效的提货信息
     * @param qto
     * @return
     */
    @PostMapping(value = "effectiveList")
    Result<List<SupplierTakeInfoListVO>> effectiveList(SupplierTakeInfoQTO qto) {
        return success(supplierTakeInfoService.effectiveList(qto));
    }

}

