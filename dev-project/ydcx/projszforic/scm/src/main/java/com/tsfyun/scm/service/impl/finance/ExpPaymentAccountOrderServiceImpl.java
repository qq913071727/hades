package com.tsfyun.scm.service.impl.finance;

import com.tsfyun.scm.entity.finance.ExpPaymentAccountOrder;
import com.tsfyun.scm.mapper.finance.ExpPaymentAccountOrderMapper;
import com.tsfyun.scm.service.finance.IExpPaymentAccountOrderService;
import com.tsfyun.common.base.extension.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 付款订单明细 服务实现类
 * </p>
 *
 *
 * @since 2021-11-17
 */
@Service
public class ExpPaymentAccountOrderServiceImpl extends ServiceImpl<ExpPaymentAccountOrder> implements IExpPaymentAccountOrderService {

    @Autowired
    private ExpPaymentAccountOrderMapper expPaymentAccountOrderMapper;

    @Override
    public void removeByPaymentId(Long paymentId) {
        ExpPaymentAccountOrder delete = new ExpPaymentAccountOrder();
        delete.setPaymentAccountId(paymentId);
        expPaymentAccountOrderMapper.delete(delete);
    }
}
