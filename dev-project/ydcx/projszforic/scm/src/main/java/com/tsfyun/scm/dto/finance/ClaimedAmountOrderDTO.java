package com.tsfyun.scm.dto.finance;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ClaimedAmountOrderDTO implements Serializable {

    @NotNull(message = "订单ID不能为空")
    private Long orderId;

    @NotNull(message = "本次认领金额不能为空")
    @Digits(integer = 10, fraction = 2, message = "本次认领金额整数位不能超过10位，小数位不能超过2")
    @DecimalMin(value = "0.01",message = "本次认领金额必须大于0")
    private BigDecimal unclaimedValue;

    private String orderNo;// 订单编号
}
