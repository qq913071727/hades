package com.tsfyun.scm.vo.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.CustomerStatusEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class CustomerVO implements Serializable {

    private Long id;

    /**
     * 客户代码
     */
    private String code;

    /**
     * 客户名称
     */
    private String name;

    /**
     * 拼音
     */
    private String pinyin;

    /**
     * 英文名称
     */
    private String nameEn;

    /**
     * 统一社会信用代码(18位)
     */
    private String socialNo;

    /**
     * 税务登记证
     */
    private String taxpayerNo;

    /**
     * 海关注册编码
     */
    private String customsCode;

    /**
     * 检验检疫编码
     */
    private String ciqNo;

    /**
     * 公司法人
     */
    private String legalPerson;

    /**
     * 公司电话
     */
    private String tel;

    /**
     * 公司传真
     */
    private String fax;

    /**
     * 公司地址
     */
    private String address;

    /**
     * 公司地址英文
     */
    private String addressEn;

    /**
     * 销售员工
     */
    private Long salePersonId;

    private String salePersonName;

    /**
     * 主要商务
     */
    private Long busPersonId;
    private String busPersonName;
    /**
     * 辅助商务
     */
    private Long busSecPersonId;
    private String busSecPersonName;


    /**
     * 状态编码
     */
    private String statusId;

    /**
     * 开户银行
     */
    private String invoiceBankName;

    /**
     * 银行帐号
     */
    private String invoiceBankAccount;

    /**
     * 开票地址
     */
    private String invoiceBankAddress;

    /**
     * 开票电话
     */
    private String invoiceBankTel;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系电话
     */
    private String linkTel;

    private String invoiceProvince;
    private String invoiceCity;
    private String invoiceArea;

    /**
     * 联系地址
     */
    private String linkAddress;

    /**=
     * 开票要求
     */
    private String invoiceMemo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;

    /**=
     * 禁用状态
     */
    private Boolean disabled;

    /**
     * 认领日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime claimDate;

    /**
     * 签约时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime signDate;

    /**
     * 首单时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime firstOrderDate;

    /**
     * 最近一单时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime endOrderDate;

    /**
     * 货款额度
     */
    private BigDecimal goodsQuota;

    /**
     * 税款额度
     */
    private BigDecimal taxQuota;

    /**
     * 状态描述
     */
    private String statusDesc;

    private String phoneNo;// 手机号
    private Boolean isPhone;// 是否注册手机
    private Boolean isWechat;// 是否注册微信

    public String getStatusDesc() {
        CustomerStatusEnum customerStatusEnum = CustomerStatusEnum.of(statusId);
        return Objects.nonNull(customerStatusEnum) ? customerStatusEnum.getName() : "";
    }

}
