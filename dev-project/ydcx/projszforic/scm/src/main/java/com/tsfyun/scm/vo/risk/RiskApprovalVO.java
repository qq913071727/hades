package com.tsfyun.scm.vo.risk;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.RiskApprovalStatusEnum;
import com.tsfyun.common.base.enums.risk.RiskApprovalDocTypeEnum;
import com.tsfyun.scm.entity.risk.RiskApproval;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * <p>
 * 风控审批响应实体
 * </p>
 *

 * @since 2020-05-08
 */
@Data
@ApiModel(value="RiskApproval响应对象", description="风控审批响应实体")
public class RiskApprovalVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "审核类型")
    private String docType;

    @ApiModelProperty(value = "单据id")
    private String docId;

    @ApiModelProperty(value = "单据编号")
    private String docNo;

    @ApiModelProperty(value = "客户id")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "提交人")
    private String submitter;

    @ApiModelProperty(value = "审批人")
    private String approver;

    @ApiModelProperty(value = "审批意见")
    private String approvalInfo;

    @ApiModelProperty(value = "审批原因")
    private String approvalReason;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;

    private String statusId;

    private String statusDesc;

    private String docTypeDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(RiskApprovalStatusEnum.of(statusId)).map(RiskApprovalStatusEnum::getName).orElse("");
    }

    public String getDocTypeDesc() {
        return Optional.ofNullable(RiskApprovalDocTypeEnum.of(docType)).map(RiskApprovalDocTypeEnum::getName).orElse("");
    }
}
