package com.tsfyun.scm.mapper.customer;

import com.tsfyun.scm.entity.customer.ImpQuote;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 进口报价 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-01
 */
@Repository
public interface ImpQuoteMapper extends Mapper<ImpQuote> {

    //根据协议删除报价
    Integer deleteByAgreementId(@Param(value = "agreementId")Long agreementId);
}
