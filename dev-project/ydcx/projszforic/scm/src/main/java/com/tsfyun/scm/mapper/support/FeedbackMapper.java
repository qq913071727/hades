package com.tsfyun.scm.mapper.support;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.support.Feedback;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 意见反馈 Mapper 接口
 * </p>
 *
 *
 * @since 2020-09-23
 */
@Repository
public interface FeedbackMapper extends Mapper<Feedback> {

}
