package com.tsfyun.scm.controller.system;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.system.MailConfigDTO;
import com.tsfyun.scm.dto.system.TestMailConfigDTO;
import com.tsfyun.scm.service.system.IMailConfigService;
import com.tsfyun.scm.vo.system.MailConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 邮箱配置 前端控制器
 * @since Created in 2020/3/30 14:32
 */
@RestController
@RequestMapping(value = "mail")
public class MailConfigController extends BaseController {

    @Autowired
    private IMailConfigService mailConfigService;

    /**=
     * 查询所有数据
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<MailConfigVO>> allList( ) {
        return success(mailConfigService.allList( ));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    public Result<Void> add(@ModelAttribute @Valid MailConfigDTO dto){
        mailConfigService.add(dto);
        return success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) MailConfigDTO dto){
        mailConfigService.edit(dto);
        return success();
    }

    /**
     * 删除（真删）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam(value = "id") Long id){
        mailConfigService.delete(id);
        return success();
    }

    /**
     * 启用/禁用
     */
    @PostMapping("/updateDisabled")
    public Result<Void> updateDisabled(@RequestParam("id")Long id,@RequestParam("disabled")Boolean disabled){
        mailConfigService.updateDisabled(id,disabled);
        return success();
    }

    /**
     * 设置默认/取消
     */
    @PostMapping("/setDefault")
    public Result<Void> setDefault(@RequestParam("id")Long id,@RequestParam("isDefault")Boolean isDefault){
        mailConfigService.setDefault(id,isDefault);
        return success();
    }


    /**
     * 详情
     */
    @GetMapping("/detail")
    public Result<MailConfigVO> detail(@RequestParam(value = "id")Long id){
        return success(mailConfigService.detail(id));
    }

    /**
     * 测试连接
     */
    @PostMapping("/checkConnect")
    public Result<Boolean> checkConnect(@ModelAttribute @Valid TestMailConfigDTO dto){
        return success(mailConfigService.checkConnect(dto));
    }

}
