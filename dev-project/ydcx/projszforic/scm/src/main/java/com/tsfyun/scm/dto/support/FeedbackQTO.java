package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

/**
 * @Description: 意见反馈查询请求实体
 * @CreateDate: Created in 2020/9/24 14:21
 */
@Data
public class FeedbackQTO extends PaginationDto {

    private String questionType;

    private String memo;

    private String contact;

}
