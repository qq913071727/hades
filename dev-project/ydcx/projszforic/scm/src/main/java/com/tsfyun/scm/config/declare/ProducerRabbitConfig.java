package com.tsfyun.scm.config.declare;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 配置rabbitMQ
 *
 *
 */
@Slf4j
@Component
@Configuration
public class ProducerRabbitConfig {

    /**
     * rabbitmq配置信息
     */
    @Value("${tsfyun.rabbitmq.host}")
    private String host;
    @Value("${tsfyun.rabbitmq.username}")
    private String username;
    @Value("${tsfyun.rabbitmq.password}")
    private String password;
    @Value("${tsfyun.rabbitmq.port}")
    private Integer port;
    @Value("${tsfyun.rabbitmq.virtual-host}")
    private String virtualHost;

    public static final String NOTICE_EXCHANGE_NAME = "noticeExchange";
    //报关单报文通知客户端消息
    public static final String DECLARE_SEND = "scm_declare_receive";
    //报关服务 资金核销
    public static final String DECLARE_SEND_WRITE_OFF = "scm_declare_write_off";

    @Bean("declareConnectionFactory")
    public ConnectionFactory declareConnectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host,port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setPublisherConfirms(true);
        return connectionFactory;
    }


    @Bean("declareRabbitTemplate")
    public RabbitTemplate declareRabbitTemplate(ConnectionFactory declareConnectionFactory){
        RabbitTemplate rabbitTemplate =new RabbitTemplate(declareConnectionFactory);
        return rabbitTemplate;
    }



}
 