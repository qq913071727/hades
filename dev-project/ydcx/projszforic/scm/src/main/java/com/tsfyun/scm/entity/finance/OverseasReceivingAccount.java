package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 境外收款
 * </p>
 *
 *
 * @since 2021-10-08
 */
@Data
public class OverseasReceivingAccount extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 收款方式
     */

    private String receivingMode;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 主体类型
     */
    private String subjectType;

    /**
     * 收款主体
     */
    private Long overseasId;

    /**
     * 收款主体
     */
    private String overseasName;

    /**
     * 收款行
     */

    private String receivingBank;

    /**
     * 收款行账号
     */
    private String receivingBankNo;

    /**
     * 收款币制
     */

    private String currencyId;

    /**
     * 收款币制
     */

    private String currencyName;

    /**
     * 收款金额
     */

    private BigDecimal accountValue;

    /**
     * 手续费承担方式
     */
    private String undertakingMode;

    /**
     * 手续费
     */

    private BigDecimal fee;

    /**
     * 收款时间
     */

    private LocalDateTime accountDate;

    /**
     * 付款方
     */

    private String payer;

    /**
     * 付款行
     */
    private String payerBank;

    /**
     * 付款行账号
     */
    private String payerBankNo;

    /**
     * 认领金额
     */

    private BigDecimal writeValue;

    /**
     * 是否结汇
     */
    private Boolean isSettlement;

    /**
     * 结汇单ID
     */
    private Long settlementId;

    /**
     * 结汇汇率
     */
    private BigDecimal customsRate;

    /**
     * 是否付款
     */
    private Boolean isPayment;

    /**
     * 付款单ID
     */
    private Long paymentId;

    /**
     * 结汇单
     */
    private String settlementNo;

    /**
     * 结汇金额（人民币）
     */
    private BigDecimal settlementValue;

    /**
     * 认领的订单编号
     */
    private String orderNo;

    /**
     * 认领的客户单号
     */
    private String clientNo;

    /**
     * 备注
     */

    private String memo;


}
