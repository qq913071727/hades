package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description:客户变更销售/变更商务请求实体
 * @since Created in 2020/4/1 16:55
 */
@Data
public class ChangeCustomerPersonDTO implements Serializable {

    /**
     * 客户id
     */
    @NotNull(message = "客户id不能为空")
    private Long id;

    /**
     * 销售人员id
     */
    @NotNull(message = "请选择销售人员",groups = AddGroup.class)
    private Long salePersonId;

    /**
     * 商务人员id
     */
    @NotNull(message = "请选择商务人员",groups = UpdateGroup.class)
    private Long busPersonId;

    /**
     * 备注
     */
    @LengthTrim(max= 500,message = "说明最大长度不能超过500")
    private String memo;

}
