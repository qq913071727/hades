package com.tsfyun.scm.service.file;

import com.tsfyun.common.base.dto.ExportExcelFileDTO;
import com.tsfyun.common.base.dto.ExportFileDTO;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.file.ExportExcelParamsDTO;
import com.tsfyun.scm.entity.file.ExportFile;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-03-04
 */
public interface IExportFileService extends IService<ExportFile> {

    //通用导出Excel文件
    Long exportExcel(ExportExcelFileDTO dto) throws Exception;
    Long exportExcel(ExportExcelParamsDTO dto) throws Exception;
    //根据ID查找文件
    File findExportFile(Long id);
    //删除文件
    void removeExportFile(Long id, String filePath);
    /**
     * 保存导出文件
     * @param dto
     * @return
     */
    Long save(ExportFileDTO dto);
}
