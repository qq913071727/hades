package com.tsfyun.scm.dto.order;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SetupDeclarationDTO implements Serializable {

    @NotNull(message = "订单ID不能为空")
    private Long id;

    private Integer feeMark;//运费标记
    private BigDecimal feeRate;//运费金额
    private String feeCurr;//运费币制

    private Integer insurMark;//保费标记
    private BigDecimal insurRate;//保费金额
    private String insurCurr;//保费币制

    private Integer otherMark;//杂费标记
    private BigDecimal otherRate;//杂费金额
    private String otherCurr;//杂费币制

    private Long tempId;//报关单模板信息
}
