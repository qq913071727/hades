package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.DateUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/10 16:16
 */
@Data
public class ExpAgreementQTO extends PaginationDto implements Serializable {

    /**
     * 客户id
     */
    private String customerId;

    /**
     * 甲方名称
     */
    private String partyaName;

    /**
     * 状态
     */
    private String statusId;

    /**=
     * 报关类型
     */
    private String declareType;

    /**=
     * 协议编号
     */
    private String docNo;

    /**=
     * 协议是否签回
     */
    private Boolean isSignBack;

    /**=
     * 日期查询类型
     */
    @Pattern(regexp = "date_created|signing_date|effect_date|invalid_date|actual_invalid_date",message = "日期查询类型错误")
    private String queryDate;

    /**=
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    /**=
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public void setDateEnd(Date dateEnd){
        if(dateEnd!=null){
            this.dateEnd = DateUtils.parseLong(DateUtils.format(dateEnd,"yyyy-MM-dd 23:59:59"));
        }
    }

}
