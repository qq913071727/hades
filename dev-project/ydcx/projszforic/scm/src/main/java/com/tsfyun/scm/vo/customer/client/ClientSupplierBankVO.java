package com.tsfyun.scm.vo.customer.client;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/29 17:46
 */
@Data
public class ClientSupplierBankVO implements Serializable {

    private Long id;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 银行名称
     */
    private String name;

    /**=
     * 银行代码
     */
    private String code;

    /**
     * 银行账号
     */
    private String account;

    /**=
     * 币制
     */
    private String currencyName;

    /**=
     * 币制
     */
    private String currencyId;

    /**
     * SWIFT CODE
     */
    private String swiftCode;

    /**
     * 银行地址
     */
    private String address;

    /**
     * 禁用标示
     */
    private Boolean disabled;


    /**
     * 默认
     */
    private Boolean isDefault;

}
