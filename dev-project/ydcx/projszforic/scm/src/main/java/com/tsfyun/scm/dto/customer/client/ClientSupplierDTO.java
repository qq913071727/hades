package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/28 09:52
 */
@Data
public class ClientSupplierDTO implements Serializable {

    @NotNull(message = "请选择需要修改的供应商",groups = UpdateGroup.class)
    private Long id;

    @NotEmptyTrim(message = "供应商名称不能为空")
    @LengthTrim(max = 100,message = "供应商名称最大长度为100")
    @ApiModelProperty(value = "名称")
    private String name;

    @LengthTrim(max = 255,message = "供应商地址不能为空")
    @ApiModelProperty(value = "地址")
    private String address;

    @NotNull(message = "请选择是否启用")
    private Boolean disabled;

}


