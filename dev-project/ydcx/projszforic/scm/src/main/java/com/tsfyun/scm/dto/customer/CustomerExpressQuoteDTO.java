package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 客户快递模式代理费报价请求实体
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Data
@ApiModel(value="CustomerExpressQuote请求对象", description="客户快递模式代理费报价请求实体")
public class CustomerExpressQuoteDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "报价名称不能为空")
   @ApiModelProperty(value = "报价名称")
   private Long customerId;

   @NotEmptyTrim(message = "报价类型不能为空")
   @LengthTrim(max = 20,message = "报价类型最大长度不能超过20位")
   @ApiModelProperty(value = "报价类型")
   private String quoteType;

   @NotNull(message = "首重重量不能为空")
   @Digits(integer = 2, fraction = 0,message = "首重重量最大长度不能超过2位")
   @ApiModelProperty(value = "首重重量-单位KG")
   private BigDecimal firstWeight;

   @NotNull(message = "首重费用不能为空")
   @Digits(integer = 3, fraction = 2, message = "首重费用整数位不能超过3位，小数位不能超过2")
   @ApiModelProperty(value = "首重费用-单位元")
   private BigDecimal firstFee;

   @ApiModelProperty(value = "报价开始时间")
   private Date quoteStartTime;

   @ApiModelProperty(value = "报价结束时间")
   private Date quoteEndTime;

   @ApiModelProperty(value = "封顶标识")
   private Boolean capped;

   @Digits(integer = 6, fraction = 2, message = "封顶费用整数位不能超过6位，小数位不能超过2")
   @ApiModelProperty(value = "封顶费用")
   private BigDecimal cappedFee;

   @ApiModelProperty(value = "禁用标识")
   private Boolean disabled;


}
