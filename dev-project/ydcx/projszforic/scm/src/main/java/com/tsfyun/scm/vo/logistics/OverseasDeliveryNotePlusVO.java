package com.tsfyun.scm.vo.logistics;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/10 09:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OverseasDeliveryNotePlusVO implements Serializable {

    private OverseasDeliveryNoteVO overseasDeliveryNote;

    private List<OverseasDeliveryNoteMemberVO> members;

}
