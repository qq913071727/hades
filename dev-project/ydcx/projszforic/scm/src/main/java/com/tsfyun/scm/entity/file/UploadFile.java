package com.tsfyun.scm.entity.file;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-04
 */
@Data
public class UploadFile extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 单据ID
     */
    private String docId;

    /**
     * 单据类型
     */
    private String docType;

    /**
     * 文件业务类型
     */
    private String businessType;

    /**
     * 说明描述
     */
    private String memo;

    /**
     * 实际文件名称
     */
    private String name;

    /**
     * 系统文件名称
     */
    private String nname;

    /**
     * 文件存放路径
     */
    private String path;

    /**
     * 上传人id
     */
    private String uid;

    /**
     * 上传人名称
     */
    private String uname;

    /**
     * 文件后缀
     */
    private String extension;

    /**
     * 是否删除
     */
    private Boolean isDelete;

    /**
     * 删除时间
     */
    private LocalDateTime dateDel;

    /**
     * 创建人id/名称
     */
    private String createBy;


}
