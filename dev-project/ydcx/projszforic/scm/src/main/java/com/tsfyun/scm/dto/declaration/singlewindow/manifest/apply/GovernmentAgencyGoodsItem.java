package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 海关通关货物信息
 * @since Created in 2020/4/22 12:12
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GovernmentAgencyGoodsItem", propOrder = {
        "goodsMeasure"
})
public class GovernmentAgencyGoodsItem {

    //货物毛重信息
    @XmlElement(name = "GoodsMeasure")
    private GoodsMeasure goodsMeasure;


    public GovernmentAgencyGoodsItem () {

    }

    public GovernmentAgencyGoodsItem(GoodsMeasure goodsMeasure) {
        this.goodsMeasure = goodsMeasure;
    }


    public GoodsMeasure getGoodsMeasure() {
        return goodsMeasure;
    }

    public void setGoodsMeasure(GoodsMeasure goodsMeasure) {
        this.goodsMeasure = goodsMeasure;
    }
}
