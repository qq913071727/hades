package com.tsfyun.scm.service.impl.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.PageUtil;
import com.tsfyun.common.base.validator.ValidatorUtils;
import com.tsfyun.scm.dto.order.ImpOrderDomesticTakeQTO;
import com.tsfyun.scm.entity.order.ImpOrderLogistics;
import com.tsfyun.scm.mapper.order.ImpOrderLogisticsMapper;
import com.tsfyun.scm.service.order.IImpOrderLogisticsService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.service.storage.IWarehouseService;
import com.tsfyun.scm.util.TsfWeekendSqls;
import com.tsfyun.scm.vo.order.ImpOrderDomesticVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsSimpleVO;
import com.tsfyun.scm.vo.order.ImpOrderLogisticsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 订单物流信息 服务实现类
 * </p>
 *

 * @since 2020-04-08
 */
@Service
public class ImpOrderLogisticsServiceImpl extends ServiceImpl<ImpOrderLogistics> implements IImpOrderLogisticsService {

    @Autowired
    private ImpOrderLogisticsMapper impOrderLogisticsMapper;

    @Override
    public ImpOrderLogisticsVO findById(Long id) {
        return impOrderLogisticsMapper.findById(id);
    }

    @Override
    public List<ImpOrderLogisticsSimpleVO> getCharterCarDeliveryInfo() {
        LocalDateTime oneMonthBefore = LocalDateTime.now().plusMonths(-1);
        return impOrderLogisticsMapper.getCharterCarDeliveryInfo(oneMonthBefore);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeByImpOrderId(Long orderId) {
        impOrderLogisticsMapper.deleteByExample(Example.builder(ImpOrderLogistics.class).where(TsfWeekendSqls.<ImpOrderLogistics>custom().andEqualTo(false,ImpOrderLogistics::getImpOrderId,orderId)).build());
    }

    @Override
    public ImpOrderLogistics findByOrderId(Long orderId) {
        return impOrderLogisticsMapper.selectOneByExample(Example.builder(ImpOrderLogistics.class).where(TsfWeekendSqls.<ImpOrderLogistics>custom().andEqualTo(false,ImpOrderLogistics::getImpOrderId,orderId)).build());
    }

    @Override
    public PageInfo<ImpOrderDomesticVO> pageList(ImpOrderDomesticTakeQTO qto) {
        List<ImpOrderDomesticVO> list = PageUtil.pageQ(qto,()->impOrderLogisticsMapper.overseasTakeList(beanMapper.map(qto, Map.class)));
        return new PageInfo<>(list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void takeConfirm(Long id,LocalDateTime takeTime) {
        ImpOrderLogistics impOrderLogistics = super.getById(id);
        ValidatorUtils.isTrue(Objects.nonNull(impOrderLogistics),()->new ServiceException("数据不存在"));
        ValidatorUtils.isTrue(!Objects.equals(Boolean.TRUE,impOrderLogistics.getOverseasTakeFlag()),()->new ServiceException("该订单已提货完成"));
        ImpOrderLogistics update = new ImpOrderLogistics();
        update.setOverseasTakeFlag(Boolean.TRUE);
        update.setOverseasTakeTime(Optional.ofNullable(takeTime).orElse(LocalDateTime.now()));
        impOrderLogisticsMapper.updateByExampleSelective(update,Example.builder(ImpOrderLogistics.class).where(
                TsfWeekendSqls.<ImpOrderLogistics>custom().andEqualTo(false,ImpOrderLogistics::getId, id)).build());
    }
}
