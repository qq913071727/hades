package com.tsfyun.scm.mapper.wms;

import com.tsfyun.scm.entity.wms.ReceivingNoteMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.wms.InspectionNoteMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 入库单明细 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface ReceivingNoteMemberMapper extends Mapper<ReceivingNoteMember> {

    List<InspectionNoteMemberVO> inspectionMembers(@Param(value = "id") Long id);
}
