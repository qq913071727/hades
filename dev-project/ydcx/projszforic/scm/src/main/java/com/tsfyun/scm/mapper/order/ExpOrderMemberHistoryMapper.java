package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.entity.order.ExpOrderMemberHistory;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-13
 */
@Repository
public interface ExpOrderMemberHistoryMapper extends Mapper<ExpOrderMemberHistory> {

    void removeByOrderId(@Param(value = "orderId") Long orderId);

    Integer batchUpdateSplitMembers(@Param(value = "expOrderMemberHistoryList") List<ExpOrderMemberHistory> expOrderMemberHistoryList);
}
