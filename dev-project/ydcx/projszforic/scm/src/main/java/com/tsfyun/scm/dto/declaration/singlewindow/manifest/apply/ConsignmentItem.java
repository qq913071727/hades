package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 商品项信息
 * @since Created in 2020/4/22 11:38
 */
@Data
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsignmentItem", propOrder = {
        "sequenceNumeric",
        "commodity",
        "goodsMeasure",
        "packaging",
})
public class ConsignmentItem {

    //商品项序号
    @XmlElement(name = "SequenceNumeric")
    protected String sequenceNumeric;

    //商品项描述
    @XmlElement(name = "Commodity")
    private Commodity commodity;

    //货物毛重信息
    @XmlElement(name = "GoodsMeasure")
    private GoodsMeasure goodsMeasure;

    //货物包装信息
    @XmlElement(name = "Packaging")
    private Packaging packaging;

}
