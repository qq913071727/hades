package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.exp.CollectionSourceEnum;
import com.tsfyun.common.base.enums.finance.ReceivingModeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 费用变更记录请求实体
 * </p>
 *

 * @since 2020-05-27
 */
@Data
@ApiModel(value="CostChangeRecord请求对象", description="费用变更记录请求实体")
public class CostChangeRecordExpDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long expOrderCostId;

   @NotEmptyTrim(message = "订单号不能为空")
   @LengthTrim(max = 20,message = "订单号最大长度不能超过20位")
   @ApiModelProperty(value = "订单号")
   private String expOrderNo;


   @NotEmptyTrim(message = "请选择费用科目")
   @LengthTrim(max = 20,message = "费用科目编码最大长度不能超过20位")
   @ApiModelProperty(value = "费用科目编码")
   private String expenseSubjectId;

   @NotEmptyTrim(message = "请选择支付方式")
   @EnumCheck(clazz = CollectionSourceEnum.class,message = "支付方式错误")
   private String collectionSource;

   @NotNull(message = "应收金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "变更后金额整数位不能超过8位，小数位不能超过2位")
   @DecimalMin(value = "0",message = "应收金额必须大于等于0")
   @ApiModelProperty(value = "应收金额")
   private BigDecimal costAmount;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
