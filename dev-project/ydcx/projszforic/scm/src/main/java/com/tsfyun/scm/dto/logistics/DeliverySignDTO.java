package com.tsfyun.scm.dto.logistics;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class DeliverySignDTO implements Serializable {

    @NotNull(message = "运单ID不能为空")
    private Long id;

    @NotNull(message = "签收时间不能为空")
    private LocalDateTime reachDate;
}
