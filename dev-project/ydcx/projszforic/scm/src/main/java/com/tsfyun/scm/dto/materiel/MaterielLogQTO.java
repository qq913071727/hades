package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

@Data
public class MaterielLogQTO extends PaginationDto {

    private String materielId;
}
