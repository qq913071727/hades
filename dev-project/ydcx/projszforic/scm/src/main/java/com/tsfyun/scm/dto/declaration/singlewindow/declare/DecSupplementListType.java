//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2020.04.20 时间 12:04:50 PM CST 
//


package com.tsfyun.scm.dto.declaration.singlewindow.declare;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 补充申报单表体信息
 * 
 * <p>DecSupplementListType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="DecSupplementListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SupType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BrandCN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BrandEN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Buyer">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BuyerContact">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BuyerAddr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BuyerTel">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Seller">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SellerContact">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SellerAddr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SellerTel">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Factory">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FactoryContact">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FactoryAddr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FactoryTel">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ContrNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ContrDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_BabRel">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PriceEffect">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PriceClose">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_OtherLimited">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_OtherEffect">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_Note1">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_IsUsefee">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_IsProfit">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_Note2">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Curr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *               &lt;minLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoicePrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InvoiceNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GoodsNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_CommissionPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_CommissionAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_CommissionNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ContainerPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ContainerAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ContainerNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PackPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PackAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PackNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PartPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PartAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_PartNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ToolPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ToolAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ToolNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_LossPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_LossAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_LossNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_DesignPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_DesignAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_DesignNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_UsefeePrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_UsefeeAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_UsefeeNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ProfitPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ProfitAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_ProfitNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_FeePrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_FeeAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_FeeNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_OtherPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_OtherAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_OtherNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_InsurPrice">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_InsurAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="I_InsurNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="E_IsDutyDel">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GNameOther">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodeTsOther">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;minLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IsClassDecision">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DecisionNO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodeTsDecision">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="10"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DecisionCus">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="4"/>
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IsSampleTest">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GOptions">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TrafMode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IsDirectTraf">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TransitCountry">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DestPort">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *               &lt;minLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclPort">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *               &lt;minLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BillNo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OriginCountry">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *               &lt;minLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OriginMark">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CertCountry">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CertNO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CertStandard">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OtherNote">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IsSecret">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AgentType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclAddr">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclPost">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DeclTel">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecSupplementListType", propOrder = {
    "gNo",
    "supType",
    "brandCN",
    "brandEN",
    "buyer",
    "buyerContact",
    "buyerAddr",
    "buyerTel",
    "seller",
    "sellerContact",
    "sellerAddr",
    "sellerTel",
    "factory",
    "factoryContact",
    "factoryAddr",
    "factoryTel",
    "contrNo",
    "contrDate",
    "invoiceNo",
    "invoiceDate",
    "iBabRel",
    "iPriceEffect",
    "iPriceClose",
    "iOtherLimited",
    "iOtherEffect",
    "iNote1",
    "iIsUsefee",
    "iIsProfit",
    "iNote2",
    "curr",
    "invoicePrice",
    "invoiceAmount",
    "invoiceNote",
    "goodsPrice",
    "goodsAmount",
    "goodsNote",
    "iCommissionPrice",
    "iCommissionAmount",
    "iCommissionNote",
    "iContainerPrice",
    "iContainerAmount",
    "iContainerNote",
    "iPackPrice",
    "iPackAmount",
    "iPackNote",
    "iPartPrice",
    "iPartAmount",
    "iPartNote",
    "iToolPrice",
    "iToolAmount",
    "iToolNote",
    "iLossPrice",
    "iLossAmount",
    "iLossNote",
    "iDesignPrice",
    "iDesignAmount",
    "iDesignNote",
    "iUsefeePrice",
    "iUsefeeAmount",
    "iUsefeeNote",
    "iProfitPrice",
    "iProfitAmount",
    "iProfitNote",
    "iFeePrice",
    "iFeeAmount",
    "iFeeNote",
    "iOtherPrice",
    "iOtherAmount",
    "iOtherNote",
    "iInsurPrice",
    "iInsurAmount",
    "iInsurNote",
    "eIsDutyDel",
    "gNameOther",
    "codeTsOther",
    "isClassDecision",
    "decisionNO",
    "codeTsDecision",
    "decisionCus",
    "isSampleTest",
    "gOptions",
    "trafMode",
    "isDirectTraf",
    "transitCountry",
    "destPort",
    "declPort",
    "billNo",
    "originCountry",
    "originMark",
    "certCountry",
    "certNO",
    "certStandard",
    "otherNote",
    "isSecret",
    "agentType",
    "declAddr",
    "declPost",
    "declTel"
})
public class DecSupplementListType {

    @XmlElement(name = "GNo", required = true)
    protected String gNo;
    @XmlElement(name = "SupType", required = true)
    protected String supType;
    @XmlElement(name = "BrandCN", required = true)
    protected String brandCN;
    @XmlElement(name = "BrandEN", required = true)
    protected String brandEN;
    @XmlElement(name = "Buyer", required = true)
    protected String buyer;
    @XmlElement(name = "BuyerContact", required = true)
    protected String buyerContact;
    @XmlElement(name = "BuyerAddr", required = true)
    protected String buyerAddr;
    @XmlElement(name = "BuyerTel", required = true)
    protected String buyerTel;
    @XmlElement(name = "Seller", required = true)
    protected String seller;
    @XmlElement(name = "SellerContact", required = true)
    protected String sellerContact;
    @XmlElement(name = "SellerAddr", required = true)
    protected String sellerAddr;
    @XmlElement(name = "SellerTel", required = true)
    protected String sellerTel;
    @XmlElement(name = "Factory", required = true)
    protected String factory;
    @XmlElement(name = "FactoryContact", required = true)
    protected String factoryContact;
    @XmlElement(name = "FactoryAddr", required = true)
    protected String factoryAddr;
    @XmlElement(name = "FactoryTel", required = true)
    protected String factoryTel;
    @XmlElement(name = "ContrNo", required = true)
    protected String contrNo;
    @XmlElement(name = "ContrDate", required = true)
    protected String contrDate;
    @XmlElement(name = "InvoiceNo", required = true)
    protected String invoiceNo;
    @XmlElement(name = "InvoiceDate", required = true)
    protected String invoiceDate;
    @XmlElement(name = "I_BabRel", required = true)
    protected String iBabRel;
    @XmlElement(name = "I_PriceEffect", required = true)
    protected String iPriceEffect;
    @XmlElement(name = "I_PriceClose", required = true)
    protected String iPriceClose;
    @XmlElement(name = "I_OtherLimited", required = true)
    protected String iOtherLimited;
    @XmlElement(name = "I_OtherEffect", required = true)
    protected String iOtherEffect;
    @XmlElement(name = "I_Note1", required = true)
    protected String iNote1;
    @XmlElement(name = "I_IsUsefee", required = true)
    protected String iIsUsefee;
    @XmlElement(name = "I_IsProfit", required = true)
    protected String iIsProfit;
    @XmlElement(name = "I_Note2", required = true)
    protected String iNote2;
    @XmlElement(name = "Curr", required = true)
    protected String curr;
    @XmlElement(name = "InvoicePrice", required = true)
    protected String invoicePrice;
    @XmlElement(name = "InvoiceAmount", required = true)
    protected String invoiceAmount;
    @XmlElement(name = "InvoiceNote", required = true)
    protected String invoiceNote;
    @XmlElement(name = "GoodsPrice", required = true)
    protected String goodsPrice;
    @XmlElement(name = "GoodsAmount", required = true)
    protected String goodsAmount;
    @XmlElement(name = "GoodsNote", required = true)
    protected String goodsNote;
    @XmlElement(name = "I_CommissionPrice", required = true)
    protected String iCommissionPrice;
    @XmlElement(name = "I_CommissionAmount", required = true)
    protected String iCommissionAmount;
    @XmlElement(name = "I_CommissionNote", required = true)
    protected String iCommissionNote;
    @XmlElement(name = "I_ContainerPrice", required = true)
    protected String iContainerPrice;
    @XmlElement(name = "I_ContainerAmount", required = true)
    protected String iContainerAmount;
    @XmlElement(name = "I_ContainerNote", required = true)
    protected String iContainerNote;
    @XmlElement(name = "I_PackPrice", required = true)
    protected String iPackPrice;
    @XmlElement(name = "I_PackAmount", required = true)
    protected String iPackAmount;
    @XmlElement(name = "I_PackNote", required = true)
    protected String iPackNote;
    @XmlElement(name = "I_PartPrice", required = true)
    protected String iPartPrice;
    @XmlElement(name = "I_PartAmount", required = true)
    protected String iPartAmount;
    @XmlElement(name = "I_PartNote", required = true)
    protected String iPartNote;
    @XmlElement(name = "I_ToolPrice", required = true)
    protected String iToolPrice;
    @XmlElement(name = "I_ToolAmount", required = true)
    protected String iToolAmount;
    @XmlElement(name = "I_ToolNote", required = true)
    protected String iToolNote;
    @XmlElement(name = "I_LossPrice", required = true)
    protected String iLossPrice;
    @XmlElement(name = "I_LossAmount", required = true)
    protected String iLossAmount;
    @XmlElement(name = "I_LossNote", required = true)
    protected String iLossNote;
    @XmlElement(name = "I_DesignPrice", required = true)
    protected String iDesignPrice;
    @XmlElement(name = "I_DesignAmount", required = true)
    protected String iDesignAmount;
    @XmlElement(name = "I_DesignNote", required = true)
    protected String iDesignNote;
    @XmlElement(name = "I_UsefeePrice", required = true)
    protected String iUsefeePrice;
    @XmlElement(name = "I_UsefeeAmount", required = true)
    protected String iUsefeeAmount;
    @XmlElement(name = "I_UsefeeNote", required = true)
    protected String iUsefeeNote;
    @XmlElement(name = "I_ProfitPrice", required = true)
    protected String iProfitPrice;
    @XmlElement(name = "I_ProfitAmount", required = true)
    protected String iProfitAmount;
    @XmlElement(name = "I_ProfitNote", required = true)
    protected String iProfitNote;
    @XmlElement(name = "I_FeePrice", required = true)
    protected String iFeePrice;
    @XmlElement(name = "I_FeeAmount", required = true)
    protected String iFeeAmount;
    @XmlElement(name = "I_FeeNote", required = true)
    protected String iFeeNote;
    @XmlElement(name = "I_OtherPrice", required = true)
    protected String iOtherPrice;
    @XmlElement(name = "I_OtherAmount", required = true)
    protected String iOtherAmount;
    @XmlElement(name = "I_OtherNote", required = true)
    protected String iOtherNote;
    @XmlElement(name = "I_InsurPrice", required = true)
    protected String iInsurPrice;
    @XmlElement(name = "I_InsurAmount", required = true)
    protected String iInsurAmount;
    @XmlElement(name = "I_InsurNote", required = true)
    protected String iInsurNote;
    @XmlElement(name = "E_IsDutyDel", required = true)
    protected String eIsDutyDel;
    @XmlElement(name = "GNameOther", required = true)
    protected String gNameOther;
    @XmlElement(name = "CodeTsOther", required = true)
    protected String codeTsOther;
    @XmlElement(name = "IsClassDecision", required = true)
    protected String isClassDecision;
    @XmlElement(name = "DecisionNO", required = true)
    protected String decisionNO;
    @XmlElement(name = "CodeTsDecision", required = true)
    protected String codeTsDecision;
    @XmlElement(name = "DecisionCus", required = true)
    protected String decisionCus;
    @XmlElement(name = "IsSampleTest", required = true)
    protected String isSampleTest;
    @XmlElement(name = "GOptions", required = true)
    protected String gOptions;
    @XmlElement(name = "TrafMode", required = true)
    protected String trafMode;
    @XmlElement(name = "IsDirectTraf", required = true)
    protected String isDirectTraf;
    @XmlElement(name = "TransitCountry", required = true)
    protected String transitCountry;
    @XmlElement(name = "DestPort", required = true)
    protected String destPort;
    @XmlElement(name = "DeclPort", required = true)
    protected String declPort;
    @XmlElement(name = "BillNo", required = true)
    protected String billNo;
    @XmlElement(name = "OriginCountry", required = true)
    protected String originCountry;
    @XmlElement(name = "OriginMark", required = true)
    protected String originMark;
    @XmlElement(name = "CertCountry", required = true)
    protected String certCountry;
    @XmlElement(name = "CertNO", required = true)
    protected String certNO;
    @XmlElement(name = "CertStandard", required = true)
    protected String certStandard;
    @XmlElement(name = "OtherNote", required = true)
    protected String otherNote;
    @XmlElement(name = "IsSecret", required = true)
    protected String isSecret;
    @XmlElement(name = "AgentType", required = true)
    protected String agentType;
    @XmlElement(name = "DeclAddr", required = true)
    protected String declAddr;
    @XmlElement(name = "DeclPost", required = true)
    protected String declPost;
    @XmlElement(name = "DeclTel", required = true)
    protected String declTel;

    /**
     * 获取gNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGNo() {
        return gNo;
    }

    /**
     * 设置gNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGNo(String value) {
        this.gNo = value;
    }

    /**
     * 获取supType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupType() {
        return supType;
    }

    /**
     * 设置supType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupType(String value) {
        this.supType = value;
    }

    /**
     * 获取brandCN属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCN() {
        return brandCN;
    }

    /**
     * 设置brandCN属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCN(String value) {
        this.brandCN = value;
    }

    /**
     * 获取brandEN属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandEN() {
        return brandEN;
    }

    /**
     * 设置brandEN属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandEN(String value) {
        this.brandEN = value;
    }

    /**
     * 获取buyer属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyer() {
        return buyer;
    }

    /**
     * 设置buyer属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyer(String value) {
        this.buyer = value;
    }

    /**
     * 获取buyerContact属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerContact() {
        return buyerContact;
    }

    /**
     * 设置buyerContact属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerContact(String value) {
        this.buyerContact = value;
    }

    /**
     * 获取buyerAddr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerAddr() {
        return buyerAddr;
    }

    /**
     * 设置buyerAddr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerAddr(String value) {
        this.buyerAddr = value;
    }

    /**
     * 获取buyerTel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerTel() {
        return buyerTel;
    }

    /**
     * 设置buyerTel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerTel(String value) {
        this.buyerTel = value;
    }

    /**
     * 获取seller属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeller() {
        return seller;
    }

    /**
     * 设置seller属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeller(String value) {
        this.seller = value;
    }

    /**
     * 获取sellerContact属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerContact() {
        return sellerContact;
    }

    /**
     * 设置sellerContact属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerContact(String value) {
        this.sellerContact = value;
    }

    /**
     * 获取sellerAddr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerAddr() {
        return sellerAddr;
    }

    /**
     * 设置sellerAddr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerAddr(String value) {
        this.sellerAddr = value;
    }

    /**
     * 获取sellerTel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerTel() {
        return sellerTel;
    }

    /**
     * 设置sellerTel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerTel(String value) {
        this.sellerTel = value;
    }

    /**
     * 获取factory属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactory() {
        return factory;
    }

    /**
     * 设置factory属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactory(String value) {
        this.factory = value;
    }

    /**
     * 获取factoryContact属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactoryContact() {
        return factoryContact;
    }

    /**
     * 设置factoryContact属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactoryContact(String value) {
        this.factoryContact = value;
    }

    /**
     * 获取factoryAddr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactoryAddr() {
        return factoryAddr;
    }

    /**
     * 设置factoryAddr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactoryAddr(String value) {
        this.factoryAddr = value;
    }

    /**
     * 获取factoryTel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactoryTel() {
        return factoryTel;
    }

    /**
     * 设置factoryTel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactoryTel(String value) {
        this.factoryTel = value;
    }

    /**
     * 获取contrNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrNo() {
        return contrNo;
    }

    /**
     * 设置contrNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrNo(String value) {
        this.contrNo = value;
    }

    /**
     * 获取contrDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrDate() {
        return contrDate;
    }

    /**
     * 设置contrDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrDate(String value) {
        this.contrDate = value;
    }

    /**
     * 获取invoiceNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * 设置invoiceNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNo(String value) {
        this.invoiceNo = value;
    }

    /**
     * 获取invoiceDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * 设置invoiceDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceDate(String value) {
        this.invoiceDate = value;
    }

    /**
     * 获取iBabRel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBabRel() {
        return iBabRel;
    }

    /**
     * 设置iBabRel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBabRel(String value) {
        this.iBabRel = value;
    }

    /**
     * 获取iPriceEffect属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPriceEffect() {
        return iPriceEffect;
    }

    /**
     * 设置iPriceEffect属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPriceEffect(String value) {
        this.iPriceEffect = value;
    }

    /**
     * 获取iPriceClose属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPriceClose() {
        return iPriceClose;
    }

    /**
     * 设置iPriceClose属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPriceClose(String value) {
        this.iPriceClose = value;
    }

    /**
     * 获取iOtherLimited属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIOtherLimited() {
        return iOtherLimited;
    }

    /**
     * 设置iOtherLimited属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIOtherLimited(String value) {
        this.iOtherLimited = value;
    }

    /**
     * 获取iOtherEffect属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIOtherEffect() {
        return iOtherEffect;
    }

    /**
     * 设置iOtherEffect属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIOtherEffect(String value) {
        this.iOtherEffect = value;
    }

    /**
     * 获取iNote1属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINote1() {
        return iNote1;
    }

    /**
     * 设置iNote1属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINote1(String value) {
        this.iNote1 = value;
    }

    /**
     * 获取iIsUsefee属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIIsUsefee() {
        return iIsUsefee;
    }

    /**
     * 设置iIsUsefee属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIIsUsefee(String value) {
        this.iIsUsefee = value;
    }

    /**
     * 获取iIsProfit属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIIsProfit() {
        return iIsProfit;
    }

    /**
     * 设置iIsProfit属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIIsProfit(String value) {
        this.iIsProfit = value;
    }

    /**
     * 获取iNote2属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINote2() {
        return iNote2;
    }

    /**
     * 设置iNote2属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINote2(String value) {
        this.iNote2 = value;
    }

    /**
     * 获取curr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurr() {
        return curr;
    }

    /**
     * 设置curr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurr(String value) {
        this.curr = value;
    }

    /**
     * 获取invoicePrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicePrice() {
        return invoicePrice;
    }

    /**
     * 设置invoicePrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicePrice(String value) {
        this.invoicePrice = value;
    }

    /**
     * 获取invoiceAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    /**
     * 设置invoiceAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceAmount(String value) {
        this.invoiceAmount = value;
    }

    /**
     * 获取invoiceNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNote() {
        return invoiceNote;
    }

    /**
     * 设置invoiceNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNote(String value) {
        this.invoiceNote = value;
    }

    /**
     * 获取goodsPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 设置goodsPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsPrice(String value) {
        this.goodsPrice = value;
    }

    /**
     * 获取goodsAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsAmount() {
        return goodsAmount;
    }

    /**
     * 设置goodsAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsAmount(String value) {
        this.goodsAmount = value;
    }

    /**
     * 获取goodsNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsNote() {
        return goodsNote;
    }

    /**
     * 设置goodsNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsNote(String value) {
        this.goodsNote = value;
    }

    /**
     * 获取iCommissionPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICommissionPrice() {
        return iCommissionPrice;
    }

    /**
     * 设置iCommissionPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICommissionPrice(String value) {
        this.iCommissionPrice = value;
    }

    /**
     * 获取iCommissionAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICommissionAmount() {
        return iCommissionAmount;
    }

    /**
     * 设置iCommissionAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICommissionAmount(String value) {
        this.iCommissionAmount = value;
    }

    /**
     * 获取iCommissionNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICommissionNote() {
        return iCommissionNote;
    }

    /**
     * 设置iCommissionNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICommissionNote(String value) {
        this.iCommissionNote = value;
    }

    /**
     * 获取iContainerPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIContainerPrice() {
        return iContainerPrice;
    }

    /**
     * 设置iContainerPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIContainerPrice(String value) {
        this.iContainerPrice = value;
    }

    /**
     * 获取iContainerAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIContainerAmount() {
        return iContainerAmount;
    }

    /**
     * 设置iContainerAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIContainerAmount(String value) {
        this.iContainerAmount = value;
    }

    /**
     * 获取iContainerNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIContainerNote() {
        return iContainerNote;
    }

    /**
     * 设置iContainerNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIContainerNote(String value) {
        this.iContainerNote = value;
    }

    /**
     * 获取iPackPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPackPrice() {
        return iPackPrice;
    }

    /**
     * 设置iPackPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPackPrice(String value) {
        this.iPackPrice = value;
    }

    /**
     * 获取iPackAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPackAmount() {
        return iPackAmount;
    }

    /**
     * 设置iPackAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPackAmount(String value) {
        this.iPackAmount = value;
    }

    /**
     * 获取iPackNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPackNote() {
        return iPackNote;
    }

    /**
     * 设置iPackNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPackNote(String value) {
        this.iPackNote = value;
    }

    /**
     * 获取iPartPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPartPrice() {
        return iPartPrice;
    }

    /**
     * 设置iPartPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPartPrice(String value) {
        this.iPartPrice = value;
    }

    /**
     * 获取iPartAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPartAmount() {
        return iPartAmount;
    }

    /**
     * 设置iPartAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPartAmount(String value) {
        this.iPartAmount = value;
    }

    /**
     * 获取iPartNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPartNote() {
        return iPartNote;
    }

    /**
     * 设置iPartNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPartNote(String value) {
        this.iPartNote = value;
    }

    /**
     * 获取iToolPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIToolPrice() {
        return iToolPrice;
    }

    /**
     * 设置iToolPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIToolPrice(String value) {
        this.iToolPrice = value;
    }

    /**
     * 获取iToolAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIToolAmount() {
        return iToolAmount;
    }

    /**
     * 设置iToolAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIToolAmount(String value) {
        this.iToolAmount = value;
    }

    /**
     * 获取iToolNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIToolNote() {
        return iToolNote;
    }

    /**
     * 设置iToolNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIToolNote(String value) {
        this.iToolNote = value;
    }

    /**
     * 获取iLossPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILossPrice() {
        return iLossPrice;
    }

    /**
     * 设置iLossPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILossPrice(String value) {
        this.iLossPrice = value;
    }

    /**
     * 获取iLossAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILossAmount() {
        return iLossAmount;
    }

    /**
     * 设置iLossAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILossAmount(String value) {
        this.iLossAmount = value;
    }

    /**
     * 获取iLossNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILossNote() {
        return iLossNote;
    }

    /**
     * 设置iLossNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILossNote(String value) {
        this.iLossNote = value;
    }

    /**
     * 获取iDesignPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDesignPrice() {
        return iDesignPrice;
    }

    /**
     * 设置iDesignPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDesignPrice(String value) {
        this.iDesignPrice = value;
    }

    /**
     * 获取iDesignAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDesignAmount() {
        return iDesignAmount;
    }

    /**
     * 设置iDesignAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDesignAmount(String value) {
        this.iDesignAmount = value;
    }

    /**
     * 获取iDesignNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDesignNote() {
        return iDesignNote;
    }

    /**
     * 设置iDesignNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDesignNote(String value) {
        this.iDesignNote = value;
    }

    /**
     * 获取iUsefeePrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIUsefeePrice() {
        return iUsefeePrice;
    }

    /**
     * 设置iUsefeePrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIUsefeePrice(String value) {
        this.iUsefeePrice = value;
    }

    /**
     * 获取iUsefeeAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIUsefeeAmount() {
        return iUsefeeAmount;
    }

    /**
     * 设置iUsefeeAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIUsefeeAmount(String value) {
        this.iUsefeeAmount = value;
    }

    /**
     * 获取iUsefeeNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIUsefeeNote() {
        return iUsefeeNote;
    }

    /**
     * 设置iUsefeeNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIUsefeeNote(String value) {
        this.iUsefeeNote = value;
    }

    /**
     * 获取iProfitPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProfitPrice() {
        return iProfitPrice;
    }

    /**
     * 设置iProfitPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProfitPrice(String value) {
        this.iProfitPrice = value;
    }

    /**
     * 获取iProfitAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProfitAmount() {
        return iProfitAmount;
    }

    /**
     * 设置iProfitAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProfitAmount(String value) {
        this.iProfitAmount = value;
    }

    /**
     * 获取iProfitNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProfitNote() {
        return iProfitNote;
    }

    /**
     * 设置iProfitNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProfitNote(String value) {
        this.iProfitNote = value;
    }

    /**
     * 获取iFeePrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIFeePrice() {
        return iFeePrice;
    }

    /**
     * 设置iFeePrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIFeePrice(String value) {
        this.iFeePrice = value;
    }

    /**
     * 获取iFeeAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIFeeAmount() {
        return iFeeAmount;
    }

    /**
     * 设置iFeeAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIFeeAmount(String value) {
        this.iFeeAmount = value;
    }

    /**
     * 获取iFeeNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIFeeNote() {
        return iFeeNote;
    }

    /**
     * 设置iFeeNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIFeeNote(String value) {
        this.iFeeNote = value;
    }

    /**
     * 获取iOtherPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIOtherPrice() {
        return iOtherPrice;
    }

    /**
     * 设置iOtherPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIOtherPrice(String value) {
        this.iOtherPrice = value;
    }

    /**
     * 获取iOtherAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIOtherAmount() {
        return iOtherAmount;
    }

    /**
     * 设置iOtherAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIOtherAmount(String value) {
        this.iOtherAmount = value;
    }

    /**
     * 获取iOtherNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIOtherNote() {
        return iOtherNote;
    }

    /**
     * 设置iOtherNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIOtherNote(String value) {
        this.iOtherNote = value;
    }

    /**
     * 获取iInsurPrice属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIInsurPrice() {
        return iInsurPrice;
    }

    /**
     * 设置iInsurPrice属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIInsurPrice(String value) {
        this.iInsurPrice = value;
    }

    /**
     * 获取iInsurAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIInsurAmount() {
        return iInsurAmount;
    }

    /**
     * 设置iInsurAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIInsurAmount(String value) {
        this.iInsurAmount = value;
    }

    /**
     * 获取iInsurNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIInsurNote() {
        return iInsurNote;
    }

    /**
     * 设置iInsurNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIInsurNote(String value) {
        this.iInsurNote = value;
    }

    /**
     * 获取eIsDutyDel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEIsDutyDel() {
        return eIsDutyDel;
    }

    /**
     * 设置eIsDutyDel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEIsDutyDel(String value) {
        this.eIsDutyDel = value;
    }

    /**
     * 获取gNameOther属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGNameOther() {
        return gNameOther;
    }

    /**
     * 设置gNameOther属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGNameOther(String value) {
        this.gNameOther = value;
    }

    /**
     * 获取codeTsOther属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeTsOther() {
        return codeTsOther;
    }

    /**
     * 设置codeTsOther属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeTsOther(String value) {
        this.codeTsOther = value;
    }

    /**
     * 获取isClassDecision属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsClassDecision() {
        return isClassDecision;
    }

    /**
     * 设置isClassDecision属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsClassDecision(String value) {
        this.isClassDecision = value;
    }

    /**
     * 获取decisionNO属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecisionNO() {
        return decisionNO;
    }

    /**
     * 设置decisionNO属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecisionNO(String value) {
        this.decisionNO = value;
    }

    /**
     * 获取codeTsDecision属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeTsDecision() {
        return codeTsDecision;
    }

    /**
     * 设置codeTsDecision属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeTsDecision(String value) {
        this.codeTsDecision = value;
    }

    /**
     * 获取decisionCus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecisionCus() {
        return decisionCus;
    }

    /**
     * 设置decisionCus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecisionCus(String value) {
        this.decisionCus = value;
    }

    /**
     * 获取isSampleTest属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSampleTest() {
        return isSampleTest;
    }

    /**
     * 设置isSampleTest属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSampleTest(String value) {
        this.isSampleTest = value;
    }

    /**
     * 获取gOptions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGOptions() {
        return gOptions;
    }

    /**
     * 设置gOptions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGOptions(String value) {
        this.gOptions = value;
    }

    /**
     * 获取trafMode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrafMode() {
        return trafMode;
    }

    /**
     * 设置trafMode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrafMode(String value) {
        this.trafMode = value;
    }

    /**
     * 获取isDirectTraf属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDirectTraf() {
        return isDirectTraf;
    }

    /**
     * 设置isDirectTraf属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDirectTraf(String value) {
        this.isDirectTraf = value;
    }

    /**
     * 获取transitCountry属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransitCountry() {
        return transitCountry;
    }

    /**
     * 设置transitCountry属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransitCountry(String value) {
        this.transitCountry = value;
    }

    /**
     * 获取destPort属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestPort() {
        return destPort;
    }

    /**
     * 设置destPort属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestPort(String value) {
        this.destPort = value;
    }

    /**
     * 获取declPort属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclPort() {
        return declPort;
    }

    /**
     * 设置declPort属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclPort(String value) {
        this.declPort = value;
    }

    /**
     * 获取billNo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * 设置billNo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillNo(String value) {
        this.billNo = value;
    }

    /**
     * 获取originCountry属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCountry() {
        return originCountry;
    }

    /**
     * 设置originCountry属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCountry(String value) {
        this.originCountry = value;
    }

    /**
     * 获取originMark属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginMark() {
        return originMark;
    }

    /**
     * 设置originMark属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginMark(String value) {
        this.originMark = value;
    }

    /**
     * 获取certCountry属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertCountry() {
        return certCountry;
    }

    /**
     * 设置certCountry属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertCountry(String value) {
        this.certCountry = value;
    }

    /**
     * 获取certNO属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertNO() {
        return certNO;
    }

    /**
     * 设置certNO属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertNO(String value) {
        this.certNO = value;
    }

    /**
     * 获取certStandard属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertStandard() {
        return certStandard;
    }

    /**
     * 设置certStandard属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertStandard(String value) {
        this.certStandard = value;
    }

    /**
     * 获取otherNote属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherNote() {
        return otherNote;
    }

    /**
     * 设置otherNote属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherNote(String value) {
        this.otherNote = value;
    }

    /**
     * 获取isSecret属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSecret() {
        return isSecret;
    }

    /**
     * 设置isSecret属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSecret(String value) {
        this.isSecret = value;
    }

    /**
     * 获取agentType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentType() {
        return agentType;
    }

    /**
     * 设置agentType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentType(String value) {
        this.agentType = value;
    }

    /**
     * 获取declAddr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclAddr() {
        return declAddr;
    }

    /**
     * 设置declAddr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclAddr(String value) {
        this.declAddr = value;
    }

    /**
     * 获取declPost属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclPost() {
        return declPost;
    }

    /**
     * 设置declPost属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclPost(String value) {
        this.declPost = value;
    }

    /**
     * 获取declTel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclTel() {
        return declTel;
    }

    /**
     * 设置declTel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclTel(String value) {
        this.declTel = value;
    }

}
