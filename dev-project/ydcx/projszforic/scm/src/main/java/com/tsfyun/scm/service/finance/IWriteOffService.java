package com.tsfyun.scm.service.finance;


import com.tsfyun.common.base.dto.CostWriteOffDTO;
import com.tsfyun.common.base.dto.WriteOffOrderCostDTO;
import com.tsfyun.common.base.dto.WriteOffOrderCostPlusDTO;
import com.tsfyun.scm.entity.finance.OverseasReceivingAccount;
import com.tsfyun.scm.entity.finance.ReceiptAccount;
import com.tsfyun.scm.entity.finance.RefundAccount;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.scm.entity.order.ExpOrderCost;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderCost;

import java.util.List;

public interface IWriteOffService {

    //自动核销
    void autoWriteOff(CostWriteOffDTO dto);
    //根据订单核销(进口)
    void writeOffOrderId(Long orderId);
    //根据订单核销(进口)
    void writeOffOrderId(ImpOrder impOrder);
    //根据订单核销(出口)
    void writeOffExpOrderId(Long orderId);
    //根据订单核销(出口)
    void writeOffExpOrderId(ExpOrder expOrder);
    //根据退款申请核销
    void writeOffRefundAccount(RefundAccount refundAccount);
    //取消订单费用核销(进口)
    void cancelWriteOffByImpOrderCost(ImpOrderCost impOrderCost);
    //取消订单费用核销(出口)
    void cancelWriteOffByExpOrderCost(ExpOrderCost expOrderCost);
    //根据收款单取消核销
    void cancelwriteOffByReceiptAccount(ReceiptAccount receiptAccount);
    //根据退款单取消核销
    void cancelwriteOffByRefundAccount(RefundAccount refundAccount);

    //#代理报关订单 核销订单指定费用
    List<WriteOffOrderCostDTO> writeOffTrustOrderCosts(WriteOffOrderCostPlusDTO dto);
    //#代理报关订单 取消订单费用核销
    void cancelWriteOffByTrustOrderCost(WriteOffOrderCostDTO dto);
}
