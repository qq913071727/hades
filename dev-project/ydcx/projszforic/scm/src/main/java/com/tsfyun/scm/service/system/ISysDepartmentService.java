package com.tsfyun.scm.service.system;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.system.DepartmentDTO;
import com.tsfyun.scm.entity.system.SysDepartment;
import com.tsfyun.scm.util.UIDepartment;
import com.tsfyun.scm.vo.system.DepartmentVO;

import java.util.List;

public interface ISysDepartmentService extends IService<SysDepartment> {


    /**
     * 获取所有的部门树
     * @return
     */
    List<UIDepartment> getAllDepartmentTree(boolean isTree);

    /**
     * 新增部门
     * @param dto
     */
    void add(DepartmentDTO dto);

    /**
     * 修改部门
     * @param dto
     */
    void edit(DepartmentDTO dto);

    /**
     * 删除部门
     * @param id
     */
    void delete(Long id);

    /**
     * 部门详情
     * @param id
     * @return
     */
    DepartmentVO detail(Long id);

    List<Long> getSubDepartments(Long id);

    /**
     * 根据名称获取一个部门信息
     * @param name
     * @return
     */
    SysDepartment findOneByName(String name);
}
