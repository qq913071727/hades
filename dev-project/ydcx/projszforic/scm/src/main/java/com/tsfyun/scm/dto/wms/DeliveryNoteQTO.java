package com.tsfyun.scm.dto.wms;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @since Created in 2020/4/24 14:55
 */
@Data
public class DeliveryNoteQTO extends PaginationDto implements Serializable {

    private String docNo;

    private String statusId;

    private String customerName;

    private Long warehouseId;

    private String documentMaker;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime deliveryDateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime deliveryDateEnd;

    public void setDeliveryDateEnd(LocalDateTime accountDateEnd) {
        if(accountDateEnd != null){
            this.deliveryDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(accountDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
