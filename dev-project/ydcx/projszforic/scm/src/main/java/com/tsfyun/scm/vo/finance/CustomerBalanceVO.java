package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/30 18:15
 */
@Data
public class CustomerBalanceVO implements Serializable {

    /**
     * 余额
     */
    private BigDecimal balance;

    /**
     * 收入
     */
    private BigDecimal incomeAmount;

    /**
     * 支出
     */
    private BigDecimal outcomeAmount;

}
