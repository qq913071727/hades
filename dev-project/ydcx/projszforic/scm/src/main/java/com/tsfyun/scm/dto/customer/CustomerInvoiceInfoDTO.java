package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 客户开票信息
 * @CreateDate: Created in 2020/9/24 16:18
 */
@Data
public class CustomerInvoiceInfoDTO implements Serializable {

    /**
     * 纳税人识别号
     */
    @ApiModelProperty(value = "纳税人识别号")
    @NotEmptyTrim(message = "纳税人识别号不能为空")
    @LengthTrim(min = 18,max = 18,message = "纳税人识别号必须为18位")
    private String taxpayerNo;

    /**
     * 开户银行
     */
    @ApiModelProperty(value = "开户银行")
    @NotEmptyTrim(message = "请输入开户银行")
    @LengthTrim(min = 2,max = 50,message = "开户银行只能为2-50位")
    private String invoiceBankName;

    /**
     * 银行帐号
     */
    @ApiModelProperty(value = "银行账号")
    @NotEmptyTrim(message = "请输入银行账号")
    @LengthTrim(min = 2,max = 50,message = "银行帐号只能为2-50位")
    private String invoiceBankAccount;

    /**
     * 开票电话
     */
    @ApiModelProperty(value = "开票电话")
    @NotEmptyTrim(message = "请输入开票电话")
    @LengthTrim(max = 50,message = "开票电话最大长度只能为50位")
    private String invoiceBankTel;

    /**
     * 开票地址
     */
    @ApiModelProperty(value = "开票地址")
    @NotEmptyTrim(message = "请输入开票地址")
    @LengthTrim(min = 2,max = 255,message = "开票地址只能为2-255位")
    private String invoiceBankAddress;


    /**
     * 联系人
     */
    @ApiModelProperty(value = "收票联系人")
    @NotEmptyTrim(message = "请输入收票联系人")
    @LengthTrim(min = 2,max = 50,message = "收票联系人只能为2-50位")
    private String linkPerson;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "收票联系电话")
    @NotEmptyTrim(message = "请输入收票联系电话")
    @LengthTrim(min = 3,max = 50,message = "收票联系电话能为3-50位")
    private String linkTel;

    @ApiModelProperty(value = "收票区域/省")
    @NotEmptyTrim(message = "收票区域/省不能为空")
    private String invoiceProvince;

    @ApiModelProperty(value = "收票区域/市")
    @NotEmptyTrim(message = "收票区域/市不能为空")
    private String invoiceCity;

    @ApiModelProperty(value = "收票区域/区")
    @NotEmptyTrim(message = "收票区域/区不能为空")
    private String invoiceArea;
    /**
     * 收票详细地址
     */
    @ApiModelProperty(value = "收票详细地址")
    @NotEmptyTrim(message = "请输入收票详细地址")
    @LengthTrim(min = 2,max = 225,message = "收票详细地址只能为2-225位")
    private String linkAddress;


    /**=
     * 开票要求
     */
    @ApiModelProperty(value = "开票要求")
    @LengthTrim(max = 255,message = "开票要求最大长度只能为255位")
    private String invoiceMemo;


}
