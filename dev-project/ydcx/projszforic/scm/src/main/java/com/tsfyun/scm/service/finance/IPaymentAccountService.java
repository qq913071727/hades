package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.enums.domain.PaymentAccountStatusEnum;
import com.tsfyun.scm.dto.finance.*;
import com.tsfyun.scm.entity.finance.PaymentAccount;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.EditPaymentPlusVO;
import com.tsfyun.scm.vo.finance.PaymentAccountPlusDetailVO;
import com.tsfyun.scm.vo.finance.PaymentAccountVO;
import com.tsfyun.scm.vo.finance.client.ClientPaymentApplyDTO;
import com.tsfyun.scm.vo.finance.client.ClientPaymentDetailPlusVO;
import com.tsfyun.scm.vo.finance.client.ClientPaymentDetailVO;
import com.tsfyun.scm.vo.finance.client.ClientPaymentListVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 付款单 服务类
 * </p>
 *

 * @since 2020-04-23
 */
public interface IPaymentAccountService extends IService<PaymentAccount> {

    /**
     * 分页
     * @param qto
     * @return
     */
    PageInfo<PaymentAccountVO> pageList(PaymentAccountQTO qto);


    /**
     * 付款单详情
     * @param id 付款单主单id
     * @param operation 操作码，如果传空（则不会校验是否允许操作）
     * @return
     */
    PaymentAccountPlusDetailVO detail(Long id,String operation);

    /**=
     * 初始化订单修改数据
     * @param id
     * @return
     */
    EditPaymentPlusVO initEdit(Long id);

    /**
     * 修改状态并记录单据状态变更
     * @param id
     * @param paymentAccount
     * @param operationEnum
     * @param changeStatusEnum
     */
    PaymentAccount changeStatus(Long id, PaymentAccount paymentAccount, DomainOprationEnum operationEnum, PaymentAccountStatusEnum changeStatusEnum, String memo);

    /**
     * 修改付汇单数据（updatePaymentAccount实体包含待修改的字段，并包含id字段）
     * @param updatePaymentAccount
     */
    void updateForRisk(PaymentAccount updatePaymentAccount);

    /**=
     * 付汇申请
     * @param dto
     */
    void apply(PaymentApplyDTO dto);

    /**
     * 付汇修改
     * @param dto
     */
    void edit(PaymentApplyDTO dto);
    void clientEdit(PaymentApplyDTO dto);

    /**=
     * 流程跳转
     * @param dto
     */
    void processJump(TaskDTO dto);

    /**=
     * 汇率调整转风控审批
     * @param dto
     */
    void adjustRate(AdjustRateDTO dto);

    /**
     * 手续费调整
     * @param dto
     */
    void adjustBankFee(AdjustBankFeeDTO dto);

    /**=
     * 汇率调整风控审核
     */
    void completeAdjustRateRisk(Long orderId,Boolean isPass,String memo);

    /**=
     * 金额异常风控审核
     */
    void completeCostRisk(Long paymentId,Boolean isPass,String memo);

    /**=
     * 确定付款行
     * @param dto
     */
    void saveConfirmBank(ConfirmBankDTO dto);

    /**=
     * 确定付汇完成
     * @param dto
     */
    void paymentCompleted(PaymentCompletedDTO dto);

    /**
     * 根据订单和状态获取对应的付汇金额
     * @param orderId
     * @param statusId
     * @return
     */
    Map<String,Object> getPaymentAmount(Long orderId,String statusId);

    /**
     * 客户端付汇申请列表
     * @param qto
     * @return
     */
    PageInfo<ClientPaymentListVO> clientPageList(ClientPaymentApplyQTO qto);

    /**
     * 状态集合对应的数量
     * @return
     */
    Map<String,Long> getClientPaymentNum( );

    /**
     * 客户端获取付汇详情
     * @param id
     * @return
     */
    ClientPaymentDetailPlusVO clientPaymentInfo(Long id);

    /**
     * 客户端付汇申请
     * @param dto
     */
    void clientApply(PaymentApplyDTO dto);

    /**
     * 计算付汇手续费
     * @param orderId
     * @param paymentTerm
     * @param counterFeeType
     * @param swiftCode
     * @return
     */
    BigDecimal calculationFee(Long orderId,String paymentTerm,String counterFeeType,String swiftCode);

    /**
     * 导出付汇委托书
     * @param id
     * @return
     */
    Long createEntrustExcel(Long id);

    /**
     * 删除付款单
     * @param id
     */
    void removeAllById(Long id);

    /**
     * 根据进口订单获取付汇汇率
     * @param impOrderNo
     * @return
     */
    List<BigDecimal> getExchangeRateByImpOrderNo(@Param(value = "impOrderNo")String impOrderNo);
}
