package com.tsfyun.scm.config.redis;

import com.tsfyun.common.base.constant.LoginConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @param listenerContainer must not be {@literal null}.
     */
    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        //过期key处理
        String expiredKey = message.toString();
        Object expireKeyValue = stringRedisTemplate.opsForValue().get(expiredKey);
        if(expiredKey.startsWith(LoginConstant.TOKEN)){ //登录过期
            log.warn("key【{}】,value【{}】登录token自动过期",expiredKey,expireKeyValue);
            String[] keyArray = expiredKey.split(":");

        }
    }
}