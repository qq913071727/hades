package com.tsfyun.scm.service.wms;

import com.tsfyun.scm.dto.wms.InspectionMemberDTO;
import com.tsfyun.scm.dto.wms.ReceivingMemberSaveDTO;
import com.tsfyun.scm.entity.wms.ReceivingNote;
import com.tsfyun.scm.entity.wms.ReceivingNoteMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.wms.InspectionNoteMemberVO;
import com.tsfyun.scm.vo.wms.ReceivingNoteMemberVO;

import java.util.List;

/**
 * <p>
 * 入库单明细 服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IReceivingNoteMemberService extends IService<ReceivingNoteMember> {

    /**
     * 根据入库单主单获取
     * @param receivingNoteId
     * @return
     */
    List<ReceivingNoteMemberVO> findByReceivingNoteId(Long receivingNoteId);

    /**=
     * 入库单验货明细
     * @param id
     * @return
     */
    List<InspectionNoteMemberVO> inspectionMembers(Long id);

    /**=
     * 保存验货明细赋值和校验
     * @param receivingNote
     * @param members
     * @return
     */
    ReceivingMemberSaveDTO checkWrapReceivingMember(ReceivingNote receivingNote,List<InspectionMemberDTO> members);
}
