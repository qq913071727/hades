package com.tsfyun.scm.service.order;

import com.tsfyun.scm.dto.order.ImpOrderMemberSaveDTO;
import com.tsfyun.scm.dto.order.UnboundDetailQTO;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.entity.order.ImpOrderMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.scm.vo.order.ImpOrderMemberVO;
import com.tsfyun.scm.vo.order.OrderReceivingBindVO;
import com.tsfyun.scm.vo.order.UnboundDetailVO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单明细 服务类
 * </p>
 *

 * @since 2020-04-08
 */
public interface IImpOrderMemberService extends IService<ImpOrderMember> {

    /**
     * 根据订单id获取订单明细数据
     * @param impOrderId
     * @return
     */
    List<ImpOrderMember> getByOrderId(Long impOrderId);

    /**=
     * 订单物料明细
     * @param impOrderId
     * @return
     */
    List<ImpOrderMemberVO> findOrderMaterielMember(Long impOrderId);

    /**=
     * 保存，修改，删除订单明细
     * @param impOrder
     * @param impOrderMemberSaveDTO
     */
    Integer saveMembers(ImpOrder impOrder, ImpOrderMemberSaveDTO impOrderMemberSaveDTO, Boolean isSubmit);

    /**=
     * 验货保存修改明细
     */
    Integer saveInspectionMembers(ImpOrder impOrder,ImpOrderMemberSaveDTO impOrderMemberSaveDTO);

    /**=
     * 根据时间和型号查询历史进口记录
     * @param dateTime
     * @param model
     * @return
     */
    List<PriceFluctuationHistory> findImpHistory(LocalDateTime dateTime,String model);

    /**=
     * 批量更新明细物料数据(品名，品牌)
     * @param impOrderMemberList
     */
    void batchUpdateMateriel(List<ImpOrderMember> impOrderMemberList);

    /**=
     * 根据物料ID修改物料ID
     * @param oldId
     * @param newId
     */
    void updateMemberMaterielId(String oldId,String newId);

    /**=
     * 更新赋值数据
     * @param impOrderMemberList
     */
    void batchUpdatePartData(List<ImpOrderMember> impOrderMemberList);

    /**
     * 批量修改订单验货明细
     * @param impOrderMemberList
     */
    void batchUpdateSplitMembers(List<ImpOrderMember> impOrderMemberList);

    /**=
     * 查询未绑定完成的订单数据
     * @param qto
     * @return
     */
    List<UnboundDetailVO> unboundDetailList(UnboundDetailQTO qto);

    /**
     * 根据订单id删除明细信息
     * @param orderId
     */
    void removeByImpOrderId(Long orderId);
}
