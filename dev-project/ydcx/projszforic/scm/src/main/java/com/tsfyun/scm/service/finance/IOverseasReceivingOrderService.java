package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.entity.finance.OverseasReceivingAccount;
import com.tsfyun.scm.entity.finance.OverseasReceivingOrder;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.OverseasReceivingOrderVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 境外收款认领订单 服务类
 * </p>
 *
 *
 * @since 2021-10-11
 */
public interface IOverseasReceivingOrderService extends IService<OverseasReceivingOrder> {

    /**
     * 根据收款单查询
     * @param accountId
     * @return
     */
    List<OverseasReceivingOrderVO> findByAccountId(Long accountId);

    List<OverseasReceivingOrder> findOverseasReceivingOrderByAccountId(Long accountId);

    /**
     * 根据收款单ID和订单ID查询
     * @param accountId
     * @param orderId
     * @return
     */
    OverseasReceivingOrder findByAccountIdAndExpOrderId(Long accountId,Long orderId);

    /**
     * 查询所有认领的订单编号
     * @param accountId
     * @return
     */
    List<String> findOrderNo(Long accountId);

    /**
     * 清空认领数据
     * @param account
     */
    void clearClaimOrder(OverseasReceivingAccount account);

    /**
     * 根据收款单编号获取订单合并信息
     * @param accountNos
     * @return
     */
    List<OverseasReceivingOrderVO> overseasReceivings(List<String> accountNos);

    /**
     * 结汇时写入结汇人民币金额
     * @param id
     * @param settlementValue
     */
    void writeSettlementValue(Long id, BigDecimal settlementValue);
}
