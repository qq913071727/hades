package com.tsfyun.scm.service.impl.order;


import com.tsfyun.common.base.enums.BaseDataTypeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.entity.customer.CustomerAbroadDeliveryInfo;
import com.tsfyun.scm.entity.customer.CustomerDeliveryInfo;
import com.tsfyun.scm.entity.order.ExpOrderLogistics;
import com.tsfyun.scm.enums.DeliveryDestinationEnum;
import com.tsfyun.scm.enums.DeliveryMethodEnum;
import com.tsfyun.scm.service.base.ISystemCacheService;
import com.tsfyun.scm.service.customer.ICustomerAbroadDeliveryInfoService;
import com.tsfyun.scm.service.customer.ICustomerDeliveryInfoService;
import com.tsfyun.scm.service.order.IExpOrderCheckWrapService;
import com.tsfyun.scm.system.vo.BaseDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Objects;
import java.util.Optional;


@Service
public class ExpOrderCheckWrapServiceImpl implements IExpOrderCheckWrapService {

    @Autowired
    private ISystemCacheService systemCacheService;
    @Autowired
    private ICustomerDeliveryInfoService customerDeliveryInfoService;
    @Autowired
    private ICustomerAbroadDeliveryInfoService customerAbroadDeliveryInfoService;

    @Override
    public void checkWrapDomesticDeliveryLogistics(ExpOrderLogistics expOrderLogistics, ExpOrderLogisticsDTO logistiscs, Boolean isSubmit) {
        expOrderLogistics.setDeliveryModeMemo(logistiscs.getDeliveryModeMemo());//交货备注
        BaseDataVO deliveryModeBaseData = systemCacheService.getBaseDataByTpeAndCode(BaseDataTypeEnum.DeliveryMode.getCode(),logistiscs.getDeliveryMode());
        Optional.ofNullable(deliveryModeBaseData).orElseThrow(()->new ServiceException("国内交货方式错误"));
        expOrderLogistics.setDeliveryMode(deliveryModeBaseData.getCode());//交货方式编码
        expOrderLogistics.setDeliveryModeName(deliveryModeBaseData.getName());//交货方式名称
        expOrderLogistics.setDeliveryExpressNo("");//快递单号
        expOrderLogistics.setDeliveryMethod(null);//提货方式
        expOrderLogistics.setTakeTime(null);//要求提货时间
        expOrderLogistics.setCustomerDeliveryInfoId(null);//国内提货信息
        expOrderLogistics.setDeliveryCompanyName("");//提货公司
        expOrderLogistics.setDeliveryLinkPerson("");//提货联系人
        expOrderLogistics.setDeliveryLinkTel("");//提货联系人电话
        expOrderLogistics.setDeliveryAddress("");//提货地址
        switch (expOrderLogistics.getDeliveryMode()) {
            case "ExpressDelivery"://快递
                expOrderLogistics.setDeliveryExpressNo(logistiscs.getDeliveryExpressNo());//快递单号
                break;
            case "TakeDelivery"://提货
                if(isSubmit) {
                    TsfPreconditions.checkArgument(StringUtils.isNotEmpty(logistiscs.getDeliveryMethod()),new ServiceException("请选择提货方式"));
                    TsfPreconditions.checkArgument(Objects.nonNull(logistiscs.getTakeTime()),new ServiceException("请选择提货时间"));
                    TsfPreconditions.checkArgument(Objects.nonNull(logistiscs.getCustomerDeliveryInfoId()),new ServiceException("请选择提货信息"));
                }
                DeliveryMethodEnum deliveryMethodEnum = DeliveryMethodEnum.of(logistiscs.getDeliveryMethod());
                if(Objects.nonNull(deliveryMethodEnum)){
                    expOrderLogistics.setDeliveryMethod(deliveryMethodEnum.getCode());//提货方式
                }
                expOrderLogistics.setTakeTime(logistiscs.getTakeTime());//要求提货时间
                //客户收货地址信息
                if(Objects.nonNull(logistiscs.getCustomerDeliveryInfoId())) {
                    CustomerDeliveryInfo customerDeliveryInfo = customerDeliveryInfoService.getById(logistiscs.getCustomerDeliveryInfoId());
                    Optional.ofNullable(customerDeliveryInfo).orElseThrow(()->new ServiceException("国内交货-提货信息错误请刷新"));
                    expOrderLogistics.setCustomerDeliveryInfoId(customerDeliveryInfo.getId());
                    expOrderLogistics.setDeliveryCompanyName(customerDeliveryInfo.getCompanyName());
                    expOrderLogistics.setDeliveryLinkPerson(customerDeliveryInfo.getLinkPerson());
                    expOrderLogistics.setDeliveryLinkTel(customerDeliveryInfo.getLinkTel());
                    expOrderLogistics.setDeliveryAddress(customerDeliveryInfo.getProvinceName()+customerDeliveryInfo.getCityName()+customerDeliveryInfo.getAreaName()+customerDeliveryInfo.getAddress());
                }
                break;
        }
    }

    @Override
    public void checkWrapOverseasDeliveryLogistics(ExpOrderLogistics expOrderLogistics, ExpOrderLogisticsDTO logistiscs, Boolean isSubmit) {
        expOrderLogistics.setReceivingModeMemo(logistiscs.getReceivingModeMemo());
        // 是否包车
        expOrderLogistics.setIsCharterCar(logistiscs.getIsCharterCar()?Boolean.TRUE:Boolean.FALSE);
        // 送货目的地
        expOrderLogistics.setDeliveryDestination(DeliveryDestinationEnum.HONG_KONG.getCode());
        expOrderLogistics.setSupplierTakeInfoId(null);//境外送货信息ID
        expOrderLogistics.setTakeLinkCompany("");//收货公司
        expOrderLogistics.setTakeLinkPerson("");//收货联系人
        expOrderLogistics.setTakeLinkTel("");//收货联系人电话
        expOrderLogistics.setTakeAddress("");//收货地址
        if(Objects.equals(expOrderLogistics.getIsCharterCar(),Boolean.TRUE)){
            DeliveryDestinationEnum deliveryDestinationEnum = DeliveryDestinationEnum.of(logistiscs.getDeliveryDestination());
            if(Objects.nonNull(deliveryDestinationEnum)){
                expOrderLogistics.setDeliveryDestination(deliveryDestinationEnum.getCode());
            }
            if(isSubmit){
                TsfPreconditions.checkArgument(Objects.nonNull(deliveryDestinationEnum),new ServiceException("请选择送货目的地"));
                if(Objects.equals(deliveryDestinationEnum,DeliveryDestinationEnum.DESTINATION)){
                    TsfPreconditions.checkArgument(Objects.nonNull(logistiscs.getSupplierTakeInfoId()),new ServiceException("请选择送货信息"));
                }
            }

            //送货信息验证
            if(Objects.nonNull(logistiscs.getSupplierTakeInfoId())) {
                CustomerAbroadDeliveryInfo customerAbroadDeliveryInfo = customerAbroadDeliveryInfoService.getById(logistiscs.getSupplierTakeInfoId());
                Optional.ofNullable(customerAbroadDeliveryInfo).orElseThrow(()->new ServiceException("境外交货-送货信息错误请刷新"));

                //送货信息
                expOrderLogistics.setSupplierTakeInfoId(customerAbroadDeliveryInfo.getId());
                expOrderLogistics.setTakeLinkCompany(customerAbroadDeliveryInfo.getCompanyName());
                expOrderLogistics.setTakeLinkPerson(customerAbroadDeliveryInfo.getLinkPerson());
                expOrderLogistics.setTakeLinkTel(customerAbroadDeliveryInfo.getLinkTel());
                expOrderLogistics.setTakeAddress(customerAbroadDeliveryInfo.getProvinceName()+customerAbroadDeliveryInfo.getCityName()+customerAbroadDeliveryInfo.getAreaName()+customerAbroadDeliveryInfo.getAddress());
            }
        }
    }

}
