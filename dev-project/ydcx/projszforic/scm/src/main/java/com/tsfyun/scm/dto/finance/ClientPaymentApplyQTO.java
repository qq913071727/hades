package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @Description: 客户端付汇申请查询实体
 * @CreateDate: Created in 2020/11/3 10:30
 */
@Data
public class ClientPaymentApplyQTO extends PaginationDto {

    /**
     * 状态
     */
    private String statusId;

    /**
     * 币制
     */
    private String currencyId;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 开始申请日期
     */
    private LocalDateTime accountDateStart;

    /**
     * 结束申请日期
     */
    private LocalDateTime accountDateEnd;

    /**
     * 客户端状态
     */
    private String clientStatusId;

    /**
     * 状态集合
     */
    private List<String> statusIds;

    public void setDateEnd(LocalDateTime accountDateEnd) {
        if(accountDateEnd != null){
            this.accountDateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(accountDateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

}
