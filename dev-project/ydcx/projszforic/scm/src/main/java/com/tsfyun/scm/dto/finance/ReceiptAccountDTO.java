package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.finance.ReceivingModeEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 收款单请求实体
 * </p>
 *
 * @since 2020-04-15
 */
@Data
@ApiModel(value="ReceiptAccount请求对象", description="收款单请求实体")
public class ReceiptAccountDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "数据id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotNull(message = "收款日期不能为空")
   @ApiModelProperty(value = "收款日期")
   private LocalDateTime accountDate;

   @NotEmptyTrim(message = "收款行不能为空")
   @LengthTrim(max = 64,message = "收款行最大长度不能超过64位")
   @ApiModelProperty(value = "收款行")
   private String receivingBank;

   @NotNull(message = "收款金额不能为空")
   @Digits(integer = 17, fraction = 2, message = "收款金额整数位不能超过17位，小数位不能超过2位")
   @DecimalMin(value = "0.01",message = "收款金额不能小于0")
   @ApiModelProperty(value = "收款金额")
   private BigDecimal accountValue;

   @NotEmptyTrim(message = "客户不能为空")
   @ApiModelProperty(value = "客户不能为空")
   private String customerName;

   @NotEmptyTrim(message = "付款人不能为空")
   @LengthTrim(max = 100,message = "付款人最大长度不能超过100位")
   @ApiModelProperty(value = "付款人")
   private String payer;

   @NotEmptyTrim(message = "收款方式不能为空")
   @LengthTrim(max = 10,message = "收款方式最大长度不能超过10位")
   @EnumCheck(clazz = ReceivingModeEnum.class,message = "收款方式错误")
   @ApiModelProperty(value = "收款方式")
   private String receivingMode;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
