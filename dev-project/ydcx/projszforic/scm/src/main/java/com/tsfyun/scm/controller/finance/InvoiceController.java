package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.dto.finance.ConfirmTaxCodeDTO;
import com.tsfyun.scm.dto.finance.InvoiceCompleteDTO;
import com.tsfyun.scm.dto.finance.InvoiceQTO;
import com.tsfyun.scm.service.finance.IInvoiceService;
import com.tsfyun.scm.vo.finance.InvoiceDetailPlusVO;
import com.tsfyun.scm.vo.finance.InvoiceVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 发票 前端控制器
 * </p>
 *
 * @since 2020-05-15
 */
@RestController
@RequestMapping("/invoice")
public class InvoiceController extends BaseController {

    private final IInvoiceService invoiceService;

    public InvoiceController(IInvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    /**
     * 分页查询（带客户权限判断）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<InvoiceVO>> pageList(@ModelAttribute InvoiceQTO qto) {
        PageInfo<InvoiceVO> page = invoiceService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 导出开票资料
     * @param ids
     * @return
     */
    @PostMapping(value = "exportSpaceFlightExcel")
    public Result<Long> exportSpaceFlightExcel(@RequestParam(value = "ids")String ids) throws Exception {
        return success(invoiceService.exportSpaceFlightExcel(StringUtils.stringToLongs(ids)));
    }


    /**
     * 详情信息（含明细）
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<InvoiceDetailPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(invoiceService.detailPlus(id,operation));
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "delete")
    public Result<Void> delete(@RequestParam(value = "id")Long id) {
        invoiceService.delete(id);
        return success();
    }

    /**
     * 开票完成
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "completeInvoice")
    public Result<Void> completeInvoice(@ModelAttribute @Validated InvoiceCompleteDTO dto) {
        invoiceService.completeInvoice(dto);
        return success();
    }

    /**=
     * 根据订单做发票申请
     * @param id
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "invoiceApply")
    public Result<Void> invoiceApply(@RequestParam(value = "id")Long id){
        invoiceService.invoiceApply(id);
        return success();
    }

    /**=
     * 财务确定税收编码
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "financialTaxCode")
    public Result<Void> financialTaxCode(@RequestBody @Valid ConfirmTaxCodeDTO dto){
        invoiceService.financialTaxCode(dto);
        return success();
    }

    /**
     * 商务开票确认
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "businessConfirm")
    public Result<Void> businessConfirm(@ModelAttribute @Valid TaskDTO dto){
        invoiceService.businessConfirm(dto);
        return success();
    }

    /**
     * 根据订单id获取发票申请id
     * @param id
     * @return
     */
    @PostMapping(value = "getInvoiceIdByOrderId")
    public Result<Long> getInvoiceIdByOrderId(@RequestParam(value = "id")Long id){
        return success(invoiceService.getInvoiceByOrderId(id).getInvId());
    }

}

