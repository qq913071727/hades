package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.SettlementAccountMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.OverseasReceivingAccountVO;
import com.tsfyun.scm.vo.finance.SettlementAccountMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 结汇明细 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Repository
public interface SettlementAccountMemberMapper extends Mapper<SettlementAccountMember> {

    List<OverseasReceivingAccountVO> findOverseasReceivingAccount(@Param(value = "settlementAccountId") Long settlementAccountId);

    List<SettlementAccountMember> findByAccountId(@Param(value = "settlementAccountId")Long settlementAccountId);
}
