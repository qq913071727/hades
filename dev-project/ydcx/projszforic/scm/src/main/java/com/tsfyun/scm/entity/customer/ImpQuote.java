package com.tsfyun.scm.entity.customer;

import java.io.Serializable;
import java.math.BigDecimal;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 进口报价
 * </p>
 *
 *
 * @since 2020-04-01
 */
@Data
public class ImpQuote implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     @KeySql(genId = IdWorker.class)
     private Long id;

    /**
     * 协议ID
     */

    private Long agreementId;

    /**
     * 报价类型 
     */

    private String quoteType;

    //代杂费账期
    private String agreeMode;
    private Integer day;
    private Integer month;
    private Integer monthDay;
    private Integer firstMonth;
    private Integer firstMonthDay;
    private Integer lowerMonth;
    private Integer lowerMonthDay;
    private Integer week;
    private Integer weekDay;

    //税款账期
    private String taxAgreeMode;
    private Integer taxDay;
    private Integer taxMonth;
    private Integer taxMonthDay;
    private Integer taxFirstMonth;
    private Integer taxFirstMonthDay;
    private Integer taxLowerMonth;
    private Integer taxLowerMonthDay;
    private Integer taxWeek;
    private Integer taxWeekDay;

    //货款账期
    private String goAgreeMode;
    private Integer goDay;
    private Integer goMonth;
    private Integer goMonthDay;
    private Integer goFirstMonth;
    private Integer goFirstMonthDay;
    private Integer goLowerMonth;
    private Integer goLowerMonthDay;
    private Integer goWeek;
    private Integer goWeekDay;

    /**
     * 税前,税后
     */

    private String taxType;

    /**
     * 是否加税点
     */

    private Boolean taxIncluded;

    /**
     * 按单收费
     */

    private BigDecimal basePrice;

    /**
     * 收费比例
     */

    private BigDecimal serviceRate;

    /**
     * 最低收费
     */

    private BigDecimal minCost;

    /**
     * 宽限天数
     */

    private Integer graceDay;

    /**
     * 逾期利率
     */

    private BigDecimal overdueRate;

    /**
     * 代理费计算模式
     */

    private String agencyFeeMode;

    /**
     * 客户快递模式计费代理费报价id（代理费）
     */
    private Long customerExpressQuoteId;


}
