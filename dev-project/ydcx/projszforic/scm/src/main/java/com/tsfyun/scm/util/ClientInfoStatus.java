package com.tsfyun.scm.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/11 09:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientInfoStatus implements Serializable {

    /**
     * 前端状态id
     */
    private String clientStatusId;

    /**
     * 前端状态描述
     */
    private String clientStatusDesc;

    /**
     * 后端状态集合
     */
    private List<String> backStatusId;

}
