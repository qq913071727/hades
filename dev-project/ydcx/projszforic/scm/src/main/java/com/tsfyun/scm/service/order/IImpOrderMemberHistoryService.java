package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.ImpOrderMemberHistory;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 * 原始订单明细 服务类
 * </p>
 *
 *
 * @since 2020-04-16
 */
public interface IImpOrderMemberHistoryService extends IService<ImpOrderMemberHistory> {

    //根据订单ID删除明细
    void removeByOrderId(Long orderId);

    List<ImpOrderMemberHistory> getByOrderId(Long orderId);

    // 拆单批量修改
    void batchUpdateSplitMembers(List<ImpOrderMemberHistory> impOrderMemberHistoryList);
}
