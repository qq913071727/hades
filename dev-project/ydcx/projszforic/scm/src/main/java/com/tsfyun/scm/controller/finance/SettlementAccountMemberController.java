package com.tsfyun.scm.controller.finance;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 结汇明细 前端控制器
 * </p>
 *
 *
 * @since 2021-10-14
 */
@RestController
@RequestMapping("/settlementAccountMember")
public class SettlementAccountMemberController extends BaseController {

}

