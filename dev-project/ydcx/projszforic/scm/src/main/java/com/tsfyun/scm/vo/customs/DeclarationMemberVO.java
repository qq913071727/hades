package com.tsfyun.scm.vo.customs;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeclarationMember响应对象", description="响应实体")
public class DeclarationMemberVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    private Long declarationId;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "规格参数")
    private String declareSpec;

    @ApiModelProperty(value = "产地")
    private String country;

    @ApiModelProperty(value = "产地")
    private String countryName;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;
    @ApiModelProperty(value = "币制")
    private String currencyId;
    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;

    private Integer rowNo;

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    private String ciqNo;

    @ApiModelProperty(value = "单位1")
    private String unit1Code;

    @ApiModelProperty(value = "单位1")
    private String unit1Name;

    @ApiModelProperty(value = "数量1")
    private BigDecimal quantity1;

    @ApiModelProperty(value = "单位2")
    private String unit2Code;

    @ApiModelProperty(value = "单位2")
    private String unit2Name;

    @ApiModelProperty(value = "数量2")
    private BigDecimal quantity2;

    @ApiModelProperty(value = "最终目的国")
    private String destinationCountry;

    @ApiModelProperty(value = "最终目的国")
    private String destinationCountryName;

    @ApiModelProperty(value = "征免方式")
    private String dutyMode;

    @ApiModelProperty(value = "征免方式")
    private String dutyModeName;

    @ApiModelProperty(value = "境内目的地")
    private String districtCode;

    @ApiModelProperty(value = "境内目的地")
    private String districtName;

    @ApiModelProperty(value = "目的地代码")
    private String ciqDestCode;

    @ApiModelProperty(value = "目的地代码")
    private String ciqDestName;


}
