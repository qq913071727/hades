package com.tsfyun.scm.config.ws;


public interface RedisReceivermsg {

    /**
     * 接受业务信息
     * @param channel
     * @param message
     */
    void receiveMessage(String channel,String message);

}
