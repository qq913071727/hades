package com.tsfyun.scm.service.impl.customs;


import cn.hutool.core.util.StrUtil;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.tsfyun.common.base.dto.ExportFileDTO;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.validator.ValidatorUtils;
import com.tsfyun.scm.entity.file.UploadFile;
import com.tsfyun.scm.service.customs.IDeclarationPdfService;
import com.tsfyun.scm.service.customs.IDeclarationService;
import com.tsfyun.scm.service.file.IExportFileService;
import com.tsfyun.scm.service.file.IUploadFileService;
import com.tsfyun.scm.vo.customs.ContractMemberVO;
import com.tsfyun.scm.vo.customs.ContractPlusVO;
import com.tsfyun.scm.vo.customs.ContractVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@RefreshScope
@Service
@Slf4j
public class DeclarationPdfServiceImpl implements IDeclarationPdfService {

    @Autowired
    private IDeclarationService declarationService;
    @Autowired
    private IExportFileService exportFileService;
    @Autowired
    private IUploadFileService uploadFileService;
    @Value("${file.directory}")
    private String filePath;


    @Override
    public Long contract(Long id,Boolean autoChapter) {
        ContractPlusVO vo = declarationService.contractData(id);
        String path = filePath+"temp/"+"scm"+"/contract/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createContractPdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long packing(Long id,Boolean autoChapter) {
        ContractPlusVO vo = declarationService.contractData(id);
        String path = filePath+"temp/"+ "scm" +"/packing/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createPackingPdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long invoice(Long id,Boolean autoChapter) {
        ContractPlusVO vo = declarationService.contractData(id);
        String path = filePath+"temp/"+"scm"+"/invoice/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createInvoicePdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long delivery(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.contractData(id);
        String path = filePath+"temp/"+"scm"+"/delivery/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createDeliveryPdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long merge(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.contractData(id);

        String contractPath = filePath+"temp/"+"scm"+"/contract/";
        String contractSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO contractExportFileDTO = createContractPdf(vo,autoChapter,contractPath,contractSysFileName);

        String invoicePath = filePath+"temp/"+"scm"+"/invoice/";
        String invoiceSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO invoiceExportFileDTO = createInvoicePdf(vo,autoChapter,invoicePath,invoiceSysFileName);

        String packingPath = filePath+"temp/"+ "scm" +"/packing/";
        String packingSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO packingExportFileDTO = createPackingPdf(vo,autoChapter,packingPath,packingSysFileName);

        String deliveryPath = filePath+"temp/"+ "scm" +"/delivery/";
        String deliverySysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO deliveryExportFileDTO = createDeliveryPdf(vo,autoChapter,deliveryPath,deliverySysFileName);

        PDFMergerUtility mergePdf = new PDFMergerUtility();
        try {
            mergePdf.addSource(contractExportFileDTO.getPath());
            mergePdf.addSource(packingExportFileDTO.getPath());
            mergePdf.addSource(invoiceExportFileDTO.getPath());
            mergePdf.addSource(deliveryExportFileDTO.getPath());

            String mergePath = filePath+"temp/"+"scm"+"/merge/";
            ValidatorUtils.isTrueCall(!new File(mergePath).exists(),()->new File(mergePath).mkdirs());
            String mergeSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
            mergePdf.setDestinationFileName(mergePath + mergeSysFileName);
            mergePdf.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());

            String fileName = StrUtil.format("{}_合同箱单发票出库单.pdf",vo.getMain().getContrNo());
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(mergePath + File.separator + mergeSysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            Long fileId = exportFileService.save(exportFileDTO);

            //删除临时文件
            new File(contractExportFileDTO.getPath()).delete();
            new File(invoiceExportFileDTO.getPath()).delete();
            new File(packingExportFileDTO.getPath()).delete();
            return fileId;
        } catch (Exception e) {
            log.error("合并PDF异常",e);
            throw new ServiceException("合并导出异常，请稍后再试");
        }
    }

    public ExportFileDTO createContractPdf(ContractPlusVO vo,Boolean autoChapter,String path,String sysFileName) {
        try{
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();

            String fileName = String.format("%s_CONTRACT.pdf",main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            Paragraph content = new Paragraph("外贸合同",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("CONTRACT",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("日期 Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getContractDate()),textFont));
            table.addCell(new Phrase("合同编号 Contract No.：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Paragraph("买方 Buyer：",textFont));
            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }
            String sellerName = "";
            if(StringUtils.isNotEmpty(main.getSellerName())){
                sellerName =  main.getSellerName() + "\n";
            }
            sellerName += main.getSellerNameEn();
            table.addCell(new Paragraph(buyerName,textFont));
            table.addCell(new Paragraph("卖方 Seller：",textFont));
            table.addCell(new Paragraph(sellerName,textFont));
            table.addCell(new Paragraph("电话 Tel：",textFont));
            table.addCell(new Paragraph(StringUtils.removeSpecialSymbol(main.getBuyerTel()),textFont));
            table.addCell(new Paragraph("电话 Tel：",textFont));
            table.addCell(new Paragraph(StringUtils.removeSpecialSymbol(main.getSellerTel()),textFont));
            table.addCell(new Paragraph("传真 Fax：",textFont));
            table.addCell(new Paragraph(StringUtils.removeSpecialSymbol(main.getBuyerFax()),textFont));
            table.addCell(new Paragraph("传真 Fax：",textFont));
            table.addCell(new Paragraph(StringUtils.removeSpecialSymbol(main.getSellerFax()),textFont));
            document.add(table);
            document.add(new Paragraph("兹经买卖双方同意由卖方出售买方购进如下货物，并按下列条款签定本合同：\n" +
                    "This contract is made by and between the Buyers and Sellers, whereby the Buyers agree to buy and the " +
                    "Sellers agree to sell the undermentioned commodity according to the terms and conditions stipulated below:",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,16,10,16,9,6,9,9,7};
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号\nNo.",textFont));
            productsTable.addCell(new Phrase("品名\nName",textFont));
            productsTable.addCell(new Phrase("品牌\nBrand",textFont));
            productsTable.addCell(new Phrase("型号\nModel",textFont));
            productsTable.addCell(new Phrase("数量\nQuantity",textFont));
            productsTable.addCell(new Phrase("单位\nUnit",textFont));
            productsTable.addCell(new Phrase("单价\nPrice",textFont));
            productsTable.addCell(new Phrase("总价\nAmount",textFont));
            productsTable.addCell(new Phrase("币制\nCurrency",textFont));
            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal ttotalPrice = BigDecimal.ZERO;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                ttotalPrice = ttotalPrice.add(womm.getTotalPrice()).setScale(2,BigDecimal.ROUND_HALF_UP);
                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getName()),textFont));
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getBrand()),textFont));
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getModel()),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getUnitName()),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getUnitPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getTotalPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCurrencyName()),textFont));
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(ttotalPrice),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);

            document.add(new Paragraph(" ",textFont));
            PdfPTable table1 = new PdfPTable(4);
            table1.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table1.setWidthPercentage(100);
            int[] array1 = {25, 25, 25, 25};
            table1.setWidths(array1);
            table1.addCell(new Phrase("1.价格条款Term：",textFont));
            table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getTransactionModeDesc()),textFont));
            table1.addCell(new Phrase("2.包装Packing：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table1.addCell(new Phrase("符合进口标准 In line with import standards",textFont));
            }else{
                table1.addCell(new Phrase("符合出口标准 In line with export standards",textFont));
            }
            table1.addCell(new Phrase("3.装运唛头Shipping Mark：",textFont));
            table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getMarkNo()),textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table1.addCell(new Phrase("4.目的地Destination：",textFont));
                table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getGoodsPlace()),textFont));
            }else{
                table1.addCell(new Phrase("4.运抵国（地区）：",textFont));
                table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getCusTradeCountryName()),textFont));
            }

            table1.addCell(new Phrase("5.装运期限Shipping Date：",textFont));
            table1.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getShipmentDate()),textFont));
            table1.addCell(new Phrase("6.保险Insurance：",textFont));
            table1.addCell(new Phrase("买方承付 PAID BY BUYER",textFont));

            table1.addCell(new Phrase("7.付款方式Term of Payment：",textFont));
            table1.addCell(new Phrase("TT",textFont));
            table1.addCell(new Phrase(" ",textFont));
            table1.addCell(new Phrase("",textFont));
            document.add(table1);

            document.add(new Paragraph("8.合同有效期：经双方签字、盖章后生效，有效期至 "+LocalDateTimeUtils.formatShort(main.getValidityDate())+"\n This contract, when duly signed by both parties, shall come into effect, expiry date shall be "+LocalDateTimeUtils.formatShort(main.getValidityDate()),textFont));

            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                document.add(new Paragraph("9.如进口货物存在任何质量问题，在进口后三年内双方协商处理（退货、换货、整机免费维修），所产生的费用由卖方负责。\n" +
                        " The improted goods with any quality problems can be returned,replaced or the complete machine shall be repai by negotiation process within three years.ALL the expense should be paid by the seller.",textFont));
            }else{
                document.add(new Paragraph("9.质量及验货：按双方约定的质量要求，符合出口质量标准，买方对货物的品名、品牌、型号、规格、数量等进行检验。\n" +
                        " According to the requirement of quality that agreed by both parties, in line with the export standards, the buyer to inspect the cargo, including brand, model, specifications, quantity etc.",textFont));
            }

            document.add(new Paragraph(" 10.本合同一式两份，双方各执一份。\n This contract is in two copies.",textFont));

            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getBuyerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getBuyerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)) {
                            image.scaleAbsolute(136,136);
                        } else {
                            image.scaleAbsolute(140,76);
                        }
                        image.setAlignment(Image.LEFT|Image.UNDERLYING);
                        image.setAbsolutePosition(100,image.getAbsoluteY());
                        document.add(image);
                    }
                }catch (Exception e){}
            }
            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)) {
                            image.scaleAbsolute(140,76);
                        } else {
                            image.scaleAbsolute(136,136);
                        }
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        image.setAbsolutePosition(400,image.getAbsoluteY());
                        document.add(image);
                    }
                }catch (Exception e){}
            }


            PdfPTable autographTable = new PdfPTable(4);
            autographTable.setWidthPercentage(100);
            autographTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            int[] autographArray = {20,30,20,30};
            autographTable.setWidths(autographArray);

            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));

            autographTable.addCell(new Phrase("买方 Buyer：",textFont));
            autographTable.addCell(new Phrase(buyerName,textFont));
            autographTable.addCell(new Phrase("卖方 Seller：",textFont));
            autographTable.addCell(new Phrase(sellerName,textFont));

            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));

            autographTable.addCell(new Phrase("签字\nAuthorized Signature：",textFont));
            autographTable.addCell(new Phrase("",textFont));
            autographTable.addCell(new Phrase("签字\nAuthorized Signature：",textFont));
            autographTable.addCell(new Phrase("",textFont));

            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            document.add(autographTable);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成合同PDF失败：",e);
            throw new ServiceException("生成合同PDF失败");
        }
    }

    public ExportFileDTO createPackingPdf(ContractPlusVO vo,Boolean autoChapter,String path,String sysFileName){
        try {
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();
            String fileName = String.format("%s_PACKING.pdf", main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            if(StringUtils.isNotEmpty(main.getSellerName())){
                Paragraph  content = new Paragraph(main.getSellerName(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerNameEn())){
                Paragraph  content = new Paragraph(main.getSellerNameEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddress())){
                Paragraph  content = new Paragraph(main.getSellerAddress(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddressEn())){
                Paragraph  content = new Paragraph(main.getSellerAddressEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            Paragraph  content = new Paragraph(String.format("电话：%s  传真：%s",(StringUtils.isNotEmpty(main.getSellerTel())?main.getSellerTel():"     "),(StringUtils.isNotEmpty(main.getSellerFax())?main.getSellerFax():"     ")),textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);

            content = new Paragraph("装箱单",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("PACKING LIST",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("Invoice No：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("Invoice Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getInvoiceDate()),textFont));

            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }
            String sellerName = "";
            if(StringUtils.isNotEmpty(main.getSellerName())){
                sellerName =  main.getSellerName() + "\n";
            }
            sellerName += main.getSellerNameEn();
            table.addCell(new Phrase("Invoice To：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table.addCell(new Phrase(buyerName,textFont));
                table.addCell(new Phrase("Ship To：",textFont));
                table.addCell(new Phrase(StringUtils.format("%s (%s)",StringUtils.removeSpecialSymbol(main.getOwnerName()),StringUtils.removeSpecialSymbol(main.getOwnerCode())),textFont));
            }else{
                // 合并单元格
                PdfPCell cell = new PdfPCell(new Phrase(buyerName,textFont));
                cell.setColspan(3);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
            document.add(table);

            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(Objects.equals(billTypeEnum,BillTypeEnum.IMP)?10:9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,14,9,14,9,5,8,8,5,8};
            if(Objects.equals(billTypeEnum,BillTypeEnum.EXP)){
                productsArray = new int[]{5,17,11,17,9,5,8,8,5};
            }
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号",textFont));
            productsTable.addCell(new Phrase("品名",textFont));
            productsTable.addCell(new Phrase("品牌",textFont));
            productsTable.addCell(new Phrase("型号",textFont));
            productsTable.addCell(new Phrase("数量",textFont));
            productsTable.addCell(new Phrase("单位",textFont));
            productsTable.addCell(new Phrase("净重(KG)",textFont));
            productsTable.addCell(new Phrase("毛重(KG)",textFont));
            productsTable.addCell(new Phrase("件数",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                productsTable.addCell(new Phrase("产地",textFont));
            }

            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal tnetWeight = BigDecimal.ZERO;
            BigDecimal tgrossWeight = BigDecimal.ZERO;
            Integer tcarton = 0;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                tnetWeight = tnetWeight.add(womm.getNetWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tgrossWeight = tgrossWeight.add(womm.getGrossWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tcarton = tcarton + womm.getCartonNum();
                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(womm.getName(),textFont));
                productsTable.addCell(new Phrase(womm.getBrand(),textFont));
                productsTable.addCell(new Phrase(womm.getModel(),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getUnitName(),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getNetWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getGrossWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCartonNum()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                    productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCountryName()),textFont));
                }
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tnetWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tgrossWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(tcarton.toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                document.add(new Paragraph(" ",textFont));
            }
            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)) {
                            image.scaleAbsolute(140,76);
                        } else {
                            image.scaleAbsolute(136,136);
                        }
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        document.add(image);
                    }
                }catch (Exception e){}
            }

            content = new Paragraph("For and on behalf of",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            document.add(new Paragraph(" ",textFont));
            content = new Paragraph("_________________",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            content = new Paragraph("Authorized Signature",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成装箱单PDF失败：",e);
            throw new ServiceException("生成装箱单PDF失败");
        }
    }

    public ExportFileDTO createInvoicePdf(ContractPlusVO vo,Boolean autoChapter,String path,String sysFileName){
        try {
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();
            String fileName = String.format("%s_INVOICE.pdf", main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            if(StringUtils.isNotEmpty(main.getSellerName())){
                Paragraph  content = new Paragraph(main.getSellerName(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerNameEn())){
                Paragraph  content = new Paragraph(main.getSellerNameEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddress())){
                Paragraph  content = new Paragraph(main.getSellerAddress(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddressEn())){
                Paragraph  content = new Paragraph(main.getSellerAddressEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            Paragraph  content = new Paragraph(String.format("电话：%s  传真：%s",(StringUtils.isNotEmpty(main.getSellerTel())?main.getSellerTel():"     "),(StringUtils.isNotEmpty(main.getSellerFax())?main.getSellerFax():"     ")),textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);

            content = new Paragraph("发票",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("INVOICE",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("Invoice No：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("Invoice Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getInvoiceDate()),textFont));

            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }


            table.addCell(new Phrase("Invoice To：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table.addCell(new Phrase(buyerName,textFont));
                table.addCell(new Phrase("Ship To：",textFont));
                table.addCell(new Phrase(StringUtils.format("%s (%s)",StringUtils.removeSpecialSymbol(main.getOwnerName()),StringUtils.removeSpecialSymbol(main.getOwnerCode())),textFont));
            }else{
                // 合并单元格
                PdfPCell cell = new PdfPCell(new Phrase(buyerName,textFont));
                cell.setColspan(3);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
            document.add(table);
            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(Objects.equals(billTypeEnum,BillTypeEnum.IMP)?9:9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,14,9,14,9,5,8,9,7};
            if(Objects.equals(billTypeEnum,BillTypeEnum.EXP)){
                productsArray = new int[]{5,17,11,17,9,5,8,8,5};
            }
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号\nNo.",textFont));
            productsTable.addCell(new Phrase("品名\nName",textFont));
            productsTable.addCell(new Phrase("品牌\nBrand",textFont));
            productsTable.addCell(new Phrase("型号\nModel",textFont));
            productsTable.addCell(new Phrase("数量\nQuantity",textFont));
            productsTable.addCell(new Phrase("单位\nUnit",textFont));
            productsTable.addCell(new Phrase("单价\nPrice",textFont));
            productsTable.addCell(new Phrase("总价\nAmount",textFont));
            productsTable.addCell(new Phrase("币制\nCurrency",textFont));
            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal ttotalPrice = BigDecimal.ZERO;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                ttotalPrice = ttotalPrice.add(womm.getTotalPrice()).setScale(2,BigDecimal.ROUND_HALF_UP);

                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(womm.getName(),textFont));
                productsTable.addCell(new Phrase(womm.getBrand(),textFont));
                productsTable.addCell(new Phrase(womm.getModel(),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getUnitName(),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getUnitPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getTotalPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getCurrencyName(),textFont));
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(ttotalPrice),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);
            document.add(new Paragraph(" ",textFont));

            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)) {
                            image.scaleAbsolute(140,76);
                        } else {
                            image.scaleAbsolute(136,136);
                        }
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        document.add(image);
                    }
                }catch (Exception e){}
            }

            content = new Paragraph("For and on behalf of",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            document.add(new Paragraph(" ",textFont));
            content = new Paragraph("_________________",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            content = new Paragraph("Authorized Signature",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成发票PDF失败：",e);
            throw new ServiceException("生成发票PDF失败");
        }
    }

    @Override
    public ExportFileDTO createDeliveryPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName) {
        try {
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();
            String fileName = String.format("%s_DELIVERY.pdf", main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            if(StringUtils.isNotEmpty(main.getSellerName())){
                Paragraph  content = new Paragraph(main.getSellerName(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerNameEn())){
                Paragraph  content = new Paragraph(main.getSellerNameEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddress())){
                Paragraph  content = new Paragraph(main.getSellerAddress(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddressEn())){
                Paragraph  content = new Paragraph(main.getSellerAddressEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            Paragraph  content = new Paragraph(String.format("电话：%s  传真：%s",(StringUtils.isNotEmpty(main.getSellerTel())?main.getSellerTel():"     "),(StringUtils.isNotEmpty(main.getSellerFax())?main.getSellerFax():"     ")),textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);

            content = new Paragraph("出库签收单",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("CARGO RECEIPT",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("Doc No：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getInvoiceDate()),textFont));

            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }
            String sellerName = "";
            if(StringUtils.isNotEmpty(main.getSellerName())){
                sellerName =  main.getSellerName() + "\n";
            }
            sellerName += main.getSellerNameEn();
            table.addCell(new Phrase("Invoice To：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table.addCell(new Phrase(buyerName,textFont));
                table.addCell(new Phrase("Ship To：",textFont));
                table.addCell(new Phrase(StringUtils.format("%s (%s)",StringUtils.removeSpecialSymbol(main.getOwnerName()),StringUtils.removeSpecialSymbol(main.getOwnerCode())),textFont));
            }else{
                // 合并单元格
                PdfPCell cell = new PdfPCell(new Phrase(buyerName,textFont));
                cell.setColspan(3);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
            document.add(table);

            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(Objects.equals(billTypeEnum,BillTypeEnum.IMP)?10:9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,14,9,14,9,5,8,8,5,8};
            if(Objects.equals(billTypeEnum,BillTypeEnum.EXP)){
                productsArray = new int[]{5,17,11,17,9,5,8,8,5};
            }
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号",textFont));
            productsTable.addCell(new Phrase("品名",textFont));
            productsTable.addCell(new Phrase("品牌",textFont));
            productsTable.addCell(new Phrase("型号",textFont));
            productsTable.addCell(new Phrase("数量",textFont));
            productsTable.addCell(new Phrase("单位",textFont));
            productsTable.addCell(new Phrase("净重(KG)",textFont));
            productsTable.addCell(new Phrase("毛重(KG)",textFont));
            productsTable.addCell(new Phrase("件数",textFont));

            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal tnetWeight = BigDecimal.ZERO;
            BigDecimal tgrossWeight = BigDecimal.ZERO;
            Integer tcarton = 0;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                tnetWeight = tnetWeight.add(womm.getNetWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tgrossWeight = tgrossWeight.add(womm.getGrossWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tcarton = tcarton + womm.getCartonNum();
                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(womm.getName(),textFont));
                productsTable.addCell(new Phrase(womm.getBrand(),textFont));
                productsTable.addCell(new Phrase(womm.getModel(),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getUnitName(),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getNetWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getGrossWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCartonNum()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tnetWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tgrossWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(tcarton.toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);

            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph("尊敬的客户：",textFont));
            document.add(new Paragraph("1.请收货后，即时签署及盖章，以证明货物己送到。",textFont));
            document.add(new Paragraph("2.请您按照本清单所列货物名称、数量、件数签收。签收前，请仔细检查货物外包装是否完好无损。若发生外\n" +
                    "包装破损等其它异常情况，请在本清单中做详细说明，请及时提供相片，并在24小时内通知我司。一经签字或\n" +
                    "盖章，确认将视为外包及防撕标签完好无损。",textFont));
            document.add(new Paragraph("3.如对本单所列之产品数量存在疑问，请于收货后24小时内以书面通知本公司。",textFont));
            document.add(new Paragraph("4.如果有任何问题，请及时与我们联系。",textFont));

            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));

            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)) {
                            image.scaleAbsolute(140,100);
                        } else {
                            image.scaleAbsolute(140,140);
                        }
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        document.add(image);
                    }
                }catch (Exception e){}
            }

            content = new Paragraph("For and on behalf of",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            document.add(new Paragraph(" ",textFont));
            content = new Paragraph("_________________",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            content = new Paragraph("Authorized Signature",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成出库签收单PDF失败：",e);
            throw new ServiceException("生成出库签收单PDF失败");
        }
    }

    @Override
    public Long overseasContract(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.overseasContractData(id);
        String path = filePath+"temp/"+"scm"+"/overseas_contract/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createOverseasContractPdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long overseasPacking(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.overseasContractData(id);
        String path = filePath+"temp/"+ "scm" +"/overseas_packing/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createOverseasPackingPdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long overseasInvoice(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.overseasContractData(id);
        String path = filePath+"temp/"+"scm"+"/overseas_invoice/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createOverseasInvoicePdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long overseasDelivery(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.overseasContractData(id);
        String path = filePath+"temp/"+"scm"+"/overseas_delivery/";
        String sysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO exportFileDTO = createOverseasDeliveryPdf(vo,autoChapter,path,sysFileName);
        Long fileId = exportFileService.save(exportFileDTO);
        return fileId;
    }

    @Override
    public Long mergeOverseas(Long id, Boolean autoChapter) {
        ContractPlusVO vo = declarationService.overseasContractData(id);
        String contractPath = filePath+"temp/"+"scm"+"/overseas_contract/";
        String contractSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO contractExportFileDTO = createOverseasContractPdf(vo,autoChapter,contractPath,contractSysFileName);

        String packingPath = filePath+"temp/"+ "scm" +"/overseas_packing/";
        String packingSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO packingExportFileDTO = createOverseasPackingPdf(vo,autoChapter,packingPath,packingSysFileName);

        String invoicePath = filePath+"temp/"+"scm"+"/overseas_invoice/";
        String invoiceSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO invoiceExportFileDTO = createOverseasInvoicePdf(vo,autoChapter,invoicePath,invoiceSysFileName);

        String deliveryPath = filePath+"temp/"+"scm"+"/overseas_delivery/";
        String deliverySysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
        ExportFileDTO deliveryExportFileDTO = createOverseasDeliveryPdf(vo,autoChapter,deliveryPath,deliverySysFileName);

        PDFMergerUtility mergePdf = new PDFMergerUtility();
        try {
            mergePdf.addSource(contractExportFileDTO.getPath());
            mergePdf.addSource(packingExportFileDTO.getPath());
            mergePdf.addSource(invoiceExportFileDTO.getPath());
            mergePdf.addSource(deliveryExportFileDTO.getPath());

            String mergePath = filePath+"temp/"+"scm"+"/merge_overseas/";
            ValidatorUtils.isTrueCall(!new File(mergePath).exists(),()->new File(mergePath).mkdirs());
            String mergeSysFileName = String.format("%s_%s.pdf",vo.getMain().getContrNo(),LocalDateTimeUtils.formatNow("yyyyMMddHHmmssSSS"));
            mergePdf.setDestinationFileName(mergePath + mergeSysFileName);
            mergePdf.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());

            String fileName = StrUtil.format("{}_合同箱单发票.pdf",vo.getMain().getContrNo());
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(mergePath + File.separator + mergeSysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            Long fileId = exportFileService.save(exportFileDTO);

            //删除临时文件
            new File(contractExportFileDTO.getPath()).delete();
            new File(invoiceExportFileDTO.getPath()).delete();
            new File(packingExportFileDTO.getPath()).delete();
            return fileId;
        } catch (Exception e) {
            log.error("合并PDF异常",e);
            throw new ServiceException("合并导出异常，请稍后再试");
        }
    }

    @Override
    public ExportFileDTO createOverseasContractPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName) {
        try{
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();

            String fileName = String.format("%s_CONTRACT.pdf",main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            Paragraph content = new Paragraph("境外合同",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("CONTRACT",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("日期 Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getContractDate()),textFont));
            table.addCell(new Phrase("合同编号 Contract No.：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Phrase("",textFont));
            table.addCell(new Paragraph("买方 Buyer：",textFont));
            String buyerName = main.getSupplierName();
            String sellerName = "";
            if(StringUtils.isNotEmpty(main.getSellerName())){
                sellerName =  main.getSellerName() + "\n";
            }
            sellerName += main.getSellerNameEn();
            table.addCell(new Paragraph(buyerName,textFont));
            table.addCell(new Paragraph("卖方 Seller：",textFont));
            table.addCell(new Paragraph(sellerName,textFont));
            table.addCell(new Paragraph("电话 Tel：",textFont));
            table.addCell(new Paragraph("",textFont));
            table.addCell(new Paragraph("电话 Tel：",textFont));
            table.addCell(new Paragraph(StringUtils.removeSpecialSymbol(main.getSellerTel()),textFont));
            table.addCell(new Paragraph("传真 Fax：",textFont));
            table.addCell(new Paragraph("",textFont));
            table.addCell(new Paragraph("传真 Fax：",textFont));
            table.addCell(new Paragraph(StringUtils.removeSpecialSymbol(main.getSellerFax()),textFont));
            document.add(table);
            document.add(new Paragraph("兹经买卖双方同意由卖方出售买方购进如下货物，并按下列条款签定本合同：\n" +
                    "This contract is made by and between the Buyers and Sellers, whereby the Buyers agree to buy and the " +
                    "Sellers agree to sell the undermentioned commodity according to the terms and conditions stipulated below:",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,16,10,16,9,6,9,9,7};
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号\nNo.",textFont));
            productsTable.addCell(new Phrase("品名\nName",textFont));
            productsTable.addCell(new Phrase("品牌\nBrand",textFont));
            productsTable.addCell(new Phrase("型号\nModel",textFont));
            productsTable.addCell(new Phrase("数量\nQuantity",textFont));
            productsTable.addCell(new Phrase("单位\nUnit",textFont));
            productsTable.addCell(new Phrase("单价\nPrice",textFont));
            productsTable.addCell(new Phrase("总价\nAmount",textFont));
            productsTable.addCell(new Phrase("币制\nCurrency",textFont));
            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal ttotalPrice = BigDecimal.ZERO;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                ttotalPrice = ttotalPrice.add(womm.getTotalPrice()).setScale(2,BigDecimal.ROUND_HALF_UP);
                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getName()),textFont));
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getBrand()),textFont));
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getModel()),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getUnitName()),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getUnitPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getTotalPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCurrencyName()),textFont));
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(ttotalPrice),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);

            document.add(new Paragraph(" ",textFont));
            PdfPTable table1 = new PdfPTable(4);
            table1.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table1.setWidthPercentage(100);
            int[] array1 = {25, 25, 25, 25};
            table1.setWidths(array1);
            table1.addCell(new Phrase("1.价格条款Term：",textFont));
            table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getTransactionModeDesc()),textFont));
            table1.addCell(new Phrase("2.包装Packing：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table1.addCell(new Phrase("符合进口标准 In line with import standards",textFont));
            }else{
                table1.addCell(new Phrase("符合出口标准 In line with export standards",textFont));
            }
            table1.addCell(new Phrase("3.装运唛头Shipping Mark：",textFont));
            table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getMarkNo()),textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table1.addCell(new Phrase("4.目的地Destination：",textFont));
                table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getGoodsPlace()),textFont));
            }else{
                table1.addCell(new Phrase("4.运抵国（地区）：",textFont));
                table1.addCell(new Phrase(StringUtils.removeSpecialSymbol(main.getCusTradeCountryName()),textFont));
            }

            table1.addCell(new Phrase("5.装运期限Shipping Date：",textFont));
            table1.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getShipmentDate()),textFont));
            table1.addCell(new Phrase("6.保险Insurance：",textFont));
            table1.addCell(new Phrase("买方承付 PAID BY BUYER",textFont));

            table1.addCell(new Phrase("7.付款方式Term of Payment：",textFont));
            table1.addCell(new Phrase("TT",textFont));
            table1.addCell(new Phrase(" ",textFont));
            table1.addCell(new Phrase("",textFont));
            document.add(table1);

            document.add(new Paragraph("8.合同有效期：经双方签字、盖章后生效，有效期至 "+LocalDateTimeUtils.formatShort(main.getValidityDate())+"\n This contract, when duly signed by both parties, shall come into effect, expiry date shall be "+LocalDateTimeUtils.formatShort(main.getValidityDate()),textFont));

            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                document.add(new Paragraph("9.如进口货物存在任何质量问题，在进口后三年内双方协商处理（退货、换货、整机免费维修），所产生的费用由卖方负责。\n" +
                        " The improted goods with any quality problems can be returned,replaced or the complete machine shall be repai by negotiation process within three years.ALL the expense should be paid by the seller.",textFont));
            }else{
                document.add(new Paragraph("9.质量及验货：按双方约定的质量要求，符合出口质量标准，买方对货物的品名、品牌、型号、规格、数量等进行检验。\n" +
                        " According to the requirement of quality that agreed by both parties, in line with the export standards, the buyer to inspect the cargo, including brand, model, specifications, quantity etc.",textFont));
            }

            document.add(new Paragraph(" 10.本合同一式两份，双方各执一份。\n This contract is in two copies.",textFont));

            /*
            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getBuyerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getBuyerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        image.scaleAbsolute(140,100);
                        image.setAlignment(Image.LEFT|Image.UNDERLYING);
                        image.setAbsolutePosition(100,image.getAbsoluteY());
                        document.add(image);
                    }
                }catch (Exception e){}
            }
             */
            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        image.scaleAbsolute(140,100);
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        image.setAbsolutePosition(400,image.getAbsoluteY());
                        document.add(image);
                    }
                }catch (Exception e){}
            }


            PdfPTable autographTable = new PdfPTable(4);
            autographTable.setWidthPercentage(100);
            autographTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            int[] autographArray = {20,30,20,30};
            autographTable.setWidths(autographArray);

            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));

            autographTable.addCell(new Phrase("买方 Buyer：",textFont));
            autographTable.addCell(new Phrase(buyerName,textFont));
            autographTable.addCell(new Phrase("卖方 Seller：",textFont));
            autographTable.addCell(new Phrase(sellerName,textFont));

            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));

            autographTable.addCell(new Phrase("签字\nAuthorized Signature：",textFont));
            autographTable.addCell(new Phrase("",textFont));
            autographTable.addCell(new Phrase("签字\nAuthorized Signature：",textFont));
            autographTable.addCell(new Phrase("",textFont));

            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            autographTable.addCell(new Phrase(" ",textFont));
            document.add(autographTable);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成合同PDF失败：",e);
            throw new ServiceException("生成合同PDF失败");
        }
    }

    @Override
    public ExportFileDTO createOverseasPackingPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName) {
        try {
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();
            String fileName = String.format("%s_PACKING.pdf", main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            if(StringUtils.isNotEmpty(main.getSellerName())){
                Paragraph  content = new Paragraph(main.getSellerName(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerNameEn())){
                Paragraph  content = new Paragraph(main.getSellerNameEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddress())){
                Paragraph  content = new Paragraph(main.getSellerAddress(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddressEn())){
                Paragraph  content = new Paragraph(main.getSellerAddressEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            Paragraph  content = new Paragraph(String.format("电话：%s  传真：%s",(StringUtils.isNotEmpty(main.getSellerTel())?main.getSellerTel():"     "),(StringUtils.isNotEmpty(main.getSellerFax())?main.getSellerFax():"     ")),textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);

            content = new Paragraph("装箱单",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("PACKING LIST",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("Invoice No：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("Invoice Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getInvoiceDate()),textFont));

            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }
            String sellerName = "";
            if(StringUtils.isNotEmpty(main.getSellerName())){
                sellerName =  main.getSellerName() + "\n";
            }
            sellerName += main.getSellerNameEn();
            table.addCell(new Phrase("Invoice To：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table.addCell(new Phrase(buyerName,textFont));
                table.addCell(new Phrase("Ship To：",textFont));
                table.addCell(new Phrase(StringUtils.format("%s (%s)",StringUtils.removeSpecialSymbol(main.getOwnerName()),StringUtils.removeSpecialSymbol(main.getOwnerCode())),textFont));
            }else{
                // 合并单元格
                PdfPCell cell = new PdfPCell(new Phrase(buyerName,textFont));
                cell.setColspan(3);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
            document.add(table);

            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(Objects.equals(billTypeEnum,BillTypeEnum.IMP)?10:9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,14,9,14,9,5,8,8,5,8};
            if(Objects.equals(billTypeEnum,BillTypeEnum.EXP)){
                productsArray = new int[]{5,17,11,17,9,5,8,8,5};
            }
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号",textFont));
            productsTable.addCell(new Phrase("品名",textFont));
            productsTable.addCell(new Phrase("品牌",textFont));
            productsTable.addCell(new Phrase("型号",textFont));
            productsTable.addCell(new Phrase("数量",textFont));
            productsTable.addCell(new Phrase("单位",textFont));
            productsTable.addCell(new Phrase("净重(KG)",textFont));
            productsTable.addCell(new Phrase("毛重(KG)",textFont));
            productsTable.addCell(new Phrase("件数",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                productsTable.addCell(new Phrase("产地",textFont));
            }

            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal tnetWeight = BigDecimal.ZERO;
            BigDecimal tgrossWeight = BigDecimal.ZERO;
            Integer tcarton = 0;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                tnetWeight = tnetWeight.add(womm.getNetWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tgrossWeight = tgrossWeight.add(womm.getGrossWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tcarton = tcarton + womm.getCartonNum();
                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(womm.getName(),textFont));
                productsTable.addCell(new Phrase(womm.getBrand(),textFont));
                productsTable.addCell(new Phrase(womm.getModel(),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getUnitName(),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getNetWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getGrossWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCartonNum()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                    productsTable.addCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCountryName()),textFont));
                }
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tnetWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tgrossWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(tcarton.toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                document.add(new Paragraph(" ",textFont));
            }
            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        image.scaleAbsolute(140,100);
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        document.add(image);
                    }
                }catch (Exception e){}
            }

            content = new Paragraph("For and on behalf of",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            document.add(new Paragraph(" ",textFont));
            content = new Paragraph("_________________",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            content = new Paragraph("Authorized Signature",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成装箱单PDF失败：",e);
            throw new ServiceException("生成装箱单PDF失败");
        }
    }

    @Override
    public ExportFileDTO createOverseasInvoicePdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName) {
        try {
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();
            String fileName = String.format("%s_INVOICE.pdf", main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            if(StringUtils.isNotEmpty(main.getSellerName())){
                Paragraph  content = new Paragraph(main.getSellerName(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerNameEn())){
                Paragraph  content = new Paragraph(main.getSellerNameEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddress())){
                Paragraph  content = new Paragraph(main.getSellerAddress(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddressEn())){
                Paragraph  content = new Paragraph(main.getSellerAddressEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            Paragraph  content = new Paragraph(String.format("电话：%s  传真：%s",(StringUtils.isNotEmpty(main.getSellerTel())?main.getSellerTel():"     "),(StringUtils.isNotEmpty(main.getSellerFax())?main.getSellerFax():"     ")),textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);

            content = new Paragraph("发票",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("INVOICE",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("Invoice No：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("Invoice Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getInvoiceDate()),textFont));

            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }


            table.addCell(new Phrase("Invoice To：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table.addCell(new Phrase(buyerName,textFont));
                table.addCell(new Phrase("Ship To：",textFont));
                table.addCell(new Phrase(StringUtils.format("%s (%s)",StringUtils.removeSpecialSymbol(main.getOwnerName()),StringUtils.removeSpecialSymbol(main.getOwnerCode())),textFont));
            }else{
                // 合并单元格
                PdfPCell cell = new PdfPCell(new Phrase(buyerName,textFont));
                cell.setColspan(3);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
            document.add(table);
            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(Objects.equals(billTypeEnum,BillTypeEnum.IMP)?10:9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,14,9,14,9,5,8,9,7};
            if(Objects.equals(billTypeEnum,BillTypeEnum.EXP)){
                productsArray = new int[]{5,17,11,17,9,5,8,8,5};
            }
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号\nNo.",textFont));
            productsTable.addCell(new Phrase("品名\nName",textFont));
            productsTable.addCell(new Phrase("品牌\nBrand",textFont));
            productsTable.addCell(new Phrase("型号\nModel",textFont));
            productsTable.addCell(new Phrase("数量\nQuantity",textFont));
            productsTable.addCell(new Phrase("单位\nUnit",textFont));
            productsTable.addCell(new Phrase("单价\nPrice",textFont));
            productsTable.addCell(new Phrase("总价\nAmount",textFont));
            productsTable.addCell(new Phrase("币制\nCurrency",textFont));
            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal ttotalPrice = BigDecimal.ZERO;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                ttotalPrice = ttotalPrice.add(womm.getTotalPrice()).setScale(2,BigDecimal.ROUND_HALF_UP);

                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(womm.getName(),textFont));
                productsTable.addCell(new Phrase(womm.getBrand(),textFont));
                productsTable.addCell(new Phrase(womm.getModel(),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getUnitName(),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getUnitPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getTotalPrice()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getCurrencyName(),textFont));
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(ttotalPrice),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);
            document.add(new Paragraph(" ",textFont));

            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        image.scaleAbsolute(140,100);
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        document.add(image);
                    }
                }catch (Exception e){}
            }

            content = new Paragraph("For and on behalf of",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            document.add(new Paragraph(" ",textFont));
            content = new Paragraph("_________________",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            content = new Paragraph("Authorized Signature",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成发票PDF失败：",e);
            throw new ServiceException("生成发票PDF失败");
        }
    }

    @Override
    public ExportFileDTO createOverseasDeliveryPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName) {
        try {
            ContractVO main = vo.getMain();
            BillTypeEnum billTypeEnum = BillTypeEnum.of(main.getBillType());
            List<ContractMemberVO> members = vo.getMembers();
            String fileName = String.format("%s_DELIVERY.pdf", main.getContrNo());
            new File(path).mkdirs();
            FileOutputStream out = new FileOutputStream(path+File.separator+sysFileName);
            BaseFont bfCN = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",true);
            // 标题的字体
            Font chFont = new Font(bfCN, 20, Font.NORMAL, BaseColor.BLACK);
            // 正文的字体
            Font textFont = new Font(bfCN, 10, Font.NORMAL, BaseColor.BLACK);
            Document document = new Document();
            PdfWriter.getInstance(document, out);
            document.open();
            //4.添加标题
            if(StringUtils.isNotEmpty(main.getSellerName())){
                Paragraph  content = new Paragraph(main.getSellerName(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerNameEn())){
                Paragraph  content = new Paragraph(main.getSellerNameEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddress())){
                Paragraph  content = new Paragraph(main.getSellerAddress(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            if(StringUtils.isNotEmpty(main.getSellerAddressEn())){
                Paragraph  content = new Paragraph(main.getSellerAddressEn(),textFont);
                content.setAlignment(Element.ALIGN_CENTER);
                document.add(content);
            }
            Paragraph  content = new Paragraph(String.format("电话：%s  传真：%s",(StringUtils.isNotEmpty(main.getSellerTel())?main.getSellerTel():"     "),(StringUtils.isNotEmpty(main.getSellerFax())?main.getSellerFax():"     ")),textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);

            content = new Paragraph("出库签收单",chFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            content = new Paragraph("CARGO RECEIPT",textFont);
            content.setAlignment(Element.ALIGN_CENTER);
            document.add(content);
            LineSeparator line = new LineSeparator(1, 100, new BaseColor(204, 204, 204), Element.ALIGN_CENTER, 0);
            Paragraph p_line = new Paragraph("",textFont);
            p_line.add(line);
            document.add(p_line);
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            PdfPTable table = new PdfPTable(4);
            table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
            table.setWidthPercentage(100);
            int[] array = {15, 35, 15, 35};
            table.setWidths(array);
            table.addCell(new Phrase("Doc No：",textFont));
            table.addCell(new Phrase(main.getContrNo(),textFont));
            table.addCell(new Phrase("Date：",textFont));
            table.addCell(new Phrase(LocalDateTimeUtils.formatShort(main.getInvoiceDate()),textFont));

            String buyerName = main.getBuyerName();
            if(StringUtils.isNotEmpty(main.getBuyerNameEn())){
                buyerName += "\n"+main.getBuyerNameEn();
            }
            String sellerName = "";
            if(StringUtils.isNotEmpty(main.getSellerName())){
                sellerName =  main.getSellerName() + "\n";
            }
            sellerName += main.getSellerNameEn();
            table.addCell(new Phrase("Invoice To：",textFont));
            if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)){
                table.addCell(new Phrase(buyerName,textFont));
                table.addCell(new Phrase("Ship To：",textFont));
                table.addCell(new Phrase(StringUtils.format("%s (%s)",StringUtils.removeSpecialSymbol(main.getOwnerName()),StringUtils.removeSpecialSymbol(main.getOwnerCode())),textFont));
            }else{
                // 合并单元格
                PdfPCell cell = new PdfPCell(new Phrase(buyerName,textFont));
                cell.setColspan(3);
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
            }
            document.add(table);

            document.add(new Paragraph(" ",textFont));
            PdfPTable productsTable = new PdfPTable(Objects.equals(billTypeEnum,BillTypeEnum.IMP)?10:9);
            productsTable.setWidthPercentage(100);
            int[] productsArray = {5,14,9,14,9,5,8,8,5,8};
            if(Objects.equals(billTypeEnum,BillTypeEnum.EXP)){
                productsArray = new int[]{5,17,11,17,9,5,8,8,5};
            }
            productsTable.setWidths(productsArray);
            productsTable.addCell(new Phrase("行号",textFont));
            productsTable.addCell(new Phrase("品名",textFont));
            productsTable.addCell(new Phrase("品牌",textFont));
            productsTable.addCell(new Phrase("型号",textFont));
            productsTable.addCell(new Phrase("数量",textFont));
            productsTable.addCell(new Phrase("单位",textFont));
            productsTable.addCell(new Phrase("净重(KG)",textFont));
            productsTable.addCell(new Phrase("毛重(KG)",textFont));
            productsTable.addCell(new Phrase("件数",textFont));

            BigDecimal tquantity = BigDecimal.ZERO;
            BigDecimal tnetWeight = BigDecimal.ZERO;
            BigDecimal tgrossWeight = BigDecimal.ZERO;
            Integer tcarton = 0;
            for(int i=0;i<members.size();i++){
                ContractMemberVO womm = members.get(i);
                tquantity = tquantity.add(womm.getQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
                tnetWeight = tnetWeight.add(womm.getNetWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tgrossWeight = tgrossWeight.add(womm.getGrossWeight()).setScale(4,BigDecimal.ROUND_HALF_UP);
                tcarton = tcarton + womm.getCartonNum();
                productsTable.addCell(new Phrase((i+1)+"",textFont));
                productsTable.addCell(new Phrase(womm.getName(),textFont));
                productsTable.addCell(new Phrase(womm.getBrand(),textFont));
                productsTable.addCell(new Phrase(womm.getModel(),textFont));
                PdfPCell cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(womm.getQuantity()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                productsTable.addCell(new Phrase(womm.getUnitName(),textFont));
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getNetWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getGrossWeight()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
                cell = new PdfPCell(new Phrase(StringUtils.removeSpecialSymbol(womm.getCartonNum()),textFont));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                productsTable.addCell(cell);
            }
            PdfPCell cell = new PdfPCell(new Paragraph("合计 Total：",textFont));
            cell.setColspan(4);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.formatCurrency(tquantity),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tnetWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(StringUtils.rounded4(tgrossWeight).toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            cell = new PdfPCell(new Phrase(tcarton.toString(),textFont));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            productsTable.addCell(cell);
            productsTable.addCell(new Phrase(" ",textFont));
            document.add(productsTable);

            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph("尊敬的客户：",textFont));
            document.add(new Paragraph("1.请收货后，即时签署及盖章，以证明货物己送到。",textFont));
            document.add(new Paragraph("2.请您按照本清单所列货物名称、数量、件数签收。签收前，请仔细检查货物外包装是否完好无损。若发生外\n" +
                    "包装破损等其它异常情况，请在本清单中做详细说明，请及时提供相片，并在24小时内通知我司。一经签字或\n" +
                    "盖章，确认将视为外包及防撕标签完好无损。",textFont));
            document.add(new Paragraph("3.如对本单所列之产品数量存在疑问，请于收货后24小时内以书面通知本公司。",textFont));
            document.add(new Paragraph("4.如果有任何问题，请及时与我们联系。",textFont));

            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));
            document.add(new Paragraph(" ",textFont));

            if(Objects.equals(Boolean.TRUE,autoChapter)&&StringUtils.isNotEmpty(main.getSellerChapter())){
                try{
                    UploadFile uploadFile = uploadFileService.getById(main.getSellerChapter().replace("/scm/download/img/",""));
                    if(Objects.nonNull(uploadFile)){
                        Image image = Image.getInstance(uploadFile.getPath()+uploadFile.getNname());
                        if(Objects.equals(billTypeEnum,BillTypeEnum.IMP)) {
                            image.scaleAbsolute(140,100);
                        } else {
                            image.scaleAbsolute(140,140);
                        }
                        image.setAlignment(Image.RIGHT|Image.UNDERLYING);
                        document.add(image);
                    }
                }catch (Exception e){}
            }

            content = new Paragraph("For and on behalf of",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            document.add(new Paragraph(" ",textFont));
            content = new Paragraph("_________________",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);
            content = new Paragraph("Authorized Signature",textFont);
            content.setAlignment(Element.ALIGN_RIGHT);
            document.add(content);

            document.close();
            out.flush();
            out.close();
            ExportFileDTO exportFileDTO = new ExportFileDTO();
            exportFileDTO.setPath(path+File.separator+sysFileName);
            exportFileDTO.setFileName(fileName);
            exportFileDTO.setOnce(Boolean.TRUE);
            exportFileDTO.setOperator("系统自动生成");
            return exportFileDTO;
        }catch (Exception e){
            log.error("生成出库签收单PDF失败：",e);
            throw new ServiceException("生成出库签收单PDF失败");
        }
    }
}
