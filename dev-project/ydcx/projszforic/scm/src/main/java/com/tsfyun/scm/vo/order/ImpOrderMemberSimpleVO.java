package com.tsfyun.scm.vo.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/6 12:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpOrderMemberSimpleVO implements Serializable {

    private Long id;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "规格参数")
    private String spec;

}
