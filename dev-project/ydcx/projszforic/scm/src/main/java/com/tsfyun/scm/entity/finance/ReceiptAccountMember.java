package com.tsfyun.scm.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 收款核销明细
 * </p>
 *

 * @since 2020-05-22
 */
@Data
public class ReceiptAccountMember implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 收款单主单
     */

    private Long receiptAccountId;

    /**
     * 进口费用
     */

    private Long impOrderCostId;

    /**
     * 出口费用
     */
    private Long expOrderCostId;

    /**
     * 代理报关费用
     */
    private Long trustOrderCostId;

    /**
     * 退款
     */

    private Long refundAccountId;

    /**
     * 核销单号
     */

    private String docNo;

    /**
     * 费用科目ID
     */

    private String expenseSubjectId;

    /**
     * 费用科目名称
     */

    private String expenseSubjectName;

    /**
     * 核销金额
     */

    private BigDecimal accountValue;

    /**
     * 核销类型
     */

    private String writeOffType;

    /**
     * 操作人
     */

    private String operation;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

}
