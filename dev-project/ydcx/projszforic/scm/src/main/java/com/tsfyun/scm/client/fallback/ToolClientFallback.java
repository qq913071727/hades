package com.tsfyun.scm.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.QichachaCompanyVO;
import com.tsfyun.common.base.vo.SimpleBusinessVO;
import com.tsfyun.scm.client.ToolClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/30 16:59
 */
@Slf4j
public class ToolClientFallback implements FallbackFactory<ToolClient> {

    @Override
    public ToolClient create(Throwable throwable) {
        return new ToolClient() {

            @Override
            public Result<QichachaCompanyVO> detailByCompanyName(String companyName) {
                log.error(String.format("获取公司【%s】工商信息异常",companyName),throwable);
                return null;
            }

            @Override
            public Result<List<SimpleBusinessVO>> vagueQuery(String name) {
                log.error(String.format("模糊查询公司信息【%s】异常",name),throwable);
                return null;
            }
        };
    }
}
