package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.scm.enums.SubjectTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ExpConfirmBankDTO implements Serializable {
    @NotNull(message = "付款单id不能为空")
    private Long id;

    @NotEmptyTrim(message = "付款主体类型不能为空")
    @EnumCheck(clazz = SubjectTypeEnum.class,message = "付款主体类型错误")
    @ApiModelProperty(value = "付款主体类型")
    private String subjectType;

    @NotNull(message = "付款主体不能为空")
    @ApiModelProperty(value = "付款主体")
    private Long subjectId;

    @NotNull(message = "付款银行不能为空")
    private Long subjectBank;
}
