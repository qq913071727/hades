package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/12/23 15:18
 */
@Data
public class ExpOrderEntrustVO implements Serializable {

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date orderDate;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 主体企业名称
     */
    private String subjectName;

    /**
     * 境外客户
     */
    private String supplierName;

    /**
     * 进出境关别
     */
    private String iePortName;

    /**
     * 币制
     */
    private String currencyName;

    /**
     * 国内交货方式
     */

    private String deliveryModeName;

    /**
     * 提货公司
     */

    private String deliveryCompanyName;

    /**
     * 提货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 提货联系人电话
     */

    private String deliveryLinkTel;

    /**
     * 提货地址
     */

    private String deliveryAddress;


    /**
     * 境外交货-中港包车
     */

    private Boolean isCharterCar;

    /**
     * 送货目的地
     */

    private String deliveryDestination;


    private String deliveryDestinationName;


    /**
     * 收货公司
     */

    private String takeLinkCompany;

    /**
     * 收货联系人
     */

    private String takeLinkPerson;

    /**
     * 收货联系人电话
     */

    private String takeLinkTel;

    /**
     * 收货地址
     */

    private String takeAddress;

    /**
     * 收货备注
     */

    private String receivingModeMemo;

    /**
     * 备注
     */
    private String memo;

    /**
     * 明细
     */
    private List<InnerExpOrderMember> members;

    @Data
    public static class InnerExpOrderMember {

        /**
         * 品名
         */
        private String name;

        /**
         * 品牌
         */
        private String brand;

        /**
         * 型号
         */
        private String model;

        /**
         * 产地
         */

        private String countryName;

        /**
         * 数量
         */

        private BigDecimal quantity;

        /**
         * 单位
         */

        private String unitName;


        /**
         * 报关单价
         */

        private BigDecimal decUnitPrice;

        /**
         * 报关总价
         */

        private BigDecimal decTotalPrice;


        /**
         * 净重
         */

        private BigDecimal netWeight;

        /**
         * 毛重
         */

        private BigDecimal grossWeight;

        /**
         * 箱数
         */

        private Integer cartonNum;

    }

}
