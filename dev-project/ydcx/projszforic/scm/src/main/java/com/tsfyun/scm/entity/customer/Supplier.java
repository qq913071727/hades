package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 境外供应商
 * </p>
 *

 * @since 2020-03-12
 */
@Data
@Accessors(chain = true)
public class Supplier extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 名称
     */
    private String name;

    /**
     * 地址
     */
    private String address;

    /**
     * 禁用标示
     */
    private Boolean disabled;



}
