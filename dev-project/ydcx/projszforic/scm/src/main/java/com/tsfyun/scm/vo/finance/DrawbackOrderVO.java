package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class DrawbackOrderVO implements Serializable {

    @ApiModelProperty(value = "采购合同ID")
    private Long purchaseId;
    @ApiModelProperty(value = "采购合同编号")
    private String purchaseNo;
    @ApiModelProperty(value = "订单ID")
    private Long orderId;
    @ApiModelProperty(value = "订单编号")
    private String orderNo;
    @ApiModelProperty(value = "订单日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;
    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderTotalPrice;
    @ApiModelProperty(value = "订单币制")
    private String orderCurrencyName;
    @ApiModelProperty(value = "采购合同金额")
    private BigDecimal purchaseTotalPrice;
    @ApiModelProperty(value = "收票金额")
    private BigDecimal invoiceValue;
    @ApiModelProperty(value = "是否结汇完成")
    private Boolean isSettleAccount;
    @ApiModelProperty(value = "结汇金额")
    private BigDecimal settlementValue;
    @ApiModelProperty(value = "代垫金额")
    private BigDecimal deductionValue;
    @ApiModelProperty(value = "代理费金额")
    private BigDecimal agencyFee;
    @ApiModelProperty(value = "本次退税金额")
    private BigDecimal drawbackValue;
    @ApiModelProperty(value = "预估货款")
    private BigDecimal estimatedPayment;
    @ApiModelProperty(value = "应退税金额")
    private BigDecimal actualDrawbackValue;
    @ApiModelProperty(value = "已退税金额")
    private BigDecimal alreadyDrawbackValue;
}
