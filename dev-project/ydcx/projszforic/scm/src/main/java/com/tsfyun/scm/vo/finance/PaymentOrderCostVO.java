package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class PaymentOrderCostVO implements Serializable {

    private Long id;
    private String docNo;//付汇单号
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDateTime accountDate;//申请时间
    private BigDecimal exchangeRate;//汇率
    private BigDecimal accountValue;//原币金额
    private BigDecimal accountValueCny;//人民币金额
    private BigDecimal bankFee;//付汇手续费
    private String currencyName;//币制
    private String statusId;//状态
}
