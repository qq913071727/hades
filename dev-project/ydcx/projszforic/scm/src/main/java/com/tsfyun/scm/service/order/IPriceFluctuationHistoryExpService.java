package com.tsfyun.scm.service.order;

import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.scm.entity.order.PriceFluctuationHistoryExp;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2021-09-17
 */
public interface IPriceFluctuationHistoryExpService extends IService<PriceFluctuationHistoryExp> {

    List<PriceFluctuationHistoryExp> list(Long memberId);
}
