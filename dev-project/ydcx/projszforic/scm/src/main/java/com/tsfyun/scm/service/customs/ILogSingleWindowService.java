package com.tsfyun.scm.service.customs;

import com.tsfyun.scm.entity.customs.LogSingleWindow;
import com.tsfyun.common.base.extension.IService;

import java.util.List;

/**
 * 单一窗口日志
 *
 * @since 2020-05-07
 */
public interface ILogSingleWindowService extends IService<LogSingleWindow> {

    List<LogSingleWindow> singleWindowList(String domainId, String domainType);
}
