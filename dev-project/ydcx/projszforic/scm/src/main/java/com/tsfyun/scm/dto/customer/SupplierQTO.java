package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * 供应商查询实体
 */
@Data
public class SupplierQTO extends PaginationDto implements Serializable {

    private String customerName;

    private String name;

    //启用禁用
    private Boolean disabled;

}
