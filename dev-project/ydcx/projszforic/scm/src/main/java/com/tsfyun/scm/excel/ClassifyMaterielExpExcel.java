package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.util.StringUtils;
import lombok.Data;

import java.io.Serializable;

@Data
public class ClassifyMaterielExpExcel extends BaseRowModel implements Serializable {


    @NotEmptyTrim(message = "物料型号不能为空")
    @LengthTrim(max = 80,message = "物料型号最大长度不能超过80位")
    @ExcelProperty(value = "物料型号", index = 0)
    private String model;

    @NotEmptyTrim(message = "物料品牌不能为空")
    @LengthTrim(max = 80,message = "物料品牌最大长度不能超过80位")
    @ExcelProperty(value = "物料品牌", index = 1)
    private String brand;

    @NotEmptyTrim(message = "物料名称不能为空")
    @ExcelProperty(value = "物料名称", index = 2)
    @LengthTrim(max = 80,message = "物料名称最大长度不能超过80位")
    private String name;

    @NotEmptyTrim(message = "海关编码不能为空")
    @ExcelProperty(value = "海关编码", index = 3)
    @LengthTrim(min=10,max = 10,message = "海关编码必须10位")
    private String hsCode;

    @NotEmptyTrim(message = "物料规格描述不能为空")
    @ExcelProperty(value = "物料规格描述", index = 4)
    @LengthTrim(max = 500,message = "物料规格描述最大长度不能超过500位")
    private String spec;

    @NotEmptyTrim(message = "申报要素不能为空")
    @ExcelProperty(value = "申报要素", index = 5)
    @LengthTrim(max = 255,message = "申报要素最大长度不能超过255位")
    private String elements;

    @ExcelProperty(value = "归类备注", index = 6)
    @LengthTrim(max = 255,message = "归类备注最大长度不能超过255位")
    private String memo;

    private Long personId;//操作人ID
    private String personName;//操作人名称


//    public String getModel(){
//        return StringUtils.removeSpecialSymbol(this.model).replace("型号","").replace("型","");
//    }
//    public String getBrand(){
//        return StringUtils.removeSpecialSymbol(this.brand).replace("品牌","").replace("牌","").toUpperCase();
//    }
    public String getName(){
        return StringUtils.removeSpecialSymbol(this.name);
    }
    public String getHsCode(){
        return StringUtils.removeSpecialSymbol(this.hsCode);
    }
    public String getSpec(){
        return StringUtils.removeSpecialSymbol(this.spec);
    }
    public String getElements(){
        return StringUtils.removeSpecialSymbol(this.elements);
    }
    public String getMemo(){
        return StringUtils.removeSpecialSymbol(this.memo);
    }

}

