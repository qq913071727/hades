package com.tsfyun.scm.controller.system;


import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.dto.system.SubjectDTO;
import com.tsfyun.scm.service.system.ISubjectService;
import com.tsfyun.scm.vo.system.SubjectVO;
import com.tsfyun.scm.vo.system.SubjectVersionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-03-03
 */
@RestController
@RequestMapping("/subject")
public class SubjectController extends BaseController {

    @Autowired
    private ISubjectService subjectService;

    /**
     * 获取主体详细详细
     * @return
     */
    @GetMapping("detail")
    public Result<SubjectVO> detail() {
        return success(subjectService.findByCode());
    }

    /**
     * 修改主体信息
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Valid SubjectDTO dto) {
        subjectService.update(dto);
        return success();
    }

    /**=
     * 企业版本信息
     * @return
     */
    @PostMapping(value = "version")
    Result<SubjectVersionVO> version(){
        return success(subjectService.version());
    }
}

