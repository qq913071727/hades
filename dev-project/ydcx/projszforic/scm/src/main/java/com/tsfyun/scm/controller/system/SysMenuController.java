package com.tsfyun.scm.controller.system;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.service.system.ISysMenuService;
import com.tsfyun.scm.util.UITree;
import com.tsfyun.scm.vo.system.SimpleSysMenuVO;
import com.tsfyun.scm.vo.system.SysMenuVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/menu")
public class SysMenuController extends BaseController {

    @Autowired
    private ISysMenuService sysMenuService;

    /**
     * 获取用户后台导航菜单（已过滤删除菜单）
     * @return
     */
    @GetMapping(value = "/userBgMenus")
    Result<List<UITree>> userBgMenus() {
        return success(sysMenuService.getBackgroundUserMenu());
    }

    /**
     * 获取用户后台导航菜单（已过滤删除菜单、指定版块）
     * @return
     */
    @GetMapping(value = "/userBgMenusSection")
    Result<List<UITree>> userBgMenusSection(@RequestParam(value = "sectionModule") String sectionModule) {
        return success(sysMenuService.getSectionBackgroundUserMenu(sectionModule));
    }


    /**
     * 获取用户后台按钮菜单（已过滤删除菜单）
     * @return
     */
    @GetMapping(value = "/userOperationMenus")
    Result<List<SysMenuVO>> userOperationMenus(@RequestParam(value = "id")String id) {
        return success(sysMenuService.getButtonMenusByMenuId(id));
    }


    /**
     * 获取所有的菜单（包含所有的菜单类型）
     * @return
     */
    @GetMapping(value = "/getAllTypeMenu")
    Result<List<SimpleSysMenuVO>> getAllTypeMenu( ) {
        return success(sysMenuService.getAllTypeMenu());
    }

    /**
     * 普通租户不能删除、新增、修改菜单，只能由运营管理平台处理
     */

}
