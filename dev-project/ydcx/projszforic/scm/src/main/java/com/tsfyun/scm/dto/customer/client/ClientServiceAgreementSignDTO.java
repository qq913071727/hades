package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 客户同意签署
 */
@Data
public class ClientServiceAgreementSignDTO implements Serializable {

    //框架协议ID
    private Long id;
    //协议编号
    private String docNo;
    //报价单主单ID
    private Long agreementId;


//    @NotEmptyTrim(message = "电话不能为空")
//    @LengthTrim(max = 13,message = "电话最大长度不能超过13位")
//    private String partyaTel;
//
//    @NotEmptyTrim(message = "税单类型不能为空")
//    @EnumCheck(clazz = BusinessTypeEnum.class,message = "税单类型错误")
//    private String businessType;

//    @NotEmptyTrim(message = "授权联系人")
    @LengthTrim(max = 20,message = "授权联系人最大长度不能超过20位")
    private String partyaLinkPerson;
    @LengthTrim(max = 50,message = "电子邮箱最大长度不能超过50位")
//    @NotEmptyTrim(message = "电子邮箱")
    private String partyaMail;

}
