package com.tsfyun.scm.controller.customer;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.CustomerAbroadDeliveryInfoDTO;
import com.tsfyun.scm.service.customer.ICustomerAbroadDeliveryInfoService;
import com.tsfyun.scm.vo.customer.CustomerAbroadDeliveryInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 客户境外送货地址 前端控制器
 * </p>
 *
 *
 * @since 2021-09-16
 */
@RestController
@RequestMapping("/customerAbroadDeliveryInfo")
public class CustomerAbroadDeliveryInfoController extends BaseController {


    @Autowired
    private ICustomerAbroadDeliveryInfoService customerAbroadDeliveryInfoService;


    /**
     * 新增
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) CustomerAbroadDeliveryInfoDTO dto) {
        customerAbroadDeliveryInfoService.add(dto,true);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) CustomerAbroadDeliveryInfoDTO dto) {
        customerAbroadDeliveryInfoService.edit(dto,true);
        return success();
    }

    /**
     * 激活禁用
     * @param id
     * @return
     */
    @PostMapping(value = "activeDisable")
    Result<Void> activeDisable(@RequestParam(value = "id")Long id) {
        customerAbroadDeliveryInfoService.activeDisable(id);
        return success();
    }

    /**
     * 设置默认
     * @param id
     * @return
     */
    @PostMapping(value = "setDefault")
    Result<Void> setDefault(@RequestParam(value = "id")Long id) {
        customerAbroadDeliveryInfoService.setDefault(id);
        return success();
    }

    /**
     * 获取客户收货地址（所有的客户收货地址，不分页）
     * @param customerId
     * @return
     */
    @GetMapping(value = "select")
    Result<List<CustomerAbroadDeliveryInfoVO>> select(@RequestParam(value = "customerId")Long customerId) {
        return success(customerAbroadDeliveryInfoService.select(customerId));
    }

    /**
     * 带权限分页查询客户收货地址信息（查询条件customerName-客户名称;companyName-收货公司;linkPerson-联系人;linkTel-联系电话)
     * @param dto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<CustomerAbroadDeliveryInfoVO>> page(CustomerAbroadDeliveryInfoDTO dto) {
        PageInfo<CustomerAbroadDeliveryInfoVO> page = customerAbroadDeliveryInfoService.pageList(dto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 获取客户地址详细信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<CustomerAbroadDeliveryInfoVO> detail(@RequestParam(value = "id")Long id) {
        return success(customerAbroadDeliveryInfoService.detail(id));
    }

    /**
     * 删除客户收货信息（真删，不做数据关联校验）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        customerAbroadDeliveryInfoService.deleteById(id);
        return success();
    }

    /**
     * 根据客户名称查询有效的收货地址
     * @return
     */
    @PostMapping(value = "effectiveList")
    Result<List<CustomerAbroadDeliveryInfoVO>> effectiveList(@RequestParam(value = "customerName")String customerName) {
        return success(customerAbroadDeliveryInfoService.effectiveList(customerName));
    }
}

