package com.tsfyun.scm.service.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.system.SubjectBankDTO;
import com.tsfyun.scm.dto.system.SubjectBankQTO;
import com.tsfyun.scm.entity.system.SubjectBank;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.SubjectBankVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-03-20
 */
public interface ISubjectBankService extends IService<SubjectBank> {

    void add(SubjectBankDTO dto);

    void edit(SubjectBankDTO dto);

    PageInfo<SubjectBankVO> pageList(SubjectBankQTO qto);

    SubjectBankVO detail(Long id);

    void updateDisabled(Long id,Boolean disabled);

    void remove(Long id);

    /**
     * 获取企业启用的收款行下拉
     * @return
     */
    List<SubjectBankVO> selectEnableBanks();

    /**=
     * 客户端收款行信息
     * @param subjectId
     * @return
     */
    List<SubjectBankVO> selectReceivablesBanks(Long subjectId);
}
