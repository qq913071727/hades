package com.tsfyun.scm.service.impl.finance;

import com.tsfyun.scm.entity.finance.PurchaseContractMember;
import com.tsfyun.scm.mapper.finance.PurchaseContractMemberMapper;
import com.tsfyun.scm.service.finance.IPurchaseContractMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * <p>
 * 采购合同明细 服务实现类
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Service
public class PurchaseContractMemberServiceImpl extends ServiceImpl<PurchaseContractMember> implements IPurchaseContractMemberService {

    @Autowired
    private PurchaseContractMemberMapper purchaseContractMemberMapper;

    @Override
    public List<PurchaseContractMember> findByPurchaseContractId(Long purchaseContractId) {
        return purchaseContractMemberMapper.findByPurchaseContractId(purchaseContractId);
    }

    @Override
    public void removeByContractId(Long purchaseContractId) {
        purchaseContractMemberMapper.deleteByExample(Example.builder(PurchaseContractMember.class)
                .where(TsfWeekendSqls.<PurchaseContractMember>custom()
                        .andEqualTo(false,PurchaseContractMember::getPurchaseContractId,purchaseContractId)).build());
    }
}
