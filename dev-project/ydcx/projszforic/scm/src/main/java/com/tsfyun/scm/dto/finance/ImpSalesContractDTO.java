package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 销售合同请求实体
 * </p>
 *
 * @since 2020-05-19
 */
@Data
@ApiModel(value="ImpSalesContract请求对象", description="销售合同请求实体")
public class ImpSalesContractDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "系统单号不能为空")
   @LengthTrim(max = 25,message = "系统单号最大长度不能超过25位")
   @ApiModelProperty(value = "系统单号")
   private String docNo;

   private String statusId;

   @LengthTrim(max = 20,message = "发票编号最大长度不能超过20位")
   @ApiModelProperty(value = "发票编号")
   private String invoiceNo;

   @NotNull(message = "合同日期不能为空")
   @ApiModelProperty(value = "合同日期")
   private LocalDateTime contractDate;

   @NotNull(message = "订单id不能为空")
   @ApiModelProperty(value = "订单id")
   private Long impOrderId;

   @NotNull(message = "购货单位（客户）不能为空")
   @ApiModelProperty(value = "购货单位（客户）")
   private Long buyerId;

   @LengthTrim(max = 200,message = "购货单位名称最大长度不能超过200位")
   @ApiModelProperty(value = "购货单位名称")
   private String buyerName;

   @LengthTrim(max = 50,message = "买方银行名称最大长度不能超过50位")
   @ApiModelProperty(value = "买方银行名称")
   private String buyerBankName;

   @LengthTrim(max = 18,message = "买方纳税人识别号最大长度不能超过18位")
   @ApiModelProperty(value = "买方纳税人识别号")
   private String buyerTaxpayerNo;

   @LengthTrim(max = 50,message = "买方银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "买方银行账号")
   private String buyerBankAccount;

   @LengthTrim(max = 255,message = "买方银行地址最大长度不能超过255位")
   @ApiModelProperty(value = "买方银行地址")
   private String buyerBankAddress;

   @LengthTrim(max = 50,message = "买方联系电话最大长度不能超过50位")
   @ApiModelProperty(value = "买方联系电话")
   private String buyerLinkPerson;

   @LengthTrim(max = 50,message = "买方银行电话最大长度不能超过50位")
   @ApiModelProperty(value = "买方银行电话")
   private String buyerBankTel;

   @NotNull(message = "卖方不能为空")
   @ApiModelProperty(value = "卖方")
   private Long sellerId;

   @LengthTrim(max = 64,message = "卖方银行名称最大长度不能超过64位")
   @ApiModelProperty(value = "卖方银行名称")
   private String sellerBankName;

   @LengthTrim(max = 50,message = "卖方银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "卖方银行账号")
   private String sellerBankAccount;

   @LengthTrim(max = 255,message = "卖方银行地址最大长度不能超过255位")
   @ApiModelProperty(value = "卖方银行地址")
   private String sellerBankAddress;

   @Digits(integer = 17, fraction = 2, message = "合同金额整数位不能超过17位，小数位不能超过2位")
   @ApiModelProperty(value = "合同金额")
   private BigDecimal totalPrice;

   @Digits(integer = 17, fraction = 2, message = "货款金额整数位不能超过17位，小数位不能超过2位")
   @ApiModelProperty(value = "货款金额")
   private BigDecimal goodsVal;

   @Digits(integer = 17, fraction = 2, message = "票差调整金额整数位不能超过17位，小数位不能超过2位")
   @ApiModelProperty(value = "票差调整金额")
   private BigDecimal differenceVal;

   private String memo;


}
