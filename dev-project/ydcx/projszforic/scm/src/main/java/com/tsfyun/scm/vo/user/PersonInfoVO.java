package com.tsfyun.scm.vo.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.scm.vo.system.SysRoleVO;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class PersonInfoVO implements Serializable {

    private Long id;

    /**
     * 用户 唯一编号，非登录账号
     */
    private String code;

    /**
     * 员工名称
     */
    private String name;


    /**
     * 邮箱
     */
    private String mail;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 头像
     */
    private String head;


    /**
     * 部门id
     */
    private Long departmentId;

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 创建人   创建人personId/创建人名称
     */
    @JsonIgnore
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @JsonIgnore
    private String updateBy;

    /**
     * 修改时间
     */
    @JsonIgnore
    private LocalDateTime dateUpdated;

    private List<SysRoleVO> roles;

    /**
     * 租户名称
     */
    private String tenantName;

    /**
     * 职位
     */
    private String position;

    private String positionName;

    /**
     * 在职状态
     */
    private Boolean incumbency;

    private String incumbencyName;

    /**
     * 性别，见枚举SexTypeEnum
     */
    private String gender;

    private String genderName;

    /**
     * 备注
     */
    private String memo;

    /**
     * 用户角色，按逗号隔开
     */
    private String roleNames;

    /***
     * 是否修改了密码
     */
    private Boolean isChangedPwd;

    /**
     * 是否删除
     */
    private Boolean isDelete;

    /**
     * 公司名称
     */
    private String customerName;

}
