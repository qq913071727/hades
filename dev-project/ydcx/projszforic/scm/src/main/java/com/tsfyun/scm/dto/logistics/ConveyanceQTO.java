package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

@Data
public class ConveyanceQTO extends PaginationDto implements Serializable {

    private Long transportSupplierId;

    private String conveyanceNo;

    private String conveyanceNo2;

    private String driver;

}
