package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.finance.*;
import com.tsfyun.scm.service.finance.IDrawbackService;
import com.tsfyun.scm.vo.finance.CreateDrawbackPlusVO;
import com.tsfyun.scm.vo.finance.DrawbackPlusVO;
import com.tsfyun.scm.vo.finance.DrawbackVO;
import com.tsfyun.scm.vo.finance.PrintDrawBackVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 退税单 前端控制器
 * </p>
 *
 *
 * @since 2021-10-20
 */
@RestController
@RequestMapping("/drawback")
public class DrawbackController extends BaseController {

    @Autowired
    private IDrawbackService drawbackService;

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<DrawbackVO>> pageList(@ModelAttribute @Valid DrawbackQTO qto) {
        PageInfo<DrawbackVO> page = drawbackService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<DrawbackPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation) {
        return success(drawbackService.detail(id,operation));
    }

    /**
     * 重新计算退税比例
     * @param dto
     * @return
     */
    @PostMapping(value = "recalculate")
    public Result<CreateDrawbackPlusVO> recalculate(@ModelAttribute @Validated(UpdateGroup.class) DrawbackDTO dto){
        return success(drawbackService.recalculate(dto));
    }

    /**
     * 保存退税
     * @return
     */
    @PostMapping(value = "save")
    public Result<Void> save(@RequestBody @Validated(AddGroup.class) DrawbackDTO dto){
        drawbackService.saveDrawback(dto);
        return success();
    }

    /**
     * 退税单确认
     * @return
     */
    @PostMapping(value = "confirm")
    @DuplicateSubmit
    public Result<Void> confirm(@ModelAttribute @Valid TaskDTO dto){
        drawbackService.confirm(dto);
        return success();
    }

    /**
     * 退回付款单确认
     * @return
     */
    @PostMapping(value = "backConfirm")
    @DuplicateSubmit
    public Result<Void> backConfirm(@ModelAttribute @Valid TaskDTO dto){
        drawbackService.backConfirm(dto);
        return success();
    }

    /**
     * 退回重新确定付款行
     * @return
     */
    @PostMapping(value = "backConfirmBank")
    @DuplicateSubmit
    public Result<Void> backConfirmBank(@ModelAttribute @Valid TaskDTO dto){
        drawbackService.backConfirmBank(dto);
        return success();
    }

    /**
     * 确定付款银行
     * @param dto
     * @return
     */
    @PostMapping(value = "confirmBank")
    @DuplicateSubmit
    public Result<Void> confirmBank(@ModelAttribute @Valid ExpConfirmBankDTO dto){
        drawbackService.confirmBank(dto);
        return success();
    }

    /**
     * 财务确定付款完成
     * @param dto
     * @return
     */
    @PostMapping(value = "paymentCompleted")
    @DuplicateSubmit
    public Result<Void> paymentCompleted(@ModelAttribute @Valid ExpPaymentCompletedDTO dto){
        drawbackService.paymentCompleted(dto);
        return success();
    }

    /**
     * 删除
     * @return
     */
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")Long id){
        drawbackService.removeDrawback(id);
        return success();
    }

    /**
     * 获取付款申请打印数据
     * @param id
     * @return
     */
    @GetMapping(value = "getPaymentPrintData")
    public Result<PrintDrawBackVO> getPaymentPrintData(@RequestParam(value = "id")Long id){
        return success(drawbackService.getPaymentPrintData(id));
    }
}

