package com.tsfyun.scm.vo.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
public class StatusHistoryVO implements Serializable {

    private String domainId;
    private String operationCode;
    private String operationName;
    private String createBy;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime dateCreated;
    private String originalStatusId;
    private String originalStatusName;
    private String nowStatusId;
    private String nowStausName;
    private String memo;

    public String getOperationName(){
        DomainOprationEnum oprationEnum = DomainOprationEnum.of(this.operationCode);
        return Objects.nonNull(oprationEnum)?oprationEnum.getName():"";
    }
}
