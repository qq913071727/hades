package com.tsfyun.scm.mapper.user;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.user.CustomerPerson;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-09-15
 */
@Repository
public interface CustomerPersonMapper extends Mapper<CustomerPerson> {
    /**
     * 根据人员取消其他默认值
     * @param personId
     * @param id
     */
    void updateCustomerPersonDefault(@Param(value = "personId")Long personId,@Param(value = "id")Long id);

    /**
     * 根据客户ID和人员ID查询
     * @param customerId
     * @param personId
     * @return
     */
    CustomerPerson findByCustomerIdAndPersonId(@Param(value = "customerId")Long customerId,@Param(value = "personId")Long personId);
}
