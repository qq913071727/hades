package com.tsfyun.scm.mapper.customs;

import com.tsfyun.scm.entity.customs.DeclarationMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.customs.DeclarationMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface DeclarationMemberMapper extends Mapper<DeclarationMember> {

    Integer removeByDeclarationId(@Param(value = "dclarationId") Long dclarationId);
}
