package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色对应的菜单
 */
@Data
public class SysRoleMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String roleId;

    private String menuId;




}