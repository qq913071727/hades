package com.tsfyun.scm.vo.finance.client;

import com.tsfyun.scm.dto.finance.PaymentAccountDTO;
import com.tsfyun.scm.dto.finance.PaymentOrderDTO;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 客户端订单提交付汇申请
 * @CreateDate: Created in 2020/11/5 10:24
 */
@Data
public class ClientPaymentApplyDTO implements Serializable {

    @NotNull(message = "请填写付汇信息")
    @Valid
    private PaymentAccountDTO payment;


    @NotNull(message = "请选择订单信息")
    @Valid
    private PaymentOrderDTO order;

}
