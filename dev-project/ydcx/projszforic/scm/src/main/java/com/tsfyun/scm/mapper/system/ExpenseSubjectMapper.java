package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.dto.system.ExpenseSubjectDTO;
import com.tsfyun.scm.entity.system.ExpenseSubject;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.common.base.vo.ExpenseSubjectVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-16
 */
@Repository
public interface ExpenseSubjectMapper extends Mapper<ExpenseSubject> {

    /**
     * 修改
     * @param dto
     */
    void update(@Param("dto") ExpenseSubjectDTO dto, @Param("userIdAndName") String userIdAndName);

    /**
     * 查询
     * @param dto
     * @return
     */
    List<ExpenseSubjectVO> list(ExpenseSubjectDTO dto);
}
