package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.exp.ExpTransactionModeEnum;
import com.tsfyun.common.base.enums.exp.SettlementTypeEnum;
import com.tsfyun.common.base.enums.exp.TaxRefundEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 出口订单请求实体
 * </p>
 *
 *
 * @since 2021-01-26
 */
@Data
@ApiModel(value="ExpOrder请求对象", description="出口订单请求实体")
public class ExpOrderDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   @NotEmptyTrim(message = "订单编号不能为空")
   @LengthTrim(max = 20,message = "订单编号最大长度不能超过20位")
   @ApiModelProperty(value = "订单编号")
   private String docNo;

   //@NotEmptyTrim(message = "客户单号不能为空")
   @LengthTrim(max = 20,message = "客户单号最大长度不能超过20位")
   @ApiModelProperty(value = "客户单号")
   private String clientNo;

   @ApiModelProperty(value = "客户")
   private Long customerId;

   @NotEmptyTrim(message = "请先选择客户")
   @ApiModelProperty(value = "客户")
   private String customerName;

   @NotEmptyTrim(message = "请选择协议报价")
   @ApiModelProperty(value = "协议报价")
   private Long agreementId;

   //@NotEmptyTrim(message = "境外客户不能为空")
   @ApiModelProperty(value = "境外客户")
   private String supplierName;

   @LengthTrim(max = 10,message = "币制最大长度不能超过10位")
   @ApiModelProperty(value = "币制")
   private String currencyName;

   @NotEmptyTrim(message = "报关类型不能为空")
   @LengthTrim(max = 10,message = "报关类型最大长度不能超过10位")
   @EnumCheck(clazz = DeclareTypeEnum.class,message = "报关类型错误")
   private String declareType;

   @NotEmptyTrim(message = "运输方式代码不能为空")
   @LengthTrim(max = 2,message = "运输方式代码最大长度不能超过2位")
   @ApiModelProperty(value = "运输方式")
   private String cusTrafMode;

   @NotEmptyTrim(message = "成交方式不能为空")
   @LengthTrim(max = 20,message = "成交方式最大长度不能超过20位")
   @EnumCheck(clazz = ExpTransactionModeEnum.class,message = "成交方式错误")
   @ApiModelProperty(value = "成交方式")
   private String transactionMode;

   @LengthTrim(max = 5,message = "境内货源地代码最大长度不能超过5位")
   @ApiModelProperty(value = "境内货源地代码")
   private String districtCode;

   @LengthTrim(max = 40,message = "境内货源地名称最大长度不能超过40位")
   @ApiModelProperty(value = "境内货源地名称")
   private String districtCodeName;

   @LengthTrim(max = 3,message = "运抵国(地区)编码最大长度不能超过3位")
   @ApiModelProperty(value = "运抵国(地区)编码")
   private String cusTradeCountry;

   @LengthTrim(max = 20,message = "运抵国(地区)名称最大长度不能超过20位")
   @ApiModelProperty(value = "运抵国(地区)名称")
   private String cusTradeCountryName;

   @LengthTrim(max = 3,message = "贸易国别(地区)编码最大长度不能超过3位")
   @ApiModelProperty(value = "贸易国别(地区)编码")
   private String cusTradeNationCode;

   @LengthTrim(max = 20,message = "贸易国别(地区)名称最大长度不能超过20位")
   @ApiModelProperty(value = "贸易国别(地区)名称")
   private String cusTradeNationCodeName;

   @LengthTrim(max = 8,message = "指运港代码最大长度不能超过8位")
   @ApiModelProperty(value = "指运港代码")
   private String distinatePort;

   @LengthTrim(max = 20,message = "指运港名称最大长度不能超过20位")
   @ApiModelProperty(value = "指运港名称")
   private String distinatePortName;

   @LengthTrim(max = 3,message = "最终目的国(地区)编码最大长度不能超过3位")
   @ApiModelProperty(value = "最终目的国(地区)编码")
   private String destinationCountry;

   @LengthTrim(max = 20,message = "最终目的国(地区)名称最大长度不能超过20位")
   @ApiModelProperty(value = "最终目的国(地区)名称")
   private String destinationCountryName;

   @ApiModelProperty(value = "外贸合同卖方")
   @NotNull(message = "请选择外贸合同卖方")
   private Long subjectId;

   @ApiModelProperty(value = "外贸合同买方")
   private Long subjectOverseasId;

   @NotEmptyTrim(message = "请选择外贸合同买方")
   @Pattern(regexp = "1|2",message = "外贸合同买方类型错误")
   private String subjectOverseasType;

   @LengthTrim(max = 100,message = "外贸合同买方最大长度不能超过100位")
   @ApiModelProperty(value = "外贸合同买方")
   private String subjectOverseasName;

   @NotNull(message = "订单日期不能为为空")
   @ApiModelProperty(value = "订单日期")
   private Date orderDate;

   @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
   @ApiModelProperty(value = "备注")
   private String memo;


}
