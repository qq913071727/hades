package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import com.tsfyun.common.base.util.jaxb.CDataAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @Description: 商品项描述
 * @since Created in 2020/4/22 11:39
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Commodity", propOrder = {
        "cargoDescription",
        "description",
        "hsCode",
        "classification",
})
public class Commodity {

    //商品项简要信息描述
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "CargoDescription")
    protected String cargoDescription;

    //商品项补充信息描述
    @XmlJavaTypeAdapter(value= CDataAdapter.class)
    @XmlElement(name = "Description")
    protected String description;

    //商品HS编码
    @XmlElement(name = "HSCode")
    protected String hsCode;

    //危险品编号
    @XmlElement(name = "Classification")
    protected Classification classification;


    public Commodity( ) {

    }


    public Commodity(String cargoDescription) {
        this.cargoDescription = cargoDescription;
    }

    public String getCargoDescription() {
        return cargoDescription;
    }

    public void setCargoDescription(String cargoDescription) {
        this.cargoDescription = cargoDescription;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }
}
