package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.tsfyun.scm.entity.order.OrderMemberReceivingNo;
import com.tsfyun.scm.mapper.order.OrderMemberReceivingNoMapper;
import com.tsfyun.scm.service.order.IOrderMemberReceivingNoService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.order.OrderMemberReceivingNoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 绑定入库单辅助表 服务实现类
 * </p>
 *
 *
 * @since 2020-05-06
 */
@Service
public class OrderMemberReceivingNoServiceImpl extends ServiceImpl<OrderMemberReceivingNo> implements IOrderMemberReceivingNoService {

    @Autowired
    private OrderMemberReceivingNoMapper orderMemberReceivingNoMapper;

    @Override
    public List<OrderMemberReceivingNoVO> getByMemberId(Long orderMemberId) {
        OrderMemberReceivingNo query = new OrderMemberReceivingNo();
        query.setOrderMemberId(orderMemberId);
        List<OrderMemberReceivingNo> list = orderMemberReceivingNoMapper.select(query);
        if(CollUtil.isNotEmpty(list)){
            return beanMapper.mapAsList(list,OrderMemberReceivingNoVO.class);
        }
        return Lists.newArrayList();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeByOrderMemberId(Long memberId) {
        OrderMemberReceivingNo delete = new OrderMemberReceivingNo();
        delete.setOrderMemberId(memberId);
        orderMemberReceivingNoMapper.delete(delete);
    }

    @Override
    public void removeByOrderMemberIds(List<Long> memberIds) {
        orderMemberReceivingNoMapper.removeByOrderMemberIds(memberIds);
    }

    @Override
    public List<OrderMemberReceivingNo> findByOrderId(Long orderId) {
        return orderMemberReceivingNoMapper.findByOrderId(orderId);
    }
}
