package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.dto.finance.ClaimedAmountPlusDTO;
import com.tsfyun.scm.dto.finance.ConfirmOverseasReceivingAccountDTO;
import com.tsfyun.scm.dto.finance.OverseasReceivingAccountDTO;
import com.tsfyun.scm.dto.finance.OverseasReceivingAccountQTO;
import com.tsfyun.scm.entity.system.Subject;
import com.tsfyun.scm.service.finance.IOverseasReceivingAccountService;
import com.tsfyun.scm.vo.finance.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 境外收款 前端控制器
 * </p>
 *
 *
 * @since 2021-10-08
 */
@RestController
@RequestMapping("/overseasReceivingAccount")
public class OverseasReceivingAccountController extends BaseController {

    private final IOverseasReceivingAccountService overseasReceivingAccountService;

    public OverseasReceivingAccountController(IOverseasReceivingAccountService overseasReceivingAccountService) {
        this.overseasReceivingAccountService = overseasReceivingAccountService;
    }

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<OverseasReceivingAccountVO>> pageList(@ModelAttribute @Valid OverseasReceivingAccountQTO qto) {
        PageInfo<OverseasReceivingAccountVO> page = overseasReceivingAccountService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 详情
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<OverseasReceivingAccountPlusVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation")String operation) {
        return success(overseasReceivingAccountService.detail(id,operation));
    }

    /**
     * 境外收款登记
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    public Result<Void> add(@ModelAttribute @Validated OverseasReceivingAccountDTO dto) {
        overseasReceivingAccountService.add(dto);
        return success( );
    }

    /**
     * 境外收款修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    public Result<Void> edit(@ModelAttribute @Validated OverseasReceivingAccountDTO dto) {
        overseasReceivingAccountService.edit(dto);
        return success( );
    }

    /**=
     * 收款单确认
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirm")
    public Result<Void> confirm(@ModelAttribute @Valid ConfirmOverseasReceivingAccountDTO dto){
        overseasReceivingAccountService.confirm(dto);
        return success();
    }

    /**
     * 收款单认领
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "claim")
    public Result<Void> claim(@RequestBody @Valid ClaimedAmountPlusDTO dto){
        overseasReceivingAccountService.claim(dto);
        return success();
    }

    /**
     * 清空收款单认领数据
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "clearClaimOrder")
    public Result<Void> clearClaimOrder(@RequestParam(value = "id")Long id){
        overseasReceivingAccountService.clearClaimOrder(id, DomainOprationEnum.OVERSEAS_RECEIVING_ACCOUNT_CLAIM);
        return success();
    }

    /**
     * 作废
     * @param dto
     * @return
     */
    @PostMapping(value = "toVoid")
    public Result<Void> toVoid(@ModelAttribute @Valid TaskDTO dto){
        overseasReceivingAccountService.toVoid(dto);
        return success();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")Long id) {
        overseasReceivingAccountService.removeAllById(id);
        return success();
    }

    /**
     * 获取主体
     * @param first
     * @return
     */
    @GetMapping(value = "subjectSelect")
    public Result<List<SubjectTypeVO>> subjectSelect(@RequestParam(value = "first",required = false,defaultValue = "abroad")String first){
        return success(overseasReceivingAccountService.subjectSelect(first));
    }

    /**
     * 根据收款单初始化结汇单
     * @return
     */
    @GetMapping(value = "initSettlement")
    public Result<SettlementPlusVO> initSettlement(@RequestParam(value = "id")String id){
        return success(overseasReceivingAccountService.initSettlement(StringUtils.stringToLongs(id)));
    }
    /**
     * 根据收款单初始化付款单
     * @return
     */
    @GetMapping(value = "initPayment")
    public Result<CreatePaymentVO> initPayment(@RequestParam(value = "id")Long id){
        return success(overseasReceivingAccountService.initPayment(id));
    }

    /**
     * 根据收款单编号获取订单合并信息
     * @param accountNos
     * @return
     */
    @PostMapping(value = "overseasReceivings")
    public Result<List<OverseasReceivingOrderVO>> overseasReceivings(@RequestParam(value = "accountNos")String accountNos){
        return success(overseasReceivingAccountService.overseasReceivings(Arrays.asList(accountNos.split(","))));
    }

}

