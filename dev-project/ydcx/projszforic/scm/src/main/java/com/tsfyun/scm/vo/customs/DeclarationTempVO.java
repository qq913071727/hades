package com.tsfyun.scm.vo.customs;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 报关单模板响应实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeclarationTemp响应对象", description="报关单模板响应实体")
public class DeclarationTempVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "模板名称")
    private String tempName;

    @ApiModelProperty(value = "单据类型-进出口")
    private String billType;

    private String billTypeName;

    public String getBillTypeName(){
        BillTypeEnum billTypeEnum = BillTypeEnum.of(getBillType());
        return Objects.nonNull(billTypeEnum)?billTypeEnum.getName():"";
    }

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "是否默认")
    private Boolean isDefault;

    @ApiModelProperty(value = "运输方式")
    private String cusTrafModeCode;

    @ApiModelProperty(value = "运输方式")
    private String cusTrafModeName;

    @ApiModelProperty(value = "监管方式")
    private String supvModeCdde;

    @ApiModelProperty(value = "监管方式")
    private String supvModeCddeName;

    @ApiModelProperty(value = "征免性质")
    private String cutModeCode;

    @ApiModelProperty(value = "征免性质")
    private String cutModeName;

    @ApiModelProperty(value = "启运国(地区)")
    private String cusTradeCountryCode;

    @ApiModelProperty(value = "启运国(地区)")
    private String cusTradeCountryName;

    @ApiModelProperty(value = "经停港")
    private String distinatePortCode;

    @ApiModelProperty(value = "经停港")
    private String distinatePortName;

    @ApiModelProperty(value = "申报地海关")
    private String customMasterCode;

    @ApiModelProperty(value = "申报地海关")
    private String customMasterName;

    @ApiModelProperty(value = "包装种类")
    private String wrapTypeCode;

    @ApiModelProperty(value = "包装种类")
    private String wrapTypeName;

    @ApiModelProperty(value = "贸易国别(地区)")
    private String cusTradeNationCode;

    @ApiModelProperty(value = "贸易国别(地区)")
    private String cusTradeNationName;

    @ApiModelProperty(value = "入境口岸")
    private String ciqEntyPortCode;

    @ApiModelProperty(value = "入境口岸")
    private String ciqEntyPortName;

    @ApiModelProperty(value = "货物存放地")
    private String goodsPlace;

    @ApiModelProperty(value = "启运港")
    private String destPortCode;

    @ApiModelProperty(value = "启运港")
    private String destPortName;

    @ApiModelProperty(value = "征免方式	")
    private String dutyMode;

    @ApiModelProperty(value = "征免方式	")
    private String dutyModeName;

    @ApiModelProperty(value = "境内目的地")
    private String districtCode;

    @ApiModelProperty(value = "境内目的地")
    private String districtName;

    @ApiModelProperty(value = "目的地代码")
    private String ciqDestCode;

    @ApiModelProperty(value = "目的地代码")
    private String ciqDestName;

    @ApiModelProperty(value = "进境关别")
    private String iePortCode;

    @ApiModelProperty(value = "进境关别")
    private String iePortName;

    @ApiModelProperty(value = "标记唛码")
    private String markNo;

    @ApiModelProperty(value = "最终目的国")
    private String destinationCountry;

    @ApiModelProperty(value = "最终目的国")
    private String destinationCountryName;


    private String updateBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateUpdated;

}
