package com.tsfyun.scm.vo.finance;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OverseasReceivingAccountPlusVO implements Serializable {

    private OverseasReceivingAccountVO account;

    private List<OverseasReceivingOrderVO> orders;
}
