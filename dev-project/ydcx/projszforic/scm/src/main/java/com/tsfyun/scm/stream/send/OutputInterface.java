package com.tsfyun.scm.stream.send;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

/**
 * @Description: 消息输出通道定义
 * @CreateDate: Created in 2019/12/16 15:43
 */
@Component
public interface OutputInterface {


    /**
     * demo
     */
    String OUT_DEMO = "scm-demo-output";
    @Output(OUT_DEMO)
    MessageChannel saasDemo();


    //物料分析
    String OUT_SCM_MATERIEL = "scm-materiel-output";
    @Output(OUT_SCM_MATERIEL)
    MessageChannel sendMateriel();


    //归类数据导入(进口)
    String OUT_SCM_CLASSIFY_MATERIEL = "scm-classify-materiel-output";
    @Output(OUT_SCM_CLASSIFY_MATERIEL)
    MessageChannel sendClassifyMateriel();

    //归类数据导入(出口)
    String OUT_SCM_CLASSIFY_MATERIEL_EXP = "scm-classify-materiel-exp-output";
    @Output(OUT_SCM_CLASSIFY_MATERIEL_EXP)
    MessageChannel sendClassifyMaterielExp();


    //费用核销
    String OUT_SCM_COST_WRITE_OFF = "scm-cost-write-off-output";
    @Output(OUT_SCM_COST_WRITE_OFF)
    MessageChannel sendCostWriteOff();

    //发送邮件
    String OUT_SCM_MAIL = "scm-mail-output";
    @Output(OUT_SCM_MAIL)
    MessageChannel sendMail();

    //发送短信
    String OUT_SCM_SMS = "scm-sms-output";
    @Output(OUT_SCM_SMS)
    MessageChannel sendSms();

    //发送微信通知
    String OUT_SCM_WX = "scm-wx-output";
    @Output(OUT_SCM_WX)
    MessageChannel sendWx();
}
