package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.DrawbackStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

/**
 * <p>
 * 退税单响应实体
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Data
@ApiModel(value="Drawback响应对象", description="退税单响应实体")
public class DrawbackVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;
    private String customerName;

    @ApiModelProperty(value = "采购合同号")
    private String purchaseNo;

    @ApiModelProperty(value = "订单号")
    private String orderNo;
    private String clientNo;

    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "退税比例")
    private BigDecimal billie;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "申请时间")
    private LocalDateTime dateCreated;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "要求付款时间")
    private Date claimPaymentDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "实际付款时间")
    private Date actualPaymentDate;

    @ApiModelProperty(value = "付款主体")
    private String subjectType;

    @ApiModelProperty(value = "付款主体")
    private Long subjectId;

    @ApiModelProperty(value = "付款主体")
    private String subjectName;

    @ApiModelProperty(value = "付款银行")
    private String subjectBank;

    @ApiModelProperty(value = "付款银行账号")
    private String subjectBankNo;

    @ApiModelProperty(value = "收款方")
    private String payeeName;

    @ApiModelProperty(value = "收款银行")
    private String payeeBank;

    @ApiModelProperty(value = "收款银行账号")
    private String payeeBankNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    // 是否函调
    private Boolean isCorrespondence;
    // 是否回函
    private String replyStatus;
    // 回函日期
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime replyDate;

    /**
     * 税局退税日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime taxRefundTime;

    /**
     * 税局退税金额
     */

    private BigDecimal taxRefundAmount;

    /**
     * 税局退税备注
     */

    private String taxRefundRemark;

    private String statusDesc;

    public String getStatusDesc() {
        return Optional.of(DrawbackStatusEnum.of(statusId)).map(DrawbackStatusEnum::getName).orElse("");
    }
}
