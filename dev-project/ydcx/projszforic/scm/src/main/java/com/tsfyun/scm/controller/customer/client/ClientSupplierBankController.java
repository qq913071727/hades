package com.tsfyun.scm.controller.customer.client;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.SecurityUtil;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.SupplierBankDTO;
import com.tsfyun.scm.dto.customer.client.ClientSupplierBankQTO;
import com.tsfyun.scm.entity.customer.Supplier;
import com.tsfyun.scm.entity.customer.SupplierBank;
import com.tsfyun.scm.service.base.ICurrencyService;
import com.tsfyun.scm.service.customer.ISupplierBankService;
import com.tsfyun.scm.service.customer.ISupplierService;
import com.tsfyun.scm.vo.base.CurrencyVO;
import com.tsfyun.scm.vo.customer.SupplierBankVO;
import com.tsfyun.scm.vo.customer.client.ClientSupplierBankVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description: 供应商银行
 * @CreateDate: Created in 2020/9/29 18:00
 */
@RestController
@RequestMapping(value = "/client/supplierBank")
@Api(tags = "供应商银行（客户端）")
public class ClientSupplierBankController extends BaseController {

    @Autowired
    private ISupplierBankService supplierBankService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private ISupplierService supplierService;

    @Autowired
    private ICurrencyService currencyService;

    /**
     * 查询客户供应商银行分页信息
     * @param qto
     * @return
     */
    @PostMapping(value = "page")
    @ApiOperation("分页")
    Result<List<ClientSupplierBankVO>> page(@ModelAttribute @Valid ClientSupplierBankQTO qto) {
        PageInfo<SupplierBank> page = supplierBankService.clientPage(qto);
        List<ClientSupplierBankVO> supplierBankVOS = beanMapper.mapAsList(page.getList(),ClientSupplierBankVO.class);
        Map<String,CurrencyVO> currencyMap = currencyService.select().stream().collect(Collectors.toMap(CurrencyVO::getId, Function.identity()));
        Supplier supplier = supplierService.getById(qto.getSupplierId());
        if(!CollectionUtils.isEmpty(supplierBankVOS)) {
            supplierBankVOS.stream().forEach(r -> {
                r.setSupplierName(supplier.getName());
                r.setCurrencyName(Optional.ofNullable(currencyMap.get(r.getCurrencyId())).isPresent() ? currencyMap.get(r.getCurrencyId()).getName() : r.getCurrencyId());
            });
        }
        return success((int)page.getTotal(),supplierBankVOS);
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation("删除")
    public Result<Void> delete(@RequestParam("id")Long id){
        supplierBankService.clientRemove(id);
        return success();
    }

    /**
     * 设置默认
     */
    @PostMapping("/setDefault")
    @ApiOperation("设置默认")
    public Result<Void> setDefault(@RequestParam("id")Long id){
        supplierBankService.setDefault(id);
        return success();
    }

    /**
     * 模糊搜索供应商银行
     * @param supplierName
     * @param bankName
     * @return
     */
    @PostMapping("vague")
    public Result<List<SupplierBankVO>> vague(@RequestParam(value = "supplierName",required = false)String supplierName, @RequestParam(value = "bankName",required = false)String bankName){
        return success(supplierBankService.vague(SecurityUtil.getCurrentCustomerId(),supplierName,bankName));
    }

    @PostMapping("getSupplierBankInfo")
    public Result<SupplierBankVO> getSupplierBankInfo(@RequestParam(value = "supplierName",required = false)String supplierName, @RequestParam(value = "bankName",required = false)String bankName){
        return success(supplierBankService.getSupplierBankInfo(SecurityUtil.getCurrentCustomerId(),supplierName,bankName));
    }

    /**
     * 新增供应商银行信息
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Long> add(@ModelAttribute @Validated(AddGroup.class) SupplierBankDTO dto) {
        return success(supplierBankService.clientAdd(dto));
    }

    /**
     * 修改供应商银行信息
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) SupplierBankDTO dto) {
        supplierBankService.clientEdit(dto);
        return success();
    }

}
