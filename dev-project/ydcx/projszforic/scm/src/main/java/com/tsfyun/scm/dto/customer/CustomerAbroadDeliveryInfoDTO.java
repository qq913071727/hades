package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;


/**
 * <p>
 * 客户境外送货地址请求实体
 * </p>
 *
 *
 * @since 2021-09-16
 */
@Data
@ApiModel(value="CustomerAbroadDeliveryInfo请求对象", description="客户境外送货地址请求实体")
public class CustomerAbroadDeliveryInfoDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   @ApiModelProperty(value = "客户")
   private Long customerId;

   //@NotEmptyTrim(message = "收货公司不能为空")
   @LengthTrim(max = 200,message = "收货公司名称不能大于200位")
   @ApiModelProperty(value = "收货公司")
   private String companyName;

   @NotEmptyTrim(message = "收货收货人不能为空")
   @LengthTrim(max = 50,message = "收货公司名称不能大于50位")
   @ApiModelProperty(value = "联系人")
   private String linkPerson;

   @NotEmptyTrim(message = "收货联系电话不能为空")
   @LengthTrim(max = 50,message = "收货联系电话不能大于50位")
   @ApiModelProperty(value = "联系电话")
   private String linkTel;

   @NotEmptyTrim(message = "收货省份不能为空")
   @ApiModelProperty(value = "省")
   private String provinceName;

   @NotEmptyTrim(message = "收货地址市不能为空")
   @ApiModelProperty(value = "市")
   private String cityName;

   @NotEmptyTrim(message = "收货地址区不能为空")
   @ApiModelProperty(value = "区")
   private String areaName;

   @NotEmptyTrim(message = "收货详细地址不能为空")
   @LengthTrim(max = 255,message = "详细地址不能超过255位")
   @ApiModelProperty(value = "详细地址")
   private String address;

   @ApiModelProperty(value = "禁用标示")
   private Boolean disabled;

   @ApiModelProperty(value = "默认地址")
   private Boolean isDefault;


}