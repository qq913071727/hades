package com.tsfyun.scm.entity.order;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 原始订单明细
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Data
public class ImpOrderMemberHistory {

    private static final long serialVersionUID=1L;

    @Id
    private Long id;

    private Integer rowNo;

    /**
     * 订单主单
     */

    private Long impOrderId;

    /**
     * 型号
     */

    private String model;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;

    /**
     * 客户物料号
     */

    private String goodsCode;

    /**
     * 产地
     */

    private String country;

    /**
     * 产地
     */

    private String countryName;

    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 单价(委托)
     */

    private BigDecimal unitPrice;

    /**
     * 总价(委托)
     */

    private BigDecimal totalPrice;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;

}
