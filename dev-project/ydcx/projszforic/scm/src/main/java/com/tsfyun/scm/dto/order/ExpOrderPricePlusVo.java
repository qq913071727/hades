package com.tsfyun.scm.dto.order;

import com.tsfyun.scm.vo.order.ExpOrderPriceMemberVO;
import com.tsfyun.scm.vo.order.ExpOrderPriceVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpOrderPricePlusVo implements Serializable {

    private ExpOrderPriceVO orderPrice;

    private List<ExpOrderPriceMemberVO> members;
}
