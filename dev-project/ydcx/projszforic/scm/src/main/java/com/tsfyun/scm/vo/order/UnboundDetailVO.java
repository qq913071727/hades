package com.tsfyun.scm.vo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**=
 * 未绑入库的订单明细
 */
@Data
public class UnboundDetailVO implements Serializable {

    private Long id;//订单明细ID
    private Long orderId;//订单明细ID
    private String orderNo;//订单编号
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime orderDate;//订单日期

    @ApiModelProperty(value = "型号")
    private String model;
    @ApiModelProperty(value = "品牌")
    private String brand;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "客户物料号")
    private String goodsCode;
    @ApiModelProperty(value = "产地")
    private String country;
    @ApiModelProperty(value = "产地")
    private String countryName;
    @ApiModelProperty(value = "单位")
    private String unitCode;
    @ApiModelProperty(value = "单位")
    private String unitName;
    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;
    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;
    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;
    @ApiModelProperty(value = "箱号")
    private String cartonNo;

    @ApiModelProperty(value = "订单数量")
    private BigDecimal quantity;
    @ApiModelProperty(value = "已绑定入库数量")
    private BigDecimal bindQuantity;

    private BigDecimal bindableQuantity;//可绑定数量
    public BigDecimal getBindableQuantity(){
        return getQuantity().subtract(getBindQuantity()).setScale(2,BigDecimal.ROUND_HALF_UP);
    }

}
