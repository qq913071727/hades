package com.tsfyun.scm.service.system;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.enums.BizParamEnum;
import com.tsfyun.scm.dto.system.SysParamDTO;
import com.tsfyun.scm.entity.system.SysParam;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.SysParamVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-04-15
 */
public interface ISysParamService extends IService<SysParam> {


    /**
     * 修改
     * @param dto
     */
    void update(SysParamDTO dto);

    /**=
     * 禁用启用
     * @param id
     * @param disabled
     */
    void updateDisabled(String id,Boolean disabled);

    /**
     * 根据获取参数信息
     * @param id
     * @return
     */
    SysParamVO getById(String id);

    /**=
     * 获取所有配置转换MAP
     * @return
     */
    Map<String,SysParamVO> obtainAllParamMap();

    /**
     * 列表
     * @param roleDTO
     * @return
     */
    PageInfo<SysParam> pageList(SysParamDTO roleDTO);

    /**
     * 批量根据参数名获取参数配置信息（供业务方查询使用）
     * @param paramEnums
     * @return
     */
    List<SysParamVO> getByIds(BizParamEnum... paramEnums);

}
