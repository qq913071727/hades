package com.tsfyun.scm.entity.support;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 意见反馈
 * </p>
 *
 *
 * @since 2020-09-23
 */
@Data
public class Feedback extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 问题或建议类型
     */

    private String questionType;

    /**
     * 问题或建议
     */

    private String memo;

    /**
     * 联系方式
     */

    private String contact;


}
