package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 国内物流请求实体
 * </p>
 *

 * @since 2020-03-25
 */
@Data
@ApiModel(value="DomesticLogistics请求对象", description="国内物流请求实体")
public class DomesticLogisticsDTO  extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "数据id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "国内物流编码不能为空")
   @LengthTrim(max = 20,message = "国内物流编码最大长度不能超过20位")
   @ApiModelProperty(value = "国内物流编码")
   private String code;

   @NotEmptyTrim(message = "国内物流名称不能为空")
   @LengthTrim(max = 100,message = "国内物流名称最大长度不能超过100位")
   @ApiModelProperty(value = "国内物流名称")
   private String name;

   @LengthTrim(max = 255,message = "时效要求最大长度不能超过255位")
   @ApiModelProperty(value = "时效要求-后台用逗号隔开")
   private List<String> timeliness;

   //@NotNull(message = "是否禁用不能为空")
   @ApiModelProperty(value = "是否禁用")
   private Boolean disabled;


}
