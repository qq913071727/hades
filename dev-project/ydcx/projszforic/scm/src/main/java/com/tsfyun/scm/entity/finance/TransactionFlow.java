package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 交易流水
 * </p>
 *

 * @since 2020-04-16
 */
@Data
public class TransactionFlow {

     private static final long serialVersionUID=1L;

     @Id
     @KeySql(genId = IdWorker.class)
     private Long id;

    /**
     * 客户id
     */

    private Long customerId;

    /**
     * 交易类型
     */

    private String transactionType;

    /**
     * 交易类别
     */

    private String transactionCategory;

    /**
     * 交易流水号
     */

    private String transactionNo;

    /**
     * 交易金额
     */

    private BigDecimal amount;

    /**
     * 余额
     */

    private BigDecimal balance;

    /**
     * 备注
     */

    private String memo;

    /**
     * 交易时间
     */

    private LocalDateTime accountDate;

    /**
     * 备用字段1
     */
    private String extend1;

    @InsertFill
    private LocalDateTime dateCreated;

}
