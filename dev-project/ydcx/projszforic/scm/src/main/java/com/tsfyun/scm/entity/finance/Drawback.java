package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 退税单
 * </p>
 *
 *
 * @since 2021-10-20
 */
@Data
public class Drawback extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 采购合同号
     */

    private String purchaseNo;

    /**
     * 订单号
     */

    private String orderNo;
    private String clientNo;

    /**
     * 退税比例
     */
    private BigDecimal billie;

    /**
     * 付款金额
     */
    private BigDecimal accountValue;



    /**
     * 状态
     */

    private String statusId;

    /**
     * 要求付款时间
     */

    private Date claimPaymentDate;

    /**
     * 实际付款时间
     */

    private Date actualPaymentDate;

    /**
     * 付款主体
     */

    private String subjectType;

    /**
     * 付款主体
     */

    private Long subjectId;

    /**
     * 付款主体
     */

    private String subjectName;

    /**
     * 付款银行
     */

    private String subjectBank;

    /**
     * 付款银行账号
     */

    private String subjectBankNo;

    /**
     * 收款方
     */

    private String payeeName;

    /**
     * 收款银行
     */

    private String payeeBank;

    /**
     * 收款银行账号
     */

    private String payeeBankNo;

    /**
     * 备注
     */

    private String memo;




}
