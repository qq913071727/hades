package com.tsfyun.scm.dto.logistics;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;

@Data
public class DeliveryWaybillQTO extends PaginationDto {

    private String statusId;//状态
    private String customerName;//公司名称
    private String docNo;//系统单号
    private String impOrderNo;//订单号
    private String receivingMode;//送货方式
    private String experssNo;//物流单号

    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**
     * 下单开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 下单结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;
}
