package com.tsfyun.scm.vo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/8 15:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpressStandardQuotePlusVO implements Serializable {

    private ExpressStandardQuoteVO quote;

    private List<ExpressStandardQuoteMemberVO> members;

}
