package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 发票
 * </p>
 *

 * @since 2020-05-15
 */
@Data
public class Invoice extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 税单类型
     */

    private String taxType;

    /**
     * 申请日期
     */

    private LocalDateTime applyDate;

    /**
     * 确认日期
     */

    private LocalDateTime confirmDate;

    /**
     * 购货单位-买方
     */

    private Long buyerId;

    /**
     * 购货单位名称
     */

    private String buyerName;

    /**
     * 买方银行名称
     */

    private String buyerBankName;

    /**
     * 买方纳税人识别号
     */

    private String buyerTaxpayerNo;

    /**
     * 买方银行账号
     */

    private String buyerBankAccount;

    /**
     * 买方银行地址
     */

    private String buyerBankAddress;

    /**
     * 买方联系人
     */

    private String buyerLinkPerson;

    /**
     * 买方联系电话
     */

    private String buyerBankTel;

    /**
     * 卖方
     */

    private Long sellerId;

    /**
     * 卖方银行名称
     */

    private String sellerBankName;

    /**
     * 卖方银行账号
     */

    private String sellerBankAccount;

    /**
     * 卖方银行地址
     */
    private String sellerBankAddress;

    /**
     * 发票类型
     */

    private String invoiceType;

    /**
     * 实际发票编号
     */

    private String invoiceNo;

    /**
     * 实际开票日期
     */

    private LocalDateTime invoiceDate;

    /**
     * 开票金额
     */

    private BigDecimal invoiceVal;

    /**
     * 税点
     */

    private BigDecimal taxPoint;

    /**
     * 订单号
     */

    private String orderNo;

    /**=
     * 订单号
     */
    private Long orderId;

    /**
     * 内贸合同号
     */

    private String saleContractNo;

    /**
     * 快递单号
     */

    private String expressNo;

    /**
     * 开票要求
     */

    private String requirement;

    /**
     * 备注
     */

    private String memo;


}
