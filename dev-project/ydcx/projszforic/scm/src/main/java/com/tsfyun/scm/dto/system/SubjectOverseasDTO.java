package com.tsfyun.scm.dto.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-03-19
 */
@Data
@ApiModel(value="SubjectOverseas请求对象", description="请求实体")
public class SubjectOverseasDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;

   private Long subjectId;

   @NotEmptyTrim(message = "英文名称不能为空")
   @LengthTrim(max = 100,message = "英文名称最大长度不能超过100位")
   @ApiModelProperty(value = "英文名称")
   private String name;

   @LengthTrim(max = 255,message = "中文名称最大长度不能超过255位")
   @ApiModelProperty(value = "中文名称")
   private String nameCn;

   @LengthTrim(max = 255,message = "英文地址最大长度不能超过255位")
   @ApiModelProperty(value = "英文地址")
   private String address;

   @LengthTrim(max = 255,message = "中文地址最大长度不能超过255位")
   @ApiModelProperty(value = "中文地址")
   private String addressCn;

   @ApiModelProperty(value = "默认")
   private Boolean isDefault;

   @LengthTrim(max = 50,message = "传真最大长度不能超过50位")
   @ApiModelProperty(value = "传真")
   private String fax;

   @LengthTrim(max = 50,message = "电话最大长度不能超过50位")
   @ApiModelProperty(value = "电话")
   private String tel;

   @ApiModelProperty(value = "电子章")
   private String chapter;
   @ApiModelProperty(value = "文件ID")
   private String fileId;

}
