package com.tsfyun.scm.vo.wms;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 入库单明细响应实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="ReceivingNoteMember响应对象", description="入库单明细响应实体")
public class ReceivingNoteMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "入库单")
    private Long receivingNoteId;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "规格参数")
    private String spec;

    @ApiModelProperty(value = "客户物料号")
    private String goodsCode;

    @ApiModelProperty(value = "产地")
    private String country;

    @ApiModelProperty(value = "产地")
    private String countryName;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;

    @ApiModelProperty(value = "箱号")
    private String cartonNo;


}
