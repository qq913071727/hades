package com.tsfyun.scm.mapper.wms;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.wms.DeliveryNote;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.wms.DeliveryNoteVO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库单 Mapper 接口
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Repository
public interface DeliveryNoteMapper extends Mapper<DeliveryNote> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<DeliveryNoteVO> list(Map<String,Object> params);

}
