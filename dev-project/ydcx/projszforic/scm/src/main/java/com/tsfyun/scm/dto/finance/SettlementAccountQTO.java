package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/14 17:59
 */
@Data
public class SettlementAccountQTO extends PaginationDto {


    private String docNo;
    private String accountNo;
    private String orderNo;

    /**
     * 状态
     */
    private String statusId;

    @Pattern(regexp = "settlement_date|date_created",message = "日期查询类型错误")
    private String queryDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
