package com.tsfyun.scm.service.logistics;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteDTO;
import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteQTO;
import com.tsfyun.scm.entity.logistics.OverseasDeliveryNote;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNotePlusVO;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNoteVO;

/**
 * <p>
 * 香港送货单 服务类
 * </p>
 *
 *
 * @since 2021-11-09
 */
public interface IOverseasDeliveryNoteService extends IService<OverseasDeliveryNote> {

    /**
     * 分页
     * @param qto
     * @return
     */
    PageInfo<OverseasDeliveryNoteVO> pageList(OverseasDeliveryNoteQTO qto);

    /**
     * 出口香港送货
     * @param id
     * @return
     */
    OverseasDeliveryNotePlusVO detail(Long id);

    /**
     * 保存香港派送单主单
     * @param dto
     * @return
     */
    Long saveMain(OverseasDeliveryNoteDTO dto);

    /**
     * 确认
     * @param dto
     */
    void confirm(TaskDTO dto);

    /**
     * 发车派送
     * @param dto
     */
    void delivery(TaskDTO dto);

    /**
     * 派送完成
     * @param dto
     */
    void reached(TaskDTO dto);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 更新主单订单号
     * @param id
     * @param expOrderNos
     */
    void updateExpOrderNos(Long id,String expOrderNos);



}
