package com.tsfyun.scm.mapper.order;

import com.tsfyun.scm.dto.order.UnboundDetailQTO;
import com.tsfyun.scm.entity.order.ImpOrderMember;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.order.PriceFluctuationHistory;
import com.tsfyun.scm.vo.order.ImpOrderMemberVO;
import com.tsfyun.scm.vo.order.OrderReceivingBindVO;
import com.tsfyun.scm.vo.order.UnboundDetailVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单明细 Mapper 接口
 * </p>
 *

 * @since 2020-04-08
 */
@Repository
public interface ImpOrderMemberMapper extends Mapper<ImpOrderMember> {

    Integer savaAndUpdateBatch(@Param(value = "impOrderMemberList") List<ImpOrderMember> impOrderMemberList);
    //批量修改明细物料信息(品牌，名称)
    Integer batchUpdateMateriel(@Param(value = "impOrderMemberList")List<ImpOrderMember> impOrderMemberList);

    List<PriceFluctuationHistory> findImpHistory(@Param(value = "dateTime")LocalDateTime dateTime,@Param(value = "model") String model);

    //订单物料明细，包含归类
    List<ImpOrderMemberVO> findOrderMaterielMember(@Param(value = "impOrderId")Long impOrderId);
    //根据物料ID修改物料ID
    Integer updateMemberMaterielId(@Param(value = "oldId")String oldId, @Param(value = "newId")String newId);

    //查询未绑入库的订单明细
    List<UnboundDetailVO> unboundDetailList(UnboundDetailQTO qto);

    // 批量修改订单验货明细
    Integer batchUpdateSplitMembers(@Param(value = "impOrderMemberList")List<ImpOrderMember> impOrderMemberList);
}
