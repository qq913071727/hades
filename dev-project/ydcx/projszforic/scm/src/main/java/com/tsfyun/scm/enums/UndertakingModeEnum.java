package com.tsfyun.scm.enums;


import java.util.Arrays;
import java.util.Objects;

public enum UndertakingModeEnum {
    CUSTOMER("customer","客户承担"),
    COMPANY("company","公司承担"),
    ;

    private String code;
    private String name;

    UndertakingModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }


    public static UndertakingModeEnum of(String code) {
        return Arrays.stream(UndertakingModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

