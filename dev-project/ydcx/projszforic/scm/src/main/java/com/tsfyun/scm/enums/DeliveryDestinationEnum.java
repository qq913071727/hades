package com.tsfyun.scm.enums;

import java.util.Arrays;
import java.util.Objects;

public enum DeliveryDestinationEnum {
    HONG_KONG("hong_kong","货运香港仓"),
    DESTINATION("destination","客户指定地点"),
    ;

    private String code;
    private String name;

    DeliveryDestinationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }


    public static DeliveryDestinationEnum of(String code) {
        return Arrays.stream(DeliveryDestinationEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

