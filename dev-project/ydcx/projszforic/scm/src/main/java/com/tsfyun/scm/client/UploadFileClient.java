package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.FileDocIdDTO;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.UploadFileCntVO;
import com.tsfyun.common.base.vo.UploadFileVO;
import com.tsfyun.scm.client.fallback.UploadFileClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.util.List;

@Component
@FeignClient(name = "scm-file",path = "file",fallbackFactory = UploadFileClientFallback.class)
public interface UploadFileClient {

    /**
     *
     * @param fileType 文件类型file|image
     * @param tenantCode 租户编码
     * @param tenantName 租户名称
     * @param docId      单据id
     * @param docType    单据类型
     * @param businessType 单据业务类型
     * @param memo   备注
     * @param uid 用户id
     * @param uname 用户名称
     * @param file 文件
     * @return
     */
    @PostMapping(value = "/upload",produces = { MediaType.APPLICATION_JSON_UTF8_VALUE }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Result<Long> upload(@RequestParam(value = "fileType") String fileType,
                        @RequestParam(value = "tenantCode") String tenantCode,
                        @RequestParam(value = "tenantName") String tenantName,
                        @RequestParam(value = "docId") String docId,
                        @RequestParam(value = "docType") String docType,
                        @RequestParam(value = "businessType") String businessType,
                        @RequestParam(value = "memo", required = false) String memo,
                        @RequestParam(value = "uid") String uid,
                        @RequestParam(value = "uname") String uname,
                        @RequestPart(name = "file", required = true) MultipartFile file);

    /**
     * 获取文件列表
     * @param docType 单据类型
     * @param docId   单据id
     * @return
     */
    @GetMapping(value = "/fileList")
    Result<List<UploadFileVO>> list(@RequestParam(value = "docType") String docType, @RequestParam(value = "docId") String docId);

    /**
     * 关联文件
     * @param docType
     * @param docId
     * @param fileId
     * @param only
     * @return
     */
    @PostMapping(value = "/updateDocId")
    Result<Void> updateDocId(
            @RequestParam(name = "docType") String docType,
            @RequestParam(name = "docId") String docId,
            @RequestParam(name = "fileId") Long fileId,
            @RequestParam(name = "only", defaultValue = "false") Boolean only
    );

    //删除文件
    /**
     * 删除文件（逻辑删）
     * @param ids
     * @return
     */
    @PostMapping(value = "/removes")
    Result<Void> remove(@RequestParam(value = "ids") List<Long> ids);

    /**
     * 根据文件类型或原始单据ID修改单据ID
     * @param list
     * @return
     */
    @PostMapping(value = "/batchUpdateDocId")
    Result<Void> batchUpdateDocId(@RequestBody @Valid List<FileDocIdDTO> list);

    /**
     * 获取文件数量
     * @param docType 单据类型
     * @param docId   单据id
     * @return
     */
    @GetMapping(value = "/obtainFileSize")
    Result<UploadFileCntVO> obtainFileSize(@RequestParam(value = "docType")String docType, @RequestParam(value = "docId")String docId);
}
