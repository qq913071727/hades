package com.tsfyun.scm.vo.order;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 订单审价明细响应实体
 * </p>
 *
 *
 * @since 2020-04-16
 */
@Data
@ApiModel(value="ImpOrderPriceMember响应对象", description="订单审价明细响应实体")
public class ImpOrderPriceMemberVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "波动率")
    private BigDecimal fluctuations;

    @ApiModelProperty(value = "是否有报关记录")
    private Boolean isImp;

    @ApiModelProperty(value = "是否新料")
    private Boolean isNewGoods;

    private Long orderMemberId;

    private Long orderPriceId;

    @ApiModelProperty(value = "计算过程")
    private String processLogs;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "产地")
    private String country;

    @ApiModelProperty(value = "产地")
    private String countryName;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "单价(委托)")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "总价(委托)")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    private Integer rowNo;

    /**
     * 币制
     */
    private String currencyCode;

    /**
     * 币制
     */
    private String currencyName;

}
