package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.order.ImpOrderPaySituationQTO;
import com.tsfyun.scm.vo.finance.client.ClientApplyPaymentPlusVO;
import com.tsfyun.scm.vo.order.ApplyPaymentPlusVO;
import com.tsfyun.scm.vo.order.ImpOrderPaySituationVO;

import java.util.List;

public interface IImpOrderPaymentService {


    PageInfo<ImpOrderPaySituationVO> list(ImpOrderPaySituationQTO qto);
    //初始化订单付汇申请
    ApplyPaymentPlusVO initApply(List<Long> ids);
    //客户端初始化订单付汇申请
    ApplyPaymentPlusVO clientInitApply(List<Long> ids);

    /**
     * 根据订单id集合查询订单付汇情况
     * @param orderIds
     * @return
     */
    List<ImpOrderPaySituationVO> paySituationByIds(List<Long> orderIds);




}
