package com.tsfyun.scm.service.file;


import com.tsfyun.common.base.dto.*;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.common.base.vo.FileQtyVO;
import com.tsfyun.common.base.vo.UploadFileCntVO;
import com.tsfyun.common.base.vo.UploadFileVO;
import com.tsfyun.scm.entity.file.UploadFile;
import com.tsfyun.scm.vo.base.ClientFileVO;

import java.util.List;
import java.util.Map;

public interface IUploadFileService extends IService<UploadFile> {

    /**
     * 上传文件
     * @param dto
     * @return
     */
    UploadFileVO upload(UploadFileDTO dto);

    /**
     * 删除文件
     * @param id
     */
    void delete(List<Long> id);

    /**
     * 根据单据ID删除文件
     * @param docId
     */
    void deleteByDocId(String docId);

    /**
     * 获取文件列表
     * @return
     */
    List<UploadFileVO> list(FileQTO fileQTO);

    /**
     * 获取文件数量
     * @return
     */
    Integer count(FileQTO fileQTO);

    /**=
     * 调整单据文件
     * @param docId
     * @param dto
     */
    void relateFile(String docId, FileDTO dto);

    /**
     * 获取文件数量(分组)
     * @return
     */
    List<FileQtyVO> countGroup(FileQTO fileQTO);

    /**
     * 获取文件
     * @param docId
     * @param docType
     * @param businessType
     * @return
     */
    List<ClientFileVO> findByFile(String docId,String docType,String businessType);

    /**
     * 分页获取文件列表
     * @return
     */
    List<UploadFileVO> list(PaginationDto page,FileQTO fileQTO);

    /**
     * 批量关联文件
     * @param fileDocIdDTOS
     */
    void batchUpdateDocId(List<FileDocIdDTO> fileDocIdDTOS);

    /**
     *
     * @param docId
     * @param docType
     * @return
     */
    List<String> getFilePath(String docId, String docType,String businessType);
}
