package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @since Created in 2020/4/28 09:51
 */
@Data
public class ImpOrderSimpleVO implements Serializable {

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 客户名称
     */
    private String customerName;

}
