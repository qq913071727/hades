package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 承运人数据
 * @since Created in 2020/4/22 11:13
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Carrier", propOrder = {
        "id",
})
public class Carrier {

    //承运人代码
    @XmlElement(name = "ID")
    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
