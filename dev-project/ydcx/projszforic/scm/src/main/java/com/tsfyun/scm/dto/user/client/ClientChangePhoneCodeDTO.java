package com.tsfyun.scm.dto.user.client;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/25 17:50
 */
@Data
public class ClientChangePhoneCodeDTO implements Serializable {

    @ApiModelProperty(value = "手机号码")
    @NotEmptyTrim(message = "手机号码不能为空")
    @Pattern(regexp = "^[1][0-9]{10}$",message = "手机号格式错误")
    private String phoneNo;

}
