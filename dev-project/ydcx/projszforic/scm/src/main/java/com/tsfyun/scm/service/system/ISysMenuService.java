package com.tsfyun.scm.service.system;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.system.SysMenu;
import com.tsfyun.scm.entity.system.SysRole;
import com.tsfyun.scm.util.UITree;
import com.tsfyun.scm.vo.system.SimpleSysMenuVO;
import com.tsfyun.scm.vo.system.SysMenuVO;

import java.util.List;

public interface ISysMenuService extends IService<SysMenu> {


    /**
     * 获取后台用户菜单
     * @return
     */
    List<UITree> getBackgroundUserMenu();

    /**
     * 获取指定版块后台用户菜单
     * @return
     */
    List<UITree> getSectionBackgroundUserMenu(String sectionModule);


    /**
     * 根据角色获取所有的菜单树
     * @param sysRole
     * @return
     */
    List<SimpleSysMenuVO> getAllMenusByRole(SysRole sysRole);


    /**
     * 获取某菜单下面的按钮菜单
     * @param menuId
     * @return
     */
    List<SysMenuVO> getButtonMenusByMenuId(String menuId);

    /**
     * 获取所有的菜单
     * @return
     */
    List<SimpleSysMenuVO> getAllTypeMenu();

    /**
     * 根据操作码获取有该菜单的跳转信息
     * @param operationCode
     * @return
     */
    SysMenu findByOperationCode(String operationCode);

}
