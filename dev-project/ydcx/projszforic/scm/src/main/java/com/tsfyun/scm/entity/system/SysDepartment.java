package com.tsfyun.scm.entity.system;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * 部门
 */
@Data
public class SysDepartment extends BaseEntity implements Serializable {

    /**
     * 部门编码
     */
    private String code;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 父级部门（保留字段，暂不使用）
     */
    private Long parentId;

    /**
     * 排序字段
     */
    private Integer sort;

}
