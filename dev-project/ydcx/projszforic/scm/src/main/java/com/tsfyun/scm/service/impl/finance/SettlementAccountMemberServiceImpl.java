package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.collection.CollUtil;
import com.tsfyun.scm.entity.finance.SettlementAccountMember;
import com.tsfyun.scm.mapper.finance.SettlementAccountMemberMapper;
import com.tsfyun.scm.service.finance.ISettlementAccountMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.finance.OverseasReceivingAccountVO;
import com.tsfyun.scm.vo.finance.SettlementAccountMemberVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 结汇明细 服务实现类
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Service
public class SettlementAccountMemberServiceImpl extends ServiceImpl<SettlementAccountMember> implements ISettlementAccountMemberService {

    @Autowired
    private SettlementAccountMemberMapper settlementAccountMemberMapper;

    @Override
    public List<OverseasReceivingAccountVO> findOverseasReceivingAccount(Long settlementAccountId) {
        return settlementAccountMemberMapper.findOverseasReceivingAccount(settlementAccountId);
    }

    @Override
    public List<SettlementAccountMember> findByAccountId(Long settlementAccountId) {
        return settlementAccountMemberMapper.findByAccountId(settlementAccountId);
    }


}
