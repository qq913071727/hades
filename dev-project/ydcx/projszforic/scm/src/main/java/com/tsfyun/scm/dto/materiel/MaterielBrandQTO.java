package com.tsfyun.scm.dto.materiel;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 物料品牌请求实体
 * </p>
 *

 * @since 2020-03-26
 */
@Data
@ApiModel(value="MaterielBrand查询请求对象", description="物料品查询牌请求实体")
public class MaterielBrandQTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String name;

   private String memo;

   private String type;

}
