package com.tsfyun.scm.vo.system;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-30
 */
@Data
@ApiModel(value="MailConfig响应对象", description="响应实体")
public class MailConfigVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "邮箱服务器")
    private String mailHost;

    @ApiModelProperty(value = "邮箱端口")
    private Integer mailPort;

    @ApiModelProperty(value = "邮箱用户名")
    private String mailUser;

    @ApiModelProperty(value = "邮箱密码")
    private String mailPassword;

    @JsonIgnore
    @ApiModelProperty(value = "发送失败重试次数")
    private Integer retryTimes;

    @ApiModelProperty(value = "是否默认")
    private Boolean isDefault;

    @ApiModelProperty(value = "是否禁用")
    private Boolean disabled;


}
