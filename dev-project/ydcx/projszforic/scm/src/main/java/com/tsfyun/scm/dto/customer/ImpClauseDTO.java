package com.tsfyun.scm.dto.customer;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.RateTypeEnum;
import com.tsfyun.common.base.enums.TransactionModeEnum;
import com.tsfyun.common.base.enums.VoluntarilyTaxEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 进口条款请求实体
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Data
@ApiModel(value="ImpClause请求对象", description="进口条款请求实体")
public class ImpClauseDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long agreementId;

   @NotEmptyTrim(message = "缴税方式不能为空")
   @EnumCheck(clazz = VoluntarilyTaxEnum.class,message = "缴税方式错误")
   @ApiModelProperty(value = "缴税方式")
   private String voluntarilyTax;

   @NotNull(message = "申请税款额度不能为空")
   @Digits(integer = 10, fraction = 2, message = "税款额度整数位不能超过10位，小数位不能超过2位")
   @ApiModelProperty(value = "申请税款额度")
   private BigDecimal taxQuota;

   @NotNull(message = "申请货款额度不能为空")
   @Digits(integer = 10, fraction = 2, message = "货款额度整数位不能超过10位，小数位不能超过2位")
   @ApiModelProperty(value = "申请货款额度")
   private BigDecimal goodsQuota;

   @LengthTrim(max = 500,message = "申请额度说明最大长度不能超过500位")
   @ApiModelProperty(value = "申请额度说明")
   private String quotaExplain;

   @EnumCheck(clazz = TransactionModeEnum.class,message = "成交方式错误")
   @ApiModelProperty(value = "成交方式")
   private String transactionMode;

   @NotEmptyTrim(message = "货款汇率类型不能为空")
   @EnumCheck(clazz = RateTypeEnum.class,message = "货款汇率类型错误")
   @ApiModelProperty(value = "货款汇率类型")
   private String goodsRate;

   @NotEmptyTrim(message = "货款汇率时间不能为空")
   @ApiModelProperty(value = "货款汇率时间")
   private String goodsRateTime;

   @NotNull(message = "最大超期天数不能为空")
   @ApiModelProperty(value = "最大超期天数")
   @Max(value = 99,message = "最大超期天数不能超过99")
   private Integer overdueDays;

   @ApiModelProperty(value = "税款以实缴定应收")
   private Boolean isPaidIn;

   @NotEmptyTrim(message = "税款汇率类型不能为空")
   @EnumCheck(clazz = RateTypeEnum.class,message = "税款汇率类型错误")
   @ApiModelProperty(value = "税款汇率类型")
   private String taxRate;

   @ApiModelProperty(value = "税款汇率时间")
   private String taxRateTime;
}
