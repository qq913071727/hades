package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SubjectBankQTO extends PaginationDto{

    @ApiModelProperty(value = "银行名称")
    private String name;

    @ApiModelProperty(value = "银行账号")
    private String account;

    private Boolean disabled;
}
