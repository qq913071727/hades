package com.tsfyun.scm.vo.wms;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.enums.domain.DeliveryNoteStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 出库单响应实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="DeliveryNote响应对象", description="出库单响应实体")
public class DeliveryNoteVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "单号")
    private String docNo;

    @ApiModelProperty(value = "单据类型-枚举")
    private String billType;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    private String customerName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "出库时间")
    private LocalDateTime deliveryDate;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "仓库")
    private Long warehouseId;

    private String warehouseName;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String billTypeDesc;

    private String statusDesc;

    public String getBillTypeDesc() {
        return Optional.ofNullable(BillTypeEnum.of(billType)).map(BillTypeEnum::getName).orElse("");
    }

    public String getStatusDesc() {
        return Optional.ofNullable(DeliveryNoteStatusEnum.of(statusId)).map(DeliveryNoteStatusEnum::getName).orElse("");
    }



}
