package com.tsfyun.scm.entity.view;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/20 11:01
 */
@Data
public class ViewExpOrderCostMember implements Serializable {

    @Id
    private Long id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 订单日期
     */
    private Date orderDate;

    /**
     * 账期日期
     */

    private Date periodDate;

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 应收金额
     */
    private BigDecimal receAmount;

    /**
     * 已收金额
     */
    private BigDecimal acceAmount;

    /**
     * 币制
     */
    private String currencyName;

    /**
     * 销售
     */
    private String salePersonName;

}
