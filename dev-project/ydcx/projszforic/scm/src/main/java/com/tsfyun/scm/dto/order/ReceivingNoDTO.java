package com.tsfyun.scm.dto.order;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ReceivingNoDTO implements Serializable {

    @NotEmptyTrim(message = "入库单号不能为空")
    @ApiModelProperty(value = "入库单号")
    private String receivingNo;

    @NotNull(message = "绑定数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "绑定数量不能为空整数位不能超过8位，小数位不能超过2")
    @ApiModelProperty(value = "绑定数量")
    private BigDecimal quantity;
}
