package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.ClaimedAmountPlusDTO;
import com.tsfyun.scm.dto.order.*;
import com.tsfyun.scm.entity.finance.OverseasReceivingOrder;
import com.tsfyun.scm.entity.order.ExpOrder;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.OverseasReceivingOrderVO;
import com.tsfyun.scm.vo.order.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 出口订单 服务类
 * </p>
 *
 *
 * @since 2021-01-26
 */
public interface IExpOrderService extends IService<ExpOrder> {

    /**
     * 新增订单
     * @param orderDto
     * @return
     */
    Long add(ExpOrderPlusDTO orderDto);

    /**
     * 修改订单
     * @param orderDto
     */
    void edit(ExpOrderPlusDTO orderDto);

    /**
     * 列表
     * @param qto
     * @return
     */
    PageInfo<ExpOrderListVO> list(ExpOrderQTO qto);

    /**
     * 订单详情
     * @param id
     * @param operation
     * @return
     */
    ExpOrderVO detail(Long id,String operation);

    /**
     * 订单详情
     * @param id
     * @return
     */
    ExpOrderVO detail(Long id);

    /**
     * 订单详情带明细
     * @param id
     * @param operation
     * @return
     */
    ExpOrderPlusVO detailPlus(Long id,String operation);

    /**
     * 订单详情带明细
     * @param id
     * @return
     */
    ExpOrderPlusVO detailPlus(Long id);

    /**
     * 删除订单
     * @param id
     */
    void removeOrder(Long id);
    void removeOrder(ExpOrder expOrder);

    /**=
     * 订单审价改变状态
     * @param dto
     */
    void orderPricing(TaskDTO dto);

    /**
     * 审核订单
     * @param dto
     */
    void examine(TaskDTO dto);

    /**=
     * 分摊净重
     * @param id
     * @param netWeight
     */
    void shareNetWeight(Long id, BigDecimal netWeight);

    /**=
     * 分摊毛重
     * @param id
     * @param grossWeight
     */
    void shareGrossWeight(Long id, BigDecimal grossWeight);

    /**=
     * 保存验货明细修改
     * @param dto
     */
    void saveInspectionMemberEdit(ExpOrderInspectionMemberDTO dto);

    /**=
     * 确认验货完成
     * @param id
     */
    void confirmInspection(Long id);

    /**=
     * 确认进口
     * @param dto
     */
    void confirmExp(TaskDTO dto);

    /**
     * 获取跨境运输单已经绑定的订单数据
     * @param transportNo
     * @return
     */
    List<BindOrderVO> waybillBindedOrderList(String transportNo);

    /**
     * 获取跨境运输单可以绑定的订单数据
     * @param isCharterCar
     * @return
     */
    List<BindOrderVO> waybillCanBindOrderList(Boolean isCharterCar);

    /**
     * 重置订单上的运输单号
     * @param id
     */
    void resetTransportNo(Long id);

    /**
     * 设置订单申报信息
     * @param dto
     */
    void setupDeclaration(SetupDeclarationDTO dto);

    /**
     * 订单更改跨境运输单号
     * @param orderId
     * @param transportNo
     */
    int updateTransportNo(Long orderId,String transportNo);

    /**
     * 确认出口后退回验货
     * @param id
     * @param info
     */
    void backInspection(Long id,String info);

    /**
     * 根据跨境运输单号获取绑单的订单数
     * @param transportNo
     * @return
     */
    int getWaybillBindOrderNumber(String transportNo);

    /**=
     * 订单报关完成（报关单触发）
     * @param orderId
     */
    void declarationCompleted(Long orderId);

    /**=
     * 跨境运单签收同步订单 已通关
     * @param bindedOrders
     */
    void expOrderClearance(List<BindOrderVO> bindedOrders);

    /**
     * 根据订单号查询
     * @param docNo
     * @return
     */
    ExpOrder findByDocNo(String docNo);

    /**
     * 获取可以调整费用的订单下拉
     * @param orderNo
     * @return
     */
    List<ExpOrderAdjustCostVO> selectAdjustOrder(String orderNo);

    /**
     * 根据采购合同修改订单
     * @param order
     */
    void updateByPurchaseContract(ExpOrder order);

    /**
     * 境外收款认领金额
     * @param dto
     * @return
     */
    List<OverseasReceivingOrderVO> collectionClaimedAmount(ClaimedAmountDTO dto);

    /**
     * 境外收款单认领
     * @param dto
     * @return
     */
    BigDecimal overseasAccountClaimed(ClaimedAmountPlusDTO dto);

    /**
     * 根据认领明细删除订单认领数据
     * @param receivingOrder
     */
    void clearClaimOrder(OverseasReceivingOrder receivingOrder);

    /**
     * 写入结汇金额
     * @param id
     * @param settlementValueUsd
     * @param settlementValueCny
     */
    void writeSettlementValue(Long id, BigDecimal settlementValueUsd, BigDecimal settlementValueCny);
    /**
     * 取消写入结汇金额
     * @param id
     * @param settlementValueUsd
     * @param settlementValueCny
     */
    void canWriteSettlementValue(Long id, BigDecimal settlementValueUsd, BigDecimal settlementValueCny);

    /**
     * 单量统计
     * @param startDate
     * @param endDate
     * @return
     */
    Integer totalSingleQuantity(Date startDate, Date endDate);

    /**
     * 金额统计(USD)
     * @param startDate
     * @param endDate
     * @return
     */
    BigDecimal totalAmountMoney(Date startDate, Date endDate);


    /**
     * 订单明细导入解析
     * @param file
     * @return
     */
    List<ImportExpOrderMemberVO> readDataFromMemberImport(MultipartFile file);

    /**
     * 根据订单编号查询中港运输单据编号
     * @param orderNos
     * @return
     */
    List<OrderCrossBorderStatusVO> getTransportNoByOrderNo(@Param(value = "orderNos")List<String> orderNos);


    /**
     * 批量修改订单状态
     * @param statusId
     * @param orderNos
     * @return
     */
    int batchUpdateStatus(@Param(value = "statusId")String statusId,@Param(value = "orderNos")List<String> orderNos);


    /**
     * 打印订单委托书
     * @param id
     * @return
     */
    ExpOrderEntrustVO getPrintEntrustData(@RequestParam("id")Long id);

    /**
     * 统计订单金额
     * @param qto
     * @return
     */
    BigDecimal summaryOrderAmount(ExpOrderQTO qto);

    /**
     * 根据订单编号查询客户单号
     * @param docNo
     * @return
     */
    List<String> findClientNoByDocNos(List<String> docNo);

}
