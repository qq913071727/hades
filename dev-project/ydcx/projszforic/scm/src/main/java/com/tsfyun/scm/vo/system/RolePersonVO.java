package com.tsfyun.scm.vo.system;

import lombok.Data;

import java.io.Serializable;

@Data
public class RolePersonVO implements Serializable {

    /**
     * 角色编码
     */
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 员工id
     */
    private Long personId;
}
