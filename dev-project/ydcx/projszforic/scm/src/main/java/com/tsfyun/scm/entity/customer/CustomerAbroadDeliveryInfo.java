package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 客户境外送货地址
 * </p>
 *
 *
 * @since 2021-09-16
 */
@Data
public class CustomerAbroadDeliveryInfo extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 收货公司
     */

    private String companyName;

    /**
     * 联系人
     */

    private String linkPerson;

    /**
     * 联系电话
     */

    private String linkTel;

    /**
     * 省
     */

    private String provinceName;

    /**
     * 市
     */

    private String cityName;

    /**
     * 区
     */

    private String areaName;

    /**
     * 详细地址
     */

    private String address;

    /**
     * 禁用标示
     */

    private Boolean disabled;

    /**
     * 默认地址
     */

    private Boolean isDefault;


}
