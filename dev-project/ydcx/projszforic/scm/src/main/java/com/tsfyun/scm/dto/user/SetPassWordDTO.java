package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class SetPassWordDTO implements Serializable {

    @NotEmptyTrim(message = "参数不能为空")
    private String accessId;

    @NotEmptyTrim(message = "密码不能为空")
    @LengthTrim(min = 6,max = 20,message = "密码长度只能为6-20位")
    private String passWord;

    @NotBlank(message = "确认密码不能为空")
    @LengthTrim(min = 6,max = 20,message = "确认密码长度只能为6-20位")
    private String comfirPassWord;

}
