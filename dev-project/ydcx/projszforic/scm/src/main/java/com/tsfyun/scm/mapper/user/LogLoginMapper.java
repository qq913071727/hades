package com.tsfyun.scm.mapper.user;


import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.user.LogLogin;
import org.springframework.stereotype.Repository;

@Repository
public interface LogLoginMapper extends Mapper<LogLogin> {


}