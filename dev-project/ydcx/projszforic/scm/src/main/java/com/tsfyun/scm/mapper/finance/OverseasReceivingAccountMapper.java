package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.finance.OverseasReceivingAccount;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.OverseasReceivingAccountVO;
import com.tsfyun.scm.vo.report.ExpOneVoteTheEndVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 境外收款 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-08
 */
@Repository
public interface OverseasReceivingAccountMapper extends Mapper<OverseasReceivingAccount> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<OverseasReceivingAccountVO> list(Map<String,Object> params);

    Integer findOrderNoIsSettlement(@Param(value = "docNo") String docNo);

    List<ExpOneVoteTheEndVO> oneVoteTheEndByOrderId(@Param(value = "orderId")Long orderId);
}
