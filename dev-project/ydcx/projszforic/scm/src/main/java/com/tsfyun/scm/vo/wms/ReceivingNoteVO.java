package com.tsfyun.scm.vo.wms;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.enums.InExpressTypeEnum;
import com.tsfyun.common.base.enums.domain.ReceivingNoteStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 入库单响应实体
 * </p>
 *
 *
 * @since 2020-04-24
 */
@Data
@ApiModel(value="ReceivingNote响应对象", description="入库单响应实体")
public class ReceivingNoteVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "入库单号")
    private String docNo;

    @ApiModelProperty(value = "单据类型-枚举")
    private String billType;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "入库时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime receivingDate;

    @ApiModelProperty(value = "入库方式")
    private String deliveryMode;

    //入库方式中文名称
    private String deliveryModeName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "仓库")
    private Long warehouseId;

    private String warehouseName;

    @ApiModelProperty(value = "供应商")
    private Long supplierId;

    private String supplierName;

    @ApiModelProperty(value = "总箱数")
    private Integer totalCartonNum;

    @ApiModelProperty(value = "快递类型")
    private String inExpressType;

    @ApiModelProperty(value = "快递公司")
    private String hkExpress;

    @ApiModelProperty(value = "快递公司")
    private String hkExpressName;

    @ApiModelProperty(value = "快递单号")
    private String hkExpressNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    private String billTypeDesc;

    private String statusDesc;

    private String inExpressTypeDesc;

    public String getStatusDesc() {
        return Optional.ofNullable(ReceivingNoteStatusEnum.of(statusId)).map(ReceivingNoteStatusEnum::getName).orElse("");
    }

    public String getBillTypeDesc() {
        return Optional.ofNullable(BillTypeEnum.of(billType)).map(BillTypeEnum::getName).orElse("");
    }

    private String getInExpressTypeDesc() {
        return Optional.ofNullable(InExpressTypeEnum.of(inExpressType)).map(InExpressTypeEnum::getName).orElse("");
    }

}
