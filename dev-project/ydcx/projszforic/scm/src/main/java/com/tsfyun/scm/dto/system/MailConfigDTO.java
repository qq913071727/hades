package com.tsfyun.scm.dto.system;

import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-30
 */
@Data
@ApiModel(value="MailConfig请求对象", description="请求实体")
public class MailConfigDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "数据id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "邮箱服务器不能为空")
   @LengthTrim(max = 100,message = "邮箱服务器最大长度不能超过100位")
   @ApiModelProperty(value = "邮箱服务器")
   private String mailHost;

   @NotNull(message = "邮箱端口不能为空")
   @LengthTrim(max = 4,message = "邮箱端口最大长度不能超过4位")
   private Integer mailPort;

   @Email(message = "邮箱格式错误")
   @NotEmptyTrim(message = "邮箱用户名不能为空")
   @LengthTrim(max = 100,message = "邮箱用户名最大长度不能超过100位")
   @ApiModelProperty(value = "邮箱用户名")
   private String mailUser;

   @NotEmptyTrim(message = "邮箱密码不能为空")
   @LengthTrim(max = 150,message = "邮箱密码最大长度不能超过150位")
   @ApiModelProperty(value = "邮箱密码")
   private String mailPassword;

   @NotNull(message = "是否默认不能为空")
   @ApiModelProperty(value = "是否默认")
   private Boolean isDefault;

   //@NotNull(message = "是否禁用不能为空")
   @ApiModelProperty(value = "是否禁用")
   private Boolean disabled;


}
