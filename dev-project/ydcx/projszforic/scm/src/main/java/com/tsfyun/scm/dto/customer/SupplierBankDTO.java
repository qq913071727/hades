package com.tsfyun.scm.dto.customer;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * <p>
 * 供应商银行请求实体
 * </p>
 *
 * @since 2020-03-12
 */
@Data
@ApiModel(value="SupplierBank请求对象", description="请求实体")
public class SupplierBankDTO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "供应商")
    private Long supplierId;

    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @NotEmptyTrim(message = "银行名称不能为空")
    @LengthTrim(max = 64,message = "银行名称长度不能超过64位")
    @ApiModelProperty(value = "银行名称")
    private String name;

    @NotEmptyTrim(message = "银行账号不能为空")
    @LengthTrim(max = 50,message = "银行账号长度不能超过50位")
    @ApiModelProperty(value = "银行账号")
    private String account;

    @NotEmptyTrim(message = "币制不能为空")
    @ApiModelProperty(value = "币制")
    private String currencyName;
    @ApiModelProperty(value = "币制ID")
    private String currencyId;

    @NotEmptyTrim(message = "银行SWIFT CODE不能为空")
    @LengthTrim(max = 50,message = "银行SWIFT CODE长度不能超过50位")
    @Pattern(regexp = "[a-zA-Z0-9_]{1,50}",message = "银行SWIFT CODE只能由字母和数字组成，最长50位")
    @ApiModelProperty(value = "SWIFT CODE")
    private String swiftCode;

    @LengthTrim(max = 20,message = "银行代码长度不能超过20位")
    @ApiModelProperty(value = "银行代码")
    private String code;

    @LengthTrim(max = 255,message = "银行地址长度不能超过255位")
    @ApiModelProperty(value = "银行地址")
    private String address;

    @NotNull(message = "银行禁用标志不能为空")
    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @NotNull(message = "提货是否默认不能为空")
    @ApiModelProperty(value = "默认地址")
    private Boolean isDefault;

}
