package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.PurchaseContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 采购合同响应实体
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Data
@ApiModel(value="PurchaseContract响应对象", description="采购合同响应实体")
public class PurchaseContractVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "合同编号")
    private String docNo;

    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "客户订单号")
    private String customerOrderNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "签定地点")
    private String signingPlace;

    @ApiModelProperty(value = "签定日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signingDate;

    @ApiModelProperty(value = "交货日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deliveryDate;

    @ApiModelProperty(value = "有效日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date effectiveDate;

    @ApiModelProperty(value = "开票日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date invoicingDate;

    @ApiModelProperty(value = "收票日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date invoiceDate;

    @ApiModelProperty(value = "创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "发票号码")
    private String invoiceNo;

    @ApiModelProperty(value = "买方单位")
    private String buyerName;

    @ApiModelProperty(value = "签约代表")
    private String buyerDeputy;

    @ApiModelProperty(value = "电话")
    private String buyerTel;

    @ApiModelProperty(value = "开户行")
    private String buyerBank;

    @ApiModelProperty(value = "帐号")
    private String buyerBankNo;

    @ApiModelProperty(value = "地址")
    private String buyerBankAddress;

    @ApiModelProperty(value = "卖方单位")
    private String sellerName;

    @ApiModelProperty(value = "签约代表")
    private String sellerDeputy;

    @ApiModelProperty(value = "电话")
    private String sellerTel;

    @ApiModelProperty(value = "开户行")
    private String sellerBank;

    @ApiModelProperty(value = "帐号")
    private String sellerBankNo;

    @ApiModelProperty(value = "地址")
    private String sellerBankAddress;

    @ApiModelProperty(value = "代理费")
    private BigDecimal agentFee;

    @ApiModelProperty(value = "代垫费")
    private BigDecimal matFee;

    @ApiModelProperty(value = "票差调整金额")
    private BigDecimal differenceVal;

    @ApiModelProperty(value = "采购合同金额")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "收票金额")
    private BigDecimal invoiceVal;

    @ApiModelProperty(value = "结汇说明")
    private String settleAccountInfo;

    @ApiModelProperty(value = "是否结汇")
    private Boolean isSettleAccount;

    @ApiModelProperty(value = "汇率")
    private BigDecimal exchangeRate;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "跟进商务")
    private String busPersonName;

    private BigDecimal actualDrawbackValue;// 可退税金额
    private BigDecimal alreadyDrawbackValue;// 已退税金额

    private String statusDesc;

    public String getStatusDesc( ) {
        return Optional.ofNullable(PurchaseContractStatusEnum.of(statusId)).map(PurchaseContractStatusEnum::getName).orElse("");
    }

    /**
     * 是否函调
     */

    private Boolean isCorrespondence;

    /**
     * 是否回函
     */

    private String replyStatus;

    /**
     * 回函日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime replyDate;

    /**
     * 退税申报日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime taxDeclareTime;

    /**
     * 退税申报金额
     */

    private BigDecimal taxDeclareAmount;

    /**
     * 税局退税日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime taxRefundTime;

    /**
     * 税局退税金额
     */

    private BigDecimal taxRefundAmount;

    /**
     * 税局退税备注
     */

    private String taxRefundRemark;


}
