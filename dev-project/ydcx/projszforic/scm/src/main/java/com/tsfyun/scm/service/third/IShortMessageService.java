package com.tsfyun.scm.service.third;

import com.tsfyun.common.base.dto.ShortMessageDTO;
import com.tsfyun.common.base.dto.SmsNoticeMessageDTO;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.scm.entity.third.LogSms;

/**=
 * 短信模块
 */
public interface IShortMessageService {
    /**=
     * 发送验证码类短信
     * @param dto
     * @param messageNode
     * @param ip
     */
    void sendVerificationCode(ShortMessageDTO dto, MessageNodeEnum messageNode, String ip);

    /**=
     * 验证编码是否正确
     * @param phoneNo
     * @param messageNode
     * @param code
     * @return
     */
    void checkVerificationCode(String phoneNo, MessageNodeEnum messageNode, String code);

    /**
     * 发送提醒短信
     * @param dto
     */
    void sendNoticeMessage(SmsNoticeMessageDTO dto);
}
