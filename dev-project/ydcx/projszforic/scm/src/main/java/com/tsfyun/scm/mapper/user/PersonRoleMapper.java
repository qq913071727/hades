package com.tsfyun.scm.mapper.user;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.entity.user.PersonRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRoleMapper extends Mapper<PersonRole> {

    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatchByRoleIds(List<String> roleIds);

}
