package com.tsfyun.scm.dto.logistics;

import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.BillTypeEnum;
import com.tsfyun.common.base.enums.logistics.CarTypeEnum;
import com.tsfyun.common.base.enums.logistics.TransportDestinationEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 跨境运输单请求实体
 * </p>
 *

 * @since 2020-04-17
 */
@Data
@ApiModel(value="CrossBorderWaybill请求对象", description="跨境运输单请求实体")
public class CrossBorderWaybillDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "数据id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotNull(message = "单据类型不能为空")
   @EnumCheck(clazz = BillTypeEnum.class,message = "单据类型错误")
   private String billType;

   @NotNull(message = "运输日期不能为空")
   @ApiModelProperty(value = "运输日期")
   private LocalDateTime transDate;

   @NotEmptyTrim(message = "运单号不能为空")
   @LengthTrim(max = 50,message = "运单号最大长度不能超过50位")
   @ApiModelProperty(value = "运单号")
   private String waybillNo;

   @LengthTrim(max = 10,message = "通关口岸编号最大长度不能超过10位")
   @ApiModelProperty(value = "通关口岸编号")
   private String customsCode;
   @LengthTrim(max = 50,message = "通关口岸名称最大长度不能超过50位")
   @ApiModelProperty(value = "通关口岸名称")
   private String customsName;

   @NotNull(message = "运输供应商不能为空")
   @ApiModelProperty(value = "运输供应商")
   private Long transportSupplierId;

   @NotNull(message = "运输工具不能为空")
   @ApiModelProperty(value = "运输工具")
   private Long conveyanceId;

   @NotEmptyTrim(message = "订车类型不能为空")
   @EnumCheck(clazz = CarTypeEnum.class,message = "订车类型错误")
   @ApiModelProperty(value = "订车类型")
   private String conveyanceType;

   @NotEmptyTrim(message = "请选择送货目地")
   @EnumCheck(clazz = TransportDestinationEnum.class,message = "送货目地错误")
   @ApiModelProperty(value = "送货目地")
   private String transportDestination;

   @LengthTrim(max = 50,message = "集装箱号最大长度不能超过50位")
   @ApiModelProperty(value = "集装箱号")
   private String containerNo;

   @NotNull(message = "发货仓库不能为空")
   @ApiModelProperty(value = "发货仓库id")
   private Long shippingWareId;

   @ApiModelProperty(value = "收货仓库id")
   private Long deliveryWareId;

   @LengthTrim(max = 200,message = "收货公司最大长度不能超过200位")
   @ApiModelProperty(value = "收货公司")
   private String deliveryCompany;

   @LengthTrim(max = 50,message = "收货联系人最大长度不能超过50位")
   @ApiModelProperty(value = "收货联系人")
   private String deliveryLinkPerson;

   @LengthTrim(max = 50,message = "收货联系电话最大长度不能超过50位")
   @ApiModelProperty(value = "收货联系电话")
   private String deliveryLinkTel;

   @LengthTrim(max = 255,message = "收货地址最大长度不能超过255位")
   @ApiModelProperty(value = "收货地址")
   private String deliveryAddress;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;

}
