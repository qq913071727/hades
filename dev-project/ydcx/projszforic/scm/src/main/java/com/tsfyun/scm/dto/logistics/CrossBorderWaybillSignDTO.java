package com.tsfyun.scm.dto.logistics;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 跨境运输单签收
 * @since Created in 2020/5/14 16:31
 */
@Data
public class CrossBorderWaybillSignDTO implements Serializable {

    /**
     * 跨境运输单id
     */
    @NotNull(message = "跨境运输单ID不能为空")
    private Long id;

    /**
     * 签收时间
     */
    @NotNull(message = "签收时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime reachDate;

}
