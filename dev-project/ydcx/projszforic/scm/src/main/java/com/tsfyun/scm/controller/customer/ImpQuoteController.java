package com.tsfyun.scm.controller.customer;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

/**
 * <p>
 * 进口报价 前端控制器
 * </p>
 *
 *
 * @since 2020-04-01
 */
@RestController
@RequestMapping("/impQuote")
public class ImpQuoteController extends BaseController {

}

