package com.tsfyun.scm.vo.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/15 10:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeixinFocusScanVO implements Serializable {

    /*
       1-二维码已失效；2-成功；3-失败; 4-使用其他已绑定微信帐号扫描; 5-操作非法; 6-扫码用户不对提醒
     */
    private Integer code;

    private String msg;

}
