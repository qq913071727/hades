package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tsfyun.common.base.extension.IdWorker;
import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 发票或内贸合同费用明细
 * </p>
 *

 * @since 2020-05-18
 */
@Data
public class InvoiceCostMember {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 单据id
     */

    private String docId;

    /**
     * 单据编号
     */

    private String docNo;

    /**
     * 单据类型
     */

    private String docType;

    /**
     * 费用科目编码
     */

    private String expenseSubjectId;

    /**
     * 费用科目名称
     */

    private String expenseSubjectName;


    /**
     * 费用
     */

    private BigDecimal costMoney;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 备注
     */

    private String memo;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;


}
