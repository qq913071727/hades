package com.tsfyun.scm.controller.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.CustomerDeliveryInfoDTO;
import com.tsfyun.scm.dto.customer.SupplierTakeInfoQTO;
import com.tsfyun.scm.service.customer.ICustomerDeliveryInfoService;
import com.tsfyun.scm.vo.customer.CustomerDeliveryInfoVO;
import com.tsfyun.scm.vo.customer.SupplierTakeInfoListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 客户收货地址
 */
@RestController
@RequestMapping(value = "/customerDeliveryInfo")
public class CustomerDeliveryInfoController extends BaseController {

    @Autowired
    private ICustomerDeliveryInfoService customerDeliveryInfoService;


    /**
     * 新增
     * @param dto
     * @return
     */
    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute  @Validated(AddGroup.class) CustomerDeliveryInfoDTO dto) {
        customerDeliveryInfoService.add(dto,true);
        return success();
    }

    /**
     * 修改
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) CustomerDeliveryInfoDTO dto) {
        customerDeliveryInfoService.edit(dto,true);
        return success();
    }

    /**
     * 激活禁用
     * @param id
     * @return
     */
    @PostMapping(value = "activeDisable")
    Result<Void> activeDisable(@RequestParam(value = "id")Long id) {
        customerDeliveryInfoService.activeDisable(id);
        return success();
    }

    /**
     * 设置默认
     * @param id
     * @return
     */
    @PostMapping(value = "setDefault")
    Result<Void> setDefault(@RequestParam(value = "id")Long id) {
        customerDeliveryInfoService.setDefault(id);
        return success();
    }

    /**
     * 获取客户收货地址（所有的客户收货地址，不分页）
     * @param customerId
     * @return
     */
    @GetMapping(value = "select")
    Result<List<CustomerDeliveryInfoVO>> select(@RequestParam(value = "customerId")Long customerId) {
        return success(customerDeliveryInfoService.select(customerId));
    }

    /**
     * 带权限分页查询客户收货地址信息（查询条件customerName-客户名称;companyName-收货公司;linkPerson-联系人;linkTel-联系电话)
     * @param dto
     * @return
     */
    @PostMapping(value = "page")
    Result<List<CustomerDeliveryInfoVO>> page(CustomerDeliveryInfoDTO dto) {
        PageInfo<CustomerDeliveryInfoVO> page = customerDeliveryInfoService.pageList(dto);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 获取客户地址详细信息
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<CustomerDeliveryInfoVO> detail(@RequestParam(value = "id")Long id) {
        return success(customerDeliveryInfoService.detail(id));
    }

    /**
     * 删除客户收货信息（真删，不做数据关联校验）
     */
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id")Long id){
        customerDeliveryInfoService.deleteById(id);
        return success();
    }

    /**
     * 根据客户名称查询有效的收货地址
     * @return
     */
    @PostMapping(value = "effectiveList")
    Result<List<CustomerDeliveryInfoVO>> effectiveList(@RequestParam(value = "customerName")String customerName) {
        return success(customerDeliveryInfoService.effectiveList(customerName));
    }


}
