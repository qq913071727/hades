package com.tsfyun.scm.service.system;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.system.SysRoleMenu;

import java.util.List;

public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 保存角色菜单信息
     * @param roleId
     * @param menuIdList
     */
    void saveOrUpdate(String roleId, List<String> menuIdList);

    /**
     * 根据角色ID，获取菜单ID列表
     */
    List<String> queryMenusByRoleId(String roleId);

    /**
     * 根据角色ID，删除
     */
    void deleteByRoleId(String roleId);

    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatchByRoleIds(List<String> roleIds);

}
