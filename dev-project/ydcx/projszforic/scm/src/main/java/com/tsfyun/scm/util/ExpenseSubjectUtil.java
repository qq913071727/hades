package com.tsfyun.scm.util;

import com.tsfyun.common.base.enums.finance.CostClassifyEnum;
import com.tsfyun.common.base.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * @Description: 费用科目工具
 * @CreateDate: Created in 2020/5/28 09:26
 */
@Slf4j
public class ExpenseSubjectUtil {

    /**
     * 根据费用科目编码得出费用分类
     * @param expenseSubjectId
     * @return
     */
    public static String getCostClassify(String expenseSubjectId){
        if(StringUtils.isEmpty(expenseSubjectId)) {
            return null;
        }
        CostClassifyEnum costClassifyEnum = null;
        switch (expenseSubjectId) {
            case "A0005": //货款
                costClassifyEnum = CostClassifyEnum.HK;
                break;
            case "A0002": //关税
                costClassifyEnum = CostClassifyEnum.SK;
                break;
            case "A0003": //增值税
                costClassifyEnum = CostClassifyEnum.SK;
                break;
            case "A0004": //消费税
                costClassifyEnum = CostClassifyEnum.SK;
                break;
            case "A0001": //代理费
                costClassifyEnum = CostClassifyEnum.DZF;
                break;
            default:
                costClassifyEnum = CostClassifyEnum.DZF;
                break;
        }
        if(Objects.nonNull(costClassifyEnum)) {
            return costClassifyEnum.getCode();
        }
        log.error("费用科目【{}】未匹配到费用分类",expenseSubjectId);
        return null;
    }

}
