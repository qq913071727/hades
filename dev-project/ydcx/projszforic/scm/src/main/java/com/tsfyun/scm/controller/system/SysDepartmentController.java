package com.tsfyun.scm.controller.system;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.system.DepartmentDTO;
import com.tsfyun.scm.service.system.ISysDepartmentService;
import com.tsfyun.scm.util.UIDepartment;
import com.tsfyun.scm.vo.system.DepartmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "department")
public class SysDepartmentController extends BaseController {

    @Autowired
    private ISysDepartmentService sysDepartmentService;

    /**
     * 获取所有的部门树
     * @return
     */
    @GetMapping(value = "/tree")
    Result<List<UIDepartment>> departmentTree() {
        return success(sysDepartmentService.getAllDepartmentTree(true));
    }

    /**
     * 获取所有的部门列表
     * @return
     */
    @GetMapping(value = "/list")
    Result<List<UIDepartment>> list() {
        return success(sysDepartmentService.getAllDepartmentTree(false));
    }

    @PostMapping(value = "add")
    Result<Void> add(@ModelAttribute @Validated(AddGroup.class) DepartmentDTO dto) {
        sysDepartmentService.add(dto);
        return success();
    }

    /**
     * 修改部门
     * @param dto
     * @return
     */
    @PostMapping(value = "edit")
    Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) DepartmentDTO dto) {
        sysDepartmentService.edit(dto);
        return success();
    }

    /**
     * 删除部门
     * @param id
     * @return
     */
    @PostMapping(value = "delete")
    Result<Void> delete(@RequestParam(value = "id")Long id) {
        sysDepartmentService.delete(id);
        return success();
    }

    /**
     * 部门详情
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    Result<DepartmentVO> detail(@RequestParam(value = "id")Long id) {
        return success(sysDepartmentService.detail(id));
    }


}
