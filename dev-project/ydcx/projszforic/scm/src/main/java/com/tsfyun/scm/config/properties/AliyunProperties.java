package com.tsfyun.scm.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Description: 腾讯云
 * @CreateDate: Created in 2021/9/15 14:23
 */
@RefreshScope
@Data
@Component
@ConfigurationProperties(prefix = AliyunProperties.PREFIX)
public class AliyunProperties {

    public static final String PREFIX = "aly.sms";

    private String signName;

    private String accessKeyId;

    private String accessKeySecret;

    private String domain = "dysmsapi.aliyuncs.com";
}
