package com.tsfyun.scm.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/28 09:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpOrderPlusVO implements Serializable {

    private ExpOrderVO order;

    private ExpOrderLogisticsVO logistiscs;

    private List<ExpOrderMemberVO> members;
}
