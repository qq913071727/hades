package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class TaxCodeDTO implements Serializable {

    @NotNull(message = "产品ID不能为空")
    private Long mid;

    @NotEmptyTrim(message = "税收分类编码不能为空")
    private String taxCode;

    @NotEmptyTrim(message = "开票名称不能为空")
    private String invoiceName;

    @NotEmptyTrim(message = "物料名称不能为空")
    private String goodsName;
}
