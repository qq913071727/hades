package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class InsidePersonSaveDTO implements Serializable {

    /**
     * 员工id
     */
    @NotNull(message = "员工id不能为空",groups = UpdateGroup.class)
    private Long id;

    /**
     * 员工编号
     */
    @NotEmptyTrim(message = "员工编号不能为空")
    @LengthTrim(min = 4,max = 20,message = "员工编号只能为4-20位")
    private String code;

    public void setCode(String code){
        this.code = code;
    }

    /**
     * 员工名称
     */
    @NotEmptyTrim(message = "员工名称不能为空")
    @LengthTrim(min = 2,max = 20,message = "员工名称只能为2-20位")
    private String name;


    /**
     * 邮箱
     */
    private String mail;

    public void setMail(String mail){
        this.mail = mail;
    }

    /**
     * 手机号
     */
    private String phone;

    public void setPhone(String phone){
        this.phone = phone;
    }

    /**
     * 头像
     */
    private String head;


    /**
     * 密码，修改密码考虑只能自己修改，管理员不能修改，可以让管理员重置
     */
    /*
    @Length(min = 6,max = 16,message = "密码长度只能为6-16位")
    private String passWord;
     */


    /**
     * 选定的角色
     */
    private List<Long> roleIds;

    @NotNull(message = "部门不能为空")
    private Long departmentId;

    /**
     * 职位
     */
    @NotEmptyTrim(message = "职位不能为空")
    private String position;

    /**
     * 在职状态
     */
    @NotNull(message = "在职状态不能为空")
    private Boolean incumbency;

    /**
     * 性别，见枚举SexTypeEnum
     */
    private String gender;

    /**
     * 备注
     */
    private String memo;




}
