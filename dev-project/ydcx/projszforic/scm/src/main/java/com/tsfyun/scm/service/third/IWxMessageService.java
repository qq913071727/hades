package com.tsfyun.scm.service.third;

import com.tsfyun.common.base.dto.WxNoticeMessageDTO;
import com.tsfyun.scm.dto.support.SendWxDTO;
import com.tsfyun.scm.vo.support.WxFocusUserInfoVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 微信公众号推送通知
 * @CreateDate: Created in 2020/12/23 11:08
 */
public interface IWxMessageService {

    /**
     * 发送微信通知消息（业务方法调用）
     * @param dto
     */
    void sendWxNotice(WxNoticeMessageDTO dto);

    /**
     * MQ消费消息 发送通知至微信公众平台
     * @param dto
     */
    void send2Wx(SendWxDTO dto);

    /**
     * 处理微信公众号事件消息
     * @param request
     */
    String handleEvent(HttpServletRequest request, HttpServletResponse response);

}
