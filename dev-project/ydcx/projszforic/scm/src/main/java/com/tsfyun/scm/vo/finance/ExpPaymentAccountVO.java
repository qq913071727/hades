package com.tsfyun.scm.vo.finance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.domain.ExpPaymentAccountStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 出口付款单响应实体
 * </p>
 *
 *
 * @since 2021-10-18
 */
@Data
@ApiModel(value="ExpPaymentAccount响应对象", description="出口付款单响应实体")
public class ExpPaymentAccountVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "系统单号")
    private String docNo;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    private String customerName;

    @ApiModelProperty(value = "收款单ID")
    private Long overseasAccountId;

    @ApiModelProperty(value = "收款单号")
    private String accountNo;

    @ApiModelProperty(value = "付款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "订单号")
    private String orderNo;
    private String clientNo;

    @ApiModelProperty(value = "状态")
    private String statusId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "申请时间")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "要求付款时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date claimPaymentDate;

    @ApiModelProperty(value = "实际付款时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date actualPaymentDate;

    @ApiModelProperty(value = "付款主体")
    private String subjectType;

    @ApiModelProperty(value = "付款主体")
    private Long subjectId;

    @ApiModelProperty(value = "付款主体")
    private String subjectName;

    @ApiModelProperty(value = "付款银行")
    private String subjectBank;

    @ApiModelProperty(value = "付款银行账号")
    private String subjectBankNo;

    @ApiModelProperty(value = "收款方")
    private String payeeName;

    @ApiModelProperty(value = "收款银行")
    private String payeeBank;

    @ApiModelProperty(value = "收款银行账号")
    private String payeeBankNo;

    @ApiModelProperty(value = "备注")
    private String memo;

    private String statusDesc;

    public String getStatusDesc() {
        return Optional.of(ExpPaymentAccountStatusEnum.of(statusId)).map(ExpPaymentAccountStatusEnum::getName).orElse("");
    }


}
