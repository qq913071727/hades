package com.tsfyun.scm.vo.customs;


import com.tsfyun.scm.system.vo.CustomsElementsVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * @Description: 报关单明细获取申报要素信息
 * @since Created in 2020/5/6 09:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeclarationMemberCustomsElementsVO {

    private DeclarationMemberVO member;

    private List<CustomsElementsVO> elements;

}
