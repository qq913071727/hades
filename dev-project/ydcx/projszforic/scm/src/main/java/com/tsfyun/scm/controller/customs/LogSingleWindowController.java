package com.tsfyun.scm.controller.customs;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.entity.customs.LogSingleWindow;
import com.tsfyun.scm.service.customs.ILogSingleWindowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/singleWindow")
public class LogSingleWindowController extends BaseController {

    @Autowired
    private ILogSingleWindowService logSingleWindowService;

    /**=
     * 单一窗口日志
     * @param domainId
     * @param domainType
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<LogSingleWindow>> singleWindowList(@RequestParam(value = "domainId")String domainId, @RequestParam(value = "domainType")String domainType){
        return success(logSingleWindowService.singleWindowList(domainId,domainType));
    }
}
