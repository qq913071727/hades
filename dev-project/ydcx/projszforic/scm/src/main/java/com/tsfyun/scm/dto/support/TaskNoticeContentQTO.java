package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 任务通知内容请求实体
 * </p>
 *

 * @since 2020-04-05
 */
@Data
@ApiModel(value="TaskNoticeContent请求对象", description="任务通知内容请求实体")
public class TaskNoticeContentQTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String documentType;

   private String documentId;

   private String operationCode;

   private String title;

   private String content;

   private Long receiverId;


}
