package com.tsfyun.scm.vo.customs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 报关行响应实体
 * </p>
 *
 *
 * @since 2021-11-15
 */
@Data
@ApiModel(value="DeclarationBroker响应对象", description="报关行响应实体")
public class DeclarationBrokerVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "统一信用代码")
    private String socialNo;

    @ApiModelProperty(value = "注册海关编码")
    private String customsNo;

    @ApiModelProperty(value = "禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "名称")
    private String name;


}
