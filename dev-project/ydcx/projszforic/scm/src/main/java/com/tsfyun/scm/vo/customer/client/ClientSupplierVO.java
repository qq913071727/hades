package com.tsfyun.scm.vo.customer.client;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/28 10:22
 */
@Data
public class ClientSupplierVO {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

}
