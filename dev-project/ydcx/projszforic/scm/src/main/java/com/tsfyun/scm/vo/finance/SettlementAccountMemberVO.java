package com.tsfyun.scm.vo.finance;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 结汇明细响应实体
 * </p>
 *
 *
 * @since 2021-10-14
 */
@Data
@ApiModel(value="SettlementAccountMember响应对象", description="结汇明细响应实体")
public class SettlementAccountMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

     @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单日期")
    private LocalDateTime orderDate;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "收款金额")
    private BigDecimal accountValue;

    @ApiModelProperty(value = "订单币制")
    private String orderCurrencyName;

    @ApiModelProperty(value = "结汇主单")
    private Long settlementId;

    @ApiModelProperty(value = "收款单ID")
    private Long accountId;

    @ApiModelProperty(value = "收款单号")
    private String accountNo;


}
