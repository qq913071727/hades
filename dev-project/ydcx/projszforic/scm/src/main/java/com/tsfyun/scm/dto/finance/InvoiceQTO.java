package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: 发票查询请求实体
 * @since Created in 2020/5/15 15:58
 */
@Data
public class InvoiceQTO extends PaginationDto implements Serializable {

    /**
     * 买方
     */
    private String customerName;

    /**
     * 单号
     */
    private String docNo;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 发票号
     */
    private String invoiceNo;

    /**
     * 状态
     */
    private String statusId;

    /**
     * 结算类型
     */
    private String taxType;


    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }


}
