package com.tsfyun.scm.vo.system;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.NoticeCategoryEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-09-22
 */
@Data
@ApiModel(value="SysNotice响应对象", description="响应实体")
public class SysNoticeVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "公告类别")
    private String category;

    @ApiModelProperty(value = "公告标题")
    private String title;

    @ApiModelProperty(value = "公告描述")
    private String represent;

    @ApiModelProperty(value = "公告内容")
    private String content;

    @ApiModelProperty(value = "banner图链接")
    private String banner;

    @ApiModelProperty(value = "公告是否对外公开")
    private Boolean isOpen;

    @ApiModelProperty(value = "公告是否登录查看")
    private Boolean isLoginSee;

    @ApiModelProperty(value = "公告是否弹出显示")
    private Boolean isPopup;

    @ApiModelProperty(value = "公告时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime publishTime;

    private String categoryDesc;

    public String getCategoryDesc() {
        NoticeCategoryEnum noticeCategoryEnum = NoticeCategoryEnum.of(category);
        return Optional.ofNullable(noticeCategoryEnum).map(NoticeCategoryEnum::getName).orElse("");
    }


}
