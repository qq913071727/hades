package com.tsfyun.scm.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;

import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsfyun.scm.entity.system.SysDepartment;
import lombok.Data;

@Data
public class UIDepartment {

    private static Integer count = 0;

    private Long id; //部门id

    private Long parentId;//父部门id

    private String code;//编码

    private String name;//名称

    @JsonIgnore
    private Integer sort;//排序

    @JsonIgnore
    private Integer level;//菜单级别

    @JsonIgnore
    private boolean leaf; //是否子部门

    //子数据，临时非数据库字段
    @Transient
    private List<UIDepartment> childs;

    public UIDepartment() {

    }

    /**
     * 构建所有组织树
     * @param departments
     */
    public static List<UIDepartment> buildTree(List<SysDepartment> departments){
        List<UIDepartment> treeList = new ArrayList<UIDepartment>(departments.size());
        departments.stream().forEach(r -> {
            //根菜单（查询时已过滤）
            if(null == r.getParentId() || 0 == r.getParentId()) {
                UIDepartment tree = new UIDepartment();
                tree.setId(r.getId());
                tree.setName(r.getName());
                tree.setCode(r.getCode());
                tree.setSort(r.getSort());
                tree.setParentId(r.getParentId());
                tree.setLevel(count);
                treeList.add(buildTree(tree, departments));
                count = 0;
            }
        });
        return  treeList;
    }

    /**
     * 构建单个树
     * @param tree 根
     * @param departments 所有部门
     * @return
     */
    public static UIDepartment buildTree(UIDepartment tree,List<SysDepartment> departments){
        count ++ ;
        List<UIDepartment> sub = sub(tree,departments);
        for(UIDepartment child:sub){
            UIDepartment t = buildTree(child,departments);
            if(tree.childs != null){
                tree.childs.add(t);
            }else {
                tree.childs = new ArrayList<UIDepartment>(){
                    {
                        add(t) ;
                    }
                };
            }
        }
        return  tree;
    }

    //获取部门的子部门
    public static List<UIDepartment> sub(UIDepartment r,List<SysDepartment> departments){
        List<UIDepartment> sub = new ArrayList<>(departments.size());
        //获取部门r的子部门
        for(SysDepartment department : departments){
            if(r.getId().equals(department.getParentId())){
                UIDepartment tree = new UIDepartment();
                tree.setId(department.getId());
                tree.setCode(department.getCode());
                tree.setName(department.getName());
                tree.setParentId(department.getParentId());
                tree.setLeaf(null != tree.getParentId());
                tree.setSort(department.getSort());
                tree.setLevel(count);
                sub.add(tree);
            }
        }
        return  sub;
    }

    /**
     * 递归获取所有的子部门id
     * @param ids
     * @param childDepartments
     */
    public static void getChildDepartmentIds(List<Long> ids,List<UIDepartment> childDepartments) {
        if(CollectionUtil.isNotEmpty(childDepartments)) {
            for(UIDepartment uiDepartment : childDepartments) {
                ids.add(uiDepartment.getId());
                getChildDepartmentIds(ids,uiDepartment.getChilds());
            }
        }
    }

    public static void getChildDepartmentMap(Map<Long, UIDepartment> uiDepartmentMap,List<UIDepartment> childDepartments) {
        if(CollectionUtil.isNotEmpty(childDepartments)) {
            for(UIDepartment uiDepartment : childDepartments) {
                uiDepartmentMap.put(uiDepartment.getId(),uiDepartment);
                getChildDepartmentMap(uiDepartmentMap,uiDepartment.getChilds());
            }
        }
    }

}
