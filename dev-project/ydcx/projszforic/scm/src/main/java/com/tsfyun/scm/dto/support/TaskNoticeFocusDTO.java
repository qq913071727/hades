package com.tsfyun.scm.dto.support;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import com.tsfyun.common.base.annotation.LengthTrim;

/**
 * <p>
 * 用户关注任务通知请求实体
 * </p>
 *

 * @since 2020-04-09
 */
@Data
@ApiModel(value="TaskNoticeFocus请求对象", description="用户关注任务通知请求实体")
public class TaskNoticeFocusDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "操作码不能为空")
   @LengthTrim(max = 50,message = "操作码最大长度不能超过50位")
   @ApiModelProperty(value = "操作码")
   private String operationCode;


}
