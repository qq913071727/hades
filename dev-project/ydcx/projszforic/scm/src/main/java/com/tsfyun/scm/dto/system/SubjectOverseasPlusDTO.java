package com.tsfyun.scm.dto.system;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class SubjectOverseasPlusDTO implements Serializable {

    /**
     * 境外公司信息
     */
    @NotNull(message = "请填写供应商信息")
    @Valid
    private SubjectOverseasDTO overseas;

    /**
     * 境外公司银行信息
     */
    @Valid
    private List<SubjectOverseasBankDTO> banks;
}
