package com.tsfyun.scm.mapper.finance;

import com.tsfyun.common.base.annotation.DataScope;
import com.tsfyun.scm.entity.finance.PurchaseContract;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.PurchaseContractVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购合同 Mapper 接口
 * </p>
 *
 *
 * @since 2021-09-26
 */
@Repository
public interface PurchaseContractMapper extends Mapper<PurchaseContract> {

    @DataScope(tableAlias = "d",customerTableAlias = "c")
    List<PurchaseContractVO> list(Map<String,Object> params);

    List<PurchaseContract> findByIds(@Param(value = "ids") List<Long> ids);
}
