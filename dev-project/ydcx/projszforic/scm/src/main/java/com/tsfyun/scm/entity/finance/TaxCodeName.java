package com.tsfyun.scm.entity.finance;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 客户税收分类编码
 * </p>
 *

 * @since 2020-05-21
 */
@Data
public class TaxCodeName implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;


    /**
     * 客户id
     */

    private Long customerId;

    /**
     * 客户名称
     */

    private String customerName;

    /**
     * 物料名称
     */

    private String name;

    /**
     * 开票名称
     */

    private String invoiceName;

    /**
     * 税收分类编码
     */

    private String code;

    /**
     * 税收分类编码名称
     */
    private String codeName;

}
