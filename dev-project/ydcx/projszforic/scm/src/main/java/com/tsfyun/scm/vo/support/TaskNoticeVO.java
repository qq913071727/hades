package com.tsfyun.scm.vo.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 任务通知响应实体
 * </p>
 *

 * @since 2020-04-05
 */
@Data
@ApiModel(value="TaskNotice响应对象", description="任务通知响应实体")
public class TaskNoticeVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "任务通知内容id")
    private Long taskNoticeId;

    @ApiModelProperty(value = "提交任务通知用户id")
    private Long senderId;

    @ApiModelProperty(value = "提交任务通知用户名称")
    private String senderName;

    @ApiModelProperty(value = "接收用户id")
    private Long receiverId;

    @ApiModelProperty(value = "接收用户名称")
    private String receiverName;


}
