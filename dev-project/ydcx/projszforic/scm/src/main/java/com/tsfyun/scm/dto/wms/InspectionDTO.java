package com.tsfyun.scm.dto.wms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class InspectionDTO implements Serializable {

    @NotNull(message = "入库单ID")
    @ApiModelProperty(value = "入库单ID不能为空")
    private Long id;

    @ApiModelProperty(value = "入库单明细")
    @Valid
    List<InspectionMemberDTO> members;
}
