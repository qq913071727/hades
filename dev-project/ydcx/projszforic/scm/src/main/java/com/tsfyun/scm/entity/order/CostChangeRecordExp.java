package com.tsfyun.scm.entity.order;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;


/**
 * <p>
 * 费用变更记录
 * </p>
 *

 * @since 2020-05-27
 */
@Data
public class CostChangeRecordExp extends BaseEntity {

     private static final long serialVersionUID=1L;

     private Long expOrderId;

    /**
     * 订单号
     */

    private String expOrderNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 费用科目编码
     */

    private String expenseSubjectId;

    /**
     * 费用科目名称
     */

    private String expenseSubjectName;

    /**
     * 原始金额
     */

    private BigDecimal originCostAmount;

    /**
     * 变更后金额
     */

    private BigDecimal costAmount;

    /**
     * 备注
     */

    private String memo;


}
