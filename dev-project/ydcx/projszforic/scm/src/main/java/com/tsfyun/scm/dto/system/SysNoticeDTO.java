package com.tsfyun.scm.dto.system;

import java.time.LocalDateTime;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.NoticeCategoryEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

import javax.validation.constraints.NotNull;


/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-09-22
 */
@Data
@ApiModel(value="SysNotice请求对象", description="请求实体")
public class SysNoticeDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "公告id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "公告分类不能为空")
   @EnumCheck(clazz = NoticeCategoryEnum.class,message = "公告分类错误")
   @ApiModelProperty(value = "公告分类")
   private String category;

   @NotEmptyTrim(message = "公告标题不能为空")
   @LengthTrim(max = 100,message = "公告标题最大长度不能超过100位")
   @ApiModelProperty(value = "公告标题")
   private String title;

   @NotEmptyTrim(message = "公告描述不能为空")
   @LengthTrim(max = 255,message = "公告描述最大长度不能超过100位")
   @ApiModelProperty(value = "公告描述")
   private String represent;

   @NotEmptyTrim(message = "公告内容不能为空")
   @ApiModelProperty(value = "公告内容")
   private String content;

   @LengthTrim(max = 255,message = "banner图链接最大长度不能超过255位")
   @ApiModelProperty(value = "banner图链接")
   private String banner;

   @NotNull(message = "公告是否对外公开不能为空")
   @ApiModelProperty(value = "公告是否对外公开")
   private Boolean isOpen;

   @NotNull(message = "公告是否允许登录查看不能为空")
   @ApiModelProperty(value = "公告是否允许登录查看")
   private Boolean isLoginSee;

   @NotNull(message = "公告是否弹出显示不能为空")
   @ApiModelProperty(value = "公告是否弹出显示")
   private Boolean isPopup;

   @ApiModelProperty(value = "公告时间")
   @DateTimeFormat(pattern = "yyyy-MM-dd")
   private LocalDateTime publishTime;

   /**
    * banner图文件关联id
    */
   private String docId;

   /**
    * banner图文件类型
    */
   private String docType;



}
