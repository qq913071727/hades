package com.tsfyun.scm.entity.logistics;

import java.math.BigDecimal;
import com.tsfyun.common.base.extension.IdWorker;
import tk.mybatis.mapper.annotation.KeySql;
import javax.persistence.Id;
import java.io.Serializable;
import lombok.Data;


/**
 * <p>
 * 香港送货单明细
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Data
public class OverseasDeliveryNoteMember implements Serializable {

     private static final long serialVersionUID=1L;


    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 订单明细id
     */
    private Long orderMemberId;

    /**
     * 订单编号
     */
    private String expOrderNo;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 型号
     */

    private String model;

    /**
     * 名称
     */

    private String name;

    /**
     * 规格参数
     */

    private String spec;


    /**
     * 单位
     */

    private String unitCode;

    /**
     * 单位
     */

    private String unitName;

    /**
     * 数量
     */

    private BigDecimal quantity;

    /**
     * 净重
     */

    private BigDecimal netWeight;

    /**
     * 毛重
     */

    private BigDecimal grossWeight;

    /**
     * 箱数
     */

    private Integer cartonNum;

    /**
     * 箱号
     */

    private String cartonNo;

    /**
     * 单价
     */

    private BigDecimal unitPrice;

    /**
     * 总价
     */

    private BigDecimal totalPrice;

    /**
     * 卡板数
     */

    private Integer palletNumber;

    /**
     * 境外送货主单id
     */

    private Long deliveryNoteId;


}
