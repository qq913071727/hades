package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.scm.enums.SubjectTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 境外收款请求实体
 * </p>
 *
 *
 * @since 2021-10-08
 */
@Data
@ApiModel(value="OverseasReceivingAccount请求对象", description="境外收款请求实体")
public class OverseasReceivingAccountDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private Long id;
   @NotNull(message = "收款主体不能为空")
   private Long overseasId;
   @NotNull(message = "收款行不能为空")
   private Long overseasBankId;

   @NotEmptyTrim(message = "收款主体类型不能为空")
   @EnumCheck(clazz = SubjectTypeEnum.class,message = "收款主体类型错误")
   @ApiModelProperty(value = "收款主体类型")
   private String subjectType;

   @NotEmptyTrim(message = "结算客户不能为空")
   @ApiModelProperty(value = "结算客户")
   private String customerName;

   @NotEmptyTrim(message = "付款方不能为空")
   @LengthTrim(max = 100,message = "付款方最大长度不能超过100位")
   @ApiModelProperty(value = "付款方")
   private String payer;

   @NotEmptyTrim(message = "付款方银行不能为空")
   @LengthTrim(max = 100,message = "付款方银行最大长度不能超过100位")
   @ApiModelProperty(value = "付款方银行")
   private String payerBank;

   @NotEmptyTrim(message = "付款方银行账号不能为空")
   @LengthTrim(max = 50,message = "付款方银行账号最大长度不能超过50位")
   @ApiModelProperty(value = "付款方银行账号")
   private String payerBankNo;

   @NotNull(message = "收款金额不能为空")
   @Digits(integer = 17, fraction = 2, message = "收款金额整数位不能超过17位，小数位不能超过2位")
   @DecimalMin(value = "0.01",message = "收款金额必须大于0")
   @ApiModelProperty(value = "收款金额")
   private BigDecimal accountValue;

   @NotEmptyTrim(message = "收款币制不能为空")
   @ApiModelProperty(value = "收款币制")
   private String currencyName;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @ApiModelProperty(value = "文件ID")
   private String fileId;
}
