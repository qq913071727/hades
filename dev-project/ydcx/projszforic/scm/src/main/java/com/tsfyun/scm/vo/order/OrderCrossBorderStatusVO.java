package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/18 15:20
 */
@Data
public class OrderCrossBorderStatusVO implements Serializable {

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 中港运输单编号
     */
    private String transportNo;

    /**
     * 中港运输单状态
     */
    private String transportStatus;

}
