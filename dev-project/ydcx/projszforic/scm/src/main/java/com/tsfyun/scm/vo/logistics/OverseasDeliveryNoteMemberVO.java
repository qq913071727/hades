package com.tsfyun.scm.vo.logistics;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Data
@ApiModel(value="OverseasDeliveryNoteMember响应对象", description="响应实体")
public class OverseasDeliveryNoteMemberVO implements Serializable {

     private static final long serialVersionUID=1L;

    private Long id;

    /**
     * 订单编号
     */
    private String expOrderNo;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "规格参数")
    private String spec;

    @ApiModelProperty(value = "批次号")
    private String batchNo;

    @ApiModelProperty(value = "单位")
    private String unitCode;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "出库数量")
    private BigDecimal quantity;

    @ApiModelProperty(value = "净重")
    private BigDecimal netWeight;

    @ApiModelProperty(value = "毛重")
    private BigDecimal grossWeight;

    @ApiModelProperty(value = "箱数")
    private Integer cartonNum;

    @ApiModelProperty(value = "箱号")
    private String cartonNo;

    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    @ApiModelProperty(value = "总价")
    private BigDecimal totalPrice;

    @ApiModelProperty(value = "卡板数量")
    private Integer palletNumber;

    @ApiModelProperty(value = "境外送货主单id")
    private Long deliveryNoteId;


}
