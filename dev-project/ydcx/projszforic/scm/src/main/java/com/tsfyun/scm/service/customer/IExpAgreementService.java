package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.customer.ExpAgreementPlusDTO;
import com.tsfyun.scm.dto.customer.ExpAgreementQTO;
import com.tsfyun.scm.entity.customer.ExpAgreement;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.ExpAgreementVO;

import java.util.List;

/**
 * <p>
 * 出口协议 服务类
 * </p>
 *
 *
 * @since 2021-09-07
 */
public interface IExpAgreementService extends IService<ExpAgreement> {

    /**
     * 分页查询出口协议
     * @param qto
     * @return
     */
    PageInfo<ExpAgreementVO> pageList(ExpAgreementQTO qto);

    /**
     * 新增
     * @param dto
     */
    Long add(ExpAgreementPlusDTO dto);

    /**=
     * 修改
     * @param dto
     * @return
     */
    void edit(ExpAgreementPlusDTO dto);

    /**
     * 获取详情
     * @param id
     * @param operation
     * @return
     */
    ExpAgreementVO detail(Long id,String operation);

    /**=
     * 审核意见
     * @param dto
     */
    void examine(TaskDTO dto);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**=
     * 原件签回
     * @param ids
     */
    void signBack(List<Long> ids);

    /**
     * 校验获取客户有效出口协议
     * @param customerId
     * @return
     */
    List<ExpAgreementVO> checkCustomerExpAgreement(Long customerId);

    /**
     * 定时扫描失效出口报价
     */
    void scheduleInvalidExpAgreement();

    /**
     * 根据协议id判断协议是否有效
     * @param id
     * @return
     */
    ExpAgreement validationAgreement(Long id);

}
