package com.tsfyun.scm.service.customs;

import com.tsfyun.scm.dto.customs.SaveDeclarationMemberElementsDTO;
import com.tsfyun.scm.entity.customs.DeclarationMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customs.DeclarationMemberCustomsElementsVO;
import com.tsfyun.scm.vo.customs.DeclarationMemberVO;

import java.util.List;

/**
 * <p>
 *  报关单明细服务类
 * </p>
 *
 *
 * @since 2020-04-24
 */
public interface IDeclarationMemberService extends IService<DeclarationMember> {

    /**
     * 根据报关单主单id获取
     * @param declarationId
     * @return
     */
    List<DeclarationMemberVO> findByDeclarationId(Long declarationId);

    /**
     * 报关单明细获取申报要素信息
     * @param id
     * @return
     */
    DeclarationMemberCustomsElementsVO getMemberElements(Long id);

    /**
     * 保存报关单明细申报要素信息
     * @param dto
     */
    void saveMemberElements(SaveDeclarationMemberElementsDTO dto);

    /**
     * 根据报关单ID删除明细
     * @param dclarationId
     */
    void removeByDeclarationId(Long dclarationId);



}
