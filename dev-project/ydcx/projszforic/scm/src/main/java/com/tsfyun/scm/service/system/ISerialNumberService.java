package com.tsfyun.scm.service.system;

import com.tsfyun.common.base.enums.SerialNumberTypeEnum;
import com.tsfyun.scm.dto.system.SerialNumberDTO;
import com.tsfyun.scm.entity.system.SerialNumber;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.system.SerialNumberVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 *
 * @since 2020-03-16
 */
public interface ISerialNumberService extends IService<SerialNumber> {

    List<SerialNumberVO> allList();

    SerialNumberVO detail(String id);

    void edit(SerialNumberDTO dto);

    /**=
     * 根据单据类型生成流水号（后续单量大可以考虑redis单号池化，不必每次从数据库中获取，但需确保重置规则不会经常变化）
     * @param serialNumberTypeEnum
     * @return
     */
    String generateDocNo(SerialNumberTypeEnum serialNumberTypeEnum);
}
