package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *  客户
 * </p>
 *

 * @since 2020-03-03
 */
@Data
public class Customer extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 禁用标示
     */
    private Boolean disabled;

    /**
     * 客户代码
     */
    private String code;

    /**
     * 客户名称
     */
    private String name;

    /**
     * 拼音
     */
    private String pinyin;

    /**
     * 英文名称
     */
    private String nameEn;

    /**
     * 统一社会信用代码(18位)
     */
    private String socialNo;

    /**
     * 税务登记证
     */
    private String taxpayerNo;

    /**
     * 海关注册编码
     */
    private String customsCode;

    /**
     * 检验检疫编码
     */
    private String ciqNo;

    /**
     * 公司法人
     */
    private String legalPerson;

    /**
     * 公司电话
     */
    private String tel;

    /**
     * 公司传真
     */
    private String fax;

    /**
     * 公司地址
     */
    private String address;

    /**
     * 公司地址英文
     */
    private String addressEn;

    /**
     * 销售员工
     */
    private Long salePersonId;

    /**
     * 主要商务
     */
    private Long busPersonId;

    /**
     * 辅助商务
     */
    private Long busSecPersonId;

    /**
     * 状态编码
     */
    private String statusId;

    /**
     * 开户银行
     */
    private String invoiceBankName;

    /**
     * 银行帐号
     */
    private String invoiceBankAccount;

    /**
     * 开票地址
     */
    private String invoiceBankAddress;

    /**=
     * 开票要求备注
     */
    private String invoiceMemo;

    /**
     * 开票电话
     */
    private String invoiceBankTel;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系电话
     */
    private String linkTel;

    /**
     * 联系地址
     */
    private String linkAddress;

    /**
     * 认领时间
     */
    private LocalDateTime claimDate;

    /**
     * (首次)签约时间
     */
    private LocalDateTime signDate;

    /**
     * 首单时间
     */
    private LocalDateTime firstOrderDate;

    /**
     * 最近一单时间
     */
    private LocalDateTime endOrderDate;

    /**
     * 货款额度
     */
    private BigDecimal goodsQuota;

    /**
     * 税款额度
     */
    private BigDecimal taxQuota;
}
