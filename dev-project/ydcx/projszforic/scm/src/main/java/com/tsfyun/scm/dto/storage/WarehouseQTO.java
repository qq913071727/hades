package com.tsfyun.scm.dto.storage;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 仓库查询请求实体
 * @since Created in 2020/3/31 15:11
 */
@Data
public class WarehouseQTO extends PaginationDto implements Serializable {

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 仓库类型
     */
    private String warehouseType;

    /**
     * 是否禁用
     */
    private Boolean disabled;

    /**
     * 是否开启国内自提
     */
    private Boolean isSelfMention;

}
