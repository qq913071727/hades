package com.tsfyun.scm.vo.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/18 15:16
 */
@Data
public class ViewExpDifferenceSummaryVO implements Serializable {

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 客户名称
     */

    private String customerName;

//    /**
//     * 采购合同总价
//     */
//
//    private BigDecimal purchaseCny;
//
//    /**
//     * 结汇人民币金额
//     */
//
//    private BigDecimal settleAccountCny;
//
//    /**
//     * 实际采购金额
//     */
//
//    private BigDecimal actualPurchaseCny;

    /**
     * 生成的票差金额
     */

    private BigDecimal differenceVal;

//    /**
//     * 票差调整金额
//     */
//
//    private BigDecimal adjustmentVal;


}
