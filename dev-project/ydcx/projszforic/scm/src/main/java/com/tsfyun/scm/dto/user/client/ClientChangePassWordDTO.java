package com.tsfyun.scm.dto.user.client;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/11/23 10:21
 */
@Data
public class ClientChangePassWordDTO implements Serializable {


    @NotEmptyTrim(message = "旧密码不能为空")
    private String password;

    @NotEmptyTrim(message = "新密码不能为空")
    @LengthTrim(min = 6,max = 18,message = "新密码长度只能为6-18位")
    private String newPassword;

    @NotEmptyTrim(message = "确认密码不能为空")
    @LengthTrim(min = 6,max = 18,message = "确认密码长度只能为6-18位")
    private String confirmPassword;

}
