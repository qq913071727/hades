package com.tsfyun.scm.dto.finance;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/8 17:01
 */
@Data
public class OverseasReceivingAccountQTO extends PaginationDto {

    private String customerName;

    private String statusId;

    private String docNo;

    private String orderNo;

    private String receivingMode;

    private String subjectType;

    private Boolean isSettlement;

    private Boolean isPayment;

    /**=
     * 日期查询类型
     */
    private String queryDate;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateStart;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateEnd;

    public void setDateEnd(LocalDateTime dateEnd) {
        if(dateEnd != null){
            this.dateEnd = LocalDateTime.parse(LocalDateTimeUtils.formatTime(dateEnd,"yyyy-MM-dd 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }



}
