package com.tsfyun.scm.controller.user;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.common.base.validator.group.AddGroup;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.user.InsidePersonDTO;
import com.tsfyun.scm.dto.user.InsidePersonSaveDTO;
import com.tsfyun.scm.entity.user.Person;
import com.tsfyun.scm.service.user.IPersonService;
import com.tsfyun.scm.vo.user.PersonInfoVO;
import com.tsfyun.scm.vo.user.SimplePersonInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 管理端员工控制器
 */
@RestController
@RequestMapping(value = "/person")
public class InnerPersonControlller extends BaseController {

    @Autowired
    private IPersonService personService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 新增员工
     * @param dto
     * @return
     */
    @PostMapping("add")
    public Result<Void> addPerson(@Validated(value = AddGroup.class) @ModelAttribute InsidePersonSaveDTO dto) {
        personService.addInnerPerson(dto);
        return success();
    }

    /**
     * 修改员工（暂不可以修改密码，出于安全考虑，只能管理员重置或者自行修改）
     * @param dto
     * @return
     */
    @PostMapping("edit")
    public Result<Void> editPerson(@Validated(value = UpdateGroup.class) @ModelAttribute InsidePersonSaveDTO dto) {
        personService.editInnerPerson(dto);
        return success();
    }

    /**
     * 员工详情（不返回密码，可以是管理员查看员工信息）
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<PersonInfoVO> detail(@RequestParam(value = "id")Long id) {
        return success(personService.detailPerson(id));
    }

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<PersonInfoVO>> pageList(@ModelAttribute InsidePersonDTO dto) {
        PageInfo<PersonInfoVO> page = personService.pageList(dto,true);
        return success((int)page.getTotal(),page.getList());
    }

    /**
     * 员工详情（不返回密码，登录用户加载）
     * @return
     */
    @GetMapping(value = "info")
    public Result<LoginVO> info( ) {
        return success(personService.info( ));
    }

    /**
     * 删除员工（超级管理员才能操作）
     * @return
     */
    @PostMapping(value = "delete")
    //@PreAuthorize("hasRole('administrators')")
    public Result<Void> delete(@RequestParam("id")Long id) {
        personService.delete(id);
        return success();
    }

    /**
     * 修改在职状态
     * @return
     */
    @PostMapping(value = "updateIncumbency")
    public Result<Void> updateIncumbency(@RequestParam("id")Long id,@RequestParam("incumbency")Boolean incumbency) {
        personService.updateIncumbency(id,incumbency);
        return success();
    }

    /**
     * 根据职位获取员工下拉框（未被删除）
     * @param dto
     * @return
     */
    @GetMapping(value = "positionList")
    public Result<List<SimplePersonInfo>> positionList(InsidePersonDTO dto ) {
        PageInfo<Person> page = personService.positionList(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(), SimplePersonInfo.class));
    }

    /**
     * 导入员工信息（生成登录信息）
     * @param file
     * @return
     */
    @PostMapping(value = "importData")
    public Result importData(@RequestPart(value = "file") MultipartFile file){
        personService.importExcel(file);
        return Result.success();
    }
}
