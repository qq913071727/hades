package com.tsfyun.scm.service.risk;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.risk.RiskApprovalQTO;
import com.tsfyun.scm.entity.risk.RiskApproval;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.risk.RiskApprovalVO;

/**
 * <p>
 * 风控审批 服务类
 * </p>
 *

 * @since 2020-05-08
 */
public interface IRiskApprovalService extends IService<RiskApproval> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<RiskApprovalVO> pageList(RiskApprovalQTO qto);

    /**
     * 详情
     * @param id
     * @return
     */
    RiskApprovalVO detail(Long id,String operation);

    /**
     * 提交风控审批
     */
    void submitRiskApproval(RiskApproval dto);

    /**
     * 风控审核
     * @param dto
     */
    void approval(TaskDTO dto);

    /**=
     * 获取最近一条记录
     * @param docType
     * @param docId
     * @return
     */
    RiskApproval lastApproval(String docType,String docId);

    /**
     * 根据单据删除风控审核数据
     * @param docType
     * @param docId
     */
    void removeByDoc(String docType,String docId);
}
