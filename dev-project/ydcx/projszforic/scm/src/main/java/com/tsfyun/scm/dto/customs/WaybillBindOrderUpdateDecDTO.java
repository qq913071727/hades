package com.tsfyun.scm.dto.customs;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 跨境运输单绑定订单修改报关单数据实体
 * @since Created in 2020/5/13 16:39
 */
@Data
@Accessors(chain = true)
public class WaybillBindOrderUpdateDecDTO implements Serializable {

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 跨境运单系统单号
     */
    private String transportNo;

    /**
     * 车牌
     */
    private String licensePlate;

    /**
     * 六联单号
     */
    private String cusVoyageNo;

    /**
     * 集装箱号
     */
    private String containerNo;

}
