package com.tsfyun.scm.support.pdf.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @since Created in 2020/4/28 17:43
 */
@Data
public class ImpContractTemplate implements Serializable {

    private String contractNo;

    private String orderDate;

    private String buyerNameEn;

    private String buyerAddressEn;

    private String buyerTel;

    private String buyerFax;

    private String buyerName;

    private String sellerNameEn;

    private String sellerAddressEn;

    private String sellerName;

    private String sellerAddress;

    private String sellerTel;

    private String sellerFax;

    private String currencyCode;

    private String currencyName;

    private String buyerImg;

    private String sellerImg;

    private List<ImpContractMember> members;


}
