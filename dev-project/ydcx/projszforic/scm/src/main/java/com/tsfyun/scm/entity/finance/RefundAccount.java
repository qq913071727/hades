package com.tsfyun.scm.entity.finance;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 退款申请
 * </p>
 *
 *
 * @since 2020-11-27
 */
@Data
public class RefundAccount implements Serializable {

     private static final long serialVersionUID=1L;

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 退款金额
     */

    private BigDecimal accountValue;

    /**
     * 收款银行id
     */

    /*
    private Long payeeBankId;
     */

    /**
     * 收款银行名称
     */

    private String payeeBankName;

    /**
     * 收款账户名称
     */

    private String payeeAccountName;

    /**
     * 收款银行账号
     */

    private String payeeBankAccountNo;

    /**
     * 收款银行地址
     */

    private String payeeBankAddress;

    /**
     * 付款银行id
     */

    private Long payerBankId;

    /**
     * 付款银行名称
     */

    private String payerBankName;

    /**
     * 付款银行账号
     */

    private String payerBankAccountNo;

    /**
     * 申请退款原因
     */

    private String memo;

    /**
     * 实际退款时间
     */

    private LocalDateTime accountDate;

    /**
     * 审批原因
     */

    private String approvalInfo;

    /**
     * 创建时间
     */
    private LocalDateTime dateCreated;


}
