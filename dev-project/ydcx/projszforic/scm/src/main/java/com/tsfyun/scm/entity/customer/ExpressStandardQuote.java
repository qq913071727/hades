package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description: 快递标准报价代理费（重量为KG公斤）
 * @CreateDate: Created in 2020/10/22 16:10
 */
@Data
public class ExpressStandardQuote extends BaseEntity {

    /**
     * 报价名称
     */
    private String name;

    /**
     * 报价类型
     */
    private String quoteType;

    /**
     * 报价开始时间
     */
    private LocalDateTime quoteStartTime;

    /**
     * 报价结束时间
     */
    private LocalDateTime quoteEndTime;

    /**
     * 封顶标识
     */
    private Boolean capped;

    /**
     * 封顶费用
     */
    private BigDecimal cappedFee;

    /**
     * 禁用标示
     */
    private Boolean disabled;

}
