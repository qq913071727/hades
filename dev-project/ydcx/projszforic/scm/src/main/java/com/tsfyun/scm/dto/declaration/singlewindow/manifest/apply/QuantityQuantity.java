package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * @Description:
 * @since Created in 2020/4/22 14:41
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class QuantityQuantity {

    @XmlAttribute(name = "unitCode")
    private String unitCode;

    @XmlValue
    private String value;	//变量名随意

    public QuantityQuantity() {

    }

    public QuantityQuantity(String unitCode, String value) {
        this.unitCode = unitCode;
        this.value = value;
    }

}
