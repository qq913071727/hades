package com.tsfyun.scm.mapper.finance;

import com.tsfyun.scm.entity.finance.OverseasReceivingOrder;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.finance.OverseasReceivingOrderVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 境外收款认领订单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-10-11
 */
@Repository
public interface OverseasReceivingOrderMapper extends Mapper<OverseasReceivingOrder> {

    List<OverseasReceivingOrderVO> findByAccountId(@Param(value = "accountId") Long accountId);
    List<OverseasReceivingOrder> findOverseasReceivingOrderByAccountId(@Param(value = "accountId") Long accountId);

    List<String> findOrderNo(@Param(value = "accountId") Long accountId);

    List<OverseasReceivingOrderVO> overseasReceivings(@Param(value = "accountNos")List<String> accountNos);

    void writeSettlementValue(@Param(value = "id")Long id,@Param(value = "settlementValue") BigDecimal settlementValue);
}
