package com.tsfyun.scm.service.impl.order;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.dto.order.ImpOrderInvoiceQTO;
import com.tsfyun.scm.entity.order.ImpOrder;
import com.tsfyun.scm.mapper.order.ImpOrderMapper;
import com.tsfyun.scm.service.impl.report.ImpReportServiceImpl;
import com.tsfyun.scm.service.order.IImpOrderInvoiceService;
import com.tsfyun.scm.service.report.IImpReportService;
import com.tsfyun.scm.vo.order.ImpOrderInvoiceVO;
import com.tsfyun.scm.vo.order.ImpOrderPaySituationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class ImpOrderInvoiceServiceImpl extends ServiceImpl<ImpOrder> implements IImpOrderInvoiceService {

    @Autowired
    private ImpOrderMapper impOrderMapper;
    @Autowired
    private IImpReportService iImpReportService;

    @Override
    public PageInfo<ImpOrderInvoiceVO> list(ImpOrderInvoiceQTO qto) {
        PageHelper.startPage(qto.getPage(),qto.getLimit());
        Map<String,Object> params = beanMapper.map(qto,Map.class);
        List<ImpOrderInvoiceVO> list = impOrderMapper.orderInvoiceList(params);
        return new PageInfo<>(list);
    }

    @Override
    public List<ImpOrderInvoiceVO> idsList(List<String> ids) {
        if(CollUtil.isEmpty(ids)){return Lists.newArrayList();}
        Map<String,Object> params = Maps.newLinkedHashMap();
        params.put("ids",ids);
        List<ImpOrderInvoiceVO> impOrderInvoiceVOS = impOrderMapper.orderInvoiceList(params);
        Map<Long, BigDecimal> differenceValMap = new LinkedHashMap<>();
        for(ImpOrderInvoiceVO vo : impOrderInvoiceVOS){
            BigDecimal totalDifferenceVal = differenceValMap.get(vo.getCustomerId());
            if(Objects.isNull(totalDifferenceVal)){
                // 查询客户总票差
                totalDifferenceVal = iImpReportService.getCustomerDifference(vo.getCustomerId());
            }
            differenceValMap.put(vo.getCustomerId(),totalDifferenceVal);
            vo.setTotalDifferenceVal(totalDifferenceVal);
        }
        return impOrderInvoiceVOS;
    }
}
