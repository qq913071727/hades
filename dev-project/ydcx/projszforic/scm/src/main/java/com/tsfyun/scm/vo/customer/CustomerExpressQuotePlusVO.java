package com.tsfyun.scm.vo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 代理费快递模式报价单响应信息
 * @CreateDate: Created in 2020/11/24 18:41
 */
@Data
@NoArgsConstructor
public class CustomerExpressQuotePlusVO implements Serializable {

    private CustomerExpressQuoteVO customerExpressQuote;

    private List<CustomerExpressQuoteMemberVO> members;

    public CustomerExpressQuotePlusVO (CustomerExpressQuoteVO customerExpressQuote,List<CustomerExpressQuoteMemberVO> members) {
        this.customerExpressQuote = customerExpressQuote;
        this.members = members;
    }

}
