package com.tsfyun.scm.vo.materiel;

import com.tsfyun.scm.system.vo.CustomsCodeVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public class MaterielExpHSVO extends MaterielExpVO {

    @ApiModelProperty(value = "消费税率")
    private String conrate;

    @ApiModelProperty(value = "监管条件")
    private String csc;

    @ApiModelProperty(value = "出口税率")
    private BigDecimal exportRate;

    @ApiModelProperty(value = "普通国汇率")
    private BigDecimal gtr;

    @ApiModelProperty(value = "检验检疫监管条件")
    private String iaqr;

    @ApiModelProperty(value = "最惠国税率")
    private BigDecimal mfntr;

    @ApiModelProperty(value = "海关编码名称")
    private String hsCodeName;

    @ApiModelProperty(value = "法二单位编码")
    private String sunitCode;

    @ApiModelProperty(value = "出口暂定税率")
    private BigDecimal ter;

    @ApiModelProperty(value = "进口暂定税率")
    private BigDecimal tit;

    @ApiModelProperty(value = "出口退税率")
    private String trr;

    @ApiModelProperty(value = "法一单位编码")
    private String unitCode;

    @ApiModelProperty(value = "增值税率")
    private BigDecimal vatr;

    @ApiModelProperty(value = "法二单位名称")
    private String sunitName;

    @ApiModelProperty(value = "法一单位名称")
    private String unitName;

    @ApiModelProperty(value = "对美加征关税")
    private BigDecimal levyTax;

    public MaterielExpHSVO(){}

    public MaterielExpHSVO(MaterielExpVO materiel, CustomsCodeVO hscode){
        super(materiel);
        if(Objects.nonNull(hscode)){
            this.conrate = hscode.getConrate();
            this.csc = hscode.getCsc();
            this.exportRate = hscode.getExportRate();
            this.gtr = hscode.getGtr();
            this.iaqr = hscode.getIaqr();
            this.mfntr = hscode.getMfntr();
            this.hsCodeName = hscode.getName();
            this.ter = hscode.getTer();
            this.tit = hscode.getTit();
            this.trr = hscode.getTrr();
            this.vatr = hscode.getVatr();
            this.levyTax = hscode.getLevyTax();
            this.unitCode = hscode.getUnitCode();
            this.unitName = hscode.getUnitName();
            this.sunitCode = hscode.getSunitCode();
            this.sunitName = hscode.getSunitName();
        }
    }
}

