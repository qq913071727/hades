package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/28 16:34
 */
@Data
public class ImportExpOrderOtherCostExcel extends BaseRowModel implements Serializable {

    @NotEmptyTrim(message = "客户合同号不能为空")
    @LengthTrim(max = 20,message = "客户合同号最大长度不能超过20位")
    @ExcelProperty(value = "客户合同号", index = 7)
    private String expOrderNo;

    @NotEmptyTrim(message = "费用日期不能为空")
    @ExcelProperty(value = "费用日期", index = 9)
    private String costDate;

    @NotEmptyTrim(message = "费用名称不能为空")
    @ExcelProperty(value = "费用名称", index = 10)
    private String expenseSubjectName;

    @NotEmptyTrim(message = "币别不能为空")
    @ExcelProperty(value = "币别", index = 11)
    private String currencyName;

    @NotEmptyTrim(message = "应收金额不能为空")
    @ExcelProperty(value = "应收金额", index = 13)
    @Digits(integer = 8, fraction = 2, message = "应收金额整数位不能超过8位，小数位不能超过2位")
    private String costAmount;

    @ExcelProperty(value = "已收金额", index = 14)
    @Digits(integer = 8, fraction = 2, message = "已收金额整数位不能超过8位，小数位不能超过2位")
    private String receivedAmount;

    @Pattern(regexp = "退税款中扣除|客户单独支付|其他",message = "支付方式错误，只能为退税款中扣除、客户单独支付、其他")
    @NotEmptyTrim(message = "请选择支付方式")
    @ExcelProperty(value = "支付方式", index = 15)
    private String collectionSource;

    @ExcelProperty(value = "应付金额", index = 16)
    @Digits(integer = 8, fraction = 2, message = "应付金额整数位不能超过8位，小数位不能超过2位")
    private String needPayAmount;

    @LengthTrim(max = 255,message = "供应商名称最大长度不能超过255位")
    @ExcelProperty(value = "供应商名称", index = 18)
    private String supplierName;

    @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
    @ExcelProperty(value = "备注", index = 20)
    private String memo;

}
