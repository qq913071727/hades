package com.tsfyun.scm.entity.customer;

import java.io.Serializable;
import java.math.BigDecimal;

import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;


/**
 * <p>
 * 进口条款
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Data
public class ImpClause implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private Long id;

    /**
     * 协议ID
     */

    private Long agreementId;

    /**
     * 缴税方式(枚举)
     */

    private String voluntarilyTax;

    /**
     * 货款额度
     */

    private BigDecimal goodsQuota;

    /**
     * 税款额度
     */

    private BigDecimal taxQuota;

    /**
     * 额度说明
     */

    private String quotaExplain;

    /**
     * 成交方式
     */
    private String transactionMode;

    /**=
     * 货款汇率类型
     */
    private String goodsRate;

    /**=
     * 货款汇率时间
     */
    private String goodsRateTime;

    /**
     * 最大超期天数
     */
    private Integer overdueDays;

    /**=
     * 税款以实缴定应收
     */
    Boolean isPaidIn;

    /**=
     * 税款汇率
     */
    private String taxRate;

    /**=
     * 税款汇率时间
     */
    private String taxRateTime;
}
