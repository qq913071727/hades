package com.tsfyun.scm.entity.finance;

import java.math.BigDecimal;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 出口付款单
 * </p>
 *
 *
 * @since 2021-10-18
 */
@Data
public class ExpPaymentAccount extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */

    private String docNo;

    /**
     * 客户
     */

    private Long customerId;

    /**
     * 收款单ID
     */

    private Long overseasAccountId;

    /**
     * 收款单号
     */

    private String accountNo;

    /**
     * 付款金额
     */

    private BigDecimal accountValue;

    /**
     * 订单号
     */

    private String orderNo;
    private String clientNo;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 要求付款时间
     */

    private Date claimPaymentDate;

    /**
     * 实际付款时间
     */

    private Date actualPaymentDate;

    /**
     * 付款主体
     */

    private String subjectType;

    /**
     * 付款主体
     */

    private Long subjectId;

    /**
     * 付款主体
     */

    private String subjectName;

    /**
     * 付款银行
     */

    private String subjectBank;

    /**
     * 付款银行账号
     */

    private String subjectBankNo;

    /**
     * 收款方
     */

    private String payeeName;

    /**
     * 收款银行
     */

    private String payeeBank;

    /**
     * 收款银行账号
     */

    private String payeeBankNo;

    /**
     * 备注
     */

    private String memo;


}
