package com.tsfyun.scm.vo.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-03
 */
@Data
@ApiModel(value="CustomerBank响应对象", description="响应实体")
public class CustomerBankVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "客户ID")
    private Long customerId;

    //客户名称
    private String customerName;

    @ApiModelProperty(value = "银行名称")
    private String name;

    @ApiModelProperty(value = "银行账号")
    private String account;

    @ApiModelProperty(value = "银行地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    private Boolean isDefault;

    @JsonIgnore
    @ApiModelProperty(value = "创建人")
    private String createBy;


}
