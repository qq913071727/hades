package com.tsfyun.scm.dto.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *
 *
 * @since 2020-03-16
 */
@Data
@ApiModel(value="SerialNumber请求对象", description="请求实体")
public class SerialNumberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private String id;

   @LengthTrim(max = 10,message = "前缀最大长度不能超过10位")
   @ApiModelProperty(value = "前缀")
   private String prefix;

   @ApiModelProperty(value = "日期规则")
   private String timeRule;

   @NotNull(message = "流水号长度不能为空")
   @LengthTrim(max = 11,message = "流水号长度最大长度不能超过11位")
   @ApiModelProperty(value = "流水号长度")
   private Integer serialLength;

   @NotEmptyTrim(message = "流水号重置方式不能为空")
   @LengthTrim(max = 10,message = "流水号重置方式最大长度不能超过10位")
   @ApiModelProperty(value = "流水号重置方式")
   private String serialClear;

   @LengthTrim(max = 11,message = "当前流水号大小最大长度不能超过11位")
   @ApiModelProperty(value = "当前流水号大小")
   private Integer nowSerial;


}
