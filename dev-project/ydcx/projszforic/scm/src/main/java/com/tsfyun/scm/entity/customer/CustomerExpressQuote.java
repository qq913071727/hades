package com.tsfyun.scm.entity.customer;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 客户快递模式代理费报价
 * </p>
 *
 *
 * @since 2020-10-30
 */
@Data
public class CustomerExpressQuote extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户id
     */

    private Long customerId;

    /**
     * 报价类型
     */

    private String quoteType;

    /**
     * 报价开始时间
     */

    private LocalDateTime quoteStartTime;

    /**
     * 报价结束时间
     */

    private LocalDateTime quoteEndTime;

    /**
     * 封顶标识
     */

    private Boolean capped;

    /**
     * 封顶费用
     */

    private BigDecimal cappedFee;

    /**
     * 禁用标识
     */

    private Boolean disabled;


}
