package com.tsfyun.scm.dto.customer.client;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/28 10:25
 */
@Data
public class ClientSupplierQTO extends PaginationDto {

    //启用禁用
    private Boolean disabled;

}
