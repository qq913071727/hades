package com.tsfyun.scm.service.impl.finance;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.tsfyun.scm.entity.finance.TaxCodeName;
import com.tsfyun.scm.mapper.finance.TaxCodeNameMapper;
import com.tsfyun.scm.service.finance.ITaxCodeNameService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.util.TsfWeekendSqls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 客户税收分类编码 服务实现类
 * </p>
 *

 * @since 2020-05-21
 */
@Service
public class TaxCodeNameServiceImpl extends ServiceImpl<TaxCodeName> implements ITaxCodeNameService {

    @Autowired
    private TaxCodeNameMapper taxCodeNameMapper;

    @Override
    public TaxCodeName getCustomerTaxCodeName(Long customerId, String goodsName) {
        TsfWeekendSqls wheres = TsfWeekendSqls.<TaxCodeName>custom().andIn(false,TaxCodeName::getCustomerId, Lists.newArrayList(customerId))
                .andEqualTo(false,TaxCodeName::getName,goodsName);
        List<TaxCodeName> taxCodeNames = taxCodeNameMapper.selectByExample(Example.builder(TaxCodeName.class).where(wheres).build());
        //按客户id升序排
        if(CollUtil.isNotEmpty(taxCodeNames)) {
            return taxCodeNames.get(0);
        }
        return null;
    }

    @Override
    public TaxCodeName defaultTaxCodeName(Long customerId, String goodsName) {
        TaxCodeName taxCodeName = getCustomerTaxCodeName(customerId,goodsName);
        if(Objects.isNull(taxCodeName)){
            taxCodeName = getCustomerTaxCodeName(Long.valueOf(0),goodsName);
        }
        return taxCodeName;
    }

    @Override
    public Integer batchInsert(List<TaxCodeName> codeNameList) {
        return taxCodeNameMapper.batchInsert(codeNameList);
    }
}
