package com.tsfyun.scm.service.impl.order;

import com.tsfyun.scm.entity.order.OrderReceivingNoteMember;
import com.tsfyun.scm.mapper.order.OrderReceivingNoteMemberMapper;
import com.tsfyun.scm.service.order.IOrderReceivingNoteMemberService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.vo.order.OrderReceivingBindVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 *
 * @since 2020-04-29
 */
@Service
public class OrderReceivingNoteMemberServiceImpl extends ServiceImpl<OrderReceivingNoteMember> implements IOrderReceivingNoteMemberService {

    @Autowired
    private OrderReceivingNoteMemberMapper orderReceivingNoteMemberMapper;

    @Override
    public List<String> findReceivingNoByOrderNo(String orderNo) {
        return orderReceivingNoteMemberMapper.findReceivingNoByOrderNo(orderNo);
    }

    @Override
    public  List<OrderReceivingNoteMember> findByOrderNo(String orderNo) {
        return orderReceivingNoteMemberMapper.findByOrderNo(orderNo);
    }

    @Override
    public List<OrderReceivingBindVO> queryOrderReceivingBind(List<String> orderNos) {
        return orderReceivingNoteMemberMapper.queryOrderReceivingBind(orderNos);
    }

    @Override
    public Integer identificationIsIssued(List<String> orderNos,Boolean isOutStock) {
        return orderReceivingNoteMemberMapper.identificationIsIssued(orderNos,isOutStock);
    }
}
