package com.tsfyun.scm.support.pdf;

import com.google.common.collect.Maps;
import com.tsfyun.common.base.exception.ServiceException;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2020/4/29 14:20
 */
@Deprecated
@Slf4j
public class FreemarkerUtil {

    private static final String ENCODING = "UTF-8";

    /**
     * 便于灵活，模板可以设置单独的模板配置
     */
    private static Map<String, Configuration> configurationCache = Maps.newConcurrentMap();

    private static Map<String, FileTemplateLoader> fileTemplateLoaderCache= Maps.newConcurrentMap();

    /**
     * 获取模板配置
     * @param pdfTemplateFile
     * @return
     */
    public static Configuration getTemplateConfiguration(PdfTemplateFile pdfTemplateFile) {
        Configuration templateConfiguration = configurationCache.get(pdfTemplateFile.getTemplatePath());
        if(Objects.nonNull(templateConfiguration)) {
            return templateConfiguration;
        }
        //取默认的模板属性
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_25);
        configuration.setDefaultEncoding(ENCODING);
        configuration.setLogTemplateExceptions(false);
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        FileTemplateLoader fileTemplateLoader = fileTemplateLoaderCache.get(pdfTemplateFile.getTemplatePath());
        if(Objects.isNull(fileTemplateLoader)) {
            try {
                fileTemplateLoader = new FileTemplateLoader(new File(pdfTemplateFile.getTemplatePath()));
                fileTemplateLoaderCache.put(pdfTemplateFile.getTemplatePath(),fileTemplateLoader);
            } catch (IOException e) {
                log.error(String.format("模板【%s】初始化异常",pdfTemplateFile.getTemplateFileName()),e);
                throw new ServiceException("PDF模板初始化异常");
            }
        }
        configuration.setTemplateLoader(fileTemplateLoader);
        configurationCache.put(pdfTemplateFile.getTemplatePath(),configuration);
        return configuration;
    }

    /**
     * 数据渲染freemarker模板
     * @param data
     * @param pdfTemplateFile
     * @return
     */
    public static String renderHtml(Object data,PdfTemplateFile pdfTemplateFile) {
        StringWriter writer = null;
        try {
            Template template = getTemplateConfiguration(pdfTemplateFile).getTemplate(pdfTemplateFile.getTemplateFileName());
            writer = new StringWriter();
            template.process(data, writer);
            writer.flush();
            return writer.toString();
        } catch (TemplateException | IOException e) {
            log.error("模板处理异常",e);
            throw new ServiceException("模板处理异常");
        } finally {
            if(null != writer) {
                try {
                    writer.close();
                } catch (IOException e) {
                    log.error("关闭string writer异常",e);
                }
            }
        }
    }


}
