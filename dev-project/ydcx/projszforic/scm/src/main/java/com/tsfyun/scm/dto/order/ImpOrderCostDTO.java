package com.tsfyun.scm.dto.order;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

    import javax.validation.constraints.Digits;

/**
 * <p>
 * 订单费用请求实体
 * </p>
 *
 *
 * @since 2020-04-21
 */
@Data
@ApiModel(value="ImpOrderCost请求对象", description="订单费用请求实体")
public class ImpOrderCostDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "订单不能为空")
   @ApiModelProperty(value = "订单")
   private Long impOrderId;

   @NotEmptyTrim(message = "科目不能为空")
   @LengthTrim(max = 20,message = "科目最大长度不能超过20位")
   @ApiModelProperty(value = "科目")
   private String expenseSubjectId;

   @NotEmptyTrim(message = "费用分类不能为空")
   @LengthTrim(max = 10,message = "费用分类最大长度不能超过10位")
   @ApiModelProperty(value = "费用分类")
   private String costClassify;

   @NotNull(message = "实际金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "实际金额整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "实际金额")
   private BigDecimal actualAmount;

   @NotNull(message = "应收金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "应收金额整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "应收金额")
   private BigDecimal receAmount;

   @NotNull(message = "已收金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "已收金额整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "已收金额")
   private BigDecimal acceAmount;

   @NotNull(message = "是否系统自动生成不能为空")
   @ApiModelProperty(value = "是否系统自动生成")
   private Boolean isAutomatic;

   @LengthTrim(max = 32,message = "标记最大长度不能超过32位")
   @ApiModelProperty(value = "标记-(系统预留字段)")
   private String mark;

   @LengthTrim(max = 255,message = "备注最大长度不能超过255位")
   @ApiModelProperty(value = "备注")
   private String memo;

   @NotNull(message = "是否需要在账期内冲销(优先核销)不能为空")
   @ApiModelProperty(value = "是否需要在账期内冲销(优先核销)")
   private Boolean isFirstWriteOff;

   @NotNull(message = "是否锁定 (可以核销)不能为空")
   @ApiModelProperty(value = "是否锁定 (可以核销)")
   private Boolean isLock;

   @NotNull(message = "发生日期不能为空")
   @ApiModelProperty(value = "发生日期")
   private LocalDateTime happenDate;

   @NotNull(message = "账期日期不能为空")
   @ApiModelProperty(value = "账期日期")
   private LocalDateTime periodDate;

   @NotNull(message = "开始计算滞纳金日期不能为空")
   @ApiModelProperty(value = "开始计算滞纳金日期")
   private LocalDateTime lateFeeDate;

   @NotNull(message = "逾期利率不能为空")
   @Digits(integer = 8, fraction = 2, message = "逾期利率整数位不能超过8位，小数位不能超过2")
   @ApiModelProperty(value = "逾期利率")
   private BigDecimal overdueRate;

   @LengthTrim(max = 20,message = "操作人最大长度不能超过20位")
   @ApiModelProperty(value = "操作人")
   private String operator;

   @NotNull(message = "是否允许修改不能为空")
   @ApiModelProperty(value = "是否允许修改")
   private Boolean isAllowEdit;


}
