package com.tsfyun.scm.vo.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-03-20
 */
@Data
@ApiModel(value="SubjectBank响应对象", description="响应实体")
public class SubjectBankVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    private Long subjectId;

    private String subjectName;

    @ApiModelProperty(value = "银行名称")
    private String name;

    @ApiModelProperty(value = "银行账号")
    private String account;

    @ApiModelProperty(value = "银行地址")
    private String address;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "是否默认")
    private Boolean isDefault;

    @ApiModelProperty(value = "默认收款账户")
    private Boolean receivables;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "币制")
    private String currencyId;

    @ApiModelProperty(value = "币制")
    private String currencyName;

    @ApiModelProperty(value = "登记时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;


}
