package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/20 11:06
 */
@Data
public class ImportOrderMemberVO implements Serializable {

    private String model;

    private String name;

    private String brand;

    private String unitName;

    private String quantity;

    private String unitPrice;

    private String totalPrice;

    private String cartonNum;

    private String netWeight;

    private String grossWeight;

    private String countryName;

    private String goodsCode;

    private String spec;

}
