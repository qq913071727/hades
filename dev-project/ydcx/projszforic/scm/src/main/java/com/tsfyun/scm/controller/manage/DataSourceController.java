//package com.tsfyun.scm.controller.manage;
//
//import com.tsfyun.common.base.config.properties.NoticeProperties;
//import com.tsfyun.common.base.controller.BaseController;
//import com.tsfyun.common.base.dto.Result;
//import com.tsfyun.common.base.exception.ServiceException;
//import com.tsfyun.ds.annotation.GlobalRequest;
//import com.tsfyun.ds.entity.Tenant;
//import com.tsfyun.ds.help.DingTalkNoticeUtil;
//import com.tsfyun.ds.support.DynamicDataSourceContextHolder;
//import com.tsfyun.ds.support.db.*;
//import com.tsfyun.ds.util.TenantUtil;
//import com.tsfyun.scm.service.manage.ITenantServeService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import java.util.Objects;
//import java.util.Optional;
//
///**
// * 动态数据源管理和租户数据管理
// */
//@Slf4j
//@RestController
//@GlobalRequest
//@RequestMapping(value = "/ds")
//public class DataSourceController extends BaseController {
//
//    @Resource
//    private TenantBiz tenantBiz;
//
//    @Resource
//    private NoticeProperties noticeProperties;
//
//    @Value("${saas.manage.salt:qwert12345}")
//    private String salt;
//
//    @Autowired
//    private ITenantServeService tenantServeService;
//
//    @Value("${scm.manage.datasource.name}")
//    private String platformDbName;
//
//
//    /**
//     * 供应链saas平台开通账户，同步修改租户状态为已审核，新增数据源到内存中并新增spring数据源，不用服务重启，刷新内存
//     * 或者服务启动时创建数据源异常，手动创建数据源也适用
//     * @param tenantCode
//     * @return
//     */
//    @PostMapping(value = "syncOpen")
//    Result<Void> syncOpen(@RequestParam(value = "tenantCode")String tenantCode) {
//        DynamicDataSourceContextHolder.setDataSourceType(platformDbName);
//        try {
//            tenantServeService.syncOpen(tenantCode);
//        } finally {
//            DynamicDataSourceContextHolder.clearDataSourceType();
//        }
//        return success();
//    }
//
//    /**
//     * 删除数据源及租户内存数据
//     * @param tenantCode
//     * @return
//     */
//    @PostMapping(value = "syncRemove")
//    Result<Void> syncRemove(@RequestHeader("accept-salt") String acceptSalt, @RequestParam(value = "tenantCode")String tenantCode) {
//        String headTenantcode = TenantUtil.getTenant(request);
//        if(!Objects.equals(headTenantcode, platformDbName)) {
//            log.info("请求头租户编码:{}",headTenantcode);
//            throw new ServiceException("非法请求");
//        }
//        if(!Objects.equals(acceptSalt,salt)) {
//            log.info("请求头租户编码:{}，请求头盐:{}",headTenantcode,acceptSalt);
//            DingTalkNoticeUtil.send2DingDingOtherException(tenantCode,"","租户非法请求删除数据源",String.format("租户【%s】非法请求删除数据源，需更换加密盐",tenantCode),null,noticeProperties.getDingdingUrl());
//            throw new ServiceException("非法请求");
//        }
//        DynamicDataSourceContextHolder.setDataSourceType(platformDbName);
//        try {
//            tenantServeService.syncRemove(tenantCode);
//        } finally {
//            DynamicDataSourceContextHolder.clearDataSourceType();
//        }
//        return success();
//    }
//
//    /**
//     * 失效租户内存数据
//     * @param tenantCode
//     * @return
//     */
//    @PostMapping(value = "syncInvalid")
//    Result<Void> syncInvalid(@RequestHeader("accept-salt") String acceptSalt, @RequestParam(value = "tenantCode")String tenantCode) {
//        String headTenantcode = TenantUtil.getTenant(request);
//        if(!Objects.equals(headTenantcode, platformDbName)) {
//            log.info("请求头租户编码:{}",headTenantcode);
//            throw new ServiceException("非法请求");
//        }
//        if(!Objects.equals(acceptSalt,salt)) {
//            log.info("请求头租户编码:{}，请求头盐:{}",headTenantcode,acceptSalt);
//            DingTalkNoticeUtil.send2DingDingOtherException(tenantCode,"","租户非法请求失效主体",String.format("租户【%s】非法请求失效数据源，需更换加密盐",tenantCode),null,noticeProperties.getDingdingUrl());
//            throw new ServiceException("非法请求");
//        }
//        DynamicDataSourceContextHolder.setDataSourceType(platformDbName);
//        try {
//            Tenant tenant = tenantBiz.getByCode(tenantCode);
//            Optional.ofNullable(tenant).orElseThrow(()->new ServiceException("租户编码错误"));
//            tenantServeService.syncInvalid(tenant);
//        } finally {
//            DynamicDataSourceContextHolder.clearDataSourceType();
//        }
//        return success();
//    }
//
//    /**
//     * 供应链saas平台续费或者升级同步刷新租户信息
//     * @param tenantCode
//     * @return
//     */
//    @PostMapping(value = "syncUpgrade")
//    Result<Void> syncUpgrade(@RequestHeader("accept-salt") String acceptSalt, @RequestParam(value = "tenantCode")String tenantCode) {
//        if(!Objects.equals(acceptSalt,salt)) {
//            log.info("请求头盐:{}",acceptSalt);
//            DingTalkNoticeUtil.send2DingDingOtherException(tenantCode,"","租户非法请求失效主体",String.format("租户【%s】非法请求失效数据源，需更换加密盐",tenantCode),null,noticeProperties.getDingdingUrl());
//            throw new ServiceException("非法请求");
//        }
//        DynamicDataSourceContextHolder.setDataSourceType(platformDbName);
//        try {
//            tenantServeService.syncUpgradeRenewalUpdateTenant(tenantCode);
//        } finally {
//            DynamicDataSourceContextHolder.clearDataSourceType();
//        }
//        return success();
//    }
//
//
//}
