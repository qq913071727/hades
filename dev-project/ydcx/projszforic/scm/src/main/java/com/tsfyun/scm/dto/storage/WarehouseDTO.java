package com.tsfyun.scm.dto.storage;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.enums.WarehouseTypeEnum;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 仓库请求实体
 * </p>
 *

 * @since 2020-03-31
 */
@Data
@ApiModel(value="Warehouse请求对象", description="仓库请求实体")
public class WarehouseDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "仓库id不能为空",groups = UpdateGroup.class)
   private Long id;

   @NotEmptyTrim(message = "仓库编码不能为空")
   @LengthTrim(max = 50,message = "仓库编码最大长度不能超过50位")
   @ApiModelProperty(value = "仓库编码")
   private String code;

   @NotEmptyTrim(message = "仓库名称不能为空")
   @LengthTrim(max = 100,message = "仓库名称最大长度不能超过100位")
   @ApiModelProperty(value = "仓库名称")
   private String name;

   @LengthTrim(max = 160,message = "仓库英文名称最大长度不能超过160位")
   @ApiModelProperty(value = "仓库英文名称")
   private String nameEn;

   @NotEmptyTrim(message = "公司名称不能为空")
   @LengthTrim(max = 200,message = "公司名称最大长度不能超过200位")
   @ApiModelProperty(value = "公司名称")
   private String companyName;

   @NotEmptyTrim(message = "仓库地址不能为空")
   @LengthTrim(max = 255,message = "仓库地址最大长度不能超过255位")
   @ApiModelProperty(value = "仓库地址")
   private String address;

   @LengthTrim(max = 255,message = "仓库英文地址最大长度不能超过255位")
   @ApiModelProperty(value = "仓库英文地址")
   private String addressEn;

   @LengthTrim(max = 50,message = "仓库联系人最大长度不能超过50位")
   private String linkPerson;

   @LengthTrim(max = 50,message = "仓库联系人电话最大长度不能超过50位")
   private String linkTel;

   @NotEmptyTrim(message = "仓库类型不能为空")
   @LengthTrim(max = 20,message = "仓库类型最大长度不能超过20位")
   @EnumCheck(clazz = WarehouseTypeEnum.class,message = "仓库类型错误")
   @ApiModelProperty(value = "仓库类型")
   private String warehouseType;

   @NotNull(message = "是否开启国内自提不能为空")
   @ApiModelProperty(value = "是否开启国内自提")
   private Boolean isSelfMention;

   @LengthTrim(max = 255,message = "备注最大长度不能超过20位")
   private String memo;


}
