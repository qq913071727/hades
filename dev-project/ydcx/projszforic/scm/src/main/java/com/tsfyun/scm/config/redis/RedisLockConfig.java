package com.tsfyun.scm.config.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.integration.redis.util.RedisLockRegistry;

/**
 * @Description: 分布式锁配置
 * @since Created in 2019/12/24 11:56
 */
@Configuration
@AutoConfigureAfter(RedisConnectionFactory.class)
public class RedisLockConfig {

    @Value("${spring.application.name:supply}")
    private String applicationName;

    @Bean
    @ConditionalOnMissingBean
    public RedisLockRegistry redisLockRegistry(RedisConnectionFactory connectionFactory) {
        return new RedisLockRegistry(connectionFactory, applicationName);
    }

}
