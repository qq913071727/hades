package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.entity.system.StatusHistory;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.system.StatusHistoryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-03
 */
@Repository
public interface StatusHistoryMapper extends Mapper<StatusHistory> {

    StatusHistoryVO findRecentByDomainAndNowStatusOne(@Param(value = "domainId") String domainId, @Param(value = "domainType") String domainType, @Param(value = "nowStatusId") String nowStatusId);

    StatusHistoryVO findRecentByDomainAndHistoryStatus(@Param(value = "domainId") String domainId, @Param(value = "domainType") String domainType, @Param(value = "historyStatusId") String historyStatusId);

    List<StatusHistoryVO> findRecentByDomainAndNowStatusOneBatch(@Param(value = "domainIds") List<String> domainId, @Param(value = "domainType") String domainType, @Param(value = "nowStatusId") String nowStatusId);

    Integer removeHistory(@Param(value = "domainId")String domainId,@Param(value = "domainType") String domainType);

    List<StatusHistory> findByOneDomainIdAndOpration(@Param(value = "domainId")String domainId,@Param(value = "opration") String opration);
}
