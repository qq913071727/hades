package com.tsfyun.scm.vo.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 销售合同详情信息
 * @CreateDate: Created in 2020/5/19 10:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpSalesContractDetailPlusVO implements Serializable {

    /**
     * 销售合同主单
     */
    private ImpSalesContractVO salesContract;

    /**
     * 销售合同明细
     */
    private List<ImpSalesContractMemberVO> members;

    /**
     * 发票费用明细
     */
    private List<InvoiceCostMemberVO> costMembers;

}
