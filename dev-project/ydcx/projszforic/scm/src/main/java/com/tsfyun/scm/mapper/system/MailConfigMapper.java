package com.tsfyun.scm.mapper.system;

import com.tsfyun.scm.entity.system.MailConfig;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  邮件配置Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface MailConfigMapper extends Mapper<MailConfig> {

}
