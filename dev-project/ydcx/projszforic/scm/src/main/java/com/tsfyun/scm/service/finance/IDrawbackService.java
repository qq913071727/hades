package com.tsfyun.scm.service.finance;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.*;
import com.tsfyun.scm.entity.finance.Drawback;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.CreateDrawbackPlusVO;
import com.tsfyun.scm.vo.finance.DrawbackPlusVO;
import com.tsfyun.scm.vo.finance.DrawbackVO;
import com.tsfyun.scm.vo.finance.PrintDrawBackVO;

/**
 * <p>
 * 退税单 服务类
 * </p>
 *
 *
 * @since 2021-10-20
 */
public interface IDrawbackService extends IService<Drawback> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<DrawbackVO> pageList(DrawbackQTO qto);

    /**
     * 详情
     * @param id
     * @param operation
     * @return
     */
    DrawbackPlusVO detail(Long id, String operation);

    /**
     * 重新计算退税比例
     * @param dto
     * @return
     */
    CreateDrawbackPlusVO recalculate(DrawbackDTO dto);
    /**
     * 保存退税
     * @param dto
     */
    void saveDrawback(DrawbackDTO dto);

    /**
     * 删除退税
     * @param id
     */
    void removeDrawback(Long id);

    /**
     * 退税单确认
     * @param dto
     */
    void confirm(TaskDTO dto);

    /**
     * 退回商务确认
     * @param dto
     */
    void backConfirm(TaskDTO dto);

    /**
     * 退回重新确定付款行
     * @param dto
     */
    void backConfirmBank(TaskDTO dto);

    /**
     * 确定付款银行
     * @param dto
     */
    void confirmBank(ExpConfirmBankDTO dto);

    /**
     * 确定付款完成
     * @param dto
     */
    void paymentCompleted(ExpPaymentCompletedDTO dto);

    /**
     * 退税付款申请打印
     * @param id
     * @return
     */
    PrintDrawBackVO getPaymentPrintData(Long id);

}
