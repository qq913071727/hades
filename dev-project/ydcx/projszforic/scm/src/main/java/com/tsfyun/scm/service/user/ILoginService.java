package com.tsfyun.scm.service.user;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.dto.user.*;
import com.tsfyun.scm.dto.user.client.ClientChangePassWordDTO;
import com.tsfyun.scm.dto.user.client.ClientSetPassWordDTO;
import com.tsfyun.scm.dto.user.client.ClientUpdatePassWordDTO;
import com.tsfyun.scm.entity.user.Login;
import com.tsfyun.scm.vo.support.WeixinFocusScanVO;
import com.tsfyun.scm.vo.user.ClientInfoVO;
import com.tsfyun.scm.vo.user.CustomerLoginInfoVO;
import com.tsfyun.scm.vo.user.LoginInfoVO;
import com.tsfyun.scm.vo.user.SGTLoginVO;

import java.util.Map;

public interface ILoginService extends IService<Login> {


    /**
     * 登录（非第三方登录，第三方登陆要走302分开处理）
     * @param dto
     * @return
     */
    LoginInfoVO login(LoginDTO dto);

    /**
     * 深关通数据交换登录
     * @param dto
     * @return
     */
    SGTLoginVO sgtLogin(SGTLoginDTO dto);

    /**
     * 微信小程序授权
     * @param dto
     * @return
     */
    LoginInfoVO wxxcxAuthorize(WxxcxLoginDTO dto);

    /**
     * 微信公众号验证登录
     * @param dto
     * @return
     */
    LoginInfoVO wxGzhCheckLogin(WxgzhLoginDTO dto);

    /**
     * 登出
     */
    void logout();

    void updateLogin(Login login);

    /**
     * 根据员工id删除用户
     * @param personId
     */
    void deleteByPersonId(Long personId);


    /**
     * 强制退出某人员的登录信息
     * @param personId
     */
    void forceLogout(String personId,String personName);

    /**
     * 管理端修改密码
     * @param dto
     */
    void updatePassWordForManager(UpdatePassWordDTO dto);

    /**
     * 保存人员登录信息
     * @param personId
     * @param type
     * @param loginName
     */
    void saveLogin(Long personId,Integer type,String loginName);

    /**
     * 获取登录客户基本信息
     * @return
     */
    ClientInfoVO clientInfo();

    /**
     * 根据登录名称登录信息
     * @param phoneNo
     * @return
     */
    Login findByLoginName(String phoneNo);

    /**
     * 根据手机验证码重置密码
     * @param dto
     */
    void retrievePassword(ClientUpdatePassWordDTO dto);

    /**
     * 修改登录手机号码
     * @param personId
     * @param originPhoneNo
     * @param newPhoneNo
     */
    void updateLoginPhone(Long personId,String originPhoneNo,String newPhoneNo);

    /**
     * 客户端根据旧密码修改密码
     * @param dto
     */
    void clientModifyPassWord(ClientChangePassWordDTO dto);

    /**
     * 客户端首次设置登录密码
     * @param dto
     */
    void clientSetPassWord(ClientSetPassWordDTO dto);

    /**
     * 微信小程序手机号码授权
     * @param dto
     * @return
     */
    LoginInfoVO wxxcxAuthorizeByPhone(WxxcxLoginDTO dto);

    /**
     * 同步微信头像、昵称、性别
     * @param dto
     */
    void syncWxHeadAndName(WxxcxLoginDTO dto);

    /**
     * 判断用户可以微信第三方登录，并同时返回绑定码（后台跟人员挂钩）
     * @return
     */
    String checkUserBindFocusWxUnion();

    /**
     * 判断登录用户是否绑定微信公众号（下单成功页面）
     * 1-二维码已失效；2-成功；3-失败
     */
    WeixinFocusScanVO checkBindFocusWxgzh(String wxCode);

    /**
     * 判断用户是否绑定了微信登录
     * @param personId
     * @return
     */
    Boolean checkUserBindWxLogin(Long personId);

    /**
     * 网页客户端绑定和换绑微信生成微信事件key参数
     * bind-绑定微信；changeBind （以客户界面传输的参数为准）
     * @return
     */
    String generateWxLoginKey(String operateType);


    /**
     * 判断登录用户是否绑定微信公众号（下单成功页面）
     * 1-二维码已失效；2-成功；3-失败
     */
    WeixinFocusScanVO checkBindWxgzh(String operateType,String wxCode);

    /**
     * 获取所有客户登录信息
     * @return
     */
    Map<Long, CustomerLoginInfoVO> allCustomerLoginInfo();
}
