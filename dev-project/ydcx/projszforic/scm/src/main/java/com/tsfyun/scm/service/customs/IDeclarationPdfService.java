package com.tsfyun.scm.service.customs;

import com.tsfyun.common.base.dto.ExportFileDTO;
import com.tsfyun.scm.vo.customs.ContractPlusVO;

public interface IDeclarationPdfService {

    Long contract(Long id,Boolean autoChapter);
    Long packing(Long id,Boolean autoChapter);
    Long invoice(Long id,Boolean autoChapter);
    Long delivery(Long id,Boolean autoChapter);
    Long merge(Long id,Boolean autoChapter);

    ExportFileDTO createContractPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
    ExportFileDTO createPackingPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
    ExportFileDTO createInvoicePdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
    ExportFileDTO createDeliveryPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);

    Long overseasContract(Long id,Boolean autoChapter);
    Long overseasPacking(Long id,Boolean autoChapter);
    Long overseasInvoice(Long id,Boolean autoChapter);
    Long overseasDelivery(Long id,Boolean autoChapter);
    Long mergeOverseas(Long id,Boolean autoChapter);

    ExportFileDTO createOverseasContractPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
    ExportFileDTO createOverseasPackingPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
    ExportFileDTO createOverseasInvoicePdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
    ExportFileDTO createOverseasDeliveryPdf(ContractPlusVO vo, Boolean autoChapter, String path, String sysFileName);
}
