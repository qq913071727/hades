package com.tsfyun.scm.entity.logistics;

import java.time.LocalDateTime;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;


/**
 * <p>
 * 跨境运输单
 * </p>
 *

 * @since 2020-04-17
 */
@Data
public class CrossBorderWaybill extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 系统单号
     */
    private String docNo;

    /**
     * 单据类型
     */
    private String billType;


    /**
     * 运输日期
     */

    private LocalDateTime transDate;

    /**
     * 发车日期
     */

    private LocalDateTime departureDate;

    /**
     * 到达日期
     */

    private LocalDateTime reachDate;

    /**
     * 状态
     */

    private String statusId;

    /**
     * 运输业务类型
     */

    private String waybillBusinessType;

    /**
     * 发货仓库id
     */

    private Long shippingWareId;

    /**
     * 发货仓库名称
     */

    private String shippingWareName;

    /**
     * 发货公司
     */

    private String shippingCompany;

    /**
     * 发货地址
     */

    private String shippingAddress;

    /**
     * 发货联系人
     */

    private String shippingLinkPerson;

    /**
     * 发货联系电话
     */

    private String shippingLinkTel;

    /**
     * 收货仓库id
     */

    private Long deliveryWareId;

    /**
     * 收货仓库名称
     */

    private String deliveryWareName;

    /**
     * 收货公司
     */

    private String deliveryCompany;

    /**
     * 收货地址
     */

    private String deliveryAddress;

    /**
     * 收货联系人
     */

    private String deliveryLinkPerson;

    /**
     * 收货联系电话
     */

    private String deliveryLinkTel;

    /**
     * 订车类型
     */

    private String conveyanceType;

    /**
     * 运输供应商
     */

    private Long transportSupplierId;

    /**
     * 运输工具
     */

    private Long conveyanceId;

    /**
     * 车牌1
     */

    private String conveyanceNo;

    /**
     * 车牌2
     */

    private String conveyanceNo2;

    /**
     * 司机信息
     */

    private String driverInfo;

    /**
     * 运单号
     */

    private String waybillNo;

    /**
     * 封条号
     */

    private String sealNo;

    /**
     * 集装箱号
     */

    private String containerNo;

    /**
     * 送货目地
     */

    private String transportDestination;

    /**
     * 通关口岸
     */

    private String customsCode;

    /**
     * 通关口岸
     */

    private String customsName;

    /**
     * 备注
     */
    private String memo;


}
