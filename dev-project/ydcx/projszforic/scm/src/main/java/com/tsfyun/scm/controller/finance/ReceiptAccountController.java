package com.tsfyun.scm.controller.finance;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.scm.dto.finance.ReceiptAccountDTO;
import com.tsfyun.scm.dto.finance.ReceiptAccountQTO;
import com.tsfyun.scm.service.finance.IReceiptAccountService;
import com.tsfyun.scm.vo.finance.ReceiptAccountDetailPlusVO;
import com.tsfyun.scm.vo.finance.ReceiptAccountVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;
import javax.validation.Valid;
import java.util.List;

/**
 * 收款单 前端控制器
 */
@RestController
@RequestMapping("/receiptAccount")
public class ReceiptAccountController extends BaseController {

    @Autowired
    private IReceiptAccountService receiptAccountService;

    /**
     * 收款登记
     */
    @PostMapping("/register")
    @DuplicateSubmit
    public Result<Long> register(@ModelAttribute @Valid ReceiptAccountDTO dto){
        return success(receiptAccountService.register(dto));
    }

    /**
     * 分页查询
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ReceiptAccountVO>> pageList(@ModelAttribute ReceiptAccountQTO qto) {
        PageInfo<ReceiptAccountVO> page = receiptAccountService.pageList(qto);
        return success((int)page.getTotal(),page.getList());
    }

    /**=
     * 收款单详情
     * @param id
     * @return
     */
    @PostMapping(value = "detail")
    public Result<ReceiptAccountVO> detail(@RequestParam(value = "id")Long id, @RequestParam(value = "operation",required = false)String operation){
        return success(receiptAccountService.detail(id,operation));
    }

    /**=
     * 收款单确认
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "confirm")
    public Result<Void> confirm(@ModelAttribute @Valid TaskDTO dto){
        receiptAccountService.confirm(dto);
        return success();
    }

    /**
     * 收款单（含收款单明细）
     * @param id
     * @return
     */
    @GetMapping(value = "info")
    public Result<ReceiptAccountDetailPlusVO> info(@RequestParam(value = "id")Long id){
        return success(receiptAccountService.plusDetail(id));
    }

    /**
     * 导出excel
     * @param qto
     * @return
     */
    @PostMapping(value = "exportExcel")
    public Result<Long> exportExcel(@ModelAttribute ReceiptAccountQTO qto){
        return success(receiptAccountService.exportExcel(qto));
    }

    /**=
     * 收款单删除
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "remove")
    public Result<Void> remove(@RequestParam(value = "id")Long id){
        receiptAccountService.removeAllById(id);
        return success();
    }
}

