package com.tsfyun.scm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.util.StringUtils;
import lombok.Data;

import java.io.Serializable;

/**=
 * 物料信息导入
 */
@Data
public class ClientMaterielExcel extends BaseRowModel implements Serializable {


    @NotEmptyTrim(message = "物料型号不能为空")
    @LengthTrim(max = 100,message = "物料型号最大长度不能超过100位")
    @ExcelProperty(value = "物料型号", index = 0)
    private String model;

    @NotEmptyTrim(message = "物料品牌不能为空")
    @LengthTrim(max = 100,message = "物料品牌最大长度不能超过100位")
    @ExcelProperty(value = "物料品牌", index = 1)
    private String brand;

    @ExcelProperty(value = "物料名称", index = 2)
    @LengthTrim(max = 100,message = "物料名称最大长度不能超过100位")
    private String name;

    @ExcelProperty(value = "物料规格描述", index = 3)
    @LengthTrim(max = 500,message = "物料规格描述最大长度不能超过500位")
    private String spec;

    public void setModel(String model){
        this.model = StringUtils.removeSpecialSymbol(model);
    }
    public void setBrand(String brand){
        this.brand = StringUtils.removeSpecialSymbol(brand);
    }
    public void setName(String name){
        this.name = StringUtils.removeSpecialSymbol(name);
    }
    public void setSpec(String spec){
        this.spec = StringUtils.removeSpecialSymbol(spec);
    }
}
