package com.tsfyun.scm.dto.declaration.singlewindow.manifest.apply;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @Description: 公路舱单申请报文头
 * @since Created in 2020/4/22 10:54
 */
@Data
@Accessors(chain = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Head", propOrder = {
        "messageId",
        "functionCode",
        "messageType",
        "senderId",
        "receiverId",
        "sendTime",
        "version",
})
public class Head {

    //报文编号
    @XmlElement(name = "MessageID")
    protected String messageId;

    //报文功能代码
    @XmlElement(name = "FunctionCode")
    protected String functionCode;

    //报文类型代码
    @XmlElement(name = "MessageType")
    protected String messageType;

    //发送方代码 发送方代码 4位备案关区代码+9位企业组织机构代码+50位自定义扩展字符
    @XmlElement(name = "SenderID")
    protected String senderId;

    //接收方代码 接收报文的4位关区号，或中国电子口岸代码（EPORT）以及信息中心（0000）
    @XmlElement(name = "ReceiverID")
    protected String receiverId;

    //发送时间 格式为YYYYMMDDHHmmssfff
    @XmlElement(name = "SendTime")
    protected String sendTime;

    //报文版本号
    @XmlElement(name = "Version")
    protected String version;

    /*
    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
     */
}
