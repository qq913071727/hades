package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomerDeliveryInfoVO implements Serializable {

    private Long id;

    /**
     * 客户
     */
    private Long customerId;

    /**
     * 收货公司
     */
    private String companyName;

    /**
     * 联系人
     */
    private String linkPerson;

    /**
     * 联系电话
     */
    private String linkTel;

    /**
     * 省
     */
    private String provinceName;

    /**
     * 市
     */
    private String cityName;

    /**
     * 区
     */
    private String areaName;

    /**
     * 详细地址
     */
    private String address;

    /**=
     * 禁止状态
     */
    private Boolean disabled;

    /**
     * 默认地址
     */
    private Boolean isDefault;

}
