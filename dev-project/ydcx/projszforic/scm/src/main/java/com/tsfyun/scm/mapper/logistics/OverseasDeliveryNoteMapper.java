package com.tsfyun.scm.mapper.logistics;

import com.tsfyun.scm.dto.logistics.OverseasDeliveryNoteQTO;
import com.tsfyun.scm.entity.logistics.OverseasDeliveryNote;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.vo.logistics.OverseasDeliveryNoteVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 香港送货单 Mapper 接口
 * </p>
 *
 *
 * @since 2021-11-09
 */
@Repository
public interface OverseasDeliveryNoteMapper extends Mapper<OverseasDeliveryNote> {

    List<OverseasDeliveryNoteVO> list(OverseasDeliveryNoteQTO qto);

    int updateExpOrderNos(@Param(value = "id") Long id,@Param(value = "expOrderNos")  String expOrderNos);

    BigDecimal getOrderSignedQuantity(@Param(value = "orderNo")String orderNo);

}
