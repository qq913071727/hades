package com.tsfyun.scm.entity.customer;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-12
 */
@Data
public class CustomerPersonChange extends BaseEntity {

     private static final long serialVersionUID=1L;

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 人员类型(0:销售,1商务)
     */
    private Integer personType;

    /**
     * 原人员ID
     */
    private Long oldPersonId;

    /**
     * 原人员名称
     */
    private String oldPersonName;

    /**
     * 现人员ID
     */
    private Long nowPersonId;

    /**
     * 现人员名称
     */
    private String nowPersonName;

    /**
     * 备注
     */
    private String memo;


}
