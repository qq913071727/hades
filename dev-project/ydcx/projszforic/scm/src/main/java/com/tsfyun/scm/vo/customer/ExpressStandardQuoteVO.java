package com.tsfyun.scm.vo.customer;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.QuoteTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Optional;

/**
 * <p>
 * 快递模式标准报价代理费响应实体
 * </p>
 *
 *
 * @since 2020-10-26
 */
@Data
@ApiModel(value="ExpressStandardQuote响应对象", description="快递模式标准报价代理费响应实体")
public class ExpressStandardQuoteVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "报价名称")
    private String name;

    @ApiModelProperty(value = "报价类型")
    private String quoteType;

    @ApiModelProperty(value = "报价开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime quoteStartTime;

    @ApiModelProperty(value = "报价结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime quoteEndTime;

    @ApiModelProperty(value = "封顶标识")
    private Boolean capped;

    @ApiModelProperty(value = "封顶费用")
    private BigDecimal cappedFee;

    @ApiModelProperty(value = "禁用标识")
    private Boolean disabled;

    private String quoteTypeDesc;

    public String getQuoteTypeDesc() {
        QuoteTypeEnum quoteTypeEnum = QuoteTypeEnum.of(quoteType);
        return Optional.ofNullable(quoteTypeEnum).map(QuoteTypeEnum::getName).orElse("");
    }


}
