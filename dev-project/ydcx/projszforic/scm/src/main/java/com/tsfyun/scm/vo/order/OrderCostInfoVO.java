package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OrderCostInfoVO implements Serializable {
    //收款未核销金额
    private BigDecimal receivablesVal;
    //订单总费用
    private BigDecimal orderTotalCost;
    //进口本单应支付金额
    private BigDecimal orderPayVal;
    //逾期金额
    private BigDecimal totalArrearsVal;
    //当前客户可用税款额度
    private BigDecimal taxOccupyVal;
    //是否通过
    private Boolean isAdopt;
    //原因
    private String reason;
    //风控是否通过
    private Boolean fkAdopt;
    //风控意见
    private String riskControl;
}
