package com.tsfyun.scm.service.finance;

import com.tsfyun.scm.dto.finance.TaxCodeDTO;
import com.tsfyun.scm.entity.customer.Customer;
import com.tsfyun.scm.entity.finance.ImpSalesContractMember;
import com.tsfyun.scm.entity.finance.Invoice;
import com.tsfyun.scm.entity.finance.InvoiceMember;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.finance.ImpSalesContractMemberVO;
import com.tsfyun.scm.vo.finance.InvoiceMemberExcel;
import com.tsfyun.scm.vo.finance.InvoiceMemberVO;

import java.util.List;

/**
 * <p>
 * 发票明细 服务类
 * </p>
 *

 * @since 2020-05-18
 */
public interface IInvoiceMemberService extends IService<InvoiceMember> {

    /**
     * 代理类发票明细信息保存
     */
    void saveAgentMember(Invoice invoice);

    /**
     * 根据发票id查询发票明细列表
     * @param invoiceId
     * @return
     */
    List<InvoiceMemberVO> getMembersByInvoiceId(Long invoiceId);

    /**
     * 根据销售合同明细生成发票明细
     * @param invoice
     * @param salesContractMembers
     */
    List<InvoiceMember> saveMemberBySalesContractMembers(Invoice invoice,List<ImpSalesContractMemberVO> salesContractMembers);

    /**
     * 根据发票ID集合查询明细
     * @param ids
     * @return
     */
    List<InvoiceMemberExcel> findByInvoiceIds(List<Long> ids);

    /**
     * 根据发票id删除明细信息
     * @param invoiceId
     */
    void deleteByInvoiceId(Long invoiceId);

    /**=
     * 保存明细税收分类编码
     * @param members
     */
    void saveMemberTaxCode(List<TaxCodeDTO> members, Customer customer);

}
