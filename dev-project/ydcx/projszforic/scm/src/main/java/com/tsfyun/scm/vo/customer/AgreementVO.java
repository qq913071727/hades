package com.tsfyun.scm.vo.customer;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tsfyun.common.base.enums.BusinessTypeEnum;
import com.tsfyun.common.base.enums.DeclareTypeEnum;
import com.tsfyun.common.base.enums.domain.AgreementStatusEnum;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 协议报价单响应实体
 * </p>
 *
 *
 * @since 2020-03-31
 */
@Data
@ApiModel(value="Agreement响应对象", description="协议报价单响应实体")
public class AgreementVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "客户")
    private Long customerId;

    @ApiModelProperty(value = "协议编号")
    private String docNo;

    @ApiModelProperty(value = "业务类型")
    private String businessType;

    @ApiModelProperty(value = "报关类型")
    private String declareType;

    @ApiModelProperty(value = "状态编码")
    private String statusId;

    @ApiModelProperty(value = "甲方")
    private String partyaName;

    /**
     * 甲方统一社会信用代码
     */

    private String partyaSocialNo;

    @ApiModelProperty(value = "甲方法人")
    private String partyaLegalPerson;

    @ApiModelProperty(value = "甲方电话")
    private String partyaTel;

    @ApiModelProperty(value = "甲方传真")
    private String partyaFax;

    @ApiModelProperty(value = "甲方地址")
    private String partyaAddress;

    @ApiModelProperty(value = "乙方")
    private String partybName;

    /**
     * 乙方统一社会信用代码
     */

    private String partybSocialNo;

    @ApiModelProperty(value = "乙方法人")
    private String partybLegalPerson;

    @ApiModelProperty(value = "乙方电话")
    private String partybTel;

    @ApiModelProperty(value = "乙方传真")
    private String partybFax;

    @ApiModelProperty(value = "乙方地址")
    private String partybAddress;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private LocalDateTime dateCreated;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "签约日期")
    private LocalDateTime signingDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "生效日期")
    private LocalDateTime effectDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "失效日期")
    private LocalDateTime invalidDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "实际失效日期")
    private LocalDateTime actualInvalidDate;

    @ApiModelProperty(value = "自动延期/年")
    private Integer delayYear;

    @ApiModelProperty(value = "协议正本是否签回")
    private Boolean isSignBack;

    @ApiModelProperty(value = "签回确定人")
    private String signBackPerson;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "报价描述")
    private String quoteDescribe;

    private Long afileId;// 框架协议文件

    private Long qfileId;// 报价单文件

    //业务类型描述
    private String businessTypeDesc;

    //报关类型
    private String declareTypeDesc;

    //状态描述
    private String statusName;


    public String getBusinessTypeDesc() {
        BusinessTypeEnum businessTypeEnum = BusinessTypeEnum.of(businessType);
        return Objects.nonNull(businessTypeEnum) ? businessTypeEnum.getName() : null;
    }
    public String getDeclareTypeDesc() {
        DeclareTypeEnum declareTypeEnum = DeclareTypeEnum.of(declareType);
        return Objects.nonNull(declareTypeEnum) ? declareTypeEnum.getName() : null;
    }

    public String getStatusName() {
        AgreementStatusEnum agreementStatusEnum = AgreementStatusEnum.of(statusId);
        return Objects.nonNull(agreementStatusEnum) ? agreementStatusEnum.getName() : null;
    }

    public String getCreateBy(){
        if(StringUtils.isNotEmpty(createBy)){
            if(createBy.indexOf("/")!=-1){
               return createBy.substring(createBy.indexOf("/")+1);
            }
        }
        return "";
    }
}
