package com.tsfyun.scm.dto.user;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.LoginRoleTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 微信公众号登录
 */
@Data
public class WxgzhLoginDTO implements Serializable {
    /**=
     * 角色类型
     */
    @NotEmptyTrim(message = "角色类型不能为空")
    @EnumCheck(clazz = LoginRoleTypeEnum.class,message = "角色类型错误")
    private String roleType;

    @NotEmptyTrim(message = "参数错误")
    private String bindAccessId;
}
