package com.tsfyun.scm.dto.finance;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

import javax.validation.constraints.Digits;
/**
 * <p>
 * 收款核销明细请求实体
 * </p>
 *
 * @since 2020-05-22
 */
@Data
@ApiModel(value="ReceiptAccountMember请求对象", description="收款核销明细请求实体")
public class ReceiptAccountMemberDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "收款单主单不能为空")
   @ApiModelProperty(value = "收款单主单")
   private Long receiptAccountId;

   @ApiModelProperty(value = "进口费用")
   private Long impOrderCostId;

   @NotEmptyTrim(message = "核销单号不能为空")
   @LengthTrim(max = 20,message = "核销单号最大长度不能超过20位")
   @ApiModelProperty(value = "核销单号")
   private String docNo;

   @LengthTrim(max = 20,message = "费用科目ID最大长度不能超过20位")
   @ApiModelProperty(value = "费用科目ID")
   private String expenseSubjectId;

   @LengthTrim(max = 50,message = "费用科目名称最大长度不能超过50位")
   @ApiModelProperty(value = "费用科目名称")
   private String expenseSubjectName;

   @NotNull(message = "核销金额不能为空")
   @Digits(integer = 8, fraction = 2, message = "核销金额整数位不能超过8位，小数位不能超过2位")
   @ApiModelProperty(value = "核销金额")
   private BigDecimal accountValue;

   @NotEmptyTrim(message = "核销类型不能为空")
   @LengthTrim(max = 20,message = "核销类型最大长度不能超过20位")
   @ApiModelProperty(value = "核销类型")
   private String writeOffType;

   @NotEmptyTrim(message = "操作人不能为空")
   @LengthTrim(max = 100,message = "操作人最大长度不能超过100位")
   @ApiModelProperty(value = "操作人")
   private String operation;


}
