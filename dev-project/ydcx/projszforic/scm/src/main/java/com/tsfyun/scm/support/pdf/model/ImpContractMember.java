package com.tsfyun.scm.support.pdf.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @since Created in 2020/4/28 18:09
 */
@Data
public class ImpContractMember implements Serializable {

    private String sort;

    private String commodity;

    private String unit;

    private String quantity;

    private String unitPrice;

    private String amount;

}
