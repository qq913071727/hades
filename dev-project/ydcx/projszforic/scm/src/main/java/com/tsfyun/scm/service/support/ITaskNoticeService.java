package com.tsfyun.scm.service.support;

import com.tsfyun.common.base.security.LoginVO;
import com.tsfyun.scm.dto.support.TaskNoticeDTO;
import com.tsfyun.scm.entity.support.TaskNotice;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.entity.support.TaskNoticeContent;

import java.util.List;

/**
 * <p>
 * 任务通知 服务类
 * </p>
 *

 * @since 2020-04-05
 */
public interface ITaskNoticeService extends IService<TaskNotice> {

    /**
     * 根据任务通知id批量删除
     * @param taskNoticeIds
     */
    void deleteByTaskNoticeIdBatch(List<Long> taskNoticeIds);

    /**=
     * 批量新增任务通知接收人记录
     * @param taskNoticeContent
     * @param loginVO
     * @param datas
     */
    void addBatch(TaskNoticeContent taskNoticeContent, LoginVO loginVO, List<TaskNoticeDTO> datas);

    /**=
     * 根据类容ID查询用户
     * @param tncIds
     * @return
     */
    List<Long> findByPersonId(List<Long> tncIds);

    /**
     * 根据操作码和员工id删除任务通知
     * @param operationCode
     * @param personId
     */
    void deleteByOperationCodeAndPersonId(String operationCode,Long personId);

}
