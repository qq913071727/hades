package com.tsfyun.scm.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.client.fallback.CustomsCodeClientFallback;
import com.tsfyun.scm.system.vo.CustomsCodeVO;
import com.tsfyun.scm.system.vo.CustomsElementsVO;
import com.tsfyun.scm.system.vo.TariffCustomsCodeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 使用name="gateway"，path="scm-system"
 * 使用name="scm-system"都可以
 * 或者测试时直接指定url也可以
 */
@Component
@FeignClient(name = "scm-system",fallbackFactory = CustomsCodeClientFallback.class)
public interface CustomsCodeClient {


    @GetMapping(value = "customsCode/detail")
    Result<CustomsCodeVO> detail(@RequestParam(value = "id")String id);

    @GetMapping(value = "customsElements/getByHsCode")
    Result<List<CustomsElementsVO>> elementDetail(@RequestParam(value = "hsCode")String hsCode);

    @PostMapping(value = "customsCode/tariffCustoms")
    Result<List<TariffCustomsCodeVO>> tariffCustoms( );
}
