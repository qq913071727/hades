package com.tsfyun.scm.vo.order;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 导入出口订单明细
 * @CreateDate: Created in 2021/10/27 09:42
 */
@Data
public class ImportExpOrderMemberVO implements Serializable {

    private String model;

    private String name;

    private String brand;

    private String goodsCode;

    private String unitName;

    private String quantity;

    private String unitPrice;

    private String totalPrice;

    private String cartonNum;

    private String netWeight;

    private String grossWeight;

    private String spec;
}
