package com.tsfyun.scm.service.customer;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.customer.CustomerDeliveryInfoDTO;
import com.tsfyun.scm.dto.customer.client.ClientCustomerDeliveryInfoQTO;
import com.tsfyun.scm.entity.customer.CustomerDeliveryInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.customer.CustomerDeliveryInfoVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-03
 */
public interface ICustomerDeliveryInfoService extends IService<CustomerDeliveryInfo> {

    /**
     * 新增
     * @param dto
     */
    void add(CustomerDeliveryInfoDTO dto,boolean handleDefault);

    /**
     * 修改
     * @param dto
     */
    void edit(CustomerDeliveryInfoDTO dto,boolean handleDefault);

    /**=
     * 批量保存
     * @param customerId
     * @param customerDeliveryInfoDTOS
     */
    void saveList(Long customerId,List<CustomerDeliveryInfoDTO> customerDeliveryInfoDTOS);

    /**
     * 禁用启用
     * @param id
     */
    void activeDisable(Long id);

    /**
     * 设置默认
     * @param id
     */
    void setDefault(Long id);

    /**
     * 取消以前设置的默认收货地址
     * @param customerId
     */
    void handleOldDefault(Long customerId);

    /**
     * 获取客户收货地址
     * @param customerId
     * @return
     */
    List<CustomerDeliveryInfoVO> select(Long customerId);

    List<CustomerDeliveryInfoVO> effectiveList(String customerName);

    /**
     * 分页查询客户收货地址
     * @param dto
     * @return
     */
    PageInfo<CustomerDeliveryInfoVO> pageList(CustomerDeliveryInfoDTO dto);

    /**
     * 获取客户收货地址详细信息
     * @param id
     * @return
     */
    CustomerDeliveryInfoVO detail(Long id);

    /**
     * 根据客户id删除客户收货地址
     * @param customerId
     */
    void removeByCustomerId(Long customerId);

    /**
     * 根据客户id获取客户收货地址列表
     * @param customerId
     * @return
     */
    List<CustomerDeliveryInfo> list(Long customerId);


    /**
     * 根据主键id删除
     * @param id
     */
    void deleteById(Long id);

    /**
     * 根据客户id获取客户收货记录数
     * @param customerId
     * @return
     */
    int countByCustomerId(Long customerId);

    /**
     * 客户端分页查询客户收货地址
     * @param qto
     * @return
     */
    PageInfo<CustomerDeliveryInfo> clientPage(ClientCustomerDeliveryInfoQTO qto);

    /**
     * 客户端删除
     * @param id
     */
    void clientRemove(Long id);

    /**
     * 客户端新增
     * @param dto
     */
    void clientAdd(CustomerDeliveryInfoDTO dto);

    /**
     * 客户端修改
     * @param dto
     */
    void clientEdit(CustomerDeliveryInfoDTO dto);

}
