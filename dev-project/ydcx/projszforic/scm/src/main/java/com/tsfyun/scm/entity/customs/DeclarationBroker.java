package com.tsfyun.scm.entity.customs;

import com.tsfyun.common.base.extension.IdWorker;
import tk.mybatis.mapper.annotation.KeySql;
import javax.persistence.Id;
import java.io.Serializable;
import lombok.Data;


/**
 * <p>
 * 报关行
 * </p>
 *
 *
 * @since 2021-11-15
 */
@Data
public class DeclarationBroker implements Serializable {

     private static final long serialVersionUID=1L;


    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 名称
     */

    private String name;

    /**
     * 统一信用代码
     */

    private String socialNo;

    /**
     * 注册海关编码
     */

    private String customsNo;

    /**
     * 禁用
     */

    private Boolean disabled;

    /**
     * 备注
     */

    private String memo;


}
