package com.tsfyun.scm.vo.customs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 报关单详情信息
 * @since Created in 2020/5/6 09:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeclarationPlusDetailVO implements Serializable {

    /**
     * 报关单主单信息
     */
    private DeclarationVO declaration;

    /**
     * 报关单明细信息
     */
    private List<DeclarationMemberVO> members;

}
