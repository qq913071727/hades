package com.tsfyun.scm.vo.customer;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/11 16:01
 */
@Data
public class AgreementApprovalVO implements Serializable {

    private AgreementDetailVO agreement;

    /**
     * 进口条款
     */
    private ImpClauseVO impClause;

    /**
     * 进口报价信息
     */
    private ImpQuoteVO impQuote;

}
