package com.tsfyun.scm.controller.customer;


import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.annotation.DuplicateSubmit;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.dto.TaskDTO;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.dto.customer.ExpAgreementPlusDTO;
import com.tsfyun.scm.dto.customer.ExpAgreementQTO;
import com.tsfyun.scm.service.customer.IExpAgreementService;
import com.tsfyun.scm.vo.customer.ExpAgreementVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 出口协议 前端控制器
 * </p>
 *
 *
 * @since 2021-09-07
 */
@RestController
@RequestMapping("/expAgreement")
public class ExpAgreementController extends BaseController {

    @Autowired
    private IExpAgreementService expAgreementService;

    /**
     * 新增出口协议
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "add")
    Result<Void> add(@RequestBody @Validated ExpAgreementPlusDTO dto) {
        expAgreementService.add(dto);
        return success();
    }

    /**
     * 分页查询（带权限）
     * @param qto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<ExpAgreementVO>> pageList(@ModelAttribute @Valid ExpAgreementQTO qto) {
        PageInfo<ExpAgreementVO> pageInfo = expAgreementService.pageList(qto);
        return success((int)pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 修改出口协议
     * @param dto
     * @return
     */
    @DuplicateSubmit
    @PostMapping(value = "edit")
    Result<Void> edit(@RequestBody @Validated(UpdateGroup.class) ExpAgreementPlusDTO dto) {
        expAgreementService.edit(dto);
        return success();
    }

    /**
     * 详情查询
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<ExpAgreementVO> detail(@RequestParam(value = "id")Long id,@RequestParam(value = "operation",required = false)String operation) {
        return success(expAgreementService.detail(id,operation));
    }

    /**=
     * 审核
     * @return
     */
    @PostMapping(value = "examine")
    public Result<Void> examine(@ModelAttribute @Valid TaskDTO dto){
        expAgreementService.examine(dto);
        return success();
    }

    /**=
     * 删除
     * @return
     */
    @PostMapping(value = "delete")
    public Result<Void> delete(@RequestParam(value = "id")Long id){
        expAgreementService.delete(id);
        return success();
    }

    /**=
     * 原件签回
     * @return
     */
    @PostMapping(value = "originSignBack")
    public Result<Void> originSignBack(@RequestParam(value = "id")Long id){
        expAgreementService.signBack(Lists.newArrayList(id));
        return success();
    }

    /**=
     * 根据客户获取有效协议报价
     * @param customerId
     * @return
     */
    @PostMapping(value = "getAuditAgreement")
    public Result<List<ExpAgreementVO>> getAuditAgreement(@RequestParam(value = "customerId")Long customerId){
        return success(expAgreementService.checkCustomerExpAgreement(customerId));
    }

}

