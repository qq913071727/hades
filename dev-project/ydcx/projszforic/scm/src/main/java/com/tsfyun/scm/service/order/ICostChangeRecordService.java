package com.tsfyun.scm.service.order;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.dto.order.CostChangeRecordDTO;
import com.tsfyun.scm.dto.order.CostChangeRecordQTO;
import com.tsfyun.scm.entity.order.CostChangeRecord;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.vo.order.CostChangeRecordVO;

/**
 * <p>
 * 费用变更记录 服务类
 * </p>
 *

 * @since 2020-05-27
 */
public interface ICostChangeRecordService extends IService<CostChangeRecord> {

    /**
     * 分页列表
     * @param qto
     * @return
     */
    PageInfo<CostChangeRecordVO> pageList(CostChangeRecordQTO qto);

    /**
     * 调整费用
     * @param dto
     */
    void adjust(CostChangeRecordDTO dto);

    /**
     * 根据订单删除
     * @param orderId
     */
    void removeByOrderId(Long orderId);

}
