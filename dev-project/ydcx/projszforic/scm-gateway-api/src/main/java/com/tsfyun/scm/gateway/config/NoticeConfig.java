package com.tsfyun.scm.gateway.config;

import com.tsfyun.scm.gateway.config.properties.NoticeProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 *
 * @Date 2020/3/4 14:20
 * @Version V1.0
 */
@Configuration
public class NoticeConfig {

    /**
     * 钉钉异常通知
     * @return
     */
    @Bean("noticeProperties")
    public NoticeProperties noticeProperties(){
        return new NoticeProperties();
    }


}
