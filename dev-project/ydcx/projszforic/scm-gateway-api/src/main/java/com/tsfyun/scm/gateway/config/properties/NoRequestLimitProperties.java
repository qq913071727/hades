package com.tsfyun.scm.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/7 15:03
 */
@RefreshScope
@ConfigurationProperties(prefix = "request.nolimit")
@Component
@Data
public class NoRequestLimitProperties {

    private String[] ignored;

}
