package com.tsfyun.scm.gateway.config.properties;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 不校验登录配置
 * add by rick at 2020-02-26
 */
@RefreshScope
@ConfigurationProperties(prefix = "security.auth")
@Component
public class PermitUrlProperties {

	/**
	 * 监控中心和swagger需要访问的url
	 */
	private static final String[] ENDPOINTS = { 
			"/**/actuator/**" , "/**/actuator/**/**" ,  //断点监控
			"/**/v2/api-docs/**", "/**/swagger-ui.html", "/**/swagger-resources/**", "/**/webjars/**", //swagger
			"/**/turbine.stream","/**/turbine.stream**/**", "/**/hystrix", "/**/hystrix.stream", "/**/hystrix/**", "/**/hystrix/**/**",	"/**/proxy.stream/**" , //熔断监控
			"/**/druid/**", "/**/favicon.ico", "/**/prometheus" 
	};

	private String[] ignored;

	/**
	 * 需要放开权限的url
	 *
	 *         自定义的url
	 * @return 自定义的url和监控中心需要访问的url集合
	 */
	public String[] getIgnored() {
		if (ignored == null || ignored.length == 0) {
			return ENDPOINTS;
		}

		List<String> list = new ArrayList<>();
		for (String url : ENDPOINTS) {
			list.add(url);
		}
		if(null != ignored && ignored.length > 0) {
			for (String url : ignored) {
				list.add(url);
			}
		}
		return list.toArray(new String[list.size()]);
	}

	public void setIgnored(String[] ignored) {
		this.ignored = ignored;
	}

}
