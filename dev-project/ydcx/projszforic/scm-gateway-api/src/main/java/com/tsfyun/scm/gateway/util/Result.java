package com.tsfyun.scm.gateway.util;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * 通用返回结果 泛型 方便API解释返回结果
 */
@Data
@NoArgsConstructor
public class Result<T> implements Serializable {

    public static final String DEFAULT_ERROR_MSG = "服务开小差了，马上就好，请您稍后再试";

    private Integer count = 0;//数据条数

    //状态码
    private String code;

    //返回信息
    private String message;

    //返回数据
    private T data;

    public Result(String code, String message) {
        this.code = code;
        this.message = message;
    }


    private Result(T data) {
        this.code = "1";
        this.message = "请求成功";
        this.data = data;
    }

    private Result(Integer count, T data) {
        this.code = "1";
        this.message = "请求成功";
        this.count = count;
        this.data = data;
    }


    /**
     * 成功时候的调用
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>(data);
    }

    /**
     * 成功时候的调用
     */
    public static Result<Void> success() {
        return new Result<Void>(null);
    }

    /**
     * 成功时候的调用
     */
    public static Result<Void> success(String code, String msg) {
        return new Result<Void>(code, msg);
    }


    /**
     * 失败时候的调用
     */
    public static <T> Result<T> error(String msg) {
        return new Result<T>("0", msg);
    }

    public static <T> Result<T> error(String code, String msg) {
        code = Objects.isNull(code) ? "0" : code;
        return new Result<T>(code, msg);
    }

    /**
     * 成功时候的调用
     */
    public static <T> Result<T> success(Integer count, T data) {
        return new Result<T>(count, data);
    }

    /**=
     * 是否成功
     * @return
     */
    public Boolean isSuccess(){
        return Objects.equals(this.code,"1");
    }
}

