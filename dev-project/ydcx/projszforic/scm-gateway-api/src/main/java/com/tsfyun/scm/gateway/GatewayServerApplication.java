package com.tsfyun.scm.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/16 17:12
 */
@SpringBootApplication
@EnableDiscoveryClient
@Slf4j
public class GatewayServerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.
                run(GatewayServerApplication.class, args);
        Environment env = application.getEnvironment();
        String host = "127.0.0.1";
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {}
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 '{}' 运行成功!\n\t" +
                        "Swagger文档: \t\thttp://{}:{}{}{}/doc.html\n\t" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                host,
                env.getProperty("server.port"),
                env.getProperty("server.servlet.context-path", ""),
                env.getProperty("spring.mvc.servlet.path", "")
        );
    }


}
