package com.tsfyun.scm.gateway.constant;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/7 15:51
 */
public interface LimitConstant {

    String FREQUENCY_VALIDATE_CODE = "frequency:code:";

    String FREQUENCY_LIMIT = "frequency:limit:";

    //免登录请求标识
    String MDL_REQUEST_URL = "MDL";
    //不限流请求标识
    String NO_LIMIT_URL = "BXL";

}
