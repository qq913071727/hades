package com.tsfyun.scm.gateway.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/21 10:22
 */
public class FrequencyUtil {

    /**
     * 请求频繁响应内容
     * @param limitRequestId
     * @return
     */
    public static JSONObject wrapResult(String limitRequestId) {
        JSONObject resultJson = new JSONObject();
        resultJson.fluentPut("code",String.valueOf(HttpStatus.TOO_MANY_REQUESTS.value())).fluentPut("message","操作频繁，请稍后再操作")
                .fluentPut("limitRequestId", limitRequestId);
        return resultJson;
    }


}
