package com.tsfyun.scm.gateway.constant;

/**
 * @Description: 跟踪常量
 * @CreateDate: Created in 2020/9/4 14:28
 */
public class TraceConstant {
	/**
     * 日志跟踪id名。
     */
    public static final String LOG_TRACE_ID = "traceId";
    
    public static final String LOG_B3_TRACEID = "X-B3-TraceId";

    /**
     * 请求头跟踪id名。
     */
    public static final String HTTP_HEADER_TRACE_ID = "app_trace_id";
    
    
    
}
