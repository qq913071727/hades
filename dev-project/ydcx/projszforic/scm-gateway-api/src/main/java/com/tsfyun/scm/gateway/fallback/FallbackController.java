package com.tsfyun.scm.gateway.fallback;

import com.tsfyun.scm.gateway.config.properties.NoticeProperties;
import com.tsfyun.scm.gateway.support.dd.DingTalkNoticeUtil;
import com.tsfyun.scm.gateway.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;

/**
 * @Description: 请求异常降级
 * @CreateDate: Created in 2020/9/16 17:51
 */
@RestController
@Slf4j
public class FallbackController {

    @Autowired
    private NoticeProperties noticeProperties;

    @Value("${spring.profiles.active:dev}")
    private String profiles;

    @RequestMapping("/fallback")
    public Mono<Result> fallback(ServerRequest request) {
        log.error("网关进入降级处理....");
        //此处钉钉通知
        DingTalkNoticeUtil.send2DingDingException(request.exchange().getRequest(),"服务降级",null,noticeProperties.getDingdingUrl(),profiles);
        return Mono.just(Result.error(Result.DEFAULT_ERROR_MSG));
    }

}
