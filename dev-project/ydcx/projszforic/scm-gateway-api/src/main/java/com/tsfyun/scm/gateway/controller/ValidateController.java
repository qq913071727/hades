package com.tsfyun.scm.gateway.controller;

import com.tsfyun.scm.gateway.constant.LimitConstant;
import com.tsfyun.scm.gateway.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/18 16:19
 */
@Slf4j
@RestController
@RequestMapping(value = "/validate")
public class ValidateController  {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping("/checkLimit")
    public Mono<Result> checkLimit(@RequestParam(value = "verifyCode")String verifyCode, @RequestParam(value = "limitRequestId")String limitRequestId) {
        //获取提交的验证码和库里的验证码是否一致
        String redisValidateCode = stringRedisTemplate.opsForValue().get(LimitConstant.FREQUENCY_VALIDATE_CODE + limitRequestId);
        if(!StringUtils.isEmpty(redisValidateCode)) {
            if(!Objects.equals(redisValidateCode,verifyCode)) {
              return Mono.just(Result.error("您的验证不通过，请重新验证或稍后再试"));
            }
            stringRedisTemplate.delete(LimitConstant.FREQUENCY_VALIDATE_CODE + limitRequestId);
            stringRedisTemplate.delete(LimitConstant.FREQUENCY_LIMIT + limitRequestId);
        }
        return Mono.just(Result.success());
    }

}
