package com.tsfyun.scm.gateway.controller;

import com.tsfyun.scm.gateway.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @since Created in 2020/4/3 17:59
 */
@Slf4j
@RestController
@RequestMapping(value = "/monitor")
public class MonitorController {

    @Value("${gateway.ipcheck.limit:1440}")
    private long blackIpLimit;

    @Value("${gateway.manage.salt:asdfg12345}")
    private String salt;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/prohibitIp")
    public Mono<Result> verificationCode(@RequestParam(value = "ip")String ip, @RequestParam(value = "acceptSalt")String acceptSalt) {
        if(!Objects.equals(acceptSalt,salt)) {
            log.info("配置盐:{}，请求盐:{}",salt,acceptSalt);
            throw new RuntimeException("非法请求");
        }
        stringRedisTemplate.opsForValue().set("BLACK_IP:" + ip, "1", blackIpLimit, TimeUnit.MINUTES);
        return Mono.just(Result.success());
    }

}
