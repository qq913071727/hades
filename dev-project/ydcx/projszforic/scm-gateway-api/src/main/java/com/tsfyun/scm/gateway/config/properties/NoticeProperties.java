package com.tsfyun.scm.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @Description
 *
 * @Date 2020/3/4 14:08
 * @Version V1.0
 */
@ConfigurationProperties(prefix = "notice")
@Data
public class NoticeProperties {

    //钉钉地址
    private List<String> dingdingUrl;



}