package com.tsfyun.scm.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description: 限流配置
 * @CreateDate: Created in 2020/12/18 10:31
 */
@RefreshScope
@Component
@ConfigurationProperties(prefix = "ratelimiter-conf")
public class RateLimiterConf {

    //每秒最大访问次数
    private static final String DEFAULT_REPLENISHRATE = "default.replenishRate";
    private static final Integer DEFAULT_REPLENISHRATE_VALUE = 3;

    //令牌桶最大容量
    private static final String DEFAULT_BURSTCAPACITY = "default.burstCapacity";
    private static final Integer DEFAULT_BURSTCAPACITY_VALUE = 8;

    private Map<String, Integer> rateLimitMap = new ConcurrentHashMap<String, Integer>() {
        {
            put(DEFAULT_REPLENISHRATE, DEFAULT_REPLENISHRATE_VALUE);
            put(DEFAULT_BURSTCAPACITY, DEFAULT_BURSTCAPACITY_VALUE);
        }
    };

    public Map<String, Integer> getRateLimitMap() {
        return rateLimitMap;
    }

    public void setRateLimitMap(Map<String, Integer> rateLimitMap) {
        this.rateLimitMap = rateLimitMap;
    }
}
