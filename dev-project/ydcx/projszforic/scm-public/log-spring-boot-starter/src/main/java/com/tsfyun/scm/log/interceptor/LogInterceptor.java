package com.tsfyun.scm.log.interceptor;

import com.tsfyun.common.base.constant.BaseContextConstant;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 日志拦截器
 * @CreateDate: Created in 2020/9/16 14:23
 */
public class LogInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String traceId = request.getHeader(BaseContextConstant.HTTP_HEADER_TRACE_ID);
        if(StringUtils.hasText(traceId)) {
            MDC.put(BaseContextConstant.LOG_TRACE_ID,traceId);
        }
        return true;
    }

}
