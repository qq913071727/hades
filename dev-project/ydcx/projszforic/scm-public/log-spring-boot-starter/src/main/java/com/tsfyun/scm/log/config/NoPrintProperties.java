package com.tsfyun.scm.log.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @CreateDate: Created in 2021/6/21 17:07
 */
@Configuration
@ConfigurationProperties(prefix = NoPrintProperties.PREFIX)
@Data
public class NoPrintProperties {

    public static final String PREFIX = "log";

    private List<String> noPrints;

}
