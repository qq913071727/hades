package com.tsfyun.scm.log.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @Description: 日志实体
 * @CreateDate: Created in 2020/9/16 14:44
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysLog implements Serializable {

	private static final long serialVersionUID = -5398795297842978376L;
	//设备来源
	private String device;
	//日志跟踪id
	private String traceId;
	//请求ip
	private String requestIp;
	//用户id
	private Long userId;
     //用户名
	private String username;
    //归属模块
	private String module;
    //执行方法的参数值
	private String params;
	//请求类和方法
	private String actionMethod;
	//请求方式
	private String httpMethod;
	//请求地址
	private String requestUrl;
    //是否执行成功
	private Boolean flag;
	//返回值
	private String result;
	//备注（记录异常信息）
	private String remark;
	//开始时间
	private LocalDateTime startTime;
	//结束时间
	private LocalDateTime endTime;
	//执行时间
	private Long execTime;
}
