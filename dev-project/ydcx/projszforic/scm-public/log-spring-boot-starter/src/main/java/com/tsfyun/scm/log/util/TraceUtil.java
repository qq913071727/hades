package com.tsfyun.scm.log.util;

import javax.servlet.http.HttpServletRequest;

import com.tsfyun.common.base.constant.BaseContextConstant;
import com.tsfyun.common.base.util.StringUtils;
import org.slf4j.MDC;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/16 14:47
 */
public class TraceUtil {

	public static String getTrace() {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		
		String appTraceId = request.getHeader(BaseContextConstant.HTTP_HEADER_TRACE_ID);
		//未经过HandlerInterceptor的设置，经过了HandlerInterceptor会放在MDC中
		String logTraceId = MDC.get(BaseContextConstant.LOG_TRACE_ID);
		if(StringUtils.isNotEmpty(appTraceId)) {
			MDC.put(BaseContextConstant.LOG_TRACE_ID, appTraceId);
		}
		return appTraceId;

	}

}
