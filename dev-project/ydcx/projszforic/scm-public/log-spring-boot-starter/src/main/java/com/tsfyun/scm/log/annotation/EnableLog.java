package com.tsfyun.scm.log.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.tsfyun.scm.log.selector.LogImportSelector;

import static java.lang.annotation.ElementType.TYPE;

/**
 * @Description: 日志开启开关
 * @CreateDate: Created in 2020/9/16 14:15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE})
@Documented
@Import(LogImportSelector.class)
public @interface EnableLog {
}
