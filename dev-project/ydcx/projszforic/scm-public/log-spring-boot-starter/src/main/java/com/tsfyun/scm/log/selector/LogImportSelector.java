package com.tsfyun.scm.log.selector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/16 14:21
 */
public class LogImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
            return new String[ ]{
                    "com.tsfyun.scm.log.config.LogAutoConfig",
                    "com.tsfyun.scm.log.config.LogAspect"
            };
    }
}
