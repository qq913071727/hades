local key1 = KEYS[1]
local key2 = KEYS[2]

local val1 = ARGV[1]
local val2 = ARGV[2]
local val3 = ARGV[3]
local expireTime = tonumber(ARGV[3])
redis.call("set", key1, val1)
redis.call("set", key2, val2)
redis.call("expire",key1,expireTime);
redis.call("expire",key2,expireTime);
return 1