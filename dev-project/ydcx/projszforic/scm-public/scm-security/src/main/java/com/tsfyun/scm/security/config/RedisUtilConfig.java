package com.tsfyun.scm.security.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/8 14:47
 */
@Configuration
public class RedisUtilConfig {

    @Bean
    @ConditionalOnClass(RedisTemplate.class)
    @ConditionalOnMissingBean(name = "stringRedisUtils")
    public StringRedisUtils stringRedisUtils() {
        return new StringRedisUtils();
    }
}
