package com.tsfyun.scm.security.config;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.util.ResultUtil;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TypeUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class UnauthorizedEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        String forceLogoutErrmsg = TypeUtils.castToString(StringUtils.null2EmptyWithTrim(httpServletRequest.getAttribute("loginErrmsg")),"请登录");
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        ResultUtil.errorBack(httpServletResponse,new Result("" + HttpServletResponse.SC_UNAUTHORIZED, forceLogoutErrmsg));
    }
}
