package com.tsfyun.scm.security.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

@Configuration
public class LuaPlugin {

    /**
     * Method Description
     * 登录/续签脚本
     * @return null
     *
     * @date 2019/12/2 9:43
     */
    @ConditionalOnMissingBean(name = "loginRedisScript")
    @Bean(name = "loginRedisScript")
    public DefaultRedisScript loginRedisScript() {
        DefaultRedisScript loginRedisScript = new DefaultRedisScript();
        loginRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("/META-INF/scripts/login.lua")));
        loginRedisScript.setResultType(Long.class);
        return loginRedisScript;
    }


}
