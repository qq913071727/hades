package com.tsfyun.common.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 费用科目响应实体
 * </p>
 *

 * @since 2020-03-16
 */
@Data
@ApiModel(value="ExpenseSubject响应对象", description="响应实体")
public class ExpenseSubjectVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "科目编码")
     private String id;

    @ApiModelProperty(value = "科目名称")
    private String name;

    @ApiModelProperty(value = "禁用状态")
    private Boolean disabled;

    @ApiModelProperty(value = "锁定状态")
    private Boolean locking;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "备注信息")
    private String memo;


}
