package com.tsfyun.common.base.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxNoticeAttr implements Serializable {

    private String value;
    private String color = "#0ba9f9";

    public WxNoticeAttr(String value){
        this.value = value;
    }

}
