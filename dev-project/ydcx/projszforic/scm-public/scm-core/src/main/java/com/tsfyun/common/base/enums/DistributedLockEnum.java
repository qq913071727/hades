package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 分布式锁key定义枚举
 * @since Created in 2019/11/8 15:46
 */
public enum DistributedLockEnum {

    //进口订单
    IMP_ORDER_CONFIRM_IMP("imp_order_confirm_imp","进口订单确认"),
    IMP_ORDER_GENERATE_SALES_CONTRACT("imp_order_generate_sales_contract","自营订单生成销售合同"),
    IMP_ORDER_BACK_INSPECTION("imp_order_back_inspection","进口订单确认进口后退单"),

    //出口订单
    EXP_ORDER_CONFIRM("exp_order_confirm","出口订单确认"),
    EXP_ORDER_BACK_INSPECTION("exp_order_back_inspection","出口订单确认出口后退单"),

    RECORD_TRANSACTION("record_transaction", "登记流水"),
    DECLARATION_CONFIRM("declaration_confirm", "确认报关单"),
    DECLARATION_REVIEW("declaration_review", "复核报关单"),
    DECLARATION_COMPLETED("declaration_completed", "申报完成"),

    SUBMIT_RISK("submit_risk", "提交风控审批"),
    RISK_APPROVAL("risk_approval", "风控审批审核"),


    CROSSBORDER_BIND_ORDER("crossborder_bind_order", "跨境运输单绑定订单"),
    CROSSBORDER_BIND("crossborder_bind", "跨境运输单绑单完成"),
    CROSSBORDER_REVIEW("crossborder_review", "跨境运输单复核"),
    CROSSBORDER_DEPART("crossborder_depart", "跨境运输单发车"),
    CROSSBORDER_SIGN("crossborder_sign", "跨境运输单签收"),

    IMP_ORDER_GENERATE_INVOICE("imp_order_generate_invoice","根据订单生成发票申请"),

    IMP_SALES_CONTRACT_DELETE("imp_sales_contract_delete","删除销售合同"),
    SALES_CONTRACT_GENERATE_INVOICE("imp_sales_contract_delete","根据销售合同生成发票申请"),

    AUTO_COST_WRITE_OFF("auto_cost_write_off","费用自动核销"),
    IMP_ORDER_WRITE_OFF("imp_order_write_off","进口订单费用核销"),
    EXP_ORDER_WRITE_OFF("exp_order_write_off","出口订单费用核销"),

    ORDER_COST_ADJUST("order_cost_adjust","进出口订单费用调整"),
    ORDER_COST_IMPORT("order_cost_import","批量导入费用"),

    PAYMENT_EDIT("payment_edit","付汇申请修改"),

    REFUND_APPLY("refund_apply","退款申请"),
    REFUND_APPROVE("refund_approve","退款申请商务审核"),
    REFUND_TO_VOLD("refund_to_void","退款申请作废"),
    REFUND_CONFIRM_BANK("refund_confirm_bank","退款申请确定付款行"),
    REFUND_BANK_RECEIPT("refund_bank_receipt","退款申请上传水单"),

    SERVICE_AGREEMENT_SIGN("service_agreement_sign","客户签署协议"),

    CUSTOMER_CONFIRM_TAX("customer_confirm_tax","客户确定税收分类编码"),

    //报关系统
    MFT_GET_DOCNO("MFT_GET_DOCNO","舱单获取系统单号"),
    MFT_IMPORT_SINGLE("MFT_IMPORT_SINGLE","舱单导入单一窗口"),
    DECL_IMPORT_SINGLE("DECL_IMPORT_SINGLE","报关单导入单一窗口"),
    DECL_IMPORT_CONSING("DECL_IMPORT_CONSING","生成电子委托导入单一窗口"),

    DECL_CHANGE_COST("DECL_CHANGE_COST","报关费用变更"),

    GEN_DOC_NO("GEN_DOC_NO","获取系统单号"),

    CEB_FILE_WATCH("ceb_file_watch","跨境电商回执文件监听"),

    EXP_DELIVERY_NOTE_BIND_ORDER_MEMBER("exp_delivery_note_bind_order_member","出口境外派送单绑定订单明细"),

    ;
    private String code;
    private String name;

    DistributedLockEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static DistributedLockEnum of(String code) {
        return Arrays.stream(DistributedLockEnum.values()).filter(r -> Objects.equals(code,r.getCode())).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
