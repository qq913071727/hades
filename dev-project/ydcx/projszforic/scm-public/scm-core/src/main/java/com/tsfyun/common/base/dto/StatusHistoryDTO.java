package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description: 状态历史请求实体
 * @CreateDate: Created in 2021/9/9 15:07
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Data
public class StatusHistoryDTO implements Serializable {

    /**
     * 单据操作
     */
    @NotEmptyTrim(message = "单据操作不能为空")
    @EnumCheck(clazz = DomainOprationEnum.class,message = "单据操作错误")
    private String operation;

    /**
     * 单据id
     */
    @NotEmptyTrim(message = "单据id不能为空")
    @LengthTrim(max = 32,message = "单据id最大长度不能超过32位")
    private String domainId;

    /**
     * 单据类型
     */
    @NotEmptyTrim(message = "单据类型不能为空")
    @LengthTrim(max = 50,message = "单据类型最大长度不能超过50位")
    private String domainType;

    /**
     * 原状态编码
     */
    @LengthTrim(max = 20,message = "原状态编码最大长度不能超过20位")
    private String historyStatusId;

    /**
     * 原状态名称
     */
    @LengthTrim(max = 50,message = "原状态名称最大长度不能超过50位")
    private String historyStatusName;

    /**
     * 新状态编码
     */
    @LengthTrim(max = 20,message = "新状态编码最大长度不能超过20位")
    private String nowStatusId;

    /**
     * 新状态名称
     */
    @LengthTrim(max = 50,message = "新状态名称最大长度不能超过50位")
    private String nowStatusName;

    /**
     * 备注
     */
    @LengthTrim(max = 500,message = "备注最大长度不能超过500位")
    private String memo;

    /**
     * 操作人
     */
    @LengthTrim(max = 50,message = "操作人最大长度不能超过50位")
    private String operator;

}
