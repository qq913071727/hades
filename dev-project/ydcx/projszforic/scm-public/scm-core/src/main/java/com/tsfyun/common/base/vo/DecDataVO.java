package com.tsfyun.common.base.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**=
 * 报关单参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DecDataVO implements Serializable {
    //参数类型
    private String type;
    //参数值
    private List<BaseDataSimple> datas;
}
