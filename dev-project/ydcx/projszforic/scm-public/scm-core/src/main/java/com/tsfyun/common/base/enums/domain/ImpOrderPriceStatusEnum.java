package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 订单审价状态
 */
public enum ImpOrderPriceStatusEnum implements OperationEnum {
    WAIT_EXAMINE("waitExamine", "待审核"),
    PASSED("passed", "已通过"),
    RETURNED("returned", "已退回")
    ;
    private String code;

    private String name;

    ImpOrderPriceStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ImpOrderPriceStatusEnum of(String code) {
        return Arrays.stream(ImpOrderPriceStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
