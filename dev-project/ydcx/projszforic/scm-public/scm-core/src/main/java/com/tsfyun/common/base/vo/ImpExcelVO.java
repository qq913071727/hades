package com.tsfyun.common.base.vo;

import lombok.Data;

import java.io.Serializable;

/**=
 * Excel导入成功返回
 */
@Data
public class ImpExcelVO implements Serializable {

    private Integer totalCount = 0;//总条数
    private Integer successCount = 0;//成功条数
    private String message = "";//消息
}
