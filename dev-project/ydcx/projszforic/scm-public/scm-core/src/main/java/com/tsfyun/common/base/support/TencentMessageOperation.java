package com.tsfyun.common.base.support;

import com.google.common.collect.ImmutableMap;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;
import com.tsfyun.common.base.enums.shortmsg.TencentMessageTemplate;

import java.util.Map;

/**
 * @Description: 操作和腾讯云模板映射
 * @CreateDate: Created in 2021/9/15 14:34
 */
public class TencentMessageOperation implements MessageOperation{

    /**
     * 阿里云模板和操作映射
     */
    private static Map<MessageNodeEnum, OperationEnum> operationMap = ImmutableMap.<MessageNodeEnum, OperationEnum>builder()

            //通用认证
            .put(MessageNodeEnum.COMMON_PROVE,TencentMessageTemplate.PROVE)
            //新用户注册
            .put(MessageNodeEnum.NEW_REGISTER,TencentMessageTemplate.NEW_REGISTER)
            //登录
            .put(MessageNodeEnum.REGISTER_LOGIN,TencentMessageTemplate.LOGIN_CONFIRM)
            //找回密码
            .put(MessageNodeEnum.RETRIEVE_PASSWORD, TencentMessageTemplate.PROVE)
            //修改密码
            .put(MessageNodeEnum.CHANGE_PASSWORD,TencentMessageTemplate.CHANGE_PASSWORD)

            .build();

    @Override
    public ShortMessagePlatformEnum getPlatform() {
        return ShortMessagePlatformEnum.TECENT;
    }

    @Override
    public Map<MessageNodeEnum, OperationEnum> getMessageTemplate() {
        return operationMap;
    }

    /**
     * 内部类单例模式
     */
    private static class TencentMessageOperationInstance{
        private static final TencentMessageOperation instance = new TencentMessageOperation();
    }

    private TencentMessageOperation(){}

    public static TencentMessageOperation getInstance(){
        return TencentMessageOperationInstance.instance;
    }

}
