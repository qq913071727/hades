package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 人员类型
 * @since Created in 2020/2/22 15:46
 */
public enum PersonTypeEnum {


    INSIDE("inside", "租户内部用户"),
    OUTSIDE("outside", "租户外部用户"),
    OTHER("other", "外部合作客户"),
    ;
    private String code;
    private String name;

    PersonTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static PersonTypeEnum of(String code) {
        return Arrays.stream(PersonTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
