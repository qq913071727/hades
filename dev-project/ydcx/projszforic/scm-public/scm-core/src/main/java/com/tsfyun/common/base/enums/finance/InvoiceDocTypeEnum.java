package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 发票费用单据类型
 * @CreateDate: Created in 2020/5/15 16:43
 */
public enum InvoiceDocTypeEnum {
    INVOICE("invoice", "发票"),
    CONTRACT("contract", "合同"),
    ;
    private String code;
    private String name;

    InvoiceDocTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static InvoiceDocTypeEnum of(String code) {
        return Arrays.stream(InvoiceDocTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}