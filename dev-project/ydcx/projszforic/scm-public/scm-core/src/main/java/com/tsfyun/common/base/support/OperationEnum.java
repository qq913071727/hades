package com.tsfyun.common.base.support;

/**
 * @Description:
 * @since Created in 2020/4/2 16:47
 */
public interface OperationEnum {

    String getCode();

    String getName();

}
