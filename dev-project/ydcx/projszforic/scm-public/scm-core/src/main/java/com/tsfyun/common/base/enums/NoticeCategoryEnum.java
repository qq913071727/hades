package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 公告分类
 * @CreateDate: Created in 2020/12/15 10:05
 */
public enum NoticeCategoryEnum {

    INDUSTRY_TRENDS("industry_trends", "行业动态"),
    POLICIES("policies", "政策法规"),
    XG_NOTICE("xg_notice", "企业公告"),
    XG_NEWS("xg_news", "企业新闻"),
    ;
    private String code;
    private String name;

    NoticeCategoryEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static NoticeCategoryEnum of(String code) {
        return Arrays.stream(NoticeCategoryEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
