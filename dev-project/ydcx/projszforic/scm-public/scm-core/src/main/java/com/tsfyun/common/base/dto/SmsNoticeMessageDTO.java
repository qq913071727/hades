package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.enums.MessageNodeEnum;
import lombok.Data;

import java.util.LinkedHashMap;

/**
 * @Description: 发送短信请求实体
 * @CreateDate: Created in 2020/12/22 09:17
 */
@Data
public class SmsNoticeMessageDTO {

    //不能为空
    private String phoneNo;

    //短信节点（不能为空）
    private MessageNodeEnum messageNodeEnum;

    //参数值（需要按短信模板AliYunMessageTemplate顺序填充）
    private LinkedHashMap<String,String> params;

}
