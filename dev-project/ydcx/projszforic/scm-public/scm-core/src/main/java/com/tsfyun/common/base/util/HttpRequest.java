package com.tsfyun.common.base.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * =
 * HTTP请求
 */
public class HttpRequest {

    private static final Logger logger = LoggerFactory.getLogger(HttpRequest.class);

    public static String sendGet(String url) {
        Map<String, String> headers = new LinkedHashMap<>();
        return sendGet(url, headers, "UTF-8");
    }

    public static String sendGet(String url, Map<String, String> headers) {
        return sendGet(url, headers, "UTF-8");
    }

    public static String sendGet(String url, Map<String, String> headers, String charsetName) {
        try {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setReadTimeout(60000);
            requestFactory.setConnectTimeout(60000);
            RestTemplate restTemplate = new RestTemplate(requestFactory);
            restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(Charset.forName(charsetName)));
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.set("accept", "*/*");
            requestHeaders.set("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36");
            if (headers != null && headers.size() > 0) {
                for (Map.Entry<String, String> headerMap : headers.entrySet()) {
                    requestHeaders.set(headerMap.getKey(), headerMap.getValue());
                }
            }
            HttpEntity<String> requestEntity = new HttpEntity<String>(null, requestHeaders);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            }
        } catch (Exception e) {
            logger.info("请求地址：", url);
            logger.info("sendGet请求失败：", e);
        }
        return "";
    }

    public static String postJSON(String url, String json) {
        try {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setReadTimeout(30000);
            requestFactory.setConnectTimeout(30000);
            RestTemplate restTemplate = new RestTemplate(requestFactory);
            restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(Charset.forName("UTF-8")));

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.set("accept", "application/json, text/javascript, */*; q=0.01");
            requestHeaders.set("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
            requestHeaders.set("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36");
            MediaType contentType = MediaType.parseMediaType("application/json; charset=UTF-8");
            requestHeaders.setContentType(contentType);

            HttpEntity<String> requestEntity = new HttpEntity<String>(json, requestHeaders);
            ResponseEntity<String> response = restTemplate.postForEntity(url, requestEntity, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            }
        } catch (Exception e) {
            logger.info("请求失败：", e);
        }
        return "";
    }
}
