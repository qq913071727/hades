package com.tsfyun.common.base.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:
 * @since Created in 2020/4/2 16:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperationInfo implements Serializable {

    //操作名称
    private String name;

    //操作允许的状态
    private String[] status;

    //备用字段
    private String memo;

    public OperationInfo (String[] status) {
        this.status = status;
    }

    public OperationInfo (String name,String[] status) {
        this.name = name;
        this.status = status;
    }


}
