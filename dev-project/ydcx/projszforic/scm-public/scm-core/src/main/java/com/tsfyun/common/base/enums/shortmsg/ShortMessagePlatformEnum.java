package com.tsfyun.common.base.enums.shortmsg;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 发送短信平台
 * @CreateDate: Created in 2021/9/15 14:43
 */
public enum ShortMessagePlatformEnum {

    ALIYUN("aliyun", "阿里云"),
    TECENT("tencent", "腾讯云"),
    WELINK("welink", "微网通联"),
    ;
    private String code;
    private String name;

    //参数分隔符
    public static final String PARAM_SPLIT = "|#|";

    ShortMessagePlatformEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ShortMessagePlatformEnum of(String code) {
        return Arrays.stream(ShortMessagePlatformEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
