package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 出口协议报价单状态（长度20位以内）
 * @since Created in 2020/4/1 10:41
 */
public enum ExpAgreementStatusEnum implements OperationEnum {
    SAVED("saved", "已保存"),
    WAIT_EXAMINE("waitExamine", "待审核"),
    AUDITED("audited", "已确认"),
    WAIT_EDIT("waitEdit", "待修改"),
    INVALID("invalid", "已作废")
    ;
    private String code;

    private String name;

    ExpAgreementStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ExpAgreementStatusEnum of(String code) {
        return Arrays.stream(ExpAgreementStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
