package com.tsfyun.common.base.enums;

import java.util.Arrays;

/**=
 * 登录角色类型
 */
public enum LoginRoleTypeEnum {

    MANAGER("manager", "管理端"),
    CLIENT("client", "客户端"),
    SALE("sale", "销售端"),
    ;
    private String code;
    private String name;

    LoginRoleTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static LoginRoleTypeEnum of(String code) {
        return Arrays.stream(LoginRoleTypeEnum.values()).filter(r -> r.code.equals(code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
