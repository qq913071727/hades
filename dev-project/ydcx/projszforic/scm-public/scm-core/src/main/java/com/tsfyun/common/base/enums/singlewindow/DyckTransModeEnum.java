package com.tsfyun.common.base.enums.singlewindow;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 单一窗口成交方式
 * @CreateDate: Created in 2021/11/12 16:33
 */
public enum DyckTransModeEnum {
    CIF("1", "cif","CIF"),
    CANDF("2", "","C&F"),
    FOB("3", "fob","FOB"),
    CANDI("4", "","C&I"),
    MARKET_PRICE("5", "","市场价"),
    CUSHION_BIN("6", "","垫仓"),
    EXW("7", "","垫仓"),
    ;

    private String code;
    private String scode;
    private String name;

    DyckTransModeEnum(String code,String scode, String name) {
        this.code = code;
        this.scode = scode;
        this.name = name;
    }

    public static DyckTransModeEnum of(String code) {
        return Arrays.stream(DyckTransModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public static DyckTransModeEnum ofScode(String scode) {
        return Arrays.stream(DyckTransModeEnum.values()).filter(r -> Objects.equals(r.getScode(), scode)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(String scode) {
        this.scode = scode;
    }
}
