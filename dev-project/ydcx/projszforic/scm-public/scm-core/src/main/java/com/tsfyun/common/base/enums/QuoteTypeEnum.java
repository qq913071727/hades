package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 报价类型 
 */
public enum QuoteTypeEnum {
    ADVANCE_CHARGE("advance_charge", "预付款"),
    PAD_TAX("pad_tax", "垫税款"),
    PAD_GOOD("pad_good", "垫货款")
    ;
    private String code;
    private String name;

    QuoteTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static QuoteTypeEnum of(String code) {
        return Arrays.stream(QuoteTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
