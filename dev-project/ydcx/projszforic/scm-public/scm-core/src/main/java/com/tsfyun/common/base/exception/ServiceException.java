package com.tsfyun.common.base.exception;

import com.tsfyun.common.base.enums.RespCodeEnum;
import com.tsfyun.common.base.enums.ResultCodeEnum;
import lombok.Data;

@Data
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String code;

    public ServiceException(ResultCodeEnum resultCode) {
        super(resultCode.getMsg());
        this.code = resultCode.getCode();
    }

    public ServiceException(RespCodeEnum resultCode) {
        super(resultCode.getMsg());
        this.code = resultCode.getCode();
    }

    public ServiceException(String code, String msg) {
        super(msg);
        this.code = code;
    }


    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(Throwable throwable) {
        super(throwable);
    }

    public ServiceException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
