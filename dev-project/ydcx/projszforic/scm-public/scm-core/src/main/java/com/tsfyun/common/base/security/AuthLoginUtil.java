package com.tsfyun.common.base.security;

import com.tsfyun.common.base.constant.UserAuthoritiesConstant;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class AuthLoginUtil {

    public static Collection<? extends GrantedAuthority> getAuthorities(List<SysRoleVO> sysRoles) {
        Collection<GrantedAuthority> collection = new HashSet<>();
        if (!CollectionUtils.isEmpty(sysRoles)) {
            sysRoles.parallelStream().forEach(role -> {
                collection.add(role.getId().startsWith(UserAuthoritiesConstant.ROLE_TYPE_PREFIX) ? new SimpleGrantedAuthority(role.getId()) :
                        new SimpleGrantedAuthority(UserAuthoritiesConstant.ROLE_TYPE_PREFIX + role.getId()));
            });
        }
        return collection;
    }

}
