package com.tsfyun.common.base.enums.logistics;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author   送货目地
 * @since Created in 2020/3/20 22：07
 */
public enum TransportDestinationEnum {

    WAREHOUSE("warehouse", "货运仓库"),
    DESTINATION("destination", "货运指定地点"),
    ;
    private String code;
    private String name;

    TransportDestinationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TransportDestinationEnum of(String code) {
        return Arrays.stream(TransportDestinationEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
