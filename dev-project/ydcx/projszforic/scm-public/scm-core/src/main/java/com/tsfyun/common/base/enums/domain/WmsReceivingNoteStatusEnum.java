package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

public enum WmsReceivingNoteStatusEnum implements OperationEnum {
    RECEIVED("received", "已入库")
    ;
    private String code;

    private String name;

    WmsReceivingNoteStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static WmsReceivingNoteStatusEnum of(String code) {
        return Arrays.stream(WmsReceivingNoteStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}