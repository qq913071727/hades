package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 收款方式
 */
public enum ReceivingModeEnum {
    CASH("cash", "银行转账"),
    BUSINESS_BILL("business_bill", "电子商票"),
    BANK_CHECK("bank_check", "银行支票"),
    ACCEPTANCE_BILL("acceptance_bill", "银行承兑汇票"),
    ;
    private String code;
    private String name;

    ReceivingModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ReceivingModeEnum of(String code) {
        return Arrays.stream(ReceivingModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
