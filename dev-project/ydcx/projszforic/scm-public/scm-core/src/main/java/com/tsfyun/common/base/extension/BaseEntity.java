package com.tsfyun.common.base.extension;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import com.tsfyun.common.base.extension.annotation.UpdateFill;
import org.apache.catalina.security.SecurityUtil;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

public class BaseEntity implements Serializable {

    //统一主键生成策略
    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 通用字段，后续可以利用Mybatisl拦截器统一处理
     *
     */

    /**
     * 创建人   创建人personId/创建人名称
     */
    @InsertFill
    private String createBy;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 修改人    修改人personId/修改人名称
     */
    @UpdateFill
    private String updateBy;

    /**
     * 修改时间
     */
    @UpdateFill
    private LocalDateTime dateUpdated;

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
