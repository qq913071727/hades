
package com.tsfyun.common.base.extension;

/**
 * 排序属性
 */
public class OrderItem {

    private String column;
    private boolean asc = true;

    public static OrderItem asc(String column) {
        return build(column, true);
    }

    public static OrderItem desc(String column) {
        return build(column, false);
    }

    private static OrderItem build(String column, boolean asc) {
        OrderItem item = new OrderItem();
        item.setColumn(column);
        item.setAsc(asc);
        return item;
    }

    public OrderItem() {
    }

    public String getColumn() {
        return this.column;
    }

    public boolean isAsc() {
        return this.asc;
    }

    public OrderItem setColumn(final String column) {
        this.column = column;
        return this;
    }

    public OrderItem setAsc(final boolean asc) {
        this.asc = asc;
        return this;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof OrderItem;
    }


    public String toString() {
        return "OrderItem(column=" + this.getColumn() + ", asc=" + this.isAsc() + ")";
    }
}
