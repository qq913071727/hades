package com.tsfyun.common.base.vo;

import java.io.Serializable;

/**
 * @Description: 下拉框包装
 * @Project:
 * @CreateDate: Created in 2019/12/25 18:43
 */
public class KeyValue<K, V> implements Serializable {
    private K key;
    private V value;

    public KeyValue() {

    }

    public KeyValue(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{" + key + ":" + value + "}";
    }

    public void setKey(K key) {
        this.key = key;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyValue<?, ?> keyValue = (KeyValue<?, ?>) o;

        if (getKey() != null ? !getKey().equals(keyValue.getKey()) : keyValue.getKey() != null) return false;
        return getValue() != null ? getValue().equals(keyValue.getValue()) : keyValue.getValue() == null;

    }

    @Override
    public int hashCode() {
        int result = getKey() != null ? getKey().hashCode() : 0;
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        return result;
    }
}
