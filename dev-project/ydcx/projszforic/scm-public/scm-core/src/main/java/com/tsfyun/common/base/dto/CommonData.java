package com.tsfyun.common.base.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * =
 * 常用数据
 */
public class CommonData {
    //请求头转发
    public static List<String> forwardHeaderNames = new ArrayList<String>() {
        {
            add("appid");
            add("token");
            add("user-agent");
            add("x-real-ip");

            add("device");
            add("app_trace_id");
        }
    };
}
