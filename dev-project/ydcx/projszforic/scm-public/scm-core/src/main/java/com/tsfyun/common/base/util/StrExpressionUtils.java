package com.tsfyun.common.base.util;

/**
 * @Description: <br> 结算平台 - 算费、试算公式中，特殊字符处理
 * @Project: <br>
 * @CreateDate: Created in 2019-07-31 16:24 <br>
 */
public class StrExpressionUtils {

    /**
     * 去除计算公式中的特殊字符
     *
     * @param billingExpression 计算公式字符串
     * @return
     */
    public static String replace(String billingExpression) {
        if (billingExpression != null) {
            billingExpression = billingExpression.toLowerCase()
                    .replace("（", "(")
                    .replace("）", ")")
                    .replace(" ", "");
        }
        return billingExpression;
    }
}
