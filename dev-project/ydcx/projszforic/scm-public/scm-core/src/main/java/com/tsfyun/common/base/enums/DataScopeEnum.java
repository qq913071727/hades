package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 数据权限类型
 * @since Created in 2020/3/10 22:25
 */
public enum DataScopeEnum {

    ALL("all", "所有客户权限"),
    SALE("sale", "该销售下的客户权限"),
    BUSINESS("business", "该商务下的客户权限"),
    ;
    private String code;
    private String name;

    DataScopeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static DataScopeEnum of(String code) {
        return Arrays.stream(DataScopeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
