package com.tsfyun.common.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 企查查查询公司信息响应实体
 * </p>
 *
 *
 * @since 2020-11-30
 */
@Data
@ApiModel(value="QichachaCompany响应对象", description="企查查查询公司信息响应实体")
public class QichachaCompanyVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "公司名称")
    private String companyName;

    @ApiModelProperty(value = "企业注册号")
    private String no;

    @ApiModelProperty(value = "法人")
    private String operName;

    @ApiModelProperty(value = "成立日期")
    private String startDate;

    @ApiModelProperty(value = "吊销日期")
    private String endDate;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "信用代码")
    private String creditCode;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "组织机构代码")
    private String orgNo;

    @ApiModelProperty(value = "经营范围")
    private String scope;

    @ApiModelProperty(value = "注册资本")
    private String registCapi;

    @ApiModelProperty(value = "公司图片")
    private String imageUrl;

    @ApiModelProperty(value = "实缴资本")
    private String recCap;

    @ApiModelProperty(value = "企业类型")
    private String econKind;

    @ApiModelProperty(value = "是否从企查查中查询的最新数据")
    private Boolean qcc;


}
