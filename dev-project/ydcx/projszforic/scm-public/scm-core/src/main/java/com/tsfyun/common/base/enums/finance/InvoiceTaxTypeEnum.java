package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 税单类型
 * @CreateDate: Created in 2020/5/15 16:43
 */
public enum InvoiceTaxTypeEnum {
    PROPRIETARY("proprietary", "增值税专票13%(自营)"),
    AGENT("agent", "增值税专票6%(代理)"),
    ;
    private String code;
    private String name;

    InvoiceTaxTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static InvoiceTaxTypeEnum of(String code) {
        return Arrays.stream(InvoiceTaxTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}