package com.tsfyun.common.base.extension;

public interface FilingFieldConstant {

    public static final String CREATE_BY = "createBy";

    public static final String UPDATE_BY = "updateBy";

    public static final String DATE_CREATED = "dateCreated";

    public static final String DATE_UPDATED = "dateUpdated";

}
