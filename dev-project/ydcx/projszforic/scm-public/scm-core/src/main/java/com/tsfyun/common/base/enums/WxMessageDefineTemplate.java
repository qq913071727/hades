package com.tsfyun.common.base.enums;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/23 10:53
 */
public enum WxMessageDefineTemplate {
    /**
     * 付汇审核退回修改
     */
    PAY_BACK("vU2PIV5r2v6fZ4iSIlfqAcI3i8lO29ONgASGp2d7lC4","付汇审核退回修改",Lists.newArrayList("first","keyword1","keyword2","remark")),

    /**
     * 付汇审核退回作废
     */
    PAY_INVALID("V_Ghw7D2dr3Z_8mSISq4dbpkm3xXUas7vyrnZhnG-uc","付汇审核退回作废",Lists.newArrayList("first","keyword1","keyword2","remark")),

    /**
     * 付汇完成（
     */
    PAY_COMPLETE("BTT9aCc_9Kd6Ps4jhMjRcHdvudFz4Uan3-VOc6nvSbU","付汇完成",Lists.newArrayList("first","keyword1","keyword2","keyword3","keyword4","keyword5","remark")),

    /**
     * 境外发车
     */
    WAYBILL_DEPARTURE("-JIHiWgppCHwh3bPfmd3fGyyRyse1MRYciRHrg2F83Q","香港发车",Lists.newArrayList("first","keyword1","keyword2","keyword3","keyword4","remark")),

    /**
     * 到达深圳仓
     */
    WAYBILL_ARRIVE("7HrpGB2sbybVZoHphLrel_OLVuKxNUDchPWf1tMVmBE","抵达深圳",Lists.newArrayList("first","keyword1","keyword2","keyword3","remark")),

    /**
     * 客户付款财务已确认
     */
    CUSTOMER_PAY("vNiulHFQnZCZj0bsDK_ZZeVAeHfYNnqW2ZubNjhUXy0","付款成功确认",Lists.newArrayList("first","keyword1","keyword2","keyword3","keyword4","remark")),

    /**
     * 作废
     * 作废标题
     * 作废内容
     * 作废原因
     */
   INVALID_NOTICE("V_Ghw7D2dr3Z_8mSISq4dbpkm3xXUas7vyrnZhnG-uc","付款作废",Lists.newArrayList("first","keyword1","keyword2","remark")),

    /**
     * 订单退回修改
     */
    ORDER_BACK("vU2PIV5r2v6fZ4iSIlfqAcI3i8lO29ONgASGp2d7lC4", "订单退回修改",Lists.newArrayList("first","keyword1","keyword2","remark")),

    /**
     * 客户审核通过（作废，通过和不通过改用一个通知模板）
     */
    //CUSTOMER_APROVED("g-RxBoJIZSEPoFaHyKIZZn5PlhrL4-KWpoL1YnFSwuE", "客户审核通过",Lists.newArrayList("first","keyword1","keyword2","remark"));

    /**
     * 审核结果通知
      */
    CUSTOMER_APROVED("KO-NJWXhANGdNZmo7qyDk7P0ytjEd1T69NXXuCXhlqs", "客户审核结果通知",Lists.newArrayList("first","keyword1","keyword2","keyword3","remark")),

    /**
     * 订单确认进口
     */
    ORDER_CONFIRM_IMPORT("h5eV09i16CHgXx-dnhkf67JqheKhnISBMXyNWaU82iI","订单确认进口",Lists.newArrayList("first","keyword1","keyword2","keyword3","remark")),

    /**
     * 帐号绑定成功通知
     * {{first.DATA}}
     * 绑定帐号：{{keyword1.DATA}}
     * 绑定时间：{{keyword2.DATA}}
     * 绑定说明：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    BIND_WX("5HS80AN-n2tBlTx390HcJW5uShOESmv07CQCEQu2x8o","绑定微信登录",Lists.newArrayList("first","keyword1","keyword2","keyword3","remark")),

    ;
    private String code;

    private String name;

    /**
     * 模板参数
     */
    private List<String> parakeys;

    WxMessageDefineTemplate(String code, String name,List<String> parakeys) {
        this.code = code;
        this.name = name;
        this.parakeys = parakeys;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getParakeys() {
        return parakeys;
    }

    public void setParakeys(List<String> parakeys) {
        this.parakeys = parakeys;
    }
}
