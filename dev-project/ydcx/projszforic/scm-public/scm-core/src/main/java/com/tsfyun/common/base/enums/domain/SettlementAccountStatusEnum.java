package com.tsfyun.common.base.enums.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2021/10/14 17:52
 */
public enum SettlementAccountStatusEnum {

    COMPLETE("complete","已结汇"),
    ;
    private String code;

    private String name;

    SettlementAccountStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static SettlementAccountStatusEnum of(String code) {
        return Arrays.stream(SettlementAccountStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
