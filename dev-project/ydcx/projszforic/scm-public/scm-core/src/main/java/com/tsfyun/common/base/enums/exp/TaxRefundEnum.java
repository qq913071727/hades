package com.tsfyun.common.base.enums.exp;

import java.util.Arrays;
import java.util.Objects;

/**
 * 出口退税方式
 */
public enum TaxRefundEnum {
    NO("no", "无需退税"),
    AUTONOMOUS("autonomous", "自主退税"),
    ENTRUST("entrust", "委托退税");
    private String code;
    private String name;

    TaxRefundEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TaxRefundEnum of(String code) {
        return Arrays.stream(TaxRefundEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
