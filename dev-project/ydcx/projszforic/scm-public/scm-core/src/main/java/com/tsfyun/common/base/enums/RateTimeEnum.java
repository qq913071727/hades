package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * 中行汇率时间查询点
 */
public enum RateTimeEnum {
    TIME1("09:30", "09:30"),
    TIME2("10:00", "10:00"),
    TIME3("10:30", "10:30"),
    ;
    private String code;
    private String name;

    RateTimeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static RateTimeEnum of(String code) {
        return Arrays.stream(RateTimeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
