package com.tsfyun.common.base.extension;

import java.io.Serializable;
import java.util.function.BiFunction;

public interface BiFn<T, U, R> extends BiFunction<T, U, R>, Serializable {

}
