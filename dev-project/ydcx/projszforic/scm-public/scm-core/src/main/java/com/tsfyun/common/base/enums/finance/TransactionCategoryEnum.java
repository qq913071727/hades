package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 交易类别（从客户层面）
 */
public enum TransactionCategoryEnum {
    INCOME("income","收入"), //增加余额
    OUTCOME("outcome","支出"), //扣减余额
    ;
    private String code;
    private String name;

    TransactionCategoryEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TransactionCategoryEnum of(String code) {
        return Arrays.stream(TransactionCategoryEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
