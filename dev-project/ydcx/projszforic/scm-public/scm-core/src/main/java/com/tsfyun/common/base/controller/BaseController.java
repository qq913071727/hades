package com.tsfyun.common.base.controller;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.ResultCodeEnum;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import com.tsfyun.common.base.util.StringUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

@Slf4j
@CrossOrigin
public class BaseController {

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        //构造方法中boolean 参数含义为如果是空白字符串,是否转换为null，即如果为true,那么 " " 会被转换为 null,否者为 ""
        StringTrimmerEditor propertyEditor = new StringTrimmerEditor(false);
        //为String类对象注册编辑器
        binder.registerCustomEditor(String.class, propertyEditor);

        //String类请求参数统一去除特殊符号，如回车符/水平制表符/换行
        binder.registerCustomEditor(String.class,new PropertyEditorSupport(){
            @Override
            public void setAsText(String text) throws java.lang.IllegalArgumentException {
                Optional.ofNullable(text).ifPresent(r->{
                    setValue(StringUtils.removeSpecialSymbol(text));
                });
            }
        });

        /**
         * 自定义日期转换器
         */
        binder.registerCustomEditor(LocalDate.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                if (StringUtils.isNotEmpty(text)) {
                    setValue(LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                }
            }
        });
        binder.registerCustomEditor(LocalDateTime.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                if (StringUtils.isNotEmpty(text)) {
                    //setValue(LocalDateTime.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                    setValue(LocalDateTimeUtils.convert2LocalDateTimeByStringWithLength(text));
                }
            }
        });
        binder.registerCustomEditor(LocalTime.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                if (StringUtils.isNotEmpty(text)) {
                    setValue(LocalTime.parse(text, DateTimeFormatter.ofPattern("HH:mm:ss")));
                }
            }
        });
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @SneakyThrows
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(LocalDateTimeUtils.convertByStringWithLength(text));
            }
        });
    }

    /**
     * 成功返回
     *
     * @param object
     * @return
     */
    public <T> Result<T> success(T object) {
        return Result.success(object);
    }


    /**
     * 成功返回
     *
     * @return
     */
    public Result<Void> success() {
        return Result.success();
    }

    /**
     * 成功返回
     *
     * @param code
     * @param msg
     * @return
     */
    public Result<Void> success(String code, String msg) {
        return Result.success(code, msg);
    }

    /**
     * 成功时候的调用
     */
    public static <T> Result<T> success(Integer count, T data) {
        return Result.success(count, data);
    }

    /**
     * 失败返回,自定义错误信息
     *
     * @return
     */
    public Result<Void> error(String code, String msg) {
        return Result.error(code, msg);
    }

    /**
     * 失败返回，默认系统内部错误
     *
     * @return
     */
    public Result<Void> fail() {
        return Result.error(ResultCodeEnum.SYSTEM_INNER_ERROR);
    }


}
