package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 汇总结果单状态
 */
public enum VehicleSummaryResultStatusEnum implements OperationEnum {
    RELEASE("800","放行"),
    CLEARANCE("899","结关"),
    INIT_GENERATE("901","初始生成"),
    QP_SUCCESS("902","报QP成功"),
    QP_FAIL("903","报QP失败"),
    CANCEL("904","作废"),
    ;
    private String code;

    private String name;

    VehicleSummaryResultStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static VehicleSummaryResultStatusEnum of(String code) {
        return Arrays.stream(VehicleSummaryResultStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

