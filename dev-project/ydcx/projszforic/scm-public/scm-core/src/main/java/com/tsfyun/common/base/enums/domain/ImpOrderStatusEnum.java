package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2020/4/1 10:41
 */
public enum ImpOrderStatusEnum implements OperationEnum {
    TEMP_SAVED("tempSaved", "临时保存"),
    WAIT_PRICE("waitPrice", "待审价"),
    WAIT_EXAMINE("waitExamine", "待审核"),
    WAIT_INSPECTION("waitInspection", "待验货"),
    WAIT_CONFIRM_IMP("waitConfirmImp", "待确认进口"),
    WAIT_FK_EXAMINE("waitFKExamine", "待风控审核"),
    WAIT_CREATE_DECLARE("waitCreateDeclare", "待生成报关单"),
    WAIT_DECLARE("waitDeclare", "待报关"),
    WAIT_CLEARANCE("waitClearance", "待通关"),
    CLEARED("cleared", "已通关"),
    WAIT_EDIT("waitEdit", "待修改"),
    ;
    private String code;

    private String name;

    ImpOrderStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ImpOrderStatusEnum of(String code) {
        return Arrays.stream(ImpOrderStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
