package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 排序类型
 * @since Created in 2020/2/22 15:46
 */
public enum SortTypeEnum {


    ASC("ASC", "升序"),
    DESC("DESC", "降序"),
    ;
    private String code;
    private String name;

    SortTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SortTypeEnum of(String code) {
        return Arrays.stream(SortTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
