package com.tsfyun.common.base.enums.core;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 设备类型
 * @CreateDate: Created in 2020/9/16 14:35
 */
public enum DeviceEnum {
    PC("pc", "PC端"),
    H5("h5", "H5"),
    MP_WEIXIN("mp_weixin", "微信小程序"),
    APP_ANDRIOD("app_android", "安卓APP"),
    APP_IOS("app_ios", "苹果APP"),
    ;
    private String code;
    private String name;

    DeviceEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static DeviceEnum of(String code) {
        return Arrays.stream(DeviceEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
