package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 税前税后
 */
public enum TaxTypeEnum {
    AFTER_TAX("after_tax", "税后"),
    PRE_TAX("pre_tax", "税前")
    ;
    private String code;
    private String name;

    TaxTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TaxTypeEnum of(String code) {
        return Arrays.stream(TaxTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
