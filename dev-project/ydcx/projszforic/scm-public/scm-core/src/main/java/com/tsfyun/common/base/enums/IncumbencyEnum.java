package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

public enum IncumbencyEnum {


    INCUMBENCY("incumbency", "在职"),
    LEAVE("leave", "离职"),
    ;
    private String code;
    private String name;

    IncumbencyEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static IncumbencyEnum of(String code) {
        return Arrays.stream(IncumbencyEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
