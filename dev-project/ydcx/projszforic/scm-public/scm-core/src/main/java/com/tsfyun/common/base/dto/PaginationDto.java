package com.tsfyun.common.base.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * @ClassName PaginationDto
 * @Description 分页
 * @Author ZM
 * @Date 2019/10/30 18:43
 */
@Setter
@Getter
public class PaginationDto implements Serializable {

    @ApiModelProperty(value = "当前页码数",example = "1")
    @Min(value = 1, message = "页数必须大于0")
    private Integer page = 1;//当前页数

    @ApiModelProperty(value = "每页显示记录数",example = "20")
    @Min(value = 1, message = "每页显示记录必须大于0")
    @Max(value = 100, message = "每页显示记录不能大于100")
    private Integer limit = 20;//每页显示
}
