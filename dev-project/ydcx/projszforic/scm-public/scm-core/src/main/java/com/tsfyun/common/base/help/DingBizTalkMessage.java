package com.tsfyun.common.base.help;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/4 11:12
 */
@Slf4j
public class DingBizTalkMessage {

    private DingTalkMessageBuilder builder;

    private String url;

    public DingBizTalkMessage(DingTalkMessageBuilder builder, String url) {
        this.builder = builder;
        this.url = url;
    }

    public void send() {
        try {
            String json = builder.build();
            String responseJson = HttpUtil.post(url, json);
            //此处需判断错误码等等
            JSONObject jsonObject = JSONObject.parseObject(responseJson);
            int errcode = jsonObject.getInteger("errcode");
            if(0 != errcode) {
                log.error("钉钉发送业务通知异常：" + jsonObject.toJSONString());
                throw new RuntimeException("钉钉发送业务通知失败");
            }
        } catch (Exception e) {
            log.error("钉钉业务通知请求异常",e);
        }
    }

}
