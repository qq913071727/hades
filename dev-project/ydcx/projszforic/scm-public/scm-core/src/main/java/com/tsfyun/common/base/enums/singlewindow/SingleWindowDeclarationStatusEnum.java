package com.tsfyun.common.base.enums.singlewindow;

import com.tsfyun.common.base.support.OperationEnum;
import com.tsfyun.common.base.util.StringUtils;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 报关单单一窗口导入状态类型
 * @since Created in 2020/2/25 15:46
 */
public enum SingleWindowDeclarationStatusEnum implements OperationEnum {

    IMPORTING("importing", "导入中"),
    IMPORT_SUCCESS("importSuccess", "导入成功"),
    IMPORT_FAIL("importFail", "导入失败"),
    ;
    private String code;
    private String name;

    SingleWindowDeclarationStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SingleWindowDeclarationStatusEnum of(String code) {
        if(StringUtils.isEmpty(code)){return null;}
        return Arrays.stream(SingleWindowDeclarationStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
