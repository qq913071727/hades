package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 报关单状态
 * @since Created in 2020/4/1 10:41
 */
public enum DeclarationStatusEnum implements OperationEnum {
    WAIT_CONFIRM("waitConfirm", "待确认"),
    WAIT_DECLARE("waitDeclare","待申报"),
    IN_DECLARE("inDeclare","申报中"),
    COMPLETE("complete","已完成"),
    ;
    private String code;

    private String name;

    DeclarationStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static DeclarationStatusEnum of(String code) {
        return Arrays.stream(DeclarationStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
