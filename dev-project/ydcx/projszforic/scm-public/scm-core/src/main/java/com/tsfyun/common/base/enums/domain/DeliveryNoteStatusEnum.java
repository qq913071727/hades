package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 出库单状态
 * @since Created in 2020/4/1 10:41
 */
public enum DeliveryNoteStatusEnum implements OperationEnum {
    OUTBOUND("outbound", "已出库");

    private String code;

    private String name;

    DeliveryNoteStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static DeliveryNoteStatusEnum of(String code) {
        return Arrays.stream(DeliveryNoteStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
