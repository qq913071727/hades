package com.tsfyun.common.base.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/4 9:27
 */
@ConfigurationProperties(prefix = "baidu")
@Data
public class BaiduProperties {

    private List<Map<String,String>> api;

}
