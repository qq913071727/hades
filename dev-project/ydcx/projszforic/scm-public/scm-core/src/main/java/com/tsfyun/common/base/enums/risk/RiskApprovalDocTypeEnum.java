package com.tsfyun.common.base.enums.risk;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @Project: 风控审核类型
 * @CreateDate: Created in 2020/5/8 11:54
 */
public enum RiskApprovalDocTypeEnum {
    IMP_ORDER_AMOUNT_INSUFFICIENT_MOMEY("orderAmountLess", "进口订单金额异常"),
    PAYMENT_ACCOUNT_INSUFFICIENT_MOMEY("paymentAmountLess", "付汇单金额异常"),
    PAYMENT_ACCOUNT_RATE_ADJUST("paymentRateAdjust", "付汇单汇率调整"),
    ;
    private String code;
    private String name;

    RiskApprovalDocTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static RiskApprovalDocTypeEnum of(String code) {
        return Arrays.stream(RiskApprovalDocTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}