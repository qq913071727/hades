package com.tsfyun.common.base.enums.logistics;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author  跨境运输单业务类型（最大长度为20位）
 * @since Created in 2020/3/20 22：07
 */
public enum WaybillBusinessTypeEnum {

    GENERAL_TRADE_IMPORT("import", "一般贸易进口"),
    GENERAL_TRADE_EXPORT("export", "一般贸易出口"),
    HK_LOCAL_DISTRIBUTION("local_distribution", "香港本地配送"),
    ;
    private String code;
    private String name;

    WaybillBusinessTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WaybillBusinessTypeEnum of(String code) {
        return Arrays.stream(WaybillBusinessTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
