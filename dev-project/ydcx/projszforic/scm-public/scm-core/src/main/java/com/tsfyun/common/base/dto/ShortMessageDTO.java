package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import lombok.Data;
import org.checkerframework.common.value.qual.MinLen;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class ShortMessageDTO implements Serializable {

    @NotEmptyTrim(message = "手机号不能为空")
    @Pattern(regexp = "^[1][0-9]{10}$",message = "手机号格式错误")
    private String phoneNo;

    //操作
    private MessageNodeEnum messageNodeEnum;
}
