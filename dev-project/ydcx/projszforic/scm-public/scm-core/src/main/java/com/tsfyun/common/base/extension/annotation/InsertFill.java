package com.tsfyun.common.base.extension.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据库字段新增时自动填充
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface InsertFill {

    /**
     * 字段名称
     *
     * @return
     */
    String value() default "";
}