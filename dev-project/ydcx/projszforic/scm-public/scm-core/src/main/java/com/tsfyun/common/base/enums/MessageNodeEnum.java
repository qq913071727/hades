package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 发送短信节点
 */
public enum MessageNodeEnum {
    COMMON_PROVE("common_prove", "通用认证"),
    REGISTER_LOGIN("register_login", "注册登录"),
    RETRIEVE_PASSWORD("retrieve_password", "找回密码"),
    UNBIND_PHONE("unbind_phone", "解绑手机"),
    BIND_PHONE("bind_phone", "绑定手机"),
    NEW_REGISTER("new_register","新用户注册"),
    HK_WAREHOUSING("hk_warehousing","香港到货通知"),
    CHANGE_PASSWORD("change_password", "修改密码"),
    PAY_BACK("pay_back","付汇审核退回修改"),
    PAY_INVALID("pay_invalid","付汇审核退回作废"),
    PAY_COMPLETE("pay_complete","付汇完成"),
    WAYBILL_DEPARTURE("waybill_departure","境外发车"),
    WAYBILL_ARRIVE("waybill_arrive","到达深圳仓"),
    CUSTOMER_PAY("customer_pay","客户付款财务已确认"),
    ORDER_BACK("order_back","订单退回修改"),
    CUSTOMER_APROVED("customer_aproved","客户审核通过"),
    CUSTOMER_REJECT("customer_reject","客户审核未通过"),
    ;
    private String code;
    private String name;

    MessageNodeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MessageNodeEnum of(String code) {
        return Arrays.stream(MessageNodeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
