package com.tsfyun.common.base.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/4 10:55
 */
@ConfigurationProperties(prefix = "remind")
@Data
public class DDRemindProperties {

    //钉钉通知配置信息
    private List<Map<String,String>> dingding;

}
