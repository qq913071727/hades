package com.tsfyun.common.base.enums.exp;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/29 10:25
 */
public enum  ExpTransactionModeEnum {

    FOB("fob", "FOB"),
    CIF("cif", "CIF"),
    C_AND_F("c&f", "C&F");
    private String code;
    private String name;

    ExpTransactionModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ExpTransactionModeEnum of(String code) {
        return Arrays.stream(ExpTransactionModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
