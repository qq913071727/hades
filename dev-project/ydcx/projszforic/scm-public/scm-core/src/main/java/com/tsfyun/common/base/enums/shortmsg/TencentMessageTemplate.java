package com.tsfyun.common.base.enums.shortmsg;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 腾讯云短信模板
 * @CreateDate: Created in 2021/9/15 14:10
 */
public enum TencentMessageTemplate implements OperationEnum {

    PROVE("1122851", "验证码{1}，您正在进行身份验证，打死不要告诉别人哦！"),
    LOGIN_CONFIRM("1122849","验证码{1}，您正在登陆，若非本人操作，请勿泄露。"),
    LOGIN_EXCEPTION("1122848","验证码{1}，您正尝试异地登陆，若非本人操作，请勿泄露。"),
    NEW_REGISTER("1122847","验证码{1}，您正在注册成为新用户，感谢您的支持！"),
    CHANGE_PASSWORD("1122845","验证码{1}，您正在尝试修改登录密码，请妥善保管账户信息。"),
    ;
    private String code;
    private String name;

    TencentMessageTemplate(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TencentMessageTemplate of(String code) {
        return Arrays.stream(TencentMessageTemplate.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
