package com.tsfyun.common.base.enums.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 单据类型，请务必与实体名称保持一致
 * @since Created in 2020/3/4 15:46
 */
public enum DomainTypeEnum {

    MATERIEL("Materiel", "进口物料"),
    MATERIEL_EXP("MaterielExp", "出口物料"),
    CUSTOMER("Customer", "客户"),
    IMPORDER("ImpOrder", "进口订单"),
    EXPORDER("ExpOrder", "出口订单"),
    PURCHASECONTRACT("PurchaseContract", "采购合同"),
    IMPORDERPRICE("ImpOrderPrice", "进口订单审价"),
    EXPORDERPRICE("ExpOrderPrice", "出口订单审价"),
    AGREEMENT("Agreement", "协议报价单"),
    RECEIPTACCOUNT("ReceiptAccount", "收款单"),
    CROSSBORDERWAYBILL("CrossBorderWaybill", "跨境运输单"),
    DELIVERYWAYBILL("DeliveryWaybill", "国内送货单"),
    PAYMENTACCOUNT("PaymentAccount", "进口付汇单"),
    EXPPAYMENTACCOUNT("ExpPaymentAccount", "出口付款单"),
    DRAWBACK("Drawback", "出口退税单"),
    RECEIVINGNOTE("ReceivingNote", "入库单"),
    DELIVERYNOTE("DeliveryNote", "出库单"),
    DECLARATION("Declaration", "报关单"),
    RISKAPPROVAL("RiskApproval", "风控审批"),
    INVOICE("Invoice", "发票"),
    IMPSALESCONTRACT("ImpSalesContract", "进口销售合同"),
    REFUNDACCOUNT("RefundAccount", "退款申请"),
    DECLQUOTE("DeclQuote", "代理报关报价单"),
    EXPAGREEMENT("ExpAgreement", "出口协议报价单"),
    OVERSEASRECEIVINGACCOUNT("OverseasReceivingAccount", "境外收款"),
    OVERSEASDELIVERYNOTE("OverseasDeliveryNote", "境外派送"),
    ;
    private String code;
    private String name;

    DomainTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static DomainTypeEnum of(String code) {
        return Arrays.stream(DomainTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
