package com.tsfyun.common.base.constant;

/**
 * @Description: 缓存常量
 * @since Created in 2020/3/11 18:59
 */
public interface CacheConstant {

    //模块名称
    String MODEL_NAME = "BASE:";
    String CONFIG_NAME = "CONFIG:";
    String DECLARE = "DECLARE:";
    String SCM = "SCM:";

    String VERSION = MODEL_NAME + "VERSION";

    //国家
    String COUNTRY_LIST = MODEL_NAME + "COUNTRY:LIST";

    String DUPLICATE_SUBMIT = "DULICATE_SUBMIT:";

    //行政区划
    String ADMINISTRATIVE_DIVISION_LIST = MODEL_NAME + "ADMINISTRATIVE_DIVISION:LIST";

    //国内地区
    String DOMESTIC_DISTRICT_LIST = MODEL_NAME + "DOMESTIC_DISTRICT:LIST";

    //国内港口
    String DOMESTIC_PORT_LIST = MODEL_NAME + "DOMESTIC_PORT:LIST";

    //港口
    String PORT_LIST = MODEL_NAME + "PORT:LIST";

    //所有的海关常量数据
    String CUSTOMS_DATA_LIST = MODEL_NAME + "CUSTOMS_DATA:LIST";

    //海关常量数据详情
    String CUSTOMS_DATA_DETAIL = MODEL_NAME + "CUSTOMS_DATA:DETAIL";

    //根据编码查询海关编码详情
    String CUSTOMS_DATA_CODE = MODEL_NAME + "CUSTOMS_DATA_CODE";

    //根据海关编码获取申报要素
    String CUSTOMS_ELEMENTS_HS_CODE = MODEL_NAME + "CUSTOMS_ELEMENTS_HS_CODE";

    String CUSTOMS_CIQ_LIST = MODEL_NAME + "CUSTOMS_CIQ:LIST";

    //所有可用的币制
    String CURRENCY_LIST = MODEL_NAME + "CURRENCY:LIST";

    //根据类型获取基础数据
    String BASE_DATA_LIST = MODEL_NAME + "BASE_DATA_LIST";

    //单位
    String UNIT_LIST = MODEL_NAME + "UNIT:LIST";

    //根据ID获取系统配置参数
    String SYSTEM_CONFIG_CODE = CONFIG_NAME + "SYSTEM_CONFIG_CODE";
    //所有系统参数集合
    String SYSTEM_CONFIG_MAP = CONFIG_NAME + "SYSTEM_CONFIG_MAP";

    String TAX_CODE_LIST = MODEL_NAME + "TAX_CODE:LIST";

    //所有省市区数据
    String ALL_DISTRICT = MODEL_NAME + "DISTRICT:LIST";

    //所有的最惠国税率大于0的海关编码
    String ALL_TARIFF_CUSTOMS_CODE = MODEL_NAME + "TARIFF_CUSTOMS_CODE";

    //#################跨境电商缓存KEY######################

    //所有的禁用产品
    String ALL_DISABLE_PRODUCT = DECLARE + "DISABLE_PRODUCT:LIST";

    //所有的收货人
    String ALL_RECEIVE_INFO = DECLARE + "RECEIVE_INFO:LIST";

    //清单编号前5位
    String BILL_NO_PREFIX = DECLARE + "BILL_NO:PRE";

    //清单编号后5位
    String BILL_NO_SUFFIX = DECLARE + "BILL_NO:SUF";

    //三单申报时间
    String THREE_ORDER_DECLARE = DECLARE + "THREE_ORDER_DECLARE:";

    //清单申报时间
    String INVENTORY_DECLARE = DECLARE + "INVENTORY_DECLARE:";

    //撤销清单申报时间
    String INVENTORY_CANCEL_DECLARE = DECLARE + "INVENTORY_CANCEL_DECLARE:";

    //总分单申报时间
    String WAYBILL_DECLARE = DECLARE + "WAYBILL_DECLARE:";

    //离境单申报时间
    String DEPARTURE_DECLARE = DECLARE + "DEPARTURE_DECLARE:";

    //深圳邻友通是否  另存为  操作
    String LYT_EXCEL_SAVE_AS = DECLARE + "LYT:";


    //##############供应链###############
    //根据编码查询海关编码详情
    String WAREHOUSE_DETAIL = SCM + "WAREHOUSE";

    //费用科目信息
    String EXPENSESUBJECT_LIST = SCM  + "EXPENSE_SUBJECT:LIST";

}
