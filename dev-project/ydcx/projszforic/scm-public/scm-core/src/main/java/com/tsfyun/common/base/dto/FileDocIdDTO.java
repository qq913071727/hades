package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * =
 * 调整单据ID
 */
@Data
public class FileDocIdDTO implements Serializable {
    //原单据ID
    private String docId;

    @NotEmpty(message = "单据类型不能为空")
    private String docType;

    @NotEmptyTrim(message = "业务类型不能为空")
    private String businessType;

    @NotEmpty(message = "新单据ID不能为空")
    private String ndocId;//新单据ID

    private String nmemo;//新单据类型描述

    private String ndocType;//新单据类型

}
