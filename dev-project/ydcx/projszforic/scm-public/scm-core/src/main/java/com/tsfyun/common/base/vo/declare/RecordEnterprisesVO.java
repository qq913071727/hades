package com.tsfyun.common.base.vo.declare;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value="RecordEnterprises响应对象", description="电商备案企业响应实体")
public class RecordEnterprisesVO implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    private Long customerId;

    @ApiModelProperty(value = "客户")
    private String customerName;

    @ApiModelProperty(value = "企业海关代码")
    private String code;

    @ApiModelProperty(value = "企业名称")
    private String name;

    @ApiModelProperty(value = "企业简称")
    private String shortName;

    private String dxpId;

    @ApiModelProperty(value = "禁用标示")
    private Boolean disabled;

    @ApiModelProperty(value = "备注")
    private String memo;


}