package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 微信公众号事件消息请求类型
 * @CreateDate: Created in 2020/12/29 14:56
 */
public enum WxRequestMessageEventEnum {

    TEXT("text", "文本"),
    IMAGE("image", "图片"),
    LINK("link", "链接"),
    LOCATION("location", "地理位置"),
    VOICE("voice", "音频"),
    SHORTVIDEO("shortvideo", "小视频"),
    EVENT("event", "推送"),
    ;
    private String code;
    private String name;

    WxRequestMessageEventEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WxRequestMessageEventEnum of(String code) {
        return Arrays.stream(WxRequestMessageEventEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
