package com.tsfyun.common.base.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CostWriteOffDTO implements Serializable {

    private String tenant;//租户(取消SAAS中使用)
    private Long customerId;//需要核销的客户ID
    private String billType;//需要核销的单据类型，空则表示核销所有单据


}
