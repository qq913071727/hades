package com.tsfyun.common.base.dto;

import java.io.Serializable;

/**
 * 枚举通用信息类
 */
public class EnumInfo implements Serializable {

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnumInfo(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public EnumInfo() {

    }

}
