package com.tsfyun.common.base.security;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/27 15:17
 */
@Data
public class TokenVO implements Serializable {

    private String token;

    private Long personId;

    /**
     * 登录时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime loginDate;

    /**
     * 登录地点
     */
    private String locate;

    /**
     * 角色类型
     */
    private String roleType;
}
