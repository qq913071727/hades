package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * =
 * 邮件内容模板类型（跟类路径模板下名称一一对应）
 */
public enum MailTemplateTypeEnum {

    REGISTER_CODE("register_code", "注册验证码"),
    PURCHASE_CONTRACT("purchase_contract", "采购合同"),
    ;
    private String code;
    private String name;

    MailTemplateTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MailTemplateTypeEnum of(String code) {
        return Arrays.stream(MailTemplateTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
