package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 单据类型
 */
public enum BillTypeEnum {
    IMP("imp", "进口"),
    EXP("exp", "出口");
    private String code;
    private String name;

    BillTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static BillTypeEnum of(String code) {
        return Arrays.stream(BillTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
