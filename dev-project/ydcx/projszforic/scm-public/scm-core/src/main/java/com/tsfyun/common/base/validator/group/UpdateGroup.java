/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.tsfyun.common.base.validator.group;

import javax.validation.groups.Default;

/**
 * 更新数据分组校验Group
 *
 *
 */
public interface UpdateGroup extends Default {

}
