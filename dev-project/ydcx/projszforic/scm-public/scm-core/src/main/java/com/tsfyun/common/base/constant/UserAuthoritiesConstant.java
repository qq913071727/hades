package com.tsfyun.common.base.constant;

public interface UserAuthoritiesConstant {

    /**
     * 人员权限前缀
     */
    String PERSON_TYPE_PREFIX = "type_";

    /**
     * 角色权限前缀
     */
    String ROLE_TYPE_PREFIX = "ROLE_";

}
