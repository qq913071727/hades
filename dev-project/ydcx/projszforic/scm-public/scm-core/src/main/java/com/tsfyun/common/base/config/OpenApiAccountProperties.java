package com.tsfyun.common.base.config;

import com.tsfyun.common.base.security.OpenApiAccount;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/29 17:26
 */
@Component
@ConfigurationProperties(prefix = OpenApiAccountProperties.PREFIX)
@Data
public class OpenApiAccountProperties {

    public final static String PREFIX = "openapi";

    //单位秒
    private long accessTokenExpireTime;

    private List<OpenApiAccount> list;


}
