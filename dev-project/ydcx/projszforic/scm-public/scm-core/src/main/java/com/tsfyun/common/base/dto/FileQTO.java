package com.tsfyun.common.base.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * =
 * 文件查询
 */
@Data
public class FileQTO implements Serializable {

    @NotBlank(message = "单据ID不能为空")
    private String docId;

    @NotBlank(message = "单据类型不能为空")
    private String docType;

    private String businessType;

    List<String> docIds;

    public void setDocId(String docId){
        this.docId = docId;
        this.docIds = Arrays.asList(docId.split(","));
    }
}
