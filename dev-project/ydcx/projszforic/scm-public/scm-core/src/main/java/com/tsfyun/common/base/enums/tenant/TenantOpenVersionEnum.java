package com.tsfyun.common.base.enums.tenant;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 租户使用平台版本
 * @Project:
 * @CreateDate: Created in 2020/5/11 13:21
 */
public enum TenantOpenVersionEnum {
    FREE("free", "体验版"),
    TEAM("team", "团队版"),
    ENTERPRISE("enterprise", "企业版"),
    //TODO 待后续完善租户付费套餐信息，因为套餐信息的配置比较多，需要配置在数据库表中
    ;
    private String code;
    private String name;

    TenantOpenVersionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TenantOpenVersionEnum of(String code) {
        return Arrays.stream(TenantOpenVersionEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
