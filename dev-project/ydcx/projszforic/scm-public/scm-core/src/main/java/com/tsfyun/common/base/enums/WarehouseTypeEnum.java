package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * =
 * 仓库类型
 */
public enum WarehouseTypeEnum {

    OVERSEAS("overseas", "境外仓库"),
    DOMESTIC("domestic", "境内仓库");
    private String code;
    private String name;

    WarehouseTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WarehouseTypeEnum of(String code) {
        return Arrays.stream(WarehouseTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
