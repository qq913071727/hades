package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 付款方式
 */
public enum PaymentTermEnum {
    TT("TT", "TT")
    ;
    private String code;
    private String name;

    PaymentTermEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static PaymentTermEnum of(String code) {
        return Arrays.stream(PaymentTermEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}