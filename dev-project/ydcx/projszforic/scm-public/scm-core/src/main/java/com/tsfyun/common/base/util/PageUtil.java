package com.tsfyun.common.base.util;

import com.github.pagehelper.PageHelper;
import com.tsfyun.common.base.dto.PaginationDto;

import java.util.List;
import java.util.function.Supplier;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/18 10:10
 */
public class PageUtil {

    /**
     * 分页查询
     * @param paginationDto
     * @param supplier
     * @return
     */
    public static <T> List<T> pageQ(PaginationDto paginationDto, Supplier<List<T>> supplier) {
        PageHelper.startPage(paginationDto.getPage(),paginationDto.getLimit());
        return supplier.get();
    }

}
