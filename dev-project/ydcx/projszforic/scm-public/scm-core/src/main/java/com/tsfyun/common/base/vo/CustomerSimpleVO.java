package com.tsfyun.common.base.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 客户简要信息
 */
@Data
@NoArgsConstructor
public class CustomerSimpleVO implements Serializable {

    private Long id;
    /**
     * 客户代码
     */
    private String code;
    /**
     * 客户名称
     */
    private String name;
    /**
     * 统一社会信用代码(18位)
     */
    private String socialNo;
}
