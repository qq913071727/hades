package com.tsfyun.common.base.enums;

/**
 * @Description:
 * @Project:
 * @CreateDate: Created in 2019-10-30 09:12
 */

public interface RespCodeEnum {

    String getCode();

    String getKey();

    String getMsg();

    void setArgs(Object[] var1);
}
