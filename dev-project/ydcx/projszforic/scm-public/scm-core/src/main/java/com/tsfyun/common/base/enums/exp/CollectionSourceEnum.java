package com.tsfyun.common.base.enums.exp;

import java.util.Arrays;
import java.util.Objects;

/**
 * 收款来源
 */
public enum CollectionSourceEnum {

    DEDUCTION("deduction", "退税款中扣除"),
    PAYMENT("payment", "客户单独支付"),
    OTHER("other", "其他")
    ;
    private String code;
    private String name;

    CollectionSourceEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CollectionSourceEnum of(String code) {
        return Arrays.stream(CollectionSourceEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public static CollectionSourceEnum ofName(String name) {
        return Arrays.stream(CollectionSourceEnum.values()).filter(r -> Objects.equals(r.getName(), name)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

