package com.tsfyun.common.base.extension;

import cn.hutool.core.lang.Snowflake;
import tk.mybatis.mapper.genid.GenId;

/**
 * @Description: 雪花生成主键（默认生成策略）
 * @CreateDate: Created in 2021/3/8 16:33
 */
public class IdWorker implements GenId<Long> {

    private Snowflake snowflake = new Snowflake(1, 1);

    @Override
    public Long genId(String table, String column) {
        return snowflake.nextId();
    }
}
