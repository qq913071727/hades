package com.tsfyun.common.base.enums.exp;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 代理费结算模式
 * @CreateDate: Created in 2021/9/10 14:10
 */
public enum ExpSettlementModeEnum {
    DECLARE_TOTAL_PERCENT("dec_percent","按报关总价百分比"),
    ONE_DOLLAR("one_dollar","每1美金收取人民币"),
    ;

    private String code;
    private String name;

    ExpSettlementModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ExpSettlementModeEnum of(String code) {
        return Arrays.stream(ExpSettlementModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
