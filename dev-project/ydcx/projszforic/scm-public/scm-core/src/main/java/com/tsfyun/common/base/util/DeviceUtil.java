package com.tsfyun.common.base.util;

import com.tsfyun.common.base.constant.BaseContextConstant;
import com.tsfyun.common.base.enums.core.DeviceEnum;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/23 09:24
 */
public class DeviceUtil {

    /**
     * 获取客户端终端平台类型（默认PC）
     * @return
     */
    public static DeviceEnum getPlatform() {
        DeviceEnum deviceEnum = DeviceEnum.PC;
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if(Objects.nonNull(request)) {
            deviceEnum = Optional.ofNullable(DeviceEnum.of(request.getHeader(BaseContextConstant.DEVICE))).orElse(DeviceEnum.PC);
        }
        return deviceEnum;
    }

}
