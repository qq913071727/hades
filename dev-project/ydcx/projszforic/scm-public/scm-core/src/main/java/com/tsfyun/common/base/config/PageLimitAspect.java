package com.tsfyun.common.base.config;

import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @Description: 分页记录数限制，防止抓取数据
 * @CreateDate: Created in 2020/11/2 17:05
 * @Author: <a href="852675742@qq.com">chunlin.zhang</a>
 */
@Aspect
@Order(-10)
@Component
@Slf4j
public class PageLimitAspect {

    private final String point = "execution(* com.tsfyun.*.controller..*.*(..))&& args(com.tsfyun.common.base.dto.PaginationDto)";

    @Around(point)
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs();
        if(args != null && args.length > 0) {
            Object requestParam = args[0];
            if(requestParam instanceof PaginationDto) {
                PaginationDto requestPageDto = (PaginationDto)requestParam;
                if(Objects.nonNull(requestPageDto) && requestPageDto.getLimit() > 100) {
                    throw new ServiceException("每页查询记录不能大于100");
                }
            }
        }
        Object result = pjp.proceed();
        return result;
    }


}
