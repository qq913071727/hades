package com.tsfyun.common.base.enums;

import java.util.Arrays;

/**
 * @Description: 文件类型
 * @since Created in 2020/2/22 15:46
 */
public enum FileTypeEnum {


    IMAGE("image", "图片"),
    FILE("file", "文件"),
    ;
    private String code;
    private String name;

    FileTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static FileTypeEnum of(String code) {
        return Arrays.stream(FileTypeEnum.values()).filter(r -> r.code.equals(code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
