package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2020/4/1 10:41
 */
public enum CrossBorderWaybillStatusEnum implements OperationEnum {

    WAIT_CONFIRM("waitConfirm", "待确定"),
    WAIT_BIND("waitBind", "待绑单"),
    WAIT_REVIEW("waitReview", "待复核"),
    WAIT_DEPART("waitDepart", "待发车"),
    WAIT_SIGN("waitSign", "待签收"),
    SIGNED("signed", "已签收"),
    ;
    private String code;

    private String name;

    CrossBorderWaybillStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static CrossBorderWaybillStatusEnum of(String code) {
        return Arrays.stream(CrossBorderWaybillStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
