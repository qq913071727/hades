package com.tsfyun.common.base.constant;

public interface DataScopeConstant {

    //所有客户权限角色编码
    String ALL_CUSTOMER = "all_customers";

    //关联客户表的join SQL片段
    String INNER_CUSTOMER_SQL = " inner join customer %s on %s.id = %s.customer_id ";

    //客户id条件
    String CUSTOMER_ID_SQL = " %s.customer_id = '%s' ";

    //存放INNER JOIN的SQL片段
    String SQL_INNER = "SQL_INNER";
    //存放查询SQL的过滤片段
    String SQL_FILTER = "SQL_FILTER";

}
