package com.tsfyun.common.base.util;

import com.tsfyun.common.base.exception.ClientException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * =
 * 数据验证
 */
public class VerificationUtils {

    /**
     * =
     * 手机号验证
     *
     * @param str
     * @return
     */
    public static boolean isMobile(final String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        Pattern p = Pattern.compile("^[1][0-9]{10}$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * =
     * 验证登录账号
     *
     * @param str
     * @return
     */
    public static boolean isLoginName(final String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        if (str.length() < 5 || str.length() > 12) {
            return false;
        }
        Pattern p = Pattern.compile("^[A-Za-z][A-Za-z0-9_-]+$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * =
     * 邮箱验证
     *
     * @param str
     * @return
     */
    public static boolean isEmail(final String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        Pattern p = Pattern.compile("^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * =
     * 验证值是否为空，空则抛出异常
     *
     * @param val
     * @param msg
     */
    public static void VerificationEmpty(Object val, String msg) {
        if (StringUtils.isEmpty(val)) {
            throw new ClientException(msg);
        }
    }

    /**
     * =
     * 验证集合是否为空，空则抛出异常
     *
     * @param vals
     * @param msgs
     */
    public static void VerificationEmpty(List vals, List<String> msgs) {
        IntStream.range(0, vals.size()).forEach(idx -> {
            VerificationEmpty(vals.get(idx), msgs.get(idx));
        });
    }
}
