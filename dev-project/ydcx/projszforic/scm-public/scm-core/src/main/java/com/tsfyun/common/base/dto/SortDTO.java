package com.tsfyun.common.base.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * =
 * 字段排序
 */
@Data
public class SortDTO implements Serializable {

    private String field;//排序字段
    private String mode;//排序方式

    public SortDTO() {
    }

    public SortDTO(String field, String mode) {
        this.field = field;
        this.mode = mode;
    }
}
