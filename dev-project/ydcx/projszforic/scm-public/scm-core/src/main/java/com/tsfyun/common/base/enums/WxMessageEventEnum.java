package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 微信公众号事件消息类型
 * @CreateDate: Created in 2020/12/29 14:56
 */
public enum WxMessageEventEnum {

    SUBSCRIBE("subscribe", "订阅"),
    UNSUBSCRIBE("unsubscribe", "取消订阅"),
    LOCATION("LOCATION","上报地理位置"),
    SCAN("SCAN", "扫描二维码"),
    CLICK("CLICK", "自定义菜单点击事件"),
    VIEW("VIEW", "自定义菜单跳转事件"),
    ;
    private String code;
    private String name;

    WxMessageEventEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WxMessageEventEnum of(String code) {
        return Arrays.stream(WxMessageEventEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
