package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 清单状态
 */
public enum DetailedListStatusEnum implements OperationEnum {
    TEMPORARY("temporary", "暂存"),
    EXCEPTION("-1","处理异常"),
    TEMP("1","已暂存"),
    DECLARING("2","申报中"),
    SEND_CUSTOMS_SUCCESS("3","发送海关成功"),
    SEND_CUSTOMS_FAIL("4","发送海关失败"),
    BACK("100","海关退单"),
    STORAGE("120","海关入库"),
    MANUAL_AUDIT("300","人工审核"),
    AUDITED("399","审核通过"),//对应海关审结
    UNBIND("401","与总单解绑"),
    CHECK("500","查验"),
    SELECTIVE_CHECK("504","选查"),
    TRANSFER("505","移交"),
    SUSPEND("600","挂起"),
    RELEASE("800","放行"),
    CLEARANCE("899","结关"),
    CANCEL("cancel","已作废"),
    ;
    private String code;

    private String name;

    DetailedListStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static DetailedListStatusEnum of(String code) {
        return Arrays.stream(DetailedListStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

