package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 发票税点
 * @CreateDate: Created in 2020/5/15 16:43
 */
public enum InvoicePointEnum {
    SIX("0.06", "代理开票税率6%"),
    THIRTEEN("0.13", "自营开票税率13%"),
    ;
    private String code;
    private String name;

    InvoicePointEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static InvoicePointEnum of(String code) {
        return Arrays.stream(InvoicePointEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}