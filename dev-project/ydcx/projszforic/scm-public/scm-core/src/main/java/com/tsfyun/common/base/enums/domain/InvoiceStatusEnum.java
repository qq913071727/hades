package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 发票状态
 * @CreateDate: Created in 2020/5/15 16:39
 */
public enum InvoiceStatusEnum implements OperationEnum {
    WAIT_CONFIRM("waitConfirm","商务开票确认"),//商务待确认开票
    WAIT_FINANCE_TAX("waitFinanceTax","待财务确定编码"),
    WAIT_CUSTOMER_TAX("waitCustomerTax","待客户确定编码"),
    WAIT_FINANCE_CONFIRM("waitFinanceConfirm","待财务确认开票"),
    INVOICED("invoiced","已开票"),
    ;
    private String code;

    private String name;

    InvoiceStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static InvoiceStatusEnum of(String code) {
        return Arrays.stream(InvoiceStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
