package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 退款申请状态
 * @CreateDate: Created in 2020/11/27 11:22
 */
public enum RefundAccountStatusEnum implements OperationEnum {
    WAIT_EXAMINE("waitExamine", "待审核"),
    CONFIRM_BANK("confirmBank", "确定付款行"),
    WAIT_PAY("waitPay", "待付款"),
    COMPLETED("completed", "已完成"),
    NOT_APPROVE("notApprove", "作废"),
    ;
    private String code;

    private String name;

    RefundAccountStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static RefundAccountStatusEnum of(String code) {
        return Arrays.stream(RefundAccountStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
