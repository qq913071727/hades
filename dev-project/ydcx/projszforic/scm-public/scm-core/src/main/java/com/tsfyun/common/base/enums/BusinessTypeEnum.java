package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 业务类型（长度20位以内）
 */
public enum BusinessTypeEnum {
    IMP_AGENT("imp_agent", "代理进口"),
    IMP_COOPERATE("imp_cooperate", "自营进口")
    ;
    private String code;
    private String name;

    BusinessTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static BusinessTypeEnum of(String code) {
        return Arrays.stream(BusinessTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
