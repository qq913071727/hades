package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 物料品牌类型枚举
 * @since Created in 2020/3/25 16:38
 */
public enum MaterielBrandTypeEnum {
    NO_BRAND("0","无品牌"),
    DOMESTIC_AUTOMY("1","境内自主品牌"),
    DOMESTIC_BUY("2","境内收购品牌"),
    OVERSEAS_OEM("3","境外品牌（贴牌生产）"),
    OVERSEAS_OTHER("4","境外品牌（其他）"),
    ;
    private String code;
    private String name;

    MaterielBrandTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MaterielBrandTypeEnum of(String code) {
        return Arrays.stream(MaterielBrandTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
