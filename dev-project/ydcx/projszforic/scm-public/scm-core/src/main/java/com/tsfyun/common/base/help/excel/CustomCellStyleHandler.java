package com.tsfyun.common.base.help.excel;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.style.AbstractCellStyleStrategy;
import org.apache.poi.ss.usermodel.*;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/31 13:51
 */
public class CustomCellStyleHandler extends AbstractCellStyleStrategy implements CellWriteHandler {

    private Workbook workbook;

    //标题样式
    private CellStyle titleStyle = null;

    @Override
    protected void initCellStyle(Workbook workbook) {
        this.workbook = workbook;
        Font font = this.workbook.createFont();
        //设置字体大小
        font.setFontHeightInPoints((short) 12);
        //设置粗体
        font.setBold(true);
        titleStyle = this.workbook.createCellStyle();
        titleStyle.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        titleStyle.setFont(font);
    }

    /**
     * 设置标题样式
     * @param cell
     * @param head
     * @param integer
     */
    @Override
    protected void setHeadCellStyle(Cell cell, Head head, Integer integer) {
        cell.setCellStyle(titleStyle);
    }

    @Override
    protected void setContentCellStyle(Cell cell, Head head, Integer integer) {

    }
}
