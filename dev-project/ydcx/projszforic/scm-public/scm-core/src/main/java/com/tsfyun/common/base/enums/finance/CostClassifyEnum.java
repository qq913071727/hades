package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 订单应收费用分类
 */
public enum CostClassifyEnum {
    DZF("dzf", "代杂费"),
    SK("sk", "税款"),
    HK("hk", "货款"),
    ;
    private String code;
    private String name;

    CostClassifyEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CostClassifyEnum of(String code) {
        return Arrays.stream(CostClassifyEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}