package com.tsfyun.common.base.enums.domain;

import java.util.Arrays;
import java.util.Objects;

public enum DeliveryWaybillStatusEnum {
    INITIAL("initial", "待定"),
    WAIT_CONFIRM("waitConfirm", "待确认"),
    WAIT_DEPART("waitDepart", "待发车"),
    WAIT_SIGN("waitSign", "待签收"),
    SIGNED("signed", "已签收");

    private String code;

    private String name;

    DeliveryWaybillStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static DeliveryWaybillStatusEnum of(String code) {
        return Arrays.stream(DeliveryWaybillStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}