package com.tsfyun.common.base.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据权限过滤注解，只能使用自定义SQL
 *
 *  at 2020-03-10 22:30:00
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {
    //业务表别名，如客户银行customer_bank对应的SQL里面的别名为t1,建议都使用别名，直接与客户表关联的表，有些是间接关联
    String tableAlias() default "";

    //客户表表别名，如客户表customer对应的SQL里面的别名为t2,建议都使用别名
    String customerTableAlias() default "";

    //是否外部表关联客户表，如客户银行customer_bank关联客户表customer，如果为true，则tableAlias和customerTableAlias都不允许为空;
    //如果为false如查询客户表本身时，则无需指定tableAlias和customerTableAlias
    boolean relateCustomer() default true;

    //是否增加客户名称左右模糊查询，参数固定为customerName，否则配置了true可能会报错，要么就配置为false，自行添加查询SQL
    //如果需要返回客户名称，注意select返回字段定义别名等等
    boolean addCustomerNameQuery() default false;
}
