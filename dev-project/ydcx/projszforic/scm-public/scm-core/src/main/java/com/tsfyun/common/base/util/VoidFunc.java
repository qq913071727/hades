package com.tsfyun.common.base.util;

/**
 * @Description:
 * @CreateDate: Created in 2021/11/18 17:11
 */
@FunctionalInterface
public interface VoidFunc {

    void call() throws  Exception;

    default void callWithRuntimeException() {
        try {
            call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
