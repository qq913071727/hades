package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 数字比较条件
 * @since Created in 2020/3/10 22:25
 */
public enum NumberConditionEnum {

    GT("gt", ">","大于"),
    GE("ge", ">=","大于等于"),
    EQ("eq", "=","等于"),
    LT("lt", "<","小于"),
    LE("le", "<=","小于等于"),
    ;
    private String code;
    private String symbol;
    private String name;

    NumberConditionEnum(String code, String symbol, String name) {
        this.code = code;
        this.symbol = symbol;
        this.name = name;
    }

    public static NumberConditionEnum of(String code) {
        return Arrays.stream(NumberConditionEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
