package com.tsfyun.common.base.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 文件数量响应实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileCntVO implements Serializable {

    private String docType;

    private String docId;

    private int count;

}
