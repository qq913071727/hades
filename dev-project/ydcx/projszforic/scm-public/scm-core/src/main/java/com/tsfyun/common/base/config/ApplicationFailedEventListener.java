package com.tsfyun.common.base.config;

import com.tsfyun.common.base.config.properties.NoticeProperties;
import com.tsfyun.common.base.help.DingTalkNoticeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @Description: 服务启动失败监听器
 * @since Created in 2020/5/23 16:47
 */
@Component
public class ApplicationFailedEventListener implements ApplicationListener<ApplicationFailedEvent> {

    @Value("${spring.application.name:saas}")
    private String applicationName;

    @Autowired
    private NoticeProperties noticeProperties;

    @Value("${server.port}")
    private Integer serverPort;

    private static Logger logger = LoggerFactory.getLogger(ApplicationFailedEventListener.class);

    @Override
    public void onApplicationEvent(ApplicationFailedEvent event) {
        logger.info("【{}】系统启动失败，请检查并修复",applicationName);
        DingTalkNoticeUtil.send2DingDingOtherException(String.format("【%s】系统启动失败",applicationName), String.format("服务实例端口【%s】系统【%s】启动失败，请检查并修正", serverPort,applicationName), event.getException(), noticeProperties.getDingdingUrl());
    }
}
