package com.tsfyun.common.base.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * @Description:
 * @since Created in 2020/4/1 15:56
 */
@Slf4j
public class CustomLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    public static final CustomLocalDateTimeDeserializer instance = new CustomLocalDateTimeDeserializer();

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String val = jsonParser.getText();
        try {
            return LocalDateTimeUtils.convert2LocalDateTimeByStringWithLength(val);
        } catch (Exception e) {
            log.error(String.format("json字符串：%s参数转日期异常",val),e);
            throw new ServiceException("日期格式错误");
        }
    }

}
