package com.tsfyun.common.base.constant;


import com.tsfyun.common.base.dto.EnumInfo;

public interface IBaseEnum {

    EnumInfo getEnumInfo();

    default String getCode() {
        return getEnumInfo().getCode();
    }

    default String getName() {
        return getEnumInfo().getName();
    }

}