package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 香港快递类型
 */
public enum InExpressTypeEnum {
    LOCAL("local", "本地快递"),
    ABROAD("abroad", "境外快递")
    ;
    private String code;
    private String name;

    InExpressTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static InExpressTypeEnum of(String code) {
        return Arrays.stream(InExpressTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
