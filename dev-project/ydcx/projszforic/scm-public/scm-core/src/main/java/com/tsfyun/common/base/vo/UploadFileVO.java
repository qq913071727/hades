package com.tsfyun.common.base.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *
 *
 * @since 2020-03-04
 */
@Data
@ApiModel(value = "UploadFile响应对象", description = "响应实体")
public class UploadFileVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty(value = "单据ID")
    private String docId;

    @ApiModelProperty(value = "单据类型")
    private String docType;

    @ApiModelProperty(value = "单据类型")
    private String docTypeDesc;

    @ApiModelProperty(value = "业务类型名称")
    private String businessType;

    @ApiModelProperty(value = "单据类型")
    private String businessTypeDesc;

    @ApiModelProperty(value = "说明描述")
    private String memo;

    @ApiModelProperty(value = "实际文件名称")
    private String name;

    //@JsonIgnore
    @ApiModelProperty(value = "系统文件名称")
    private String nname;

    //@JsonIgnore
    @ApiModelProperty(value = "文件存放路径")
    private String path;

    @JsonIgnore
    @ApiModelProperty(value = "上传人id")
    private String uid;

    @ApiModelProperty(value = "上传人名称")
    private String uname;

    @ApiModelProperty(value = "文件后缀")
    private String extension;

    @JsonIgnore
    @ApiModelProperty(value = "删除标志")
    private Boolean isDelete;

    @JsonIgnore
    @ApiModelProperty(value = "删除时间")
    private LocalDateTime dateDel;

    @JsonIgnore
    @ApiModelProperty(value = "创建人id/名称")
    private String createBy;

    @JsonIgnore
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime dateCreated;


}
