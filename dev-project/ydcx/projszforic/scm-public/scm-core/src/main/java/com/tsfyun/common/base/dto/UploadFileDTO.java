package com.tsfyun.common.base.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-04
 */
@Data
@ApiModel(value = "UploadFile请求对象", description = "请求实体")
public class UploadFileDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "文件不能为空")
    private MultipartFile file;

    @NotBlank(message = "文件类型不能为空")
    @Pattern(regexp = "image|file", message = "文件类型只能位image或者file")
    private String fileType;

//    @ApiModelProperty(value = "租户名称")
//    private String tenantName;
//
//    @NotBlank(message = "租户编码不能为空")
//    @ApiModelProperty(value = "租户编码")
//    private String tenantCode;

    @NotBlank(message = "单据id不能为空")
    @Length(max = 37, message = "单据id长度不能超过37位")
    @ApiModelProperty(value = "单据ID")
    private String docId;

    @NotBlank(message = "单据类型不能为空")
    @Length(max = 30, message = "单据类型长度不能超过30位")
    @ApiModelProperty(value = "单据类型")
    private String docType;

    @NotBlank(message = "单据业务类型不能为空")
    @Length(max = 30, message = "单据业务类型长度不能超过30位")
    @ApiModelProperty(value = "单据业务类型")
    private String businessType;

    @Length(max = 200, message = "说明描述不能超过200")
    @ApiModelProperty(value = "说明描述")
    private String memo;

    @Length(max = 100, message = "实际文件名称不能超过200")
    @ApiModelProperty(value = "实际文件名称")
    private String name;

    @ApiModelProperty(value = "系统文件名称")
    private String nname;

    @ApiModelProperty(value = "文件存放路径")
    private String path;

    @NotBlank(message = "上传人id不能为空")
    @ApiModelProperty(value = "上传人id")
    private String uid;

    @NotBlank(message = "上传人名称不能为空")
    @Length(max = 64, message = "上传人名称不能超过64位")
    @ApiModelProperty(value = "上传人名称")
    private String uname;

    @Length(max = 10, message = "文件后缀不能超过64位")
    @ApiModelProperty(value = "文件后缀")
    private String extension;

    @ApiModelProperty(value = "删除标志")
    private Boolean isDelete;

    @ApiModelProperty(value = "删除时间")
    private LocalDateTime dateDel;

    @ApiModelProperty(value = "仅仅允许存在一个类型相同的文件")
    private Boolean isOnlyOne;

}
