package com.tsfyun.common.base.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatusTotalVO implements Serializable {

    private String statusId;
    private Integer total;
    private String statusDesc;

    private Boolean isStatistics = Boolean.TRUE;// 是否参与全部统计
}
