package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 成交方式
 */
public enum TransactionModeEnum {
    CIF("cif", "CIF"),
    FOB("fob", "FOB")
    ;
    private String code;
    private String name;

    TransactionModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TransactionModeEnum of(String code) {
        return Arrays.stream(TransactionModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
