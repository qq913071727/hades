package com.tsfyun.common.base.annotation;


import com.tsfyun.common.base.validator.EnumCheckValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * 是否存在枚举类校验，空则不校验
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Documented
@Constraint(validatedBy = {EnumCheckValidator.class})
public @interface EnumCheck {

    String message() default "参数格式错误";

    Class<?> clazz();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}