package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 舱单单证类型
 * @CreateDate: Created in 2020/7/15 15:59
 */
public enum ManifestTypeEnum {

    MT1401("MT1401","进口原始舱单"),
    MT2401("MT2401","出口预配舱单"),

    MT3402("MT3402","出口运抵报告"),
    MT4401("MT4401","进口载货运输工具承运确报"),
    MT4402("MT4402","出口载货运输工具（装载舱单）承运确报"),
    MT4403("MT4403","进口空载运输工具承运确报"),
    MT4404("MT4404","出口空载运输工具（装载舱单）承运确报"),
    MT4405("MT4405","进口空集装箱运输工具承运确报"),
    MT4406("MT4406","出口空集装箱运输工具（装载舱单）承运确报"),
    MT5401("MT5401","进口理货报告"),
    MT5402("MT5402","出口理货报告"),
    MT7402("MT7402","出口装箱清单"),
    MT8401("MT8401","进口落装申请"),
    MT8402("MT8402","出口落装申请"),
    MT8403("MT8403","进口落装改配申请"),
    MT8404("MT8404","出口落装改配申请"),
    ;

    private String code;
    private String name;

    ManifestTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ManifestTypeEnum of(String code) {
        return Arrays.stream(ManifestTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
