package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 客户状态类型
 * @since Created in 2020/2/25 15:46
 */
public enum CustomerStatusEnum implements OperationEnum {

    TYPE_0("waitPerfect", "待完善资料"),
    TYPE_1("waitApprove", "待审核"),
    TYPE_2("audited", "已审核"),
    ;
    private String code;
    private String name;

    CustomerStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CustomerStatusEnum of(String code) {
        return Arrays.stream(CustomerStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
