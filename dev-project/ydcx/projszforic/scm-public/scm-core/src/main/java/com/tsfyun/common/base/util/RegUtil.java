package com.tsfyun.common.base.util;

import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/22 10:17
 */
public class RegUtil {

    //短信模板匹配
    static final String MESSAGE_REG = "\\{(.*?)}";

    public static LinkedHashSet<String> getMessageParams(String content){
        LinkedHashSet<String> params = new LinkedHashSet<>();
        Pattern pattern = Pattern.compile(MESSAGE_REG);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            params.add(matcher.group(1));
        }
        return params;
    }

}
