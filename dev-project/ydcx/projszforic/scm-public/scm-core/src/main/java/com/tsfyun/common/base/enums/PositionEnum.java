package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 职位类型
 * @since Created in 2020/2/22 15:46
 */
public enum PositionEnum {

    BUSINESS("business", "商务"),
    SALE("sale", "销售"),
    OTHER("other", "其他")
    ;
    private String code;
    private String name;

    PositionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static PositionEnum of(String code) {
        return Arrays.stream(PositionEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    /**
     * 根据描述获取枚举
     *
     * @param name
     * @return
     */
    public static PositionEnum ofName(String name) {
        return Arrays.stream(PositionEnum.values()).filter(r -> Objects.equals(r.getName(), name)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
