package com.tsfyun.common.base.enums.core;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 系统版块
 * @CreateDate: Created in 2021/9/24 09:24
 */
public enum SectionModuleEnum {

    IMP("imp", "进口"),
    EXP("exp", "出口"),
    ;
    private String code;
    private String name;

    SectionModuleEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SectionModuleEnum of(String code) {
        return Arrays.stream(SectionModuleEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
