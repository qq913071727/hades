package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * =
 * 日期规则
 */
public enum TimeRuleEnum {

    NO("no", "无"),
    yy("yy", "两位年(22)"),
    yyMMdd("yyMMdd", "两位年月日(200101)"),
    yyyyMMdd("yyyyMMdd", "四位年月日(20200101)"),
    MMdd("MMdd", "月日(0101)");
    private String code;
    private String name;

    TimeRuleEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TimeRuleEnum of(String code) {
        return Arrays.stream(TimeRuleEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
