package com.tsfyun.common.base.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-04
 */
@Data
@ApiModel(value = "ExportFile请求对象", description = "请求实体")
public class ExportFileDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @ApiModelProperty(value = "下载一次则删除")
    private Boolean once;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "文件存放路径")
    private String path;

    @ApiModelProperty(value = "下载次数")
    private Integer times;

    @ApiModelProperty(value = "创建人id/创建人名称")
    private String createBy;

}
