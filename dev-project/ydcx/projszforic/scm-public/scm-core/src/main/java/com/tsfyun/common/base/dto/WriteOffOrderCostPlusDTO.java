package com.tsfyun.common.base.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WriteOffOrderCostPlusDTO implements Serializable {

    private Long customerId;
    private List<WriteOffOrderCostDTO> orderCosts;
}
