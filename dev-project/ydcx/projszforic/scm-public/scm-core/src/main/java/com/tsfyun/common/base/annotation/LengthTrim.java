package com.tsfyun.common.base.annotation;

import com.tsfyun.common.base.validator.LengthTrimValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * 长度校验，去前后空格，值不为空则校验，集合如果是null则不校验
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Documented
@Constraint(validatedBy = {LengthTrimValidator.class})
public @interface LengthTrim {

    int min() default 0;

    int max() default 2147483647;

    String message() default "参数长度不正确";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
