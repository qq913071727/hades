package com.tsfyun.common.base.config;

import cn.hutool.core.lang.Snowflake;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 雪花算法id生成器配置
 * @since Created in 2019/11/8 17:38
 */
@Configuration
public class IdWorkerConfig {

    @Bean
    public Snowflake snowflakeIdWorker() {
        return new Snowflake(1, 1);
    }

}
