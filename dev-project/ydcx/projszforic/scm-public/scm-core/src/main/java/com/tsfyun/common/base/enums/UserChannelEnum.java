package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 登录用户渠道
 * @CreateDate: Created in 2020/12/22 17:59
 */
public enum UserChannelEnum {
    MANAGER("manager", "管理端"),
    CLIENT("client", "客户端"),
    SALE("sale", "销售端"),
    ;
    private String code;
    private String name;

    UserChannelEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static UserChannelEnum of(String code) {
        return Arrays.stream(UserChannelEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
