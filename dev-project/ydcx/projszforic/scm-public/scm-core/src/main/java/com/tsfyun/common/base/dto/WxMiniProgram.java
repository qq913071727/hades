package com.tsfyun.common.base.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description: 小程序参数
 * @CreateDate: Created in 2020/12/23 10:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxMiniProgram implements Serializable {

    /**
     * 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏）
     */
    private String appid;

    /**
     * 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），要求该小程序已发布，暂不支持小游戏
     */
    private String pagepath;

    public WxMiniProgram(String pagepath) {
        this.pagepath = pagepath;
    }


}
