package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 发票类型
 * @CreateDate: Created in 2020/5/15 16:43
 */
public enum InvoiceTypeEnum {
    VAT("vat", "增值税专用发票"),
    ;
    private String code;
    private String name;

    InvoiceTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static InvoiceTypeEnum of(String code) {
        return Arrays.stream(InvoiceTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}