package com.tsfyun.common.base.util;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.CssInfoVO;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//海关数据
@Slf4j
public class CssInfoUtil {

    public static CssInfoVO search(Map<String,String> params){
        try{
            String response = HttpRequest.post("https://manage.xxxx.com/declare/cssInfo/search").addHeaders(new HashMap<String, String>(){{
                put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36");
                put("accept-salt","xxxx");
                put("Content-Type","application/json");
            }}).body(JSON.toJSONString(params)).execute().body();
            Result result = JSONObject.parseObject(response, Result.class);
            if(result.isSuccess()&& Objects.nonNull(result.getData())){
               return JSONObject.parseObject(JSONObject.toJSONString(result.getData()),CssInfoVO.class);
            }
        }catch (Exception e){
            log.error("数据调用失败：",e);
        }
       return null;
    }
}
