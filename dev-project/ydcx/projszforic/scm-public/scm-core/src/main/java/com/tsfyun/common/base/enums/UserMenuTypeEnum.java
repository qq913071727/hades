package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 菜单使用账户类型
 * @since Created in 2020/2/22 15:46
 */
public enum UserMenuTypeEnum {


    BACKGROUND_MENU(1, "后台用户菜单"),
    FRONT_MENU(2, "客户端用户菜单"),
    ;
    private Integer code;
    private String name;

    UserMenuTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static UserMenuTypeEnum of(Integer code) {
        return Arrays.stream(UserMenuTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
