package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.domain.DomainOprationEnum;
import com.tsfyun.common.base.enums.domain.DomainTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class TaskDTO implements Serializable {

    @NotNull(message = "单据ID不能为空")
    Serializable documentId;//单据id
    @NotEmptyTrim(message = "单据类型不能为空")
    @EnumCheck(clazz = DomainTypeEnum.class,message = "单据类型错误")
    String documentClass;//单据类型 (枚举类DomainTypeEnum中定义)
    @NotEmptyTrim(message = "修改状态不能为空")
    String newStatusCode;//修改后状态
    @LengthTrim(max = 500,message = "意见长度不能大于500位")
    String memo;//备注
    @NotEmptyTrim(message = "操作码不能为空")
    @EnumCheck(clazz = DomainOprationEnum.class,message = "错误的操作")
    String operation;
    //操作人
    String operator;
}
