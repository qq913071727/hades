package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 出口订单状态
 * @CreateDate: Created in 2021/1/26 17:38
 */
public enum ExpOrderStatusEnum implements OperationEnum {
    TEMP_SAVED("tempSaved", "临时保存"),
    WAIT_PRICE("waitPrice", "待审价"),
    WAIT_EXAMINE("waitExamine", "待审核"),
    WAIT_INSPECTION("waitInspection", "待验货"),
    WAIT_CONFIRM_EXP("waitConfirmExp", "待确认出口"),
    WAIT_DECLARE("waitDeclare", "待报关"),
    WAIT_CLEARANCE("waitClearance", "待通关"),
    CLEARED("cleared", "已通关"),
    WAIT_EDIT("waitEdit", "待修改"),
    SIGNED("signed", "已签收"),
    ;
    private String code;

    private String name;

    ExpOrderStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ExpOrderStatusEnum of(String code) {
        return Arrays.stream(ExpOrderStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
