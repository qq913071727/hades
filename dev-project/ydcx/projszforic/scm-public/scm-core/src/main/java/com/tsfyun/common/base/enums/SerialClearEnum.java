package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * =
 * 流水号重置方式
 */
public enum SerialClearEnum {

    NO("no", "不重置"),
    DAY("day", "按天重置"),
    MONTH("month", "按月重置"),
    YEAR("year", "按年重置");
    private String code;
    private String name;

    SerialClearEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SerialClearEnum of(String code) {
        return Arrays.stream(SerialClearEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
