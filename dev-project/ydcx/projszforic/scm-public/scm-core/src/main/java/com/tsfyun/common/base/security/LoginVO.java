package com.tsfyun.common.base.security;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class LoginVO implements Serializable {

    /**
     * 登录id
     */
    @ApiModelProperty(value = "登录id")
    private Long loginId;

    /**
     * 登录帐号
     */
    @ApiModelProperty(value = "登录帐号")
    private String loginName;

    /**
     * 登录密码
     */
    @JsonIgnore
    @JSONField(serialize = false)
    private String loginPassWord;

    /**
     * 员工id
     */
    @ApiModelProperty(value = "人员id")
    private Long personId;

    /**
     * 员工名称，如昵称
     */
    @ApiModelProperty(value = "昵称")
    private String personName;

    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String userHead;

    /**
     * 客户ID
     */
    @ApiModelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    /**
     * 登录时间
     */
    @ApiModelProperty(value = "登录时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime loginDate;

    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phone;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 登录类型
     */
    @ApiModelProperty(value = "登录类型")
    private Integer loginType;

    /**
     * 职位
     */
    @ApiModelProperty(value = "职位")
    private String position;

    /**
     * 在职状态
     */
    @ApiModelProperty(value = "在职状态")
    private Boolean incumbency;

    /**
     * 当前token
     */
    private String token;
    /**
     * 登录角色类型
     */
    @ApiModelProperty(value = "登录角色类型")
    private String roleType;

    /**
     * 登录地址
     */
    @ApiModelProperty(value = "登录地址")
    private String locate;

    /**
     * 系统角色
     */
    private List<SysRoleVO> sysRoles;

}
