package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 整车申报单据进度（暂存）
 */
public enum VehicleDeclareSubStatus1Enum implements OperationEnum {
    CANCEL("cancel", "已作废"),
    ;
    private String code;

    private String name;

    VehicleDeclareSubStatus1Enum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static VehicleDeclareSubStatus1Enum of(String code) {
        return Arrays.stream(VehicleDeclareSubStatus1Enum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
