package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 物料状态
 * @since Created in 2020/2/25 15:46
 */
public enum MaterielStatusEnum implements OperationEnum {

    WAIT_MATCH("waitMatch", "待匹配"),
    WAIT_CLASSIFY("waitClassify", "待归类"),
    CLASSED("classed", "已归类");
    ;
    private String code;
    private String name;

    MaterielStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static MaterielStatusEnum of(String code) {
        return Arrays.stream(MaterielStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
