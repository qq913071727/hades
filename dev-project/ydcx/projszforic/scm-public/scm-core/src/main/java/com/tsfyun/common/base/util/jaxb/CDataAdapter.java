package com.tsfyun.common.base.util.jaxb;

import org.springframework.util.StringUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @Description: XmlAdapter类, 修饰类字段, 达到自动添加CDATA标记的目标
 * @since Created in 2020/4/21 10:21
 */
public class CDataAdapter extends XmlAdapter<String, String> {

    @Override
    public String unmarshal(String v) throws Exception {
        return v;
    }

    @Override
    public String marshal(String v) throws Exception {
        if(StringUtils.isEmpty(v)){
            return "";
        }
        return v.trim();
        //return v.trim().replaceAll("&","&amp;").replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll("'","&quot;").replaceAll("\"","&apos;");
    }
}
