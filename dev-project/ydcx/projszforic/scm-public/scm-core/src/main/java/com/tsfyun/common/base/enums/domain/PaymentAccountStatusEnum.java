package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2020/4/24 10:35
 */
public enum  PaymentAccountStatusEnum implements OperationEnum {
    WAIT_SW_EXAMINE("waitSWExamine", "待商务审核"),
    WAIT_FK_EXAMINE("waitFKExamine", "待风控审核"),
    CONFIRM_BANK("confirmBank", "出纳确定付款行"),
    WAIT_PAY("waitPay", "待付汇"),
    COMPLETED("completed", "已完成"),
    WAITT_EDIT("waitEdit", "待修改"),
    TO_VOID("toVoid", "已作废"),
    ;


    private String code;

    private String name;

    PaymentAccountStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static PaymentAccountStatusEnum of(String code) {
        return Arrays.stream(PaymentAccountStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }
}
