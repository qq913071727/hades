package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 结算汇率类型
 */
public enum RateTypeEnum {
    PAY_BANK_CHINA_SELL("pay_bank_china_sell", "付汇日牌价卖出价"),
    IMP_BANK_CHINA_SELL("imp_bank_china_sell", "报关日牌价卖出价"),
    IMP_CUSTOMS("imp_customs", "报关日海关汇率"),
    ;
    private String code;
    private String name;

    RateTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static RateTypeEnum of(String code) {
        return Arrays.stream(RateTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
