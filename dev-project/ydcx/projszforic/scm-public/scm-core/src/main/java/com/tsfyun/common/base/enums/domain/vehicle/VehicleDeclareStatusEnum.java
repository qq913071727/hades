package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 整车申报单据状态
 */
public enum VehicleDeclareStatusEnum implements OperationEnum {
    TEMPORARY("temporary", "暂存"),
    THREE_DECLARE("three_declare", "三单"),
    INVENTORY("inventory", "清单"),
    WAYBILL("waybill", "总分单"),
    DEPARTURE("departure", "离境单"),
    ;
    private String code;

    private String name;

    VehicleDeclareStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static VehicleDeclareStatusEnum of(String code) {
        return Arrays.stream(VehicleDeclareStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
