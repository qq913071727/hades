package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 收款单状态
 * @since Created in 2020/4/1 10:41
 */
public enum ReceiptAccountStatusEnum implements OperationEnum {
    WAIT_CONFIRM("waitConfirm", "待确定"),
    CONFIRMED("confirmed", "已确定"),
    INVALID("invalid", "已作废"),
    ;
    private String code;

    private String name;

    ReceiptAccountStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ReceiptAccountStatusEnum of(String code) {
        return Arrays.stream(ReceiptAccountStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
