package com.tsfyun.common.base.enums.customs;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 运输方式
 */
public enum TrafModeCodeEnum {
    TM_0("0", "非保税区"),
    TM_1("1", "监管仓库"),
    TM_2("2", "水路运输"),
    TM_3("3", "铁路运输"),
    TM_4("4", "公路运输"),
    TM_5("5", "航空运输"),
    TM_6("6", "邮件运输"),
    TM_7("7", "保税区"),
    TM_8("8", "保税仓库"),
    TM_9("9", "其他运输"),
    TM_A("A", "全部运输"),
    TM_H("H", "边境页数海关作业区"),
    TM_W("W", "物流中心"),
    TM_X("X", "物流园区"),
    TM_Y("Y", "保税港区"),
    TM_Z("Z", "出口加工区"),
    ;
    private String code;
    private String name;

    TrafModeCodeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TrafModeCodeEnum of(String code) {
        return Arrays.stream(TrafModeCodeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
