package com.tsfyun.common.base.util;

import cn.hutool.core.io.IoUtil;
import com.google.common.collect.Lists;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/24 18:14
 */
public class IOUtil extends IoUtil {

    /**
     * 复制新流
     * @param input
     * @param copySize
     * @return
     * @throws IOException
     */
    public static List<InputStream> copys(InputStream input, Integer copySize) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = input.read(buffer)) > -1 ) {
            baos.write(buffer, 0, len);
        }
        baos.flush();
        List<InputStream> inputStreams = Lists.newArrayListWithExpectedSize(copySize);
        for(int i = 0;i < copySize;i++) {
            InputStream copyInput = new ByteArrayInputStream(baos.toByteArray());
            inputStreams.add(copyInput);
        }
        return inputStreams;
    }

}
