package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 菜单类型
 * @since Created in 2020/2/22 15:46
 */
public enum MenuTypeEnum {


    CATALOG(1, "目录"),
    NAVIGATION(2, "导航菜单"),
    BUTTON(3, "按钮菜单"),
    ;
    private Integer code;
    private String name;

    MenuTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MenuTypeEnum of(Integer code) {
        return Arrays.stream(MenuTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
