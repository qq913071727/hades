package com.tsfyun.common.base.constant;

import com.tsfyun.common.base.enums.core.DeviceEnum;

/**
 * @Description:
 * @CreateDate: Created in 2020/9/16 14:29
 */
public interface BaseContextConstant {


    /**
     * 设备来源类型
     */
    String DEVICE = "device";

    /**
     * 日志跟踪id名。
     */
    String LOG_TRACE_ID = "traceId";

    String LOG_B3_TRACEID = "X-B3-TraceId";

    /**
     * 请求头跟踪id名。
     */
    String HTTP_HEADER_TRACE_ID = "app_trace_id";

}
