package com.tsfyun.common.base.support;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.tsfyun.common.base.util.StringUtils;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @Description: BigDecimal序列化自定义处理
 * @CreateDate: Created in 2021/5/17 16:24
 */
public class BigDecimalFastJsonSerializer implements ObjectSerializer {


    @Override
    public void write(JSONSerializer jsonSerializer, Object object, Object fieldName, Type type, int i) throws IOException {
        SerializeWriter writer = jsonSerializer.getWriter();
        if(object == null) {
            writer.writeNull();
            return;
        }
        writer.write(StringUtils.null2EmptyWithTrim(object));
    }
}
