package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 微信公众号事件消息返回类型
 * @CreateDate: Created in 2020/12/29 14:56
 */
public enum WxResponseMessageEventEnum {

    TEXT("text", "文本"),
    IMAGE("image", "图片"),
    VOICE("voice", "语音"),
    VIDEO("video", "视频"),
    NUSIC("music", "音乐"),
    NEWS("news", "图文(跳转到外链)"),
    MPNEWS("mpnews", "图文(跳转到图文消息页面)"),
    WXCARD("wxcard", "卡券"),
    MINIPROGRAM("miniprogrampage","小程序卡片"),
    ;
    private String code;
    private String name;

    WxResponseMessageEventEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WxResponseMessageEventEnum of(String code) {
        return Arrays.stream(WxResponseMessageEventEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
