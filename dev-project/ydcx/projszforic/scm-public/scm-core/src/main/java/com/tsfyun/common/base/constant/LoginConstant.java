package com.tsfyun.common.base.constant;

/**
 * 登录常量
 */
public interface LoginConstant {

    String TOKEN = "token:";

    String AUTH = "auth:";

    String TEMP_LOGIN = "tempLogin:";

    String COMPANY = "company:";

    String ERROR_PWD = "errorpwd:";

    String PWD_CHANGE = "pwd:";

    String LOGIN_LIST = "login:list:";

    String UNBIND_PHONE = "unbindphone:";

    String LOGIN_VERIFY = "verify:";

    String LOGIN_NEED_CHECK = "login:check:";

    String WXGZH_USER_INFO = "wx:userinfo:";

    //登录用户绑定微信/关注公众号缓存key
    String BIND_WEIXIN = "bindWeixin:";

    String BIND_WEIXIN_PREFIX = "BINDWF";

    //登录用户绑定微信登录缓存key
    String BIND_WEIXIN_LOGIN = "bindWeixinLogin:";

    String BIND_WEIXIN_LOGIN_PREFIX = "BINDWL";

    //服务端存储的accessToken
    String OPENAPI_ACCESS_TOKEN = "openApiAccessToken:";
    //客户端存储的accessToken
    String CLIENT_CUSTOMER_ACCESS_TOKEN = "clientCustomerAccessToken";



}
