package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

public enum DeclQuoteStatusEnum implements OperationEnum {
    SAVED("saved", "已保存"),
    WAIT_EXAMINE("waitExamine", "待审核"),
    WAIT_CONFIRM("waitConfirm", "待确认"),
    AUDITED("audited", "已确认"),
    WAIT_EDIT("waitEdit", "待修改"),
    INVALID("invalid", "已作废")
    ;
    private String code;

    private String name;

    DeclQuoteStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static DeclQuoteStatusEnum of(String code) {
        return Arrays.stream(DeclQuoteStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
