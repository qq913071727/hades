package com.tsfyun.common.base.enums.singlewindow;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 单一窗口日志类型
 */
public enum SingleWindowLogEnum {
    DECLARE("declare", "报关单"),
    MANIFEST("mainfest", "舱单"),
    ;
    private String code;
    private String name;

    SingleWindowLogEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SingleWindowLogEnum of(String code) {
        return Arrays.stream(SingleWindowLogEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
