package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.enums.finance.TransactionCategoryEnum;
import com.tsfyun.common.base.enums.finance.TransactionTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 交易流水请求实体
 * </p>
 *

 * @since 2020-04-16
 */
@Data
@ApiModel(value="TransactionFlow请求对象", description="交易流水请求实体")
public class TransactionFlowDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotNull(message = "客户id不能为空")
   @ApiModelProperty(value = "客户id")
   private Long customerId;

   @NotEmptyTrim(message = "交易类型不能为空")
   @LengthTrim(max = 20,message = "交易类型最大长度不能超过20位")
   @EnumCheck(clazz = TransactionTypeEnum.class,message = "交易类型错误")
   @ApiModelProperty(value = "交易类型")
   private String transactionType;

   @NotEmptyTrim(message = "交易类别不能为空")
   @LengthTrim(max = 10,message = "交易类别最大长度不能超过10位")
   @EnumCheck(clazz = TransactionCategoryEnum.class,message = "交易类别错误")
   @ApiModelProperty(value = "交易类别")
   private String transactionCategory;

   @NotEmptyTrim(message = "交易单号不能为空")
   @LengthTrim(max = 20,message = "交易单号最大长度不能超过32位")
   private String transactionNo;

   @NotNull(message = "交易金额不能为空")
   @Digits(integer = 17, fraction = 2, message = "交易金额整数位不能超过17位，小数位不能超过2位")
   @ApiModelProperty(value = "交易金额")
   private BigDecimal amount;

   @ApiModelProperty(value = "备注")
   private String memo;

}
