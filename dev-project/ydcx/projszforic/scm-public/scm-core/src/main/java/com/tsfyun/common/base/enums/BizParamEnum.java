package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 业务参数名配置
 * @since Created in 2020/2/22 15:46
 */
public enum BizParamEnum {
    IMPCIF("imp_cif","进口CIF成交方式运保费率%"),
    IMPFOB("imp_fob","进口FOB成交方式运保杂费");
    private String code;
    private String name;

    BizParamEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static BizParamEnum of(String code) {
        return Arrays.stream(BizParamEnum.values()).filter(r -> Objects.equals(code,r.getCode())).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
