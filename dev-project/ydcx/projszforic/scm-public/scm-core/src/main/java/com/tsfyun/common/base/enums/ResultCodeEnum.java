package com.tsfyun.common.base.enums;

/**
 * @description: 公共状态码枚举类，其他各自项目的错误码可以放到各自的项目中去
 * @author: rick
 * @date: 2019-04-30 15:16
 */
public enum ResultCodeEnum {


    /**
     * 按每三位一组区分，前三位为模块，中间三位为具体某个功能点，后三位为序号
     * 前三位为模块
     */

    /**
     * 成功状态码
     */
    SUCCESS("1", "请求成功"),

    /**
     * 失败状态码
     */
    FAIL("0", "失败"),

    METHOD_NOT_PERMMIT("403", "方法不被允许"),

    NOT_PERMMISSION("405", "您无此访问权限"),

    PARAMS_IS_ERROR("415", "请求参数错误"),

    /**
     * 参数错误
     */
    PARAMS_IS_NULL("777001000", "参数为空错误"),
    PARAMS_NOT_COMPLETE("777001010", "参数不全"),
    PARAMS_TYPE_ERROR("777001020", "参数类型匹配错误"),
    PARAMS_IS_INVALID("777001030", "参数无效"),


    /**
     * 数据错误
     */
    DATA_NOT_FOUND("777002000", "数据未找到"),
    DATA_IS_WRONG("777002010", "数据有误"),
    DATA_ALREADY_EXISTED("777002020", "数据已存在"),

    /**
     * 用户错误
     */
    USER_NOT_EXIST("777003000", "用户不存在"),
    USER_NOT_LOGGED_IN("777003010", "用户未登陆"),
    USER_ACCOUNT_ERROR("777003020", "用户名或密码错误"),
    USER_ACCOUNT_FORBIDDEN("777003030", "用户账户已被禁用"),
    USER_HAS_EXIST("777003040", "该用户已存在"),
    USER_CODE_ERROR("777003050", "验证码错误"),
    PASSWORD_OVER_TIMES_ERROR("777003051", "密码输入次数超限"),
    PASSWORD_NEED_VERIFY_ERROR("777003052", "请先进行验证"),
    PASSWORD_VERIFY_ERROR("777003053", "验证失败，请重新验证"),
    PASSWORD_MANY_ERROR_VERIFY_ERROR("777003054", "登录密码输入错误"),

    /**
     * 系统错误
     */
    SYSTEM_INNER_ERROR("777004000", " 系统内部错误"),
    SERVICE_TRANSFER_ERROR("777004010", "跨服务调用错误"),
    SERVICE_HYSTRIX_ERROR("777004020", "服务不可用"),


    /**
     * 接口错误
     */
    INTERFACE_INNER_INVOKE_ERROR("777005000", "系统内部接口调用异常"),
    INTERFACE_OUTER_INVOKE_ERROR("777005010", "系统外部接口调用异常"),
    INTERFACE_FORBIDDEN("777005020", "接口禁止访问"),
    INTERFACE_ADDRESS_INVALID("777005030", "接口地址无效"),
    INTERFACE_REQUEST_TIMEOUT("777005040", "接口请求超时"),
    INTERFACE_EXCEED_LOAD("777005050", "接口负载过高"),
    INTERFACE_REQUEST_ERROR("777005060", "请求异常"),

    /**
     * 业务错误
     */
    BUSINESS_ERROR("777006000", "服务开小差了，马上就好，请您稍后再试"),
    ID_GENERATOR_ERROR("777006010", "ID生成异常"),
    HTTP_REQUEST_METHOD_ERROR("777006020", "请求方式异常"),
    CODE_IS_EXIST_ERROR("777006030", "该编码已存在"),
    MONGO_QUERY_TYPE_ERROR("777006040", "查询mongo异常"),
    MONGO_ADD_TYPE_ERROR("777006050", "新增mongo异常，主键重复"),

    /**
     * 权限错误
     */
    PERMISSION_NO_ACCESS("777007000", "当前操作没有权限"),
    PERMISSION_CUSTOMER_DATA_NO_ACCESS("777007001", "当前数据您没有操作权限"),
    PERMISSION_NO_ACCESS_VISIT_AUDITING("777007002", "贵公司申请的平台服务还在审核中，请您耐心等待"),
    PERMISSION_NO_ACCESS_VISIT_SYSTEM("777007003", "贵公司申请的平台服务已失效，您无权访问，如果需要继续使用本平台服务，请联系平台，谢谢"),

    /**
     * 客户信息错误
     */
    CUSTOMER_NO_EXISTS_ERROR("200001001", "尚未维护公司信息"),
    CUSTOMER_DATA_NO_COMPLETE_ERROR("200001002", "客户尚未完善公司资料"),

    /**
     * 单一错误
     */
    DECLARE_CEB_WARN("777008000", "未收到单一回执，请选择发送新增或发送变更"),

    /**
     * OPEN API错误
     */
    OPENAPI_CUSTOMER_INVALID("601","客户信息不存在"),
    OPENAPI_APPKEY_INVALID("602","appKey错误"),
    OPENAPI_APPSECRET__INVALID("603","appSecret错误"),
    ;

    private String code;
    private String msg;

    ResultCodeEnum() {
    }

    ResultCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


    @Override
    public String toString() {
        return "ResultCodeEnum [code=" + code + ", msg=" + msg + "]";
    }
}
