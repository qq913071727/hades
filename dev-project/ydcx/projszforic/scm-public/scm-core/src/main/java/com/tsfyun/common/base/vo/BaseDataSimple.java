package com.tsfyun.common.base.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseDataSimple implements Serializable {

    private String decCode;
    private String name;
}
