package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 校验当前单据是否可以进行某操作
 */
@Data
public class CheckTaskDTO implements Serializable {

    @NotNull(message = "单据ID不能为空")
    Serializable documentId;//单据id
    @NotEmptyTrim(message = "单据类型不能为空")
    String documentClass;//单据类型 (枚举类DomainTypeEnum中定义)
    @NotEmptyTrim(message = "操作码不能为空")
    @LengthTrim(max = 50,message = "操作码不能大于50位")
    String operation;
}
