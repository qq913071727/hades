package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import java.io.Serializable;

@Data
public class CssInfoQTO implements Serializable {

    @NotEmptyTrim(message = "参数错误")
    private String queryFlag;
    @NotEmptyTrim(message = "参数错误")
    private String enterCode;
}
