package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 基础数据类型枚举
 * @since Created in 2020/3/25 16:38
 */
public enum BaseDataTypeEnum {
    DeliveryMode("DeliveryMode","境外交货方式"),
    HkExpress("HkExpress","境外快递公司"),
    ReceivingMode("ReceivingMode","国内收货方式"),
    CusTrafMode("CusTrafMode","运输方式"),


//    CUS_LEVYTYPE("CUS_LEVYTYPE","征免性质"),
//    CUS_CUSTOMS("CUS_CUSTOMS","海关"),
//    CUS_MAPPING_NORMS_CODE_V("CUS_MAPPING_NORMS_CODE_V","集装箱规格"),
//    CUS_MAPPING_PACKAGEKIND_CODE_V("CUS_MAPPING_PACKAGEKIND_CODE_V","包装种类"),
//    CUS_MAPPING_TRADE_CODE_V("CUS_MAPPING_TRADE_CODE_V","监管方式"),
//    CUS_MAPPING_PORT_CODE_V("CUS_MAPPING_PORT_CODE_V","港口"),
//    CIQ_PORT_CN("CIQ_PORT_CN","国内口岸"),
//    CUS_TRANSF("CUS_TRANSF","运输方式"),
//    CUS_LEVYMODE("CUS_LEVYMODE","征免方式"),
//    CUS_MARK("CUS_MARK","运保杂方式"),
//    CUS_DISTRICT("CUS_DISTRICT","境内目的地"),
//    CIQ_CITY_CN("CIQ_CITY_CN","目的地代码"),
//    CUS_TRANSAC("CUS_TRANSAC","成交方式"),
//    DEC_ENTRY_TYPE("DEC_ENTRY_TYPE","报关单类型"),
//    CIQ_CUS_ORGANIZATION("CIQ_CUS_ORGANIZATION","验检疫机关"),



    ;
    private String code;
    private String name;

    BaseDataTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static BaseDataTypeEnum of(String code) {
        return Arrays.stream(BaseDataTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
