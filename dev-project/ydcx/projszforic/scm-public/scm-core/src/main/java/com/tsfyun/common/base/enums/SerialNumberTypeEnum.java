package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * =
 * (单据流水)单据类型
 */
public enum SerialNumberTypeEnum {

    IMP_AGREE("imp_agree", "供应链服务协议编号"),
    IMP_QUOTE("imp_quote", "报价单编号"),
    IMP_ORDER("imp_order", "进口订单编号"),
    EXP_ORDER("exp_order", "出口订单编号"),
    PURCHASE_CONTRACT("purchase_contract", "采购合同编号"),
    TRUST_ORDER("trust_order", "代理报关订单编号"),
    DECLARATION("declaration", "报关单编号"),
    IMP_RECEIPT_ACCOUNT("imp_receipt_account", "进口收款编号"),
    IMP_CROSS_BORDER_WAYBILL("imp_cross_border_waybill", "跨境运输编号"),
    DELIVERY_WAYBILL("delivery_waybill", "送货单"),
    ABROAD_RECEIVING_NOTE("abroad_receiving_note", "入库单编号"),
    DELIVERY_NOTE("delivery_note", "出库单编号"),
    INVOICE("invoice", "发票编号"),
    IMP_PAYMENT("imp_pay", "进口付汇单号"),
    EXP_PAYMENT("exp_pay", "出口付款单号"),
    REFUND_ACCOUNT("refund_account", "退款单单号"),
    MANIFEST("manifest", "代理报关舱单"),
    DECL_QUOTE("decl_quote", "报关报价单单号"),
    VEHICLE_DECLARE("vehicle_declare", "整车申报单号"),
    EXP_AGREE("exp_agree", "供应链出口协议编号"),
    OVERSEAS_RECEIVING_ACCOUNT("overseas_receiving_account", "境外收款单号"),
    SETTLEMENT_ACCOUNT("settlement_account", "结汇单单号"),
    DRAWBACK("drawback", "退税单单号"),
    OVERSEAS_DELIVERY_NOTE("overseas_delivery_note", "境外派送单号"),
    ;
    private String code;
    private String name;

    SerialNumberTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SerialNumberTypeEnum of(String code) {
        return Arrays.stream(SerialNumberTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
