package com.tsfyun.common.base.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class WriteOffOrderCostDTO implements Serializable {


    private Long id;//费用ID
    private Long trustOrderId;//订单ID
    private String trustOrderNo;//订单号
    private String expenseSubjectId;//科目ID
    private String expenseSubjectName;//科目名称
    private BigDecimal receAmount;//应收金额
    private BigDecimal acceAmount;//已收金额


    // #此字段为返回参数
    private BigDecimal currentWriteAmount = BigDecimal.ZERO;// 本次实际核销金额(返回修改数据使用)

    public WriteOffOrderCostDTO(Long id,BigDecimal currentWriteAmount){
        this.id = id;
        this.currentWriteAmount = currentWriteAmount;
    }
}
