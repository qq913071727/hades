package com.tsfyun.common.base.enums.exp;

import java.util.Arrays;
import java.util.Objects;

/**
 * 出口结汇方式
 */
public enum SettlementTypeEnum {
    NO("no", "无需结汇"),
    AUTONOMOUS("autonomous", "自主结汇"),
    ENTRUST("entrust", "委托结汇");
    private String code;
    private String name;

    SettlementTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SettlementTypeEnum of(String code) {
        return Arrays.stream(SettlementTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
