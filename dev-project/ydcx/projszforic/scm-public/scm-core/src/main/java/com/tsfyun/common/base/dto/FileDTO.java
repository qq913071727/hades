package com.tsfyun.common.base.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileDTO implements Serializable {

    @ApiModelProperty(value = "文件单据id")
    private String docId;

    @ApiModelProperty(value = "文件单据类型")
    private String docType;
}
