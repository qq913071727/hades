package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 交易类型
 */
public enum TransactionTypeEnum {
    IMP_ORDER("imp_order","进口"),
    EXP_ORDER("exp_order","出口"),
    IMP_payment("imp_payment","付汇"),
    RECEIVABLES("receivables","转账"),
    REFUND("refund","退款"),
    DECLATE("declare","代理报关"),
    ;
    private String code;
    private String name;

    TransactionTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static TransactionTypeEnum of(String code) {
        return Arrays.stream(TransactionTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
