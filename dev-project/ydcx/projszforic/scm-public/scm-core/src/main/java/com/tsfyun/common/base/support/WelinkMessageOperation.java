package com.tsfyun.common.base.support;

import com.google.common.collect.ImmutableMap;
import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.AliYunMessageTemplate;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;

import java.util.Map;

public class WelinkMessageOperation implements MessageOperation{

    /**
     * 阿里云模板和操作映射
     */
    private static Map<MessageNodeEnum, OperationEnum> operationMap = ImmutableMap.<MessageNodeEnum, OperationEnum>builder()

            //通用认证
            .put(MessageNodeEnum.COMMON_PROVE, AliYunMessageTemplate.PROVE)
            //注册登录
            .put(MessageNodeEnum.REGISTER_LOGIN,AliYunMessageTemplate.PROVE)
            //找回密码
            .put(MessageNodeEnum.RETRIEVE_PASSWORD,AliYunMessageTemplate.PROVE)
            //解绑手机
            .put(MessageNodeEnum.UNBIND_PHONE,AliYunMessageTemplate.CHANGE_IMPORTANT)
            //绑定手机
            .put(MessageNodeEnum.BIND_PHONE,AliYunMessageTemplate.CHANGE_IMPORTANT)

            //付汇审核退回修改
            .put(MessageNodeEnum.PAY_BACK,AliYunMessageTemplate.PAY_BACK)
            //付汇审核退回作废
            .put(MessageNodeEnum.PAY_INVALID,AliYunMessageTemplate.PAY_INVALID)
            //付汇完成
            .put(MessageNodeEnum.PAY_COMPLETE,AliYunMessageTemplate.PAY_COMPLETE)

            //境外发车
            .put(MessageNodeEnum.WAYBILL_DEPARTURE,AliYunMessageTemplate.WAYBILL_DEPARTURE)

            //到达深圳仓
            .put(MessageNodeEnum.WAYBILL_ARRIVE,AliYunMessageTemplate.WAYBILL_ARRIVE)

            //客户付款财务已确认
            .put(MessageNodeEnum.CUSTOMER_PAY,AliYunMessageTemplate.CUSTOMER_PAY)

            //订单退回修改
            .put(MessageNodeEnum.ORDER_BACK,AliYunMessageTemplate.ORDER_BACK)

            //客户审核通过
            .put(MessageNodeEnum.CUSTOMER_APROVED,AliYunMessageTemplate.CUSTOMER_APROVED)

            //客户审核未通过待完善资料
            .put(MessageNodeEnum.CUSTOMER_REJECT,AliYunMessageTemplate.CUSTOMER_REJECT)

            .build();

    @Override
    public ShortMessagePlatformEnum getPlatform() {
        return ShortMessagePlatformEnum.ALIYUN;
    }

    @Override
    public Map<MessageNodeEnum, OperationEnum> getMessageTemplate() {
        return operationMap;
    }

    /**
     * 内部类单例模式
     */
    private static class WelinkMessageOperationInstance{
        private static final WelinkMessageOperation instance = new WelinkMessageOperation();
    }

    private WelinkMessageOperation(){}

    public static WelinkMessageOperation getInstance(){
        return WelinkMessageOperation.WelinkMessageOperationInstance.instance;
    }


}

