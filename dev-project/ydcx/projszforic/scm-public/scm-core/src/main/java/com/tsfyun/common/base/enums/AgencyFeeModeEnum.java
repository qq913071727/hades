package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 代理费计算模式
 * @CreateDate: Created in 2020/10/26 14:20
 */
public enum AgencyFeeModeEnum {

    EXPRESS("express", "按重量计费"),
    STANDARD("standard", "按金额计费"),
    ;
    private String code;
    private String name;

    AgencyFeeModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static AgencyFeeModeEnum of(String code) {
        return Arrays.stream(AgencyFeeModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
