package com.tsfyun.common.base.enums.customs;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 运费支付方式
 */
public enum PaymentMethodCodeEnum {
    FREIGHT_PAY_1("1", "Direct payment"),
    FREIGHT_PAY_2("2", "Automatic clearing house credit"),
    FREIGHT_PAY_3("3", "Automatic clearing house debit"),
    FREIGHT_PAY_4("4", "Automatic clearing house credit-savings account"),
    FREIGHT_PAY_5("5", "Automatic clearing house debit-demand account"),
    FREIGHT_PAY_6("6", "Bank book transfer (credit)"),
    FREIGHT_PAY_7("7", "Bank book transfer (debit)"),
    FREIGHT_PAY_8("8", "Doc collection via 3rd party with bill of EX"),
    FREIGHT_PAY_9("9", "Doc collection via 3rd party no bill of EX"),
    FREIGHT_PAY_10("10", "Irrevocable documentary credit"),
    FREIGHT_PAY_11("11", "Transferable irrevocable documentary credit"),
    FREIGHT_PAY_12("12", "Confirmed irrevocable documentary credit"),
    FREIGHT_PAY_13("13", "Transferable confirmed irrevocable documentary credit"),
    FREIGHT_PAY_14("14", "Revocable documentary credit"),
    FREIGHT_PAY_15("15", "Irrevocable letter of credit-confirmed"),
    FREIGHT_PAY_16("16", "Letter of guarantee"),
    FREIGHT_PAY_17("17", "Revocable letter of credit"),
    FREIGHT_PAY_18("18", "Standby letter of credit"),
    FREIGHT_PAY_19("19", "Irrevocable letter of credit unconfirmed"),
    FREIGHT_PAY_20("20", "Clean collection (ICC)"),
    FREIGHT_PAY_21("21", "Documentary collection (ICC)"),
    FREIGHT_PAY_22("22", "Documentary sight collection (ICC)"),
    FREIGHT_PAY_23("23", "Documentary collection with date of expiry (ICC)"),
    FREIGHT_PAY_24("24", "Documentary collection: bill of exchange against acceptance"),
    FREIGHT_PAY_25("25", "Documentary collection: bill of exchange against payment"),
    FREIGHT_PAY_26("26", "Collection subject to buyer's approval (ICC)"),
    FREIGHT_PAY_27("27", "Collection by a bank consignee for the goods (ICC)"),
    FREIGHT_PAY_34("34", "Seller to advise buyer"),
    FREIGHT_PAY_35("35", "Documents through banks"),
    FREIGHT_PAY_36("36", "Charging (to account)"),
    FREIGHT_PAY_37("37", "Available with issuing bank"),
    FREIGHT_PAY_38("38", "Available with advising bank"),
    FREIGHT_PAY_39("39", "Available with named bank"),
    FREIGHT_PAY_40("40", "Available with any bank"),
    FREIGHT_PAY_41("41", "Available with any bank in ..."),
    FREIGHT_PAY_42("42", "Indirect payment"),
    FREIGHT_PAY_43("43", "Reassignment"),
    FREIGHT_PAY_44("44", "Offset"),
    FREIGHT_PAY_45("45", "Special entries"),
    FREIGHT_PAY_46("46", "Instalment payment"),
    FREIGHT_PAY_47("47", "Instalment payment with draft"),
    FREIGHT_PAY_48("48", "Available with advise through bank"),
    FREIGHT_PAY_49("49", "General conditions of sale"),
    FREIGHT_PAY_50("50", "Special payment"),
    FREIGHT_PAY_61("61", "Set-off by exchange of documents"),
    FREIGHT_PAY_62("62", "Set-off by reciprocal credits"),
    FREIGHT_PAY_64("64", "Set-off by exchange of goods"),
    FREIGHT_PAY_65("65", "Reverse factoring"),
    FREIGHT_PAY_69("69", "Other set-off"),
    FREIGHT_PAY_70("70", "Supplier to invoice"),
    FREIGHT_PAY_71("71", "Recipient to self bill"),
    FREIGHT_PAY_ZZZ("ZZZ", "Mutually defined"),
    ;
    private String code;
    private String name;

    PaymentMethodCodeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static PaymentMethodCodeEnum of(String code) {
        return Arrays.stream(PaymentMethodCodeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
