package com.tsfyun.common.base.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FileQtyVO implements Serializable {

    private String docId;
    private String docType;
    private String businessType;
    private Integer quantity;
    private String fileId;//取其中一个文件ID
}
