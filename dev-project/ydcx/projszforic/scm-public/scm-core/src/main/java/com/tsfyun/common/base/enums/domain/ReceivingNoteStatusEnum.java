package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 入库单状态
 * @since Created in 2020/4/1 10:41
 */
public enum ReceivingNoteStatusEnum implements OperationEnum {
    RECEIVED("received", "已入库")
    ;
    private String code;

    private String name;

    ReceivingNoteStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ReceivingNoteStatusEnum of(String code) {
        return Arrays.stream(ReceivingNoteStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
