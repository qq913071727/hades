package com.tsfyun.common.base.help.excel.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ExcelCellProp implements Serializable {

    /**
     * 是否是合并单元格
     */
    private Boolean isCombineCell;

    /**
     * 单元格内容
     */
    private String cellValue;

    /**
     * 合并开始列序号
     */
    private Integer firstColumn;

    /**
     * 合并结束列序号
     */
    private Integer lastColumn;

    /**
     * 合并开始行序号
     */
    private Integer firstRow;

    /**
     * 合并结束行序号
     */
    private Integer lastRow;

}
