package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 核销类型
 */
public enum WriteOffTypeEnum {
    RECEIVABLE("receivable","进口应收"),
    RECEIVABLE_EXP("receivable_exp","出口应收"),
    DECLARE("declare","代理报关"),
    REFUND("refund","退款"),
    BAD_DEBT("bad_debt","坏账"),
    ;
    private String code;
    private String name;

    WriteOffTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WriteOffTypeEnum of(String code) {
        return Arrays.stream(WriteOffTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
