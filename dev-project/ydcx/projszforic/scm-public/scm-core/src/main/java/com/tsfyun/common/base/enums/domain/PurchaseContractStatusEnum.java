package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 采购合同状态
 */
public enum PurchaseContractStatusEnum implements OperationEnum {
    WAIT_CONFIRM("waitConfirm","待确认"),
    WAIT_INVOICE("waitInvoice","待收票"),
    COMPLETE("complete","已完成")
    ;
    private String code;

    private String name;

    PurchaseContractStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static PurchaseContractStatusEnum of(String code) {
        return Arrays.stream(PurchaseContractStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

