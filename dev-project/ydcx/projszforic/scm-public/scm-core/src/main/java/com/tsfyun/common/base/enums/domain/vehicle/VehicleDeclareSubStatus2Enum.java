package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 整车申报单据进度（三单，清单）
 */
public enum VehicleDeclareSubStatus2Enum implements OperationEnum {
    APPROVING("approving", "审核中"),
    COMPLETE("complete", "已完成"),
    CANCEL("cancel", "已作废"),
    ;
    private String code;

    private String name;

    VehicleDeclareSubStatus2Enum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static VehicleDeclareSubStatus2Enum of(String code) {
        return Arrays.stream(VehicleDeclareSubStatus2Enum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
