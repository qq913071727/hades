package com.tsfyun.common.base.annotation;

import java.lang.annotation.*;

/**
 * 重复提交验证注解，可以用于一些重要的场景，防止后台响应慢前端未做处理重复提交
 * add by rick 2020-03-15
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DuplicateSubmit {

    /**
     * 如果有唯一字段区分一个请求（如前端传一个唯一的请求id），则可以使用key，否则系统将将请求的url和参数组装唯一key
     * 如果定义了key，则解析spel表达式作为key；否则通过参数nonce获取
     * map为接收参数，格式为#data[key]
     * list为接收参数，格式为#list[index]
     * JSONObject为接收参数，格式同map
     *
     * @return
     */
    //暂不使用，直接用参数代替
    @Deprecated
    String key() default "";

}
