package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 进口缴税方
 */
public enum VoluntarilyTaxEnum {

    PARTYB("partyb", "乙方缴税"),
    PARTYA("partya", "甲方缴税")
    ;
    private String code;
    private String name;

    VoluntarilyTaxEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static VoluntarilyTaxEnum of(String code) {
        return Arrays.stream(VoluntarilyTaxEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
