package com.tsfyun.common.base.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class ExportExcelFileDTO implements Serializable {

    private String fileName;//文件名称
    private String sheetName;//sheet名称
    private String operator;//操作人
    private List<String> titles;//标题名称
    private List datas;//内容
    private List<String> total;//需要合计的数据
    private Boolean showIndex;//是否添加序号列

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List getDatas() {
        return datas;
    }

    public void setDatas(List datas) {
        this.datas = datas;
    }

    public List<String> getTotal() {
        return total;
    }

    public void setTotal(List<String> total) {
        this.total = total;
    }

    public void setShowIndex(Boolean showIndex) {
        this.showIndex = showIndex;
    }

    public Boolean getShowIndex() {
        if(Objects.isNull(showIndex)) {
            return Boolean.TRUE;
        }
        return showIndex;
    }

}
