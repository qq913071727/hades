package com.tsfyun.common.base.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CssInfoVO implements Serializable {

    /**
     * 18位社会信用代码
     */
    private String id;
    /**
     * 公司名称
     */
    private String name;
    /**
     * 10位海关代码
     */
    private String customsCode;
    /**
     * 10位检验检疫编码
     */
    private String ciqCode;
    /**
     * 组织机构代码
     */
    private String orgCode;
//    /**
//     * 海关代码状态
//     */
//    private String cusStatus;
//    /**
//     * 检验检疫编码状态
//     */
//    private String ciqStatus;
//    /**
//     * 描述
//     */
//    private String resultDetail;
//    private String manaType;
//    private String apanageRegion;
//    /**
//     * 创建时间
//     */
//    private Date dateCreated;
//    private String copMasterCus;
}
