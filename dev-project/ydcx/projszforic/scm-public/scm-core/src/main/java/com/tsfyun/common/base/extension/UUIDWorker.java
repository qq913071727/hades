package com.tsfyun.common.base.extension;

import tk.mybatis.mapper.genid.GenId;

import java.util.UUID;

/**
 * @Description: UUID主键生成策略
 * @CreateDate: Created in 2021/3/8 16:28
 */
public class UUIDWorker implements GenId<String> {

    @Override
    public String genId(String table, String column) {
        return UUID.randomUUID().toString().replaceAll("-","").toLowerCase();
    }

}
