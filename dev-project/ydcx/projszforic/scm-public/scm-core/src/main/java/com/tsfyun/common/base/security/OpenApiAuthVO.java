package com.tsfyun.common.base.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/29 17:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenApiAuthVO implements Serializable {

    private Long customerId;

    private String customerName;

}
