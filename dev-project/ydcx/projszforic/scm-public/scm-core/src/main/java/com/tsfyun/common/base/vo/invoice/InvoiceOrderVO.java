package com.tsfyun.common.base.vo.invoice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceOrderVO implements Serializable {

    private Long orderId; // 订单ID
    private String orderNo;// 订单编号
    private String expenseSubjectId;// 费用科目编码
    private String expenseSubjectName;// 费用科目名称
    private BigDecimal invoiceValue;// 发票金额

}
