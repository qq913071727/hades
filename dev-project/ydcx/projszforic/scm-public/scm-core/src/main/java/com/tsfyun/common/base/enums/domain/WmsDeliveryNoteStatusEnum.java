package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

public enum WmsDeliveryNoteStatusEnum implements OperationEnum {
    BE_DELIVERED("beDelivered", "待出库"),
    OUTBOUND("outbound", "已出库"),
    TO_VOID("toVoid", "已作废");

    private String code;

    private String name;

    WmsDeliveryNoteStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static WmsDeliveryNoteStatusEnum of(String code) {
        return Arrays.stream(WmsDeliveryNoteStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
