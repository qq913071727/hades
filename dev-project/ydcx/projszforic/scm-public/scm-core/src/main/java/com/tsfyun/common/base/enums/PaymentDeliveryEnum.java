package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 国内送货付款方式
 * @since Created in 2020/3/25 16:38
 */
public enum PaymentDeliveryEnum {
    TOPAY("TOPAY","到付"),
    ADVPAY("ADVPAY","垫付"),
    ;
    private String code;
    private String name;

    PaymentDeliveryEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static PaymentDeliveryEnum of(String code) {
        return Arrays.stream(PaymentDeliveryEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
