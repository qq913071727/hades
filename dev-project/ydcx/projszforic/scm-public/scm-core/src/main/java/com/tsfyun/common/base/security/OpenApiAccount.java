package com.tsfyun.common.base.security;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/29 18:26
 */
@Data
public class OpenApiAccount implements Serializable {

    private String customerName;

    private String appKey;

    private String appSecret;

}
