package com.tsfyun.common.base.config;

import com.tsfyun.common.base.config.properties.BaiduProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/4 09:28
 */
@Configuration
public class BaiduConfig {

    @Bean
    public BaiduProperties baiduProperties(){
        return new BaiduProperties();
    }

}
