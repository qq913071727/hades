package com.tsfyun.common.base.vo.invoice;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class InvoiceTotalVO implements Serializable {

    private Long customerId;// 客户ID
    private String customerName;// 客户名称
    private BigDecimal invoiceValue;// 发票金额

    // 开票订单信息
    private List<InvoiceOrderVO> invoiceOrderList;
}
