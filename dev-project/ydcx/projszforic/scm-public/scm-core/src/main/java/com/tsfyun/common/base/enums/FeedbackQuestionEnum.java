package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 意见反馈类型
 * @CreateDate: Created in 2020/9/23 18:30
 */
public enum FeedbackQuestionEnum {

    EXCEPTION("exception", "功能异常"),
    SUGGESTION("suggestion", "优化建议"),
    CONSULTING("consulting", "业务咨询"),

    ;
    private String code;
    private String name;

    FeedbackQuestionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static FeedbackQuestionEnum of(String code) {
        return Arrays.stream(FeedbackQuestionEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
