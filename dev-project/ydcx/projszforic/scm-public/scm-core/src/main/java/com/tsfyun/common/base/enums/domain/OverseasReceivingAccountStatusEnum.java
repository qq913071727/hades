package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 境外收款状态
 * @since Created in 2021/10/8 10:41
 */
public enum OverseasReceivingAccountStatusEnum implements OperationEnum {
    WAIT_CONFIRM("waitConfirm", "待确定"),
    WAIT_CLAIM("waitClaim", "待认领"),
    COMPLETE("complete","已完成"),
    INVALID("invalid", "已作废"),
    ;
    private String code;

    private String name;

    OverseasReceivingAccountStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static OverseasReceivingAccountStatusEnum of(String code) {
        return Arrays.stream(OverseasReceivingAccountStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
