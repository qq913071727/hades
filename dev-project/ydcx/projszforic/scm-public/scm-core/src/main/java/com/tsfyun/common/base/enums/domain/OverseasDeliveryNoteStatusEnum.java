package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

public enum OverseasDeliveryNoteStatusEnum implements OperationEnum {
    WAIT_CONFIRM("waitConfirm", "待确认"),
    WAIT_DELIVERY("waitDelivery", "待派送"),
    DELIVERING("delivering", "派送中"),
    SIGNED("signed", "已派送"),;

    private String code;

    private String name;

    OverseasDeliveryNoteStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static OverseasDeliveryNoteStatusEnum of(String code) {
        return Arrays.stream(OverseasDeliveryNoteStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
