package com.tsfyun.common.base.vo;

import com.tsfyun.common.base.enums.WarehouseTypeEnum;
import com.tsfyun.common.base.util.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 仓库响应实体
 * </p>
 *

 * @since 2020-03-31
 */
@Data
@ApiModel(value="Warehouse响应对象", description="仓库响应实体")
public class WarehouseVO implements Serializable {

     private static final long serialVersionUID=1L;

     private Long id;

    @ApiModelProperty(value = "仓库编码")
    private String code;

    @ApiModelProperty(value = "仓库中文名称")
    private String name;

    @ApiModelProperty(value = "仓库英文名称")
    private String nameEn;

    @ApiModelProperty(value = "公司名称")
    private String companyName;

    @ApiModelProperty(value = "仓库中文地址")
    private String address;

    @ApiModelProperty(value = "仓库英文地址")
    private String addressEn;

    private String linkPerson;

    private String linkTel;

    @ApiModelProperty(value = "仓库类型")
    private String warehouseType;

    @ApiModelProperty(value = "是否禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "是否开启国内自提")
    private Boolean isSelfMention;

    private String memo;

    private String warehouseTypeDesc;

    private String detailDesc;

    public String getWarehouseTypeDesc() {
        WarehouseTypeEnum warehouseTypeEnum = WarehouseTypeEnum.of(warehouseType);
        return Objects.nonNull(warehouseTypeEnum) ? warehouseTypeEnum.getName() : null;
    }

    public String getDetailDesc() {
        return String.format("【%s】%s %s (%s %s)",name, StringUtils.null2EmptyWithTrim(companyName),StringUtils.null2EmptyWithTrim(address),StringUtils.null2EmptyWithTrim(linkPerson),StringUtils.null2EmptyWithTrim(linkTel));
    }

}
