package com.tsfyun.common.base.annotation;

import java.lang.annotation.*;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/22 14:24
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LocalDateTimeFormat {

    String value() default "yyyy-MM-dd HH:mm:ss";

}
