package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 登录类型
 * @since Created in 2020/2/25 15:46
 */
public enum LoginTypeEnum {

    ACCOUNT(0, "账号密码"),
    PHONE(1, "手机验证码"),
    WXXCX(2, "微信小程序(用户信息)"),
    WXXCX_PHONE(3, "微信小程序(手机号码)"),
    WXGZH(4, "微信公众号"),
    ;
    private Integer code;
    private String name;

    LoginTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static LoginTypeEnum of(Integer code) {
        return Arrays.stream(LoginTypeEnum.values()).filter(r -> r.code.equals(code)).findFirst().orElse(null);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
