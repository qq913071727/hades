package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 汇总单申请单状态
 */
public enum VehicleSummaryStatusEnum implements OperationEnum {
    EXCEPTION("-1","处理异常"),
    TEMP("1","已暂存"),
    DECLARING("2","申报"),
    SEND_CUSTOMS_FAIL("4","发送海关失败"),
    BACK("100","海关退单"),
    ;
    private String code;

    private String name;

    VehicleSummaryStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static VehicleSummaryStatusEnum of(String code) {
        return Arrays.stream(VehicleSummaryStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

