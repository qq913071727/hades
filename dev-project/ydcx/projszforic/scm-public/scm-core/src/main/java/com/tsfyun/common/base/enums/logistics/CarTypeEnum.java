package com.tsfyun.common.base.enums.logistics;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @since Created in 2020/3/20 22：07
 */
public enum CarTypeEnum {


    CAR_03("3T", "3T"),
    CAR_5T("5T", "5T"),
    CAR_8T("8T", "8T"),
    CAR_10T("10T", "10T"),
    CAR_12T("12T", "12T"),
    CAR_20C("20C", "20尺"),
    CAR_40C("40C", "40尺"),
    CAR_45C("45C", "45尺"),
    ;
    private String code;
    private String name;

    CarTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private static List<String> cabinets = Lists.newArrayList(CAR_20C.code,CAR_40C.code,CAR_45C.code);

    public static CarTypeEnum of(String code) {
        return Arrays.stream(CarTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public static Boolean isCabinet(String code) {
        return cabinets.contains(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
