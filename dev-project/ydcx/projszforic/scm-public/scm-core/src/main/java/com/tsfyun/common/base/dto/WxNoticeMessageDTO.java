package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.enums.WxMessageDefineTemplate;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/23 11:09
 */
@Data
public class WxNoticeMessageDTO implements Serializable {

    /**
     * 用户微信公众号id
     */
    private String wxgzhOpenid;

    /**
     * 微信通知消息所用模板
     */
    private WxMessageDefineTemplate wxMessageDefineTemplate;

    /**
     * 是否需要跳转至微信小程序，必传（如果设置为true，miniprogram必传）
     */
    private Boolean isNavToMiniprogram = Boolean.TRUE;

    /**
     * 微信通知消息参数字段和值以及文本颜色
     */
    private LinkedHashMap<String,WxNoticeAttr> params;


    /**
     * 微信小程序跳转参数
     */
    private WxMiniProgram miniprogram;

    /**
     * 模板跳转链接
     */
    private String url;

}
