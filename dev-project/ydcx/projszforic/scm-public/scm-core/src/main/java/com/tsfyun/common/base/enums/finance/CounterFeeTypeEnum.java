package com.tsfyun.common.base.enums.finance;

import java.util.Arrays;
import java.util.Objects;

public enum CounterFeeTypeEnum {
    OUR("OUR", "客户承担"),//付款方承担
    BEN("BEN", "供应商承担"),//收款方承担
    SHA("SHA", "共同承担"),//共同承担
    ;
    private String code;
    private String name;

    CounterFeeTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CounterFeeTypeEnum of(String code) {
        return Arrays.stream(CounterFeeTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
