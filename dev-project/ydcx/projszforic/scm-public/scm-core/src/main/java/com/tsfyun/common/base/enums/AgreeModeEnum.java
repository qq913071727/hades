package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 约定方式
 */
public enum AgreeModeEnum {
    IMPORT_DAY("import_day", "进口日"),
    PAYMENT_DAY("payment_day", "付款日"),
    MONTH("month", "月结"),
    HALF_MONTH("half_month", "半月结"),
    WEEK("week", "周结")
    ;
    private String code;
    private String name;

    AgreeModeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static AgreeModeEnum of(String code) {
        return Arrays.stream(AgreeModeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
