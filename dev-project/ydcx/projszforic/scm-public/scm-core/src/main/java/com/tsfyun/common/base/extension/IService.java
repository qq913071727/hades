package com.tsfyun.common.base.extension;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.enums.SortTypeEnum;
import org.springframework.lang.NonNull;
import tk.mybatis.mapper.entity.SqlsCriteria;
import tk.mybatis.mapper.util.Sqls;
import tk.mybatis.mapper.weekend.Fn;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;

public interface IService<T> {

    /**
     * 新增
     *
     * @param t
     * @return
     */
    boolean save(T t);

    /**
     * 根据id删除
     *
     * @param o
     * @return
     */
    boolean removeById(Serializable o);

    /**
     * 删除
     *
     * @param t
     * @return
     */
    boolean remove(T t);

    /**
     * 根据id批量删除
     *
     * @param ids
     * @return
     */
    boolean removeByIds(List ids);

    /**
     * 保存非空字段
     *
     * @param t
     * @return
     */
    boolean saveNonNull(T t);

    /**
     * 根据主键查询
     *
     * @param o
     * @return
     */
    T getById(Serializable o);

    /**
     * 根据入参的条件，进行绝对匹配筛选，并返回符合条件的结果总数，该方法如果入参为null，则返回的是表的记录总数；
     * 条件采用的是绝对匹配；
     *
     * @param t
     * @return
     */
    int count(T t);

    default int count() {
        return this.count(null);
    }

    ;

    /**
     * 根据入参的条件，进行绝对匹配筛选，
     * <p>
     * 并返回<T>对象的集
     * 该方法如果入参为null，则和selectAll的效果是一致的；
     * 该方法如果入参对象存在，但是其属性都是null，则和selectAll的效果是一致的
     *
     * @param t
     * @return
     */
    List<T> list(T t);

    /**
     * 查询所有
     *
     * @return
     */
    default List<T> list() {
        return this.list(null);
    }

    ;

    /**
     * 根据入参的条件，进行绝对匹配筛选，
     * 该方法如果查询到0-1个结果，则返回null或者<T>；
     * 并返回<T>对象
     * 该方法如果查询到0-1个结果，则返回null或者<T>；
     *
     * @param t
     * @return
     */
    T getOne(T t);

    /**
     * 根据主键全部字段更新
     *
     * @param t
     * @return
     */
    boolean updateById(T t);

    /**
     * 根据主键更新非空字段
     *
     * @param t
     * @return
     */
    boolean updateByIdSelective(T t);

    /**
     * 批量更新(修复支持自增主键,不会自动生成创建时间，创建人，修改时间，修改人，需单独赋值)
     *
     * @param list
     * @return
     */
    int savaBatch(List<T> list);

    /**
     * @Description: 分页查询
     * @Date: 2019/6/9 18:36
     * @Param: pojo 条件,根据实体中的属性查询，查询条件使用等号
     * @Param: pageNo 当前页号
     * @Param: pageSize 页大小
     * @param： orderBy排序字段，格式如code desc或code asc
     * @return:
     **/
    PageInfo<T> pageList(T pojo, Integer pageNo, Integer pageSize, String orderBy);

    //支持多字段排序，查询指定字段
    PageInfo<T> pageList(Integer pageNo, Integer pageSize, SqlsCriteria where, List<OrderItem> sorts, Fn<T, Object>... fields);

    /**
     * 排序参数
     *
     * @param orderByColumn
     * @param sort
     * @return
     */
    String getOrderBy(String orderByColumn, String sort);

    /**
     * 获取所有的字段
     * @param fields
     * @return
     */
    List<String> getFields(Fn<T, Object>... fields);

    /**
     * 修改单据状态，需确保表有status_id字段
     * @param documentId
     * @param oldDocumentStatus
     * @param nowDocumentStatus
     * @return
     */
    int updateDocumentStatus(T t, Serializable documentId, String oldDocumentStatus,@NonNull String nowDocumentStatus);


}
