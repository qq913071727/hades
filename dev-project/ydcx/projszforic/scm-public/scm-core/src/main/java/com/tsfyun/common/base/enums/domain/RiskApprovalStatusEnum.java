package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 风控审批单据状态
 * @Project:
 * @CreateDate: Created in 2020/5/8 14:31
 */
public enum RiskApprovalStatusEnum implements OperationEnum {
    WAIT_EXAMINE("waitExamine", "待审核"),
    APPROVED("approved", "审核通过"),
    NO_PASS("noPass", "审核不通过"),
    ;
    private String code;

    private String name;

    RiskApprovalStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static RiskApprovalStatusEnum of(String code) {
        return Arrays.stream(RiskApprovalStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
