package com.tsfyun.common.base.util;

import java.util.regex.Pattern;

/**
 * SQL注入检测
 */
public class SQLFilter {

    private static String regex = ".*(update|delete|insert|trancate|drop|execute).*";

    /**
     * SQL注入过滤
     * @param str  待验证的字符串
     */
    public static boolean sqlInject(String str){
        if(StringUtils.isEmpty(str)){
            return false;
        }
        //去掉换行符，制表符等特殊符号
        str = StringUtils.removeSpecialSymbol(str);
        str = str.replaceAll("\t","");
        //转换成小写
        str = str.toLowerCase();
        boolean isMatch = Pattern.matches(regex, str);
        return isMatch;
    }

}
