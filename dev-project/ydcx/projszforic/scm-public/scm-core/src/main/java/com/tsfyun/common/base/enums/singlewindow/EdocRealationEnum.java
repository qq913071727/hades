package com.tsfyun.common.base.enums.singlewindow;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 单一窗口随附单证类型
 */
public enum EdocRealationEnum {
    INVOICE("00000001", "发票"),
    PACKINGLIST("00000002", "装箱单"),
    PICK_UP("00000003", "提/运单"),
    CONTRACT("00000004", "合同"),
    AGENCY_AGREEMENT("10000001", "代理报关委托协议（电子）"),
    RELIEF_GUAREENT("10000002", "减免税货物税款担保证明"),
    DELAY_RELIEF_GUAREENT("10000003", "减免税货物税款担保延期证明"),
    COMPANY_PROVE_DATA("50000001", "企业提供的证明材料"),
    COMPANY_OTHER_DATA("50000004", "企业提供的其他"),
    ;
    private String code;
    private String name;

    EdocRealationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static EdocRealationEnum of(String code) {
        return Arrays.stream(EdocRealationEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
