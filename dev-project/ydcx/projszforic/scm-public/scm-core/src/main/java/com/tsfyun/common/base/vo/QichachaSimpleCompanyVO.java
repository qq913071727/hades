package com.tsfyun.common.base.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value="QichachaCompany响应对象", description="企查查查询公司基础响应实体")
public class QichachaSimpleCompanyVO implements Serializable {

    private String keyNo;//KeyNo
    private String name;//企业名称
    private String creditCode;//统一社会信用代码
    private String startDate;//成立日期
    private String operName;//法定代表人姓名
    private String status;//状态
    private String no;//注册号
    private String address;//注册地址
}
