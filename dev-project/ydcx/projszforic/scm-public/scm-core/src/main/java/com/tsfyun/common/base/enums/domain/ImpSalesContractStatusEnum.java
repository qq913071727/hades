package com.tsfyun.common.base.enums.domain;


import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description:
 * @since Created in 2020/4/1 10:41
 */
public enum ImpSalesContractStatusEnum implements OperationEnum {
    COMFIRMED("confirmed", "已确认")
    ;
    private String code;

    private String name;

    ImpSalesContractStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static ImpSalesContractStatusEnum of(String code) {
        return Arrays.stream(ImpSalesContractStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
