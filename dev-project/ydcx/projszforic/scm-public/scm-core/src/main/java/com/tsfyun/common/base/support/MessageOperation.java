package com.tsfyun.common.base.support;

import com.tsfyun.common.base.enums.MessageNodeEnum;
import com.tsfyun.common.base.enums.shortmsg.ShortMessagePlatformEnum;

import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/15 14:58
 */
public interface MessageOperation {

    ShortMessagePlatformEnum getPlatform();

    Map<MessageNodeEnum, OperationEnum> getMessageTemplate();

}
