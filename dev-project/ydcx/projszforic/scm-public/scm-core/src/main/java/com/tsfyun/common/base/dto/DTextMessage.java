package com.tsfyun.common.base.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * =
 * 钉钉文本消息
 */
@Setter
@Getter
public class DTextMessage implements Serializable {

    @NotEmpty(message = "消息内容不能为空")
    private String text;//文本
    private List<String> atList;//@人员
}
