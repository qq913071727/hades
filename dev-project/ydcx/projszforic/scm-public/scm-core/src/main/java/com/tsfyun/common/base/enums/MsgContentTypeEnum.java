package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 消息内容类型
 */
public enum MsgContentTypeEnum {
    TEXT("text", "文本"),
    IMAGE("image", "图片"),
    HTML("html", "网页"),
    ;
    private String code;
    private String name;

    MsgContentTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MsgContentTypeEnum of(String code) {
        return Arrays.stream(MsgContentTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
