package com.tsfyun.common.base.exception;

/**
 * =
 * 客户端错误信息异常
 */
public class ClientException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    //异常信息
    private String message; //错误描述

    public ClientException(String message) {
        super(message);
        this.message = message;
    }

    public ClientException(String code, String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
