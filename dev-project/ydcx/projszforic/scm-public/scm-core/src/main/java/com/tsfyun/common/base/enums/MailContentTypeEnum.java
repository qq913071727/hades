package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * =
 * 邮件内容类型
 */
public enum MailContentTypeEnum {

    REGISTER("register", "注册"),
    COMMON("common", "通用");
    private String code;
    private String name;

    MailContentTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static MailContentTypeEnum of(String code) {
        return Arrays.stream(MailContentTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
