package com.tsfyun.common.base.config;

import com.tsfyun.common.base.support.ContextDecorator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Rick on 2018/12/18.
 *
 * @ Description：线程池配置
 */
@Configuration
public class ExecutePoolConfiguration {

    @Value("${threadpool.core-pool-size:10}")
    private int corePoolSize;

    @Value("${threadpool.max-pool-size:50}")
    private int maxPoolSize;

    @Value("${threadpool.queue-capacity:500}")
    private int queueCapacity;

    @Value("${threadpool.keep-alive-seconds:100}")
    private int keepAliveSeconds;

    /**
     * corePoolSize-核心线程数：线程池创建时候初始化的线程数
     * maxPoolSize-最大线程数：线程池最大的线程数，只有在缓冲队列满了之后才会申请超过核心线程数的线程
     * maxPoolSize-缓冲队列：用来缓冲执行任务的队列
     * keepAliveSeconds-允许线程的空闲时间配置秒：当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
     * 线程池名的前缀：设置好了之后可以方便我们定位处理任务所在的线程池
     * 线程池对拒绝任务的处理策略：这里采用了CallerRunsPolicy策略，当线程池没有处理能力的时候，
     * 该策略会直接在 execute 方法的调用线程中运行被拒绝的任务；如果执行程序已关闭，则会丢弃该任务
     */
    @Bean("threadPoolTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(corePoolSize);
        pool.setMaxPoolSize(maxPoolSize);
        pool.setQueueCapacity(queueCapacity);
        pool.setKeepAliveSeconds(keepAliveSeconds);
        //pool.setTaskDecorator(new ContextDecorator());
        //当有新任务添加到线程池被拒绝时，线程池会将被拒绝的任务添加到"线程池正在运行的线程"中去运行
        pool.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return pool;
    }

}
