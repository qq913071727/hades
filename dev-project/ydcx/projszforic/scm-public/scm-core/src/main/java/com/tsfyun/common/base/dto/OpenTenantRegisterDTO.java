package com.tsfyun.common.base.dto;

import com.tsfyun.common.base.annotation.EnumCheck;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 租户开通请求实体
 * @since Created in 2020/5/11 14:08
 */
@Data
public class OpenTenantRegisterDTO implements Serializable {

    @NotEmptyTrim(message = "还未注册成为平台用户")
    private String customerId;

    /**
     * 租户编码
     */
    @LengthTrim(max = 10,message = "租户编码长度不能超过10位")
    @NotEmptyTrim(message = "租户编码不能为空")
    private String tenantCode;

    /**
     * 公司名称
     */
    @LengthTrim(max = 255,message = "公司名称长度不能超过255位")
    @NotEmptyTrim(message = "公司名称不能为空")
    private String companyName;

    /**
     * 统一社会信用代码
     */
    @NotEmptyTrim(message = "统一社会信用代码不能为空")
    @LengthTrim(min=18, max = 18,message = "统一社会信用代码长度只能为10位")
    private String socialNo;

    /**
     * 海关注册编码
     */
    @LengthTrim(min=10, max = 10,message = "海关注册编码长度只能为18位")
    private String customsCode;

    /**
     * 企业法人
     */
    @LengthTrim(max = 50,message = "企业法人最大长度只能为50位")
    private String legalPerson;

    /**
     * 联系人
     */
    @LengthTrim(max = 50,message = "联系人名称最大长度不能超过64位")
    private String linkMan;

    /**
     * 联系电话
     */
    @LengthTrim(max = 64,message = "联系人电话最大长度不能超过64位")
    private String linkPhone;

    /**
     * 公司地址
     */
    @LengthTrim(max = 255,message = "公司地址最大长度不能超过255位")
    private String address;

    /**
     * 开通租户服务版本
     */
    @NotNull(message = "开通服务版本不能为空")
    private String openVersion;

}
