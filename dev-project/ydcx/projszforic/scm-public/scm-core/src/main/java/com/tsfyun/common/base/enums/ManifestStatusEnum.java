package com.tsfyun.common.base.enums;

import org.apache.commons.collections4.map.LinkedMap;

import java.util.*;

/**=
 * 舱单状态
 */
public enum ManifestStatusEnum {
    TEMPORARY("temporary","暂存"),
    PROCESSING("processing","处理中"),
    FAILED("failed","处理失败"),
    SAVED("saved","保存"),
    DECLARED("declared","已申报"),
    CHARGEBACK("chargeback","海关退单"),
    UNDERREVIEW("underReview","海关审核中"),
    REMOVE("remove","海关接受删除")
    ;

    private String code;
    private String name;

    ManifestStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ManifestStatusEnum of(String code) {
        return Arrays.stream(ManifestStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
