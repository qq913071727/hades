package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**=
 * 报关类型（长度20位以内）
 */
public enum DeclareTypeEnum {
    SINGLE("single", "单抬头"),
    DOUBLE("double", "双抬头")
    ;
    private String code;
    private String name;

    DeclareTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static DeclareTypeEnum of(String code) {
        return Arrays.stream(DeclareTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}