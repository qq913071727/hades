package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 微信公众号点击菜单事件key定义
 * @CreateDate: Created in 2020/12/29 14:56
 */
public enum WxMenuClickKeyEnum {

    XG_GYWM001_introduce("XG_GYWM001_introduce", "企业简介"),
    XG_GYWM001_link("XG_GYWM001_link", "关于我们"),
    ;
    private String code;
    private String name;

    WxMenuClickKeyEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WxMenuClickKeyEnum of(String code) {
        return Arrays.stream(WxMenuClickKeyEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
