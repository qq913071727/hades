package com.tsfyun.common.base.enums.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 单据操作定义，注意操作码不要超过20位
 * @since Created in 2020/2/25 15:46
 */
public enum DomainOprationEnum {
    //##########################################进口进口进口进口进口进口########################################
    //协议模块
    AREEMENT_ADD("agreement_add", "新增进口协议"),
    AGREEMENT_EDIT("agreement_edit", "进口协议修改"),
    AGREEMENT_EXAMINE("agreement_examine", "进口协议审核"),
    AGREEMENT_SIGN("agreement_sign", "进口协议签署"),
    AGREEMENT_RETURN("agreement_return", "进口协议原件签回"),
    AGREEMENT_INVALID("agreement_invalid", "进口作废协议"),
    AGREEMENT_DELETE("agreement_delete", "删除进口协议"),
    //物料模块
    MATERIEL_CLASSIFY("materiel_classify", "进口物料归类"),
    MATERIEL_CLASSIFY_EXP("materiel_classify_exp", "出口物料归类"),

    //进口订单模块
    ORDER_ADD("order_add","新增进口订单"),
    ORDER_EDIT("order_edit","修改进口订单"),
    ORDER_EDIT_DISTRIBUTION("order_edit_distribution","修改国内配送"),
    ORDER_REMOVE("order_remove","删除进口订单"),
    ORDER_PRICING("order_pricing","进口订单审价"),
    ORDER_EXAMINE("order_examine","进口订单审核"),
    ORDER_INSPECTION("order_inspection","进口订单验货确认"),
    ORDER_SPLIT("order_split","进口订单拆单"),
    ORDER_CONFIRMIMP("order_confirm_imp","进口订单进口确认"),
    ORDER_CREATE_DECL("order_create_decl","待生成报关单"),
    ORDER_CONFIRMIMP_BACK("confirm_imp_back","确认进口退回"),
    ORDER_COMPLETE_RISK("order_complete_risk","完成风控审核"),
    ORDER_COMPLETE_DEC("order_complete_dec","订单报关完成"),
    ORDER_BY_SC("order_by_sc","根据订单生产销售合同"),
    ORDER_BY_INV("order_by_inv","根据订单做发票申请"),
    ORDER_ADJUST_COST("order_adjust_cost","订单调整费用"),

    //入库模块
    RECEIVING_ADD("receiving_add","新增入库"),


    //订单审价
    ORDER_PRICE_EXAMINE("order_price_examine","进口订单审价"),
    EXP_ORDER_PRICE_EXAMINE("exp_order_price_examine","出口订单审价"),

    //收款模块
    RECEIPT_REGISTER("receipt_register","收款登记"),
    RECEIPT_CONFIRM("receipt_confirm","收款确认"),
    RECEIPT_CANCLE("receipt_cancle","收款作废"),
    RECEIPT_REMOVE("receipt_remove","收款删除"),


    //跨境运输(进口)
    CROSS_BORDER_WAYBILL_ADD("waybill_add","新增进口跨境运输单"),
    CROSS_BORDER_WAYBILL_EDIT("waybill_edit","修改进口跨境运输单"),
    CROSS_BORDER_WAYBILL_DELETE("waybill_delete","删除进口跨境运输单"),
    CROSS_BORDER_WAYBILL_CONFIRM("waybill_confirm","确认进口跨境运输单"),
    CROSS_BORDER_WAYBILL_BIND("waybill_bind","进口跨境运输单绑单"),
    CROSS_BORDER_WAYBILL_BACK("waybill_back","进口跨境运输单退回"),
    CROSS_BORDER_WAYBILL_REVIRE("waybill_review","复核进口跨境运输"),
    CROSS_BORDER_WAYBILL_DEPART("waybill_depart","进口跨境运输发车"),
    CROSS_BORDER_WAYBILL_SIGN("waybill_sign","进口跨境运输签收"),

    //跨境运输(出口)
    EXP_CROSS_BORDER_WAYBILL_ADD("exp_waybill_add","新增出口跨境运输单"),
    EXP_CROSS_BORDER_WAYBILL_EDIT("exp_waybill_edit","修改出口跨境运输单"),
    EXP_CROSS_BORDER_WAYBILL_DELETE("exp_waybill_delete","删除出口跨境运输单"),
    EXP_CROSS_BORDER_WAYBILL_CONFIRM("exp_waybill_confirm","确认出口跨境运输单"),
    EXP_CROSS_BORDER_WAYBILL_BIND("exp_waybill_bind","出口跨境运输单绑单"),
    EXP_CROSS_BORDER_WAYBILL_BACK("exp_waybill_back","出口跨境运输单退回"),
    EXP_CROSS_BORDER_WAYBILL_REVIRE("exp_waybill_review","复核出口跨境运输"),
    EXP_CROSS_BORDER_WAYBILL_DEPART("exp_waybill_depart","出口跨境运输发车"),
    EXP_CROSS_BORDER_WAYBILL_SIGN("exp_waybill_sign","出口跨境运输签收"),

    //国内送货
    DELIVERY_WAYBILL_ADD("delivery_way_add","新增国内送货"),
    DELIVERY_WAYBILL_CONFIRM("delivery_way_confirm","确认国内送货"),
    DELIVERY_WAYBILL_DEPART("delivery_way_depart","国内送货发车确认"),
    DELIVERY_WAYBILL_SIGN("delivery_way_sign","国内送货签收确认"),

    //报关单（进口）
    DECLARATION_ADD("dec_add","新增报关单"),
    DECLARATION_CONFIRM("dec_confirm","确认报关单"),
    DECLARATION_DECLARE("dec_declare","确认申报"),
    DECLARATION_SEND_SINGLE("dec_send_single","发送单一窗口"),
    DECLARATION_SEND_MANIFEST("dec_send_manifest","舱单发送/删除"),
    DECLARATION_SEND_SCMBOT("dec_send_scmbot","发送至xxxx申报"),
    DECLARATION_COMPLETED("dec_completed","报关单申报完成"),

    //报关单（出口）
    EXP_DECLARATION_ADD("exp_dec_add","新增报关单"),
    EXP_DECLARATION_CONFIRM("exp_dec_confirm","确认报关单"),
    EXP_DECLARATION_DECLARE("exp_dec_declare","确认申报"),
    EXP_DECLARATION_SEND_SINGLE("exp_dec_send_single","发送单一窗口"),
    EXP_DECLARATION_SEND_MANIFEST("exp_dec_send_manifest","舱单发送/删除"),
    EXP_DECLARATION_SEND_SCMBOT("exp_dec_send_scmbot","发送至xxxx申报"),
    EXP_DECLARATION_COMPLETED("exp_dec_completed","报关单申报完成"),

    //代理报关订单
    TRUSTORDER_RECEIVING("trustorder_receiving","接单"),
    TRUSTORDER_REMOVE("trustorder_remove","删除订单"),

    //风控审批
    RISK_APPROVAL_AUDIT("risk_audit","风控审批"),


    //付汇单
    PAYMENT_ACCOUNT_LIST("payment_list","进口付汇列表"),
    PAYMENT_ACCOUNT_APPLY("payment_apply","付款申请"),
    PAYMENT_ACCOUNT_AUDIT("payment_audit","商务审核"),
    PAYMENT_ACCOUNT_EDIT("payment_edit","修改付汇单"),
    PAYMENT_ACCOUNT_INVALID("payment_invalid","作废付汇单"),
    PAYMENT_ACCOUNT_REMOVE("payment_remove","删除付汇单"),
    PAYMENT_RATE_ADJUST("rate_adjust","调整汇率"),
    PAYMENT_RATE_BANK_FEE("rate_bank_fee","调整手续费"),
    PAYMENT_WAIT_RISK_AUDIT("payment_risk_audit","风控审批"),
    PAYMENT_WAIT_CONFIRM_BANK("payment_confirm_bank","确定付款行"),
    PAYMENT_CONFIRM_PAY("payment_confirm_pay","确定付汇完成"),

    //发票
    INVOICE_CONFIRM_SALE("inv_confirm_sale","商务开票确认"),
    INVOICE_CONFIRM_CUSTOMER("inv_confirm_customer","客户确定税收编码"),
    INVOICE_CONFIRM_TAX("inv_confirm_tax","财务确定税收编码"),
    INVOICE_DELETE("inv_delete","删除发票"),
    INVOICE_CW_COMPLETE("inv_cw_complete","财务确定开票"),
    INVOICE_CREATE("inv_create","创建开票申请"),

    //销售合同
    SALES_CONTRACT_ORDER_APPLY("contract_order_apply","订单生成销售合同"),

    //客户
    CUSTOMER_ADD("customer_add","新增客户"),
    CUSTOMER_EDIT("customer_edit","修改客户"),
    CUSTOMER_APPROVE("customer_approve","审核客户"),

    //退款申请
    REFUND_ADD("refund_add","退款申请"),
    REFUND_TO_VOID("refund_to_void","作废申请"),
    REFUND_AUDIT("refund_audit","退款审核"),
    REFUND_CONFIRM_BANK("refund_confirm_bank","退款确定付款行"),
    REFUND_REMOVE("refund_remove","删除退款单"),
    REFUND_BANK_RECEIPT("refund_bank_receipt","退款上传水单"),



    //##########################################出口出口出口出口出口出口########################################
    EXP_ORDER_ADD("exp_order_add","新增出口订单"),
    EXP_ORDER_EDIT("exp_order_edit","修改出口订单"),
    EXP_ORDER_REMOVE("exp_order_remove","删除出口订单"),
    EXP_ORDER_PRICING("exp_order_pricing","出口订单审价"),
    EXP_ORDER_EXAMINE("exp_order_examine","出口订单审核"),
    EXP_ORDER_INSPECTION("exp_order_inspection","出口订单验货确认"),
    EXP_ORDER_CONFIRMIMP("order_confirm_exp","出口订单出口确认"),
    EXP_ORDER_CONFIRMIMP_BACK("confirm_exp_back","确认出口退回"),
    EXP_ORDER_COMPLETE_DEC("exp_order_complete_dec","订单报关完成"),
    EXP_ORDER_ADJUST_COST("exp_order_adjust_cost","订单调整费用"),
    CREATE_PURCHASE_CONTRACT("create_purchase_contract","做采购合同"),
    // 采购合同
    PURCHASE_CONTRACT_ADD("purchase_contract_add","创建采购合同"),
    PURCHASE_CONTRACT_CONFIRM("purchase_contract_confirm","采购合同确认"),
    PURCHASE_CONTRACT_INVOICE("purchase_contract_invoice","采购合同收票"),
    PURCHASE_CONTRACT_REMOVE("purchase_contract_remove","采购合同删除"),
    PURCHASE_CONTRACT_DRAWNACK("purchase_contract_drawback","退税申请"),
    PURCHASE_TAX_BUREAU("purchase_tax_bureau","税局退税登记"),
    PURCHASE_CONTRACT_SENDMAIL("purchase_sendmail","采购合同发送邮件"),



    //##########################################代理报关########################################
    DECL_QUOTE_ADD("decl_quote_add", "新增报价"),
    DECL_QUOTE_EDIT("decl_quote_edit", "报价修改"),
    DECL_QUOTE_EXAMINE("decl_quote_examine", "报价审核"),
    DECL_QUOTE_INVALID("decl_quote_invalid", "作废报价"),
    DECL_QUOTE_DELETE("decl_quote_delete", "删除报价"),


    //##############################出口#######################
    //协议模块
    EXP_AREEMENT_ADD("exp_agreement_add", "新增出口报价"),
    EXP_AGREEMENT_EDIT("exp_agreement_edit", "出口报价修改"),
    EXP_AGREEMENT_EXAMINE("exp_agreement_examine", "出口报价审核"),
    EXP_AGREEMENT_RETURN("exp_agreement_return", "出口报价原件签回"),
    EXP_AGREEMENT_INVALID("exp_agreement_invalid", "作废出口报价"),
    EXP_AGREEMENT_DELETE("exp_agreement_delete", "删除出口报价"),

    //境外收款
    OVERSEAS_RECEIVING_ACCOUNT_REGISTER("overseas_receiving_account_register","境外收款登记"),
    OVERSEAS_RECEIVING_ACCOUNT_EDIT("overseas_receiving_account_edit","境外收款修改"),
    OVERSEAS_RECEIVING_ACCOUNT_CONFIRM("overseas_receiving_account_confirm","财务确认境外收款"),
    OVERSEAS_RECEIVING_ACCOUNT_CLAIM("overseas_receiving_account_claim","商务认领"),
    OVERSEAS_RECEIVING_ACCOUNT_CANCEL("overseas_receiving_account_cancel","作废境外收款"),
    OVERSEAS_RECEIVING_ACCOUNT_REMOVE("overseas_receiving_account_remove","删除境外收款"),
    OVERSEAS_RECEIVING_ACCOUNT_SETTLEMENT("overseas_receiving_account_settlement","结汇申请"),
    OVERSEAS_RECEIVING_ACCOUNT_PAYMENT("overseas_receiving_account_payment","付款申请"),

    // 出口付款
    EXP_PAYMENT_ACCOUNT_CREATE("exp_payment_account_create","创建付款申请"),
    EXP_PAYMENT_ACCOUNT_CONFIRM("exp_payment_account_confirm","商务确定付款单"),
    EXP_PAYMENT_ACCOUNT_BANK("exp_payment_account_bank","出纳确定付款行"),
    EXP_PAYMENT_ACCOUNT_COMPLETED("exp_payment_account_completed","财务确定付款完成"),
    EXP_PAYMENT_ACCOUNT_REMOVE("exp_payment_account_remove","删除付款单"),

    // 出口退税
    DRAWBACK_CREATE("drawback_create","创建退税申请"),
    DRAWBACK_CONFIRM("drawback_confirm","商务确定退税单"),
    DRAWBACK_BANK("drawback_bank","出纳确定付款行"),
    DRAWBACK_COMPLETED("drawback_completed","财务确定付款完成"),
    DRAWBACK_REMOVE("drawback_remove","删除退税单"),

    //出口境外派送
    EXP_OVERSEAS_DELIVERY_NOTE_EDIT("overseas_delivery_note_edit","出口境外派送修改"),
    EXP_OVERSEAS_DELIVERY_NOTE_CONFIRM("overseas_delivery_note_confirm","出口境外派送确认"),
    EXP_OVERSEAS_DELIVERY_NOTE_DISTRIBUTION("overseas_delivery_note_distribution","出口境外派送发车派送"),
    EXP_OVERSEAS_DELIVERY_NOTE_SIGN("overseas_delivery_note_sign","出口境外派送完成"),
    EXP_OVERSEAS_DELIVERY_NOTE_DELETE("overseas_delivery_note_delete","出口境外派送单删除"),

    ;
    private String code;
    private String name;

    DomainOprationEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static DomainOprationEnum of(String code) {
        return Arrays.stream(DomainOprationEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
