package com.tsfyun.common.base.enums.exp;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 出口业务类型
 * @CreateDate: Created in 2021/9/10 15:00
 */
public enum ExpBusinessTypeEnum {

    EXP_AGENT("exp_agent", "代理出口"),
    EXP_COOPERATE("exp_cooperate", "自营出口")
    ;
    private String code;
    private String name;

    ExpBusinessTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ExpBusinessTypeEnum of(String code) {
        return Arrays.stream(ExpBusinessTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
