package com.tsfyun.common.base.security;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/1/7 15:50
 */
@Data
public class SysRoleVO implements Serializable {

    /**
     * id手工生成
     */
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 是否禁用
     */
    private Boolean disabled;

    /**
     * 是否锁定
     */
    private Boolean locking;

    /**=
     * 备注
     */
    private String memo;

}
