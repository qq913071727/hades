package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 微信公众号事菜单类型
 * @CreateDate: Created in 2020/12/29 14:56
 */
public enum WxMenuEnum {

    VIEW("view", "网页"),
    CLICK("click", "点击"),
    MINIPROGRAM("miniprogram", "小程序"),
    ;
    private String code;
    private String name;

    WxMenuEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static WxMenuEnum of(String code) {
        return Arrays.stream(WxMenuEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
