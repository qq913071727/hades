package com.tsfyun.common.base.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 性别类型
 * @since Created in 2020/2/22 15:46
 */
public enum SexTypeEnum {


    MAN("man", "男"),
    FEMALE("femal", "女"),
    UNKNOW("unknow", "保密"),
    ;
    private String code;
    private String name;

    SexTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static SexTypeEnum of(String code) {
        return Arrays.stream(SexTypeEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    /**
     * 根据描述获取枚举
     *
     * @param name
     * @return
     */
    public static SexTypeEnum ofName(String name) {
        return Arrays.stream(SexTypeEnum.values()).filter(r -> Objects.equals(r.getName(), name)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
