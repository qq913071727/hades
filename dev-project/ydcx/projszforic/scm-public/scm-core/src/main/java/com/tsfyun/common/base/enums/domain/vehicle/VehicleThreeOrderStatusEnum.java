package com.tsfyun.common.base.enums.domain.vehicle;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

/**
 * 三单状态（订单、收款单，运单）
 */
public enum VehicleThreeOrderStatusEnum implements OperationEnum {
    EXCEPTION("-1","处理异常"),
    TEMP("1","已暂存"),
    DECLARING("2","申报中"),
    SEND_CUSTOMS_SUCCESS("3","发送海关成功"),
    SEND_CUSTOMS_FAIL("4","发送海关失败"),
    BACK("100","海关退单"),
    STORAGE("120","海关入库"),
    ;
    private String code;

    private String name;

    VehicleThreeOrderStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static VehicleThreeOrderStatusEnum of(String code) {
        return Arrays.stream(VehicleThreeOrderStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

