package com.tsfyun.common.base.enums.domain;

import com.tsfyun.common.base.support.OperationEnum;

import java.util.Arrays;
import java.util.Objects;

public enum DrawbackStatusEnum implements OperationEnum {

    WAIT_CONFIRM("waitConfirm", "待商务确定"),
    CONFIRM_BANK("confirmBank", "出纳确定付款行"),
    WAIT_PAY("waitPay", "待付款"),
    COMPLETED("completed", "已完成")
    ;
    private String code;

    private String name;

    DrawbackStatusEnum(String code, String name){
        this.code = code;
        this.name = name;
    }

    public static DrawbackStatusEnum of(String code) {
        return Arrays.stream(DrawbackStatusEnum.values()).filter(r -> Objects.equals(r.getCode(), code)).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
