package com.tsfyun.common.base.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/29 18:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenApiAccessVO implements Serializable {

    private String accessToken;

    private long expiresIn;
}
