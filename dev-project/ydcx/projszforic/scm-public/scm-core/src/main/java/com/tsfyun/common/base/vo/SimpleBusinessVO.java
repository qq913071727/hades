package com.tsfyun.common.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value="SimpleBusiness响应对象", description="响应实体")
public class SimpleBusinessVO implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "公司名称")
    private String name;

    @ApiModelProperty(value = "统一社会信用代码")
    private String creditCode;

    @ApiModelProperty(value = "法人")
    private String operName;

    @ApiModelProperty(value = "营业执照号")
    private String no;

    @ApiModelProperty(value = "公司状态")
    private String status;

    @ApiModelProperty(value = "注册地址")
    private String address;

//    @ApiModelProperty(value = "成立日期")
//    private Date startDate;


}
