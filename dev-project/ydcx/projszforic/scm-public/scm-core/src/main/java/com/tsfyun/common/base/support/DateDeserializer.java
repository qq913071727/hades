package com.tsfyun.common.base.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Date;

/**
 * @Description:
 * @since Created in 2020/4/1 15:56
 */
@Slf4j
public class DateDeserializer extends JsonDeserializer<Date> {

    public static final DateDeserializer instance = new DateDeserializer();

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String val = jsonParser.getText();
        try {
            return LocalDateTimeUtils.convertByStringWithLength(val);
        } catch (Exception e) {
            log.error(String.format("json字符串：%s参数转日期异常",val),e);
            throw new ServiceException("日期格式错误");
        }
    }

}
