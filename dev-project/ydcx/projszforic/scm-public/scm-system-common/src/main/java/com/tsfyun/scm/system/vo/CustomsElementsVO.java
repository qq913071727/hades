package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="CustomsElements响应对象", description="响应实体")
public class CustomsElementsVO implements Serializable {

     private static final long serialVersionUID=1L;

    private String hsCode;

    private Boolean isNeed;

    private String name;

    private Integer sort;

    private String val;


}
