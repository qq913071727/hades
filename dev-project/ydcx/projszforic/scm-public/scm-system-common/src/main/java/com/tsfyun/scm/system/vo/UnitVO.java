package com.tsfyun.scm.system.vo;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="Unit响应对象", description="响应实体")
public class UnitVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "单位名称")
    private String name;

    @ApiModelProperty(value = "对应统计计量单位代码")
    private String sid;

    @ApiModelProperty(value = "转换率")
    private BigDecimal convertRate;

    private Integer sort;


}
