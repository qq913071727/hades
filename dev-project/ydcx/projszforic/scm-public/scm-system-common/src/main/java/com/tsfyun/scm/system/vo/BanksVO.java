package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="Banks响应对象", description="响应实体")
public class BanksVO implements Serializable {

     private static final long serialVersionUID=1L;

    private String name;


}
