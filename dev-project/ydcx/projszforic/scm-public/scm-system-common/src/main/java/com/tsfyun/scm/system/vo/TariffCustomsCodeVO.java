package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2020/12/23 15:22
 */
@Data
public class TariffCustomsCodeVO implements Serializable {

    private String id;

   private BigDecimal mfntr;

}
