package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomerDataS1VO implements Serializable {
    private String i;
    //别名简写 考虑到内存存储
    private String c;//海关编码
    private String n;//编码名称
}
