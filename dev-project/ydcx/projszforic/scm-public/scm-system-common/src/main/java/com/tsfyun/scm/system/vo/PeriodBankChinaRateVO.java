package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @CreateDate: Created in 2020/6/1 12:09
 */
@Data
public class PeriodBankChinaRateVO implements Serializable {

    /**
     * 汇率日期
     */
    private String date;

    /**
     * 汇率时间
     */
    private String time;

    /**
     * 币制
     */
    private String currencyId;

    /**
     * 币制名称
     */
    private String currencyName;

    /**
     * 币制日期
     */
    private String currencyDate;

    /**
     * 现汇买入价
     */

    private BigDecimal buyingRate;

    /**
     * 现钞买入价
     */

    private BigDecimal cashPurchase;

    /**
     * 现汇卖出价
     */

    private BigDecimal spotSelling;

    /**
     * 现钞卖出价
     */

    private BigDecimal cashSelling;

    /**
     * 中行折算价
     */

    private BigDecimal bocConversion;

    private Integer rowNumber;

}
