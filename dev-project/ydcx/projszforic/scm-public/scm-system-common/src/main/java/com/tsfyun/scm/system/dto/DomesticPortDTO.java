package com.tsfyun.scm.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 国内港口请求实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="DomesticPort请求对象", description="国内港口请求实体")
public class DomesticPortDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private String code;

   private String name;

   private String nameEn;


}
