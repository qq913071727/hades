package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 国内港口响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="DomesticPort响应对象", description="国内港口响应实体")
public class DomesticPortVO implements Serializable {

     private static final long serialVersionUID=1L;

    private String code;

    private String name;

    private String nameEn;


}
