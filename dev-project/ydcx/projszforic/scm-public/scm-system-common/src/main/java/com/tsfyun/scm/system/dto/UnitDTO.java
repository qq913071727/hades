package com.tsfyun.scm.system.dto;

import java.math.BigDecimal;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="Unit请求对象", description="请求实体")
public class UnitDTO  extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String id;

   @NotEmptyTrim(message = "单位名称不能为空")
   @LengthTrim(max = 10,message = "单位名称最大长度不能超过10位")
   @ApiModelProperty(value = "单位名称")
   private String name;

   @LengthTrim(max = 10,message = "对应统计计量单位代码最大长度不能超过10位")
   @ApiModelProperty(value = "对应统计计量单位代码")
   private String sid;

   @NotNull(message = "转换率不能为空")
   @Digits(integer = 13, fraction = 6, message = "转换率整数位不能超过13位，小数位不能超过6")
   @ApiModelProperty(value = "转换率")
   private BigDecimal convertRate;

   private Integer sort;


}
