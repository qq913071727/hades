package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="CiqCodes请求对象", description="请求实体")
public class CiqCodesDTO  extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String id;

   @NotEmptyTrim(message = "商检编码名称不能为空")
   @LengthTrim(max = 255,message = "商检编码名称最大长度不能超过255位")
   @ApiModelProperty(value = "商检编码名称")
   private String name;

   @NotEmptyTrim(message = "海关编码不能为空")
   @LengthTrim(max = 10,message = "海关编码最大长度不能超过10位")
   @ApiModelProperty(value = "海关编码")
   private String hsCode;

   @NotEmptyTrim(message = "商检编码不能为空")
   @LengthTrim(max = 3,message = "商检编码最大长度不能超过3位")
   @ApiModelProperty(value = "商检编码")
   private String code;

   @NotEmptyTrim(message = "类型名称不能为空")
   @LengthTrim(max = 70,message = "类型名称最大长度不能超过70位")
   @ApiModelProperty(value = "类型名称")
   private String typeName;

   @NotNull(message = "是否有效不能为空")
   @ApiModelProperty(value = "是否有效")
   private Boolean effective;

   //海关编码，名称
   private String keyword;


}
