package com.tsfyun.scm.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 国内地区请求实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="DomesticDistrict请求对象", description="国内地区请求实体")
public class DomesticDistrictDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "编码不能为空")
   @LengthTrim(max = 100,message = "编码最大长度不能超过100位")
   @ApiModelProperty(value = "编码")
   private String code;

   @LengthTrim(max = 255,message = "中文名称最大长度不能超过255位")
   @ApiModelProperty(value = "中文名称")
   private String name;

   @LengthTrim(max = 100,message = "英文名称最大长度不能超过100位")
   @ApiModelProperty(value = "英文名称")
   private String shortName;

   @NotNull(message = "是否禁用，1不能为空")
   @ApiModelProperty(value = "是否禁用，1-是;0-否")
   private Boolean disabled;

   @LengthTrim(max = 2,message = "国内地区性质标记最大长度不能超过2位")
   @ApiModelProperty(value = "国内地区性质标记")
   private String propertyFlag;


}
