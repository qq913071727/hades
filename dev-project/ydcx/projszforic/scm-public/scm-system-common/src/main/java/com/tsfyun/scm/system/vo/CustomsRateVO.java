package com.tsfyun.scm.system.vo;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class CustomsRateVO implements Serializable {

    private String id;

    /**
     * 币制ID
     */
    private String currencyId;

    /**
     * 币制名称
     */
    private String currencyName;

    /**
     * 公布时间
     */
    private LocalDateTime publishDate;

    /**
     * 汇率日期
     */
    private Date rateDate;


    /**
     * 汇率
     */
    private BigDecimal rate;

}
