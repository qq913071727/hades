package com.tsfyun.scm.system.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SelectCustomsCodeVO implements Serializable {

    private String c;
    private String n;
}
