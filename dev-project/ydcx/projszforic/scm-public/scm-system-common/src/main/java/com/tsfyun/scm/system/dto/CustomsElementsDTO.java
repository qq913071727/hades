package com.tsfyun.scm.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="CustomsElements请求对象", description="请求实体")
public class CustomsElementsDTO implements Serializable {

   private static final long serialVersionUID=1L;

   private String id;

   private String hsCode;

   private Boolean isNeed;

   private String name;

   private Integer sort;


}
