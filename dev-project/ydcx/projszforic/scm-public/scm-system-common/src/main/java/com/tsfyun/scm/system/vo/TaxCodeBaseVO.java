package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 税收分类编码响应实体
 * </p>
 *

 * @since 2020-05-15
 */
@Data
@ApiModel(value="TaxCodeBase响应对象", description="税收分类编码响应实体")
public class TaxCodeBaseVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "货物和劳务名称")
    private String name;

    @ApiModelProperty(value = "商品和服务分类简称")
    private String shortName;

    private String memo;


}
