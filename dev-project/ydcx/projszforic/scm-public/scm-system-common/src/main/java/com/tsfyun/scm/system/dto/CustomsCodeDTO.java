package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @CreateDate: Created in 2021/9/14 16:13
 */
@Data
public class CustomsCodeDTO implements Serializable {

    @NotEmptyTrim(message = "海关编码不能为空")
    @LengthTrim(min = 10,max = 10,message = "海关编码必须为10位")
    private String id;

    @NotEmptyTrim(message = "原海关编码不能为空",groups = UpdateGroup.class)
    private String preId;

    /**
     * 消费税率
     */
    @Digits(integer = 15, fraction = 4, message = "消费税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "消费税率不能小于0")
    private BigDecimal conrate;

    /**
     * 监管条件
     */
    @LengthTrim(max = 20,message = "监管条件长度不能超过20位")
    private String csc;

    /**
     * 出口税率
     */
    @Digits(integer = 15, fraction = 4, message = "出口税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "出口税率不能小于0")
    private BigDecimal exportRate;

    /**
     * 普通国汇率
     */
    @Digits(integer = 15, fraction = 4, message = "普通国汇率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "普通国汇率不能小于0")
    private BigDecimal gtr;

    /**
     * 检验检疫监管条件
     */
    @LengthTrim(max = 20,message = "检验检疫监管条件长度不能超过20位")
    private String iaqr;

    /**
     * 最惠国税率
     */
    @Digits(integer = 15, fraction = 4, message = "最惠国税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "最惠国税率不能小于0")
    private BigDecimal mfntr;

    /**
     * 海关编码名称
     */
    @NotEmptyTrim(message = "海关编码名称不能为空")
    @LengthTrim(max = 255,message = "海关编码名称长度不能超过255位")
    private String name;

    /**
     * 法二单位编码
     */
    @LengthTrim(message = "法二单位编码长度不能超过10位")
    private String sunitCode;

    /**
     * 暂定出口税率
     */
    @Digits(integer = 15, fraction = 4, message = "暂定出口税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "暂定出口税率不能小于0")
    private BigDecimal ter;

    /**
     * 暂定进口税率
     */
    @Digits(integer = 15, fraction = 4, message = "暂定进口税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "暂定进口税率不能小于0")
    private BigDecimal tit;

    /**
     * 出口退税率
     */
    @Digits(integer = 15, fraction = 4, message = "出口退税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "出口退税率不能小于0")
    private String trr;

    /**
     * 法一单位编码
     */
    @NotEmptyTrim(message = "法一单位编码不能为空")
    @LengthTrim(max = 10,message = "法一单位编码长度不能超过10位")
    private String unitCode;

    /**
     * 增值税率
     */
    @NotNull(message = "增值税率不能为空")
    @Digits(integer = 15, fraction = 4, message = "增值税率整数位不能超过15位，小数位不能超过4")
    @DecimalMin(value = "0.0000",message = "增值税率不能小于0")
    private BigDecimal vatr;

    /**
     * 法二单位名称
     */
    //@LengthTrim(max = 10,message = "法二单位名称长度不能超过10位")
    private String sunitName;

    /**
     * 法一单位名称
     */
    //@NotEmptyTrim(message = "法一单位名称不能为空")
    @LengthTrim(max = 10,message = "法一单位名称长度不能超过10位")
    private String unitName;

    /**
     * 备注
     */
    @LengthTrim(max = 255,message = "备注长度不能超过255位")
    private String memo;

    //申报要素
    @Size(min = 1,message = "请至少添加一个海关编码要素")
    List<String> elementsName;

    //是否必须
    List<String> isNeed;
}
