package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.PipedReader;
import java.io.Serializable;

/**
 * @Description:
 * @CreateDate: Created in 2020/5/31 16:18
 */
@Data
public class RateSimpleVO implements Serializable {

    private String currencyCode;

    private String currencyName;

    private String rate;

}
