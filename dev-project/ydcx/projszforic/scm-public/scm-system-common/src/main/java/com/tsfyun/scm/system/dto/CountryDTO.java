package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="Country请求对象", description="请求实体")
public class CountryDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String id;

   private Boolean disabled;

   private Boolean disinfectCheck;

   private Boolean isDiscount;

   private String name;

   private String namee;

   private Integer sort;

   //编码或者中文名称
   private String keyword;


}
