package com.tsfyun.scm.system.vo;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="LevyDuties响应对象", description="响应实体")
public class LevyDutiesVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "国家代码")
    private String countryCode;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    @ApiModelProperty(value = "是否启用")
    private Boolean isStart;

    @ApiModelProperty(value = "加征比例")
    private BigDecimal levyVal;


}
