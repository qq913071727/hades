package com.tsfyun.scm.system.vo;

import lombok.Data;

@Data
public class CustomerDataS2VO extends CustomerDataS1VO {
    ////别名简写 考虑到内存存储
    private String o;//原海关编码
    private String q;//商检编码
}
