package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 基础数据配置响应实体
 * </p>
 *

 * @since 2020-04-08
 */
@Data
@ApiModel(value="BaseData响应对象", description="基础数据配置响应实体")
public class BaseDataVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "基础数据类型")
    private String baseDataType;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "上级编码")
    private String pcode;

    @ApiModelProperty(value = "报关编码")
    private String decCode;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "是否禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "行号")
    private Integer rowNo;


}
