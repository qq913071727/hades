package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 海关基础数据响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="CustomsData响应对象", description="海关基础数据响应实体")
public class CustomsDataVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "类别")
    private String category;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "中文名称")
    private String name;

    @ApiModelProperty(value = "简称")
    private String shortName;

    @ApiModelProperty(value = "是否禁用，1-是;0-否")
    private Boolean disabled;


}
