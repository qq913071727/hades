package com.tsfyun.scm.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 税收分类编码请求实体
 * </p>
 *

 * @since 2020-05-15
 */
@Data
@ApiModel(value="TaxCodeBase请求对象", description="税收分类编码请求实体")
public class TaxCodeBaseDTO implements Serializable {

   private static final long serialVersionUID=1L;

   @LengthTrim(max = 64,message = "货物和劳务名称最大长度不能超过64位")
   @ApiModelProperty(value = "货物和劳务名称")
   private String name;

   @NotEmptyTrim(message = "商品和服务分类简称不能为空")
   @LengthTrim(max = 64,message = "商品和服务分类简称最大长度不能超过64位")
   @ApiModelProperty(value = "商品和服务分类简称")
   private String shortName;

   private String memo;


}
