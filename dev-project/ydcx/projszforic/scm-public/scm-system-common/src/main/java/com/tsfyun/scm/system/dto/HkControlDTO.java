package com.tsfyun.scm.system.dto;

import java.time.LocalDateTime;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="HkControl请求对象", description="请求实体")
public class HkControlDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "品牌不能为空")
   @LengthTrim(max = 100,message = "品牌最大长度不能超过100位")
   @ApiModelProperty(value = "品牌")
   private String brand;

   @LengthTrim(max = 255,message = "产品说明描述最大长度不能超过255位")
   @ApiModelProperty(value = "产品说明描述")
   private String description;

   @NotNull(message = "是否管制不能为空")
   @ApiModelProperty(value = "是否管制")
   private Boolean isControl;

   @NotEmptyTrim(message = "型号不能为空")
   @LengthTrim(max = 200,message = "型号最大长度不能超过200位")
   @ApiModelProperty(value = "型号")
   private String model;

   @NotNull(message = "同步时间不能为空")
   @ApiModelProperty(value = "同步时间")
   private LocalDateTime synchroDate;

   @LengthTrim(max = 100,message = "管制类别最大长度不能超过100位")
   @ApiModelProperty(value = "管制类别")
   private String type;

   private Integer offset = 0;//分页开始数

   public Integer getOffset(){
      Integer offset = (super.getPage() - 1)*super.getLimit();
      this.offset = (offset>0)?offset-1:offset;
      return this.offset;
   }


}
