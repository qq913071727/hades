package com.tsfyun.scm.system.vo;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="CustomsCode响应对象", description="响应实体")
public class CustomsCodeVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "消费税率")
    private String conrate;

    @ApiModelProperty(value = "监管条件")
    private String csc;

    @ApiModelProperty(value = "是否禁用")
    private Boolean disabled;

    @ApiModelProperty(value = "出口税率")
    private BigDecimal exportRate;

    @ApiModelProperty(value = "普通国汇率")
    private BigDecimal gtr;

    @ApiModelProperty(value = "检验检疫监管条件")
    private String iaqr;

    @ApiModelProperty(value = "最惠国税率")
    private BigDecimal mfntr;

    @ApiModelProperty(value = "海关编码名称")
    private String name;

    @ApiModelProperty(value = "法二单位编码")
    private String sunitCode;

    @ApiModelProperty(value = "临时出口税率")
    private BigDecimal ter;

    @ApiModelProperty(value = "进口暂定税率")
    private BigDecimal tit;

    @ApiModelProperty(value = "出口退税率")
    private String trr;

    @ApiModelProperty(value = "法一单位编码")
    private String unitCode;

    @ApiModelProperty(value = "增值税率")
    private BigDecimal vatr;

    @ApiModelProperty(value = "对美加征关税")
    private BigDecimal levyTax;

    @ApiModelProperty(value = "法二单位名称")
    private String sunitName;

    @ApiModelProperty(value = "法一单位名称")
    private String unitName;

    @ApiModelProperty(value = "备注")
    private String memo;





}
