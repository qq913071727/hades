package com.tsfyun.scm.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="CiqCodes响应对象", description="响应实体")
public class CiqCodesVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "商检编码名称")
    private String name;

    @ApiModelProperty(value = "海关编码")
    private String hsCode;

    @ApiModelProperty(value = "商检编码")
    private String code;

    @ApiModelProperty(value = "类型名称")
    private String typeName;

    @ApiModelProperty(value = "是否有效")
    private Boolean effective;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime dateCreated;

}
