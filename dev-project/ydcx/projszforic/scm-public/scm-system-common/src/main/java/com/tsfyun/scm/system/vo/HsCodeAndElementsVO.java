package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class HsCodeAndElementsVO implements Serializable {

    private HsCodeSimpleVO hsCode;
    private List<CustomsElementsVO> elements;
    private List<CiqCodeSimpleVo> ciqCodes;
}
