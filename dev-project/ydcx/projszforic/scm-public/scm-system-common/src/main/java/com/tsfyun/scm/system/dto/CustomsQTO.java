package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

/**
 * @CreateDate: Created in 2021/9/14 16:36
 */
@Data
public class CustomsQTO extends PaginationDto {

    private String id;

    private String name;

}
