package com.tsfyun.scm.system.dto;

import java.math.BigDecimal;

import com.tsfyun.common.base.dto.PaginationDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

import javax.validation.constraints.NotNull;
import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import javax.validation.constraints.Digits;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="LevyDuties请求对象", description="请求实体")
public class LevyDutiesDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   @NotEmptyTrim(message = "国家代码不能为空")
   @LengthTrim(max = 10,message = "国家代码最大长度不能超过10位")
   @ApiModelProperty(value = "国家代码")
   private String countryCode;

   @NotEmptyTrim(message = "国家名称不能为空")
   @LengthTrim(max = 30,message = "国家名称最大长度不能超过30位")
   @ApiModelProperty(value = "国家名称")
   private String countryName;

   @NotEmptyTrim(message = "海关编码不能为空")
   @LengthTrim(max = 10,message = "海关编码最大长度不能超过10位")
   @ApiModelProperty(value = "海关编码")
   private String hsCode;

   @NotNull(message = "是否启用不能为空")
   @ApiModelProperty(value = "是否启用")
   private Boolean isStart;

   @NotNull(message = "加征比例不能为空")
   @Digits(integer = 17, fraction = 2, message = "加征比例整数位不能超过17位，小数位不能超过2")
   @ApiModelProperty(value = "加征比例")
   private BigDecimal levyVal;


}
