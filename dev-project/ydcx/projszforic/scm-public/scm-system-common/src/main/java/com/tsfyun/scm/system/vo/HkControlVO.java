package com.tsfyun.scm.system.vo;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="HkControl响应对象", description="响应实体")
public class HkControlVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "产品说明描述")
    private String description;

    @ApiModelProperty(value = "是否管制")
    private Boolean isControl;

    @ApiModelProperty(value = "型号")
    private String model;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private LocalDateTime synchroDate;

    @ApiModelProperty(value = "管制类别")
    private String type;


}
