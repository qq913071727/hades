package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 请求实体
 * </p>
 *

 * @since 2020-03-17
 */
@Data
@ApiModel(value="BankChinaRate请求对象", description="请求实体")
public class BankChinaRateDTO extends PaginationDto implements Serializable {

   private static final long serialVersionUID=1L;

   private String id;

   @NotEmptyTrim(message = "原数据id不能为空",groups = UpdateGroup.class)
   private String preId;

   @NotEmptyTrim(message = "币制ID不能为空")
   @LengthTrim(max = 10,message = "币制ID最大长度不能超过10位")
   @ApiModelProperty(value = "币制ID")
   private String currencyId;

   //@NotEmptyTrim(message = "币制名称不能为空")
   //@LengthTrim(max = 20,message = "币制名称最大长度不能超过20位")
   @ApiModelProperty(value = "币制名称")
   private String currencyName;

   @NotNull(message = "公布时间不能为空")
   @ApiModelProperty(value = "公布时间")
   private Date publishDate;

   @Digits(integer = 15, fraction = 4, message = "现汇买入价整数位不能超过15位，小数位不能超过4")
   @ApiModelProperty(value = "现汇买入价")
   private BigDecimal buyingRate;

   @NotNull(message = "现钞买入价不能为空")
   @Digits(integer = 15, fraction = 4, message = "现钞买入价整数位不能超过15位，小数位不能超过4")
   @ApiModelProperty(value = "现钞买入价")
   private BigDecimal cashPurchase;

   @Digits(integer = 15, fraction = 4, message = "现汇卖出价整数位不能超过15位，小数位不能超过4")
   @ApiModelProperty(value = "现汇卖出价")
   private BigDecimal spotSelling;

   @NotNull(message = "现钞卖出价不能为空")
   @Digits(integer = 15, fraction = 4, message = "现钞卖出价整数位不能超过15位，小数位不能超过4")
   @ApiModelProperty(value = "现钞卖出价")
   private BigDecimal cashSelling;

   @NotNull(message = "中行折算价不能为空")
   @Digits(integer = 15, fraction = 4, message = "中行折算价整数位不能超过15位，小数位不能超过4")
   @ApiModelProperty(value = "中行折算价")
   private BigDecimal bocConversion;


}
