package com.tsfyun.scm.system.dto;

import lombok.Getter;
import lombok.Setter;

/**=
 * 行政区名称显示
 */
@Setter
@Getter
public class DistrictNameDto {
    //编码
    private String code;
    //名称
    private String name;
}
