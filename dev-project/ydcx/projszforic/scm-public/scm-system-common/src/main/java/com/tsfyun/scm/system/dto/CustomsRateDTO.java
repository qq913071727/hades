package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.annotation.LengthTrim;
import com.tsfyun.common.base.annotation.NotEmptyTrim;
import com.tsfyun.common.base.dto.PaginationDto;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class CustomsRateDTO extends PaginationDto {

    @NotEmptyTrim(message = "数据id不能为空",groups = UpdateGroup.class)
    private String preId;

    //@NotEmptyTrim(message = "数据id不能为空",groups = UpdateGroup.class)
    private String id;

    /**
     * 币制ID
     */
    @NotEmptyTrim(message = "币制ID不能为空")
    @LengthTrim(max = 10,message = "币制ID最大长度不能超过10位")
    private String currencyId;

    /**
     * 币制名称
     */
    //@NotEmptyTrim(message = "币制名称不能为空")
    //@LengthTrim(max = 20,message = "币制名称最大长度不能超过20位")
    private String currencyName;

    /**
     * 公布时间
     */
    @NotNull(message = "公布时间不能为空")
    private Date publishDate;

    /**
     * 汇率日期
     */
    @NotNull(message = "汇率日期不能为空")
    @DateTimeFormat(pattern = "yyyy-MM")
    private Date rateDate;


    /**
     * 汇率
     */
    @NotNull(message = "汇率不能为空")
    @Digits(integer = 4, fraction = 6, message = "汇率整数位不能超过4位，小数位不能超过6")
    private BigDecimal rate;

}
