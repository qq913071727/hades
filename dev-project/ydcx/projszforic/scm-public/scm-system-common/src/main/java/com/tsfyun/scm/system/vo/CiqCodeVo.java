package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**=
 * 检验检疫编码
 */
@Data
public class CiqCodeVo implements Serializable {
    /**
     * 海关编码+商检编码
     */
    private String id;

    /**
     * 商检编码名称
     */
    private String name;

    /**
     * 海关编码
     */
    private String hsCode;

    /**
     * 商检编码
     */
    private String code;

    /**
     * 类型名称
     */
    private String typeName;

    /**
     * 创建时间
     */
    private Date dateCreated;

    /**
     * 是否有效
     */
    private Boolean effective;
}
