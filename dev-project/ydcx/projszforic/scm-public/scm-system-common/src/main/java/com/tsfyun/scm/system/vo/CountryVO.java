package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-19
 */
@Data
@ApiModel(value="Country响应对象", description="响应实体")
public class CountryVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    private Boolean disabled;

    private Boolean disinfectCheck;

    private Boolean isDiscount;

    private String name;

    private String namee;

    private Integer sort;


}
