package com.tsfyun.scm.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 响应实体
 * </p>
 *

 * @since 2020-03-17
 */
@Data
@ApiModel(value="BankChinaRate响应对象", description="响应实体")
public class BankChinaRateVO implements Serializable {

     private static final long serialVersionUID=1L;

     private String id;

    @ApiModelProperty(value = "币制ID")
    private String currencyId;

    @ApiModelProperty(value = "币制名称")
    private String currencyName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "公布时间")
    private LocalDateTime publishDate;

    @ApiModelProperty(value = "现汇买入价")
    private BigDecimal buyingRate;

    @ApiModelProperty(value = "现钞买入价")
    private BigDecimal cashPurchase;

    @ApiModelProperty(value = "现汇卖出价")
    private BigDecimal spotSelling;

    @ApiModelProperty(value = "现钞卖出价")
    private BigDecimal cashSelling;

    @ApiModelProperty(value = "中行折算价")
    private BigDecimal bocConversion;


}
