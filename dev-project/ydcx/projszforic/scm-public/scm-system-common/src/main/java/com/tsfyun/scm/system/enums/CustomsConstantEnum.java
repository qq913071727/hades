package com.tsfyun.scm.system.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Description: 海关常量数据
 * @Project:
 * @CreateDate: Created in 2020-03-20 13:42:00
 */
public enum CustomsConstantEnum {

    //默认值，没有任何意义，不能使用
    CUS_DEFAULT("",""),

    DECL_TRN_REL("DECL_TRN_REL","转关提前报关"),
    TRAN_FLAG("TRAN_FLAG","转关类型"),
    EDIID_TYPE("EDIID_TYPE","报关标志"),
    E_SEAL_FLAG("E_SEAL_FLAG","启用电子关锁标志"),
    BRAND_TYPE("BRAND_TYPE","品牌类型"),
    EXP_DISCONUNT("EXP_DISCONUNT","出口享惠情况"),
    REQ_CONTENT("REQ_CONTENT","证书信息"),
    DEC_TRANS_TYPE("DEC_TRANS_TYPE","转关单申报类型"),

    CUS_MAPPING_TRADE_CODE_V("CUS_MAPPING_TRADE_CODE_V","监管方式"),
    CUS_MAPPING_COUNTRY_CODE_V("CUS_MAPPING_COUNTRY_CODE_V","国家地区"),
    CUS_CUSTOMS("CUS_CUSTOMS","海关"),
    CUS_MAPPING_TRANSPORT_CODE_V("CUS_MAPPING_TRANSPORT_CODE_V","运输方式"),
    CUS_LEVYTYPE("CUS_LEVYTYPE","征免性质"),
    CUS_MAPPING_PORT_CODE_V("CUS_MAPPING_PORT_CODE_V","港口"),
    CUS_MAPPING_PORT_CODE("CUS_MAPPING_PORT_CODE","国内港口"),
    CUS_TRANSAC("CUS_TRANSAC","成交方式"),
    DEC_FEE_MARK("DEC_FEE_MARK","运费率"),
    DEC_INSUR_MARK("DEC_INSUR_MARK","保费率"),
    DEC_OTHER_MARK("DEC_OTHER_MARK","杂费率"),
    CUS_MAPPING_CURRENCY_CODE_V("CUS_MAPPING_CURRENCY_CODE_V","币制"),
    CUS_MAPPING_PACKAGEKIND_CODE_V("CUS_MAPPING_PACKAGEKIND_CODE_V","包装种类"),
    CIQ_PORT_CN("CIQ_PORT_CN","口岸"),
    DEC_ENTRY_TYPE("DEC_ENTRY_TYPE","报关单类型"),
    DEC_SPECL_RL_AFM_CODE("DEC_SPECL_RL_AFM_CODE","特殊关系确认"),
    CIQ_CUS_ORGANIZATION("CIQ_CUS_ORGANIZATION","检验检机关"),
    CIQ_APTT_NAME_I("CIQ_APTT_NAME_I","企业资质"),
    CIQ_LICENSE_I("CIQ_LICENSE_I","许可证类别进口"),
    CIQ_LICENSE_E("CIQ_LICENSE_E","许可证类别出口"),
    DEC_ORIG_BOX_FLAG("DEC_ORIG_BOX_FLAG","原箱运输"),
    CUS_MAPPING_NORMS_CODE_V("CUS_MAPPING_NORMS_CODE_V","集装箱规格"),
    CUS_LICENSEDOCU("CUS_LICENSEDOCU","随附单证代码"),
    CUS_UNIT("CUS_UNIT","单位"),
    CIQ_ORIGIN_PLACE("CIQ_ORIGIN_PLACE","原产地区"),
    CUS_DISTRICT("CUS_DISTRICT","境内目的地"),
    CIQ_CITY_CN("CIQ_CITY_CN","国内地区"),
    CUS_LEVYMODE("CUS_LEVYMODE","征免方式"),
    CIQ_USE("CIQ_USE","用途"),
    DEC_NO_DANG_FLAG("DEC_NO_DANG_FLAG","非危险化学品"),
    CIQ_DANGER_PACK_TYPE("CIQ_DANGER_PACK_TYPE","危包类别"),
    DEC_BILL_TYPE("DEC_BILL_TYPE","清单类型"),
    DEC_1_0_FLAG("DEC_1_0_FLAG","拼箱标识"),
    CIQ_CORRELATION_REASON("CIQ_CORRELATION_REASON","关联理由"),
    PER_IDEN_RESPGOODS("PER_IDEN_RESPGOODS","身份"),
    SEX("SEX","性别"),
    GOODS_TYPE_FLAG("GOODS_TYPE_FLAG","物品种类"),
    PSU_I_E_FLAG("PSU_I_E_FLAG","进出标志"),
    PRI_USE_GOODS_TYPE("PRI_USE_GOODS_TYPE","公自用物品业务类型"),
    CUS_TRANSF("CUS_TRANSF","境内运输方式"),
    DEC_ATT_TYPE_CODE_V("DEC_ATT_TYPE_CODE_V","随附单据类型"),

    //舱单参数
    CUS_MFT8_ROAD_CUSTOMS_STATUS("CUS_MFT8_ROAD_CUSTOMS_STATUS","海关货物通关代码"),
    CUS_MFT8_CHANGE_REASON("CUS_MFT8_CHANGE_REASON","变更原因"),
    CUS_MFT8_CONTR_CAR_COND("CUS_MFT8_CONTR_CAR_COND","运输条款"),
    CUS_MFT8_ROAD_PAYMENT_METHOD("CUS_MFT8_ROAD_PAYMENT_METHOD","运费支付方法"),
    CUS_MFT8_PACKAGING("CUS_MFT8_PACKAGING","包装种类"),
    CUS_MFT8_COUNTRY_CODE("CUS_MFT8_COUNTRY_CODE","国家或地区"),
    CUS_MFT8_COMMUNI_TYPE("CUS_MFT8_COMMUNI_TYPE","通讯方式"),
    CUS_MFT8_DAN_GOODS("CUS_MFT8_DAN_GOODS","危险品编号"),
    CUS_MFT8_ROAD_CURR("CUS_MFT8_ROAD_CURR","金额类型"),

    //跨境电商清关数据
    VEH_PAY_WAY("VEH_PAY_WAY","支付方式"),
    VEH_ORDER_TYPE("VEH_ORDER_TYPE","订单类型"),
    VEH_ORDER_SEND("VEH_ORDER_SEND","订单发送单位"),
    VEH_INVT_SEND("VEH_INVT_SEND","清单发送单位"),
    VEH_DEC_FLAG("VEH_DEC_FLAG","申报业务类型"),
    VEH_PACKAGING("VEH_PACKAGING","包装种类")
    ;
    private String code;

    private String name;

    CustomsConstantEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CustomsConstantEnum of(String code) {
        return Arrays.stream(CustomsConstantEnum.values()).filter(r -> Objects.equals(code,r.getCode())).findFirst().orElse(null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }}