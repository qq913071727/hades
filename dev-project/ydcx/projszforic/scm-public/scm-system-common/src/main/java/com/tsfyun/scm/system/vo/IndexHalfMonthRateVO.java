package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 最近十五天的中行汇率
 * @CreateDate: Created in 2020/5/31 15:39
 */
@Data
public class IndexHalfMonthRateVO implements Serializable {

    /**
     * 币制
     */
    private String currencyCode;

    private String currencyName;

    /**
     * 日期
     */
    private String date;

    /**
     * 汇率
     */
    private String rate;

}
