package com.tsfyun.scm.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 国内地区响应实体
 * </p>
 *

 * @since 2020-03-20
 */
@Data
@ApiModel(value="DomesticDistrict响应对象", description="国内地区响应实体")
public class DomesticDistrictVO implements Serializable {

     private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "中文名称")
    private String name;

    @ApiModelProperty(value = "英文名称")
    private String shortName;

    @ApiModelProperty(value = "是否禁用，1-是;0-否")
    private Boolean disabled;

    @ApiModelProperty(value = "国内地区性质标记")
    private String propertyFlag;


}
