package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CiqCodeSimpleVo implements Serializable {

    private String id;//海关编码+商检编码
    private String name;//商检编码名称
    private String code;//商检编码
}
