package com.tsfyun.scm.system.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 海关编码+申报要素
 * @CreateDate: Created in
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomsCodeDetailVO implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 海关编码
     */
    private CustomsCodeVO customsCode;

    /**
     * 申报要素
     */
    private List<CustomsElementsVO> customsElements;
}
