layui.extend({
    com: 'lib/common'
    , request: 'lib/request'
}).define(['com', 'request','form'], function (exports) {
    var com = layui.com, request = layui.request, $ = layui.jquery, form = layui.form,
        $obtainSms = $('#obtainSms'),
        fn = {
            codeSeconds:60
            , getSmsCode() {
                let $phoneNo = $('#phoneNo');
                if (com.isEmpty($phoneNo.val())) {
                    $phoneNo.focus();
                    layer.msg('请输入手机号');
                    return;
                }
                if (!com.reg.phone.test($phoneNo.val())) {
                    $phoneNo.focus();
                    layer.msg('请输入正确的手机号码');
                    return;
                }
                $obtainSms.attr({'disabled':true});
                request.post('/scm/auth/sendRetrievePasswordCode',{phoneNo:$phoneNo.val()}).then(res => {
                    layer.msg('发送成功，请注意查收手机短信');
                    com.setLocalStorage('$sendLoginMessageTime', moment().valueOf() / 1000);
                    fn.handleLoginMessageTime(59);
                }).catch(err => {
                    $obtainSms.attr({'disabled':null}).removeClass('layui-btn-disabled');
                });
            },
            handleLoginMessageTime(time) {
                let timer = setInterval(() => {
                    if (time === 0) {
                        clearInterval(timer);
                        $obtainSms.attr({'disabled':null}).html('获取短信验证码');
                    } else {
                        fn.codeSeconds = time;
                        $obtainSms.attr({'disabled':true}).html(fn.codeSeconds+'秒后再次发送');
                        time--;
                    }
                }, 1000);
            }
        };
    $obtainSms.click(function () {
        fn.getSmsCode();
    });

    form.verify({
        passWord: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入登录密码';
            }
        }, 
        confirmPassword: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入确认登录密码';
            }
            if (value != $("#password").val()) {
                return '两次密码输入不一致';
            }
        },
        phoneNo: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入手机号';
            }
            if (!com.reg.phone.test(value)) {
                return '请输入正确的手机号码';
            }
        }, 
        validateCode: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入短信验证码';
            }
            if(!(/(^[1-9]\d*$)/.test(value))){
                $("#validateCode").val("");
                return '短信验证码格式错误';
            }
        }
    });

    form.on('submit(forget-btn)', function (data) {
        var params = $('#forgetPassForm').serializeJson();
        request.postForm('/scm/auth/retrievePassword', params).then(res => {
            $("#forget-btn").off("click");
            layer.msg('重置密码成功');
            setTimeout(function () {
                location.href = '/login.html';
            }, 1000);
        }).catch(res => {
            //清空登录密码
            $("#password").val("");
            $("#confirmPassword").val("");
        });
        return false;
    });
    //发送短信验证码计时器
    const time = moment().valueOf() / 1000 - com.getLocalStorage('$sendLoginMessageTime');
    if (time < 60) {
        fn.codeSeconds = fn.codeSeconds - parseInt(time, 10);
        fn.handleLoginMessageTime(fn.codeSeconds);
    }
    exports('forgetPass', fn);
});