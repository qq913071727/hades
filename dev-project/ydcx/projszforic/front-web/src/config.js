/**
 *  全局配置
 */
layui.define(['laytpl', 'layer', 'element', 'util'], function(exports){
    exports('setter', {
        version: "3.5.1",
        data:{
            companyName:'深圳市芯达通供应链管理有限公司',
            companyAddress:'深圳市龙岗区吉华街道水径社区吉华路393号英达丰工业区A栋厂房101',
            serviceTel:'0755-83765188',
            shortName:'芯达通',
            wechatName:'芯达通',//公众号名称
            realmName:'221.122.108.44:180',
            serviceMail:'xtd@qq.com'
        }
        ,container: 'app' //容器ID
        ,base: layui.cache.base //记录layuiAdmin文件夹所在路径
        ,views: layui.cache.base + 'views/' //视图所在目录
        ,entry: 'index' //默认视图文件名
        ,engine: '.html' //视图文件后缀名

        ,MOD_NAME: 'admin' //模块事件名

        ,debug: false //是否开启调试模式。如开启，接口异常时会抛出异常 URL 等信息

        ,interceptor: false //是否开启未登入拦截

        //自定义响应字段
        ,response: {
            statusName: 'code' //数据状态的字段名称
            ,statusCode: {
                ok: '1' //数据状态一切正常的状态码
                ,logout: 1001 //登录状态失效的状态码
            }
            ,msgName: 'message' //状态信息的字段名称
            ,dataName: 'data' //数据详情的字段名称
        }

        //独立页面路由，可随意添加（无需写参数）
        ,indPage: [
            '/user/login' //登入页
            ,'/user/reg' //注册页
            ,'/user/forget' //找回密码
        ]

        //扩展的第三方模块
        ,extend: [
            'autocomplete'
            ,'supplier'
            ,'supplierBank'
            ,'takeInformation'
            ,'deliveryInformation'
            ,'staticData'
            ,'uploadList'
            ,'importProduct'
            ,'editPhone'
            ,'editPwd'
            ,'editMail'
            ,'companyInfo'
            ,'applyRefund'
            ,'shortcutKey'
            ,'jqueryAutocompleter'
            ,'declAutocompleter'
            ,'bindWx'
        ]
    });
});