layui.define(['laytpl','table'], function (exports) {
    var $ = layui.$,request = layui.request, laytpl = layui.laytpl,table = layui.table,
    id = layui.router().search.id,
    fn = {
        query: function () {
            request.postForm('/scm/invoice/detail',{'id':id}).then(res => {     
               fn.renderData(res.data);
            });
        },
        renderData(data){
            let mainData = data.invoice,members = data.members,costMembers = data.costMembers;
            laytpl($('#invoiceInfoTemp').html()).render(mainData,function (html) {
                $('#invoiceInfo').html(html);
            });
            if(mainData.clientStatusId=='waitConfirm'){
                $('#option-box').show();
            }
            //发票明细信息
            table.render({
                elem: '#member-list',limit:5000,page:false,totalRow: true,height:members.length>10?400:'auto'
                ,cols: [[
                    {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                    ,{field:'isFirst',title:'新料',width:45,align:'center',templet:'#isFirstTemp'}
                    ,{field:'taxCode',title:'税收分类编码',width:150}
                    ,{field:'invoiceName',title:'开票名称',width:100}
                    ,{field:'goodsName',title:'物料名称',width:100}
                    ,{field:'goodsModel',title:'物料型号',width:140}
                    ,{field:'goodsBrand',title:'物料品牌',width:140}
                    ,{field:'unitName',title:'单位',width:50,align:'center'}
                    ,{field:'quantity',title:'数量',width:90,align:'right', totalRow: true}
                    ,{field:'totalPrice',title:'总价',align:'right', totalRow: true}
                ]]
                , data: members
            });

            //发票费用明细
            table.render({
                elem: '#cost-list',limit:5000,page:false,totalRow: true
                ,cols: [[
                    {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                    ,{field:'expenseSubjectName',title:'费用名称',width:160}
                    ,{field:'costMoney',title:'金额',width:100,align:'right', totalRow: true}
                    ,{field:'orderNo',title:'订单号',width:140}
                    ,{field:'docNo',title:'合同号',width:140}
                    ,{field:'memo',title:'说明'}
                ]]
                , data: costMembers
            });
        },
        confrimInvoice(confirm){
            if(!confirm){
                layer.prompt({formType:2,maxlength: 500,title:'请填写原因'},function(val, index){
                    fn.saveConfrim(confirm,val);
                    layer.close(index);
                });
            }else{
                fn.saveConfrim(confirm,'OK');
            }
        }
        ,saveConfrim(confirm,reason){
            request.post('/scm/client/invoice/confirm',{'id':id,'confirm':confirm,'reason':reason}).then(res =>{
               layer.msg('操作成功');
                history.back();
            });
        }
    };
    fn.query();
    exports('invoiceInfo', fn);
});