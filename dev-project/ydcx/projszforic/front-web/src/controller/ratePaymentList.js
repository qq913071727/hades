layui.define(['form','laytpl'], function (exports) {
    var $ = layui.$,com = layui.com, request = layui.request,form=layui.form,laytpl = layui.laytpl,search = layui.router().search,
    fn={
            layer_index:0,
            params: {
                page: 1
                , limit: 10
            },
            list:[]
            ,currencyMap : {}
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,async query(){
                com.showLoading();
                fn.list = [];
                let params = $.extend({},fn.params,$('#form-payment-list').serializeJson());
                request.postForm('/rate/txn/payList',params).then(res => {
                    fn.list = res.data;
                    // 模板渲染
                    laytpl($('#tpl-payment-list').html()).render(res.data, function (html) {
                        $('#tab-payment-list').find('tbody').remove();
                        $('#tab-payment-list').append(html);
                        // 重新渲染表单内表单数据
                        com.chooseCheckbox($('#checkbox-payment-list-check-all'),false);
                        form.render(null, 'form-payment-list');
                    });
                    $.each(res.data || [],function (i,o) {
                        if(o.orderStatus == '01') {
                            laytpl($('#tpl-order').html()).render(o, function (html){
                                $('div[status-id="' + o.id +'"]').html(html);
                            });
                        }
                    });
                    // 开启分页
                    com.pagination('payment-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                     // 初始化复制功能
                     com.createCopyContent();
                    if(res.count>0){
                        $('#payment-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#payment-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            //详细信息
            ,async show(id){
                await $.get('/views/sub/showSettleRatePayment.html',{'id':id},function (html) {
                    fn.layer_index = layer.open({
                        title: '转账信息'
                        , id: 'win-rate-payment'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['550px', 'auto']
                        , content: html
                    });
                });
                await request.get('/rate/txn/payDetail?id=' + id).then(res =>{
                    form.val('form-payment-detail',res.data);
                    $('#form-payment-detail').find('input,select,textarea').not('[type="file"]').attr({'disabled':true});
                }).catch(()=>{
                    layer.close(fn.layer_index);
                });
            }
        };
    fn.clearQuery();
    if(com.isNotEmpty(search.status)) {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $('#queryStatus').find('a[status=' + search.status +']').addClass('curr');
        form.val('form-payment-list',{'status':search.status});
    }
    form.on('submit(search-payment-list)',function () {
        fn.clearQuery();
    });
    form.on('checkbox(checkbox-payment-list-status)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-payment-list',{'status':$(this).attr('status')});
        fn.clearQuery();
    });
    form.render(null, 'form-payment-list');
    exports('ratePaymentList', fn);
});