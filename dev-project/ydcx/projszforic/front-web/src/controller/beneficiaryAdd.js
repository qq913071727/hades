layui.define(['form','staticData','autocomplete'], function (exports) {
    var $ = layui.$, com = layui.com,form = layui.form, staticData = layui.staticData, request = layui.request,
    autocomplete = layui.autocomplete,
    fn = {
        async init(){
            let groupCountryData = await staticData.getGroupCountryData();
            Object.keys(groupCountryData).map(key => { 
                $('#regArea').append('<optgroup label="' + key + '" ' + '>');
                $('#bankCountryCode').append('<optgroup label="' + key + '" ' + '>');
                $.each(groupCountryData[key] || [], function (i, o) {
                    $('#regArea').append('<option value="'+o.id+'" '+'>'+o.name+'</option>');
                    $('#bankCountryCode').append('<option value="' + o.id + '-' + o.areaCode + '" '+'>'+o.name+'</option>');
                });
                $('#regArea').append('</optgroup>');
                $('#bankCountryCode').append('</optgroup>');
            });
            // 联行号模糊查询
            autocomplete.render({
                elem: $("#beneficiary-add-routeNo"),
                url: '/rate/bankNumber/obtainBankNumbers',
                template_txt: '{{d.name}}  {{d.id}}',
                template_val: '{{d.id}}',
                ajaxParams: {
                    type: 'get',
                    contentType: 'application/x-www-form-urlencoded'
                },
                request: {
                    keywords: 'keyword'
                },
                onselect:function (da) {
                   $("#routeNo").val(da.id);
                }
            });
            form.render('select');
        },
        //收款人类型选择
        checkBenificiaryType(){
            let payeeType = form.val("form-beneficiary")['payeeType'];
            if(payeeType == '1') {
                //本公司银行账户，收款人注册地区和银行所在地区默认为中国，路由方式默认为联行号
                form.val("form-beneficiary",{
                    'regArea':'CN',
                    'bankCountryCode':'CN-AS',
                    'routeType':'4',
                    'currency':'CNY'
                });
                $(".bank-number").show();
                $(".iban-number").hide();
                $(".swift-number").hide();
            } else {
                //第三方收款人账户，路由方式默认为SWIFT
                form.val("form-beneficiary",{
                    'routeType':'3',
                    'regArea':'',
                    'bankCountryCode':'',
                    'currency':''
                });
                $(".bank-number").hide();
                $(".iban-number").hide();
                $(".swift-number").show();
            }
            fn.checkBankCountryCode();
            form.render('select');
            form.render('radio');
        },
        //银行所在地选择
        checkBankCountryCode(){
            let bankCountryCodeArry = form.val("form-beneficiary")['bankCountryCode'];
            let bankCountryCode = bankCountryCodeArry.split("-")[0];
            let bankCountryAreaCode = bankCountryCodeArry.split("-")[1];
            if(bankCountryCode == 'CN'){ //中国-联行号
                form.val("form-beneficiary",{
                    'routeType':'4',
                    'currency':'CNY',
                    'swiftCode':'',
                    'ibanNo':''
                });
                $(".bank-number").show();
                $(".iban-number").hide();
                $(".swift-number").hide();
                //swift和iban不能选择
                $("input[name='routeType'][value!='4']").prop("disabled", true);
                $("input[name='routeType'][value='4']").prop("disabled", null);

                $("input[name='bankName']").prop("placeholder", '国内银行精确到开户行，如XXX银行XX支行');
                form.render();
            } else if (bankCountryAreaCode != 'EU') { //非欧洲客户
                form.val("form-beneficiary",{
                    'routeType':'3',
                    'routeNo':'',
                    'ibanNo':'',
                    'currency':''
                });
                $(".bank-number").hide();
                $(".iban-number").hide();
                $(".swift-number").show();

                //联行号不能选择
                $("input[name='routeType'][value='4']").prop("disabled", true);
                $("input[name='routeType'][value='3']").prop("disabled", null);
                $("input[name='routeType'][value='2']").prop("disabled", true);
                $("input[name='bankName']").prop("placeholder", '请输入银行完整的英文名称');
                form.render();
            } else { //欧洲客户
                //默认路由方式选择swift code，同时控制联行号不能选择
                form.val("form-beneficiary",{
                    'routeType':'3',
                    'routeNo':'',
                    'currency':''
                });
                $(".bank-number").hide();
                $(".iban-number").hide();
                $(".swift-number").show();

                //联行号不能选择
                $("input[name='routeType'][value='4']").prop("disabled", true);
                $("input[name='routeType'][value!='4']").prop("disabled", null);
                $("input[name='bankName']").prop("placeholder", '请输入银行完整的英文名称行');
                form.render();
            }
            form.render('select');
            form.render('radio');
        },
        //路由方式选择
        checkRouteType(){
            let routeType = form.val("form-beneficiary")['routeType'];
            if(routeType == '4') {
                $(".bank-number").show();
                $(".iban-number").hide();
                $(".swift-number").hide();
            } else if(routeType == '3'){
                $(".bank-number").hide();
                $(".iban-number").hide();
                $(".swift-number").show();
            } else if(routeType == '2') {
                $(".bank-number").hide();
                $(".iban-number").show();
                $(".swift-number").show();
            }
        }
    }
    fn.init();
    form.render(null, 'form-beneficiary');
    form.on('radio(radio-benificiary-type)',function () {
        fn.checkBenificiaryType();
    });
    form.on('select(select-bankCountryCode)',function(){
        fn.checkBankCountryCode();
    });
    form.on('radio(radio-routeType)',function(){
        fn.checkRouteType();
    });
    form.on('submit(beneficiary-submit)', function (data) {
        let params = data.field;
        //银行所在地区拆解
        params.bankCountryCode = params.bankCountryCode.split("-")[0];
        if(com.isNotEmpty(params.ibanNo)) {
            params.routeNo = params.ibanNo
        }
        request.postForm('/rate/beneficiary/add',params).then(res => {
            layer.msg('新增成功，请耐心等待审核',{time: 1200},function () {
                location.hash = '/rate/beneficiary/list';
            });
        });
    });
    exports('beneficiaryAdd', fn);
});