layui.define(['form','uploadList','autocomplete'], function (exports) {
    var $ = layui.$, com = layui.com,form = layui.form,request = layui.request,uploadList = layui.uploadList,
        autocomplete = layui.autocomplete,
        fileId = 'temp_' + com.uuid(),
    fn = {
        checkIdentityType(){
            let $authorizedPersonFile = $('#authorized-person-file'),params = $('#rateAccountForm').serializeJson();
            $authorizedPersonFile.hide();
            if('2'==params.identityType){
                $authorizedPersonFile.show();
            }
        },
        async init(){
            form.render(null, 'form-rate-account');
            let res = await request.get('/rate/openAccount/obtainOpenAccount');
            let account = res.data;
            if(com.isNotEmpty(account.id)){
                $('#rate-account-status-area').show();
                fileId = account.id;
                form.val('form-rate-account',account);
                let $form = $('#rateAccountForm');
                $form.addClass('form-disabled');
                $form.find('input').attr({'disabled':true});
                $form.find('button').remove();
                $('.upload-box').attr({'file-id': fileId, 'edit': 'false'});
                uploadList.init();

                if(account.status=='1'){
                    $('#rate-account-status').addClass('audited').css({'backgroundColor':'#fff'});
                }
            }else{
                let customer = await request.get('/scm/company/info');
                if(customer.data.statusId == 'audited'){
                    account.name = customer.data.name;
                    account.registerName = customer.data.legalPerson;
                    account.orgCode = customer.data.socialNo;
                    account.openAddress = customer.data.address;
                }
                form.val('form-rate-account',account);
                fn.checkIdentityType();
                $('.upload-box').attr({'file-id': fileId, 'edit': 'true'});
                uploadList.init();

                // autocomplete.render({
                //     elem: $('#account-register-name'),
                //     url: '/scm/company/vagueSimpleBusinessQuery',
                //     template_txt: '{{d.name}} {{d.creditCode}} {{d.operName}}',
                //     template_val: '{{d.name}}',
                //     ajaxParams: {
                //         type: 'get'
                //     },
                //     request: {
                //         keywords: 'name'
                //     },
                //     onselect:function (data) {
                //         form.val('form-rate-account',{
                //             'name':data.name,
                //             'orgCode':data.creditCode,
                //             'registerName':data.operName
                //         });
                //     }
                // });
                form.on('radio(radio-identity-type)',function () {
                    fn.checkIdentityType();
                });
                form.on('submit(submit-save)', function(data){
                    if(uploadList.fileSize('sunrate_open_account','business_license')==0){
                        layer.msg('请上传营业执照');
                        return false;
                    }
                    if(uploadList.fileSize('sunrate_open_account','legal_id_front')==0){
                        layer.msg('请上传法人身份证正面');
                        return false;
                    }
                    if(uploadList.fileSize('sunrate_open_account','legal_id_back')==0){
                        layer.msg('请上传法人身份证反面');
                        return false;
                    }
                    let params = $('#rateAccountForm').serializeJson();
                    if('2'==params.identityType){
                        if(uploadList.fileSize('sunrate_open_account','agent_id_front')==0){
                            layer.msg('请上传授权人身份证正面');
                            return false;
                        }
                        if(uploadList.fileSize('sunrate_open_account','agent_id_back')==0){
                            layer.msg('请上传授权人身份证反面');
                            return false;
                        }
                    }
                    params['fileId'] = fileId;
                    request.post('/rate/openAccount/openAccount',params).then(res =>{
                        let idx = com.alertSuccess('提交成功，请等待审核。',function () {
                            layer.close(idx);
                            layui.manage.render();
                        });
                    });
                    return false;
                });
            }
        }
    };
    fn.init();
    exports('rateAccount', fn);
});