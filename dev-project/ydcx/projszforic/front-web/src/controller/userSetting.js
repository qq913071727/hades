layui.define(['form','editPwd','editPhone','editMail','upload','laytpl','bindWx'], function (exports) {
    var $ = layui.$, com = layui.com,
        form = layui.form,
        view = layui.view,
        setter = layui.setter,
        request = layui.request,
        editPwd = layui.editPwd,
        editPhone = layui.editPhone,
        editMail = layui.editMail,
        bindWx = layui.bindWx,
        upload = layui.upload,
        laytpl = layui.laytpl,
        fn = {
            layer_index:0,
            // 修改密码
            showEditPwd(old){
                editPwd.show(old);
            }
            // 修改手机
            ,showEditPhone(){
                editPhone.show();
            }
            // 修改邮箱
            ,showEditMail(old){
                editMail.show(old,function () {
                    setTimeout(function () {
                        location.reload();
                    },500);
                    view('tpl_user_setting').refresh();
                });
            }
            //修改昵称
            ,async showEditNickName(name){
                await $.get('/views/sub/edit-nike-name.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: '修改昵称'
                        , id: 'win-edit-nike-name'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['550px', 'auto']
                        , content: html
                    });
                });
                $('#input-edit-nike-name').val(name).focus();
                form.on('submit(save-edit-nike-name)',function (data) {
                    request.postForm('/scm/client/person/changeNick',data.field).then((res)=>{
                        com.refPerson(res.data);
                        layer.close(fn.layer_index);
                        layer.msg('修改成功');
                        setTimeout(function () {
                            location.reload();
                        },500);
                    });
                });
            }
            //修改性别
            ,async showEditGender(gender){
                await $.get('/views/sub/edit-gender.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: '修改性别'
                        , id: 'win-edit-gender'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['550px', 'auto']
                        , content: html
                    });
                });
                form.val('form-edit-gender',{'gender':gender||'unknow'});
                form.on('submit(save-edit-gender)',function (data) {
                    request.postForm('/scm/client/person/changeGender',data.field).then(()=>{
                        view('tpl-user-setting').refresh();
                        layer.msg('修改成功');
                        layer.close(fn.layer_index);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    });
                });
            }
            ,uploadHead(){
                //上传头像
                upload.render({
                    elem: '#editHead',size: 5120,exts: 'jpg|png'
                    ,acceptMime:'image/jpeg,image/png'
                    ,url: '/scm/client/person/uploadHead'
                    ,before: function () {
                        com.showLoading();
                    }
                    ,done: function(res){
                        com.hideLoading();
                        if(com.stateInterception(res)){
                            com.refPerson(res.data);
                            layer.msg('修改成功');
                            setTimeout(function () {
                                location.reload();
                            },500);
                        }
                    }
                    ,error: function(){
                        com.hideLoading();
                    }
                });
            }
            ,init(){
                request.get('/scm/client/person/info').then(res =>{
                    laytpl($('#tpl-user-setting').html()).render(res, function (html) {
                        $('#view-user-setting').html(html);
                        fn.uploadHead();
                    })
                });
            }
            //换绑微信
            ,async showBindWx(isBindWx) {
                let option = {'title': isBindWx ? "换绑微信" : "绑定微信",'callback':function(){
                  fn.init();
                }};
                bindWx.show(option);
            }
        };
    fn.init();
    exports('userSetting', fn);
});