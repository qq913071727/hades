layui.define(['form', 'laytpl','table', 'staticData','uploadList'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request,uploadList = layui.uploadList,
        form = layui.form,staticData = layui.staticData,table = layui.table,search = layui.router().search,
        $quantityTotal = $('#quantityTotal'), $totalPriceTotal = $('#totalPrice'),
        $cartonTotal = $('#cartonTotal'), $netWeightTotal = $('#netWeightTotal'),
        $grossWeightTotal = $('#grossWeightTotal'), fn = {
            createRows(members){
                table.render({
                    elem: '#order-material-member',limit:10000,page:false,height:members.length>5?200:'auto',
                    text: {
                        none: '<div class="blank-list" style="display: block;min-height: 100px;padding-top: 10px"><i class="iconfont iconwushuju"></i><dd>未添加产品明细</dd></div>'
                    }
                    ,cols: [[
                        {type:'numbers',title:'NO',width:45}
                        ,{field:'model',title:'型号'}
                        ,{field:'name',title:'品名',width:95}
                        ,{field:'brand',title:'品牌',width:80}
                        ,{field:'unitName',title:'单位',width:45}
                        ,{field:'quantity',title:'数量',width:90,align:'right',templet:'#quantityTpl' }
                        ,{field:'unitPrice',title:'单价',width:80,align:'right'}
                        ,{field:'totalPrice',title:'总价',width:90,align:'right',templet:'#totalPriceTpl'}
                        ,{field:'cartonNum',title:'箱数',width:50,align:'right'}
                        ,{field:'netWeight',title:'净重',width:60,align:'right'}
                        ,{field:'grossWeight',title:'毛重',width:60,align:'right'}
                        ,{field:'countryName',title:'产地',width:60,align:'right'}
                    ]]
                    , data: members
                });
                var mhtml = [],tquantity = 0.0,ttotalPrice = 0.0,tcartonNum = 0,tnetWeight = 0.0,tgrossWeight = 0.0;
                $.each(members,function (i,o) {
                    tquantity+=o.quantity;ttotalPrice+=o.totalPrice;tcartonNum+=o.cartonNum;tnetWeight+=o.netWeight;tgrossWeight+=o.grossWeight;
                });
                $('#materialBody').append(mhtml.join(''));
                $quantityTotal.html(Math.round(tquantity*100)/100);
                $totalPriceTotal.html(com.formatCurrency(Math.round(ttotalPrice*100)/100));
                $cartonTotal.html(tcartonNum);
                $netWeightTotal.html(Math.round(tnetWeight*10000)/10000);
                $grossWeightTotal.html(Math.round(tgrossWeight*10000)/10000);
            }
            // 获取订单详情
            ,async detail(id,operation){
                let data = {order:{businessType:'',impQuoteId:'',transactionMode:''},logistiscs:{
                        deliveryMode:'GiveDelivery'
                        ,hkExpress:'HK_SF',supplierTakeInfoId:null,logisticsCompanyId:null,prescription:'',customerDeliveryInfoId:null,
                        receivingMode:'ThirdLogistics',selfWareId:''
                    },members:[]};
                if(com.isNotEmpty(id)){
                    await request.postForm('/scm/impOrder/detail',{id:id,operation:operation}).then(res =>{
                        data = res.data;
                    }).catch(()=>{
                        location.hash = '/order/list';
                    });
                }
                return data;
            }
            ,showOrHide(mark,val) {
                var $mark = $('[mark="'+mark+'"]');
                $mark.hide();
                $.each($mark,function (j,k) {
                    var zmkeys = $(k).attr('zm-key').split(",");
                    if(com.contain(zmkeys,val)){
                        $(k).show();
                    }
                });
            }
            ,async init() {
                let detail = await fn.detail(search.id,'');
                let order = detail.order,members=detail.members,logistiscs=detail.logistiscs;
                form.val('form-order-base',order);
                logistiscs.takeInformationDesc = ['【',com.removeNull(logistiscs.takeLinkCompany),'】',logistiscs.takeAddress,'(',com.removeNull(logistiscs.takeLinkPerson),' ',com.removeNull(logistiscs.takeLinkTel),')'].join('');
                logistiscs.selfDesc = logistiscs.selfWareName;
                if(com.isNotEmpty(logistiscs.selfWareId)){
                    let wares = await staticData.getTakeSelfWare() || [];
                    wares.some(o =>{
                        if(o.id==logistiscs.selfWareId){
                            logistiscs.selfDesc = o.detailDesc;
                            return true;
                        }
                    });
                }
                logistiscs.deliveryDesc = ['【',com.removeNull(logistiscs.deliveryCompanyName),'】',logistiscs.deliveryAddress,'(',com.removeNull(logistiscs.deliveryLinkPerson),' ',com.removeNull(logistiscs.deliveryLinkTel),')'].join('');
                form.val('form-order-logistics',logistiscs);
                fn.showOrHide('deliveryMode',logistiscs.deliveryMode);
                fn.showOrHide('receivingMode',logistiscs.receivingMode);
                $('#memo').val(order.memo||'');
                // 产品明细数据
                fn.createRows(members || []);
                $('.upload-box').attr({'file-id': order.id, 'edit': 'true'});
                uploadList.init();
                $('#orderMainForm,#orderLogisticsForm,#orderOtherForm').find('input,select,textarea').not('[type="file"]').attr({'disabled':true});
                form.render(null, 'form-order-base');
                form.render(null, 'form-order-logistics');

                $('#order-detail-copy').click(function () {
                    location.hash = '/order/manage/id='+search.id+'/copy=true';
                });
                $('#order-detail-cost').click(function () {
                    location.hash = '/order/printCost/id='+search.id;
                });
            }
        }
    ;
    fn.init();
    exports('orderDetail', fn);
});