layui.define(['form', 'laytpl'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request, form = layui.form, laytpl = layui.laytpl,
        fn = {
            params: {
                page: 1
                , limit: 10
            }
            , query: function () {
                com.showLoading();
                let params = $.extend({}, fn.params, $('#form-payment-list').serializeJson());
                request.postForm('/scm/receiptAccount/list', params).then(res => {
                    // 模板渲染
                    laytpl($('#tpl-payment-list').html()).render(res.data, function (html) {
                        $('#tab-payment-list').find('tbody').remove();
                        $('#tab-payment-list').append(html);
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('payment-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#payment-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#payment-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,judstatus(st){
                if(['waitConfirm'].contain(st)){
                    return 1;
                }else if(['confirmed'].contain(st)){
                    return 2;
                }
                return 0;
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-payment-list',{'accountDateStart':othis.attr('dateStart'),'accountDateEnd':othis.attr('dateEnd')});
                fn.clearQuery();
            }
        };
    let now = new Date(),dates=[];
    now.setMonth(now.getMonth() - 1);
    form.val('form-payment-list',{'accountDateStart':com.formatDate(now,'yyyy-MM-dd')});
    dates.push({'title':'近一个月付款',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    now.setMonth(now.getMonth() - 2);
    dates.push({'title':'近三个月付款',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    let year = new Date().getFullYear();
    dates.push({'title':'今年内付款',dateStart:year+'-01-01',dateEnd:''});
    for(var i=1;i<5;i++){
        let yearLast = new Date();
        yearLast.setFullYear(year-i);
        yearLast.setMonth(0);
        yearLast.setDate(1);
        let item = {'title':yearLast.getFullYear()+'年付款',dateStart:com.formatDate(yearLast,'yyyy-MM-dd'),dateEnd:''};
        yearLast.setFullYear(yearLast.getFullYear()+1);
        yearLast.setDate(yearLast.getDate() - 1);
        item.dateEnd = com.formatDate(yearLast,'yyyy-MM-dd');
        dates.push(item);
    }
    let $listDate = $('#payment-list-date'),dataItems=[];
    dates.forEach(o =>{
        dataItems.push('<li><a href="javascript:" dateStart="'+o.dateStart+'" dateEnd="'+o.dateEnd+'"><b></b>'+o.title+'</a></li>');
    });
    $listDate.find('.more-operation-txt div').html(dates[0].title);
    $listDate.find('.more-operation-list ul').html(dataItems.join(''));
    $($listDate.find('.more-operation-list ul li a')[0]).addClass('curr');

    form.render(null, 'form-payment-list');
    fn.clearQuery();
    form.on('submit(search-payment-list)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-payment-list',{'statusId':$(this).attr('status')});
        fn.clearQuery();
    });
    exports('paymentList', fn);
});