layui.define(['form', 'laytpl'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request, form = layui.form, laytpl = layui.laytpl,
        fn = {
            params: {
                page: 1
                , limit: 10
            }
            , query: function () {
                com.showLoading();
                let params = $.extend({}, fn.params, $('#form-invoice-list').serializeJson());
                request.post('/scm/client/invoice/page', params).then(res => {
                    // 模板渲染
                    laytpl($('#tpl-invoice-list').html()).render(res.data, function (html) {
                        $('#tab-invoice-list').find('tbody').remove();
                        $('#tab-invoice-list').append(html);
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('invoice-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#invoice-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#invoice-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-invoice-list',{'applyDateStart':othis.attr('dateStart'),'applyDateEnd':othis.attr('dateEnd')});
                fn.clearQuery();
            }
        };
    let now = new Date(),dates=[];
    now.setMonth(now.getMonth() - 3);
    dates.push({'title':'近三个月申请',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    let year = new Date().getFullYear();
    dates.push({'title':'今年内申请',dateStart:year+'-01-01',dateEnd:''});
    for(var i=1;i<5;i++){
        let yearLast = new Date();
        yearLast.setFullYear(year-i);
        yearLast.setMonth(0);
        yearLast.setDate(1);
        let item = {'title':yearLast.getFullYear()+'年申请',dateStart:com.formatDate(yearLast,'yyyy-MM-dd'),dateEnd:''};
        yearLast.setFullYear(yearLast.getFullYear()+1);
        yearLast.setDate(yearLast.getDate() - 1);
        item.dateEnd = com.formatDate(yearLast,'yyyy-MM-dd');
        dates.push(item);
    }
    let $listDate = $('#invoice-list-date'),dataItems=[];
    dates.forEach(o =>{
        dataItems.push('<li><a href="javascript:" dateStart="'+o.dateStart+'" dateEnd="'+o.dateEnd+'"><b></b>'+o.title+'</a></li>');
    });
    $listDate.find('.more-operation-txt div').html(dates[0].title);
    $listDate.find('.more-operation-list ul').html(dataItems.join(''));
    $($listDate.find('.more-operation-list ul li a')[0]).addClass('curr');

    form.render(null, 'form-invoice-list');
    fn.clearQuery();
    form.on('submit(search-invoice-list)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-invoice-list',{'clientStatusId':$(this).attr('status')});
        fn.clearQuery();
    });
    exports('invoiceList', fn);
});