layui.define(['form','laytpl'], function (exports) {
    var $ = layui.$,com = layui.com, request = layui.request,form=layui.form,laytpl = layui.laytpl,search = layui.router().search,fn={
            params: {
                page: 1
                , limit: 10
            },
            list:[]
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,async query(){
                com.showLoading();
                fn.list = [];
                let params = $.extend({},fn.params,$('#form-beneficiary-list').serializeJson());
                request.postForm('/rate/beneficiary/list',params).then(res => {
                    fn.list = res.data;
                    // 模板渲染
                    laytpl($('#tpl-beneficiary-list').html()).render(res.data, function (html) {
                        $('#tab-beneficiary-list').find('tbody').remove();
                        $('#tab-beneficiary-list').append(html);
                        // 重新渲染表单内表单数据
                        com.chooseCheckbox($('#checkbox-beneficiary-list-check-all'),false);
                        form.render(null, 'form-beneficiary-list');
                    });
                    $.each(res.data || [],function (i,o) {
                        if(o.status == '2') {
                            laytpl($('#tpl-no-pass').html()).render(o, function (html){
                                $('div[status-id="' + o.id +'"]').html(html);
                            });
                        }
                    });
                    // 开启分页
                    com.pagination('beneficiary-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#beneficiary-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#beneficiary-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
        };
    fn.clearQuery();
    if(com.isNotEmpty(search.status)) {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $('#queryStatus').find('a[status=' + search.status +']').addClass('curr');
        form.val('form-beneficiary-list',{'status':search.status});
    }
    form.on('submit(search-beneficiary-list)',function () {
        fn.clearQuery();
    });
    form.on('checkbox(checkbox-beneficiary-list-status)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-beneficiary-list',{'status':$(this).attr('status')});
        fn.clearQuery();
    });
    form.render(null, 'form-beneficiary-list');
    exports('rateBeneficiaryList', fn);
});