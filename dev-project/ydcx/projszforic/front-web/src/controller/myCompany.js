layui.define(['form','uploadList','companyInfo'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request,
        uploadList = layui.uploadList,
        companyInfo = layui.companyInfo,
        form = layui.form,search = layui.router().search,
        fn = {
        getCompanyInfo(){
            request.get('/scm/company/info').then(res=>{
                let data = res.data;
                form.val('form-company-info',data);
                data.linkAddress = [data.invoiceProvince,data.invoiceCity,data.invoiceArea,data.linkAddress].join(' ');
                form.val('form-invoice-info',data);
                $('.upload-box').attr({'file-id': data.id, 'edit': 'false'});
                uploadList.init();
                $('#company-info-status-name').addClass(data.statusId);
                if(data.statusId == 'waitPerfect' && com.isNotEmpty(data.auditOpinion) && com.isNotEmpty(data.name)){
                    $('#show-audit-opinion-tips').show();
                }
            });
        }
        // 修改公司信息
        ,showEditCompany(){
            companyInfo.editCompany(function (){
                fn.getCompanyInfo();
            });
        }
        // 修改开票信息
        ,showEditInvoice(){
            companyInfo.editInvoice(function (){
                fn.getCompanyInfo();
            });
        }

    };
    fn.getCompanyInfo();
    exports('myCompany', fn);
});