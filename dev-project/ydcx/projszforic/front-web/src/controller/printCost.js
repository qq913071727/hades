layui.define(['laytpl'], function (exports) {
    var $ = layui.$, laytpl = layui.laytpl, com = layui.com, request = layui.request,
        search = layui.router().search
        ,fn = {
            load(){
                let orderId = search.id;
                if(com.isEmpty(orderId)){
                    layer.msg('请选择您要查看的订单');
                    history.back();
                    return;
                }
                request.postForm('/scm/impOrderCost/adviceReceipt',{id:orderId}).then(res =>{
                    let data = res.data,total = {'quantity':0.0,'grossWeight':0.0,'totalPrice':0.0,'tariffRateVal':0.0,'addedTaxTateVal':0.0,'receAmount':0.0};
                    $.each(data.members || [],function (i,o) {
                        total.quantity = Math.round((total.quantity + o.quantity)*100)/100;
                        total.totalPrice = Math.round((total.totalPrice + o.totalPrice)*100)/100;
                        total.grossWeight = Math.round((total.grossWeight + o.grossWeight)*100)/100;
                        total.tariffRateVal = Math.round((total.tariffRateVal + o.tariffRateVal)*100)/100;
                        total.addedTaxTateVal = Math.round((total.addedTaxTateVal + o.addedTaxTateVal)*100)/100;
                    });
                    $.each(data.costs || [],function (i,o) {
                        total.receAmount = Math.round((total.receAmount + o.receAmount)*100)/100;
                    });
                    data['total'] = total;
                    laytpl($('#print-cost-temp').html()).render(data,function (html) {
                        $('#print-cost-view').html(html);
                    });
                }).catch(()=>{
                    history.back();
                });
            },
            export2Excel() { //导出为excel
                let orderId = search.id;
                request.get('/scm/impOrderCost/excel',{id:orderId}).then(res=>{
                    //下载文件
                    window.open('/scm/export/down?id=' + res.data);
                })
            },
            export2Pdf() { //导出为pdf
                let orderId = search.id;
                request.get('/scm/impOrderCost/pdf',{id:orderId}).then(res=>{
                    //下载文件
                    window.open('/scm/export/down?id=' + res.data);
                })
            }
        };
        fn.load();
    exports('printCost', fn);
});