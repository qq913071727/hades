layui.define(['form','table','shortcutKey','uploadList','declAutocompleter'], function (exports) {
    var $ = layui.$,setter=layui.setter,com = layui.com, request = layui.request,search = layui.router().search,shortcutKey=layui.shortcutKey,uploadList = layui.uploadList,
        form = layui.form,table=layui.table,declAutocompleter=layui.declAutocompleter,fileId = 'temp_' + com.uuid(),fn={
            data:{containers:[]},
            renderContainers:function (reset) {
                if(reset){$.each(fn.data.containers,function (i,o) {o['LAY_CHECKED'] = false;o['LAY_TABLE_INDEX'] = i;o['contSeqNo'] = (i+1);});}
                table.render({
                    elem: '#container-list',id:'container-list',limit:500,page:false,
                    height:110,text:{none:'暂无数据'},
                    cols:[[
                        {type: 'radio',width:45}
                        ,{field:'contSeqNo',title:'项号',width:60,align:'center'}
                        ,{field:'containerNo',title:'集装箱号',width:120}
                        ,{field:'containerMdCode',title:'集装箱规格编号',width:200}
                        ,{field:'containerMdCodeName',title:'集装箱规格'}
                    ]]
                    ,data: fn.data.containers
                });
            },
            saveContainer:function(){
                var itemData = $('#containerForm').serializeJson();
                if(com.isEmpty(itemData.containerNo)){com.tipsMessage('集装箱号不能为空',$('#containerNo'));$('#containerNo').focus();return;}
                if(com.isEmpty(itemData.containerMdCodeName)){com.tipsMessage('集装箱规格不能为空',$('#containerMdCodeName'));$('#containerMdCodeName').focus();return;}
                var isAdd = true;
                for(var i=0;i<fn.data.containers.length;i++){
                    if(fn.data.containers[i].containerNo==itemData.containerNo && fn.data.containers[i].contSeqNo!=itemData.contSeqNo){com.tipsMessage('集装箱号不能重复',$('#containerNo'));$('#containerNo').select();return;}
                    if(fn.data.containers[i].contSeqNo==itemData.contSeqNo){
                        fn.data.containers[i] = itemData;isAdd = false;
                    }
                }
                if(isAdd){fn.data.containers.push(itemData);}
                fn.renderContainers(true);
                $('#newContainer').click();
            },
            async init(){
                if(com.isNotEmpty(com.getLocalStorageJson('$def-dec-order-data'))){
                    form.val('form-trust-declare-manage',com.getLocalStorageJson('$def-dec-order-data'));
                }
                request.postForm('/scm/trustOrder/init').then(res =>{
                    $('#docNo').val(res.data);
                    fn.renderContainers();
                    form.render(null, 'form-trust-declare-manage');
                    shortcutKey.init($('#containerForm'));
                    $('.upload-box').attr({'file-id': fileId, 'edit': 'true'});
                    uploadList.init();

                }).catch((err)=>{
                    layer.closeAll();
                    let idx = com.alertWarn(err.message,function () {
                        layer.close(idx);
                        location.hash = '/company/basicInfo';
                    });
                });

                declAutocompleter.init();
            }
        };
    fn.init();
    // 保存订单
    $('#dec-trust-order-save').click(function () {
        var order = $('#form-trust-declare-manage').serializeJson();
        if(com.isEmpty(order.cusIEFlag)){
            com.tipsMessage('请选择进出口类型',$('#cusIEFlag'));
            $('#cusIEFlag').focus();
            return;
        }
        if(com.isEmpty(order.customsCode)){
            com.tipsMessage('进/出境关别不能为空',$('#iePortName'));
            $('#iePortName').focus();
            return;
        }
        let $declareDraft = $('#trust-order-declare-draft');
        if($declareDraft.find('.file-box li').size()==0){
            com.tipsMessage('请至少上传报关草单',$declareDraft);return;
        }
        order['fileId'] = fileId;
        request.post('/declare/trustOrder/save',{trustOrder:order,containers:fn.data.containers}).then(res =>{
            com.setLocalStorageJson('$def-dec-order-data',{'customsCode':order.customsCode,'customsName':order.customsName,'cusIEFlag':order.cusIEFlag});
            $('#dec-trust-order-save').off('click');
            layer.msg('下单成功，我司会尽快处理您的订单',{time: 1200},function () {
                location.hash = '/declare/list';
            });
        });
    });
    //添加集装箱号
    $('#containerMdCodeName').enter(function () {
        fn.saveContainer();
    });
    //集装箱行选择监听
    table.on('row(dec-container-tab)', function(obj){
        com.tabRadioChoice(obj,'container-list');
        form.val('containerForm',obj.data);
    });
    //新增集装箱号
    $('#newContainer').click(function () {
        form.val('containerForm',{'contSeqNo':(fn.data.containers.length+1),'containerNo':'','containerMdCode':'','containerMdCodeName':''});
        $('#containerNo').focus();
    });
    //复制集装箱号
    $('#copyContainer').click(function () {
        var selectData = table.checkStatus('container-list').data;
        if(selectData.length!=1){
            layer.msg('请选择一条您要复制的集装箱信息');return;
        }
        $.each(fn.data.containers,function (i,o) {
            o['LAY_CHECKED'] = false;
            o['LAY_TABLE_INDEX'] = i;
            o['contSeqNo'] = (i+1);
        });
        var itemData = selectData[0];
        itemData['contSeqNo'] = fn.data.containers.length + 1;
        itemData['LAY_CHECKED'] = true;
        fn.data.containers.push(itemData);
        form.val('containerForm',itemData);
        fn.renderContainers(false);
    });
    //删除集装箱号
    $('#delContainer').click(function () {
        var selectData = table.checkStatus('container-list').data;
        if(selectData.length!=1){
            layer.msg('请选择一条您要删除的集装箱信息');return;
        }
        com.confirm_inquiry('您确定要删除所选集装箱信息？',function () {
            var newContainers = [];
            $.each(table.cache["container-list"],function (i,o) {
                if(!o["LAY_CHECKED"]){
                    newContainers.push(o);
                }
            });
            fn.data.containers = newContainers;
            fn.renderContainers(true);
            $('#newContainer').click();
        });
    });
    exports('trustDeclareManage', fn);
});