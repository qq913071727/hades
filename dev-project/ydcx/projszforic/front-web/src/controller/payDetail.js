layui.define(['form','table','uploadList'], function (exports) {
    var $ = layui.$, form = layui.form,table = layui.table,com = layui.com,uploadList = layui.uploadList,
        request = layui.request,
        search = layui.router().search
        ,fn = {
            obtainBank(){
                request.get('/scm/subjectBank/selectEnableBanks').then(res =>{
                    let bh = [];
                    $.each(res.data || [],function (i,o) {
                        bh.push('<p>'+(i+1)+' 开户行：'+o.name+' 银行账号：'+o.account+' '+o.subjectName+'</p>');
                    });
                    $('#subjectBank').html(bh.join(''));
                });
            }
            ,load(){
                let paymentId = search.id;
                if(com.isEmpty(paymentId)){
                    layer.msg('请选择您要查看的付汇单');
                    history.back();
                    return;
                }
                request.get('/scm/client/payment/detail',{'id':paymentId}).then(res =>{
                    var data = res.data,payment=data.payment,members=data.members;
                    payment.totalPayValue = com.formatCurrency(payment.accountValueCny + payment.bankFee);
                    payment.accountValueCny = com.formatCurrency(payment.accountValueCny);
                    payment.bankFee = com.formatCurrency(payment.bankFee);
                    form.val("form-pay-detail",payment);
                    $('.exchangeRate').html(payment.exchangeRate);
                    $('.payCny').html(payment.accountValueCny);
                    $('.handlingFee').html(payment.bankFee);
                    $('.payTotalCny').html(payment.totalPayValue);
                    $('#form-pay-detail').find('input,select,textarea').attr({'disabled':true});
                    $('.upload-box').attr({'file-id': payment.id, 'edit': 'false'});
                    uploadList.init();

                    table.render({
                        elem: '#pay-detail-order-tab',limit:10000,page:false,
                        text: {
                            none: '<div class="blank-list" style="display: block;min-height: 100px;padding-top: 10px"><i class="iconfont iconwushuju"></i><dd>无订单信息</dd></div>'
                        }
                        ,cols: [[
                            {type:'numbers',title:'序号',width:50}
                            ,{field:'impOrderNo',title:'订单号',width:120}
                            ,{field:'orderDate',title:'订单日期',width:100}
                            ,{field:'supplierName',title:'供应商'}
                            ,{field:'totalPrice',title:'订单金额',width:100,align:'right',templet:'#totalPriceTpl'}
                            ,{field:'accountValue',title:'付款金额',width:100,align:'right',templet:'#accountValueTpl'}
                            ,{field:'accountValueCny',title:'人民币金额',width:100,align:'right',templet:'#accountValueCnyTpl'}
                        ]]
                        , data: members
                    });

                    fn.obtainBank();
                }).catch(()=>{
                    history.back();
                });
            }
        };
    fn.load();
    exports('payDetail', fn);
});