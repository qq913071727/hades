layui.define(['form', 'laytpl'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request, form = layui.form, laytpl = layui.laytpl,
        fn = {
            params: {
                page: 1
                , limit: 10
            }
            , query: function () {
                com.showLoading();
                let params = $.extend({}, fn.params, $('#form-receivingnote-list').serializeJson());
                request.post('/scm/client/receivingNote/list', params).then(res => {
                    // 模板渲染
                    laytpl($('#tpl-receiving-list').html()).render(res.data, function (html) {
                        $('#tab-receiving-list').find('tbody').remove();
                        $('#tab-receiving-list').append(html);
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('receivingnote-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#receivingnote-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#receiving-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-receivingnote-list',{'receivingDateStart':othis.attr('dateStart'),'receivingDateEnd':othis.attr('dateEnd')});
                fn.clearQuery();
            }
        };
    let now = new Date(),dates=[];
    now.setMonth(now.getMonth() - 1);
    form.val('form-receivingnote-list',{'receivingDateStart':com.formatDate(now,'yyyy-MM-dd')});
    dates.push({'title':'近一个月入库',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    now.setMonth(now.getMonth() - 2);
    dates.push({'title':'近三个月入库',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    let year = new Date().getFullYear();
    dates.push({'title':'今年内入库',dateStart:year+'-01-01',dateEnd:''});
    for(var i=1;i<5;i++){
        let yearLast = new Date();
        yearLast.setFullYear(year-i);
        yearLast.setMonth(0);
        yearLast.setDate(1);
        let item = {'title':yearLast.getFullYear()+'年入库',dateStart:com.formatDate(yearLast,'yyyy-MM-dd'),dateEnd:''};
        yearLast.setFullYear(yearLast.getFullYear()+1);
        yearLast.setDate(yearLast.getDate() - 1);
        item.dateEnd = com.formatDate(yearLast,'yyyy-MM-dd');
        dates.push(item);
    }
    let $listDate = $('#receiving-list-date'),dataItems=[];
    dates.forEach(o =>{
        dataItems.push('<li><a href="javascript:" dateStart="'+o.dateStart+'" dateEnd="'+o.dateEnd+'"><b></b>'+o.title+'</a></li>');
    });
    $listDate.find('.more-operation-txt div').html(dates[0].title);
    $listDate.find('.more-operation-list ul').html(dataItems.join(''));
    $($listDate.find('.more-operation-list ul li a')[0]).addClass('curr');

    form.render(null, 'form-receivingnote-list');
    fn.clearQuery();
    form.on('submit(search-receiving-list)',function () {
        fn.clearQuery();
    });
    exports('receivingnoteList', fn);
});