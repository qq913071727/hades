layui.define(['form','laytpl'], function (exports) {
    var $ = layui.$,com = layui.com, request = layui.request,form=layui.form,laytpl = layui.laytpl,search = layui.router().search,
    $buyCur = $('#buyCur'),
    fn={
            layer_index:0,
            params: {
                page: 1
                , limit: 10
            },
            list:[]
            ,currencyMap : {}
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,loadCurrency(){
                fn.currencyMap = {};
                $buyCur.get(0).length = 0;
                request.get('/rate/txn/getCurrency',{}).then(res => {
                    let data = res.data;
                    $buyCur.append("<option value=''>买入币种</option>");
                    $.each(data||[],function (i,o){
                        fn.currencyMap[o.id] = o;
                        $buyCur.append('<option value="'+o.id+'" ' + '>'+o.name+'</option>');
                    });
                    form.render('select','form-txn-list');
                });
            }
            ,async query(){
                com.showLoading();
                fn.list = [];
                let params = $.extend({},fn.params,$('#form-txn-list').serializeJson());
                request.postForm('/rate/txn/txnList',params).then(res => {
                    fn.list = res.data;
                    // 模板渲染
                    laytpl($('#tpl-txn-list').html()).render(res.data, function (html) {
                        $('#tab-txn-list').find('tbody').remove();
                        $('#tab-txn-list').append(html);
                        // 重新渲染表单内表单数据
                        com.chooseCheckbox($('#checkbox-txn-list-check-all'),false);
                        form.render(null, 'form-txn-list');
                    });
                    $.each(res.data || [],function (i,o) {
                        if(o.orderStatus == '01') {
                            laytpl($('#tpl-order').html()).render(o, function (html){
                                $('div[status-id="' + o.id +'"]').html(html);
                            });
                        }
                    });
                    // 开启分页
                    com.pagination('txn-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                     // 初始化复制功能
                     com.createCopyContent();
                    if(res.count>0){
                        $('#txn-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#txn-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,async declare(txnNo){
                await $.get('/views/sub/rateSettlementDeclare.html',{},function (html) {
                    fn.layer_index = layer.open({
                        title: '新增收支交易类别'
                        , id: 'win-rate-settlement-declare'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['550px', 'auto']
                        , content: html
                    });
                });
                $('#rate-settlement-declare-txnNo').val(txnNo);
                form.render('select');
                form.on('submit(save-rate-settlement-declare)',function (data) {
                    request.postForm('/rate/txn/declare',data.field).then((res)=>{
                        layer.close(fn.layer_index);
                        layer.msg('新增成功');
                        //重新加载列表
                        fn.clearQuery();
                    });
                });
            }
            //交易信息
            ,async show(id,type){
                //此处区分结汇/购汇
                await $.get('/views/sub/showSettleRateTxn.html',{'id':id},function (html) {
                    fn.layer_index = layer.open({
                        title: '交易信息'
                        , id: 'win-rate-txn'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['550px', 'auto']
                        , content: html
                    });
                });
                await request.get('/rate/txn/detailById?id=' + id).then(res =>{
                    form.val('form-txn-detail',res.data);
                }).catch(()=>{
                    layer.close(fn.layer_index);
                });
            }
        };
    fn.clearQuery();
    if(com.isNotEmpty(search.status)) {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $('#queryStatus').find('a[status=' + search.status +']').addClass('curr');
        form.val('form-txn-list',{'status':search.status});
    }
    form.on('submit(search-txn-list)',function () {
        fn.clearQuery();
    });
    form.on('checkbox(checkbox-txn-list-status)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-txn-list',{'status':$(this).attr('status')});
        fn.clearQuery();
    });
    fn.loadCurrency();
    form.render(null, 'form-txn-list');
    exports('rateTxnList', fn);
});