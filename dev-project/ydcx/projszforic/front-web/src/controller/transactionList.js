layui.define(['form', 'laytpl','staticData'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request, form = layui.form,
        laytpl = layui.laytpl,
        staticData = layui.staticData,
        fn = {
            params: {
                page: 1
                , limit: 10
            }
            , query: function () {
                com.showLoading();
                let params = $.extend({}, fn.params, $('#form-transaction-list').serializeJson());
                request.post('/scm/client/transactionFlow/list', params).then(res => {
                    // 模板渲染
                    laytpl($('#tpl-transaction-list').html()).render(res.data.transactions || [], function (html) {
                        $('#tab-transaction-list').find('tbody').remove();
                        $('#tab-transaction-list').append(html);
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('transaction-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#transaction-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#transaction-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,async init(){
                let now = new Date(),dates=[];
                now.setMonth(now.getMonth() - 1);
                form.val('form-order-list',{'accountDateStart':com.formatDate(now,'yyyy-MM-dd')});
                dates.push({'title':'近一个月交易',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                now.setMonth(now.getMonth() - 2);
                dates.push({'title':'近三个月交易',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                let year = new Date().getFullYear();
                dates.push({'title':'今年内交易',dateStart:year+'-01-01',dateEnd:''});
                for(var i=1;i<5;i++){
                    let yearLast = new Date();
                    yearLast.setFullYear(year-i);
                    yearLast.setMonth(0);
                    yearLast.setDate(1);
                    let item = {'title':yearLast.getFullYear()+'年交易',dateStart:com.formatDate(yearLast,'yyyy-MM-dd'),dateEnd:''};
                    yearLast.setFullYear(yearLast.getFullYear()+1);
                    yearLast.setDate(yearLast.getDate() - 1);
                    item.dateEnd = com.formatDate(yearLast,'yyyy-MM-dd');
                    dates.push(item);
                }
                let $listDate = $('#transaction-list-date'),dataItems=[];
                dates.forEach(o =>{
                    dataItems.push('<li><a href="javascript:" dateStart="'+o.dateStart+'" dateEnd="'+o.dateEnd+'"><b></b>'+o.title+'</a></li>');
                });
                $listDate.find('.more-operation-txt div').html(dates[0].title);
                $listDate.find('.more-operation-list ul').html(dataItems.join(''));
                $($listDate.find('.more-operation-list ul li a')[0]).addClass('curr');
                form.render(null, 'form-pay-list');
                // 获取交易类型
                let transactionType = await staticData.getTransactionType(),$queryTransactionType=$('#queryTransactionType'),traHtml=[];
                $.each(transactionType || [],function (i,o) {
                    traHtml.push('<li><a href="javascript:" transactionType="'+o.value+'">'+o.label+'</a></li>');
                });
                $($queryTransactionType.find('li')[0]).after(traHtml.join(''));
                // 交易类型查询
                $queryTransactionType.find('a[transactionType]').click(function () {
                    $queryTransactionType.find('a[transactionType]').removeClass('curr');
                    $(this).addClass('curr');
                    form.val('form-transaction-list',{'transactionType':$(this).attr('transactionType')});
                    fn.clearQuery();
                });
                // 查询客户当前余额
                request.get('/scm/client/transactionFlow/outBalance').then(res =>{
                    $('#transaction-list-out-balance').html(com.formatCurrency(res.data));
                });
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-transaction-list',{'dateCreatedStart':othis.attr('dateStart'),'dateCreatedEnd':othis.attr('dateEnd')});
                fn.clearQuery();
            }
        };
    fn.init();
    fn.clearQuery();
    form.on('submit(search-transaction-list)',function () {
        fn.clearQuery();
    });
    exports('transactionList', fn);
});