layui.define(['form', 'laytpl','importProduct'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request, form = layui.form, laytpl = layui.laytpl,
        importProduct = layui.importProduct,
        fn = {
            params: {
                page: 1
                , limit: 10
            }
            , query: function () {
                com.showLoading();
                let params = $.extend({}, fn.params, $('#form-product-list').serializeJson());
                request.postForm('/scm/client/materiel/list', params).then(res => {
                    // 模板渲染
                    laytpl($('#tpl-product-list').html()).render(res.data, function (html) {
                        $('#tab-product-list').find('tbody').remove();
                        $('#tab-product-list').append(html);
                        formatCsc();
                        formatIaqr();
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('product-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#product-list-pagination').show();
                        $('.blank-list').hide();
                    }else{
                        $('#product-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-product-list',{'dateCreateStart':othis.attr('dateStart'),'dateCreateEnd':othis.attr('dateEnd')});
                fn.clearQuery();
            }
            ,init(){
                let now = new Date(),dates=[];
                form.val('form-product-list',{'dateCreateStart':com.formatDate(now,'yyyy-MM-dd')});
                dates.push({'title':'今日产品',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                now.setDate(now.getDate() - 3);
                dates.push({'title':'近3天产品',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                now.setDate(now.getDate() - 4);
                dates.push({'title':'近7天产品',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                now.setDate(now.getDate() + 7);
                now.setMonth(now.getMonth() - 1);
                dates.push({'title':'近一个月产品',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                now.setMonth(now.getMonth() - 2);
                dates.push({'title':'近三个月产品',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
                let year = new Date().getFullYear();
                dates.push({'title':'今年内产品',dateStart:year+'-01-01',dateEnd:''});
                for(var i=1;i<5;i++){
                    let yearLast = new Date();
                    yearLast.setFullYear(year-i);
                    yearLast.setMonth(0);
                    yearLast.setDate(1);
                    let item = {'title':yearLast.getFullYear()+'年订单',dateStart:com.formatDate(yearLast,'yyyy-MM-dd'),dateEnd:''};
                    yearLast.setFullYear(yearLast.getFullYear()+1);
                    yearLast.setDate(yearLast.getDate() - 1);
                    item.dateEnd = com.formatDate(yearLast,'yyyy-MM-dd');
                    dates.push(item);
                }

                let $productListDate = $('#product-list-date'),dataItems=[];
                dates.forEach(o =>{
                    dataItems.push('<li><a href="javascript:" dateStart="'+o.dateStart+'" dateEnd="'+o.dateEnd+'"><b></b>'+o.title+'</a></li>');
                });
                $productListDate.find('.more-operation-txt div').html(dates[0].title);
                $productListDate.find('.more-operation-list ul').html(dataItems.join(''));
                $($productListDate.find('.more-operation-list ul li a')[0]).addClass('curr');
                fn.clearQuery();
            }
        };
    fn.init();
    form.render(null, 'form-product-list');
    form.on('submit(search-product-list)',function () {
        fn.clearQuery();
    });
    //有关税查询
    form.on('checkbox(checkbox-has-tariff)',function (data) {
        form.val('form-product-list',{'hasTariff':data.elem.checked});
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-product-list',{'statusId':$(this).attr('status')});
        fn.clearQuery();
    });
    //导入
    $(".import-btn") .click(function(){
        //打开导入窗口
        importProduct.show();
    });
    exports('product', fn);
});