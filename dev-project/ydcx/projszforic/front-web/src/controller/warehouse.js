layui.define(['laytpl'], function (exports) {
    var $ = layui.$, request = layui.request, com = layui.com, laytpl = layui.laytpl, fn = {
        async init(){
            let warehouseNo = '';
            await request.get('/scm/company/info').then(res=>{
                warehouseNo = res.data.code;
            });
            request.get('/scm/warehouse/all').then(res => {
                let overseasData = res.data.filter(item => item.warehouseType == 'overseas');
                let deomesticData = res.data.filter(item => item.warehouseType == 'domestic');
                $.each(overseasData,function (i,o) {
                   o. warehouseNo = warehouseNo;
                });
                laytpl($('#tpl-overseas-ware-list').html()).render(overseasData, function (html) {
                    $('#overseas-ware-div').append(html);
                });
                laytpl($('#tpl-overseas-ware-list').html()).render(deomesticData, function (html) {
                    $('#domestic-ware-div').append(html);
                });
            });
        }
    };
    fn.init();
    exports('warehouse', fn);
});