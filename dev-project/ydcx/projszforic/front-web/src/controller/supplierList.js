layui.define(['laytpl','form','element','table','supplier','takeInformation','supplierBank'], function (exports) {
    var $ = layui.$,request = layui.request, laytpl = layui.laytpl, form = layui.form,
    com = layui.com,element = layui.element,table = layui.table,
    supplier = layui.supplier,
    takeInformation = layui.takeInformation,
    supplierBank = layui.supplierBank,
    $supplier = $('#sullier-info-name'),
    fn = {
        supplierMap:{},
        // 加载所有供应商
        load(id){
            fn.supplierMap = {};
            $supplier.get(0).length = 0;
            request.postForm('/scm/client/supplier/page',{'page':1,'limit':100}).then(res => {
                let data = res.data;
                $.each(data||[],function (i,o){
                    o['disabledDesc'] = o.disabled?'禁用':'启用';
                    fn.supplierMap[o.id] = o;
                    $supplier.append('<option value="'+o.id+'" '+((id&&id==o.id)?'selected':'')+'>'+o.name+'</option>');
                });
                form.render('select','form-sullier-info');
                fn.fullInfo();
            });
        },
        // 填充供应商基本信息
        fullInfo(){
            let supplierData = {'id':'','disabled':false,'disabledDesc':'','address':''},supplierId = $supplier.val();
            if(com.isNotEmpty(supplierId)){
                supplierData = fn.supplierMap[supplierId];
            }
            form.val('form-sullier-info',supplierData);
            laytpl($('#tpl-supplier-opt').html()).render(supplierData, function (html) {
                $('#view-supplier-opt').html(html);
            });
            fn.fullBankList();
            fn.fullTakeList();
        }
        // 填充供应商银行列表
        ,async fullBankList(){
            let supplierId = $supplier.val(),bankList = [];
            if(com.isNotEmpty(supplierId)){
                await request.postForm('/scm/client/supplierBank/page',{'supplierId':supplierId,'page':1,'limit':100}).then(res => {
                    bankList = res.data;
                });
            }
            table.render({
                elem: '#tab-supplier-bank-list',limit:10000,page:false,height:bankList.length>5?200:'auto',
                text: {
                    none: '<div class="blank-list" style="display: block;min-height: 100px;padding-top: 10px"><i class="iconfont iconwushuju"></i><dd>未添加银行信息</dd></div>'
                }
                ,cols: [[
                    {type:'numbers',title: 'No.',}
                    ,{field: 'name', title: '银行名称', width: 200}
                    ,{field: 'account', title: '账号', width: 150}
                    ,{field: 'swiftCode', title: 'SWIFT CODE', width: 120}
                    ,{field: 'currencyName', title: '币制', width: 55,align:'center'}
                    ,{field: 'address', title: '银行地址'}
                    ,{field: 'isDefault', title: '默认',width: 45,align:'center',templet:'#tpl-default',fixed:'right'}
                    ,{field: 'opt', title: '操作',width: 160,align:'left',templet:'#tpl-bank-opt',fixed:'right'}
                ]],
                data:bankList
            });
        }
        // 填充供应商提货地址
        ,async fullTakeList(){
            let supplierId = $supplier.val(),takeList = [];
            if(com.isNotEmpty(supplierId)){
                await request.postForm('/scm/client/supplierTakeInfo/page',{'supplierId':supplierId,'page':1,'limit':100}).then(res => {
                    takeList = res.data;
                });
            }
            table.render({
                elem: '#tab-supplier-take-list',limit:10000,page:false,height:takeList.length>5?200:'auto',
                text: {
                    none: '<div class="blank-list" style="display: block;min-height: 100px;padding-top: 10px"><i class="iconfont iconwushuju"></i><dd>未添加提货地址信息</dd></div>'
                }
                ,cols: [[
                    {type:'numbers',title: 'No.',}
                    ,{field: 'companyName', title: '提货公司', width: 200}
                    ,{field: 'linkPerson', title: '联系人', width: 80}
                    ,{field: 'linkTel', title: '联系电话', width: 140}
                    ,{field: 'address', title: '提货地址',templet:'#tpl-address'}
                    ,{field: 'isDefault', title: '默认',width: 45,align:'center',templet:'#tpl-default',fixed:'right'}
                    ,{field: 'opt', title: '操作',width: 160,align:'left',templet:'#tpl-take-opt',fixed:'right'}
                ]],
                data:takeList
            });
        }
        // 新增供应商
        ,addSupplier(){
            supplier.add(function (su){
                fn.load(su.id);
            });
        }
        // 修改供应商
        ,editSupplier(){
            let supplierId = $supplier.val();
            supplier.edit(supplierId,function (su){
                fn.load(su.id);
            });
        }
        // 禁用启用供应商
        ,enableDisable(disable){
            let supplierId = $supplier.val();
            request.postForm('/scm/supplier/updateDisabled',{id:supplierId,disabled:disable}).then(res => {
                layer.msg(disable?'已禁用':'已启用');
                fn.load(supplierId);
            });
        }
        // 删除供应商
        ,removeSupplier(){
            layer.confirm('您确定要删除此供应商信息?',{icon: 3,closeBtn:false,title:'温馨提示'},function(index){
                layer.close(index);
                request.postForm('/scm/client/supplier/delete',{id:$supplier.val()}).then(res => {
                    layer.msg('删除成功');
                    fn.load();
                });
            });
        }
        // 新增供应商银行
        ,addSupplierBank(){
            let supplierId = $supplier.val();
            if(com.isEmpty(supplierId)){
                layer.msg('请先添加供应商');return;
            }
            supplierBank.show(null,fn.supplierMap[supplierId].name,function () {
                fn.fullBankList();
            });
        }
        // 修改供应商银行
        ,editSupplierBank(id){
            let supplierId = $supplier.val();
            supplierBank.show(id,fn.supplierMap[supplierId].name,function () {
                fn.fullBankList();
            });
        }
        // 供应商银行默认
        ,setBankDefault(id){
            request.postForm('/scm/client/supplierBank/setDefault',{id:id}).then(() => {
                layer.msg('设置成功');
                fn.fullBankList();
            });
        }
        // 删除供应商银行
        ,removeBankSupplier(id){
            layer.confirm('您确定要删除此供应商银行信息?',{icon: 3,closeBtn:false,title:'温馨提示'},function(index){
                layer.close(index);
                request.postForm('/scm/client/supplierBank/delete',{id:id}).then(() => {
                    layer.msg('删除成功');
                    fn.fullBankList();
                });
            });
        }
        // 新增供应商提货地址
        ,addSupplierTake(){
            let supplierId = $supplier.val();
            if(com.isEmpty(supplierId)){
                layer.msg('请先添加供应商');return;
            }
            takeInformation.show(null,fn.supplierMap[supplierId].name,function () {
                fn.fullTakeList();
            });
        }
        // 修改供应商提货地址
        ,editSupplierTake(id){
            let supplierId = $supplier.val();
            takeInformation.show(id,fn.supplierMap[supplierId].name,function () {
                fn.fullTakeList();
            });
        }
        // 设置提货地址默认
        ,setTakeDefault(id){
            request.postForm('/scm/client/supplierTakeInfo/setDefault',{id:id}).then(res => {
                layer.msg('设置成功');
                fn.fullTakeList();
            });
        }
        // 删除提货地址
        ,removeTakeSupplier(id){
            layer.confirm('您确定要删除此供应商提货信息?',{icon: 3,closeBtn:false,title:'温馨提示'},function(index){
                layer.close(index);
                request.postForm('/scm/client/supplierTakeInfo/delete',{id:id}).then(() => {
                    layer.msg('删除成功');
                    fn.fullTakeList();
                });
            });
        }
    };
    fn.load();
    form.on('select(select-sullier-info-name)',function(){
        fn.fullInfo();
    });
    exports('supplierList', fn);
});