layui.define(['form','laytpl'], function (exports) {
    var $ = layui.$,request = layui.request, laytpl = layui.laytpl, form = layui.form,
        setter = layui.setter,
        com = layui.com,
        fn = {
            init(){
                $('.init-hide').hide();
                request.get('/scm/client/agreement/load').then(res =>{
                    let agreement = res.data.agreement,quoteList = res.data.quoteList;
                    if(com.isEmpty(agreement.statusId)){
                        layer.alert('请联系我司销售人员为您备案协议报价', {icon: 6,title:'温馨提醒',btn:['朕知道了']});
                        return;
                    }else if(agreement.statusId=='waitConfirm'){
                        layer.alert('您存在待确认的进口供应链报价协议，请点击右下角同意签署按钮签署', {icon: 6,title:'温馨提醒',btn:['朕知道了']});
                        $('#agreement-box').show();
                        $('#quote-box').show();
                        $('#agreeSign').show();
                    }else if(agreement.statusId=='audited'){
                        // 已经存在正常协议报价
                        $('#agreement-box').show();
                        $('#quote-box').show();
                        // 显示下载按钮
                        $('#down-print-box').show();
                        if(com.isNotEmpty(agreement.fileId)){
                            $('#down-agreement').attr({'href':'/scm/download/file/'.concat(agreement.fileId)});
                        }
                        if(com.isNotEmpty(quoteList[0].fileId)){
                            $('#down-quote').attr({'href':'/scm/download/file/'.concat(quoteList[0].fileId)});
                        }
                    }
                    form.val('form-sign-agreement',agreement);
                    $('.span-partya-name').text(agreement.partyaName);
                    $('.span-partyb-name').text(agreement.partybName);
                    //报价单
                    laytpl($('#tpl-agr-quote').html()).render(quoteList, function (html) {
                        $('#view-agr-quote').html(html);
                        let $businessType = $('#businessType');
                        if(com.isEmpty($businessType.val())){
                            let $checkbox = $('.zm-checkbox');
                            $checkbox.click(function () {
                                let othis = $(this);
                                $checkbox.removeClass('checked');
                                othis.addClass('checked');
                                $businessType.val(othis.attr('z-value'));
                            });
                        }
                    });

                    $.get('/views/version/agreement'+agreement.version+'.html', {'v': setter.version}, function (temp) {
                        laytpl(temp).render(agreement,function (html) {
                            $('#agrement-context').html(html);
                        });
                    });

                    let $agreeSign = $('#agreeSign'),readTime=5,timer = setInterval(function () {
                        readTime--;
                        $agreeSign.html('<i class="iconfont iconicon-1"></i>同意并签署('+readTime+'s)');
                        if(readTime<=0){
                            clearInterval(timer);
                            $agreeSign.attr({'disabled':null}).html('<i class="iconfont iconicon-1"></i>同意并签署');
                        }
                    },1000);
                    $agreeSign.click(function () {
                        let params = $('#form-sign-agreement').serializeJson();
                        // if(com.isEmpty(params.partyaLinkPerson)){
                        //     layer.msg('请输入授权联系人');
                        //     $('#input-partyaLinkPerson').focus();return;
                        // }
                        // if(com.isEmpty(params.partyaMail)){
                        //     layer.msg('请输入电子邮箱');
                        //     $('#input-partyaMail').focus();return;
                        // }
                        request.post('/scm/client/agreement/agreeSign',params).then(res =>{
                            fn.init();
                            layer.confirm('签署成功，是否需要立即下单？',{icon: 1,title:'温馨提示'},function(index){
                                layer.close(index);
                                location.hash = '/order/manage';
                            });
                        });
                    });
                }).catch((err)=>{
                    layer.closeAll();
                    let idx = com.alertWarn(err.message,function () {
                        layer.close(idx);
                        location.hash = '/company/basicInfo';
                    });
                });
            }
        };
    fn.init();
    exports('agreement', fn);
});