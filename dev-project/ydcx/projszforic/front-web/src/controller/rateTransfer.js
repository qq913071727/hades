
/**
 * 同名转账
 */
 layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form , com = layui.com,
        request = layui.request,
        fn = {
            async init(){
                await request.get('/rate/txn/initPay').then(res=>{
                    if(res.code != '1') {
                        $(".transfer-btn").attr("disabled",true);
                    } else {
                        form.val('form-transfer',{
                            "txnCur":res.data.txnCur,
                            "txnCurDesc":res.data.txnCurDesc,
                            "payorAccountNo":res.data.payorAccountNo
                        });
                    }
                });
            },
            async getBeneficiarys(cityName){
                //获取所有的人民币受益人
                await request.postForm('/rate/beneficiary/select?currency=CNY').then(res=>{
                    com.fullSelect({
                        elem:$('#beneficiaryId'),
                        data:res.data,
                        label:'accountName',
                        value:'beneficiaryId',
                        defValue:'',
                        firstLabel:'请选择'
                    });
                    form.render('select','form-transfer');
                });
            },
            getBeneficiaryInfo(beneficiaryId){
                request.get('/rate/beneficiary/getByBeneficiaryId?beneficiaryId=' + beneficiaryId).then(res=>{
                    $("#rate-beneficiary").show();
                    form.val('form-transfer',res.data);
                });
            }
        };
    form.on('select(beneficiaryId)',function (o) {
        fn.getBeneficiaryInfo(o.value);
    });
    form.on('submit(transfer-submit)', function (data) {
        //if(!com.validateForm($('#form-transfer'))){return;}
        let params = data.field;
        request.postForm('/rate/txn/payment',params).then(res => {
            layer.msg('保存成功',{time: 1200},function () {
                location.hash = '/rate/txn/transferList';
            });
        });
    });
    fn.init();
    fn.getBeneficiarys();
    exports('rateTransfer', fn)
})