layui.define(['laytpl','form','staticData','deliveryInformation'], function (exports) {
    var $ = layui.$,request = layui.request, laytpl = layui.laytpl,
   deliveryInformation = layui.deliveryInformation,
    fn = {
        query: function () {
            request.postForm('/scm/client/customerDeliveryInfo/page',{'page':1,'limit':100}).then(res => {
                laytpl($('#tpl-delivery-address-list').html()).render(res.data, function (html) {
                    $('#view-delivery-address-list').html(html);
                });
            });
        },
        //删除客户收货地址
        remove(id){
            layer.confirm('您是否确定要删除该收货地址?',{icon: 3,closeBtn:false,title:'温馨提示'},function(index){
                layer.close(index);
                request.postForm('/scm/client/customerDeliveryInfo/delete',{id:id}).then(res => {
                    fn.query();
                });
            });
        },
        //设为默认
        setDefault(id){
            request.postForm('/scm/customerDeliveryInfo/setDefault',{id:id}).then(res => {
                layer.msg('设为成功');
                //重新加载
                fn.query();
              }); 
        },
        //启用禁用
        enableDisable(id,disabled){
            request.postForm('/scm/customerDeliveryInfo/activeDisable',{id:id,disabled:disabled}).then(res => {
                layer.msg(disabled?'已禁用':'已启用');
                fn.query();
              }); 
        },
        // 新增地址
        add(){
            deliveryInformation.show(null,function () {
               fn.query();
            });
        }
        // 修改地址
        ,edit(id){
            deliveryInformation.show(id,function () {
                fn.query();
            });
        }
    };
    fn.query();
    exports('deliveryAddress', fn);
});