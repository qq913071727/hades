layui.define(['form', 'laytpl', 'applyRefund'], function (exports) {
    var $ = layui.$, com = layui.com, request = layui.request, form = layui.form, laytpl = layui.laytpl,
    applyRefund = layui.applyRefund,
        fn = {
            params: {
                page: 1
                , limit: 10
            }
            ,queryReceipt(){
                let $receipta = $('a[refund-receipt]'),docIds=[];
                $receipta.each(function (i,o) {
                    docIds.push($(o).attr('refund-receipt'));
                });
                if(docIds.length>0){
                    request.postForm('/scm/file/fileSizeGroup',{docId:docIds.join(','),docType:'refund_account',businessType:'refund_receipt'}).then(res =>{
                        $.each(res.data || [],function (i,o) {
                            $('a[refund-receipt="'+o.docId+'"]').attr({'href':'/scm/download/file/'+o.fileId}).show();
                        });
                    });
                }
            }
            , getAvaliableRefundAmount(){
                // 查询客户当前余额
                request.get('/scm/client/transactionFlow/outBalance').then(res =>{
                    let val = res.data;
                    if(val>0){
                        $('#refund-avaliable-refund').html(com.formatCurrency(val));
                        $('#refund-apply-btn').attr({'disabled':null});
                    }else{
                        $('#refund-avaliable-refund').html('0.00');
                        $('#refund-apply-btn').attr({'disabled':true});
                    }
                });
            }
            , query: function () {
                com.showLoading();
                let params = $.extend({}, fn.params, $('#form-refund-list').serializeJson());
                request.postForm('/scm/refundAccount/list', params).then(res => {
                    // 模板渲染
                    laytpl($('#tpl-refund-list').html()).render(res.data, function (html) {
                        $('#tab-refund-list').find('tbody').remove();
                        $('#tab-refund-list').append(html);
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('refund-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#refund-list-pagination').show();
                        $('.blank-list').hide();

                        // 查询水单是否上传
                        fn.queryReceipt();
                    }else{
                        $('#refund-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,judstatus(st){
                if(['waitExamine'].contain(st)){
                    return 1;
                }else if(['waitRefund'].contain(st)){
                    return 2;
                }else if(['refunded'].contain(st)){
                    return 3;
                }
                return 0;
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-refund-list',{'dateCreatedStart':othis.attr('dateStart'),'dateCreatedEnd':othis.attr('dateEnd')});
                fn.clearQuery();
            }
            ,apply(){
                applyRefund.apply(function (){
                    fn.clearQuery();
                    fn.getAvaliableRefundAmount();
                });
            }
        };
    let now = new Date(),dates=[];
    now.setMonth(now.getMonth() - 1);
    form.val('form-refund-list',{'dateCreatedStart':com.formatDate(now,'yyyy-MM-dd')});
    dates.push({'title':'近一个月退款',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    now.setMonth(now.getMonth() - 2);
    dates.push({'title':'近三个月退款',dateStart:com.formatDate(now,'yyyy-MM-dd'),dateEnd:''});
    let year = new Date().getFullYear();
    dates.push({'title':'今年内退款',dateStart:year+'-01-01',dateEnd:''});
    for(var i=1;i<5;i++){
        let yearLast = new Date();
        yearLast.setFullYear(year-i);
        yearLast.setMonth(0);
        yearLast.setDate(1);
        let item = {'title':yearLast.getFullYear()+'年退款',dateStart:com.formatDate(yearLast,'yyyy-MM-dd'),dateEnd:''};
        yearLast.setFullYear(yearLast.getFullYear()+1);
        yearLast.setDate(yearLast.getDate() - 1);
        item.dateEnd = com.formatDate(yearLast,'yyyy-MM-dd');
        dates.push(item);
    }
    let $listDate = $('#refund-list-date'),dataItems=[];
    dates.forEach(o =>{
        dataItems.push('<li><a href="javascript:" dateStart="'+o.dateStart+'" dateEnd="'+o.dateEnd+'"><b></b>'+o.title+'</a></li>');
    });
    $listDate.find('.more-operation-txt div').html(dates[0].title);
    $listDate.find('.more-operation-list ul').html(dataItems.join(''));
    $($listDate.find('.more-operation-list ul li a')[0]).addClass('curr');

    form.render(null, 'form-refund-list');
    fn.clearQuery();
    form.on('submit(search-refund-list)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-refund-list',{'clientStatusId':$(this).attr('status')});
        fn.clearQuery();
    });
    fn.getAvaliableRefundAmount();
    exports('refundList', fn);
});