layui.define(['laytpl'], function (exports) {
    var $ = layui.$, request = layui.request, com = layui.com, search = layui.router().search, laytpl = layui.laytpl, fn = {
        async init(){
            request.get('/rate/beneficiary/detail?id=' + search.id).then(res => {
                laytpl($('#tpl-basic-info').html()).render(res.data, function (html) {
                    $('#basic-info-div').append(html);
                });
                laytpl($('#tpl-bank-info').html()).render(res.data, function (html) {
                    $('#bank-info-div').append(html);
                });
            });
        }
    };
    fn.init();
    exports('rateBeneficiaryDetail', fn);
});