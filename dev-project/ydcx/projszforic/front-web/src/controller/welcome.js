layui.define(['editPhone','editPwd','editMail','companyInfo','bindWx','upload','laytpl','form'],function (exports) {
    var $ = layui.$,com = layui.com,request = layui.request,
        editPhone = layui.editPhone,
        editPwd = layui.editPwd,
        editMail = layui.editMail,
        bindWx = layui.bindWx,
        companyInfo = layui.companyInfo,
        view = layui.view,
        upload = layui.upload,
        laytpl = layui.laytpl,
        fn = {
            // 获取人员信息
            getUserInfo(){
                request.get('/scm/client/person/info').then(res=>{
                    let person = res.data;
                    if(com.isNotEmpty(person.head)){
                        $("#index-user-head").attr('src',res.data.head);
                    }
                    fn.uploadHead();
                });
            }
            // 修改手机
            ,showEditPhone(){
                editPhone.show();
            }
            // 修改密码
            ,showEditPwd(old){
                editPwd.show(old);
            }
            // 修改头像
            ,uploadHead(){
                upload.render({
                    elem: '#index-edit-head',size: 5120,exts: 'jpg|png'
                    ,acceptMime:'image/jpeg,image/png'
                    ,url: '/scm/client/person/uploadHead'
                    ,before: function () {
                        com.showLoading();
                    }
                    ,done: function(res){
                        com.hideLoading();
                        if(com.stateInterception(res)){
                            com.refPerson(res.data);
                            layer.msg('修改成功');
                            setTimeout(function () {location.reload();},500);
                        }
                    }
                    ,error: function(){
                        com.hideLoading();
                    }
                });
            }
            // 修改邮箱
            ,showEditMail(old){
                editMail.show(old,function () {
                    view('TPL_welcome_user_content').refresh();
                });
            }
            // 修改公司信息
            ,showEditCompany(){
                companyInfo.editCompany(function (){
                    view('TPL_welcome_user_content').refresh();
                });
            }
            // 获取需要修改的订单数
            ,editOrderSize() {
                request.get('/scm/client/impOrder/editOrderSize').then(res=>{
                    if(res.data>0){
                        $('#welcome-wait-edit').text(res.data).show();
                        $('#wait-edit-tooltip').show();
                    }
                });
            },
            // 获取账户信息
            getCompanyFinanceInfo() {
                request.postForm('/scm/company/getCompanyFinance').then(res=>{
                    Object.keys(res.data).forEach(key => {
                        let val = res.data[key];
                        $("#welcome-" + key).html((val<0?'-':'').concat('￥').concat(com.formatCurrency(Math.abs(val))));
                    });
                });
            },
            //获取公告信息
            getNotice(){
                request.postForm('/scm/notice/loginList',{page:1,limit:10}).then(res=>{
                    laytpl($('#TPL_welcome_notice').html()).render(res.data, function (html) {
                        $('#view-welcome-notice').html(html);
                    });
                });
            },
            //绑定/换绑微信
            showBindWx(isBindWx) {
                let option = {'title': isBindWx ? "换绑微信" : "绑定微信",'callback':function(){
                        fn.getUserInfo();
                    }};
                bindWx.show(option);
            },
            // 获取客户代理报关 未放行单据数据
            getMyNoReleasedOrder(){
                // if(com.isNotEmpty(localStorage.getItem('WR_no_released_tip')) && com.getNowFormatDate() == localStorage.getItem('WR_no_released_tip')){
                //     return;
                // }
                // request.get('/declare/client/declaration/myNoReleasedOrder').then(res =>{
                //     if(res.data.length>0){
                //        layer.alert('报关订单【'+res.data.join(',')+'】下单已超过24小时尚未放行，点击<a href="javascript:layer.closeAll()" lay-href="/declare/list">立即查看</a> ', {icon: 7,title:'温馨提醒',btn:['朕知道了'], success:function (layero, index){
                //             $(layero).find('.layui-layer-btn a').before('<div class="tps-displayed"><label><input type="checkbox" id="CK_no_released_tip"/> 今日不再提醒</label></div>');
                //             $('#CK_no_released_tip').click(function(){
                //                 localStorage.setItem('WR_no_released_tip',com.getNowFormatDate());
                //                 layer.close(index);
                //             });
                //         }});
                //     }
                // });
            },
            // 检查客户是否存在需要签约的协议
            existSignContract(){
                request.get('/scm/client/agreement/existSignContract').then(res =>{
                    if(res.data==true){
                        location.hash = '/company/agreement';
                    }else{
                        fn.getUserInfo();
                        fn.editOrderSize();
                        fn.getCompanyFinanceInfo();
                        fn.getNotice();
                        fn.getMyNoReleasedOrder();
                    }
                });
            }
        };
    fn.existSignContract();
    exports('welcome', fn);
});