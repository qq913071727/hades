layui.define(['form','table','shortcutKey','uploadList'], function (exports) {
    var $ = layui.$,setter=layui.setter,com = layui.com, request = layui.request,search = layui.router().search,shortcutKey=layui.shortcutKey,uploadList = layui.uploadList,
        form = layui.form,table=layui.table,fileId = 'temp_' + com.uuid(),fn={
            renderContainers:function (data) {
                table.render({
                    elem: '#container-list',id:'container-list',limit:500,page:false,
                    text:{none:'暂无数据'},
                    cols:[[
                        {field:'contSeqNo',title:'项号',width:60,align:'center'}
                        ,{field:'containerNo',title:'集装箱号',width:120}
                        ,{field:'containerMdCode',title:'集装箱规格编号',width:200}
                        ,{field:'containerMdCodeName',title:'集装箱规格'}
                    ]]
                    ,data: data
                });
            },
            async init(){
                let orderId = search.id;
                request.get('/declare/trustOrder/detail',{id:orderId}).then(res =>{
                    let data = res.data,order = data.order;
                    fn.renderContainers(data.containers || []);
                    order.cusIEFlag = 'I'==order.cusIEFlag?'进口':'出口';
                    form.val('form-trust-declare-detail',data.order);
                    form.render(null, 'form-trust-declare-detail');

                    $('.upload-box').attr({'file-id': order.docNo, 'edit': 'false'});
                    uploadList.init();
                }).catch(()=>{
                    location.hash = '/declare/list';
                });
            }
        };
    fn.init();
    exports('trustDeclareDetail', fn);
});