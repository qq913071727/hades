layui.define(['laytpl'], function (exports) {
    var $ = layui.$, laytpl = layui.laytpl, com = layui.com, request = layui.request,
        search = layui.router().search
        ,fn = {
            load(){
                let orderId = search.id;
                if(com.isEmpty(orderId)){
                    layer.msg('请选择您要查看的订单');
                    history.back();
                    return;
                }
                request.postForm('/scm/impOrder/getPrintEntrustData',{id:orderId}).then(res =>{
                    let data = res.data;
                    var total = {'tquantity':0.0,'ttotalPrice':0.0,'tnetWeight':0.0,'tgrossWeight':0.0,'tcartonNum':0};
                    $.each(data.members || [],function (i,o) {
                        total.tquantity = Math.round((total.tquantity + o.quantity)*100)/100;
                        total.ttotalPrice = Math.round((total.ttotalPrice + o.decTotalPrice)*100)/100;
                        total.tnetWeight = Math.round((total.tnetWeight + o.netWeight)*10000)/10000;
                        total.tgrossWeight = Math.round((total.tgrossWeight + o.grossWeight)*10000)/10000;

                        total.tcartonNum = total.tcartonNum + (null == o.cartonNum ? 0 : o.cartonNum);
                    });
                    data['total'] = total;
                    laytpl($('#print-cost-temp').html()).render(data,function (html) {
                        $('#print-cost-view').html(html);
                    });
                }).catch(()=>{
                    history.back();
                });
            },
        };
        fn.load();
    exports('printEntrust', fn);
});