layui.define(['form','laytpl'], function (exports) {
    var $ = layui.$,com = layui.com, request = layui.request,form=layui.form,laytpl = layui.laytpl,fn={
            params: {
                page: 1
                , limit: 10
            },
            list:[]
            ,async queryDecFile(){
                let $files = $('div[down-declare]'),docIds=[];
                $files.each(function (i,o) {
                    docIds.push($(o).attr('down-declare'));
                });
                if(docIds.length>0){
                    await request.postForm('/scm/file/fileSizeGroup',{docId:docIds.join(','),docType:'declaration',businessType:'DEC_original'}).then(res =>{
                        $.each(res.data || [],function (i,o) {
                            $('div[down-declare="'+o.docId+'"]').append('<a href="/scm/download/file/'+o.fileId+'" class="def-a" target="_blank">报关单</a>&nbsp;&nbsp;');
                        });
                    });
                    await request.postForm('/scm/file/fileSizeGroup',{docId:docIds.join(','),docType:'declaration',businessType:'DEC_goods'}).then(res =>{
                        $.each(res.data || [],function (i,o) {
                            $('div[down-declare="'+o.docId+'"]').append('<a href="/scm/download/file/'+o.fileId+'" class="def-a" target="_blank">附加页</a>&nbsp;&nbsp;');
                        });
                    });
                    await request.postForm('/scm/file/fileSizeGroup',{docId:docIds.join(','),docType:'declaration',businessType:'DEC_notice'}).then(res =>{
                        $.each(res.data || [],function (i,o) {
                            $('div[down-declare="'+o.docId+'"]').append('<a href="/scm/download/file/'+o.fileId+'" class="def-a" target="_blank">放行单</a>');
                        });
                    });
                }
            }
            ,query: function () {
                com.showLoading();
                fn.list = [];
                let params = $.extend({},fn.params,$('#form-order-list').serializeJson());
                request.post('/declare/trustOrder/clientPage',params).then(res => {
                    fn.list = res.data;
                    // 模板渲染
                    laytpl($('#tpl-order-list').html()).render(res.data, function (html) {
                        $('#tab-order-list').find('tbody').remove();
                        $('#tab-order-list').append(html);
                        // 重新渲染表单内表单数据
                        com.chooseCheckbox($('#checkbox-order-list-check-all'),false);
                        form.render(null, 'form-order-list');
                        // 滚动条置顶
                        layui.view().container.scrollTop(0);
                        // 初始化复制功能
                        com.createCopyContent();
                    });
                    // 开启分页
                    com.pagination('order-list-pagination', res.count, fn.params, function () {
                        fn.query();
                    });
                    if(res.count>0){
                        $('#order-list-pagination').show();
                        $('.blank-list').hide();
                        fn.queryDecFile();
                    }else{
                        $('#order-list-pagination').hide();
                        $('.blank-list').show();
                    }
                });
            }
            ,clearQuery(){
                fn.params.page = 1;
                fn.params.limit = 10;
                fn.query();
            }
            ,setDate(othis){
                form.val('form-order-list',{'dateStart':othis.attr('orderDateStart'),'dateEnd':othis.attr('orderDateEnd')});
                fn.clearQuery();
            }
        };
    fn.clearQuery();
    form.on('submit(search-order-list)',function () {
        fn.clearQuery();
    });
    // 状态查询
    $('#queryStatus').find('a[status]').click(function () {
        $('#queryStatus').find('a[status]').removeClass('curr');
        $(this).addClass('curr');
        form.val('form-order-list',{'statusId':$(this).attr('status')});
        fn.clearQuery();
    });
    form.render(null, 'form-order-list');
    exports('trustDeclareList', fn);
});