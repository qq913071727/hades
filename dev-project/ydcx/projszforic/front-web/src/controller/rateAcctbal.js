layui.define(['form','laytpl'], function (exports) {
    var $ = layui.$,com = layui.com, request = layui.request,form=layui.form,laytpl = layui.laytpl,search = layui.router().search,
    fn={
            layer_index:0,
            list:[]
            ,clearQuery(){
                fn.query();
            }
            ,async query(){
                com.showLoading();
                fn.list = [];
                request.postForm('/rate/txn/queryBalance').then(res => {
                    fn.list = res.data;
                    // 模板渲染
                    laytpl($('#tpl-acctbal-list').html()).render(res.data, function (html) {
                        $('#tab-acctbal-list').find('tbody').remove();
                        $('#tab-acctbal-list').append(html);
                    });
                    if(res.data != null && res.data.length > 0){
                        $('.blank-list').hide();
                    }else{
                        $('.blank-list').show();
                    }
                });
            }
        };
    fn.clearQuery();
    exports('rateAcctbal', fn);
});