layui.extend({
    com: 'lib/common',
    request: 'lib/request'
}).define(['com','request','laytpl','laypage'], function (exports) {
    var $ = layui.jquery,com = layui.com,request=layui.request,laytpl=layui.laytpl,laypage=layui.laypage
        , fn = {
        params:{
            page:1,
            limit:10,
            category:'policies'
        }
        ,init(){
            let $items = $('.news-list-tab').find('.tab-item'),$bra=$('.news-list-tab').find('.tab-bar');
            $items.click(function () {
                if(fn.params.category!=$(this).attr('id')){
                    fn.params.category = $(this).attr('id');
                    $items.removeClass('active');
                    $(this).addClass('active');
                    $.each($items,function (i,o) {
                        if(fn.params.category==$(o).attr('id')){
                            $bra.css({'transform':'translateX('+(289*i)+'px)'});
                        }
                    });
                    fn.clear();
                }
            });
            if(com.isNotEmpty(com.getUrlParam('t'))){
                let $tabItem = $('#'+com.getUrlParam('t'));
                if($tabItem.length==1){
                    fn.params.category='';
                    $tabItem.click();
                }else{
                    fn.clear();
                }
            }else{
                fn.clear();
            }
        }
        ,clear(){
            fn.params.page = 1;
            fn.query();
        }
        ,query: function () {
            request.postForm('/scm/notice/list',fn.params).then(res =>{
                laytpl($('#tpl-new-item').html()).render(res, function (html) {
                    $('#view-new-item').html(html);

                    if(res.count>0){
                        laypage.render({
                            elem: 'new-list-page'
                            ,count: res.count
                            ,limit: fn.params.limit
                            ,curr: fn.params.page
                            ,theme: '#0ba9f9'
                            ,jump: function(obj, first){
                                if(!first){
                                    fn.params.page = obj.curr;
                                    fn.query();
                                }
                            }
                        });
                    }
                });
            });
        }
    };
    fn.init();
    exports('newsList', fn);
});