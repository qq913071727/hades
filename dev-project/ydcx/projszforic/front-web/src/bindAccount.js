layui.extend({
    com: 'lib/common'
    , request: 'lib/request'
    , staticData: 'lib/extend/staticData'
}).define(['form','com', 'request','staticData'], function (exports) {
    var $ = layui.jquery, form = layui.form,com = layui.com, request = layui.request, staticData = layui.staticData,
    original = (com.getUrlParam('original') || '/manage.html#/'),bindAccessId=com.getUrlParam('bindAccessId'),
    $obtainSms = $('#obtainSms');
    let $phoneNo = $('#phoneNo');
    $phoneNo.focus();
    let fn = {
        codeSeconds:60,
        getSmsCode: function() {
            if (com.isEmpty($phoneNo.val())) {
                $phoneNo.focus();
                layer.msg('请输入手机号');
                return;
            }
            if (!com.reg.phone.test($phoneNo.val())) {
                $phoneNo.focus();
                layer.msg('请输入正确的手机号码');
                return;
            }
            $obtainSms.attr({'disabled':true});
            request.post('/scm/auth/sendVerificationCode',{phoneNo:$phoneNo.val()}).then(res => {
                layer.msg('发送成功');
                com.setLocalStorage('$sendLoginMessageTime', moment().valueOf() / 1000);
                fn.handleLoginMessageTime(59);
            }).catch(err => {
                $obtainSms.attr({'disabled':null}).removeClass('layui-btn-disabled');
            });
        }
        ,handleLoginMessageTime: function(time) {
            let timer = setInterval(() => {
                if (time === 0) {
                    clearInterval(timer);
                    $obtainSms.attr({'disabled':null}).html('获取短信验证码');
                } else {
                    fn.codeSeconds = time;
                    $obtainSms.attr({'disabled':true}).html(fn.codeSeconds+'秒后再次发送');
                    time--;
                }
            }, 1000);
        }
    };
    $obtainSms.click(function () {
        fn.getSmsCode();
    });
    form.verify({
       phoneNo: function (value, item) {
           if (com.isEmpty(value)) {
               return '请输入手机号';
           }
           if (!com.reg.phone.test(value)) {
               return '请输入正确的手机号码';
           }
        }, messageCode: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入短信验证码';
            }
        }
    });
    form.on('submit(bind-login-btn)', function (data) {
        let params = $('#loginForm').serializeJson();
        let reqparams = $.extend({},params,{
            loginType:1,
            userName:params.phoneNo,
            bindAccessId:bindAccessId
        });
        request.post('/scm/auth/login', reqparams).then(res => {
            let data = res.data;
            com.setLocalStorageJson('$user', data);
            staticData.clearAll();
            layer.msg('登录成功');
            setTimeout(function () {
                if (original.startsWith('/')) {
                    location.href = '/manage.html#/';
                } else {
                    location.href = original;
                }
            }, 1000);
        });
        return false;
    });
    //发送短信验证码计时器
    const time = moment().valueOf() / 1000 - com.getLocalStorage('$sendLoginMessageTime');
    if (time < 60) {
        fn.codeSeconds = fn.codeSeconds - parseInt(time, 10);
        fn.handleLoginMessageTime(fn.codeSeconds);
    }
    exports('bindAccount', {});
});