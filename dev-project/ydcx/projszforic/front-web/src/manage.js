/**
 * 后台管理框架页面
 */
layui.extend({
    com: 'lib/common' //通用模块
    ,request: 'lib/request' //请求模块
    ,admin: 'lib/admin' //核心模块
}).define(['com','admin','request'], function(exports){
    var setter = layui.setter
        ,com = layui.com
        ,admin = layui.admin
        ,view = layui.view
        ,element = layui.element
        //根据路由渲染页面
        ,renderPage = function(){
            var router = layui.router()
                ,path = router.path
                ,pathURL = admin.correctRouter(router.path.join('/'))
            //默认读取主页
            if(!path.length) path = [''];
            //如果最后一项为空字符，则读取默认文件
            if(path[path.length - 1] === ''){
                path[path.length - 1] = setter.entry;
            }
            //请求视图渲染
            view().render(path.join('/')).then(function(res){
                this.container.scrollTop(0);// 跳转时重置滚动条
                var layerOpen = $('.layui-layer')[0];
                layerOpen && layer.closeAll('tips');
            }).done(function(){
                element.render('breadcrumb', 'breadcrumb');
                com.init();
                this.container.on('scroll',function () {
                    var layerOpen = $('.layui-layer')[0];
                    layerOpen && layer.closeAll('tips');
                })
            });
        }

        //入口页面
        ,entryPage = function(fn){
            var router = layui.router()
                ,container = view(setter.container)
                ,pathURL = admin.correctRouter(router.path.join('/'))
                ,isIndPage;

            //检查是否属于独立页面
            layui.each(setter.indPage, function(index, item){
                if(pathURL === item){
                    return isIndPage = true;
                }
            });

            //将模块根路径设置为 controller 目录
            layui.config({
                base: setter.base + 'controller/'
            });
            //独立页面
            if(isIndPage || pathURL === '/user/login'){ //此处单独判断登入页，是为了兼容旧版（即未在 config.js 配置 indPage 的情况）
                container.render(router.path.join('/')).done(function(){
                    admin.pageType = 'alone';
                });
            } else { //后台框架页面

                //强制拦截未登入
                if(setter.interceptor){
                    var local = layui.data(setter.tableName);
                    if(!local[setter.request.tokenName]){
                        return location.hash = '/user/login/redirect='+ encodeURIComponent(pathURL); //跳转到登入页
                    }
                }
                //渲染后台结构
                if(admin.pageType === 'console') { //后台主体页
                    renderPage();
                } else { //初始控制台结构
                    container.render('layout').done(function(){
                        renderPage();
                        layui.element.render();
                        admin.pageType = 'console';
                        com.zmLoad();
                    });
                }

            }
        }
        ,$ = layui.$;

    //初始主体结构
    layui.link(
        setter.base + 'style/manage.css?v='+ setter.version
        ,function(){
            entryPage()
        }
        ,'manage'
    );

    //监听Hash改变
    window.onhashchange = function(){
        entryPage();
        //执行 {setter.MOD_NAME}.hash 下的事件
        layui.event.call(this, setter.MOD_NAME, 'hash({*})', layui.router());
        //清除所有的定时器
        let optask = localStorage.getItem('optask') || '[]';
        let optaskArray = JSON.parse(optask);
        for(var i = 0;i < optaskArray.length;i++){
            clearInterval(optaskArray[i]);
        }
        localStorage.removeItem('optask');
    };
    //扩展 lib 目录下的其它模块
    layui.each(setter.extend, function(index, item){
        var mods = {};
        mods[item] = '{/}' + setter.base + 'lib/extend/' + item;
        layui.extend(mods);
    });
    //对外输出
    exports('manage', {
        render: renderPage
    });
});
