{
    "code": "1",
    "message": "",
    "data": [{
    "title": "委托进口",
    "icon": "iconfont iconicon",
    "list": [{
        "title": "快捷下单",
        "jump": "order/manage"
    }, {
        "title": "订单列表",
        "jump": "order/list"
    }]
},
    {
        "title": "我的财务",
        "icon": "iconfont iconcaiwucopy",
        "list": [{
            "title": "我的付汇",
            "jump": "finance/payList"
        }, {
            "title": "账务明细",
            "jump": "finance/transactionList"
        },
            {
                "title": "我的发票",
                "jump": "finance/invoceList"
            },
            {
                "title": "我的付款",
                "jump": "finance/paymentList"
            },
            {
                "title": "我的退款",
                "jump": "finance/refundList"
            }
        ]
    },
    {
        "title": "我的仓库",
        "icon": "iconfont iconcangku",
        "list": [{
            "title": "境外入库",
            "jump": "wms/receivingnote"
        }, {
            "title": "仓库信息",
            "jump": "wms/ware"
        }]
    },
    {
        "title": "产品中心",
        "icon": "iconfont iconchanpin",
        "list": [{
            "title": "我的产品",
            "jump": "product/myProduct"
        }]
    },
    {
        "title": "供 应 商 ",
        "icon": "iconfont icongonghuoshang",
        "list": [{
            "title": "我的供应商",
            "jump": "supplier/mySupplier"
        }]
    },
    {
        "title": "客户中心",
        "icon": "iconfont iconkehu",
        "list": [
            {
                "title": "公司资料",
                "jump": "company/basicInfo"
            },
            {
                "title": "协议签约",
                "jump": "company/agreement"
            },
            {
                "title": "收提货地址",
                "jump": "company/deliveryAddress"
            },
            {
                "title": "用户设置",
                "jump": "user/setting"
            }
        ]
    }
]
}