var fcsc = {"1":"进口许可证", "2":"两用物项和技术进口许可证", "3":"两用物项和技术出口许可证", "4":"出口许可证", "5":"纺织品临时出口许可证", "6":"旧机电产品禁止进口", "7":"自动进口许可证", "8":"禁止出口商品", "9":"禁止进口商品", "A":"入境检验检疫", "B":"出境检验检疫（电子底账）", "D":"毛坯钻石进出境检验", "E":"濒危物种允许出口证明书", "F":"濒危物种允许进口证明书", "G":"两用物项和技术出口许可证(定向)", "H":"港澳OPA纺织品证明", "I":"麻醉药品精神药物进(出)口准许证", "J":"黄金及黄金制品进出口准许证", "L":"药品进出口准许证", "M":"密码产品和设备进口许可证", "O":"自动进口许可证(新旧机电产品)", "P":"固体废物进口许可证", "Q":"进口药品通关单", "R":"进口兽药通关单", "S":"农药进出口登记管理放行通知单", "T":"银行调运外币现钞进出境证明文件", "U":"合法捕捞产品通关证明", "V":"人类遗传资源材料出口、出境证明", "X":"有毒化学品进出口环境管理放行通知单", "Y":"原产地证明", "Z":"赴境外加工光盘进口备案证明", "b":"进口广播电影电视节目带(片)提取单", "c":"内销征税联系单", "d":"援外项目任务通知函", "e":"关税配额外优惠税率进口棉花配额证", "f":"音像制品（成品）进口批准单", "g":"技术出口合同登记证", "h":"核增核扣表", "i":"技术出口许可证", "k":"民用爆炸物品进出口审批单", "m":"银行调运人民币现钞进出境证明", "n":"音像制品（版权引进）批准单", "q":"国别关税配额证明", "r":"预归类标志", "s":"适用ITA税率的商品用途认定证明", "t":"关税配额证明", "u":"钟乳石出口批件", "v":"自动进口许可证(加工贸易)", "x":"出口许可证(加工贸易)", "y":"出口许可证(边境小额贸易)", "z":"古生物化石出境批件"};
var fiaqr = {"L":"民用商品入境验证（3C）", "M":"进口商品检验", "N":"出口商品检验", "P":"进境动植物、动植物产品检疫", "Q":"出境动植物、动植物产品检疫", "R":"进口食品卫生监督检验", "S":"出口食品卫生监督检验", "V":"进境卫生检疫", "W":"出境卫生检疫"};
var $ = layui.$
var com = layui.com;
function formatCsc() {
    var $ts = $('.format-csc');
    conformat($ts,fcsc);
}
function formatIaqr() {
    var $ts = $('.format-iaqr');
    conformat($ts,fiaqr);
}
function conformat($ts,odata) {
    $.each($ts,function (i,o) {
        var $t = $(o),v = $t.text();
        if(!com.isEmpty(v)){
            var vs = v.split(','),fv=[];
            $.each(vs,function (i,o) {
                if(odata[o]&&!com.isEmpty(odata[o])){
                    fv.push('<div>'+o+'：'+odata[o]+'</div>');
                }
            });
            $t.html('<a href="javascript:" class="def-a">'+v+'</a>');
            if(fv.length>0){
                $t.mouseover(function () {
                    layer.tips(fv.join(''), $t, {
                        tips: [1,'#0ba9f9'],time:3000
                    });
                });
            }
        }
    });
}