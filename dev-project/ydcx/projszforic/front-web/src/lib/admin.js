
layui.define('view', function(exports){
    var $ = layui.jquery
        ,setter = layui.setter
        ,view = layui.view
        ,$body = $('body'),THIS = 'layui-this',SIDE_MENU = 'LAY-system-side-menu'
        //通用方法
        ,admin = {
            v: '2.3.0'
            //数据的异步请求
            ,req: view.req
            //清除本地 token，并跳转到登入页
            ,exit: view.exit

            //事件监听
            ,on: function(events, callback){
                return layui.onevent.call(this, setter.MOD_NAME, events, callback);
            }

            //弹出面板
            ,popup: view.popup

            //纠正路由格式
            ,correctRouter: function(href){
                if(!/^\//.test(href)) href = '/' + href;

                //纠正首尾
                return href.replace(/^(\/+)/, '/')
                    .replace(new RegExp('\/' + setter.entry + '$'), '/'); //过滤路由最后的默认视图文件名（如：index）
            }
        };
    //事件
    var events = admin.events = {
        //刷新
        refresh: function(){
            layui.manage.render();
        }
        //返回上一页
        ,back: function(){
            history.back();
        }
    };

    admin.on('hash(side)', function(router){
        var path = router.path, getData = function(item){
                return {
                    list: item.children('.layui-nav-child')
                    ,name: item.data('name')
                    ,jump: item.data('jump')
                }
            }
            ,sideMenu = $('#'+ SIDE_MENU)
            ,SIDE_NAV_ITEMD = 'layui-nav-itemed'

            //捕获对应菜单
            ,matchMenu = function(list){
                var pathURL = admin.correctRouter(path.join('/'));
                list.each(function(index1, item1){
                    var othis1 = $(item1)
                        ,data1 = getData(othis1)
                        ,listChildren1 = data1.list.children('dd')
                        ,matched1 = path[0] == data1.name || (index1 === 0 && !path[0])
                        || (data1.jump && pathURL == admin.correctRouter(data1.jump));

                    listChildren1.each(function(index2, item2){
                        var othis2 = $(item2)
                            ,data2 = getData(othis2)
                            ,listChildren2 = data2.list.children('dd')
                            ,matched2 = (path[0] == data1.name && path[1] == data2.name)
                            || (data2.jump && pathURL == admin.correctRouter(data2.jump));

                        listChildren2.each(function(index3, item3){
                            var othis3 = $(item3)
                                ,data3 = getData(othis3)
                                ,matched3 = (path[0] ==  data1.name && path[1] ==  data2.name && path[2] == data3.name)
                                || (data3.jump && pathURL == admin.correctRouter(data3.jump))

                            if(matched3){
                                var selected = data3.list[0] ? SIDE_NAV_ITEMD : THIS;
                                othis3.addClass(selected).siblings().removeClass(selected); //标记选择器
                                return false;
                            }

                        });

                        if(matched2){
                            var selected = data2.list[0] ? SIDE_NAV_ITEMD : THIS;
                            othis2.addClass(selected).siblings().removeClass(selected); //标记选择器
                            return false
                        }

                    });

                    if(matched1){
                        // var selected = data1.list[0] ? SIDE_NAV_ITEMD : THIS;
                        // othis1.addClass(selected).siblings().removeClass(selected); //标记选择器
                        return false;
                    }

                });
            }

        //重置状态
        sideMenu.find('.'+ THIS).removeClass(THIS);
        //开始捕获
        matchMenu(sideMenu.children('li'));
    });

    //页面跳转
    $body.on('click', '*[lay-href]', function(){
        var othis = $(this),href = othis.attr('lay-href');
        //执行跳转
        location.hash = admin.correctRouter(href);
    });
    //点击事件
    $body.on('click', '*[layadmin-event]', function(){
        var othis = $(this)
            ,attrEvent = othis.attr('layadmin-event');
        events[attrEvent] && events[attrEvent].call(this, othis);
    });
    //接口输出
    exports('admin', admin);
});
