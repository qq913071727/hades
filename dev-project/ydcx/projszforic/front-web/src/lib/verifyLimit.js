/**
 * 请求频繁验证
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form,
        setter = layui.setter,
        fn = {
            layer_index: 0,
            callBack:null,
            show: function(callBack){
                fn.callBack = callBack;
                $.get('/views/sub/verify-request.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: '验证'
                        , id: 'win-verify-request'
                        , type: 1
                        , resize: false
                        , shadeClose: false
                        , area: ['550px', '220px']
                        , content: html
                    });
                    fn.getVerifyCode();   
                });
                form.on('submit(verify-request)', function (data) {
                    let params = data.field;
                    if(params.verifyCode == null || params.verifyCode == ''){
                        layer.msg("请先填写图片验证码")
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: '/validate/checkLimit?verifyCode=' + params.verifyCode + "&limitRequestId=" + localStorage.getItem('limitRequestId'),
                        dataType: "json",
                        headers : {
                            
                        },
                        success: function(res){
                           if(res.code != '1') {
                              layer.msg(res.message || '您的验证不通过，请重新验证或稍后再试');
                              fn.getVerifyCode();
                           } else {
                              localStorage.removeItem('limitRequestId');
                              layer.close(fn.layer_index);
                              //重新刷新页面
                              layui.manage.render();
                           }
                        }
                    });        
                });
            }, 
            getVerifyCode: function(){
                let limitRequestId = localStorage.getItem('limitRequestId');
                $("#verify_img_code").attr("src", '/scm/kaptcha/limit?limitRequestId=' + limitRequestId);
                localStorage.setItem('limitRequestId', limitRequestId);
            },
        };
    $('#verify_img_code').click(function (){
       fn.getVerifyCode();
    }).click();
    exports('verifyLimit', fn)
})