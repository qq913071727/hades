/**
 * 变更密码
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer,
        form = layui.form ,
        setter = layui.setter,
        request = layui.request,
        com = layui.com,fn = {
        layer_index: 0,
        async show(old) {
            await $.get('/views/sub/edit-pwd.html',{'v':setter.version},function (html) {
                fn.layer_index = layer.open({
                    title: old?'修改密码':'设置密码'
                    , id: 'win-edit-pwd'
                    , type: 1
                    , resize: false
                    , shadeClose: true
                    , area: ['550px', 'auto']
                    , content: html
                    , success: function () {
                        if(old){
                            $('#edit-pwd-old-pwd').focus();
                        }else{
                            $('#old-password-v').hide();
                            $('#new-pwd-tip').show();
                            $('#edit-pwd-new-pwd').focus();
                        }
                    }
                });
            });
            form.verify({
                'edit-pwd-old-pwd': function (value, item) {
                    if(old && com.isEmpty(value)){
                        return '请输入原始密码';
                    }
                }
                ,'edit-pwd-new-pwd': function (value, item) {
                    if (com.isEmpty(value)) {
                        return '请输入新密码';
                    }
                    if (value.length<6) {
                        return '密码最低六位';
                    }
                    if(old && value==$('#edit-pwd-old-pwd').val()){
                        return '新密码不能与原始密码一致';
                    }
                }
                ,'edit-pwd-confirm-pwd': function (value, item) {
                    if (com.isEmpty(value)) {
                        return '请输入确认密码';
                    }
                    if(value!=$('#edit-pwd-new-pwd').val()){
                        return '两次密码输入不一致';
                    }
                }
            });
            //修改密码
            form.on('submit(save-edit-pwd)',function (data) {
                request.post('/scm/auth/'.concat(old?'clientModifyPassWord':'clientSetPassWord'),data.field).then(()=>{
                    layer.close(fn.layer_index);
                    layer.msg((old?'密码修改':'密码设置')+'成功，请您重新登录',{time:2000},function () {
                        com.logout();
                    });
                });
            });
        }
     };
    exports('editPwd', fn)
});