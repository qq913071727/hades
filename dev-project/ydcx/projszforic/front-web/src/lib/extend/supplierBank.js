/**
 * 供应商银行
 */
layui.define(['layer', 'form','autocomplete','staticData'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form , com = layui.com,
        request = layui.request,
        autocomplete = layui.autocomplete,
        setter = layui.setter,
        staticData = layui.staticData,
        fn = {
            layer_index: 0,
            callBack:null,
            async show(id,supplierName,callBack) {
                await $.get('/views/sub/supplier-bank-manage.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: "供应商银行维护"
                        , id: 'win-supplier-bank'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['600px', 'auto']
                        , content: html
                    });
                });
                let supplierBank = {
                    supplierName:supplierName || '',
                    isDefault:true
                };

                //修改时
                if(com.isNotEmpty(id)){
                    await request.get('/scm/supplierBank/detail',{id:id}).then(res =>{
                        supplierBank = res.data;
                    }).catch(()=>{
                        layer.close(fn.layer_index);
                    });
                }

                form.val('form-supplier-bank-manage',supplierBank);
                // 供应商模糊查询
                autocomplete.render({
                    elem: $("#supplier-bank-manage-supplier-name"),
                    url: '/scm/client/supplier/vague',
                    template_txt: '{{d.name}}',
                    template_val: '{{d.name}}',
                    ajaxParams: {
                        type: 'post',
                        contentType: 'application/x-www-form-urlencoded'
                    },
                    request: {
                        keywords: 'name'
                    }
                });
                // 币制模糊查询
                let currencyData = await staticData.getCurrencyData();
                autocomplete.render({
                    elem: $("#supplier-bank-manage-currency-name"),
                    template_txt: '{{d.label}}',
                    template_val: '{{d.label}}',
                    result: currencyData
                });

                fn.callBack = callBack;
            }
        };

    form.on('submit(add-supplier-bank-manage-save)', function (data) {
        let params = data.field;
        params.isDefault = com.isNotEmpty(params.isDefault);
        request.postForm('/scm/client/supplierBank/'.concat(com.isEmpty(params.id)?'add':'edit'),params).then(res => {
            layer.close(fn.layer_index);
            if(fn.callBack){
                if(com.isEmpty(params.id)){
                    params.id = res.data;
                }
                params.name = com.trim(params.name);
                eval(fn.callBack).call(this,params);
            }
        });
    });
    exports('supplierBank', fn)
});