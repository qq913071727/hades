/**
 * 供应商提货地址
 */
layui.define(['layer', 'form','autocomplete','staticData'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form , com = layui.com,
        request = layui.request,
        setter = layui.setter,
        autocomplete = layui.autocomplete,
        staticData = layui.staticData,$province,$city,$area,
        fn = {
            layer_index: 0,
            callBack:null,
            async show(id,supplierName,callBack) {
                await $.get('/views/sub/take-information-manage.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: "提货地址维护"
                        , id: 'win-take-infomation'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['500px', 'auto']
                        , content: html
                        , success: function () {
                            //$('#add-supplier-name').focus();
                        }
                    });
                });
                $province = $('#take-info-manage-province'),
                    $city = $('#take-info-manage-city'),
                    $area = $('#take-info-manage-area'),detailInfo={
                    supplierName:supplierName||'',
                    provinceName:'香港',
                    cityName:'香港',
                    areaName:'',
                    isDefault:true
                };
                if(com.isNotEmpty(id)){
                    await request.get('/scm/supplierTakeInfo/detail',{id:id}).then(res =>{
                        detailInfo = res.data;
                    }).catch(()=>{
                        layer.close(fn.layer_index);
                    });
                }
                //获取省份信息
                let provinces = await staticData.getAllProvinces();
                com.fullSelect({
                    elem:$province,
                    data:provinces,
                    label:'name',
                    value:'name',
                    defValue:detailInfo.provinceName,
                    firstLabel:'请选择'
                });
                await fn.getCitys(detailInfo.cityName);
                await fn.getAreas(detailInfo.areaName);
                form.val('form-take-information-manage',detailInfo);
                form.render(null,'form-take-information-manage');

                // 供应商模糊查询
                autocomplete.render({
                    elem: $("#take-info-manage-supplier-name"),
                    url: '/scm/client/supplier/vague',
                    template_txt: '{{d.name}}',
                    template_val: '{{d.name}}',
                    ajaxParams: {
                        type: 'post',
                        contentType: 'application/x-www-form-urlencoded'
                    },
                    request: {
                        keywords: 'name'
                    }
                });
                fn.callBack = callBack;
            }
            ,async getCitys(cityName){
                let provinceName = $province.val();
                $city.get(0).length = 0;
                $area.get(0).length = 0;
                if(com.isNotEmpty(provinceName)){
                    let citys = await staticData.getCitys(provinceName);
                    com.fullSelect({
                        elem:$city,
                        data:citys,
                        label:'name',
                        value:'name',
                        defValue:cityName||'',
                        firstLabel:'请选择'
                    });
                }
                form.render('select','form-take-information-manage');
            }
            ,async getAreas(areaName){
                let provinceName = $province.val(),cityName = $city.val();
                $area.get(0).length = 0;
                if(com.isNotEmpty(provinceName)&&com.isNotEmpty(cityName)){
                    let areas = await staticData.getAreas(provinceName,cityName);
                    com.fullSelect({
                        elem:$area,
                        data:areas,
                        label:'name',
                        value:'name',
                        defValue:areaName||'',
                        firstLabel:'请选择'
                    });
                }
                form.render('select','form-take-information-manage');
            }
        };
    form.on('select(take-info-manage-province)',function () {
        fn.getCitys();
    });
    form.on('select(take-info-manage-city)',function () {
        fn.getAreas();
    });
    form.on('submit(add-take-info-manage-save)', function (data) {
        let params = data.field;
        params.disabled = false;
        params.isDefault = com.isNotEmpty(params.isDefault);
        request.postForm('/scm/client/supplierTakeInfo/'.concat(com.isEmpty(params.id)?'add':'edit'),params).then(res => {
            layer.close(fn.layer_index);
            if(fn.callBack){
                params.id = res.data;
                eval(fn.callBack).call(this,params);
            }
        });
    });
    exports('takeInformation', fn)
})