/**
 * 变更手机号
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer,
        form = layui.form ,
        setter = layui.setter,
        request = layui.request,
        com = layui.com,fn = {
        layer_index: 0,
        codeSeconds:60,
        async show() {
            await $.get('/views/sub/edit-phone.html',{'v':setter.version},function (html) {
                fn.layer_index = layer.open({
                    title: "修改手机号"
                    , id: 'win-edit-phone'
                    , type: 1
                    , resize: false
                    , shadeClose: true
                    , area: ['550px', 'auto']
                    , content: html
                    , success: function () {
                        let phone = com.getPerson().phone;
                        if(com.isNotEmpty(phone)){
                            $('#edit-origin-phone').text(phone.substr(0,3).concat('****').concat(phone.substr(7,4)));
                        }else{
                            layer.close(fn.layer_index);
                        }
                        $('#edit-phone-verify-code').focus();
                    }
                });
            });
            let $getVerifyCode = $('#get-sms-verify-code'),$getBindCode = $('#get-sms-bind-code'),
                time1 = moment().valueOf() / 1000 - com.getLocalStorage('$editPhoneVerify'),
                time2 = moment().valueOf() / 1000 - com.getLocalStorage('$editPhoneBind');
            if (time1 < 60) {
                $getVerifyCode.attr({'disabled':true}).addClass('layui-btn-disabled');
                fn.codeSeconds = fn.codeSeconds - parseInt(time1, 10);
                fn.handleMessageTime(fn.codeSeconds,$getVerifyCode);
            }
            if (time2 < 60) {
                $getBindCode.attr({'disabled':true}).addClass('layui-btn-disabled');
                fn.codeSeconds = fn.codeSeconds - parseInt(time2, 10);
                fn.handleMessageTime(fn.codeSeconds,$getBindCode);
            }
            // 发送解绑短信验证
            $getVerifyCode.click(function () {
                $getVerifyCode.attr({'disabled':true}).addClass('layui-btn-disabled');
                request.postForm('/scm/client/person/changePhoneSendCode').then(()=>{
                    layer.msg('发送成功');
                    com.setLocalStorage('$editPhoneVerify', moment().valueOf() / 1000);
                    fn.handleMessageTime(59,$getVerifyCode);
                    $('#edit-phone-verify-code').focus();
                }).catch(err => {
                    $getVerifyCode.attr({'disabled':null}).removeClass('layui-btn-disabled');
                });
            });
            // 发送绑定短信验证
            $getBindCode.click(function () {
                let $newPhoneNo = $('#edit-phone-bind-phone-new');
                if (com.isEmpty($newPhoneNo.val())) {
                    $newPhoneNo.focus();
                    layer.msg('请输入手机号');
                    return;
                }
                if (!com.reg.phone.test($newPhoneNo.val())) {
                    $newPhoneNo.focus();
                    layer.msg('请输入正确的手机号码');
                    return;
                }
                $getBindCode.attr({'disabled':true}).addClass('layui-btn-disabled');
                request.postForm('/scm/client/person/changePhoneBindSendCode',{phoneNo:$newPhoneNo.val()}).then(()=>{
                    layer.msg('发送成功');
                    com.setLocalStorage('$editPhoneBind', moment().valueOf() / 1000);
                    fn.handleMessageTime(59,$getBindCode);
                    $('#edit-phone-bind-code').focus();
                }).catch(err => {
                    $getBindCode.attr({'disabled':null}).removeClass('layui-btn-disabled');
                });
            });
        }
        , handleMessageTime(time,$obtainSms) {
                let timer = setInterval(() => {
                    if (time === 0) {
                        clearInterval(timer);
                        $obtainSms.attr({'disabled':null}).removeClass('layui-btn-disabled').html('获取短信验证码');
                    } else {
                        fn.codeSeconds = time;
                        $obtainSms.attr({'disabled':true}).addClass('layui-btn-disabled').html(fn.codeSeconds+'秒后再次发送');
                        time--;
                    }
                }, 1000);
         }
     };
    form.verify({
        phoneNo: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入手机号';
            }
            if (!com.reg.phone.test(value)) {
                return '请输入正确的手机号码';
            }
        }, messageCode: function (value, item) {
            if (com.isEmpty(value)) {
                return '请输入短信验证码';
            }
        }
    });
    //验证短信验证码是否正确
    form.on('submit(next-edit-verify-phone)',function (data) {
        request.postForm('/scm/client/person/changePhoneValidate',{'validateCode':data.field.validateCode}).then(()=>{
            $('#form-edit-verify-phone').hide();
            $('#form-edit-bind-phone').show();
            $('#win-edit-phone').css({'height':$('#form-edit-bind-phone').outerHeight()+'px'});
        });
    });
    //修改手机号
    form.on('submit(save-edit-bind-phone)',function (data) {
        request.postForm('/scm/client/person/changePhone',data.field).then(()=>{
            layer.close(fn.layer_index);
            layer.msg('手机号变更成功，请您重新登录',{time:2000},function () {
                com.logout();
            });
        });
    });
    exports('editPhone', fn)
});