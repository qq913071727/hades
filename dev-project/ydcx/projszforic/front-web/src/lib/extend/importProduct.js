/**
 * 导入产品
 */
layui.define(['layer', 'upload'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, upload = layui.upload , com = layui.com,
        request = layui.request,
        setter = layui.setter,
        fn = {
            layer_index: 0,
            async show(){
                await $.get('/views/sub/importProduct.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: "导入产品"
                        , id: 'win-import-product'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['500px', 'auto']
                        , content: html
                        , success: function () {
                           
                        }
                    });
                });
                upload.render({
                    elem: '#importProductExcel',url:'scm/client/materiel/import',size:10240,exts:'xls|xlsx'
                    ,acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    ,before:function (res) {
                        com.showLoading();
                    } ,done: function(res){
                        if(com.isSuccess(res)){
                            var data = res.data;
                            layer.msg(['导入条数：',data.successCount,' ',data.message].join(''));
                            //关闭对话框
                            layer.close(fn.layer_index);
                        }
                    },error:function (index, upload) {
                        com.hideLoading();
                    }
                });
            }
           
        };
    exports('importProduct', fn)
})