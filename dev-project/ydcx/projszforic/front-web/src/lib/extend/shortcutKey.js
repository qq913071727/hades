
layui.define(['layer'], function (exports) {
    var $ = layui.jquery,fn={
        init:function (elem) {
            let $elem = elem || $('body');
            $elem.on('keydown', 'textarea', function(e) {
                var self = $(this),eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
                if (eCode == 13) {
                    e.preventDefault();
                }
            });
            $elem.on('keyup','input, select, textarea',function(e){
                var self = $(this), form = self.parents('form:eq(0)'), focusable, next, prev,eCode = e.keyCode ? e.keyCode : e.which ? e.which: e.charCode;
                if (e.shiftKey) {
                    if (e.keyCode == 13) {
                        focusable = form.find('input,a,select,textarea').filter(':visible').not('.disabled').not(':input[disabled]');
                        prev = focusable.eq(focusable.index(this) - 1);
                        if (prev.length){if($(this).attr("shiftEnter")=="no"){return false;}else{prev.select();}}
                    }
                } else if (e.ctrlKey && eCode == 13 && this.localName == "textarea") {
                    var myValue = "\n",$t = $(this)[0];
                    if (document.selection){
                        this.focus();
                        var sel = document.selection.createRange();
                        sel.text = myValue;
                        this.focus();
                        sel.moveStart('character', -l);
                    }else if ($t.selectionStart || $t.selectionStart == '0') {
                        var startPos = $t.selectionStart;
                        var endPos = $t.selectionEnd;
                        var scrollTop = $t.scrollTop;
                        $t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos,$t.value.length);
                        this.focus();
                        $t.selectionStart = startPos + myValue.length;
                        $t.selectionEnd = startPos + myValue.length;
                        $t.scrollTop = scrollTop;

                    } else {
                        this.value += myValue;
                        this.focus();
                    }
                } else  if (eCode == 13) {
                    if (this.localName == "textarea") {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    focusable = form.find('input,select,textarea').filter(':visible').not('.disabled').not(':input[disabled]');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        if($(this).attr("enter")=="no"){
                            return false;
                        }else{
                            next.select();
                        }
                    }
                    return false;
                }
            });
        }
    };
    exports('shortcutKey', fn)
});