layui.define(['upload'], function (exports) {
    var $ = layui.jquery, com = layui.com,request = layui.request, upload = layui.upload, fn = {

        renderList(othis){
            let isEdit = (othis.attr('edit')||'false')=='true',
            maxSize = parseInt(othis.attr('max-size') || '10',10);

            request.postForm('/scm/file/fileList',{
                docId:othis.attr('file-id'),docType:othis.attr('file-type'),businessType:othis.attr('business-type') || ''
            }).then(res => {
               let ulbox = [];
                (res.data || []).forEach((item,idx) =>{
                    ulbox.push('<li><a href="/scm/download/file/',item.id,'" target="_blank">',(idx+1),'. ',item.name,'</a>',(isEdit)?'<i class="iconfont iconhuishou remove-file" title="删除" file-id="'+item.id+'"></i>':'','</li>');
                });
                if(res.data.length>=maxSize){
                    othis.find('.upload-title').hide();
                    othis.find('.file-box').css({'margin-top':'0px'});
                }
                if(isEdit && res.data.length < maxSize){
                    othis.find('.upload-title').show();
                    othis.find('.file-box').css({'margin-top':'25px'});
                }
               othis.find('.file-box').html(ulbox.join(''));
               if(isEdit){
                   othis.find('.remove-file').click(function () {
                       fn.remove($(this),othis)
                   });
               }
            });
        },
        remove(othis,filebox){
            let fileId = othis.attr('file-id');
            layer.confirm('当前操作不可回退是否继续？',{icon: 3},function (index) {
                layer.close(index);
                request.postForm('/scm/file/removes',{ids:fileId}).then(res=>{
                    fn.renderList(filebox);
                });
            });
        },
        init(element) {
            if(!element){
                element = $('body');
            }
            var $files = element.find('.upload-box');
            $files.each(function (i, o) {
                let othis = $(o), fileId = othis.attr('file-id'),
                    fileType = othis.attr('file-type'),
                    businessType = othis.attr('business-type') || '',
                    fileMemo = othis.attr('file-memo') || '',
                    exts = othis.attr('exts') || 'pdf|xls|xlsx|doc|docx|ppt|pptx|txt|zip|rar|7z|jpg|jpeg|gif|png|bmp|tif',
                    multiple = (othis.attr('multiple') || 'true') == 'true'?true:false,
                    btnName = othis.attr('btn-name') || '点击上传',
                    isEdit = (othis.attr('edit')||'false')=='true';
                othis.html([isEdit?'<div class="upload-title"><span class="upload-text">'+fileMemo+'</span><div class="upload-loading"><i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i></div><a href="javascript:" class="upload-btn"><i class="iconfont iconic_upload"></i>'+btnName+'</a></div>':'','<ul class="file-box scrollbar" ',isEdit?'':'style="margin-top:0"','></ul>'].join(''));
                fn.renderList(othis);
                if(isEdit){
                    let $loading = othis.find('.upload-loading'),$upload = othis.find('.upload-btn');
                    upload.render({
                        elem:$upload ,url:'/scm/file/upload',size:10240,exts:exts
                        ,multiple:multiple,data:{docId:fileId,docType:fileType,businessType:businessType,memo:fileMemo}
                        ,before:function (res) {
                            $loading.show();
                            $upload.hide();
                        }
                        ,done: function(res){
                            $loading.hide();
                            $upload.show();
                            if(com.isSuccess(res)){
                                fn.renderList(othis);
                            }
                        },error:function () {
                            $loading.hide();
                            $upload.show();
                        }
                    });
                }
            });
        },
        fileSize(fileType,businessType){
            return $('.upload-box[file-type="'+fileType+'"][business-type="'+businessType+'"]').find('li').size();
        }
    }
    exports('uploadList', fn);
});