/**
 * 供应商组件
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form , com = layui.com,
        setter = layui.setter,
        request = layui.request,
        fn = {
            layer_index: 0,
            callBack:null,
            async add(callBack) {
               await fn.edit(null,callBack);
            },
            async edit(id,callBack){
                fn.callBack = callBack;
                await $.get('/views/sub/supplier-manage.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: com.isNotEmpty(id)?'修改供应商':'新增供应商'
                        , id: 'win-supplier-edit'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['500px', 'auto']
                        , content: html
                        , success: function () {
                            $('#add-supplier-name').focus();
                        }
                    });
                });
                let subData = {id:'',disabled:false};
                if(com.isNotEmpty(id)){
                    await request.get('/scm/client/supplier/detail',{id:id}).then(res =>{
                        subData = res.data;
                    }).catch(()=>{
                        layer.close(fn.layer_index);
                    });
                }
                form.val('form-supplier-manage',subData);
                com.chooseCheckbox($('#supplier-manage-disabled'),!subData.disabled);
                form.render('checkbox','form-supplier-manage');
                form.verify({
                    "add-supplier-name": function (value, item) {
                        if (com.isEmpty(value)){
                            return '请输入供应商名称';
                        }
                        if (/[\uFF00-\uFFFF]/g.test(value) || /[\u4e00-\u9fa5]/g.test(value)) {
                            return '供应商名称不能有中文和全角括号';
                        }
                    }
                });
                form.on('submit(add-supplier-save)', function (data) {
                    let params = data.field;
                    params.disabled = com.isEmpty(params.disabled);
                    request.postForm('/scm/client/supplier/'.concat(com.isEmpty(params.id)?'add':'edit'),params).then(res => {
                        layer.close(fn.layer_index);
                        layer.msg(com.isEmpty(params.id)?'添加成功':'修改成功');
                        if(fn.callBack){
                            if(com.isEmpty(params.id)){
                                params.id = res.data;
                            }
                            params.name = com.trim(params.name);
                            eval(fn.callBack).call(this,params);
                        }
                    });
                });
            }
        };
    exports('supplier', fn)
})