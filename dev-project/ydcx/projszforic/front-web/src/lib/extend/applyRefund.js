/**
 * 申请退款
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form , com = layui.com,
        setter = layui.setter,
        request = layui.request,
        fn = {
            layer_index: 0,
            callBack:null,
            async apply(callBack){
                fn.callBack = callBack;
                await $.get('/views/sub/apply-refund.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: '申请退款'
                        , id: 'win-apply-refund'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['600px', 'auto']
                        , content: html
                        , success: function () {
                           
                        }
                    });
                });
                form.on('submit(refund-apply-save)', function (data) {
                    let params = data.field;
                    if(parseFloat(params.accountValue) <= 0){
                        layer.msg("申请金额需大于0")
                        return false;
                    }
                    request.post('/scm/refundAccount/clientApply',params).then(res => {
                        layer.close(fn.layer_index);
                        layer.msg('申请成功，我司会尽快给您处理');
                        if(fn.callBack){
                            eval(fn.callBack).call(this,params);
                        }
                    });
                });
            }
        };
    exports('applyRefund', fn)
})