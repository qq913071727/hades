/**
 * 修改邮箱
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer,
        form = layui.form ,
        setter = layui.setter,
        request = layui.request,
        com = layui.com,fn = {
            layer_index: 0,
            callBack: null,
            async show(old,callBack) {
                fn.callBack = callBack;
                await $.get('/views/sub/edit-mail.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: old?'修改邮箱':'设置邮箱'
                        , id: 'win-edit-mail'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['550px', 'auto']
                        , content: html
                    });
                });
                await $.get('/scm/client/person/info').then(res =>{
                    $('#edit-mail-name').val(com.removeNull(res.data.mail)).focus();
                });
                form.verify({
                    'edit-mail-name': function (value, item) {
                        if(com.isEmpty(value)){
                            return '请输入您的常用邮箱';
                        }
                        if (!com.reg.mail.test(value)) {
                            return '请输入正确的邮箱号码';
                        }
                    }
                });
                //修改邮箱
                form.on('submit(save-edit-mail)',function (data) {
                    request.postForm('/scm/client/person/changeMail',data.field).then(()=>{
                        layer.msg(old?'修改成功':'设置成功');
                        layer.close(fn.layer_index);
                        if(fn.callBack){
                            eval(fn.callBack).call(this,data);
                        }
                    });
                });
            }
        };
    exports('editMail', fn)
});