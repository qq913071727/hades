layui.define(function (exports) {
    var  $ = layui.jquery,com = layui.com, request = layui.request,fn = {
        keys:['static_transaction_mode','static_payment_term','static_currency','static_unit','static_country','static_DeliveryMode','static_HkExpress','static_ReceivingMode',
              'static_province','static_domestic_logistics','static_take_self_ware','static_transaction_type']
        // 成交方式
        ,async getTransactionModeData() {
            let data = com.getLocalStorageJson('static_transaction_mode');
            if(com.isEmpty(data)){
                await request.get('/scm/common/bizSelect', {'type': 'TransactionMode'}).then(res => {
                    data = res.data['TransactionMode'];
                    com.setLocalStorageJson('static_transaction_mode', data);
                });
            }
            return data;
        }
        // 付款方式
        ,async getPaymentTermData() {
            let data = com.getLocalStorageJson('static_payment_term');
            if(com.isEmpty(data)){
                await request.get('/scm/common/bizSelect', {'type': 'PaymentTerm'}).then(res => {
                    data = res.data['PaymentTerm'];
                    com.setLocalStorageJson('static_payment_term', data);
                });
            }
            return data;
        }
        // 交易类型
        ,async getTransactionType(){
            let data = com.getLocalStorageJson('static_transaction_type');
            if(com.isEmpty(data)){
                await request.get('/scm/common/bizSelect', {'type': 'TransactionType'}).then(res => {
                    data = res.data['TransactionType'];
                    com.setLocalStorageJson('static_transaction_type', data);
                });
            }
            return data;
        }
        // 币制
        ,async getCurrencyData(){
            let data = com.getLocalStorageJson('static_currency');
            if(com.isEmpty(data)){
                await request.get('/scm/currency/select').then(res => {
                    data = [];
                    $.each(res.data,function (i,o){
                        data.push({label:o.name,value:o.id});
                    });
                    com.setLocalStorageJson('static_currency', data);
                });
            }
            return data;
        }
        // 单位
        ,async getUnitData(){
            let data = com.getLocalStorageJson('static_unit');
            if(com.isEmpty(data)){
                await request.get('/system/unit/select').then(res => {
                    data = [];
                    $.each(res.data,function (i,o){
                        data.push({label:o.name,value:o.id});
                    });
                    com.setLocalStorageJson('static_unit', data);
                });
            }
            return data;
        }
        // 国家
        ,async getCountryData(){
            let data = com.getLocalStorageJson('static_country');
            if(com.isEmpty(data)){
                await request.get('/system/country/select').then(res => {
                    data = [];
                    $.each(res.data,function (i,o){
                        data.push({label:o.name,value:o.id});
                    });
                    com.setLocalStorageJson('static_country', data);
                });
            }
            return data;
        }
        // 国家 按洲分组
        ,async getGroupCountryData(){
            let data = com.getLocalStorageJson('static_group_country');
            if(com.isEmpty(data)){
                await request.get('/rate/country/obtainCountry').then(res => {
                     com.setLocalStorageJson('static_group_country', res.data);
                });
            }
            return data;
        }
         // 联行号
         ,async getBankNumbers(){
            let data = com.getLocalStorageJson('static_bank_number');
            if(com.isEmpty(data)){
                await request.get('/rate/bankNumber/obtainBankNumbers').then(res => {
                    data = [];
                    $.each(res.data,function (i,o){
                        data.push({label:o.name,value:o.id});
                    });
                    com.setLocalStorageJson('static_bank_number', data);
                });
            }
            return data;
        }
        // 获取枚举数据
        , async getByTypes(){
            let types = ['DeliveryMode', 'HkExpress', 'ReceivingMode'];
            await request.get('/system/baseData/getByTypes',{types: types.join(',')}).then(res =>{
                types.forEach(k =>{
                   let items = [];
                    res.data[k].forEach(o => {
                        items.push({
                            label: o.name,
                            value: o.code
                        })
                    });
                    com.setLocalStorageJson('static_'.concat(k), items);
                });
            });
        }
        // 获取国内物流信息
        , async getDomesticLogistics(){
            let data = com.getLocalStorageJson('static_domestic_logistics');
            if(com.isEmpty(data)){
                await request.get('/scm/domesticLogistics/select').then(res=>{
                    data = res.data;
                    com.setLocalStorageJson('static_domestic_logistics', data);
                });
            }
            return data;
        }
        // 获取自提仓库信息
        , async getTakeSelfWare(){
            let data = com.getLocalStorageJson('static_take_self_ware');
            if(com.isEmpty(data)){
                await request.get('/scm/warehouse/selfWarehouses').then(res=>{
                    data = res.data || [];
                    com.setLocalStorageJson('static_take_self_ware', data);
                });
            }
            return data;
        }
        // 获取国内送货方式
        , async getReceivingMode(){
            let data = com.getLocalStorageJson('static_ReceivingMode');
            if(com.isEmpty(data)){
                await fn.getByTypes();
                data = com.getLocalStorageJson('static_ReceivingMode');
            }
            return data;
        }
        // 获取境外交货方式
        , async getDeliveryMode(){
            let data = com.getLocalStorageJson('static_DeliveryMode');
            if(com.isEmpty(data)){
                await fn.getByTypes();
                data = com.getLocalStorageJson('static_DeliveryMode');
            }
            return data;
        }
        // 获取境外快递公司
        , async getHkExpress(){
            let data = com.getLocalStorageJson('static_HkExpress');
            if(com.isEmpty(data)){
                await fn.getByTypes();
                data = com.getLocalStorageJson('static_HkExpress');
            }
            return data;
        }
        //获取所有的省份
        , async getAllProvinces(){
            let data = com.getLocalStorageJson('static_province');
            if(com.isEmpty(data)){
                await request.get('/system/district/provinces').then(res => {
                    data = [];
                    $.each(res.data,function (i,o){
                        data.push({name:o.name});
                    });
                    com.setLocalStorageJson('static_province', data);
                });
            }
            return data;
        }
        // 根据省获取市
        , async getCitys(province){
            let data = [];
            await request.get('/system/district/citys',{pname:province}).then(res => {
                data = [];
                $.each(res.data,function (i,o){
                    data.push({name:o.name});
                });
            });
            return data;
        }
        // 根据省市获取区
        , async getAreas(province,city){
            let data = [];
            await request.get('/system/district/areas',{pname:province,cname:city}).then(res => {
                data = [];
                $.each(res.data,function (i,o){
                    data.push({name:o.name});
                });
            });
            return data;
        }
        // 清除所有缓存数据
        ,clearAll:function () {
            $.each(this.keys,function (i,o) {
               com.setLocalStorage(o,'');
            });
        }
    };
    exports('staticData',fn);
});