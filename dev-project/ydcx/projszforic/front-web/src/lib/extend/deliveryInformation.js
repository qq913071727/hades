/**
 * 客户收货地址
 */
layui.define(['layer', 'form','staticData'], function (exports) {
    var $ = layui.jquery, layer = layui.layer, form = layui.form , com = layui.com,
        request = layui.request,
        setter = layui.setter,
        staticData = layui.staticData,$province,$city,$area,
        fn = {
            layer_index: 0,
            callBack:null,
            async show(id,callBack) {
                await $.get('/views/sub/delivery-information-manage.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: "收提货地址维护"
                        , id: 'win-delivery-inform'
                        , type: 1
                        , resize: false
                        , shadeClose: true
                        , area: ['600px', 'auto']
                        , content: html
                        , success: function () {
                            //$('#add-supplier-name').focus();
                        }
                    });
                });
                $province = $('#delivery-information-manage-province'),
                    $city = $('#delivery-information-manage-city'),
                    $area = $('#delivery-information-manage-area'),detailInfo={
                    provinceName:'广东省',
                    cityName:'深圳市',
                    areaName:'',
                    isDefault:true
                };
                if(com.isNotEmpty(id)){
                    await request.get('/scm/customerDeliveryInfo/detail',{id:id}).then(res =>{
                        detailInfo = res.data;
                    }).catch(()=>{
                        layer.close(fn.layer_index);
                    });
                }
                //获取省份信息
                let provinces = await staticData.getAllProvinces();
                com.fullSelect({
                    elem:$province,
                    data:provinces,
                    label:'name',
                    value:'name',
                    defValue:detailInfo.provinceName,
                    firstLabel:'请选择'
                });
                await fn.getCitys(detailInfo.cityName);
                await fn.getAreas(detailInfo.areaName);
                form.val('form-delivery-information-manage',detailInfo);
                form.render(null,'form-delivery-information-manage');

                fn.callBack = callBack;
            }
            ,async getCitys(cityName){
                let provinceName = $province.val();
                $city.get(0).length = 0;
                $area.get(0).length = 0;
                if(com.isNotEmpty(provinceName)){
                    let citys = await staticData.getCitys(provinceName);
                    com.fullSelect({
                        elem:$city,
                        data:citys,
                        label:'name',
                        value:'name',
                        defValue:cityName||'',
                        firstLabel:'请选择'
                    });
                }
                form.render('select','form-delivery-information-manage');
            }
            ,async getAreas(areaName){
                let provinceName = $province.val(),cityName = $city.val();
                $area.get(0).length = 0;
                if(com.isNotEmpty(provinceName)&&com.isNotEmpty(cityName)){
                    let areas = await staticData.getAreas(provinceName,cityName);
                    com.fullSelect({
                        elem:$area,
                        data:areas,
                        label:'name',
                        value:'name',
                        defValue:areaName||'',
                        firstLabel:'请选择'
                    });
                }
                form.render('select','form-delivery-information-manage');
            }
        };
    form.on('select(delivery-information-manage-province)',function () {
        fn.getCitys();
    });
    form.on('select(delivery-information-manage-city)',function () {
        fn.getAreas();
    });
    form.on('submit(add-delivery-information-manage-save)', function (data) {
        let params = data.field;
        params.disabled = false;
        params.isDefault = com.isNotEmpty(params.isDefault);
        request.postForm('/scm/client/customerDeliveryInfo/'.concat(com.isEmpty(params.id)?'add':'edit'),params).then(res => {
            layer.close(fn.layer_index);
            if(fn.callBack){
                params.id = res.data;
                eval(fn.callBack).call(this,params);
            }
        });
    });
    exports('deliveryInformation', fn)
})