/**
 * 换绑/绑定 微信
 */
layui.define(['layer', 'form'], function (exports) {
    var $ = layui.jquery, layer = layui.layer,
        setter = layui.setter,
        request = layui.request,
        com = layui.com,fn = {
            layer_index: 0,
            callBack: null,
            operateType:'',
            scanTimer:null,
            wxCode:'',
            async show(option) {
                let title = option.title;
                fn.callBack = option.callback;
                await $.get('/views/sub/bind-wx.html',{'v':setter.version},function (html) {
                    fn.layer_index = layer.open({
                        title: title
                        , id: 'win-bind-wx'
                        , type: 1
                        , resize: false
                        , shadeClose: false
                        , area: ['550px', '500px']
                        , content: html
                        , cancel: function() {
                            if(fn.scanTimer != null) {
                                clearInterval(fn.scanTimer);
                            }
                            layer.close(fn.layer_index);
                        }
                    });
                    if(title == '换绑微信') {
                        fn.operateType = 'changeBind';
                        $(".wx-notice").text("温馨提示：您当前登录的芯达通帐号已经绑定了微信帐号，请使用您需要绑定的微信扫描二维码关注”芯达通“进行换绑。");
                    } else if(title == '绑定微信') {
                        fn.operateType = 'bind';
                        $(".wx-notice").text("温馨提示：您当前登录的芯达通帐号未绑定微信帐号，请使用您需要绑定的微信扫描二维码关注”芯达通“进行绑定，便捷微信登录。");
                    }
                    fn.generateWxLoginKey();
                });
            },
            //生成微信临时二维码
            genTempQrcode:function(){
                request.postForm('/scm/wx/genTempQrcode',{'code':fn.wxCode}).then(res =>{
                   $('#wxQrCode').attr({'src':'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.concat(res.data)});
                   fn.startScan();
               });
            },
            generateWxLoginKey:function(){
                request.postForm('/scm/auth/generateWxLoginKey',{'operateType': fn.operateType}).then(res=>{
                    fn.wxCode = res.data;
                    fn.genTempQrcode();
                }).catch(res=>{
                    //后台检查报错，关闭窗口
                    layer.close(fn.layer_index);
                });
            },
            startScan:function () {
                fn.scanTimer = setInterval(function () {
                    clearInterval(fn.scanTimer);
                    if(com.isNotEmpty(fn.wxCode)){
                        request.send({
                            type: 'POST'
                            , contentType: 'application/x-www-form-urlencoded;charset=utf-8'
                            , showLoading: false
                            , url: '/scm/auth/checkBindWxgzh'
                            , data: {'operateType':fn.operateType,'code':fn.wxCode}
                        }).then(res =>{
                            let data = res.data;
                            if(res.data != null) {
                                if(res.data.code == 2) {
                                  if(fn.callBack){
                                     eval(fn.callBack).call(this);
                                  }
                                  //提示绑定成功
                                  layer.msg(res.data.msg);
                                  layer.close(fn.layer_index);
                                } else if (res.data.code == 1 || res.data.code == 4 || res.data.code == 5 || res.data.code == 6) {
                                    //重新刷新二维码
                                    layer.msg(res.data.msg,{'time':2500});
                                    fn.generateWxLoginKey();
                                } else {
                                    fn.startScan();
                                }
                            } else {
                                fn.startScan();
                            }
                        }).catch(res =>{
                            //清除定时检查是否绑定定时器
                            layer.close(fn.layer_index);
                            clearInterval(fn.scanTimer);
                        });
                    }
                },1000);
            },
        };
    exports('bindWx', fn)
});