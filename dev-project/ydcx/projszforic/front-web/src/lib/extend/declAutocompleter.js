layui.define(['layer','jqueryAutocompleter'], function (exports) {
    var $ = layui.jquery,ja=layui.jqueryAutocompleter,com = layui.com,request = layui.request,cb = {
        transModeControl:function () { },
        transModeControl:function(){ },
        feeMarkSelect:function(){ },
        insurMarkSelect:function(){ },
        otherMarkSelect:function(){ },
        showBussinessOptions:function(){ },
        speclRlAfmCodeChange:function(){ },
        joinElements:function(){ },
        selectAttTypeCode:function(){ },
        sysQty:function(){ },
    },auto1 = {
        "customMasterName": {hid: 'customMaster',type:'CUS_CUSTOMS'},
        "iePortName": {hid: 'iePort',type:'CUS_CUSTOMS'},
        "cutModeName": {hid: 'cutMode',type:'CUS_LEVYTYPE'},
        "despPortCodeName": {hid: 'despPortCode',type:'CUS_MAPPING_PORT_CODE',style:'min-width:240px'},
        "transModeName": {hid: 'transMode',type:'CUS_TRANSAC',callback:cb.transModeControl()},
        "feeMarkName": {hid: 'feeMark',type:'DEC_FEE_MARK',callback:cb.feeMarkSelect()},
        "insurMarkName": {hid: 'insurMark',type:'DEC_INSUR_MARK',callback:cb.insurMarkSelect()},
        "otherMarkName": {hid: 'otherMark',type:'DEC_OTHER_MARK',callback:cb.otherMarkSelect()},
        "ciqEntyPortCodeName": {hid: 'ciqEntyPortCode',type:'CIQ_PORT_CN',style:'min-width:240px'},
        "entryTypeName": {hid: 'entryType',type:'DEC_ENTRY_TYPE',callback:cb.showBussinessOptions()},
        "orgCodeName": {hid: 'orgCode',type:'CIQ_CUS_ORGANIZATION'},
        "vsaOrgCodeName": {hid: 'vsaOrgCode',type:'CIQ_CUS_ORGANIZATION'},
        "inspOrgCodeName": {hid: 'inspOrgCode',type:'CIQ_CUS_ORGANIZATION'},
        "purpOrgCodeName": {hid: 'purpOrgCode',type:'CIQ_CUS_ORGANIZATION'},
        "correlationReasonFlagName": {hid: 'correlationReasonFlag',type:'CIQ_CORRELATION_REASON'},
        "origBoxFlagName": {hid: 'origBoxFlag',type:'DEC_ORIG_BOX_FLAG'},
        "gunitName": {hid: 'gunit',type:'CUS_UNIT',dropUp:true,callback:cb.sysQty()},
        "origPlaceCodeName": {hid: 'origPlaceCode',type:'CIQ_ORIGIN_PLACE',dropUp:true},
        "districtCodeName": {hid: 'districtCode',type:'CUS_DISTRICT',dropUp:true},
        "bdistrictCodeName": {hid: 'bdistrictCode',type:'CUS_DISTRICT'},
        "ciqDestCodeName": {hid: 'ciqDestCode',type:'CIQ_CITY_CN',dropUp:true},
        "bciqDestCodeName": {hid: 'bciqDestCode',type:'CIQ_CITY_CN'},
        "purposeName": {hid: 'purpose',type:'CIQ_USE',dropUp:true},
        "dutyModeName": {hid: 'dutyMode',type:'CUS_LEVYMODE',dropUp:true},
        "bdutyModeName": {hid: 'bdutyMode',type:'CUS_LEVYMODE'},
        "containerMdCodeName": {hid: 'containerMdCode',type:'CUS_MAPPING_NORMS_CODE_V'},
        "tcontainerMdCodeName": {hid: 'tcontainerMdCode',type:'CUS_MAPPING_NORMS_CODE_V'},
        "lclFlagName": {hid: 'lclFlag',type:'DEC_1_0_FLAG'},
        "acmpFormCodeName": {hid: 'acmpFormCode',type:'CUS_LICENSEDOCU'},
        "transTypeName": {hid: 'transType',type:'DEC_TRANS_TYPE'},
        "transFlagName": {hid: 'transFlag',type:'TRAN_FLAG'},
        "ediIdName": {hid: 'ediId',type:'EDIID_TYPE'},
        "esealFlagName": {hid: 'esealFlag',type:'E_SEAL_FLAG'},
        "promise1Name": {hid: 'promiseItem1',type:'DEC_SPECL_RL_AFM_CODE',callback:cb.speclRlAfmCodeChange()},
        "promise2Name": {hid: 'promiseItem2',type:'DEC_SPECL_RL_AFM_CODE'},
        "promise3Name": {hid: 'promiseItem3',type:'DEC_SPECL_RL_AFM_CODE'},
        "entQualifTypeName": {hid: 'entQualifTypeCode',type:'CIQ_APTT_NAME_I',style:'min-width:340px'},
        "brandType": {hid: 'brandTypeCode',type:'BRAND_TYPE',callback:cb.joinElements()},
        "expDiscount": {hid: 'expDiscountCode',type:'EXP_DISCONUNT',callback:cb.joinElements()},
        "attTypeCodeName": {hid: 'attTypeCode',type:'DEC_ATT_TYPE_CODE_V',callback:cb.selectAttTypeCode()},
        "licWrtofUnitName": {hid: 'licWrtofUnit',type:'CUS_UNIT',style:'min-width:100px'},
        "dnoDangFlagName": {hid: 'dnoDangFlag',type:'DEC_NO_DANG_FLAG',style:'min-width:100px'},
        "dpackTypeName": {hid: 'dpackType',type:'CIQ_DANGER_PACK_TYPE',style:'min-width:100px'},
    },auto2 = {
        "cusTrafModeName": {hid: 'cusTrafMode',type:'CUS_MAPPING_TRANSPORT_CODE_V'},
        "inbordTrafModeName": {hid: 'inbordTrafMode',type:'CUS_MAPPING_TRANSPORT_CODE_V'},
        "supvModeCddeName": {hid: 'supvModeCdde',type:'CUS_MAPPING_TRADE_CODE_V'},
        "cusTradeCountryName": {hid: 'cusTradeCountry',type:'CUS_MAPPING_COUNTRY_CODE_V'},
        "cusTradeNationCodeName": {hid: 'cusTradeNationCode',type:'CUS_MAPPING_COUNTRY_CODE_V'},
        "destinationCountryName": {hid: 'destinationCountry',type:'CUS_MAPPING_COUNTRY_CODE_V',dropUp:true},
        "cusOriginCountryName": {hid: 'cusOriginCountry',type:'CUS_MAPPING_COUNTRY_CODE_V',dropUp:true},
        "bcusOriginCountryName": {hid: 'bcusOriginCountry',type:'CUS_MAPPING_COUNTRY_CODE_V'},
        "distinatePortName": {hid: 'distinatePort',type:'CUS_MAPPING_PORT_CODE_V',style:'min-width:300px'},
        "wrapTypeName": {hid: 'wrapType',type:'CUS_MAPPING_PACKAGEKIND_CODE_V'},
        "feeCurrName": {hid: 'feeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
        "insurCurrName": {hid: 'insurCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
        "otherCurrName": {hid: 'otherCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
        "tradeCurrName": {hid: 'tradeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V',dropUp:true},
        "btradeCurrName": {hid: 'btradeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'}
    },fn = {
        initAuto1:function(data){
            $.each(auto1,function (key,val) {
                var $input = $('#'+key);
                if($input.length>0){
                    var source = [];
                    $.each(data[val.type]||[],function (i,o) {
                        source.push({'label':o.c+'-'+o.n,'value':o.n});
                    });
                    if(source.length>0){
                        $input.autocompleter({
                            source:source,
                            style:com.removeNull(val.style),
                            limit:20,
                            focusOpen:(val.focusOpen),
                            hiddenId:val.hid,
                            dropUp:val.dropUp,
                            callback:val.callback
                        });
                    }
                }
            });
        },
        initAuto2:function(data){
            $.each(auto2,function (key,val) {
                var $input = $('#'+key);
                if($input.length>0){
                    var source = [];
                    $.each(data[val.type]||[],function (i,o) {
                        o['label'] = o.c+'_'+o.n+'_'+o.o+'_'+o.q;
                        source.push(o);
                    });
                    if(source.length>0){
                        $input.autocompleter({
                            source:source,
                            style:com.removeNull(val.style),
                            limit:20,
                            focusOpen:(val.focusOpen),
                            hiddenId:val.hid,
                            dropUp:val.dropUp,
                            callback:val.callback,
                            template:'<div>{{c}}-{{n}} <span class="fr">{{o}}&nbsp;&nbsp;{{q}}</span></div>'
                        });
                    }
                }
            });
        },
        initBaseData:function(){
            var declDatas1 = com.getLocalStorageJson("static_decl_datas1");
            if(com.isEmpty(declDatas1)){
                request.postForm('system/customsData/formatData1').then(res =>{
                    com.setLocalStorageJson('static_decl_datas1',res.data);
                    fn.initAuto1(res.data);
                });
            }else{
                fn.initAuto1(declDatas1);
            }
            var declDatas2 = com.getLocalStorageJson("static_decl_datas2");
            if(com.isEmpty(declDatas2)){
                request.postForm('system/customsData/formatData2').then(res =>{
                    com.setLocalStorageJson('static_decl_datas2',res.data);
                    fn.initAuto2(res.data);
                });
            }else{
                fn.initAuto2(declDatas2);
            }
        },
        localData1(type) {
            var declDatas1 = localStorage.getItem("static_decl_datas1");
            if(com.isEmpty(declDatas1)){return [];}
            return JSON.parse(declDatas1)[type];
        },
        localData2(type) {
            var declDatas2 = localStorage.getItem("static_decl_datas2");
            if(com.isEmpty(declDatas2)){return [];}
            return JSON.parse(declDatas2)[type];
        },
        findByLocalData1ByCode(type,code) {
            var d1 = fn.localData1(type);
            for(var i=0;i<d1.length;i++){if(code==d1[i].c){return d1[i];}}
            return {'c':'','n':''};
        },
        init:function () {
            request.get('/system/version').then(res=>{
                if(res.data != com.getLocalStorage('static_version')){
                    com.setLocalStorage("static_code_datas","");
                    com.setLocalStorage("static_decl_datas1","");
                    com.setLocalStorage("static_decl_datas2","");
                    com.setLocalStorage("static_prod_traf_data","");
                    com.setLocalStorage("static_version",res.data);
                }
                fn.initBaseData();
            });
        }
    };
    exports('declAutocompleter', fn);
});