var auto1 = {
    "customMasterName": {hid: 'customMaster',type:'CUS_CUSTOMS'},
    "iePortName": {hid: 'iePort',type:'CUS_CUSTOMS'},
    "cutModeName": {hid: 'cutMode',type:'CUS_LEVYTYPE'},
    "despPortCodeName": {hid: 'despPortCode',type:'CUS_MAPPING_PORT_CODE',style:'min-width:240px'},
    "transModeName": {hid: 'transMode',type:'CUS_TRANSAC',callback:transModeControl},
    "feeMarkName": {hid: 'feeMark',type:'DEC_FEE_MARK',callback:feeMarkSelect},
    "insurMarkName": {hid: 'insurMark',type:'DEC_INSUR_MARK',callback:insurMarkSelect},
    "otherMarkName": {hid: 'otherMark',type:'DEC_OTHER_MARK',callback:otherMarkSelect},
    "ciqEntyPortCodeName": {hid: 'ciqEntyPortCode',type:'CIQ_PORT_CN',style:'min-width:240px'},
    "entryTypeName": {hid: 'entryType',type:'DEC_ENTRY_TYPE',callback:showBussinessOptions},
    "orgCodeName": {hid: 'orgCode',type:'CIQ_CUS_ORGANIZATION'},
    "vsaOrgCodeName": {hid: 'vsaOrgCode',type:'CIQ_CUS_ORGANIZATION'},
    "inspOrgCodeName": {hid: 'inspOrgCode',type:'CIQ_CUS_ORGANIZATION'},
    "purpOrgCodeName": {hid: 'purpOrgCode',type:'CIQ_CUS_ORGANIZATION'},
    "correlationReasonFlagName": {hid: 'correlationReasonFlag',type:'CIQ_CORRELATION_REASON'},
    "origBoxFlagName": {hid: 'origBoxFlag',type:'DEC_ORIG_BOX_FLAG'},
    "gunitName": {hid: 'gunit',type:'CUS_UNIT',dropUp:true,callback:sysQty},
    "origPlaceCodeName": {hid: 'origPlaceCode',type:'CIQ_ORIGIN_PLACE',dropUp:true},
    "districtCodeName": {hid: 'districtCode',type:'CUS_DISTRICT',dropUp:true},
    "bdistrictCodeName": {hid: 'bdistrictCode',type:'CUS_DISTRICT'},
    "ciqDestCodeName": {hid: 'ciqDestCode',type:'CIQ_CITY_CN',dropUp:true},
    "bciqDestCodeName": {hid: 'bciqDestCode',type:'CIQ_CITY_CN'},
    "purposeName": {hid: 'purpose',type:'CIQ_USE',dropUp:true},
    "dutyModeName": {hid: 'dutyMode',type:'CUS_LEVYMODE',dropUp:true},
    "bdutyModeName": {hid: 'bdutyMode',type:'CUS_LEVYMODE'},
    "containerMdCodeName": {hid: 'containerMdCode',type:'CUS_MAPPING_NORMS_CODE_V'},
    "tcontainerMdCodeName": {hid: 'tcontainerMdCode',type:'CUS_MAPPING_NORMS_CODE_V'},
    "lclFlagName": {hid: 'lclFlag',type:'DEC_1_0_FLAG'},
    "acmpFormCodeName": {hid: 'acmpFormCode',type:'CUS_LICENSEDOCU'},
    "transTypeName": {hid: 'transType',type:'DEC_TRANS_TYPE'},
    "transFlagName": {hid: 'transFlag',type:'TRAN_FLAG'},
    "ediIdName": {hid: 'ediId',type:'EDIID_TYPE'},
    "esealFlagName": {hid: 'esealFlag',type:'E_SEAL_FLAG'},
    "promise1Name": {hid: 'promiseItem1',type:'DEC_SPECL_RL_AFM_CODE',callback:speclRlAfmCodeChange},
    "promise2Name": {hid: 'promiseItem2',type:'DEC_SPECL_RL_AFM_CODE'},
    "promise3Name": {hid: 'promiseItem3',type:'DEC_SPECL_RL_AFM_CODE'},
    "entQualifTypeName": {hid: 'entQualifTypeCode',type:'CIQ_APTT_NAME_I',style:'min-width:340px'},
    "brandType": {hid: 'brandTypeCode',type:'BRAND_TYPE',callback:joinElements},
    "expDiscount": {hid: 'expDiscountCode',type:'EXP_DISCONUNT',callback:joinElements},
    "attTypeCodeName": {hid: 'attTypeCode',type:'DEC_ATT_TYPE_CODE_V',callback:selectAttTypeCode},
    "licWrtofUnitName": {hid: 'licWrtofUnit',type:'CUS_UNIT',style:'min-width:100px'},
    "dnoDangFlagName": {hid: 'dnoDangFlag',type:'DEC_NO_DANG_FLAG',style:'min-width:100px'},
    "dpackTypeName": {hid: 'dpackType',type:'CIQ_DANGER_PACK_TYPE',style:'min-width:100px'},
},auto2 = {
    "cusTrafModeName": {hid: 'cusTrafMode',type:'CUS_MAPPING_TRANSPORT_CODE_V'},
    "inbordTrafModeName": {hid: 'inbordTrafMode',type:'CUS_MAPPING_TRANSPORT_CODE_V'},
    "supvModeCddeName": {hid: 'supvModeCdde',type:'CUS_MAPPING_TRADE_CODE_V'},
    "cusTradeCountryName": {hid: 'cusTradeCountry',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "cusTradeNationCodeName": {hid: 'cusTradeNationCode',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "destinationCountryName": {hid: 'destinationCountry',type:'CUS_MAPPING_COUNTRY_CODE_V',dropUp:true},
    "cusOriginCountryName": {hid: 'cusOriginCountry',type:'CUS_MAPPING_COUNTRY_CODE_V',dropUp:true},
    "bcusOriginCountryName": {hid: 'bcusOriginCountry',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "distinatePortName": {hid: 'distinatePort',type:'CUS_MAPPING_PORT_CODE_V',style:'min-width:300px'},
    "wrapTypeName": {hid: 'wrapType',type:'CUS_MAPPING_PACKAGEKIND_CODE_V'},
    "feeCurrName": {hid: 'feeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
    "insurCurrName": {hid: 'insurCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
    "otherCurrName": {hid: 'otherCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
    "tradeCurrName": {hid: 'tradeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V',dropUp:true},
    "btradeCurrName": {hid: 'btradeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'}
};
if("I"==$('#cusIEFlag').val()){
    auto1['licTypeName'] = {hid: 'licTypeCode',type:'CIQ_LICENSE_I',style:'min-width:300px'}
}else{
    auto1['licTypeName'] = {hid: 'licTypeCode',type:'CIQ_LICENSE_E',style:'min-width:300px'}
}
//修改成交数量同步法一发二数量
function sysQty() {
    if($('#gunit').val()==$('#unit1').val()){$('#qty1').val($('#gqty').val());}
    if($('#gunit').val()==$('#unit2').val() && isNotEmpty($('#unit2').val())){$('#qty2').val($('#gqty').val());}
}
function initAuto1(data){
    $.each(auto1,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                source.push({'label':o.c+'-'+o.n,'value':o.n});
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback
                });
            }
        }
    });
}
function initAuto2(data){
    $.each(auto2,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                o['label'] = o.c+'_'+o.n+'_'+o.o+'_'+o.q;
                source.push(o);
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback,
                    template:'<div>{{c}}-{{n}} <span class="fr">{{o}}&nbsp;&nbsp;{{q}}</span></div>'
                });
            }
        }
    });
}
function initAutoHsCode(data) {
    var source = [];
    $.each(data||[],function (i,o) {
        o['label'] = o.c;
        source.push(o);
    });
    if(source.length>0){
        $('#codeTs').autocompleter({
            source:source,
            style:'min-width:400px',
            limit:5,
            focusOpen:false,
            minLength:2,
            template:'<div>{{c}} <span class="fr">{{n}}</span></div>'
        });
    }
}
function initCiqAuto(data) {
    if(data.length==0){
        $('#ciqCode').val('');$('#ciqName').val('');
    }
    // else if(data.length==1){
    //     $('#ciqCode').val(data[0].code);$('#ciqName').val(data[0].name);
    // }
    var source = [],hitCode=false;
    $.each(data,function (i,o) {
        o['label'] = o.code+'_'+o.name;
        source.push(o);
        if($('#ciqCode').val()==o.code){
            $('#ciqName').val(o.name);
            hitCode=true;
        }
    });
    if(hitCode==false){
        $('#ciqCode').val('');$('#ciqName').val('');
    }
    $('#ciqName').autocompleter("destroy").autocompleter({
        source:source,
        style:'min-width:400px',
        limit:10,
        focusOpen:true,
        minLength:0,
        hiddenId:'ciqCode',
        template:'<div>{{code}} <span class="fr">{{name}}</span></div>'
    });
}
function initBaseData(){
    var declDatas1 = localStorage.getItem("static_decl_datas1");
    if(isEmpty(declDatas1)){
        $.post(rurl('base/customsData/formatData1'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_decl_datas1",JSON.stringify(res.data));
                initAuto1(res.data);
            }
        });
    }else{
        initAuto1(JSON.parse(declDatas1));
    }
    var declDatas2 = localStorage.getItem("static_decl_datas2");
    if(isEmpty(declDatas2)){
        $.post(rurl('base/customsData/formatData2'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_decl_datas2",JSON.stringify(res.data));
                initAuto2(res.data);
            }
        });
    }else{
        initAuto2(JSON.parse(declDatas2));
    }
    var hsCodeDatas = localStorage.getItem("static_code_datas");
    if(isEmpty(hsCodeDatas)){
        $.post(rurl('base/customsCode/selectAll'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_code_datas",JSON.stringify(res.data));
                initAutoHsCode(res.data);
            }
        });
    }else{
        initAutoHsCode(JSON.parse(hsCodeDatas));
    }
    var prodTrafData = localStorage.getItem("static_prod_traf_data");
    if(isEmpty(prodTrafData)){
        $.get(rurl('declare-manage/declaration/prodTrafData'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_prod_traf_data",JSON.stringify(res.data));
            }
        });
    }
}
$(function () {
    $.get(rurl('base/version'),{},function (res) {
        if(isSuccess(res)){
            if(res.data != localStorage.getItem('static_version')){
                localStorage.setItem("static_code_datas","");
                localStorage.setItem("static_decl_datas1","");
                localStorage.setItem("static_decl_datas2","");
                localStorage.setItem("static_prod_traf_data","");
                localStorage.setItem("static_version",res.data);
            }
            initBaseData();
        }
    });
});
function localData1(type) {
    var declDatas1 = localStorage.getItem("static_decl_datas1");
    if(isEmpty(declDatas1)){return [];}
    return JSON.parse(declDatas1)[type];
}
function findByLocalData1ByCode(type,code) {
    var d1 = localData1(type);
    for(var i=0;i<d1.length;i++){if(code==d1[i].c){return d1[i];}}
    return {'c':'','n':''};
}
function localData2(type) {
    var declDatas2 = localStorage.getItem("static_decl_datas2");
    if(isEmpty(declDatas2)){return [];}
    return JSON.parse(declDatas2)[type];
}
//成交方式，运保杂
function transModeControl() {
    var cusIEFlag=$('#cusIEFlag').val(),transMode = $("#transMode").val();
    if (("I"==cusIEFlag&&transMode == "1")||("E"==cusIEFlag&&transMode == "3")) {
        //进口时，成交方式是CIF/出口成交方式是FOB，则不允许录入运费和保费；
        $("#feeMark").val("");
        $("#feeMarkName").val("");
        $("#feeRate").val("");
        $("#feeCurr").val("");
        $("#feeCurrName").val("");

        $("#feeMarkName").attr("disabled", "disabled").addClass("disabled");
        $("#feeCurrName").attr("disabled", "disabled").addClass("disabled");
        $("#feeRate").attr("disabled", "disabled").addClass("disabled");

        $("#insurMark").val("");
        $("#insurMarkName").val("");
        $("#insurRate").val("");
        $("#insurCurrName").val("");
        $("#insurCurr").val("");
        $("#insurMarkName").attr("disabled", "disabled").addClass("disabled");
        $("#insurCurrName").attr("disabled", "disabled").addClass("disabled");
        $("#insurRate").attr("disabled", "disabled").addClass("disabled");
    } else if (("I"==cusIEFlag&&transMode == "2")||("E"==cusIEFlag&&transMode == "4")) {
        //进口成交方式是C&I/出口成交方式是C&F，则允许录入运费，而不允许录入保费；
        $("#feeMarkName").val("");
        $("#feeMark").val("");
        $("#feeRate").val("");
        $("#feeCurrName").val("");
        $("#feeCurr").val("");
        $("#feeMarkName").attr("disabled", "disabled").addClass("disabled");
        $("#feeCurrName").attr("disabled", "disabled").addClass("disabled");
        $("#feeRate").attr("disabled", "disabled").addClass("disabled");

        $("#insurMarkName").removeAttr("disabled", "disabled").removeClass("disabled");
        $("#insurRate").removeAttr("disabled", "disabled").removeClass("disabled");
        if($("#insurMark").val()!='1'){
            $("#insurCurrName").removeAttr("disabled", "disabled").removeClass("disabled");
        }
    } else if (("I"==cusIEFlag&&transMode == "4")||("E"==cusIEFlag&&transMode == "2")) {
        //进口成交方式是C&F/出口成交方式是C&I，则不允许录入运费，而允许录入保费；否则，运费和保费都允许录入
        $("#insurMarkName").val("");
        $("#insurMark").val("");
        $("#insurRate").val("");
        $("#insurCurrName").val("");
        $("#insurCurr").val("");
        $("#insurMarkName").attr("disabled", "disabled").addClass("disabled");
        $("#insurCurrName").attr("disabled", "disabled").addClass("disabled");
        $("#insurRate").attr("disabled", "disabled").addClass("disabled");

        $("#feeMarkName").removeAttr("disabled", "disabled").removeClass("disabled");
        if($("#feeMark").val()!='1'){
            $("#feeCurrName").removeAttr("disabled", "disabled").removeClass("disabled");
        }
        $("#feeRate").removeAttr("disabled", "disabled").removeClass("disabled");
    } else {
        $("#feeMarkName").removeAttr("disabled", "disabled").removeClass("disabled");
        $("#feeRate").removeAttr("disabled", "disabled").removeClass("disabled");
        if($("#feeMark").val()!='1'){
            $("#feeCurrName").removeAttr("disabled", "disabled").removeClass("disabled");
        }
        $("#insurMarkName").removeAttr("disabled", "disabled").removeClass("disabled");
        $("#insurRate").removeAttr("disabled", "disabled").removeClass("disabled");
        if($("#insurMark").val()!='1'){
            $("#insurCurrName").removeAttr("disabled", "disabled").removeClass("disabled");
        }
    }
}
//运保杂费标志为率时币制灰掉
function markSelect(mark, code, id) {
    if ($("#" + mark).val() == "1") {
        $("#" + code).val("");
        $("#" + id).val("");
        $("#"+id).attr("disabled", "disabled").addClass("disabled");
    }else {
        $("#" + id).removeAttr("disabled", "disabled").removeClass("disabled");
    }
}
function feeMarkSelect() {
    markSelect('feeMark','feeCurr','feeCurrName');
}
function insurMarkSelect() {
    markSelect('insurMark','insurCurr','insurCurrName');
}
function otherMarkSelect() {
    markSelect('otherMark','otherCurr','otherCurrName');
}
//特殊关系确认
function speclRlAfmCodeChange(){
    var promiseItem1=$("#promiseItem1").val();
    if("0"==promiseItem1){
        $("#promiseItem2").val("0");
        $("#promise2Name").val("否").attr("readonly", true).addClass("layui-disabled").addClass("disabled");
    }else{
        $("#promise2Name").attr("readonly", false).removeClass("layui-disabled").removeClass("disabled");
    }
}
//业务事项
function showBussinessOptions(){
    var entryType=$("#entryType").val();//报关单类型
    if("M"!=entryType){
        $("#cusRemark1").attr({"disabled":true,"checked":false});
        $("#cusRemark6").attr({"disabled":true,"checked":false});
    }else{
        $("#cusRemark1").attr("disabled",false);
        $("#cusRemark6").attr("disabled",false);
    }
    layui.form.render('checkbox');
}