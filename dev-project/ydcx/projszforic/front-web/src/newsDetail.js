layui.extend({
    com: 'lib/common',
    request: 'lib/request'
}).define(['com', 'request', 'laytpl'], function (exports) {
    var $ = layui.jquery, com = layui.com, request = layui.request, laytpl = layui.laytpl, fn = {
        init() {
            request.get('/scm/notice/info', {'id': com.getUrlParam('id')}).then(res => {
                let data = res.data;
                console.log(data);
                $('#categoryDesc').text(data.categoryDesc).attr({'href':'/news.html?t='+data.category});
                $('#newsTitle').text(data.title);
                $('#newsPublishTime').text(data.publishTime);
                $('#newsContent').html(data.content);
            });
        }
    };
    fn.init();
    exports('newsDetail', fn);
});