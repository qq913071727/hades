package com.tsfyun.scm.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.file.FileReader;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.system.entity.CustomsData;
import com.tsfyun.scm.system.enums.CustomsConstantEnum;
import com.tsfyun.scm.system.mapper.CustomsDataMapper;
import com.tsfyun.scm.system.service.ICustomsDataService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.CustomerDataS1VO;
import com.tsfyun.scm.system.vo.CustomerDataS2VO;
import com.tsfyun.scm.system.vo.CustomsDataVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 海关基础数据 服务实现类
 * </p>
 *

 * @since 2020-03-20
 */
@Slf4j
@Service
public class CustomsDataServiceImpl extends ServiceImpl<CustomsData> implements ICustomsDataService {

    @Autowired
    private CustomsDataMapper customsDataMapper;

    @Override
    public void synchroData() {
        FileReader fileReader = new FileReader("D:\\tsf\\zmark-cloud\\base-server\\src\\main\\resources\\data\\autocompAllGbSource.txt","GBK");
        String text = fileReader.readString();
//        System.out.println(text);
        List<Map<String,Object>> list = JSONObject.parseObject(text, List.class);
        System.out.println("-----------------------------1---------------------------"+list.size());
        for(Map<String,Object> map : list){
            System.out.println(map.get("paraName"));
            analysisData("1",map);
        }
        fileReader = new FileReader("D:\\tsf\\zmark-cloud\\base-server\\src\\main\\resources\\data\\autocompAllGbSource1.txt");
        text = fileReader.readString();
//        System.out.println(text);
        list = JSONObject.parseObject(text, List.class);
        System.out.println("-----------------------------2---------------------------"+list.size());
        for(Map<String,Object> map : list){
            System.out.println(map.get("paraName"));
            analysisData("2",map);
        }
        fileReader = new FileReader("D:\\tsf\\zmark-cloud\\base-server\\src\\main\\resources\\data\\autocompAllGbSource2.txt");
        text = fileReader.readString();
//        System.out.println(text);
        list = JSONObject.parseObject(text, List.class);
        System.out.println("-----------------------------3---------------------------"+list.size());
        for(Map<String,Object> map : list){
            System.out.println(map.get("paraName"));
            analysisData("2",map);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteByType(String category) {
        customsDataMapper.deleteByType(category);
    }

    @Override
    @Cacheable(value = CacheConstant.CUSTOMS_DATA_LIST+"|-1",key = "#category")
    public List<CustomerDataS1VO> formatData1ByCategory(String category) {
        return customsDataMapper.formatData1ByCategory(category);
    }
    @Override
    @Cacheable(value = CacheConstant.CUSTOMS_DATA_LIST+"|-1",key = "#category")
    public List<CustomerDataS2VO> formatData2ByCategory(String category) {
        List<CustomerDataS2VO> list = customsDataMapper.formatData2ByCategory(category);
        switch (CustomsConstantEnum.of(category)){
            case CUS_MAPPING_PACKAGEKIND_CODE_V://包装种类
            case CUS_MAPPING_COUNTRY_CODE_V://国家地区
            case CUS_MAPPING_PORT_CODE_V://港口
                list = list.stream().sorted(Comparator.comparing(CustomerDataS2VO::getC)).collect(Collectors.toList());
                break;
        }
        return list;
    }

    @CacheEvict(value = CacheConstant.CUSTOMS_DATA_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除海关数据缓存成功");
    }

    @Transactional(rollbackFor = Exception.class)
    public void analysisData(String type,Map<String,Object> map){
        CustomsConstantEnum baseDataTypeEnum = CustomsConstantEnum.of(map.get("paraName").toString());
        if(Objects.nonNull(baseDataTypeEnum)){
            if(Objects.equals(baseDataTypeEnum,CustomsConstantEnum.CUS_MAPPING_PORT_CODE_V)&&"2".equals(type)){
                baseDataTypeEnum = CustomsConstantEnum.CUS_MAPPING_PORT_CODE;//国内港口
            }
            deleteByType(baseDataTypeEnum.getCode());
            List<CustomsData> customsDataList = Lists.newArrayList();
            List<Map<String,String>> datas = (List<Map<String,String>>)map.get("paraData");
            for(int i=0;i<datas.size();i++){
                Map<String,String> data = datas.get(i);
//                System.out.println(JSONObject.toJSONString(data));
                //{"gbCode":"VEN","ciqCode":"862","codeName":"委内瑞拉","cusCode":"445","gbName":"委内瑞拉","codeValue":"VEN"}
                //codeValue 海关编码
                //codeName 编码名称
                //cusCode 原海关编码
                //ciqCode 商检编码
                CustomsData cd = new CustomsData();
                cd.setCode(StringUtils.removeSpecialSymbol(data.get("codeValue")));
                cd.setCategory(baseDataTypeEnum.getCode());
                cd.setName(StringUtils.removeSpecialSymbol(data.get("codeName")));
                cd.setCusCode(StringUtils.removeSpecialSymbol(data.get("cusCode")));
                cd.setCiqCode(StringUtils.removeSpecialSymbol(data.get("ciqCode")));
                cd.setId(String.format("%s-%s",cd.getCategory(),cd.getCode()));
                customsDataList.add(cd);
            }
            if(CollUtil.isNotEmpty(customsDataList)){
                customsDataMapper.saveBatch(customsDataList);
            }
        }
    }


}
