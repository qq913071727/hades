package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.system.dto.CountryDTO;
import com.tsfyun.scm.system.entity.Country;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.CountryVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-19
 */
public interface ICountryService extends IService<Country> {


    /**
     * 获取国家列表
     * @return
     */
    public List<CountryVO> select ( );

    /**
     * 清楚缓存
     */
    public void clear( );


    /**
     * 分页查询国家信息
     * @param dto
     * @return
     */
    PageInfo<Country> page(CountryDTO dto);

    /**
     * 国家详情
     * @param id
     * @return
     */
    CountryVO detail(String id);

}
