package com.tsfyun.scm.system.controller;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping(value = "/support")
public class SupportController extends BaseController {

    @Value("${saas.manage.salt:qwert12345}")
    private String salt;

    @Autowired
    private RedisTemplate redisTemplate;

    @ApiOperation(value = "缓存token")
    @PostMapping(value = "cacheTsfToken")
    public Result<String> cacheTsfToken(@RequestHeader(value = "accept-salt")String requestSalt,
                                       @RequestParam(value = "token")String token){
        TsfPreconditions.checkArgument(Objects.equals(salt,requestSalt),new ServiceException("请求非法"));
        redisTemplate.opsForValue().set("AGENT_SINGLE_WINDOW:TOKEN",token,24, TimeUnit.HOURS);
        return Result.success("SUCCESS");
    }

    @ApiOperation(value = "缓存token")
    @PostMapping(value = "cacheScmToken")
    public Result<String> cacheScmToken(@RequestHeader(value = "accept-salt")String requestSalt,
                                       @RequestParam(value = "token")String token){
        TsfPreconditions.checkArgument(Objects.equals(salt,requestSalt),new ServiceException("请求非法"));
        redisTemplate.opsForValue().set("SINGLE_WINDOW:TOKEN",token,24, TimeUnit.HOURS);
        return Result.success("SUCCESS");
    }

}
