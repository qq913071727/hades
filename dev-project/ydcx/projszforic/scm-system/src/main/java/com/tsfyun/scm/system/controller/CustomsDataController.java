package com.tsfyun.scm.system.controller;


import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Maps;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.enums.CustomsConstantEnum;
import com.tsfyun.scm.system.service.ICustomsDataService;
import com.tsfyun.scm.system.vo.CustomerDataS1VO;
import com.tsfyun.scm.system.vo.CustomerDataS2VO;
import com.tsfyun.scm.system.vo.CustomsDataVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 海关基础数据 前端控制器
 * </p>
 *

 * @since 2020-03-20
 */
@RestController
@RequestMapping("/customsData")
public class CustomsDataController extends BaseController {

    @Autowired
    private ICustomsDataService customsDataService;

    @ApiOperation(value = "数据同步")
    @PostMapping(value = "synchroData")
    public Result<Void> synchroData(){
        customsDataService.synchroData();
        return Result.success();
    }

    private static final List<String> type1s = Arrays.asList(
            CustomsConstantEnum.CUS_CUSTOMS.getCode(),//海关
            CustomsConstantEnum.CUS_TRANSAC.getCode(),//成交方式
            CustomsConstantEnum.DEC_FEE_MARK.getCode(),//运费率
            CustomsConstantEnum.DEC_INSUR_MARK.getCode(),//保费率
            CustomsConstantEnum.DEC_OTHER_MARK.getCode(),//杂费率
            CustomsConstantEnum.CUS_UNIT.getCode(),//单位
            CustomsConstantEnum.CIQ_PORT_CN.getCode(),//口岸
            CustomsConstantEnum.DEC_ENTRY_TYPE.getCode(),//报关单类型
            CustomsConstantEnum.CIQ_CUS_ORGANIZATION.getCode(),//检验检机关
            CustomsConstantEnum.CUS_LEVYTYPE.getCode(),//征免性质
            CustomsConstantEnum.CUS_LEVYMODE.getCode(),//征免方式
            CustomsConstantEnum.CIQ_USE.getCode(),//用途
            CustomsConstantEnum.CUS_DISTRICT.getCode(),//境内目的地
            CustomsConstantEnum.CUS_MAPPING_PORT_CODE.getCode(),//国内港口
            CustomsConstantEnum.CIQ_CORRELATION_REASON.getCode(),//关联理由
            CustomsConstantEnum.DEC_ORIG_BOX_FLAG.getCode(),//原箱运输
            CustomsConstantEnum.CIQ_ORIGIN_PLACE.getCode(),//原产地区
            CustomsConstantEnum.CUS_MAPPING_NORMS_CODE_V.getCode(),//集装箱规格
            CustomsConstantEnum.DEC_1_0_FLAG.getCode(),//拼箱标识
            CustomsConstantEnum.CUS_LICENSEDOCU.getCode(),//随附单证代码
            CustomsConstantEnum.CIQ_CITY_CN.getCode(),//国内地区
            CustomsConstantEnum.DEC_SPECL_RL_AFM_CODE.getCode(),//特殊关系确认
            CustomsConstantEnum.CIQ_APTT_NAME_I.getCode(),//企业资质
            CustomsConstantEnum.BRAND_TYPE.getCode(),//品牌类型
            CustomsConstantEnum.EXP_DISCONUNT.getCode(),//出口享惠情况
            CustomsConstantEnum.DECL_TRN_REL.getCode(),//转关提前报关
            CustomsConstantEnum.TRAN_FLAG.getCode(),//转关类型
            CustomsConstantEnum.DEC_TRANS_TYPE.getCode(),//转关单申报类型
            CustomsConstantEnum.EDIID_TYPE.getCode(),//报关标志
            CustomsConstantEnum.E_SEAL_FLAG.getCode(),//启用电子关锁标志
            CustomsConstantEnum.DEC_ATT_TYPE_CODE_V.getCode(),//随附单据类型
            CustomsConstantEnum.CIQ_LICENSE_I.getCode(),//许可证类别（进口）
            CustomsConstantEnum.CIQ_LICENSE_E.getCode(),//许可证类别(出口)
            CustomsConstantEnum.DEC_NO_DANG_FLAG.getCode(),//非危险化学品
            CustomsConstantEnum.CIQ_DANGER_PACK_TYPE.getCode(),//危包类别
            CustomsConstantEnum.REQ_CONTENT.getCode(),//证书信息

            CustomsConstantEnum.CUS_MFT8_ROAD_CUSTOMS_STATUS.getCode(),//海关货物通关代码
            CustomsConstantEnum.CUS_MFT8_ROAD_PAYMENT_METHOD.getCode(),//运费支付方法
            CustomsConstantEnum.CUS_MFT8_PACKAGING.getCode(),//包装种类
            CustomsConstantEnum.CUS_MFT8_CONTR_CAR_COND.getCode(),//运输条款
            CustomsConstantEnum.CUS_MFT8_DAN_GOODS.getCode(),//危险品编号
            CustomsConstantEnum.CUS_MFT8_ROAD_CURR.getCode(),//金额类型

            CustomsConstantEnum.VEH_PAY_WAY.getCode(),//支付方式
            CustomsConstantEnum.VEH_ORDER_TYPE.getCode(),//订单类型
            CustomsConstantEnum.VEH_ORDER_SEND.getCode(),//订单发送单位
            CustomsConstantEnum.VEH_INVT_SEND.getCode(),//清单发送单位
            CustomsConstantEnum.VEH_DEC_FLAG.getCode(),//申报业务类型
            CustomsConstantEnum.VEH_PACKAGING.getCode()//包装种类
    );
    private static final List<String> type2s = Arrays.asList(
            CustomsConstantEnum.CUS_MAPPING_COUNTRY_CODE_V.getCode(),//国家
            CustomsConstantEnum.CUS_MAPPING_TRANSPORT_CODE_V.getCode(),//运输方式
            CustomsConstantEnum.CUS_MAPPING_CURRENCY_CODE_V.getCode(),//币制
            CustomsConstantEnum.CUS_MAPPING_TRADE_CODE_V.getCode(),//监管方式
            CustomsConstantEnum.CUS_MAPPING_PORT_CODE_V.getCode(),//港口
            CustomsConstantEnum.CUS_MAPPING_PACKAGEKIND_CODE_V.getCode()//包装种类
    );

    @PostMapping(value = "formatData1")
    public Result<Map<String,List<CustomerDataS1VO>>> formatData1(){
        Map<String,List<CustomerDataS1VO>> map = Maps.newHashMap();
        type1s.stream().forEach(category ->{
            List<CustomerDataS1VO> list = customsDataService.formatData1ByCategory(category);
            if(CollUtil.isNotEmpty(list)){
                map.put(category,list);
            }
        });
        return Result.success(map);
    }

    @PostMapping(value = "formatData2")
    public Result<Map<String,List<CustomerDataS2VO>>> formatData2(){
        Map<String,List<CustomerDataS2VO>> map = Maps.newHashMap();
        type2s.stream().forEach(category ->{
            List<CustomerDataS2VO> list = customsDataService.formatData2ByCategory(category);
            if(CollUtil.isNotEmpty(list)){
                map.put(category,list);
            }
        });
        return Result.success(map);
    }
}

