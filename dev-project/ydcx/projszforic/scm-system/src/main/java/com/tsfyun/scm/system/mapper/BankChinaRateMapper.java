package com.tsfyun.scm.system.mapper;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.dto.BankChinaRateDTO;
import com.tsfyun.scm.system.entity.BankChinaRate;
import com.tsfyun.scm.system.vo.PeriodBankChinaRateVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-17
 */
@Repository
public interface BankChinaRateMapper extends Mapper<BankChinaRate> {

    /**
     * 修改
     * @param dto
     */
    void updateRate(@Param("dto") BankChinaRateDTO dto);

    //根据币制和日期获取当日最晚的一条汇率
    BankChinaRate findLastOne(@Param(value = "currencyCode")String currencyCode,@Param(value = "date")String date);

    //批量保存数据
    int batchInsert(@Param(value = "bankChinaRates") List<BankChinaRate> bankChinaRates);

    //根据币制和时间查询最接近的一个汇率
    BankChinaRate selectRate(@Param(value = "currencyCode")String currencyCode,@Param(value = "dateTime")String dateTime);

    //获取从开始时间到现在某个时间点最近的数据，固定搜索09:00:00到12:00:00时间范围内的
    List<PeriodBankChinaRateVO> getPeriodRates(@Param(value = "rateTime")String rateTime, @Param(value = "startTime")String startTime);

    //获取某日的汇率（所有币制）
    List<PeriodBankChinaRateVO> getCurrentDateRates(@Param(value = "rateTime")String rateTime, @Param(value = "rateDate")String rateDate);
}
